package com.globus.valueobject.quality;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;
import java.util.Date;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmEventEvalVO extends GmDataStoreVO { 
	
	private String partnm = "";
	private String partid = "";
	private String lotnumber = "";
	private String qty = "";
	private String typeofsale = "";
	private String descr = "";

    private String evnthappen = "";
    private String rptcond = "";
    private String revsurg = "";
    private String revsurgtxt = "";
    private String advefct = "";
    private String typofsurg = "";
    private String surg = "";
    private String hospital = "";
    private String postoprt = "";
    private String prcout = "";
    private String replacement = "";
    private String partaval = "";
    private String shipped = "";
    private String decontaminated = "";
    private String carrier = "";
    private String trknumber = "";
    private String fldservice = "";
    private String repid="";
    private String viodfl="";
    private String fromdate="";
    private String todate="";
    private String status="";
    private String createdate="";
    private String evalformid="";
    private String loanerfl="";
    private String consignmentfl="";
    private String soldfl="";
    private String repnm="";
    private String compnm="";
    private String repnum="";
    private String repaddress="";
    private String stropt="";



	private String dateshipped = null;
    private String  evntdt = null;
    private String  orgdate = null;
    
    private String mstatus = "";
    private String mailstatus = "";
     
    
    List<GmEventEvalVO> listGmEvalFormVO = null;
    
    public String getStropt() {
		return stropt;
	}
	public void setStropt(String stropt) {
		this.stropt = stropt;
	}


	/**
	 * @return the partnm
	 */
	public String getPartnm() {
		return partnm;
	}







	/**
	 * @param partnm the partnm to set
	 */
	public void setPartnm(String partnm) {
		this.partnm = partnm;
	}







	/**
	 * @return the partid
	 */
	public String getPartid() {
		return partid;
	}







	/**
	 * @param partid the partid to set
	 */
	public void setPartid(String partid) {
		this.partid = partid;
	}







	/**
	 * @return the lotnumber
	 */
	public String getLotnumber() {
		return lotnumber;
	}







	/**
	 * @param lotnumber the lotnumber to set
	 */
	public void setLotnumber(String lotnumber) {
		this.lotnumber = lotnumber;
	}







	/**
	 * @return the qty
	 */
	public String getQty() {
		return qty;
	}







	/**
	 * @param qty the qty to set
	 */
	public void setQty(String qty) {
		this.qty = qty;
	}







	/**
	 * @return the typeofsale
	 */
	public String getTypeofsale() {
		return typeofsale;
	}







	/**
	 * @param typeofsale the typeofsale to set
	 */
	public void setTypeofsale(String typeofsale) {
		this.typeofsale = typeofsale;
	}







	/**
	 * @return the descr
	 */
	public String getDescr() {
		return descr;
	}







	/**
	 * @param descr the descr to set
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}







	/**
	 * @return the evnthappen
	 */
	public String getEvnthappen() {
		return evnthappen;
	}







	/**
	 * @param evnthappen the evnthappen to set
	 */
	public void setEvnthappen(String evnthappen) {
		this.evnthappen = evnthappen;
	}







	/**
	 * @return the rptcond
	 */
	public String getRptcond() {
		return rptcond;
	}







	/**
	 * @param rptcond the rptcond to set
	 */
	public void setRptcond(String rptcond) {
		this.rptcond = rptcond;
	}







	/**
	 * @return the revsurg
	 */
	public String getRevsurg() {
		return revsurg;
	}







	/**
	 * @param revsurg the revsurg to set
	 */
	public void setRevsurg(String revsurg) {
		this.revsurg = revsurg;
	}







	/**
	 * @return the revsurgtxt
	 */
	public String getRevsurgtxt() {
		return revsurgtxt;
	}







	/**
	 * @param revsurgtxt the revsurgtxt to set
	 */
	public void setRevsurgtxt(String revsurgtxt) {
		this.revsurgtxt = revsurgtxt;
	}







	/**
	 * @return the advefct
	 */
	public String getAdvefct() {
		return advefct;
	}







	/**
	 * @param advefct the advefct to set
	 */
	public void setAdvefct(String advefct) {
		this.advefct = advefct;
	}







	/**
	 * @return the typofsurg
	 */
	public String getTypofsurg() {
		return typofsurg;
	}







	/**
	 * @param typofsurg the typofsurg to set
	 */
	public void setTypofsurg(String typofsurg) {
		this.typofsurg = typofsurg;
	}







	/**
	 * @return the surg
	 */
	public String getSurg() {
		return surg;
	}







	/**
	 * @param surg the surg to set
	 */
	public void setSurg(String surg) {
		this.surg = surg;
	}







	/**
	 * @return the hospital
	 */
	public String getHospital() {
		return hospital;
	}







	/**
	 * @param hospital the hospital to set
	 */
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}







	/**
	 * @return the postoprt
	 */
	public String getPostoprt() {
		return postoprt;
	}







	/**
	 * @param postoprt the postoprt to set
	 */
	public void setPostoprt(String postoprt) {
		this.postoprt = postoprt;
	}







	/**
	 * @return the prcout
	 */
	public String getPrcout() {
		return prcout;
	}







	/**
	 * @param prcout the prcout to set
	 */
	public void setPrcout(String prcout) {
		this.prcout = prcout;
	}







	/**
	 * @return the replacement
	 */
	public String getReplacement() {
		return replacement;
	}







	/**
	 * @param replacement the replacement to set
	 */
	public void setReplacement(String replacement) {
		this.replacement = replacement;
	}







	/**
	 * @return the partaval
	 */
	public String getPartaval() {
		return partaval;
	}







	/**
	 * @param partaval the partaval to set
	 */
	public void setPartaval(String partaval) {
		this.partaval = partaval;
	}







	/**
	 * @return the shipped
	 */
	public String getShipped() {
		return shipped;
	}







	/**
	 * @param shipped the shipped to set
	 */
	public void setShipped(String shipped) {
		this.shipped = shipped;
	}







	/**
	 * @return the decontaminated
	 */
	public String getDecontaminated() {
		return decontaminated;
	}







	/**
	 * @param decontaminated the decontaminated to set
	 */
	public void setDecontaminated(String decontaminated) {
		this.decontaminated = decontaminated;
	}







	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}







	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}







	/**
	 * @return the trknumber
	 */
	public String getTrknumber() {
		return trknumber;
	}







	/**
	 * @param trknumber the trknumber to set
	 */
	public void setTrknumber(String trknumber) {
		this.trknumber = trknumber;
	}







	/**
	 * @return the fldservice
	 */
	public String getFldservice() {
		return fldservice;
	}







	/**
	 * @param fldservice the fldservice to set
	 */
	public void setFldservice(String fldservice) {
		this.fldservice = fldservice;
	}







	/**
	 * @return the repid
	 */
	public String getRepid() {
		return repid;
	}







	/**
	 * @param repid the repid to set
	 */
	public void setRepid(String repid) {
		this.repid = repid;
	}







	/**
	 * @return the viodfl
	 */
	public String getViodfl() {
		return viodfl;
	}







	/**
	 * @param viodfl the viodfl to set
	 */
	public void setViodfl(String viodfl) {
		this.viodfl = viodfl;
	}







	/**
	 * @return the fromdate
	 */
	public String getFromdate() {
		return fromdate;
	}







	/**
	 * @param fromdate the fromdate to set
	 */
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}







	/**
	 * @return the todate
	 */
	public String getTodate() {
		return todate;
	}







	/**
	 * @param todate the todate to set
	 */
	public void setTodate(String todate) {
		this.todate = todate;
	}







	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}







	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}







	/**
	 * @return the createdate
	 */
	public String getCreatedate() {
		return createdate;
	}







	/**
	 * @param createdate the createdate to set
	 */
	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}







	






	/**
	 * @return the evalformid
	 */
	public String getEvalformid() {
		return evalformid;
	}







	/**
	 * @param evalformid the evalformid to set
	 */
	public void setEvalformid(String evalformid) {
		this.evalformid = evalformid;
	}







	/**
	 * @return the dateshipped
	 */
	public String getDateshipped() {
		return dateshipped;
	}







	/**
	 * @param dateshipped the dateshipped to set
	 */
	public void setDateshipped(String dateshipped) {
		this.dateshipped = dateshipped;
	}







	/**
	 * @return the evntdt
	 */
	public String getEvntdt() {
		return evntdt;
	}







	/**
	 * @param evntdt the evntdt to set
	 */
	public void setEvntdt(String evntdt) {
		this.evntdt = evntdt;
	}







	/**
	 * @return the orgdate
	 */
	public String getOrgdate() {
		return orgdate;
	}







	/**
	 * @param orgdate the orgdate to set
	 */
	public void setOrgdate(String orgdate) {
		this.orgdate = orgdate;
	}







	/**
	 * @return the listGmEvalFormVO
	 */
	public List<GmEventEvalVO> getListGmEvalFormVO() {
		return listGmEvalFormVO;
	}







	/**
	 * @param listGmEvalFormVO the listGmEvalFormVO to set
	 */
	public void setListGmEvalFormVO(List<GmEventEvalVO> listGmEvalFormVO) {
		this.listGmEvalFormVO = listGmEvalFormVO;
	}







	/**
	 * @return the loanerfl
	 */
	public String getLoanerfl() {
		return loanerfl;
	}







	/**
	 * @param loanerfl the loanerfl to set
	 */
	public void setLoanerfl(String loanerfl) {
		this.loanerfl = loanerfl;
	}







	/**
	 * @return the consignmentfl
	 */
	public String getConsignmentfl() {
		return consignmentfl;
	}







	/**
	 * @param consignmentfl the consignmentfl to set
	 */
	public void setConsignmentfl(String consignmentfl) {
		this.consignmentfl = consignmentfl;
	}







	/**
	 * @return the soldfl
	 */
	public String getSoldfl() {
		return soldfl;
	}







	/**
	 * @param soldfl the soldfl to set
	 */
	public void setSoldfl(String soldfl) {
		this.soldfl = soldfl;
	}







	/**
	 * @return the repnm
	 */
	public String getRepnm() {
		return repnm;
	}







	/**
	 * @param repnm the repnm to set
	 */
	public void setRepnm(String repnm) {
		this.repnm = repnm;
	}







	/**
	 * @return the compnm
	 */
	public String getCompnm() {
		return compnm;
	}







	/**
	 * @param compnm the compnm to set
	 */
	public void setCompnm(String compnm) {
		this.compnm = compnm;
	}







	/**
	 * @return the mstatus
	 */
	public String getMstatus() {
		return mstatus;
	}







	/**
	 * @param mstatus the mstatus to set
	 */
	public void setMstatus(String mstatus) {
		this.mstatus = mstatus;
	}







	/**
	 * @return the mailstatus
	 */
	public String getMailstatus() {
		return mailstatus;
	}







	/**
	 * @param mailstatus the mailstatus to set
	 */
	public void setMailstatus(String mailstatus) {
		this.mailstatus = mailstatus;
	}







	/**
	 * @return the repnum
	 */
	public String getRepnum() {
		return repnum;
	}







	/**
	 * @param repnum the repnum to set
	 */
	public void setRepnum(String repnum) {
		this.repnum = repnum;
	}







	/**
	 * @return the repaddress
	 */
	public String getRepaddress() {
		return repaddress;
	}







	/**
	 * @param repaddress the repaddress to set
	 */
	public void setRepaddress(String repaddress) {
		this.repaddress = repaddress;
	}
	

    

	


	@Transient
	  public Properties getMappingProperties() {
	    Properties properties = new Properties();
	    properties.put("PARTID", "PARTID");
	    properties.put("PARTNM", "PARTNM");
	    properties.put("LOTNUMBER", "LOTNUMBER");
	    properties.put("QTY", "QTY");
	    properties.put("TYPEOFSALE", "TYPEOFSALE");
	    properties.put("DESCR", "DESCR");
	    properties.put("EVNTDT", "EVNTDT");
	    properties.put("EVNTHAPPEN", "EVNTHAPPEN");
	    properties.put("RPTCOND", "RPTCOND");
	    properties.put("REVSURG", "REVSURG");
	    properties.put("REVSURGTXT", "REVSURGTXT");
	    properties.put("ADVEFCT", "ADVEFCT");
	    properties.put("TYPOFSURG", "TYPOFSURG");
	    properties.put("SURG", "SURG");
	    properties.put("HOSPITAL", "HOSPITAL");
	    properties.put("POSTOPRT", "POSTOPRT");
	    properties.put("ORGDATE", "ORGDATE");
	    properties.put("PRCOUT", "PRCOUT");
	    properties.put("REPLACEMENT", "REPLACEMENT");
	    properties.put("PARTAVAL", "PARTAVAL");
	    properties.put("SHIPPED", "SHIPPED");
	    properties.put("DECONTAMINATED", "DECONTAMINATED");
	    properties.put("DATESHIPPED", "DATESHIPPED");
	    properties.put("CARRIER", "CARRIER");
	    properties.put("TRKNUMBER", "TRKNUMBER");
	    properties.put("FLDSERVICE", "FLDSERVICE");
	    properties.put("EVALFORMID", "EVALFORMID");
	    properties.put("CREATEDATE", "CREATEDATE");
	    properties.put("STATUS", "STATUS");
	    properties.put("REPNM", "REPNM");
	    properties.put("COMPNM", "COMPNM");	 
	    properties.put("LOANERFL", "LOANERFL");	 
	    properties.put("CONSIGNMENTFL", "CONSIGNMENTFL");	 
	    properties.put("SOLDFL", "SOLDFL");	 
	    properties.put("STROPT", "STROPT");	 
	    properties.put("MSTATUS", "MSTATUS");	 
	    properties.put("MAILSTATUS", "MAILSTATUS");	 	 
	    return properties;
	  }

	
	
	
}
 