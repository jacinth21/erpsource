package com.globus.valueobject.crm.merc;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCRMMercPartyVO extends GmDataStoreVO {

	private String attendeeid = "";
	private String mercid = "";
	private String partyid = "";
	private String statusid = "";
	private String roleid = "";
	private String voidfl = "";
	
	/**
	 * @return the attendeeid
	 */
	public String getAttendeeid() {
		return attendeeid;
	}
	
	/**
	 * @param attendeeid the attendeeid to set
	 */
	public void setAttendeeid(String attendeeid) {
		this.attendeeid = attendeeid;
	}
	
	/**
	 * @return the mercid
	 */
	public String getMercid() {
		return mercid;
	}
	
	/**
	 * @param mercid the mercid to set
	 */
	public void setMercid(String mercid) {
		this.mercid = mercid;
	}
	
	/**
	 * @return the partyid
	 */
	public String getPartyid() {
		return partyid;
	}
	
	/**
	 * @param partyid the partyid to set
	 */
	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}
	
	/**
	 * @return the statusid
	 */
	public String getStatusid() {
		return statusid;
	}
	
	/**
	 * @param statusid the statusid to set
	 */
	public void setStatusid(String statusid) {
		this.statusid = statusid;
	}
	
	/**
	 * @return the roleid
	 */
	public String getRoleid() {
		return roleid;
	}
	
	/**
	 * @param roleid the roleid to set
	 */
	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	
	/**
	 * @return the voidfl
	 */
	public String getVoidfl() {
		return voidfl;
	}
	
	/**
	 * @param voidfl the voidfl to set
	 */
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

	
}
