package com.globus.valueobject.crm.merc;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmCRMMercVO")
public class GmCRMMercVO extends GmDataStoreVO {

	private String regnid = "";
	private String mercid = "";
	private String attendeefl = "";
	private String fromdt = "";
	private String todt = "";
	private String title = "";
	private String eventnm = "";
	private String region = "";
	private String type = "";
	private String city = "";
	private String state = "";
	private String country = "";
	private String url = "";
	private String opento = "";
	private String language = "";
	private String status = "";
	private String firstnm = "";
	private String lastnm = "";
    private String voidfl = "";  
	
	private List<GmCRMMercPartyVO> arrgmmercpartyvo = null;
	
	/**
	 * @return the arrgmmercpartyvo
	 */
	public List<GmCRMMercPartyVO> getArrgmmercpartyvo() {
		return arrgmmercpartyvo;
	}
	
	/**
	 * @param arrgmmercpartyvo the arrgmmercpartyvo to set
	 */
	public void setArrgmmercpartyvo(List<GmCRMMercPartyVO> arrgmmercpartyvo) {
		this.arrgmmercpartyvo = arrgmmercpartyvo;
	}
	/**
	 * @return the firstnm
	 */
	public String getFirstnm() {
		return firstnm;
	}
	/**
	 * @param firstnm the firstnm to set
	 */
	public void setFirstnm(String firstnm) {
		this.firstnm = firstnm;
	}
	/**
	 * @return the lastnm
	 */
	public String getLastnm() {
		return lastnm;
	}
	/**
	 * @param lastnm the lastnm to set
	 */
	public void setLastnm(String lastnm) {
		this.lastnm = lastnm;
	}
	/**
	 * @return the regnid
	 */
	public String getRegnid() {
		return regnid;
	}
	/**
	 * @param regnid the regnid to set
	 */
	public void setRegnid(String regnid) {
		this.regnid = regnid;
	}
	/**
	 * @return the mercid
	 */
	public String getMercid() {
		return mercid;
	}
	/**
	 * @param mercid the mercid to set
	 */
	public void setMercid(String mercid) {
		this.mercid = mercid;
	}
	/**
	 * @return the attendeefl
	 */
	public String getAttendeefl() {
		return attendeefl;
	}
	/**
	 * @param attendeefl the attendeefl to set
	 */
	public void setAttendeefl(String attendeefl) {
		this.attendeefl = attendeefl;
	}
	  
	/**
	 * @return the fromdt
	 */
	public String getFromdt() {
		return fromdt;
	}

	/**
	 * @param fromdt the fromdt to set
	 */
	public void setFromdt(String fromdt) {
		this.fromdt = fromdt;
	}

	/**
	 * @return the todt
	 */
	public String getTodt() {
		return todt;
	}

	/**
	 * @param todt the todt to set
	 */
	public void setTodt(String todt) {
		this.todt = todt;
	}

	/**
	 * @return the name
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param name the name to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getEventnm() {
		return eventnm;
	}

	public void setEventnm(String eventnm) {
		this.eventnm = eventnm;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getOpento() {
		return opento;
	}

	public void setOpento(String opento) {
		this.opento = opento;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
    public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	} 
	@Transient
	public Properties getMappingProperties() {
		Properties properties = new Properties();
		properties.put("REGNID", "REGNID");
		properties.put("MERCID", "MERCID");
		properties.put("ATTENDEEFL", "ATTENDEEFL");
		properties.put("FROMDT", "FROMDT");
		properties.put("TODT", "TODT");
		properties.put("TITLE", "TITLE");
		properties.put("EVENTNM", "EVENTNM");
		properties.put("REGION", "REGION");
		properties.put("TYPE", "TYPE");
		properties.put("CITY", "CITY");
		properties.put("STATE", "STATE");
		properties.put("COUNTRY", "COUNTRY");
		properties.put("URL", "URL");
		properties.put("OPENTO", "OPENTO");
		properties.put("LANGUAGE", "LANGUAGE");
		properties.put("STATUS", "STATUS");
		return properties;
	}

}
