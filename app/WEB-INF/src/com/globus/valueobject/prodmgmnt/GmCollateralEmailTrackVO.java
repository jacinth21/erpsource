package com.globus.valueobject.prodmgmnt;

import java.util.List; 

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.globus.valueobject.common.GmDataStoreVO;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCollateralEmailTrackVO extends GmDataStoreVO {
	private String startdate;
	private String enddate;
	private String eventtype;
	private String emaildetailsid;
	private String emailid;
	private String contenttitle;
	/**
	 * @return the startdate
	 */
	public String getStartdate() {
		return startdate;
	}
	/**
	 * @param startdate the startdate to set
	 */
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	/**
	 * @return the enddate
	 */
	public String getEnddate() {
		return enddate;
	}
	/**
	 * @param enddate the enddate to set
	 */
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	/**
	 * @return the eventtype
	 */
	public String getEventtype() {
		return eventtype;
	}
	/**
	 * @param eventtype the eventtype to set
	 */
	public void setEventtype(String eventtype) {
		this.eventtype = eventtype;
	}
	/**
	 * @return the emailid
	 */
	public String getEmailid() {
		return emailid;
	}
	/**
	 * @param emailid the emailid to set
	 */
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	/**
	 * @return the emaildetailsid
	 */
	public String getEmaildetailsid() {
		return emaildetailsid;
	}
	/**
	 * @param emaildetailsid the emaildetailsid to set
	 */
	public void setEmaildetailsid(String emaildetailsid) {
		this.emaildetailsid = emaildetailsid;
	}
	/**
	 * @return the contenttitle
	 */
	public String getContenttitle() {
		return contenttitle;
	}
	/**
	 * @param contenttitle the contenttitle to set
	 */
	public void setContenttitle(String contenttitle) {
		this.contenttitle = contenttitle;
	}
	
}