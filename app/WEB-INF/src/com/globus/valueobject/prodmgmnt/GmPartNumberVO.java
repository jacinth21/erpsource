package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartNumberVO extends GmDataTstVO {
  private String partnum = "";
  private String partdesc = "";
  private String partdtldesc = "";
  private String partstat = "";
  private String partfmly = "";
  private String partactfl = "";
  private String systemid = "";
  private String productmaterial = "";
  private String productclass = "";
  private String dinumber = "";
  private String udipattern = "";


  @Transient
  public Properties getPartNumberProperties() {
    Properties properties = new Properties();
    properties.put("PARTNUM", "PARTNUM");
    properties.put("PARTDESC", "PARTDESC");
    properties.put("PARTDTLDESC", "PARTDTLDESC");
    properties.put("PARTSTAT", "PARTSTAT");
    properties.put("PARTFMLY", "PARTFMLY");
    properties.put("PARTACTFL", "PARTACTFL");
    properties.put("SYSTEMID", "SYSTEMID");
    properties.put("PRODMAT", "PRODUCTMATERIAL");
    properties.put("PRODCLS", "PRODUCTCLASS");
    properties.put("DINUMBER", "DINUMBER");
    properties.put("UDIPATTERN", "UDIPATTERN");
    properties.put("TOTALPAGES", "TOTALPAGES");
    properties.put("PAGENO", "PAGENO");
    properties.put("TOTALSIZE", "TOTALSIZE");
    properties.put("PAGESIZE", "PAGESIZE");
    return properties;
  }



  public String getPartnum() {
    return partnum;
  }



  public void setPartnum(String partnum) {
    this.partnum = partnum;
  }



  public String getPartdesc() {
    return partdesc;
  }



  public void setPartdesc(String partdesc) {
    this.partdesc = partdesc;
  }



  public String getPartdtldesc() {
    return partdtldesc;
  }



  public void setPartdtldesc(String partdtldesc) {
    this.partdtldesc = partdtldesc;
  }



  public String getPartstat() {
    return partstat;
  }



  public void setPartstat(String partstat) {
    this.partstat = partstat;
  }

  public String getPartfmly() {
    return partfmly;
  }



  public void setPartfmly(String partfmly) {
    this.partfmly = partfmly;
  }



  public String getPartactfl() {
    return partactfl;
  }



  public void setPartactfl(String partactfl) {
    this.partactfl = partactfl;
  }



  public String getSystemid() {
    return systemid;
  }



  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }



  public String getProductmaterial() {
    return productmaterial;
  }



  public void setProductmaterial(String productmaterial) {
    this.productmaterial = productmaterial;
  }



  public String getProductclass() {
    return productclass;
  }



  public void setProductclass(String productclass) {
    this.productclass = productclass;
  }



  /**
   * getter method to get the dinumber
   * 
   * @return the dinumber
   */
  public String getDinumber() {
    return dinumber;
  }



  /**
   * setter method to set the dinumber
   * 
   * @param dinumber the dinumber to set
   */
  public void setDinumber(String dinumber) {
    this.dinumber = dinumber;
  }



  /**
   * getter method to get the udipattern
   * 
   * @return the udipattern
   */
  public String getUdipattern() {
    return udipattern;
  }



  /**
   * setter method to set the udipattern
   * 
   * @param udipattern the udipattern to set
   */
  public void setUdipattern(String udipattern) {
    this.udipattern = udipattern;
  }

}
