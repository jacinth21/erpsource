package com.globus.valueobject.prodmgmnt;

import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmShareListVO extends GmDataStoreVO{
	
	private String firstnm="";
	private String lastnm="";
	private String emailid="";
	private String emailfromdate="";
	private String emailtodate="";
	private String partytype="";
	private List<GmShareEngineVO> listGmShareEngineVO =null;
	
	public String getFirstnm() {
		return firstnm;
	}
	public void setFirstnm(String firstnm) {
		this.firstnm = firstnm;
	}
	public String getLastnm() {
		return lastnm;
	}
	public void setLastnm(String lastnm) {
		this.lastnm = lastnm;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getEmailfromdate() {
		return emailfromdate;
	}
	public void setEmailfromdate(String emailfromdate) {
		this.emailfromdate = emailfromdate;
	}
	public String getEmailtodate() {
		return emailtodate;
	}
	public void setEmailtodate(String emailtodate) {
		this.emailtodate = emailtodate;
	}
	public String getPartytype() {
		return partytype;
	}
	public void setPartytype(String partytype) {
		this.partytype = partytype;
	}
	public List<GmShareEngineVO> getListGmShareEngineVO() {
		return listGmShareEngineVO;
	}
	public void setListGmShareEngineVO(List<GmShareEngineVO> listGmShareEngineVO) {
		this.listGmShareEngineVO = listGmShareEngineVO;
	}
	
}
