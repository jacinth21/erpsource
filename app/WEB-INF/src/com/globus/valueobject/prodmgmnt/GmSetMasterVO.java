package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSetMasterVO extends GmDataTstVO {
  private String setid = "";
  private String setnm = "";
  private String setdesc = "";
  private String setdtldesc = "";
  private String systemid = "";
  private String setcategoryid = "";
  private String settypeid = "";
  private String sethierarchyid = "";
  private String setstatusid = "";
  private String voidfl = "";

  /**
   * @return the setid
   */
  public String getSetid() {
    return setid;
  }

  /**
   * @param setid the setid to set
   */
  public void setSetid(String setid) {
    this.setid = setid;
  }

  /**
   * @return the setnm
   */
  public String getSetnm() {
    return setnm;
  }

  /**
   * @param setnm the setnm to set
   */
  public void setSetnm(String setnm) {
    this.setnm = setnm;
  }

  /**
   * @return the setdesc
   */
  public String getSetdesc() {
    return setdesc;
  }

  /**
   * @param setdesc the setdesc to set
   */
  public void setSetdesc(String setdesc) {
    this.setdesc = setdesc;
  }

  /**
   * @return the setdtldesc
   */
  public String getSetdtldesc() {
    return setdtldesc;
  }

  /**
   * @param setdtldesc the setdtldesc to set
   */
  public void setSetdtldesc(String setdtldesc) {
    this.setdtldesc = setdtldesc;
  }

  /**
   * @return the systemid
   */
  public String getSystemid() {
    return systemid;
  }

  /**
   * @param systemid the systemid to set
   */
  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }

  /**
   * @return the setcategoryid
   */
  public String getSetcategoryid() {
    return setcategoryid;
  }

  /**
   * @param setcategoryid the setcategoryid to set
   */
  public void setSetcategoryid(String setcategoryid) {
    this.setcategoryid = setcategoryid;
  }

  /**
   * @return the settypeid
   */
  public String getSettypeid() {
    return settypeid;
  }

  /**
   * @param settypeid the settypeid to set
   */
  public void setSettypeid(String settypeid) {
    this.settypeid = settypeid;
  }

  /**
   * @return the sethierarchyid
   */
  public String getSethierarchyid() {
    return sethierarchyid;
  }

  /**
   * @param sethierarchyid the sethierarchyid to set
   */
  public void setSethierarchyid(String sethierarchyid) {
    this.sethierarchyid = sethierarchyid;
  }

  /**
   * @return the setstatusid
   */
  public String getSetstatusid() {
    return setstatusid;
  }

  /**
   * @param setstatusid the setstatusid to set
   */
  public void setSetstatusid(String setstatusid) {
    this.setstatusid = setstatusid;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getSetMasterProperties() {
    Properties props = new Properties();
    props.put("SETID", "SETID");
    props.put("SETNM", "SETNM");
    props.put("SETDESC", "SETDESC");
    props.put("SETDTLDESC", "SETDTLDESC");
    props.put("SYSTEMID", "SYSTEMID");
    props.put("SETCATEGORYID", "SETCATEGORYID");
    props.put("SETTYPEID", "SETTYPEID");
    props.put("SETHIERARCHYID", "SETHIERARCHYID");
    props.put("SYSTEMDTLDESC", "SYSTEMDTLDESC");
    props.put("SETSTATUSID", "SETSTATUSID");
    props.put("VOIDFL", "VOIDFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");

    return props;
  }

}
