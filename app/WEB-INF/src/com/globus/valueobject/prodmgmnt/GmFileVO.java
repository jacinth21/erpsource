package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmFileVO extends GmDataTstVO {
  private String fileid = "";
  private String filenm = "";
  private String filerefid = "";
  private String filetitle = "";
  private String filerefgroup = "";
  private String type = "";
  private String filesize = "";
  private String fileseq = "";
  private String uploadedby = "";
  private String uploadeddt = "";
  private String voidfl = "";
  private String subtype = "";
  private String shareablefl = "";

  public String getFileid() {
    return fileid;
  }

  public void setFileid(String fileid) {
    this.fileid = fileid;
  }

  public String getFilenm() {
    return filenm;
  }

  public void setFilenm(String filenm) {
    this.filenm = filenm;
  }

  public String getFilerefid() {
    return filerefid;
  }

  public void setFilerefid(String filerefid) {
    this.filerefid = filerefid;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getFilesize() {
    return filesize;
  }

  public void setFilesize(String filesize) {
    this.filesize = filesize;
  }

  public String getFileseq() {
    return fileseq;
  }

  public void setFileseq(String fileseq) {
    this.fileseq = fileseq;
  }

  public String getUploadedby() {
    return uploadedby;
  }

  public void setUploadedby(String uploadedby) {
    this.uploadedby = uploadedby;
  }

  public String getUploadeddt() {
    return uploadeddt;
  }

  public void setUploadeddt(String uploadeddt) {
    this.uploadeddt = uploadeddt;
  }

  public String getFiletitle() {
    return filetitle;
  }

  public void setFiletitle(String filetitle) {
    this.filetitle = filetitle;
  }

  public String getFilerefgroup() {
    return filerefgroup;
  }

  public void setFilerefgroup(String filerefgroup) {
    this.filerefgroup = filerefgroup;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


  public String getSubtype() {
    return subtype;
  }

  public void setSubtype(String subtype) {
    this.subtype = subtype;
  }

  public String getShareablefl() {
    return shareablefl;
  }

  public void setShareablefl(String shareablefl) {
    this.shareablefl = shareablefl;
  }

  @Transient
  public Properties getFileInfoProperties() {
    Properties props = new Properties();
    props.put("FILEID", "FILEID");
    props.put("FILENM", "FILENM");
    props.put("FILEREFID", "FILEREFID");
    props.put("FILETITLE", "FILETITLE");
    props.put("FILEREFGROUP", "FILEREFGROUP");
    props.put("TYPE", "TYPE");
    props.put("FILESIZE", "FILESIZE");
    props.put("FILESEQ", "FILESEQ");
    props.put("UPLOADEDBY", "UPLOADEDBY");
    props.put("UPLOADEDDT", "UPLOADEDDT");
    props.put("VOIDFL", "VOIDFL");
    props.put("SUBTYPE", "SUBTYPE");
    props.put("SHAREABLEFL", "SHAREABLEFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }

}
