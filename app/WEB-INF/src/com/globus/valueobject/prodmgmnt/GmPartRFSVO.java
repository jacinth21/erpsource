package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartRFSVO extends GmDataTstVO {
  private String rfsmapid = "";
  private String rfsid = "";
  private String voidfl = "";
  private String cmpid = "";

  @Transient
  public Properties getPartRFSProperties() {
    Properties props = new Properties();
    props.put("RFSMAPID", "RFSMAPID");
    props.put("RFSID", "RFSID");
    props.put("VOIDFL", "VOIDFL");
    props.put("CMPID", "CMPID");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }



  public String getCmpid() {
    return cmpid;
  }



  public void setCmpid(String cmpid) {
    this.cmpid = cmpid;
  }



  /**
   * @return the rfsmapid
   */
  public String getRfsmapid() {
    return rfsmapid;
  }



  /**
   * @param rfsmapid the rfsmapid to set
   */
  public void setRfsmapid(String rfsmapid) {
    this.rfsmapid = rfsmapid;
  }



  /**
   * @return the rfsid
   */
  public String getRfsid() {
    return rfsid;
  }



  /**
   * @param rfsid the rfsid to set
   */
  public void setRfsid(String rfsid) {
    this.rfsid = rfsid;
  }



  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }



  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


}
