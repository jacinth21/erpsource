package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSharedFileVO extends GmDataStoreVO{
	
	private String sharefileid="";
	private String shareid="";
	private String sharefilestatus="";
	private String updfileid="";
	private String linkid="";
	private String linkexpirydate="";
	private String linkretrycnt="";
	private String lastupdateddate="";
	
	@Transient
	public Properties getFileInfoProperties(){
		Properties properties = new Properties();
		properties.put("SHAREFILEID", "SHAREFILEID");
		properties.put("SHAREID", "SHAREID");
		properties.put("SHAREFILESTATUS", "SHAREFILESTATUS");
		properties.put("UPDFILEID", "UPDFILEID");
		properties.put("LINKID", "LINKID");
		properties.put("LINKEXPIRYDATE", "LINKEXPIRYDATE");
		properties.put("LINKRETRYCNT", "LINKRETRYCNT");
		properties.put("LASTUPDATEDDATE", "LASTUPDATEDDATE");
		return properties;
	}
	
	/**
	 * getSaveSharedFileProperties is used put method name in a order required by 
	 * database while loop variable order. this method should be written when you have to
	 * save multiple VO in database using inputstring of specified order in this linked hashmap.
	 * @return
	 */
	public static Map getSaveSharedFileProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		
		hmLinkedDbInputStr.put("UPDFILEID", "getUpdfileid");
		return hmLinkedDbInputStr;
	}
	
	public String getSharefileid() {
		return sharefileid;
	}
	public void setSharefileid(String sharefileid) {
		this.sharefileid = sharefileid;
	}
	public String getShareid() {
		return shareid;
	}
	public void setShareid(String shareid) {
		this.shareid = shareid;
	}
	public String getSharefilestatus() {
		return sharefilestatus;
	}
	public void setSharefilestatus(String sharefilestatus) {
		this.sharefilestatus = sharefilestatus;
	}
	public String getUpdfileid() {
		return updfileid;
	}
	public void setUpdfileid(String updfileid) {
		this.updfileid = updfileid;
	}
	public String getLinkid() {
		return linkid;
	}
	public void setLinkid(String linkid) {
		this.linkid = linkid;
	}
	public String getLinkexpirydate() {
		return linkexpirydate;
	}
	public void setLinkexpirydate(String linkexpirydate) {
		this.linkexpirydate = linkexpirydate;
	}
	public String getLinkretrycnt() {
		return linkretrycnt;
	}
	public void setLinkretrycnt(String linkretrycnt) {
		this.linkretrycnt = linkretrycnt;
	}
	public String getLastupdateddate() {
		return lastupdateddate;
	}
	public void setLastupdateddate(String lastupdateddate) {
		this.lastupdateddate = lastupdateddate;
	}
	
}
