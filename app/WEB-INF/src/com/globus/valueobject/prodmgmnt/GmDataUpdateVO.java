package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDataUpdateVO extends GmDataStoreVO{
	private String	refid = "";
	private String	reftype = "";
	private String 	action = "";
	private String	appid = "";
	private String	updateddt = "";
	private String	updatedby = "";
	private String 	voidfl = "";
	
	@Transient
	public Properties getDataUpdateProperties(){
		Properties properties = new Properties();
		properties.put("REFID", "REFID");
		properties.put("REFTYPE", "REFTYPE");
		properties.put("ACTION", "ACTION");
		properties.put("APPID", "APPID");
		properties.put("UPDATEDDT", "UPDATEDDT");
		properties.put("UPDATEDBY", "UPDATEDBY");	
		properties.put("VOIDFL", "VOIDFL");
		return properties;
	}

	public String getRefid() {
		return refid;
	}

	public void setRefid(String refid) {
		this.refid = refid;
	}

	public String getReftype() {
		return reftype;
	}

	public void setReftype(String reftype) {
		this.reftype = reftype;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUpdateddt() {
		return updateddt;
	}

	public void setUpdateddt(String updateddt) {
		this.updateddt = updateddt;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public String getVoidfl() {
		return voidfl;
	}

	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

}
