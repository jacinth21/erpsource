package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGroupPartVO extends GmDataTstVO {
  private String groupid = "";
  private String partnum = "";
  private String pricingtypeid = "";
  transient private String grpdtlid = "";

  @Transient
  public Properties getGroupPartProperties() {
    Properties properties = new Properties();
    properties.put("GROUPID", "GROUPID");
    properties.put("PARTNUM", "PARTNUM");
    properties.put("PRICINGTYPEID", "PRICINGTYPEID");
    properties.put("TOTALPAGES", "TOTALPAGES");
    properties.put("PAGENO", "PAGENO");
    properties.put("TOTALSIZE", "TOTALSIZE");
    properties.put("PAGESIZE", "PAGESIZE");
    properties.put("GRPDTLID", "GRPDTLID");
    return properties;
  }



  public String getGrpdtlid() {
    return grpdtlid;
  }



  public void setGrpdtlid(String grpdtlid) {
    this.grpdtlid = grpdtlid;
  }



  public String getGroupid() {
    return groupid;
  }

  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }

  public String getPartnum() {
    return partnum;
  }

  public void setPartnum(String partnum) {
    this.partnum = partnum;
  }

  public String getPricingtypeid() {
    return pricingtypeid;
  }

  public void setPricingtypeid(String pricingtypeid) {
    this.pricingtypeid = pricingtypeid;
  }
}
