package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSystemAttributeVO extends GmDataTstVO {
  private String systemid = "";
  private String attribtypeid = "";
  private String attribvalue = "";
  private String voidfl = "";
  transient private String setattrid = "";

  @Transient
  public Properties getSystemAttributeProperties() {
    Properties props = new Properties();
    props.put("SYSTEMID", "SYSTEMID");
    props.put("ATTRIBTYPEID", "ATTRIBTYPEID");
    props.put("ATTRIBVALUE", "ATTRIBVALUE");
    props.put("VOIDFL", "VOIDFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    props.put("SETATTRID", "SETATTRID");
    return props;
  }


  public String getSetattrid() {
    return setattrid;
  }


  public void setSetattrid(String setattrid) {
    this.setattrid = setattrid;
  }


  /**
   * @return the systemid
   */
  public String getSystemid() {
    return systemid;
  }

  /**
   * @param systemid the systemid to set
   */
  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }

  /**
   * @return the attribtypeid
   */
  public String getAttribtypeid() {
    return attribtypeid;
  }

  /**
   * @param attribtypeid the attribtypeid to set
   */
  public void setAttribtypeid(String attribtypeid) {
    this.attribtypeid = attribtypeid;
  }

  /**
   * @return the attribvalue
   */
  public String getAttribvalue() {
    return attribvalue;
  }

  /**
   * @param attribvalue the attribvalue to set
   */
  public void setAttribvalue(String attribvalue) {
    this.attribvalue = attribvalue;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }
}
