package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartAttributeVO extends GmDataTstVO {
  private String partattrid = "";
  private String pnum = "";
  private String partattrval = "";
  private String partattrvoidfl = "";
  private String partattrtype = "";

  @Transient
  public Properties getPartAttributeProperties() {
    Properties props = new Properties();
    props.put("PARTATTRID", "PARTATTRID");
    props.put("PNUM", "PNUM");
    props.put("PARTATTRVAL", "PARTATTRVAL");
    props.put("PARTATTRVOIDFL", "PARTATTRVOIDFL");
    props.put("PARTATTRTYPE", "PARTATTRTYPE");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");

    return props;
  }


  /**
   * @return the partattrid
   */
  public String getPartattrid() {
    return partattrid;
  }


  /**
   * @param partattrid the partattrid to set
   */
  public void setPartattrid(String partattrid) {
    this.partattrid = partattrid;
  }


  /**
   * @return the pnum
   */
  public String getPnum() {
    return pnum;
  }


  /**
   * @param pnum the pnum to set
   */
  public void setPnum(String pnum) {
    this.pnum = pnum;
  }


  /**
   * @return the partattrval
   */
  public String getPartattrval() {
    return partattrval;
  }


  /**
   * @param partattrval the partattrval to set
   */
  public void setPartattrval(String partattrval) {
    this.partattrval = partattrval;
  }


  /**
   * @return the partattrvoidfl
   */
  public String getPartattrvoidfl() {
    return partattrvoidfl;
  }


  /**
   * @param partattrvoidfl the partattrvoidfl to set
   */
  public void setPartattrvoidfl(String partattrvoidfl) {
    this.partattrvoidfl = partattrvoidfl;
  }


  /**
   * @return the partattrtype
   */
  public String getPartattrtype() {
    return partattrtype;
  }


  /**
   * @param partattrtype the partattrtype to set
   */
  public void setPartattrtype(String partattrtype) {
    this.partattrtype = partattrtype;
  }



}
