package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSharedEmailVO extends GmDataStoreVO{
	
	private String sharedtlid="";
	private String shareid="";
	private String refid="";
	private String reftype="";
	private String emailid="";
	private String emailstatus="";
	private String emailsentdate="";
	private String recipienttype="";
	private String username="";
	
	@Transient
	public Properties getEmailInfoProperties(){
		Properties properties = new Properties();
		properties.put("SHAREDTLID", "SHAREDTLID");
		properties.put("SHAREID", "SHAREID");
		properties.put("REFID", "REFID");
		properties.put("REFTYPE", "REFTYPE");
		properties.put("EMAILID", "EMAILID");
		properties.put("EMAILSTATUS", "EMAILSTATUS");
		properties.put("RECIPIENTTYPE", "RECIPIENTTYPE");
		properties.put("USERNAME", "USERNAME");
		return properties;
	}
	/**
	 * getSaveSharedEmailProperties is used put method name in a order required by 
	 * database while loop variable order. this method should be written when you have to
	 * save multiple VO in database using inputstring of specified order in this linked hashmap.
	 * @return
	 */
	public static Map getSaveSharedEmailProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		
		hmLinkedDbInputStr.put("EMAILID", "getEmailid");
		hmLinkedDbInputStr.put("RECIPIENTTYPE", "getRecipienttype");
		hmLinkedDbInputStr.put("REFID", "getRefid");
		hmLinkedDbInputStr.put("REFTYPE", "getReftype");
		return hmLinkedDbInputStr;
	}
	public String getSharedtlid() {
		return sharedtlid;
	}
	public void setSharedtlid(String sharedtlid) {
		this.sharedtlid = sharedtlid;
	}
	public String getShareid() {
		return shareid;
	}
	public void setShareid(String shareid) {
		this.shareid = shareid;
	}
	public String getRefid() {
		return refid;
	}
	public void setRefid(String refid) {
		this.refid = refid;
	}
	public String getReftype() {
		return reftype;
	}
	public void setReftype(String reftype) {
		this.reftype = reftype;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	public String getEmailstatus() {
		return emailstatus;
	}
	public void setEmailstatus(String emailstatus) {
		this.emailstatus = emailstatus;
	}
	public String getEmailsentdate() {
		return emailsentdate;
	}
	public void setEmailsentdate(String emailsentdate) {
		this.emailsentdate = emailsentdate;
	}
	public String getRecipienttype() {
		return recipienttype;
	}
	public void setRecipienttype(String recipienttype) {
		this.recipienttype = recipienttype;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
}
