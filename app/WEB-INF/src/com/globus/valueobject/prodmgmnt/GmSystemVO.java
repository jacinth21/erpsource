package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSystemVO extends GmDataTstVO {
  private String systemid = "";
  private String systemnm = "";
  private String systemdesc = "";
  private String systemdtldesc = "";
  private String systemstatusid = "";
  private String setgrptyp = "";

  /**
   * @return the setgrptyp
   */
  public String getSetgrptyp() {
    return setgrptyp;
  }

  /**
   * @param setgrptyp the setgrptyp to set
   */
  public void setSetgrptyp(String setgrptyp) {
    this.setgrptyp = setgrptyp;
  }

  private String voidfl = "";

  /**
   * @return the systemid
   */
  public String getSystemid() {
    return systemid;
  }

  /**
   * @param systemid the systemid to set
   */
  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }

  /**
   * @return the systemnm
   */
  public String getSystemnm() {
    return systemnm;
  }

  /**
   * @param systemnm the systemnm to set
   */
  public void setSystemnm(String systemnm) {
    this.systemnm = systemnm;
  }

  /**
   * @return the systemdesc
   */
  public String getSystemdesc() {
    return systemdesc;
  }

  /**
   * @param systemdesc the systemdesc to set
   */
  public void setSystemdesc(String systemdesc) {
    this.systemdesc = systemdesc;
  }

  /**
   * @return the systemdtldesc
   */
  public String getSystemdtldesc() {
    return systemdtldesc;
  }

  /**
   * @param systemdtldesc the systemdtldesc to set
   */
  public void setSystemdtldesc(String systemdtldesc) {
    this.systemdtldesc = systemdtldesc;
  }

  /**
   * @return the systemstatusid
   */
  public String getSystemstatusid() {
    return systemstatusid;
  }

  /**
   * @param systemstatusid the systemstatusid to set
   */
  public void setSystemstatusid(String systemstatusid) {
    this.systemstatusid = systemstatusid;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getSystemProperties() {
    Properties props = new Properties();
    props.put("SYSTEMID", "SYSTEMID");
    props.put("SYSTEMNM", "SYSTEMNM");
    props.put("SYSTEMDESC", "SYSTEMDESC");
    props.put("SYSTEMDTLDESC", "SYSTEMDTLDESC");
    props.put("SYSTEMSTATUSID", "SYSTEMSTATUSID");
    props.put("SETGRPTYP", "SETGRPTYP");
    props.put("VOIDFL", "VOIDFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }
}
