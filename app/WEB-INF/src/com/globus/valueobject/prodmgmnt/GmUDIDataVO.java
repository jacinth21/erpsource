package com.globus.valueobject.prodmgmnt;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmUDIDataVO extends GmDataStoreVO {
	
	private String dinum = "";
	private String partnum = "";
	private String controlnum = "";
	private String expdate = "";
	private String mfgdate = "";
	private String serialno = "";
	private String mrf = "";
	private String hrf = "";
	
	private List<GmUDIDataVO> listGmUDIDataVO = null;

	/**
	 * @return the dinum
	 */
	public String getDinum() {
		return dinum;
	}

	/**
	 * @param dinum the dinum to set
	 */
	public void setDinum(String dinum) {
		this.dinum = dinum;
	}

	/**
	 * @return the partnum
	 */
	public String getPartnum() {
		return partnum;
	}

	/**
	 * @param partnum the partnum to set
	 */
	public void setPartnum(String partnum) {
		this.partnum = partnum;
	}

	/**
	 * @return the controlnum
	 */
	public String getControlnum() {
		return controlnum;
	}

	/**
	 * @param controlnum the controlnum to set
	 */
	public void setControlnum(String controlnum) {
		this.controlnum = controlnum;
	}

	/**
	 * @return the expdate
	 */
	public String getExpdate() {
		return expdate;
	}

	/**
	 * @param expdate the expdate to set
	 */
	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}

	/**
	 * @return the mfgdate
	 */
	public String getMfgdate() {
		return mfgdate;
	}

	/**
	 * @param mfgdate the mfgdate to set
	 */
	public void setMfgdate(String mfgdate) {
		this.mfgdate = mfgdate;
	}

	/**
	 * @return the serialno
	 */
	public String getSerialno() {
		return serialno;
	}

	/**
	 * @param serialno the serialno to set
	 */
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}

	/**
	 * @return the mrf
	 */
	public String getMrf() {
		return mrf;
	}

	/**
	 * @param mrf the mrf to set
	 */
	public void setMrf(String mrf) {
		this.mrf = mrf;
	}

	/**
	 * @return the hrf
	 */
	public String getHrf() {
		return hrf;
	}

	/**
	 * @param hrf the hrf to set
	 */
	public void setHrf(String hrf) {
		this.hrf = hrf;
	}
	
	
}
