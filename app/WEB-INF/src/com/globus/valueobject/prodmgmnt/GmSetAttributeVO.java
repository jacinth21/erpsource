package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSetAttributeVO extends GmDataTstVO {
  private String setid = "";
  private String setnm = "";
  private String attrtype = "";
  private String attrvalue = "";
  private String voidfl = "";
  transient private String setattrid = "";

  @Transient
  public Properties getSetAttributeProperties() {
    Properties props = new Properties();
    props.put("SETID", "SETID");
    props.put("SETNM", "SETNM");
    props.put("ATTRTYPE", "ATTRTYPE");
    props.put("ATTRVALUE", "ATTRVALUE");
    props.put("VOIDFL", "VOIDFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    props.put("SETATTRID", "SETATTRID");
    return props;
  }

  public String getSetattrid() {
    return setattrid;
  }

  public void setSetattrid(String setattrid) {
    this.setattrid = setattrid;
  }

  public String getSetid() {
    return setid;
  }

  public void setSetid(String setid) {
    this.setid = setid;
  }

  public String getSetnm() {
    return setnm;
  }

  public void setSetnm(String setnm) {
    this.setnm = setnm;
  }

  public String getAttrtype() {
    return attrtype;
  }

  public void setAttrtype(String attrtype) {
    this.attrtype = attrtype;
  }

  public String getAttrvalue() {
    return attrvalue;
  }

  public void setAttrvalue(String attrvalue) {
    this.attrvalue = attrvalue;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }


}
