package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGroupVO extends GmDataTstVO {
  private String groupid = "";
  private String groupnm = "";
  private String groupdesc = "";
  private String groupdtldesc = "";
  private String grouppublishfl = "";
  private String grouptypeid = "";
  private String grouppricedtypeid = "";
  private String systemid = "";
  private String voidfl = "";

  @Transient
  public Properties getGroupProperties() {
    Properties properties = new Properties();
    properties.put("GROUPID", "GROUPID");
    properties.put("GROUPNM", "GROUPNM");
    properties.put("GROUPDESC", "GROUPDESC");
    properties.put("GROUPDTLDESC", "GROUPDTLDESC");
    properties.put("GROUPPUBLISHFL", "GROUPPUBLISHFL");
    properties.put("GROUPTYPEID", "GROUPTYPEID");
    properties.put("GROUPPRICEDTYPEID", "GROUPPRICEDTYPEID");
    properties.put("SYSTEMID", "SYSTEMID");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("TOTALPAGES", "TOTALPAGES");
    properties.put("PAGENO", "PAGENO");
    properties.put("TOTALSIZE", "TOTALSIZE");
    properties.put("PAGESIZE", "PAGESIZE");
    return properties;
  }

  public String getGroupid() {
    return groupid;
  }



  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }



  public String getGroupnm() {
    return groupnm;
  }



  public void setGroupnm(String groupnm) {
    this.groupnm = groupnm;
  }



  public String getGroupdesc() {
    return groupdesc;
  }



  public void setGroupdesc(String groupdesc) {
    this.groupdesc = groupdesc;
  }



  public String getGroupdtldesc() {
    return groupdtldesc;
  }



  public void setGroupdtldesc(String groupdtldesc) {
    this.groupdtldesc = groupdtldesc;
  }



  public String getGrouppublishfl() {
    return grouppublishfl;
  }



  public void setGrouppublishfl(String grouppublishfl) {
    this.grouppublishfl = grouppublishfl;
  }



  public String getGrouptypeid() {
    return grouptypeid;
  }



  public void setGrouptypeid(String grouptypeid) {
    this.grouptypeid = grouptypeid;
  }



  public String getGrouppricedtypeid() {
    return grouppricedtypeid;
  }



  public void setGrouppricedtypeid(String grouppricedtypeid) {
    this.grouppricedtypeid = grouppricedtypeid;
  }



  public String getSystemid() {
    return systemid;
  }



  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

}
