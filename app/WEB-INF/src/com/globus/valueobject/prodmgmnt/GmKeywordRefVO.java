package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmKeywordRefVO extends GmDataTstVO {

  private String keywordid = "";
  private String keywordnm = "";
  private String refid = "";
  private String reftype = "";
  private String voidfl = "";
  private String updatedby = "";
  private String updateddt = "";

  @Transient
  public Properties getFileInfoProperties() {
    Properties properties = new Properties();
    properties.put("KEYWORDID", "KEYWORDID");
    properties.put("KEYWORDNM", "KEYWORDNM");
    properties.put("REFID", "REFID");
    properties.put("REFTYPE", "REFTYPE");
    properties.put("VOIDFL", "VOIDFL");
    properties.put("UPDATEDBY", "UPDATEDBY");
    properties.put("UPDATEDDT", "UPDATEDDT");
    properties.put("TOTALPAGES", "TOTALPAGES");
    properties.put("PAGENO", "PAGENO");
    properties.put("TOTALSIZE", "TOTALSIZE");
    properties.put("PAGESIZE", "PAGESIZE");
    return properties;
  }

  public String getKeywordid() {
    return keywordid;
  }

  public void setKeywordid(String keywordid) {
    this.keywordid = keywordid;
  }

  public String getKeywordnm() {
    return keywordnm;
  }

  public void setKeywordnm(String keywordnm) {
    this.keywordnm = keywordnm;
  }

  public String getRefid() {
    return refid;
  }

  public void setRefid(String refid) {
    this.refid = refid;
  }

  public String getReftype() {
    return reftype;
  }

  public void setReftype(String reftype) {
    this.reftype = reftype;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  public String getUpdatedby() {
    return updatedby;
  }

  public void setUpdatedby(String updatedby) {
    this.updatedby = updatedby;
  }

  public String getUpdateddt() {
    return updateddt;
  }

  public void setUpdateddt(String updateddt) {
    this.updateddt = updateddt;
  }

}
