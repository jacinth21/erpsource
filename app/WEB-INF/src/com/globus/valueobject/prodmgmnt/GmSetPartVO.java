package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSetPartVO extends GmDataTstVO {
  private String setid = "";
  private String partnum = "";
  private String setqty = "";
  private String insetfl = "";
  private String voidfl = "";
  transient private String setdtlid = "";



  public String getSetdtlid() {
    return setdtlid;
  }

  public void setSetdtlid(String setdtlid) {
    this.setdtlid = setdtlid;
  }

  /**
   * @return the setid
   */
  public String getSetid() {
    return setid;
  }

  /**
   * @param setid the setid to set
   */
  public void setSetid(String setid) {
    this.setid = setid;
  }

  /**
   * @return the partnum
   */
  public String getPartnum() {
    return partnum;
  }

  /**
   * @param partnum the partnum to set
   */
  public void setPartnum(String partnum) {
    this.partnum = partnum;
  }

  /**
   * @return the setqty
   */
  public String getSetqty() {
    return setqty;
  }

  /**
   * @param setqty the setqty to set
   */
  public void setSetqty(String setqty) {
    this.setqty = setqty;
  }

  /**
   * @return the insetfl
   */
  public String getInsetfl() {
    return insetfl;
  }

  /**
   * @param insetfl the insetfl to set
   */
  public void setInsetfl(String insetfl) {
    this.insetfl = insetfl;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getSetPartProperties() {
    Properties props = new Properties();
    props.put("SETID", "SETID");
    props.put("PARTNUM", "PARTNUM");
    props.put("SETQTY", "SETQTY");
    props.put("INSETFL", "INSETFL");
    props.put("VOIDFL", "VOIDFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    props.put("SETDTLID", "SETDTLID");

    return props;
  }

}
