package com.globus.valueobject.prodmgmnt;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmRulesVO;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmShareEngineVO extends GmDataStoreVO{
	
	private String shareid="";
	private String shextrefid="";
	private String sharetype="";
	private String subject="";
	private String msgdetail="";
	private String sharestatus="";
	private String partytype="";
	private String emailsentdate="";
	private List<GmSharedEmailVO> listGmSharedEmailVO =null; 
	private List<GmSharedFileVO> listGmSharedFileVO= null;
	
	
	// Below properties added to receive data from webservice-IPAD
	private GmSharedEmailVO[] gmSharedEmailVO;
	//private String[] gmSharedFileVO;
	private GmSharedFileVO[] gmSharedFileVO;
	
	@Transient
	public Properties getShareEngineInfoProperties(){
		Properties properties = new Properties();
		properties.put("SHAREID", "SHAREID");
		properties.put("PARTYTYPE", "PARTYTYPE");
		properties.put("SHEXTREFID", "SHEXTREFID");
		properties.put("SHARETYPE", "SHARETYPE");
		properties.put("SHARESTATUS", "SHARESTATUS");
		properties.put("SUBJECT", "SUBJECT");
		properties.put("MSGDETAIL", "MSGDETAIL");
		properties.put("EMAILSENTDATE", "EMAILSENTDATE");
		return properties;
	}
	
	public String getShareid() {
		return shareid;
	}
	public void setShareid(String shareid) {
		this.shareid = shareid;
	}
	
	/**
	 * @return the shextrefid
	 */
	public String getShextrefid() {
		return shextrefid;
	}

	/**
	 * @param shextrefid the shextrefid to set
	 */
	public void setShextrefid(String shextrefid) {
		this.shextrefid = shextrefid;
	}

	public String getSharetype() {
		return sharetype;
	}
	public void setSharetype(String sharetype) {
		this.sharetype = sharetype;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMsgdetail() {
		return msgdetail;
	}
	public void setMsgdetail(String msgdetail) {
		this.msgdetail = msgdetail;
	}
	public String getSharestatus() {
		return sharestatus;
	}
	public void setSharestatus(String sharestatus) {
		this.sharestatus = sharestatus;
	}
	public String getPartytype() {
		return partytype;
	}
	public void setPartytype(String partytype) {
		this.partytype = partytype;
	}
	public String getEmailsentdate() {
		return emailsentdate;
	}
	public void setEmailsentdate(String emailsentdate) {
		this.emailsentdate = emailsentdate;
	}

	public List<GmSharedEmailVO> getListGmSharedEmailVO() {
		return listGmSharedEmailVO;
	}

	public void setListGmSharedEmailVO(List<GmSharedEmailVO> listGmSharedEmailVO) {
		this.listGmSharedEmailVO = listGmSharedEmailVO;
	}

	public List<GmSharedFileVO> getListGmSharedFileVO() {
		return listGmSharedFileVO;
	}

	public void setListGmSharedFileVO(List<GmSharedFileVO> listGmSharedFileVO) {
		this.listGmSharedFileVO = listGmSharedFileVO;
	}

	/**
	 * @return the gmSharedEmailVO
	 */
	public GmSharedEmailVO[] getGmSharedEmailVO() {
		return gmSharedEmailVO;
	}

	/**
	 * @param gmSharedEmailVO the gmSharedEmailVO to set
	 */
	public void setGmSharedEmailVO(GmSharedEmailVO[] gmSharedEmailVO) {
		this.gmSharedEmailVO = gmSharedEmailVO;
	}

	/**
	 * @return the gmSharedFileVO
	 */
	public GmSharedFileVO[] getGmSharedFileVO() {
		return gmSharedFileVO;
	}

	/**
	 * @param gmSharedFileVO the gmSharedFileVO to set
	 */
	public void setGmSharedFileVO(GmSharedFileVO[] gmSharedFileVO) {
		this.gmSharedFileVO = gmSharedFileVO;
	}

	
	
	
}
