package com.globus.valueobject.operations.request;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartDetailVO extends GmDataStoreVO {

  private String pnum = "";
  private String qty = "";
  private String retnFlag = "";



  public static Map getPartDetailProperties() {
    Map hmLinkedDbInputStr = new LinkedHashMap();

    hmLinkedDbInputStr.put("PNUM", "getPnum");
    hmLinkedDbInputStr.put("QTY", "getQty");
    hmLinkedDbInputStr.put("RETNFLAG", "getRetnFlag");

    return hmLinkedDbInputStr;
  }

  public String getPnum() {
    return pnum;
  }

  public void setPnum(String pnum) {
    this.pnum = pnum;
  }

  public String getQty() {
    return qty;
  }

  public void setQty(String qty) {
    this.qty = qty;
  }

  /**
   * @return the retnFlag
   */
  public String getRetnFlag() {
    return retnFlag;
  }

  /**
   * @param retnFlag the retnFlag to set
   */
  public void setRetnFlag(String retnFlag) {
    this.retnFlag = retnFlag;
  }


}
