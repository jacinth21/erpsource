package com.globus.valueobject.operations.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.sales.GmSalesDashReqVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCaseInfoListVO extends GmSalesDashReqVO {
	private String status = "";
	private String fromdt = "";
	private String todt = "";
	private String acctid = "";
	private String prdreqdid = "";  

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFromdt() {
		return fromdt;
	}

	public void setFromdt(String fromdt) {
		this.fromdt = fromdt;
	}

	public String getTodt() {
		return todt;
	}

	public void setTodt(String todt) {
		this.todt = todt;
	}

	public String getAcctid() {
		return acctid;
	}

	public void setAcctid(String acctid) {
		this.acctid = acctid;
	}

	public String getPrdreqdid() {
		return prdreqdid;
	}

	public void setPrdreqdid(String prdreqdid) {
		this.prdreqdid = prdreqdid;
	}
	
}
