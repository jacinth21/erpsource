package com.globus.valueobject.operations.request;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.request.GmCaseAttributeVO;
import com.globus.valueobject.operations.request.GmCaseSetInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCaseInfoVO extends GmDataStoreVO {

	private String cainfoid = "";
	private String caseid = "";
	private String surdt = "";
	private String surtime = "";
	private String timezone = "";
	private String did = "";
	private String repid = "";
	private String asrepid = "";	
	private String acid = "";
	private String voidfl = "";
	private String prnotes = "";
	private String type = "";
	private String status = "";
	private String prtcaid = "";
	private String skipvalidationfl = "";
	private String dnm = "";
	private String repnm = "";
	private String acnm = "";
	private String createdby = "";
	
	private GmCaseAttributeVO[] arrGmCaseAttributeVO;
	private GmCaseSetInfoVO[] arrGmCaseSetInfoVO;

	@Transient
	public Properties getCaseInfoProperties(){
		Properties properties = new Properties();
		properties.put("CASEINFOID", "CAINFOID");
		properties.put("CASEID", "CASEID");
		properties.put("SURGERYDT", "SURDT");
		properties.put("SURGERYTIME", "SURTIME");
		properties.put("TIMEZONE", "TIMEZONE");
		properties.put("DISTID", "DID");
		properties.put("REPID", "REPID");
		properties.put("ASREPID", "ASREPID");
		properties.put("ACCOUNTID", "ACID");
		properties.put("VOIDFL", "VOIDFL");
		properties.put("PNOTES", "PRNOTES");
		properties.put("TYPE", "TYPE");
		properties.put("STATUS", "STATUS");
		properties.put("PRNTCASEINFOID", "PRTCAID");
		properties.put("DISTNM", "DNM");
		properties.put("REPNM", "REPNM");
		properties.put("ACCOUNTNM", "ACNM");
		properties.put("CREATEDBY", "CREATEDBY");	
		return properties;
	}
	
	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public String getCainfoid() {
		return cainfoid;
	}
	public void setCainfoid(String cainfoid) {
		this.cainfoid = cainfoid;
	}
	public String getCaseid() {
		return caseid;
	}
	public void setCaseid(String caseid) {
		this.caseid = caseid;
	}
	public String getSurdt() {
		return surdt;
	}
	public void setSurdt(String surdt) {
		this.surdt = surdt;
	}
	public String getSurtime() {
		return surtime;
	}
	public void setSurtime(String surtime) {
		this.surtime = surtime;
	}
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getRepid() {
		return repid;
	}
	public void setRepid(String repid) {
		this.repid = repid;
	}
	public String getAsrepid() {
		return asrepid;
	}
	public void setAsrepid(String asrepid) {
		this.asrepid = asrepid;
	}
	public String getAcid() {
		return acid;
	}
	public void setAcid(String acid) {
		this.acid = acid;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	public String getPrnotes() {
		return prnotes;
	}
	public void setPrnotes(String prnotes) {
		this.prnotes = prnotes;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPrtcaid() {
		return prtcaid;
	}
	public void setPrtcaid(String prtcaid) {
		this.prtcaid = prtcaid;
	}
	public GmCaseAttributeVO[] getArrGmCaseAttributeVO() {
		return arrGmCaseAttributeVO;
	}
	public void setArrGmCaseAttributeVO(GmCaseAttributeVO[] arrGmCaseAttributeVO) {
		this.arrGmCaseAttributeVO = arrGmCaseAttributeVO;
	}
	public GmCaseSetInfoVO[] getArrGmCaseSetInfoVO() {
		return arrGmCaseSetInfoVO;
	}
	public void setArrGmCaseSetInfoVO(GmCaseSetInfoVO[] arrGmCaseSetInfoVO) {
		this.arrGmCaseSetInfoVO = arrGmCaseSetInfoVO;
	}
	public String getSkipvalidationfl() {
		return skipvalidationfl;
	}
	public void setSkipvalidationfl(String skipvalidationfl) {
		this.skipvalidationfl = skipvalidationfl;
	}
	public String getDnm() {
		return dnm;
	}
	public void setDnm(String dnm) {
		this.dnm = dnm;
	}
	public String getRepnm() {
		return repnm;
	}
	public void setRepnm(String repnm) {
		this.repnm = repnm;
	}
	public String getAcnm() {
		return acnm;
	}
	public void setAcnm(String acnm) {
		this.acnm = acnm;
	}
}
