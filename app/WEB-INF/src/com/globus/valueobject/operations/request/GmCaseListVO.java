package com.globus.valueobject.operations.request;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDeviceDetailVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.sales.GmProdRequestDetVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCaseListVO extends GmDeviceDetailVO {

  private List<GmCaseInfoVO> listGmCaseInfoVO = null;
  private List<GmCaseAttributeVO> listGmCaseAttributeVO = null;
  private List<GmTxnShipInfoVO> listGmTxnShipInfoVO = null;
  private List<GmProdRequestDetVO> listGmProdRequestDetVO = null;

  private GmProdRequestDetVO[] arrGmProdRequestDetVO;

  public List<GmCaseInfoVO> getListGmCaseInfoVO() {
    return listGmCaseInfoVO;
  }

  public void setListGmCaseInfoVO(List<GmCaseInfoVO> listGmCaseInfoVO) {
    this.listGmCaseInfoVO = listGmCaseInfoVO;
  }

  public List<GmCaseAttributeVO> getListGmCaseAttributeVO() {
    return listGmCaseAttributeVO;
  }

  public void setListGmCaseAttributeVO(List<GmCaseAttributeVO> listGmCaseAttributeVO) {
    this.listGmCaseAttributeVO = listGmCaseAttributeVO;
  }

  public List<GmTxnShipInfoVO> getListGmTxnShipInfoVO() {
    return listGmTxnShipInfoVO;
  }

  public void setListGmTxnShipInfoVO(List<GmTxnShipInfoVO> listGmTxnShipInfoVO) {
    this.listGmTxnShipInfoVO = listGmTxnShipInfoVO;
  }

  public List<GmProdRequestDetVO> getListGmProdRequestDetVO() {
    return listGmProdRequestDetVO;
  }

  public void setListGmProdRequestDetVO(List<GmProdRequestDetVO> listGmProdRequestDetVO) {
    this.listGmProdRequestDetVO = listGmProdRequestDetVO;
  }

  public GmProdRequestDetVO[] getArrGmProdRequestDetVO() {
    return arrGmProdRequestDetVO;
  }

  public void setArrGmProdRequestDetVO(GmProdRequestDetVO[] arrGmProdRequestDetVO) {
    this.arrGmProdRequestDetVO = arrGmProdRequestDetVO;
  }

  public void populatePageProperties(List alList) {
    int alReturnSize = alList.size();
    if (alReturnSize > 0) {
      HashMap hmPageInfo = GmCommonClass.parseNullHashMap((HashMap) alList.get(0));
      this.setPageno((String) hmPageInfo.get("PAGENO"));
      this.setTotalpages((String) hmPageInfo.get("TOTALPAGES"));
      this.setTotalsize((String) hmPageInfo.get("TOTALSIZE"));
      this.setPagesize((String) hmPageInfo.get("PAGESIZE"));
    }
  }

  public void setPageProperties(List alList) {
    int alReturnSize = alList.size();
    String strAlSize = Integer.toString(alReturnSize);
    if (alReturnSize > 0) {
      this.setPageno("1");
      this.setTotalpages("1");
      this.setTotalsize(strAlSize);
      this.setPagesize(strAlSize);
    } else {
      this.setPageno("1");
      this.setTotalpages("");
      this.setTotalsize("");
      this.setPagesize("");
    }
  }

}
