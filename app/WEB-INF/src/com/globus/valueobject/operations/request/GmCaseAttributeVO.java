package com.globus.valueobject.operations.request;

import java.beans.Transient;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCaseAttributeVO extends GmDataVO {

  private String caattrid = "";
  private String cainfoid = "";
  private String attrtp = "";
  private String attrval = "";
  private String voidfl = "";

  @Transient
  public Properties getCaseAttributeProperties() {
    Properties properties = new Properties();
    properties.put("CASEINFOID", "CAINFOID");
    properties.put("CASEATTRID", "CAATTRID");
    properties.put("ATTRTYPE", "ATTRTP");
    properties.put("ATTRVALUE", "ATTRVAL");
    properties.put("VOIDFL", "VOIDFL");
    return properties;
  }

  public String getCaattrid() {
    return caattrid;
  }

  public void setCaattrid(String caattrid) {
    this.caattrid = caattrid;
  }

  public String getCainfoid() {
    return cainfoid;
  }

  public void setCainfoid(String cainfoid) {
    this.cainfoid = cainfoid;
  }

  public String getAttrtp() {
    return attrtp;
  }

  public void setAttrtp(String attrtp) {
    this.attrtp = attrtp;
  }

  public String getAttrval() {
    return attrval;
  }

  public void setAttrval(String attrval) {
    this.attrval = attrval;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  /**
   * getSaveCaseAttributeProperties is used put method name in a order required by database while
   * loop variable order. this method should be written when you have to save multiple VO in
   * database using inputstring of specified order in this linked hashmap.
   * 
   * @return
   */
  public static Map getSaveCaseAttributeProperties() {
    Map hmLinkedDbInputStr = new LinkedHashMap();

    hmLinkedDbInputStr.put("CAATTRID", "getCaattrid");
    hmLinkedDbInputStr.put("CAINFOID", "getCainfoid");
    hmLinkedDbInputStr.put("ATTRTP", "getAttrtp");
    hmLinkedDbInputStr.put("ATTRVAL", "getAttrval");
    hmLinkedDbInputStr.put("VOIDFL", "getVoidfl");

    return hmLinkedDbInputStr;
  }
}
