package com.globus.valueobject.operations.request;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCaseSetInfoVO extends GmDataStoreVO {

	private String cainfoid = "";
	private String casetid = "";
	private String setid = "";
	private String systp = "";
	private String pnum = "";
	private String qty = "";	
	private String reqdt = "";
	private String planshipdt = "";	

	private GmTxnShipInfoVO gmTxnShipInfoVO;
	private GmPartDetailVO[] arrGmPartDetailVO;
	

	public String getCasetid() {
		return casetid;
	}

	public void setCasetid(String casetid) {
		this.casetid = casetid;
	}

	public String getCainfoid() {
		return cainfoid;
	}

	public void setCainfoid(String cainfoid) {
		this.cainfoid = cainfoid;
	}

	public String getSetid() {
		return setid;
	}

	public void setSetid(String setid) {
		this.setid = setid;
	}

	public String getPnum() {
		return pnum;
	}

	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getSystp() {
		return systp;
	}

	public void setSystp(String systp) {
		this.systp = systp;
	}

	public String getReqdt() {
		return reqdt;
	}

	public void setReqdt(String reqdt) {
		this.reqdt = reqdt;
	}

	public String getPlanshipdt() {
		return planshipdt;
	}

	public void setPlanshipdt(String planshipdt) {
		this.planshipdt = planshipdt;
	}

	public GmTxnShipInfoVO getGmTxnShipInfoVO() {
		return gmTxnShipInfoVO;
	}
	public void setGmTxnShipInfoVO(GmTxnShipInfoVO gmTxnShipInfoVO) {
		this.gmTxnShipInfoVO = gmTxnShipInfoVO;
	}
	public GmPartDetailVO[] getArrGmPartDetailVO() {
		return arrGmPartDetailVO;
	}

	public void setArrGmPartDetailVO(GmPartDetailVO[] arrGmPartDetailVO) {
		this.arrGmPartDetailVO = arrGmPartDetailVO;
	}
	
}
