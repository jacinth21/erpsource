package com.globus.valueobject.operations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmShipStationVO extends GmDataStoreVO {

	private String toteid = "";
	private String stationid = "";
	private String cnt = "";
	/**
	 * @return the toteid
	 */
	public String getToteid() {
		return toteid;
	}
	/**
	 * @param toteid the toteid to set
	 */
	public void setToteid(String toteid) {
		this.toteid = toteid;
	}
	/**
	 * @return the stationid
	 */
	public String getStationid() {
		return stationid;
	}
	/**
	 * @param stationid the stationid to set
	 */
	public void setStationid(String stationid) {
		this.stationid = stationid;
	}
	/**
	 * @return the cnt
	 */
	public String getCnt() {
		return cnt;
	}
	/**
	 * @param cnt the cnt to set
	 */
	public void setCnt(String cnt) {
		this.cnt = cnt;
	}
	
	
	
}
