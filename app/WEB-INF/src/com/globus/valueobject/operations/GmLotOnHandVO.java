package com.globus.valueobject.operations;


import java.beans.Transient; 
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmSalesDashResVO;


/**
 * @author aprasath
 * @version 1.0
 * @created 08-Apr-2019 10:40:21 PM
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLotOnHandVO extends GmDataStoreVO{

	private String partInfo;
	private String lotExpDays;
	private String partDetails;
	/**
	 * Used to identify 30,60,90 days report
	 */
	private int expDays;
	private String partNum;
	private int expiryFlag;
	
	public GmLotOnHandVO(){

	}


	public String getPartInfo() {
		return partInfo;
	}

	public void setPartInfo(String partInfo) {
		this.partInfo = partInfo;
	}

	public String getLotExpDays() {
		return lotExpDays;
	}

	public void setLotExpDays(String lotExpDays) {
		this.lotExpDays = lotExpDays;
	}

	public String getPartDetails() {
		return partDetails;
	}

	public void setPartDetails(String partDetails) {
		this.partDetails = partDetails;
	}

	public int getExpDays() {
		return expDays;
	}

	public void setExpDays(int expDays) {
		this.expDays = expDays;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	public int getExpiryFlag() {
		return expiryFlag;
	}

	public void setExpiryFlag(int expiryFlag) {
		this.expiryFlag = expiryFlag;
	}
	
	

	
}//end GmLotOnHandVO.java