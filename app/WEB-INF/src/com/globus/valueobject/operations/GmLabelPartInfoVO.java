package com.globus.valueobject.operations;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLabelPartInfoVO extends GmDataStoreVO {

  private String txnid = "";
  private String txntype = "";
  private String partid = "";
  private String partdesc = "";
  private String partfamily = "";
  private String partclass = "";
  private String controlnum = "";
  private String qty = "";
  private String di = "";
  private String udino = "";
  private String udimrf = "";
  private String expdate = "";
  private String etch = "";
  private String storagetemp="";
  private String lblsize = "";
  private String companyid = "";
  
  // Added BBA LPA Changes required parameters.
  private String donorno = "";
  private String genericspec = "";
  private String patientlabel = "";
  private String boxlabel = "";
  private String finalcpcver = "";
  private String cpcname = "";
  private String cpcaddress = "";
  private String cpcbarcodelogo="";
  private String cpcid="";
  private String compid="";
  private String cpcfinallogo="";
  private String lbl_path="";
   


public String getStoragetemp() {
	return storagetemp;
}

public void setStoragetemp(String storagetemp) {
	this.storagetemp = storagetemp;
}

public String getLblsize() {
	return lblsize;
}

public void setLblsize(String lblsize) {
	this.lblsize = lblsize;
}

public String getFinalcpcver() {
	return finalcpcver;
}

public void setFinalcpcver(String finalcpcver) {
	this.finalcpcver = finalcpcver;
}


  public String getUdimrf() {
    return udimrf;
  }

  public void setUdimrf(String udimrf) {
    this.udimrf = udimrf;
  }

  public String getExpdate() {
    return expdate;
  }

  public void setExpdate(String expdate) {
    this.expdate = expdate;
  }

  public String getTxnid() {
    return txnid;
  }

  public void setTxnid(String txnid) {
    this.txnid = txnid;
  }

  public String getTxntype() {
    return txntype;
  }

  public void setTxntype(String txntype) {
    this.txntype = txntype;
  }

  public String getPartid() {
    return partid;
  }

  public void setPartid(String partid) {
    this.partid = partid;
  }

  public String getPartdesc() {
    return partdesc;
  }

  public void setPartdesc(String partdesc) {
    this.partdesc = partdesc;
  }

  public String getPartfamily() {
    return partfamily;
  }

  public void setPartfamily(String partfamily) {
    this.partfamily = partfamily;
  }

  public String getPartclass() {
    return partclass;
  }

  public void setPartclass(String partclass) {
    this.partclass = partclass;
  }

  public String getControlnum() {
    return controlnum;
  }

  public void setControlnum(String controlnum) {
    this.controlnum = controlnum;
  }

  public String getQty() {
    return qty;
  }

  public void setQty(String qty) {
    this.qty = qty;
  }

  public String getDi() {
    return di;
  }

  public void setDi(String di) {
    this.di = di;
  }

  public String getUdino() {
    return udino;
  }

  public void setUdino(String udino) {
    this.udino = udino;
  }
  
   /**
   * @return the etch
   */
  public String getEtch() {
    return etch;
  }

  /**
   * @param etch the etch to set
   */
  public void setEtch(String etch) {
    this.etch = etch;
  }




  public String getDonorno() {
  	return donorno;
  }

  public void setDonorno(String donorno) {
  	this.donorno = donorno;
  }

  
  public String getGenericspec() {
  	return genericspec;
  }

  
  public void setGenericspec(String genericspec) {
  	this.genericspec = genericspec;
  }

  public String getPatientlabel() {
  	return patientlabel;
  }

  public void setPatientlabel(String patientlabel) {
  	this.patientlabel = patientlabel;
  }

  public String getBoxlabel() {
  	return boxlabel;
  }

  public void setBoxlabel(String boxlabel) {
  	this.boxlabel = boxlabel;
  }


  public String getCpcname() {
  	return cpcname;
  }

  public void setCpcname(String cpcname) {
  	this.cpcname = cpcname;
  }

  public String getCpcaddress() {
  	return cpcaddress;
  }

  public void setCpcaddress(String cpcaddress) {
  	this.cpcaddress = cpcaddress;
  }
  
  public String getCpcbarcodelogo() {
	return cpcbarcodelogo;
}

public void setCpcbarcodelogo(String cpcbarcodelogo) {
	this.cpcbarcodelogo = cpcbarcodelogo;
}

public String getCpcfinallogo() {
	return cpcfinallogo;
}

public void setCpcfinallogo(String cpcfinallogo) {
	this.cpcfinallogo = cpcfinallogo;
}

public String getCpcid() {
	return cpcid;
}

public void setCpcid(String cpcid) {
	this.cpcid = cpcid;
}



public String getCompanyid() {
	return companyid;
}

public void setCompanyid(String companyid) {
	this.companyid = companyid;
}



public String getCompid() {
	return compid;
}

public void setCompid(String compid) {
	this.compid = compid;
}


public String getLbl_path() {
	return lbl_path;
}

public void setLbl_path(String lbl_path) {
	this.lbl_path = lbl_path;
}

	@Transient
  public Properties getLabelDataProperties() {
    Properties props = new Properties();
    props.put("TRANSACTION_ID", "TXNID");
    props.put("PART_NUMBER", "PARTID");
    props.put("PART_DESC", "PARTDESC");
    props.put("PART_FAMILY", "PARTFAMILY");
    props.put("PART_CLASS", "PARTCLASS");
    props.put("CONTROL_NUMBER", "CONTROLNUM");
    props.put("QUANTITY", "QTY");
    props.put("TRANSACTION_TYPE", "TXNTYPE");
    props.put("DI", "DI");
    props.put("UDI_NO", "UDINO");
    props.put("UDI_MRF", "UDIMRF");
    props.put("EXP_DATE", "EXPDATE");
    props.put("DONORNO", "DONORNO");
    props.put("GENERICSPEC", "GENERICSPEC");
    props.put("PATIENTLABEL", "PATIENTLABEL");
    props.put("BOXLABEL", "BOXLABEL");
    props.put("FINALCPCVER", "FINALCPCVER");
    props.put("CPCNAME", "CPCNAME");
    props.put("CPCADDRESS", "CPCADDRESS");
    props.put("CPCBARCODELOGO", "CPCBARCODELOGO");
    props.put("CPCFINALLOGO", "CPCFINALLOGO");
    props.put("CPCID", "CPCID");
    props.put("LBL_PATH", "LBL_PATH");
  //  props.put("COMPANYID", "COMPANYID");
    props.put("COMPID", "COMPID");
    props.put("STORAGETEMP", "STORAGETEMP");
    props.put("LBLSIZE", "LBLSIZE");
    props.put("ETCH", "ETCH");
    
    return props;
  }

 }