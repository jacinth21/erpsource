package com.globus.valueobject.operations;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDeviceDetailVO;
import com.globus.valueobject.operations.GmLabelInfoVO;
import com.globus.valueobject.operations.GmLabelPartInfoVO;
import com.globus.valueobject.sales.GmProdRequestDetVO;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLabelPrintListVO extends GmDeviceDetailVO {

	private List<GmLabelInfoVO> listGmLabelInfoVO   = null;
	
	private GmLabelInfoVO[] arrGmLabelInfoVO;
	
	private List<GmLabelPartInfoVO> listGmLabelPartInfoVO   = null;
	


	public List<GmLabelPartInfoVO> getlistGmLabelPartInfoVO() {
		return listGmLabelPartInfoVO;
	}


	public void setlistGmLabelPartInfoVO(List<GmLabelPartInfoVO> listGmLabelPartInfoVO) {
		this.listGmLabelPartInfoVO = listGmLabelPartInfoVO;
	}


	public GmLabelInfoVO[] getArrGmLabelInfoVO() {
		return arrGmLabelInfoVO;
	}


	public void setArrGmLabelPathInfoVO(GmLabelInfoVO[] arrGmLabelInfoVO) {
		this.arrGmLabelInfoVO = arrGmLabelInfoVO;
	}


	public List<GmLabelInfoVO> getListGmLabelInfoVO() {
		return listGmLabelInfoVO;
	}


	public void setListGmLabelInfoVO(
			List<GmLabelInfoVO> listGmLabelInfoVO) {
		this.listGmLabelInfoVO = listGmLabelInfoVO;
	}


	public void populatePageProperties(List alList){
	int alReturnSize= alList.size();
	if(alReturnSize >0){
		HashMap hmPageInfo = GmCommonClass.parseNullHashMap((HashMap)alList.get(0));
		this.setPageno((String)hmPageInfo.get("PAGENO"));
		this.setTotalpages((String)hmPageInfo.get("TOTALPAGES"));
		this.setTotalsize((String)hmPageInfo.get("TOTALSIZE"));
		this.setPagesize((String)hmPageInfo.get("PAGESIZE"));
		}		
	}		

}

