package com.globus.valueobject.operations;


import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)


public class GmLabelStockInfoVO extends GmDataStoreVO {
	private String stocknm = "";
	private String stockdesc = "";
	private String stocktype = "";
	private String stocklength = "";
	private String stockwidth = "";

	private String companyid= "";




	private String compid="";


	
	public String getStocklength() {
		return stocklength;
	}



	public void setStocklength(String stocklength) {
		this.stocklength = stocklength;
	}



	public String getStockwidth() {
		return stockwidth;
	}



	public void setStockwidth(String stockwidth) {
		this.stockwidth = stockwidth;
	}



	public String getStocktype() {
		return stocktype;
	}



	public void setStocktype(String stocktype) {
		this.stocktype = stocktype;
	}



	public String getStocknm() {
		return stocknm;
	}



	public void setStocknm(String stocknm) {
		this.stocknm = stocknm;
	}



	public String getStockdesc() {
		return stockdesc;
	}



	public void setStockdesc(String stockdesc) {
		this.stockdesc = stockdesc;
	}
	


    public String getCompanyid() {
		return companyid;
	}



	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	
	public String getCompid() {
		return compid;
	}



	public void setCompid(String compid) {
		this.compid = compid;
	}



	@Transient
	public Properties getLabelDataProperties(){
		Properties props = new Properties();
		props.put("STOCK_NM","STOCKNM");
		props.put("STOCK_DESC","STOCKDESC");
		props.put("STOCK_TYPE","STOCKTYPE");
		props.put("STOCK_LENGTH","STOCKLENGTH");
		props.put("STOCK_WIDTH","STOCKWIDTH");
	    props.put("COMPANYID", "COMPANYID");
	    props.put("COMPID", "COMPID");

		return props;
}
}
