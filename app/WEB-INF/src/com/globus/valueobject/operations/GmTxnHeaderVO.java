package com.globus.valueobject.operations;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmTxnHeaderVO extends GmDataStoreVO {
	
	private String txnid = "";
	private String txntype = "";
	private String txntypenm = "";
	private String statusfl = "";
	private String setid = "";
	private String setnm = "";
	private String hardcopy = "";
	private String txnparentid = "";
	private String verifyfl = "";
	private String etchid = "";
	private String txnwt = "";
	private String initiatedby = "";
	private String initiatedbynm = "";
	private String reasonnm = "";
	private String statusnm = "";
	
	/*
	 * Mapping
	 * used for gmItemControlRptBean.fetchTransHeaderInfo(strTxnId,strTxnType)
	 */
	@Transient
	public Properties getTxnHeaderMappProps()
  	{
  		Properties properties = new Properties();
  		properties.put("TXNID", "TXNID");
  		properties.put("TXNTYPE", "TXNTYPE");
  		properties.put("TXNTYPENM", "TXNTYPENM");
  		properties.put("STATUSFL", "STATUSFL");
  		properties.put("SETID", "SETID");
  		properties.put("SETNAME", "SETNM");
  		properties.put("HARDCOPY", "HARDCOPY");
  		properties.put("TXNPARENTID", "TXNPARENTID");
  		properties.put("VERIFYFL", "VERIFYFL");
  		properties.put("TXNWT", "TXNWT");
  		properties.put("INITIATEDBY", "INITIATEDBY");
  		properties.put("INITIATEDBYNM", "INITIATEDBYNM");
  		properties.put("REASONNM", "REASONNM");
  		properties.put("STATUSNM", "STATUSNM");
  		return properties;
  	}
	/*
	 * Below method is used for the Loaner mapping under GmShipScanReqResource.java
	 * used for gmInHouseSetsReportBean.getConsignDetails(strTxnId)
	 */
	@Transient
	public Properties getTxnHeaderLoanerMappProps()
  	{
  		Properties properties = new Properties();
  		properties.put("CONSIGNID", "TXNID");
  		properties.put("CTYPE", "TXNTYPE");
  		properties.put("TXNTYPENM", "TXNTYPENM");
  		properties.put("LOANSFLCD", "STATUSFL");
  		properties.put("SETID", "SETID");
  		properties.put("SETNAME", "SETNM");
  		properties.put("TXNPARENTID", "TXNPARENTID");
  		properties.put("TXNWT", "TXNWT");
  		properties.put("ETCHID", "ETCHID");  		
  		properties.put("INITIATEDBY", "INITIATEDBY");
  		properties.put("INITIATEDBYNM", "INITIATEDBYNM");
  		properties.put("LOANSFL", "STATUSNM");
  		return properties;
  	}
	
	/**
	 * @return the txnid
	 */
	public String getTxnid() {
		return txnid;
	}
	/**
	 * @param txnid the txnid to set
	 */
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	/**
	 * @return the txntype
	 */
	public String getTxntype() {
		return txntype;
	}
	/**
	 * @param txntype the txntype to set
	 */
	public void setTxntype(String txntype) {
		this.txntype = txntype;
	}
	/**
	 * @return the txntypenm
	 */
	public String getTxntypenm() {
		return txntypenm;
	}
	/**
	 * @param txntypenm the txntypenm to set
	 */
	public void setTxntypenm(String txntypenm) {
		this.txntypenm = txntypenm;
	}
	/**
	 * @return the statusfl
	 */
	public String getStatusfl() {
		return statusfl;
	}
	/**
	 * @param statusfl the statusfl to set
	 */
	public void setStatusfl(String statusfl) {
		this.statusfl = statusfl;
	}
	/**
	 * @return the setid
	 */
	public String getSetid() {
		return setid;
	}
	/**
	 * @param setid the setid to set
	 */
	public void setSetid(String setid) {
		this.setid = setid;
	}
	
	/**
	 * @return the setnm
	 */
	public String getSetnm() {
		return setnm;
	}

	/**
	 * @param setnm the setnm to set
	 */
	public void setSetnm(String setnm) {
		this.setnm = setnm;
	}

	/**
	 * @return the hardcopy
	 */
	public String getHardcopy() {
		return hardcopy;
	}
	/**
	 * @param hardcopy the hardcopy to set
	 */
	public void setHardcopy(String hardcopy) {
		this.hardcopy = hardcopy;
	}
	/**
	 * @return the txnparentid
	 */
	public String getTxnparentid() {
		return txnparentid;
	}
	/**
	 * @param txnparentid the txnparentid to set
	 */
	public void setTxnparentid(String txnparentid) {
		this.txnparentid = txnparentid;
	}
	/**
	 * @return the verifyfl
	 */
	public String getVerifyfl() {
		return verifyfl;
	}
	/**
	 * @param verifyfl the verifyfl to set
	 */
	public void setVerifyfl(String verifyfl) {
		this.verifyfl = verifyfl;
	}
	/**
	 * @return the etchid
	 */
	public String getEtchid() {
		return etchid;
	}
	/**
	 * @param etchid the etchid to set
	 */
	public void setEtchid(String etchid) {
		this.etchid = etchid;
	}

	/**
	 * @return the txnwt
	 */
	public String getTxnwt() {
		return txnwt;
	}

	/**
	 * @param txnwt the txnwt to set
	 */
	public void setTxnwt(String txnwt) {
		this.txnwt = txnwt;
	}

	/**
	 * @return the initiatedby
	 */
	public String getInitiatedby() {
		return initiatedby;
	}

	/**
	 * @param initiatedby the initiatedby to set
	 */
	public void setInitiatedby(String initiatedby) {
		this.initiatedby = initiatedby;
	}

	/**
	 * @return the initiatedbynm
	 */
	public String getInitiatedbynm() {
		return initiatedbynm;
	}

	/**
	 * @param initiatedbynm the initiatedbynm to set
	 */
	public void setInitiatedbynm(String initiatedbynm) {
		this.initiatedbynm = initiatedbynm;
	}

	/**
	 * @return the reasonnm
	 */
	public String getReasonnm() {
		return reasonnm;
	}

	/**
	 * @param reasonnm the reasonnm to set
	 */
	public void setReasonnm(String reasonnm) {
		this.reasonnm = reasonnm;
	}
	/**
	 * @return the statusnm
	 */
	public String getStatusnm() {
		return statusnm;
	}
	/**
	 * @param statusnm the statusnm to set
	 */
	public void setStatusnm(String statusnm) {
		this.statusnm = statusnm;
	}		
}
