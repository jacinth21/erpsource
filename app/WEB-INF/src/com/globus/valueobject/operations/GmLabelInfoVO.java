package com.globus.valueobject.operations;


import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)


public class GmLabelInfoVO extends GmDataStoreVO {
	private String partid = "";
	private String controlnum = "";
	private String stocktype = "";
	private String stockname = "";
	private String stockdesc = "";
	private String stocklength = "";
	private String stockwidth = "";
	private String labelpath = "";
	private String voidfl = "";
	
	private String companyid= "";

	
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	public String getStocklength() {
		return stocklength;
	}
	public void setStocklength(String stocklength) {
		this.stocklength = stocklength;
	}
	public String getStockwidth() {
		return stockwidth;
	}
	public void setStockwidth(String stockwidth) {
		this.stockwidth = stockwidth;
	}
	public String getStocktype() {
		return stocktype;
	}
	public void setStocktype(String stocktype) {
		this.stocktype = stocktype;
	}
	public String getStockname() {
		return stockname;
	}
	public void setStockname(String stockname) {
		this.stockname = stockname;
	}
	public String getStockdesc() {
		return stockdesc;
	}
	public void setStockdesc(String stockdesc) {
		this.stockdesc = stockdesc;
	}

	public String getPartid() {
		return partid;
	}
	public void setPartid(String partid) {
		this.partid = partid;
	}
	public String getControlnum() {
		return controlnum;
	}
	public void setControlnum(String controlnum) {
		this.controlnum = controlnum;
	}
	public String getLabelpath() {
		return labelpath;
	}
	public void setLabelpath(String labelpath) {
		this.labelpath = labelpath;
	}
	
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	
	@Transient
	public Properties getLabelDataProperties(){
		Properties props = new Properties();
		props.put("PART_NUMBER","PARTID");
		props.put("STOCK_TYPE","STOCKTYPE");
		props.put("LABEL_PATH","LABELPATH");
		props.put("VOID_FL","VOIDFL");
		props.put("STOCK_NAME","STOCKNAME");
		props.put("STOCK_DESC","STOCKDESC");
		props.put("STOCK_LENGTH","STOCKLENGTH");
		props.put("STOCK_WIDTH","STOCKWIDTH");
		props.put("COMPANYID","COMPANYID");

		return props;
}
}
