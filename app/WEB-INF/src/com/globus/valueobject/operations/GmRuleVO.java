package com.globus.valueobject.operations;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmUserVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmRuleVO extends GmUserVO {

	private String ruleid = "";
	private String ruledesc = "";
	private String rulevalue = "";
	private String rulegroup = "";
	private String ruletype = "";
	
	@JsonInclude(Include.NON_NULL)
	private List<GmRuleVO> listGmRuleVO = null;
	
	
	/**
	 * @return the ruleid
	 */
	public String getRuleid() {
		return ruleid;
	}
	/**
	 * @param ruleid the ruleid to set
	 */
	public void setRuleid(String ruleid) {
		this.ruleid = ruleid;
	}
	/**
	 * @return the ruledesc
	 */
	public String getRuledesc() {
		return ruledesc;
	}
	/**
	 * @param ruledesc the ruledesc to set
	 */
	public void setRuledesc(String ruledesc) {
		this.ruledesc = ruledesc;
	}
	/**
	 * @return the rulevalue
	 */
	public String getRulevalue() {
		return rulevalue;
	}
	/**
	 * @param rulevalue the rulevalue to set
	 */
	public void setRulevalue(String rulevalue) {
		this.rulevalue = rulevalue;
	}
	/**
	 * @return the rulegroup
	 */
	public String getRulegroup() {
		return rulegroup;
	}
	/**
	 * @param rulegroup the rulegroup to set
	 */
	public void setRulegroup(String rulegroup) {
		this.rulegroup = rulegroup;
	}
	/**
	 * @return the ruletype
	 */
	public String getRuletype() {
		return ruletype;
	}
	/**
	 * @param ruletype the ruletype to set
	 */
	public void setRuletype(String ruletype) {
		this.ruletype = ruletype;
	}
	/**
	 * @return the listGmRuleVO
	 */
	public List<GmRuleVO> getListGmRuleVO() {
		return listGmRuleVO;
	}
	
	/**
	 * @param listGmRuleVO the listGmRuleVO to set
	 */
	public void setListGmRuleVO(List<GmRuleVO> listGmRuleVO) {
		this.listGmRuleVO = listGmRuleVO;
	}
	
	@Transient
	public Properties getUDIRuleProperties(){
  		Properties properties = new Properties();
  		properties.put("RULE_ID", "RULEID");
  		properties.put("RULE_DESC", "RULEDESC");
  		properties.put("RULE_VALUE", "RULEVALUE");
  		properties.put("RULE_GROUP_ID", "RULEGROUP");
  		properties.put("RULE_TYPE", "RULETYPE");
  		return properties;
  	}		
}
