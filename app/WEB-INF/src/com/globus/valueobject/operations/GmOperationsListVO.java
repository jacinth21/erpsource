package com.globus.valueobject.operations;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmOperationsListVO extends GmDataVO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmLabelStockInfoVO> gmLabelStockInfoVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmTxnPartDtlVO> gmTxnPartDtlVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmTxnCountVO> gmTxnCountVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmTxnShipInfoVO> gmTxnShipInfoVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmShipStationVO> gmShipStationVO = null;
	
	public List<GmLabelStockInfoVO> getGmLabelStockInfoVO() {
		return gmLabelStockInfoVO;
	}
	public void setGmLabelStockInfoVO(List<GmLabelStockInfoVO> gmLabelStockInfoVO) {
		this.gmLabelStockInfoVO = gmLabelStockInfoVO;
	}
	public List<GmTxnPartDtlVO> getGmTxnPartDtlVO() {
		return gmTxnPartDtlVO;
	}
	public void setGmTxnPartDtlVO(List<GmTxnPartDtlVO> gmTxnPartDtlVO) {
		this.gmTxnPartDtlVO = gmTxnPartDtlVO;
	}
	public List<GmTxnCountVO> getGmTxnCountVO() {
		return gmTxnCountVO;
	}
	public void setGmTxnCountVO(List<GmTxnCountVO> gmTxnCountVO) {
		this.gmTxnCountVO = gmTxnCountVO;
	}
	public List<GmTxnShipInfoVO> getGmTxnShipInfoVO() {
		return gmTxnShipInfoVO;
	}
	public void setGmTxnShipInfoVO(List<GmTxnShipInfoVO> gmTxnShipInfoVO) {
		this.gmTxnShipInfoVO = gmTxnShipInfoVO;
	}
	public List<GmShipStationVO> getGmShipStationVO() {
		return gmShipStationVO;
	}
	public void setGmShipStationVO(List<GmShipStationVO> gmShipStationVO) {
		this.gmShipStationVO = gmShipStationVO;
	}	
	
	
		
}
