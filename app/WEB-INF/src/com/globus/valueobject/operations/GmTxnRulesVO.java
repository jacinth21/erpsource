package com.globus.valueobject.operations;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmTxnRulesVO extends GmDataStoreVO {

	private String ruleid = "";
	private String rulemessage = "";
	private String ruledesc = "";
	private String ruleholdtxn = "";
	private String rulepicture = "";
	private String ruleinitby= "";
	private String ruleinitdate = "";
	private String ruleupdatedby ="";
	private String ruleupdateddate ="";
	private String rulepnum = "";
	
	
	
	/**
	 * @return the ruleid
	 */
	public String getRuleid() {
		return ruleid;
	}



	/**
	 * @param ruleid the ruleid to set
	 */
	public void setRuleid(String ruleid) {
		this.ruleid = ruleid;
	}



	/**
	 * @return the rulemessage
	 */
	public String getRulemessage() {
		return rulemessage;
	}



	/**
	 * @param rulemessage the rulemessage to set
	 */
	public void setRulemessage(String rulemessage) {
		this.rulemessage = rulemessage;
	}



	/**
	 * @return the ruledesc
	 */
	public String getRuledesc() {
		return ruledesc;
	}



	/**
	 * @param ruledesc the ruledesc to set
	 */
	public void setRuledesc(String ruledesc) {
		this.ruledesc = ruledesc;
	}



	/**
	 * @return the ruleholdtxn
	 */
	public String getRuleholdtxn() {
		return ruleholdtxn;
	}



	/**
	 * @param ruleholdtxn the ruleholdtxn to set
	 */
	public void setRuleholdtxn(String ruleholdtxn) {
		this.ruleholdtxn = ruleholdtxn;
	}



	/**
	 * @return the rulepicture
	 */
	public String getRulepicture() {
		return rulepicture;
	}



	/**
	 * @param rulepicture the rulepicture to set
	 */
	public void setRulepicture(String rulepicture) {
		this.rulepicture = rulepicture;
	}



	/**
	 * @return the ruleinitby
	 */
	public String getRuleinitby() {
		return ruleinitby;
	}



	/**
	 * @param ruleinitby the ruleinitby to set
	 */
	public void setRuleinitby(String ruleinitby) {
		this.ruleinitby = ruleinitby;
	}



	/**
	 * @return the ruleinitdate
	 */
	public String getRuleinitdate() {
		return ruleinitdate;
	}



	/**
	 * @param ruleinitdate the ruleinitdate to set
	 */
	public void setRuleinitdate(String ruleinitdate) {
		this.ruleinitdate = ruleinitdate;
	}



	/**
	 * @return the ruleupdatedby
	 */
	public String getRuleupdatedby() {
		return ruleupdatedby;
	}



	/**
	 * @param ruleupdatedby the ruleupdatedby to set
	 */
	public void setRuleupdatedby(String ruleupdatedby) {
		this.ruleupdatedby = ruleupdatedby;
	}



	/**
	 * @return the ruleupdateddate
	 */
	public String getRuleupdateddate() {
		return ruleupdateddate;
	}



	/**
	 * @param ruleupdateddate the ruleupdateddate to set
	 */
	public void setRuleupdateddate(String ruleupdateddate) {
		this.ruleupdateddate = ruleupdateddate;
	}



	/**
	 * @return the rulepnum
	 */
	public String getRulepnum() {
		return rulepnum;
	}



	/**
	 * @param rulepnum the rulepnum to set
	 */
	public void setRulepnum(String rulepnum) {
		this.rulepnum = rulepnum;
	}



	// Consequences from Rule Engine
	@Transient
	public Properties getMappingProperties()
  	{
  		Properties properties = new Properties();
  		properties.put("ID", "RULEID");
  		properties.put("NAME", "RULEDESC");
  		properties.put("MESSAGE", "RULEMESSAGE");
  		properties.put("HOLDTXN", "RULEHOLDTXN");
  		properties.put("PICTURE", "RULEPICTURE");
  		properties.put("INITBY", "RULEINITBY");
  		properties.put("INITDATE", "RULEINITDATE");
  		properties.put("UPDBY", "RULEUPDATEDBY");
  		properties.put("UPDDATE", "RULEUPDATEDDATE");
  		properties.put("PNUM", "RULEPNUM");
  		
  		return properties;
  	}
	
	
}
