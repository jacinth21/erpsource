package com.globus.valueobject.operations.rfid;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDeviceInfoVO;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("GmRFIDRequestVO")
public class GmRFIDResponseVO  {
	
	 
	  private String globusbatchid = "";	 
	  private String status = "";
	  private String statusdesc = "";
	  
	public String getGlobusbatchid() {
		return globusbatchid;
	}
	public void setGlobusbatchid(String globusbatchid) {
		this.globusbatchid = globusbatchid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusdesc() {
		return statusdesc;
	}
	public void setStatusdesc(String statusdesc) {
		this.statusdesc = statusdesc;
	}
	
	  
	
}
