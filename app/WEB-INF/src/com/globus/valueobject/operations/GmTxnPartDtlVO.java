package com.globus.valueobject.operations;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmTxnPartDtlVO extends GmDataStoreVO {

  private String pnum = "";
  private String cnum = "";
  private String pdesc = "";
  private String qty = "";
  private String dinum = "";

  /**
   * @return the pnum
   */
  public String getPnum() {
    return pnum;
  }

  /**
   * @param pnum the pnum to set
   */
  public void setPnum(String pnum) {
    this.pnum = pnum;
  }

  /**
   * @return the cnum
   */
  public String getCnum() {
    return cnum;
  }

  /**
   * @param cnum the cnum to set
   */
  public void setCnum(String cnum) {
    this.cnum = cnum;
  }

  /**
   * @return the pdesc
   */
  public String getPdesc() {
    return pdesc;
  }

  /**
   * @param pdesc the pdesc to set
   */
  public void setPdesc(String pdesc) {
    this.pdesc = pdesc;
  }

  /**
   * @return the qty
   */
  public String getQty() {
    return qty;
  }

  /**
   * @param qty the qty to set
   */
  public void setQty(String qty) {
    this.qty = qty;
  }

  /**
   * @return the dinum
   */
  public String getDinum() {
    return dinum;
  }

  /**
   * @param dinum the dinum to set
   */
  public void setDinum(String dinum) {
    this.dinum = dinum;
  }

  // GMOperationsBean.loadSavedSetMaster method is mapped
  @Transient
  public Properties getConsgMappingProperties() {
    Properties properties = new Properties();
    properties.put("PNUM", "PNUM");
    properties.put("CNUM", "CNUM");
    properties.put("PDESC", "PDESC");
    properties.put("IQTY", "QTY");
    properties.put("DINUM", "DINUM");
    return properties;
  }

  // GMOperationsBean.loadSavedSetMaster method is mapped
  @Transient
  public Properties getLNMappingProperties() {
    Properties properties = new Properties();
    properties.put("PNUM", "PNUM");
    properties.put("CNUM", "CNUM");
    properties.put("PDESC", "PDESC");
    properties.put("QTY", "QTY");
    properties.put("DINUM", "DINUM");
    return properties;
  }

  // GmDOBean.fetchCartDetails method is mapped
  @Transient
  public Properties getOrderMappingProperties() {
    Properties properties = new Properties();
    properties.put("ID", "PNUM");
    properties.put("CNUM", "CNUM");
    properties.put("PDESC", "PDESC");
    properties.put("PICKQTY", "QTY");
    properties.put("DINUM", "DINUM");
    return properties;
  }

  // GmLogisticsBean.viewInHouseTransItemsDetails method is mapped
  @Transient
  public Properties getLoanerExtnMappingProperties() {
    Properties properties = new Properties();
    properties.put("PNUM", "PNUM");
    properties.put("CNUM", "CNUM");
    properties.put("PDESC", "PDESC");
    properties.put("DINUM", "DINUM");
    properties.put("IQTY", "QTY");
    return properties;
  }
  
  @Transient
  public Properties getTagPartDetailProperties() {
    Properties properties = new Properties();
    properties.put("PNUM", "PNUM");
    properties.put("PDESC", "PDESC");
    properties.put("QTY", "QTY");
    return properties;
  }
}
