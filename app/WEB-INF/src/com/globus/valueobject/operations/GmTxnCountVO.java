package com.globus.valueobject.operations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmTxnCountVO extends GmDataStoreVO {
	
	private String txntype = "";
	private String txntypenm = "";
	private String txncnt = "";
	private String source = "";
	/**
	 * @return the txntype
	 */
	public String getTxntype() {
		return txntype;
	}
	/**
	 * @param txntype the txntype to set
	 */
	public void setTxntype(String txntype) {
		this.txntype = txntype;
	}
	/**
	 * @return the txntypenm
	 */
	public String getTxntypenm() {
		return txntypenm;
	}
	/**
	 * @param txntypenm the txntypenm to set
	 */
	public void setTxntypenm(String txntypenm) {
		this.txntypenm = txntypenm;
	}
	/**
	 * @return the txncnt
	 */
	public String getTxncnt() {
		return txncnt;
	}
	/**
	 * @param txncnt the txncnt to set
	 */
	public void setTxncnt(String txncnt) {
		this.txncnt = txncnt;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
}
