package com.globus.valueobject.operations.shipping;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPrinterMapVO extends GmDataStoreVO {

	private String regprinternm = "";
	private String zplprinternm = "";
	
	List<GmCodeLookupVO> alRegPrinters = new ArrayList<GmCodeLookupVO>();
	List<GmCodeLookupVO> alZPLPrinters = new ArrayList<GmCodeLookupVO>();
	
	/**
	 * @return the regprinternm
	 */
	public String getRegprinternm() {
		return regprinternm;
	}
	/**
	 * @param regprinternm the regprinternm to set
	 */
	public void setRegprinternm(String regprinternm) {
		this.regprinternm = regprinternm;
	}
	/**
	 * @return the zplprinternm
	 */
	public String getZplprinternm() {
		return zplprinternm;
	}
	/**
	 * @param zplprinternm the zplprinternm to set
	 */
	public void setZplprinternm(String zplprinternm) {
		this.zplprinternm = zplprinternm;
	}
	/**
	 * @return the alRegPrinters
	 */
	public List<GmCodeLookupVO> getAlRegPrinters() {
		return alRegPrinters;
	}
	/**
	 * @param alRegPrinters the alRegPrinters to set
	 */
	public void setAlRegPrinters(List<GmCodeLookupVO> alRegPrinters) {
		this.alRegPrinters = alRegPrinters;
	}
	/**
	 * @return the alZPLPrinters
	 */
	public List<GmCodeLookupVO> getAlZPLPrinters() {
		return alZPLPrinters;
	}
	/**
	 * @param alZPLPrinters the alZPLPrinters to set
	 */
	public void setAlZPLPrinters(List<GmCodeLookupVO> alZPLPrinters) {
		this.alZPLPrinters = alZPLPrinters;
	}


}
