package com.globus.valueobject.operations.shipping;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmTxnShipInfoVO extends GmDataStoreVO {
	
	private String shipid = "";
	private String holdflag = "";
	private String txnid = "";
	private String etchid = "";
	private String shipto = "";
	private String ship_to_id = "";
	private String ship_to_nm = "";
	private String dtype = "";
	private String source = "";
	private String shipmode = "";
	private String shipmodesh = "";
	private String shipcarrier = "";
	private String trackingno = "";
	private String vflag = "";
	private String addressid = "";
	private String status = "";
	private String statusnm = "";
	private String frieghtamt = "";
	private String custpo = "";
	private String distid = "";
	private String repid = "";
	private String accid = "";
	private String ctype = "";
	private String shiptoteid = "";
	private String shipstationid = "";
	private String addr1 = "";
	private String addr2 = "";
	private String shipcity = "";
	private String shipstate = "";
	private String shipcountry = "";
	private String shipzip = "";
	private String sourcenm = "";
	private String shipcarriernm = "";
	private String shipinstruction  = "";
	private String currtxncnt = "";
	private String tottxncnt = "";
	private String suggtoteid = "";
	private String sugstationid = "";
	private String thirdpartyaccno = "";
	private String ph_no = "";
	private String attnto = "";
	private String acctpayorno = "";
	private String countrycode = "";
	
	/*
	 * gmShipScanReqRptBean.fetchSearchTxnList
	 */
	@Transient
	public Properties getSearchTxnListProperties()
	{
		Properties properties = new Properties();
		properties.put("TXNID", "TXNID");
  		properties.put("SOURCE", "SOURCE");  
  		properties.put("ETCHID", "ETCHID");  
  		properties.put("TRACKINGNO", "TRACKINGNO");
  		return properties;
	}
	
	/*
	 * Mapping
	 */
	@Transient
	public Properties getPendingShippingProperties()
	{
		Properties properties = new Properties();
  		properties.put("SHIPID", "SHIPID");
  		properties.put("HOLDFLAG", "HOLDFLAG");
  		properties.put("REFID", "TXNID");
  		properties.put("SHIPTO", "SHIPTO");
  		properties.put("NAMES", "SHIP_TO_ID");
  		properties.put("DTYPE", "DTYPE");
  		properties.put("SOURCE", "SOURCE");
  		properties.put("SHIPMODE", "SHIPMODE");
  		properties.put("SHIPMODESH", "SHIPMODESH");
  		properties.put("SHIPCARRIER", "SHIPCARRIER");
  		properties.put("TRACKINGNO", "TRACKINGNO");  		
  		properties.put("VFLAG", "VFLAG");
  		properties.put("ADDRESSID", "ADDRESSID");
  		properties.put("STATUS", "STATUS");
  		properties.put("STATUSNM", "STATUSNM");
  		properties.put("FRIEGHTAMT", "FRIEGHTAMT");
  		properties.put("CUSTPO", "CUSTPO");
  		properties.put("DISTID", "DISTID");
  		properties.put("REPID", "REPID");
  		properties.put("ACCID", "ACCID");
  		properties.put("CTYPE", "CTYPE");
  		properties.put("TOTEID", "SHIPTOTEID");
  		properties.put("PICKSTATIONID", "SHIPSTATIONID");
  		properties.put("ADDR1", "ADDR1");
  		properties.put("ADDR2", "ADDR2");
  		properties.put("SHIPCITY", "SHIPCITY");
  		properties.put("SHIPSTATE", "SHIPSTATE");
  		properties.put("SHIPCOUNTRY", "SHIPCOUNTRY");
  		properties.put("SHIPZIP", "SHIPZIP");
  		properties.put("SOURCENM", "SOURCENM");
  		properties.put("SHIPCARRIERNM", "SHIPCARRIERNM");
  		properties.put("SHIPINSTRUCTION", "SHIPINSTRUCTION");  		
  		properties.put("CURRTXNCNT", "CURRTXNCNT");
  		properties.put("TOTTXNCNT", "TOTTXNCNT");
  		properties.put("SUGGTOTEID", "SUGGTOTEID");
  		properties.put("SUGSTATIONID", "SUGSTATIONID");
  		//properties.put("PRINTPAPERWORK", "PRINTPAPERWORK");
  		//properties.put("PRINTSHIPLABEL", "PRINTSHIPLABEL");
  		//properties.put("PRINTRETURNLABEL", "PRINTRETURNLABEL");
  		properties.put("SHIPNM", "SHIP_TO_NM");
  		properties.put("THIRDPARTYACCNO", "THIRDPARTYACCNO");
  		properties.put("PH_NO", "PH_NO");
  		properties.put("ETCHID", "ETCHID");
  		properties.put("ATTNTO", "ATTNTO");
  		properties.put("ACCTPAYORNO", "ACCTPAYORNO");
  		properties.put("COUNTRYCODE", "COUNTRYCODE");
  		
  		
  		return properties;
	}
	
	/*
	 * GmShippingInfoBean.fetchShippingReport
	 */
	@Transient
	public Properties  getShippingReportProperties()
	{
		Properties properties = new Properties();
		properties.put("REFID", "TXNID");
		properties.put("ETCHID", "ETCHID");
		properties.put("SHIPTONM", "SHIP_TO_NM");	
		properties.put("SID", "SHIPID");	
		
		return properties;
	}
	
	/**
	 * @return the countrycode
	 */
	public String getCountrycode() {
		return countrycode;
	}

	/**
	 * @param countrycode the countrycode to set
	 */
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	/**
	 * @return the shipid
	 */
	public String getShipid() {
		return shipid;
	}

	/**
	 * @param shipid the shipid to set
	 */
	public void setShipid(String shipid) {
		this.shipid = shipid;
	}

	/**
	 * @return the holdflag
	 */
	public String getHoldflag() {
		return holdflag;
	}

	/**
	 * @param holdflag the holdflag to set
	 */
	public void setHoldflag(String holdflag) {
		this.holdflag = holdflag;
	}

	

	/**
	 * @return the txnid
	 */
	public String getTxnid() {
		return txnid;
	}

	/**
	 * @param txnid the txnid to set
	 */
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	/**
	 * @return the shipto
	 */
	public String getShipto() {
		return shipto;
	}

	/**
	 * @param shipto the shipto to set
	 */
	public void setShipto(String shipto) {
		this.shipto = shipto;
	}

	/**
	 * @return the ship_to_id
	 */
	public String getShip_to_id() {
		return ship_to_id;
	}

	/**
	 * @param shipToId the ship_to_id to set
	 */
	public void setShip_to_id(String shipToId) {
		ship_to_id = shipToId;
	}

	/**
	 * @return the dtype
	 */
	public String getDtype() {
		return dtype;
	}

	/**
	 * @param dtype the dtype to set
	 */
	public void setDtype(String dtype) {
		this.dtype = dtype;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the shipmode
	 */
	public String getShipmode() {
		return shipmode;
	}

	/**
	 * @param shipmode the shipmode to set
	 */
	public void setShipmode(String shipmode) {
		this.shipmode = shipmode;
	}

	/**
	 * @return the shipcarrier
	 */
	public String getShipcarrier() {
		return shipcarrier;
	}

	/**
	 * @param shipcarrier the shipcarrier to set
	 */
	public void setShipcarrier(String shipcarrier) {
		this.shipcarrier = shipcarrier;
	}

	/**
	 * @return the trackingno
	 */
	public String getTrackingno() {
		return trackingno;
	}

	/**
	 * @param trackingno the trackingno to set
	 */
	public void setTrackingno(String trackingno) {
		this.trackingno = trackingno;
	}

	/**
	 * @return the vflag
	 */
	public String getVflag() {
		return vflag;
	}

	/**
	 * @param vflag the vflag to set
	 */
	public void setVflag(String vflag) {
		this.vflag = vflag;
	}

	/**
	 * @return the addressid
	 */
	public String getAddressid() {
		return addressid;
	}

	/**
	 * @param addressid the addressid to set
	 */
	public void setAddressid(String addressid) {
		this.addressid = addressid;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the frieghtamt
	 */
	public String getFrieghtamt() {
		return frieghtamt;
	}

	/**
	 * @param frieghtamt the frieghtamt to set
	 */
	public void setFrieghtamt(String frieghtamt) {
		this.frieghtamt = frieghtamt;
	}

	/**
	 * @return the custpo
	 */
	public String getCustpo() {
		return custpo;
	}

	/**
	 * @param custpo the custpo to set
	 */
	public void setCustpo(String custpo) {
		this.custpo = custpo;
	}

	/**
	 * @return the distid
	 */
	public String getDistid() {
		return distid;
	}

	/**
	 * @param distid the distid to set
	 */
	public void setDistid(String distid) {
		this.distid = distid;
	}

	/**
	 * @return the repid
	 */
	public String getRepid() {
		return repid;
	}

	/**
	 * @param repid the repid to set
	 */
	public void setRepid(String repid) {
		this.repid = repid;
	}

	/**
	 * @return the accid
	 */
	public String getAccid() {
		return accid;
	}

	/**
	 * @param accid the accid to set
	 */
	public void setAccid(String accid) {
		this.accid = accid;
	}

	/**
	 * @return the ctype
	 */
	public String getCtype() {
		return ctype;
	}

	/**
	 * @param ctype the ctype to set
	 */
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	/**
	 * @return the shiptoteid
	 */
	public String getShiptoteid() {
		return shiptoteid;
	}

	/**
	 * @param shiptoteid the shiptoteid to set
	 */
	public void setShiptoteid(String shiptoteid) {
		this.shiptoteid = shiptoteid;
	}

	/**
	 * @return the shipstationid
	 */
	public String getShipstationid() {
		return shipstationid;
	}

	/**
	 * @param shipstationid the shipstationid to set
	 */
	public void setShipstationid(String shipstationid) {
		this.shipstationid = shipstationid;
	}

	/**
	 * @return the addr1
	 */
	public String getAddr1() {
		return addr1;
	}

	/**
	 * @param addr1 the addr1 to set
	 */
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	/**
	 * @return the addr2
	 */
	public String getAddr2() {
		return addr2;
	}

	/**
	 * @param addr2 the addr2 to set
	 */
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	/**
	 * @return the shipcity
	 */
	public String getShipcity() {
		return shipcity;
	}

	/**
	 * @param shipcity the shipcity to set
	 */
	public void setShipcity(String shipcity) {
		this.shipcity = shipcity;
	}

	/**
	 * @return the shipstate
	 */
	public String getShipstate() {
		return shipstate;
	}

	/**
	 * @param shipstate the shipstate to set
	 */
	public void setShipstate(String shipstate) {
		this.shipstate = shipstate;
	}

	/**
	 * @return the shipcountry
	 */
	public String getShipcountry() {
		return shipcountry;
	}

	/**
	 * @param shipcountry the shipcountry to set
	 */
	public void setShipcountry(String shipcountry) {
		this.shipcountry = shipcountry;
	}

	/**
	 * @return the shipzip
	 */
	public String getShipzip() {
		return shipzip;
	}

	/**
	 * @param shipzip the shipzip to set
	 */
	public void setShipzip(String shipzip) {
		this.shipzip = shipzip;
	}

	/**
	 * @return the currtxncnt
	 */
	public String getCurrtxncnt() {
		return currtxncnt;
	}

	/**
	 * @param currtxncnt the currtxncnt to set
	 */
	public void setCurrtxncnt(String currtxncnt) {
		this.currtxncnt = currtxncnt;
	}

	/**
	 * @return the tottxncnt
	 */
	public String getTottxncnt() {
		return tottxncnt;
	}

	/**
	 * @param tottxncnt the tottxncnt to set
	 */
	public void setTottxncnt(String tottxncnt) {
		this.tottxncnt = tottxncnt;
	}

	/**
	 * @return the shipmodesh
	 */
	public String getShipmodesh() {
		return shipmodesh;
	}

	/**
	 * @param shipmodesh the shipmodesh to set
	 */
	public void setShipmodesh(String shipmodesh) {
		this.shipmodesh = shipmodesh;
	}

	/**
	 * @return the statusnm
	 */
	public String getStatusnm() {
		return statusnm;
	}

	/**
	 * @param statusnm the statusnm to set
	 */
	public void setStatusnm(String statusnm) {
		this.statusnm = statusnm;
	}

	/**
	 * @return the sourcenm
	 */
	public String getSourcenm() {
		return sourcenm;
	}

	/**
	 * @param sourcenm the sourcenm to set
	 */
	public void setSourcenm(String sourcenm) {
		this.sourcenm = sourcenm;
	}

	/**
	 * @return the shipcarriernm
	 */
	public String getShipcarriernm() {
		return shipcarriernm;
	}

	/**
	 * @param shipcarriernm the shipcarriernm to set
	 */
	public void setShipcarriernm(String shipcarriernm) {
		this.shipcarriernm = shipcarriernm;
	}

	/**
	 * @return the shipinstruction
	 */
	public String getShipinstruction() {
		return shipinstruction;
	}

	/**
	 * @param shipinstruction the shipinstruction to set
	 */
	public void setShipinstruction(String shipinstruction) {
		this.shipinstruction = shipinstruction;
	}

	/**
	 * @return the suggtoteid
	 */
	public String getSuggtoteid() {
		return suggtoteid;
	}

	/**
	 * @param suggtoteid the suggtoteid to set
	 */
	public void setSuggtoteid(String suggtoteid) {
		this.suggtoteid = suggtoteid;
	}

	/**
	 * @return the sugstationid
	 */
	public String getSugstationid() {
		return sugstationid;
	}

	/**
	 * @param sugstationid the sugstationid to set
	 */
	public void setSugstationid(String sugstationid) {
		this.sugstationid = sugstationid;
	}

	
	/**
	 * @return the ship_to_nm
	 */
	public String getShip_to_nm() {
		return ship_to_nm;
	}

	/**
	 * @param shipToNm the ship_to_nm to set
	 */
	public void setShip_to_nm(String shipToNm) {
		ship_to_nm = shipToNm;
	}

	/**
	 * @return the thirdpartyaccno
	 */
	public String getThirdpartyaccno() {
		return thirdpartyaccno;
	}

	/**
	 * @param thirdpartyaccno the thirdpartyaccno to set
	 */
	public void setThirdpartyaccno(String thirdpartyaccno) {
		this.thirdpartyaccno = thirdpartyaccno;
	}

	/**
	 * @return the ph_no
	 */
	public String getPh_no() {
		return ph_no;
	}

	/**
	 * @param phNo the ph_no to set
	 */
	public void setPh_no(String phNo) {
		ph_no = phNo;
	}

	/**
	 * @return the etchid
	 */
	public String getEtchid() {
		return etchid;
	}

	/**
	 * @param etchid the etchid to set
	 */
	public void setEtchid(String etchid) {
		this.etchid = etchid;
	}

	/**
	 * @return the attnto
	 */
	public String getAttnto() {
		return attnto;
	}

	/**
	 * @param attnto the attnto to set
	 */
	public void setAttnto(String attnto) {
		this.attnto = attnto;
	}

	/**
	 * @return the acctpayorno
	 */
	public String getAcctpayorno() {
		return acctpayorno;
	}

	/**
	 * @param acctpayorno the acctpayorno to set
	 */
	public void setAcctpayorno(String acctpayorno) {
		this.acctpayorno = acctpayorno;
	}
	
	
	
}
