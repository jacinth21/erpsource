package com.globus.valueobject.operations.shipping;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmShipScanReqVO extends GmDataStoreVO {

	private String scanid = null;
	private String scantype = null;
	private String txnid = null;
	private String source = null;
	
	private String printpaperwork = "";
	private String printshiplabel = "";
	private String printreturnlabel = "";
	private String returntrackingno = ""; 
	
	private String trackingno = "";
	
	private String trackingnolength = "";
	
	@Transient
	public Properties getPrintStatusMapProperties()
	{
		Properties properties = new Properties();
		properties.put("PRINTSHIPLABEL", "PRINTSHIPLABEL");
  		properties.put("PRINTRETURNLABEL", "PRINTRETURNLABEL");  
  		properties.put("TRACKINGNO", "TRACKINGNO"); 
  		properties.put("RETURNTRACKINGNO", "RETURNTRACKINGNO");
  		return properties;
	}
	
	/**
	 * @return the scanid
	 */
	public String getScanid() {
		return scanid;
	}
	/**
	 * @param scanid the scanid to set
	 */
	public void setScanid(String scanid) {
		this.scanid = scanid;
	}
	/**
	 * @return the scantype
	 */
	public String getScantype() {
		return scantype;
	}
	/**
	 * @param scantype the scantype to set
	 */
	public void setScantype(String scantype) {
		this.scantype = scantype;
	}
	/**
	 * @return the txnid
	 */
	public String getTxnid() {
		return txnid;
	}
	/**
	 * @param txnid the txnid to set
	 */
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the printpaperwork
	 */
	public String getPrintpaperwork() {
		return printpaperwork;
	}
	/**
	 * @param printpaperwork the printpaperwork to set
	 */
	public void setPrintpaperwork(String printpaperwork) {
		this.printpaperwork = printpaperwork;
	}
	/**
	 * @return the printshiplabel
	 */
	public String getPrintshiplabel() {
		return printshiplabel;
	}
	/**
	 * @param printshiplabel the printshiplabel to set
	 */
	public void setPrintshiplabel(String printshiplabel) {
		this.printshiplabel = printshiplabel;
	}
	/**
	 * @return the printreturnlabel
	 */
	public String getPrintreturnlabel() {
		return printreturnlabel;
	}
	/**
	 * @param printreturnlabel the printreturnlabel to set
	 */
	public void setPrintreturnlabel(String printreturnlabel) {
		this.printreturnlabel = printreturnlabel;
	}

	/**
	 * @return the trackingno
	 */
	public String getTrackingno() {
		return trackingno;
	}

	/**
	 * @param trackingno the trackingno to set
	 */
	public void setTrackingno(String trackingno) {
		this.trackingno = trackingno;
	}

	public String getTrackingnolength() {
		return trackingnolength;
	}

	public void setTrackingnolength(String trackingnolength) {
		this.trackingnolength = trackingnolength;
	}

	/**
	 * @return the returntrackingno
	 */
	public String getReturntrackingno() {
		return returntrackingno;
	}

	/**
	 * @param returntrackingno the returntrackingno to set
	 */
	public void setReturntrackingno(String returntrackingno) {
		this.returntrackingno = returntrackingno;
	}
		
	
}
