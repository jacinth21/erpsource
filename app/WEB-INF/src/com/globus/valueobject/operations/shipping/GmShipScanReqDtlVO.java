package com.globus.valueobject.operations.shipping;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.globus.valueobject.operations.GmTxnHeaderVO;
import com.globus.valueobject.operations.GmTxnPartDtlVO;
import com.globus.valueobject.operations.GmTxnRulesVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmShipScanReqDtlVO extends GmShipScanReqVO {

	private GmTxnShipInfoVO gmTxnShipInfoVO= new GmTxnShipInfoVO();
	private GmTxnHeaderVO gmTxnHeaderVO= new GmTxnHeaderVO();
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<GmTxnRulesVO> gmTxnRulesListVO = null;
	private List<GmTxnPartDtlVO>  gmTxnPartListVO = new ArrayList<GmTxnPartDtlVO>();
	
	/**
	 * @return the gmTxnRulesListVO
	 */
	public List<GmTxnRulesVO> getGmTxnRulesListVO() {
		return gmTxnRulesListVO;
	}
	/**
	 * @param gmTxnRulesListVO the gmTxnRulesListVO to set
	 */
	public void setGmTxnRulesListVO(List<GmTxnRulesVO> gmTxnRulesListVO) {
		this.gmTxnRulesListVO = gmTxnRulesListVO;
	}
	/**
	 * @return the gmTxnPartListVO
	 */
	public List<GmTxnPartDtlVO> getGmTxnPartListVO() {
		return gmTxnPartListVO;
	}
	/**
	 * @param gmTxnPartListVO the gmTxnPartListVO to set
	 */
	public void setGmTxnPartListVO(List<GmTxnPartDtlVO> gmTxnPartListVO) {
		this.gmTxnPartListVO = gmTxnPartListVO;
	}
	/**
	 * @return the gmTxnShipInfoVO
	 */
	public GmTxnShipInfoVO getGmTxnShipInfoVO() {
		return gmTxnShipInfoVO;
	}
	/**
	 * @param gmTxnShipInfoVO the gmTxnShipInfoVO to set
	 */
	public void setGmTxnShipInfoVO(GmTxnShipInfoVO gmTxnShipInfoVO) {
		this.gmTxnShipInfoVO = gmTxnShipInfoVO;
	}
	/**
	 * @return the gmTxnHeaderVO
	 */
	public GmTxnHeaderVO getGmTxnHeaderVO() {
		return gmTxnHeaderVO;
	}
	/**
	 * @param gmTxnHeaderVO the gmTxnHeaderVO to set
	 */
	public void setGmTxnHeaderVO(GmTxnHeaderVO gmTxnHeaderVO) {
		this.gmTxnHeaderVO = gmTxnHeaderVO;
	}
	
	
}
