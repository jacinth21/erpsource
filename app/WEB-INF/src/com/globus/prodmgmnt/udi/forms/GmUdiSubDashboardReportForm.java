package com.globus.prodmgmnt.udi.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmUdiSubDashboardReportForm extends GmCommonForm {
  private String partNum = "";
  private String gridXmlData = "";
  private String diNumber = "";
  private String errorFromDt = "";
  private String errorToDt = "";
  private String errorStatus = "";
  private String errorCategory = "";
  private String inputStr = "";

  private String projectId = "";
  private String screenName = "";
  private String gridErrorXmlData = "";
  private String internalSts = "";
  private String externalSts = "";
  private String udiErrorAcessfl = "";
  
  //PC-2158 UDI SubDiStatus - Improvement work within multiple Di Numbers and Part Numbers
  private String diNumberText = "";
  private String partNumberText  = "";

  private ArrayList alErrorStatus = new ArrayList();
  private ArrayList alErrorCategory = new ArrayList();
  private ArrayList alProjectId = new ArrayList();
  private ArrayList alInternalStatus = new ArrayList();
  private ArrayList alExternalStatus = new ArrayList();



  /**
   * @return the inputStr
   */
  public String getInputStr() {
    return inputStr;
  }

  /**
   * @param inputStr the inputStr to set
   */
  public void setInputStr(String inputStr) {
    this.inputStr = inputStr;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the diNumber
   */
  public String getDiNumber() {
    return diNumber;
  }

  /**
   * @param diNumber the diNumber to set
   */
  public void setDiNumber(String diNumber) {
    this.diNumber = diNumber;
  }

  /**
   * @return the errorFromDt
   */
  public String getErrorFromDt() {
    return errorFromDt;
  }

  /**
   * @param errorFromDt the errorFromDt to set
   */
  public void setErrorFromDt(String errorFromDt) {
    this.errorFromDt = errorFromDt;
  }

  /**
   * @return the errorToDt
   */
  public String getErrorToDt() {
    return errorToDt;
  }

  /**
   * @param errorToDt the errorToDt to set
   */
  public void setErrorToDt(String errorToDt) {
    this.errorToDt = errorToDt;
  }

  /**
   * @return the errorStatus
   */
  public String getErrorStatus() {
    return errorStatus;
  }

  /**
   * @param errorStatus the errorStatus to set
   */
  public void setErrorStatus(String errorStatus) {
    this.errorStatus = errorStatus;
  }

  /**
   * @return the errorCategory
   */
  public String getErrorCategory() {
    return errorCategory;
  }

  /**
   * @param errorCategory the errorCategory to set
   */
  public void setErrorCategory(String errorCategory) {
    this.errorCategory = errorCategory;
  }

  /**
   * @return the alErrorStatus
   */
  public ArrayList getAlErrorStatus() {
    return alErrorStatus;
  }

  /**
   * @param alErrorStatus the alErrorStatus to set
   */
  public void setAlErrorStatus(ArrayList alErrorStatus) {
    this.alErrorStatus = alErrorStatus;
  }

  /**
   * @return the alErrorCategory
   */
  public ArrayList getAlErrorCategory() {
    return alErrorCategory;
  }

  /**
   * @param alErrorCategory the alErrorCategory to set
   */
  public void setAlErrorCategory(ArrayList alErrorCategory) {
    this.alErrorCategory = alErrorCategory;
  }

  /**
   * @return the screenName
   */
  public String getScreenName() {
    return screenName;
  }

  /**
   * @param screenName the screenName to set
   */
  public void setScreenName(String screenName) {
    this.screenName = screenName;
  }

  /**
   * @return the gridErrorXmlData
   */
  public String getGridErrorXmlData() {
    return gridErrorXmlData;
  }

  /**
   * @param gridErrorXmlData the gridErrorXmlData to set
   */
  public void setGridErrorXmlData(String gridErrorXmlData) {
    this.gridErrorXmlData = gridErrorXmlData;
  }

  /**
   * @return the projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @param projectId the projectId to set
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @return the alProjectId
   */
  public ArrayList getAlProjectId() {
    return alProjectId;
  }

  /**
   * @param alProjectId alProjectId the alProjectId to set
   */
  public void setAlProjectId(ArrayList alProjectId) {
    this.alProjectId = alProjectId;
  }

  /**
   * @return the internalSts
   */
  public String getInternalSts() {
    return internalSts;
  }

  /**
   * @param internalSts internalSts the internalSts to set
   */
  public void setInternalSts(String internalSts) {
    this.internalSts = internalSts;
  }

  /**
   * @return the externalSts
   */
  public String getExternalSts() {
    return externalSts;
  }

  /**
   * @param externalSts externalSts the externalSts to set
   */
  public void setExternalSts(String externalSts) {
    this.externalSts = externalSts;
  }

  /**
   * @return the alInternalStatus
   */
  public ArrayList getAlInternalStatus() {
    return alInternalStatus;
  }

  /**
   * @param alInternalStatus alInternalStatus the alInternalStatus to set
   */
  public void setAlInternalStatus(ArrayList alInternalStatus) {
    this.alInternalStatus = alInternalStatus;
  }

  /**
   * @return the alExternalStatus
   */
  public ArrayList getAlExternalStatus() {
    return alExternalStatus;
  }

  /**
   * @param alExternalStatus alExternalStatus the alExternalStatus to set
   */

  public void setAlExternalStatus(ArrayList alExternalStatus) {
    this.alExternalStatus = alExternalStatus;
  }

  /**
   * @return the udiErrorAcessfl
   */
  public String getUdiErrorAcessfl() {
    return udiErrorAcessfl;
  }

  /**
   * @param udiErrorAcessfl the udiErrorAcessfl to set
   */
  public void setUdiErrorAcessfl(String udiErrorAcessfl) {
    this.udiErrorAcessfl = udiErrorAcessfl;
  }

  //PC-2158 UDI SubDiStatus - Improvement work within multiple Di Numbers and Part Numbers
  /**
	* @return the diNumberText
	*/
  public String getDiNumberText() {
	return diNumberText;
  }

  /**
	* @param diNumberText the diNumberText to set
	*/
  public void setDiNumberText(String diNumberText) {
	this.diNumberText = diNumberText;
  }

  /**
	* @return the partNumberText
	*/
	public String getPartNumberText() {
		return partNumberText;
	}

  /**
	* @param partNumberText the partNumberText to set
	*/
	public void setPartNumberText(String partNumberText) {
		this.partNumberText = partNumberText;
	}


}
