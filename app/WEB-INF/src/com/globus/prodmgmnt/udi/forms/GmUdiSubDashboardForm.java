package com.globus.prodmgmnt.udi.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;


public class GmUdiSubDashboardForm extends GmCommonForm {
  private String diNumber = "";
  private String partNum = "";
  private String gridXmlData = "";
  private String projectId = "";
  private String inputStr = "";
  private String missingPartNum = "";
  private String UDIacessFl = "";
  
  //PC-1006 UDI Review Dashboard - Improvement work the diNumber and partNum field - with comma separated values.
  private String diNumberText = "";
  private String partNumberText  = "";

  private ArrayList alProjectId = new ArrayList();


  /**
   * @return the inputStr
   */

  public String getInputStr() {
    return inputStr;
  }

  /**
   * @param inputStr the inputStr to set
   */
  public void setInputStr(String inputStr) {
    this.inputStr = inputStr;
  }

  /**
   * @return the diNumber
   */
  public String getDiNumber() {
    return diNumber;
  }

  /**
   * @param diNumber the diNumber to set
   */
  public void setDiNumber(String diNumber) {
    this.diNumber = diNumber;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the alProjectId
   */
  public ArrayList getAlProjectId() {
    return alProjectId;
  }

  /**
   * @param alProjectId the alProjectId to set
   */
  public void setAlProjectId(ArrayList alProjectId) {
    this.alProjectId = alProjectId;
  }

  /**
   * @return the projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @param projectId the projectId to set
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the missingPartNum
   */
  public String getMissingPartNum() {
    return missingPartNum;
  }

  /**
   * @param missingPartNum the missingPartNum to set
   */

  public void setMissingPartNum(String missingPartNum) {
    this.missingPartNum = missingPartNum;
  }

  /**
   * @return the uDIacessFl
   */
  public String getUDIacessFl() {
    return UDIacessFl;
  }

  /**
   * @param uDIacessFl the uDIacessFl to set
   */
  public void setUDIacessFl(String uDIacessFl) {
    UDIacessFl = uDIacessFl;
  }

  //PC-1006 UDI Review Dashboard - Improvement work the diNumber and partNum field - with comma separated values.
  /**
   * @return the diNumberText
   */
  public String getDiNumberText() {
	return diNumberText;
  }
  
  /**
   * @param diNumberText the diNumberText to set
   */
  public void setDiNumberText(String diNumberText) {
	this.diNumberText = diNumberText;
  }

  /**
   * @return the partNumberText
   */
  public String getPartNumberText() {
	return partNumberText;
  }
  
  /**
   * @param partNumberText the partNumberText to set
   */
  public void setPartNumberText(String partNumberText) {
	this.partNumberText = partNumberText;
  }

}
