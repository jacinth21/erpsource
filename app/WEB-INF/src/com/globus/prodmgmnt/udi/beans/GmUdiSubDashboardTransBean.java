package com.globus.prodmgmnt.udi.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmUdiSubDashboardTransBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * saveReviewDashboardDtls - save the review dash board status
	 * 
	 * @param hmParam
	 * @throws AppError
	 */

	public void saveReviewDashboardDtls (HashMap hmParam) throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		
		String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_pd_udi_sub_dashboard.gm_upd_gudid_status_dtls", 2);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	/**
	 * saveErrorReportDtls - update the error status
	 * 
	 * @param hmParam
	 * @throws AppError
	 */

	public void saveErrorReportDtls (HashMap hmParam) throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		
		String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_pd_udi_sub_dashboard.gm_upd_error_report_dtls", 2);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
