package com.globus.prodmgmnt.udi.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

public class GmUdiSubDashboardReportBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchWIPDashboardDtls - This method used to fetch the WIP dash board details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchWIPDashboardDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strDINumber = GmCommonClass.parseNull((String) hmParam.get("DINUMBER"));
    String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
    Boolean blAppendFl = false;

    sbQuery
        .append(" SELECT v2071.C2060_DI_NUMBER di_number, v2071.C202_PROJECT_ID project_id, v2071.C202_PROJECT_NM project_desc ");
    sbQuery
        .append(", v2071.C205_PART_NUMBER_ID part_number, v2071.C205_PART_NUM_DESC part_desc, v2071.VALIDATION_ERROR_COUNT ");
    sbQuery.append("vali_error_count, v2071.MISSING_COUNT mis_count, v2071.RFS_FLAG rfs_flag ");
    sbQuery
        .append(", v2071.C2071_LAST_UPDATED_BY last_upd_by, v2071.C2071_LAST_UPDATED_DATE last_upd_dt, v2071.REQUIRED_COUNT req_count ");
    sbQuery
        .append(", v2071.AVAILABLE_COUNT avai_count, v2071.MODIFIED_COUNT mod_count, v2071.C2071_IS_ALL_DATA_AVAILABLE all_data_ava ");
    sbQuery
        .append(", v2071.C9030_JOB_ACTIVITY_ID job_activity_id, v2071.C2071_IS_READY_FOR_VALIDATION is_ready_for_val ");
    sbQuery.append("FROM v2071_udi_wip_summary v2071 ");
    if (!strDINumber.equals("") || !strPartNumbers.equals("") || !strProjectId.equals("0")) {
      sbQuery.append("WHERE ");
    }
    // DI Number check
    if (!strDINumber.equals("")) {
      sbQuery.append(" v2071.C2060_DI_NUMBER IN ( '"); //PC-2158 UDI WIP Dashboard - Improvement work within multiple Di Numbers
      sbQuery.append(strDINumber);
      sbQuery.append("') ");
      blAppendFl = true;
    }
    // Part numbers check
    if (!strPartNumbers.equals("")) {
      if (blAppendFl) {
        sbQuery.append(" AND ");
      }
      
      sbQuery.append(" REGEXP_LIKE (v2071.c205_part_number_id, REGEXP_REPLACE('");
      sbQuery.append(strPartNumbers);
      sbQuery.append("','[+]','\\+'))");
      
      blAppendFl = true;
    }
    // Project Id check
    if (!strProjectId.equals("0")) {
      if (blAppendFl) {
        sbQuery.append(" AND ");
      }
      sbQuery.append(" v2071.c202_project_id = '" + strProjectId + "' ");
    }
    log.debug(" query for fetchWIPDashboardDtls ==> " + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alResult;
  }

  /**
   * fetchWIPMissingAttributeDtls - This method used to fetch the WIP Missing attribute details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchWIPMissingAttributeDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();

    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));

    gmDBManager.setPrepareString("gm_pkg_pd_udi_sub_dashboard.gm_fch_wip_missing_attr_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alResult;
  }

  /**
   * fetchReviewDashboardDtls - This method used to fetch the Review dash board details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchReviewDashboardDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS);
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strDINumber = GmCommonClass.parseNull((String) hmParam.get("DINUMBER"));
    String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
    Boolean appendFlg = false;

    sbQuery
        .append(" SELECT v2070.DE2_PRIMARY_DI_NUMBER di_number, v2070.DE9_MODEL_NUMBER_S part_number, v2070.C205_PART_NUM_DESC part_desc ");
    sbQuery
        .append(", v2070.C202_PROJECT_ID project_id, v2070.C202_PROJECT_NM project_desc, v2070.DE3_DEVICE_COUNT dev_count ");
    sbQuery
        .append(", v2070.DE4_UNIT_DI_NUMBER unit_di_number, v2070.DE5_LABELER_DUNS_NUMBER labeler_duns_num, v2070.DE8_BRAND_NUM brand_num ");
    sbQuery
        .append(", v2070.DE9_MODEL_NUMBER_S model_num, v2070.DE12_FDAGUDIDPUBLISHDATE fda_gudid_pub_dt, v2070.DE15_DEVICE_SUBJECT_DM device_sub_dm ");
    sbQuery
        .append(", v2070.DE17_DM_DI_NUMBER dm_di_num, v2070.DE21_QUANTITY_PER_PACKAGE qty_per_pack, v2070.DE26_CUSTOMER_CONTACT_PHONE cust_con_ph ");
    sbQuery
        .append(", v2070.DE27_CUSTOMER_CONTACT_EMAIL cust_con_email, v2070.DE28_HCTP hctp, v2070.DE31_DEV_EXEMPT_FROM_PREMARKET dev_exempt ");
    sbQuery
        .append(", v2070.DE36_FDA_LISTING_NUMBER fda_list_num, v2070.DE37_GMDNCODE gmdn_code, v2070.DE40_FOR_SINGLE_USE for_single_use ");
    sbQuery
        .append(", v2070.DE41_LOT_BATCH lot_batch, v2070.DE42_MANUFACTURING_DATE manuf_dt, v2070.DE43_SERIAL_NUMBER serial_num ");
    sbQuery
        .append(", v2070.DE44_EXPIRATION_DATE exp_dt, v2070.DE45_DONATION_IDENTIFICATION donation_iden, v2070.DE46_NATURAL_RUBBER_LATEX natural_rubber ");
    sbQuery
        .append(", v2070.DE50_MRI_SAFETY_STATUS mri_safety, v2070.DE51_TYPE1 type1, v2070.DE51_TYPE2 type2 ");
    sbQuery.append(", v2070.DE51_TYPE3 type3, v2070.DE51_TYPE4 type4, v2070.DE52_VALUE1 value1 ");
    sbQuery
        .append(", v2070.DE52_VALUE2 value2, v2070.DE52_VALUE3 value3, v2070.DE52_VALUE4 value4 ");
    sbQuery.append(", v2070.DE53_UOM1 uom1, v2070.DE53_UOM2 uom2, v2070.DE53_UOM3 uom3 ");
    sbQuery
        .append(", v2070.DE53_UOM4 uom4, v2070.DE54_SIZE_TYPE_TEXT1 text1, v2070.DE54_SIZE_TYPE_TEXT2 text2 ");
    sbQuery
        .append(", v2070.DE54_SIZE_TYPE_TEXT3 text3, v2070.DE54_SIZE_TYPE_TEXT4 text4, v2070.DE60_DEVICE_PACKAGED_STERILE dev_pack_sterile ");
    sbQuery
        .append(", v2070.DE62_STERILIZATION_METHOD steril_method, v2070.DE32_FDA_SUB_NUM_1 sub_num1, v2070.DE32_FDA_SUB_NUM_2 sub_num2 ");
    sbQuery
        .append(", v2070.DE32_FDA_SUB_NUM_3 sub_num3, v2070.DE32_FDA_SUB_NUM_4 sub_num4, v2070.DE32_FDA_SUB_NUM_5 sub_num5 ");
    sbQuery
        .append(", v2070.DE32_FDA_SUB_NUM_6 sub_num6, v2070.DE32_FDA_SUB_NUM_7 sub_num7, v2070.DE32_FDA_SUB_NUM_8 sub_num8 ");
    sbQuery
        .append(", v2070.DE32_FDA_SUB_NUM_9 sub_num9, v2070.DE32_FDA_SUB_NUM_10 sub_num10, v2070.DE33_FDA_SUPP_NUM_1 supp_num1 ");
    sbQuery
        .append(", v2070.DE33_FDA_SUPP_NUM_2 supp_num2, v2070.DE33_FDA_SUPP_NUM_3 supp_num3, v2070.DE33_FDA_SUPP_NUM_4 supp_num4 ");
    sbQuery
        .append(", v2070.DE33_FDA_SUPP_NUM_5 supp_num5, v2070.DE33_FDA_SUPP_NUM_6 supp_num6, v2070.DE33_FDA_SUPP_NUM_7 supp_num7 ");
    sbQuery
        .append(", v2070.DE33_FDA_SUPP_NUM_8 supp_num8, v2070.DE33_FDA_SUPP_NUM_9 supp_num9, v2070.DE33_FDA_SUPP_NUM_10 supp_num10 ");
    sbQuery
        .append(", v2070.DE34_FDA_PRODUCTCODE_1 prod_code1, v2070.DE34_FDA_PRODUCTCODE_2 prod_code2, v2070.DE34_FDA_PRODUCTCODE_3 prod_code3 ");
    sbQuery
        .append(", v2070.DE34_FDA_PRODUCTCODE_4 prod_code4, v2070.DE34_FDA_PRODUCTCODE_5 prod_code5, v2070.DE34_FDA_PRODUCTCODE_6 prod_code6 ");
    sbQuery
        .append(", v2070.DE34_FDA_PRODUCTCODE_7 prod_code7, v2070.DE34_FDA_PRODUCTCODE_8 prod_code8, v2070.DE34_FDA_PRODUCTCODE_9 prod_code9 ");
    sbQuery
        .append(", v2070.DE34_FDA_PRODUCTCODE_10 prod_code10, v2070.EXTERNAL_STATUS_ID  EXTERNAL_STATUS_ID ");
    sbQuery.append(", v2070.external_status_name ext_status_nm ");
    sbQuery.append("FROM v2070_udi_parameter_for_review v2070 ");
    if (!strDINumber.equals("") || !strPartNumbers.equals("") || !strProjectId.equals("0")) {
      sbQuery.append("WHERE ");
    }
    // DI Number check
    if (!strDINumber.equals("")) {
    	sbQuery.append(" v2070.DE2_PRIMARY_DI_NUMBER IN ( '"); //PC-1006 UDI Review Dashboard - Improvement work within multiple Di Numbers
        sbQuery.append(strDINumber);
        sbQuery.append("') ");
        appendFlg = true;
    }
    // Part numbers check
    if (!strPartNumbers.equals("")) {
      if (appendFlg) {
        sbQuery.append(" AND ");
      }
      
      sbQuery.append("  REGEXP_LIKE (v2070.c205_part_number_id, REGEXP_REPLACE('");
      sbQuery.append(strPartNumbers);
      sbQuery.append("','[+]','\\+'))");
      
      appendFlg = true;
    }
    // Project Id check
    if (!strProjectId.equals("0")) {
      if (appendFlg) {
        sbQuery.append(" AND ");
      }
      sbQuery.append(" v2070.c202_project_id = '" + strProjectId + "' ");
    }
    log.debug(" query for fetchReviewDashboardDtls ==> " + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alResult;
  }

  /**
   * fetchErrorReportDtls - This method used to fetch the Error reports details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchErrorReportDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strDINumber = GmCommonClass.parseNull((String) hmParam.get("DINUMBER"));
    String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strErrorFromDt = GmCommonClass.parseNull((String) hmParam.get("ERRORFROMDT"));
    String strErrorToDt = GmCommonClass.parseNull((String) hmParam.get("ERRORTODT"));
    String strApplicationDtFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strErrorStatus = GmCommonClass.parseZero((String) hmParam.get("ERRORSTATUS"));
    String strErrorCategory = GmCommonClass.parseZero((String) hmParam.get("ERRORCATEGORY"));
    Boolean appendFlg = false;

    sbQuery
        .append(" SELECT v9033.C9030_JOB_ACTIVITY_ID job_activity_id, v9033.C9033_ERROR_DATE error_dt, v9033.C9030_SOURCE_COUNT ");
    sbQuery
        .append("    source_count, v9033.C9030_PASS_COUNT pass_count, v9033.C9030_FAIL_COUNT fail_count ");
    sbQuery
        .append("  , v9033.C9033_REF_ID ref_id, v9033.C205_PART_NUMBER_ID PART_NUMBER, v9033.C205_PART_NUM_DESC part_desc ");
    sbQuery
        .append("  , v9033.C202_PROJECT_ID project_id, v9033.C202_PROJECT_NM project_desc, get_code_name (v9033.C901_STATUS) status_nm");
    sbQuery
        .append("  , v9033.C901_STATUS status_id, get_code_name (v9033.C901_SCREEN_CATEGORY) screen_category_nm,");
    sbQuery
        .append("    v9033.C901_SCREEN_CATEGORY screen_category_id, v9033.C9032_SCREEN_NAME screen_nm, v9033.C9032_SCREEN_DESCRIPTION");
    sbQuery
        .append("    screen_des, v9033.C9032_ADDITIONAL_INFO add_info, v9033.C9032_CORRECTIVE_ACTION corr_action");
    sbQuery
        .append("  , v9033.C9032_CORRECTIVE_DESCRIPTION corr_desc, v9033.C9033_JOB_ERROR_EVENT_ID error_event_id ");
    sbQuery.append("   FROM V9033_ERROR_EVENT v9033 ");

    if (!strDINumber.equals("") || !strPartNumbers.equals("") || !strErrorFromDt.equals("")
        || !strErrorStatus.equals("0") || !strErrorCategory.equals("0")) {
      sbQuery.append("WHERE ");
    }
    // DI Number check
    if (!strDINumber.equals("")) {
      sbQuery.append(" v9033.c9033_ref_id  = '");
      sbQuery.append(strDINumber);
      sbQuery.append("'");
      appendFlg = true;
    }
    // Part numbers check
    if (!strPartNumbers.equals("")) {
      if (appendFlg) {
        sbQuery.append(" AND ");
      }
      
      sbQuery.append("  REGEXP_LIKE (v9033.c205_part_number_id,REGEXP_REPLACE('");
      sbQuery.append(strPartNumbers);
      sbQuery.append("','[+]','\\+'))");
      
      appendFlg = true;
    }
    // Error Date check
    if (!strErrorFromDt.equals("")) {
      if (appendFlg) {
        sbQuery.append(" AND ");
      }
      sbQuery.append(" TRUNC (v9033.c9033_error_date) BETWEEN to_date ('" + strErrorFromDt + "', '"
          + strApplicationDtFmt + "') AND to_date ('" + strErrorToDt + "', '" + strApplicationDtFmt
          + "') ");
      appendFlg = true;
    }
    // Error Status check
    if (!strErrorStatus.equals("0")) {
      if (appendFlg) {
        sbQuery.append(" AND ");
      }
      sbQuery.append(" v9033.c901_status = " + strErrorStatus);
      appendFlg = true;
    }
    // Error Category check
    if (!strErrorCategory.equals("0")) {
      if (appendFlg) {
        sbQuery.append(" AND ");
      }
      sbQuery.append(" v9033.c901_screen_category =" + strErrorCategory);
    }
    log.debug(" query for fetchErrorReportDtls ==> " + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alResult;
  }

  /**
   * fetchMissingAttributeReports - This method used to fetch the Missing attribute reports details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchMissingAttributeReports(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strDINumber = GmCommonClass.parseNull((String) hmParam.get("DINUMBER"));
    String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    Boolean blAppendFl = false;

    sbQuery
        .append(" SELECT v2071.DE9_PART_NUMBER_ID part_number, v2071.DE2_DI_NUMBER di_number, v2071.C202_PROJECT_ID project_id ");
    sbQuery
        .append("   , v2071.C205_PART_NUM_DESC part_desc, v2071.C202_PROJECT_NM project_desc, v2071.DE50_MRI_SAFETY_INFORMATION ");
    sbQuery
        .append("     mri_safety_info, v2071.DE15_SUBJECT_TO_DM_BUT_EXEMPT subject_dm_exempt, v2071.DE16_DM_DI_IS_NOT_PRIMARY_DI");
    sbQuery
        .append("     dm_di_primary_di, v2071.DE17_DM_DI_NUMBER dm_di_number, v2071.DE28_HCTP hctp");
    sbQuery
        .append("   , v2071.DE40_FOR_SINGLE_USE for_single_use, v2071.DE44_EXPIRATION_DATE expiration_dt, ");
    sbQuery
        .append("     v2071.DE60_DEVICE_PKG_AS_STERILE device_as_sterile, v2071.DE41_LOT_OR_BATCH_NUMBER lot_batch_num, ");
    sbQuery
        .append("     v2071.DE42_MANUFACTURING_DATE manuf_dt, v2071.DE43_SERIAL_NUMBER serial_num, v2071.DE3_DEVICE_COUNT device_count ");
    sbQuery
        .append("   , v2071.DE45_DONATION_IDENT_NUMBER donation_ide_num, v2071.DE46_CONTAINS_LATEX cont_latex, ");
    sbQuery
        .append("     v2071.DE61_REQU_STERIL_PRIOR_TO_USE req_steril_use, v2071.DE62_STERILIZATION_METHOD steril_method, ");
    sbQuery
        .append("     v2071.DE8_BRAND_NAME brand_name, v2071.DE36_FDA_LISTING fda_listing, v2071.DE37_GMDN_CODE gmdn_code ");
    sbQuery
        .append("   , v2071.DE31_EXEMPT_FROM_PREMARKET_SUB exempt_from_premarket, v2071.DE34_PRODUCT_CODE product_code, ");
    sbQuery
        .append("     v2071.DE32_FDA_SUBMISSION_NUMBER fda_subm_num, v2071.DE33_FDA_SUPPLEMENT_NUMBER fda_supp_num, ");
    sbQuery
        .append("     v2071.DE1_ISSUING_AGENCY_CONFIG issuing_agn_config, v2071.DE4_UNIT_OF_USE_DI_NUMBER unit_use_di_num, ");
    sbQuery
        .append("     v2071.DE20_PACKAGE_DI_NUMBER pack_di_num, v2071.DE21_QUANTITY_PER_PACKAGE qty_pack, v2071.DE22_CONTAINS_DI_PACKAGE ");
    sbQuery
        .append("     contains_di_pack, v2071.DE5_LABELER_DUNS_NUMBER labler_duns_num, v2071.DE26_CUSTOMER_CONTACT_PHONE cust_cont_ph ");
    sbQuery
        .append("   , v2071.DE27_CUSTOMER_CONTACT_EMAIL cust_cont_email, v2071.DE51_DEVICE_SIZE_TYPE size_type, ");
    sbQuery
        .append("     v2071.DE52_DEVICE_SIZE_VALUE size_value, v2071.DE53_DEVICE_SIZE_UOM size_uom, v2071.DE53_DEVICE_SIZE_TYPE_TEXT ");
    sbQuery.append("     size_type_text, v2071.DE24_PACKAGE_DISCONTINUE_DATE pack_disc_dt ");
    sbQuery.append("    FROM V2071_UDI_MISSING_ATTRI v2071 ");
    if (!strDINumber.equals("") || !strPartNumbers.equals("") || !strProjectId.equals("0")
        || !strInputStr.equals("")) {
      sbQuery.append("WHERE ");
    }
    // DI Number check
    if (!strDINumber.equals("")) {
      sbQuery.append(" v2071.DE2_DI_NUMBER = '");
      sbQuery.append(strDINumber);
      sbQuery.append("'");
      blAppendFl = true;
    }
    // Part numbers check
    if (!strPartNumbers.equals("")) {
      if (blAppendFl) {
        sbQuery.append(" AND ");
      }
      
      sbQuery.append(" REGEXP_LIKE (v2071.DE9_PART_NUMBER_ID, REGEXP_REPLACE('");
      sbQuery.append(strPartNumbers);
      sbQuery.append("','[+]','\\+'))");
      blAppendFl = true;
    }
    // Project Id check
    if (!strProjectId.equals("0")) {
      if (blAppendFl) {
        sbQuery.append(" AND ");
      }
      sbQuery.append(" v2071.C202_PROJECT_ID = '" + strProjectId + "' ");
    }
    // DI number input String check
    if (!strInputStr.equals("")) {
      if (blAppendFl) {
        sbQuery.append(" AND ");
      }
      sbQuery.append(" v2071.DE2_DI_NUMBER IN (");
      sbQuery.append(strInputStr);
      sbQuery.append(")");
    }
    log.debug(" query for fetchMissingAttributeReports ==> " + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alResult;
  }

  /**
   * fetchDiNumberStatus - This method used to fetch the DI Number Status
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchDiNumberStatus(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strDINumber = GmCommonClass.parseNull((String) hmParam.get("DINUMBER"));
    String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
    String strInternalSts = GmCommonClass.parseZero((String) hmParam.get("INTERNALSTS"));
    String strExternalSts = GmCommonClass.parseZero((String) hmParam.get("EXTERNALSTS"));
    Boolean blAppendFl = false;

    sbQuery
        .append(" SELECT t2072.c2072_di_gudid_status_id di_gudid_status_id, t2072.c2060_di_number di_number, t205.c205_part_number_id part_number, t205.c205_part_num_desc part_desc ");
    sbQuery
        .append("  , t202.c202_project_id project_id, t202.c202_project_nm project_desc, get_code_name (t2072.c901_internal_submit_status) internal_status_nm, t2072.c901_internal_submit_status internal_status_id");
    sbQuery
        .append("  , get_code_name (t2072.c901_external_submit_status) enternal_status_nm, t2072.c901_external_submit_status enternal_status_id, t2072.c2072_document_id document_id");
    sbQuery
        .append("  , get_user_name (t2072.c2072_reviewed_by) reviewed_by, TO_CHAR(t2072.c2072_reviewed_date,get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH:MI:SS PM') reviewed_dt, t2072.c2072_gdsn_item_upload_fl item_upload_fl");
    sbQuery
        .append("  , TO_CHAR(t2072.c2072_gdsn_item_upload_date,get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH:MI:SS PM') item_upload_dt, t2072.c2072_gdsn_publish_upload_fl publish_upload_fl, TO_CHAR(t2072.c2072_gdsn_publish_upload_date,get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH:MI:SS PM') publish_upload_dt");
    sbQuery
        .append("  , t2072.c2072_gdsn_cic_fl cic_fl, TO_CHAR(t2072.c2072_gdsn_cic_date,get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH:MI:SS PM') cic_dt, t2072.c2072_gudid_publish_fl publish_fl, TO_CHAR(t2072.c2072_gudid_publish_date,get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH:MI:SS PM') pblish_dt");
    sbQuery
        .append("  , get_user_name (t2072.c2072_last_updated_by) last_updated_by, TO_CHAR(t2072.c2072_last_updated_date,get_rule_value ('DATEFMT', 'DATEFORMAT') || ' HH:MI:SS PM') last_updated_date");
    sbQuery
        .append("   FROM t2072_di_gudid_status t2072, t2060_di_part_mapping t2060, t205_part_number t205, t202_project t202");
    sbQuery.append("  WHERE t2060.c2060_di_number = t2072.c2060_di_number");
    sbQuery.append("   AND t205.c205_part_number_id = t2060.c205_part_number_id");
    sbQuery.append("   AND t202.c202_project_id = t205.c202_project_id ");
    // DI Number check
    if (!strDINumber.equals("")) {
      sbQuery.append(" AND t2060.c2060_di_number IN ( '"); //PC-2158 UDI SubDiStatus - Improvement work within multiple Di Numbers
      sbQuery.append(strDINumber);
      sbQuery.append("') ");
    }
    // Part numbers check
    if (!strPartNumbers.equals("")) {
    	
    sbQuery.append(" AND REGEXP_LIKE (t205.c205_part_number_id,REGEXP_REPLACE('");
      sbQuery.append(strPartNumbers);
      sbQuery.append("','[+]','\\+'))");
    	
    }
    // Project Id check
    if (!strProjectId.equals("0")) {
      sbQuery.append(" AND t202.c202_project_id = '" + strProjectId + "' ");
    }
    // Internal Status check
    if (!strInternalSts.equals("0")) {
      sbQuery.append(" AND t2072.c901_internal_submit_status = '" + strInternalSts + "' ");
    }
    // External Status check
    if (!strExternalSts.equals("0")) {
      sbQuery.append(" AND t2072.c901_external_submit_status = '" + strExternalSts + "' ");
    }
    sbQuery.append(" AND t2072.c2072_void_fl IS NULL ");
    sbQuery.append(" AND t202.c202_void_fl IS NULL ");
    log.debug(" query for fetchDiNumberStatus ==> " + sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alResult;
  }

  /**
   * fetchDiNumberAttributeDtls - This method used to fetch the DI Number attribute details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchDiNumberAttributeDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();

    String strDiNumber = GmCommonClass.parseNull((String) hmParam.get("DINUMBER"));

    gmDBManager.setPrepareString("gm_pkg_pd_udi_sub_dashboard.gm_fch_di_number_attr_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDiNumber);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alResult;
  }
}
