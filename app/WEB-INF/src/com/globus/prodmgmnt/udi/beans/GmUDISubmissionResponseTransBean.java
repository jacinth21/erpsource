package com.globus.prodmgmnt.udi.beans;

import java.io.File;
import java.util.HashMap;

import org._1sync.read.GmUDISub1wsCatResponseProxyBean;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.util.GmUDIUtil;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author mpatel
 * 
 */

public class GmUDISubmissionResponseTransBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  public static final String CREATE_UDI_XML_FAILED = "GmUDISubCreateXmlFailedEmail";

  // this will be removed all place changed with Data Store VO constructor

  public GmUDISubmissionResponseTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmUDISubmissionResponseTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * UDI response xml.
   * 
   * @return the string
   * @throws Exception
   * @throws AppError
   */

  public void UDIResponseXml(HashMap hmXmlGenParam) {


    String strPathTempDir = "";
    try {
      String strResFilePath =
          GmCommonClass.parseNull(GmCommonClass.getUDIFdaXmlConfig("UDIFDA_SUBRESPONSE_FILEPATH"));
      String strResConTempFilePath =
          GmCommonClass.parseNull(GmCommonClass
              .getUDIFdaXmlConfig("UDIFDA_SUBRESPONSE_TEMP_FILEPATH"));
      String strResFinalFilePath =
          GmCommonClass.parseNull(GmCommonClass
              .getUDIFdaXmlConfig("UDIFDA_SUBRESPONSE_FINAL_FILEPATH"));


      File folderCopy = new File(strResFilePath);

      File[] folderCopylistOfFiles = folderCopy.listFiles();
      log.debug("No: of response files:" + folderCopylistOfFiles.length);
      // Check if any response file exist, if exist create temp folder and proceed further
      if (folderCopylistOfFiles.length > 0) {
        // Get Unique temp Dir for this Job
        strPathTempDir = GmUDIUtil.getUniqueTempDir(strResConTempFilePath);

        log.debug("Unique temp Folder Created strPathTempDir:" + strPathTempDir);

        // Copy file from AS2 to Temp folder created for this Job and delete files from AS2 folder
        GmUDIUtil.copyFilesToDestAndDelFromSrc(strResFilePath, strPathTempDir);
        log.debug("Files Copied from :" + strResFilePath + "  to:" + strPathTempDir);

        moveResponseXmlFileAndSaveUDIResponseXml(hmXmlGenParam, strPathTempDir, strResFinalFilePath);
      } else {
        log.debug("No Response Files found to Read");
      }



    } catch (Exception ex) {
      HashMap hmGeneralParam = new HashMap();
      hmGeneralParam.put("strFileName", strPathTempDir);
      hmGeneralParam.put("strMsgId", strPathTempDir);
      hmGeneralParam.put("XMLTYPE", "Response Xml Coping to Temp Folders");
      hmGeneralParam.put("strTempStringforCreateandResponse", hmXmlGenParam.toString());
      hmGeneralParam.put("strDestFileName", strPathTempDir);
      hmGeneralParam.put("strDestPathAs2", "");
      hmGeneralParam.put("strRequestXsdPath", "");
      hmGeneralParam.put("exception", ex);
      GmUDIUtil.sendJiraEmail(hmGeneralParam);
    }

  }



  /**
   * Move response xml file and save udi response xml.
   * 
   * @param hmXmlGenParam the hm xml gen param
   * @param strPathTempDir the str path temp dir
   * @param strResFinalFilePath the str res final file path
   */
  public void moveResponseXmlFileAndSaveUDIResponseXml(HashMap hmXmlGenParam,
      String strPathTempDir, String strResFinalFilePath) {

    String strFileName = "";
    HashMap hmCollectResParam = null;

    GmUDISub1wsCatResponseProxyBean gmResProxyBean = new GmUDISub1wsCatResponseProxyBean();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());

    File readFolder = new File(strPathTempDir);
    File[] ReadFolderlistOfFiles = readFolder.listFiles();

    // Comparator to Sort file by last modified date.Required in case of multiple responses
    // when server is down
    GmUDIUtil.sortFileByLastModifiedDate(ReadFolderlistOfFiles);

    log.debug("Reading files into VO from Path:" + strPathTempDir);
    for (int i = 0; i < ReadFolderlistOfFiles.length; i++) {
      try {
        if (ReadFolderlistOfFiles[i].isFile()) {

          hmCollectResParam = new HashMap();
          strFileName = ReadFolderlistOfFiles[i].getName();
          hmCollectResParam.put("strPathTempDir", strPathTempDir + strFileName);
          hmCollectResParam.put("strFileName", strFileName);
          log.debug(" Before Reading file into VO:" + strPathTempDir + strFileName);
          gmResProxyBean.populate1wsResponseVO(hmCollectResParam);

          // log.debug("File Read into VO:" + strPathTempDir + strFileName);
          // log.debug("hmCollectResParam:" + hmCollectResParam);
          hmCollectResParam.put("strUserId", hmXmlGenParam.get("strUserId"));

          // Copy file from Temp folder created for this Job and to final Destination where all
          // response Files will be saved
          gmCommonUploadBean.copyFile(strPathTempDir + strFileName, strResFinalFilePath
              + strFileName);

          saveUDIResponseData(hmCollectResParam);
          // Delete files from source folder
          gmCommonUploadBean.deleteFile(strPathTempDir + strFileName);

        } else if (ReadFolderlistOfFiles[i].isDirectory()) {
          log.debug("Directory: " + strFileName);
        }
      } catch (Exception ex) {
        HashMap hmGeneralParam = new HashMap();
        hmGeneralParam.put("strFileName", strFileName);
        hmGeneralParam.put("strMsgId", strFileName);
        hmGeneralParam.put("XMLTYPE", "Response Xml VO Reading");
        hmGeneralParam.put("strTempStringforCreateandResponse", hmCollectResParam.toString());
        hmGeneralParam.put("strDestFileName", strPathTempDir + strFileName);
        hmGeneralParam.put("strDestPathAs2", "");
        hmGeneralParam.put("strRequestXsdPath", "");
        hmGeneralParam.put("exception", ex);
        GmUDIUtil.sendJiraEmail(hmGeneralParam);
      }

    }
    if (readFolder.exists()) {
      readFolder.delete();
    }
  }

  /**
   * Save udi response status dtls.
   * 
   * @param hmCollectResParam the hm collect res param
   */
  public void saveUDIResponseData(HashMap hmCollectResParam) {
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_udi_submission_trans.gm_sav_udi_response_data", 9);
    gmDBManager.setString(1, (String) hmCollectResParam.get("strFileName"));
    gmDBManager.setString(2, (String) hmCollectResParam.get("strMsgId"));
    gmDBManager.setString(3, (String) hmCollectResParam.get("strOrgMsgId"));
    gmDBManager.setString(4, (String) hmCollectResParam.get("strDBInputMsgExc"));
    gmDBManager.setString(5, (String) hmCollectResParam.get("sbDBInputStatus"));
    gmDBManager.setString(6, (String) hmCollectResParam.get("sbDBInputDesc"));
    gmDBManager.setString(7, (String) hmCollectResParam.get("sbDBInputSrcSys"));
    gmDBManager.setString(8, (String) hmCollectResParam.get("XMLTYPE"));
    gmDBManager.setString(9, (String) hmCollectResParam.get("strUserId"));
    gmDBManager.execute();
    gmDBManager.commit();
    log.debug("savesaveUDIResponseStatusDtls:");
  }



}
