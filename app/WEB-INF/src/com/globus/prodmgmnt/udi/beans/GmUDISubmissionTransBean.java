package com.globus.prodmgmnt.udi.beans;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

import oracle.jdbc.driver.OracleTypes;

import org._1sync.create.GmUDISub1wsCatRequestProxyBean;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.util.GmUDIUtil;



/**
 * The Class GmUDISubmissionTransBean.
 */
public class GmUDISubmissionTransBean {

  /** The log. */
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public static final String XMLDATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";

  /**
   * Fetch data to create item xml
   * 
   * @param hmXmlGenParam the hm xml gen param
   * @throws Exception
   */
  public void UDISubItemRequest(HashMap hmXmlGenParam) throws Exception {

    // save and fetch UDI Submission Details
    HashMap hmItemInfo = createUDIItemXml(hmXmlGenParam);
    hmItemInfo.put("hmXmlGenParam", hmXmlGenParam);
    ArrayList alDataAttr = (ArrayList) hmItemInfo.get("alDataAttr");

    if (alDataAttr.size() > 0) {
      // split and create files
      splitAndCreateXMLFiles(hmItemInfo);
    } else {
      log.debug("No Item Data fetched for XML");
    }


  }


  /**
   * Fetch data to create publish xml
   * 
   * @param hmXmlGenParam the hm xml gen param
   * @throws Exception
   */
  public void UDISubPublishRequest(HashMap hmXmlGenParam) throws Exception {

    // save and fetch UDI Submission Details
    HashMap hmItemInfo = createUDIPublishXml(hmXmlGenParam);
    hmItemInfo.put("hmXmlGenParam", hmXmlGenParam);
    ArrayList alDataAttr = (ArrayList) hmItemInfo.get("alDataAttr");

    if (alDataAttr.size() > 0) {
      // split and create files
      splitAndCreateXMLFiles(hmItemInfo);
    } else {
      log.debug("No Publish Data fetched for XML");
    }


  }

  /**
   * Save all the the data required to create item xml and fetch necessary data to create Xml
   * 
   * @param hmItemGenParam the hm item gen param
   * @return the hash map
   * @throws AppError the app error
   */
  public HashMap createUDIItemXml(HashMap hmItemGenParam) throws AppError {

    HashMap hmItemInfo = new HashMap();
    ArrayList alDataAttr = new ArrayList();
    ArrayList alMultiAttr = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS);
    gmDBManager.setPrepareString("gm_pkg_pd_udi_submission_trans.gm_create_udi_item_xml_main", 8);
    gmDBManager.setString(1, (String) hmItemGenParam.get("strInputstr"));
    gmDBManager.setString(2, (String) hmItemGenParam.get("strIntStatus"));
    gmDBManager.setString(3, (String) hmItemGenParam.get("strExtStatus"));
    gmDBManager.setString(4, (String) hmItemGenParam.get("XMLTYPE"));
    gmDBManager.setString(5, (String) hmItemGenParam.get("strUserId"));
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, Types.CHAR);

    gmDBManager.execute();
    alDataAttr =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    alMultiAttr =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(7)));
    String strJobId = gmDBManager.getString(8);
    gmDBManager.commit();
    hmItemInfo.put("alDataAttr", alDataAttr);
    hmItemInfo.put("alMultiAttr", alMultiAttr);
    hmItemInfo.put("strJobId", strJobId);


    return hmItemInfo;
  }


  /**
   * Save all the the data required to create item xml and fetch necessary data to create Xml
   * 
   * @param hmPubGenParam the hm pub gen param
   * @return the hash map
   * @throws AppError the app error
   */
  public HashMap createUDIPublishXml(HashMap hmPubGenParam) throws AppError {

    HashMap hmItemInfo = new HashMap();
    ArrayList alDataAttr = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS);
    gmDBManager.setPrepareString("gm_pkg_pd_udi_submission_trans.gm_create_udi_pub_xml_main", 7);
    gmDBManager.setString(1, (String) hmPubGenParam.get("strInputstr"));
    gmDBManager.setString(2, (String) hmPubGenParam.get("strIntStatus"));
    gmDBManager.setString(3, (String) hmPubGenParam.get("strExtStatus"));
    gmDBManager.setString(4, (String) hmPubGenParam.get("XMLTYPE"));
    gmDBManager.setString(5, (String) hmPubGenParam.get("strUserId"));
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, Types.CHAR);

    gmDBManager.execute();
    alDataAttr =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    String strJobId = gmDBManager.getString(7);

    gmDBManager.commit();
    hmItemInfo.put("alDataAttr", alDataAttr);
    hmItemInfo.put("strJobId", strJobId);
    return hmItemInfo;
  }

  /**
   * Split the fetched data based on split number provided into different data set. Then fetch the
   * message id which will be added to name of the file and in the message id element in the xml.
   * Then take each split data set and message id to create an xml. Once the xml is created save the
   * status details of the Job
   * 
   * @param hmItemInfo the hm item info
   * @throws Exception
   * @throws AppError
   */
  public void splitAndCreateXMLFiles(HashMap hmItemInfo) {

    String strReqFilePath =
        GmCommonClass
            .parseNull(GmCommonClass.getUDIFdaXmlConfig("UDIFDA_SUBREQUEST_TEMP_FILEPATH"));
    String strReqFileExt =
        GmCommonClass.parseNull(GmCommonClass.getUDIFdaXmlConfig("UDIFDA_SUB_FILEEXT"));
    String strsplitXmlSize =
        GmCommonClass.parseNull(GmCommonClass.getUDIFdaXmlConfig("UDIFDA_SUBXML_FILESPLIT_SIZE"));
    String strRequestXsdPath =
        GmCommonClass.parseNull(GmCommonClass.getUDIFdaXmlConfig("UDIFDA_SUBXML_XSDPATH"));
    String strDestPathAs2 =
        GmCommonClass.parseNull(GmCommonClass.getUDIFdaXmlConfig("UDIFDA_SUBREQUEST_AS2_FILEPATH"));


    String strTempDestFileName = "";
    String strDestFileName = "";
    int splitSize = 0;
    int fileSize = 1;
    String strMsgId = "";
    String strFileName = "";

    ArrayList alFinalList = new ArrayList();
    HashMap hmGeneralParam = null;
    ArrayList alTempList = new ArrayList();

    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    GmCommonClass gmCommonClass = new GmCommonClass();

    // split Specific Info for Publish and Item Xml
    hmGeneralParam = additionalXmlSpecificParam(hmItemInfo);

    splitSize = Integer.parseInt(strsplitXmlSize);
    // split data set
    alFinalList = gmCommonClass.splitArrayList((ArrayList) hmItemInfo.get("alDataAttr"), splitSize);

    strTempDestFileName = strReqFilePath;
    strDestFileName = strTempDestFileName;

    // Loop each data set to create XML
    for (int count = 0; count < alFinalList.size(); count++) {
      try {
        alTempList = (ArrayList) alFinalList.get(count);
        splitSize = alTempList.size() < splitSize ? alTempList.size() : splitSize;

        // Get unique Message ID
        strMsgId = getUDISubXMlMessageID(hmGeneralParam);

        // Add Message Id to File Name
        strFileName =
            strMsgId + "_" + (count + 1) + "_" + (fileSize) + "_" + (fileSize + (splitSize - 1))
                + strReqFileExt;
        strDestFileName = strDestFileName + strFileName;
        fileSize = fileSize + splitSize;

        hmGeneralParam.put("strMsgId", strMsgId);
        hmGeneralParam.put("strFileName", strFileName);
        hmGeneralParam.put("alTempList", alTempList);

        // Extract and Create Files
        createXMLFiles(alTempList, strDestFileName, hmGeneralParam);

        boolean boolValidXml = GmUDIUtil.validateXMLSchema(strRequestXsdPath, strDestFileName);
        log.debug("validateXMLSchema:" + boolValidXml);

        // Copy files to destination folder
        gmCommonUploadBean.copyFile(strDestFileName, strDestPathAs2 + strFileName);
        log.debug("Copied files to writeAS2 folder");
        // Delete files from source folder
        gmCommonUploadBean.deleteFile(strDestFileName);
        log.debug("Deleted file from  writetemp folder:" + strDestFileName);

        // Update status dtls
        saveUDIRequestXMlFileDtls(hmGeneralParam);

      } catch (Exception e) {
        // Add Email Param
        hmGeneralParam.put("strDestFileName", strDestFileName);
        hmGeneralParam.put("strTempStringforCreateandResponse", alTempList.toString());
        hmGeneralParam.put("strDestPathAs2", strDestPathAs2);
        hmGeneralParam.put("strRequestXsdPath", strRequestXsdPath);
        hmGeneralParam.put("exception", e);
        GmUDIUtil.sendJiraEmail(hmGeneralParam);
        // Delete files from source folder
        gmCommonUploadBean.deleteFile(strDestFileName);
      }

      // Set to null explicitly
      alTempList = null;
      strMsgId = null;
      strFileName = null;

      strDestFileName = strTempDestFileName;
    }
  }



  /**
   * Method used to split parameters based on the the type of xml to create
   * 
   * @param hmItemInfo the hm item info
   * @return the hash map
   */
  public HashMap additionalXmlSpecificParam(HashMap hmItemInfo) {
    HashMap hmGeneralParam = new HashMap();
    HashMap hmXmlGenParam = (HashMap) hmItemInfo.get("hmXmlGenParam");
    String strXmlType = (String) hmXmlGenParam.get("XMLTYPE");

    hmGeneralParam.put("XMLTYPE", strXmlType);
    hmGeneralParam.put("strJobId", hmItemInfo.get("strJobId"));
    hmGeneralParam.put("strUserId", hmXmlGenParam.get("strUserId"));
    hmGeneralParam.put("strItemUploadFl", "");
    hmGeneralParam.put("strXmlDataFormat", "yyyy-MM-dd'T'HH:mm:ss");

    if (strXmlType.equals("ITEMXML")) {
      hmGeneralParam.put("alMultiAttr", hmItemInfo.get("alMultiAttr"));
      hmGeneralParam.put("strIntSaveStatus", "104824");// Submitted
      hmGeneralParam.put("strExtSaveStatus", "104825");// Submitted Item to 1ws

    } else if (strXmlType.equals("PUBXML")) {

      hmGeneralParam.put("strExtSaveStatus", "104829");// Submitted Publication to 1ws


    }
    return hmGeneralParam;

  }


  /**
   * Instantiate request Proxy object and populates data into VOs
   * 
   * @param alTempList the al temp list
   * @param strDestFileName the str dest file name
   * @param hmGeneralParam the hm general param
   * @throws JAXBException the JAXB exception
   */
  public void createXMLFiles(ArrayList alTempList, String strDestFileName, HashMap hmGeneralParam)
      throws JAXBException {

    GmUDISub1wsCatRequestProxyBean gmUDISub1wsCatRequestProxyBean =
        new GmUDISub1wsCatRequestProxyBean();
    gmUDISub1wsCatRequestProxyBean.populateCatRequestProxyXsd(alTempList, hmGeneralParam,
        strDestFileName);


  }

  /**
   * Save the details of the job and Xml data
   * 
   * @param hmGeneralParam the hm general param
   * @throws AppError the app error
   */
  public void saveUDIRequestXMlFileDtls(HashMap hmGeneralParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager();

    String strInputStr = formInputStrFromXMLFile(hmGeneralParam);

    gmDBManager.setPrepareString("gm_pkg_pd_udi_submission_trans.gm_sav_udi_request_xml_dtls", 6);
    gmDBManager.setString(1, (String) hmGeneralParam.get("strFileName"));
    gmDBManager.setString(2, (String) hmGeneralParam.get("strMsgId"));
    gmDBManager.setString(3, strInputStr);
    gmDBManager.setString(4, (String) hmGeneralParam.get("strUserId"));
    gmDBManager.setString(5, (String) hmGeneralParam.get("XMLTYPE"));
    gmDBManager.setString(6, (String) hmGeneralParam.get("strJobId"));
    gmDBManager.execute();
    gmDBManager.commit();
    log.debug("saveUDIXMlFileDtls:");

  }

  /**
   * This method will fetch message ID.
   * 
   * @author
   * @param hmGeneralParam the hm general param
   * @return the UDI sub x ml message id
   * @throws AppError the app error
   */
  public String getUDISubXMlMessageID(HashMap hmGeneralParam) throws AppError {

    String strMsgid = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("gm_pkg_pd_udi_submission_trans.get_udi_message_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, (String) hmGeneralParam.get("XMLTYPE"));// Not required but need this
                                                                     // param
    gmDBManager.execute();
    strMsgid = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strMsgid;
  }



  /**
   * Form an input string to save data for status and job details
   * 
   * @param hmGeneralParam the hm general param
   * @return the string
   * @throws AppError the app error
   */
  public String formInputStrFromXMLFile(HashMap hmGeneralParam) throws AppError {
    String strFDAPubDt = "";
    String strIntSaveStatus =
        GmCommonClass.parseNull((String) hmGeneralParam.get("strIntSaveStatus"));// "104824"
                                                                                 // ;//Submitted
    String strExtSaveStatus =
        GmCommonClass.parseNull((String) hmGeneralParam.get("strExtSaveStatus"));// "104825";//Submitted
                                                                                 // Item to 1ws
    String strItemUploadFl =
        GmCommonClass.parseNull((String) hmGeneralParam.get("strItemUploadFl"));// "Y";
    StringBuffer sbInputStr = new StringBuffer();

    ArrayList alTempList = (ArrayList) hmGeneralParam.get("alTempList");
    // Loop for each DI
    for (int dataCnt = 0; dataCnt < alTempList.size(); dataCnt++) {
      HashMap hmDataAttrs = (HashMap) alTempList.get(dataCnt);
      String strPkGtin = (String) hmDataAttrs.get("DE2_PRIMARY_DI_NUMBER");
      String strDocID = (String) hmDataAttrs.get("DOCID");
      if (((String) hmGeneralParam.get("XMLTYPE")).equals("ITEMXML")) {
        Date publishDt =
            GmUDIUtil.getStringToDate((String) hmDataAttrs.get("DE12_FDAGUDIDPUBLISHDATE"),
                XMLDATEFORMAT);
        strFDAPubDt = GmCommonClass.getStringFromDate(publishDt, "MM/dd/yyyy HH:mm:ss");
      }

      sbInputStr =
          sbInputStr.append(strPkGtin).append("^").append(strDocID).append("^")
              .append(strItemUploadFl).append("^").append(strIntSaveStatus).append("^")
              .append(strExtSaveStatus).append("^").append(strFDAPubDt).append("|");
    }
    return sbInputStr.toString();
  }



}
