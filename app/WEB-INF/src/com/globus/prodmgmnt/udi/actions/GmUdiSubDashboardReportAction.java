package com.globus.prodmgmnt.udi.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.udi.beans.GmUdiSubDashboardReportBean;
import com.globus.prodmgmnt.udi.beans.GmUdiSubDashboardTransBean;
import com.globus.prodmgmnt.udi.forms.GmUdiSubDashboardReportForm;

public class GmUdiSubDashboardReportAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * UdiMissingAttributeDetail This method is used to load UDI Missing Attribute Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward udiMissingAttributeDetail(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws Exception, AppError {

    GmUdiSubDashboardReportForm gmUdiSubDashboardReportForm =
        (GmUdiSubDashboardReportForm) actionForm;
    gmUdiSubDashboardReportForm.loadSessionParameters(request);

    GmUdiSubDashboardReportBean gmUdiSubDashboardReportBean = new GmUdiSubDashboardReportBean();

    String strxmlGridData = "";
    String strDispatch = "";
    ArrayList almissingAttributeData = new ArrayList();
    HashMap hmUdiWipformData = new HashMap();
    HashMap hmTemplateData = new HashMap();

    hmUdiWipformData = GmCommonClass.getHashMapFromForm(gmUdiSubDashboardReportForm);
    String strOpt = gmUdiSubDashboardReportForm.getStrOpt();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    if (strOpt.equals("MissingAttribute")) {

      almissingAttributeData =
          gmUdiSubDashboardReportBean.fetchWIPMissingAttributeDtls(hmUdiWipformData);
      hmTemplateData.put("ARRAY_DATA", almissingAttributeData);
      hmTemplateData.put("TEMPLATE_NAME", "GmUdiMissingAttributeDetail.vm");
      hmTemplateData.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmTemplateData.put("VMFILEPATH",
          "properties.labels.prodmgmnt.udi.GmUdiMissingAttributeDetail");
      strxmlGridData = generateOutPut(hmTemplateData);
      gmUdiSubDashboardReportForm.setGridXmlData(strxmlGridData);
      strDispatch = "gmMissingAttribute";

    }

    return actionMapping.findForward(strDispatch);

  }

  /**
   * UdiMissingErrorReport This method is used to load UDI Missing Error report Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward udiMissingErrorReport(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {

    GmUdiSubDashboardReportForm gmUdiSubDashboardReportForm =
        (GmUdiSubDashboardReportForm) actionForm;
    gmUdiSubDashboardReportForm.loadSessionParameters(request);

    GmUdiSubDashboardReportBean gmUdiSubDashboardReportBean = new GmUdiSubDashboardReportBean();

    String strxmlGridData = "";
    String strDispatch = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    ArrayList almissingAttributeData = new ArrayList();
    HashMap hmUdiMissingData = new HashMap();
    HashMap hmTemplateData = new HashMap();

    hmUdiMissingData = GmCommonClass.getHashMapFromForm(gmUdiSubDashboardReportForm);
    String strOpt = gmUdiSubDashboardReportForm.getStrOpt();

    almissingAttributeData =
        gmUdiSubDashboardReportBean.fetchMissingAttributeReports(hmUdiMissingData);
    hmTemplateData.put("ARRAY_DATA", almissingAttributeData);
    hmTemplateData.put("TEMPLATE_NAME", "GmUdiMissingAttributeRpt.vm");
    hmTemplateData.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmTemplateData.put("VMFILEPATH", "properties.labels.prodmgmnt.udi.GmUdiMissingAttributeRpt");
    strxmlGridData = generateOutPut(hmTemplateData);
    gmUdiSubDashboardReportForm.setGridXmlData(strxmlGridData);
    strDispatch = "gmMissingErrorReport";

    return actionMapping.findForward(strDispatch);

  }

  /**
   * udiErrorReport This method is used to load UDI Error report Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward udiErrorReport(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {

    GmUdiSubDashboardReportForm gmUdiSubDashboardReportForm =
        (GmUdiSubDashboardReportForm) actionForm;
    gmUdiSubDashboardReportForm.loadSessionParameters(request);

    GmUdiSubDashboardReportBean gmUdiSubDashboardReportBean = new GmUdiSubDashboardReportBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmUdiSubDashboardTransBean gmUdiSubDashboardTransBean = new GmUdiSubDashboardTransBean();

    String strGridXmlData = "";
    String strDispatch = "gmUdiSubErrorReport";
    String strOpt = "";
    String strSearch = "";
    String strPartnum = "";
    String strPartNumFormat = "";
    String strErrorStatus = "";
    String strUdiAcess = "UDI_ERROR_ACCESS";
    String strUserId = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmTemplateData = new HashMap();
    ArrayList alErrorStatus = new ArrayList();
    ArrayList alErrorCategory = new ArrayList();
    ArrayList alReturn = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmUdiSubDashboardReportForm);
    //
    alErrorStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ERRACT"));
    alErrorCategory = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SCRCAT"));

    strErrorStatus = GmCommonClass.parseNull((String) hmParam.get("ERRORSTATUS"));
    strUserId = GmCommonClass.parseNull(gmUdiSubDashboardReportForm.getUserId());
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            strUdiAcess));
    String strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmUdiSubDashboardReportForm.setUdiErrorAcessfl(strAccessFl);

    if (strErrorStatus.equals("")) {

      gmUdiSubDashboardReportForm.setErrorStatus("104900");
    }

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));



    if (strOpt.equals("Save")) {
      gmUdiSubDashboardTransBean.saveErrorReportDtls(hmParam);
      strOpt = "Load";
    }

    if (strOpt.equals("Load")) {
      strSearch = GmCommonClass.parseZero(request.getParameter("Cbo_Search"));
      strPartnum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
      strPartNumFormat = strPartnum;

      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("0")) {
        strPartNumFormat = strPartNumFormat;
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }

      request.setAttribute("hSearch", strSearch);
      hmParam.put("PARTNUM", strPartNumFormat);
      alReturn =
          GmCommonClass.parseNullArrayList(gmUdiSubDashboardReportBean
              .fetchErrorReportDtls(hmParam));
      hmTemplateData.put("ARRAY_DATA", alReturn);
      hmTemplateData.put("TEMPLATE_NAME", "GmUdiSubErrorReport.vm");
      hmTemplateData.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmTemplateData.put("VMFILEPATH", "properties.labels.prodmgmnt.udi.GmUdiSubErrorReport");
      hmTemplateData.put("hmData", hmParam);
      strGridXmlData = generateOutPut(hmTemplateData);
      gmUdiSubDashboardReportForm.setGridXmlData(strGridXmlData);
    }
    gmUdiSubDashboardReportForm.setScreenName("");
    gmUdiSubDashboardReportForm.setAlErrorStatus(alErrorStatus);
    gmUdiSubDashboardReportForm.setAlErrorCategory(alErrorCategory);

    return actionMapping.findForward(strDispatch);

  }

  /**
   * udiDiNumberAttributeDtls This method is used to load UDI DI Number Attribute Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward udiDiNumberAttributeDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {

    GmUdiSubDashboardReportForm gmUdiSubDashboardReportForm =
        (GmUdiSubDashboardReportForm) actionForm;
    gmUdiSubDashboardReportForm.loadSessionParameters(request);

    GmUdiSubDashboardReportBean gmUdiSubDashboardReportBean = new GmUdiSubDashboardReportBean();

    String strGridXmlData = "";
    String strDispatch = "gmUdiDiNumberAttributeDtls";
    String strOpt = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    HashMap hmParam = new HashMap();
    HashMap hmTemplateData = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmUdiSubDashboardReportForm);
    ArrayList alResult = new ArrayList();
    ArrayList alReturn = new ArrayList();
    // attribute details
    alResult =
        GmCommonClass.parseNullArrayList(gmUdiSubDashboardReportBean
            .fetchDiNumberAttributeDtls(hmParam));
    hmTemplateData.put("ARRAY_DATA", alResult);
    hmTemplateData.put("TEMPLATE_NAME", "GmUdiDiNumberAttributeDtls.vm");
    hmTemplateData.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmTemplateData.put("VMFILEPATH", "properties.labels.prodmgmnt.udi.GmUdiDiNumberAttributeDtls");
    strGridXmlData = generateOutPut(hmTemplateData);
    gmUdiSubDashboardReportForm.setGridXmlData(strGridXmlData);
    // error details
    strGridXmlData = "";
    alReturn =
        GmCommonClass.parseNullArrayList(gmUdiSubDashboardReportBean.fetchErrorReportDtls(hmParam));
    hmTemplateData.put("ARRAY_DATA", alReturn);
    hmTemplateData.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmTemplateData.put("VMFILEPATH", "properties.labels.prodmgmnt.udi.GmUdiSubErrorReport");
    hmTemplateData.put("TEMPLATE_NAME", "GmUdiSubErrorReport.vm");
    hmTemplateData.put("hmData", hmParam);
    strGridXmlData = generateOutPut(hmTemplateData);
    gmUdiSubDashboardReportForm.setGridErrorXmlData(strGridXmlData);
    gmUdiSubDashboardReportForm.setScreenName("DIATTR");
    return actionMapping.findForward(strDispatch);
  }

  /**
   * UdiDiStatus This method is used to load UDI DI Status Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward udiDiStatus(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {

    GmUdiSubDashboardReportForm gmUdiSubDashboardReportForm =
        (GmUdiSubDashboardReportForm) actionForm;
    gmUdiSubDashboardReportForm.loadSessionParameters(request);

    GmProjectBean gmProjectBean = new GmProjectBean();
    GmUdiSubDashboardReportBean gmUdiSubDashboardReportBean = new GmUdiSubDashboardReportBean();

    String strxmlGridData = "";
    String strSearch = "";
    String strPartnum = "";
    String strPartNumFormat = "";
    String strDispatch = "gmUdiStatus";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    ArrayList alProjectDetails = new ArrayList();
    ArrayList aludiidistatusdata = new ArrayList();
    ArrayList alInternalStatus = new ArrayList();
    ArrayList alExternalStatus = new ArrayList();
    HashMap hmProjectDetails = new HashMap();
    HashMap hmudidistatus = new HashMap();
    HashMap hmDtls = new HashMap();

    hmudidistatus = GmCommonClass.getHashMapFromForm(gmUdiSubDashboardReportForm);

    hmProjectDetails.put("COLUMN", "ID");
    hmProjectDetails.put("STATUSID", "20301");

    alInternalStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FADIST"));
    alExternalStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FADEST"));
    alProjectDetails = gmProjectBean.reportProject(hmProjectDetails);
    gmUdiSubDashboardReportForm.setAlProjectId(alProjectDetails);
    gmUdiSubDashboardReportForm.setAlInternalStatus(alInternalStatus);
    gmUdiSubDashboardReportForm.setAlExternalStatus(alExternalStatus);


    String strOpt = GmCommonClass.parseNull((String) hmudidistatus.get("STROPT"));

    if (strOpt.equals("load")) {

      // Searching Option using part number
      strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
      strPartnum = GmCommonClass.parseNull((String) hmudidistatus.get("PARTNUM"));
      strPartNumFormat = strPartnum;

      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("")) {
        strPartNumFormat = strPartNumFormat;
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }

      hmudidistatus.put("PARTNUM", strPartNumFormat);
      aludiidistatusdata = gmUdiSubDashboardReportBean.fetchDiNumberStatus(hmudidistatus);
      hmudidistatus.put("ARRAY_DATA", aludiidistatusdata);
      hmudidistatus.put("TEMPLATE_NAME", "GmUdiSubDIStatus.vm");
      hmudidistatus.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmudidistatus.put("VMFILEPATH", "properties.labels.prodmgmnt.udi.GmUdiSubDiStatus");
      hmDtls.put("APPLNDATEFMT", hmudidistatus.get("APPLNDATEFMT"));
      hmudidistatus.put("hmData", hmDtls);
      strxmlGridData = generateOutPut(hmudidistatus);
      gmUdiSubDashboardReportForm.setGridXmlData(strxmlGridData);
      request.setAttribute("hSearch", strSearch);

    }
    return actionMapping.findForward(strDispatch);

  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  private String generateOutPut(HashMap hmMapData) throws AppError {

    ArrayList alResult = new ArrayList();
    String strTemplatename = "";
    HashMap hmData = new HashMap();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmMapData.get("ARRAY_DATA"));
    strTemplatename = GmCommonClass.parseNull((String) hmMapData.get("TEMPLATE_NAME"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmMapData.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmMapData.get("VMFILEPATH"));
    hmData = GmCommonClass.parseNullHashMap((HashMap) hmMapData.get("hmData"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));

    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmData", hmData);
    templateUtil.setTemplateSubDir("prodmgmnt/udi/templates");
    templateUtil.setTemplateName(strTemplatename);
    return templateUtil.generateOutput();

  }

}
