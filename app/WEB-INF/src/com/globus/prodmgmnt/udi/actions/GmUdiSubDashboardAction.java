package com.globus.prodmgmnt.udi.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.udi.beans.GmUdiSubDashboardReportBean;
import com.globus.prodmgmnt.udi.beans.GmUdiSubDashboardTransBean;
import com.globus.prodmgmnt.udi.forms.GmUdiSubDashboardForm;

public class GmUdiSubDashboardAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * UdiWIPDashboard This method is used to load UDI WIP Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward udiWIPDashboard(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {

    GmUdiSubDashboardForm gmUdiSubDashboardForm = (GmUdiSubDashboardForm) actionForm;
    gmUdiSubDashboardForm.loadSessionParameters(request);

    GmProjectBean gmProjectBean = new GmProjectBean();
    GmUdiSubDashboardReportBean gmUdiSubDashboardReportBean = new GmUdiSubDashboardReportBean();

    String strxmlGridData = "";
    String strSearch = "";
    String strPartnum = "";
    String strPartNumFormat = "";
    String strDispatch = "gmUdiWIPDash";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    ArrayList alProjectDetails = new ArrayList();
    ArrayList alUdiWIPdata = new ArrayList();
    ArrayList almissingAttributeData = new ArrayList();
    HashMap hmProjectDetails = new HashMap();
    HashMap hmUdiWipformData = new HashMap();

    hmUdiWipformData = GmCommonClass.getHashMapFromForm(gmUdiSubDashboardForm);

    hmProjectDetails.put("COLUMN", "ID");
    hmProjectDetails.put("STATUSID", "20301");

    alProjectDetails = gmProjectBean.reportProject(hmProjectDetails);
    gmUdiSubDashboardForm.setAlProjectId(alProjectDetails);

    String strOpt = gmUdiSubDashboardForm.getStrOpt();

    if (strOpt.equals("load")) {

      // Searching Option using part number
      strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
      strPartnum = GmCommonClass.parseNull((String) hmUdiWipformData.get("PARTNUM"));
      strPartNumFormat = strPartnum;

      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("")) {
        strPartNumFormat = strPartNumFormat;
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }

      hmUdiWipformData.put("PARTNUM", strPartNumFormat);
      gmUdiSubDashboardForm.setMissingPartNum(strPartNumFormat);
      alUdiWIPdata = gmUdiSubDashboardReportBean.fetchWIPDashboardDtls(hmUdiWipformData);
      hmUdiWipformData.put("ARRAY_DATA", alUdiWIPdata);
      hmUdiWipformData.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmUdiWipformData.put("VMFILEPATH", "properties.labels.prodmgmnt.udi.GmUdiSubWIPDashboard");
      hmUdiWipformData.put("TEMPLATE_NAME", "GmUdiSubWIPDashboard.vm");
      strxmlGridData = generateOutPut(hmUdiWipformData);
      gmUdiSubDashboardForm.setGridXmlData(strxmlGridData);
      request.setAttribute("hSearch", strSearch);

    }
    return actionMapping.findForward(strDispatch);

  }

  /**
   * udiReviewDashboard This method is used to load UDI Review Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward udiReviewDashboard(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    GmProjectBean gmProjectBean = new GmProjectBean();
    GmUdiSubDashboardReportBean gmUdiSubDashboardReportBean = new GmUdiSubDashboardReportBean();
    GmUdiSubDashboardTransBean gmUdiSubDashboardTransBean = new GmUdiSubDashboardTransBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmUdiSubDashboardForm gmUdiSubDashboardForm = (GmUdiSubDashboardForm) actionForm;
    gmUdiSubDashboardForm.loadSessionParameters(request);

    String strOpt = "";
    String strPartNum = "";
    String strPartNumFormat = "";
    String strSearch = "";
    String strInputString = "";
    String StrAcessGrpName = "UDI_REVIEW_ACESSFLAG";
    String strUserId = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmTemplateParams = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alProjectId = new ArrayList();
    ArrayList alResult = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmUdiSubDashboardForm);
    strUserId = GmCommonClass.parseNull(gmUdiSubDashboardForm.getUserId());
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            StrAcessGrpName));
    gmUdiSubDashboardForm.setUDIacessFl(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301");

    alProjectId = gmProjectBean.reportProject(hmTemp);
    gmUdiSubDashboardForm.setAlProjectId(alProjectId);

    if (strOpt.equals("Save")) {

      strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
      gmUdiSubDashboardTransBean.saveReviewDashboardDtls(hmParam);
      strOpt = "Load";
      gmUdiSubDashboardForm.setStrOpt(strOpt);

    }

    if (strOpt.equals("Load")) {

      strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
      strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
      strPartNumFormat = strPartNum;

      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("")) {
        strPartNumFormat = strPartNumFormat;
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }
      hmParam.put("PARTNUM", strPartNumFormat);
      alResult =
          GmCommonClass.parseNullArrayList(gmUdiSubDashboardReportBean
              .fetchReviewDashboardDtls(hmParam));
      hmParam.put("TEMPLATE_NAME", "GmUdiSubReviewDashboard.vm");
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmParam.put("VMFILEPATH", "properties.labels.prodmgmnt.udi.GmUdiSubReviewDashboard");
      hmParam.put("ARRAY_DATA", alResult);

      // XML String
      String strXmlString = generateOutPut(hmParam);
      gmUdiSubDashboardForm.setGridXmlData(strXmlString);
      request.setAttribute("hSearch", strSearch);

    }

    return actionMapping.findForward("gmUdiReviewDash");

  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  private String generateOutPut(HashMap hmMapData) throws AppError {

    ArrayList alResult = new ArrayList();
    String strTemplatename = "";
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmMapData.get("ARRAY_DATA"));
    strTemplatename = GmCommonClass.parseNull((String) hmMapData.get("TEMPLATE_NAME"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmMapData.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmMapData.get("VMFILEPATH"));

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmMapData", hmMapData);
    templateUtil.setTemplateSubDir("prodmgmnt/udi/templates");
    templateUtil.setTemplateName(strTemplatename);
    return templateUtil.generateOutput();
  }

}
