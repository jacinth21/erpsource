/**
 * FileName    : DTProjectsDashboardWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 11, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.displaytag;

import org.apache.commons.beanutils.DynaBean;

import com.globus.displaytag.beans.GmCommonDecorator;

/**
 * @author sthadeshwar
 *
 */
public class DTProjectsDashboardWrapper extends GmCommonDecorator {

	public String getID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String projectId = String.valueOf(db.get("ID"));
		String strValue = "<a href=javascript:fnShowProjectSetup('"+projectId+"');>"+projectId+"</a>";
		return strValue;
	}
	
	public String getKICKOFFDT()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = getStyle(String.valueOf(db.get("KICKOFFDT")), String.valueOf(db.get("KICKOFFDT_FL")));
		return strValue;
		
		
	}
	
	public String getLAUNCHDT()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = getStyle(String.valueOf(db.get("LAUNCHDT")), String.valueOf(db.get("LAUNCHDT_FL")));
		return strValue;
	}
	
	public String getDESIGNSTAFFINGDT()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = getStyle(String.valueOf(db.get("DESIGNSTAFFINGDT")), String.valueOf(db.get("DESIGNSTAFFINGDT_FL")));
		return strValue;
	}
	
	public String getPUGSTAFFINGDT()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = getStyle(String.valueOf(db.get("PUGSTAFFINGDT")), String.valueOf(db.get("PUGSTAFFINGDT_FL")));
		return strValue;
	}
	
	public String getPLINE()
	{
		return "|";
	}
	
	public String getStyle(String strValue, String strDateFlag)
	{
		String strReturnValue = strValue;
		if(strDateFlag.equals("B")) {
			strReturnValue = "<font style=\"FONT-WEIGHT:bold;font-size:10px;\">" + strValue + "</font>";
		} else if(strDateFlag.equals("T")) {
			strReturnValue = "<font style=\"background-color: #ffecb9;\">TBD</font>";
		} else if(strDateFlag.equals("R")) {
			strReturnValue = "<font style=\"FONT-WEIGHT:bold;COLOR:red;font-size:10px;\">" + strValue + "</font>";
		}
		return strReturnValue;
	}
	
	public String addRowClass()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		return String.valueOf(db.get("LAUNCHDT_FL")).equals("T") ? "ShadeDarkGrayTD" : "ShadeDarkGreenTD";
	}
}
