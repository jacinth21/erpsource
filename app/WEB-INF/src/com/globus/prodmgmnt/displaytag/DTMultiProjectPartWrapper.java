package com.globus.prodmgmnt.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmCommonClass;

public class DTMultiProjectPartWrapper extends TableDecorator {
	private DynaBean db;
	private String strMultiPrjPart = "";
	 
	public StringBuffer strValue = new StringBuffer();
	private int intCount = 0;
	private String partNum = "";

	public DTMultiProjectPartWrapper() {
		super();
	}

	public String getMPRJFLG() {
		db = (DynaBean) this.getCurrentRowObject();
		strMultiPrjPart = GmCommonClass.parseNull((String) db.get("MPRJFLG"));
		partNum= GmCommonClass.parseNull((String) db.get("PNUM"));
		String strIsChecked = "";
		if (strMultiPrjPart.equals("on")) {
			strIsChecked = "checked";
		}
		strValue.setLength(0);

		strValue.append("<input type='hidden' name='hpartnum");
		strValue.append(String.valueOf(intCount));
		strValue.append("' value='");
		strValue.append(partNum);
		strValue.append("'>"); 
		strValue.append("<input class=RightText type='checkbox' ");
		strValue.append(strIsChecked);
		strValue.append(" onclick='javascript:fnMasterCheckBox();' name='checkMultiProject");
		strValue.append("' value='");
		strValue.append(strMultiPrjPart);

		strValue.append("'> ");

		intCount++;
		return strValue.toString();
	}

}