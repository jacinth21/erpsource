/**
 * FileName    : DTProjectMilestonesWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.displaytag;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.displaytag.beans.GmCommonDecorator;

/**
 * @author sthadeshwar
 *
 */
public class DTProjectMilestonesWrapper extends GmCommonDecorator {

	private HashMap hmParam = new HashMap();
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	Logger log = GmLogger.getInstance(this.getClass().getName());
	String strRes = "";
	
	public String getPRJDATE()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strNo = hmParam.get("NO").toString();
		String strId = hmParam.get("ID").toString();
		String strPrjDate = hmParam.get("PDT").toString();
		String histFl = hmParam.get("PRJCTDHISTFL").toString();
		StringBuffer strBufVal = new StringBuffer();
		//if ("".equals(strPrjDate))
		
		strBufVal.append("<input type=\"text\" maxlength=\"10\" name=\"prjDt" + strNo+"\" value=\""+strPrjDate+"\"");		
		strBufVal.append(" size='9' class=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strBufVal.append("/>&nbsp;<img id='Img_Date' style='cursor:hand'");	
		strBufVal.append("onclick=\"javascript:show_calendar('frmPdPrjMilestoneSetup.prjDt"+strNo+"');\" title='Click to open Calendar'");
		strBufVal.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
		strBufVal.append("<input type=\"hidden\" value=\""+strId+"\" name=\"projectedDate\" id=");
		strBufVal.append(strNo);
		strBufVal.append("> &nbsp; ");
		if(histFl.equals("Y"))
		{
			strBufVal.append("<img  id=\"imgEdit\" style=\"cursor:hand\" onclick=\"javascript:fnReqHistoryPrjDt("+strId+");\" title=\"Click to open required date history\"");
			strBufVal.append("src=\"/images/icon_History.gif\" align=\"bottom\"  height=\"18\" width=\"18\" />&nbsp; ");
		}		
		/*
		else
		{
			strBufVal.append(strPrjDate);
			strBufVal.append("<input type=\"hidden\" name=\"prjDt" + strNo+"\" value=\""+strPrjDate+"\"/>");
			strBufVal.append("<input type=\"hidden\" value=\""+strId+"\" name=\"projectedDate\" id=");
			strBufVal.append(strNo);
			strBufVal.append("> &nbsp; ");
		}*/	
		strRes = strBufVal.toString(); 
		return strRes;
	}
	public String getACTDATE()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strNo = hmParam.get("NO").toString();
		String strId = hmParam.get("ID").toString();
		String strActualDate = hmParam.get("ADT").toString();
		String histFl = hmParam.get("ACTHISTFL").toString();
		StringBuffer strBufVal = new StringBuffer();
		//if ("".equals(strActualDate))
		
		strBufVal.append("<input type=\"text\" maxlength=\"10\" name=\"actDt" + strNo+"\" value=\""+strActualDate+"\"");		
		strBufVal.append(" size='9' class=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strBufVal.append("/>&nbsp;<img id='Img_Date' style='cursor:hand'");	
		strBufVal.append("onclick=\"javascript:show_calendar('frmPdPrjMilestoneSetup.actDt"+strNo+"');\" title='Click to open Calendar'");
		strBufVal.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
		strBufVal.append("<input type=\"hidden\" value=\""+strId+"\" name=\"actualDate\" id=");
		strBufVal.append(strNo);
		strBufVal.append("> &nbsp; ");
		if(histFl.equals("Y"))
		{
			strBufVal.append("<img  id=\"imgEdit\" style=\"cursor:hand\" onclick=\"javascript:fnReqHistoryActDt("+strId+");\" title=\"Click to open required date history\"");
			strBufVal.append("src=\"/images/icon_History.gif\" align=\"bottom\"  height=\"18\" width=\"18\" />&nbsp; ");
		}
		
		/*
		else
		{
			strBufVal.append(strActualDate);
			strBufVal.append("<input type=\"hidden\" name=\"actDt" + strNo+"\" value=\""+strActualDate+"\"/>");
			strBufVal.append("<input type=\"hidden\" value=\""+strId+"\" name=\"projectedDate\" id=");
			strBufVal.append(strNo);
			strBufVal.append("> &nbsp; ");
		}*/	
		strRes = strBufVal.toString(); 
		return strRes;
	}

	public String getIMG()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strFlag = hmParam.get("LOG").toString();
		String strSeq = hmParam.get("ID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		if(strFlag.equals("N"))
        {
			strBufVal.append(strImagePath+"/phone_icon.jpg'");
        }
        else
        {            
            strBufVal.append(strImagePath+"/phone-icon_ans.gif'");
        }
		strBufVal.append(strImagePath+"/phone_icon.jpg'");
   		strBufVal.append(" title='Click to add comments' width='17' height='12'" ); 
	   	strBufVal.append(" onClick =\"fnOpenLog('"+strSeq+"','1243');\" </img>");
	   	strRes = strBufVal.toString(); 
	   	return strRes;
		
	}
}
