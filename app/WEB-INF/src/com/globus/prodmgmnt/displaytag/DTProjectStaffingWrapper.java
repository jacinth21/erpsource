/**
 * FileName    : DTProjectStaffingWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 12, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.displaytag;

import org.apache.commons.beanutils.DynaBean;

import com.globus.displaytag.beans.GmCommonDecorator;

/**
 * @author sthadeshwar
 *
 */
public class DTProjectStaffingWrapper extends GmCommonDecorator {

	public String getID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String partyId = String.valueOf(db.get("ID"));
		String projectId = String.valueOf(db.get("PROJECTID"));
		String partyType = String.valueOf(db.get("PARTYTYPE"));
		//projectId = projectId.replaceAll("-", "");
		String strValue = "<a href=javascript:fnViewPartySummary('"+partyId+"','"+partyType+"');><img title='Click here to view party profile' style='cursor:hand' src='/images/icon_info.gif'/></a>" +
		"&nbsp;&nbsp;<a href=javascript:fnViewProjects('"+partyId+"');><img title='Click here to view projects by this party' style='cursor:hand' src='/images/product_icon.jpg'/></a>";
		return strValue;
	}
	
	//fnCallPart
	public String getPROJECT()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String project = String.valueOf(db.get("PROJECT"));
		String projectId = String.valueOf(db.get("PROJECTID"));
		
		String strValue = "<a href=javascript:fnShowProjectSetup('"+projectId+"');>"+project+"</a>";
		return strValue;
	}	
}
