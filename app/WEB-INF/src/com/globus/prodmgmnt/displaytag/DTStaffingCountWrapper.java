/**
 * FileName    : DTStaffingCountWrapper.java 
 * Description :
 * Author      : rshah
 * Date        : Feb 10, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.displaytag;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.displaytag.beans.GmCommonDecorator;

/**
 * @author rshah
 *
 */
public class DTStaffingCountWrapper extends GmCommonDecorator{
	private HashMap hmParam = new HashMap();
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	Logger log = GmLogger.getInstance(this.getClass().getName());
	String strRes = "";
	private int intCount = 0;
	
	public String getMEMBERS()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("ID").toString();
		String strCnt = hmParam.get("MEMBERINFO").toString();
		StringBuffer strBufVal = new StringBuffer();
		
		strBufVal.append("<input type=\"text\" maxlength=\"4\" name=\"staffingCnt" +String.valueOf(intCount)+"\" value=\""+strCnt+"\"");
		strBufVal.append(" size='9' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strBufVal.append("> &nbsp; ");
		strBufVal.append("<input type=\"hidden\" value=\""+strId+"\" name=\"staffCount\" id=");
		strBufVal.append(String.valueOf(intCount));
		strBufVal.append("> &nbsp; ");
		
		intCount++;
        
		strRes = strBufVal.toString(); 
		return strRes;
	}
	public String getIMG()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strSeq = hmParam.get("ID").toString();
		String strFlag = hmParam.get("LOG").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		if(strFlag.equals("N"))
        {
			strBufVal.append(strImagePath+"/phone_icon.jpg'");
        }
        else
        {            
            strBufVal.append(strImagePath+"/phone-icon_ans.gif'");
        }
   		strBufVal.append(" title='Click to add comments' width='17' height='12'" ); 
	   	strBufVal.append(" onClick =\"fnOpenLog('"+strSeq+"','1244');\" </img>");
	   	strRes = strBufVal.toString(); 
	   	return strRes;
		
	}
	
	public String getID()  
	{
		
		hmParam = (HashMap)this.getCurrentRowObject();
		String strSeq = hmParam.get("ID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit Staffing Details' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =fnEdtId('"+strSeq+"'); </img> ");
   		strRes = strBufVal.toString(); 
		
		//return  "<a href= javascript:fnViewInv('" + strPO + "','" + strId + "')>" + "Y" + "</a>" ; ,'" +strSessNm+ "','" + 2 + "','" + 2 + "','" + 2 + "','" + 2 + "' ----  "','"+strSessNm+"','"+strSessStartDt+"','"+strSessEndDt+"','"+strSessOwner+"','"+strSessStatus+
 		return strRes;
	}
}
