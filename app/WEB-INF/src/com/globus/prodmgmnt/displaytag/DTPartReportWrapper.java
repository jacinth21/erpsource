package com.globus.prodmgmnt.displaytag;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;
import org.displaytag.decorator.TableDecorator;

public class DTPartReportWrapper extends TableDecorator{
	private HashMap hm = new HashMap();
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	private String strPartNumber= "";
	
	
	public String getPARTNUM()
	{
		hm = (HashMap)this.getCurrentRowObject();
		strPartNumber = GmCommonClass.parseNull((String)hm.get("PARTNUM"));
		String strCntrlFl = "<a href=\"javascript:fnPartDetails('"+strPartNumber+"')\";><img title='View Part Details' src="+strImagePath+"/packslip.gif border=0></a> "+strPartNumber;
		return strCntrlFl; 
	}
} 	