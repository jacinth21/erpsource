/*****************************************************************************
 * File			 : GmSetReportServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmSetReportServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strProdPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = strProdPath.concat("/GmSetMapReport.jsp");
		
		int i = 0;
		i = i +1;
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			instantiate(request, response);
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

			
				String strAction = request.getParameter("hAction");
				String strFromPage = request.getParameter("hFromPage");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				String strOpt = request.getParameter("hOpt");
				
				if (strOpt == null)
				{
					strOpt = (String)session.getAttribute("strSessOpt");
				}
				strOpt = (strOpt == null)?"":strOpt;
				GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
				
				// Adding code for Resource Bundle - to load Property files corresponding to SET / BOM
				java.util.Locale locale = new Locale("en","US",strOpt);
				GmResourceBundleBean resourceBundleBean = new GmResourceBundleBean("com.globus.properties.GmBrand",locale);
				request.setAttribute("brandBean", resourceBundleBean);
				// Code ends for Resource Bundle Configuration

				HashMap hmReturn = new HashMap();
				HashMap hmDropDownValues = new HashMap();
				HashMap hmValues;
				ArrayList alReturn = new ArrayList();
				ArrayList alMapReturn = new ArrayList();
				String strUserId = 	(String)session.getAttribute("strSessUserId");
				String strDeptId = (String)session.getAttribute("strSessDeptSeq");
				String strSetId = "";
				String strSetName = "";
				String strLoadData = GmCommonClass.parseNull(request.getParameter("hLoadData"));
				
				
				
                log.debug("strOpt : "+strOpt+ "strAction :"+strAction);
                
				if (strAction.equals("Load"))
				{
					hmValues = new HashMap();
					
					String strSearchSet = (String)request.getParameter("Txt_Setname");
					String strSystem = (String)request.getParameter("Cbo_System");
					String strHierarchy = (String)request.getParameter("Cbo_Hierarchy");
					String strCategory = (String)request.getParameter("Cbo_Category");
					String strType = (String)request.getParameter("Cbo_Type");
					String strStatus = (String)request.getParameter("Cbo_Status");
					strSetId = GmCommonClass.parseNull((String)request.getParameter("Txt_SetId")); 
					// PMT-42068
					String strProjectID = (String)request.getParameter("Cbo_Proj");
					String strProjName = (String)request.getParameter("searchCbo_Proj");
					
					
					hmValues.put("SETNAME",strSearchSet);
					hmValues.put("SYSTEM",strSystem);
					hmValues.put("HIERARCHY",strHierarchy);
					hmValues.put("CATEGORY",strCategory);
					hmValues.put("SETTYPE",strType);
					hmValues.put("SETSTATUS",strStatus);
					hmValues.put("SETID",strSetId);
					//Report Based On Project - PMT-42068
					hmValues.put("PROJECTID", strProjectID);
					hmValues.put("PROJECTNAME", strProjName);
					hmValues.put("LOADDATA", strLoadData);
					
					log.debug(" hmValues "+hmValues);
					// Avoid Report data while page load for SetReport
					if (strOpt.equals("SETRPT") && strLoadData.equals("Y"))
					{
						hmValues.put("TYPE", "1601");
						hmValues.put("DEPTID", strDeptId);
						alReturn = gmProj.loadSetMap(hmValues);
						request.setAttribute("strOpt",strOpt);
						
					}
					else if (strOpt.equals("GRPRPT"))
					{
						alReturn = gmProj.loadSetMap("1600");
					}
					// Avoid Report data while page load for BOMReport
					else if (strOpt.equals("BOMRPT") && strLoadData.equals("Y"))
					{
						 hmValues.put("TYPE", "1602");
						alReturn = gmProj.loadSetMap(hmValues);
						request.setAttribute("strOpt",strOpt);
					}
					request.setAttribute("strOpt",strOpt); 
					hmDropDownValues = gmProj.fetchSetMappingList();
					
					request.setAttribute("HMDROPDOWN",hmDropDownValues);
					//request.setAttribute("HMPARAM",hmValues);
					request.setAttribute("SETMASTERLIST",alReturn);
					request.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hAction",strAction);
					request.setAttribute("HMPARAM",hmValues);
				}
				else if (strAction.equals("Drilldown"))
				{
					strSetId = request.getParameter("hSetId")==null?"":request.getParameter("hSetId");
					strSetName = request.getParameter("hSetNm")==null?"":request.getParameter("hSetNm");
					String strHidePaging = GmCommonClass.parseNull(request.getParameter("hidePaging"));
					hmValues = new HashMap();
					hmValues.put("SETID",strSetId);
					hmValues.put("MODE","ALL");
					hmValues.put("DEPTID",strDeptId);
					hmValues.put("FROMPGOPT",strOpt);
					hmValues.put("COGSTYPE","4909");
					alMapReturn = gmProj.fetchSetMaping(hmValues);
					request.setAttribute("SETMAPPEDLIST",alMapReturn);
					request.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hSetId",strSetId);
					request.setAttribute("hSetNm",strSetName);
					request.setAttribute("hAction",strAction);
					request.setAttribute("hFromPage",strFromPage);
					request.setAttribute("hidePaging",strHidePaging);
					strDispatchTo = strProdPath.concat("/GmSetMapReport.jsp");
				}				
		
				dispatch(strDispatchTo,request,response);
				
		}// End of try
		catch (Exception e)
		{
			
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmSetReportServlet