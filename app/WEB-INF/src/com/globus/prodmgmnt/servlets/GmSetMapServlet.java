/*****************************************************************************
 * File : GmSetMapServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.beans.GmSetMapAutoCompleteRptBean;

public class GmSetMapServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strProdPath = GmCommonClass.getString("GMPRODMGMNT");
    String strDispatchTo = strProdPath.concat("/GmSetMapSetup.jsp");
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    int i = 0;
    i = i + 1;

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class

      HashMap hmAccess = new HashMap();
      String strAccessFlag = "";
      String strBOMButtonDisableFlag = "";


      String strAction = request.getParameter("hAction");

      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;


      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());

      // Adding code for Resource Bundle - to load Property files corresponding to SET / BOM
      java.util.Locale locale = new Locale("en", "US", strOpt);
      GmResourceBundleBean resourceBundleBean = new GmResourceBundleBean("GmBrand", locale);
      request.setAttribute("brandBean", resourceBundleBean);
      // Code ends for Resource Bundle Configuration

      HashMap hmReturn = new HashMap();
      HashMap hmSave = new HashMap();
      HashMap hmRuleValue = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alMapReturn = new ArrayList();
      ArrayList alUnitReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strInputString = "";
      String strSetId = "";
      String strSetName = "";
      String strType = "";
      String strComments = "";
      strType = strOpt.equals("SET") || strOpt.equals("SETMAP") ? "1601" : "1602";
      ArrayList alLogReason = new ArrayList();
      hmRuleValue.put("RULEID", strUserId);
      hmRuleValue.put("RULEGROUP", "SET_CRT_FLG");

      String strUserStatusFlag = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleValue));
      request.setAttribute("strUserStatusFlag", strUserStatusFlag);
      // To Get the add row access and set it in request
      hmAccess = gmAccessControlBean.getAccessPermissions(strUserId, "ADD_ROW_ACC");
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      request.setAttribute("ADDROWACCFL", strAccessFlag);

      // To Get the Critical Qty access and set it in request for updating the value.
      hmAccess = gmAccessControlBean.getAccessPermissions(strUserId, "SET_MAP_ACC");
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      request.setAttribute("ACCESSFL", strAccessFlag);
      
      // PMT-50777: BOM setup - mapping button disable flag
      strBOMButtonDisableFlag = GmCommonClass.getRuleValue(strType.concat("_SET_MAPPING_BUTTON"), "DISABLE");
      hmReturn.put("BOM_BUTTON_DISABLE_FL", strBOMButtonDisableFlag);

      if (strAction.equals("Go")) {

        strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_Set"));
        strSetName = GmCommonClass.parseNull(request.getParameter("searchCbo_Set"));
        alMapReturn = gmProj.fetchSetMaping(strSetId, "ALL");
        // hmReturn = gmProj.loadSetMaster(strType);
        hmReturn.put("SETMAPPEDLIST", alMapReturn);
        // loading CodeList from redis server
        alReturn = gmAutoCompleteReportBean.getCodeList("SETCT", null);
        hmReturn.put("SETCRITTYPE", alReturn);
        // loading CodeList from redis server
        alUnitReturn = gmAutoCompleteReportBean.getCodeList("UNTYP", null);
        hmReturn.put("SETUNITTYPE", alUnitReturn);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hSetId", strSetId);
        request.setAttribute("hSetName", strSetName);
        request.setAttribute("hAction", "Go");
      } else if (strAction.equals("Save")) {
        strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_Set"));
        strSetName = GmCommonClass.parseNull(request.getParameter("searchCbo_Set"));
        strInputString =
            request.getParameter("inputString") == null ? "" : request.getParameter("inputString");
        strComments = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        hmSave = gmProj.saveSetMap(strSetId, strInputString, strComments, strUserId);
        alMapReturn = gmProj.fetchSetMaping(strSetId, "ALL");
        // hmReturn = gmProj.loadSetMaster(strType);
        hmReturn.put("SETMAPPEDLIST", alMapReturn);
        // loading CodeList from redis server
        alReturn = gmAutoCompleteReportBean.getCodeList("SETCT", null);
        hmReturn.put("SETCRITTYPE", alReturn);
        // loading CodeList from redis server
        alUnitReturn = gmAutoCompleteReportBean.getCodeList("UNTYP", null);
        hmReturn.put("SETUNITTYPE", alUnitReturn);

        hmReturn.put("SETMAPPEDLIST", alMapReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hSetId", strSetId);
        request.setAttribute("hSetName", strSetName);
        request.setAttribute("hAction", "Go");
      }
      // Checks if the set ID is valid
      else if (strAction.equals("validateSet")) {
        strSetId = GmCommonClass.parseNull(request.getParameter("setId"));
        strType = GmCommonClass.parseNull(request.getParameter("strType"));
        strType = strType.equals("SET") || strType.equals("SETMAP") ? "1601" : "1602";
        strSetName = GmCommonClass.parseNull(request.getParameter("searchCbo_Set"));
        GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
            new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());
        HashMap hmData = gmSetMapAutoCompleteRptBean.validateSetName(strSetId, strType, strSetName);
        String setListName = GmCommonClass.parseNull((String) hmData.get("SETNAME"));
        response.setContentType("text/json");
        PrintWriter pw = response.getWriter();
        pw.write(setListName);
        pw.flush();
        pw.close();


      }
      alLogReason = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strSetId, "1249"));
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hmLog", alLogReason);
      dispatch(strDispatchTo, request, response);


    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSetMapServlet
