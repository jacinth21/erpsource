/*****************************************************************************
 * File			 : GmIdeaServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;
import com.globus.common.beans.*;
import com.globus.prodmgmnt.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmIdeaServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strProdPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = "";

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				GmCommonClass gmCommon = new GmCommonClass();
				GmProjectBean gmProject = new GmProjectBean();
				
				HashMap hmReturn = new HashMap();
				String strProjId = "";

				if (strAction.equals("Load"))
				{
					//hmReturn = gmProject.loadProject();
					//request.setAttribute("hmReturn",hmReturn);
				}
				else if (strAction.equals("Add") || strAction.equals("Edit"))
				{
				
					String strUserId = 	(String)session.getAttribute("strSessUserId");
					String strProjectNm = request.getParameter("Txt_ProjNm")== null ?"":request.getParameter("Txt_ProjNm");
					String strIdeaId = request.getParameter("Txt_IdeaId")== null ?"":request.getParameter("Txt_IdeaId");
					String strProjDesc	= request.getParameter("Txt_ProjDesc")== null ?"":request.getParameter("Txt_ProjDesc");
					String strDevClass = request.getParameter("hChkDevClass")== null ?"":request.getParameter("hChkDevClass");

					HashMap hmValues = new HashMap();
					hmValues.put("PNAME",strProjectNm);
					hmValues.put("IDEAID",strIdeaId);
					hmValues.put("PDESC",strProjDesc);
					hmValues.put("DEVCLASS",strDevClass);

					strProjId = (String)session.getAttribute("strSessProjectId")== null ?"0":(String)session.getAttribute("strSessProjectId"); 
					hmValues.put("PROJID",strProjId);

					HashMap hmSaveReturn = new HashMap();
					hmSaveReturn = gmProject.saveProject(hmValues,strUserId,strAction);
					
					hmReturn = gmProject.loadProject();
					request.setAttribute("hmReturn",hmReturn);

				}
				else if (strAction.equals("EditLoad"))
				{
					strProjId = (String)session.getAttribute("strSessProjectId");
					hmReturn = gmProject.loadEditProject(strProjId);
					request.setAttribute("hmReturn",hmReturn);
					session.removeAttribute("hAction");
				}
				dispatch(strProdPath.concat("/GmIdeaSetup.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmIdeaServlet