/*****************************************************************************
 * File			 : GmProdReportServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 

public class GmProdReportServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = strOperPath.concat("/GmProjectReport.jsp");
		String strAction = "";
        String strProjectStatus = "";
        String strGroupId = "";
        String strSegmentId = "";
        String strPartNum = "";
        String strLikeOption = "";
        String strConsignmentId = "";
        String strTechniqueId = "";
		ArrayList alReturn = new ArrayList();
        ArrayList alStatus = new ArrayList();
        ArrayList alGroup = new ArrayList();
        ArrayList alSegment = new ArrayList();
        ArrayList alProject = new ArrayList();
        ArrayList alLikeOptions = new ArrayList();
        ArrayList alTechList = new ArrayList();
        
		HashMap hmReturn = new HashMap();
        HashMap hmParam  = new HashMap();
        HashMap hmValues = new HashMap();
        HashMap hmTrans = new HashMap();
		String strProjId = "";
        String strPartDesc = "";
        String strSubCompFl = "";
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			instantiate(request, response);
			GmPartNumBean gmPart = new GmPartNumBean(getGmDataStoreVO());
		    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
            GmCommonClass gmCommon = new GmCommonClass();
            
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
            
            strAction = request.getParameter("hAction")== null ?"":request.getParameter("hAction");
            
            String strOpt = request.getParameter("hOpt");
            if (strOpt == null)
            {
                strOpt = (String)session.getAttribute("strSessOpt");
            }
            strOpt = (strOpt == null)?"":strOpt;
			
			if (strAction.equals("Part") || strOpt.equals("Part"))
			{
                hmParam.put("COLUMN","ID");
                
                alLikeOptions = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
                alProject = GmCommonClass.parseNullArrayList(gmProject.reportProject(hmParam));
                
                strProjId = request.getParameter("hId")== null ?"":request.getParameter("hId");
                if (strProjId.equals(""))
                {
                    strProjId = GmCommonClass.parseNull(request.getParameter("Cbo_Project"));
                }
				strPartNum = GmCommonClass.parseNull((String)request.getParameter("Txt_PartNum"));
                strLikeOption = GmCommonClass.parseNull((String)request.getParameter("Cbo_Search"));
                strPartDesc = GmCommonClass.parseNull((String)request.getParameter("Txt_PartDesc"));
                strConsignmentId = GmCommonClass.parseNull((String)request.getParameter("hConsignID"));
                strConsignmentId = GmCommonClass.parseNull((String)request.getParameter("hConsignID"));
                // PMT-42085: Part number report (added sub component filter)
                strSubCompFl = GmCommonClass.parseNull(request.getParameter("Chk_subcomp_fl"));
                
                
                // to get the Part number based on the CN (Consginment Reports)
                if(strPartNum.equals("") && !strConsignmentId.equals("")){
                	hmTrans.put("TRANSID",strConsignmentId);
                	strPartNum = GmCommonClass.parseNull((String)gmPart.loadTransactoinPartNumber(hmTrans));
                }
                hmValues.put("PROJECTID",strProjId);
                hmValues.put("PARTNUM",strPartNum);
                hmValues.put("LIKEOPTION",strLikeOption);
                hmValues.put("ACTION","ALL");
                hmValues.put("PARTDESC",strPartDesc);
                hmValues.put("SUBCOMPONENTFL",strSubCompFl);
                log.debug(" values inside hmap " + hmValues);
                
				hmReturn = GmCommonClass.parseNullHashMap(gmPart.loadEditPartNum(hmValues));
				request.setAttribute("Chk_subcomp_fl", strSubCompFl);
				request.setAttribute("alReturn",(ArrayList)hmReturn.get("PARTNUMDETAILS"));
                request.setAttribute("PROJECTLIST",alProject);
                request.setAttribute("LIKEOPTIONS",alLikeOptions);
                request.setAttribute("PARAM",hmValues);
				strDispatchTo = strOperPath.concat("/GmPartReport.jsp");				
			}
			
            //if (strOpt.equals("LoadProjectReport"))
            else
			{
				hmParam = new HashMap();
                String strColumn = request.getParameter("hColumn");
				strColumn = (strColumn == null)?"ID":strColumn;
                hmParam.put("COLUMN",strColumn);
                
                alStatus = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("PROJS"));
                alGroup = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("GROUP"));
                alSegment = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("SEGMT"));
                alTechList = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("TECHQ"));
                
                hmReturn.put("STATUS",alStatus);
                hmReturn.put("GROUP",alGroup);
                hmReturn.put("SEGMENT",alSegment);
                hmReturn.put("TECHNIQUE",alTechList);
                
                if (strAction.equals("ReportProject")) {
                    
                strProjectStatus  =  GmCommonClass.parseNull((String)request.getParameter("Cbo_Status"));
                strGroupId   =  GmCommonClass.parseNull((String)request.getParameter("Cbo_Group"));   
                strSegmentId =  GmCommonClass.parseNull((String)request.getParameter("Cbo_Segment"));
                strTechniqueId =  GmCommonClass.parseNull((String)request.getParameter("Cbo_Technique"));
                String strAttributeVal =  GmCommonClass.parseNull((String)request.getParameter("hAttributeVal"));
               
                 hmParam.put("STATUSID",strProjectStatus);
                 hmParam.put("GROUPID",strGroupId);
                 hmParam.put("SEGMENTID",strSegmentId);
                 hmParam.put("TECHNIQUE",strTechniqueId);
                 hmParam.put("FROM","ProjectReport");
                 hmParam.put("ATTRIBUTEVAL",strAttributeVal);

                }
                    
				alReturn = GmCommonClass.parseNullArrayList(gmProject.projectReport(hmParam));
				request.setAttribute("alReturn",alReturn);
                request.setAttribute("HMRETURN",hmReturn);
                request.setAttribute("HMPARAM",hmParam);
                
				strDispatchTo = strOperPath.concat("/GmProjectReport.jsp");
			}
			
			dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				e.printStackTrace();
                session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmProdReportServlet