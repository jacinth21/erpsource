/*****************************************************************************
 * File			 : GmPartNumEditServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;
import com.globus.common.beans.*;
import com.globus.operations.beans.*;
import com.globus.prodmgmnt.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

import java.util.*;

public class GmPartNumEditServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = "";

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				GmCommonClass gmCommon = new GmCommonClass();
				GmVendorBean gmVendor = new GmVendorBean();
				GmProjectBean gmProject = new GmProjectBean();
				GmPartNumBean gmPart = new GmPartNumBean();

				HashMap hmReturn = new HashMap();
				String strId = "";
				String strPartNum = "";
				ArrayList alReturn = new ArrayList();
				String strInputString = "";
				String strUserId = 	(String)session.getAttribute("strSessUserId");
				HashMap hmBean = new HashMap();
				String strControlReq = "";

				if (strAction.equals("Load"))
				{
                    HashMap hmTemp = new HashMap();
                    hmTemp.put("COLUMN","ID");
                    hmTemp.put("STATUSID","20301"); // Filter all approved projects

					alReturn = gmProject.reportProject(hmTemp);
					hmReturn.put("PROJECTS",alReturn);
					session.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hAction",strAction);
				}
				else if (strAction.equals("Part"))
				{
					strId = request.getParameter("Cbo_Proj")== null ?"":request.getParameter("Cbo_Proj");
					alReturn = gmProject.reportPartNumByProject(strId);
					hmReturn = (HashMap)session.getAttribute("hmReturn");
					hmReturn.put("PARTNUMS",alReturn);
					session.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hAction",strAction);
					request.setAttribute("hProjId",strId);
				}				
				else if (strAction.equals("Go"))
				{
					strId = request.getParameter("Cbo_Proj")== null ?"":request.getParameter("Cbo_Proj");
					strPartNum = request.getParameter("Cbo_Part")== null ?"":request.getParameter("Cbo_Part");
					hmBean = gmPart.loadEditPartNumTemp(strPartNum);
					hmReturn = (HashMap)session.getAttribute("hmReturn");
                    
                    if (hmReturn == null ){
                        HashMap hmTemp = new HashMap();
                        ArrayList alTemp = new ArrayList();
                        hmReturn = new HashMap();
                        hmTemp.put("COLUMN","ID");
                        hmTemp.put("STATUSID","20301"); // Filter all approved projects

                        alReturn = gmProject.reportProject(hmTemp);
                        hmReturn.put("PROJECTS",alReturn);
                        alTemp = gmProject.reportPartNumByProject(strId);
                        hmReturn.put("PARTNUMS",alTemp);
                    }
                    
					hmReturn.put("MATSPEC",(ArrayList)hmBean.get("MATSPEC"));
					hmReturn.put("DATALIST",(ArrayList)hmBean.get("DATALIST"));
                    
					session.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hAction",strAction);
					request.setAttribute("hProjId",strId);
					request.setAttribute("hPartNum",strPartNum);
				}
				else if (strAction.equals("Save"))
				{
					strId = request.getParameter("Cbo_Proj")== null ?"":request.getParameter("Cbo_Proj");
					strPartNum = request.getParameter("Cbo_Part")== null ?"":request.getParameter("Cbo_Part");
					strInputString = request.getParameter("inputString")== null ?"":request.getParameter("inputString");
					strControlReq = request.getParameter("Chk_Con")== null ?"":request.getParameter("Chk_Con");
					strControlReq = strControlReq.equals("on")?"1":"";
					
					hmBean = gmPart.saveEditPartNumTemp(strPartNum,strControlReq,strInputString,strUserId);

					hmBean = gmPart.loadEditPartNumTemp(strPartNum);
					hmReturn = (HashMap)session.getAttribute("hmReturn");
					hmReturn.put("MATSPEC",(ArrayList)hmBean.get("MATSPEC"));
					hmReturn.put("DATALIST",(ArrayList)hmBean.get("DATALIST"));

					session.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hAction","Go");
					request.setAttribute("hProjId",strId);
					request.setAttribute("hPartNum",strPartNum);
				}
				dispatch(strOperPath.concat("/GmPartNumEdit.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				e.printStackTrace();
                session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmPartNumEditServlet