/*****************************************************************************
 * File			 : GmPartReportServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;
import com.globus.common.beans.*;
import com.globus.common.db.GmDBManager;
import com.globus.prodmgmnt.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;

import oracle.jdbc.driver.OracleTypes;

import java.sql.ResultSet;
import java.util.*;

public class GmGenInvPdfServlet extends GmServlet
{
	String strInvoicePDF = "";
	
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		//String strOperPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = "";
		
		GmJasperReport gmJasperReport = null;
    	log.debug(" inside GmGeneratePDFAction ");
    	    	
    	//final String FILEPATH = "C:/com/invoices/";
    	final String FILEEXT = ".pdf";
    	final String SWISS = "SWISS_";
    	 
    	//log.debug(" alDataList"+alDataList);
       	
       	ArrayList alAllInvoices = new ArrayList();
       	HashMap hmEachInvoice = new HashMap();
       	GmDBManager gmDBManager = new GmDBManager();
    	ArrayList alInvOrderDetails = new ArrayList();
    	HashMap hmInvoiceDet = new HashMap();
    	String strHeadAddress = "";
    	String strRemitAddress = "";
    	
    	
    	
    	
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			
			
			alAllInvoices = fetchSwissInvoices();
			log.debug(" alAllInvoices:"+alAllInvoices);
			Iterator itInvoice = alAllInvoices.iterator();
			while(itInvoice.hasNext()){
				 hmEachInvoice = (HashMap)itInvoice.next();
				 String strInvoiceId = (String)hmEachInvoice.get("INVOICEID");
				 String strOrderId = (String)hmEachInvoice.get("ORDERID");
				 String strInvTypeId = (String)hmEachInvoice.get("INVTYPEID");
				 
					 gmDBManager.setPrepareString
						("gm_pkg_pd_staffing_trans.gm_fch_invoice_details", 4);
						gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
						gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
						gmDBManager.setString(1, strInvoiceId);
						gmDBManager.setString(2, strOrderId);
						gmDBManager.execute();
						
						hmInvoiceDet = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(3));
						hmInvoiceDet.put("ADDWATERMARK","Commercial Invoice");
						
						String straddress = GmCommonClass.getString("GMCOMPANYADDRESS");
						String strGMName = GmCommonClass.getString("GMCOMPANYNAME");
						//straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");

						String strRremitaddr = GmCommonClass.getString("GMREMIT");
						String strRemitName = GmCommonClass.getString("GMREMITHEAD");
						//strRremitaddr = "&nbsp;" + strRremitaddr.replaceAll("/", "\n&nbsp;&nbsp;<br>&nbsp;");
						strInvoicePDF = GmCommonClass.getString("INVOICEPDF");
						
						strHeadAddress =  strGMName + "/" + straddress;
						strRemitAddress = strRemitName + "/" + strRremitaddr;
						
						hmInvoiceDet.put("REMITADDRESS",strRemitAddress);
						hmInvoiceDet.put("GMADDRESS", strHeadAddress);
						hmInvoiceDet.put("ORDERID",strOrderId);
						hmInvoiceDet.put("VARIABLECURRENCY","$");
						alInvOrderDetails = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
						log.debug(" hmInvoiceDet:"+hmInvoiceDet);
						log.debug(" alInvOrderDetails:"+alInvOrderDetails);
						gmDBManager.close();
						
						String fileName = strInvoicePDF + SWISS+strInvoiceId+FILEEXT;
						File newFile = new File(strInvoicePDF );
						if(!newFile.exists()){
						newFile.mkdir();
						}
						//log.debug(" fileName:"+fileName);
						gmJasperReport = new GmJasperReport();								
						gmJasperReport.setRequest(request);
						gmJasperReport.setResponse(response);
						gmJasperReport.setJasperReportName("/GmCommercialInvoice.jasper");
						gmJasperReport.setHmReportParameters(hmInvoiceDet);
						gmJasperReport.setReportDataList(alInvOrderDetails);				
						gmJasperReport.exportJasperReportToPdf(fileName);
				
			}
			//Others
			printOtherInvoices(request,response);
		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
  	
  	public ArrayList fetchSwissInvoices ()throws AppError
    {
        ArrayList alInvoiceList = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_pd_staffing_trans.gm_fch_invoices_swiss",1);
        gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
        
        gmDBManager.execute();
        alInvoiceList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
        gmDBManager.close();
        
        return alInvoiceList;
    }
    
  	public ArrayList fetchOtherInvoices ()throws AppError
    {
        ArrayList alInvoiceList = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_pd_staffing_trans.gm_fch_invoices_others",1);
        gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
        
        gmDBManager.execute();
        alInvoiceList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
        gmDBManager.close();
        
        return alInvoiceList;
    }
  	
  	public void printOtherInvoices (HttpServletRequest request, HttpServletResponse response)throws AppError,IOException, ServletException
    {
  		ArrayList alOtherInvoices = new ArrayList();
    	HashMap hmOtherInvoice = new HashMap();
    	
  		alOtherInvoices= fetchOtherInvoices();
  		log.debug("alOtherInvoices======================"+alOtherInvoices);
		log.debug(" alOtherInvoices:"+alOtherInvoices);
		Iterator itOtherInv = alOtherInvoices.iterator();
		String dispatch ="/GmInvoiceServlet";
		while(itOtherInv.hasNext()){
			 hmOtherInvoice = (HashMap)itOtherInv.next();
			 String strOtherInvoiceId = (String)hmOtherInvoice.get("INVOICEID");
			 String strAccType = (String)hmOtherInvoice.get("ACCTYPE");
			 strAccType= strAccType.replaceAll(" ", "");
			 
			 request.setAttribute("hAction", "Print");
			 request.setAttribute("hInv",strOtherInvoiceId );
			 request.setAttribute("printPDF","pdfprint" );
			 request.setAttribute("filePath",strInvoicePDF );
			 request.setAttribute("invoiceType",strAccType );
			dispatch(dispatch,request, response);
		}
       
        
    }
  	
  	

  	
}// End of GmPartReportServlet