/*****************************************************************************
 * File          : GmPartNumMapServlet
 * Desc          : For Sub assembly part mapping
 * Version       : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPartNumMapServlet extends GmServlet
{

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
        HttpSession session = request.getSession(false);
        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strOperPath = GmCommonClass.getString("GMPRODMGMNT");
        String strDispatchTo = "";

        try
        {
            checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
            
            GmCommonClass gmCommon = new GmCommonClass();
            GmProjectBean gmProject = new GmProjectBean();
            GmPartNumBean gmPartNumBean = new GmPartNumBean();

            HashMap hmReturn = new HashMap();
            HashMap hmTemp = new HashMap();
            HashMap hmParam = new HashMap(); 
            
            HashMap hmFetchResultSet = new HashMap();
            HashMap hmPPartDetails = new HashMap();
            
            String strId = "";
            String strPartNum = "";
            ArrayList alReturn = new ArrayList();
            ArrayList alPartMapList = new ArrayList();
            ArrayList alPartType = new ArrayList();
            String strType = "";
            String strSubAdmPart = "";
            String strInputString = "";
            String strHSubAdmPart = "";
            String strSubPart = "";
            String strPPartStatus = "";
            String strDisabled = "";
            String strUserId =  (String)session.getAttribute("strSessUserId");
            String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
            HashMap hmBean = new HashMap();

                String strAction = request.getParameter("hAction");
                if (strAction == null)
                {
                    strAction = (String)session.getAttribute("hAction");
                }
                strAction = (strAction == null)?"Load":strAction;
                String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
                
                log.debug(" Action is " + strAction + " Opt ios " + strOpt);
                
                
                if (strOpt.equals("LoadPartFromProject"))
                {
                    strId = GmCommonClass.parseNull(request.getParameter("Cbo_Proj"));
                    alReturn = gmProject.reportPartNumByProject(strId);
                    log.debug(" # of parts available for proj " + strId +"  is " + alReturn.size());
                    hmReturn.put("PARTNUMS",alReturn);
                    request.setAttribute("hProjId",strId);
                }
                
                if (strAction.equals("LoadPartDetails") || strAction.equals("SavePartDetails")|| strAction.equals("AddPartDetails"))
                {
                    strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
                    if (strPartNum.equals(""))
                    {
                        strPartNum = request.getParameter("Cbo_Part") == "0" ? "":request.getParameter("Cbo_Part");
                    }
                    strSubAdmPart = GmCommonClass.parseNull(request.getParameter("Txt_SubAsmPart"));
                    strHSubAdmPart = GmCommonClass.parseNull(request.getParameter("hSubAsmPart"));
                    if (strAction.equals("LoadPartDetails"))
                    {
                        strSubAdmPart = "";
                        strHSubAdmPart = "";
                    }
                    
                    if (!strSubAdmPart.equals("") && !strHSubAdmPart.equals("")){
                        strSubPart = strSubAdmPart.concat(",").concat(strHSubAdmPart);
                    }
                    else if(!strSubAdmPart.equals(""))
                    {
                        strSubPart = strSubAdmPart;
                    }
                    else if (!strHSubAdmPart.equals(""))
                    {
                        strSubPart = strHSubAdmPart;
                    }
                    
                    strType = GmCommonClass.parseNull(request.getParameter("hType"));
                    
                    hmParam.put("PPNUM",strPartNum);
                    hmParam.put("SCPNUM",strSubAdmPart);
                    hmParam.put("SUBPART",strSubPart);
                    hmParam.put("TYPE",strType);
                    hmParam.put("USERID",strUserId);
                                     
                    if (strAction.equals("SavePartDetails")){
                        strInputString = GmCommonClass.parseNull(request.getParameter("hInputString"));
                        hmParam.put("INPUTSTRING",strInputString);
                        log.debug(" HmParam values to save part details " + hmParam);
                        gmPartNumBean.savePartNumMapping(hmParam);
                        hmParam.put("SUBPART","");
                    }
                        hmFetchResultSet = gmPartNumBean.loadPartNumMapping(hmParam);
                        alPartMapList = (ArrayList)hmFetchResultSet.get("PARTDETAILS");
                        hmPPartDetails = (HashMap)hmFetchResultSet.get("PPARTDETAILS");
                        strPPartStatus = GmCommonClass.parseNull((String)hmPPartDetails.get("PSTATUSID"));
                        log.debug(" strPPartStatus " + strPPartStatus);
                        if (!(strPPartStatus.equals("20365") || strPPartStatus.equals("20366")))
                        {
                            strDisabled = "disabled";
                        }

                        if (strDeptId.equals("Q"))
                        {
                            strDisabled = "Enable";
                        }
                        alPartType = GmCommonClass.getCodeList("PTMAP");

                        hmReturn.put("PARTTYPE",alPartType);
                        hmReturn.put("DATALIST",alPartMapList);
                        hmReturn.put("PPARTDETAILS",hmPPartDetails);
                        hmReturn.put("PSTATUSID",strPPartStatus);
                        hmReturn.put("PDISABLED",strDisabled);
                        log.debug(" Disabled " +strDisabled);
                        request.setAttribute("PARAM",hmParam);
                        
                        strAction = "LoadPartDetails";
                }
                
                hmTemp.put("COLUMN","ID");
                hmTemp.put("STATUSID","20301"); // Filter all approved projects
                
                alReturn = gmProject.reportProject(hmTemp);
                hmReturn.put("PROJECTS",alReturn);
                request.setAttribute("hmReturn",hmReturn);
                request.setAttribute("hAction",strAction);


                dispatch(strOperPath.concat("/GmPartNumMap.jsp"),request,response);

        }// End of try
        catch (Exception e)
        {
                e.printStackTrace();
                session.setAttribute("hAppErrorMessage",e.getMessage());
                strDispatchTo = strComnPath + "/GmError.jsp";
                gotoPage(strDispatchTo,request,response);
        }// End of catch
    }//End of service method
}// End of GmPartNumEditServlet