/*****************************************************************************
 * File			 : GmPartReportServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;
import com.globus.common.beans.*;
import com.globus.prodmgmnt.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmPartReportServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = "";

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				String strColumn = request.getParameter("hColumn");
				strColumn = (strColumn == null)?"ID":strColumn;

				//GmPartNumBean gmPart = new GmPartNumBean();
				
				ArrayList alReturn = new ArrayList();

				///alReturn = gmPart.reportPartNumPricing(strColumn);
				request.setAttribute("alReturn",alReturn);

				dispatch(strOperPath.concat("/GmPartPriceReport.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmPartReportServlet