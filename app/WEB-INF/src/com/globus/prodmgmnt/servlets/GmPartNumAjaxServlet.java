/*****************************************************************************
 * File			 : GmPartNumAjaxServlet
 * Desc		 	 : This servlet is used to check the availability of part number to show the image
 * 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmPartNumBean;

/**
 * @author arajan
 *
 */
public class GmPartNumAjaxServlet extends GmServlet {

	private ServletContext context;
	
	String strComnPath = GmCommonClass.getString("GMCOMMON");

	/**
	 * 
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.context = config.getServletContext();
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			GmPartNumBean gmPartNumBean = new GmPartNumBean();
			GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
			HashMap hmParam = new HashMap();
			ArrayList alResult = new ArrayList();
			String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
			String strPartNumber = GmCommonClass.parseNull(request.getParameter("partNum"));
			String strAttrType = "";
			String strAttrValue = "";
			String strXMLString = "";
			if(strOpt.equals("CHECKPARTNUM")) {
				strXMLString = GmCommonClass.parseNull((String) gmPartNumBean.validatePartNumber(strPartNumber));
			}else if(strOpt.equals("PARTSBYATTR")) {
				strAttrType = GmCommonClass.parseNull(request.getParameter("attrType"));
				strAttrValue = GmCommonClass.parseNull(request.getParameter("attrValue"));
				alResult= gmPartNumBean.fetchPartListByAttribute(strAttrType, strAttrValue);
				strXMLString = gmXMLParserUtility.createXMLString(alResult);

			}
			 response.setContentType("text/xml;charset=utf-8");
			 response.setCharacterEncoding("UTF-8");
	         response.setHeader("Cache-Control", "no-cache");
			 PrintWriter pw = response.getWriter();
			 if(!strXMLString.equals("")) {
				pw.write(strXMLString);
			  }
			 pw.flush();
			 pw.close();
		}
		catch (Exception e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			String strDispatchTo = strComnPath + "/GmError.jsp";
			gotoPage(strDispatchTo, request, response);
		}
		
	}

}
