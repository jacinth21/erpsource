/*****************************************************************************
 * File			 : GmPartReportYTD.jsp
 * Desc		 	 : This Servlet will be used for all Part Sales YTD Report
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;

import com.globus.common.beans.*;
import com.globus.prodmgmnt.beans.GmProdReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class GmPartSalesYTDReportServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strProdPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = "";
		String strJSPName = "";
		String strDistributorID = "";
		String strFromMonth = "";
		String strFromYear = "";
		String strToMonth = "";
		String strToYear = "";
		String strProject = "";
		String strType = "";
		
		String strShowArrowFl = "";
		String strShowPercentFl = "";
		String strHideAmount = "";
		String strHeader = "";
		
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			String strAction = request.getParameter("hAction");
			
			GmCommonClass gmCommon = new GmCommonClass();
			GmProdReportBean gmProd = new GmProdReportBean();
			GmProjectBean gmProject = new GmProjectBean();
			GmCommonBean gmCommonBean = new GmCommonBean();
			
			ArrayList alReturn = new ArrayList();	// Value used to fill the combo box value 
			HashMap hmReturn = new HashMap();
			HashMap hmComonValue = new HashMap();
			HashMap hmFromDate = new HashMap();
			ArrayList alMonthDropDown = new ArrayList();
			ArrayList alYearDropDown = new ArrayList();

            HashMap hmTemp = new HashMap();
            hmTemp.put("COLUMN","ID");
            hmTemp.put("STATUSID","20302"); // Filter all launched projects

			
			String strCondition = null;
			
			String strId = "";
			ArrayList alDistList = new ArrayList();
			
			
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("strSessOpt");
			}
			
			strFromMonth = request.getParameter("Cbo_FromMonth");
			strFromYear = request.getParameter("Cbo_FromYear");
			strToMonth = request.getParameter("Cbo_ToMonth");
			strToYear = request.getParameter("Cbo_ToYear");
			strShowArrowFl =  request.getParameter("Chk_ShowArrowFl");
			strShowPercentFl = request.getParameter("Chk_ShowPercentFl"); 
			strHideAmount = request.getParameter("Chk_HideAmountFl");
			strProject = request.getParameter("Cbo_Proj");
			strType = request.getParameter("Cbo_Type");
			
			strType = (strType == null)?"0":strType;
			
			strAction = (strAction == null)?"LoadPart":strAction;
			
			//To get the Access Level information 
			strCondition =  getAccessCondition(request,response);
			
			alReturn = gmProject.reportProject(hmTemp);
			hmComonValue.put("PROJECTS",alReturn);
			
			request.setAttribute("hmComonValue",hmComonValue);
			strHeader = "Sales By Part By Month";
			hmReturn = gmProd.reportYTDByPart(strProject,strType,strFromMonth,strFromYear,
						strToMonth, strToYear, strCondition);
			
			// To fetch the value for the unload condition 
			hmFromDate = (HashMap)hmReturn.get("FromDate");
			if (hmFromDate.get("FirstTime") != null)
			{
				strFromMonth = (String)hmFromDate.get("FROMMONTH");
				strFromYear  = (String)hmFromDate.get("FROMYEAR");
				strToMonth 	 = (String)hmFromDate.get("TOMONTH");
				strToYear 	 = (String)hmFromDate.get("TOYEAR");
			}
			alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
			alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

			hmReturn.put("ALMONTHDROPDOWN",alMonthDropDown );
			hmReturn.put("ALYEARDROPDOWN",alYearDropDown );

			request.setAttribute("hAction",strAction);
			request.setAttribute("strHeader" ,strHeader);
			request.setAttribute("Cbo_FromMonth",strFromMonth);
			request.setAttribute("Cbo_FromYear",strFromYear );
			request.setAttribute("Cbo_ToMonth",strToMonth );
			request.setAttribute("Cbo_ToYear",strToYear );
			request.setAttribute("hmReturn",hmReturn);
			request.setAttribute("Chk_ShowArrowFl", strShowArrowFl);
			request.setAttribute("Chk_ShowPercentFl",strShowPercentFl);
			request.setAttribute("Chk_HideAmountFl",strHideAmount);
			request.setAttribute("Cbo_Proj",strProject);
			request.setAttribute("Cbo_Type",strType);

			strJSPName= "/GmPartSalesReportYTD.jsp";
			dispatch(strProdPath.concat(strJSPName),request,response);
		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of 