/*****************************************************************************
 * File : GmSetMasterServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.servlets.GmUploadServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.beans.GmSetMapAutoCompleteRptBean;


public class GmSetMasterServlet extends GmUploadServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMPRODMGMNT");
    String strWhiteList = GmCommonClass.getString("GMLNINSHEETWHITELIST");
    String strDispatchTo = strOperPath.concat("/GmSetMasterSetup.jsp");
    MultipartFormDataRequest mrequest = null;
    String strAction = "";
    String strOpt = "";
    try {

      // The method is called to get the company related information.
      if (MultipartFormDataRequest.isMultipartFormData(request)) {
        // creating MultiPart request
        mrequest = new MultipartFormDataRequest(request);
        instantiate(request, response, mrequest);// The method is called to get the company related
                                                 // information.
        strAction = mrequest.getParameter("hAction");
        strOpt = mrequest.getParameter("hOpt");
      } else {
        strAction = request.getParameter("hAction");
        strOpt = request.getParameter("hOpt");
        instantiate(request, response);// The method is called to get the company related
                                       // information.
      }

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;
      log.debug("strAction>>>>>>>>>" + strAction);

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmCommonClass gmCommon = new GmCommonClass();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
      GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
      GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
          new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());

      // Adding code for Resource Bundle - to load Property files corresponding to SET / BOM
      // log.debug(" value of strOpt is " + strOpt);
      java.util.Locale locale = new Locale("en", "US", strOpt);
      GmResourceBundleBean resourceBundleBean = new GmResourceBundleBean("GmBrand", locale);
      request.setAttribute("ResourceBundleBean", resourceBundleBean);
      // Code ends for Resource Bundle Configuration
      request.setAttribute("hOpt", strOpt);
      HashMap hmReturn = new HashMap();
      HashMap hmUpdateHistory = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmJmsParam = new HashMap();
      HashMap hmMap = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strApplnDateFmt = (String) session.getAttribute("strSessApplDateFmt");
      String strProjId = "";
      String strSetId = "";
      String strSetName = "";
      String strSetDesc = "";
      String strSetCat = "";
      String strSetType = "";
      String strType = "";
      String strMode = "";
      String strhSetId = "";
      String strRevLevel = "";
      String strStatus = "";
      String strFileSts = "";
      String strFileName = "";
      String strUploadHome = "";
      String strReportFl = "";
      String strGridData = "";
      String strScreenType = "";
      String strSetDetailDesc = "";
      String strForword = "";
      String strDivisionID = "";
      String strOldSetNm = "";
      String strOldInspSheet = "";
      String strSearchCboInspSheet = "";
      String strLotTrackfl = "";//PMT-42618
      strForword = GmCommonClass.parseNull(request.getParameter("hForward"));
      strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_SetId"));
      strOpt = strOpt.equals("SET") || strOpt.equals("SETMAP") ? "1601" : "1602";
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      log.debug(" Action is " + strAction + " Opt is " + strOpt);
      // When strAction is save, we need to fetch the set List after Saving the New Set.
      // So this condition added here
      if (!strAction.equals("Save")) {
        hmReturn = gmProj.loadSetMaster(strOpt);
        if (strAction.equals("SETFILELIST")) {
          alReturn = gmProj.loadSetMap(strOpt);
          hmReturn.put("SETMASTERLIST", alReturn);
        }
      }

      if (strAction.equals("Reload")) {

        strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_SetId"));
        // strSetId =
        // mrequest.getParameter("Cbo_SetId")==null?"":mrequest.getParameter("Cbo_SetId");
        log.debug("The data comming from the databsace fo the Set **** " + strSetId);
        hmResult = gmProj.loadEditSetMaster(strSetId);
        ArrayList alList = gmCommonUploadBean.fetchUploadInfo("91184", strSetId);
        if (alList.size() > 0) {
          hmMap = (HashMap) alList.get(0);
          strFileName = (String) hmMap.get("NAME");
        }
        hmResult.put("FILENAME", strFileName);
        hmReturn.put("SETMASTERDETAILS", hmResult);

        // Including code for adding Updation History
        hmUpdateHistory = gmCommonBean.getUpdateHistory("T207_SET_MASTER", "C207_SET_ID", strSetId);
        request.setAttribute("UPDHISTORY", hmUpdateHistory);
      } else if (strAction.equals("Save")) {
        strProjId = GmCommonClass.parseNull(mrequest.getParameter("Cbo_Proj"));
        strSetId = GmCommonClass.parseNull(mrequest.getParameter("Txt_SetId"));
        strSetName = GmCommonClass.parseNull(mrequest.getParameter("Txt_SetNm"));
        strSetDesc = GmCommonClass.parseNull(mrequest.getParameter("Txt_SetDesc"));
        strSetDetailDesc = GmCommonClass.parseNull(mrequest.getParameter("Txt_SetDetailDesc"));
        strSetCat = GmCommonClass.parseNull(mrequest.getParameter("Cbo_Cat"));
        strSetType = GmCommonClass.parseNull(mrequest.getParameter("Cbo_Type"));
        strType = GmCommonClass.parseNull(mrequest.getParameter("hType"));
        strMode = GmCommonClass.parseNull(mrequest.getParameter("hMode"));
        strhSetId = GmCommonClass.parseNull(mrequest.getParameter("hSetId"));
        strSetId = strMode.equals("Edit") ? strhSetId : strSetId;
        strStatus = GmCommonClass.parseNull(mrequest.getParameter("Cbo_SetStatus"));
        strRevLevel = GmCommonClass.parseNull(mrequest.getParameter("Txt_RevLvl"));
        String strLogReason = GmCommonClass.parseNull(mrequest.getParameter("Txt_LogReason"));
        String strCoreSetFl = GmCommonClass.parseNull(mrequest.getParameter("coreSetFl"));
        strFileSts = GmCommonClass.parseNull(mrequest.getParameter("h_uploadfile"));
        strFileName = GmCommonClass.parseNull(mrequest.getParameter("h_filename"));
        strDivisionID = GmCommonClass.parseNull(mrequest.getParameter("Cbo_Division"));
        strOldSetNm = GmCommonClass.parseNull(mrequest.getParameter("hOld_SetId"));
        strOldInspSheet = GmCommonClass.parseNull(mrequest.getParameter("hOld_InspSheet"));
        strSearchCboInspSheet =
            GmCommonClass.parseNull(mrequest.getParameter("searchcbo_inspection_sheets"));
        strLotTrackfl = GmCommonClass.parseNull(mrequest.getParameter("Chk_Lottrack"));//PMT-42618
        // mrequest default encoding is iso-8859-1 - so we are changing to UTF-8
        strSetName = GmCommonClass.encodeToUTF8(strSetName);
        strSetDesc = GmCommonClass.encodeToUTF8(strSetDesc);

        if (strFileSts.equals("true")) {
          strUploadHome = GmCommonClass.getString("GMLNINSHEET") + "\\\\";
          setStrUploadHome(strUploadHome);
          strFileName = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
          strFileName = strFileName.substring(0, strFileName.lastIndexOf("."));
          setStrFileName(strFileName);
          setStrWhitelist(strWhiteList);
          // checking folder having write access
          try {
            File chkFile = new File(strUploadHome + "File.txt");
            if (chkFile.createNewFile()) {
              chkFile.delete();
            }
          } catch (IOException ioe) {
            session.setAttribute("hType", "A");
            throw new AppError("Cannot upload file as the directory does not have write access",
                "", 'E');
          }
          // Upload the file on server
          super.uploadFile(mrequest, request);
          // To sync the Inspection sheet list in the redis cache
          if (!strOldInspSheet.equals(strFileName)) {
            syncCacheData(strSetId, strOldInspSheet, strFileName, "InspectSheet", strOpt,
                GmCommonClass.parseNull(mrequest.getParameter("companyInfo")));
          }
        }

        hmParam.put("PROJID", strProjId);
        hmParam.put("SETID", strSetId);
        hmParam.put("SETNM", strSetName);
        hmParam.put("SETDESC", strSetDesc);
        hmParam.put("SETDETAILDESC", strSetDetailDesc);
        hmParam.put("SETCAT", strSetCat);
        hmParam.put("SETTYPE", strSetType);
        hmParam.put("TYPE", strType);
        hmParam.put("USERID", strUserId);
        hmParam.put("MODE", strMode);
        hmParam.put("STATUS", strStatus);
        hmParam.put("REVLVL", strRevLevel);
        hmParam.put("LOG", strLogReason);
        hmParam.put("CORESETFL", strCoreSetFl);
        hmParam.put("FILENAME", strFileName);
        hmParam.put("DIVISIONID", strDivisionID);
        hmParam.put("SEARCHCBOINSPSHEET", strSearchCboInspSheet);
        hmParam.put("LOTTRACKFL",strLotTrackfl);//PMT-42618
        gmProj.saveSetMaster(hmParam);

        // JMS Call to update the cache if new record is added or the Set/BOM name is changed
        if (!strOldSetNm.equals(strSetName)) {
          syncCacheData(strSetId, strOldSetNm, strSetName, "SetBomSetup", strOpt,
              GmCommonClass.parseNull(mrequest.getParameter("companyInfo")));
        }

     // JMS Call to update the cache if new record is added or the Set name is changed for PMT-37080
        if (strSetType.equals("4074") && (!strOldSetNm.equals(strSetName) || !strStatus.equals("20369"))){ //4074-Loaner 20369-Obsolete
          syncCacheData(strSetId, strOldSetNm, strSetName, "LoanerSetList", strOpt,
              GmCommonClass.parseNull(mrequest.getParameter("companyInfo")));
        }
        
        hmResult = gmProj.loadEditSetMaster(strSetId);
        hmResult.put("FILENAME", strFileName);
        hmReturn = gmProj.loadSetMaster(strOpt);
        hmReturn.put("SETMASTERDETAILS", hmResult);
        // Including code for adding Updation History
        hmUpdateHistory = gmCommonBean.getUpdateHistory("T207_SET_MASTER", "C207_SET_ID", strSetId);
        request.setAttribute("UPDHISTORY", hmUpdateHistory);
        strAction = "Reload";
      } else if (strAction.equals("validateSet")) {
        // Checks if the set ID/BOM Id is valid
        strSetId = GmCommonClass.parseNull(request.getParameter("setId"));
        strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));

        HashMap hmData = gmSetMapAutoCompleteRptBean.validateSetName(strSetId, strOpt, strSetName);
        String setListName = GmCommonClass.parseNull((String) hmData.get("SETNAME"));
        response.setContentType("text/json");

        PrintWriter pw = response.getWriter();
        pw.write(setListName);
        pw.flush();
        pw.close();

      }
      /*
       * The Following block of code is written by dreddy for Set Inspection Sheet Mapping Report as
       * part of the IST Project
       */
      if (strAction.equals("SETFILELIST")) {

        strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_SetId"));
        strReportFl = GmCommonClass.parseNull(request.getParameter("hReport"));
        strScreenType = GmCommonClass.parseNull(request.getParameter("hScreenType"));
        if (strReportFl.equals("Y")) {
          ArrayList alList = gmCommonUploadBean.fetchUploadInfo("91184", strSetId);
          hmParam.put("ALRESULT", alList);
          hmParam.put("APPLNDATEFMT", strApplnDateFmt);
          hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
          strGridData = generateOutPut(hmParam);
          request.setAttribute("GRIDATA", strGridData);
        }
        request.setAttribute("SETID", strSetId);
        request.setAttribute("SCREENTYPE", strScreenType);
        strDispatchTo = strOperPath.concat("/GmSetFileListRpt.jsp");
      } else {
        // ArrayList alList = gmCommonUploadBean.fetchUploadInfo("91184", null);
        // hmReturn.put("SETFILELIST", alList);

        // displaying comments
        alReturn = gmCommonBean.getLog(strSetId, "1219");
        request.setAttribute("hmLog", alReturn);
      }
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      request.setAttribute("SETID", strSetId);

      if (strForword.equals("FETCHCONTAINER")) {
        strDispatchTo = strOperPath.concat("/GmSetMasterSetupContainer.jsp");
      } else if (strAction.equals("SETFILELIST")) {
        strDispatchTo = strOperPath.concat("/GmSetFileListRpt.jsp");
      }
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      strAction = "Load";
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private String generateOutPut(HashMap hmParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmSetFileListRpt", strSessCompanyLocale));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("prodmgmnt/templates");
    templateUtil.setTemplateName("GmSetFileListRpt.vm");
    return templateUtil.generateOutput();
  }

  // To sync the modified/ new value with the redis cache
  public void syncCacheData(String strId, String strOldNm, String strNewNm, String strMethod,
      String strType, String strCompanyInfo) throws Exception {
    GmAutoCompleteTransBean gmAutoCompleteTransBean =
        new GmAutoCompleteTransBean(getGmDataStoreVO());
    HashMap hmJmsParam = new HashMap();
    hmJmsParam.put("ID", strId);
    hmJmsParam.put("OLD_NM", strOldNm);
    hmJmsParam.put("NEW_NM", strNewNm);
    hmJmsParam.put("METHOD", strMethod);
    hmJmsParam.put("TYPE", strType);
    hmJmsParam.put("companyInfo", strCompanyInfo);
    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
  }

}// End of GmSetMasterServlet
