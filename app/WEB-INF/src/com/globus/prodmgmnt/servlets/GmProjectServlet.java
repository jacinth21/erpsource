/*****************************************************************************
 * File			 : GmProjectServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;
import com.globus.common.beans.*;
import com.globus.prodmgmnt.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;
 
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 


public class GmProjectServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strProdPath = GmCommonClass.getString("GMPRODMGMNT");
		String strDispatchTo = "";
        
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

			String strProjIde = (String)session.getAttribute("strSessProjectId")== null ?"0":(String)session.getAttribute("strSessProjectId");
            String strUserId =  GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
			log.debug("Project ID calues is -------------- " + strProjIde);
			
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				GmCommonClass gmCommon = new GmCommonClass();
				GmProjectBean gmProject = new GmProjectBean();
				
				HashMap hmReturn = new HashMap();
				String strProjId = "";
                String strMessage = "";
                 
                boolean blApprove = false;

				if (strAction.equals("Load"))
				{
					hmReturn = gmProject.loadProject();
                    log.debug(" values in load " + hmReturn);
					request.setAttribute("hmReturn",hmReturn);
				}
				else if (strAction.equals("Add") || strAction.equals("Edit"))
				{
                    String strStatus = GmCommonClass.parseNull(request.getParameter("hStatus"));
                    strStatus = strStatus.equals("0")? "20300":strStatus;
					String strProjectNm = GmCommonClass.parseNull(request.getParameter("Txt_ProjNm"));
					//String strIdeaId = request.getParameter("Txt_IdeaId")== null ?"":request.getParameter("Txt_IdeaId");
					String strProjDesc	= GmCommonClass.parseNull(request.getParameter("Txt_ProjDesc"));
					String strDevClass = GmCommonClass.parseNull(request.getParameter("hChkDevClass"));
                    String strApproval = GmCommonClass.parseNull(request.getParameter("hApprove"));
					if ( strApproval.equals("approved") && (strUserId.equals("303010") || strUserId.equals("303061")))
                    {
                        strStatus = "20301";
                        strMessage = " The Project has been approved ";
                    }
					String strGroup = GmCommonClass.parseNull(request.getParameter("Cbo_Group"));
                    String strSegment = GmCommonClass.parseNull(request.getParameter("Cbo_Segment"));
					String strProjectName = GmCommonClass.parseNull(request.getParameter("Cbo_Proj"));
					HashMap hmValues = new HashMap();
					hmValues.put("PNAME",strProjectNm);
					//hmValues.put("IDEAID",strIdeaId);
					hmValues.put("PDESC",strProjDesc);
					hmValues.put("DEVCLASS",strDevClass);
					hmValues.put("STRSTATUS",strStatus);
					hmValues.put("STRGROUP",strGroup);
                    hmValues.put("STRSEGMENT",strSegment);
					hmValues.put("PROJECTTYPE",strProjectName);

					strProjId = GmCommonClass.parseZero((String)session.getAttribute("strSessProjectId")== null ?(String)request.getParameter("hTxnId"):(String)session.getAttribute("strSessProjectId"));
                    
                    log.debug(" Project Id " + strProjId);
					hmValues.put("PROJID",strProjId);
                    
					HashMap hmSaveReturn = new HashMap();
					hmSaveReturn = gmProject.saveProject(hmValues,strUserId,strAction);
                    
					if(strProjId.equals("0")) {
					strProjId = GmCommonClass.parseNull((String)hmSaveReturn.get("PROJECTID"));
					log.debug(" PROJECT ID inside loop is >>> " + strProjId);
					}
					//hmReturn = gmProject.loadProject();
                    
                    // Setting the boolean for Approval access condition
                    if (strUserId.equals("303010") || strUserId.equals("303061"))
                    {
                        blApprove = true;
                    }
                    
					hmReturn = gmProject.loadEditProject(strProjId);
                    log.debug(" Approve " + String.valueOf(blApprove));
                    request.setAttribute("BLAPPROVEACCESS",String.valueOf(blApprove));
                    request.setAttribute("MESSAGE",strMessage);
                    request.setAttribute("PROJECTID",strProjId);
					request.setAttribute("hmReturn",hmReturn);

				}
				else if (strAction.equals("EditLoad"))
				{
                    strProjId = GmCommonClass.parseZero((String)session.getAttribute("strSessProjectId")== null ?(String)request.getParameter("hTxnId"):(String)session.getAttribute("strSessProjectId"));
                    log.debug(" Projecty Id " + strProjId);
                    if (strUserId.equals("303010") || strUserId.equals("303061"))
                    {
                        blApprove = true;
                    }
                    
					hmReturn = gmProject.loadEditProject(strProjId);
					request.setAttribute("hmReturn",hmReturn);
                    request.setAttribute("BLAPPROVEACCESS",String.valueOf(blApprove));
					session.removeAttribute("hAction");
				}
				dispatch(strProdPath.concat("/GmProjectSetup.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmProjectServlet