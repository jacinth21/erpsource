/**
 * 
 */
package com.globus.prodmgmnt.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmGrowthBean;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.beans.GmTerritoryBean;
import com.globus.sales.beans.GmTerritoryReportBean;

/**
 * @author vprasath
 *
 */
public class GmGrowthServlet extends GmServlet
{
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
        HttpSession session = request.getSession(false);
        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strOperPath = GmCommonClass.getString("GMPRODMGMNT");
        String strDispatchTo = strOperPath.concat("/GmGrowthSetup.jsp");
        GmCommonBean gmCommonBean = new GmCommonBean();
        
        GmGrowthBean growthBean = new GmGrowthBean();
        Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
        
        try
        {
            log.debug("Enter");
            checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
            String strAction = request.getParameter("hAction");
            if (strAction == null)
            {
                strAction = (String)session.getAttribute("hAction");
            }
            strAction = (strAction == null)?"Load":strAction;

            
            
            HashMap hmReturn = new HashMap();            
            ArrayList alGrowthDetails = new ArrayList();
            ArrayList alLog = new ArrayList();
            String strRefType       = "";
            String strRefClass      = "";                        
            String strYear          = "";
            String strRefID         = "";
            String strInputString   = "";
            String strUserId        =   (String)session.getAttribute("strSessUserId");
            String strLogReason = "";
            String errMessage = "";
            //String strAccessFilter  = "";
            
            strRefType = request.getParameter("Cbo_RefType")== null ?"":request.getParameter("Cbo_RefType");
            strRefClass = request.getParameter("Cbo_RefClass")== null ?"":request.getParameter("Cbo_RefClass");
            strRefID = request.getParameter("Txt_RefID")== null ?"":request.getParameter("Txt_RefID");
            strYear = request.getParameter("Cbo_Year")== null ?"":request.getParameter("Cbo_Year");
            strInputString = request.getParameter("inputString")== null ?"":request.getParameter("inputString");
            strLogReason = request.getParameter("Txt_LogReason")== null ?"":request.getParameter("Txt_LogReason");
            
            HashMap hmValues = new HashMap();
            hmValues.put("REFTYPE",strRefType);
            hmValues.put("REFCLASS",strRefClass);
            hmValues.put("REFID",strRefID);
            hmValues.put("YEAR",strYear);
            hmValues.put("INPUTSTR",strInputString);   
            hmValues.put("LOG",strLogReason);
            
                       
            if (strAction.equals("Add") || strAction.equals("Update"))
            {                              
                errMessage = growthBean.saveGrowth(hmValues,strUserId);
                alGrowthDetails = growthBean.loadGrowthDetails(hmValues);
                alLog = gmCommonBean.getLog(strYear+"-"+strRefID,"1215");
                
                if(!errMessage.equals(""))
                {
                    hmReturn.put("ERRMSG",errMessage);
                }
                hmReturn.put("GROWTH",alGrowthDetails);
                hmReturn.put("STRREFTYPE",strRefType);
                hmReturn.put("STRREFCLASS",strRefClass);
                hmReturn.put("STRREFID",strRefID);
                request.setAttribute("hmLog",alLog);
                request.setAttribute("hYear",strYear);
                request.setAttribute("hAction","Update");
            }
            else if(strAction.equals("Reload"))
            {
                alGrowthDetails = growthBean.loadGrowthDetails(hmValues);
                alLog = gmCommonBean.getLog(strYear+"-"+strRefID,"1215");
                                
                hmReturn.put("GROWTH",alGrowthDetails);
                hmReturn.put("STRREFTYPE",strRefType);
                hmReturn.put("STRREFCLASS",strRefClass);
                hmReturn.put("STRREFID",strRefID);
                request.setAttribute("hmLog",alLog);
                request.setAttribute("hYear",strYear);
                request.setAttribute("hAction","Update");
            }
                            
            hmReturn.putAll(growthBean.loadGrowth());
            request.setAttribute("hmReturn",hmReturn);
            dispatch(strDispatchTo,request,response);
            log.debug("Exit");
        }// End of try
        catch (Exception e)
        {
                e.printStackTrace();                
                session.setAttribute("hAppErrorMessage",e.getMessage());
                strDispatchTo = strComnPath + "/GmError.jsp";
                gotoPage(strDispatchTo,request,response);
        }// End of catch
    }//End of service method
}// End of GmGrowthServlet

