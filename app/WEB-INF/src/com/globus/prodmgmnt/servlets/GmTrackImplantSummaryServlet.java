package com.globus.prodmgmnt.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProdReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmTrackImplantSummaryServlet extends GmServlet {
    private final String strSetGrpID = "1600";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strProdPath = GmCommonClass.getString("GMPRODMGMNT");
        String strDispatchTo = "/GmTrackImplantSummary.jsp";

        try {
            checkSession(response, session);
            Logger log = GmLogger.getInstance(this.getClass().getName());
            ArrayList alGroup = new ArrayList();
            ArrayList alImplantSummary = new ArrayList();
            String strSetID = "";
            GmProjectBean gmProjectBean = new GmProjectBean();
            GmProdReportBean gmProdReportBean = new GmProdReportBean();

            log.debug("Entering Service Method");

            String strAction = request.getParameter("hAction");
            if (strAction == null) {
                strAction = (String) session.getAttribute("hAction");
            }
            strAction = (strAction == null) ? "Load" : strAction;

            String strOpt = request.getParameter("hOpt");
            if (strOpt == null) {
                strOpt = (String) session.getAttribute("strSessOpt");
            }
            strOpt = (strOpt == null) ? "" : strOpt;

            if (strAction.equals("Go")) {
                strSetID = GmCommonClass.parseNull(request.getParameter("Cbo_GrpID"));
                alImplantSummary = gmProdReportBean.reportTrackImplantSummary(strSetID);                     
                request.setAttribute("alImplantSummary", alImplantSummary);
                request.setAttribute("strSelectedGrpID", strSetID);
            }
            alGroup = gmProjectBean.loadSetMap(strSetGrpID);
            request.setAttribute("alGroup", alGroup);
            log.debug("Exiting Service Method");
            dispatch(strProdPath.concat(strDispatchTo), request, response);

        } // End of try
        catch (Exception e) {
            session.setAttribute("hAppErrorMessage", e.getMessage());
            strDispatchTo = strComnPath + "/GmError.jsp";
            gotoPage(strDispatchTo, request, response);
        }// End of catch

    }
}
