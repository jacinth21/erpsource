/*****************************************************************************
 * File : GmPartNumServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.prodmgmnt.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.common.jms.GmConsumerUtil;
public class GmPartNumServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strProdPath = GmCommonClass.getString("GMPRODMGMNT");
    String strDispatchTo = "/GmPartNumberSetup.jsp";

    try {
      checkSession(response, session); // Checks if the current session
      // is valid, else redirecting to
      // SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code
      // to
      // Initialize
      // the
      // Logger
      // Class.

      // Code to get the Party Access Condition.
      HashMap hmPartyParam = new HashMap();
      hmPartyParam.put("PartyId", session.getAttribute("strSessPartyId"));
      hmPartyParam.put("FunctionId", "1015");// Corresponds to Demand Sheet Access Function Name

      HashMap hmPartyAccess = (new GmAccessFilter()).fetchPartyAccessPermission(hmPartyParam);

      String strUpdateFlg = GmCommonClass.parseNull((String) hmPartyAccess.get("UPDFL"));
      String strVoidFlg = GmCommonClass.parseNull((String) hmPartyAccess.get("VOIDFL"));
      String strReadFlg = GmCommonClass.parseNull((String) hmPartyAccess.get("READFL"));
      String strLimitedAccess = "";

      if (strUpdateFlg != "" && !strUpdateFlg.equalsIgnoreCase("Y")) {
        strLimitedAccess = "true";
        request.setAttribute("DMNDSHT_LIMITED_ACC", strLimitedAccess);
      }

      RowSetDynaClass resultSet = null;
      String strPartNum = "";
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strActionForVali = GmCommonClass.parseNull(request.getParameter("hActionForVali"));


      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      strPartNum =
          request.getParameter("Txt_PartNum") == null ? "" : request.getParameter("Txt_PartNum");

      GmCommonClass gmCommon = new GmCommonClass();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmPartNumBean gmPartNum = new GmPartNumBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmDropValues = new HashMap();
      HashMap hmPartDtls = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strProjId = "";
      String strParentProjId = "";
      String strStatus = "";
      String strProdFly = "";
      String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strQltyEnable = "disabled";

      String strRFS = "";
      ArrayList alPathwayDtls = new ArrayList();
      ArrayList alRegulatoryDtls = new ArrayList();
      String strPathway = "";
      String strTaggableFL = "";

      String strDINumberFl = "";
      String strDINumber = "";
      String strWoCreated = "";
      String strUdiEtchReq = "";
      String strDiOnlyReq = "";
      String strPiOnlyReq = "";
      String strPartAttributeData = "";
      String strUpdateAccess = "";
      String strReadAccess = "";
      String strVoidAccess = "";
      String strLotTrckChk = GmCommonClass.parseNull(request.getParameter("lotTrackFl"));
      String strSerialChk = GmCommonClass.parseNull(request.getParameter("serialNumFl"));
      HashMap hmChkEnableVal = new HashMap();
      // Part number setup screen Submit and field access

      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "PARTNUM_SUBMIT_ACC"));
      strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      strReadAccess = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      strVoidAccess = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
      if (strUpdateAccess.equals("Y")) { // To enable submit button in part num setup screen
        strQltyEnable = "Enable";
      }
      request.setAttribute("hQltyDisabled", strQltyEnable);
      // The Following code is added for resolving the BUG-4725.
      // Due to gmPartNum.loadPartNum() method call from many blocks, we are getting performence
      // issue for loading the many drop down values for more times.
      // so here we are commenting all the method calls and we are getting the drop down values one
      // time and using the same hash map object where ever we are calling the
      // gmPartNum.loadPartNum().
      // and from the GmPartNumBean.loadEditPartNum() method also we are calling
      // gmPartNum.loadPartNum() again. here are also we need remove.

      if (strOpt.equals("FETCHCONTAINER")) {
        hmTemp.put("ACTION", "PART");
        hmTemp.put("PARTNUM", strPartNum);
        hmReturn = gmPartNum.loadEditPartNum(hmTemp);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = "/GmPartNumberSetupContainer.jsp";
      }
      if (!strOpt.equals("FETCHCONTAINER") || (strAction.equals("Save"))) {
        if (!strAction.equals("Save")) {
          hmDropValues = gmPartNum.loadPartNum();
        }
        hmReturn.put("LOADDETAILS", hmDropValues);
        if (strAction.equals("Load") && !strOpt.equals("SUBASMSCH")) {
          // hmReturn = gmPartNum.loadPartNum();
          ArrayList alAcc = gmSales.reportAccount();
          hmReturn.put("LOADDETAILS", hmDropValues);
          hmReturn.put("ACCLIST", alAcc);
          request.setAttribute("hmReturn", hmReturn);
        } else if (strAction.equals("Lookup")) {
          strPartNum =
              request.getParameter("Txt_PartNum") == null ? "" : request
                  .getParameter("Txt_PartNum");
          strProjId =
              request.getParameter("Cbo_AllProj") == null ? "" : request
                  .getParameter("Cbo_AllProj");
          hmTemp.put("ACTION", "PART");
          hmTemp.put("PARTNUM", strPartNum);
          hmReturn = gmPartNum.loadEditPartNum(hmTemp);
		  // If the lot tracking check box or serial number check box is 'Y', then check whether need to disable the check box or not
          hmPartDtls = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("PARTNUMDETAILS"));
          strLotTrckChk = GmCommonClass.parseNull((String) hmPartDtls.get("LOTTRACKFL"));
          strSerialChk = GmCommonClass.parseNull((String) hmPartDtls.get("SERIALNUMNEEDFL"));
          if (strLotTrckChk.equals("Y") || strSerialChk.equals("Y")) {
            hmChkEnableVal = gmPartNum.fchSerialNumVisibility(strPartNum);
          }
          hmReturn.put("LOADDETAILS", hmDropValues);
          strRFS = gmPartNum.fetchRFS(strPartNum);
          alPathwayDtls = gmPartNum.loadPathwayDtls(strPartNum);
          alRegulatoryDtls = gmPartNum.loadRegulatoryDtls(strPartNum);

          request.setAttribute("RFS", strRFS);
          request.setAttribute("PathwayDetail", alPathwayDtls);
          request.setAttribute("RegulatoryDetail", alRegulatoryDtls);
          request.setAttribute("chkboxEnable", hmChkEnableVal);

          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hProjId", strProjId);
          request.setAttribute("hAction", strAction);
        } else if (strAction.equals("Duplicate")) {
          strPartNum =
              request.getParameter("Txt_PartNum") == null ? "" : request
                  .getParameter("Txt_PartNum");
          strProjId =
              request.getParameter("Cbo_AllProj") == null ? "" : request
                  .getParameter("Cbo_AllProj");
          strParentProjId =
              request.getParameter("hParentProjId") == null ? "" : request
                  .getParameter("hParentProjId");

          hmReturn = gmPartNum.saveDuplPartNum(strPartNum, strProjId, strParentProjId, strUserId);
          // hmReturn = gmPartNum.loadPartNum();
          hmReturn.put("LOADDETAILS", hmDropValues);
          request.setAttribute("hmReturn", hmReturn);
        } else if (strAction.equals("Save")) {
          strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
          strProjId = GmCommonClass.parseNull(request.getParameter("Cbo_Proj"));
          strProdFly = GmCommonClass.parseNull(request.getParameter("Cbo_ProdFly"));
          String strPartDesc = GmCommonClass.parseNull(request.getParameter("Txt_PartDesc"));
          String strDetailDesc = GmCommonClass.parseNull(request.getParameter("Txt_DetailDesc"));
          String strProdMat = GmCommonClass.parseNull(request.getParameter("Cbo_ProdMat"));
          String strClass = GmCommonClass.parseNull(request.getParameter("Cbo_Class"));
          String strMeasDev = GmCommonClass.parseNull(request.getParameter("Cbo_MesDev"));
          String strMatSpec = GmCommonClass.parseNull(request.getParameter("Cbo_Mat"));
          String strDrawNum = GmCommonClass.parseNull(request.getParameter("Txt_Draw"));
          String strRevNum = GmCommonClass.parseNull(request.getParameter("Txt_Rev"));
          String strInsertId = GmCommonClass.parseNull(request.getParameter("Txt_InsertId"));
          String strInspectRev = GmCommonClass.parseNull(request.getParameter("Txt_InspRev"));
          String strTrackingImplant = GmCommonClass.parseNull(request.getParameter("Chk_TracImpl"));
          String strActiveFl = GmCommonClass.parseNull(request.getParameter("Txt_AFl"));
          String strPartStatus = GmCommonClass.parseNull(request.getParameter("Cbo_PartStatus"));
          String strReleaseForSale =
              GmCommonClass.parseNull(request.getParameter("Chk_RelForSale"));
          String strSupplyFlag = GmCommonClass.parseNull(request.getParameter("Chk_SupplyFl"));
          String strSubAssemblyFlag =
              GmCommonClass.parseNull(request.getParameter("Chk_SubAssemblyFl"));
          String strSalesGroup = GmCommonClass.parseNull(request.getParameter("Cbo_SalesGroup"));
          String strLogReason = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
          String strUOM = GmCommonClass.parseNull(request.getParameter("Cbo_UOM"));
          String strMasterProd = GmCommonClass.parseNull(request.getParameter("Txt_MastProd"));

          String strRPF = GmCommonClass.parseNull(request.getParameter("Cbo_RPF"));
          String strMinAccpRev = GmCommonClass.parseNull(request.getParameter("Txt_MinAccpRev"));
          strRFS = GmCommonClass.parseNull(request.getParameter("hChkRFS"));
          strPathway = GmCommonClass.parseNull(request.getParameter("hPathway"));
          strPartAttributeData = GmCommonClass.parseNull(request.getParameter("hPartAttribute"));

          strTaggableFL = GmCommonClass.parseNull(request.getParameter("Chk_TaggablePartFl"));


          // get the text field value of part number
          String strTxtPartNumber = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
          // Check whether the entered part number is existing or not
          String strPartCnt =
              GmCommonClass.parseNull(gmPartNum.validatePartNumber(strTxtPartNumber));

          if (strPartCnt.equals("1") && strPartNum.equals("")) {
            throw new AppError("", "20600");
          }

          if (strPartNum.equals("")) {
            strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
          }

          log.debug(" Part Desc ----------- is " + strPartDesc);

          HashMap hmValues = new HashMap();
          hmValues.put("PNUM", strPartNum);
          hmValues.put("PROJID", strProjId);
          hmValues.put("PDESC", strPartDesc);
          hmValues.put("DETAILDESC", strDetailDesc);
          hmValues.put("PFLY", strProdFly);
          hmValues.put("PMAT", strProdMat);
          hmValues.put("PCLASS", strClass);
          hmValues.put("AFL", strActiveFl);
          hmValues.put("MDEV", strMeasDev);
          hmValues.put("MATSP", strMatSpec);
          hmValues.put("DRAW", strDrawNum);
          hmValues.put("REV", strRevNum);
          hmValues.put("INSERTID", strInsertId);
          hmValues.put("INSPREV", strInspectRev);
          hmValues.put("TRACIMPL", strTrackingImplant);
          hmValues.put("PARTSTATUS", strPartStatus);
          hmValues.put("RELFORSALE", strReleaseForSale);
          hmValues.put("SUPPLYFL", strSupplyFlag);
          hmValues.put("SUBASSMFL", strSubAssemblyFlag);
          hmValues.put("SALESGROUP", strSalesGroup);
          hmValues.put("LOG", strLogReason);
          hmValues.put("UOM", strUOM);
          hmValues.put("MINACCPREV", strMinAccpRev);
          hmValues.put("RPF", strRPF);
          hmValues.put("CHKRFS", strRFS);
          hmValues.put("PART_ATTRIBUTE_VALES", strPartAttributeData);
          hmValues.put("TAGGABLEPART", strTaggableFL);
          hmValues.put("MASTERPRODUCT", strMasterProd);
          hmValues.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
          /*
           * hmValues.put("LOTTRKCHK", strLotTrckChk); hmValues.put("SERIALNUMCHK", strSerialChk);
           */


          gmPartNum.savePartNumber(hmValues, strUserId);
        
       
          
          //Part Number Setup screen is timing out while saving data--(PMT-27473)
          GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
          // View refresh JMS called
          HashMap hmViewRefresh = new HashMap();
          hmViewRefresh.put("M_VIEW", "V207B_SET_PART_DETAILS");
          hmViewRefresh.put("companyInfo",
              GmCommonClass.parseNull(request.getParameter("companyInfo")));

          String strViewConsumerClass =
              GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
          hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
          gmConsumerUtil.sendMessage(hmViewRefresh);
          
          hmTemp.put("ACTION", "PART");
          hmTemp.put("PARTNUM", strPartNum);
          hmReturn = gmPartNum.loadEditPartNum(hmTemp);
           // If the lot tracking check box or serial number check box is 'Y', then check whether need to disable the check box or not
          hmPartDtls = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("PARTNUMDETAILS"));
          strLotTrckChk = GmCommonClass.parseNull((String) hmPartDtls.get("LOTTRACKFL"));
          strSerialChk = GmCommonClass.parseNull((String) hmPartDtls.get("SERIALNUMNEEDFL"));
          if (strLotTrckChk.equals("Y") || strSerialChk.equals("Y")) {
            hmChkEnableVal = gmPartNum.fchSerialNumVisibility(strPartNum);
          }
          hmReturn.put("LOADDETAILS", hmDropValues);
          strRFS = gmPartNum.fetchRFS(strPartNum);
          alPathwayDtls = gmPartNum.loadPathwayDtls(strPartNum);
          alRegulatoryDtls = gmPartNum.loadRegulatoryDtls(strPartNum);

          request.setAttribute("chkboxEnable", hmChkEnableVal);
          request.setAttribute("RFS", strRFS);
          request.setAttribute("PathwayDetail", alPathwayDtls);
          request.setAttribute("RegulatoryDetail", alRegulatoryDtls);

          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hProjId", strProjId);
          request.setAttribute("hAction", "Lookup");

        } else if (strAction.equals("EditLoad")) {
          /*
           * strProjId = (String)session.getAttribute("strSessProjectId"); hmReturn =
           * gmProject.loadEditProject(strProjId); request.setAttribute("hmReturn",hmReturn);
           * session.removeAttribute("hAction");
           */
        }

        else if (strAction.equals("Go")) {
          String strPartNums = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
          String strPartStatus = GmCommonClass.parseNull(request.getParameter("PartNum"));

          log.debug(" PArtNum is " + strPartNums + " PartStatus " + strPartStatus);
          alReturn = gmPartNum.searchSubAssembly(strPartNums, strPartStatus);

          log.debug(" Result Set values are " + alReturn.size());
          request.setAttribute("subassemblydetails", alReturn);
          request.setAttribute("ACTION", "Reload");
          request.setAttribute("hPartNum", strPartNums);
          request.setAttribute("hStatus", strPartStatus);
          strDispatchTo = "/GmSubAssemblyPartSearch.jsp";

        }

        else if (strAction.equals("LoadPartDetail")) {
          HashMap hmPartReturn = new HashMap();
          HashMap hmSetReturn = new HashMap();
          HashMap hmVendorReturn = new HashMap();
          strPartNum = GmCommonClass.parseNull(request.getParameter("partNumber"));
          // strStatus = GmCommonClass.parseNull(request.getParameter("status"));
          // temp set hardcode here
          // strStatus = "S";
          // strPartNum = "124.000";
          // hmTemp.put("PARTNUM",strPartNum);
          log.debug("strPartNum is " + strPartNum);
          hmReturn = gmPartNum.loadPartDetail(strPartNum);
          hmPartReturn = (HashMap) hmReturn.get("PARTNUMDETAIL");

          ArrayList alSetDetail = new ArrayList();
          ArrayList alVendorDetail = new ArrayList();
          ArrayList alSubPart = new ArrayList();
          ArrayList alParentPart = new ArrayList();
          ArrayList alPartGroup = new ArrayList();
          alSetDetail = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RDSETDETAIL"));
          alVendorDetail =
              GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RDVENDORDETAIL"));
          alSubPart = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RDSUBPARTDETAIL"));
          alParentPart =
              GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RDPARENTPARTDETAIL"));
          alPartGroup = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RDPARTGROUP"));

          ArrayList al_AccessList = new ArrayList();
          al_AccessList =
              gmAccessControlBean.getPartyList("VENDOR_PRCING_ACS",
                  session.getAttribute("strSessPartyId").toString());
          log.debug("::::::AccessList:::Size..:::" + al_AccessList.size());

          // log.debug("alSetDetail " +
          // (RowSetDynaClass)hmReturn.get("RDSETDETAIL"));
          request.setAttribute("al_AccessList", al_AccessList);
          request.setAttribute("hmReturn", hmPartReturn);
          request.setAttribute("rdSetDetail", alSetDetail);
          request.setAttribute("rdVendorDetail", alVendorDetail);
          request.setAttribute("rdSubPartDetail", alSubPart);
          request.setAttribute("rdParentPartDetail", alParentPart);
          request.setAttribute("rdPartGroup", alPartGroup);
          request.setAttribute("hOpt", strOpt);
          // request.setAttribute("status", strStatus);
          strDispatchTo = "/GmPartNumDetail.jsp";
        }

        else if (strOpt.equals("SUBASMSCH") && !strAction.equals("Go")) {
          strDispatchTo = "/GmSubAssemblyPartSearch.jsp";
        }



        // displaying comments
        alReturn = gmCommonBean.getLog(strPartNum, "1218");
        request.setAttribute("hmLog", alReturn);
        if (strActionForVali.equals("Save") && strProdFly.equals("4058")) // Send mail notification
        {
          try {
            HashMap hmParams = new HashMap();
            hmParams.put("PARTNUMBER", strPartNum);
            ArrayList alBody = new ArrayList();
            alBody.add(hmParams);
            GmCommonClass.sendEmailFromTemplate("GmPartInsertMail", alBody, hmParams);
          } catch (Exception e) {
            try {
              GmEmailProperties emailProps = new GmEmailProperties();
              emailProps.setSender("notification@globusmedical.com");
              emailProps.setRecipients(GmCommonClass
                  .getEmailProperty("GmPartInsertMail.ExceptionMailTo"));
              emailProps.setSubject(GmCommonClass
                  .getEmailProperty("GmPartInsertMail.ExceptionMailSubject") + " " + strPartNum);
              emailProps.setMessage("Exception : " + GmCommonClass.getExceptionStackTrace(e, "\n"));

              GmCommonClass.sendMail(emailProps);
            } catch (Exception ex) {
              log.error("Exception in sending 'exception mail notification' to IT"
                  + ex.getMessage());
            }
          }
        }
      }


      dispatch(strProdPath.concat(strDispatchTo), request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPartNumServlet
