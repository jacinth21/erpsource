package com.globus.prodmgmnt.productcatalog.forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.globus.common.forms.GmCancelForm;

public class GmTranslateByTypeForm extends GmCancelForm{
	private String typeId = "";
	private String typepartid = "";
	private String typesetid = "";
	private String typesystemid = "";
	private String typeprojectid = "";
	private String typegroupid = "";
	private String langId = "";
	private String langNm = "";
	private String description = "";
	private String dtl_description= "";
	private String id = "";
	private String partdescription = "";
	private String xmlGridData  = "";
	private String sysprogrpid = "";
	private String metaid = "";
	private String metaname = "";
	private String savevoidaccess = "";
	private String enableVoidButton = "";
	private String setbundleId = "";
		
	private ArrayList alTranslateTypes = new ArrayList();
	private ArrayList alTransTypeList = new ArrayList();
	private ArrayList alLanguageList = new ArrayList();
	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the typepartid
	 */
	public String getTypepartid() {
		return typepartid;
	}
	/**
	 * @param typepartid the typepartid to set
	 */
	public void setTypepartid(String typepartid) {
		this.typepartid = typepartid;
	}
	/**
	 * @return the typesetid
	 */
	public String getTypesetid() {
		return typesetid;
	}
	/**
	 * @param typesetid the typesetid to set
	 */
	public void setTypesetid(String typesetid) {
		this.typesetid = typesetid;
	}
	/**
	 * @return the typesystemid
	 */
	public String getTypesystemid() {
		return typesystemid;
	}
	/**
	 * @param typesystemid the typesystemid to set
	 */
	public void setTypesystemid(String typesystemid) {
		this.typesystemid = typesystemid;
	}
	/**
	 * @return the typeprojectid
	 */
	public String getTypeprojectid() {
		return typeprojectid;
	}
	/**
	 * @param typeprojectid the typeprojectid to set
	 */
	public void setTypeprojectid(String typeprojectid) {
		this.typeprojectid = typeprojectid;
	}
	/**
	 * @return the typegroupid
	 */
	public String getTypegroupid() {
		return typegroupid;
	}
	/**
	 * @param typegroupid the typegroupid to set
	 */
	public void setTypegroupid(String typegroupid) {
		this.typegroupid = typegroupid;
	}
	/**
	 * @return the langId
	 */
	public String getLangId() {
		return langId;
	}
	/**
	 * @param langId the langId to set
	 */
	public void setLangId(String langId) {
		this.langId = langId;
	}
	/**
	 * @return the langNm
	 */
	public String getLangNm() {
		return langNm;
	}
	/**
	 * @param langNm the langNm to set
	 */
	public void setLangNm(String langNm) {
		this.langNm = langNm;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the dtl_description
	 */
	public String getDtl_description() {
		return dtl_description;
	}
	/**
	 * @param dtl_description the dtl_description to set
	 */
	public void setDtl_description(String dtl_description) {
		this.dtl_description = dtl_description;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the partdescription
	 */
	public String getPartdescription() {
		return partdescription;
	}
	/**
	 * @param partdescription the partdescription to set
	 */
	public void setPartdescription(String partdescription) {
		this.partdescription = partdescription;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the sysprogrpid
	 */
	public String getSysprogrpid() {
		return sysprogrpid;
	}
	/**
	 * @param sysprogrpid the sysprogrpid to set
	 */
	public void setSysprogrpid(String sysprogrpid) {
		this.sysprogrpid = sysprogrpid;
	}	
	/**
	 * @return the metaid
	 */
	public String getMetaid() {
		return metaid;
	}
	/**
	 * @param metaid the metaid to set
	 */
	public void setMetaid(String metaid) {
		this.metaid = metaid;
	}
	
	/**
	 * @return the savevoidaccess
	 */
	public String getSavevoidaccess() {
		return savevoidaccess;
	}
	/**
	 * @param savevoidaccess the savevoidaccess to set
	 */
	public void setSavevoidaccess(String savevoidaccess) {
		this.savevoidaccess = savevoidaccess;
	}
	/**
	 * @return the alTranslateTypes
	 */
	public ArrayList getAlTranslateTypes() {
		return alTranslateTypes;
	}
	/**
	 * @param alTranslateTypes the alTranslateTypes to set
	 */
	public void setAlTranslateTypes(ArrayList alTranslateTypes) {
		this.alTranslateTypes = alTranslateTypes;
	}
	/**
	 * @return the alTransTypeList
	 */
	public ArrayList getAlTransTypeList() {
		return alTransTypeList;
	}
	/**
	 * @param alTransTypeList the alTransTypeList to set
	 */
	public void setAlTransTypeList(ArrayList alTransTypeList) {
		this.alTransTypeList = alTransTypeList;
	}
	/**
	 * @return the alLanguageList
	 */
	public ArrayList getAlLanguageList() {
		return alLanguageList;
	}
	/**
	 * @param alLanguageList the alLanguageList to set
	 */
	public void setAlLanguageList(ArrayList alLanguageList) {
		this.alLanguageList = alLanguageList;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {		
    	try {   
    		  request.setCharacterEncoding("UTF-8");   
    		} catch (UnsupportedEncodingException ex) {   
    		} 
	}
	public String getEnableVoidButton() {
		return enableVoidButton;
	}
	public void setEnableVoidButton(String enableVoidButton) {
		this.enableVoidButton = enableVoidButton;
	}
	public String getMetaname() {
		return metaname;
	}
	public void setMetaname(String metaname) {
		this.metaname = metaname;
	}
	
	public String getSetbundleId() {
		return setbundleId;
	}
	public void setSetbundleId(String setbundleId) {
		this.setbundleId = setbundleId;
	}
}