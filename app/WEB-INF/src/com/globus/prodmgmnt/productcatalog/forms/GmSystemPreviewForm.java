package com.globus.prodmgmnt.productcatalog.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;

public class GmSystemPreviewForm extends GmCancelForm {
	private String systemId = "";
	private String systemNm = "";
	private String sysDesc  = "";
	private String sysDtlDesc = "";
	private String xmlGroupdData = "";
	private String xmlSetData = "";
	private String sysimgid = "";
	private String sysimageNm = "";
	private ArrayList alImageList = new ArrayList();

	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param systemId the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @return the systemNm
	 */
	public String getSystemNm() {
		return systemNm;
	}

	/**
	 * @param systemNm the systemNm to set
	 */
	public void setSystemNm(String systemNm) {
		this.systemNm = systemNm;
	}

	/**
	 * @return the sysDesc
	 */
	public String getSysDesc() {
		return sysDesc;
	}

	/**
	 * @param sysDesc the sysDesc to set
	 */
	public void setSysDesc(String sysDesc) {
		this.sysDesc = sysDesc;
	}

	/**
	 * @return the sysDtlDesc
	 */
	public String getSysDtlDesc() {
		return sysDtlDesc;
	}

	/**
	 * @param sysDtlDesc the sysDtlDesc to set
	 */
	public void setSysDtlDesc(String sysDtlDesc) {
		this.sysDtlDesc = sysDtlDesc;
	}

	/**
	 * @return the xmlGroupdData
	 */
	public String getXmlGroupdData() {
		return xmlGroupdData;
	}

	/**
	 * @param xmlGroupdData the xmlGroupdData to set
	 */
	public void setXmlGroupdData(String xmlGroupdData) {
		this.xmlGroupdData = xmlGroupdData;
	}

	/**
	 * @return the xmlSetData
	 */
	public String getXmlSetData() {
		return xmlSetData;
	}

	/**
	 * @param xmlSetData the xmlSetData to set
	 */
	public void setXmlSetData(String xmlSetData) {
		this.xmlSetData = xmlSetData;
	}

	/**
	 * @return the alImageList
	 */
	public ArrayList getAlImageList() {
		return alImageList;
	}

	/**
	 * @param alImageList the alImageList to set
	 */
	public void setAlImageList(ArrayList alImageList) {
		this.alImageList = alImageList;
	}

	/**
	 * @return the sysimgid
	 */
	public String getSysimgid() {
		return sysimgid;
	}

	/**
	 * @param sysimgid the sysimgid to set
	 */
	public void setSysimgid(String sysimgid) {
		this.sysimgid = sysimgid;
	}

	/**
	 * @return the sysimageNm
	 */
	public String getSysimageNm() {
		return sysimageNm;
	}

	/**
	 * @param sysimageNm the sysimageNm to set
	 */
	public void setSysimageNm(String sysimageNm) {
		this.sysimageNm = sysimageNm;
	}
	
	
}
