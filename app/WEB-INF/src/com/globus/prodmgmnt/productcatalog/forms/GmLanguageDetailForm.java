package com.globus.prodmgmnt.productcatalog.forms;

import com.globus.common.forms.GmCommonForm;

public class GmLanguageDetailForm extends GmCommonForm{
	
	private String gridXmlData = "";
	private int intResultSize;
	private String refID = "";
	private String typeID = "";
	private String langID = "";
	
	public String getGridXmlData() {
		return gridXmlData;
	}
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	public int getIntResultSize() {
		return intResultSize;
	}
	public void setIntResultSize(int intResultSize) {
		this.intResultSize = intResultSize;
	}
	public String getRefID() {
		return refID;
	}
	public void setRefID(String refID) {
		this.refID = refID;
	}
	public String getTypeID() {
		return typeID;
	}
	public void setTypeID(String typeID) {
		this.typeID = typeID;
	}
	public String getLangID() {
		return langID;
	}
	public void setLangID(String langID) {
		this.langID = langID;
	}
	

}
	
	