package com.globus.prodmgmnt.productcatalog.forms;

import com.globus.common.forms.GmCommonForm;

public class GmUploadDetailForm extends GmCommonForm{
	
	private String gridXmlData = "";
	private int intResultSize;
	private String refID = "";
	private String typeID = "";
	private String hParentForm="";
	
	public String getGridXmlData() {
		return gridXmlData;
	}
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	public int getIntResultSize() {
		return intResultSize;
	}
	public void setIntResultSize(int intResultSize) {
		this.intResultSize = intResultSize;
	}
	public String getRefID() {
		return refID;
	}
	public void setRefID(String refID) {
		this.refID = refID;
	}
	public void setTypeID(String typeID) {
		this.typeID = typeID;
	}
	public String getTypeID() {
		return typeID;
	}
	public String gethParentForm() {
		return hParentForm;
	}
	public void sethParentForm(String hParentForm) {
		this.hParentForm = hParentForm;
	}

}