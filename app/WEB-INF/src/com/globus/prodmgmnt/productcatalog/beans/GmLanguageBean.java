package com.globus.prodmgmnt.productcatalog.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmLanguageBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * fetchLanguageDetails - This method returns will give language details.
	 * @param String strRefId
	 * 
	 * @exception AppError
	 * @return ArrayList
	 */
	public ArrayList fetchLanguageDetails (String strRefId, String strType, String strLangId) throws AppError {
		
		ArrayList alResult = new ArrayList();
		
		GmDBManager db = new GmDBManager();
		db.setPrepareString("gm_pkg_pd_language.gm_fch_lang_details",4);
		db.registerOutParameter(4, OracleTypes.CURSOR);
		db.setString(1,strRefId);
		db.setString(2,strType);
		db.setString(3,strLangId);
		db.execute();
		alResult = db.returnArrayList((ResultSet) db.getObject(4));
		db.close();
		return alResult;
	}// End of fetchLanguageDetails

}
