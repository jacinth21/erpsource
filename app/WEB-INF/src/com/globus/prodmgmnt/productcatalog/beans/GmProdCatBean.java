package com.globus.prodmgmnt.productcatalog.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmProdCatBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveDeviceSyncDetail - To save device sync details and sync log after every sync.
   * 
   * @param hmParams
   * @throws AppError
   */
  // this will be removed all place changed with Data Store VO constructor

  public GmProdCatBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmProdCatBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public HashMap saveDeviceSyncDetail(HashMap hmParams) throws AppError {
    HashMap hmReturn = new HashMap();
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    gmDBManager.setPrepareString("gm_pkg_pdpc_prodcattxn.gm_sav_device_sync_dtl", 12);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(12, OracleTypes.VARCHAR);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("REFTYPE"));
    gmDBManager.setString(3, (String) hmParams.get("STATUSID"));
    gmDBManager.setString(4, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(5, (String) hmParams.get("PAGESIZE"));
    gmDBManager.setString(6, (String) hmParams.get("UUID"));
    gmDBManager.setString(7, (String) hmParams.get("USERID"));
    gmDBManager.setString(8, (String) hmParams.get("TOTALSIZE"));
    gmDBManager.setString(9, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(10, (String) hmParams.get("LASTREC"));
    gmDBManager.setString(11, (String) hmParams.get("COMPID"));
    gmDBManager.execute();
    hmReturn.put("STATUS", GmCommonClass.parseNull(gmDBManager.getString(3)));
    hmReturn.put("SERVERCNT", GmCommonClass.parseNull(gmDBManager.getString(12)));
    gmDBManager.commit();
    return hmReturn;
  }

  /**
   * fetchSystems - To fetch details of all Data's to be sync'd with device.
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchDataSyncDtls(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_pdpc_prodcatrpt.gm_fch_all_datasync_update", 6);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.setString(5, (String) hmParams.get("PARTYID"));
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchSystems - To fetch Auto update ref type with count
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */

  public ArrayList fetchAutoUpdate(HashMap hmParams) throws AppError {
    String strRefType = "";
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();

    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
    // fetch Case Auto Update
    alResult = gmCaseBookRptBean.fetchCaseSynUpdate(hmParams);

    return alResult;
  }

  /**
   * saveDeviceSyncClearDetail - This method is used to void the sync details for respective device
   * based on the UUID and token.
   * 
   * @param hmParams
   * @throws AppError
   */
  public void saveDeviceClearSyncDetail(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_pdpc_prodcattxn.gm_sav_device_clr_sync", 4);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("TOKEN")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("UUID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("SYNCTYPE")));
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveLangDetail - To save Different Language Details for a PART/SET/SYSTEM/GROUP/PROJECT
   * 
   * @param hmParams
   * @throws AppError
   */
  public void saveLangDetail(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    String strLangTypeId = GmCommonClass.parseNull((String) hmParams.get("SYSPROGRPID"));
    String strTypeId = GmCommonClass.parseNull((String) hmParams.get("TYPEID"));
    String strLangName = GmCommonClass.parseNull((String) hmParams.get("LANGNM"));
    String strDesc = GmCommonClass.parseNull((String) hmParams.get("DESCRIPTION"));
    String strDtlDesc = GmCommonClass.parseNull((String) hmParams.get("DTL_DESCRIPTION"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strLangId = GmCommonClass.parseNull((String) hmParams.get("LANGID"));

    gmDBManager.setPrepareString("gm_pkg_pd_language.gm_sav_lang_details", 7);
    gmDBManager.setString(1, strLangTypeId);
    gmDBManager.setString(2, strTypeId);
    gmDBManager.setString(3, strLangName);
    gmDBManager.setString(4, strDesc);
    gmDBManager.setString(5, strDtlDesc);
    gmDBManager.setString(6, strUserId);
    gmDBManager.setString(7, strLangId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchPartNumberInfo - To fetch details of Part.
   * 
   * @param String strPartNumber
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchPartNumberInfo(String strPartNumber) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmPartData = new HashMap();
    Boolean ErrorFl = false;
    gmDBManager.setPrepareString("gm_pkg_pd_prod_cat.gm_fch_part_info", 2);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmPartData =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmPartData;
  }

  /**
   * fetchSetInfo - To fetch details of Set.
   * 
   * @param String strSetId
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchSetInfo(String strSetId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmSetData = new HashMap();
    Boolean ErrorFl = false;
    gmDBManager.setPrepareString("gm_pkg_pd_prod_cat.gm_fch_set_info", 2);
    gmDBManager.setString(1, strSetId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmSetData =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmSetData;
  }
  
  
  /**
   * fetchSetBundleInfo - To fetch details of SetBundle.
   * 
   * @param 
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchSetBundleInfo() throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alSetBundleData = new ArrayList();
    Boolean ErrorFl = false;
    gmDBManager.setPrepareString("gm_pkg_pd_prod_cat.gm_fch_set_bundle_info", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alSetBundleData =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(1)));
    gmDBManager.close();
    log.debug("hmSetBundleData-->"+alSetBundleData);
    return alSetBundleData;
  }
}
