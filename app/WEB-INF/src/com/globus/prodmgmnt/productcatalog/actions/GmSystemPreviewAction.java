package com.globus.prodmgmnt.productcatalog.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.beans.GmSystemBean;
import com.globus.prodmgmnt.productcatalog.forms.GmSystemPreviewForm;

public class GmSystemPreviewAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    GmSystemPreviewForm gmSystemPreviewForm = (GmSystemPreviewForm) form;
    gmSystemPreviewForm.loadSessionParameters(request);
    String strOpt = GmCommonClass.parseNull(gmSystemPreviewForm.getStrOpt());
    GmProjectBean gmProjectBean = new GmProjectBean();
    GmSystemBean gmSystemBean = new GmSystemBean();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    HashMap hmParam = new HashMap();
    String strSystemID = "";
    String strSysDes = "";
    String strSySDtlDesc = "";
    String strXmlGroupData = "";
    String strXmlSetData = "";
    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmSystemPreviewForm));
    HashMap hmSystemInfo = new HashMap();
    ArrayList alGroupList = new ArrayList();
    ArrayList alSetList = new ArrayList();
    ArrayList alImageList = new ArrayList();
    HashMap hmReturn = new HashMap();
    if (strOpt.equals("Load")) {
      strSystemID = GmCommonClass.parseNull(gmSystemPreviewForm.getSystemId());
      log.debug("The System ID inside the Load strOption of GmSystemPreviewAction ***** "
          + strSystemID);
      hmSystemInfo = gmProjectBean.loadEditSetMaster(strSystemID);
      strSysDes = GmCommonClass.parseNull((String) hmSystemInfo.get("SETDESC"));
      strSySDtlDesc = GmCommonClass.parseNull((String) hmSystemInfo.get("SETDETAILDESC"));
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));
      gmSystemPreviewForm.setSysDesc(strSysDes);
      gmSystemPreviewForm.setSysDtlDesc(strSySDtlDesc);
      alGroupList = gmSystemBean.fetchSystemGroups(strSystemID);
      hmReturn.put("GROUPDATA", alGroupList);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      strXmlGroupData = generateGroupOutPut(hmReturn);
      alSetList = gmSystemBean.fetchSystemSets(strSystemID);
      hmReturn.put("SETDATA", alSetList);
      strXmlSetData = generateSetOutPut(hmReturn);
      alImageList = gmCommonUploadBean.fetchUploadInfo("103112", strSystemID); // [103112]Image
      gmSystemPreviewForm.setAlImageList(alImageList);
      gmSystemPreviewForm.setXmlSetData(strXmlSetData);
      gmSystemPreviewForm.setXmlGroupdData(strXmlGroupData);
    }
    String strFrdPage = "gmSystemPreview";
    return mapping.findForward(strFrdPage);
  }

  /**
   * generateGroupOutPut xml content generation for Available Group Details
   * 
   * @param HashMap
   * @return String
   */
  private String generateGroupOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setTemplateSubDir("prodmgmnt/productcatalog/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.productcatalog.GmAvailableGroups", strSessCompanyLocale));
    templateUtil.setTemplateName("GmAvailableGroups.vm");
    return templateUtil.generateOutput();
  }

  /**
   * generateSetOutPut xml content generation for Available Set Details
   * 
   * @param HashMap
   * @return String
   */
  private String generateSetOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("prodmgmnt/productcatalog/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.productcatalog.GmAvailableSets", strSessCompanyLocale));
    templateUtil.setTemplateName("GmAvailableSets.vm");
    return templateUtil.generateOutput();
  }
}
