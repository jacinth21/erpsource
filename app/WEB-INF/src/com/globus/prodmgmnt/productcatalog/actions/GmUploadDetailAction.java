package com.globus.prodmgmnt.productcatalog.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.productcatalog.forms.GmUploadDetailForm;
import com.globus.sales.pricing.beans.GmGroupPricingBean;

public class GmUploadDetailAction extends GmAction {

  private static String strComnPath = GmCommonClass.getString("GMCOMMON");

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                  // Class.

    instantiate(request, response);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    GmGroupPricingBean gmGroupPricingBean = new GmGroupPricingBean();
    GmUploadDetailForm gmUploadDtlForm = (GmUploadDetailForm) form;
    gmUploadDtlForm.loadSessionParameters(request);

    ArrayList alReport = new ArrayList();
    ArrayList alResult = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();

    String strOpt = "";
    String strFrdPage = "UploadDtl";
    String strPricingType = "";
    String strPartGrpId = "";


    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";

    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmUploadDtlForm));
    strOpt = GmCommonClass.parseNull(gmUploadDtlForm.getStrOpt());
    String strParentForm = GmCommonClass.parseNull(gmUploadDtlForm.gethParentForm());

    String strRefID = GmCommonClass.parseNull(gmUploadDtlForm.getRefID());
    String strTypeID = GmCommonClass.parseNull(gmUploadDtlForm.getTypeID());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    if (strOpt.equals("report")) {
      if (strParentForm.equals("PartSetup")) {
        // fetchGrpPartPricingType is used to get the Pricing Type and Part Group ID
        alResult =
            GmCommonClass.parseNullArrayList(gmGroupPricingBean.fetchGrpPartPricingType(strRefID));
        if (alResult.size() > 0) {
          hmResult = GmCommonClass.parseNullHashMap((HashMap) alResult.get(0));
          strPartGrpId = (String) hmResult.get("GROUPID");
          strPricingType = (String) hmResult.get("PRICEDTYPE");
        }
      }
      if (strPricingType.equals("52110")) { // Pricing by type is Group
        alReport =
            GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo("", strRefID,
                strTypeID, strPartGrpId));
      } else {
        alReport =
            GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo("", strRefID,
                strTypeID));
      }

      gmUploadDtlForm.setIntResultSize(alReport.size());

      String strXmlData = generateOutPut(alReport, strSessCompanyLocale);
      gmUploadDtlForm.setGridXmlData(strXmlData);
      strFrdPage = "UploadDtl";
    }

    if (strOpt.equals("Download")) {
      File file = null;
      String strUploadString = "";
      FileInputStream istr = null;
      String strFileName = "";
      String strUploadHomeDir = GmCommonClass.getString("PRODCATALOGUPLOADDIR");
      String strFileId = GmCommonClass.parseNull(request.getParameter("fileID"));
      String strFileGroupNm = GmCommonClass.parseNull(request.getParameter("fileGroupNM"));
      String strRefId = GmCommonClass.parseNull(request.getParameter("refID"));

      strUploadHomeDir = strUploadHomeDir + strFileGroupNm + "\\" + strRefId + "\\";

      strFileName = gmCommonUploadBean.fetchFileName(strFileId);
      ServletOutputStream out = response.getOutputStream();
      response.setContentType("application/binary");
      response.setHeader("Content-disposition", "attachment;filename=" + strFileName);
      log.debug("file name......" + strUploadHomeDir + strFileName);
      try {
        file = new File(strUploadHomeDir + strFileName);
        istr = new FileInputStream(file);
      } catch (FileNotFoundException ffe) {
        log.debug("CATCH EXE" + ffe);
        response.sendRedirect(response.encodeRedirectUrl(strErrorFileToDispatch));
        throw new AppError("", "20676");
      }
      response.setContentLength((int) file.length());
      int curByte = -1;
      while ((curByte = istr.read()) != -1)
        out.write(curByte);
      istr.close();
      out.flush();
      out.close();
      strFrdPage = "UploadDtl";
    }

    gmUploadDtlForm.setStrOpt(strOpt);
    return mapping.findForward(strFrdPage);
  }

  private String generateOutPut(ArrayList alGridData, String strSessCompanyLocale) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmPartNumberSetup", strSessCompanyLocale));
    templateUtil.setDataList("alResult", alGridData);
    templateUtil.setTemplateSubDir("prodmgmnt/productcatalog/templates");
    templateUtil.setTemplateName("GmUploadDetail.vm");
    return templateUtil.generateOutput();
  }

}
