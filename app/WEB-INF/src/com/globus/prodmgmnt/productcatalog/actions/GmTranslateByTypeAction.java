package com.globus.prodmgmnt.productcatalog.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.prodmgmnt.beans.GmProjectInfoBean;
import com.globus.prodmgmnt.productcatalog.beans.GmLanguageBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.prodmgmnt.productcatalog.forms.GmTranslateByTypeForm;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;

public class GmTranslateByTypeAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    GmTranslateByTypeForm gmTranslateByTypeForm = (GmTranslateByTypeForm) form;
    gmTranslateByTypeForm.loadSessionParameters(request);

    GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean();
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
    GmLanguageBean gmLanguageBean = new GmLanguageBean();
    GmProdCatBean gmProdCatBean = new GmProdCatBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean();
    String strOpt = GmCommonClass.parseNull(gmTranslateByTypeForm.getStrOpt());
    String strAction = GmCommonClass.parseNull(gmTranslateByTypeForm.getHaction());

    ArrayList alProjectList = new ArrayList();
    ArrayList alSystemList = new ArrayList();
    ArrayList alGroupList = new ArrayList();
    ArrayList alTranslateData = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmPartInfo = new HashMap();
    HashMap hmSetInfo = new HashMap();
    HashMap hmSetBundleInfo = new HashMap();
    HashMap hmSaveVoidAccess = new HashMap();
    HashMap hmDetails = new HashMap();

    String strFrdPage = "gmTranslateByType";
    String strLangID = "";
    String strTypeID = "";
    String strId = "";
    String strDescription = "";
    String strTypePartId = "";
    String strTypeSetId = "";
    String strTypeSystemId = "";
    String strTypeProjectId = "";
    String strTypeGroupId = "";
    String strPartyId = "";
    String strSaveVoidAccess = "";
    String strxmlGridData = "";
    String strBundleId = "";
    String strBundleDesc = "";

    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmTranslateByTypeForm));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    log.debug("strOpt= " + strOpt + " ----strAction= " + strAction);
    gmTranslateByTypeForm.setAlTranslateTypes(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("MREFTY")));

    strPartyId = gmTranslateByTypeForm.getSessPartyId();
    hmSaveVoidAccess =
        GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
            "PD_TRANSLATE_TYPE")));
    strSaveVoidAccess = GmCommonClass.parseNull((String) hmSaveVoidAccess.get("UPDFL"));
    gmTranslateByTypeForm.setSavevoidaccess(strSaveVoidAccess);

    if (strOpt.equals("Save")) {
      log.debug("The hashmap values insdie the SaveLanguageDetail *****" + hmParam);
      gmProdCatBean.saveLangDetail(hmParam);
      gmTranslateByTypeForm.setStrOpt("FetchTranslate");
      strOpt = "FetchTranslate";
      gmTranslateByTypeForm.setLangId("0");
      gmTranslateByTypeForm.setLangNm("");
      gmTranslateByTypeForm.setDescription("");
      gmTranslateByTypeForm.setDtl_description("");
      gmTranslateByTypeForm.setMetaid("");
    }
    if (strAction.equals("OnReloadPart")) {
      strTypePartId = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypepartid());
      strOpt = "FetchTranslate";
    }
    if (strAction.equals("OnReloadSet")) {
      strTypeSetId = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypesetid());
      strOpt = "FetchTranslate";
    }
    if (strAction.equals("OnReloadSetBundle")) {
      alSystemList = GmCommonClass.parseNullArrayList(gmProdCatBean.fetchSetBundleInfo());
      gmTranslateByTypeForm.setAlTransTypeList(alSystemList);
    }
    if (strAction.equals("OnReloadProject")) {
      alProjectList = GmCommonClass.parseNullArrayList(gmProjectInfoBean.fetchProjectList());
      gmTranslateByTypeForm.setAlTransTypeList(alProjectList);
    }
    if (strAction.equals("OnReloadSystem")) {
      alSystemList = GmCommonClass.parseNullArrayList(gmSalesReportBean.loadProjectGroup());
      // alSystemList = GmCommonClass.parseNullArrayList(gmPricingRequestBean.loadSystemsList());
      gmTranslateByTypeForm.setAlTransTypeList(alSystemList);
    }
    if (strAction.equals("OnReloadGroup")) {
      hmParam.put("STROPT", "40045");
      alGroupList =
          GmCommonClass.parseNullArrayList(gmDemandSetupBean.loadDemandGroupList(hmParam));
      gmTranslateByTypeForm.setAlTransTypeList(alGroupList);
    }
    gmTranslateByTypeForm.setAlLanguageList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("MLANG")));
    if (strOpt.equals("FetchTranslate") || strOpt.equals("FetchTranslateEdit")) {
      strTypeID = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypeId());
      if (strTypeID.equals("103090")) { // PART
        strTypePartId = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypepartid());
        if (!strTypePartId.equals("")) {
          hmPartInfo = gmProdCatBean.fetchPartNumberInfo(strTypePartId);
          strId = GmCommonClass.parseNull((String) hmPartInfo.get("PARTNUM"));
          strDescription = GmCommonClass.parseNull((String) hmPartInfo.get("PDESC"));
        }
        alTranslateData = gmLanguageBean.fetchLanguageDetails(strTypePartId, strTypeID, strLangID);
        gmTranslateByTypeForm.setSysprogrpid(strTypePartId);
      }
      if (strTypeID.equals("103091")) { // SET
        strTypeSetId = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypesetid());
        if (!strTypeSetId.equals("")) {
          hmSetInfo = gmProdCatBean.fetchSetInfo(strTypeSetId);
          strId = GmCommonClass.parseNull((String) hmSetInfo.get("SETID"));
          strDescription = GmCommonClass.parseNull((String) hmSetInfo.get("SETNM"));
        }
        alTranslateData = gmLanguageBean.fetchLanguageDetails(strTypeSetId, strTypeID, strLangID);
        gmTranslateByTypeForm.setSysprogrpid(strTypeSetId);
      }
     if (strTypeID.equals("26240459")) { // SETBUNDLE
          strBundleId = GmCommonClass.parseNull(gmTranslateByTypeForm.getSetbundleId());
          alTranslateData = gmLanguageBean.fetchLanguageDetails(strBundleId, strTypeID, strLangID);
          gmTranslateByTypeForm.setSysprogrpid(strBundleId);
      }
      if (strTypeID.equals("103092")) { // SYSTEM
        strTypeSystemId = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypesystemid());
        alTranslateData =
            gmLanguageBean.fetchLanguageDetails(strTypeSystemId, strTypeID, strLangID);
        gmTranslateByTypeForm.setSysprogrpid(strTypeSystemId);
      }
      if (strTypeID.equals("103093")) { // PROJECT
        strTypeProjectId = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypeprojectid());
        alTranslateData =
            gmLanguageBean.fetchLanguageDetails(strTypeProjectId, strTypeID, strLangID);
        gmTranslateByTypeForm.setSysprogrpid(strTypeProjectId);
      }
      if (strTypeID.equals("103094")) { // GROUP
        strTypeGroupId = GmCommonClass.parseNull(gmTranslateByTypeForm.getTypegroupid());
        alTranslateData = gmLanguageBean.fetchLanguageDetails(strTypeGroupId, strTypeID, strLangID);
        gmTranslateByTypeForm.setSysprogrpid(strTypeGroupId);
      }
      if (strOpt.equals("FetchTranslateEdit") || strAction.equals("OnReloadSet")
          || strAction.equals("OnReloadPart") || strAction.equals("OnReloadSetBundle")) {
        strLangID = GmCommonClass.parseNull(gmTranslateByTypeForm.getLangId());
        if (alTranslateData.size() > 0) {
          for (int i = 0; i < alTranslateData.size(); i++) {
            hmDetails = GmCommonClass.parseNullHashMap((HashMap) alTranslateData.get(i));
            String strLanuageID = GmCommonClass.parseNull((String) hmDetails.get("LANGID"));
            if (strLanuageID.equals(strLangID)) {
              gmTranslateByTypeForm.setLangId((String) hmDetails.get("LANGID"));
              gmTranslateByTypeForm.setLangNm((String) hmDetails.get("NAME"));
              gmTranslateByTypeForm.setDescription((String) hmDetails.get("DESCRIPTION"));
              gmTranslateByTypeForm.setDtl_description((String) hmDetails.get("DTL_DESCRIPTION"));
              gmTranslateByTypeForm.setMetaid((String) hmDetails.get("METAID"));
              gmTranslateByTypeForm.setMetaname((String) hmDetails.get("LANGNM"));
            }
          }
        }
        if (strSaveVoidAccess.equals("Y")) {
          gmTranslateByTypeForm.setEnableVoidButton("Y");
        }
        strOpt = "FetchTranslate";
      }

      gmTranslateByTypeForm.setId(strId);
      gmTranslateByTypeForm.setPartdescription(strDescription);
      gmTranslateByTypeForm.setTypeId(strTypeID);
      gmTranslateByTypeForm.setTypepartid(strTypePartId);
      gmTranslateByTypeForm.setTypesetid(strTypeSetId);
      gmTranslateByTypeForm.setTypesystemid(strTypeSystemId);
      gmTranslateByTypeForm.setTypeprojectid(strTypeProjectId);
      gmTranslateByTypeForm.setTypegroupid(strTypeGroupId);

      hmReturn.put("TRANSLATEDATA", alTranslateData);
      hmReturn.put("APPLNDATEFMT", strApplDateFmt);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

      strxmlGridData = generateOutPut(hmReturn);
      gmTranslateByTypeForm.setXmlGridData(strxmlGridData);
      // gmTranslateByTypeForm.setLangId("0");
      log.debug("The Language Details **** " + alTranslateData);
    }
    if (strOpt.equals("Load")) {
      gmTranslateByTypeForm.setSysprogrpid("");
    }
    gmTranslateByTypeForm.setStrOpt(strOpt);
    gmTranslateByTypeForm.setHaction(strAction);
    gmTranslateByTypeForm.reset(mapping, request);
    return mapping.findForward(strFrdPage);
  }

  /**
   * generateOutPut xml content generation for Translate Language Details
   * 
   * @param HashMap
   * @return String
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("prodmgmnt/productcatalog/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.productcatalog.GmTranslateByType", strSessCompanyLocale));
    templateUtil.setTemplateName("GmTranslateByType.vm");
    return templateUtil.generateOutput();
  }
}
