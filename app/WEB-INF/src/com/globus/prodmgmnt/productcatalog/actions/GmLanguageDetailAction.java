package com.globus.prodmgmnt.productcatalog.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.productcatalog.beans.GmLanguageBean;
import com.globus.prodmgmnt.productcatalog.forms.GmLanguageDetailForm;

public class GmLanguageDetailAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                  // Class.

    instantiate(request, response);
    GmLanguageDetailForm gmLangDtlForm = (GmLanguageDetailForm) form;
    gmLangDtlForm.loadSessionParameters(request);
    GmLanguageBean gmLanguageBean = new GmLanguageBean();
    HashMap hmParam = new HashMap();

    String strOpt = "";
    String strFrdPage = "LanguageDetail";
    String strRefId = "";
    String strType = "";
    String strLangId = "";

    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLangDtlForm));
    strOpt = GmCommonClass.parseNull(gmLangDtlForm.getStrOpt());
    strRefId = GmCommonClass.parseNull(gmLangDtlForm.getRefID());
    strType = GmCommonClass.parseNull(gmLangDtlForm.getTypeID());
    strLangId = GmCommonClass.parseNull(gmLangDtlForm.getLangID());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    if (strOpt.equals("report")) {
      ArrayList alReport =
          GmCommonClass.parseNullArrayList(gmLanguageBean.fetchLanguageDetails(strRefId, strType,
              strLangId));
      gmLangDtlForm.setIntResultSize(alReport.size());
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      String strXmlData = generateOutPut(alReport, hmParam);
      gmLangDtlForm.setGridXmlData(strXmlData);
      strFrdPage = "LanguageDetail";
    }

    gmLangDtlForm.setStrOpt(strOpt);
    return mapping.findForward(strFrdPage);
  }

  private String generateOutPut(ArrayList alGridData, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alResult", alGridData);
    templateUtil.setDataMap("hmResult", hmParam);
    templateUtil.setTemplateSubDir("prodmgmnt/productcatalog/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmPartNumberSetup", strSessCompanyLocale));
    templateUtil.setTemplateName("GmLanguageDetail.vm");
    return templateUtil.generateOutput();
  }
}
