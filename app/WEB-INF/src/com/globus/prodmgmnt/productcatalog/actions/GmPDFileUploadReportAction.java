package com.globus.prodmgmnt.productcatalog.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.prodmgmnt.productcatalog.forms.GmPDFileUploadForm;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.prodmgmnt.beans.GmFileUploadBean;

/**
* GmPDFileUploadReportAction : Contains the methods to load file uploaded reports
* author : Agilan Singaravel
*/
public class GmPDFileUploadReportAction extends GmDispatchAction {
	private static String strComnPath = GmCommonClass.getString("GMCOMMON");
	/**This used to get the instance of this class for enabling logging purpose*/
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
  
   /**
   	 * PMT-51171: Report For Uploaded Files List
  	 * loadUploadedFilesReport - used to load report screen for uploaded files 
  	 * @param mapping
  	 * @param form
  	 * @param request
  	 * @param response
  	 * @return
  	 * @throws Exception
  	 */
  public ActionForward loadUploadedFilesReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
	  
    instantiate(request, response);
    GmPDFileUploadForm gmPDFileUploadForm = (GmPDFileUploadForm) form;
    gmPDFileUploadForm.loadSessionParameters(request);
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    
    ArrayList alSystemList = new ArrayList();
    ArrayList alPartSearch = new ArrayList(); 
    ArrayList alGroupList = new ArrayList(); 
    ArrayList alFilePath = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmFileProp = new HashMap(); 
    String strArtifactNm = "";
    String strSubTypeNm = "";
    String strUploadHomeDir = "";
    String strAction = GmCommonClass.parseNull(gmPDFileUploadForm.getHaction());
    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    //PC-3631 Corporate Files Upload on SpineIT - Globus App
    String strSetGroupType = "1600,26241209";
    
    ArrayList alUploadType = new ArrayList();
    alUploadType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MREFTY"));
    for (int i = 0; i < alUploadType.size(); i++) {
      HashMap hmTemp = GmCommonClass.parseNullHashMap((HashMap) alUploadType.get(i));
      String strCodeId = GmCommonClass.parseNull((String) hmTemp.get("CODEID"));
      if (!strCodeId.equals("103092")){
   	   alUploadType.remove(i);
        i--;
      }
    }
    
    gmPDFileUploadForm.setAlUploadTypes(alUploadType);
    gmPDFileUploadForm.setAlSubType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FSBTYP")));
    gmPDFileUploadForm.setAlFileType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FTYPE")));
    gmPDFileUploadForm.setAlPartSearch(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES")));
  
   // loadSystemsList is used to fetch all the system names for dropdown
   //PC-3631 Corporate Files Upload on SpineIT - Globus App - add strSetGroupType parameter
   alSystemList = GmCommonClass.parseNullArrayList(gmSalesReportBean.loadProjectGroup(strSetGroupType));
   gmPDFileUploadForm.setAlSysTypeList(alSystemList);
	if (strAction.equals("Download")) {
        File file = null;
        String strUploadString = "";
        FileInputStream istr = null;
        String strFileName = "";
        String strFileId = GmCommonClass.parseNull(request.getParameter("fileID"));
        String strFileGroupNm = GmCommonClass.parseNull(request.getParameter("fileGroupNM"));
        String strTypeId = GmCommonClass.parseNull(request.getParameter("typeID"));
        strArtifactNm = GmCommonClass.parseNull(request.getParameter("artifactNm"));
        strSubTypeNm = GmCommonClass.parseNull(request.getParameter("subTypeNm"));
        alFilePath = new ArrayList();
        alFilePath.add(strFileGroupNm);
        alFilePath.add(strTypeId);
        alFilePath.add(strArtifactNm);
        alFilePath.add(strSubTypeNm);

        hmFileProp.put("PATHPROPERTY", "PRODCATALOGUPLOADDIR");
        hmFileProp.put("SEPARATOR", "\\");
        hmFileProp.put("APPENDPATH", alFilePath);

        strUploadHomeDir = gmProdCatUtil.getFilePath(hmFileProp);
        strFileName = gmCommonUploadBean.fetchFileName(strFileId);
        ServletOutputStream out = response.getOutputStream();
        response.setContentType("application/binary");
        response.setHeader("Content-disposition", "attachment;filename=" + strFileName);
        log.debug("file name......" + strUploadHomeDir + strFileName);
        try {
          file = new File(strUploadHomeDir + strFileName);
          istr = new FileInputStream(file);
        } catch (FileNotFoundException ffe) {
          log.debug("CATCH EXE" + ffe);
          response.sendRedirect(response.encodeRedirectUrl(strErrorFileToDispatch));
          throw new AppError("", "20676");
        }
        response.setContentLength((int) file.length());
        int curByte = -1;
        while ((curByte = istr.read()) != -1)
          out.write(curByte);
        istr.close();
        out.flush();
        out.close();
      }

    return mapping.findForward("gmPDFileUploadReport");
  }
  
  /**
	 * fetchUploadedFilesReport - used to fetch uploaded files for products
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchUploadedFilesReport(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmPDFileUploadForm gmPDFileUploadForm = (GmPDFileUploadForm) form;
		gmPDFileUploadForm.loadSessionParameters(request);
		GmFileUploadBean gmFileUploadBean = new GmFileUploadBean();

		String strPartNumFormat = "";
		String strJSONGridData = "";
		String strCompanyId = "";
		HashMap hmParam = new HashMap();

		hmParam = GmCommonClass.getHashMapFromForm(gmPDFileUploadForm);
			
		strPartNumFormat = getPartNumFromLikes(hmParam);
		strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
		hmParam.put("PARTNUMID", strPartNumFormat);
		hmParam.put("COMPANYFMT", strCompanyId);
		
		strJSONGridData = GmCommonClass.parseNull(gmFileUploadBean.fetchUploadedFilesReport(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;			
	}

/**
 * This method is used to format the part number based on likes
 * 
 * @param hmParam
 */	
	public String getPartNumFromLikes(HashMap hmParam) throws AppError {
	
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLIKES"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMID"));
		String strPartNumFormat = strPartNum;
	
			if (strSearch.equals("LIT")) {
				strPartNumFormat = ("^").concat(strPartNumFormat);
				strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
				strPartNumFormat = strPartNumFormat.concat("$");
			} else if (strSearch.equals("LIKEPRE")) {
				strPartNumFormat = ("^").concat(strPartNumFormat);
				strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
			} else if (strSearch.equals("LIKESUF")) {
				strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
				strPartNumFormat = strPartNumFormat.concat("$");
			} else if (strSearch.equals("")) {
				strPartNumFormat = strPartNumFormat;
			} else {
				strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
			}

		return strPartNumFormat;
	}
}