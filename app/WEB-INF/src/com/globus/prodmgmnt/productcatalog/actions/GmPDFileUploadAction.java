package com.globus.prodmgmnt.productcatalog.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.prodmgmnt.beans.GmProjectInfoBean;
import com.globus.prodmgmnt.beans.GmShareEngineTransBean;
import com.globus.prodmgmnt.productcatalog.beans.GmProdCatBean;
import com.globus.prodmgmnt.productcatalog.forms.GmPDFileUploadForm;
import com.globus.sales.beans.GmSalesReportBean;

public class GmPDFileUploadAction extends GmAction {

  private static String strComnPath = GmCommonClass.getString("GMCOMMON");
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPDFileUploadForm gmPDFileUploadForm = (GmPDFileUploadForm) form;
    gmPDFileUploadForm.loadSessionParameters(request);

    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean(getGmDataStoreVO());
    GmProdCatBean gmProdCatBean = new GmProdCatBean(getGmDataStoreVO());
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmShareEngineTransBean gmShareEngineTransBean = new GmShareEngineTransBean(getGmDataStoreVO());
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();

    String strOpt = GmCommonClass.parseNull(gmPDFileUploadForm.getStrOpt());
    String strAction = GmCommonClass.parseNull(gmPDFileUploadForm.getHaction());
    String strUserID = GmCommonClass.parseNull(gmPDFileUploadForm.getUserId());
    String strPartyId = GmCommonClass.parseNull(gmPDFileUploadForm.getSessPartyId());
    String strFwdAction = GmCommonClass.parseNull(gmPDFileUploadForm.getForwardAction());

    ArrayList alProjectList = new ArrayList();
    ArrayList alSystemList = new ArrayList();
    ArrayList alGroupList = new ArrayList();
    ArrayList alPartList = new ArrayList();
    ArrayList alSetList = new ArrayList();
    ArrayList alFilePath = new ArrayList();
    HashMap hmAccess = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmPartInfo = new HashMap();
    HashMap hmSetInfo = new HashMap();
    HashMap hmRule = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmMessageObj = new HashMap();
    HashMap hmParamJob = new HashMap();
    HashMap hmFileProp = new HashMap();
    HashMap hmSetBundleInfo = new HashMap();

    String accessFlag = "";
    String strForwardPage = "gmPDFileUpload";
    String strUploadTypeId = "";
    String strPartListId = "";
    String strSetListId = "";
    String strSysListId = "";
    String strPrjListId = "";
    String strGrpListId = "";
    String strListId = "";
    String strPartNumId = "";
    String strPartNumDesc = "";
    String strSetId = "";
    String strSetDesc = "";
    String strInputstring = "";
    String strForward = "";
    String strRefId = "";
    String strSubType = "";
    String strTitleType = "";
    String strDcoedFlag = "";
    String strNonDcoedFlag = "";
    String strUploadSubDir = "";
    String strArtifactNm = "";
    String strSubTypeNm = "";
    String strUploadHomeDir = "";
    String strConsumFileSync = "";
    String strUploadTypeNm = "";
    String strRefID = "";
    String strMessage = "";
    String strFileIds = "";
    String strFileTypeList = "";
    String strFileNm = "";
    String strSbListId = "";
    String strSetGroupType = ""; //PC-3631 Corporate Files Upload on SpineIT - Globus App

    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmPDFileUploadForm));

    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    log.debug("strOpt= " + strOpt + " ----strAction= " + strAction);

    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_FILE_UPD_VIEW");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPDFileUploadForm.setFileUploadSubmitAcc(accessFlag);

    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "FILE_VOID");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPDFileUploadForm.setAccessFileVoid(accessFlag);

    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "DCOED_DOC");
    strDcoedFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPDFileUploadForm.setAccessDcoed(strDcoedFlag);

    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "NONDCOED_DOC");
    strNonDcoedFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPDFileUploadForm.setAccessNonDcoed(strNonDcoedFlag);


    
    hmRule.put("RULEID", "FILEFMT");
    gmPDFileUploadForm.setHmFileFmt(gmCommonBean.fetchTransactionRules(hmRule));

    gmPDFileUploadForm.setAlUploadTypes(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("MREFTY")));
    gmPDFileUploadForm.setAlFileType(getArtifactList(strDcoedFlag, strNonDcoedFlag));
    gmPDFileUploadForm.setAlSubType(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("FSBTYP")));
    

    if (strOpt.equals("SaveUploadedFiles")) {
      log.debug("SaveUploadedFiles hashMap *****" + hmParam);

      strInputstring = GmCommonClass.parseNull(gmPDFileUploadForm.getFileInputStr());
      if (!strInputstring.equals("")) {
        // saveUploadBulkInfo is used to save uploaded files details
        gmCommonUploadBean.saveUploadBulkInfo(hmParam);
      }
      strOpt = "FETCHUPLOADFILES";
    }

    if (strAction.equals("OnReloadPart") && !strOpt.equals("UploadFiles")) {
      strPartListId = GmCommonClass.parseNull(gmPDFileUploadForm.getPartListId());
      if (!strPartListId.equals("")) {
        // fetchPartNumberInfo is used to fetch the part detail based on part number
        hmPartInfo = gmProdCatBean.fetchPartNumberInfo(strPartListId);
        strPartNumId = GmCommonClass.parseNull((String) hmPartInfo.get("PNUM"));
        strPartNumDesc = GmCommonClass.parseNull((String) hmPartInfo.get("PDESC"));
        strOpt = "FETCHUPLOADFILES";
      }
    }

    if (strAction.equals("OnReloadSet") && !strOpt.equals("UploadFiles")) {
      strSetListId = GmCommonClass.parseNull(gmPDFileUploadForm.getSetListId());
      if (!strSetListId.equals("")) {
        // fetchSetInfo is used to fetch the set detail based on setId
        hmSetInfo = gmProdCatBean.fetchSetInfo(strSetListId);
        strSetId = GmCommonClass.parseNull((String) hmSetInfo.get("SETID"));
        strSetDesc = GmCommonClass.parseNull((String) hmSetInfo.get("SETNM"));
        strOpt = "FETCHUPLOADFILES";
      }
    }
    
    if (strAction.equals("OnReloadSetBundle") && !strOpt.equals("UploadFiles")) {  	
    	alSystemList = GmCommonClass.parseNullArrayList(gmProdCatBean.fetchSetBundleInfo());
    	gmPDFileUploadForm.setAlSysTypeList(alSystemList);
      }

    if (strAction.equals("OnReloadSystemList") || strOpt.equals("FetchVoidUploadFiles")
        || strOpt.equals("UploadFiles")) {
      // loadSystemsList is used to fetch all the system names for dropdown
    	//PC-3631 Corporate Files Upload on SpineIT - Globus App - add strSetGroupType parameter
    	strSetGroupType = GmCommonClass.parseNull(gmPDFileUploadForm.getStrSetGroupType());
    	alSystemList = GmCommonClass.parseNullArrayList(gmSalesReportBean.loadProjectGroup(strSetGroupType));
      gmPDFileUploadForm.setAlSysTypeList(alSystemList);
    }

    if (strAction.equals("OnReloadProjectList") || strOpt.equals("FetchVoidUploadFiles")
        || strOpt.equals("UploadFiles")) {
      // fetchProjectList is used to fetch all the project names for dropdown
      alProjectList = GmCommonClass.parseNullArrayList(gmProjectInfoBean.fetchProjectList());
      gmPDFileUploadForm.setAlPrjTypeList(alProjectList);
    }

    if (strAction.equals("OnReloadGroupList") || strOpt.equals("FetchVoidUploadFiles")
        || strOpt.equals("UploadFiles")) {
      hmParam.put("STROPT", "40045");
      // loadDemandGroupList is used to fetch all the groups names for dropdown
      alGroupList =
          GmCommonClass.parseNullArrayList(gmDemandSetupBean.loadDemandGroupList(hmParam));
      gmPDFileUploadForm.setAlGrpTypeList(alGroupList);
    }

    if (strOpt.equals("UploadFiles")) {
      /*
       * Here we going to upload files at some location into the system and once files are upload we
       * are going to store thatinformation into the database table. path for PRODCATALOGUPLOADDIR
       * is available into constant properties file.
       */
      strUploadTypeId = GmCommonClass.parseNull(gmPDFileUploadForm.getUploadTypeId());
      strUploadTypeNm =
          GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(gmPDFileUploadForm
              .getUploadTypeId()));
      strRefID = GmCommonClass.parseNull(gmPDFileUploadForm.getRefId());
      log.debug("strUploadHomeDir::" + strUploadHomeDir + "::strUploadTypeNm::" + strUploadTypeNm
          + "::strRefID::" + strRefID);
      ArrayList alFileNmLists = gmPDFileUploadForm.getListFile();
      strFileTypeList = gmPDFileUploadForm.getFileTypeList();
    

      /*
       * convertStringToList method is used to get all the values from the input string for
       * FileType, FileTitle and FileName
       */
      ArrayList alFileTypeList = convertInputStringToList(strFileTypeList);

      /*
       * In the below loop we are itrating the fileType, fileTitle, fileName from the screen and
       * getting the values upload the files at perticular loaction into the system and also store
       * the information into database table.
       */
      for (int i = 0; i < alFileNmLists.size(); i++) {// Get file from the form
        FormFile formFile = (FormFile) alFileNmLists.get(i);
        strFileNm = GmCommonClass.parseNull(formFile.getFileName());
        if (strFileNm.equals("")) { // alFileNmLists having empty values.So, to continue this loop
                                    // added continue here
          continue;
        }

        int fileSize = formFile.getFileSize();
        String strFileSize = Integer.toString(fileSize);
        String strFileType = "";
        String strFileTitle = "";
        String strFileId = "";
        String strPartNum="";

        for (int j = 0; j < alFileTypeList.size(); j++) {
          HashMap hmInputParams = GmCommonClass.parseNullHashMap((HashMap) alFileTypeList.get(j));
          String strFileNam = GmCommonClass.parseNull((String) hmInputParams.get("FILENAME"));
          
          
          if (strFileNam.equals(strFileNm)) {
            strFileType = GmCommonClass.parseNull((String) hmInputParams.get("TYPE"));
            strFileTitle = GmCommonClass.parseNull((String) hmInputParams.get("TITLE"));
            strPartNum = GmCommonClass.parseNull((String) hmInputParams.get("PARTNUMBER"));
            log.debug("strPartNum-------- "+strPartNum);
            strSubType = GmCommonClass.parseNull((String) hmInputParams.get("SUBTYPE"));
            strTitleType = GmCommonClass.parseNull((String) hmInputParams.get("TITLETYPE"));
            strFileId = GmCommonClass.parseNull((String) hmInputParams.get("FILEID"));
          }
        }
        strArtifactNm = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strFileType));
        strSubTypeNm = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strSubType));
        alFilePath = new ArrayList();
        alFilePath.add(strUploadTypeNm);
        alFilePath.add(strRefID);
        alFilePath.add(strFileType);
        alFilePath.add(strSubType);

        hmFileProp.put("PATHPROPERTY", "PRODCATALOGUPLOADDIR");
        hmFileProp.put("SEPARATOR", "\\");
        hmFileProp.put("APPENDPATH", alFilePath);
        strUploadSubDir = gmProdCatUtil.getFilePath(hmFileProp);
        // uploadFile method will upload the files into the system based on defined path
        gmCommonUploadBean.uploadFile(formFile, strUploadSubDir, strUploadSubDir + strFileNm);

        hmValues.put("FILEID", strFileId);
        hmValues.put("REFID", strRefID);
        hmValues.put("REFTYPE", strFileType);
        hmValues.put("FILENAME", strFileNm);
        hmValues.put("USERID", strUserID);
        hmValues.put("FILETITLE", strFileTitle);
        hmValues.put("FILETYPE", strFileType);
        hmValues.put("FILESIZE", strFileSize);
        hmValues.put("FILESEQNO", "");
        hmValues.put("REFGRP", strUploadTypeId);
        hmValues.put("SUBTYPE", strSubType);
        hmValues.put("TITLETYPE", strTitleType);
        hmValues.put("PARTNUMBER", strPartNum.toUpperCase());
        log.debug("strPartNum-UPPERCASE----- "+strPartNum);
        // saveUploadUniqueInfo method will store the information for the files into database table.
        strFileId = gmCommonUploadBean.saveUploadUniqueInfo(hmValues);
        strFileIds += strFileId + ",";
        strMessage = "File Uploaded Successfully";
        gmPDFileUploadForm.setMessage(strMessage);

      }
      // PMT-53131 due this comment is removed
      strConsumFileSync =
          GmCommonClass.parseNull(GmCommonClass
              .getString("BATCH_CONSUMER_MARKETING_COLL_FILE_SYNC"));
      if (strConsumFileSync.equals("YES")) {
        hmMessageObj.put("FILEIDS", strFileIds);
        hmMessageObj.put("ACTION", "UPLOAD");
        
        // PC-4295: spineIT upload details added to cloud
        hmMessageObj.put("USERID", strUserID);
        
        hmParamJob.put("CONSUMERCLASS",
            GmCommonClass.getString("BATCH_CONSUMER_MARKETING_COLL_FILE_SYNC_CLASS"));
        hmParamJob.put("MESSAGEOBJECT", hmMessageObj);
        // saveConsumerJms is used to send a message to JMS to process the Upload Files.
        gmShareEngineTransBean.saveConsumerJms(hmParamJob);
      }
      strOpt = "FETCHUPLOADFILES";
    }

    if (strOpt.equals("Download")) {
      File file = null;
      String strUploadString = "";
      FileInputStream istr = null;
      String strFileName = "";
      String strFileId = GmCommonClass.parseNull(request.getParameter("fileID"));
      String strFileGroupNm = GmCommonClass.parseNull(request.getParameter("fileGroupNM"));
      String strTypeId = GmCommonClass.parseNull(request.getParameter("typeID"));
      strArtifactNm = GmCommonClass.parseNull(request.getParameter("artifactNm"));
      strSubTypeNm = GmCommonClass.parseNull(request.getParameter("subTypeNm"));
      alFilePath = new ArrayList();
      alFilePath.add(strFileGroupNm);
      alFilePath.add(strTypeId);
      alFilePath.add(strArtifactNm);
      alFilePath.add(strSubTypeNm);

      hmFileProp.put("PATHPROPERTY", "PRODCATALOGUPLOADDIR");
      hmFileProp.put("SEPARATOR", "\\");
      hmFileProp.put("APPENDPATH", alFilePath);

      strUploadHomeDir = gmProdCatUtil.getFilePath(hmFileProp);
      strFileName = gmCommonUploadBean.fetchFileName(strFileId);
      ServletOutputStream out = response.getOutputStream();
      response.setContentType("application/binary");
      response.setHeader("Content-disposition", "attachment;filename=" + strFileName);
      log.debug("file name......" + strUploadHomeDir + strFileName);
      try {
        file = new File(strUploadHomeDir + strFileName);
        istr = new FileInputStream(file);
      } catch (FileNotFoundException ffe) {
        log.debug("CATCH EXE" + ffe);
        response.sendRedirect(response.encodeRedirectUrl(strErrorFileToDispatch));
        throw new AppError("", "20676");
      }
      response.setContentLength((int) file.length());
      int curByte = -1;
      while ((curByte = istr.read()) != -1)
        out.write(curByte);
      istr.close();
      out.flush();
      out.close();
    }
    if (strOpt.equals("previewzip")) {
      PrintWriter pw = response.getWriter();
      pw.write(previewZipFile(request, response));
      response.setContentType("text/plain");
      pw.flush();
      return null;
    }

    if (strOpt.equals("FETCHUPLOADFILES")) {
      strUploadTypeId = GmCommonClass.parseNull(gmPDFileUploadForm.getUploadTypeId());

      if (strUploadTypeId.equals("103090")) {
        strListId = GmCommonClass.parseNull(gmPDFileUploadForm.getPartListId());
        hmPartInfo = gmProdCatBean.fetchPartNumberInfo(strListId);
        strPartNumId = GmCommonClass.parseNull((String) hmPartInfo.get("PNUM"));
        strPartNumDesc = GmCommonClass.parseNull((String) hmPartInfo.get("PDESC"));
      } else if (strUploadTypeId.equals("103091")) {
        strListId = GmCommonClass.parseNull(gmPDFileUploadForm.getSetListId());
        hmSetInfo = gmProdCatBean.fetchSetInfo(strListId);
        strSetId = GmCommonClass.parseNull((String) hmSetInfo.get("SETID"));
        strSetDesc = GmCommonClass.parseNull((String) hmSetInfo.get("SETNM"));
      } else if (strUploadTypeId.equals("26240459")) {  
    	strListId = GmCommonClass.parseNull(gmPDFileUploadForm.getSbListId());
      } else if (strUploadTypeId.equals("103092")) {
        strListId = GmCommonClass.parseNull(gmPDFileUploadForm.getSysListId());
      } else if (strUploadTypeId.equals("103093")) {
        strListId = GmCommonClass.parseNull(gmPDFileUploadForm.getPrjListId());
      } else if (strUploadTypeId.equals("103094")) {
        strListId = GmCommonClass.parseNull(gmPDFileUploadForm.getGrpListId());
      }

      if (!strUploadTypeId.equals("")) {
        if (!strListId.equals("")) {
          hmValues.put("LISTID", strListId);
          hmValues.put("UPLOADTYPEID", strUploadTypeId);
          hmValues.put("DCOEDFL", strDcoedFlag);
          hmValues.put("NONDCOEDFL", strNonDcoedFlag);
          fetchUploadedFiles(mapping, form, request, response, hmValues);
        }
      }

      gmPDFileUploadForm.setUploadTypeId(strUploadTypeId);
      gmPDFileUploadForm.setPartListId(strListId);
      gmPDFileUploadForm.setPartNumId(strPartNumId);
      gmPDFileUploadForm.setPartNumDesc(strPartNumDesc);
      gmPDFileUploadForm.setSetListId(strListId);
      gmPDFileUploadForm.setSetId(strSetId);
      gmPDFileUploadForm.setSetDesc(strSetDesc);
      gmPDFileUploadForm.setSysListId(strListId);
      gmPDFileUploadForm.setPrjListId(strListId);
      gmPDFileUploadForm.setGrpListId(strListId);
    }


    gmPDFileUploadForm.setStrOpt(strOpt);
    return mapping.findForward(strForwardPage);
  }

  private void fetchUploadedFiles(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response, HashMap hmParam) throws AppError {

    GmPDFileUploadForm gmPDFileUploadForm = (GmPDFileUploadForm) form;
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());

    ArrayList alReport = new ArrayList();

    // Getting Template column names from properties files
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    String strRBPath = "properties.labels.common.GmUploadedFileInclude";
    hmParam.put("COMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("RBPATH", strRBPath);


    alReport =
        GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo("",
            hmParam.get("LISTID").toString(), hmParam.get("UPLOADTYPEID").toString()));
    log.debug("alReport === " + alReport.size());
    gmPDFileUploadForm.setIntResultSize(alReport.size());
    hmParam.put("ALRESULT", alReport);
    String strXmlGridData = generateOutPut(hmParam);
    gmPDFileUploadForm.setGridXmlData(strXmlGridData);

  }

  private String generateOutPut(HashMap hmParam) throws AppError {

    String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"));
    String strRBPath = GmCommonClass.parseNull((String) hmParam.get("RBPATH"));

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strRBPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("common/templates");
    templateUtil.setTemplateName("GmUploadedFileInclude.vm");
    return templateUtil.generateOutput();
  }

  /*
   * convertInputStringToList method is used to get all the values from the input string for
   * FileType, FileTitle and FileName inputSting like:
   * FileType^FileTitle^FileName|FileType^FileTitle^FileNam|
   */
  private ArrayList convertInputStringToList(String strInputStr) throws AppError {
	  
	  log.debug("inside the convertInputStringToList");

    String strTemp, strType, strTitle, strFileName = "";
    String strSubType = "";
    String strTitleType = "";
    String strFileId = "";
    String[] strInnerTok;
    String strPartNumber = "";
    int intInputSize;
    StringTokenizer strTok = new StringTokenizer(strInputStr, "|");

    ArrayList alList = new ArrayList();
    HashMap hmParam = null;

    while (strTok.hasMoreTokens()) {
      hmParam = new HashMap();
      strTemp = strTok.nextToken();
      strInnerTok = strTemp.split("\\^");
      intInputSize = strInnerTok.length;
      strType = strInnerTok[0];
      strTitle = strInnerTok[1];
      strFileName = strInnerTok[2];
      strSubType = strInnerTok[3];
     
     
      // Title Type will come when DCOed doc upload/edit
      // FileId will come when edit existing file
      strTitleType = (intInputSize > 4) ? strInnerTok[4] : "";
      strFileId = (intInputSize > 5) ? strInnerTok[5] : "";
      strPartNumber = (intInputSize > 6) ? strInnerTok[6] : "";
      
      log.debug("strPartNumber  value------------  "+strPartNumber);

      hmParam.put("TYPE", strType);
      hmParam.put("TITLE", strTitle);
      hmParam.put("FILENAME", strFileName);
      hmParam.put("SUBTYPE", strSubType);
      hmParam.put("TITLETYPE", strTitleType);
      hmParam.put("FILEID", strFileId);
      hmParam.put("PARTNUMBER", strPartNumber);
      alList.add(hmParam);
    }
    return alList;
  }

  /*
   * getArtifactList method is used to get the artifact list base on DCOed Document and Non DCOed
   * Document access.
   */
  private ArrayList getArtifactList(String strDcoedFlag, String strNonDcoedFlag) throws AppError {
    ArrayList alTemp = new ArrayList();
    if (strDcoedFlag.equals("Y") && strNonDcoedFlag.equals("Y")) {
      alTemp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FTYPE"));
    } else if (strDcoedFlag.equals("Y")) {
      alTemp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FTYPE", "103114"));// Image,Video,DCOed
                                                                                              // doc.
    } else if (strNonDcoedFlag.equals("Y")) {
      alTemp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FTYPE", "4000441")); // Image,Video,Non
                                                                                                // DCOed
                                                                                                // doc.
    } else {
      alTemp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FTYPE", "103112")); // Image,Video.
    }
    return alTemp;
  }

  /*
   * previewZipFile method is used to unzip the zip file and get sample filenames from unzip folder
   */
  private String previewZipFile(HttpServletRequest request, HttpServletResponse response)
      throws AppError {

    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    ArrayList alFilePath = new ArrayList();
    HashMap hmFileProp = new HashMap();
    String strFileName = "";

    String strUploadHomeDir = "";
    String strFileId = GmCommonClass.parseNull(request.getParameter("fileID"));
    String strFileGroupNm = GmCommonClass.parseNull(request.getParameter("fileGroupNM"));
    String strTypeId = GmCommonClass.parseNull(request.getParameter("typeID"));
    String strArtifactNm = GmCommonClass.parseNull(request.getParameter("artifactNm"));
    String strSubTypeNm = GmCommonClass.parseNull(request.getParameter("subTypeNm"));
    alFilePath = new ArrayList();
    alFilePath.add(strFileGroupNm);
    alFilePath.add(strTypeId);
    alFilePath.add(strArtifactNm);
    alFilePath.add(strSubTypeNm);

    hmFileProp.put("PATHPROPERTY", "PRODCATALOGUPLOADDIR");
    hmFileProp.put("SEPARATOR", "\\");
    hmFileProp.put("APPENDPATH", alFilePath);

    strUploadHomeDir = gmProdCatUtil.getFilePath(hmFileProp);
    strFileName = gmCommonUploadBean.fetchFileName(strFileId);

    return GmCommonClass.parseNull(gmCommonUploadBean.processZip(strUploadHomeDir, strFileName));
  }
}
