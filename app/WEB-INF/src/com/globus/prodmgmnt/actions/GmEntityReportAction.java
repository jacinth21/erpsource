/**
 *  FileName : GmEntityReportAction.java 
 *  Author   : T S Ramachandiran
 *  PMT		 : 40410-region mapping info on popup window
 */
package com.globus.prodmgmnt.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.forms.GmSystemDetailForm;
import com.globus.prodmgmnt.beans.GmEntityReportBean;

/**
 * GmEntityReportAction class for fetching company details
 */
public class GmEntityReportAction extends GmDispatchAction {

	/**
	 * This used to get the instance of this class for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * loadEntityReport - used to redirect request to another view page
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadEntityReport(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) form;
		gmSystemDetailForm.loadSessionParameters(request);
		return mapping.findForward("gmentityreport");
	}

	/**
	 * fetchEntityDtls - used to fetch company details based on selected region
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchEntityDtls(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) form;
		gmSystemDetailForm.loadSessionParameters(request);
		GmEntityReportBean gmEntityReportBean = new GmEntityReportBean(
				getGmDataStoreVO());

		String strRegionId = "";
		strRegionId = GmCommonClass.parseNull(gmSystemDetailForm.getRegionId());
		String strCompanyDetailsJsonString = GmCommonClass
				.parseNull(gmEntityReportBean.fetchEntityDtls(strRegionId));

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strCompanyDetailsJsonString.equals("")) {
			pw.write(strCompanyDetailsJsonString);
		}
		pw.flush();
		return null;

	}

}
