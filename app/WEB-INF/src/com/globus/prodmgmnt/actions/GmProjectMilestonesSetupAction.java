/**
 * FileName    : GmProjectMilestonesSetupAction.java 
 * Description :
 * Author      : rshah
 * Date        : Feb 5, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.forms.GmProjectMilestonesSetupForm;
import com.globus.prodmgmnt.beans.GmProjectInfoBean;
import com.globus.prodmgmnt.beans.GmProjectTransBean;

/**
 * @author rshah
 *
 */
public class GmProjectMilestonesSetupAction extends GmAction{
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmProjectMilestonesSetupForm gmProjectMilestonesSetupForm = (GmProjectMilestonesSetupForm) form;
		gmProjectMilestonesSetupForm.loadSessionParameters(request);

		GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean();
		GmProjectTransBean gmProjectTransBean = new GmProjectTransBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmProjectMilestonesSetupForm.getStrOpt();
		String strPrjId = gmProjectMilestonesSetupForm.getPrjId();
		log.debug("1@...2@...."+strOpt+"......"+strPrjId+"............"+gmProjectMilestonesSetupForm.getHinputString());
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmProjectMilestonesSetupForm);

		if (strOpt.equals("save")) {
			gmProjectTransBean.saveProjectMilestoneDetails(hmParam);			
		} 
		if (!strPrjId.equals("")) {
			gmProjectMilestonesSetupForm.setAlMilestoneList(GmCommonClass.parseNullArrayList(gmProjectInfoBean.fetchProjectMilestoneDetails(strPrjId)));
		}
		
		
		return mapping.findForward("success");
	}
}
