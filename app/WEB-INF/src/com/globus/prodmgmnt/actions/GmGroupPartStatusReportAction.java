/**
 * FileName : GmCustDashBoardDispatchAction.java Description : Author : tchandure Date : Jun 4, 2008
 * Copyright : Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;


import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmProdReportBean;
import com.globus.prodmgmnt.forms.GmGroupPartStatusReportForm;



public class GmGroupPartStatusReportAction extends GmDispatchAction {


  public ActionForward groupPartStatusReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmGroupPartStatusReportForm gmGroupPartStatusReportForm = (GmGroupPartStatusReportForm) form;
    gmGroupPartStatusReportForm.loadSessionParameters(request);

    GmProdReportBean gmProdReportBean = new GmProdReportBean();

    HashMap hmReturn = new HashMap();
    HashMap hmParams = new HashMap();

    hmParams = GmCommonClass.getHashMapFromForm(gmGroupPartStatusReportForm);

    hmReturn = gmProdReportBean.fetchGroupPartStatusReport(hmParams);

    GmCommonClass.getFormFromHashMap(gmGroupPartStatusReportForm, hmReturn);

    return mapping.findForward("GmGroupPartStatusReport");
  }

  public ActionForward partStatusReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmGroupPartStatusReportForm gmGroupPartStatusReportForm = (GmGroupPartStatusReportForm) form;
    gmGroupPartStatusReportForm.loadSessionParameters(request);
    log.debug(" Inside partStatusReport vaklues "
        + GmCommonClass.getHashMapFromForm(gmGroupPartStatusReportForm));
    GmProdReportBean gmProdReportBean = new GmProdReportBean();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    ArrayList alReturn = new ArrayList();
    HashMap hmParams = new HashMap();

    hmParams = GmCommonClass.getHashMapFromForm(gmGroupPartStatusReportForm);

    alReturn = gmProdReportBean.fetchPartStatusReport(hmParams);
    gmGroupPartStatusReportForm.setGridXmlData(getXmlGridData(alReturn, strSessCompanyLocale));
    log.debug(" Grid Data for Part Status  " + getXmlGridData(alReturn, strSessCompanyLocale));
    return mapping.findForward("GmPartStatusReport");

  }

  public String getXmlGridData(ArrayList alParam, String strSessCompanyLocale) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmPartStatusReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPartStatusReport.vm");
    templateUtil.setTemplateSubDir("prodmgmnt/templates");
    return templateUtil.generateOutput();
  }
}
