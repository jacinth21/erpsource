package com.globus.prodmgmnt.actions;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProdReportBean;


public class GmPDReportAction extends GmDispatchAction {


  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.



  public ActionForward multiSetPartReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    RowSetDynaClass rdMSPReport = null;
    instantiate(request, response);
    DynaActionForm gmMultiSetPartReportForm = (DynaActionForm) form;
    GmProdReportBean gmProdReportBean = new GmProdReportBean();
    log.debug(" inside multiSetPartReport ");
    try {
      rdMSPReport = gmProdReportBean.fetchMultiSetPartReport();
      log.debug(" size of result " + rdMSPReport.getRows().size());
      // request.setAttribute("results",rdMSPReport);
      gmMultiSetPartReportForm.set("ldtResult", rdMSPReport.getRows());

    } catch (Exception e) {
      e.printStackTrace();
      log.debug("Exception " + e);
    }

    return mapping.findForward("success");
  }
}
