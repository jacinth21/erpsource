/**
 * FileName : GmSystemDetailAction.java Description : Author : Elango
 */
package com.globus.prodmgmnt.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.beans.GmSystemBean;
import com.globus.prodmgmnt.forms.GmSystemDetailForm;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.common.db.GmCacheManager;
/**
 * @author Elango
 * 
 */
public class GmSystemDetailAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) form;
    gmSystemDetailForm.loadSessionParameters(request);

    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String accessFlag = "";
    String strForward = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();

    String strOpt = GmCommonClass.parseNull(gmSystemDetailForm.getStrOpt());
    String strSysId = GmCommonClass.parseNull(gmSystemDetailForm.getSetId());
    String strPartyId = GmCommonClass.parseNull(gmSystemDetailForm.getSessPartyId()); // Need to
                                                                                      // pass this
                                                                                      // to
                                                                                      // getAccessPermissions


    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_SYSTEM_SUBMIT");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmSystemDetailForm.setSetSetupSubmitAcc(accessFlag);
    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_SYSTEM_VOID");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmSystemDetailForm.setSetSetupVoidAcc(accessFlag);
    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_SYSTEM_PREVIEW");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmSystemDetailForm.setSetSetupPreviewAcc(accessFlag);
    
    
    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "SYSTEM_PUBLISH_ACC");   // Allow Security Event to Publish Button in PMT-37488
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if(!accessFlag.equals("Y")){
    	gmSystemDetailForm.setSetSysPublishAcc("true");  
    }

     hmParam = GmCommonClass.getHashMapFromForm(gmSystemDetailForm);
    // After saving the SYSTEM details, need to redirect thecontainer page.
    if (strOpt.equals("save")) {
      String strSysID = GmCommonClass.parseNull(saveSystemDetail(gmSystemDetailForm, hmParam));
      strForward = "/gmSystemSetup.do?strOpt=fetchcontainer&setId=" + strSysID;
      // ActionRedirect actionRedirect = new ActionRedirect(strForward);
      //remove system list in redis
      GmCacheManager gmCacheManager = new GmCacheManager();
      gmCacheManager.removePatternKeys("SYSTEM_LIST");

      return actionRedirect(strForward, request);
    }
    if (strOpt.equals("") || strOpt.equals("fetchcontainer") || strOpt.equals("fetch")) {
      fetchSystemDetail(gmSystemDetailForm, hmParam);
    }
    if (strOpt.equals("systemCompanyMap")) {
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      fetchSystemCompanyMap(gmSystemDetailForm, hmParam);
    }
    // Below condition is used to validate the system name by AJAX call from the screen.
    if (strOpt.equals("validateSystem")) {
      String strSetNm = GmCommonClass.parseNull(gmSystemDetailForm.getSetNm());
      GmSystemBean gmSystemBean = new GmSystemBean(getGmDataStoreVO());
      String strSystemStatus = gmSystemBean.validateSystem(strSetNm);
      response.setContentType("text/xml");
      if (!strSystemStatus.equals("")) {
        PrintWriter pw = response.getWriter();
        pw.write(strSystemStatus);
        pw.flush();
      }
      return null;
    }
    // When the strOpt is NULL, need to forward the page to default Container.
    if (strOpt.equals("")) {
      strOpt = "default";
    }

    log.debug("Forwarding to = >" + strOpt + "<");
    return mapping.findForward(strOpt);
  }

  // fetchSystemDetail - This method is used to fetch System Setup details.
  private void fetchSystemDetail(GmSystemDetailForm gmSystemDetailForm, HashMap hmParam)
      throws AppError {

    GmSystemBean gmSystemBean = new GmSystemBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

    HashMap hmValues = new HashMap();
    ArrayList alAttrValues = new ArrayList();
    ArrayList alDivisionList = new ArrayList();
    String strSystemId = GmCommonClass.parseNull(gmSystemDetailForm.getSetId());

    String strGroupAttrId = "103116"; // GmCommonClass.getCodeID("Market/Group","PRATTR");
    String strSegAttrId = "103117"; // GmCommonClass.getCodeID("Segment/Category","PRATTR");
    String strTechAttrId = "103118"; // GmCommonClass.getCodeID("Technique","PRATTR");
    String strPubAttrId = ""; // PMT-46227 code id removed
    //PC-3631 Corporate Files Upload on SpineIT - Globus App 
    String strSetGroupType = "1600,26241209";
    gmSystemDetailForm.setGroupAttrId(strGroupAttrId);
    gmSystemDetailForm.setSegmentAttrId(strSegAttrId);
    gmSystemDetailForm.setTechniqueAttrId(strTechAttrId);
    gmSystemDetailForm.setPublishInAttrId(strPubAttrId);

    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
    gmSystemDetailForm.setAlDivisionList(alDivisionList);

    if (!strSystemId.equals("")) {
      // Get the SYSTEM Details for the selected SYSTEM ID.
      hmValues = gmProjectBean.loadEditSetMaster(strSystemId);
      gmSystemDetailForm =
          (GmSystemDetailForm) GmCommonClass.getFormFromHashMap(gmSystemDetailForm, hmValues);
      // Get the SYSTEM Group Attr Details for the selected SYSTEM ID.
      alAttrValues = gmSystemBean.fetchSetAttribute(strSystemId, strGroupAttrId);
      gmSystemDetailForm.sethGroupIds(GmCommonClass.parseNull(GmCommonClass
          .createStringFromListbyKey(alAttrValues, "ATTRVAL")));
      // Get the SYSTEM Segment Attr Details for the selected SYSTEM ID.
      alAttrValues = gmSystemBean.fetchSetAttribute(strSystemId, strSegAttrId);
      gmSystemDetailForm.sethSegmentIds(GmCommonClass.parseNull(GmCommonClass
          .createStringFromListbyKey(alAttrValues, "ATTRVAL")));
      // Get the SYSTEM Technique Attr Details for the selected SYSTEM ID.
      alAttrValues = gmSystemBean.fetchSetAttribute(strSystemId, strTechAttrId);
      gmSystemDetailForm.sethTechniqueIds(GmCommonClass.parseNull(GmCommonClass
          .createStringFromListbyKey(alAttrValues, "ATTRVAL")));
      // Get the SYSTEM Publish in Attr Details for the selected SYSTEM ID.
      alAttrValues = gmSystemBean.fetchSetAttribute(strSystemId, strPubAttrId);
      gmSystemDetailForm.sethPublishInIds(GmCommonClass.parseNull(GmCommonClass
          .createStringFromListbyKey(alAttrValues, "ATTRVAL")));
    }
    // Get the SYSTEM List Dropdown Details
    //PC-3631 Corporate Files Upload on SpineIT - Globus App - add strSetGroupType parameter
    gmSystemDetailForm.setAlSystemList(GmCommonClass.parseNullArrayList(gmSalesReportBean
        .loadProjectGroup(strSetGroupType)));
    gmSystemDetailForm.setAlSystemGrpList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("GROUP")));
    gmSystemDetailForm.setAlSystemSgmtList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("SEGMT")));
    gmSystemDetailForm.setAlSystemTechList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("TECHQ")));
    gmSystemDetailForm.setAlSystemPubToList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("PUBTO")));
    // Get the SYSTEM Setup Comments Log Details
    gmSystemDetailForm.setAlLogReasons(gmCommonBean.getLog(strSystemId, "4000413"));
  }

  // saveSystemDetail - This method is used to save System Setup details.
  private String saveSystemDetail(GmSystemDetailForm gmSystemDetailForm, HashMap hmParam)
      throws AppError {
    GmSystemBean gmSystemBean = new GmSystemBean(getGmDataStoreVO());
    String strSetIdFromDb = gmSystemBean.saveSystemDetail(hmParam); // Rename the name
    gmSystemDetailForm.setSetId(strSetIdFromDb);
    return strSetIdFromDb;
  }

  /**
   * fetchSystemCompanyMap This method is used to fetch the System Company Mapping details for a
   * particular project
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */
  private void fetchSystemCompanyMap(GmSystemDetailForm gmSystemDetailForm, HashMap hmParam)
      throws AppError {

    GmSystemBean gmSystemBean = new GmSystemBean(getGmDataStoreVO());

    HashMap hmTemplateParam = new HashMap();
    ArrayList alSysCompList = new ArrayList();
    String strSystemId = gmSystemDetailForm.getSetId();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    hmParam.put("SYSID", strSystemId);
    hmParam.put("DATEFMT", getGmDataStoreVO().getCmpdfmt());

    alSysCompList = GmCommonClass.parseNullArrayList(gmSystemBean.fetchSystemCompanyMap(hmParam));
    hmTemplateParam.put("DATA_MAP", hmParam);
    hmTemplateParam.put("DATA_LIST", alSysCompList);
    hmTemplateParam.put("TEMPLATE_NAME", "GmSystemCompanyMap.vm");
    hmTemplateParam.put("TEMPLATE_SUB_DIR", "prodmgmnt/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmTemplateParam.put("VMFILEPATH", "properties.labels.prodmgmnt.GmSystemCompanyMap");
    // XML String
    String strXmlString = GmCommonClass.getXmlGridData(hmTemplateParam);
    gmSystemDetailForm.setGridXmlData(strXmlString);
  }

}
