/**
 * FileName    : GmStaffingCountSetupAction.java 
 * Description :
 * Author      : rshah
 * Date        : Feb 10, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmStaffingInfoBean;
import com.globus.prodmgmnt.beans.GmStaffingTransBean;
import com.globus.prodmgmnt.forms.GmStaffingDetailsForm;

/**
 * @author rshah
 *
 */
public class GmStaffingCountSetupAction extends GmAction{
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmStaffingDetailsForm gmStaffingDetailsForm = (GmStaffingDetailsForm) form;
		gmStaffingDetailsForm.loadSessionParameters(request);

		GmStaffingInfoBean gmStaffingInfoBean = new GmStaffingInfoBean();
		GmStaffingTransBean gmStaffingTransBean = new GmStaffingTransBean();
		//GmProjectTransBean gmProjectTransBean = new GmProjectTransBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmStaffingDetailsForm.getStrOpt();
		String strPrjId = gmStaffingDetailsForm.getPrjId();
		log.debug("1@...3@...."+strOpt+"......"+strPrjId+"............");
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmStaffingDetailsForm);

		if (strOpt.equals("save")) {
			gmStaffingTransBean.saveStaffingCountDetails(hmParam);			
		} 
		if (!strPrjId.equals("")) {
			gmStaffingDetailsForm.setAlStaffingCountDetails(GmCommonClass.parseNullArrayList(gmStaffingInfoBean.fetchStaffingCountDetails(strPrjId)));
		}
		
		
		return mapping.findForward("success");
	}
}
