/**
 * FileName    : GmProjectsReportAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectInfoBean;
import com.globus.prodmgmnt.forms.GmProjectsReportForm;

/**
 * @author sthadeshwar
 *
 */
public class GmProjectsReportAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		try {
			GmProjectsReportForm gmProjectsReportForm = (GmProjectsReportForm)form;
			gmProjectsReportForm.loadSessionParameters(request);
			GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean();
			GmCommonBean gmCommonBean = new GmCommonBean();
			
			ArrayList alMilestones = new ArrayList();
			
			HashMap hmParam = new HashMap();
			String strMilestones = "";
			String strOpt = gmProjectsReportForm.getStrOpt();
			log.debug("strOpt = " + strOpt);
			
			if(strOpt.equals("reload")) {
				String strSelection = gmProjectsReportForm.getStrSelection();
				String strSelectMilestones = "";
				log.debug("strSelection = " + strSelection);
				if(!strSelection.equals("0") && !strSelection.equals(""))
				{
					HashMap hmParams = new HashMap();
					hmParams.put("RULEID", strSelection);
					hmParams.put("RULEGROUP", "PRJRPTFILTER");
					strSelectMilestones = gmCommonBean.getRuleValue(hmParams);
					log.debug("strSelectMilestones = " + strSelectMilestones);
					//gmProjectsReportForm.setSelectedMilestones(new String[]{"92018", "92019"});
					gmProjectsReportForm.setSelectedMilestones(strSelectMilestones.split(","));
				}
				
			}
			
			if(strOpt.equals("fetch")) {
				
				String strProjectId = gmProjectsReportForm.getPrjId();
				strMilestones = GmCommonClass.createInputString(gmProjectsReportForm.getSelectedMilestones());
				
				log.debug("strMilestones = " + strMilestones);
				
				hmParam.put("PROJECTID", strProjectId);
				hmParam.put("MILESTONES", strMilestones);
				HashMap hmMap = gmProjectInfoBean.fetchProjectsReport(hmParam);
				request.setAttribute("HMVALUES", hmMap);
			}
			
			alMilestones = GmCommonClass.getCodeList("PROMS");
			
			gmProjectsReportForm.setAlPrjList(gmProjectInfoBean.fetchProjectList());
			gmProjectsReportForm.setAlMilestones(alMilestones);
			gmProjectsReportForm.setAlPrjGrpList(GmCommonClass.getCodeList("GROUP"));
			gmProjectsReportForm.setAlPrjSgmtList(GmCommonClass.getCodeList("SEGMT"));
			gmProjectsReportForm.setAlPrjStatus(GmCommonClass.getCodeList("PROJS"));
			gmProjectsReportForm.setAlSelection(GmCommonClass.getCodeList("PDFSL"));
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("success");
	}

}
