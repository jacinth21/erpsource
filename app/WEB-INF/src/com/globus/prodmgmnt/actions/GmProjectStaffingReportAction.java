/**
 * FileName    : GmProjectStaffingReportAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 6, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectInfoBean;
import com.globus.prodmgmnt.beans.GmStaffingInfoBean;
import com.globus.prodmgmnt.forms.GmStaffingDetailsForm;

/**
 * @author sthadeshwar
 *
 */
public class GmProjectStaffingReportAction extends GmAction {

Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		try {
			GmStaffingDetailsForm gmStaffingDetailsForm = (GmStaffingDetailsForm)form;
			gmStaffingDetailsForm.loadSessionParameters(request);
			
			GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean();
			GmStaffingInfoBean gmStaffingInfoBean = new GmStaffingInfoBean();
			
			HashMap hmParam = new HashMap();
			String strOpt = gmStaffingDetailsForm.getStrOpt();
			log.debug("strOpt = " + strOpt);
			
			if(strOpt.equals("fetch")) {
				hmParam = GmCommonClass.getHashMapFromForm(gmStaffingDetailsForm);
				log.debug("hmParam = " + hmParam);
				List lReturn = gmStaffingInfoBean.fetchProjectStaffingReport(hmParam).getRows();
				log.debug("lReturn = " + lReturn);
				gmStaffingDetailsForm.setLresult(lReturn);
			}
			
			gmStaffingDetailsForm.setAlPrjList(gmProjectInfoBean.fetchProjectList());
			gmStaffingDetailsForm.setAlRoles(GmCommonClass.getCodeList("GROLE,SROLE"));
			gmStaffingDetailsForm.setAlStatus(GmCommonClass.getCodeList("CSTAT,SSTAT"));
			gmStaffingDetailsForm.setAlPartyTypes(GmCommonClass.getCodeList("PTTYP", "92056"));
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward("success");
	}
}
