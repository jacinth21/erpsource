package com.globus.prodmgmnt.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmAutoCompleteForm;
import com.globus.prodmgmnt.beans.GmSetMapAutoCompleteRptBean;

public class GmSetMapAutoCompleteAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code


  /**
   * loadSetList - Fetch the loadSetList setup information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadSetNameList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    String strKey = "";
    String strType = "";
    String strSearchType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));

    String term = request.getParameter("term");
    strSearchKey = strSearchKey.equals("") ? term : strSearchKey;
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());
    strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
    strKey = GmCommonClass.parseNull((String) request.getAttribute("STRKEY"));
    HashMap hmParm = new HashMap();
    hmParm.put("TYPE", strType);
    hmParm.put("IMAXCOUNT", strMaxCount);
    hmParm.put("KEY", strKey);
    hmParm.put("SEARCHTYPE", GmCommonClass.parseNull((String) request.getParameter("searchType")));
    String strResults = gmSetMapAutoCompleteRptBean.loadSetListSetup(hmParm, strSearchKey);
    log.debug("loadSetNameList strResults  "+strResults);
    response.getWriter().write(strResults);
    return null;
  }

  /**
   * loadProjectNameList - Fetch the Project list information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadProjectNameList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    String strKey = "";
    String strType = "";
    String strSearchType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    String strProjId = GmCommonClass.parseNull((String) request.getAttribute("PROJID"));
    String term = request.getParameter("term");
    strSearchKey = strSearchKey.equals("") ? term : strSearchKey;
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());
    strKey = GmCommonClass.parseNull((String) request.getAttribute("STRKEY"));
    HashMap hmParm = new HashMap();
    hmParm.put("IMAXCOUNT", strMaxCount);
    hmParm.put("COLUMN", "ID");
    hmParm.put("STATUSID", "20301");// Approved
    hmParm.put("KEY", strKey);
    hmParm.put("PROJID",strProjId);
    hmParm.put("SEARCHTYPE", GmCommonClass.parseNull((String) request.getParameter("searchType")));
    String strResults = gmSetMapAutoCompleteRptBean.loadProjectNameList(hmParm, strSearchKey);
    response.getWriter().write(strResults);
    return null;
  }

  /**
   * loadInspectionSheetList - Fetch the inspection sheet list information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadInspectionSheetList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    String strKey = "";
    String strType = "";
    String strSearchType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));

    String term = request.getParameter("term");
    strSearchKey = strSearchKey.equals("") ? term : strSearchKey;
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());
    strKey = GmCommonClass.parseNull((String) request.getAttribute("STRKEY"));
    HashMap hmParm = new HashMap();
    hmParm.put("IMAXCOUNT", strMaxCount);
    hmParm.put("KEY", strKey);
    hmParm.put("ARTIFACTSTYPE", "91184");
    hmParm.put("SEARCHTYPE", GmCommonClass.parseNull((String) request.getParameter("searchType")));
    String strResults = gmSetMapAutoCompleteRptBean.loadInspectionSheetList(hmParm, strSearchKey);
    response.getWriter().write(strResults);
    return null;
  }
  
  /**
   * loadSetList - Fetch the loadSetList setup information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadSetIdNameList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    String strKey = "";
    String strType = "";
    String strSearchType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    String term = request.getParameter("term");
    strSearchKey = strSearchKey.equals("") ? term : strSearchKey;
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());
    strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
    strKey = GmCommonClass.parseNull((String) request.getAttribute("STRKEY"));
    HashMap hmParm = new HashMap();
    hmParm.put("TYPE", strType);
    hmParm.put("IMAXCOUNT", strMaxCount);
    hmParm.put("KEY", strKey);
    hmParm.put("SEARCHTYPE", GmCommonClass.parseNull((String) request.getParameter("searchType")));
    String strResults = gmSetMapAutoCompleteRptBean.loadSetIdListSetup(hmParm, strSearchKey);
    response.getWriter().write(strResults);
    return null;
  }

}
