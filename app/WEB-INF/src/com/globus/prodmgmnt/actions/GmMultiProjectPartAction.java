package com.globus.prodmgmnt.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.prodmgmnt.forms.GmMultiProjectPartForm;



public class GmMultiProjectPartAction extends GmAction
{
	private Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class
	
    public ActionForward execute(ActionMapping mapping,ActionForm form,
    							HttpServletRequest request,HttpServletResponse response)throws Exception {
        
        GmMultiProjectPartForm GmMultiProjectPartForm = (GmMultiProjectPartForm)form;
        GmMultiProjectPartForm.loadSessionParameters(request);         
        GmPartNumBean gmPartNumBean = new GmPartNumBean();
        
        String strAction = GmMultiProjectPartForm.getHaction();
       // String strPartInput = GmCommonClass.removeSpaces(GmMultiProjectPartForm.getPartNumbers());
        String strPartInput = GmMultiProjectPartForm.getPartNumbers().trim();
        String strSubCompFlg = GmCommonClass.removeSpaces(GmMultiProjectPartForm.getEnablesubcomponent());
        GmMultiProjectPartForm.setPartNumberSearch(GmCommonClass.getCodeList("LIKES"));       
        HashMap hmParam = new HashMap();
        String strUserID = "";
        List alPartNumDetails = new ArrayList();
        log.info(" strAction is " + strAction + " strPartInput : "+strPartInput+" strSubCompFlg:"+strSubCompFlg);
        hmParam = GmCommonClass.getHashMapFromForm(GmMultiProjectPartForm);
        
        log.debug(" values inside hmParam " + hmParam);
        if (strAction.equals("save"))
        {
        	String strInput = (String)hmParam.get("HINPUTSTRING");
    		strInput = strInput.replaceAll(",","|^");
    		strInput = "^".concat(strInput).concat("|");
    		// The Input Will be : ^102.041#Y|^103.042#Y|^103.043#N|^104.044#Y|^105.045#Y|
    		
    		strUserID=(String)hmParam.get("USERID");
    		log.debug(" strInput for Save : "+strInput+" strUserID:"+strUserID);
    		gmPartNumBean.saveMultiProjectPart(strInput,strUserID);
        	strAction = "loadParts";
        }
        
        if (strAction.equals("loadParts")){
            if (!strPartInput.equals("") ){
                String strSearch = GmCommonClass.removeSpaces(GmMultiProjectPartForm.getSearch());
                HashMap hmParams = new HashMap();
                hmParams.put("PARTNUM", strPartInput);
                hmParams.put("SEARCH", strSearch);
            	strPartInput = GmCommonClass.createRegExpString(hmParams);
                alPartNumDetails = gmPartNumBean.loadPartNumberDetails(strPartInput,strSubCompFlg).getRows();
                GmMultiProjectPartForm.setLdtPartNumberDetails(alPartNumDetails);
            }
        }
        
        return mapping.findForward("GmMultiProjectPartMap");
    }
}
