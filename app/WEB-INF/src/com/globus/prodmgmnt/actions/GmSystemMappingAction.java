/**
 * FileName : GmSystemMappingAction.java Description : Author : Elango
 */
package com.globus.prodmgmnt.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmSystemBean;
import com.globus.prodmgmnt.forms.GmSystemMappingForm;

/**
 * @author Elango
 * 
 */
public class GmSystemMappingAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    GmSystemMappingForm gmSystemMappingForm = (GmSystemMappingForm) form;
    gmSystemMappingForm.loadSessionParameters(request);
    GmSystemBean gmSystemBean = new GmSystemBean();
    ArrayList alSystemMapDtls = new ArrayList();

    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmCommonBean gmCommonBean = new GmCommonBean();

    String mapAccessFlag = "";
    String strForward = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();

    String strOpt = GmCommonClass.parseNull(gmSystemMappingForm.getStrOpt());
    String strSystemId = GmCommonClass.parseNull(gmSystemMappingForm.getSystemId());
    String strPartyId = GmCommonClass.parseNull(gmSystemMappingForm.getSessPartyId()); // Need to
                                                                                       // pass this
                                                                                       // to
                                                                                       // getAccessPermissions

    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_SYSTEM_SUBMITVOID");
    mapAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmSystemMappingForm.setMapAccessFlag(mapAccessFlag);

    hmParam = GmCommonClass.getHashMapFromForm(gmSystemMappingForm);
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    log.debug("hmParam>>" + hmParam);
    // After saving the SYSTEM-SET mapping details, need to redirect the SYSTEM SETUP container
    // page.
    if (strOpt.equals("save")) {
      saveSystemMapping(gmSystemMappingForm, hmParam);
      strForward = "/gmSystemSetup.do?strOpt=fetchcontainer&tabToLoad=mapping&setId=" + strSystemId;
      
      // View refresh JMS called Start >>>
      String strCompInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      viewRefresh("V207A_SET_CONSIGN_LINK", strCompInfo);
      viewRefresh("V240A_SALES_TARGET_LIST", strCompInfo);
      viewRefresh("V207B_SET_PART_DETAILS", strCompInfo);
      // View refresh JMS called END <<<
      
      return actionRedirect(strForward, request);
    }

    if (strOpt.equals("") || strOpt.equals("fetch")) {
      fetchSystemMapping(gmSystemMappingForm, hmParam);
    }
    // When the strOpt is NULL, need to forward the page to GmSystemMapping.jsp.
    if (strOpt.equals("")) {
      strOpt = "fetch";
    }
    // Below condition is used to set status by AJAX call from the screen.
    if (strOpt.equals("getSetStatus")) {
      String strSetGroup = GmCommonClass.parseNull(gmSystemMappingForm.getSetGroup());
      String strSetID = GmCommonClass.parseNull(gmSystemMappingForm.getSetId());
      String strSetStatus = gmSystemBean.getSetStatus(strSetID, strSetGroup);
      response.setContentType("text/xml");
      if (!strSetStatus.equals("")) {
        PrintWriter pw = response.getWriter();
        pw.write(strSetStatus);
        pw.flush();
      }
      return null;
    }
    log.debug("Forwarding to = >" + strOpt + "<");
    return mapping.findForward(strOpt);
  }

  // fetchSystemMapping - This method is used to fetch System Mapping details.
  private void fetchSystemMapping(GmSystemMappingForm gmSystemMappingForm, HashMap hmParam)
      throws Exception {

    GmSystemBean gmSystemBean = new GmSystemBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    ArrayList alSystemMapDtls = new ArrayList();

    String strSystemId = GmCommonClass.parseNull(gmSystemMappingForm.getSystemId());
    gmSystemMappingForm.setAlSetType(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("SSETYP")));
    gmSystemMappingForm.setAlSetShareType(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("SHRSE")));
    gmSystemMappingForm.setAlLogReasons(gmCommonBean.getLog(strSystemId, "4000419"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    alSystemMapDtls = gmSystemBean.fetchSystemMapping(strSystemId);
    gmSystemMappingForm.setGridData(generateOutPut(alSystemMapDtls, strSessCompanyLocale));
    gmSystemMappingForm.setAlSetMapDtls(alSystemMapDtls);
  }

  // saveSystemMapping - This method is used to save System mapping details.
  private void saveSystemMapping(GmSystemMappingForm gmSystemMappingForm, HashMap hmParam)
      throws AppError {
    GmSystemBean gmSystemBean = new GmSystemBean();
    gmSystemBean.saveSystemMapping(hmParam);
  }

  private String generateOutPut(ArrayList alResult, String strSessCompanyLocale) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateSubDir("prodmgmnt/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmSetMapReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmSetMapDtlsRpt.vm");
    return templateUtil.generateOutput();
  }
  
  /**
   * This method used for refreshing the system setup submit changes related views
   * @param strViewName
   * @param strCompInfo
   * @throws AppError
   */
  public void viewRefresh(String strViewName,String strCompInfo) throws AppError {
	  GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
	  HashMap hmViewRefresh = new HashMap();
	  hmViewRefresh.put("M_VIEW", strViewName);
	  hmViewRefresh.put("companyInfo", strCompInfo); 
	  String strViewConsumerClass =
	      GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
	  hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
	  gmConsumerUtil.sendMessage(hmViewRefresh);
  }
}
