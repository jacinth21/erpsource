/**
 * FileName : GmStaffingDetailsSetupAction.java Description : Author : rshah Date : Feb 12, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.prodmgmnt.beans.GmStaffingInfoBean;
import com.globus.prodmgmnt.beans.GmStaffingTransBean;
import com.globus.prodmgmnt.forms.GmStaffingDetailsForm;

/**
 * @author rshah
 * 
 */
public class GmStaffingDetailsSetupAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward loadStaffingDetailsSetup(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmStaffingDetailsForm gmStaffingDetailsForm = (GmStaffingDetailsForm) form;
    gmStaffingDetailsForm.loadSessionParameters(request);

    GmStaffingInfoBean gmStaffingInfoBean = new GmStaffingInfoBean(getGmDataStoreVO());
    GmStaffingTransBean gmStaffingTransBean = new GmStaffingTransBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());



    String strOpt = gmStaffingDetailsForm.getStrOpt();
    String strPrjId = gmStaffingDetailsForm.getPrjId();
    String strPartyType = gmStaffingDetailsForm.getPartyType();
    String strPrjTeamId = gmStaffingDetailsForm.getHstaffingdetailsId();

    log.debug("9@...." + strOpt + "......" + strPrjId + "...." + strPartyType + "................"
        + strPrjTeamId);
    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmStaffingDetailsForm);
    // ArrayList lResult = null;

    if (strOpt.equals("save")) {
      String strPrjTeamIdFromDb = gmStaffingTransBean.saveProjectStaffingDetails(hmParam);
      gmStaffingDetailsForm.setHstaffingdetailsId(strPrjTeamIdFromDb);
      gmStaffingDetailsForm.reset();
      strOpt = "reload";
    }
    if (strOpt.equals("edit")) {
      hmValues = gmStaffingInfoBean.fetchEditProjectStaffingDetails(strPrjTeamId);
      gmStaffingDetailsForm =
          (GmStaffingDetailsForm) GmCommonClass.getFormFromHashMap(gmStaffingDetailsForm, hmValues);
      log.debug("1@...hmValues......" + hmValues + ".........." + gmStaffingDetailsForm.getRole()
          + "....." + strPrjTeamId);
      alLogReasons = gmCommonBean.getLog(strPrjTeamId, "1244");
      gmStaffingDetailsForm.setAlLogReasons(GmCommonClass.parseNullArrayList(alLogReasons));
      strOpt = "reload";
    }
    if (strOpt.equals("reload")) {
      // lResult = gmStaffingInfoBean.fetchProjectStaffingDetails(strPrjId, strPartyType);
      // log.debug("1@...Result......"+lResult);
      // gmStaffingDetailsForm.setAlResult(GmCommonClass.parseNullArrayList(lResult));
      if (strPartyType.equals("7000")) {
        gmStaffingDetailsForm.setAlRoles(GmCommonClass.getCodeList("SROLE"));
        gmStaffingDetailsForm.setAlStatus(GmCommonClass.getCodeList("CSTAT,SSTAT"));
      } else {
        gmStaffingDetailsForm.setAlRoles(GmCommonClass.getCodeList("GROLE"));
        gmStaffingDetailsForm.setAlStatus(GmCommonClass.getCodeList("CSTAT"));
      }
      gmStaffingDetailsForm.setAlPartyNames(gmPartyInfoBean.fetchPartyNameList(strPartyType));
    }

    gmStaffingDetailsForm.setAlPartyTypes(GmCommonClass.getCodeList("PTTYP", "92056"));
    return mapping.findForward("setup");
  }

  public ActionForward loadStaffingDetailsReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmStaffingDetailsForm gmStaffingDetailsForm = (GmStaffingDetailsForm) form;
    GmStaffingInfoBean gmStaffingInfoBean = new GmStaffingInfoBean(getGmDataStoreVO());
    ArrayList lResult = null;

    String strPrjId = gmStaffingDetailsForm.getPrjId();
    String strPartyType = gmStaffingDetailsForm.getPartyType();
    log.debug("8@.........." + strPrjId + "...." + strPartyType);
    if (!strPrjId.equals("") && !strPartyType.equals("")) {
      lResult = gmStaffingInfoBean.fetchProjectStaffingDetails(strPrjId, strPartyType);
      gmStaffingDetailsForm.setAlResult(GmCommonClass.parseNullArrayList(lResult));
    }
    return mapping.findForward("report");
  }

  public ActionForward loadProjectsByParty(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmStaffingDetailsForm gmStaffingDetailsForm = (GmStaffingDetailsForm) form;
    GmStaffingInfoBean gmStaffingInfoBean = new GmStaffingInfoBean(getGmDataStoreVO());
    ArrayList lResult = null;


    String strPartyId = gmStaffingDetailsForm.getPartyId();
    log.debug("strPartyId = " + strPartyId);
    if (!strPartyId.equals("")) {
      gmStaffingDetailsForm.setLresult(gmStaffingInfoBean.fetchProjectsByParty(strPartyId)
          .getRows());
    }
    return mapping.findForward("projects");
  }
  
}
