/**
 * 
 */
package com.globus.prodmgmnt.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmSubComponentMappingBean;
import com.globus.prodmgmnt.forms.GmSubComponentMappingForm;

/**
 * @author Hreddi
 * 
 */
public class GmSubComponentMappingAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * savePartSubCompDtls' To Map the Sub Component Mapping details
   * 
   * @exception AppError
   */
  public ActionForward savePartSubCompDtls(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);

    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=UTF-8");
    response.setCharacterEncoding("UTF-8");

    String strRequestInputString = "";
    String strFormInputString = "";
    strRequestInputString = GmCommonClass.parseNull(request.getParameter("strInputString"));
    log.debug(" The  Input String from Request Object before convertion ***** "
        + strRequestInputString);
    strRequestInputString =
        new String(GmCommonClass.parseNull(request.getParameter("strInputString"))
            .getBytes("UTF-8"), "UTF-8");
    log.debug(" The  Input String from Request Object after convertion ***** "
        + strRequestInputString);

    GmSubComponentMappingForm gmSubComponentMappingForm = (GmSubComponentMappingForm) form;
    gmSubComponentMappingForm.loadSessionParameters(request);
    GmSubComponentMappingBean gmSubComponentMappingBean =
        new GmSubComponentMappingBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    HashMap hmParams = new HashMap();
    HashMap hmResult = new HashMap();
    String strSubCmponentID = "";
    String strPartNumberID = "";
    String strSuccessMessage = "";
    String strHeaderMessage = "";
    String[] strDbResultArray = null;
    String strErrorParts = "";
    String strRecordsSaveFl = "";
    String strErrorSubParts = "";
    String strDuplicateSubParts = "";
    hmParams = GmCommonClass.getHashMapFromForm(gmSubComponentMappingForm);
    String strOpt = GmCommonClass.parseNull(gmSubComponentMappingForm.getStrOpt());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // strOpt = strOpt.equals("") ? "save" : strOpt;
    String strSesPartyId = GmCommonClass.parseNull(gmSubComponentMappingForm.getSessPartyId());
    HashMap hmPricingAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
            "SUB_COMP_PRICE_TEAM"));
    String strPricingAccessFl = GmCommonClass.parseNull((String) hmPricingAccess.get("UPDFL"));
    gmSubComponentMappingForm.setPricingAccessFl(strPricingAccessFl);

    HashMap hmQualityAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
            "SUB_COMP_QULTY_TEAM"));
    String strQualityAccessFl = GmCommonClass.parseNull((String) hmQualityAccess.get("UPDFL"));
    gmSubComponentMappingForm.setQualityAccessFl(strQualityAccessFl);

    strFormInputString = GmCommonClass.parseNull(gmSubComponentMappingForm.getStrInputString());
    log.debug(" The  Input String from FORM Object  ***** " + strFormInputString);
    hmParams.put("STRINPUTSTRING", strRequestInputString);

    if (strOpt.equals("save")) {
      hmResult =
          GmCommonClass.parseNullHashMap(gmSubComponentMappingBean.savePartSubCompDtls(hmParams));
      strSubCmponentID = GmCommonClass.parseNull((String) hmResult.get("SUBCOMPONENTDATA"));
      strPartNumberID = GmCommonClass.parseNull((String) hmResult.get("PARTNUMDETAILS"));
      log.debug("The Result strSubCmponentID ***  " + strSubCmponentID);
      if (!strSubCmponentID.equals("")) {
        strDbResultArray = strSubCmponentID.split("@");
        strSubCmponentID = strDbResultArray[0];
        strRecordsSaveFl = strDbResultArray[1];
        // Fetching the Saved Sub Component Mapping details
        strSubCmponentID = strSubCmponentID.substring(1);
        strPartNumberID = strPartNumberID.substring(1);
        hmParams.put("SUBCOMPONENTID", strSubCmponentID);
        hmParams.put("PARENTPARTNUMBERS", strPartNumberID);
        alResult =
            GmCommonClass.parseNullArrayList(gmSubComponentMappingBean
                .loadPartSubCompDtls(hmParams));

        // Preparing Appropriate Messages During Sub Component Mapping Setup.
        /*
         * First IF: Preparing the Error Message for Part(s) which are entered are invalid. Second
         * IF: Preparing the Error Message for Sub Component (s) which are Mapped In valid like
         * trying to Map Parent Part Number as Sub Component. Third IF: Preparing the Message for
         * Sub Component Mapping Saving Success and Access related Message based on the Security
         * Group Users.
         */
        if (strDbResultArray.length > 2)
          strErrorParts = strDbResultArray[2];
        if (strDbResultArray.length > 3)
          strErrorSubParts = strDbResultArray[3];
        if (strDbResultArray.length > 4)
          strDuplicateSubParts = strDbResultArray[4];

        // Preparing the Error Message for Part(s) which are trying to Map the same Sub Component
        // Number for same Parts.
        if (!strDuplicateSubParts.equals("")) {
          strDuplicateSubParts = strDuplicateSubParts.substring(1);
          strDuplicateSubParts =
              " The following Sub Component Number(s) are already mapped with some other Part Number(s). "
                  + strDuplicateSubParts;
          gmSubComponentMappingForm.setDuplicateSubCompMessage(strDuplicateSubParts.replaceAll(",",
              ", "));
        }
        // Preparing the Error Message for Part(s) which are entered are invalid.
        if (!strErrorParts.equals("")) {
          strErrorParts = strErrorParts.substring(1);
          strErrorParts =
              " From the following Mapping entries [Sub Component Number - Part Number],the <B>Part Number(s)</B> are Invalid. "
                  + strErrorParts;
          gmSubComponentMappingForm.setStrErrorPartMsg(strErrorParts.replaceAll(",", ", "));
        }
        // Preparing the Error Message for Sub Component (s) which are Mapped In valid like trying
        // to Map Parent Part Number as Sub Component.
        if (!strErrorSubParts.equals("")) {
          strErrorSubParts = strErrorSubParts.substring(1);
          strErrorSubParts =
              " The <B>Part Number<B> should not be mapped as <B>Sub Component Number</B> for the following Mapping entries [Sub Component Number - Part Number]. "
                  + strErrorSubParts;
          gmSubComponentMappingForm.setStrErrorSubPartMsg(strErrorSubParts.replaceAll(",", ", "));
        }
        // Preparing the Message for Sub Component Mapping Saving Success and Access related Message
        // based on the Security Group Users.
        if (strRecordsSaveFl.equals("Y")) {
          strSuccessMessage = "Sub Component Mapping details are saved successfully";
          if (strQualityAccessFl.equals("Y") && strPricingAccessFl.equals("")) {
            strHeaderMessage = " You do not have access to save Price details ";
            gmSubComponentMappingForm.setHeaderMessage(strHeaderMessage);
          } else if (strQualityAccessFl.equals("") && strPricingAccessFl.equals("Y")) {
            strHeaderMessage = " You do not have access to save Sub Component Description details ";
            gmSubComponentMappingForm.setHeaderMessage(strHeaderMessage);
          }
        }
      }
    }

    gmSubComponentMappingForm.setSuccessMessage(strSuccessMessage);
    HashMap hmTemplateParams = new HashMap();
    hmTemplateParams.put("TEMPLATE", "GmSubComponentMapping.vm");
    hmTemplateParams.put("alResult", alResult);
    hmTemplateParams.put("SCREENOPT", strOpt);
    hmTemplateParams.put("QUALITYACCESS", strQualityAccessFl);
    hmTemplateParams.put("PRICINGACCESS", strPricingAccessFl);
    hmTemplateParams.put("TEMPLATEPATH", "prodmgmnt/templates");
    hmTemplateParams.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

    String strXmlString = gmSubComponentMappingBean.getXmlGridData(hmTemplateParams);
    gmSubComponentMappingForm.setStrXMLGrid(strXmlString);
    GmWSUtil gmWSUtil = new GmWSUtil();
    String  strJSON = GmCommonClass.parseNull((String) gmWSUtil.parseArrayListToJson(alResult));
    log.debug("strJSON=="+strJSON);
    gmSubComponentMappingForm.setStrXMLGrid(strJSON);
    gmSubComponentMappingForm.setStrOpt(strOpt);
    return mapping.findForward("GmSubComponentMappingSetup");
  }

  /**
   * loadPartSubCompDtls' To Report the Sub Component Mapping Details
   * 
   * @exception AppError
   */
  public ActionForward loadPartSubCompDtls(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSubComponentMappingForm gmSubComponentMappingForm = (GmSubComponentMappingForm) form;
    gmSubComponentMappingForm.loadSessionParameters(request);
    GmSubComponentMappingBean gmSubComponentMappingBean =
        new GmSubComponentMappingBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    ArrayList alLikeOptions = new ArrayList();
    HashMap hmParams = new HashMap();
    alLikeOptions = GmCommonClass.getCodeList("LIKES");
    gmSubComponentMappingForm.setAlLikeOptions(alLikeOptions);

    hmParams = GmCommonClass.getHashMapFromForm(gmSubComponentMappingForm);
    String strOpt = GmCommonClass.parseNull(gmSubComponentMappingForm.getStrOpt());
    // strOpt = strOpt.equals("") ? "load" : strOpt;
    String strPartNumFormat = "";
    String strPartNumber = GmCommonClass.parseNull(gmSubComponentMappingForm.getPartNum());
    String strPartLike = GmCommonClass.parseNull(gmSubComponentMappingForm.getPartLike());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    if (!strPartNumber.equals(""))
      strPartNumFormat = formatPartNumber(strPartNumber, strPartLike);

    String strSubCompFormat = "";
    String strSubComponent =
        GmCommonClass.parseNull(gmSubComponentMappingForm.getSubComponentNum());
    String strSubCompoLike = GmCommonClass.parseNull(gmSubComponentMappingForm.getSubPartLike());
    if (!strSubComponent.equals(""))
      strSubCompFormat = formatPartNumber(strSubComponent, strSubCompoLike);

    hmParams.put("PARTNUM", strPartNumFormat);
    hmParams.put("SUBCOMPONENTNUM", strSubCompFormat);
    if (strOpt.equals("load")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmSubComponentMappingBean.loadPartSubCompDtls(hmParams));
      HashMap hmTemplateParams = new HashMap();
      hmTemplateParams.put("SCREENOPT", strOpt);
      hmTemplateParams.put("TEMPLATE", "GmSubComponentMapping.vm");
      hmTemplateParams.put("alResult", alResult);
      hmTemplateParams.put("APPLNDATEFMT", getGmDataStoreVO().getCmpdfmt());
      hmTemplateParams.put("TEMPLATEPATH", "prodmgmnt/templates");
      hmTemplateParams.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

      String strXmlString = gmSubComponentMappingBean.getXmlGridData(hmTemplateParams);
      gmSubComponentMappingForm.setStrXMLGrid(strXmlString);
    }
    return mapping.findForward("GmSubComponentMappingRpt");
  }

  /*
   * formatPartNumber(): Formating the part number based on the wild-card search option, IF we
   * select Like - Prefix, we are returning the part like %partnumber IF we select Like - Suffix, we
   * are returning the part like partnumber% ELSE we are simply returning the Input part. Author:
   * HReddi
   */
  public String formatPartNumber(String strPartNUmber, String strPartLike) throws AppError,
      Exception {

    if (strPartLike.equals("90182")) { // Suffix
      strPartNUmber = "%" + strPartNUmber;
    } else if (strPartLike.equals("90181")) { // Prefix
      strPartNUmber = strPartNUmber + "%";
    } else {
      strPartNUmber = strPartNUmber;
    }
    return strPartNUmber;
  }

}
