package com.globus.prodmgmnt.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.beans.GmUDIProductDevelopmentBean;
import com.globus.prodmgmnt.forms.GmUDIProductDevelopmentForm;

public class GmUDIProductDevelopmentAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * pdUDISetup This method used to fetch/save the product development UDI setup details.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws IOException
   */
  public ActionForward pdUDISetup(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, IOException {

    GmUDIProductDevelopmentForm gmUDIProductDevelopmentForm =
        (GmUDIProductDevelopmentForm) actionForm;
    GmUDIProductDevelopmentBean gmUDIProductDevelopmentBean = new GmUDIProductDevelopmentBean();
    GmProjectBean gmProjectBean = new GmProjectBean();
    GmPartNumBean gmPartNumBean = new GmPartNumBean();
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();

    HashMap hmParam = new HashMap();
    String strXmlData = "";
    String strOpt = "";
    String strProjectId = "";
    String strPartNum = "";
    String strPartNumFormat = "";
    String strPartDesc = "";
    String strErrorMsg = "";
    String strSuccessParts = "";
    String strVoidedParts = "";
    String strMaxPdData = "";
    ArrayList alSavedParts = new ArrayList();
    HashMap hmTemp = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alVoidedParts = new ArrayList();

    gmUDIProductDevelopmentForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmUDIProductDevelopmentForm);

    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301"); // Filter all approved projects
    ArrayList alProjList = GmCommonClass.parseNullArrayList(gmProjectBean.reportProject(hmTemp));
    ArrayList alSterilMthod = new ArrayList();
    ArrayList alSizeType = new ArrayList();
    ArrayList alSizeUOM = new ArrayList();
    HashMap<String, ArrayList> hmDropdwon = new HashMap<String, ArrayList>();
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104037"));// length
    hmDropdwon.put("alLength", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104045"));// Area
    hmDropdwon.put("alArea", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104042"));// Weight
    hmDropdwon.put("alWeight", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104040"));// Total
                                                                                              // Volume
    hmDropdwon.put("alTotalVolume", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104034"));// Catheter
                                                                                              // Gauge
    hmDropdwon.put("alGauge", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104046"));// Angle
    hmDropdwon.put("alAngle", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104043"));// Pressure
    hmDropdwon.put("alPressure", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YES/NO"));
    hmDropdwon.put("alYesNo", alResult);
    alResult = new ArrayList();
    alSterilMthod = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDSTER"));
    alSizeType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDSIZE"));
    alSizeUOM = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM"));

    hmDropdwon.put("alSterilMthod", alSterilMthod);
    hmDropdwon.put("alSizeType", alSizeType);
    hmDropdwon.put("alSizeUOM", alSizeUOM);
    gmUDIProductDevelopmentForm.setAlSterilMthod(alSterilMthod);
    gmUDIProductDevelopmentForm.setAlSizeType(alSizeType);
    gmUDIProductDevelopmentForm.setAlSizeUOM(alSizeUOM);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
    strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    strPartDesc = GmCommonClass.parseNull((String) hmParam.get("PARTDESC"));
    String strURL = "/gmUDIProductDevelopment.do?method=pdUDISetup&partNumberID=" + strPartNum;
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    GmResourceBundleBean gmResourceBundleBeanlbl =
        GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmPdUDISetup",
            strSessCompanyLocale);
    String strSuccessMsg =
        GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_RECORD_SAVED"));
    String strDisplayNm =
        GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_PD_UDI_SETUP"));
    String strError = "";

    HashMap hmMaxPdData = new HashMap();
    strMaxPdData = GmCommonClass.parseNull(GmCommonClass.getRuleValue("MAX_DATA", "PD_DATA_SETUP"));
    hmMaxPdData.put("MAX_DATA", strMaxPdData);

    HashMap hmResult = new HashMap();
    hmDropdwon.put("alProjList", alProjList);
    hmResult.put("alProjList", alProjList);
    hmResult.put("hmDropdwon", hmDropdwon);
    hmResult.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

    if (strOpt.equals("Save")) {
      request.setAttribute("hRedirectURL", strURL);
      request.setAttribute("hDisplayNm", strDisplayNm);
      hmReturn =
          GmCommonClass
              .parseNullHashMap(gmUDIProductDevelopmentBean.savePdUDISetupDetails(hmParam));
      strErrorMsg = GmCommonClass.parseNull((String) hmReturn.get("ERROR_PARTS_STR"));
      strSuccessParts = GmCommonClass.parseNull((String) hmReturn.get("SUCCESS_PARTS_STR"));

      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (!strErrorMsg.equals("")) {
        response.setStatus(300);
        pw.write(strErrorMsg);
      } else {
        pw.write(strSuccessParts);
      }
      pw.flush();
      pw.close();
      return null;
    } else if (strOpt.equals("Void")) {
      strVoidedParts =
          GmCommonClass.parseNull(gmUDIProductDevelopmentBean.saveVoidPdDetails(hmParam));
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (!strVoidedParts.equals("")) {
        pw.write(strVoidedParts);
      }
      pw.flush();
      pw.close();
      return null;
    }
    String  strJSON="";
    if (strOpt.equals("Reload")) {
      String strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
      strPartNumFormat = strPartNum;
      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
        // log.debug(" strPartNumFormat prefix --> " + strPartNumFormat);
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("")) {
        strPartNumFormat = strPartNumFormat;
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }
      GmWSUtil gmWSUtil = new GmWSUtil();
      hmValues.put("PROJECTID", strProjectId);
      hmValues.put("PARTNUM", strPartNum);
      hmValues.put("ACTION", "ALL");
      hmValues.put("PARTDESC", strPartDesc);
      hmParam.put("PARTNUMBER", strPartNumFormat);
      request.setAttribute("hSearch", strSearch);
      alResult = GmCommonClass.parseNullArrayList(gmUDIProductDevelopmentBean.fetchProdDevUdiDetails(hmParam));
     // strXmlData = getXmlGridData(alResult, hmResult);
      log.debug(" alResult --> " + alResult.size());
        strJSON = gmWSUtil.parseArrayListToJson(alResult);
      log.debug(" strJSON --> " + strJSON);
      gmUDIProductDevelopmentForm.setGridData(strJSON);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (!strJSON.equals("")) {
        pw.write(strJSON);
      }
      pw.flush();
      // pw.close();
      return null;
    }
    log.debug(" alResult --> " + alResult.size());
    log.debug(" strJSON OUT side --> " + strJSON);
    strXmlData = getXmlGridData(alResult, hmResult);
    gmUDIProductDevelopmentForm.setGridData(strXmlData);
    gmUDIProductDevelopmentForm.setAlProjectId(alProjList);
    gmUDIProductDevelopmentForm.setHmAttrVal(hmDropdwon);
    gmUDIProductDevelopmentForm.setHmResult(hmMaxPdData);
    gmUDIProductDevelopmentForm.setGridData(strJSON);
    return actionMapping.findForward("gmPdUDISetup");
  }

  /**
   * pdPartUDISetup This method used to fetch/save the product development UDI setup details.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   */
  public ActionForward pdPartUDISetup(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    GmUDIProductDevelopmentBean gmUDIProductDevelopmentBean = new GmUDIProductDevelopmentBean();
    GmUDIProductDevelopmentForm gmUDIProductDevelopmentForm =
        (GmUDIProductDevelopmentForm) actionForm;
    gmUDIProductDevelopmentForm.loadSessionParameters(request);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmProjectBean gmProjectBean = new GmProjectBean();
    String strForwardName = "gmPartPDSetup";
    HashMap hmParam = new HashMap();
    // String strOpt = "";
    String strPartNum = "";
    String strAction = "";
    String strAccessFlag = "";
    HashMap hmTemp = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alContainLatex = new ArrayList();
    ArrayList alReqSteril = new ArrayList();
    ArrayList alSterilMthod = new ArrayList();
    ArrayList alIsDevice = new ArrayList();
    ArrayList alSizeType = new ArrayList();
    ArrayList alSizeUOM = new ArrayList();
    ArrayList alLogReasons = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmUDIProductDevelopmentForm);
    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301"); // Filter all approved projects
    ArrayList alProjList = GmCommonClass.parseNullArrayList(gmProjectBean.reportProject(hmTemp));
    strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    // Getting all the DB values for the drop down list
    alContainLatex = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YES/NO"));
    alReqSteril = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YES/NO"));
    alSterilMthod = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDSTER"));
    alIsDevice = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YES/NO"));
    alSizeType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDSIZE"));
    alSizeUOM = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM"));
    ArrayList alResult = new ArrayList();
    HashMap<String, ArrayList> hmDropdwon = new HashMap<String, ArrayList>();
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104037"));// length
    hmDropdwon.put("alLength", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104045"));// Area
    hmDropdwon.put("alArea", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104042"));// Weight
    hmDropdwon.put("alWeight", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104040"));// Total
                                                                                              // Volume
    hmDropdwon.put("alTotalVolume", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104034"));// Catheter
                                                                                              // Gauge
    hmDropdwon.put("alGauge", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104046"));// Angle
    hmDropdwon.put("alAngle", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PDUOM", "104043"));// Pressure
    hmDropdwon.put("alPressure", alResult);

    gmUDIProductDevelopmentForm.setAlContainLatex(alContainLatex);
    gmUDIProductDevelopmentForm.setAlReqSteril(alReqSteril);
    gmUDIProductDevelopmentForm.setAlSterilMthod(alSterilMthod);
    gmUDIProductDevelopmentForm.setAlIsDevice(alIsDevice);
    gmUDIProductDevelopmentForm.setAlSizeType(alSizeType);
    gmUDIProductDevelopmentForm.setAlSizeUOM(alSizeUOM);
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PARTNUM_SUBMIT_ACC"); // Access
                                                                                           // for PD
                                                                                           // team
                                                                                           // to
                                                                                           // have
                                                                                           // Read
                                                                                           // Only
                                                                                           // Access
    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug(" strAccessFlag pdPartUDISetup  --> " + strAccessFlag);
    gmUDIProductDevelopmentForm.setSetSetupSubmitAcc(strAccessFlag);

    log.debug(" strAction inside pdPartUDISetup  --> " + strAction);
    strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    if (strAction.equals("Save")) {
      gmUDIProductDevelopmentBean.savePdPartUDISetupDetails(hmParam);
    }

    alLogReasons = gmCommonBean.getLog(strPartNum, "1293");// log comments
    // for Part PD tab
    // Input screen
    log.debug("alLogReasons-->" + alLogReasons.size());
    gmUDIProductDevelopmentForm.setAlLogReasons(alLogReasons);
    hmReturn = gmUDIProductDevelopmentBean.fetchProdDevPartUdiDetails(hmParam);
    gmUDIProductDevelopmentForm.setHmAttrVal(hmReturn);
    gmUDIProductDevelopmentForm.setHmResult(hmDropdwon);
    return actionMapping.findForward(strForwardName);
  }

  public String getXmlGridData(ArrayList alResult, HashMap hmResult) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    HashMap hmDropdwon = new HashMap();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmResult.get("STRSESSCOMPANYLOCALE"));
    ArrayList alProjList = new ArrayList();
    ArrayList alDropDownSterilizationMethod = new ArrayList();
    ArrayList alDropDownSizeInformationType = new ArrayList();
    // Size information type - detail
    ArrayList alDropDownLength = new ArrayList();
    ArrayList alDropDownArea = new ArrayList();
    ArrayList alDropDownWeight = new ArrayList();
    ArrayList alDropDownTotalVolume = new ArrayList();
    ArrayList alDropDownCatheterGauge = new ArrayList();
    ArrayList alDropDownAngle = new ArrayList();
    ArrayList alDropDownPressure = new ArrayList();
    ArrayList alDropDownYesNo = new ArrayList();
    //
    ArrayList alDropDownUom = new ArrayList();
    hmDropdwon = GmCommonClass.parseNullHashMap((HashMap) hmResult.get("hmDropdwon"));

    alDropDownSterilizationMethod =
        GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alSterilMthod"));
    alDropDownSizeInformationType =
        GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alSizeType"));
    alDropDownYesNo = GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alYesNo"));

    // get the size information
    alDropDownLength = GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alLength"));
    alDropDownArea = GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alArea"));
    alDropDownWeight = GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alWeight"));
    alDropDownTotalVolume =
        GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alTotalVolume"));
    alDropDownCatheterGauge =
        GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alGauge"));
    alDropDownAngle = GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alAngle"));
    alDropDownPressure = GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alPressure"));
    //
    alDropDownUom = GmCommonClass.parseNullArrayList((ArrayList) hmDropdwon.get("alSizeUOM"));
    alProjList = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("alProjList"));
    log.debug(" alProjList size --> " + alProjList.size());
    templateUtil.setDataList("alReturn", alResult);
    templateUtil.setDropDownMaster("alDropDownSterilizationMethod", alDropDownSterilizationMethod);
    templateUtil.setDropDownMaster("alDropDownSizeInformationType", alDropDownSizeInformationType);
    // Size detail
    templateUtil.setDropDownMaster("alDropDownLength", alDropDownLength);
    templateUtil.setDropDownMaster("alDropDownArea", alDropDownArea);
    templateUtil.setDropDownMaster("alDropDownWeight", alDropDownWeight);
    templateUtil.setDropDownMaster("alDropDownTotalVolume", alDropDownTotalVolume);
    templateUtil.setDropDownMaster("alDropDownCatheterGauge", alDropDownCatheterGauge);
    templateUtil.setDropDownMaster("alDropDownAngle", alDropDownAngle);
    templateUtil.setDropDownMaster("alDropDownPressure", alDropDownPressure);
    templateUtil.setDropDownMaster("alDropDownYesNo", alDropDownYesNo);
    templateUtil.setDropDownMaster("alDropDownUom", alDropDownUom);
    templateUtil.setDropDownMaster("alProjList", alProjList);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmPdUDISetup", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("prodmgmnt/templates");
    templateUtil.setTemplateName("GmPdUDISetup.vm");
    return templateUtil.generateOutput();
  }

  /**
   * pdUDISetupPrint This method used to fetch the PD UDI print details.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward pdUDISetupPrint(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmUDIProductDevelopmentForm gmUDIProductDevelopmentForm =
        (GmUDIProductDevelopmentForm) actionForm;
    GmUDIProductDevelopmentBean gmUDIProductDevelopmentBean = new GmUDIProductDevelopmentBean();
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();

    HashMap hmParam = new HashMap();
    HashMap hmJasper = new HashMap();
    HashMap hmHeader = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alResult = new ArrayList();

    gmUDIProductDevelopmentForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmUDIProductDevelopmentForm);
    String strApplnDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strPrintedDate = gmCalenderOperations.getCurrentDate(strApplnDateFmt);
    String strOpt = request.getParameter("strOpt") == null ? "" : request.getParameter("strOpt");
    String strinputStr =
        request.getParameter("partNumStr") == null ? "" : request.getParameter("partNumStr");
    String strPartNum =
        request.getParameter("strPartNum") == null ? "" : request.getParameter("strPartNum");
    String strUserId = (String) request.getSession().getAttribute("strSessUserId");
    String strprintPrjId =
        request.getParameter("printProjectId") == null ? "" : request
            .getParameter("printProjectId");
    hmParam.put("PRINTPROJECTID", strprintPrjId);
    hmParam.put("PARTNUMSTR", strinputStr);
    hmParam.put("STRPARTNUM", strPartNum);
    hmParam.put("USERID", strUserId);

    hmResult =
        GmCommonClass.parseNullHashMap(gmUDIProductDevelopmentBean.fetchPDPrintDtls(hmParam));
    hmHeader = GmCommonClass.parseNullHashMap((HashMap) hmResult.get("HEADER"));
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("DETAILS"));
    gmUDIProductDevelopmentForm.setHmResult(hmResult);

    return actionMapping.findForward("gmPdUDIInputPrint");
  }

  /**
   * fetchTagQty - This Method is used to Fetch the Tag Qty based on Distributor(Ajax request -
   * Distributor id)
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward validatePartNumber(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmUDIProductDevelopmentBean gmUDIProductDevelopmentBean = new GmUDIProductDevelopmentBean();
    String strPartNumber = GmCommonClass.parseNull(request.getParameter("pnum"));
    String strProject = GmCommonClass.parseNull(request.getParameter("project"));

    String strPartCount =
        GmCommonClass.parseNull(gmUDIProductDevelopmentBean.validatePartNumber(strPartNumber));
    String strProjectCount =
        GmCommonClass.parseNull(gmUDIProductDevelopmentBean.validateProject(strProject));
    log.debug("strPartNumber --> " + strPartNumber + " Tag Qty -->" + strPartCount);
    response.setContentType("text/xml;charset=utf-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter pw = response.getWriter();
    StringBuffer sbXML = new StringBuffer();
    if (!strPartCount.equals("")) {
      sbXML.append("<data><pnum>");
      sbXML.append(strPartCount);
      sbXML.append("</pnum>");
      sbXML.append("<project>" + strProjectCount + "</project></data>");
      pw.write(sbXML.toString());
      pw.flush();
    }
    pw.close();
    return null;
  }
}
