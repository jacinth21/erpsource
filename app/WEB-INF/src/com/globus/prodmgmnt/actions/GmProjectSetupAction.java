/**
 * FileName : GmProjectSetupAction.java Description : Author : rshah Date : Feb 2, 2009 Copyright :
 * Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmWSUtil;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.prodmgmnt.beans.GmProjectInfoBean;
import com.globus.prodmgmnt.beans.GmProjectTransBean;
import com.globus.prodmgmnt.forms.GmProjectDetailsForm;

/**
 * @author rshah
 * 
 */
public class GmProjectSetupAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  public ActionForward loadProjectInformation(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmProjectDetailsForm gmProjectDetailsForm = (GmProjectDetailsForm) form;
    gmProjectDetailsForm.loadSessionParameters(request);

    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();

    String accessFlag = "";
    String strForward = "";
    String strNewProjectId = "";
    String strProjId = "";
    String strOldProjName = "";
    String strProjName = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmJmsParam = new HashMap();
    String strOpt = gmProjectDetailsForm.getStrOpt();
    String strPrjId = gmProjectDetailsForm.getPrjId();
    String strPartyId = gmProjectDetailsForm.getSessPartyId(); // Need to pass this to
                                                               // getAccessPermissions


    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PROJECT-SUBMIT");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if (accessFlag.equals("Y")) {
      gmProjectDetailsForm.setstrEnableSubmit("true");
      gmProjectDetailsForm.setstrEnableReset("true");
    }

    log.debug("strOpt = " + strOpt);
    log.debug("strPrjId ==== " + strPrjId);

    hmParam = GmCommonClass.getHashMapFromForm(gmProjectDetailsForm);

    if (strOpt.equals("save")) {
      log.debug("Saving data = " + hmParam);
      saveProjectInfo(mapping, form, request, response);
      strProjId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
      strOldProjName = GmCommonClass.parseNull((String) hmParam.get("HOLDPROJNAME"));
      strProjName = GmCommonClass.parseNull((String) hmParam.get("PRJNM"));
      strNewProjectId = GmCommonClass.parseNull(gmProjectDetailsForm.getProjectID());
      if (strNewProjectId.equals("") || strNewProjectId.equals("0")) {
        strNewProjectId = strProjId;
      }
      String strProjType = GmCommonClass.parseNull(gmProjectDetailsForm.getPrjType());

      // JMS call to update the redis cache if the project name is changed or new project is
      // created
      if (!strOldProjName.equals(strProjName)) {
        hmJmsParam.put("ID", strProjId);
        hmJmsParam.put("OLD_NM", strOldProjName);
        hmJmsParam.put("NEW_NM", strProjName);
        hmJmsParam.put("METHOD", "ProjectList");
        hmJmsParam.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
        String strConsumerClass =
            GmCommonClass.parseNull(GmCommonClass.getString("REDIS_CONSUMER_CLASS"));
        hmJmsParam.put("CONSUMERCLASS", strConsumerClass);
        log.debug(hmJmsParam);
        gmConsumerUtil.sendMessage(hmJmsParam);

      }

      strForward =
          "/gmPDProjectSetup.do?method=loadProjectInformation&strOpt=fetchcontainer&prjId="
              + strNewProjectId + "&prjType=" + strProjType;
      // ActionRedirect actionRedirect = new ActionRedirect(strForward);
      return actionRedirect(strForward, request);
    }
    if (strOpt.equals("") || strOpt.equals("fetchcontainer") || strOpt.equals("fetch")) {
      fetchProjectInfo(mapping, form, request, response);
    }
    if (strOpt.equals("projectCompanyMap")) {
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      loadProjectCompanyMap(gmProjectDetailsForm, hmParam);
    }

    if (strOpt.equals("")) {
      strOpt = "default";
    }

    log.debug("Forwarding to = >" + strOpt + "<");
    return mapping.findForward(strOpt);
  }

  private void fetchProjectInfo(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {

    GmProjectDetailsForm gmProjectDetailsForm = (GmProjectDetailsForm) form;
    instantiate(request, response);
    GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

    HashMap hmValues = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alAttrValues = new ArrayList();
    ArrayList alDivisionList = new ArrayList();

    String strPartyId = gmProjectDetailsForm.getSessPartyId();
    String strPrjId = gmProjectDetailsForm.getPrjId();

    String accessFlag = "";
    String strDevClassGrpId = "103115";// GmCommonClass.getCodeID("Device Classification","PRATTR");
    String strGroupGrpId = "103116";// GmCommonClass.getCodeID("Market/Group","PRATTR");
    String strSegGrpId = "103117";// GmCommonClass.getCodeID("Segment/Category","PRATTR");
    String strTechGrpId = "103118";// GmCommonClass.getCodeID("Technique","PRATTR");

    gmProjectDetailsForm.setDeviceClassGrpId(strDevClassGrpId);
    gmProjectDetailsForm.setGroupGrpId(strGroupGrpId);
    gmProjectDetailsForm.setSegmentGrpId(strSegGrpId);
    gmProjectDetailsForm.setTechniqueGrpId(strTechGrpId);

    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
    gmProjectDetailsForm.setAlDivisionList(alDivisionList);

    if (!strPrjId.equals("")) {
      hmValues = gmProjectInfoBean.fetchProjectDetails(strPrjId); // rename the name
      gmProjectDetailsForm =
          (GmProjectDetailsForm) GmCommonClass.getFormFromHashMap(gmProjectDetailsForm, hmValues);

      alAttrValues = gmProjectInfoBean.fetchProjectAttr(strPrjId, strDevClassGrpId); // fetch
                                                                                     // project
                                                                                     // attributes
      String strDevAttrIds = GmCommonClass.parseNull(getAttributeValues(alAttrValues)); // get the
                                                                                        // attribute
                                                                                        // values

      alAttrValues = gmProjectInfoBean.fetchProjectAttr(strPrjId, strGroupGrpId);
      String strGroupAttrIds = GmCommonClass.parseNull(getAttributeValues(alAttrValues));

      alAttrValues = gmProjectInfoBean.fetchProjectAttr(strPrjId, strSegGrpId);
      String strSegAttrIds = GmCommonClass.parseNull(getAttributeValues(alAttrValues));

      alAttrValues = gmProjectInfoBean.fetchProjectAttr(strPrjId, strTechGrpId);
      String strTechAttrIds = GmCommonClass.parseNull(getAttributeValues(alAttrValues));


      gmProjectDetailsForm.sethDeviceIds(strDevAttrIds);
      gmProjectDetailsForm.sethSegmentIds(strSegAttrIds);
      gmProjectDetailsForm.sethGroupIds(strGroupAttrIds);
      gmProjectDetailsForm.sethTechniqueIds(strTechAttrIds);

      request.setAttribute("hmReturn", hmValues);

      /* Code for access rights.... */
      String strStatus = (GmCommonClass.parseNull((String) hmValues.get("PRJSTATUSID")));
      if (strStatus.equals("20300") || strStatus.equals("20301")) {
        if (strStatus.equals("20300")) /*
                                        * If status is Initiated, then only check for approval
                                        * rights
                                        */
        {
          hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PROJECT-SUBMIT"); // get
                                                                                             // only
                                                                                             // based
                                                                                             // on
                                                                                             // the
                                                                                             // PRJSTATUSID
          accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          log.debug("Permissions for approval = " + hmAccess);
          if (accessFlag.equals("Y")) {
            gmProjectDetailsForm.setStrEnableApproval("true");
            gmProjectDetailsForm.setPrjAprvdBy(strPartyId);
          }
        }
        hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PROJECT-SUBMIT"); // get
                                                                                           // only
                                                                                           // based
                                                                                           // on the
                                                                                           // PRJSTATUSID
        accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("Permissions for change status = " + hmAccess);
        if (accessFlag.equals("Y")) {
          gmProjectDetailsForm.setStrEnableChangeStatus("true");
        }
      }
    }

    gmProjectDetailsForm.setAlDevClass(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("DEVCL")));
    gmProjectDetailsForm.setAlPrjList(GmCommonClass.parseNullArrayList(gmProjectInfoBean
        .fetchProjectList()));
    gmProjectDetailsForm.setAlPrjTypeList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("PKPRN")));
    gmProjectDetailsForm.setAlPrjGrpList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("GROUP")));
    gmProjectDetailsForm.setAlPrjSgmtList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("SEGMT")));
    gmProjectDetailsForm.setAlPrjTechList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("TECHQ")));
    gmProjectDetailsForm.setAlPrjIniList(GmCommonClass.parseNullArrayList(gmAccessControlBean
        .getPartyList("3,4", null))); // 3 for the Project Creation List- combine 3 & 4
    gmProjectDetailsForm.setAlPrjAprvdList(GmCommonClass.parseNullArrayList(gmAccessControlBean
        .getPartyList("4", null)));// 4 for Project Approval List
    gmProjectDetailsForm.setAlLogReasons(gmCommonBean.getLog(strPrjId, "4000397"));
  }

  private void saveProjectInfo(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    /*
     * This method is ussed to save Project details. In bean saveProjectDetails () inside this
     * method it will call saveProjectAttribute() which is used to save project attributes details.
     */
    instantiate(request, response);
    GmProjectDetailsForm gmProjectDetailsForm = (GmProjectDetailsForm) form;
    GmProjectTransBean gmProjectTransBean = new GmProjectTransBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmProjectDetailsForm);

    String strPrjIdFromDb = gmProjectTransBean.saveProjectDetails(hmParam); // Rename the name
    gmProjectDetailsForm.setPrjId(strPrjIdFromDb);

  }

  private String getAttributeValues(ArrayList alAttrValues) throws AppError {

    HashMap hmAttrValues = new HashMap();

    int intSize = 0;
    String strAttrId = "";
    String strAttrIds = "";

    intSize = alAttrValues.size();
    if (intSize > 0) {
      for (int i = 0; i < intSize; i++) {
        hmAttrValues = (HashMap) alAttrValues.get(i);
        strAttrId = GmCommonClass.parseNull((String) hmAttrValues.get("ATTRVAL"));
        strAttrIds += strAttrId + ',';
      }
      strAttrIds = strAttrIds.substring(0, strAttrIds.length() - 1);
    }
    return strAttrIds;
  }

  /**
   * loadProjectCompanyMap This method is used to load Project Company Mapping details for a
   * particular project
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */

  private void loadProjectCompanyMap(GmProjectDetailsForm gmProjectDetailsForm, HashMap hmParam)
      throws AppError {


    GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean(getGmDataStoreVO());

    HashMap hmTemplateParam = new HashMap();
    ArrayList alProjCompList = new ArrayList();
    String strPrjId = gmProjectDetailsForm.getPrjId();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    hmParam.put("PRJID", strPrjId);
    hmParam.put("DATEFMT", getGmDataStoreVO().getCmpdfmt());
    hmParam.put("PRJ_MAP_TYPE", "105361");

    alProjCompList =
        GmCommonClass.parseNullArrayList(gmProjectInfoBean.loadProjectCompanyMap(hmParam));
    hmTemplateParam.put("DATA_MAP", hmParam);
    hmTemplateParam.put("DATA_LIST", alProjCompList);
    hmTemplateParam.put("TEMPLATE_NAME", "GmProjectCompanyMap.vm");
    hmTemplateParam.put("TEMPLATE_SUB_DIR", "prodmgmnt/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmTemplateParam.put("VMFILEPATH", "properties.labels.prodmgmnt.GmProjectCompanyMap");
    // XML String
    String strXmlString = GmCommonClass.getXmlGridData(hmTemplateParam);

    gmProjectDetailsForm.setGridXmlData(strXmlString);

  }


  /**
   * checkProjectID This method is used to Check Project Id is valid.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */
  public ActionForward checkProjectID(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    String strProjectId = GmCommonClass.parseNull(request.getParameter("prjId"));
    GmWSUtil gmWSUtil = new GmWSUtil();
    instantiate(request, response);
    GmProjectDetailsForm gmProjectDetailsForm = (GmProjectDetailsForm) actionForm;
    gmProjectDetailsForm.loadSessionParameters(request);
    GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean(getGmDataStoreVO());
    strJSON = gmProjectInfoBean.checkProject(strProjectId);
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return null;

  }

  /**
   * lookupPorjectInfo This method is used to load To call the existing method to get the project
   * details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */
  public ActionForward lookupProjectInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean(getGmDataStoreVO());
    String strProjectId = GmCommonClass.parseNull(request.getParameter("prjId"));
    HashMap hmReturn = gmProjectInfoBean.fetchProjectDetails(strProjectId);
    request.setAttribute("hmReturn", hmReturn);
    return actionMapping.findForward("projectDetails");
  }

  /**
   * lookupPorjectInfo This method is used to load To call the existing method to get the project
   * details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */
  public ActionForward loadProjectCompanyMapSetup(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    instantiate(request, response);
    GmProjectDetailsForm gmProjectDetailsForm = (GmProjectDetailsForm) actionForm;
    gmProjectDetailsForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmProjectDetailsForm);
    GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean(getGmDataStoreVO());
    GmProjectTransBean gmProjectTransBean = new GmProjectTransBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));

    ArrayList alProjCompList = new ArrayList();
    HashMap hmTemplateParam = new HashMap();
    String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PRJID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    hmParam.put("PROJECTID", strProjectId);
    hmParam.put("MAP_TYPE", "105361");

    if (strOpt.equalsIgnoreCase("save")) {
      String strTokenString = "";
      // to get the project - company mapping string
      String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
      // to split the company id
      StringTokenizer stToken = new StringTokenizer(strInputString, "^");
      while (stToken.hasMoreElements()) {
        strTokenString = stToken.nextToken();
        hmParam.put("COMP_ID", strTokenString);
        gmProjectTransBean.saveProjectCompanyMap(hmParam);
        log.debug(" Project mapping :: " + strProjectId + " company mapping :: " + strTokenString);
        // JMS call
        GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
        String strConsumerClass =
            GmCommonClass.parseNull(GmJMSConsumerConfigurationBean
                .getJmsConfig("PROJECT_COMPANY_MAP_CONSUMER_CLASS"));
        hmParam.put("CONSUMERCLASS", strConsumerClass);
        hmParam.put("COMPANYINFO", GmCommonClass.parseNull(request.getParameter("companyInfo")));
        gmConsumerUtil.sendMessage(hmParam);
      }
      gmProjectDetailsForm.setStrOpt("");
      String strForward =
          "/gmPDProjectSetup.do?method=loadProjectCompanyMapSetup&strOpt=&prjId=" + strProjectId;
      return actionRedirect(strForward, request);
    }
    HashMap hmAccess = new HashMap();
    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PROJECT_COMPANY_MAP");
    log.debug(" hmAccess ==> " + hmAccess);
    String accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if (accessFlag.equals("Y")) {
      gmProjectDetailsForm.setstrEnableSubmit("true");
    }
    // to load the non mapped company id
    alProjCompList = gmProjectInfoBean.loadProjectParentCompanyList(strProjectId);
    gmProjectDetailsForm.setAlParentCompany(alProjCompList);

    hmTemplateParam.put("DATA_MAP", hmParam);
    hmTemplateParam.put("DATA_LIST", new ArrayList());
    hmTemplateParam.put("TEMPLATE_NAME", "GmProjectCompanyMapSetup.vm");
    hmTemplateParam.put("TEMPLATE_SUB_DIR", "prodmgmnt/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmTemplateParam.put("VMFILEPATH", "properties.labels.prodmgmnt.GmProjectCompanyMapSetup");
    // XML String
    String strXmlString = GmCommonClass.getXmlGridData(hmTemplateParam);
    gmProjectDetailsForm.setGridXmlData(strXmlString);
    return actionMapping.findForward("projectMapSetupDetails");
  }
}
