/**
 *  FileName : GmSystemPublishAction.java 
 *  Author : N RAJA
 */
package com.globus.prodmgmnt.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmSystemPublishReportBean;
import com.globus.prodmgmnt.forms.GmSystemDetailForm;
import com.globus.prodmgmnt.beans.GmSystemPublishTransBean;
/**
 * GmSystemPublishAction class for saving, the System details
 */
public class GmSystemPublishAction extends GmDispatchAction {

	/**
	 *  This used to get the instance of this class for enabling logging 
	 */
  Logger log = GmLogger.getInstance(this.getClass().getName());
  
  /**
   * saveSystemPublishDtls - publish to save System details using Ajax call
   * @param mapping, form, request, response
   * @return
   * @throws Exception
   */
  public ActionForward saveSystemPublishDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
	    instantiate(request, response);
	    GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) form;
	    gmSystemDetailForm.loadSessionParameters(request);
	    GmSystemPublishTransBean gmSystemPublishTransBean = new GmSystemPublishTransBean(getGmDataStoreVO());
	    ArrayList alSystemmMap = new ArrayList();
	    HashMap hmParam = new HashMap();
	    String strResult = "";
	    String strCompanyInfo = "";
		strCompanyInfo = GmCommonClass.parseNull(request
				.getParameter("companyInfo"));
	    hmParam = GmCommonClass.getHashMapFromForm(gmSystemDetailForm);
	    hmParam.put("COMPANYINFO", strCompanyInfo);
	    strResult = gmSystemPublishTransBean.savSystemPublishDtls(hmParam); 
	     if (!strResult.equals("")) {
	    	      response.setContentType("text/plain");
	    	      PrintWriter pw = response.getWriter();
	    	      pw.write(strResult);
	    	      pw.flush();
	    	      return null;
	     }
       return null;
  }

	/**
	 * fetchSystemReleaseDtls - fetch system release and unreleased details based on region
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchSystemReleaseDtls(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) form;
		gmSystemDetailForm.loadSessionParameters(request);
		GmSystemPublishReportBean gmSystemPublishReportBean = new GmSystemPublishReportBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmSystemDetailForm);
		String strSystemReleaseDetailsJsonString = GmCommonClass
				.parseNull(gmSystemPublishReportBean
						.fetchSystemRelease(hmParam));

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strSystemReleaseDetailsJsonString.equals("")) {
			pw.write(strSystemReleaseDetailsJsonString);
		}
		pw.flush();
		return null;

	}
 
	  /**
	 * loadSystemReleaseDtls - redirect request to system release page
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadSystemReleaseDtls(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) form;
		gmSystemDetailForm.loadSessionParameters(request);
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

		HashMap hmAccess = new HashMap();
		String strPartyId = "";
		String strButtonAcc = "";
		String strFromPage = "";

		// Button access for system  and set release and unrelease screen
		strPartyId = GmCommonClass.parseNull((String) gmSystemDetailForm
				.getSessPartyId());
		strFromPage = GmCommonClass.parseNull((String) gmSystemDetailForm
				.getFrompage());
		// check request for set mapping screen
		if(strFromPage.equals("setsetup"))
		{
		hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean
				.getAccessPermissions(strPartyId, "SET_MAP_SUB_BTN_ACC")));
		}
		// check request for system mapping screen
		else
		{
		hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean
				.getAccessPermissions(strPartyId, "SYS_MAP_SUB_BTN_ACC")));
		}
		strButtonAcc = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		log.debug(" System and Set Release Submit - Permissions Flag = " + hmAccess);
		gmSystemDetailForm.setButtonAccessFl(strButtonAcc);
		return mapping.findForward("gmsystemreleasemapping");
	}
  	
	/**
	 * saveSystemReleaseDtls - used to save system released and unreleased details then call JMS to map system details for company
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward saveSystemReleaseDtls(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError,Exception 
			 {

		instantiate(request, response);
		GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) form;
		gmSystemDetailForm.loadSessionParameters(request);
		GmSystemPublishTransBean gmSystemPublishTransBean = new GmSystemPublishTransBean(
				getGmDataStoreVO());
		String strCompanyInfo = "";
		strCompanyInfo = GmCommonClass.parseNull(request
				.getParameter("companyInfo"));
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmSystemDetailForm);
		hmParam.put("COMPANYINFO", strCompanyInfo);
		gmSystemPublishTransBean.saveSystemReleaseDtls(hmParam);

		return null;

	}
	/**
   * loadSystemSetMappingdtls - to load System set Mapping details
   * @param actionMapping, actionForm, request, response
   * @return
   * @throws Exception
   */
  public ActionForward loadSystemSetMappingdtls (ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws AppError,Exception {
	      instantiate(request, response);
		  GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) actionForm;
		  gmSystemDetailForm.loadSessionParameters(request);
	  return actionMapping.findForward("gmSystemSetMap");
  }
  
  /**
   * fetchSystemSetMappingDtls - to fetch System Set Details 
   * @param actionMapping, actionForm, request, response
   * @return
   * @throws Exception
   */
  public ActionForward fetchSystemSetMappingDtls (ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws AppError,Exception {
	  
		  instantiate(request, response);
		  GmSystemDetailForm gmSystemDetailForm = (GmSystemDetailForm) actionForm;
		  gmSystemDetailForm.loadSessionParameters(request);
		  GmSystemPublishReportBean gmSystemPublishReportBean = new GmSystemPublishReportBean(getGmDataStoreVO());
		  
		  String strJsonString = "";
          HashMap hmParam = new HashMap();
		  hmParam = GmCommonClass.getHashMapFromForm(gmSystemDetailForm);
		  
		  hmParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
		  strJsonString = gmSystemPublishReportBean.fetchSystemSetMappingDtls(hmParam);
		  response.setContentType("text/json");
		  PrintWriter printWriter = response.getWriter();
		  if (!strJsonString.equals("")) {
			  printWriter.write(strJsonString);
		   }
		  printWriter.flush();
		 return null;
  }
}
