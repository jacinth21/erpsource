package com.globus.prodmgmnt.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmPartNumLoadBean;

public class GmPartNumUploadAction extends GmAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//GmPartNumUploadForm gmProjectMilestonesSetupForm = (GmPartNumUploadForm) form;
		String strFileName = "C://com//revere.csv";
		log.debug(" FileName ");
		GmPartNumLoadBean gmPartNumLoadBean =  new GmPartNumLoadBean();
		gmPartNumLoadBean.bulkUploadPartDetails(strFileName);
		
		
		return mapping.findForward("partNumUpload");
	}
}
