/**
 * FileName    : GmProjectsDashboardAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 11, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectInfoBean;
import com.globus.prodmgmnt.forms.GmProjectDetailsForm;

/**
 * @author sthadeshwar
 *
 */
public class GmProjectsDashboardAction extends GmAction {

Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		try {
			GmProjectDetailsForm gmProjectDetailsForm = (GmProjectDetailsForm)form;
			gmProjectDetailsForm.loadSessionParameters(request);
			GmProjectInfoBean gmProjectInfoBean = new GmProjectInfoBean();
			
			String strOpt = gmProjectDetailsForm.getStrOpt();
			log.debug("strOpt = " + strOpt);
			
			if(strOpt.equals("fetch")) {
				List lReturn = gmProjectInfoBean.fetchProjectsDashboard().getRows();
				gmProjectDetailsForm.setLresult(lReturn);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return mapping.findForward("success");
	}
}
