package com.globus.prodmgmnt.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmPartProjectMapBean;
import com.globus.prodmgmnt.forms.GmPartProjectMapForm;

public class GmPartProjectMapAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * saveProject: This method is used to save the Part Project Mapping details for the particular
   * part.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */

  public ActionForward saveProject(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);

    GmPartProjectMapForm gmPartProjectMapForm = (GmPartProjectMapForm) form;
    gmPartProjectMapForm.loadSessionParameters(request);
    GmPartProjectMapBean gmPartProjectMapBean = new GmPartProjectMapBean(getGmDataStoreVO());

    ArrayList alAvailableProject = new ArrayList();
    ArrayList alAssProject = new ArrayList();

    HashMap hmParam = new HashMap();

    String strOpt = "";
    String strPartNumber = "";
    String strRedirectURL = "";
    String strDispatchTo = "partproject";

    hmParam = GmCommonClass.getHashMapFromForm(gmPartProjectMapForm);
    strOpt = GmCommonClass.parseNull(gmPartProjectMapForm.getStrOpt());
    strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    if (!strPartNumber.equals("")) {
      hmParam.put("PROJECTMAPTYPE", "10304537"); // Status Hard coded to Secondary
      if (strOpt.equals("save")) {
        gmPartProjectMapBean.saveAsstProject(hmParam);// Saving associate Projects.
      }
      strRedirectURL =
          "/gmPartProjectMapDtls.do?method=fetchProject&partnumber=" + strPartNumber + "";
      // ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
      return actionRedirect(strRedirectURL, request);
    }
    return mapping.findForward(strDispatchTo);

  }

  // Class.
  /**
   * fetchProject: This method is used to fetch the Part Project Mapping details for particular
   * part.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */
  public ActionForward fetchProject(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmPartProjectMapForm gmPartProjectMapForm = (GmPartProjectMapForm) form;
    gmPartProjectMapForm.loadSessionParameters(request);
    GmPartProjectMapBean gmPartProjectMapBean = new GmPartProjectMapBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    ArrayList alAvailableProject = new ArrayList();
    ArrayList alAssProject = new ArrayList();
    ArrayList alCompanyList = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmAssocProjData = new HashMap();

    String strOpt = "";
    String strDispatchTo = "partproject";
    String strPartNumber = "";
    String strxmlGridData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmParam = GmCommonClass.getHashMapFromForm(gmPartProjectMapForm);
    strOpt = GmCommonClass.parseNull(gmPartProjectMapForm.getStrOpt());
    strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));

    if (!strPartNumber.equals("")) {
      hmParam.put("PROJECTMAPTYPE", "10304537"); // Status Hard coded to Secondary

      // alAvailableProject -->Getting the Available Projects and the same will be loaded in Project
      // List Dropdown
      alAvailableProject = gmPartProjectMapBean.fetchNonAsstProject(hmParam);
      // alAssProject -->The Project Associated for the Part will be Loaded here.
      alAssProject = gmPartProjectMapBean.fetchAssociatePartProject(hmParam);
      // fetching the parent company list.
      alCompanyList = GmCommonClass.parseNullArrayList(gmCommonBean.getParentCompanyList());
      gmPartProjectMapForm.setAlAvailableProject(alAvailableProject);
      gmPartProjectMapForm.setAlAssproject(alAssProject);
      gmPartProjectMapForm.setAlCompany(alCompanyList);

      hmAssocProjData.put("ASSOCPROJECTDATA", alAssProject);
      hmAssocProjData.put("DATEFMT", getGmDataStoreVO().getCmpdfmt());
      hmAssocProjData.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

      strxmlGridData = generateOutPut(hmAssocProjData);
      gmPartProjectMapForm.setGridXmlData(strxmlGridData);

      // To load the project list based on the company selected on onchange event of company list.
      if (strOpt.equals("AJAX")) {

        HashMap hmComboMap = new HashMap();

        hmComboMap.put("alResult", alAvailableProject);
        hmComboMap.put("ID", "PROJECTID");
        hmComboMap.put("NAME", "PROJECTNAME");
        hmComboMap.put("DEFAULTVALUE", "[Choose One]");
        hmComboMap.put("selectedCodeId", "");

        String strXmlData = generateOutPut(alAvailableProject, hmComboMap);

        if (!strXmlData.equals("")) {
          response.setContentType("text/plain");
          PrintWriter pw = response.getWriter();
          pw.write(strXmlData);
          pw.flush();
          return null;
        }

      }
    }

    return mapping.findForward(strDispatchTo);

  }

  /**
   * getXmlGridData: This method is used to prepare xml string for combobox file
   * 
   * @param ArrayList
   * @param HashMap
   * @return String
   * @exception AppError
   */
  private String generateOutPut(ArrayList alComboData, HashMap hmCombo) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alComboData);
    templateUtil.setDataMap("hmCombo", hmCombo);
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName("GmCombAutoList.vm");
    return templateUtil.generateOutput();
  }

  /**
   * getXmlGridData: This method is used to prepare xml string for combobox file
   * 
   * @param ArrayList
   * @param HashMap
   * @return String
   * @exception AppError
   */
  private String generateOutPut(HashMap hmParam) throws AppError { // Processing HashMap into XML
    // data
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("prodmgmnt/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmPartProjectMap", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPartProjectMap.vm");
    return templateUtil.generateOutput();
  }
}
