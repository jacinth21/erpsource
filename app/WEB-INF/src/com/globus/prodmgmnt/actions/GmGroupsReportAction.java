package com.globus.prodmgmnt.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass; 
import com.globus.prodmgmnt.beans.GmProdReportBean;
import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;

public class GmGroupsReportAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	public ActionForward allGroupsReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RowSetDynaClass rdMSPReport = null;
		DynaActionForm gmGroupsReportForm = (DynaActionForm) form;
		GmProdReportBean gmProdReportBean = new GmProdReportBean(); 

		rdMSPReport = gmProdReportBean.fetchGroupsReport(); 
		gmGroupsReportForm.set("ldtResult", rdMSPReport.getRows());

		return mapping.findForward("GmGroupsReport");
	}
}
