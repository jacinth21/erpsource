/**
 * FileName    : GmStaffingDetailsForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sthadeshwar
 *
 */
public class GmStaffingDetailsForm extends GmProjectDetailsForm {
	
	private String hstaffingdetailsId ="";
	private String partyType = "";
	private String partyId = "";
	private String role = "";
	private String status = "";	
	
	private ArrayList alPartyTypes = new ArrayList();
	private ArrayList alPartyNames = new ArrayList();
	private ArrayList alRoles = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alResult= new ArrayList();
	
	private String firstName = "";
	private String lastName = "";
	
	public void reset()
	{
		hstaffingdetailsId = "";
		partyId="";
		role = "";
		status = "";
	}
	private ArrayList alStaffingCountDetails = new ArrayList();

	/**
	 * @return the hstaffingdetailsId
	 */
	public String getHstaffingdetailsId() {
		return hstaffingdetailsId;
	}

	/**
	 * @param hstaffingdetailsId the hstaffingdetailsId to set
	 */
	public void setHstaffingdetailsId(String hstaffingdetailsId) {
		this.hstaffingdetailsId = hstaffingdetailsId;
	}

	/**
	 * @return the partyType
	 */
	public String getPartyType() {
		return partyType;
	}

	/**
	 * @param partyType the partyType to set
	 */
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}

	/**
	 * @return the partyId
	 */
	public String getPartyId() {
		return partyId;
	}

	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the alPartyTypes
	 */
	public ArrayList getAlPartyTypes() {
		return alPartyTypes;
	}

	/**
	 * @param alPartyTypes the alPartyTypes to set
	 */
	public void setAlPartyTypes(ArrayList alPartyTypes) {
		this.alPartyTypes = alPartyTypes;
	}

	/**
	 * @return the alPartyNames
	 */
	public ArrayList getAlPartyNames() {
		return alPartyNames;
	}

	/**
	 * @param alPartyNames the alPartyNames to set
	 */
	public void setAlPartyNames(ArrayList alPartyNames) {
		this.alPartyNames = alPartyNames;
	}

	/**
	 * @return the alRoles
	 */
	public ArrayList getAlRoles() {
		return alRoles;
	}

	/**
	 * @param alRoles the alRoles to set
	 */
	public void setAlRoles(ArrayList alRoles) {
		this.alRoles = alRoles;
	}

	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}

	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	/**
	 * @return the alStaffingCountDetails
	 */
	public ArrayList getAlStaffingCountDetails() {
		return alStaffingCountDetails;
	}

	/**
	 * @param alStaffingCountDetails the alStaffingCountDetails to set
	 */
	public void setAlStaffingCountDetails(ArrayList alStaffingCountDetails) {
		this.alStaffingCountDetails = alStaffingCountDetails;
	}

	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}

	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
