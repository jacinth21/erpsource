package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonForm;

public class GmMultiProjectPartForm extends GmCommonForm
{
    
    private String enablesubcomponent = "";
    private String partNumbers = "";
    private String selectAll = "";
    private List ldtPartNumberDetails = new ArrayList();
    private String[] checkMultiProject= new String[20];
    private String hinputString = "";
    private String[] checkPartNumbers = new String [20]; 
    private List partNumberSearch = new ArrayList();
    private String search = "" ;
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

    /**
     * @return Returns the partNumbers.
     */
    public String getPartNumbers()
    {
        return partNumbers;
    }

    /**
     * @param partNumbers The partNumbers to set.
     */
    public void setPartNumbers(String partNumbers)
    {
        this.partNumbers = partNumbers;
    }

    /**
     * @return Returns the checkPartNumbers.
     */
    public String[] getCheckPartNumbers()
    {
        return checkPartNumbers;
    }

    /**
     * @param checkPartNumbers The checkPartNumbers to set.
     */
    public void setCheckPartNumbers(String[] checkPartNumbers)
    {
            this.checkPartNumbers = checkPartNumbers;
    }

    /**
     * @return Returns the selectAll.
     */
    public String getSelectAll()
    {
        return selectAll;
    }

    /**
     * @param selectAll The selectAll to set.
     */
    public void setSelectAll(String selectAll)
    {
        this.selectAll = selectAll;
    }

	/**
	 * @return the enablesubcomponent
	 */
	public String getEnablesubcomponent() {
		return enablesubcomponent;
	}

	/**
	 * @param enablesubcomponent the enablesubcomponent to set
	 */
	public void setEnablesubcomponent(String enablesubcomponent) {
		this.enablesubcomponent = enablesubcomponent;
	}

	/**
	 * @return the ldtPartNumberDetails
	 */
	public List getLdtPartNumberDetails() {
		return ldtPartNumberDetails;
	}

	/**
	 * @param ldtPartNumberDetails the ldtPartNumberDetails to set
	 */
	public void setLdtPartNumberDetails(List ldtPartNumberDetails) {
		this.ldtPartNumberDetails = ldtPartNumberDetails;
	}

	/**
	 * @return the checkMultiProject
	 */
	public String[] getCheckMultiProject() {
		return checkMultiProject;
	}

	/**
	 * @param checkMultiProject the checkMultiProject to set
	 */
	public void setCheckMultiProject(String[] checkMultiProject) {
		this.checkMultiProject = checkMultiProject;
	}
	/**
	 * @return the hInputString
	 */
	public String getHinputString() {
		return hinputString;
	}

	/**
	 * @param inputString the hInputString to set
	 */
	public void setHinputString(String inputString) {
		hinputString = inputString;
	}
	/**
	 * @return the PartNumberSearch
	 */
	public List getPartNumberSearch() {
		return partNumberSearch;
	}
	/**
	 * @param PartNumberSearch the PartNumberSearch to set
	 */
	public void setPartNumberSearch(List partNumberSearch) {
		this.partNumberSearch = partNumberSearch;
	}
	/**
	 * @return the Search
	 */
	public String getSearch() {
		return search;
	}
	/**
	 * @param Search the Search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

  
}
