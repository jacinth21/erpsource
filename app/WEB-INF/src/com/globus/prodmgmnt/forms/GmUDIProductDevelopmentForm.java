package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmUDIProductDevelopmentForm extends GmLogForm {

  private String inputStr = "";
  private String strinputStr = "";
  private String strOpt = "";
  private String strAction = "";
  private String setSetupSubmitAcc = "";
  private String strMessage = "";
  // PD UDI setup
  private String projectId = "";
  private String partNumber = "";
  private String partDesc = "";
  private String partNumStr = "";
  private String printProjectId = "";
  private String gridData = "";
  // JSP String values
  private String strPartNum = "";
  private String strPartDesc = "";
  private String strProjectId = "";
  private String strContainsLatex = "";
  private String strReqSteril = "";
  private String strSterilMthod = "";
  private String strIsDevice = "";
  private String strSize1Type = "";
  private String strSize1TypeText = "";
  private String strSize1Value = "";
  private String strSize1UOM = "";
  private String strSize2Type = "";
  private String strSize2TypeText = "";
  private String strSize2Value = "";
  private String strSize2UOM = "";
  private String strSize3Type = "";
  private String strSize3TypeText = "";
  private String strSize3Value = "";
  private String strSize3UOM = "";
  private String strSize4Type = "";
  private String strSize4TypeText = "";
  private String strSize4Value = "";
  private String strSize4UOM = "";
  private String deletePartStr = "";
  private String msgFl = "";
  private String editDisable = "";

  private ArrayList alContainLatex = new ArrayList();
  private ArrayList alReqSteril = new ArrayList();
  private ArrayList alSterilMthod = new ArrayList();
  private ArrayList alIsDevice = new ArrayList();
  private ArrayList alSizeType = new ArrayList();
  private ArrayList alSizeUOM = new ArrayList();
  private ArrayList alProjectId = new ArrayList();

  private HashMap hmAttrVal = new HashMap();
  private HashMap hmResult = new HashMap();

  /**
   * @return the inputStr
   */
  public String getInputStr() {
    return inputStr;
  }

  /**
   * @param inputStr the inputStr to set
   */
  public void setInputStr(String inputStr) {
    this.inputStr = inputStr;
  }

  /**
   * @return the strinputStr
   */
  public String getStrinputStr() {
    return strinputStr;
  }

  /**
   * @param strinputStr the strinputStr to set
   */
  public void setStrinputStr(String strinputStr) {
    this.strinputStr = strinputStr;
  }

  /**
   * @return the strOpt
   */
  @Override
  public String getStrOpt() {
    return strOpt;
  }

  /**
   * @param strOpt the strOpt to set
   */
  @Override
  public void setStrOpt(String strOpt) {
    this.strOpt = strOpt;
  }

  /**
   * @return the strAction
   */
  public String getStrAction() {
    return strAction;
  }

  /**
   * @param strAction the strAction to set
   */
  public void setStrAction(String strAction) {
    this.strAction = strAction;
  }

  /**
   * @return the setSetupSubmitAcc
   */
  public String getSetSetupSubmitAcc() {
    return setSetupSubmitAcc;
  }

  /**
   * @param setSetupSubmitAcc the setSetupSubmitAcc to set
   */
  public void setSetSetupSubmitAcc(String setSetupSubmitAcc) {
    this.setSetupSubmitAcc = setSetupSubmitAcc;
  }

  /**
   * @return the strMessage
   */
  public String getStrMessage() {
    return strMessage;
  }

  /**
   * @param strMessage the strMessage to set
   */
  public void setStrMessage(String strMessage) {
    this.strMessage = strMessage;
  }

  /**
   * @return the projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @param projectId the projectId to set
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @return the partNumber
   */
  public String getPartNumber() {
    return partNumber;
  }

  /**
   * @param partNumber the partNumber to set
   */
  public void setPartNumber(String partNumber) {
    this.partNumber = partNumber;
  }

  /**
   * @return the partDesc
   */
  public String getPartDesc() {
    return partDesc;
  }

  /**
   * @param partDesc the partDesc to set
   */
  public void setPartDesc(String partDesc) {
    this.partDesc = partDesc;
  }

  /**
   * @return the partNumStr
   */
  public String getPartNumStr() {
    return partNumStr;
  }

  /**
   * @param partNumStr the partNumStr to set
   */
  public void setPartNumStr(String partNumStr) {
    this.partNumStr = partNumStr;
  }

  /**
   * @return the printProjectId
   */
  public String getPrintProjectId() {
    return printProjectId;
  }

  /**
   * @param printProjectId the printProjectId to set
   */
  public void setPrintProjectId(String printProjectId) {
    this.printProjectId = printProjectId;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the strPartNum
   */
  public String getStrPartNum() {
    return strPartNum;
  }

  /**
   * @param strPartNum the strPartNum to set
   */
  public void setStrPartNum(String strPartNum) {
    this.strPartNum = strPartNum;
  }

  /**
   * @return the strPartDesc
   */
  public String getStrPartDesc() {
    return strPartDesc;
  }

  /**
   * @param strPartDesc the strPartDesc to set
   */
  public void setStrPartDesc(String strPartDesc) {
    this.strPartDesc = strPartDesc;
  }

  /**
   * @return the strProjectId
   */
  public String getStrProjectId() {
    return strProjectId;
  }

  /**
   * @param strProjectId the strProjectId to set
   */
  public void setStrProjectId(String strProjectId) {
    this.strProjectId = strProjectId;
  }

  /**
   * @return the strContainsLatex
   */
  public String getStrContainsLatex() {
    return strContainsLatex;
  }

  /**
   * @param strContainsLatex the strContainsLatex to set
   */
  public void setStrContainsLatex(String strContainsLatex) {
    this.strContainsLatex = strContainsLatex;
  }

  /**
   * @return the strReqSteril
   */
  public String getStrReqSteril() {
    return strReqSteril;
  }

  /**
   * @param strReqSteril the strReqSteril to set
   */
  public void setStrReqSteril(String strReqSteril) {
    this.strReqSteril = strReqSteril;
  }

  /**
   * @return the strSterilMthod
   */
  public String getStrSterilMthod() {
    return strSterilMthod;
  }

  /**
   * @param strSterilMthod the strSterilMthod to set
   */
  public void setStrSterilMthod(String strSterilMthod) {
    this.strSterilMthod = strSterilMthod;
  }

  /**
   * @return the strIsDevice
   */
  public String getStrIsDevice() {
    return strIsDevice;
  }

  /**
   * @param strIsDevice the strIsDevice to set
   */
  public void setStrIsDevice(String strIsDevice) {
    this.strIsDevice = strIsDevice;
  }

  /**
   * @return the strSize1Type
   */
  public String getStrSize1Type() {
    return strSize1Type;
  }

  /**
   * @param strSize1Type the strSize1Type to set
   */
  public void setStrSize1Type(String strSize1Type) {
    this.strSize1Type = strSize1Type;
  }

  /**
   * @return the strSize1TypeText
   */
  public String getStrSize1TypeText() {
    return strSize1TypeText;
  }

  /**
   * @param strSize1TypeText the strSize1TypeText to set
   */
  public void setStrSize1TypeText(String strSize1TypeText) {
    this.strSize1TypeText = strSize1TypeText;
  }

  /**
   * @return the strSize1Value
   */
  public String getStrSize1Value() {
    return strSize1Value;
  }

  /**
   * @param strSize1Value the strSize1Value to set
   */
  public void setStrSize1Value(String strSize1Value) {
    this.strSize1Value = strSize1Value;
  }

  /**
   * @return the strSize1UOM
   */
  public String getStrSize1UOM() {
    return strSize1UOM;
  }

  /**
   * @param strSize1UOM the strSize1UOM to set
   */
  public void setStrSize1UOM(String strSize1UOM) {
    this.strSize1UOM = strSize1UOM;
  }

  /**
   * @return the strSize2Type
   */
  public String getStrSize2Type() {
    return strSize2Type;
  }

  /**
   * @param strSize2Type the strSize2Type to set
   */
  public void setStrSize2Type(String strSize2Type) {
    this.strSize2Type = strSize2Type;
  }

  /**
   * @return the strSize2TypeText
   */
  public String getStrSize2TypeText() {
    return strSize2TypeText;
  }

  /**
   * @param strSize2TypeText the strSize2TypeText to set
   */
  public void setStrSize2TypeText(String strSize2TypeText) {
    this.strSize2TypeText = strSize2TypeText;
  }

  /**
   * @return the strSize2Value
   */
  public String getStrSize2Value() {
    return strSize2Value;
  }

  /**
   * @param strSize2Value the strSize2Value to set
   */
  public void setStrSize2Value(String strSize2Value) {
    this.strSize2Value = strSize2Value;
  }

  /**
   * @return the strSize2UOM
   */
  public String getStrSize2UOM() {
    return strSize2UOM;
  }

  /**
   * @param strSize2UOM the strSize2UOM to set
   */
  public void setStrSize2UOM(String strSize2UOM) {
    this.strSize2UOM = strSize2UOM;
  }

  /**
   * @return the strSize3Type
   */
  public String getStrSize3Type() {
    return strSize3Type;
  }

  /**
   * @param strSize3Type the strSize3Type to set
   */
  public void setStrSize3Type(String strSize3Type) {
    this.strSize3Type = strSize3Type;
  }

  /**
   * @return the strSize3TypeText
   */
  public String getStrSize3TypeText() {
    return strSize3TypeText;
  }

  /**
   * @param strSize3TypeText the strSize3TypeText to set
   */
  public void setStrSize3TypeText(String strSize3TypeText) {
    this.strSize3TypeText = strSize3TypeText;
  }

  /**
   * @return the strSize3Value
   */
  public String getStrSize3Value() {
    return strSize3Value;
  }

  /**
   * @param strSize3Value the strSize3Value to set
   */
  public void setStrSize3Value(String strSize3Value) {
    this.strSize3Value = strSize3Value;
  }

  /**
   * @return the strSize3UOM
   */
  public String getStrSize3UOM() {
    return strSize3UOM;
  }

  /**
   * @param strSize3UOM the strSize3UOM to set
   */
  public void setStrSize3UOM(String strSize3UOM) {
    this.strSize3UOM = strSize3UOM;
  }

  /**
   * @return the strSize4Type
   */
  public String getStrSize4Type() {
    return strSize4Type;
  }

  /**
   * @param strSize4Type the strSize4Type to set
   */
  public void setStrSize4Type(String strSize4Type) {
    this.strSize4Type = strSize4Type;
  }

  /**
   * @return the strSize4TypeText
   */
  public String getStrSize4TypeText() {
    return strSize4TypeText;
  }

  /**
   * @param strSize4TypeText the strSize4TypeText to set
   */
  public void setStrSize4TypeText(String strSize4TypeText) {
    this.strSize4TypeText = strSize4TypeText;
  }

  /**
   * @return the strSize4Value
   */
  public String getStrSize4Value() {
    return strSize4Value;
  }

  /**
   * @param strSize4Value the strSize4Value to set
   */
  public void setStrSize4Value(String strSize4Value) {
    this.strSize4Value = strSize4Value;
  }

  /**
   * @return the strSize4UOM
   */
  public String getStrSize4UOM() {
    return strSize4UOM;
  }

  /**
   * @param strSize4UOM the strSize4UOM to set
   */
  public void setStrSize4UOM(String strSize4UOM) {
    this.strSize4UOM = strSize4UOM;
  }

  /**
   * @return the alContainLatex
   */
  public ArrayList getAlContainLatex() {
    return alContainLatex;
  }

  /**
   * @param alContainLatex the alContainLatex to set
   */
  public void setAlContainLatex(ArrayList alContainLatex) {
    this.alContainLatex = alContainLatex;
  }

  /**
   * @return the alReqSteril
   */
  public ArrayList getAlReqSteril() {
    return alReqSteril;
  }

  /**
   * @param alReqSteril the alReqSteril to set
   */
  public void setAlReqSteril(ArrayList alReqSteril) {
    this.alReqSteril = alReqSteril;
  }

  /**
   * @return the alSterilMthod
   */
  public ArrayList getAlSterilMthod() {
    return alSterilMthod;
  }

  /**
   * @param alSterilMthod the alSterilMthod to set
   */
  public void setAlSterilMthod(ArrayList alSterilMthod) {
    this.alSterilMthod = alSterilMthod;
  }

  /**
   * @return the alIsDevice
   */
  public ArrayList getAlIsDevice() {
    return alIsDevice;
  }

  /**
   * @param alIsDevice the alIsDevice to set
   */
  public void setAlIsDevice(ArrayList alIsDevice) {
    this.alIsDevice = alIsDevice;
  }

  /**
   * @return the alSizeType
   */
  public ArrayList getAlSizeType() {
    return alSizeType;
  }

  /**
   * @param alSizeType the alSizeType to set
   */
  public void setAlSizeType(ArrayList alSizeType) {
    this.alSizeType = alSizeType;
  }

  /**
   * @return the alSizeUOM
   */
  public ArrayList getAlSizeUOM() {
    return alSizeUOM;
  }

  /**
   * @param alSizeUOM the alSizeUOM to set
   */
  public void setAlSizeUOM(ArrayList alSizeUOM) {
    this.alSizeUOM = alSizeUOM;
  }

  /**
   * @return the alProjectId
   */
  public ArrayList getAlProjectId() {
    return alProjectId;
  }

  /**
   * @param alProjectId the alProjectId to set
   */
  public void setAlProjectId(ArrayList alProjectId) {
    this.alProjectId = alProjectId;
  }

  /**
   * @return the hmAttrVal
   */
  public HashMap getHmAttrVal() {
    return hmAttrVal;
  }

  /**
   * @param hmAttrVal the hmAttrVal to set
   */
  public void setHmAttrVal(HashMap hmAttrVal) {
    this.hmAttrVal = hmAttrVal;
  }

  /**
   * @return the hmResult
   */
  public HashMap getHmResult() {
    return hmResult;
  }

  /**
   * @param hmResult the hmResult to set
   */
  public void setHmResult(HashMap hmResult) {
    this.hmResult = hmResult;
  }

  /**
   * @return the deletePartStr
   */
  public String getDeletePartStr() {
    return deletePartStr;
  }

  /**
   * @param deletePartStr the deletePartStr to set
   */
  public void setDeletePartStr(String deletePartStr) {
    this.deletePartStr = deletePartStr;
  }

  /**
   * @return the msgFl
   */
  public String getMsgFl() {
    return msgFl;
  }

  /**
   * @param msgFl the msgFl to set
   */
  public void setMsgFl(String msgFl) {
    this.msgFl = msgFl;
  }

  /**
   * @return the editDisable
   */
  public String getEditDisable() {
    return editDisable;
  }

  /**
   * @param editDisable the editDisable to set
   */
  public void setEditDisable(String editDisable) {
    this.editDisable = editDisable;
  }


}
