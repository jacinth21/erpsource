/**
 * FileName    : GmProjectMilestonesSetupForm.java 
 * Description :
 * Author      : rshah
 * Date        : Feb 5, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmProjectMilestonesSetupForm extends GmProjectDetailsForm {
	
	private String hinputString = "";
	private ArrayList alMilestoneList;

	/**
	 * @return the alMilestoneList
	 */
	public ArrayList getAlMilestoneList() {
		return alMilestoneList;
	}
	/**
	 * @param alMilestoneList the alMilestoneList to set
	 */
	public void setAlMilestoneList(ArrayList alMilestoneList) {
		this.alMilestoneList = alMilestoneList;
	}
}
