/**
 * FileName    : GmSystemDetailForm.java 
 * Description :
 * Author      : Elango
 */
package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmLogForm;

/**
 * @author Elango
 * 
 */
public class GmSystemDetailForm extends GmLogForm {

	/**
	 * setId String field for system id
	 */
	private String setId = "";

	/**
	 * Set Name String field for system Name
	 */
	private String setNm = "";

	/**
	 * setDesc String field for setDesc
	 */
	private String setDesc = "";

	/**
	 * setDetailDesc String field
	 */
	private String setDetailDesc = "";

	/**
	 * tabToLoad String field
	 */
	private String tabToLoad = "";

	/**
	 * setSetupSubmitAcc String field
	 */
	private String setSetupSubmitAcc = "";

	/**
	 * setSetupVoidAcc String field
	 */
	private String setSetupVoidAcc = "";

	/**
	 * setSetupPreviewAcc String field
	 */
	private String setSetupPreviewAcc = "";

	/**
	 * groupAttrId String field
	 */
	private String groupAttrId = "";

	/**
	 * segmentAttrId String field
	 */
	private String segmentAttrId = "";

	/**
	 * techniqueAttrId String field
	 */
	private String techniqueAttrId = "";

	/**
	 * publishInAttrId String field
	 */
	private String publishInAttrId = "";

	/**
	 * setAttrInputStr String field
	 */
	private String setAttrInputStr = "";

	/**
	 * attrTypeInputStr String field
	 */
	private String attrTypeInputStr = "";

	/**
	 * message String field
	 */
	private String message = "";

	/**
	 * mapcount String field
	 */
	private String mapcount = "";

	/**
	 * publishtype String field
	 */
	private String publishtype = "";

	/**
	 * hGroupIds String field
	 */
	private String hGroupIds = "";

	/**
	 * hSegmentIds String field
	 */
	private String hSegmentIds = "";

	/**
	 * hTechniqueIds String field
	 */
	private String hTechniqueIds = "";

	/**
	 * hPublishInIds String field
	 */
	private String hPublishInIds = "";

	/**
	 * divisionId String field
	 */
	private String divisionId = "";

	/**
	 * gridXmlData String field
	 */
	private String gridXmlData = "";

	/**
	 * strSysType String field
	 */
	private String strSysType = "";

	/**
	 * setSysPublishAcc String field
	 */
	private String setSysPublishAcc = "";
		
	/**
	 * frompage String field
	 */
	private String frompage = ""; //PMT_37772_Set release to Region in Set Setup screen
	/**
	 * alDivisionList ArrayList field
	 */
	private ArrayList alDivisionList = new ArrayList();

	/**
	 * alSystemList ArrayList field
	 */
	private ArrayList alSystemList = new ArrayList();

	/**
	 * alSystemGrpList ArrayList field
	 */
	private ArrayList alSystemGrpList = new ArrayList();

	/**
	 * alSystemSgmtList ArrayList field
	 */
	private ArrayList alSystemSgmtList = new ArrayList();

	/**
	 * alSystemTechList ArrayList field
	 */
	private ArrayList alSystemTechList = new ArrayList();

	/**
	 * alSystemPubToList ArrayList field
	 */
	private ArrayList alSystemPubToList = new ArrayList();

	/**
	 * systemReleaseDtls String field
	 */
	private String systemReleaseDtls = "";

	/**
	 * setGroupType String field
	 */
	private String setGroupType = "";

	/**
	 * regionId String field
	 */
	private String regionId = "";

	/**
	 * buttonAccessFl String field
	 */
	private String buttonAccessFl = "";

	/**
	 * @return buttonAccessFl
	 */
	public String getButtonAccessFl() {
		return buttonAccessFl;
	}

	/**
	 * @param buttonAccessFl
	 *            value to set
	 */
	public void setButtonAccessFl(String buttonAccessFl) {
		this.buttonAccessFl = buttonAccessFl;
	}

	/**
	 * @return the regionId
	 */

	public String getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId the regionId to set
	 *           
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	/**
	 * regionName String field
	 */
	private String regionName = "";

	/**
	 * @return the regionName
	 */
	public String getRegionName() {
		return regionName;
	}

	/**
	 * @param regionName the regionName to set
	 *            
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return systemReleaseDtls
	 */
	public String getSystemReleaseDtls() {
		return systemReleaseDtls;
	}

	/**
	 * @param systemReleaseDtls
	 */
	public void setSystemReleaseDtls(String systemReleaseDtls) {
		this.systemReleaseDtls = systemReleaseDtls;
	}

	/**
	 * @return setGroupType
	 */
	public String getSetGroupType() {
		return setGroupType;
	}

	/**
	 * @param setSetGroupType
	 */
	public void setSetGroupType(String setGroupType) {
		this.setGroupType = setGroupType;
	}

	/**
	 * @return the setId
	 */

	public String getSetId() {
		return setId;
	}

	/**
	 * @return the setNm
	 */
	public String getSetNm() {
		return setNm;
	}

	/**
	 * @return the setDesc
	 */
	public String getSetDesc() {
		return setDesc;
	}

	/**
	 * @return the setDetailDesc
	 */
	public String getSetDetailDesc() {
		return setDetailDesc;
	}

	/**
	 * @return the tabToLoad
	 */
	public String getTabToLoad() {
		return tabToLoad;
	}

	/**
	 * @return the setSetupSubmitAcc
	 */
	public String getSetSetupSubmitAcc() {
		return setSetupSubmitAcc;
	}

	/**
	 * @return the setSetupVoidAcc
	 */
	public String getSetSetupVoidAcc() {
		return setSetupVoidAcc;
	}

	/**
	 * @return the setSetupPreviewAcc
	 */
	public String getSetSetupPreviewAcc() {
		return setSetupPreviewAcc;
	}

	/**
	 * @return the groupAttrId
	 */
	public String getGroupAttrId() {
		return groupAttrId;
	}

	/**
	 * @return the segmentAttrId
	 */
	public String getSegmentAttrId() {
		return segmentAttrId;
	}

	/**
	 * @return the techniqueAttrId
	 */
	public String getTechniqueAttrId() {
		return techniqueAttrId;
	}

	/**
	 * @return the publishInAttrId
	 */
	public String getPublishInAttrId() {
		return publishInAttrId;
	}

	/**
	 * @return the setAttrInputStr
	 */
	public String getSetAttrInputStr() {
		return setAttrInputStr;
	}

	/**
	 * @return the attrTypeInputStr
	 */
	public String getAttrTypeInputStr() {
		return attrTypeInputStr;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the mapcount
	 */
	public String getMapcount() {
		return mapcount;
	}

	/**
	 * @return the publishtype
	 */
	public String getPublishtype() {
		return publishtype;
	}

	/**
	 * @return the hGroupIds
	 */
	public String gethGroupIds() {
		return hGroupIds;
	}

	/**
	 * @return the hSegmentIds
	 */
	public String gethSegmentIds() {
		return hSegmentIds;
	}

	/**
	 * @return the hTechniqueIds
	 */
	public String gethTechniqueIds() {
		return hTechniqueIds;
	}

	/**
	 * @return the hPublishInIds
	 */
	public String gethPublishInIds() {
		return hPublishInIds;
	}

	/**
	 * @return the divisionId
	 */
	public String getDivisionId() {
		return divisionId;
	}

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @return the strSysType
	 */
	public String getStrSysType() {
		return strSysType;
	}

	/**
	 * @return the setSysPublishAcc
	 */
	public String getSetSysPublishAcc() {
		return setSysPublishAcc;
	}

	/**
	 * @return the alDivisionList
	 */
	public ArrayList getAlDivisionList() {
		return alDivisionList;
	}

	/**
	 * @return the alSystemList
	 */
	public ArrayList getAlSystemList() {
		return alSystemList;
	}

	/**
	 * @return the alSystemGrpList
	 */
	public ArrayList getAlSystemGrpList() {
		return alSystemGrpList;
	}

	/**
	 * @return the alSystemSgmtList
	 */
	public ArrayList getAlSystemSgmtList() {
		return alSystemSgmtList;
	}

	/**
	 * @return the alSystemTechList
	 */
	public ArrayList getAlSystemTechList() {
		return alSystemTechList;
	}

	/**
	 * @return the alSystemPubToList
	 */
	public ArrayList getAlSystemPubToList() {
		return alSystemPubToList;
	}

	/**
	 * @param setId
	 *  the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}

	/**
	 * @param setNm
	 * the setNm to set
	 */
	public void setSetNm(String setNm) {
		this.setNm = setNm;
	}

	/**
	 * @param setDesc
	 *  the setDesc to set
	 */
	public void setSetDesc(String setDesc) {
		this.setDesc = setDesc;
	}

	/**
	 * @param setDetailDesc
	 * the setDetailDesc to set
	 */
	public void setSetDetailDesc(String setDetailDesc) {
		this.setDetailDesc = setDetailDesc;
	}

	/**
	 * @param tabToLoad
	 * the tabToLoad to set
	 */
	public void setTabToLoad(String tabToLoad) {
		this.tabToLoad = tabToLoad;
	}

	/**
	 * @param setSetupSubmitAcc
	 * the setSetupSubmitAcc to set
	 */
	public void setSetSetupSubmitAcc(String setSetupSubmitAcc) {
		this.setSetupSubmitAcc = setSetupSubmitAcc;
	}

	/**
	 * @param setSetupVoidAcc
	 * the setSetupVoidAcc to set
	 */
	public void setSetSetupVoidAcc(String setSetupVoidAcc) {
		this.setSetupVoidAcc = setSetupVoidAcc;
	}

	/**
	 * @param setSetupPreviewAcc
	 * the setSetupPreviewAcc to set
	 */
	public void setSetSetupPreviewAcc(String setSetupPreviewAcc) {
		this.setSetupPreviewAcc = setSetupPreviewAcc;
	}

	/**
	 * @param groupAttrId
	 * the groupAttrId to set
	 */
	public void setGroupAttrId(String groupAttrId) {
		this.groupAttrId = groupAttrId;
	}

	/**
	 * @param segmentAttrId
	 * the segmentAttrId to set
	 */
	public void setSegmentAttrId(String segmentAttrId) {
		this.segmentAttrId = segmentAttrId;
	}

	/**
	 * @param techniqueAttrId
	 * the techniqueAttrId to set
	 */
	public void setTechniqueAttrId(String techniqueAttrId) {
		this.techniqueAttrId = techniqueAttrId;
	}

	/**
	 * @param publishInAttrId
	 * the publishInAttrId to set
	 */
	public void setPublishInAttrId(String publishInAttrId) {
		this.publishInAttrId = publishInAttrId;
	}

	/**
	 * @param setAttrInputStr
	 * the setAttrInputStr to set
	 */
	public void setSetAttrInputStr(String setAttrInputStr) {
		this.setAttrInputStr = setAttrInputStr;
	}

	/**
	 * @param attrTypeInputStr
	 * the attrTypeInputStr to set
	 */
	public void setAttrTypeInputStr(String attrTypeInputStr) {
		this.attrTypeInputStr = attrTypeInputStr;
	}

	/**
	 * @param message
	 * the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @param mapcount
	 * the mapcount to set
	 */
	public void setMapcount(String mapcount) {
		this.mapcount = mapcount;
	}

	/**
	 * @param publishtype
	 * the publishtype to set
	 */
	public void setPublishtype(String publishtype) {
		this.publishtype = publishtype;
	}

	/**
	 * @param hGroupIds
	 * the hGroupIds to set
	 */
	public void sethGroupIds(String hGroupIds) {
		this.hGroupIds = hGroupIds;
	}

	/**
	 * @param hSegmentIds
	 * the hSegmentIds to set
	 */
	public void sethSegmentIds(String hSegmentIds) {
		this.hSegmentIds = hSegmentIds;
	}

	/**
	 * @param hTechniqueIds
	 * the hTechniqueIds to set
	 */
	public void sethTechniqueIds(String hTechniqueIds) {
		this.hTechniqueIds = hTechniqueIds;
	}

	/**
	 * @param hPublishInIds
	 * the hPublishInIds to set
	 */
	public void sethPublishInIds(String hPublishInIds) {
		this.hPublishInIds = hPublishInIds;
	}

	/**
	 * @param divisionId
	 * the divisionId to set
	 */
	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}

	/**
	 * @param gridXmlData
	 * the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @param strSysType
	 * the strSysType to set
	 */
	public void setStrSysType(String strSysType) {
		this.strSysType = strSysType;
	}

	/**
	 * @param setSysPublishAcc
	 * the setSysPublishAcc to set
	 */
	public void setSetSysPublishAcc(String setSysPublishAcc) {
		this.setSysPublishAcc = setSysPublishAcc;
	}

	/**
	 * @param alDivisionList
	 * the alDivisionList to set
	 */
	public void setAlDivisionList(ArrayList alDivisionList) {
		this.alDivisionList = alDivisionList;
	}

	/**
	 * @param alSystemList
	 * the alSystemList to set
	 */
	public void setAlSystemList(ArrayList alSystemList) {
		this.alSystemList = alSystemList;
	}

	/**
	 * @param alSystemGrpList
	 * the alSystemGrpList to set
	 */
	public void setAlSystemGrpList(ArrayList alSystemGrpList) {
		this.alSystemGrpList = alSystemGrpList;
	}

	/**
	 * @param alSystemSgmtList
	 *  the alSystemSgmtList to set
	 */
	public void setAlSystemSgmtList(ArrayList alSystemSgmtList) {
		this.alSystemSgmtList = alSystemSgmtList;
	}

	/**
	 * @param alSystemTechList 
	 * the alSystemTechList to set
	 *            
	 */
	public void setAlSystemTechList(ArrayList alSystemTechList) {
		this.alSystemTechList = alSystemTechList;
	}

	/**
	 * @param alSystemPubToList 
	 * the alSystemPubToList to set
	 *            
	 */
	public void setAlSystemPubToList(ArrayList alSystemPubToList) {
		this.alSystemPubToList = alSystemPubToList;
	}

	/**
	 * @return the frompage
	 */
	public String getFrompage() {
		return frompage;
	}

	/**
	 * @param frompage the frompage to set
	 */
	public void setFrompage(String frompage) {
		this.frompage = frompage;
	}

}
