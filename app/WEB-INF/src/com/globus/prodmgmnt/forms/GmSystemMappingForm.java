/**
 * FileName    : GmSystemMappingForm.java
 * Description :
 * Author      : Elango
 */
package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmLogForm;

/**
 * @author Elango
 *
 */
public class GmSystemMappingForm extends GmLogForm {
	private String systemId = "";
	private String setId = "";
	private String setType = "";
	private String setBaseLine = "";
	private String setShared = "";
	private String gridData = "";
	private String setGroup= "";
	private String mapAccessFlag= "";
	
	private String stdsetidsInputStr = "";
	private String addlsetidsInputStr = "";
	private String loanerstdsetidsInputStr = "";
	private String loaneraddlsetidsInputStr = "";

	private ArrayList alSetType = new ArrayList();
	private ArrayList alSetMapDtls = new ArrayList();
	private ArrayList alSetShareType = new ArrayList();
	
	
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public ArrayList getAlSetType() {
		return alSetType;
	}
	public void setAlSetType(ArrayList alSetType) {
		this.alSetType = alSetType;
	}
	public ArrayList getAlSetShareType() {
		return alSetShareType;
	}
	public void setAlSetShareType(ArrayList alSetShareType) {
		this.alSetShareType = alSetShareType;
	}
	public String getStdsetidsInputStr() {
		return stdsetidsInputStr;
	}
	public void setStdsetidsInputStr(String stdsetidsInputStr) {
		this.stdsetidsInputStr = stdsetidsInputStr;
	}
	public String getAddlsetidsInputStr() {
		return addlsetidsInputStr;
	}
	public void setAddlsetidsInputStr(String addlsetidsInputStr) {
		this.addlsetidsInputStr = addlsetidsInputStr;
	}
	public String getLoanerstdsetidsInputStr() {
		return loanerstdsetidsInputStr;
	}
	public void setLoanerstdsetidsInputStr(String loanerstdsetidsInputStr) {
		this.loanerstdsetidsInputStr = loanerstdsetidsInputStr;
	}
	public String getLoaneraddlsetidsInputStr() {
		return loaneraddlsetidsInputStr;
	}
	public void setLoaneraddlsetidsInputStr(String loaneraddlsetidsInputStr) {
		this.loaneraddlsetidsInputStr = loaneraddlsetidsInputStr;
	}
	public String getSetType() {
		return setType;
	}
	public void setSetType(String setType) {
		this.setType = setType;
	}
	public String getSetBaseLine() {
		return setBaseLine;
	}
	public void setSetBaseLine(String setBaseLine) {
		this.setBaseLine = setBaseLine;
	}
	public String getSetShared() {
		return setShared;
	}
	public void setSetShared(String setShared) {
		this.setShared = setShared;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public ArrayList getAlSetMapDtls() {
		return alSetMapDtls;
	}
	public void setAlSetMapDtls(ArrayList alSetMapDtls) {
		this.alSetMapDtls = alSetMapDtls;
	}
	public String getGridData() {
		return gridData;
	}
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	public String getSetGroup() {
		return setGroup;
	}
	public void setSetGroup(String setGroup) {
		this.setGroup = setGroup;
	}	
	public String getMapAccessFlag() {
		return mapAccessFlag;
	}
	public void setMapAccessFlag(String mapAccessFlag) {
		this.mapAccessFlag = mapAccessFlag;
	}
		
}
