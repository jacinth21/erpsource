/**
 * 
 */
package com.globus.prodmgmnt.forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

/**
 * @author HReddi
 * 
 */
public class GmSubComponentMappingForm extends GmLogForm {

  private String strXMLGrid = "";
  private String partNum = "";
  private String subComponentNum = "";
  private String subComponentId = "";
  private String strInputString = "";
  private String successMessage = ""; 
  private String partLike = "";
  private String subPartLike = "";
  private String pricingAccessFl = "";
  private String qualityAccessFl = "";
  private String headerMessage = "";
  private String strErrorPartMsg = "";
  private String strErrorSubPartMsg = "";
  private String partInputString = "";
  private String duplicateSubCompMessage = "";
  private String screenAccessFl = "";
  
  private ArrayList alLikeOptions = new ArrayList();
  
  
/**
 * @return the strXMLGrid
 */
public String getStrXMLGrid() {
    return strXMLGrid;
}
/**
 * @param strXMLGrid the strXMLGrid to set
 */
public void setStrXMLGrid(String strXMLGrid) {
    this.strXMLGrid = strXMLGrid;
}
/**
 * @return the partNum
 */
public String getPartNum() {
    return partNum;
}
/**
 * @param partNum the partNum to set
 */
public void setPartNum(String partNum) {
    this.partNum = partNum;
}

/**
 * @return the subComponentNum
 */
public String getSubComponentNum() {
    return subComponentNum;
}
/**
 * @param subComponentNum the subComponentNum to set
 */
public void setSubComponentNum(String subComponentNum) {
    this.subComponentNum = subComponentNum;
}

/**
 * @return the subComponentId
 */
public String getSubComponentId() {
    return subComponentId;
}
/**
 * @param subComponentId the subComponentId to set
 */
public void setSubComponentId(String subComponentId) {
    this.subComponentId = subComponentId;
}
/**
 * @return the strInputString
 */
public String getStrInputString() {
    return strInputString;
}
/**
 * @param strInputString the strInputString to set
 */
public void setStrInputString(String strInputString) {
    try {
	this.strInputString = new String(strInputString.getBytes("UTF-8"), "UTF-8");
    }catch (UnsupportedEncodingException ex) {
	
    }    
}
/**
 * @return the successMessage
 */
public String getSuccessMessage() {
    return successMessage;
}
/**
 * @param successMessage the successMessage to set
 */
public void setSuccessMessage(String successMessage) {
    this.successMessage = successMessage;
}
/**
 * @return the partLike
 */
public String getPartLike() {
    return partLike;
}
/**
 * @param partLike the partLike to set
 */
public void setPartLike(String partLike) {
    this.partLike = partLike;
}
/**
 * @return the subPartLike
 */
public String getSubPartLike() {
    return subPartLike;
}
/**
 * @param subPartLike the subPartLike to set
 */
public void setSubPartLike(String subPartLike) {
    this.subPartLike = subPartLike;
}

/**
 * @return the pricingAccessFl
 */
public String getPricingAccessFl() {
    return pricingAccessFl;
}
/**
 * @param pricingAccessFl the pricingAccessFl to set
 */
public void setPricingAccessFl(String pricingAccessFl) {
    this.pricingAccessFl = pricingAccessFl;
}
/**
 * @return the qualityAccessFl
 */
public String getQualityAccessFl() {
    return qualityAccessFl;
}
/**
 * @param qualityAccessFl the qualityAccessFl to set
 */
public void setQualityAccessFl(String qualityAccessFl) {
    this.qualityAccessFl = qualityAccessFl;
}

/**
 * @return the headerMessage
 */
public String getHeaderMessage() {
    return headerMessage;
}
/**
 * @param headerMessage the headerMessage to set
 */
public void setHeaderMessage(String headerMessage) {
    this.headerMessage = headerMessage;
}
/**
 * @return the strErrorPartMsg
 */
public String getStrErrorPartMsg() {
    return strErrorPartMsg;
}
/**
 * @param strErrorPartMsg the strErrorPartMsg to set
 */
public void setStrErrorPartMsg(String strErrorPartMsg) {
    this.strErrorPartMsg = strErrorPartMsg;
}
/**
 * @return the strErrorSubPartMsg
 */
public String getStrErrorSubPartMsg() {
    return strErrorSubPartMsg;
}
/**
 * @param strErrorSubPartMsg the strErrorSubPartMsg to set
 */
public void setStrErrorSubPartMsg(String strErrorSubPartMsg) {
    this.strErrorSubPartMsg = strErrorSubPartMsg;
}
/**
 * @return the partInputString
 */
public String getPartInputString() {
    return partInputString;
}
/**
 * @param partInputString the partInputString to set
 */
public void setPartInputString(String partInputString) {
    this.partInputString = partInputString;
}
/**
 * @return the alLikeOptions
 */
public ArrayList getAlLikeOptions() {
    return alLikeOptions;
}
/**
 * @param alLikeOptions the alLikeOptions to set
 */
public void setAlLikeOptions(ArrayList alLikeOptions) {
    this.alLikeOptions = alLikeOptions;
}
/**
 * @return the duplicateSubCompMessage
 */
public String getDuplicateSubCompMessage() {
    return duplicateSubCompMessage;
}
/**
 * @param duplicateSubCompMessage the duplicateSubCompMessage to set
 */
public void setDuplicateSubCompMessage(String duplicateSubCompMessage) {
    this.duplicateSubCompMessage = duplicateSubCompMessage;
}
/**
 * @return the screenAccessFl
 */
public String getScreenAccessFl() {
    return screenAccessFl;
}
/**
 * @param screenAccessFl the screenAccessFl to set
 */
public void setScreenAccessFl(String screenAccessFl) {
    this.screenAccessFl = screenAccessFl;
}


}
