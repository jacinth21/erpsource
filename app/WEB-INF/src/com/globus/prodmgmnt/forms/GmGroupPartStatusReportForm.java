package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

public class GmGroupPartStatusReportForm extends GmCommonForm{

	
	 private String    hid 	= "";
	 private String    htype 	= "";
	 private String    grouptype 	= "";
	 private String    priceband	= "";
	 private String    publishfl 	= "";
	 private String    constructfl 	= ""; 
	 private String    owner 	= ""; 
	 private String    groupnm 	= ""; 
	 private String gridXmlData = "";
	 
	 
	public String getGridXmlData() {
		return gridXmlData;
	}
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the hid
	 */
	public String getHid() {
		return hid;
	}
	/**
	 * @param hid the hid to set
	 */
	public void setHid(String hid) {
		this.hid = hid;
	}
	/**
	 * @return the htype
	 */
	public String getHtype() {
		return htype;
	}
	/**
	 * @param htype the htype to set
	 */
	public void setHtype(String htype) {
		this.htype = htype;
	}
	/**
	 * @return the grouptype
	 */
	public String getGrouptype() {
		return grouptype;
	}
	/**
	 * @param grouptype the grouptype to set
	 */
	public void setGrouptype(String grouptype) {
		this.grouptype = grouptype;
	}
	/**
	 * @return the priceband
	 */
	public String getPriceband() {
		return priceband;
	}
	/**
	 * @param priceband the priceband to set
	 */
	public void setPriceband(String priceband) {
		this.priceband = priceband;
	}
	/**
	 * @return the publishfl
	 */
	public String getPublishfl() {
		return publishfl;
	}
	/**
	 * @param publishfl the publishfl to set
	 */
	public void setPublishfl(String publishfl) {
		this.publishfl = publishfl;
	}
	/**
	 * @return the constructfl
	 */
	public String getConstructfl() {
		return constructfl;
	}
	/**
	 * @param constructfl the constructfl to set
	 */
	public void setConstructfl(String constructfl) {
		this.constructfl = constructfl;
	}
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}
	/**
	 * @return the groupnm
	 */
	public String getGroupnm() {
		return groupnm;
	}
	/**
	 * @param groupnm the groupnm to set
	 */
	public void setGroupnm(String groupnm) {
		this.groupnm = groupnm;
	}
	  
	  
}
