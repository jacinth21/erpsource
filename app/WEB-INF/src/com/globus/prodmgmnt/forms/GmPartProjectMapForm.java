package com.globus.prodmgmnt.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmPartProjectMapForm extends GmCommonForm {

  private String partnumber = "";
  private String hassociatedprojects = "";
  private String hvoidString = "";
  private String gridXmlData = "";
  private String companyId = "";
  private String editDisable = "";

  ArrayList alAvailableProject = new ArrayList();
  ArrayList alAssproject = new ArrayList();
  ArrayList alCompany = new ArrayList();

  /**
   * @return the partnumber
   */
  public String getPartnumber() {
    return partnumber;
  }

  /**
   * @param partnumber the partnumber to set
   */
  public void setPartnumber(String partnumber) {
    this.partnumber = partnumber;
  }


  /**
   * @return the hassociatedprojects
   */
  public String getHassociatedprojects() {
    return hassociatedprojects;
  }

  /**
   * @param hassociatedprojects the hassociatedprojects to set
   */
  public void setHassociatedprojects(String hassociatedprojects) {
    this.hassociatedprojects = hassociatedprojects;
  }

  /**
   * @return the alAvailableProject
   */
  public ArrayList getAlAvailableProject() {
    return alAvailableProject;
  }

  /**
   * @param alAvailableProject to set
   */
  public void setAlAvailableProject(ArrayList alAvailableProject) {
    this.alAvailableProject = alAvailableProject;
  }

  /**
   * @return the alAssproject
   */
  public ArrayList getAlAssproject() {
    return alAssproject;
  }

  /**
   * @param alAssproject to set
   */
  public void setAlAssproject(ArrayList alAssproject) {
    this.alAssproject = alAssproject;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the hvoidString
   */
  public String getHvoidString() {
    return hvoidString;
  }

  /**
   * @param hvoidString the partnumber to set
   */
  public void setHvoidString(String hvoidString) {
    this.hvoidString = hvoidString;
  }

  /**
   * @return the companyId
   */
  public String getCompanyId() {
    return companyId;
  }

  /**
   * @param companyId the companyId to set
   */
  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  /**
   * @return the alCompany
   */
  public ArrayList getAlCompany() {
    return alCompany;
  }

  /**
   * @param alCompany the alCompany to set
   */
  public void setAlCompany(ArrayList alCompany) {
    this.alCompany = alCompany;
  }

  /**
   * @return the editDisable
   */
  public String getEditDisable() {
    return editDisable;
  }

  /**
   * @param editDisable the editDisable to set
   */
  public void setEditDisable(String editDisable) {
    this.editDisable = editDisable;
  }

}
