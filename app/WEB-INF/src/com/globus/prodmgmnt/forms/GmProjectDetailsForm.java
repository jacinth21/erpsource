/**
 * FileName : GmProjectDetailsForm.java Description : Author : sthadeshwar Date : Feb 9, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmLogForm;

/**
 * @author sthadeshwar
 * 
 */
public class GmProjectDetailsForm extends GmLogForm {

  private String prjNm = "";
  private String prjDesc = "";
  private String prjId = "";
  private String prjStatusId = "";
  private String prjStatus = "";
  private String prjDevClass = "";
  private String prjGrpId = "";
  private String prjSgmtId = "";
  private String prjCrtdBy = "";
  private String prjType = "";
  private String prjIniBy = "";
  private String prjAprvdBy = "";
  private String chkPrjDevClass = "";
  private String strEnableApproval = "false";
  private String strEnableChangeStatus = "false";
  private String strEnableSubmit = "false";
  private String strEnableReset = "false";
  private String divisionId = "";
  private String deviceClassGrpId = "";
  private String groupGrpId = "";
  private String segmentGrpId = "";
  private String techniqueGrpId = "";
  private String projAttrInputStr = "";
  private String attrTypeInputStr = "";
  private String prjDtlDesc = "";
  private String hDeviceIds = "";
  private String hGroupIds = "";
  private String hSegmentIds = "";
  private String hTechniqueIds = "";
  private String gridXmlData = "";
  private String projectID = "";
  private String projectReleaseCompanyId = "";



  private ArrayList alTabs = new ArrayList();
  private ArrayList alPrjList = new ArrayList();
  private ArrayList alPrjTypeList = new ArrayList();
  private ArrayList alPrjGrpList = new ArrayList();
  private ArrayList alPrjSgmtList = new ArrayList();
  private ArrayList alPrjIniList = new ArrayList();
  private ArrayList alPrjAprvdList = new ArrayList();
  private ArrayList alDevClass = new ArrayList();
  private ArrayList alPrjStatus = new ArrayList(); // Used in projects report
  private ArrayList alDivisionList = new ArrayList();
  private ArrayList alPrjTechList = new ArrayList();
  private ArrayList alParentCompany = new ArrayList();
  
  private List lresult;
  private String hinputString = "";

  
  public String getProjectReleaseCompanyId() {
	return projectReleaseCompanyId;
  }

  public void setProjectReleaseCompanyId(String projectReleaseCompanyId) {
	this.projectReleaseCompanyId = projectReleaseCompanyId;
  }

  public ArrayList getAlParentCompany() {
	return alParentCompany;
  }

  public void setAlParentCompany(ArrayList alParentCompany) {
		this.alParentCompany = alParentCompany;
  }

/**
   * @return the prjNm
   */
  public String getPrjNm() {
    return prjNm;
  }

  /**
   * @param prjNm the prjNm to set
   */
  public void setPrjNm(String prjNm) {
    this.prjNm = prjNm;
  }

  /**
   * @return the prjDesc
   */
  public String getPrjDesc() {
    return prjDesc;
  }

  /**
   * @param prjDesc the prjDesc to set
   */
  public void setPrjDesc(String prjDesc) {
    this.prjDesc = prjDesc;
  }

  /**
   * @return the prjId
   */
  public String getPrjId() {
    return prjId;
  }

  /**
   * @param prjId the prjId to set
   */
  public void setPrjId(String prjId) {
    this.prjId = prjId;
  }

  /**
   * @return the prjStatusId
   */
  public String getPrjStatusId() {
    return prjStatusId;
  }

  /**
   * @param prjStatusId the prjStatusId to set
   */
  public void setPrjStatusId(String prjStatusId) {
    this.prjStatusId = prjStatusId;
  }

  /**
   * @return the prjStatus
   */
  public String getPrjStatus() {
    return prjStatus;
  }

  /**
   * @param prjStatus the prjStatus to set
   */
  public void setPrjStatus(String prjStatus) {
    this.prjStatus = prjStatus;
  }

  /**
   * @return the prjDevClass
   */
  public String getPrjDevClass() {
    return prjDevClass;
  }

  /**
   * @param prjDevClass the prjDevClass to set
   */
  public void setPrjDevClass(String prjDevClass) {
    this.prjDevClass = prjDevClass;
  }

  /**
   * @return the prjGrpId
   */
  public String getPrjGrpId() {
    return prjGrpId;
  }

  /**
   * @param prjGrpId the prjGrpId to set
   */
  public void setPrjGrpId(String prjGrpId) {
    this.prjGrpId = prjGrpId;
  }

  /**
   * @return the prjSgmtId
   */
  public String getPrjSgmtId() {
    return prjSgmtId;
  }

  /**
   * @param prjSgmtId the prjSgmtId to set
   */
  public void setPrjSgmtId(String prjSgmtId) {
    this.prjSgmtId = prjSgmtId;
  }

  /**
   * @return the prjCrtdBy
   */
  public String getPrjCrtdBy() {
    return prjCrtdBy;
  }

  /**
   * @param prjCrtdBy the prjCrtdBy to set
   */
  public void setPrjCrtdBy(String prjCrtdBy) {
    this.prjCrtdBy = prjCrtdBy;
  }

  /**
   * @return the prjType
   */
  public String getPrjType() {
    return prjType;
  }

  /**
   * @param prjType the prjType to set
   */
  public void setPrjType(String prjType) {
    this.prjType = prjType;
  }

  /**
   * @return the prjIniBy
   */
  public String getPrjIniBy() {
    return prjIniBy;
  }

  /**
   * @param prjIniBy the prjIniBy to set
   */
  public void setPrjIniBy(String prjIniBy) {
    this.prjIniBy = prjIniBy;
  }

  /**
   * @return the prjAprvdBy
   */
  public String getPrjAprvdBy() {
    return prjAprvdBy;
  }

  /**
   * @param prjAprvdBy the prjAprvdBy to set
   */
  public void setPrjAprvdBy(String prjAprvdBy) {
    this.prjAprvdBy = prjAprvdBy;
  }

  /**
   * @return the chkPrjDevClass
   */
  public String getChkPrjDevClass() {
    return chkPrjDevClass;
  }

  /**
   * @param chkPrjDevClass the chkPrjDevClass to set
   */
  public void setChkPrjDevClass(String chkPrjDevClass) {
    this.chkPrjDevClass = chkPrjDevClass;
  }

  /**
   * @return the alTabs
   */
  public ArrayList getAlTabs() {
    return alTabs;
  }

  /**
   * @param alTabs the alTabs to set
   */
  public void setAlTabs(ArrayList alTabs) {
    this.alTabs = alTabs;
  }

  /**
   * @return the alPrjList
   */
  public ArrayList getAlPrjList() {
    return alPrjList;
  }

  /**
   * @param alPrjList the alPrjList to set
   */
  public void setAlPrjList(ArrayList alPrjList) {
    this.alPrjList = alPrjList;
  }

  /**
   * @return the alPrjTypeList
   */
  public ArrayList getAlPrjTypeList() {
    return alPrjTypeList;
  }

  /**
   * @param alPrjTypeList the alPrjTypeList to set
   */
  public void setAlPrjTypeList(ArrayList alPrjTypeList) {
    this.alPrjTypeList = alPrjTypeList;
  }

  /**
   * @return the alPrjGrpList
   */
  public ArrayList getAlPrjGrpList() {
    return alPrjGrpList;
  }

  /**
   * @param alPrjGrpList the alPrjGrpList to set
   */
  public void setAlPrjGrpList(ArrayList alPrjGrpList) {
    this.alPrjGrpList = alPrjGrpList;
  }

  /**
   * @return the alPrjSgmtList
   */
  public ArrayList getAlPrjSgmtList() {
    return alPrjSgmtList;
  }

  /**
   * @param alPrjSgmtList the alPrjSgmtList to set
   */
  public void setAlPrjSgmtList(ArrayList alPrjSgmtList) {
    this.alPrjSgmtList = alPrjSgmtList;
  }

  /**
   * @return the alPrjIniList
   */
  public ArrayList getAlPrjIniList() {
    return alPrjIniList;
  }

  /**
   * @param alPrjIniList the alPrjIniList to set
   */
  public void setAlPrjIniList(ArrayList alPrjIniList) {
    this.alPrjIniList = alPrjIniList;
  }

  /**
   * @return the alPrjAprvdList
   */
  public ArrayList getAlPrjAprvdList() {
    return alPrjAprvdList;
  }

  /**
   * @param alPrjAprvdList the alPrjAprvdList to set
   */
  public void setAlPrjAprvdList(ArrayList alPrjAprvdList) {
    this.alPrjAprvdList = alPrjAprvdList;
  }

  /**
   * @return the alDevClass
   */
  public ArrayList getAlDevClass() {
    return alDevClass;
  }

  /**
   * @param alDevClass the alDevClass to set
   */
  public void setAlDevClass(ArrayList alDevClass) {
    this.alDevClass = alDevClass;
  }

  /**
   * @return the lresult
   */
  public List getLresult() {
    return lresult;
  }

  /**
   * @param lresult the lresult to set
   */
  public void setLresult(List lresult) {
    this.lresult = lresult;
  }

  /**
   * @return the hinputString
   */
  public String getHinputString() {
    return hinputString;
  }

  /**
   * @param hinputString the hinputString to set
   */
  public void setHinputString(String hinputString) {
    this.hinputString = hinputString;
  }

  /**
   * @return the strEnableApproval
   */
  public String getStrEnableApproval() {
    return strEnableApproval;
  }

  /**
   * @param strEnableApproval the strEnableApproval to set
   */
  public void setStrEnableApproval(String strEnableApproval) {
    this.strEnableApproval = strEnableApproval;
  }


  /**
   * @return the strEnableSubmit
   */
  public String getstrEnableSubmit() {
    return strEnableSubmit;
  }

  /**
   * @param strEnableSubmit the strEnableSubmit to set
   */
  public void setstrEnableSubmit(String strEnableSubmit) {
    this.strEnableSubmit = strEnableSubmit;
  }


  /**
   * @return the strEnableReset
   */
  public String getstrEnableReset() {
    return strEnableReset;
  }

  /**
   * @param strEnableReset the strEnableReset to set
   */
  public void setstrEnableReset(String strEnableReset) {
    this.strEnableReset = strEnableReset;
  }

  /**
   * @return the alPrjStatus
   */
  public ArrayList getAlPrjStatus() {
    return alPrjStatus;
  }

  /**
   * @param alPrjStatus the alPrjStatus to set
   */
  public void setAlPrjStatus(ArrayList alPrjStatus) {
    this.alPrjStatus = alPrjStatus;
  }

  /**
   * @return the strEnableChangeStatus
   */
  public String getStrEnableChangeStatus() {
    return strEnableChangeStatus;
  }

  /**
   * @param strEnableChangeStatus the strEnableChangeStatus to set
   */
  public void setStrEnableChangeStatus(String strEnableChangeStatus) {
    this.strEnableChangeStatus = strEnableChangeStatus;
  }

  /**
   * @return the alDivisionList
   */
  public ArrayList getAlDivisionList() {
    return alDivisionList;
  }

  /**
   * @param alDivisionList the alDivisionList to set
   */
  public void setAlDivisionList(ArrayList alDivisionList) {
    this.alDivisionList = alDivisionList;
  }

  /**
   * @return the divisionId
   */
  public String getDivisionId() {
    return divisionId;
  }

  /**
   * @param divisionId the divisionId to set
   */
  public void setDivisionId(String divisionId) {
    this.divisionId = divisionId;
  }

  public String getDeviceClassGrpId() {
    return deviceClassGrpId;
  }


  public void setDeviceClassGrpId(String deviceClassGrpId) {
    this.deviceClassGrpId = deviceClassGrpId;
  }

  public String getGroupGrpId() {
    return groupGrpId;
  }

  public void setGroupGrpId(String groupGrpId) {
    this.groupGrpId = groupGrpId;
  }

  public String getSegmentGrpId() {
    return segmentGrpId;
  }

  public void setSegmentGrpId(String segmentGrpId) {
    this.segmentGrpId = segmentGrpId;
  }

  public String getTechniqueGrpId() {
    return techniqueGrpId;
  }

  public void setTechniqueGrpId(String techniqueGrpId) {
    this.techniqueGrpId = techniqueGrpId;
  }

  public String getProjAttrInputStr() {
    return projAttrInputStr;
  }

  public void setProjAttrInputStr(String projAttrInputStr) {
    this.projAttrInputStr = projAttrInputStr;
  }

  public String getAttrTypeInputStr() {
    return attrTypeInputStr;
  }

  public void setAttrTypeInputStr(String attrTypeInputStr) {
    this.attrTypeInputStr = attrTypeInputStr;
  }

  public String getPrjDtlDesc() {
    return prjDtlDesc;
  }

  public void setPrjDtlDesc(String prjDtlDesc) {
    this.prjDtlDesc = prjDtlDesc;
  }

  public String gethDeviceIds() {
    return hDeviceIds;
  }

  public void sethDeviceIds(String hDeviceIds) {
    this.hDeviceIds = hDeviceIds;
  }

  public String gethGroupIds() {
    return hGroupIds;
  }

  public void sethGroupIds(String hGroupIds) {
    this.hGroupIds = hGroupIds;
  }

  public String gethSegmentIds() {
    return hSegmentIds;
  }

  public void sethSegmentIds(String hSegmentIds) {
    this.hSegmentIds = hSegmentIds;
  }

  public String gethTechniqueIds() {
    return hTechniqueIds;
  }

  public void sethTechniqueIds(String hTechniqueIds) {
    this.hTechniqueIds = hTechniqueIds;
  }

  public ArrayList getAlPrjTechList() {
    return alPrjTechList;
  }

  public void setAlPrjTechList(ArrayList alPrjTechList) {
    this.alPrjTechList = alPrjTechList;
  }

  public String getGridXmlData() {
    return gridXmlData;
  }

  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  public String getProjectID() {
    return projectID;
  }

  public void setProjectID(String projectID) {
    this.projectID = projectID;
  }

}
