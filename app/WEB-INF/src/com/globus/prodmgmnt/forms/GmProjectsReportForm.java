/**
 * FileName    : GmProjectsReportForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Feb 19, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.forms;

import java.util.ArrayList;

/**
 * @author sthadeshwar
 *
 */
public class GmProjectsReportForm extends GmProjectDetailsForm {

	private String strMilestone = "";
	private ArrayList alMilestones = new ArrayList();
	
	private String[] selectedMilestones = new String[30];
	
	private ArrayList alSelection = new ArrayList();
	private String strSelection = "";
	
	/**
	 * @return the strMilestone
	 */
	public String getStrMilestone() {
		return strMilestone;
	}

	/**
	 * @param strMilestone the strMilestone to set
	 */
	public void setStrMilestone(String strMilestone) {
		this.strMilestone = strMilestone;
	}

	/**
	 * @return the alMilestones
	 */
	public ArrayList getAlMilestones() {
		return alMilestones;
	}

	/**
	 * @param alMilestones the alMilestones to set
	 */
	public void setAlMilestones(ArrayList alMilestones) {
		this.alMilestones = alMilestones;
	}

	/**
	 * @return the selectedMilestones
	 */
	public String[] getSelectedMilestones() {
		return selectedMilestones;
	}

	/**
	 * @param selectedMilestones the selectedMilestones to set
	 */
	public void setSelectedMilestones(String[] selectedMilestones) {
		this.selectedMilestones = selectedMilestones;
	}

	/**
	 * @return the alSelection
	 */
	public ArrayList getAlSelection() {
		return alSelection;
	}

	/**
	 * @param alSelection the alSelection to set
	 */
	public void setAlSelection(ArrayList alSelection) {
		this.alSelection = alSelection;
	}

	/**
	 * @return the strSelection
	 */
	public String getStrSelection() {
		return strSelection;
	}

	/**
	 * @param strSelection the strSelection to set
	 */
	public void setStrSelection(String strSelection) {
		this.strSelection = strSelection;
	}
	
	
}
