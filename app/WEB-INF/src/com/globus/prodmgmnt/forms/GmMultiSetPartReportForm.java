package com.globus.prodmgmnt.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

public class GmMultiSetPartReportForm extends GmCommonForm{

	
	 private String    partNumber 	= "";
	 private String    partID 	= "";
	 private ArrayList alMSParts    = new ArrayList();
	 
	 private List ldtResult = new ArrayList();
	 
	 /**
	  * @return partNumber
	  */
	public String getPartNumber() {
		return partNumber;
	}
	
	/**
	 * @param partNumber The partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	/**
	 * @return alMSParts
	 */
	public ArrayList getAlMSParts() {
		return alMSParts;
	}
	
	/**
	 * @param alMSParts The alMSParts to set
	 */
	public void setAlMSParts(ArrayList alMSParts) {
		this.alMSParts = alMSParts;
	}

	/**
	 * @return partID
	 */
	public String getPartID() {
		return partID;
	}

	/**
	 * @param partID The partID to set
	 */
	public void setPartID(String partID) {
		this.partID = partID;
	}
	 
	 /**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }
}
