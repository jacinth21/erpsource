/*
 * Module: GmProjectBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11
 * Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.quality.beans.GmSetMapTransactionBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmProjectBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  // this will be removed all place changed with Data Store VO constructor

  public GmProjectBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmProjectBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  ArrayList alType = new ArrayList();
  ArrayList alState = new ArrayList();
  ArrayList alCountry = new ArrayList();
  ArrayList alPay = new ArrayList();

  ArrayList alDevClass = new ArrayList();
  ArrayList alProjectType = new ArrayList();
  ArrayList alProjectStatus = new ArrayList();
  ArrayList alGroup = new ArrayList();
  ArrayList alSegment = new ArrayList();

  private final String EMPTY_STRING = "";
  private String gridXmlData = EMPTY_STRING;

  /**
   * loadVendor - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadProject() throws AppError {
    HashMap hmReturn = new HashMap();

    try {
      alDevClass = gmCommon.getCodeList("DEVCL");
      alProjectType = gmCommon.getCodeList("PKPRN");
      alGroup = gmCommon.getCodeList("GROUP");
      alSegment = gmCommon.getCodeList("SEGMT");

      hmReturn.put("DEVCLASS", alDevClass);
      hmReturn.put("PROTYPE", alProjectType);
      hmReturn.put("GROUP", alGroup);
      hmReturn.put("SEGMENT", alSegment);

    } catch (Exception e) {
      GmLogError.log("Exception in GmProjectBean:loadProject", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadVendor

  /**
   * loadEditProject - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadEditProject(String strId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug(" Project ID inside bean is " + strId);

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	C202_PROJECT_ID ID, C202_PROJECT_NM NAME, C202_DEVICE_CLASS DEVCLASS, C901_PROJECT_TYPE PROJECTTYPE , ");
    sbQuery
        .append(" C202_CREATED_BY PMAN, C202_PROJECT_DESC PDESC, DECODE(C901_STATUS_ID,20300,'Initiated',20301,'Approved',20302,'Launched') STATUSID, C901_GROUP_ID GROUPID, C901_SEGMENT_ID SEGMENTID ");
    sbQuery.append(" , C901_STATUS_ID STATUSIDID ");
    sbQuery.append(" FROM T202_PROJECT ");
    sbQuery.append(" WHERE C202_PROJECT_ID = '");
    sbQuery.append(strId);
    sbQuery.append("'");
    log.debug(" Query inside Project Details inside Load Edit Project is >>> " + sbQuery.toString());
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("PROJECTDETAILS", hmResult);
    log.debug(" Values inside Project Details inside Load Edit Project is >>> " + hmResult);

    hmTemp = loadProject();
    hmReturn.putAll(hmTemp);
    log.debug(" values inside hmTemp " + hmReturn);

    return hmReturn;
  } // End of loadEditVendor

  /**
   * saveProject - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveProject(HashMap hmParam, String strUsername, String strAction) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;
    String strProjectId = null;


    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    log.debug(" Values inside HmParam >> " + hmParam);


    String strProjectNm = (String) hmParam.get("PNAME");
    // String strIdeaId = (String)hmParam.get("IDEAID");
    String strProjDesc = (String) hmParam.get("PDESC");
    String strProjId = (String) hmParam.get("PROJID");
    String strDevClass = (String) hmParam.get("DEVCLASS");
    String strStatus = (String) hmParam.get("STRSTATUS");
    String strGroup = (String) hmParam.get("STRGROUP");
    String strSegment = (String) hmParam.get("STRSEGMENT");
    String strProjectName = (String) hmParam.get("PROJECTTYPE");



    gmDBManager.setPrepareString("GM_SAVE_PROJECT", 12);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);
    gmDBManager.setString(1, strProjectNm);
    // csmt.setInt(2,Integer.parseInt(strIdeaId));
    gmDBManager.setString(2, strProjDesc);
    gmDBManager.setString(3, strProjId);
    gmDBManager.setString(4, strDevClass);
    gmDBManager.setString(5, strStatus);
    gmDBManager.setString(6, strGroup);
    gmDBManager.setString(7, strSegment);
    gmDBManager.setString(8, strUsername);
    gmDBManager.setString(9, strAction);
    gmDBManager.setString(10, strProjectName);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(11);
    strProjectId = gmDBManager.getString(12);
    gmDBManager.commit();
    hmReturn.put("PROJECTID", strProjectId);
    log.debug("strMsg" + strMsg);

    return hmReturn;
  }


  /**
   * reportProject - This method will fetch all the records of project
   * 
   * @param String strColumn
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList reportProject(String strColumn) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("COLUMN", strColumn);
    return reportProject(hmParam);
  }

  /**
   * reportProject - This method will fetch all the records of project
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList reportProject(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strColumn = GmCommonClass.parseNull((String) hmParam.get("COLUMN"));
    String strProjectStatus = GmCommonClass.parseNull((String) hmParam.get("STATUSID"));
    String strGroupId = GmCommonClass.parseNull((String) hmParam.get("GROUPID"));
    String strSegmentId = GmCommonClass.parseNull((String) hmParam.get("SEGMENTID"));
    String strFrom = GmCommonClass.parseNull((String) hmParam.get("FROM"));
    String strComparison = " >= ";
    
    String strCompanyID = getCompId();
    String CompanyID=GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID_PROJECT_LIST"));
    strCompanyID = strCompanyID.isEmpty() ? CompanyID : strCompanyID ;

    if (strFrom.equals("ProjectReport")) {
      strComparison = " = ";
    }

    log.debug(" strColumn ID inside bean is " + strColumn);

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T202.C202_PROJECT_ID ID, C202_PROJECT_NM NAME, T202.C202_PROJECT_ID ||' / ' ||C202_PROJECT_NM IDNAME, ");
    sbQuery.append(" GET_USER_NAME(C202_CREATED_BY) PMANAGER, C202_CREATED_DATE CDATE, ");
    sbQuery
        .append(" GET_CODE_NAME(C901_STATUS_ID) STATUS, GET_CODE_NAME(C901_GROUP_ID) GROUPNAME, ");
    sbQuery
        .append(" GET_CODE_NAME(C901_SEGMENT_ID) SEGMENT ,t202.c1910_division_id divisionid ,t1910.c1910_division_name divisionname ");
    sbQuery
        .append(" FROM T202_PROJECT T202, T2021_PROJECT_COMPANY_MAPPING T2021 ,t1910_division t1910 ");
    sbQuery.append(" WHERE NVL(C202_RELEASE_FOR_SALE,'Y')  = DECODE('");
    sbQuery.append(strColumn);
    sbQuery.append("',");
    sbQuery.append(" 'APRICE', 'Y', NVL(C202_RELEASE_FOR_SALE,'Y') ) ");
    sbQuery.append(" AND T202.C202_PROJECT_ID = T2021.C202_PROJECT_ID");
    sbQuery.append(" and t202.c1910_division_id = t1910.c1910_division_id ");
    sbQuery.append(" AND T2021.C1900_COMPANY_ID = '");
    sbQuery.append(strCompanyID);
    sbQuery.append("'");
    sbQuery.append(" AND T2021.C2021_VOID_FL IS NULL");
    sbQuery.append(" AND C202_VOID_FL IS NULL ");
    sbQuery.append(" and C1910_VOID_FL is null ");

    /*
     * 20300 - Initiated, 20301 - Approved, 20302 - Launched . If we want to show all approved
     * projects, we would also have to show the launched projects, hence we are using C901_STATUS_ID
     * >=
     */
    if (!strProjectStatus.equals("0") && !strProjectStatus.equals("")) {
      sbQuery.append(" AND C901_STATUS_ID ");
      sbQuery.append(strComparison);
      sbQuery.append("'");
      sbQuery.append(strProjectStatus);
      sbQuery.append("'");
    }

    if (!strGroupId.equals("0") && !strGroupId.equals("")) {
      sbQuery.append(" AND C901_GROUP_ID = '");
      sbQuery.append(strGroupId);
      sbQuery.append("'");
    }

    if (!strSegmentId.equals("0") && !strSegmentId.equals("")) {
      sbQuery.append(" AND C901_SEGMENT_ID = '");
      sbQuery.append(strSegmentId);
      sbQuery.append("'");
    }

    sbQuery.append(" ORDER BY UPPER(C202_PROJECT_NM) ");
    /*
     * sbQuery.append(" ORDER BY GROUPNAME, SEGMENT, DECODE('"); sbQuery.append(strColumn);
     * sbQuery.append("'"); sbQuery.append(
     * " ,'NAME',C202_PROJECT_NM,'ID',C202_PROJECT_ID,'PMAN',C202_CREATED_BY,'CDATE',C202_CREATED_DATE, 'APRICE', C202_PROJECT_NM) "
     * );
     */
    log.debug(" Query inside reportProject is >>> " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    log.debug(" Size inside reportProject is >>> " + alReturn.size());

    return alReturn;
  }


  /**
   * reportProject - This method will fetch all the records of project for the Project Reports
   * screen. This method was added new, as the Group, Segment, Technique will be fetched from the
   * Project Attribute table
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList projectReport(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();

    String strColumn = GmCommonClass.parseNull((String) hmParam.get("COLUMN"));
    String strProjectStatus = GmCommonClass.parseNull((String) hmParam.get("STATUSID"));
    String strFrom = GmCommonClass.parseNull((String) hmParam.get("FROM"));
    String strComparison = " >= ";
    String strCompanyID = getCompId();

    // Combination of Group, Segment and Technique based on the selected dropdown value
    String strAttVal = GmCommonClass.parseNull((String) hmParam.get("ATTRIBUTEVAL"));

    if (strFrom.equals("ProjectReport")) {
      strComparison = " = ";
    }

    log.debug(" strColumn ID inside bean is " + strColumn);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" 	SELECT ID , NAME, IDNAME,GET_USER_NAME(PMANAGER) PMANAGER,CDATE,GET_CODE_NAME(STATUS) STATUS ");
    sbQuery.append(" , MAX(CASE WHEN T207.ATYPE = 103116 THEN T207.AVALUE END) GROUPNAME ");
    sbQuery.append(" , MAX(CASE WHEN T207.ATYPE = 103117 THEN T207.AVALUE END) SEGMENT ");
    sbQuery.append(" , MAX(CASE WHEN T207.ATYPE = 103118 THEN T207.AVALUE END) TECHNIQUE ");
    sbQuery.append(" FROM ( ");
    sbQuery
        .append("  SELECT T202.C202_PROJECT_ID ID, C202_PROJECT_NM NAME, T202.C202_PROJECT_ID ||' / ' ||C202_PROJECT_NM IDNAME,  ");
    sbQuery.append("  C202_CREATED_BY PMANAGER, C202_CREATED_DATE CDATE,  ");
    sbQuery.append("  C901_STATUS_ID STATUS, T202A.C901_ATTRIBUTE_TYPE ATYPE, ");
    sbQuery
        .append(" REPLACE(RTRIM (XMLAGG (XMLELEMENT (N, GET_CODE_NAME(T202A.C202A_ATTRIBUTE_VALUE) || ',')).EXTRACT ('//text()'), ','),',',',<BR>') AVALUE ");
    sbQuery
        .append(" FROM T202_PROJECT T202, T202A_PROJECT_ATTRIBUTE T202A ,T2021_PROJECT_COMPANY_MAPPING t2021 ");
    sbQuery.append(" WHERE NVL(C202_RELEASE_FOR_SALE,'Y')  = DECODE('");
    sbQuery.append(strColumn);
    sbQuery.append("',");
    sbQuery.append(" 'APRICE', 'Y', NVL(C202_RELEASE_FOR_SALE,'Y') ) ");
    sbQuery.append("  AND T202A.C901_ATTRIBUTE_TYPE in ('103116','103117','103118')  ");
    sbQuery.append("  AND T202.C202_PROJECT_ID = T202A.C202_PROJECT_ID  ");
    sbQuery.append("  AND C202_VOID_FL IS NULL  ");
    sbQuery.append("  AND T2021.C1900_COMPANY_ID ='");
    sbQuery.append(strCompanyID);
    sbQuery.append("' AND T2021.C2021_VOID_FL IS NULL ");
    sbQuery.append("  AND T202.C202_PROJECT_ID = T2021.C202_PROJECT_ID ");
    sbQuery.append("  AND T202A.C202A_VOID_FL IS NULL  AND C202A_ATTRIBUTE_VALUE IS NOT NULL  ");
    if (!strProjectStatus.equals("0") && !strProjectStatus.equals("")) {
      sbQuery.append(" AND C901_STATUS_ID ");
      sbQuery.append(strComparison);
      sbQuery.append("'");
      sbQuery.append(strProjectStatus);
      sbQuery.append("'");
    }

    if (!strAttVal.equals("")) {
      sbQuery.append(" AND T202A.C202A_ATTRIBUTE_VALUE IN ('");
      sbQuery.append(strAttVal);
      sbQuery.append("')");
    }

    sbQuery
        .append("  GROUP BY T202.C202_PROJECT_ID, C202_PROJECT_NM, C202_CREATED_BY,C202_CREATED_DATE,C901_STATUS_ID,T202A.C901_ATTRIBUTE_TYPE ");
    sbQuery.append("  )T207 ");
    sbQuery.append("  GROUP BY ID,NAME,IDNAME,PMANAGER,STATUS,CDATE  ");
    sbQuery.append(" ORDER BY UPPER(NAME) ");

    log.debug(" Query inside reportProject is >>> " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }

  /**
   * reportPartNumByProject - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList reportPartNumByProject(String strProjId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;


    gmDBManager.setPrepareString("GM_REPORT_PROJECT_PARTNUM", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strProjId);

    gmDBManager.execute();
    strBeanMsg = gmDBManager.getString(2);
    rs = (ResultSet) gmDBManager.getObject(3);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();

    return alReturn;
  } // end of reportPartNumByProject

  /**
   * fetchSalesGroup - This method will be used to fetch the sales group based on setid
   * 
   * @param strSetId
   * @return ArrayList -
   * @exception AppError
   */
  public String fetchSalesGroup(String strSetId) throws AppError {
    String strSalesGroupId = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_pd_set_grouping.get_salesgroup", 1);
    gmDBManager.setString(2, strSetId);
    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.execute();
    strSalesGroupId = gmDBManager.getString(1);
    log.debug("Sales Group..1@..." + strSalesGroupId);
    gmDBManager.close();

    return strSalesGroupId;
  }

  /**
   * loadSetMaster - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadSetMaster(String strOpt) throws AppError {
    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    GmDivisionBean gmdivision = new GmDivisionBean(getGmDataStoreVO());
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    ArrayList alSetCategory = new ArrayList();
    ArrayList alSetType = new ArrayList();
    ArrayList alReturn = new ArrayList();
    ArrayList alSetStatus = new ArrayList();
    ArrayList alSetHierarchy = new ArrayList();
    ArrayList alSalesGrp = new ArrayList();
    ArrayList alDivision = new ArrayList();

    String strCompanyID = getCompId();


    HashMap hmTemp = new HashMap();
    HashMap hmData = new HashMap();
    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301"); // Filter all approved projects
    hmData.put("COMP_ID", strCompanyID);

    try {
      alSetCategory = gmAutoCompleteReportBean.getCodeList("SETCA", null);
      alSetType = gmAutoCompleteReportBean.getCodeList("SETTY", null);

      hmReturn.put("SETCATEGORY", alSetCategory);
      hmReturn.put("SETTYPE", alSetType);


      alReturn = reportProject(hmTemp);
      hmReturn.put("PROJECTLIST", alReturn);
      // alReturn = gmProj.loadSetMap(strOpt);
      // hmReturn.put("SETMASTERLIST", alReturn);

      alSetStatus = gmAutoCompleteReportBean.getCodeList("PSTAT", null);
      hmReturn.put("SETSTATUS", alSetStatus);

      // alSetHierarchy = gmCommon.getCodeList("SETHR");
      // hmReturn.put("SETHIERARCHY", alSetHierarchy);

      // alSalesGrp = gmProj.loadSetMap("1600");
      // hmReturn.put("SALESGROUP", alSalesGrp);

      alDivision = gmdivision.fetchDivision(hmData);
      hmReturn.put("DIVISION_LIST", alDivision);


    } catch (Exception e) {
      GmLogError.log("Exception in GmProjectBean:loadSetMaster", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadSetMaster

  /**
   * saveSetMaster - This method will add/update Set Master data
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   **/
  public void saveSetMaster(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    log.debug(" HashMao in bena " + hmParam);

    String strProjId = (String) hmParam.get("PROJID");
    String strSetId = (String) hmParam.get("SETID");
    String strSetName = (String) hmParam.get("SETNM");
    String strSetDesc = (String) hmParam.get("SETDESC");
    String strSetDetailDesc = (String) hmParam.get("SETDETAILDESC");
    String strSetCat = (String) hmParam.get("SETCAT");
    String strSetType = (String) hmParam.get("SETTYPE");
    String strType = (String) hmParam.get("TYPE");
    String strUserId = (String) hmParam.get("USERID");
    String strAction = (String) hmParam.get("MODE");
    String strStatus = (String) hmParam.get("STATUS");
    String strRevLevel = (String) hmParam.get("REVLVL");
    String strLogReason = (String) hmParam.get("LOG");
    String strCoreSetFl = (String) hmParam.get("CORESETFL");
    String strFileName = (String) hmParam.get("FILENAME");
    String strSearchCboFileName = (String) hmParam.get("SEARCHCBOINSPSHEET");// Get the name from Search box
    String strCompanyID = getCompId();
    String strDivisionId = (String) hmParam.get("DIVISIONID");
    String strLotTrackFl = (String)hmParam.get("LOTTRACKFL");//PMT-42618

    /*
     * String strSetHierarchy = (String)hmParam.get("SETHIERARCHY"); String strBaseLineFl =
     * (String)hmParam.get("BASELINEFL"); String strSalesGrp = (String)hmParam.get("SALESGROUP");
     */
    gmDBManager.setPrepareString("GM_SAVE_SETMASTER", 15);
    // gmDBManager.registerOutParameter(15, java.sql.Types.CHAR);
    gmDBManager.setString(1, strProjId);
    gmDBManager.setString(2, strSetId);
    gmDBManager.setString(3, strSetName);
    gmDBManager.setString(4, strSetDesc);
    gmDBManager.setInt(5, Integer.parseInt(strSetCat));
    gmDBManager.setInt(6, Integer.parseInt(strSetType));
    gmDBManager.setInt(7, Integer.parseInt(strType));
    gmDBManager.setString(8, strUserId);
    gmDBManager.setString(9, strAction);
    gmDBManager.setString(10, strStatus);
    gmDBManager.setString(11, strRevLevel);
    gmDBManager.setString(12, strCoreSetFl);
    gmDBManager.setString(13, strSetDetailDesc);
    gmDBManager.setString(14, strDivisionId);
    gmDBManager.setString(15, strLotTrackFl);//PMT-42618

    /*
     * gmDBManager.setString(12,strSetHierarchy); gmDBManager.setString(13,strBaseLineFl);
     * gmDBManager.setString(14,strSalesGrp); gmDBManager.setString(15,strSetId);
     */
    gmDBManager.execute();
//  PC-5611 Refresh the Set List Redis key while release to region
	GmCacheManager gmCacheManager = new GmCacheManager();
	if (strStatus.equals("20367") || strStatus.equals("20369")) {
		String strKey = "SET_ID_LST:" + strCompanyID + ":AUTO"; // SET_ID_LST:1000:AUTO
		//form the redis key dynamically on the basis of company id, sample key: SET_ID_LST:1000:AUTO
		gmCacheManager.removeKey(strKey); // remove the cache manager key
	}
    // strSetIdFromDb = gmDBManager.getString(15);
    // log.debug("strSetIdFromDb.........."+strSetIdFromDb);
    if (!strLogReason.equals("")) {
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      gmCommonBean.saveLog(gmDBManager, strSetId, strLogReason, strUserId, "1219");// 1219 - Log
                                                                                   // Code Number
                                                                                   // for partnum
                                                                                   // setup
    }
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    // saving the upload file information
    if (!strFileName.equals("")) {
      gmCommonUploadBean.saveUploadUniqueInfo(gmDBManager, strSetId, "91184", strFileName,
          strUserId);
    }

    hmParam.put("MAP_TYPE", "105360");// 105360 - created
    hmParam.put("COMP_ID", strCompanyID);
    // This method used to save the Set company mapping details
    saveSetCompanyMap(gmDBManager, hmParam);
    gmDBManager.commit();

    // voiding the upload file information, only if entering a valid file name in the seach box
    if (strFileName.equals("") && strSearchCboFileName.equals("")) {
      ArrayList alList = gmCommonUploadBean.fetchUploadInfo("91184", strSetId);
      if (alList.size() > 0) {
        HashMap hmMap = (HashMap) alList.get(0);
        String strFileID = (String) hmMap.get("ID") + "|";
        gmCommonUploadBean.deleteUploadInfo(strFileID, strUserId);
      }
    }


    // return strSetIdFromDb;

  } // End of saveSetMaster

  /**
   * loadEditSetMaster - This method will load the set and system details
   * 
   * @param String strId - Set ID or System ID to get the details
   * @return HashMap hmResult - Having details of Set or System
   * @exception AppError
   **/
  public HashMap loadEditSetMaster(String strId) throws AppError {
    HashMap hmResult = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strCompanyDateFormat = getGmDataStoreVO().getCmpdfmt();//PMT-37488
    sbQuery
        .append(" SELECT	C207_SET_ID SETID, C207_SET_NM SETNM, C207_SET_DESC	SETDESC , C207_SET_DTL_DESC SETDETAILDESC, C202_PROJECT_ID PROJID, GET_PROJECT_NAME(C202_PROJECT_ID) PROJNAME, C207_CATEGORY SETCAT ");
    sbQuery
        .append(" , C207_TYPE SETTYPE, C901_STATUS_ID SETSTATUS, C207_REV_LVL REVLVL , GET_CODE_NAME(C901_STATUS_ID) STATUSMSG, C207_CORE_SET_FL CORESETFL ");
    sbQuery
        .append(" , C901_HIERARCHY SETHIERARCHY, C901_CONS_RPT_ID CONSRPTID , C1910_DIVISION_ID DIVISIONID , C207_LOT_TRACK_FL LOTTRACKFL,");//PMT-42618
     sbQuery
       .append(" DECODE(c901_published_status,108660,'<b>Product Catalog &<br> Marketing Collateral </b><br> <br> System Name  <b>' || c207_set_nm || '</b><br> Published by <b>'||get_user_name(c207_published_by)||'</b><br> Published on <b>'||to_char(c207_published_date,'"+strCompanyDateFormat+" HH:MI:SS AM'),'') message, ");//PMT-37488
    sbQuery.append(" gm_pkg_pd_system_rpt.get_system_set_map_cnt(C207_SET_ID) mapcount, c901_published_status publishtype  ");//PMT-37488
    sbQuery.append(" FROM T207_SET_MASTER ");
    sbQuery.append(" WHERE C207_SET_ID = '");
    sbQuery.append(strId);
    sbQuery.append("'");
    log.debug(" Query for loadEditSetMaster " + sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    return hmResult;
  } // End of loadEditSetMaster


  /**
   * loadSetMap - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList loadSetMap(String strType) throws AppError {
    log.debug("the value insdie strType *** " + strType);
    HashMap hmValues = new HashMap();
    if (strType.equals("GROUPPARTMAPPING")) {
      hmValues.put("TYPE", "1600");
      hmValues.put("GROUPPART", strType);
    } else if (strType.equals("1600")) {
      hmValues.put("TYPE", strType);
      hmValues.put("GROUPPART", "GROUPPARTMAPPING");
    } else {
      hmValues.put("TYPE", strType);
    }
    return loadSetMap(hmValues);
  } // End of loadSetMap

  /**
   * loadSetMap - This method will load the set mapping details
   * 
   * @param HashMap hmParam - Having the set or system filter values
   * @return ArrayList alReturn - Having set or system values
   * @exception AppError
   **/
  public ArrayList loadSetMap(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strGroupPartMapping = GmCommonClass.parseNull((String) hmParam.get("GROUPPART"));
    String strCompanyId = getCompId();


    HashMap hmRuleData = new HashMap();
    hmRuleData.put("RULEID", strDeptId);
    hmRuleData.put("RULEGROUP", "SETMAPACCESS");

    String strStatus = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
    strStatus = GmCommonClass.getStringWithQuotes(strStatus);
    gmDBManager.close();

    String strSearchSet = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strSystem = GmCommonClass.parseZero((String) hmParam.get("SYSTEM"));
    String strHierarchy = GmCommonClass.parseZero((String) hmParam.get("HIERARCHY"));
    String strCategory = GmCommonClass.parseZero((String) hmParam.get("CATEGORY"));
    String strSetType = GmCommonClass.parseZero((String) hmParam.get("SETTYPE"));
    String strSetStatus = GmCommonClass.parseZero((String) hmParam.get("SETSTATUS"));
    //Report Based On Project Name - PMT-42068
    String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    
    log.debug(" status is " + strStatus);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT T207.C207_SET_ID ID, C207_SET_NM NAME, T207.C207_SET_ID ||'-'|| C207_SET_NM IDNAME, T207.C202_PROJECT_ID PROJID,  T202.C202_PROJECT_NM PROJNAME, ");
    sbQuery.append(" GET_CODE_NAME(T207.C207_CATEGORY) CATEG, GET_CODE_NAME(C207_TYPE) TYPE, ");
    sbQuery.append(" GET_CODE_NAME(T207.C901_STATUS_ID) STAT, C207_REV_LVL REVLVL, ");
    sbQuery
        .append(" C207_SET_DESC  PARTNUMDESC, C207_TYPE TYPEID, C207_SET_SALES_SYSTEM_ID SYSTEMID, ");
    sbQuery.append(" GET_USER_NAME(C207_LAST_UPDATED_BY) UPDBY, C207_LAST_UPDATED_DATE UPDDT ");
    sbQuery
        .append(" , GET_SET_NAME(C207_SET_SALES_SYSTEM_ID) SYSTEMNAME, GET_CODE_NAME_ALT(C901_HIERARCHY) HIERARCHY ");
    sbQuery
        .append(" , GET_CODE_NAME (DECODE (T207.C207_TYPE,4074, DECODE (T207.C901_HIERARCHY, 20700, '103148', '103150'),  DECODE (T207.C901_HIERARCHY, 20700, '103147', '103149'))) SETTYPENM");
    sbQuery.append(" , DECODE (T207.C901_CONS_RPT_ID, 20100,'Yes', 'No') BASELINENM");
    sbQuery.append(" , GET_CODE_NAME (T207A.C901_SHARED_STATUS) SHAREDSTATNM");

    sbQuery
        .append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080, T207A_SET_LINK T207A, T202_PROJECT T202  WHERE C901_SET_GRP_TYPE ='");
    sbQuery.append(strType);
    sbQuery.append("' ");
    sbQuery.append(" AND T207.C207_SET_ID = T2080.C207_SET_ID");
    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
    sbQuery.append(" AND C207_VOID_FL IS NULL ");
    sbQuery.append(" AND C207_OBSOLETE_FL IS NULL ");
    sbQuery.append(" AND T207. C202_PROJECT_ID = T202.C202_PROJECT_ID(+)");
    sbQuery.append(" AND T202.C202_VOID_FL(+) IS NULL");
    //Check Project values empty or not for BOM/SET Report
    if (!strProjectID.equals("")) {
      sbQuery.append(" AND T207.C202_PROJECT_ID = '"+strProjectID+ "'");
    }
    sbQuery.append(" AND T207.C207_SET_ID  = T207A.C207_LINK_SET_ID (+)");
    if (!strStatus.equals("")) {
      sbQuery.append(" AND T207.C901_STATUS_ID IN ('");
      sbQuery.append(strStatus);
      sbQuery.append("')");
    }

    if (!strSearchSet.equals("")) {
      sbQuery.append(" AND UPPER(C207_SET_NM) LIKE ('%");
      sbQuery.append(strSearchSet.toUpperCase());
      sbQuery.append("%')");
    }

    if (!strSystem.equals("0")) {
      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN ('");
      sbQuery.append(strSystem);
      sbQuery.append("')");
    }

    if (!strHierarchy.equals("0")) {
      sbQuery.append(" AND C901_HIERARCHY IN ('");
      sbQuery.append(strHierarchy);
      sbQuery.append("')");
    }

    if (!strCategory.equals("0")) {
      sbQuery.append(" AND C207_CATEGORY IN ('");
      sbQuery.append(strCategory);
      sbQuery.append("')");
    }

    if (!strSetType.equals("0")) {
      sbQuery.append(" AND C207_TYPE IN ('");
      sbQuery.append(strSetType);
      sbQuery.append("')");
    }

    if (!strSetStatus.equals("0")) {
      sbQuery.append(" AND T207.C901_STATUS_ID IN ('");
      sbQuery.append(strSetStatus);
      sbQuery.append("')");
    }
    
    //added for PMT#49675 to filter by SET Id
    if (strSetId.contains(",")) {
        sbQuery.append(" AND T207.C207_SET_ID IN ('");
        sbQuery.append(strSetId.replaceAll(",", "','"));
        sbQuery.append("')");
      }
    else if(!strSetId.equals("")) {
    	sbQuery.append(" AND T207.C207_SET_ID LIKE ('%");
        sbQuery.append(strSetId);
        sbQuery.append("%')");
    }

    sbQuery.append(" AND T2080.C1900_COMPANY_ID = '");
    sbQuery.append(strCompanyId);
    sbQuery.append("'");
    sbQuery.append(" ORDER BY ");


    if (strGroupPartMapping.equals("GROUPPARTMAPPING")) {
      sbQuery.append(" UPPER(C207_SET_NM), ");
    }

    sbQuery.append(" T207.C207_SET_ID");
    log.debug(" Query for loading set map " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of loadSetMap


  /**
   * fetchSetMaping - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchSetMaping(HashMap hmParam) throws AppError {
    ArrayList alMapReturn = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("MODE"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strFromPage = GmCommonClass.parseNull((String) hmParam.get("FROMPAGE"));
    String strFromPageOption = GmCommonClass.parseNull((String) hmParam.get("FROMPGOPT"));
    String strCogsType = GmCommonClass.parseNull((String) hmParam.get("COGSTYPE"));

    HashMap hmRuleData = new HashMap();
    hmRuleData.put("RULEID", strDeptId);
    hmRuleData.put("RULEGROUP", "RFSPARTACCESS");


    log.debug(" Dept Id is " + strDeptId);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strStatus = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
    gmDBManager.close();
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T208.C207_SET_ID ID, T208.C205_PART_NUMBER_ID PNUM, NVL(T208.C208_SET_QTY,0) QTY, T208.C208_SET_DETAILS_ID MID, ");
    sbQuery
        .append(" GET_PARTNUM_DESC(T208.C205_PART_NUMBER_ID) PDESC, T208.C208_INSET_FL INSETFL, T208.C208_CRITICAL_FL CRITICALFL , ");
    sbQuery
        .append(" T208.C208_CRITICAL_QTY CRITQTY,  T208.C208_CRITICAL_TAG CRITTAG, T208.C901_CRITICAL_TYPE	CRITTYPE, GET_PARTNUM_PRODUCT(T208.C205_PART_NUMBER_ID) PARTFAMILY , get_part_attribute_value(T208.C205_PART_NUMBER_ID,'92340') PRODATTRIBUTEVALUE,  t207.c207_type SET_TYPE,");
    sbQuery
        .append(" GET_RULE_VALUE(GET_PARTNUM_PRODUCT(T208.C205_PART_NUMBER_ID), 'PART_TAGGABLE')  PARTFAMILYFL ,  GET_RULE_VALUE(t207.c207_type, 'PART_CRITICAL') SETTYPEFL, T208.C901_UNIT_TYPE UNITTYPE");

    if (strFromPage.equals("SETINITIATE")) {
      sbQuery.append(" , NVL(GET_QTY_IN_BULK(T208.C205_PART_NUMBER_ID),0) QTYBULK ");
    }
    // For BOM report page in MFG, show the COGS value instead of List Price
    if (strFromPageOption.equals("BOMRPT")) {
      sbQuery.append(" , NVL(get_ac_cogs_value(T208.C205_PART_NUMBER_ID,");
      sbQuery.append(strCogsType);
      sbQuery.append("),0)  LPRICE ");
    } else {
      sbQuery.append(" , NVL(GET_PART_PRICE(T208.C205_PART_NUMBER_ID,'L'),0) LPRICE");
    }
    sbQuery.append(" FROM T208_SET_DETAILS T208, t207_set_master t207");
    if (strStatus.equals("Y")) {
      sbQuery.append(" , T205_PART_NUMBER T205,  V205D_PART_ATTR ");
      sbQuery.append(" WHERE  T208.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND C208_VOID_FL IS NULL ");
      // sbQuery.append(" AND t205.C205_RELEASE_FOR_SALE = 'Y' ");
      // sbQuery.append(" AND gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id) is not null ");
      sbQuery.append(" and T205.C205_PART_NUMBER_ID  = V205D_PART_ATTR.PNUM ");
    } else {
      sbQuery.append(" WHERE T208.C208_VOID_FL IS NULL ");
    }
    sbQuery.append(" AND  T208.C207_SET_ID = '");
    sbQuery.append(strSetId);
    sbQuery.append("' AND t207.C207_SET_ID = T208.C207_SET_ID");

    if (strMode.equals("SET")) {
      sbQuery.append(" AND T208.C208_SET_QTY <> 0 AND T208.C208_INSET_FL = 'Y' ");
    } else if (strMode.equals("OPT")) {
      sbQuery.append(" AND T208.C208_SET_QTY = 0  AND T208.C208_INSET_FL = 'N' ");
    } else if (strMode.equals("VSET")) {
      sbQuery.append(" AND T208.C208_SET_QTY <>  0   ");
    }


    sbQuery
        .append(" ORDER BY T208.C205_PART_NUMBER_ID , T208.C208_CRITICAL_FL  DESC , T208.C208_INSET_FL DESC  ");
    log.debug(" Query for drill down is " + sbQuery.toString());
    alMapReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alMapReturn;
  } // End of fetchSetMaping

  /**
   * fetchSetMaping - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchSetMaping(String strSetId, String strMode) throws AppError {
    HashMap hmValues = new HashMap();
    hmValues.put("SETID", strSetId);
    hmValues.put("MODE", strMode);
    return fetchSetMaping(hmValues);
  } // End of fetchSetMaping

  /**
   * saveSetMap - This method will used to store set mapping details to T208 table
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveSetMap(String strSetId, String strInputString, String strComments,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmSetMapTransactionBean gmSetMapTransactionBean = new GmSetMapTransactionBean (getGmDataStoreVO());

   HashMap hmReturn=  saveSetMap (gmDBManager, strSetId, strInputString, strComments, strUserId);

    gmDBManager.commit();
    
	// PMT-43616 code has been moved to separate method on this file itself
    gmSetMapTransactionBean.sendSetListUpdateEmail (strSetId);
    
    return hmReturn;
  } // End of saveSetMap

  
  /**
   * saveSetMap - This method overloaded by gmDBManager
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveSetMap(GmDBManager gmDBManager, String strSetId, String strInputString, String strComments,
      String strUserId) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_SAVE_SETPARTMAP", 4);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.setString(1, strSetId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(4);
   
    if (!strComments.equals("")) {
      log.debug("LOG strComments::" + strComments);
      gmCommonBean.saveLog(gmDBManager, strSetId, strComments, strUserId, "1249");
    }

    return hmReturn;
  } 

  

  /**
   * loadVanGuardList - This method will fetch the vanguard details
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadVanGuardList() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;


    gmDBManager.setPrepareString("GM_PKG_CM_SET.GM_CM_FCH_VANGUARD_LIST", 1);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(1);
    alReturn = gmDBManager.returnArrayList(rs);
    log.debug(" Return values size " + alReturn.size());
    gmDBManager.close();

    return alReturn;
  } // End of loadVanGuardList

  /**
   * saveVanGuardList - This method will save the vanguard details
   * 
   * @param strVGuardParameters - contains the input string with Project Id and the corresponding
   *        vanguard type
   * @param strUserID - contains the user id
   * @exception AppError
   **/
  public void saveVanGuardList(String strVGuardParameters, String strUserID) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_PKG_CM_SET.GM_CM_SAV_VANGUARD_LIST", 2);

    gmDBManager.setString(1, strVGuardParameters);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();


  } // End of saveBaselineValue

  public HashMap fetchSetMappingList() throws AppError {

    HashMap hmValues = new HashMap();

    hmValues.put("SYSTEM", loadSetMap("1600"));
    hmValues.put("HIERARCHY", GmCommonClass.getCodeList("SETHR"));
    hmValues.put("CATEGORY", GmCommonClass.getCodeList("SETCA"));
    hmValues.put("SETTYPE", GmCommonClass.getCodeList("SETTY"));
    hmValues.put("STATUS", GmCommonClass.getCodeList("PSTAT"));

    return hmValues;
  }

  /**
   * getXmlGridData - This method will return the Xml String
   * 
   * @param hmParamV - contains the Templare and Template path and Flag Conditions - BuildSet, MFG,
   *        DHR
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParamV) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();

    ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
    String strBuildSetCond = (String) hmParamV.get("BUILDSET");
    String strMfgCond = (String) hmParamV.get("MFG");
    String strDhrCond = (String) hmParamV.get("DHR");

    HashMap hmParam = new HashMap();
    HashMap hmParam1 = new HashMap();
    hmParam.put("buildset", strBuildSetCond);
    hmParam.put("mfg", strMfgCond);
    hmParam.put("dhr", strDhrCond);

    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmConditions", hmParam);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);

    return templateUtil.generateOutput();
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * saveSetCompanyMap - This method saves the Set company mapping details
   * 
   * @param gmDBManager - Object to make Connection with database
   * @param hmParam - Contains the project and company details to pass it into procedure.
   * @throws AppError
   */
  public void saveSetCompanyMap(GmDBManager gmDBManager, HashMap hmParam) throws AppError {


    gmDBManager.setPrepareString("gm_pkg_pd_system_trans.gm_sav_system_company_map", 4);
    gmDBManager.setString(1, (String) hmParam.get("SETID"));
    gmDBManager.setString(2, (String) hmParam.get("COMP_ID"));
    gmDBManager.setString(3, (String) hmParam.get("MAP_TYPE"));
    gmDBManager.setString(4, (String) hmParam.get("USERID"));
    gmDBManager.execute();


  }
}// end of class GmProjectBean
