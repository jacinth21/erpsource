package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.prodmgmnt.GmPartAttributeVO;
import com.globus.valueobject.prodmgmnt.GmPartNumberVO;
import com.globus.valueobject.prodmgmnt.GmPartRFSVO;

public class GmPartNumberServiceBean extends GmBean {

  // this will be removed all place changed with Data Store VO constructor
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmPartNumberServiceBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartNumberServiceBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * This method will be used to fetch part numbers details
   * 
   * @param HashMap
   * @return
   * @throws AppError
   */
  public List<GmPartNumberVO> fetchPartNumbers(HashMap hmParams) throws AppError {
    long startTime, finishTime;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmPartNumberVO> listGmPartNumberVO;
    GmPartNumberVO gmPartNumberVO = (GmPartNumberVO) hmParams.get("VO");


    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_partnumber_prodcat", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();

    listGmPartNumberVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmPartNumberVO,
            gmPartNumberVO.getPartNumberProperties());
    gmDBManager.close();
    return listGmPartNumberVO;
  }
  
  
  
  // PMT#40762-part-availability-in-ipad 
  public String fetchJsonPartNumbers(HashMap hmParams) throws AppError {
   
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strReturnJSON="";
    gmDBManager.setPrepareString("gm_pkg_device_part_master_sync_rpt.gm_fch_part_prodcat", 6);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
    gmDBManager.execute();

    strReturnJSON =  GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.close();
    return strReturnJSON;
  }
  

  /**
   * This method will be used to fetch part attributes details
   * 
   * @param HashMap
   * @return
   * @throws AppError
   */
  public List<GmPartAttributeVO> fetchPartAttributes(HashMap hmParams) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    ArrayList alResult = new ArrayList();
	    List<GmPartAttributeVO> listGmPartAttributeVO;
	    GmPartAttributeVO gmPartAttributeVO = (GmPartAttributeVO) hmParams.get("VO");
	    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_partattr_prodcat", 6);
	    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	    gmDBManager.setString(4, (String) hmParams.get("UUID"));
	    gmDBManager.setString(5, (String) hmParams.get("COUNTRYID"));
	    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    listGmPartAttributeVO =
	        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(6), gmPartAttributeVO,
	            gmPartAttributeVO.getPartAttributeProperties());
	    gmDBManager.close();
	    return listGmPartAttributeVO;
	  }

  /**
   * This method will be used to fetch part attributes details
   * 
   * @param HashMap
   * @return
   * @throws AppError
   */
  public String fetchJsonPartAttributes(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strReturnJSON = "";
    gmDBManager.setPrepareString("gm_pkg_device_part_master_sync_rpt.gm_fch_partattr_prodcat", 6);
        gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
        gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
        gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
        gmDBManager.setString(4, (String) hmParams.get("UUID"));
        gmDBManager.setString(5, (String) hmParams.get("COMPID"));
        gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
        gmDBManager.execute();
        strReturnJSON =  GmCommonClass.parseNull(gmDBManager.getString(6));
        gmDBManager.close();
    return strReturnJSON;
  }
  
  /**
   * processPartVisibility - This method will form the message and send through JMS for part to
   * companies sync.By using this method we are sending the message to GmPartNumberConsumerJob.java.
   * 
   * @param - strUserId:user Login Id.
   * @return - Void
   * @exception - AppError
   */
  public void processPartVisibility(String strPartNum, String strUserId) throws AppError {

    HashMap hmParam = new HashMap();
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    log.debug("Entering in processPartVisibility before");
    // PART_NUMBER_CONSUMER_CLASS:it is mentioned in constant properties file.This key contain the
    // path for : com\globus\jms\consumers\processors\GmPartNumberConsumerJob.java
    String strConsumerClass =
        GmCommonClass.parseNull(GmCommonClass.getString("PART_NUMBER_CONSUMER_CLASS"));


    hmParam.put("STRUSERID", strUserId);
    hmParam.put("PNUM", strPartNum);
    hmParam.put("CONSUMERCLASS", strConsumerClass);
    log.debug("Entering in processPartVisibility bean");
    gmConsumerUtil.sendMessage(hmParam);

  }


  /**
   * syncPartCompanyMapping - This method is calling from GmPartNumberConsumerJob.java.This Method
   * will sync the system ,Project and Group for the part based on RFS .If the system and project of
   * the part is is released for two companies .The system,Project and groups (that are available
   * for the system) should be visible for the two companies Alone.For the remaining companies it
   * will not be visible.
   * 
   * @param - strUserId:Login user id
   * @return - Void
   * @exception - AppError
   */
  public void syncPartCompanyMapping(String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("Entering in syncPartCompanyMapping");
    gmDBManager.setPrepareString("gm_pkg_qa_rfs.gm_sync_part_to_all_company", 1);

    gmDBManager.setString(1, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * fetchPartRFSMapping-This method will be used to fetch part RFS details from
   * T901C_RFS_COMPANY_MAPPING table.
   * 
   * @param HashMap
   * @return
   * @throws AppError
   */
  public List<GmPartRFSVO> fetchPartRFSMapping(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmPartRFSVO> listGmPartRFSVO;
    GmPartRFSVO gmPartRFSVO = (GmPartRFSVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_partrfs_mapping", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();

    listGmPartRFSVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmPartRFSVO,
            gmPartRFSVO.getPartRFSProperties());

    gmDBManager.close();
    return listGmPartRFSVO;
  }

  /**
   * @param strPartNum
   * @param strUserId
   * @throws AppError
   */
  public void syncProjectPartMapping(String strPartNum, String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("Entering in syncProjectPartMapping");
    gmDBManager.setPrepareString("gm_pkg_qa_rfs.gm_sync_part_company_map", 2);

    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();

    log.debug(" End ");
  }
  
  
  
  /**processWORevisionUpdateJMS:This method used to update the wo revision status Using JMS
 * @param hmParam
 * @throws AppError
 */
public void processWORevisionUpdate(HashMap hmParam) throws AppError {

	try{
	    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();	      
	    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));
	    hmParam.put("QUEUE_NAME", strQueueName);
	    // ORDER_CONSUMER_CLASS is mentioned in JMSConsumerConfig. properties file.This key contain the
	    // path for : com.globus.jms.consumers.processors.GmWorkOrderUpdateConsumerJob
	    String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("WO_REV_CONSUMER_CLASS"));
	    hmParam.put("HMHOLDVALUES", hmParam);
	    hmParam.put("CONSUMERCLASS", strConsumerClass);
	    log.debug("strQueueName=>"+strQueueName+" strConsumerClass=>"+strConsumerClass+" hmParam=>>>"+hmParam);
	    gmConsumerUtil.sendMessage(hmParam);  

	  }catch(Exception exe){
		    log.error("Exception occured in the class GmPartNumberServiceBean : "  + exe.getMessage());
		  }

}
  
}
