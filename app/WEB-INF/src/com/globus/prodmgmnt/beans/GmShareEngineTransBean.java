package com.globus.prodmgmnt.beans;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmEgnyteClient;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmEgnyteReceiveVO;
import com.globus.valueobject.common.GmEgnyteSendVO;
import com.globus.valueobject.common.Links;
import com.globus.common.beans.GmBean;



/**
 * GmShareEngineTransBean
 * @author mpatel
 *
 */
public class GmShareEngineTransBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public static final String MKTEMAIL_TEMPLATE_NAME = "GmMarketingCollEmail";
	public static final String LINKUNAVAIL_TEMPLATE_NAME = "GmMarketingCollLinkUnavailalbeEmail";
	public static final String TOKENFILE_TEMPLATE_NAME = "GmMarketingCollTokenFileEmail";
	public static final String MKTEMAIL_FAILURE_TEMPLATE_NAME = "GmMarketingCollFailureEmail";
	
	public GmShareEngineTransBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmShareEngineTransBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }

	/**
	 * /
	 *  saveSharedInfo to save the detail of each share ie files and email details
	 *  The call to the proc also validate and filter emails
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public HashMap saveSharedInfo(HashMap hmParam) throws AppError {
		String strShareID = "";
		String strShareStatus = "";
		
		
		GmDBManager gmDBManager = new GmDBManager();
		String strFileInputstr = GmCommonClass.parseNull((String) hmParam.get("FILEINPUTSTR"));
		String strEmailInputStr = GmCommonClass.parseNull((String) hmParam.get("EMAILINPUTSTR"));

		
		gmDBManager.setPrepareString("gm_pkg_pdmc_share_eng_trans.gm_sav_shared_info", 9);
		gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);
		gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);
		//gmDBManager.setString(1, (String)hmParam.get("USERID"));
		gmDBManager.setString(1, (String)hmParam.get("SHEXTREFID"));
		gmDBManager.setString(2, (String)hmParam.get("SHARETYPE"));
		gmDBManager.setString(3, (String)hmParam.get("SUBJECT"));
		gmDBManager.setString(4, (String)hmParam.get("MSGDETAIL"));
		gmDBManager.setString(5, (strEmailInputStr));
		gmDBManager.setString(6, (strFileInputstr));
		gmDBManager.setString(7, ((String)hmParam.get("USERID")));

		gmDBManager.execute();
		
		strShareID = GmCommonClass.parseNull((String)gmDBManager.getString(8));
		strShareStatus = GmCommonClass.parseNull((String)gmDBManager.getString(9));

		hmParam.put("strShareID",strShareID);
		hmParam.put("strShareStatus",strShareStatus);

		gmDBManager.commit();
		log.debug("saveSharedInfo:"+hmParam);
		
		
		return hmParam;
	}// End of saveSharedInfo
	
	
	/**
	 * 
	 *  saveShareIdJms send a message to using JMS to process the share.
	 *  This method also contains JMS Code which put the share on a queue and trigger a background JOB o
	 *  
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public HashMap saveShareIdJms(HashMap hmParam) throws AppError {
			
		//Class name which will process the message
		String strConsumerClass = GmCommonClass.parseNull(GmCommonClass.getString("BATCH_CONSUMER_MARKETING_COLL_EMAIL_CLASS"));

		// This is JMS Code which will process the  share
		GmQueueProducer qprod = new GmQueueProducer();
		GmMessageTransferObject tf = new GmMessageTransferObject();
		tf.setMessageObject((String)hmParam.get("strShareID"));
		tf.setConsumerClass(strConsumerClass);
		qprod.sendMessage(tf);
		log.debug("Message Sent ShareID #:" + (String)hmParam.get("strShareID"));
		
		return hmParam;
	}// End of saveShareIdJms
	
	/**
	 * 
	 *  saveConsumerJms send a message to using JMS to process the Upload Files.
	 *  
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public void saveConsumerJms(HashMap hmParam)throws AppError {
		
		String strConsumerClass = "";
		// This is JMS Code which will process the  save upload files
		GmQueueProducer gmQueueProduce = new GmQueueProducer();
		GmMessageTransferObject tf = new GmMessageTransferObject();
		
		strConsumerClass = (String)hmParam.get("CONSUMERCLASS");
		HashMap hmMsgObjects = (HashMap)hmParam.get("MESSAGEOBJECT"); 
		log.debug("strConsumerClass="+strConsumerClass);
		tf.setMessageObject(hmMsgObjects);
		tf.setConsumerClass(strConsumerClass);
		gmQueueProduce.sendMessage(tf);
	}// End of saveConsumerJms
	
	/**
	 * 
	 *  saveEgnyteFileSync is used to save Upload Files in the Egnyte Folder.
	 *  
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public void saveEgnyteFileSync(HashMap hmParam)throws AppError, Exception {
		ArrayList alFilesInfo = new ArrayList();
		ArrayList alFilePath = new ArrayList();
		HashMap hmFileProp = new HashMap();
		GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
		GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
		String strFileGrp = "";
		String strFileRefId = "";
		String strFileName = "";     
		String strArtifactType = "";
		String strSubType = "";
		String strFileIds = GmCommonClass.parseNull((String)hmParam.get("FILEIDS"));
		String strAction = GmCommonClass.parseNull((String)hmParam.get("ACTION"));
		String strAdditionalFilePath = "";
		String strPortalPath = GmCommonClass.parseNull(GmCommonClass.getString("PRODCATALOGPATH"));
		String strEgnytePath = GmCommonClass.parseNull(GmCommonClass.getString("EGNYTEDIR"))
								+GmCommonClass.parseNull( GmCommonClass.getString("EGNYTEMARKETINGCOLLBASEDIR"))
								+ GmCommonClass.parseNull(GmCommonClass.getString("PRODCATALOGIMAGEURL"));
		log.debug(strEgnytePath);
		log.debug(strPortalPath);
		//fetchUploadFilesInfo - is used to save the upload files and it returns fileids
		alFilesInfo = gmCommonUploadBean.fetchUploadFilesInfo(strFileIds);
		for(int i=0;i<alFilesInfo.size();i++){
			
			alFilePath = new ArrayList();
			HashMap hmInputParams = GmCommonClass.parseNullHashMap((HashMap)alFilesInfo.get(i));
			strFileGrp = GmCommonClass.parseNull((String)hmInputParams.get("FILEREFGRP"));
    		strFileRefId = GmCommonClass.parseNull((String)hmInputParams.get("FILEREFID"));
			strFileName = GmCommonClass.parseNull((String)hmInputParams.get("FILENAME"));
			strArtifactType = GmCommonClass.parseNull((String)hmInputParams.get("REFTYPE"));
			strSubType = GmCommonClass.parseNull((String)hmInputParams.get("SUBTYPE"));
			
	    	if(!strFileName.equals("")){
	    		
	    		alFilePath.add(strFileGrp);
				alFilePath.add(strFileRefId);
				alFilePath.add(strArtifactType);
				alFilePath.add(strSubType);
				
				hmFileProp.put("SEPARATOR", "\\");
				hmFileProp.put("APPENDPATH", alFilePath);
				hmFileProp.put("FILENAME", strFileName);
				// getFilePath is used for forming the file path
				strAdditionalFilePath = gmProdCatUtil.getFilePath(hmFileProp);
				log.debug("strAdditionalFilePath="+strAdditionalFilePath);
				if(strAction.equals("UPLOAD")){
					//copyFiles is used to copy the files from Portal Folder to Egnyte Folder 
					gmCommonUploadBean.copyFile(strPortalPath+strAdditionalFilePath+strFileName,strEgnytePath+strAdditionalFilePath+strFileName);
				}
				if(strAction.equals("VOID")){
					//deleteFiles is used to delete the files from  Egnyte Folder 
					gmCommonUploadBean.deleteFile(strEgnytePath+strAdditionalFilePath+strFileName);
				}
	    	}
        }
	}// End of saveEgnyteFileSync
	/**
	 * /
	 *  saveSharedFileDtls to save the details of the links and retry count.Retry count is the no of we tried creating a link with Egnyte
	 *  
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public void saveSharedFileDtls(HashMap hmParam) throws AppError {
		log.debug("saveSharedFileDtls"+hmParam);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pdmc_share_eng_trans.gm_upd_file_link_dtls", 6);
		
		gmDBManager.setString(1, GmCommonClass.parseNull(((String) hmParam.get("SHAREFILEID"))));
		gmDBManager.setString(2, GmCommonClass.parseNull((String)hmParam.get("LINKSTATUS")));
		gmDBManager.setString(3, GmCommonClass.parseNull((String)hmParam.get("LINK")));
		gmDBManager.setString(4, GmCommonClass.parseNull((String)hmParam.get("LINKEXPDATE")));
		gmDBManager.setString(5, GmCommonClass.parseNull((String)hmParam.get("LINKRETRYCNT")));
		gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("SHAREID")));
				
		gmDBManager.execute();
		gmDBManager.commit();
		
	}// End of saveSharedFileDtls
	
	
	/**
	 * /
	 *  saveSharedEmailStaus is used to update the status of the of email
	 *  
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public void saveSharedEmailStaus(String strInputstrStatus,String strStatus) throws AppError {
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pdmc_share_eng_trans.gm_upd_share_email_status", 2);
		gmDBManager.setString(1, strInputstrStatus);
		gmDBManager.setString(2, strStatus);
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	/**
	 * /
	 *  saveShareDtls is used to update the status of the share
	 *  
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public void saveShareDtls(HashMap hmParam) throws AppError {
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pdmc_share_eng_trans.gm_sav_shared_dtls", 7);
		gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);
		
		gmDBManager.setString(1, GmCommonClass.parseNull(((String)hmParam.get("EXTSHAREID"))));
		gmDBManager.setString(2, GmCommonClass.parseNull(((String)hmParam.get("SHARETYPE"))));
		gmDBManager.setString(3, GmCommonClass.parseNull(((String)hmParam.get("SUBJECT"))));
		gmDBManager.setString(4, GmCommonClass.parseNull(((String)hmParam.get("MSG"))));
		gmDBManager.setString(5, GmCommonClass.parseNull(((String)hmParam.get("USERID"))));
		gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("SHARESTATUS")));
		gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("SHAREID")));
				
		gmDBManager.execute();
		String strShareID = GmCommonClass.parseNull((String)gmDBManager.getString(7));
		gmDBManager.commit();
		log.debug("saveShareDtls:strShareID"+strShareID);
		
	}
	
	
	/**
	 * createEgnyteResourceLink this method is used to create egnyte link by transforming the return JSON in VO.
	 * @param hmParam
	 * @return
	 * @throws AppError
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws IOException
	 * @throws ParseException
	 */
	public HashMap createEgnyteResourceLink(HashMap hmParam) throws AppError, JsonGenerationException, JsonMappingException, JsonParseException, IOException, ParseException ,Exception{
		
		String strLinkExpiryDate = "";
		Links [] arrLinks;
		String strlink ="";
		String strJsonRecEgnyte  = "";
		GmEgnyteReceiveVO gmEgnyteReceiveVO = new  GmEgnyteReceiveVO(); 
		HashMap hmParamOut = new HashMap();
		GmWSUtil gmWSUtil=new GmWSUtil();
		GmEgnyteClient gmEgnyteClient = new GmEgnyteClient();
		GmEgnyteSendVO gmEgnyteSendVO = new GmEgnyteSendVO();
		
		HashMap hmErrorEmailParam = new HashMap();
		
		String strExpDate = "";
		
		try{
			
			gmEgnyteSendVO.setExpiry_date((String)hmParam.get("EXPDATE"));
			gmEgnyteSendVO.setPath((String)hmParam.get("RESPATH"));
			// PC-3588 :: The below single line code change has been done for temporary.
			gmEgnyteSendVO.setRecipients("notification-nonprod@globusmedical.com");
			
			gmEgnyteClient.setObjEgnyteSendVO(gmEgnyteSendVO);
			//Create Egnyte Link
			strJsonRecEgnyte = gmEgnyteClient.createEgnyteLink();

			log.debug("strJsonRecEgnyte >>>>"+ strJsonRecEgnyte);
			
			ObjectMapper mapper = new ObjectMapper();
			log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readValue(strJsonRecEgnyte, Object.class)));
			gmEgnyteReceiveVO =mapper.readValue(strJsonRecEgnyte, GmEgnyteReceiveVO.class);
			
			log.debug("gmEgnyteReceiveVO:"+gmEgnyteReceiveVO.toString());
			strLinkExpiryDate = gmEgnyteReceiveVO.getExpiry_date();
			
			Date dtExpDate = GmCommonClass.getStringToDate(strLinkExpiryDate, "yyyy-MM-dd");
			//1 day added since egnyte returns correct  expiry date in VO but during viewing collateral 1 day is added to expiry date
			strExpDate= GmCommonClass.getAddDaysToCurrentDateWithFormat(1,dtExpDate,"MM/dd/yyyy");
			//String strExpDate1 =GmCommonClass.getFormattedDate(strLinkExpiryDate,"yyyy-MM-dd","MM/dd/yyyy");
			//log.debug("strExpDate1:"+strExpDate1);
			log.debug("strExpDate added 1 day:"+strExpDate);
			
			arrLinks = gmEgnyteReceiveVO.getLinks();
			strlink = gmWSUtil.getDbInputStringFromVO(arrLinks,",","",Links.getSaveSharedLinkProperties(),"getSaveSharedLinkProperties");
			
			log.debug("createEgnyteResourceLink: strlink:"+strlink);
			hmParamOut.put("LINK", strlink);
			hmParamOut.put("LINKEXPDATE", strExpDate);
			hmParamOut.put("LINKSTATUS", "103242");//103242 -Link Shared
			
		}catch(JsonParseException jpe){
			//Token error and parse Json error
			hmParamOut.put("LINK", "");
			hmParamOut.put("LINKEXPDATE", "");
			hmParamOut.put("LINKSTATUS", "103243");//103243 link not Available
			//Send email if token invalid
			if (strJsonRecEgnyte.equals("<h1>Developer Inactive</h1>")){
				hmParamOut.put("INVALIDEGNYTETOKEN", "INVALIDEGNYTETOKEN");
			}
			log.debug("JsonParseException hmParamOut:"+hmParamOut);
			jpe.printStackTrace();
		}catch(Exception ex){
			String strMessage = GmCommonClass.parseNull(gmEgnyteReceiveVO.getMessage());
			if (strMessage.equals("Specified file or folder does not exist")){
				hmErrorEmailParam.put("FILEPATH", gmEgnyteSendVO.getPath());
				hmErrorEmailParam.put("SUBJECT", strMessage);
				sendJiraTokenAndFileEmail(hmErrorEmailParam);
			}
			hmParamOut.put("LINK", "");
			hmParamOut.put("LINKEXPDATE", "");
			hmParamOut.put("LINKSTATUS", "103243");//103243 link not Available
			log.debug("Exception hmParamOut:"+hmParamOut);
			ex.printStackTrace();
		}
		log.debug("createEgnyteResourceLink hmParamOut"+hmParamOut);
		return hmParamOut;
	}
	
	/**
	 *  getVOfromJson is used to send email to recipients. It contains links to resource in egnyte
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 
	public String getVOfromJson(String strJson ,Object objToVO) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readValue(strJson, Object.class)));
		Object objSendVO =mapper.readValue(strJson, objToVO.class);
		
	}*/

	
	/**
	 *  sendMarketingCollEmail is used to send email to recipients. It contains links to resource in egnyte
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 */
	public String sendMarketingCollEmail(HashMap hmShDetParam) throws Exception {

		//Get all list from input param
		ArrayList alShareDtls = new ArrayList();
		ArrayList alShareEmailDtls = new ArrayList();
		ArrayList alEmailLinkFound = new ArrayList();
		ArrayList alEmailLinkNotFound = new ArrayList();
		
		//Rep Info
		HashMap hmParamRepDtls = new HashMap(); 
		
		HashMap hmEmailParam = new HashMap();
		HashMap hmShareParam = new HashMap();
		
		//To and CC buffer
		StringBuffer sbToEmailId = new StringBuffer();
		StringBuffer sbCcEmailId = new StringBuffer();
		StringBuffer sbShareDetlId = new StringBuffer();//to update status
		StringBuffer sbFilteredToEmailId = new StringBuffer();//Filtered email
		
		String strMsgDtls = "";
		String strSubject = "";
		String strFrom ="";
		String strMimeType = "";
		
		String strRepId = "";
		String strRepPhone ="";
		String strRepEmail ="";
		String strFirstName ="";
		String strLastName ="";
		
		String strExpDate ;
		
		String strCompId ="";
		String strCompAdd ="";
		String strCompName ="";
		String strPaperWork="";
		String strHeadPath ="";
		String strUserId = "";
		
		GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(); 

		//Get all list from input param
		alShareEmailDtls = (ArrayList)hmShDetParam.get("SHAREEMAILDTLS");
		alShareDtls = (ArrayList)hmShDetParam.get("SHAREDTLS");
		strExpDate = (String)hmShDetParam.get("EXPDATE");
		
		alEmailLinkFound = GmCommonClass.parseNullArrayList((ArrayList)hmShDetParam.get("ALLINKFOUND"));
		alEmailLinkNotFound = GmCommonClass.parseNullArrayList((ArrayList)hmShDetParam.get("ALLINKNOTFOUND"));
		log.debug("alEmailLinkNotFound:1111"+alEmailLinkNotFound);
		//Convert expiry date to MM/DD/YYYY
		//remove Conversion done in During Egnyte call 
		//strExpDate =GmCommonClass.getFormattedDate(strExpDate,"yyyy-MM-dd","MM/dd/yyyy");
		Date dtExpDate = GmCommonClass.getStringToDate(strExpDate, "yyyy-MM-dd");
		strExpDate= GmCommonClass.getAddDaysToCurrentDateWithFormat(1,dtExpDate,"MM/dd/yyyy");
		
		String strJasperName = "/GmMarketingCollEmail.jasper";
		GmEmailProperties emailProps = new GmEmailProperties();
		GmJasperMail jasperMail = new GmJasperMail();
		HashMap hmJasperParam = new HashMap();
		HashMap hmReturnDetails = new HashMap();
		
		
			//extract subject , message rep for this share for jasper Email
			hmShareParam = (HashMap)alShareDtls.get(0);//Only one share
			strSubject = GmCommonClass.parseNull((String)hmShareParam.get("SUBJECT"));
			strMsgDtls = GmCommonClass.parseNull((String)hmShareParam.get("MSGDETAIL"));
			strRepId = GmCommonClass.parseNull((String)hmShareParam.get("REPID"));
			strCompId = GmCommonClass.parseNull((String)hmShareParam.get("COMPID"));
			strUserId = GmCommonClass.parseNull((String)hmShareParam.get("USERID"));
		
			
			//Loop the details of the all the email Ids and based recipient type for To and CC email list seperate by comma
			for (int intEmailId = 0;intEmailId < alShareEmailDtls.size() ;intEmailId++ )
			{
				hmEmailParam = (HashMap)alShareEmailDtls.get(intEmailId); 
				String strEmailId = GmCommonClass.parseNull((String)hmEmailParam.get("EMAILID"));
				String strEmailStatus = GmCommonClass.parseNull((String)hmEmailParam.get("EMAILSTATUS"));
				String strRecipientType = GmCommonClass.parseNull((String)hmEmailParam.get("RECIPIENTTYPE"));
				String strShareDetlId = GmCommonClass.parseNull((String)hmEmailParam.get("SHAREDTLID"));

				//103236-TO -strRecipientType  and 103235 filter email status
				if (strRecipientType.equals("103236") &&  !strEmailStatus.equals("103235")){
					sbToEmailId.append(strEmailId).append(",");
					//Else 103237 - CC and 103235 c901_email_status
				}else if(strRecipientType.equals("103237") &&  !strEmailStatus.equals("103235")){
					sbCcEmailId.append(strEmailId).append(",");
				}
				//103235 filter email status
				if(!strEmailStatus.equals("103235")){
					sbShareDetlId.append(strShareDetlId).append(",");// update status using this string
				}
				
				//Filtered list
				if (strRecipientType.equals("103236") &&  strEmailStatus.equals("103235")){
					sbFilteredToEmailId.append(strEmailId).append(",").append(" ");
					
				}
				
			}
			//convert to string and strip the last comma
			String strShareDetlId = sbShareDetlId.toString();
			strShareDetlId = strShareDetlId.substring(0, strShareDetlId.length()-1);
			
			
			
		//fetch address
		
		strPaperWork = System.getProperty("ENV_PAPERWORKIMAGES");
		
		// PMT-34743 : Robotics Rep cannot share collateral details
		
		// Currently robotics company logo and information not yet added (properties)
		// Robotics company is same as Globus Medical Inc - so we always look the default company information.
		
		if (strCompId.equals("100801")){
			String strAlgeahead = GmCommonClass.parseNull(GmCommonClass.getString(strCompId+"_LETTERHEAD"));
			strCompAdd = GmCommonClass.parseNull(GmCommonClass.getString(strCompId+"_COMPADDRESS"));	
			strCompName = GmCommonClass.parseNull(GmCommonClass.getString(strCompId+"_COMPNAME"));
			//strHeadPath = strPaperWork+strAlgeahead;
			strHeadPath = strAlgeahead;
		} else {
			// PMT-34743: to default to Globus Medical Inc
			strCompId = "100800";
			
			String strGlobushead = GmCommonClass.parseNull(GmCommonClass.getString(strCompId+"_LETTERHEAD"));
			strCompAdd = GmCommonClass.parseNull(GmCommonClass.getString(strCompId+"_COMPADDRESS"));	
			strCompName = GmCommonClass.parseNull(GmCommonClass.getString(strCompId+"_COMPNAME"));
			//strHeadPath = strPaperWork+strGlobushead;
			strHeadPath = strGlobushead;
		}
	
		
		//Fetch Rep Info and prepare haspmap for Jasper	email
		//If the Rep ID is not coming in, then it is an user,hence we are getting the user details rather than Rep Details.
		if (!strRepId.equals(""))
			hmParamRepDtls = gmCustSalesSetupBean.loadEditSalesRep(strRepId);
		else
			hmParamRepDtls = fetchUserDetails(strUserId);
		
		strRepPhone = GmCommonClass.parseNull((String)hmParamRepDtls.get("PHONE"));
		strRepEmail = GmCommonClass.parseNull((String)hmParamRepDtls.get("EMAIL"));
		strFirstName = GmCommonClass.parseNull((String)hmParamRepDtls.get("FNAME"));
		strLastName = GmCommonClass.parseNull((String)hmParamRepDtls.get("LNAME"));
		
		hmJasperParam.put("MESSAGE", strMsgDtls);
		hmJasperParam.put("EMAIL", strRepEmail);
		hmJasperParam.put("PHONE", strRepPhone);
		hmJasperParam.put("EXPDATE", strExpDate);
		hmJasperParam.put("REPNAME", strFirstName+" "+strLastName);
		hmJasperParam.put("COMPNM", strCompName);
		hmJasperParam.put("COMPADD", strCompAdd);
		hmJasperParam.put("HEADER", strHeadPath);
		hmJasperParam.put("CONTEXTURL", GmCommonClass.contextURL);
		//Prepare Properties for jasper email
		
		strMimeType = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(MKTEMAIL_TEMPLATE_NAME	+ "." + GmEmailProperties.MIME_TYPE));
		
		//Email should go from rep and not from notification@globusmedical, that is why the below line is commented and rep email equals from
		//strFrom = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(MKTEMAIL_TEMPLATE_NAME + "."+ GmEmailProperties.FROM));
		strFrom = strRepEmail;
		
		emailProps.setMimeType(strMimeType);
		emailProps.setSender(strFrom); 
		emailProps.setRecipients(sbToEmailId.toString());
		emailProps.setCc(sbCcEmailId.toString());
		emailProps.setSubject(strSubject);
		
		log.debug("hmJasperParam Addi param:"+hmJasperParam);
		log.debug("emailProps:"+emailProps);
		
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setTemplateSubDir("prodmgmnt/templates");
		templateUtil.setTemplateName("GmMCShareEmail.vm");
		templateUtil.setDataList("alEmailLinkFound", alEmailLinkFound);
	    templateUtil.setDataMap("hmEmailParam",  hmJasperParam);
	    String strEmailContent = templateUtil.generateOutput();
	    log.debug(" : strOut >>> " + strEmailContent);
	    
	    HashMap hmEmailProperty = new HashMap();
	    hmEmailProperty.put("strFrom", emailProps.getSender());
		hmEmailProperty.put("strTo", emailProps.getRecipients());
		hmEmailProperty.put("strCc", emailProps.getCc());
		hmEmailProperty.put("strSubject", emailProps.getSubject());
		hmEmailProperty.put("strMessageDesc", strEmailContent);
		hmEmailProperty.put("strMimeType", emailProps.getMimeType());
		
		// PC-3000: integrate email to sendgrid api
		hmEmailProperty.put("TRANSACTION_ID", GmCommonClass.parseNull((String) hmShDetParam.get("SHAREID")));
		//110680	Marketing Email
		hmEmailProperty.put("EMAIL_TRACKING_TYPE", GmCommonClass.parseNull((String) hmShDetParam.get("EMAIL_TRACKING_TYPE")));
		// based on the flag to call the sendGrid api
		hmEmailProperty.put("EMAIL_TRACKING_FL", GmCommonClass.parseNull((String) hmShDetParam.get("EMAIL_TRACKING_FL")));
		
	    GmCommonClass.sendMail(hmEmailProperty);
	    
		/*
		jasperMail.setJasperReportName(strJasperName);
		jasperMail.setAdditionalParams(hmJasperParam);
		jasperMail.setReportData(alEmailLinkFound);//alEmailMsg
		jasperMail.setEmailProperties(emailProps);
		hmReturnDetails = jasperMail.sendMail();
		
		*/
	  //Link not found email
	    //log.debug("alEmailLinkNotFound:"+alEmailLinkNotFound);
		String strFilteredToEmailId = GmCommonClass.parseNull(sbFilteredToEmailId.toString());
		if(!strFilteredToEmailId.equals("")){//Required for index out of bound
			strFilteredToEmailId = strFilteredToEmailId.substring(0, strFilteredToEmailId.length()-1);
		}
		
		if(!strFilteredToEmailId.equals("") ||alEmailLinkNotFound.size()>0 ){
			sendMarketingCollFailureEmail(strHeadPath,strFilteredToEmailId,alEmailLinkNotFound,emailProps);
		}
		
		
		return strShareDetlId;
	}
	
	/*
	 * This method is written temporarily and it will be moved to the User Management Bean Class.
	 * This method will get the user details for the passed in USer ID.
	 * For Internal Users as there is no phone number available in the DB, we are taking it from the properties file. 
	 */
	public HashMap fetchUserDetails(String strUserID) throws AppError {
		log.debug("Enter");
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn = new HashMap();
		String strDemandType = "";
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT C101_USER_F_NAME FNAME,C101_USER_L_NAME LNAME,C101_EMAIL_ID EMAIL,C101_USER_SH_NAME SHRT_NAME ");
		sbQuery.append("  FROM T101_USER WHERE  C901_USER_STATUS='311' AND C101_USER_ID=? ");

		gmDBManager.setPrepareString(sbQuery.toString());
		gmDBManager.setString(1, strUserID);
		hmReturn = gmDBManager.querySingleRecord();
		//Globus Phone Number
		String strCompanyPhone = GmCommonClass.parseNull(GmCommonClass.getString("FEDEX_PHONE"));
		hmReturn.put("PHONE", strCompanyPhone);
		
		log.debug("hmReturn: " + hmReturn);
		return hmReturn;
	}
	
	
	/**
	 *  splitMarketingCollEmailParam is used to split the arraylist to have two section links avaialble and not available
	 *  
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 * 
	 */
	public HashMap splitMarketingCollEmailParam(ArrayList alEmailMsg) throws AppError{
		
		HashMap hmSplitreportParam = new HashMap();
		ArrayList alLinkFound = new ArrayList();
		ArrayList alLinkNotFound = new ArrayList();
		for (int intLinkId = 0;intLinkId < alEmailMsg.size() ;intLinkId++ )
		{
			HashMap hmLinkFoundParam =  new HashMap(); ; 
			HashMap hmLinkNotFoundParam  = new HashMap(); ; 
			HashMap hmLinkParam = (HashMap)alEmailMsg.get(intLinkId); 
			String strLink = GmCommonClass.parseNull((String)hmLinkParam.get("LINK"));
			if (!strLink.equals("")){
				hmLinkFoundParam.put("FILETITLE",hmLinkParam.get("FILETITLE"));
				hmLinkFoundParam.put("FILEEXTENSION",hmLinkParam.get("FILEEXTENSION"));
				hmLinkFoundParam.put("ARTIFACTNM",hmLinkParam.get("ARTIFACTNM"));
				hmLinkFoundParam.put("LINK",hmLinkParam.get("LINK"));
				hmLinkFoundParam.put("HYPERLINK",hmLinkParam.get("HYPERLINK"));
				hmLinkFoundParam.put("LINKEXPDATE",hmLinkParam.get("LINKEXPDATE"));
				hmLinkFoundParam.put("LINKSTATUS",hmLinkParam.get("LINKSTATUS"));
				
				alLinkFound.add(hmLinkFoundParam);
			}else{
				
				hmLinkNotFoundParam.put("FILETITLE", hmLinkParam.get("FILETITLE"));
				hmLinkNotFoundParam.put("ARTIFACTNM", hmLinkParam.get("ARTIFACTNM"));
				hmLinkNotFoundParam.put("LINKSTATUS", hmLinkParam.get("LINKSTATUS"));
				hmLinkNotFoundParam.put("FILEEXTENSION",hmLinkParam.get("FILEEXTENSION"));
				hmLinkNotFoundParam.put("HYPERLINK",hmLinkParam.get("HYPERLINK"));
				
				alLinkNotFound.add(hmLinkNotFoundParam);
			}
			
			
		}
		hmSplitreportParam.put("ALLINKFOUND", alLinkFound);
		hmSplitreportParam.put("ALLINKNOTFOUND", alLinkNotFound);
		log.debug("hmSplitreportParam:"+hmSplitreportParam);

		return hmSplitreportParam;
		
	}
	/**
	 *  sendMarketingCollFailureEmail is used to send failed email for filter list and failed collaterals
	 *  
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 * @throws Exception 
	 * 
	 */
	public void sendMarketingCollFailureEmail(String strHeadPath,String strFilterIDs,ArrayList alLinkNotFound,GmEmailProperties emailPropsFail) throws Exception{
		
		GmEmailProperties emailProps = new GmEmailProperties();
		GmJasperMail jasperMail = new GmJasperMail();
		HashMap hmJasperParam = new HashMap();
		HashMap hmReturnDetails = new HashMap();
		//String strSubject = "Product Information - Filtered Email IDs / Unavailable Links";
		String strJasperName = "/GmMarketingCollEmailFailure.jasper";
		
		String strFrom = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(MKTEMAIL_FAILURE_TEMPLATE_NAME + "."+ GmEmailProperties.FROM));
		String strSubject = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(MKTEMAIL_FAILURE_TEMPLATE_NAME + "."+ GmEmailProperties.SUBJECT));
		String strCC = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(MKTEMAIL_FAILURE_TEMPLATE_NAME + "."+ GmEmailProperties.CC));
		
		hmJasperParam.put("FILTERLIST", strFilterIDs);
		hmJasperParam.put("HEADER", strHeadPath);
		
			
		//set email properties
		emailProps.setMimeType(emailPropsFail.getMimeType());
		emailProps.setSender(strFrom); 
		emailProps.setRecipients(emailPropsFail.getSender());
		emailProps.setSubject(strSubject);
		emailProps.setCc(strCC);
		
		log.debug("Failure emailPropsFail:"+emailPropsFail);
		log.debug("Failure emailProps:"+emailProps);
		log.debug("Failure hmJasperParam:"+hmJasperParam);
		log.debug("Failure alLinkNotFound:"+alLinkNotFound);
		
	/*	HashMap hmEmailProperty = new HashMap();
	    hmEmailProperty.put("strFrom",strFrom);
		hmEmailProperty.put("strTo", emailProps.getRecipients());
		//hmEmailProperty.put("strCc", emailProps);
		hmEmailProperty.put("strSubject", strSubject);
		//hmEmailProperty.put("strMessageDesc", strEmailContent);
		hmEmailProperty.put("strMimeType", emailPropsFail.getMimeType());
		
		
		
		
		log.debug("Failure hmEmailProperty:"+hmEmailProperty);
			*/
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setTemplateSubDir("prodmgmnt/templates");
		templateUtil.setTemplateName("GmMCShareFailureEmail.vm");
		templateUtil.setDataList("alLinkNotFound", alLinkNotFound);
	    templateUtil.setDataMap("hmEmailParam",  hmJasperParam);
	    String strEmailContent = templateUtil.generateOutput();
	    log.debug(" : strOut >>> " + strEmailContent);
	    
	    HashMap hmEmailProperty = new HashMap();
	    hmEmailProperty.put("strFrom", emailProps.getSender());
		hmEmailProperty.put("strTo", emailProps.getRecipients());
		hmEmailProperty.put("strCc", emailProps.getCc());
		hmEmailProperty.put("strSubject", emailProps.getSubject());
		hmEmailProperty.put("strMessageDesc", strEmailContent);
		hmEmailProperty.put("strMimeType", emailProps.getMimeType());
		
	
/*		jasperMail.setJasperReportName(strJasperName);
		if(!strFilterIDs.equals("")){
			jasperMail.setAdditionalParams(hmJasperParam);
		}
		
		if(alLinkNotFound.size()>0){
			jasperMail.setReportData(alLinkNotFound);//alEmailMsg
		}
		
		jasperMail.setEmailProperties(emailProps);
		hmReturnDetails = jasperMail.sendMail();*/
		
	    GmCommonClass.sendMail(hmEmailProperty);
	    log.debug("Failure email Sent >>>");
		
	}

	
	/**
	 *  sendJiraEmail create JIRA tick if link cannot be created  
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 * 
	 */
	public void sendJiraEmail(HashMap hmShDetParam) throws Exception{


		String strProdCatFilePath = GmCommonClass.getString("PRODCATALOGIMAGEURL");
		String strEgnyteBaseDir = GmCommonClass.getString("EGNYTEMARKETINGCOLLBASEDIR");
		StringBuffer sbFile = new StringBuffer();
		//Fetch share files details declaration
		HashMap hmFileParam = new HashMap();
		String strGrpNm = "";
		String strRefIdFile = "";
		String strTypeId = ""; //
		String strRefTpId = "";
		String strRefTpNm = "";
		String strFileNm = "";
		String strShareFileId = "";
		String strFileTitle = "";
		String strFileExt = "";
		String strShareId ="";
		String strLinkId ="";
		
		String strSubject = "";

		//ArrayList alEmailMsg = (ArrayList)hmShDetParam.get("EMAILMSG");
		ArrayList alShareFileDtls = (ArrayList)hmShDetParam.get("SHAREFILEDTLS");
		//Loop the details of all the files 
		for (int intFile = 0;intFile < alShareFileDtls.size() ;intFile++ )
		{
			//Preprare Hashmap for Email
			HashMap hmEmailParam = new HashMap();

			hmFileParam = (HashMap)alShareFileDtls.get(intFile);
			strGrpNm = GmCommonClass.parseNull((String)hmFileParam.get("REFGRPNM"));
			strRefIdFile = GmCommonClass.parseNull((String)hmFileParam.get("REFIDFILE"));
			strRefTpId = GmCommonClass.parseNull((String)hmFileParam.get("REFTPID"));
			strRefTpNm = GmCommonClass.parseNull((String)hmFileParam.get("REFTPNM"));
			strTypeId = GmCommonClass.parseNull((String)hmFileParam.get("TYPEID")); //
			strFileNm = GmCommonClass.parseNull((String)hmFileParam.get("FILENM"));
			strShareId = GmCommonClass.parseNull((String)hmFileParam.get("SHAREID"));
			strLinkId = GmCommonClass.parseNull((String)hmFileParam.get("LINKID"));
			
			if(strLinkId.equals("")){
				sbFile.append(strEgnyteBaseDir+strProdCatFilePath+"/"+strGrpNm+"/"+strRefIdFile+"/"+strRefTpId+"/"+strTypeId+"/"+strFileNm).append("<BR/>");
			}
			 
		}
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(LINKUNAVAIL_TEMPLATE_NAME+ "." + GmEmailProperties.FROM));
		emailProps.setRecipients(GmCommonClass.getEmailProperty(LINKUNAVAIL_TEMPLATE_NAME+ "." + GmEmailProperties.TO));
		emailProps.setCc(GmCommonClass.getEmailProperty(LINKUNAVAIL_TEMPLATE_NAME+ "." + GmEmailProperties.CC));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(LINKUNAVAIL_TEMPLATE_NAME+ "." + GmEmailProperties.MIME_TYPE));
		String strMessage = GmCommonClass.getEmailProperty(LINKUNAVAIL_TEMPLATE_NAME+ "." + GmEmailProperties.MESSAGE);
		strMessage = GmCommonClass.replaceAll(strMessage,"#<MessageBody>", sbFile.toString());
		//emailProps.setSubject();

		strSubject =  GmCommonClass.getEmailProperty(LINKUNAVAIL_TEMPLATE_NAME+ "." + GmEmailProperties.SUBJECT);
		strSubject = GmCommonClass.replaceAll(strSubject,"#<SHAREID>", strShareId);
		
		emailProps.setMessage(strMessage);
		emailProps.setSubject(strSubject);
		GmCommonClass.sendMail(emailProps);
		
	}
	
	/**
	 *  sendJiraTokenAndFileEmail create JIRA ticket if file is missing or if the token to connect to Egnyte is invalid 
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 */
	public void sendJiraTokenAndFileEmail(HashMap hmParam) throws Exception {


		String strSubject = "";
		String strMessage  = "";
		
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(TOKENFILE_TEMPLATE_NAME+ "."  + GmEmailProperties.FROM));
		emailProps.setRecipients(GmCommonClass.getEmailProperty(TOKENFILE_TEMPLATE_NAME+ "." + GmEmailProperties.TO));
		emailProps.setCc(GmCommonClass.getEmailProperty(TOKENFILE_TEMPLATE_NAME+ "." + GmEmailProperties.CC));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(TOKENFILE_TEMPLATE_NAME+ "." + GmEmailProperties.MIME_TYPE));
		
		strMessage = GmCommonClass.getEmailProperty(TOKENFILE_TEMPLATE_NAME+ "." + GmEmailProperties.MESSAGE);
		strMessage = GmCommonClass.replaceAll(strMessage,"#<MessageBody>", (String)hmParam.get("FILEPATH"));
		strSubject = (String)hmParam.get("SUBJECT");
		
		//If token subject is empty string
		if (strSubject.equals("")){
			strSubject =  GmCommonClass.getEmailProperty(TOKENFILE_TEMPLATE_NAME+ "." + GmEmailProperties.SUBJECT);
			strMessage = strSubject;
		}
		emailProps.setSubject(strSubject);
		emailProps.setMessage(strMessage);
		GmCommonClass.sendMail(emailProps);
		
	}
	
	
}
