package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmUDIProductDevelopmentBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * savePdUDISetupDetails - save the PD part setup details to PD table
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public HashMap savePdUDISetupDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn = new HashMap();
		String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
		String strDeleteParts = GmCommonClass.parseNull((String) hmParam.get("DELETEPARTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strErrorMsg = "";
		String strSuccessParts = "";
		ArrayList alSavedParts = new ArrayList();

		gmDBManager.setPrepareString("gm_pkg_pd_sav_udi.gm_sav_prod_dev_udi_dtls", 4);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		strSuccessParts = GmCommonClass.parseNull((String)gmDBManager.getString(3));
		strErrorMsg = GmCommonClass.parseNull((String)gmDBManager.getString(4));
		if(!strErrorMsg.equals("")){
			gmDBManager.close();
		}else{
			gmDBManager.commit();
		}
		hmReturn.put("ERROR_PARTS_STR", strErrorMsg);
		hmReturn.put("SUCCESS_PARTS_STR", strSuccessParts);
		return hmReturn;
	}

	/**
	 * savePdPartUDISetupDetails - save the Part number PD tab details.
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void savePdPartUDISetupDetails(HashMap hmParam) throws AppError {
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmDBManager gmDBManager = new GmDBManager();
		
		String strInputStr = GmCommonClass.parseNull((String) hmParam.get("STRINPUTSTR"));
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		
		gmDBManager.setPrepareString("gm_pkg_pd_sav_udi.gm_sav_part_pd_udi_dtls", 3);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strPartNumber);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		if (!strLogReason.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strPartNumber, strLogReason,
					strUserId, "1293");// 1293-->PD Tab input screen log comments
		}
		gmDBManager.commit();
	}

	/**
	 * fetchConfigName - This method used to fetch the issuing agency configured
	 * names
	 * 
	 * @param String
	 * @return HashMap
	 * @exception AppError
	 */
	public ArrayList fetchProdDevUdiDetails(HashMap hmParam) throws AppError {
		ArrayList alReturn = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager();
		boolean blFilterFl = false;
		String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
		String strPartNumDesc = GmCommonClass.parseNull((String) hmParam.get("PARTDESC"));
		String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		String strInput = GmCommonClass.parseNull((String) hmParam.get("PARTNUMSTR"));
		strInput = GmCommonClass.getStringWithQuotes(strInput);
		

		sbQuery.append(" SELECT T205G.c205g_pd_part_number_id pd_id, NVL2( V205G.C205_PART_NUMBER_ID, V205G.C205_PART_NUMBER_ID,T205G.C205_PART_NUMBER_ID) pnum, ");
		sbQuery.append(" NVL2(V205G.C205_PART_NUMBER_ID, V205G.PROJECTID, T205G.c202_project_id) PROJECTID,");
		sbQuery.append("  NVL2 (V205G.C205_PART_NUMBER_ID, V205G.part_desc, T205G.c205_part_num_desc) part_desc, ");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_CONTAINS_LATEX), T205G.C901_HAS_LATEX) LATEX , ");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_REQUIRE_STERIL), T205G.C901_REQUIRES_STERILIZATION) REQ_STERILE ,");
		sbQuery.append("NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_STERIL_METHOD), T205G.C901_STERILIZATION_METHOD) STERILE_METHOD ,");
		sbQuery.append("NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_DEVICE_AVAILABLE), T205G.C901_HAS_MORE_THAN_ONE_SIZE) MORE_SIZE,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE1_TYPE), T205G.C901_SIZE1_TYPE) SIZE1,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, V205G.C205D_SIZE1_TYPE_TEXT,T205G.C205G_SIZE1_TYPE_TEXT) SIZE1_TEXT,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE1_VALUE),T205G.C205G_SIZE1_VALUE) SIZE1_VALUE,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID,  to_number (v205g.c205d_size1_uom),T205G.c901_size1_uom) SIZE1_UOM,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE2_TYPE), T205G.C901_SIZE2_TYPE) SIZE2,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, V205G.C205D_SIZE2_TYPE_TEXT,T205G.C205G_SIZE2_TYPE_TEXT) SIZE2_TEXT,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE2_VALUE),T205G.C205G_SIZE2_VALUE) SIZE2_VALUE,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID,  to_number (v205g.c205d_size2_uom),T205G.c901_size2_uom) SIZE2_UOM,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE3_TYPE), T205G.C901_SIZE3_TYPE) SIZE3,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, V205G.C205D_SIZE3_TYPE_TEXT,T205G.C205G_SIZE3_TYPE_TEXT) SIZE3_TEXT,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE3_VALUE),T205G.C205G_SIZE3_VALUE) SIZE3_VALUE,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID,  TO_NUMBER (V205G.C205D_SIZE3_UOM),T205G.C901_SIZE3_UOM) SIZE3_UOM,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE4_TYPE), T205G.C901_SIZE4_TYPE) SIZE4,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, V205G.C205D_SIZE4_TYPE_TEXT,T205G.C205G_SIZE4_TYPE_TEXT) SIZE4_TEXT,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID, TO_NUMBER (V205G.C205D_SIZE4_VALUE),T205G.C205G_SIZE4_VALUE) SIZE4_VALUE,");
		sbQuery.append(" NVL2 (V205G.C205_PART_NUMBER_ID,  to_number (v205g.c205d_size4_uom),T205G.c901_size4_uom) SIZE4_UOM, ");
		sbQuery.append(" DECODE (V205G.PART_STATUS, 20367, 'Y', 20369, 'Y', 'N') QUALITY_FL, "); // 20367 Approved 20369 - Obsolete
		sbQuery.append(" gm_pkg_pd_rpt_udi.get_part_number_cnt(T205G.C205_PART_NUMBER_ID) Q_PART_FL ");
		sbQuery.append(" FROM T205G_PD_PART_NUMBER T205G, V205G_PART_PD_PARAMETER V205G");
		sbQuery.append(" WHERE T205G.C205_PART_NUMBER_ID = V205G.C205_PART_NUMBER_ID(+) ");

		if (!strPartNum.equals("")) {
			blFilterFl = true;
			
	sbQuery.append(" AND REGEXP_LIKE (t205g.c205_part_number_id,REGEXP_REPLACE('");
      sbQuery.append(strPartNum);
      sbQuery.append("','[+]','\\+'))");
		}

		if (!strProjectId.equals("0")) {
			blFilterFl = true;
			sbQuery.append(" AND NVL(V205G.PROJECTID, t205g.c202_project_id) = '" + strProjectId
					+ "' ");
		}
		if (!strPartNumDesc.equals("")) {
			blFilterFl = true;
			sbQuery.append(" AND UPPER (NVL(V205G.part_desc, t205g.c205_part_num_desc)) LIKE '%"
					+ strPartNumDesc.toUpperCase() + "%' ");
		}
		if (strOpt.equals("Print") && !blFilterFl) {
			sbQuery.append(" AND t205g.c205_part_number_id IN ('" + strInput
					+ "') ");
		}
		sbQuery.append(" AND t205g.c205g_void_fl IS NULL ");
		sbQuery.append(" ORDER BY pnum");
		log.debug(" quers for fetchProdDevUdiDetails ==> " + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	/**
	 * fetchProdDevPartUdiDetails
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public HashMap fetchProdDevPartUdiDetails(HashMap hmParam) throws AppError {
		HashMap hmResult = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
		gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_prod_part_dev_udi_dtls", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strPartNum);
		gmDBManager.execute();
		hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmResult;
	}

	/**
	 * validatePartNumber
	 * 
	 * @param strPartNumber
	 * @return
	 * @throws AppError
	 */
	public String validatePartNumber(String strPartNumber) throws AppError {
		String strPartCount = "";
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setFunctionString("gm_pkg_pd_rpt_udi.get_part_number_cnt", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strPartNumber);
		gmDBManager.execute();
		strPartCount = gmDBManager.getString(1);
		gmDBManager.close();
		return strPartCount;
	}

	/**
	 * validateProject
	 * 
	 * @param strProjectId
	 * @return
	 * @throws AppError
	 */
	public String validateProject(String strProjectId) throws AppError {
		String strProjectCount = "";
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setFunctionString("gm_pkg_pd_rpt_udi.get_project_cnt", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strProjectId);
		gmDBManager.execute();
		strProjectCount = gmDBManager.getString(1);
		gmDBManager.close();
		return strProjectCount;
	}

	/**
	 * fetchPDPrintDtls
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public HashMap fetchPDPrintDtls(HashMap hmParam) throws AppError {
		ArrayList alReturn = new ArrayList();
		HashMap hmHeaderDtls = new HashMap();
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		String strInput = GmCommonClass.parseNull((String) hmParam.get("PARTNUMSTR"));
		String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PRINTPROJECTID"));
		String strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
		String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_pd_print_dtls", 6);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.setString(1, strProjectId);
		gmDBManager.setString(2, strInput);
		gmDBManager.setString(3, strOpt);
		gmDBManager.setString(4, strUserId);
		gmDBManager.execute();
		hmHeaderDtls = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(5));
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		gmDBManager.close();
		hmReturn.put("HEADER", hmHeaderDtls);
		hmReturn.put("DETAILS", alReturn);
		return hmReturn;
	}
	/**
	 * saveVoidPdDetails - save the PD part void flag to PD table
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public String saveVoidPdDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		String strDeleteParts = GmCommonClass.parseNull((String) hmParam.get("DELETEPARTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strVoidedParts = "";

		gmDBManager.setPrepareString("gm_pkg_pd_sav_udi.gm_void_prod_dev_udi_dtls", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.setString(1, strDeleteParts);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		strVoidedParts = GmCommonClass.parseNull((String)gmDBManager.getString(3));
		gmDBManager.commit();
		return strVoidedParts;
	}
}
