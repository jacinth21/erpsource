package com.globus.prodmgmnt.beans;

import java.util.ArrayList; 
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.prodmgmnt.GmCollateralEmailTrackVO;
import com.globus.sales.beans.GmSalesFilterConditionBean;

public class GmCollateralEmailTrackBean extends GmSalesFilterConditionBean {

	/**
	 * Code to Initialize the Logger Class.
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmCollateralEmailTrackBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * fetchCollateralActivitityChartDetails - used to fetch the delivered email
	 * reports details
	 * 
	 * @return
	 * @throws AppError
	 */
	public String fetchCollateralActivitityChartDetails(HashMap hmParam) throws AppError {
		initializeParameters(hmParam);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String queryJSONRecord = "";
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCS_LVL"));
		String strDateFormat = getGmDataStoreVO().getCmpdfmt();

		sbQuery.append(
				" SELECT '['||activity_chart_details||']' FROM (SELECT LISTAGG (activity_chart_result,',') WITHIN GROUP (ORDER BY activity_chart_result) activity_chart_details FROM (   ");
		sbQuery.append(
				" SELECT JSON_OBJECT( 'label' VALUE 'TOTAL','value' VALUE count(1)) activity_chart_result FROM mv9161_email_tracking_master mv9161_master");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 
		sbQuery.append(" AND event_type = 110690"); 			// 110690 - Processed

		sbQuery.append(" AND email_sent_date >= TO_DATE('" + strFrmDate + "','" + strDateFormat + "') ");
		sbQuery.append(" AND email_sent_date <= TO_DATE('" + strToDate + "','" + strDateFormat + "') ");
		sbQuery.append(
				" UNION ALL  SELECT JSON_OBJECT( 'label' VALUE 'DELIVERED','value' VALUE count(1)) activity_chart_result FROM mv9161_email_tracking_master mv9161_master ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 
		sbQuery.append(" AND event_type = 110694 "); 				//110694 - Delivered

		sbQuery.append(" AND email_sent_date >= TO_DATE('" + strFrmDate + "','" + strDateFormat + "') ");
		sbQuery.append(" AND email_sent_date <= TO_DATE('" + strToDate + "','" + strDateFormat + "') )) ");

		log.debug("fetchCollateralActivitityChartDetails  Query  " + sbQuery.toString());
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		gmDBManager.close();
		return queryJSONRecord;
	}

	/**
	 * fetchCollateralOpenChartDetails - used to fetch the Open email reports
	 * details
	 * 
	 * @return
	 * @throws AppError
	 */
	public String fetchCollateralOpenChartDetails(HashMap hmParam) throws AppError {
		initializeParameters(hmParam);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCS_LVL"));
		String strDateFormat = getGmDataStoreVO().getCmpdfmt();
		String queryJSONRecord = "";

		sbQuery.append(
				" SELECT '['||open_chart_details||']' FROM (SELECT LISTAGG (open_chart_result,',') WITHIN GROUP (ORDER BY open_chart_result) open_chart_details FROM (   ");
		sbQuery.append(
				" SELECT JSON_OBJECT( 'label' VALUE 'OPEN','value' VALUE count(1)) open_chart_result ");
		sbQuery.append(
				" FROM mv9161_email_tracking_master mv9161_master  ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 
		sbQuery.append(" AND mv9161_master.email_sent_date >= TO_DATE('" + strFrmDate + "','"
					+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','"
					+ strDateFormat + "') ");
		sbQuery.append(" AND event_type = 110697"); 			// 110697 - Open
		sbQuery.append(
				" UNION ALL SELECT JSON_OBJECT( 'label' VALUE 'UNIQUE OPEN','value' VALUE count(1)) open_chart_result FROM  (");
		sbQuery.append(" SELECT MSG_ID,EMAIL_ID, COUNT (1) FROM mv9161_email_tracking_master mv9161_master");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') ");
		sbQuery.append(" AND EVENT_TYPE    = 110697");
		sbQuery.append(" AND mv9161_master.email_sent_date >= TO_DATE('" + strFrmDate + "','"
					+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','"
					+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.first_open_date IS NOT NULL GROUP BY  MSG_ID,EMAIL_ID HAVING COUNT (1) > 0 ))) ");

		log.debug("fetchCollateralOpenChartDetails  Query  " + sbQuery.toString());
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		gmDBManager.close();

		return queryJSONRecord;
	}

	/**
	 * fetchCollateralClickChartDetails - used to click email reports details
	 * 
	 * @return
	 * @throws AppError
	 */
	public String fetchCollateralClickChartDetails(HashMap hmParam) throws AppError {
		initializeParameters(hmParam);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String queryJSONRecord = "";
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCS_LVL"));
		String strDateFormat = getGmDataStoreVO().getCmpdfmt();

		sbQuery.append(
				" SELECT '['||click_chart_details||']' FROM (SELECT LISTAGG (click_chart_result,',') WITHIN GROUP (ORDER BY click_chart_result) click_chart_details FROM (   ");
		sbQuery.append(
				" SELECT JSON_OBJECT( 'label' VALUE 'CLICK','value' VALUE count(1)) click_chart_result FROM mv9161_email_tracking_master mv9161_master ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 
		
		sbQuery.append(" AND mv9161_master.EVENT_TYPE    = 110696");//110696 click
		
		sbQuery.append(" AND mv9161_master.email_sent_date >= TO_DATE('" + strFrmDate + "','"
					+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','"
					+ strDateFormat + "') ");
		sbQuery.append(
				" UNION ALL SELECT JSON_OBJECT( 'label' VALUE 'UNIQUE CLICK','value' VALUE count(1)) click_chart_result FROM (");
		sbQuery.append(" SELECT MSG_ID,EMAIL_ID, COUNT (1) FROM ");
		sbQuery.append(" mv9161_email_tracking_master mv9161_master ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 
		
		sbQuery.append(" AND mv9161_master.EVENT_TYPE    = 110696");//110696 click
		
		sbQuery.append(" AND mv9161_master.email_sent_date >= TO_DATE('" + strFrmDate + "','"
					+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','"
					+ strDateFormat + "')  ");
		sbQuery.append(" AND mv9161_master.first_click_date IS NOT NULL GROUP BY MSG_ID,EMAIL_ID HAVING COUNT (1) > 0 ))) ");

		log.debug("fetchCollateralClickChartDetails  Query  " + sbQuery.toString());
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		gmDBManager.close();

		return queryJSONRecord;
	}
	/**
	 * fetchTotalLinkSends - used to fetch the total link sends
	 * 
	 * @return
	 * @throws AppError
	 */
	public String fetchTotalLinkSends(HashMap hmParam ) {
		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = new GmCollateralEmailTrackVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String queryJSONRecord = "";	
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCS_LVL"));
		String strDateFormat = getGmDataStoreVO().getCmpdfmt();
		sbQuery.append(" SELECT JSON_OBJECT ('Total_link_send' VALUE COUNT(1)) FROM( ");
		sbQuery.append(" SELECT * FROM t1016_share t1016,t9161_email_sent_master t9161,t1017_share_detail t1017,t1018_share_file t1018 ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" t9161.c9161_email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  t9161.c701_distributor_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  t9161.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  t9161.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  t9161.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR t9161.c9161_email_sent_by = '"+ strUserId+ "') "); 
		sbQuery.append(" AND t1016.c1016_share_id = t9161.c9161_transaction_id");
		sbQuery.append(" AND t1016.c1016_share_id = t1017.c1016_share_id");
		sbQuery.append(" AND t1017.c1016_share_id = t1018.c1016_share_id");
		sbQuery.append(" AND t9161.c9161_email_sent_date >= TO_DATE('" + strFrmDate + "','"
				+ strDateFormat + "') ");
		sbQuery.append(" AND t9161.c9161_email_sent_date <= TO_DATE('" + strToDate + "','"
				+ strDateFormat + "')  ");
		sbQuery.append("AND t1017.c1017_email_id NOT IN ('notification-nonprod@globusmedical.com'))");
		log.debug("fetchTotalLinkSends sbQuery--->"+sbQuery);
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		log.debug("fetchTotalLinkSends--> "+hmParam);
		return queryJSONRecord;
	}

	/**
	 * fetchCollateralEmailTrackDetail - used to show Email Track Detailed View report By Sales Rep, Distributor, AD and VP 
	 * @return
	 * @throws AppError
	 */
	public String fetchCollateralEmailTrackDetail(HashMap hmParam) {
		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = new GmCollateralEmailTrackVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String queryJSONRecord = "";
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strToEmailId = GmCommonClass.parseNull((String) hmParam.get("EMAILID"));
		String strEventType = GmCommonClass.parseNull((String) hmParam.get("EVENTTYPE"));
		String strContentTitle = GmCommonClass.parseNull((String) hmParam.get("CONTENTITLE")); 
		String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCS_LVL"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCmpDtFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
		String strDateFormat = strCmpDtFmt + " HH:mi:ss";
		String strTimeDateFormat = strCmpDtFmt + " HH12:mi:ss AM";

		sbQuery.append(
				" SELECT JSON_ARRAYAGG(JSON_OBJECT('status' VALUE eventname, 'status_time' VALUE to_char(emaildate, 'MM/dd/yyyy HH12:mi:ss AM'), ");
		sbQuery.append(
				" 'recipient' VALUE recipientname,'emailid' VALUE emailid, 'content_title' VALUE ftitle, 'links' VALUE fname, 'total_opens' VALUE NVL(TO_CHAR(opencnt), '-'), 'total_clicks' VALUE NVL(TO_CHAR(clickedcnt), '-'), ");
		sbQuery.append(
				" 'email_details_id' VALUE emaildtlsid, 'event_type' VALUE eventtype, 'url' VALUE url) RETURNING CLOB) ");
		sbQuery.append(
				" FROM ( SELECT COUNT(1), mv9161_master.event_type_name   eventname, mv9161_master.email_sent_date emaildate, ");
		sbQuery.append(
				" mv9161_details.recipient_name  recipientname, mv9161_details.email_id emailid,  mv9161_details.file_title ftitle,  mv9161_details.system_name fname, ");
		sbQuery.append(
				" get_email_track_event_count(mv9161_master.email_dtls_id, 110697) opencnt,  get_email_track_click_count(mv9161_master.email_dtls_id, url) clickedcnt, mv9161_master.email_dtls_id emaildtlsid, ");
		sbQuery.append(" mv9161_master.event_type eventtype, mv9161_details.url url ");
		sbQuery.append(
				" FROM mv9161_email_tracking_master mv9161_master, mv9161_email_tracking_details mv9161_details ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 
		if (!strToEmailId.equals("")) {
			sbQuery.append(" AND mv9161_details.email_id = '" + strToEmailId + "' ");
		}
		else if (!strContentTitle.equals("")) {
			sbQuery.append(" AND mv9161_details.file_title = '" + strContentTitle + "' ");
		}
		sbQuery.append(
				" AND mv9161_master.email_sent_master_id = mv9161_details.email_sent_master_id ");
		sbQuery.append(" AND mv9161_master.email_dtls_id = mv9161_details.email_dtls_id ");
		sbQuery.append(" AND mv9161_master.event_type = 110694 ");
		sbQuery.append(" AND mv9161_master.email_sent_date >= to_date('" + strFrmDate + "','"
					+ strDateFormat + "')");
		sbQuery.append(" AND mv9161_master.email_sent_date <= to_date('" + strToDate + "','" + strDateFormat
					+ "')");
		sbQuery.append(
				" GROUP BY mv9161_master.event_type_name, mv9161_master.email_sent_date , mv9161_details.email_id, mv9161_details.recipient_name, ");
		sbQuery.append(
				" mv9161_details.file_title, mv9161_details.system_name, mv9161_details.open_cnt, ");
		sbQuery.append(
				" mv9161_details.clicked_cnt, mv9161_master.email_dtls_id, mv9161_master.event_type, mv9161_details.url ");
		sbQuery.append(" ORDER BY mv9161_master.email_sent_date DESC ) ");
		log.debug("fetchCollateralEmailTrackDetail Query==>  " + sbQuery.toString());
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		gmDBManager.close();

		return queryJSONRecord;
	}

	/**
	 * fetchEventTimeStamp - used to show Time Stamp in popup when Opens/Clicks Events count clicked
	 * 
	 * @return
	 * @throws AppError
	 */
	public String fetchEventTimeStamp(HashMap hmParam) {
		StringBuffer sbQuery = new StringBuffer();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = new GmCollateralEmailTrackVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String queryJSONRecord = "";
		String strEventType = GmCommonClass.parseNull((String) hmParam.get("EVENTYPE"));
		String strEmailDetailsId = GmCommonClass.parseNull((String) hmParam.get("EMAILDETAILSID"));
		String strDateFormat = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
		String strTimeDateFormat = strDateFormat + " HH12:mi:ss AM";

		sbQuery.append(
				" SELECT JSON_ARRAYAGG(JSON_OBJECT('TIMESTAMP' VALUE TO_CHAR(t9164.c9164_time_stamp, '"
						+ strTimeDateFormat + "'))) ");
		sbQuery.append(
				" FROM t9161_email_sent_master t9161 , t9163_email_sent_dtls t9163, t9164_email_event_tracker t9164 ");
		sbQuery.append(" WHERE t9161.c9161_email_sent_master_id = t9163.c9161_email_sent_master_id ");
		sbQuery.append(" AND t9163.c9163_email_sent_dtls_id = t9164.c9163_email_sent_dtls_id ");
		sbQuery.append(" AND  t9164.c901_event_type = '" + strEventType + "' ");
		sbQuery.append(" AND t9164.c9163_email_sent_dtls_id = '" + strEmailDetailsId + "' ");
		sbQuery.append(" AND t9161.c9161_void_fl IS NULL ");
		sbQuery.append(" AND t9163.c9163_void_fl IS NULL ");
		sbQuery.append(" AND t9164.c9164_void_fl IS NULL ");
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		log.debug("fetchEventTimeStamp Query==>  " + sbQuery.toString());
		gmDBManager.close();

		return queryJSONRecord;
	}

	/**
	 * fetchCollateralFrequentContact - used to fetch frequent contacts By Sales Rep, Distributor, AD and VP
	 * @return
	 * @throws AppError
	 */
	public String fetchCollateralFrequentContacts(HashMap hmParam) throws AppError {
		initializeParameters(hmParam);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCS_LVL"));
		String strDateFormat = getGmDataStoreVO().getCmpdfmt();
		String queryJSONRecord = "";

		sbQuery.append(
				"SELECT JSON_ARRAYAGG(JSON_OBJECT('recipients' VALUE recipientname, 'emailid' VALUE emailid, 'clickedcount' VALUE clickcnt, 'openedcnt' VALUE opencnt) RETURNING CLOB) FROM (");
		
		sbQuery.append(
				" SELECTCOUNT(1) cnt, email_id emailid,recipient_name recipientname,SUM(clicked_cnt) clickcnt,SUM(open_cnt) opencnt FROM mv9161_email_tracking_master mv9161_master ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') ");
		sbQuery.append(" AND mv9161_master.event_type = 110694 "); 			// 110694 - delivered
		sbQuery.append(" AND mv9161_master.email_sent_date >= to_date('" + strFrmDate + "','"
				+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','"
				+ strDateFormat + "') ");
		sbQuery.append(" GROUP BY email_id,recipient_name");
		sbQuery.append(" ORDER BY cnt DESC OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY )");
		log.debug("fetchCollateralFrequentContacts  Query  " + sbQuery.toString());
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		return queryJSONRecord;
	}

	/**
	 * fetchCollateralFrequentDoc - used to show Frequently used Collateral in
	 * Summary Report by Sales Rep, Distributor, AD and VP
	 * 
	 * @return
	 * @throws AppError
	 */
	public String fetchCollateralFrequentDoc(HashMap hmParam) throws AppError {
		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = new GmCollateralEmailTrackVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String queryJSONRecord = "";
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCS_LVL"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strDateFormat = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());

		sbQuery.append(
				" SELECT JSON_ARRAYAGG(JSON_OBJECT ('content_title' VALUE filetitle, 'total_sends' VALUE sent, 'total_clicks' VALUE clicked) RETURNING CLOB) ");
		sbQuery.append(
				" FROM ( SELECT mv9161_details.file_title filetitle, COUNT(t1018.c1016_share_id) sent, 0 clicked ");
		sbQuery.append(
				" FROM mv9161_email_tracking_master mv9161_master, mv9161_email_tracking_details mv9161_details, t1018_share_file t1018 ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {   //For Sales Rep
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {   //For Distributor
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {    //For AD
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {   //For VP
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {  
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 	
		sbQuery.append(" AND mv9161_master.email_sent_master_id = mv9161_details.email_sent_master_id ");
		sbQuery.append(" AND mv9161_master.email_dtls_id = mv9161_details.email_dtls_id ");
		sbQuery.append(" AND t1018.c1016_share_id =  mv9161_details.txn_id ");
		sbQuery.append(" AND t1018.c1018_void_fl IS NULL ");
		sbQuery.append(" AND mv9161_master.email_sent_date >= to_date('" + strFrmDate + "','"
				+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','" + strDateFormat
					+ "') ");
		sbQuery.append(" GROUP BY mv9161_details.file_title UNION ALL ");
		sbQuery.append(
				" SELECT mv9161_details.file_title contenttile, 0 sent, count (mv9161_master.event_type) clicked ");
		sbQuery.append(
				" FROM mv9161_email_tracking_master mv9161_master, mv9161_email_tracking_details mv9161_details, t1018_share_file t1018 ");
		sbQuery.append( getAccessFilterClause());
		sbQuery.append(" WHERE ( ");
		if(strAccessLevel.equals("1")) {
			sbQuery.append(" mv9161_master.email_sent_by = '"+ strUserId+ "' "); 	
		}
		else if (strAccessLevel.equals("2")) {//For Sales Rep
			sbQuery.append("  mv9161_master.dist_id = v700.d_id ");
		} 
		else if (strAccessLevel.equals("3")) {
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		else if (strAccessLevel.equals("4")) {
			sbQuery.append("  mv9161_master.vp_id = v700.vp_id ");
		}
		else {
			sbQuery.append("  mv9161_master.ad_id = v700.ad_id ");
		}
		sbQuery.append(" OR mv9161_master.email_sent_by = '"+ strUserId+ "') "); 	
		sbQuery.append(" AND mv9161_master.email_sent_master_id = mv9161_details.email_sent_master_id ");
		sbQuery.append(" AND mv9161_master.email_dtls_id = mv9161_details.email_dtls_id ");
		sbQuery.append(" AND t1018.c1016_share_id =  mv9161_details.txn_id ");
		sbQuery.append(" AND mv9161_master.event_type = 110696 "); 			// 110696 - clicked
		sbQuery.append(" AND t1018.c1018_void_fl IS NULL ");
		sbQuery.append(" AND mv9161_master.email_sent_date >= to_date('" + strFrmDate + "','"
				+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','" + strDateFormat
				+ "') ");
		sbQuery.append(" GROUP BY mv9161_details.file_title ORDER BY sent desc ");
		sbQuery.append(" OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY) ");
		log.debug("fetchCollateralFrequentDoc Query==>  " + sbQuery.toString());
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		gmDBManager.close();

		return queryJSONRecord;
	}

	/**
	 * fetchCollateralGlobalFrequentDoc - used to show Most shared content in Summary Report for All Globus Reps
	 * @return
	 * @throws AppError
	 */
	public String fetchCollateralGlobalFrequentDoc(HashMap hmParam) throws AppError {
		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		GmCollateralEmailTrackVO gmCollateralEmailTrackVO = new GmCollateralEmailTrackVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String queryJSONRecord = "";
		String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		String strDateFormat = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());

		sbQuery.append(
				" SELECT JSON_ARRAYAGG (JSON_OBJECT ('content_title' VALUE filetitle, 'total_sends' VALUE sent,'ref_id' VALUE ref_id) RETURNING CLOB) FROM ( ");
		sbQuery.append(
				" SELECT mv9161_details.file_title filetitle, count (mv9161_master.email_sent_master_id) sent,mv9161_details.file_ref_id ref_id ");
		sbQuery.append(
				" FROM mv9161_email_tracking_master mv9161_master, mv9161_email_tracking_details mv9161_details, t101_party t101p, t101_user t101u ");
		sbQuery.append(
				" WHERE mv9161_master.email_sent_master_id = mv9161_details.email_sent_master_id ");
		sbQuery.append(
				" AND mv9161_master.email_sent_by = t101u.c101_user_id ");
		sbQuery.append(
				" AND t101p.c101_party_id = t101u.c101_party_id ");
		sbQuery.append(
				" AND t101p.c1900_company_id = '" + strCompanyId + "' "); 
		sbQuery.append(
				" AND mv9161_master.email_sent_by = '" + strUserId + "' ");
		sbQuery.append(
				" AND t101p.c101_void_fl IS NULL ");
		sbQuery.append(" AND mv9161_master.email_sent_date >= to_date('" + strFrmDate + "','"
				+ strDateFormat + "') ");
		sbQuery.append(" AND mv9161_master.email_sent_date <= TO_DATE('" + strToDate + "','" + strDateFormat
				+ "') ");
		sbQuery.append(
				" GROUP BY mv9161_details.file_title, mv9161_details.file_ref_id order by sent desc OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY) ");
		log.debug("fetchCollateralGlobalFrequentDoc Query==>  " + sbQuery.toString());
		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
		gmDBManager.close();

		return queryJSONRecord;
	}
}