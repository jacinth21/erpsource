/**
 *  FileName : GmSystemPublishTransBean.java 
 *  Author : N RAJA
 */
package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author pvigneshwaran
 *GmSystemPublishTransBean:This bean is used to save system release details
 *Note:For avoid PMD Report gave comments to log and common class
 */
public class GmSystemPublishTransBean extends GmBean {

	/**
	 * Log is used to track bugs
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**GmSystemPublishTransBean
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmSystemPublishTransBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**saveSystemReleaseDtls:This method used to save system release details
	 * @param hmParam
	 * @throws AppError
	 */
	public void saveSystemReleaseDtls(HashMap hmParam) throws AppError {	
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_system_publish_trans.gm_sav_system_release_dtls", 4);

		String strSystemId = "";
		String strSystemType = "";
		String strInput = "";
		String strUserId = "";

		strSystemId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		strInput = GmCommonClass.parseNull((String) hmParam.get("SYSTEMRELEASEDTLS"));
		strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		strSystemType = GmCommonClass.parseNull((String) hmParam.get("SETGROUPTYPE"));
		gmDBManager.setString(1, strSystemId);
		gmDBManager.setString(2, strInput);
		gmDBManager.setString(3, strUserId);
		gmDBManager.setInt(4, Integer.parseInt(strSystemType));
		gmDBManager.execute();
		gmDBManager.commit();
//		PC-5611 Refresh the Set List Redis key while release to region
		GmCacheManager gmCacheManager = new GmCacheManager();  //declare cache manager here
		ArrayList regionIdArrays = getCompanyIdByRegionId(strInput);		
		for(int i = 0; i < regionIdArrays.size(); i++) {
			HashMap hmTemp = new HashMap();
			hmTemp = (HashMap)regionIdArrays.get(i);
			String strKey = "SET_ID_LST:" +hmTemp.get("C1900_COMPANY_ID") + ":AUTO"; //SET_ID_LST:1000:AUTO
			//form the redis key dynamically on the basis of company id, sample key: SET_ID_LST:1000:AUTO
			log.debug("Redis key refresh = "+strKey);
			gmCacheManager.removeKey(strKey); //remove the cache manager key
		}
	   //JMS Call once save done
		hmParam.put("SETID", strSystemId);// pass systemID
		hmParam.put("SETGROUPTYPE", strSystemType);// pass type whether it is system or set
		hmParam.put("USERID", strUserId);// User Id
		hmParam.put("ACTION", "RELEASE");// SYSTEM/SET Release
		processSystemRelease(hmParam);	
	}

	/**processSystemRelease;JMS call Using GmSystemReleaseConsumerJob
	 * @param hmParam
	 * @throws AppError
	 */
	public void processSystemRelease(HashMap hmParam) throws AppError {
			GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
			String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("IPAD_SYNC_QUEUE"));
			hmParam.put("QUEUE_NAME", strQueueName);
			// SYS_RELEASE_CONSUMER_CLASS is mentioned in JMSConsumerConfig.
			// properties file.This key contain the
			// path for :
			// com.globus.jms.consumers.processors.GmSystemReleaseConsumerJob
			String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("SYS_RELEASE_CONSUMER_CLASS"));
			hmParam.put("CONSUMERCLASS", strConsumerClass);
			log.debug("strQueueName=>" + strQueueName + " strConsumerClass=>"+ strConsumerClass + " hmParam=>>>" + hmParam);
			gmConsumerUtil.sendMessage(hmParam);
	}

	/**This method used to process system release dtls using JMS
	 * @param strSysId
	 * @param strType
	 * @param strUserId
	 * @throws AppError
	 */
	public void processSystemReleaseDtls(String strSysId, String strType,
			String strUserId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_system_publish_trans.gm_process_system_release", 3);
		gmDBManager.setString(1, strSysId);
		gmDBManager.setString(2, strType);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

	}
	

	/**This method used to process system publish dtls using JMS
	 * @param strSysId
	 * @param strType
	 * @param strUserId
	 * @throws AppError
	 */
	public void processSystemPublishDtls(String strSysId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("GM_PKG_DEVICE_COMMON_SYNC.GM_SAV_SYSTEM_PUBLISH_NEW", 1);
		gmDBManager.setString(1, strSysId);
		gmDBManager.execute();
		gmDBManager.commit();

	}
	
    /**
     * savSystemPublishDtls - to save System Details publish 
     * @param hmParam
     * @return
     * @throws AppError
     */
    public String savSystemPublishDtls(HashMap hmParam) throws AppError {
    	String strSysId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    	String strSysPubType = GmCommonClass.parseNull((String) hmParam.get("STRSYSTYPE"));
    	String strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    	String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    	String strReturnMsg = "";
    	
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_system_publish_trans.gm_sav_system_publish_dtls", 5);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSysId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strSysPubType));
		gmDBManager.setString(3, GmCommonClass.parseNull(strUserId));
		gmDBManager.setString(5, GmCommonClass.parseNull(strComments));
		
		gmDBManager.execute();
		   
		strReturnMsg = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();
		
		
	   //JMS Call once save done
		hmParam.put("SETID", strSysId);// pass systemID
		hmParam.put("SETGROUPTYPE", strSysPubType);// pass type whether it is system or set
		hmParam.put("USERID", strUserId);// User Id
		hmParam.put("ACTION", "PUBLISH");// SYSTEM/SET Publish 
		processSystemRelease(hmParam);	
		
	    return strReturnMsg;
    }
    
//  PC-5611 Refresh the Set List Redis key while release to region
    /**getCompanyIdByUsingRegionId:This method used get company id by using region id 
	 * @param String strInput
	 * @throws AppError
	 */
	  public ArrayList getCompanyIdByRegionId(String strInput) throws AppError {
	  	ArrayList alResult = new ArrayList();
	  	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_system_publish_trans.gm_get_company_id_using_region_id", 2);
		gmDBManager.setString(1, strInput);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	    gmDBManager.close();
	  	return alResult;
	  }
}
