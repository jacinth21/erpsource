/**
 *  FileName : GmSystemPublishReportBean.java 
 *  Author   : T.S Ramachandiran
 */

package com.globus.prodmgmnt.beans;

import java.util.HashMap;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

/**
 * GmSystemPublishReportBean class for fetch system release details
 * @author RSelvaraj
 * 
 */
public class GmSystemPublishReportBean extends GmBean {

	/**
	 *  This used to get the instance of this class for enabling logging 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
    
	/**
	 * constructor - GmSystemPublishReportBean
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmSystemPublishReportBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {

		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
		
	/**
	 * fetchSystemRelease - fetch system release details using Ajax call
	 * @param strSetid
	 * @return
	 * @throws AppError
	 */
	public String fetchSystemRelease(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		String strGroupType = GmCommonClass.parseNull((String) hmParam.get("SETGROUPTYPE"));
		String strJsonString = "";
		gmDBManager.setPrepareString("gm_pkg_qa_system_publish_rpt.gm_fch_system_release_dtls", 3);

		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strSystemId);
		gmDBManager.setString(2, strGroupType);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		return strJsonString;

	}
	
	/**
	 * fetchSystemSetMappingDtls - used to fetch the set count details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchSystemSetMappingDtls(HashMap hmParam) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strJsonString = "";
		    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		    String strRegionId = GmCommonClass.parseNull((String) hmParam.get("REGIONID"));
		    String strGroupType = GmCommonClass.parseNull((String) hmParam.get("SETGROUPTYPE"));
		    
		    gmDBManager.setPrepareString("gm_pkg_qa_system_publish_rpt.gm_fch_system_set_mapping_dtls",4);
			gmDBManager.setString(1, strSystemId);
			gmDBManager.setInt(2, Integer.parseInt(strRegionId));
			gmDBManager.setInt(3, Integer.parseInt(strGroupType));
			gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
			gmDBManager.execute();
			strJsonString = GmCommonClass.parseNull(gmDBManager.getString(4));
			gmDBManager.close();
			return strJsonString;
	}
}
