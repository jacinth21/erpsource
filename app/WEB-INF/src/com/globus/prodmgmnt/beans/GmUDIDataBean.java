/**
 * 
 */
package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.prodmgmnt.GmUDIDataVO;

/**
 * @author Yoga to fetch UDI data and populate in VO
 *
 */
public class GmUDIDataBean {
	
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	/*
	 * @author Yoga
	 * Following method is used to fetch the UDI Data for the MRF given.
	 */
	public HashMap fetchUDIData(String strScanMrf) throws AppError
	{
		//ArrayList alReturn = new ArrayList();
		HashMap hmReturn = new HashMap();
	    
	    String strKey = "";
	    String strValue = "";
	    
	    GmDBManager gmDBManager = new GmDBManager();
	    
	    gmDBManager.setPrepareString("GM_PKG_OP_INV_SCAN.gm_fch_udi_data", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strScanMrf);		
		gmDBManager.execute();
		
		hmReturn = GmCommonClass.parseNullHashMap((HashMap)gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)));
        gmDBManager.close();
        return hmReturn;

	}
	
	/**
	 * @author Fetch the UDI data for the given mrf and populate in VO
	 *
	 */
	public GmUDIDataVO populateInputVO(String str_mrf)
	{
		GmUDIDataBean gmUDIDataBean = new GmUDIDataBean();
		GmUDIDataVO gmUDIDataVO =new GmUDIDataVO();
		HashMap hmUDIData = new HashMap();

		String strDiNum = "";
		String strPartNum = "";
  		String strControlNum = "";
  		String strExpDate = "";
  		String strMfgDate = "";
  		String strSerialNo = "";
  		String strMrf = str_mrf;//gmUDIDataVO.getMrf();
  		String strHrf = "";
  		
  		//Fetch UDI data 
  		hmUDIData =gmUDIDataBean.fetchUDIData(strMrf);
		
  		//If Data not fetched throw following
		if(hmUDIData.size() == 0){
			return gmUDIDataVO;
		}  		
			
		//Populate UDI VO
		strDiNum = GmCommonClass.parseNull((String)hmUDIData.get("DI_NUM"));
		strPartNum = GmCommonClass.parseNull((String)hmUDIData.get("PART_NUM"));
		strControlNum = GmCommonClass.parseNull((String)hmUDIData.get("CTRL_NUM"));
		strExpDate = GmCommonClass.parseNull((String)hmUDIData.get("EXP_DT"));
		strMfgDate = GmCommonClass.parseNull((String)hmUDIData.get("MFG_DT"));
		strSerialNo = GmCommonClass.parseNull((String)hmUDIData.get("SERIAL_NUM"));
		strHrf = GmCommonClass.parseNull((String)hmUDIData.get("UDI_HRF"));
		strMrf = GmCommonClass.parseNull((String)hmUDIData.get("UDI_MRF"));
		
		gmUDIDataVO.setDinum(strDiNum);
		gmUDIDataVO.setPartnum(strPartNum);
		gmUDIDataVO.setControlnum(strControlNum);
		gmUDIDataVO.setExpdate(strExpDate);
		gmUDIDataVO.setMfgdate(strMfgDate);
		gmUDIDataVO.setMrf(strMrf);
		gmUDIDataVO.setHrf(strHrf);
		gmUDIDataVO.setSerialno(strSerialNo);
		
		return gmUDIDataVO;
		
	}

}
