package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.prodmgmnt.GmShareEngineVO;
import com.globus.valueobject.prodmgmnt.GmShareListVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;
import com.globus.valueobject.prodmgmnt.GmSharedFileVO;

/**
 * @class GmShareEngineRptBean
 * @author Anilkumar
 *
 */

public class GmShareEngineRptBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * fetchSharedStatusDtls - This method will be used to fetch all shared information
	 * @param HashMap
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchSharedStatusDtls(HashMap hmParams)throws AppError{
		
		GmDBManager gmDBManager = new GmDBManager();
		ArrayList alShareDtls = new ArrayList();
		ArrayList alShareFileDtls = new ArrayList();
		ArrayList alShareEmailDtls = new ArrayList();
		HashMap hmReturn = new HashMap();
		
		gmDBManager.setPrepareString("gm_pkg_pdmc_share_eng_rpt.gm_fch_shared_status_dtls",12);
		gmDBManager.setString(1, (String)hmParams.get("FIRSTNM"));
		gmDBManager.setString(2, (String)hmParams.get("LASTNM"));
		gmDBManager.setString(3, (String)hmParams.get("PARTYTYPE"));
		gmDBManager.setString(4, (String)hmParams.get("EMAILFROMDATE"));
		gmDBManager.setString(5, (String)hmParams.get("EMAILTODATE"));
		gmDBManager.setString(6, (String)hmParams.get("EMAILID"));
		gmDBManager.setString(7, (String)hmParams.get("USERID"));
		gmDBManager.setString(8, (String)hmParams.get("SHAREID"));
		gmDBManager.setString(9, (String)hmParams.get("SHARESTATUS"));
		
		gmDBManager.registerOutParameter(10, OracleTypes.CURSOR); 
		gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(12, OracleTypes.CURSOR);
		gmDBManager.execute();	
				
		alShareDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(10));
		alShareEmailDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(11));
		alShareFileDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(12));
		
		hmReturn.put("SHAREDTLS", alShareDtls);
		hmReturn.put("SHAREEMAILDTLS", alShareEmailDtls);
		hmReturn.put("SHAREFILEDTLS", alShareFileDtls);

		gmDBManager.close();
		
		return hmReturn;
	}
	
	/**
	 * populateShareListVO - This method will be used to populate shareListVo from shareEngineVO
	 * @param HashMap
	 * @return GmShareListVO
	 * @exception AppError
	 */
	public GmShareListVO populateShareListVO(HashMap hmParam) throws AppError{	
			
			ArrayList alShareDtls = new ArrayList();
			ArrayList alShareFileDtls = new ArrayList();
			ArrayList alShareEmailDtls = new ArrayList();
			ArrayList alTempFile = new ArrayList();
			ArrayList alTempEmail = new ArrayList();
			HashMap hmResult = new HashMap();
			HashMap hmFileResult = new HashMap();
			HashMap hmEmailResult = new HashMap();
			
			List<GmShareEngineVO> listGmShareEngineVO = new ArrayList<GmShareEngineVO>();
			List<GmSharedEmailVO> listGmSharedEmailVO = new ArrayList<GmSharedEmailVO>(); 
			List<GmSharedFileVO> listGmSharedFileVO= new ArrayList<GmSharedFileVO>();
			
			GmShareListVO gmShareListVO = new GmShareListVO();
			GmShareEngineVO gmShareEngineVO = null;
			GmSharedFileVO gmSharedFileVO = new GmSharedFileVO();
			GmSharedEmailVO gmSharedEmailVO = new GmSharedEmailVO();
			GmWSUtil gmWSUtil = new GmWSUtil();
	
			String strShareEngineId="";
			String strShareFileId="";
			String strShareEmailId="";
			boolean blActivefl = false;
			
			alShareDtls = (ArrayList)hmParam.get("SHAREDTLS");
			alShareEmailDtls = (ArrayList)hmParam.get("SHAREEMAILDTLS");
			alShareFileDtls = (ArrayList)hmParam.get("SHAREFILEDTLS");
			
			for(int i=0; i<alShareDtls.size();i++){
				gmShareEngineVO = new GmShareEngineVO();
				hmResult =  GmCommonClass.parseNullHashMap((HashMap)alShareDtls.get(i));
				strShareEngineId = GmCommonClass.parseNull((String)hmResult.get("SHAREID"));
				alTempFile = new ArrayList();
				alTempEmail = new ArrayList();
				blActivefl = false;
				for(int j=0; j<alShareFileDtls.size();j++){
					hmFileResult = GmCommonClass.parseNullHashMap((HashMap)alShareFileDtls.get(j));	
						strShareFileId = GmCommonClass.parseNull((String)hmFileResult.get("SHAREID"));
						if(strShareEngineId.equals(strShareFileId)){
							alTempFile.add(hmFileResult);
							blActivefl = true;
						}
						if(blActivefl && !strShareEngineId.equals(strShareFileId)){
							break;
						}
				}
				blActivefl = false;
				for(int k=0; k<alShareEmailDtls.size();k++){
					hmEmailResult = GmCommonClass.parseNullHashMap((HashMap)alShareEmailDtls.get(k));
						strShareEmailId = GmCommonClass.parseNull((String)hmEmailResult.get("SHAREID"));
						if(strShareEngineId.equals(strShareEmailId)){
							alTempEmail.add(hmEmailResult);
							blActivefl = true;
						}
						if(blActivefl && !strShareEngineId.equals(strShareEmailId)){
							break;
						}
				}
	
				//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes
				listGmSharedFileVO = gmWSUtil.getVOListFromHashMapList(alTempFile, gmSharedFileVO,
																				gmSharedFileVO.getFileInfoProperties());
			
				//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes
				listGmSharedEmailVO = gmWSUtil.getVOListFromHashMapList(alTempEmail, gmSharedEmailVO, 
																			  gmSharedEmailVO.getEmailInfoProperties());
				//Converting list of Hashmap into list of VO and last arg for mapping the hashmap key into vo attributes
				gmShareEngineVO = (GmShareEngineVO)gmWSUtil.getVOFromHashMap(hmResult, gmShareEngineVO, 
														    	gmShareEngineVO.getShareEngineInfoProperties());
				
				gmShareEngineVO.setListGmSharedFileVO(listGmSharedFileVO);
				gmShareEngineVO.setListGmSharedEmailVO(listGmSharedEmailVO);
				listGmShareEngineVO.add(gmShareEngineVO);
			}
			gmShareListVO.setListGmShareEngineVO(listGmShareEngineVO);
				
			return gmShareListVO;
		}
	
	
	
	/**fetchShareIdsEmail - This  method and will be used to fetch all share IDs
	 * The proc call here is used as part of email job as well 
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchShareIdsEmail(HashMap hmParams)throws AppError{
	
		GmDBManager gmDBManager = new GmDBManager();
		ArrayList alShareIDs = new ArrayList();
	
		gmDBManager.setPrepareString("gm_pkg_pdmc_share_eng_rpt.gm_fch_share_ids_email",3);
		gmDBManager.setString(1, GmCommonClass.parseNull((String)hmParams.get("SHAREID")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String)hmParams.get("SHARESTATUS")));
	
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); 
		gmDBManager.execute();	
		alShareIDs = GmCommonClass.parseNullArrayList( gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3)));
		gmDBManager.close();
		log.debug("alShareIDs:"+alShareIDs);
		return alShareIDs;
	}
}
