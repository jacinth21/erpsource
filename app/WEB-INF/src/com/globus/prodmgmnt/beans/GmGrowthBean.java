/**
 * 
 */
package com.globus.prodmgmnt.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.sales.beans.GmTerritoryBean;

/**
 * @author vprasath
 *
 */
public class GmGrowthBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.  
    /**
      * loadGrowth - This method is used to retrieve load details for the Growth Setup screen
      * @return HashMap
      * @exception AppError
    **/
    public HashMap loadGrowth() throws AppError
    {
        HashMap hmReturn = new HashMap();
        ArrayList alTypes = new ArrayList();
        ArrayList alGrowthRefType = new ArrayList();
        ArrayList alGrowthRefClass = new ArrayList();
        ArrayList alProjectList = new ArrayList();
        ArrayList alSetList = new ArrayList();
        
        GmProjectBean gmProjectBean = new GmProjectBean();
        //GmCommonClass gmCommon = new GmCommonClass();
        try{
     
                alProjectList = gmProjectBean.reportProject("ID");
                hmReturn.put("REFPROJECTLIST",alProjectList);
                alSetList = gmProjectBean.loadSetMap("1601"); // 1601 set report group
                hmReturn.put("REFSETLIST",alSetList);
                alGrowthRefType = GmCommonClass.getCodeList("GRFTP");
                hmReturn.put("GROWTHREFTYPE",alGrowthRefType);
                alGrowthRefClass = GmCommonClass.getCodeList("GRFCL");
                hmReturn.put("GROWTHREFCLASS",alGrowthRefClass);
                alTypes = GmCommonClass.getCodeList("YEAR");
                hmReturn.put("YEARLIST",alTypes);
        }catch (Exception e)
        {
            GmLogError.log("Exception in GmCustomerBean:loadGrowth","Exception is:"+e);
        }
        return hmReturn;
    } //End of loadTerritory
    public ArrayList loadGrowthDetails(HashMap hmParam) throws AppError
    {        
        String strRefID = GmCommonClass.parseNull((String)hmParam.get("REFID"));
        String strYear = GmCommonClass.parseNull((String)hmParam.get("YEAR"));
      
        ArrayList alResult = new ArrayList();        
        DBConnectionWrapper dbCon = null;  
      
        try{                     
                dbCon = new DBConnectionWrapper();
                StringBuffer sbQuery = new StringBuffer();
                sbQuery.append(" SELECT  C4001_REF_ID ID, TO_CHAR(C4001_START_DATE,'MM/DD/YYYY') SDATE, ");
                sbQuery.append(" TO_CHAR(C4001_END_DATE,'MM/DD/YYYY') EDATE, C4001_VALUE VALUE");
                sbQuery.append(" FROM T4001_GROWTH ");
                sbQuery.append(" WHERE C4001_REF_ID =");
                sbQuery.append(strRefID);
                sbQuery.append(" AND C4001_START_DATE BETWEEN to_date('01/01/");
                sbQuery.append(strYear);
                sbQuery.append("','MM/DD/YYYY') AND TO_DATE('12/31/");
                sbQuery.append(strYear);
                sbQuery.append("','MM/DD/YYYY') ");
                sbQuery.append(" ORDER BY C4001_START_DATE ");
                alResult = dbCon.queryMultipleRecords(sbQuery.toString());
        }catch (Exception e)
        {
                GmLogError.log("Exception in GmCustomerBean:loadTerritoryDetails","Exception is:"+e);
        }
        return alResult;        
    }
    public String saveGrowth(HashMap hmParam, String strUserId) throws AppError
    {
        log.debug("Enter");
        String message = "";
        
        GmCommonBean gmCom = new GmCommonBean();
        DBConnectionWrapper dbCon = null;
        dbCon = new DBConnectionWrapper();

        CallableStatement csmt = null;
        Connection conn = null;
        
        String strGrowthID="";
        String strPrepareString = null;

        try
        {            
            log.debug("Parameters in Bean : "+hmParam);
            String strRefType = GmCommonClass.parseNull((String)hmParam.get("REFTYPE"));            
            String strRefClass = GmCommonClass.parseNull((String)hmParam.get("REFCLASS"));
            String strRefID = GmCommonClass.parseNull((String)hmParam.get("REFID"));
            String strYear = GmCommonClass.parseNull((String)hmParam.get("YEAR"));
            String strInputString = GmCommonClass.parseNull((String)hmParam.get("INPUTSTR"));  
            String strLog = GmCommonClass.parseNull((String)hmParam.get("LOG"));
            
            conn = dbCon.getConnection();
            conn.setAutoCommit(false);
            
            strPrepareString = dbCon.getStrPrepareString("GM_PD_SAVE_GROWTH",7);
            csmt = conn.prepareCall(strPrepareString);
            
            /*
             *register out parameter and set input parameters
             */
            csmt.registerOutParameter(7,java.sql.Types.CHAR);
            

            csmt.setString(1,strRefType);
            csmt.setString(2,strRefClass);
            csmt.setString(3,strRefID);
            csmt.setInt(4,Integer.parseInt(strYear));
            csmt.setString(5,strInputString);
            csmt.setString(6,strUserId);            

            csmt.execute();
            strGrowthID = csmt.getString(7);
            
            if (!strLog.equals(""))
            {
                gmCom.saveLog(conn,strGrowthID,strLog,strUserId,"1215");
            }
            conn.commit();
            
            log.debug("Exit");

        }catch(Exception e)
        {
            try
            {
                conn.rollback();
            }
            catch (Exception sqle)
            {
                
                sqle.printStackTrace();
                throw new AppError(sqle);                
            }
            
            log.error(""+e.getMessage().indexOf("20029"));
            
            GmLogError.log("Exception in GmCustomerBean:saveGrowth","Exception is:"+e);
            if(e.getMessage().indexOf("20029") > 0)
            {
                message = "Part number does not exist";
            }
            else if(e.getMessage().indexOf("20030") > 0)
            {
                message = "Data already exist";
            }
            else
            {     
              throw new AppError(e);
            } 
        }
        finally
        {
            try
            {
                if (csmt != null)
                {
                    csmt.close();
                }//Enf of if  (csmt != null)
                if(conn!=null)
                {
                    conn.close();       /* closing the Connections */
                }
            }
            catch(Exception e)
            {
                throw new AppError(e);
            }//End of catch
            finally
            {
                 csmt = null;
                 conn = null;
                 dbCon = null;
            }
        }     
        return message; 
    }
  
}
