/**
 *  FileName : GmEntityReportBean.java 
 *  Author   : T S Ramachandiran
 *  PMT      : 40410-region mapping info on popup window
 */
package com.globus.prodmgmnt.beans;


import java.util.HashMap;
import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmEntityReportBean extends GmBean {

	/**
	 * This used to get the instance of this class for enabling logging 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * GmEntityReportBean
	 * 
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmEntityReportBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * fetchEntityDtls method used to fetch region-company details
	 * 
	 * @param hmParam
	 * @throws AppError
	 */

	public String fetchEntityDtls(String strRegionId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strSystemRegionJsonString = "";
		gmDBManager.setPrepareString(
				"gm_pkg_cm_entity_rpt.gm_fch_entity_company_dtls", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.setInt(1, Integer.parseInt(strRegionId));
		gmDBManager.execute();
		strSystemRegionJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strSystemRegionJsonString;

	}

}
