package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSetMapAutoCompleteRptBean extends GmAutoCompleteReportBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */

  public GmSetMapAutoCompleteRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * loadSetListSetup - This method to fetch the sales list information based on user search
   * 
   * @return
   * @throws AppError
   */
  public String loadSetListSetup(HashMap hmParam, String searchPrefix) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String striMaxCount = GmCommonClass.parseNull((String) hmParam.get("IMAXCOUNT"));
    int iMaxCount = Integer.parseInt(striMaxCount);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
    HashMap hmdata = new HashMap();
    hmdata.put("TYPE", strType);
    String strReturn = gmCacheManager.returnJsonString(getLoadSetMap(hmdata));
    return strReturn;
  }

  // To get the SetMap drop down list based on Type
  public String getLoadSetMap(HashMap hmParam) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));;
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strGroupPartMapping = GmCommonClass.parseNull((String) hmParam.get("GROUPPART"));
    String strCompanyId = getCompId();

    HashMap hmRuleData = new HashMap();
    hmRuleData.put("RULEID", strDeptId);
    hmRuleData.put("RULEGROUP", "SETMAPACCESS");
    String strStatus = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
    strStatus = GmCommonClass.getStringWithQuotes(strStatus);
    gmDBManager.close();
    String strSearchSet = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strSystem = GmCommonClass.parseZero((String) hmParam.get("SYSTEM"));
    String strHierarchy = GmCommonClass.parseZero((String) hmParam.get("HIERARCHY"));
    String strCategory = GmCommonClass.parseZero((String) hmParam.get("CATEGORY"));
    String strSetType = GmCommonClass.parseZero((String) hmParam.get("SETTYPE"));
    String strSetStatus = GmCommonClass.parseZero((String) hmParam.get("SETSTATUS"));

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T207.C207_SET_ID ID, C207_SET_NM NAME");
     /*   		+ ", T207.C207_SET_ID ||'-'|| C207_SET_NM IDNAME, ");
    sbQuery.append(" GET_CODE_NAME(C207_CATEGORY) CATEG, GET_CODE_NAME(C207_TYPE) TYPE, ");
    sbQuery.append(" GET_CODE_NAME(C901_STATUS_ID) STAT, C207_REV_LVL REVLVL, ");
    sbQuery
        .append(" C207_SET_DESC  PARTNUMDESC, C207_TYPE TYPEID, C207_SET_SALES_SYSTEM_ID SYSTEMID, ");
    sbQuery.append(" GET_USER_NAME(C207_LAST_UPDATED_BY) UPDBY, C207_LAST_UPDATED_DATE UPDDT ");
    sbQuery
        .append(" , GET_SET_NAME(C207_SET_SALES_SYSTEM_ID) SYSTEMNAME, GET_CODE_NAME_ALT(C901_HIERARCHY) HIERARCHY ");
    sbQuery
        .append(" , GET_CODE_NAME (DECODE (T207.C207_TYPE,4074, DECODE (T207.C901_HIERARCHY, 20700, '103148', '103150'),  DECODE (T207.C901_HIERARCHY, 20700, '103147', '103149'))) SETTYPENM");
    sbQuery.append(" , DECODE (T207.C901_CONS_RPT_ID, 20100,'Yes', 'No') BASELINENM");
    sbQuery.append(" , GET_CODE_NAME (T207A.C901_SHARED_STATUS) SHAREDSTATNM");*/

    sbQuery
        .append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080, t207a_set_link t207a WHERE C901_SET_GRP_TYPE ='");
    sbQuery.append(strType);
    sbQuery.append("' ");
    sbQuery.append(" AND T207.C207_SET_ID = T2080.C207_SET_ID");
    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
    sbQuery.append(" AND C207_VOID_FL IS NULL ");
    sbQuery.append(" AND C207_OBSOLETE_FL IS NULL ");
    sbQuery.append("AND t207.c207_set_id  = t207a.c207_link_set_id (+)");
    if (!strStatus.equals("")) {
      sbQuery.append(" AND C901_STATUS_ID IN ('");
      sbQuery.append(strStatus);
      sbQuery.append("')");
    }

    if (!strSearchSet.equals("")) {
      sbQuery.append(" AND UPPER(C207_SET_NM) LIKE ('%");
      sbQuery.append(strSearchSet.toUpperCase());
      sbQuery.append("%')");
    }

    if (!strSystem.equals("0")) {
      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN ('");
      sbQuery.append(strSystem);
      sbQuery.append("')");
    }

    if (!strHierarchy.equals("0")) {
      sbQuery.append(" AND C901_HIERARCHY IN ('");
      sbQuery.append(strHierarchy);
      sbQuery.append("')");
    }

    if (!strCategory.equals("0")) {
      sbQuery.append(" AND C207_CATEGORY IN ('");
      sbQuery.append(strCategory);
      sbQuery.append("')");
    }

    if (!strSetType.equals("0")) {
      sbQuery.append(" AND C207_TYPE IN ('");
      sbQuery.append(strSetType);
      sbQuery.append("')");
    }

    if (!strSetStatus.equals("0")) {
      sbQuery.append(" AND C901_STATUS_ID IN ('");
      sbQuery.append(strSetStatus);
      sbQuery.append("')");
    }


    sbQuery.append(" AND T2080.C1900_COMPANY_ID = '");
    sbQuery.append(strCompanyId);
    sbQuery.append("'");
    sbQuery.append(" ORDER BY ");

    sbQuery.append(" UPPER(C207_SET_NM)");
    log.debug(" Query for loading set map " + sbQuery.toString());
    return sbQuery.toString();
  }

  /**
   * validateSetName - This method to validate the Set/BOM id
   * 
   * @return HashMap
   * @throws AppError
   */
  public HashMap validateSetName(String strId, String strType, String strName) throws AppError,
      Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_CM_SET.GM_CM_VALIDATE_SET_LIST", 4);
    gmDBManager.setString(1, strId);
    gmDBManager.setInt(2, Integer.parseInt(strType));
    gmDBManager.setString(3, strName);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * loadProjectNameList - This method to fetch the project list information based on user search
   * 
   * @return String
   * @throws AppError
   */
  public String loadProjectNameList(HashMap hmParam, String searchPrefix) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String striMaxCount = GmCommonClass.parseNull((String) hmParam.get("IMAXCOUNT"));
    int iMaxCount = Integer.parseInt(striMaxCount);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
    String strReturn = gmCacheManager.returnJsonString(loadProjectNameQueryStr(hmParam));
    return strReturn;
  }

  /**
   * loadProjectNameQueryStr - This method will fetch all the project list
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public String loadProjectNameQueryStr(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();

    String strColumn = GmCommonClass.parseNull((String) hmParam.get("COLUMN"));
    String strProjectStatus = GmCommonClass.parseNull((String) hmParam.get("STATUSID"));
    String strGroupId = GmCommonClass.parseNull((String) hmParam.get("GROUPID"));
    String strSegmentId = GmCommonClass.parseNull((String) hmParam.get("SEGMENTID"));
    String strFrom = GmCommonClass.parseNull((String) hmParam.get("FROM"));
    String strProjId = GmCommonClass.parseNull((String) hmParam.get("PROJID"));
    String strComparison = " >= ";
    String strCompanyID = getCompId();
    if (strFrom.equals("ProjectReport")) {
      strComparison = " = ";
    }

    StringBuffer sbQuery = new StringBuffer();
if(!strProjId.equals("0") && !strProjId.equals(""))
{
	sbQuery
	.append(" SELECT T202.C202_PROJECT_ID ID, T202.C202_PROJECT_ID ||' / ' ||C202_PROJECT_NM NAME, ");
}
else
{
	sbQuery
    .append(" SELECT T202.C202_PROJECT_ID ID,C202_PROJECT_NM NAME ,");
}

sbQuery.append(" GET_USER_NAME(C202_CREATED_BY) PMANAGER, C202_CREATED_DATE CDATE, ");
sbQuery
.append(" GET_CODE_NAME(C901_STATUS_ID) STATUS, GET_CODE_NAME(C901_GROUP_ID) GROUPNAME, ");
sbQuery
.append(" GET_CODE_NAME(C901_SEGMENT_ID) SEGMENT ,t202.c1910_division_id divisionid ,t1910.c1910_division_name divisionname ");
sbQuery
.append(" FROM T202_PROJECT T202, T2021_PROJECT_COMPANY_MAPPING T2021 ,t1910_division t1910 ");
sbQuery.append(" WHERE NVL(C202_RELEASE_FOR_SALE,'Y')  = DECODE('");
sbQuery.append(strColumn);
sbQuery.append("',");
sbQuery.append(" 'APRICE', 'Y', NVL(C202_RELEASE_FOR_SALE,'Y') ) ");
sbQuery.append(" AND T202.C202_PROJECT_ID = T2021.C202_PROJECT_ID");
sbQuery.append(" and t202.c1910_division_id = t1910.c1910_division_id ");
sbQuery.append(" AND T2021.C1900_COMPANY_ID = '");
sbQuery.append(strCompanyID);
sbQuery.append("'");
sbQuery.append(" AND T2021.C2021_VOID_FL IS NULL");
sbQuery.append(" AND C202_VOID_FL IS NULL ");
sbQuery.append(" and C1910_VOID_FL is null ");
    /*
     * 20300 - Initiated, 20301 - Approved, 20302 - Launched . If we want to show all approved
     * projects, we would also have to show the launched projects, hence we are using C901_STATUS_ID
     * >=
     */
    if (!strProjectStatus.equals("0") && !strProjectStatus.equals("")) {
      sbQuery.append(" AND C901_STATUS_ID ");
      sbQuery.append(strComparison);
      sbQuery.append("'");
      sbQuery.append(strProjectStatus);
      sbQuery.append("'");
    }

    if (!strGroupId.equals("0") && !strGroupId.equals("")) {
      sbQuery.append(" AND C901_GROUP_ID = '");
      sbQuery.append(strGroupId);
      sbQuery.append("'");
    }

    if (!strSegmentId.equals("0") && !strSegmentId.equals("")) {
      sbQuery.append(" AND C901_SEGMENT_ID = '");
      sbQuery.append(strSegmentId);
      sbQuery.append("'");
    }

    sbQuery.append(" ORDER BY UPPER(C202_PROJECT_NM) ");
    log.debug(" Query inside reportProject is >>> " + sbQuery.toString());


    return sbQuery.toString();
  }

  /**
   * loadInspectionSheetList - This method to fetch the inspection sheet list information based on
   * user search
   * 
   * @return
   * @throws AppError
   */
  public String loadInspectionSheetList(HashMap hmParam, String searchPrefix) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String striMaxCount = GmCommonClass.parseNull((String) hmParam.get("IMAXCOUNT"));
    int iMaxCount = Integer.parseInt(striMaxCount);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
    String strReturn = gmCacheManager.returnJsonString(loadInspectionSheetListQueryStr(hmParam));
    log.debug("strReturn>>>>>>>>>>" + strReturn);
    return strReturn;
  }

  /**
   * loadInspectionSheetListQueryStr - This method will return the query to get inspection sheet
   * list
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public String loadInspectionSheetListQueryStr(HashMap hmParam) throws AppError {

    ArrayList alReturn = null;
    String strRefType = GmCommonClass.parseNull((String) hmParam.get("ARTIFACTSTYPE"));
    String strListId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strRefGrpId = GmCommonClass.parseNull((String) hmParam.get("UPLOADTYPE"));
    String strSubTypeId = GmCommonClass.parseNull((String) hmParam.get("SUBTYPE"));
    String strDateFmt = getCompDateFmt();
    log.debug(hmParam);
    strRefType = strRefType.equals("0") ? "" : strRefType;
    strSubTypeId = strSubTypeId.equals("0") ? "" : strSubTypeId;
    strListId = strListId.equals("0") ? "" : strListId;
    strRefGrpId = strRefGrpId.equals("0") ? "" : strRefGrpId;

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append("SELECT t903.c903_upload_file_list id, t903.c903_file_name name, get_code_name ( c901_ref_type ) mattype, c901_ref_type reftype,");
    sbQuery.append("TO_CHAR(NVL(c903_last_updated_date, c903_created_date), '" + strDateFmt
        + "') cdate, TO_CHAR(NVL(c903_last_updated_date, c903_created_date), '" + strDateFmt
        + "' ||' HH:MI') cdatetime ,");
    sbQuery
        .append("NVL ( TRIM (get_user_name (t903.c903_last_updated_by)), TRIM (get_user_name (t903.c903_created_by)) ) uname , t903.c903_ref_id setid,");
    sbQuery.append("DECODE(c901_ref_type,91184,get_set_name(t903.c903_ref_id),'') setname ,");
    sbQuery
        .append("lower(SUBSTR( t903.c903_file_name, INSTR( t903.c903_file_name,'.', -1, 1)+1)) extention ,");
    sbQuery
        .append("DECODE(T903.C901_REF_GRP,'103092',DECODE(T903.C901_REF_TYPE,'103114',GET_PARTNUM_DESC(T903A.titlerefid),T903.C901_FILE_TITLE),T903.C901_FILE_TITLE) FTITLE ,");
    sbQuery
        .append("DECODE(T903.C901_REF_GRP,'103092',DECODE(T903.C901_REF_TYPE,'103114','103216')) titletype ,");
    sbQuery.append("T903A.titlerefid titlerefid,");
    sbQuery
        .append("t903.c903_file_seq_no fseqno ,t903.c901_ref_grp fgrp , get_code_name (t903.c901_ref_grp) fgrpnm,");
    sbQuery
        .append("get_code_name(t903.c901_type) subtypenm,t903.c901_type subtype,t903a.shareable shareable ,t2050.keywords keywords");
    sbQuery.append(" FROM t903_upload_file_list t903,(SELECT c903_upload_file_list fileid,");
    sbQuery.append("MAX (DECODE (c901_attribute_type, 103216, c903a_attribute_value)) titlerefid,");
    sbQuery.append("MAX (DECODE (c901_attribute_type, 103217, c903a_attribute_value)) shareable");
    sbQuery
        .append(" FROM t903a_file_attribute WHERE c903a_void_fl IS NULL GROUP BY c903_upload_file_list) t903a,");
    sbQuery
        .append("(SELECT RTRIM (XMLAGG (XMLELEMENT (N, t2050.c2050_keyword_ref_name || ',')) .EXTRACT ('//text()'), ', ') keywords, t2050.c2050_ref_id fileid");
    sbQuery.append(" FROM t2050_keyword_ref t2050 WHERE t2050.c901_ref_type = 4000410 ");
    sbQuery.append("AND t2050.c2050_void_fl  IS NULL GROUP BY t2050.c2050_ref_id) t2050 ");
    sbQuery.append("WHERE t903.c901_ref_type = NVL ('" + strRefType + "', t903.c901_ref_type)");
    sbQuery.append("AND t903.c903_ref_id = NVL ('" + strListId + "', t903.c903_ref_id)");
    sbQuery.append("AND NVL(t903.C901_TYPE, -999) = NVL ('" + strSubTypeId
        + "', NVL(t903.C901_TYPE, -999))");
    sbQuery.append("AND NVL(t903.c901_ref_grp, -999) = NVL ('" + strRefGrpId
        + "', NVL(t903.c901_ref_grp, -999))");
    sbQuery
        .append("AND C903_DELETE_FL IS NULL AND t903.c903_upload_file_list = t903a.fileid(+) AND t903.c903_upload_file_list = t2050.fileid(+)");
    sbQuery
        .append("ORDER BY c903_file_seq_no, NVL(c903_last_updated_date, c903_created_date) DESC");

    log.debug(" Query inside loadInspectionSheetListQueryStr is >>> " + sbQuery.toString());
    return sbQuery.toString();

  }
  
  /**
   * loadSetListSetup - This method to fetch the sales list information based on user search
   * 
   * @return
   * @throws AppError
   */
  public String loadSetIdListSetup(HashMap hmParam, String searchPrefix) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String striMaxCount = GmCommonClass.parseNull((String) hmParam.get("IMAXCOUNT"));
    String strScrType = GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE"));
    String strReturn = "";
    int iMaxCount = Integer.parseInt(striMaxCount);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(strScrType);
    HashMap hmdata = new HashMap();
    hmdata.put("TYPE", strType);
    strReturn = gmCacheManager.returnJsonString(getLoadSetIdMap(hmdata));
 
    return strReturn;
  }

  // To get the SetMap drop down list based on Type
  public String getLoadSetIdMap(HashMap hmParam) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));;
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strGroupPartMapping = GmCommonClass.parseNull((String) hmParam.get("GROUPPART"));
    String strCompanyId = getCompId();

    HashMap hmRuleData = new HashMap();
    hmRuleData.put("RULEID", strDeptId);
    hmRuleData.put("RULEGROUP", "SETMAPACCESS");
    String strStatus = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
    strStatus = GmCommonClass.getStringWithQuotes(strStatus);
    gmDBManager.close();
    String strSearchSet = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strSystem = GmCommonClass.parseZero((String) hmParam.get("SYSTEM"));
    String strHierarchy = GmCommonClass.parseZero((String) hmParam.get("HIERARCHY"));
    String strCategory = GmCommonClass.parseZero((String) hmParam.get("CATEGORY"));
    String strSetType = GmCommonClass.parseZero((String) hmParam.get("SETTYPE"));
    String strSetStatus = GmCommonClass.parseZero((String) hmParam.get("SETSTATUS"));
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T207.C207_SET_ID ID, T207.C207_SET_ID||' - ' ||C207_SET_NM NAME");
    sbQuery
        .append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080, t207a_set_link t207a, T2082_SET_RELEASE_MAPPING T2082 ");
    sbQuery.append(" WHERE C901_SET_GRP_TYPE ='");
    sbQuery.append(strType);
    sbQuery.append("' ");
    sbQuery.append(" AND T207.C207_SET_ID = T2080.C207_SET_ID ");
    sbQuery.append(" AND T2082.C207_SET_ID = T2080.C207_SET_ID ");
    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
    sbQuery.append(" AND T2082.C901_STATUS_ID = '105361'"); //(105361 = Released) -- it's fetching only release set
    sbQuery.append(" AND t2082.C901_ENTITY_ID IN (select C901_ENTITY_ID FROM T1902_ENTITY_COMPANY_MAP WHERE C1902_VOID_FL IS NULL AND C1900_COMPANY_ID ='"+strCompanyId+"') ");
    sbQuery.append(" AND T207.C901_STATUS_ID= '20367'"); //(20367 = Approved) -- it's fetching only active status
    sbQuery.append(" AND C207_VOID_FL IS NULL "); 
    sbQuery.append(" AND T2082.C2082_VOID_FL is null ");
    sbQuery.append(" AND C207_OBSOLETE_FL IS NULL ");
    sbQuery.append("AND t207.c207_set_id  = t207a.c207_link_set_id (+)");
    if (!strStatus.equals("")) {
      sbQuery.append(" AND C901_STATUS_ID IN ('");
      sbQuery.append(strStatus);
      sbQuery.append("')");
    }

    if (!strSearchSet.equals("")) {
      sbQuery.append(" AND UPPER(C207_SET_NM) LIKE ('%");
      sbQuery.append(strSearchSet.toUpperCase());
      sbQuery.append("%')");
    }

    if (!strSystem.equals("0")) {
      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN ('");
      sbQuery.append(strSystem);
      sbQuery.append("')");
    }

    if (!strHierarchy.equals("0")) {
      sbQuery.append(" AND C901_HIERARCHY IN ('");
      sbQuery.append(strHierarchy);
      sbQuery.append("')");
    }

    if (!strCategory.equals("0")) {
      sbQuery.append(" AND C207_CATEGORY IN ('");
      sbQuery.append(strCategory);
      sbQuery.append("')");
    }

    if (!strSetType.equals("0")) {
      sbQuery.append(" AND C207_TYPE IN ('");
      sbQuery.append(strSetType);
      sbQuery.append("')");
    }

    if (!strSetStatus.equals("0")) {
      sbQuery.append(" AND C901_STATUS_ID IN ('");
      sbQuery.append(strSetStatus);
      sbQuery.append("')");
    }


    sbQuery.append(" AND T2080.C1900_COMPANY_ID = '");
    sbQuery.append(strCompanyId);
    sbQuery.append("'");
    sbQuery.append(" ORDER BY ");
    sbQuery.append(" UPPER(ID), UPPER(C207_SET_NM)");
    log.debug("set list"+sbQuery.toString());
    return sbQuery.toString();
  }
  
  
  /**
   * loadLoanerSetList - This method is used to fetch the Loaner set list information based on user search
   * @return
   * @throws AppError
   */
  public String loadLoanerSetList(HashMap hmParam) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	int iMaxCount = (Integer) hmParam.get("MAX_CNT");
    HashMap hmData = new HashMap();
	
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);

    hmData.put("TYPE", "4127"); //product loaner
    String strReturn = gmCacheManager.returnJsonString(loadLoanerSetQueryStr(hmData));
    
    log.debug("strReturn>>>>>>>>>>" + strReturn);
    return strReturn;
  }
  
  
  /**
   * loadLoanerSetQueryStr - This method is used to fetch the Loaner set list information based on user search
   * @return
   * @throws AppError
   */
  public String loadLoanerSetQueryStr(HashMap hmParam)throws AppError {
	
	  String strLoanerType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	  String strPlantId = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());
	  String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
	  StringBuffer sbQuery = new StringBuffer();
	  
	  
	 sbQuery.append(" SELECT DISTINCT(t504.c207_set_id) ID, t504.c207_set_id ||' / '|| c207_set_nm NAME");
	 sbQuery.append(" FROM t504_consignment t504,  t504a_consignment_loaner t504a ,t207_set_master t207, t2080_set_company_mapping t2080 ");
	 sbQuery.append(" WHERE t504.c504_consignment_id = t504a.c504_consignment_id ");
	 sbQuery.append(" AND t207.c207_set_id = t504.c207_set_id ");
	 sbQuery.append(" AND T504.c504_void_fl IS NULL ");
	 sbQuery.append(" AND t504.c504_type = '"+strLoanerType+"'");  
	 sbQuery.append(" AND t504.c504_status_fl = '4'");
	 sbQuery.append(" AND t504a.c504A_status_fl != '60' ");
	 sbQuery.append(" AND t504a.c504a_void_fl IS NULL ");
	 sbQuery.append(" AND t207.c207_void_fl IS NULL ");
	 sbQuery.append(" AND t2080.c207_set_id = t207.c207_set_id ");
	 sbQuery.append(" AND t2080.c2080_void_fl IS NULL ");
	 //PC-3425: Sets not loading in Manage Loaner screen when company mapped to multiple plants
	 sbQuery.append(" AND t2080.c1900_company_id IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE c5041_primary_fl = (CASE WHEN '"+strPlantId+"' = '3004' AND '"+strCompanyId+"' = '1017' THEN 'N' ELSE 'Y' END) AND c5040_plant_id = '"+strPlantId+"' AND c5041_void_fl IS NULL) ");
	 sbQuery.append(" AND t504a.c5040_plant_id = '");
	 sbQuery.append(strPlantId);
	 sbQuery.append("'");
	 sbQuery.append(" AND t207.c207_type != 4078 ");  // --Demo Sets
	 sbQuery.append(" ORDER BY UPPER(ID), UPPER(NAME) ");
	 		  
	 log.debug(" Query for loading LoanerSetList " + sbQuery.toString());
	 	  
	  
	  return  sbQuery.toString();
  }
  
  
}
