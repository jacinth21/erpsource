package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmTagInfoVO;
import com.globus.valueobject.operations.GmTxnPartDtlVO;
import com.globus.valueobject.prodmgmnt.GmSetAttributeVO;
import com.globus.valueobject.prodmgmnt.GmSetMasterVO;
import com.globus.valueobject.prodmgmnt.GmSetPartVO;
import com.globus.valueobject.prodmgmnt.GmSystemAttributeVO;
import com.globus.valueobject.prodmgmnt.GmSystemVO;

public class GmSystemServiceBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  // op changess start
  public GmSystemServiceBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSystemServiceBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // op changess end
  /**
   * fetchSystems - To fetch details of systems to be sync'd with device.
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public List<GmSystemVO> fetchSystems(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    List<GmSystemVO> listGmSystemVO;
    GmSystemVO gmSystemVO = (GmSystemVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_system_info.gm_fch_systems_prodcat", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmSystemVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmSystemVO,
            gmSystemVO.getSystemProperties());
    gmDBManager.close();
    return listGmSystemVO;
  }
  
  
//PMT# - System availability in iPad based on System Release
 /**
  * fetchSystems - To fetch details of systems to be sync'd with device.
  * 
  * @param hmParams
  * @return ArrayList
  * @throws AppError
  */
  public String fetchJsonSystems(HashMap hmParams) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager();
	    String strJasonReturn="";
	    gmDBManager.setPrepareString("gm_pkg_device_system_master_sync_rpt.gm_fch_systems_prodcat", 6);
	    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	    gmDBManager.setString(4, (String) hmParams.get("UUID"));
	    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
	    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
	    gmDBManager.execute();
	    strJasonReturn =  GmCommonClass.parseNull(gmDBManager.getString(6));
	    gmDBManager.close();
	    return strJasonReturn;
	  }

  /**
   * fetchSetMaster - To fetch details of set List to be sync'd with device.
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public List<GmSetMasterVO> fetchSetMaster(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmSetMasterVO> listGmSetMasterVO;
    GmSetMasterVO gmSetMasterVO = (GmSetMasterVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_system_info.gm_fch_sets_prodcat", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();

    listGmSetMasterVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmSetMasterVO,
            gmSetMasterVO.getSetMasterProperties());
    gmDBManager.close();
    return listGmSetMasterVO;
  }
  
//PMT# - Set availability in iPad based on System Release 
  public String fetchJsonSetMaster(HashMap hmParams) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strJasonReturn="";
	    gmDBManager.setPrepareString("gm_pkg_device_set_master_sync_rpt.gm_fch_sets_prodcat", 6);
	    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	    gmDBManager.setString(4, (String) hmParams.get("UUID"));
	    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
	    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
	    gmDBManager.execute();
	    strJasonReturn =  GmCommonClass.parseNull(gmDBManager.getString(6));
	    gmDBManager.close();
	    return strJasonReturn;
	  }

  /**
   * fetchSetPartDetails - To fetch details of set parts to be sync'd with device.
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public List<GmSetPartVO> fetchSetPartDetails(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    ArrayList alResult = new ArrayList();
    List<GmSetPartVO> listSetPartVO;
    GmSetPartVO gmSetPartVO = (GmSetPartVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_system_info.gm_fch_setpartdtls_prodcat", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listSetPartVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmSetPartVO,
            gmSetPartVO.getSetPartProperties());
    gmDBManager.close();
    return listSetPartVO;
  }
  
//PMT# - Set availability in iPad based on System Release
  public String fetchJsonSetPartDetails(HashMap hmParams) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strJasonReturn="";
	    gmDBManager.setPrepareString("gm_pkg_device_set_master_sync_rpt.gm_fch_setpartdtls_prodcat", 6);
	    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	    gmDBManager.setString(4, (String) hmParams.get("UUID"));
	    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
	    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
	    gmDBManager.execute();
	    strJasonReturn =  GmCommonClass.parseNull(gmDBManager.getString(6));
	    gmDBManager.close();
	    return strJasonReturn;
	  }

  /**
   * fetchSystemAttributes - To fetch Attribute details of systems to be sync'd with device.
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public List<GmSystemAttributeVO> fetchSystemAttributes(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    ArrayList alResult = new ArrayList();
    List<GmSystemAttributeVO> listGmSystemAttributeVO;
    GmSystemAttributeVO gmSystemAttributeVO = (GmSystemAttributeVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_system_info.gm_fch_systemattrib_prodcat", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmSystemAttributeVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmSystemAttributeVO,
            gmSystemAttributeVO.getSystemAttributeProperties());
    gmDBManager.close();
    return listGmSystemAttributeVO;
  }

  
//PMT# - System availability in iPad based on System Release
  public String fetchSystemJsonAttributes(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strJasonReturn="";
    gmDBManager.setPrepareString("gm_pkg_device_system_master_sync_rpt.gm_fch_systems_attribute", 6);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
    gmDBManager.execute();
    strJasonReturn =  GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.close();
    return strJasonReturn;
  }
  
  
  
  /**
   * fetchSetAttributes - To fetch Attribute details of sets to be sync'd with device.
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public List<GmSetAttributeVO> fetchSetAttributes(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    ArrayList alResult = new ArrayList();
    List<GmSetAttributeVO> listGmSetAttributeVO;
    GmSetAttributeVO gmSetAttributeVO = (GmSetAttributeVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_system_info.gm_fch_setattr_tosync", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmSetAttributeVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmSetAttributeVO,
            gmSetAttributeVO.getSetAttributeProperties());

    gmDBManager.close();
    return listGmSetAttributeVO;
  }
  
//PMT# - System availability in iPad based on System Release
  public String fetchJsonSetAttributes(HashMap hmParams) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strJasonReturn="";
	    GmSetAttributeVO gmSetAttributeVO = (GmSetAttributeVO) hmParams.get("VO");
	    gmDBManager.setPrepareString("gm_pkg_device_set_master_sync_rpt.gm_fch_setattr_tosync", 6);
	    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	    gmDBManager.setString(4, (String) hmParams.get("UUID"));
	    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
	    gmDBManager.registerOutParameter(6,OracleTypes.CLOB);
	    gmDBManager.execute();
	   
	    strJasonReturn =  GmCommonClass.parseNull(gmDBManager.getString(6));
	    gmDBManager.close();
	    return strJasonReturn;
	  }

  /**
   * populateTagDetailsVO - To fetch Tag details of set which is requested. Based on the Tag Id
   * scanned or loaded in device the respective header details and Part details need to be shown in
   * device.
   * 
   * @param gmTagInfoVO
   */

  public void populateTagDetailsVO(GmTagInfoVO gmTagInfoVO) {

    GmTxnPartDtlVO gmTxnPartDtlVO = new GmTxnPartDtlVO();
    List<GmTxnPartDtlVO> listGmTagPartDetailsVO = new ArrayList<GmTxnPartDtlVO>();
    GmSystemServiceBean gmSystemServiceBean = new GmSystemServiceBean();
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean();
    GmWSUtil gmWSUtil = new GmWSUtil();

    String strTagId = "";
    String strConsignId = "";


    HashMap hmTagHeader = new HashMap();
    ArrayList alTagPartDetails = new ArrayList();

    strTagId = GmCommonClass.parseNull(gmTagInfoVO.getTagid());
    hmTagHeader = gmSystemServiceBean.fetchTagInfo(strTagId);

    gmTagInfoVO =
        (GmTagInfoVO) gmWSUtil.getVOFromHashMap(hmTagHeader, gmTagInfoVO,
            gmTagInfoVO.getTagHeaderProperties());

    hmTagHeader = gmWSUtil.getHashMapFromVO(gmTagInfoVO);
    strConsignId = GmCommonClass.parseNull((String) hmTagHeader.get("CONSIGNID"));
    if (!strConsignId.equals("")) {
      alTagPartDetails = gmSystemServiceBean.fetchTagPartDetails(hmTagHeader);
      listGmTagPartDetailsVO =
          gmWSUtil.getVOListFromHashMapList(alTagPartDetails, gmTxnPartDtlVO,
              gmTxnPartDtlVO.getTagPartDetailProperties());
      gmTagInfoVO.setGmTagPartListVO(listGmTagPartDetailsVO);
    }



  }

  /**
   * fetchTagHeaderInfo - To fetch Tag Header Details for the respective Tag Id. For the Respective
   * Tag id ,Header information such as set id and set name is returned.
   * 
   * @param strTagId
   * @return HashMap
   */

  public HashMap fetchTagInfo(String strTagId) {

    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmResult = null;
    gmDBManager.setPrepareString("gm_pkg_pd_system_info.gm_fch_tag_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTagId);
    gmDBManager.execute();
    hmResult =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmResult;
  }

  /**
   * fetchTagPartDetails - To fetch Part Details for Set which is requested.
   * 
   * @param hmTagInfoData
   * @return ArrayList
   * @throws AppError
   */

  public ArrayList fetchTagPartDetails(HashMap hmTagInfoData) {


    GmDBManager gmDBManager = new GmDBManager();
    String strconsign_id = "";
    String strtype = "";
    String strCountryID = "";
    ArrayList alResult = new ArrayList();

    strconsign_id = GmCommonClass.parseNull((String) hmTagInfoData.get("CONSIGNID"));
    strtype = GmCommonClass.parseNull((String) hmTagInfoData.get("SETTYPE"));
    strCountryID = GmCommonClass.parseNull((String) hmTagInfoData.get("COUNTRYID"));


    gmDBManager.setPrepareString("gm_pkg_pd_system_info.gm_fch_tag_part_dtl", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strconsign_id);
    gmDBManager.setString(2, strtype);
    gmDBManager.setString(3, strCountryID);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(4)));

    gmDBManager.close();
    return alResult;



  }
}
