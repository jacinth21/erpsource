
package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPartProjectMapBean extends GmBean {
	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmPartProjectMapBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
 /**
   * This method is used to save the project for the part in part number setup  screen.This
   * method calling gm_pkg_part_mapping.gm_sav_asstproject procedure to save the associated projects
   * to the part in project mapping screen.we are calling this method when trying to add available projects to
   * a part in the part project mapping Screen.
   * 
   * @param hmParam :passing the associate Project ,Part Number id , Login user id and Project Map type.
   * @throws AppError
   */
  public void saveAsstProject(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    
    String strAssProjects = GmCommonClass.parseNull((String) hmParam.get("HASSOCIATEDPROJECTS"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strProjectMapType = GmCommonClass.parseNull((String) hmParam.get("PROJECTMAPTYPE"));
    
    gmDBManager.setPrepareString("gm_pkg_part_mapping.gm_sav_asstproject", 4);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strProjectMapType); 
    gmDBManager.setString(3, strAssProjects);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();
  }
  
  /**
   * This method is used to fetch the projects other than default project for the part in part project mapping screen
   * This method is calling
   * gm_pkg_part_mapping.gm_part_fch_nonasstproject procedure to fetch the available projects for the
   * particular Part in Part-Project Mapping screen.
   * 
   * @param hmParam :passing part number.
   * @return alResult : returns the avaialble project for the part.
   * @throws AppError
   */
  public ArrayList fetchNonAsstProject(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alAvailableProject = new ArrayList();

    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    //Getting company Id from form to load the project based on company.
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));

    gmDBManager.setPrepareString("gm_pkg_part_mapping.gm_part_fch_nonasstproject", 3);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strCompanyId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.execute();
    alAvailableProject =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    
    gmDBManager.close();
    return alAvailableProject;
  }

  /**
   * This method is used to fetch the projects which are mapped to a Part
   * This method is calling
   * gm_pkg_part_mapping.gm_part_fch_partproject procedure to fetch the available projects for the
   * part.
   * 
   * @param hmParam :passing part number and project mapping type.
   * @return alResult : returns the associated project for the part.
   * @throws AppError
   */
  public ArrayList fetchAssociatePartProject(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alAssProject = new ArrayList();

    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strProjectType = GmCommonClass.parseNull((String) hmParam.get("PROJECTMAPTYPE"));

    gmDBManager.setPrepareString("gm_pkg_part_mapping.gm_part_fch_assocProject",3);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strProjectType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.execute();
    alAssProject =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alAssProject;
  }
  
}