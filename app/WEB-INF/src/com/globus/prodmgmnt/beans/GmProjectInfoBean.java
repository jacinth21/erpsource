/**
 * FileName : GmProjectInfoBean.java Description : Author : sthadeshwar Date : Feb 9, 2009 Copyright
 * : Globus Medical Inc
 */
package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sthadeshwar
 *
 */
public class GmProjectInfoBean extends GmBean {
  /**
   * @author rshah
   * 
   */
  Logger log = GmLogger.getInstance(this.getClass().getName());


  public GmProjectInfoBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmProjectInfoBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

  /**
   * fetchProjectList - This method will be used to load the project list
   * 
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList fetchProjectList() throws AppError {
    ArrayList alList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_projectlist", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    // log.debug("In setup bean......"+alList);
    return alList;
  }

  /**
   * fetchProjectDetails - This method will be used to retrieve specific project records
   * 
   * @param strPrjId
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchProjectDetails(String strPrjId) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_projectdetails", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPrjId));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();

    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchProjectMilestoneDetails - This method will be used to retrieve milestone records
   * 
   * @param strPrjId
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchProjectMilestoneDetails(String strPrjId) throws AppError {
    ArrayList alList = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_milestonedetails", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPrjId));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alList;
  }

  /**
   * This method will fetch the projects report
   * 
   * @return RowSetDynaClass - will be used by the report displaytag
   * @exception AppError
   */
  public HashMap fetchProjectsReport(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    ResultSet rsMilestonesHeader = null;
    ResultSet rsMilestonesDetails = null;

    HashMap hmMilestones = new HashMap();
    HashMap hmFinalValue = new HashMap();
    HashMap hmLinkInfo = new HashMap();

    ArrayList alProjectDetails = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_projectsreport", 5);

    String strProjectId = (String) hmParam.get("PROJECTID");
    String strMilestones = (String) hmParam.get("MILESTONES");

    gmDBManager.setString(1, GmCommonClass.parseNull(strProjectId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strMilestones));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);

    gmDBManager.execute();

    alProjectDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    log.debug("alProjectDetails.size() = " + alProjectDetails.size());
    // log.debug("alProjectDetails = " + alProjectDetails);
    rsMilestonesHeader = (ResultSet) gmDBManager.getObject(4);
    rsMilestonesDetails = (ResultSet) gmDBManager.getObject(5);

    gmCrossReport.setAcceptExtraColumn(true);
    gmCrossReport.setExtraColumnParams(5, ""); // This is done to get the date values instead of
                                               // number column as values
    gmCrossReport.setExtraColumnParams(6, "_FL");

    hmMilestones = gmCrossReport.GenerateCrossTabReport(rsMilestonesHeader, rsMilestonesDetails);

    gmDBManager.close();

    // Further steps for linking
    hmLinkInfo = getLinkInfo("ProjectsReport");
    hmFinalValue = gmCrossReport.linkCrossTab(hmMilestones, alProjectDetails, hmLinkInfo);

    // log.debug("hmFinalValue = " + hmFinalValue);
    return hmFinalValue;
  }

  /**
   * getLinkInfo - This Method is used to to get the link object information
   * 
   * @return HashMap
   * @exception AppError
   **/
  private HashMap getLinkInfo(String strType) {
    HashMap hmLinkDetails = new HashMap();
    HashMap hmapValue = new HashMap();
    ArrayList alLinkList = new ArrayList();

    // First parameter holds the link object name
    hmLinkDetails.put("LINKID", "ID");

    if (strType.equals("ProjectsReport")) {
      // Parameter to be added to link list

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PROJSTATUS");
      hmapValue.put("VALUE", "Status");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PROJMANAGER");
      hmapValue.put("VALUE", "Project Manager");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PRODMANAGER");
      hmapValue.put("VALUE", "Product Manager");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PROJSEG");
      hmapValue.put("VALUE", "Segment");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PROJGRP");
      hmapValue.put("VALUE", "Group");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "BEFORE");

    }
    hmLinkDetails.put("LINKVALUE", alLinkList);
    return hmLinkDetails;
  }

  /**
   * fetchProjectList - This method will fetch the projects report
   * 
   * @return RowSetDynaClass - will be used by the report displaytag
   * @exception AppError
   */
  public RowSetDynaClass fetchProjectsDashboard() throws AppError {
    RowSetDynaClass rsDynaResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_projectsdashboard", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return rsDynaResult;
  }

  /**
   * fetchProjectAttr - This method will be used to retrieve specific project attributes
   * 
   * @param strPrjId, strAttrType
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchProjectAttr(String strPrjId, String strAttrType) throws AppError {
    ArrayList alreturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_project_attr", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPrjId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strAttrType));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alreturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alreturn;
  }// End of fetchProjectAttr

  /**
   * loadProjectCompanyMap - This method will be used to load the project company mapping details
   * for passed project.
   * 
   * @param HashMap hmParam - Contains the Project ID to fetch the details.
   * @return ArrayList alreturn - Having the project company mapping details.
   * @exception AppError
   */
  public ArrayList loadProjectCompanyMap(HashMap hmParam) throws AppError {

    ArrayList alreturn = new ArrayList();
    String strPrjId = GmCommonClass.parseNull((String) hmParam.get("PRJID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_proj_comp_map", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPrjId));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alreturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alreturn;
  }

  /**
   * checkProject - This method will be used to check project Id.
   * 
   * @param String string - Contain the Project ID to fetch the details.
   * @return String strflag - Having the project Id Status.
   * @exception AppError
   */
  public String checkProject(String strProjectId) {
    String strFlag = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_check_project_id", 2);
    gmDBManager.setString(1, strProjectId);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strFlag = gmDBManager.getString(2);
    gmDBManager.close();
    return strFlag;
  }

  public ArrayList loadProjectParentCompanyList(String strProjectId) throws AppError {
    ArrayList alreturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_project_info.gm_fch_project_comp_list", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strProjectId));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alreturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alreturn;
  }
}
