/**
 * FileName    : GmStaffingInfoBean.java 
 * Description :
 * Author      : rshah
 * Date        : Feb 10, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author rshah
 *
 */
public class GmStaffingInfoBean extends GmBean {

	/**
	 * @author rshah
	 * 
	 */
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																		// Initialize
																		// the
																		// Logger
																		// Class.
	GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

	 public GmStaffingInfoBean() {
			super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmStaffingInfoBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	  }
		/**
		 * fetchStaffingCountDetails - This method will be used to load the staffing count detail
		 * @param String strPrjId
		 * @return ArrayList - will be used by drop down tag
		 * @exception AppError
		 */
		public ArrayList fetchStaffingCountDetails(String strPrjId) throws AppError 
		{
			ArrayList alList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_pd_staffing_info.gm_fch_staffingcount", 2);
			gmDBManager.setString(1, GmCommonClass.parseNull(strPrjId));
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
			gmDBManager.close();
			log.debug("In setup bean......"+alList);
			return alList;
		}
		
		/**
		 * Fetches the staffing details for team id from db
		 * 
		 * @param strPrjId, strPartyType
		 * @return RowSetDynaClass
		 * @throws com.globus.common.beans.AppError
		 * 
		 */
		public ArrayList fetchProjectStaffingDetails(String strPrjId, String strPartyType) throws AppError {
			ArrayList alList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

			gmDBManager.setPrepareString("gm_pkg_pd_staffing_info.gm_fch_staffingdetails", 3);
			gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
			log.debug("2@.........."+strPrjId+"....2@....."+strPartyType);
			gmDBManager.setString(1, strPrjId);
			gmDBManager.setInt(2, Integer.parseInt(strPartyType));
			gmDBManager.execute();
			alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
			gmDBManager.close();
			return alList;
		}
		
		/**
		 * Fetches the project details for the party
		 * @param strPartyId
		 * @return RowSetDynaClass
		 * @throws com.globus.common.beans.AppError
		 * 
		 */
		public RowSetDynaClass fetchProjectsByParty(String strPartyId) throws AppError {
			RowSetDynaClass rsDynaClass = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

			gmDBManager.setPrepareString("gm_pkg_pd_staffing_info.gm_fch_projectsbyparty", 2);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			
			gmDBManager.setString(1, strPartyId);
			gmDBManager.execute();
			rsDynaClass = gmDBManager.returnRowSetDyna(((ResultSet) gmDBManager.getObject(2)));
			gmDBManager.close();
			return rsDynaClass;
		}
		
		
		/**
		 * Fetches the staffing details from db when user clicks on
		 * 'Edit'
		 * 
		 * @param strPrjTeamId
		 * @return java.lang.String
		 * @throws com.globus.common.beans.AppError
		 */
		public HashMap fetchEditProjectStaffingDetails(String strPrjTeamId) throws AppError {
			HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

			gmDBManager.setPrepareString("gm_pkg_pd_staffing_info.gm_fch_edit_staffingdetails", 2);

			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.setString(1, strPrjTeamId);
			gmDBManager.execute();
			hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
			gmDBManager.close();
			log.debug("edit...1@....."+hmReturn);
			return hmReturn;
		}
		
		
		/**
		 * fetchProjectStaffingReport - This method will fetch the project staffing report
		 * @return RowSetDynaClass - will be used by the report displaytag
		 * @exception AppError
		 */
		public RowSetDynaClass fetchProjectStaffingReport(HashMap hmParam) throws AppError 
		{
			RowSetDynaClass rsDynaResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_pd_staffing_info.gm_fch_staffingreport", 7);
			
			String strProjectId = (String)hmParam.get("PRJID");
			String strRoleId = (String)hmParam.get("ROLE");
			String strPartyType = (String)hmParam.get("PARTYTYPE");
			String strStatusId = (String)hmParam.get("STATUS");
			String strLastName = GmCommonClass.parseNull((String)hmParam.get("LASTNAME"));
			String strFirstName = GmCommonClass.parseNull((String)hmParam.get("FIRSTNAME"));
			
			gmDBManager.setString(1, strProjectId);
			gmDBManager.setString(2, strRoleId);
			gmDBManager.setString(3, strPartyType);
			gmDBManager.setString(4, strStatusId);
			gmDBManager.setString(5, strLastName);
			gmDBManager.setString(6, strFirstName);
			gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);

			gmDBManager.execute();
			rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));
			gmDBManager.close();
			return rsDynaResult;
		}		
		
		
}
