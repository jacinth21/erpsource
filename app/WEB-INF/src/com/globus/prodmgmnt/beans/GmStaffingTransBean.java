/**
 * FileName    : GmStaffingTransBean.java 
 * Description :
 * Author      : rshah
 * Date        : Feb 10, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.prodmgmnt.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

/**
 * @author rshah
 *
 */
public class GmStaffingTransBean extends GmBean{
	GmCommonBean gmCommonBean = new GmCommonBean();
	Logger log = GmLogger.getInstance(this.getClass().getName());
	

	  public GmStaffingTransBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmStaffingTransBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	  }
	  
	/**
	 * saveStaffingCountDetails - This method saves the staffing count details 
	 * 
	 * @param hmParam -
	 *            parameters to be saved
	 * @exception AppError
	 * @return String
	 */
	public void saveStaffingCountDetails(HashMap hmParam) throws AppError {
		String strPrjId = GmCommonClass.parseNull((String) hmParam.get("PRJID"));
		String strInputStr = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		log.debug("in Bean........."+strPrjId +"........."+strInputStr+".........."+strUserId);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_staffing_trans.gm_sav_staffingcount", 3);
		gmDBManager.setString(1, strPrjId);
		gmDBManager.setString(2, strInputStr);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();		
	}
	
	/**
	 * Saves the staffing information 
	 * @param hmParam - the values to save
	 * @return String - project team id
	 */
	public String saveProjectStaffingDetails(HashMap hmParam) throws AppError {
		
		String strPrjTeamIdFromDb = "";
		GmDBManager gmDBManager = new GmDBManager();
		
		String strPrjTeamId = (String)hmParam.get("HSTAFFINGDETAILSID");
		String strMemId = GmCommonClass.parseZero((String)hmParam.get("PARTYID"));
		String strRole = GmCommonClass.parseZero((String)hmParam.get("ROLE"));
		String strStatus = GmCommonClass.parseZero((String)hmParam.get("STATUS"));
		String strPrjId = (String)hmParam.get("PRJID");
		String strUserId = (String)hmParam.get("USERID");
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		log.debug("in Bean........."+strPrjId +"........."+strMemId+".........."+strRole);
		
		gmDBManager.setPrepareString("gm_pkg_pd_staffing_trans.gm_sav_staffingdetails", 6);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		
		gmDBManager.setString(1, strPrjTeamId);
		gmDBManager.setInt(2, Integer.parseInt(strMemId));
		gmDBManager.setInt(3,  Integer.parseInt(strRole));
		gmDBManager.setInt(4,  Integer.parseInt(strStatus));
		gmDBManager.setString(5, strPrjId);
		gmDBManager.setInt(6,  Integer.parseInt(strUserId));
		gmDBManager.execute();
		
		strPrjTeamIdFromDb = gmDBManager.getString(1);
		if (!strLogReason.equals("")) {
			log.debug("log reason...."+strPrjTeamIdFromDb+".............."+strLogReason+"........."+strUserId);
			gmCommonBean.saveLog(gmDBManager, strPrjTeamIdFromDb, strLogReason, strUserId, "1244");
		}
        gmDBManager.commit();

        return strPrjTeamIdFromDb;
	}
}
