package com.globus.prodmgmnt.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmProdReportBean {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	/**
		  * reportYTDByDist - This Method is used to fetch the sales YTD by Part
		  * Will have additional Filter   
		  * @param String strFromMonth
		  * @param String strFromYear
		  * @param String strToMonth
		  * @param String strToYear   
		  *  
		  * @return HashMap
		  * @exception AppError
		**/
		  public HashMap reportYTDByPart(String strProjectID,String strType, String strFromMonth,String strFromYear,
		  		String strToMonth, String strToYear, String strCondition) throws AppError
		  {
		  	StringBuffer sbHeaderQuery = new StringBuffer();
		  	StringBuffer sbWhereCondtion = new StringBuffer();
		  	StringBuffer sbDetailQuery = new StringBuffer();
			HashMap hmapFinalValue = new HashMap();		;		// Final value report details
			HashMap hmapFromToDate = new HashMap();		// Final value report details
			GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
			
			sbWhereCondtion.append("");
			// Code to frame the Where Condition
			if (strFromMonth != null && strFromYear != null)
			{
				sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('"  );
				sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
				sbWhereCondtion.append("','MM/YYYY') ");
			}
			
			if (strToMonth != null && strToYear != null)
			{
				if (sbWhereCondtion != null)
				{
					sbWhereCondtion.append(" AND");
				}
				sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('"  );
				sbWhereCondtion.append(strToMonth + "/" + strToYear);
				sbWhereCondtion.append("','MM/YYYY')) ");
			}
			
			// If the From and To Dates values are not passed then system will perform the below operation
			if (sbWhereCondtion.toString().equals(""))
			{
				sbWhereCondtion.append(" T501.C501_ORDER_DATE >= "  );
				sbWhereCondtion.append("ADD_MONTHS(TRUNC(SYSDATE ,'MONTH') , -11)  ");
				sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= SYSDATE "   );
				
				// To Store the from and the to date 
				hmapFromToDate = gmCrossTabRep.getFromToDate();
			}
			
			sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE ");  
			sbHeaderQuery.append("FROM T501_ORDER T501 WHERE " );
			sbHeaderQuery.append(sbWhereCondtion.toString());
			sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY')");
		  	
			//***********************************************************
			// Query to Fetch the Distributor month report
			//***********************************************************
			sbDetailQuery.append("SELECT   T205.C205_PART_NUMBER_ID DISTRIBUTOR_ID "); 
			sbDetailQuery.append(" ,T205.C205_PART_NUMBER_ID  || ' - ' || T205.C205_PART_NUM_DESC NAME ");
			sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE "); 
			if (strType.equals("0") )
			{	
				//sbDetailQuery.append(" ,SUM(C502_ITEM_QTY) AMOUNT ");
			    // 2524 to skip the price adjustment count
				sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");

			}
			else
			{
				sbDetailQuery.append(" ,SUM(C502_ITEM_QTY * C502_ITEM_PRICE) AMOUNT ");
			}
			sbDetailQuery.append(" FROM     T501_ORDER T501 ");
			sbDetailQuery.append(" ,T502_ITEM_ORDER T502 ");
			sbDetailQuery.append(" ,T205_PART_NUMBER T205 ");
			sbDetailQuery.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
			sbDetailQuery.append(" AND	   T502.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID");
			sbDetailQuery.append(" AND T205.C202_PROJECT_ID = '");
			sbDetailQuery.append(strProjectID);
			sbDetailQuery.append("' AND	 T501.C501_VOID_FL IS NULL");
			sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
			sbDetailQuery.append(" AND	");
			sbDetailQuery.append(sbWhereCondtion.toString());
			
			//Filter Condition to fetch record based on access code
			if (!strCondition.toString().equals(""))
			{
				sbDetailQuery.append(" AND ");
				sbDetailQuery.append(strCondition);
			}
			sbDetailQuery.append(" GROUP BY T205.C205_PART_NUMBER_ID ");
			sbDetailQuery.append(" ,T205.C205_PART_NUM_DESC ");
			sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY')"); 
			sbDetailQuery.append(" ORDER BY T205.C205_PART_NUMBER_ID ,");
			sbDetailQuery.append(" TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY')");
		  	
			if (strProjectID != null)
			{
				hmapFinalValue = gmCrossTabRep.GenerateCrossTabReport(sbHeaderQuery.toString(),sbDetailQuery.toString());
			}
			hmapFinalValue.put("FromDate",hmapFromToDate);
			return hmapFinalValue;
			
		  }
		  		    
          /**
              * loadPendingTransfer - This method will 
              * @return ArrayList
              * @exception AppError
            **/
             
            public ArrayList reportTrackImplantSummary(String strSetID)throws AppError
            {
                DBConnectionWrapper dbCon =new DBConnectionWrapper();        
                CallableStatement csmt = null;
                Connection conn = null;
                String strPrepareQuery = null;
                ArrayList alResult = new ArrayList();
                ResultSet rs = null;

                try
                {            
                    conn = dbCon.getConnection();
                    strPrepareQuery = dbCon.getStrPrepareString("GM_PD_RPT_TRACKIMPLANT_SUMMARY",2);
                    csmt = conn.prepareCall(strPrepareQuery);            
                    csmt.registerOutParameter(2,OracleTypes.CURSOR); //register out parameter     
                    csmt.setString(1,strSetID);
                    csmt.execute();
                    rs = (ResultSet)csmt.getObject(2);
                    alResult = dbCon.returnArrayList(rs);         
                }catch(Exception e)
                {
                    e.printStackTrace();
                    GmLogError.log("Exception in GmTransferBean:loadPendingTransfer","Exception is:"+e);
                    throw new AppError(e);
                }
                finally
                {
                    try
                    {
                        if (csmt != null)
                            csmt.close();                  
                        if(conn!=null)                
                            conn.close();                       
                    }
                    catch(Exception e)
                    {
                        throw new AppError(e);
                    }
                    finally
                    {
                         csmt = null;
                         conn = null;
                         dbCon = null;
                    }
                }
                return alResult;   
            } // End of reportTrackImplantSummary      
            
            
            /**
             * fetchTTPReport - This method will report on the TTP
             * @return RowSetDynaClass
             * @exception AppError
             */
            public RowSetDynaClass fetchMultiSetPartReport ()throws AppError
            {
                RowSetDynaClass rdResult = null;
                GmDBManager gmDBManager = new GmDBManager ();
                log.debug(" inside fetch MultiSet Part report ");
                gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_pd_fch_multisetpart_report",1);
                gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
                gmDBManager.execute();
                
                rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(1));
                gmDBManager.close();
                
                return rdResult;
            }
            
            
            /**
             * fetchTTPReport - This method will report on the TTP
             * @return RowSetDynaClass
             * @exception AppError
             */
            public RowSetDynaClass fetchGroupsReport ()throws AppError
            {
                RowSetDynaClass rdResult = null;
                GmDBManager gmDBManager = new GmDBManager (); 
                gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_pd_fch_groups_report",1);
                gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
                gmDBManager.execute();
                
                rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(1));
                gmDBManager.close();
                
                return rdResult;
            }
            
            public HashMap fetchGroupPartStatusReport (HashMap hmParams)throws AppError
            {
                	HashMap hmReturn = new HashMap();
                	GmDBManager gmDBManager = new GmDBManager ();
               
                	gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_pd_fch_status_report", 5); 
                	gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
 
                	gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("HID"))); 
                	gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("HTYPE")));
                	gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("LISTP")));
                	gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("TRIPWIREP")));

                	gmDBManager.execute();

                	hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(5));
    	            
    	            gmDBManager.close(); 
                
                return hmReturn;
            }
            public ArrayList fetchPartStatusReport (HashMap hmParams)throws AppError
            {
                ArrayList alResult = new ArrayList();
                

                GmDBManager gmDBManager = new GmDBManager ();
         
               
                	gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_pd_fch_part_status_report", 3); 
                	gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
 
                	gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("HID"))); 
                	gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("HTYPE")));

                	gmDBManager.execute();
                	alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
                	log.debug("alResult----------->"+alResult);
    	            gmDBManager.close(); 
                
                return alResult;
            }
}