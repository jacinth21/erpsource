package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmSubComponentMappingBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j



  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSubComponentMappingBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * loadPartSubCompDtls : To fetch the Sub Component Mapping details
   * 
   * @return String
   * @throws AppError
   * */
  public ArrayList loadPartSubCompDtls(HashMap hmParam) throws AppError {
    ArrayList alSubCmpMpngDtls = new ArrayList();
    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strSubComponet = GmCommonClass.parseNull((String) hmParam.get("SUBCOMPONENTNUM"));
    String strSubComponetStr = GmCommonClass.parseNull((String) hmParam.get("SUBCOMPONENTID"));
    String strPartNumberStr = GmCommonClass.parseNull((String) hmParam.get("PARENTPARTNUMBERS"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_sub_cmp_mpng.gm_fch_part_mpng_dtls", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strSubComponet);
    gmDBManager.setString(3, strSubComponetStr);
    gmDBManager.setString(4, strPartNumberStr);
    gmDBManager.execute();
    alSubCmpMpngDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();
    return alSubCmpMpngDtls;
  }


  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParam) throws AppError {

    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE"));
    String strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATEPATH"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));

    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alResult"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setTemplateName(strTemplateName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.GmSubComponentMappingSetup", strSessCompanyLocale));
    return templateUtil.generateOutput();
  }

  /**
   * savePartSubCompDtls : To save the Sub Component Mapping details
   * 
   * @throws AppError
   * */
  public HashMap savePartSubCompDtls(HashMap hmParam) throws AppError {
    String strSubComponentIds = "";
    String strPartNumbersIds = "";
    HashMap hmResult = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("The hmParam values before going to call the Procedure **** " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_pd_sub_cmp_mpng.gm_sav_part_mpng_dtls", 5);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("STRINPUTSTRING")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("SCREENACCESSFL")));
    gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
    gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
    gmDBManager.execute();
    strSubComponentIds = GmCommonClass.parseNull(gmDBManager.getString(4));
    strPartNumbersIds = GmCommonClass.parseNull(gmDBManager.getString(5));
    hmResult.put("SUBCOMPONENTDATA", strSubComponentIds);
    hmResult.put("PARTNUMDETAILS", strPartNumbersIds);
    gmDBManager.commit();
    return hmResult;
  }
}
