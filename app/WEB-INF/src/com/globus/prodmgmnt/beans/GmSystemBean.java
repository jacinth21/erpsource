/**
 * FileName    : GmSystemBean.java 
 * Description :
 * Author      : Elango
 */
package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Elango
 *
 */
public class GmSystemBean extends GmBean {
	/**
	 * @author Elango
	 * 
	 */
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																		// Initialize
																		// the
																		// Logger
																		// Class.
		
		  public GmSystemBean() {
			super(GmCommonClass.getDefaultGmDataStoreVO());
		  }	
		  public GmSystemBean(GmDataStoreVO gmDataStore) {
		    super(gmDataStore);
		  }

		/**
		 * fetchSystemGroups - This method will be used to fetch Available Group Details for the System
		 * @param strSystemId
		 * @return ArrayList
		 * @exception AppError
		 */
		public ArrayList fetchSystemGroups(String strSystemId) throws AppError {
			ArrayList alreturn = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_pd_system_rpt.gm_fch_sys_groups", 2);
			gmDBManager.setString(1, strSystemId);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			alreturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
			gmDBManager.close();
			return alreturn;
		}// End of fetchSystemGroups
		
		/**
		 * fetchSystemSets - This method will be used to fetch Available Set Details for the System
		 * @param strSystemId
		 * @return ArrayList
		 * @exception AppError
		 */
		public ArrayList fetchSystemSets(String strSystemId) throws AppError {
			ArrayList alreturn = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_pd_system_rpt.gm_fch_sys_sets", 2);
			gmDBManager.setString(1, strSystemId);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			alreturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
			gmDBManager.close();
			return alreturn;
		}// End of fetchSystemSets
		
		/**
		 * saveSystemDetail - This method saves the System Setup Detail 
		 * @param HashMap hmParam 
		 * @returns String strSystemIdFromDb
		 * @exception AppError		 * 
		 */
		public String saveSystemDetail(HashMap hmParam) throws AppError {
			String strSystemIdFromDb = "";
			String strMsg = "";
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    
		    String strSetAttrInputStr = GmCommonClass.parseNull((String) hmParam.get("SETATTRINPUTSTR"));
		    String strCompanyId = getCompId();

		    gmDBManager.setPrepareString("gm_pkg_pd_system_trans.gm_sav_system_detail", 9);
		    gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);
		    gmDBManager.setString(1, ((String) hmParam.get("SETID")));
		    gmDBManager.setString(2, ((String) hmParam.get("SETNM")));
		    gmDBManager.setString(3, ((String) hmParam.get("SETDESC")));
		    gmDBManager.setString(4, ((String) hmParam.get("SETDETAILDESC")));
		    gmDBManager.setString(5, ((String) hmParam.get("TXT_LOGREASON")));
		    gmDBManager.setString(6, ((String) hmParam.get("USERID")));
		    gmDBManager.setString(7, ((String) hmParam.get("DIVISIONID")));
		    gmDBManager.setString(8, strCompanyId);
		    gmDBManager.execute();

		    strSystemIdFromDb = GmCommonClass.parseNull(gmDBManager.getString(9));

		    hmParam.put("SYSTEMIDFROMDB", strSystemIdFromDb);
		    // To save Attribute details, we should call this method.
		    saveSetAttribute(gmDBManager, hmParam);
		    hmParam.put("MAP_TYPE","105360");//105360- created
		    hmParam.put("COMP_ID", strCompanyId);
		    //This method used to save the System company mapping details
		    saveSystemCompanyMap(gmDBManager,hmParam);

		    gmDBManager.commit();
			
			return strSystemIdFromDb;
		}// End of saveSystemDetail
		
		/**
		 * saveSetAttribute - This method saves the Set attributes.
		 * @param GmDBManager gmDBManager, HashMap hmParam
		 * @exception AppError
		 */
		public void saveSetAttribute(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
	
			gmDBManager.setPrepareString("gm_pkg_pd_system_trans.gm_sav_set_attribute", 4);
			gmDBManager.setString(1, ((String)hmParam.get("SYSTEMIDFROMDB")));
			gmDBManager.setString(2, ((String)hmParam.get("SETATTRINPUTSTR")));
			gmDBManager.setString(3, ((String)hmParam.get("ATTRTYPEINPUTSTR")));
			gmDBManager.setString(4, ((String)hmParam.get("USERID")));
			gmDBManager.execute();		
		}	// End of saveSetAttribute

		/**
		 * fetchSetAttribute - This method will be used to retrieve specific system attributes
		 * @param strSetId, strAttrType
		 * @return ArrayList
		 * @exception AppError
		 */
		public ArrayList fetchSetAttribute(String strSetId,String strAttrType) throws AppError {
			ArrayList alreturn = new ArrayList();

			GmDBManager gmDBManager = new GmDBManager();
			gmDBManager.setPrepareString("gm_pkg_pd_system_rpt.gm_fch_set_attribute", 3);
			gmDBManager.setString(1, strSetId);
			gmDBManager.setString(2, strAttrType);
			gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
			gmDBManager.execute();
			alreturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3)));
			gmDBManager.close();
			return alreturn;
		}// End of fetchSetAttribute
		
		/**
		 * fetchSystemMapping - This method will retrieve specific system Mapping details
		 * @param String strSystemId
		 * @return ArrayList
		 * @exception AppError
		 */
		public ArrayList fetchSystemMapping(String strSystemId) throws AppError {
			ArrayList alreturn = new ArrayList();

			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_pd_system_rpt.gm_fch_sys_mapping", 2);
			gmDBManager.setString(1, strSystemId);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			alreturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
			gmDBManager.close();
			return alreturn;
		}// End of fetchSystemMapping
		/**
	     * getSetStatus - This method will be used to fetch the status of  strSetId, strSetGroup 
	     * @param strSetId, strSetGroup
	     * @return String  
	     * @exception AppError
	     */
	    public String getSetStatus(String strSetID,String strSetGroup) throws AppError
	    {
	        String strSetStatus = "";
	        
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	        gmDBManager.setFunctionString("gm_pkg_pd_system_rpt.get_set_status",2);
		    gmDBManager.registerOutParameter(1,OracleTypes.CHAR);
		    gmDBManager.setString(2, strSetID);
		    gmDBManager.setString(3, strSetGroup);
		    gmDBManager.execute();
		    strSetStatus =  GmCommonClass.parseNull((String)gmDBManager.getString(1));
		    gmDBManager.close();
	                    
	        return strSetStatus;
	    }	// End of getSetStatus
		/**
	     * validateSystem - This method validate SYSTEM Name
	     * @param String strSetNm
	     * @return String  
	     * @exception AppError
	     */
	    public String validateSystem(String strSetNm) throws AppError
	    {
	        String strSystemAvail = "";
	        
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	        gmDBManager.setFunctionString("gm_pkg_pd_system_rpt.gm_chk_system",1);
		    gmDBManager.registerOutParameter(1,OracleTypes.VARCHAR);
		    gmDBManager.setString(2, strSetNm);
		    gmDBManager.execute();
		    strSystemAvail =  GmCommonClass.parseNull((String)gmDBManager.getString(1));
		    gmDBManager.close();
	                    
	        return strSystemAvail;
	    }// End of validateSystem
	   
		/**
	     * saveSystemMapping - This method will save the System Mapping 
	     * @param HashMap hmParam
	     * @exception AppError
	     */
		public void saveSystemMapping(HashMap hmParam) throws AppError {

			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
	        String strUserid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
			String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
			String strCompanyId = getCompId();

			gmDBManager.setPrepareString("gm_pkg_pd_system_trans.gm_sav_system_map", 9);
			gmDBManager.setString(1, strSystemId);
			gmDBManager.setString(2, ((String) hmParam.get("STDSETIDSINPUTSTR")));
			gmDBManager.setString(3, ((String) hmParam.get("ADDLSETIDSINPUTSTR")));
			gmDBManager.setString(4, ((String) hmParam.get("LOANERSTDSETIDSINPUTSTR")));
			gmDBManager.setString(5, ((String) hmParam.get("LOANERADDLSETIDSINPUTSTR")));
			gmDBManager.setString(6, strUserid);
			gmDBManager.setString(7, ((String) hmParam.get("HACTION")));
			gmDBManager.setString(8, ((String) hmParam.get("DIVISION")));
			gmDBManager.setString(9, strCompanyId);
			gmDBManager.execute();			
			
			if (!strLogReason.equals("")) {
				GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
   				gmCommonBean.saveLog(gmDBManager, strSystemId, strLogReason, strUserid, "4000419");// 4000419 - Log Code Number for SystemMapping.
   			}
			gmDBManager.commit();			
		}// End of saveSystemMapping
	  /**
	   * saveSystemCompanyMap - This method saves the system company mapping details.
	   * 
	   * @param GmDBManager gmDBManager, HashMap hmParam
	   * @throws AppError
	   */
	  public void saveSystemCompanyMap(GmDBManager gmDBManager, HashMap hmParam) throws AppError {

		gmDBManager.setPrepareString("gm_pkg_pd_system_trans.gm_sav_system_company_map", 4);
		gmDBManager.setString(1, ((String) hmParam.get("SYSTEMIDFROMDB")));
		gmDBManager.setString(2, ((String) hmParam.get("COMP_ID")));
		gmDBManager.setString(3, ((String) hmParam.get("MAP_TYPE")));
		gmDBManager.setString(4, ((String) hmParam.get("USERID")));
		gmDBManager.execute();
	  } 
	  
	  /**
	   * fetchSystemCompanyMap - This method will be used to load the system company mapping details
	   * 
	   * @param HashMap
	   * @return ArrayList
	   * @throws AppError
	   */
	  public ArrayList fetchSystemCompanyMap(HashMap hmParam) throws AppError {
		  
		ArrayList alreturn = new ArrayList();
		String strSysId = GmCommonClass.parseNull((String) hmParam.get("SYSID"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		gmDBManager.setPrepareString("gm_pkg_pd_system_rpt.gm_fch_system_comp_map", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSysId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		
		alreturn =GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		
		gmDBManager.close();
		
		return alreturn;
	  }
}
