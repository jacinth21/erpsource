package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.prodmgmnt.GmFileVO;

public class GmFileUploadBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchUploadedFileInfo - To fetch details of uploaded files to be sync'd with device.
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public List<GmFileVO> fetchUploadedFileInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    List<GmFileVO> listGmFileVO;
    GmFileVO gmFileVO = (GmFileVO) hmParams.get("VO");

    gmDBManager.setPrepareString("gm_pkg_pd_file_info.gm_fch_uploaded_file_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmFileVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmFileVO,
            gmFileVO.getFileInfoProperties());
    gmDBManager.close();
    return listGmFileVO;
  }

  public void sendFileUploadExceptionEmail(HashMap hmParams) throws AppError, Exception {
    String strFileNm = "";
    String strTransException = "";
    String strMessage = "";
    String strMessageHeader = "";
    String strMessageBody = "";
    String strMessageFooter = "";
    String strusername = "";
    String TEMPLATE_NAME = "GmFileUploadExcp";

    GmEmailProperties emailProps = new GmEmailProperties();

    strFileNm = GmCommonClass.parseNull((String) hmParams.get("FILENAME"));
    strusername = GmCommonClass.parseNull((String) hmParams.get("USERNAME"));
    strTransException = GmCommonClass.parseNull((String) hmParams.get("EXCEPTION"));
    emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.FROM));
    emailProps.setRecipients(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.TO));
    emailProps.setSubject(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.MIME_TYPE));
    strMessage = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE);
    strMessageHeader =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_HEADER);
    strMessageHeader = GmCommonClass.replaceAll(strMessageHeader, "#<REPNAME>", strusername);
    strMessageBody =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_BODY);
    strMessageBody = GmCommonClass.replaceAll(strMessageBody, "#<FILENAME>", strFileNm);
    strMessageBody = GmCommonClass.replaceAll(strMessageBody, "#<ERRORMESS>", strTransException);
    strMessageFooter =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_FOOTER);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageHeader>", strMessageHeader);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageBody>", strMessageBody);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageFooter>", strMessageFooter);

    emailProps.setMessage(strMessage);
    GmCommonClass.sendMail(emailProps);
  }
  
  /**
   * fetchUploadedFileInfo - To fetch details of uploaded files to be sync with device.
   * @param hmParams
   * @return
   * @throws AppError
   */
  public String fetchJsonUploadedFileInfo(HashMap hmParams) throws AppError {
	     GmDBManager gmDBManager = new GmDBManager();
	     String strReturn = "";
	     gmDBManager.setPrepareString("gm_pkg_device_file_master_sync_rpt.gm_fch_uploaded_file_info",5);
	     gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
	     gmDBManager.setString(1,(String) hmParams.get("TOKEN"));
	     gmDBManager.setString(2,(String) hmParams.get("CMPID"));
	     gmDBManager.setString(3,(String) hmParams.get("LANGUAGE"));
	     gmDBManager.setString(4,(String) hmParams.get("UUID"));
	     gmDBManager.execute();
	     strReturn = GmCommonClass.parseNull(gmDBManager.getString(5));
	     gmDBManager.close();
	     return strReturn;
  }
  /**
   * fetchUploadedFilesReport - To fetch details of uploaded files to show the report screen
   * @param hmParam
   * @return
   * @throws AppError
   */ 
  public String fetchUploadedFilesReport(HashMap hmParam) throws AppError{
	  
	  String strUploadTypeId = GmCommonClass.parseNull((String) hmParam.get("UPLOADTYPEID"));
	  String strSysListId = GmCommonClass.parseNull((String) hmParam.get("SYSLISTID"));
	  String strFileTypeId = GmCommonClass.parseNull((String) hmParam.get("FILETYPEID"));
	  String strTypeListId = GmCommonClass.parseNull((String) hmParam.get("TYPELISTID"));
	  String strFileTitle = GmCommonClass.parseNull((String) hmParam.get("FILETITLE"));
	  String strFileName = GmCommonClass.parseNull((String) hmParam.get("FILENAME"));
	  String strPartNumId = GmCommonClass.parseNull((String) hmParam.get("PARTNUMID"));
	  String strCompanyDateFmt = GmCommonClass.parseNull((String) hmParam.get("COMPANYFMT"));
	  String strUploadedListJSON = "";
	  
	  StringBuffer strQuery = new StringBuffer();
	  GmDBManager gmDBManager = new GmDBManager();

	  strQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT('uploadtype' value get_code_name(c901_ref_grp), ");
	  strQuery.append(" 'systemid' value get_set_name(c903_ref_id), ");
	  strQuery.append(" 'partnum' value c205_part_number_id, 'refid' value c903_ref_id, ");
	  strQuery.append(" 'filename' value c903_file_name,'fileid' value c903_upload_file_list , ");
	  strQuery.append(" 'filetitle' value c901_file_title, ");	
	  strQuery.append(" 'artifact' value get_code_name(c901_ref_type), 'artifactid' value c901_ref_type, 'subtypeid' value c901_type, ");
	  strQuery.append(" 'subtype' value get_code_name(c901_type), 'filesize' value gm_pkg_upload_info.get_mbkb_from_bytes(c903_file_size), ");
	  strQuery.append(" 'updatedby' value get_user_name(NVL (c903_last_updated_by, c903_created_by)), ");
	  strQuery.append(" 'updateddate' value to_char(NVL (c903_last_updated_date, c903_created_date),'"+strCompanyDateFmt+" HH:MI AM') ");	  
	  strQuery.append(" ) ORDER BY get_set_name(c903_ref_id),c205_part_number_id  RETURNING CLOB) ");
	  strQuery.append(" FROM t903_upload_file_list ");
	  strQuery.append(" WHERE c903_delete_fl IS NULL");
	  
	  if(!strUploadTypeId.equals("0")){
		  strQuery.append(" AND c901_ref_grp = ");
		  strQuery.append(strUploadTypeId);
	  }
	  if(!strSysListId.equals("0")){
		  strQuery.append(" AND c903_ref_id = '"+strSysListId+"'");
	  }
	  if(!strFileTypeId.equals("0")){
		  strQuery.append(" AND c901_ref_type = ");
		  strQuery.append(strFileTypeId);
	  }
	  if(!strTypeListId.equals("0")){
		  strQuery.append(" AND c901_type = ");
		  strQuery.append(strTypeListId);
	  }
	  if(!strFileTitle.equals("")){
		  strFileTitle = strFileTitle.trim().replaceAll(",", "|");
		  strQuery.append(" AND REGEXP_LIKE(upper(c901_file_title),upper(REGEXP_REPLACE('");
		  strQuery.append(strFileTitle);
		  strQuery.append("','[+]','\\+')))");

	  }
	  if(!strFileName.equals("")){
		  strFileName = strFileName.trim().replaceAll(",", "|");
		  strQuery.append(" AND REGEXP_LIKE(upper(c903_file_name),upper(REGEXP_REPLACE('");
		  strQuery.append(strFileName);
		  strQuery.append("','[+]','\\+')))");
	  }
	  if (!strPartNumId.equals("")) {  	
		   	strQuery.append(" AND REGEXP_LIKE(c205_part_number_id, CASE WHEN TO_CHAR(REGEXP_REPLACE('"+strPartNumId
			    		  +"','[+]','\\+')) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE('"+strPartNumId
			    		  +"','[+]','\\+')) ELSE c205_part_number_id END) ");  	
	  }
	  log.debug(" fetchUploadedFilesReport => "+ strQuery.toString());
	  
	  strUploadedListJSON = gmDBManager.queryJSONRecord(strQuery.toString());
	  return strUploadedListJSON;
  }
  
}
