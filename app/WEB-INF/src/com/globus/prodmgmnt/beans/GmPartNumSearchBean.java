/*
 * Module:       GmPartNumBean.java
 * Author:       Dhinakaran James
 * Project:      Globus Medical App
 * Date-Written: 19 Jul 2004
 * Security:     Unclassified
 * Description:  
 *
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */

package com.globus.prodmgmnt.beans;
 
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPartNumSearchBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); 

	 public GmPartNumSearchBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }
   public GmPartNumSearchBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

	/**

	   * loadPartNumSearchDrillDown - This method will load part number search, migrated from GmCustomerbean
	   * 
	   * @param HashMap hmParams
	   * @return HashMap
	   * @exception AppError
	   */
	  public HashMap loadPartNumSearchDrillDown(HashMap hmParams) throws AppError {
		  HashMap hmReturn = new HashMap();
		  ArrayList alReturn = new ArrayList();
    	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  String strId=GmCommonClass.parseNull((String) hmParams.get("STRIDS"));
		  String strType=GmCommonClass.parseNull((String) hmParams.get("STRTYPE"));
		  String strPartNum=GmCommonClass.parseNull((String) hmParams.get("STRPARTNUM"));
		  String strControl=GmCommonClass.parseNull((String) hmParams.get("STRCONTROL"));
		  String strFrmDt=GmCommonClass.parseNull((String) hmParams.get("STRFROMDATE"));
		  String strToDt=GmCommonClass.parseNull((String) hmParams.get("STRTODATE"));
		  String strDtFmt=GmCommonClass.parseNull((String) hmParams.get("STRDTFMT"));
		  String strReportBy=GmCommonClass.parseNull((String) hmParams.get("STRREPORTBY"));
    	  String strDateFrmt = GmCommonClass.parseNull(getCompDateFmt()).equals("")?strDtFmt:getCompDateFmt();

	      StringBuffer sbQuery = new StringBuffer();
	      if (strType.equals("S")) {
	        sbQuery.append(" SELECT B.C501_ORDER_ID CID, B.C501_SHIPPING_DATE SDATE, ");
	        sbQuery.append(" A.C502_CONTROL_NUMBER CNUM  ");
	        sbQuery.append(" ,SUM (decode(NVL (b.c901_order_type, -9999),2528,decode(sign(a.c502_item_qty),-1,a.c502_item_qty,0),a.c502_item_qty)) QTY ");
	        sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B ");
	        sbQuery.append(" WHERE A.C205_PART_NUMBER_ID ='");
	        sbQuery.append(strPartNum);
	        sbQuery.append("' AND A.C501_ORDER_ID = B.C501_ORDER_ID AND A.C502_VOID_FL IS NULL AND B.C501_VOID_FL IS NULL AND A.C502_DELETE_FL IS NULL");
	        sbQuery.append(" AND NVL(B.C901_ORDER_TYPE,-9999) NOT IN (2524) ");
	        sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
	        sbQuery.append(" SELECT t906.c906_rule_value ");
	        sbQuery.append(" FROM t906_rules t906 ");
	        sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
	        sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");

	        if (!strControl.equals("")) {
	          sbQuery.append(" AND A.C502_CONTROL_NUMBER = '");
	          sbQuery.append(strControl);
	          sbQuery.append("'");
	        }
	        sbQuery.append(" AND B.C501_ORDER_DATE BETWEEN ");
	        sbQuery.append(" to_date('");
	        sbQuery.append(strFrmDt);
	        sbQuery.append("','"+strDateFrmt+"') AND to_date('");
	        sbQuery.append(strToDt);
	        sbQuery.append("','"+strDateFrmt+"')");
	        sbQuery.append(" AND B.C704_ACCOUNT_ID ='");
	        sbQuery.append(strId);
	        sbQuery.append("' GROUP BY B.C501_ORDER_ID, B.C501_SHIPPING_DATE, A.C502_CONTROL_NUMBER, b.c901_order_type ");
	        sbQuery.append(" ORDER BY B.C501_ORDER_ID DESC ");
	      } else if (strType.equals("C")) // For Consignment
	      {
	        sbQuery.append(" SELECT A.C504_CONSIGNMENT_ID CID, sum(B.C505_ITEM_QTY) QTY, A.C504_SHIP_DATE SDATE, ");
	        sbQuery.append(" A.C207_SET_ID SETID, B.C505_CONTROL_NUMBER CNUM ");
	        sbQuery.append(" FROM T504_CONSIGNMENT A, T505_ITEM_CONSIGNMENT B ");
	        sbQuery.append(" WHERE B.C205_PART_NUMBER_ID = '");
	        sbQuery.append(strPartNum);
	        sbQuery.append("' AND A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID ");
	        if (!strControl.equals("")) {
	          sbQuery.append(" AND B.C505_CONTROL_NUMBER = '");
	          sbQuery.append(strControl);
	          sbQuery.append("'");
	        } else {
	          sbQuery.append(" AND trim(B.C505_CONTROL_NUMBER) IS NOT NULL ");
	        }
	        sbQuery.append(" AND A.C504_TYPE <> 4129 ");
	        sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
	        sbQuery.append(" AND A.C504_SHIP_DATE BETWEEN ");
	        sbQuery.append(" to_date('");
	        sbQuery.append(strFrmDt);
	        sbQuery.append("','"+strDateFrmt+"') AND to_date('");
	        sbQuery.append(strToDt);
	        sbQuery.append("','"+strDateFrmt+"')");
	        sbQuery.append(" AND A.C504_STATUS_FL = '4' ");
	       if (strReportBy.equals("ACCOUNT") || strId.equals("01")) {
	          sbQuery.append(" AND A.C704_ACCOUNT_ID ='");
	        } else {
	          sbQuery.append(" AND A.C701_DISTRIBUTOR_ID ='");
	        }
	        sbQuery.append(strId);
	        sbQuery.append("' GROUP BY A.C504_CONSIGNMENT_ID, A.C504_SHIP_DATE, A.C207_SET_ID, B.C505_CONTROL_NUMBER ");
	        sbQuery.append(" ORDER BY A.C504_CONSIGNMENT_ID DESC ");
	      } else if (strType.equals("R")) // For Returns
	      {
	        sbQuery.append(" SELECT A.C506_RMA_ID CID, sum(B.C507_ITEM_QTY) QTY, A.C506_CREDIT_DATE SDATE, ");
	        sbQuery.append(" A.C207_SET_ID SETID, B.C507_CONTROL_NUMBER CNUM ");
	        sbQuery.append(" FROM T506_RETURNS A, T507_RETURNS_ITEM B ");
	        sbQuery.append(" WHERE B.C205_PART_NUMBER_ID ='");
	        sbQuery.append(strPartNum);
	        sbQuery.append("' AND A.C506_RMA_ID = B.C506_RMA_ID ");
	        sbQuery.append(" AND A.C506_VOID_FL IS NULL ");
	        if (!strControl.equals("")) {
	          sbQuery.append(" AND B.C507_CONTROL_NUMBER = '");
	          sbQuery.append(strControl);
	          sbQuery.append("'");
	        }
	        sbQuery.append(" AND A.C506_CREDIT_DATE BETWEEN ");
	        sbQuery.append(" to_date('");
	        sbQuery.append(strFrmDt);
	        sbQuery.append("','"+strDateFrmt+"') AND to_date('");
	        sbQuery.append(strToDt);
	        sbQuery.append("','"+strDateFrmt+"')");
	        sbQuery.append(" AND A.C506_STATUS_FL > 0 AND B.C507_STATUS_FL <> 'N'");
	        if (strReportBy.equals("ACCOUNT") || strId.equals("01")) {
	          sbQuery.append(" AND A.C704_ACCOUNT_ID ='");
	        } else {
	          sbQuery.append(" AND A.C701_DISTRIBUTOR_ID ='");
	        }
	        sbQuery.append(strId);
	        sbQuery.append("' GROUP BY A.C506_RMA_ID, A.C506_CREDIT_DATE, A.C207_SET_ID, B.C507_CONTROL_NUMBER ");
	        sbQuery.append(" ORDER BY A.C506_RMA_ID DESC ");
	      }
	      log.debug("DRILLDOWN:" + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
	      hmReturn.put("DRILLREPORT", alReturn);

    return hmReturn;
  } // End of loadPartNumSearchDrillDown
  
  
  /**	This Method will check if the Part is visible for the Company.
   * 	If the Part is not visible for the Company, the method throws exception.
   *    If the Part is available for the Company, the method returns "Y".
   *    
   *   	@param String strPartNum
   * 	@return String
   * 	@exception AppError   
   **/
  public String checkPartCompanyVisibility(String strPartNum) throws AppError
  {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  StringBuffer sbQuery = new StringBuffer();
	  String strCompanyID = getCompId();
	  HashMap hmResult = null;
	  String strPartVisibility = "";
	  String strCompanyName = "";
	  
	  sbQuery.append(" SELECT T205.C205_PART_NUMBER_ID PNUM, DECODE(T2023.C205_PART_NUMBER_ID,'','N','Y') PART_VISIBILITY ");
	  sbQuery.append(" , GET_COMPANY_NAME('");
	  sbQuery.append(strCompanyID);
	  sbQuery.append("') COMP_NAME ");
	  sbQuery.append(" FROM T205_PART_NUMBER T205, T2023_PART_COMPANY_MAPPING T2023 ");
	  sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID  = '");
	  sbQuery.append( strPartNum );
	  sbQuery.append("' AND T205.C205_PART_NUMBER_ID = T2023.C205_PART_NUMBER_ID(+) ");
	  sbQuery.append(" AND T2023.C2023_VOID_FL(+) IS NULL ");
	  sbQuery.append(" AND T2023.C1900_COMPANY_ID(+) = '");
	  sbQuery.append(strCompanyID);
	  sbQuery.append("'");
	  
	  log.debug(" Query for Part Visibility: " + sbQuery.toString());
	  hmResult = GmCommonClass.parseNullHashMap(gmDBManager.querySingleRecord(sbQuery.toString()));
	  
	  strPartVisibility = GmCommonClass.parseNull((String)hmResult.get("PART_VISIBILITY"));
  	  strCompanyName= GmCommonClass.parseNull((String)hmResult.get("COMP_NAME"));
  	
  	//If the part is not visible for the Company, throw an error.
  	if (strPartVisibility.equals("N")){
  		String strMsg = AppError.rbApp.getString("PARTNOTRFS");
        strMsg = GmCommonClass.replaceAll(strMsg, "#<PARTNUM>", strPartNum);
        strMsg = GmCommonClass.replaceAll(strMsg, "#<CMPYNM>", strCompanyName);
  		throw new AppError(strMsg, "", 'E');
  	}
      
	  return strPartVisibility;
	  
  }
  
  /** This Method will return the query that will be appended to filter the part based on company visibility.
   * 
   * @param strPartNum
   * @param strPnumAlias Table Alias (For Part Number) used in the Query from where this method will be called.
   * @return String
   */
  public String getPartCompanyFilter(String strPartNum, String strPnumAlias) {
	
	  String strPartMapType = GmCommonClass.getRuleValue("ALL", "PART_MAP_TYPE");
	  return getPartCompanyFilter(strPartNum,strPnumAlias,strPartMapType);
  }
  
  /** This Method will return the query that will be appended to filter the part based on company visibility and Part Mapping Type.
   * 
   * @param strPartNum
   * @param strPnumAlias Table Alias (For Part Number) used in the Query from where this method will be called.
   * @param strMappingType Part Mapping Type , Mapping of the part having relationship to that company (Created,Released,UnReleased).
   * @return String
   */
  public String getPartCompanyFilter(String strPartNum, String strPnumAlias, String strMappingType) {
	
	  StringBuffer sbQuery = new StringBuffer();
	  String strCompanyID = getCompId();
	  
	  sbQuery.append(" AND "+strPnumAlias+".C205_PART_NUMBER_ID IN (SELECT T2023.C205_PART_NUMBER_ID ");
	  sbQuery.append(" FROM T2023_PART_COMPANY_MAPPING T2023 ");	 
	  
	  sbQuery.append(" WHERE REGEXP_LIKE(T2023.C205_PART_NUMBER_ID,REGEXP_REPLACE('");
      sbQuery.append(strPartNum);
      sbQuery.append("','[+]','\\+'))");
	  
	  sbQuery.append(" AND T2023.C2023_VOID_FL IS NULL AND T2023.C1900_COMPANY_ID='");
      sbQuery.append(strCompanyID);
      sbQuery.append("' ");
      sbQuery.append(" AND T2023.C901_PART_MAPPING_TYPE IN ( ");
      sbQuery.append(strMappingType);
      sbQuery.append(" ))");
	  
      log.debug(" getPartCompanyFilter with Mapping Type: "+sbQuery);
	  
	  return sbQuery.toString();
  }
}// end of class GmPartNumBean
