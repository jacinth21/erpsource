package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.prodmgmnt.GmGroupPartVO;
import com.globus.valueobject.prodmgmnt.GmGroupVO;

public class GmGroupServiceBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger

  // this will be removed all place changed with Data Store VO constructor

  public GmGroupServiceBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmGroupServiceBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * This method will be used to fetch group details
   * 
   * @param HashMap hmParams
   * @return
   * @throws AppError
   */
  public List<GmGroupVO> fetchGroups(HashMap hmParams) throws AppError {
    long startTime, finishTime;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmGroupVO> listGmGroupVO;
    GmGroupVO gmGroupVO = (GmGroupVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_group_info.gm_fch_groups_prodcat", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmGroupVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmGroupVO,
            gmGroupVO.getGroupProperties());
    gmDBManager.close();
    return listGmGroupVO;
  }
  
  //PMT# Group sync in iPad with Interantional Changes
  
  /**
   * This method will be used to fetch group details
   * 
   * @param HashMap hmParams
   * @return
   * @throws AppError
   */
  public String fetchJasonGroups(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strJasonReturn="";
    gmDBManager.setPrepareString("gm_pkg_device_group_master_sync_rpt.gm_fch_groups_prodcat", 6);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
    gmDBManager.execute();
    strJasonReturn =  GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.close();
    return strJasonReturn;
  }
  
 
  /**
   * This method will be used to fetch group part details
   * 
   * @param HashMap hmParams
   * @return
   * @throws AppError
   */
  public List<GmGroupPartVO> fetchGroupParts(HashMap hmParams) throws AppError {
    long startTime, finishTime;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmGroupPartVO> listGmGroupPartVO;
    GmGroupPartVO gmGroupPartVO = (GmGroupPartVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pd_group_info.gm_fch_group_parts", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();

    listGmGroupPartVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmGroupPartVO,
            gmGroupPartVO.getGroupPartProperties());
    gmDBManager.close();
    return listGmGroupPartVO;
  }
  
  //PMT# Group Part sync in iPad with Interantional Changes
  
  /**
   * This method will be used to fetch group part details
   * 
   * @param HashMap hmParams
   * @return
   * @throws AppError
   */
  public String fetchJsonGroupParts(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strJasonReturn="";
    gmDBManager.setPrepareString("gm_pkg_device_group_master_sync_rpt.gm_fch_group_parts", 6);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.setString(5, (String) hmParams.get("COMPID"));
    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
    gmDBManager.execute();
    strJasonReturn =  GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.close();
    return strJasonReturn;
  }
  
 
  /**
   * checkGroupExist - This method will check the group name.
   * 
   * @param - String
   * @return - String
   * @exception - AppError
   */
  public String checkGroupExist(String strGroupName) throws AppError {
    String strName = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_pd_group_info.gm_chk_group_count", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strGroupName);
    gmDBManager.execute();
    strName = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strName;
  }

  /**
   * processGroupVisibility - This Method will form the message for new Group and it is send through
   * JMS by using the sendMessage method.If the Group is not a new one nothing will happen.
   * 
   * @param - strGroupID,strGroupIdFromDb,strSystemId,strUserId :here the strGroupIdFromDb means
   *        newly created Group ID.strSystemId means set id for that Group.
   * @return - Void
   * @exception - AppError
   */
  public void processGroupVisibility(String strGroupID, String strGroupIdFromDb,
      String strSystemId, String strUserId) throws AppError {

    HashMap hmParam = new HashMap();
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();

    // Here we are checking the Group whether it is new or existing .if it is new Group we are
    // creating new Group.
    if (strGroupID.equals("0")) {

      // GOP_GROUP_CONSUMER_CLASS:It is in constant property file.This key contain the path
      // :C:\oneportal\GlobusMedEar\globusMedApp\WEB-INF\src\com\globus\jms\consumers\processors\GmPDGroupConsumerJob.java
      String strConsumerClass =
          GmCommonClass.parseNull(GmCommonClass.getString("GOP_GROUP_CONSUMER_CLASS"));

      hmParam.put("STRGROUPID", strGroupIdFromDb);
      hmParam.put("STRSYSTEMID", strSystemId);
      hmParam.put("STRUSERID", strUserId);
      hmParam.put("CONSUMERCLASS", strConsumerClass);

      gmConsumerUtil.sendMessage(hmParam);

    }
  }

  /**
   * syncGroupCompanyMapping - This method is calling from GmPDGroupConsumerJob.java.This Method
   * will save Group based on system .If the system is released to Two companies .The Group will be
   * visible for the two companies.
   * 
   * @param - strGroupID,strSystemId,strMapType,strUserId :strGroupID is newly created GroupId .
   *        strMapType:105361(Released).
   * @return - Void
   * @exception - AppError
   */
  public void syncGroupCompanyMapping(String strGroupID, String strSystemId, String strMapType,
      String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_pd_group_txn.gm_sync_group_to_all_company", 4);


    gmDBManager.setString(1, strGroupID);
    gmDBManager.setString(2, strSystemId);
    gmDBManager.setString(3, strMapType);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();

  }
}
