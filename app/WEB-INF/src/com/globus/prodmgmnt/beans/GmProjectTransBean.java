/**
 * FileName : GmProjectTransBean.java Description : Author : sthadeshwar Date : Feb 9, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.prodmgmnt.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sthadeshwar
 *
 */
public class GmProjectTransBean extends GmBean {
  /**
   * @author rshah
   * 
   */
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
  // Initialize
  // the
  // Logger
  // Class.

  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmProjectTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmProjectTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /*
   * saveProjectDetails - This method saves the project details
   * 
   * @param hmParam - parameters to be saved
   * 
   * @exception AppError
   * 
   * @return String
   */

  public String saveProjectDetails(HashMap hmParam) throws AppError {
    String strPrjIdFromDb = "";
    String strMsg = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPrjNm = GmCommonClass.parseNull((String) hmParam.get("PRJNM"));
    String strPrjDesc = GmCommonClass.parseNull((String) hmParam.get("PRJDESC"));
    String strPrjId = GmCommonClass.parseNull((String) hmParam.get("PRJID"));
    String strPrjStatusId = GmCommonClass.parseZero((String) hmParam.get("PRJSTATUSID"));
    String strPrjCrtdBy = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPrjType = GmCommonClass.parseZero((String) hmParam.get("PRJTYPE"));
    String strPrjIniBy = GmCommonClass.parseNull((String) hmParam.get("PRJINIBY"));
    String strPrjAprvdBy = GmCommonClass.parseNull((String) hmParam.get("PRJAPRVDBY"));
    String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("DIVISIONID"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strProjAttrInputStr = GmCommonClass.parseNull((String) hmParam.get("PROJATTRINPUTSTR"));
    String strPrjDtlDesc = GmCommonClass.parseNull((String) hmParam.get("PRJDTLDESC"));
    String strCompanyId = getCompId();
    String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));

    gmDBManager.setPrepareString("gm_pkg_pd_project_trans.gm_sav_projectdetails", 14);
    gmDBManager.registerOutParameter(13, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(14, java.sql.Types.CHAR);

    gmDBManager.setString(1, strPrjNm);
    gmDBManager.setString(2, strPrjDesc);
    gmDBManager.setString(3, strPrjId);
    gmDBManager.setString(4, strPrjStatusId);
    gmDBManager.setString(5, strPrjCrtdBy);
    gmDBManager.setString(6, strPrjType);
    gmDBManager.setString(7, strPrjIniBy);
    gmDBManager.setString(8, strPrjAprvdBy);
    gmDBManager.setInt(9, Integer.parseInt(strDivisionId));
    gmDBManager.setString(10, strPrjDtlDesc);
    gmDBManager.setString(11, strCompanyId);
    gmDBManager.setString(12, strProjectId);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(13);
    strPrjIdFromDb = gmDBManager.getString(14);
    log.debug("In Bean...." + strPrjIdFromDb + "........" + strMsg);

    hmParam.put("PRJID", strPrjIdFromDb);
    if (strProjAttrInputStr.length() > 0) {
      saveProjectAttribute(gmDBManager, hmParam);
    }

    if (!strLog.equals("")) {
      log.debug("The log value insdie the bean class is  ****** " + strLog + " *********** "
          + strPrjId);
      gmCommonBean.saveLog(gmDBManager, strPrjIdFromDb, strLog, strPrjCrtdBy, "4000397"); // Project
                                                                                          // Setup
      // Log - GPLOG
    }
    hmParam.put("MAP_TYPE", "105360");// 105360 - created
    hmParam.put("COMP_ID", strCompanyId);
    // This method used to save the project company mapping details
    saveProjectCompanyMap(gmDBManager, hmParam);

    gmDBManager.commit();
    return strPrjIdFromDb;
  }

  /**
   * saveProjectMilestoneDetails - This method saves the project details
   * 
   * @param hmParam - parameters to be saved
   * @exception AppError
   * @return String
   */
  public void saveProjectMilestoneDetails(HashMap hmParam) throws AppError {
    String strPrjId = GmCommonClass.parseNull((String) hmParam.get("PRJID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug("in Bean........." + strPrjId + "........." + strInputStr + ".........." + strUserId);
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_pd_project_trans.gm_sav_milestone_details", 3);
    gmDBManager.setString(1, strPrjId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * saveProjectAttribute - This method saves the project attributes
   * 
   * @param hmParam -
   * @exception AppError
   * 
   */
  public void saveProjectAttribute(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    String strPrjId = GmCommonClass.parseNull((String) hmParam.get("PRJID"));
    String strProjAttrInputStr = GmCommonClass.parseNull((String) hmParam.get("PROJATTRINPUTSTR"));
    String strAttrTypeInputStr = GmCommonClass.parseNull((String) hmParam.get("ATTRTYPEINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_pd_project_trans.gm_sav_project_attribute", 4);
    gmDBManager.setString(1, strPrjId);
    gmDBManager.setString(2, strProjAttrInputStr);
    gmDBManager.setString(3, strAttrTypeInputStr);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }// End of saveProjectAttribute

  /**
   * saveProjectCompanyMap - This method saves the project company mapping details
   * 
   * @param gmDBManager - Object to make Connection with database
   * @param hmParam - Contains the project and company details to pass it into procedure.
   * @throws AppError
   */
  public void saveProjectCompanyMap(GmDBManager gmDBManager, HashMap hmParam) throws AppError {

    gmDBManager.setPrepareString("gm_pkg_pd_project_trans.gm_sav_project_company_map", 4);
    gmDBManager.setString(1, (String) hmParam.get("PROJECTID"));
    gmDBManager.setString(2, (String) hmParam.get("COMP_ID"));
    gmDBManager.setString(3, (String) hmParam.get("MAP_TYPE"));
    gmDBManager.setString(4, (String) hmParam.get("USERID"));
    gmDBManager.execute();

  }


  /**
   * 
   * saveProjectReleaseToCompany - Method used to map the project details
   * 
   * @param hmParam
   * @throws AppError
   */
  public void saveProjectReleaseToCompany(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMP_ID"));
    String strMapType = GmCommonClass.parseNull((String) hmParam.get("MAP_TYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug(" to sync the project information " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_pd_project_trans.gm_sav_prj_release_to_company", 4);
    gmDBManager.setString(1, strProjectId);
    gmDBManager.setString(2, strCompanyId);
    gmDBManager.setString(3, strMapType);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
    log.debug(" Project - Company mapping done ");
    // Set to be update to cache server
    // to delete the redis key ()
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strKey = "SET_LST:" + strCompanyId + ":AUTO";
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.removeKey(strKey);
    log.debug(" Set key removed ");
  }

  /**
   * saveProjectCompanyMap - this method used to call the existing method with DBmanager object
   * 
   * @param hmParam
   * @throws AppError
   */
  public void saveProjectCompanyMap(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveProjectCompanyMap(gmDBManager, hmParam);
    gmDBManager.commit();
  }

}
