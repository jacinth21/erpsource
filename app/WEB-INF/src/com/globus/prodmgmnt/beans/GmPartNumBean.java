/*
 * Module: GmPartNumBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 19
 * Jul 2004 Security: Unclassified Description:
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.prodmgmnt.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.prodmgmnt.OrdClassification.beans.GmClassifyOrdBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisTransBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingReportBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingWrapperBean;

public class GmPartNumBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
  // Initialize
  // the
  // Logger
  // Class.

  GmCommonClass gmCommon = new GmCommonClass();

  // this will be removed all place changed with Data Store VO constructor
  public GmPartNumBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartNumBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  ArrayList alType = new ArrayList();
  ArrayList alState = new ArrayList();
  ArrayList alCountry = new ArrayList();
  ArrayList alPay = new ArrayList();

  ArrayList alDevClass = new ArrayList();
  ArrayList alProdMat = new ArrayList();

  /**
   * loadPartNum - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPartNum() throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301"); // Filter all approved projects

    HashMap hmVal = new HashMap();
    String strCodeGrp = "";
    String strCodeNm = "";
    try {
      GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
      GmPartNumBean gmPartNum = new GmPartNumBean(getGmDataStoreVO());
      GmClassifyOrdBean gmClassifyOrdBean = new GmClassifyOrdBean(getGmDataStoreVO());
      GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());

      alDevClass = gmCommon.parseNullArrayList(gmCommon.getCodeList("PCLAS"));
      alProdMat = gmCommon.parseNullArrayList(gmClassifyOrdBean.getCodeList("PRMAT")); // order by
                                                                                       // Code name
      ArrayList alProjList = gmCommon.parseNullArrayList(gmProjectBean.reportProject(hmTemp));
      ArrayList alProdFly = gmCommon.parseNullArrayList(gmCommon.getCodeList("PFLY"));
      ArrayList alMeasDev = gmCommon.parseNullArrayList(gmCommon.getCodeList("MEUSR"));
      ArrayList alMatSpec = gmCommon.parseNullArrayList(gmCommon.getCodeList("MATSP"));
      ArrayList alPackaging = gmCommon.parseNullArrayList(gmCommon.getCodeList("PACKM"));
      ArrayList alPartStatus = gmCommon.parseNullArrayList(gmCommon.getCodeList("PSTAT"));
      ArrayList alCertificate = gmCommon.parseNullArrayList(gmCommon.getCodeList("CERTR"));
      ArrayList alUOM = gmCommon.parseNullArrayList(gmCommon.getCodeList("PTUOM"));
      ArrayList alSalesGroup = gmCommon.parseNullArrayList(gmProjectBean.loadSetMap("1600")); // Sales
      // Report
      // Group
      ArrayList alRFS = gmCommon.parseNullArrayList(gmCommon.getCodeList("RFSALE"));
      ArrayList alPathway = gmCommon.parseNullArrayList(GmCommonClass.getCodeList("PATHW"));
      ArrayList alRPF = gmCommon.parseNullArrayList(gmCommon.getCodeList("RPFORM"));
      ArrayList alRegulatory = gmCommon.parseNullArrayList(gmCommon.getCodeList("REGUL"));
      ArrayList alArtifacts = gmCommon.parseNullArrayList(gmCommon.getCodeList("FSBTYP", "103114"));// 103114
                                                                                                    // -
                                                                                                    // DCOed
                                                                                                    // Documents
      ArrayList alWOCreated = gmCommon.parseNullArrayList(gmCommon.getCodeList("WOCFL"));
      ArrayList alUDIEtchRequired = gmCommon.parseNullArrayList(gmCommon.getCodeList("UDIER"));
      ArrayList alDIOnlyEtchRequired = gmCommon.parseNullArrayList(gmCommon.getCodeList("DIOER"));
      ArrayList alPIOnlyEtchRequired = gmCommon.parseNullArrayList(gmCommon.getCodeList("PIOER"));
      ArrayList alVendorDesign = gmCommon.parseNullArrayList(gmCommon.getCodeList("VENDGN"));
      ArrayList alStructType = gmCommon.parseNullArrayList(gmCommon.getCodeList("STLTYP"));
      ArrayList alPresMethod = gmCommon.parseNullArrayList(gmCommon.getCodeList("PREMET"));
      ArrayList alStorTemp = gmCommon.parseNullArrayList(gmCommon.getCodeList("STRGTP"));
      ArrayList alProSpecType = gmCommon.parseNullArrayList(gmCommon.getCodeList("PSTYPE"));
      ArrayList alAccList = gmCommon.parseNullArrayList(fetchContractProcessingClient());

      hmReturn.put("CLASS", alDevClass);
      hmReturn.put("PRODMAT", alProdMat);
      hmReturn.put("PROJLIST", alProjList);
      hmReturn.put("PRODFLY", alProdFly);
      hmReturn.put("MEASDEV", alMeasDev);
      hmReturn.put("MATSPEC", alMatSpec);
      hmReturn.put("PACK", alPackaging);
      hmReturn.put("PARTSTATUS", alPartStatus);
      hmReturn.put("PARTCERT", alCertificate);
      hmReturn.put("SALESGROUP", alSalesGroup);
      hmReturn.put("UOM", alUOM);
      hmReturn.put("RFS", alRFS);
      hmReturn.put("PATHWAY", alPathway);
      hmReturn.put("RPF", alRPF);
      hmReturn.put("REGULATORY", alRegulatory);
      hmReturn.put("ALARTIFACTS", alArtifacts);

      hmReturn.put("ALWOCREATED", alWOCreated);
      hmReturn.put("ALUDIETCHREQUIRED", alUDIEtchRequired);
      hmReturn.put("ALDIONLYETCHREQUIRED", alDIOnlyEtchRequired);
      hmReturn.put("ALPIONLYETCHREQUIRED", alPIOnlyEtchRequired);
      hmReturn.put("ALVENDORDESIGN", alVendorDesign);
      hmReturn.put("STRUCTTYPE", alStructType);
      hmReturn.put("PRESMETHOD", alPresMethod);
      hmReturn.put("STORTEMP", alStorTemp);
      hmReturn.put("PROSPECTYPE", alProSpecType);
      hmReturn.put("ALACCLIST", alAccList);

      for (int i = 0; i < alPathway.size(); i++) {
        hmVal = (HashMap) alPathway.get(i);
        strCodeGrp = (String) hmVal.get("CODENMALT");
        strCodeNm = (String) hmVal.get("CODENM");
        hmReturn.put(strCodeNm, gmCommon.getCodeList(strCodeGrp));

      }


    } catch (Exception e) {
      GmLogError.log("Exception in GmPartNumBean:loadPartNum", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadPartNum

  public void validateControlNumber(String strPartNum, String strCntrlNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_cntrl_num_exists", 2);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strCntrlNum);
    gmDBManager.execute();
    gmDBManager.close();
  }

  public HashMap loadEditPartNum(String strId, String strval) throws AppError {
    HashMap hmtemp = new HashMap();
    return hmtemp;
  }

  /**
   * loadEditPartNum - This method will be used by report and setup pages of Part # for querying
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadEditPartNum(HashMap hmValues) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
    HashMap hmPartVisibility = null;
    String strCompanyName = "";
    String strPartVisibility = "";

    String strPartCompFiltr = "";
    String strAction = GmCommonClass.parseNull((String) hmValues.get("ACTION"));
    String strProjectId = GmCommonClass.parseNull((String) hmValues.get("PROJECTID"));
    String strPartNum = GmCommonClass.parseNull((String) hmValues.get("PARTNUM"));
    String strSearch = GmCommonClass.parseNull((String) hmValues.get("LIKEOPTION"));
    String strPartNumFormat = GmCommonClass.parseNull((String) hmValues.get("PARTNUM"));
    String strPartDesc = GmCommonClass.parseNull((String) hmValues.get("PARTDESC"));
    String strTaggablePart = GmCommonClass.parseNull((String) hmValues.get("TAGGABLEFL"));
    String strSubComponentFlag = GmCommonClass.parseNull((String) hmValues.get("SUBCOMPONENTFL"));
    
    if(strSubComponentFlag.equalsIgnoreCase("ON")){
    	strSubComponentFlag = "Y";
    }else{
    	strSubComponentFlag="";
    }
    log.debug(":strSubComponentFlag bean:"+strSubComponentFlag);
    String strCompanyID = getCompId();

    if (!strAction.equals("ALL")) {
      gmPartSearchBean.checkPartCompanyVisibility(strPartNumFormat);
    }
    if (strSearch.equals("90181")) {
      strPartNumFormat = ("^").concat(strPartNumFormat);
      strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
    } else if (strSearch.equals("90182")) {
      strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
      strPartNumFormat = strPartNumFormat.concat("$");
    } else {
      strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
    }

    if (strAction.equals("ALL")) {

      strPartCompFiltr = gmPartSearchBean.getPartCompanyFilter(strPartNumFormat, "t205");

    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT t205.C205_PART_NUMBER_ID PARTNUM, ");
    if (strCompanyID.equals("1001")) {
      sbQuery.append("NVL(T2021.C202_PROJECT_ID,t205.C202_PROJECT_ID) PROJID,");
      sbQuery
          .append(" NVL(GET_PROJECT_NAME(T2021.C202_PROJECT_ID),GET_PROJECT_NAME(t205.C202_PROJECT_ID)) PROJNM ,");
    } else {
      sbQuery.append("t205.C202_PROJECT_ID PROJID,");
      sbQuery.append(" GET_PROJECT_NAME(t205.C202_PROJECT_ID) PROJNM , ");
    }
    sbQuery
        .append(" C205_PART_NUM_DESC PDESC,C205_PRODUCT_FAMILY PFLY, C205_PRODUCT_MATERIAL PMAT, ");
    sbQuery.append(" C205_PRODUCT_CLASS PCLASS,C205_MEASURING_DEV MDEV,C205_MATERIAL_SPEC MSPEC, ");
    sbQuery.append(" C205_PART_DRAWING DRAW, C205_REV_NUM REV, ");
    sbQuery.append(" v205g.c205d_artifacts ARTIFACTS, ");
    sbQuery
        .append(" GET_CODE_NAME(C205_MATERIAL_SPEC) MSPECNM, C205_INSPECTION_REV INSPREV, C205_TRACKING_IMPLANT_FL TRACIMPL, ");
    // sbQuery.append(" C205_ACTIVE_FL AFL, C901_STATUS_ID PARTSTATUS, C205_RELEASE_FOR_SALE RELFORSALE, ");
    sbQuery
        .append(" C205_ACTIVE_FL AFL, C901_STATUS_ID PARTSTATUS, decode(get_rfs_country(t205.c205_part_number_id,'80120'), NULL,'N','Y')  RELFORSALE, ");
    sbQuery
    .append("  decode(get_rfs_country(t205.c205_part_number_id,'80121'), NULL,'N','Y')  RELFORSALE_EU, ");
    sbQuery
        .append(" C205_SUPPLY_FL SUPPLYFL, C205_SUB_ASMBLY_FL SUBASSMFL, GET_SET_ID_FROM_PART(t205.C205_PART_NUMBER_ID) SALESGROUP, ");
    sbQuery
        .append(" C205_INSERT_ID INSERTID ,t205.C205_INSERT_ISSUE_FL INSERTFL, GET_CODE_NAME(C901_STATUS_ID) STATUSMSG ,  ");
    sbQuery.append(" GET_CODE_NAME(C205_PRODUCT_FAMILY) PRODF , ");
    sbQuery
        .append(" get_code_name(C205_PRODUCT_MATERIAL) PMATNM, get_code_name(C205_PRODUCT_FAMILY) PFLYNM  ");
    sbQuery.append(" ,get_set_name(GET_SET_ID_FROM_PART(t205.C205_PART_NUMBER_ID)) SALESGROUPNM ");
    sbQuery.append(" ,C901_UOM UOM");
    sbQuery
        .append(" , c205_min_accpt_rev_lel MINACCPREV, c901_rpf  RPF, decode(v205g.c205d_taggable_part,'Y','on','off') TAGGABLEFL");
    sbQuery.append(" , get_part_attr_value_by_comp (t205.C205_PART_NUMBER_ID, '200001', "
        + getCompId() + ") NAPPICODE, c205_part_num_dtl_desc DETAILDESC");
    sbQuery
        .append(" , v205g.c205d_shelf_Life SHELFLIFE , v205g.c205d_structural_Type STRCTYPE , v205g.c205d_preservation_Method PRESERVMETHOD ");
    sbQuery
        .append(" , v205g.c205d_storage_temperature STORETEMP, v205g.c205d_processing_spec_type PROCSPECTYPE , v205g.c205d_master_Product MASTPROD ");
    sbQuery
        .append(" , v205g.c205d_contract_process_client CONTPROCLNT ,v205g.c205d_is_fixed_size FIXEDSIZE ");
    sbQuery.append(" , v205g.c205d_tollerance TOLLERANCE, t2023.c1900_company_id CREATEDCMPNY");
    // 104480: Lot tracking required ?; 106040: Serial Number Needed ?
    sbQuery.append(" , GET_PART_ATTR_VALUE_BY_COMP(t205.C205_PART_NUMBER_ID,'104480',"
        + strCompanyID + ") LOTTRACKFL");
    sbQuery.append(" , get_part_attr_value_by_comp(t205.C205_PART_NUMBER_ID,'106040',"
        + strCompanyID + ") SERIALNUMNEEDFL");
    sbQuery.append(" FROM T205_PART_NUMBER t205 , v205g_part_label_parameter v205g ");
    sbQuery.append(" , T2023_PART_COMPANY_MAPPING t2023 ");
    sbQuery
        .append(" ,( SELECT c205_part_number_id, c202_project_id from T2021_PART_PROJECT_MAPPING ");
    if (strAction.equals("ALL") && (!strProjectId.equals("0"))) {
      sbQuery.append(" WHERE c202_project_id = '");
      sbQuery.append(strProjectId);
      sbQuery.append("'");
      sbQuery.append("   AND c2021_void_fl  IS NULL");
    }
    sbQuery.append(" )t2021");
    sbQuery.append(" WHERE C205_CREATED_BY IS NOT NULL ");
    sbQuery.append(" AND t205.C205_PART_NUMBER_ID = v205g.C205_PART_NUMBER_ID (+) ");

    if (strAction.equals("ALL")) {
      if (!strProjectId.equals("0")) {
        sbQuery.append(" AND ( T205.C202_PROJECT_ID = '");
        sbQuery.append(strProjectId);
        sbQuery.append("'");
        sbQuery.append(" OR T2021.C202_PROJECT_ID = '");
        sbQuery.append(strProjectId);
        sbQuery.append("' )");
      }

      if (!strPartNum.equals("")) {
    sbQuery.append(" AND REGEXP_LIKE(t205.C205_PART_NUMBER_ID,REGEXP_REPLACE('");
      sbQuery.append(strPartNumFormat);
      sbQuery.append("','[+]','\\+'))");
        sbQuery.append(strPartCompFiltr);
      }


      if (!strPartDesc.equals("")) {
        sbQuery.append(" AND UPPER (t205.C205_PART_NUM_DESC) LIKE'%");
        sbQuery.append(strPartDesc.toUpperCase());
        sbQuery.append("%' ");
      }
      sbQuery.append(" AND NVL(t205.c205_sub_component_fl,'-9999') = CASE WHEN NVL('"+strSubComponentFlag+"','N') = 'Y' THEN NVL(t205.c205_sub_component_fl,'-9999') ELSE   '-9999' END ");
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID = t2023.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID = T2021.c205_part_number_id(+) ");
      sbQuery.append(" AND t2023.C1900_COMPANY_ID = " + strCompanyID);
      sbQuery.append(" AND t2023.C2023_VOID_FL IS NULL ");
      //For  Sub Component scenario,we should not include the below condition as for sub component parts we will have more than one "." in the part number.
      
      if(!strSubComponentFlag.equals("Y"))
    	  sbQuery.append(" AND INSTR(t205.C205_PART_NUMBER_ID, '.', 1, 2) = 0 ");
      
      sbQuery.append(" ORDER BY t205.C205_PART_NUMBER_ID");
      log.debug(" Query for Part Report " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("PARTNUMDETAILS", alReturn);
    } else {
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID = '");
      sbQuery.append(strPartNum);
      sbQuery.append("'");
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID = t2023.c205_part_number_id(+) ");
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID = T2021.c205_part_number_id(+) ");
      sbQuery.append(" AND t2023.c2023_void_fl(+) IS NULL ");
      sbQuery.append(" AND t2023.c901_part_mapping_type(+) = 105360");// 105360: Created
      sbQuery.append(strPartCompFiltr);
      log.debug(" Query for Part Setup " + sbQuery.toString());
      HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmReturn.put("PARTNUMDETAILS", hmResult);

      // The Following code is added for resolving the BUG-4725.
      // Due to gmPartNum.loadPartNum() method call from many blocks, we are getting performence
      // issue for loading the many drop down values for more times.
      // so here we are commenting all the method calls and we are getting the drop down values one
      // time and using the same hash map object where ever we are calling the
      // gmPartNum.loadPartNum().
      // and from the GmPartNumBean.loadEditPartNum() method also we are calling
      // gmPartNum.loadPartNum() again. here are also we need remove.

      /*
       * hmResult = loadPartNum(); hmReturn.put("LOADDETAILS", hmResult);
       */
    }
    return hmReturn;
  } // End of loadEditPartNum


  public String fetchRFS(String strPartNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRFS = "";

    gmDBManager.setFunctionString("gm_pkg_pd_partnumber.get_rfs_info", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strPartNum);

    gmDBManager.execute();
    strRFS = gmDBManager.getString(1);
    gmDBManager.close();

    return strRFS;
  }


  public ArrayList loadPathwayDtls(String strPartNum) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_pathway_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPartNum);

    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alReturn;
  }


  public ArrayList loadRegulatoryDtls(String strPartNum) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_regulatory_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPartNum);

    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alReturn;
  }


  /**
   * loadPartNumMapping - This method will load the sub assemly parts for a parent part or the
   * parent parts of a sub assembly part depending on the type
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPartNumMapping(HashMap hmParam) {
    ArrayList alReturn = new ArrayList();
    HashMap hmPPartDetails;
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strParentPNum = GmCommonClass.parseNull((String) hmParam.get("PPNUM"));
    String strSubPNum = GmCommonClass.getStringWithQuotes((String) hmParam.get("SUBPART"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    log.debug(" strSubPNum before " + strSubPNum);
    if (!strSubPNum.equals("") && strSubPNum.indexOf(",") > 0) {
      strSubPNum = strSubPNum.replaceAll("','", "|^");
      strSubPNum = "^".concat(strSubPNum);
      strSubPNum = strSubPNum.substring(0, (strSubPNum.lastIndexOf("|^")));
    }
    log.debug(" strSubPNum after " + strSubPNum);
    String strFromPNumQuery = "";
    String strToPNumQuery = "";

    if (strType.equals("pnum")) {
      strFromPNumQuery = " T205A.C205_FROM_PART_NUMBER_ID ";
      strToPNumQuery = " T205A.C205_TO_PART_NUMBER_ID ";
    }

    else {
      strFromPNumQuery = " T205A.C205_TO_PART_NUMBER_ID ";
      strToPNumQuery = " T205A.C205_FROM_PART_NUMBER_ID ";
    }


    StringBuffer sbQuery = new StringBuffer();
    StringBuffer sbPartDescQuery = new StringBuffer();

    sbQuery.append(" SELECT T205A.C205A_PART_MAPPING_ID PMAPID , ");
    sbQuery.append(strToPNumQuery);
    sbQuery
        .append(" PNUM , T205A.C205A_QTY QTY, T205.C205_PART_NUM_DESC PDESC , T205A.C901_TYPE PTYPE ");
    sbQuery.append(" , T205.C901_STATUS_ID STATUSID  , T205.C205_REV_NUM REVLVL ");
    sbQuery.append(" FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205 ");
    sbQuery.append(" WHERE ");
    sbQuery.append(strFromPNumQuery);
    sbQuery.append("  = '");
    sbQuery.append(strParentPNum);
    sbQuery.append("' AND ");
    sbQuery.append(strToPNumQuery);
    sbQuery.append(" = T205.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T205A.C205A_VOID_FL IS NULL ");   // added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
    if (!strSubPNum.equals("")) {
      sbQuery.append(" UNION ");
      sbQuery.append(" SELECT 0, T205.C205_PART_NUMBER_ID, 1, T205.C205_PART_NUM_DESC ,0  ");
      sbQuery.append(" , T205.C901_STATUS_ID STATUSID  , T205.C205_REV_NUM REVLVL ");
      sbQuery.append(" FROM T205_PART_NUMBER T205 ");
      
      sbQuery.append(" WHERE REGEXP_LIKE(C205_PART_NUMBER_ID,REGEXP_REPLACE('");
      sbQuery.append(strSubPNum);
      sbQuery.append("','[+]','\\+'))");
      sbQuery.append(" AND T205.C205_PART_NUMBER_ID NOT IN(SELECT ");
      
      sbQuery.append(strToPNumQuery);
      sbQuery.append(" FROM T205A_PART_MAPPING T205A WHERE ");
      sbQuery.append(strFromPNumQuery);
      sbQuery.append(" = '");
      sbQuery.append(strParentPNum);
      sbQuery.append("'  ");
      sbQuery.append(" AND T205A.C205A_VOID_FL IS NULL ");   // added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
      sbQuery.append(" ) ");
    }
    sbQuery.append(" ORDER BY PNUM ");

    log.debug(" Query to loadPartNumMapping  " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    sbPartDescQuery
        .append(" SELECT T205.C205_PART_NUM_DESC PDESC, T205.C901_STATUS_ID PSTATUSID , GET_CODE_NAME(T205.C901_STATUS_ID) STATUSMSG ");
    sbPartDescQuery.append(" FROM T205_PART_NUMBER T205 ");
    sbPartDescQuery.append(" WHERE T205.C205_PART_NUMBER_ID = '");
    sbPartDescQuery.append(strParentPNum);
    sbPartDescQuery.append("'");

    log.debug(" Query for PPart Details  " + sbPartDescQuery.toString());

    hmPPartDetails = gmDBManager.querySingleRecord(sbPartDescQuery.toString());
    hmReturn.put("PARTDETAILS", alReturn);
    hmReturn.put("PPARTDETAILS", hmPPartDetails);
    log.debug(" hmreturn " + hmReturn);

    return hmReturn;
  }

  /**
   * loadEditProject - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadEditProject(String strId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	C202_PROJECT_ID ID, C202_PROJECT_NM NAME, C202_DEVICE_CLASS DEVCLASS, ");
    sbQuery.append(" C202_CREATED_BY PMAN, C202_PROJECT_DESC PDESC, C201_IDEA_ID IDEAID ");
    sbQuery.append(" FROM T202_PROJECT ");
    sbQuery.append(" WHERE C202_PROJECT_ID = '");
    sbQuery.append(strId);
    sbQuery.append("'");
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("PROJECTDETAILS", hmResult);

    alDevClass = gmCommon.getCodeList("DEVCL");

    hmReturn.put("DEVCLASS", alDevClass);

    return hmReturn;
  } // End of loadEditProject

  /**
   * savePartNumber
   * After called the savePartNumber method - to call the new JMS for Work order update.
   * Saving attributes from Part Number Setup screen and also sends email s re Open Work Orders if the Rev was changed 
   * @param HashMap hmParam
   * @param String strUsername
   * @exception AppError
   */
  public void savePartNumber(HashMap hmParam, String strUsername) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPartNumberServiceBean gmPartNumberServiceBean =
        new GmPartNumberServiceBean(getGmDataStoreVO());

    String strUserId = "";
    HashMap hmPartInfo = new HashMap();

    String strPartNm = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strProjId = GmCommonClass.parseNull((String) hmParam.get("PROJID"));
    String strPartDesc = GmCommonClass.parseNull((String) hmParam.get("PDESC"));
    String strDetailDesc = GmCommonClass.parseNull((String) hmParam.get("DETAILDESC"));
    String strProdFly = GmCommonClass.parseZero((String) hmParam.get("PFLY"));
    String strProdMat = GmCommonClass.parseZero((String) hmParam.get("PMAT"));
    String strClass = GmCommonClass.parseZero((String) hmParam.get("PCLASS"));
    String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("AFL"));
    String strMeasDev = GmCommonClass.parseZero((String) hmParam.get("MDEV"));
    String strMatSpec = GmCommonClass.parseZero((String) hmParam.get("MATSP"));
    String strDrawNum = GmCommonClass.parseNull((String) hmParam.get("DRAW"));
    String strRevNum = GmCommonClass.parseNull((String) hmParam.get("REV"));
    String strInsertId = GmCommonClass.parseZero((String) hmParam.get("INSERTID"));
    String strInspectRev = GmCommonClass.parseNull((String) hmParam.get("INSPREV"));
    String strTrackingImpl = GmCommonClass.parseNull((String) hmParam.get("TRACIMPL"));
    String strPartStatus = GmCommonClass.parseNull((String) hmParam.get("PARTSTATUS"));
    String strReleaseForSale = GmCommonClass.parseNull((String) hmParam.get("RELFORSALE"));
    String strSupplyFlag = GmCommonClass.parseNull((String) hmParam.get("SUPPLYFL"));
    String strSubAssemblyFlag = GmCommonClass.parseNull((String) hmParam.get("SUBASSMFL"));
    String strSalesGroup = GmCommonClass.parseNull((String) hmParam.get("SALESGROUP"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOG"));
    String strUOM = GmCommonClass.parseNull((String) hmParam.get("UOM"));
    String strMinAccpRev = GmCommonClass.parseNull((String) hmParam.get("MINACCPREV"));
    String strRPF = GmCommonClass.parseNull((String) hmParam.get("RPF"));
    String strRFS = GmCommonClass.parseNull((String) hmParam.get("CHKRFS"));
    String strPathway = GmCommonClass.parseNull((String) hmParam.get("PATHWAY"));
    String strTaggablePart = GmCommonClass.parseNull((String) hmParam.get("TAGGABLEPART"));
    String strPartAttributeVal =
        GmCommonClass.parseNull((String) hmParam.get("PART_ATTRIBUTE_VALES"));
    /* The following code is added for validating Master Product */
    String strMasterProd = GmCommonClass.parseNull((String) hmParam.get("MASTERPRODUCT"));
    String strpartCount;
    if (!strMasterProd.equals("")) {
      strMasterProd = strMasterProd.trim();
      strpartCount = validatePartNumber(strMasterProd);
      if (strpartCount.equals("0") || strPartNm.equals(strMasterProd)) {
        // throw new AppError("Please enter valid part number for <B>Master Product</B>", "", 'E');
        throw new AppError(AppError.APPLICATION, "10001");
      }
    }
    String strTagID = "";
    gmDBManager.setPrepareString("gm_pd_sav_partnumber", 27);

    gmDBManager.registerOutParameter(27, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPartNm);
    gmDBManager.setString(2, strProjId);
    gmDBManager.setString(3, strPartDesc);
    gmDBManager.setString(4, strProdFly);
    gmDBManager.setString(5, strProdMat);
    gmDBManager.setString(6, strClass);
    gmDBManager.setString(7, strActiveFl);
    gmDBManager.setString(8, strMeasDev);
    gmDBManager.setString(9, strMatSpec);
    gmDBManager.setString(10, strDrawNum);
    gmDBManager.setString(11, strRevNum);
    gmDBManager.setString(12, strUsername);
    gmDBManager.setString(13, strInspectRev);
    gmDBManager.setString(14, strTrackingImpl);
    gmDBManager.setString(15, strPartStatus);
    gmDBManager.setString(16, strReleaseForSale);
    gmDBManager.setString(17, strSupplyFlag);
    gmDBManager.setString(18, strSubAssemblyFlag);
    gmDBManager.setString(19, strSalesGroup);
    gmDBManager.setString(20, strInsertId);
    gmDBManager.setString(21, strUOM);
    gmDBManager.setString(22, strMinAccpRev);
    gmDBManager.setString(23, strRFS);
    gmDBManager.setString(24, strPartAttributeVal);
    gmDBManager.setString(25, strTaggablePart);
    gmDBManager.setString(26, strDetailDesc);
    gmDBManager.execute();

    hmPartInfo = GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(27)));
     
      // PMT-27473 : to fix the user request cancel error.
      // Sometimes comments log not saved issue fixed (before loop the arrayList to save the
      // comments).
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartNm, strLogReason, strUsername, "1218");
    }

    gmDBManager.commit();
    /**
     * 
     *PMT-32340 workorder revision dashboard
     * -------------------------------------------------------------------
     * This method used to send a email notification to purchasing team. 
     * If quality team modify any of the part attribute 
     *(Revision, product Class, Material specification, Drawing number) - 
     * to send the notification to purchasing team (all the open WO)
     * ---------------------------------------------------------------------
     */
     int intSize = hmPartInfo.size();
     if(intSize != 0){
         hmPartInfo.put("PARTNUM",strPartNm);
         hmPartInfo.put("PDESC",strPartDesc);
 	     hmPartInfo.put("USERID",strUsername);
 	     hmPartInfo.put("ACTION", "EMAIL");
         hmPartInfo.put("COMPANYINFO", GmCommonClass.parseNull((String) hmParam.get("companyInfo")));
         gmPartNumberServiceBean.processWORevisionUpdate(hmPartInfo);
     }
    // This method is used for the Group ,System and project visibility of the part to all
    // companies, that
    // are released for the particular part.
    gmPartNumberServiceBean.processPartVisibility(strPartNm, strUsername);
    
    //This below code is used to update the redis cache values when we are edit or add partnum
    if(!strPartNm.equals("")){
    	HashMap hmParams = new HashMap();
    	String strCompanyInfo ="{\"cmpid\":\""+getGmDataStoreVO().getCmpid()+"\",\"partyid\":\""+getGmDataStoreVO().getPartyid()+"\",\"plantid\":\""+getGmDataStoreVO().getPlantid()+"\"}";
    	hmParams.put("PART_NUM", strPartNm);
    	hmParams.put("USER_ID", strUserId);
    	hmParams.put("CMP_ID", getGmDataStoreVO().getCmpid());
    	hmParams.put("COMPANY_INFO",strCompanyInfo);
    	log.debug("JMS CALL-------------"); 
    	GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
    	gmKitMappingBean.UpdateParttoRedisKeyJMS(hmParams);
    }
    
  } // End of savePartNumber

  /**
   * saveDuplPartNum - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveDuplPartNum(String strPartNum, String strProjId, String strParentProjId,
      String strUsername) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();


    gmDBManager.setPrepareString("GM_SAVE_DUPL_PARTNUMBER", 5);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strProjId);
    gmDBManager.setString(3, strParentProjId);
    gmDBManager.setString(4, strUsername);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(5);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of saveDuplPartNum

  /**
   * loadEditPartNumTemp -
   */
  public HashMap loadEditPartNumTemp(String strPartNum) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    ArrayList alMatSpec = gmCommon.getCodeList("MATSP");

    hmReturn.put("MATSPEC", alMatSpec);

    ArrayList alReturn = new ArrayList();


    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT	C206_SUB_ASMBLY_ID PNUM, GET_CODE_NAME(C206_MATERIAL_SPEC) MSPEC, ");
    sbQuery.append(" C206_SUB_ASMBLY_DRAWING DRAW, C206_SUB_ASMBLY_DESC PDESC, ");
    sbQuery.append(" C206_SUB_ASMBLY_REV_NUM REV, C206_SUB_ASMBLY_SEQ_ID SEQID ");
    sbQuery.append(" FROM T206_SUB_ASSEMBLY  WHERE C205_PART_NUMBER_ID ='");
    sbQuery.append(strPartNum);
    sbQuery.append("' ORDER BY C206_SUB_ASMBLY_ID");

    log.debug(" Query to fetch sub assm part details  " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return hmReturn;
  } // End of loadEditPartNumTemp

  /**
   * saveEditPartNumTemp - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveEditPartNumTemp(String strPartNum, String strControlReq,
      String strInputString, String strUsername) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();



    gmDBManager.setPrepareString("GM_SAVE_EDIT_PARTNUMBER_TEMP", 5);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUsername);
    gmDBManager.setString(3, strPartNum);
    gmDBManager.setString(4, strControlReq);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(5);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of saveEditPartNumTemp

  /**
   * - This method will
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   */
  public ArrayList searchSubAssembly(String strPartNums, String strPartStatus) throws AppError {
    ArrayList alReturn = new ArrayList();
    strPartNums = GmCommonClass.getStringWithQuotes(strPartNums);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    /*
     * StringBuffer sbQuery = new StringBuffer(); sbQuery.append(" SELECT T206.C205_PART_NUMBER_ID
     * PARTNUM "); sbQuery.append(" , T205.C205_PART_NUM_DESC PARTDESC "); sbQuery.append(" ,
     * Get_Project_Name(T205.C202_PROJECT_ID) PROJECTNAME ");
     * sbQuery.append(" , T206.C206_SUB_ASMBLY_ID SUBASSEMID ");
     * sbQuery.append(" , T206.C206_SUB_ASMBLY_DESC SUBASSEMBDESC ");
     * sbQuery.append(" , T206.C206_SUB_ASMBLY_REV_NUM SUBASSEMREV "); sbQuery.append(" FROM
     * T206_SUB_ASSEMBLY T206, T205_PART_NUMBER T205 "); // sbQuery.append(" WHERE C701_VOID_FL IS
     * NULL "); if (strPartStatus.equals("rdPartNumber")) { sbQuery.append(" WHERE
     * T206.C205_PART_NUMBER_ID IN ('"); sbQuery.append(strPartNums); sbQuery.append("')"); }
     * 
     * else { sbQuery.append(" WHERE T206.C206_SUB_ASMBLY_ID IN ('"); sbQuery.append(strPartNums);
     * sbQuery.append("')"); }
     * 
     * sbQuery.append(" AND T205.C205_PART_NUMBER_ID = T206.C205_PART_NUMBER_ID (+) "); log.debug("
     * Query to get the searchSubAssembly List is " + sbQuery.toString() + "size of AList " +
     * alReturn.size()); alReturn = dbCon.queryMultipleRecords(sbQuery.toString());
     * log.debug(" done ");
     */
    String strFromPNumQuery = "";
    String strToPNumQuery = "";

    if (strPartStatus.equals("rdPartNumber")) {
      strFromPNumQuery = " T205A.C205_FROM_PART_NUMBER_ID ";
      strToPNumQuery = " T205A.C205_TO_PART_NUMBER_ID ";
    }

    else {
      strFromPNumQuery = " T205A.C205_TO_PART_NUMBER_ID ";
      strToPNumQuery = " T205A.C205_FROM_PART_NUMBER_ID ";
    }



    StringBuffer sbQuery = new StringBuffer();
    StringBuffer sbPartDescQuery = new StringBuffer();

    sbQuery.append(" SELECT ");
    sbQuery.append(strFromPNumQuery);
    sbQuery.append(" PARTNUM , GET_PARTNUM_DESC( ");
    sbQuery.append(strFromPNumQuery);
    sbQuery.append(")PARTDESC , GET_PROJECT_NAME(T205.C202_PROJECT_ID) PROJECTNAME ");
    sbQuery
        .append(" , T205.C205_PART_NUMBER_ID SUBASSEMID  ,  T205.C205_PART_NUM_DESC SUBASSEMBDESC , T205.C205_REV_NUM SUBASSEMREV ");
    sbQuery.append(" FROM T205A_PART_MAPPING T205A, T205_PART_NUMBER T205 ");
    sbQuery.append(" WHERE ");
    sbQuery.append(strFromPNumQuery);
    sbQuery.append("  IN ('");
    sbQuery.append(strPartNums);
    sbQuery.append("') AND ");
    sbQuery.append(strToPNumQuery);
    sbQuery.append("  = T205.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T205A.C205A_VOID_FL IS NULL ");   // added new column c205a_void_fl in t205a used to PMT-50777 - Create BOM and sub-component Mapping
    sbQuery.append(" ORDER BY ");
    sbQuery.append(strFromPNumQuery);

    log.debug(" Query to get the searchSubAssembly List is " + sbQuery.toString()
        + "size of AList " + alReturn.size());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alReturn;

  } // End of methodName

  /**
   * savePartNumMapping - This method will save the part number mapping from subasm to parent part
   * and vice versa
   * 
   * @param String strUsername
   * @param String strPassword
   * @return void
   * @exception AppError
   */
  public void savePartNumMapping(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PPNUM"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));


    ArrayList outList = null;
    String strTemp = "";
    HashMap hmReturn = new HashMap();
    int loopflag = 1;

    outList = null;

    if (!strInputString.equals("") && strInputString.length() > 4000) {

      outList = GmCommonClass.convertStringToList(strInputString, 4000, "|");
    } else {
      outList = new ArrayList();
      outList.add(strInputString);
    }
    if (outList != null) {

      for (Iterator itr = outList.iterator(); itr.hasNext();) {

        strTemp = (String) itr.next();
        gmDBManager.setPrepareString("GM_PD_SAV_PARTNUM_MAP", 5);

        gmDBManager.setString(1, strPartNum);
        gmDBManager.setString(2, strType);
        gmDBManager.setString(3, strTemp);
        gmDBManager.setString(4, strUserId);
        gmDBManager.setInt(5, loopflag);
        gmDBManager.execute();
        loopflag++;
      }
      gmDBManager.commit();
    }


  } // End of savePartNumMapping

  /**
   * fetchDemandGroupMap - This method will fetch the Group info for a particular demand sheet
   * 
   * @param hmParam - demandSheetId, Reference Type
   * @exception AppError
   */
  public HashMap loadPartDetail(String strPartNum) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmPartDetail = new HashMap();

    log.debug("strPartNum  is  " + strPartNum);
    RowSetDynaClass rdSetDetail = null;
    RowSetDynaClass rdVendorDetail = null;
    RowSetDynaClass rdSubPartDetail = null;
    RowSetDynaClass rdParentPartDetail = null;
    RowSetDynaClass rdPartGroup = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_all_detail", 7);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPartNum);
    // gmDBManager.setString(2, strStatus);

    gmDBManager.execute();

    hmPartDetail =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    rdSetDetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    rdVendorDetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    rdSubPartDetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
    rdParentPartDetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));
    rdPartGroup = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));

    log.debug("rdSetDetail size is  " + rdSetDetail.getRows().size());
    gmDBManager.close();

    hmReturn.put("PARTNUMDETAIL", hmPartDetail);
    hmReturn.put("RDSETDETAIL", rdSetDetail.getRows());
    hmReturn.put("RDVENDORDETAIL", rdVendorDetail.getRows());
    hmReturn.put("RDSUBPARTDETAIL", rdSubPartDetail.getRows());
    hmReturn.put("RDPARENTPARTDETAIL", rdParentPartDetail.getRows());
    hmReturn.put("RDPARTGROUP", rdPartGroup.getRows());

    return hmReturn;
  }

  /**
   * loadPartNumberDetails - This method will fetch PartNumber for the passed in partNumbers
   * 
   * @param hmParam - PartNumber Details
   * @exception AppError
   */
  public RowSetDynaClass loadPartNumberDetails(String strPartNum, String strSubCompFlg)
      throws AppError {
    ArrayList alTempPartNumList = new ArrayList();
    String strPartNumInputString = "";
    RowSetDynaClass rdPartNumberDetail = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    if (strPartNum != null && strPartNum.length() > 500) {
      alTempPartNumList = GmCommonClass.convertStringToList(strPartNum, 500, "|");
    } else {
      alTempPartNumList = new ArrayList();
      alTempPartNumList.add(strPartNum);
    }
    if (alTempPartNumList != null) {
      for (Iterator itr = alTempPartNumList.iterator(); itr.hasNext();) {
        strPartNumInputString = (String) itr.next();
        if (strPartNumInputString.endsWith("|")) {
          strPartNumInputString =
              strPartNumInputString.substring(0, strPartNumInputString.length() - 1);
        }
        log.debug(" Into the loadPartNumberDetails Method " + "PartNumber:" + strPartNum
            + " SubComp Flag: " + strSubCompFlg);
        gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_detail", 3);
        gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

        gmDBManager.setString(1, strPartNumInputString);
        gmDBManager.setString(2, strSubCompFlg);

        gmDBManager.execute();

        rdPartNumberDetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
      }
    }
    gmDBManager.close();

    return rdPartNumberDetail;
  }

  /**
   * This method will save the multipProject Part flag for all the partNumbers that is passed in.
   * 
   * @param strInput - Has the list of PartNumber and the MultiProject part flag that has to be
   *        updated.
   * @param strUserID - The user who is doing the save operation.
   * @exception AppError
   */
  public void saveMultiProjectPart(String strInput, String strUserID) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_sav_multiprojectpart", 2);

    gmDBManager.setString(1, strInput);
    gmDBManager.setString(2, strUserID);

    gmDBManager.execute();
    gmDBManager.commit();
  }

  public String savePartAttrribute(HashMap hmParam) throws AppError {
    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strPartAttributeType = GmCommonClass.parseNull((String) hmParam.get("ATTRIBUTETYPE"));
    String strPartAttributeValue = GmCommonClass.parseNull((String) hmParam.get("ATTRIBUTEVALUE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_sav_part_attribute", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strPartAttributeType);
    gmDBManager.setString(3, strPartAttributeValue);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    String strAttribteId = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.commit();
    return strAttribteId;
  }

  public String validatePartNumber(String strPartNum) throws AppError {

    String strPartCount = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_validate_part_number", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.execute();
    strPartCount = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strPartCount;
  }


  /**
   * loadTransactoinPartNumber - This method will fetch PartNumber for the passed in Transactions
   * (CN)
   * 
   * @param hmParam - PartNumber Details
   * @return String - comma separated part number.
   * @exception AppError
   */
  public String loadTransactoinPartNumber(HashMap hmParam) throws AppError {
    RowSetDynaClass rdPartNumberDetail = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPartNumbers = "";
    String strConsignemntID = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));

    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_trans_parts", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strConsignemntID);

    gmDBManager.execute();
    strPartNumbers = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();

    return strPartNumbers;
  }

  /**
   * fetchPartListByAttribute - To fetch list part detail based on attribute type and value.
   * 
   * @param String strType,strValue
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchPartListByAttribute(String strType, String strValue) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_list_by_attribute", 3);
    gmDBManager.setString(1, strType);
    gmDBManager.setString(2, strValue);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchUDIParameterDtls - This method used to fetch the UDI Parameter details
   * 
   * @param String strPartNumber
   * @return HashMap
   * 
   * @exception AppError
   */
  public HashMap fetchUDIParameterDtls(String strPartNumber) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_qa_udi_param", 2);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * This metod used to save the Label Parameter details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */

  public String savePartNumParams(HashMap hmParam) throws AppError {
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOGVAL"));
    String strLabelAttr = GmCommonClass.parseNull((String) hmParam.get("HLABELATTRIBUTE"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_sav_part_params", 3);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strLabelAttr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    if (!strLog.equals("")) {
      gmCom.saveLog(gmDBManager, strPartNumber, strLog, strUserId, "400547");
    }

    gmDBManager.commit();
    return strPartNumber;
  }

  /**
   * This method used to fetch Label param values.
   * 
   * @param strPartNumber
   * @return
   * @throws AppError
   */

  public HashMap fetchLBLParameterDtls(String strPartNumber) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_lbl_param", 2);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchPartDhrRequirementDtls - To fetch part number DHR Requirements details.
   * 
   * @param HashMap
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchPartDhrRequirementDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strPartNumber = "";
    strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));

    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_dhr_dtls", 2);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * saveDhrRequirementDtls - To save the part number DHR Requirements details.
   * 
   * @param HashMap
   * @return void
   * @throws AppError
   */
  public void saveDhrRequirementDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strMaterialCert = GmCommonClass.parseZero((String) hmParam.get("MATERIALCERT"));
    String strHandnessCert = GmCommonClass.parseZero((String) hmParam.get("HANDNESSCERT"));
    String strPrimaryPack = GmCommonClass.parseZero((String) hmParam.get("PRIMARYPACK"));
    String strSecondaryPack = GmCommonClass.parseZero((String) hmParam.get("SECONDARYPACK"));
    String strComplianceCert = GmCommonClass.parseZero((String) hmParam.get("COMPLIANCECERT"));
    String strInputString = GmCommonClass.parseZero((String) hmParam.get("INPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_sav_part_dhr_req", 8);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strMaterialCert);
    gmDBManager.setString(3, strHandnessCert);
    gmDBManager.setInt(4, Integer.parseInt(strPrimaryPack));
    gmDBManager.setInt(5, Integer.parseInt(strSecondaryPack));
    gmDBManager.setString(6, strComplianceCert);
    gmDBManager.setString(7, strInputString);
    gmDBManager.setString(8, strUserId);
    gmDBManager.execute();
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartNumber, strLogReason, strUserId, "1291");// 1291-->Part
                                                                                        // Number
                                                                                        // -DHR Req.
                                                                                        // Comments
    }
    gmDBManager.commit();
  }

  /**
   * saveDUNSInformation - To save the part number UDI DUNS details.
   * 
   * @param HashMap
   * @return void
   * @throws AppError
   */
  public void saveDUNSInformation(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("OVERRIDECOMPANYNM"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_pd_sav_udi.gm_sav_qa_udi_duns_information", 3);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.setString(2, strCompanyId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchPartMaterialType - To fetch part material type details.
   * 
   * @param String
   * @return String
   * @throws AppError
   */
  public static String fetchPartMaterialType(String strPartNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    String strPartType = "";

    gmDBManager.setFunctionString("get_partnum_material_type", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.execute();
    strPartType = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strPartType;
  }

  /**
   * fetchContractProcessingClient - To fetch ContractProcessingClient details.
   * 
   * @param
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchContractProcessingClient() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_report_cpc_account", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(1)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchRegulatoryParameterDtls - This method used to fetch the Regulatory Parameter details
   * 
   * @param String strPartNumber
   * @return HashMap
   * 
   * @exception AppError
   */
  public HashMap fetchRegulatoryParameterDtls(String strPartNumber) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_qa_regulatory_param", 2);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }


  /**
   * fetchRegulatoryReportDtls - This method will fetch the Regulatory details
   * 
   * @param HashMap hmParams
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchRegulatoryReportDtls(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
    String strRegulatoryPartNum =
        GmCommonClass.parseNull((String) hmParam.get("REGULATORYPARTNUM"));
    String strSubmissionNo = GmCommonClass.parseNull((String) hmParam.get("SUBMISSIONNO"));
    String strPartDesc = GmCommonClass.parseNull((String) hmParam.get("REGULATORYPARTDESC"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    sbQuery
        .append("SELECT t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc, v205g.c205d_Brand_Name Brand_Name");
    sbQuery
        .append(", v205g.c205d_US_Reg_Classification US_Reg_Classification_id, get_code_name (v205g.c205d_US_Reg_Classification)");
    sbQuery
        .append(" US_Reg_Classification_Nm, v205g.c205d_US_Reg_Pathway US_Reg_Pathway_id, get_code_name (v205g.c205d_US_Reg_Pathway)");
    sbQuery
        .append(" US_Reg_Pathway_nm, v205g.c205d_FDA_Sub_No1 FDA_Sub_No1, v205g.c205d_FDA_NTF_Reported FDA_NTF_Reported");
    sbQuery
        .append(" , v205g.c205d_PMA_Sup_No1 PMA_Sup_No1, DECODE (get_rfs_country (t205.c205_part_number_id, '80120'), NULL, 'N', 'Y')");
    sbQuery
        .append(" rfs_us, DECODE (get_rfs_country (t205.c205_part_number_id, '80121'), NULL, 'N', 'Y') rfs_eu, t205.c205_insert_id insert_id");
    sbQuery.append(" FROM v205g_part_regulatory_param v205g, t205_part_number t205");
    sbQuery.append(" WHERE t205.c205_part_number_id = v205g.c205_part_number_id");
    if (!strRegulatoryPartNum.equals("")) {
    	sbQuery.append(" AND REGEXP_LIKE(t205.c205_part_number_id,REGEXP_REPLACE('" + strRegulatoryPartNum + "','[+]','\\+'))");
    }
    // Searching Option using Part Description
    if (!strPartDesc.equals("")) {
      sbQuery.append(" AND UPPER (t205.C205_PART_NUM_DESC) LIKE'%");
      sbQuery.append(strPartDesc.toUpperCase());
      sbQuery.append("%' ");
    }
    if (!strProjectId.equals("0")) {
      sbQuery.append(" AND t205.c202_project_id    = '" + strProjectId + "' ");
    }
    if (!strSubmissionNo.equals("")) {
      sbQuery.append(" AND REGEXP_LIKE(v205g.c205d_FDA_Sub_No1,UPPER('" + strSubmissionNo + "'))");
    }
    sbQuery.append(" ORDER BY pnum");
    log.debug(" Query is " + sbQuery.toString());

    gmDBManager.setPrepareString(sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecord();
    return alResult;


  }

  /**
   * savePartAttrributeBulk - This method will save the Part Attribute details
   * 
   * @param HashMap hmParam
   * @return hmReturn
   * @exception
   */

  public HashMap savePartAttrributeBulk(HashMap hmParam) throws AppError {

    HashMap hmReturn = new HashMap();

    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    String strInvalidParts = "";
    String strValidParts = "";
    String struserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAttributeType = GmCommonClass.parseNull((String) hmParam.get("ATTRIBUTETYPE"));
    String strAttributeValue = GmCommonClass.parseNull((String) hmParam.get("ATTRIBUTEVALUE"));
    String strAppfrAllcmp = GmCommonClass.parseNull((String) hmParam.get("CHKALLCOMPANY"));

    if (strAppfrAllcmp.equals("on")) {
      strAppfrAllcmp = "Y";
    }

    strAttributeValue = strAttributeValue.replaceAll("\t", "");
    strAttributeValue = strAttributeValue.replaceAll("\r\n", "|");
    strAttributeValue = strAttributeValue.concat("|");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_sav_part_attribute_bulk", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
    gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
    gmDBManager.setString(1, strAttributeType);
    gmDBManager.setString(2, strAttributeValue);
    gmDBManager.setString(3, strAppfrAllcmp);
    gmDBManager.setString(4, struserId);
    gmDBManager.execute();


    strValidParts = GmCommonClass.parseNull(gmDBManager.getString(5));
    strInvalidParts = GmCommonClass.parseNull(gmDBManager.getString(6));


    gmDBManager.commit();

    hmReturn.put("VALIDPARTS", strValidParts);
    hmReturn.put("INVALIDPARTS", strInvalidParts);
    return hmReturn;
  }

  /**
   * fetchPartAttribute - This method will fetch the Part Attribute details
   * 
   * @param HashMap hmParam
   * @return alResult
   * @exception
   * 
   */

  public ArrayList fetchPartAttribute(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();
    String strAttributeType = GmCommonClass.parseNull((String) hmParam.get("ATTRIBUTETYPE"));
    String strParts = GmCommonClass.parseNull((String) hmParam.get("PARTNUMS"));
    String strSetId = GmCommonClass.parseZero((String) hmParam.get("SETID"));
    String strAppfrAllcmp = GmCommonClass.parseNull((String) hmParam.get("CHKALLCOMPANY"));
    String strExcldType = GmCommonClass.parseNull((String) hmParam.get("EXCLDTYPE"));
    String strPartFl = GmCommonClass.parseNull((String) hmParam.get("PARTFL"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_part_attribute", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAttributeType);
    gmDBManager.setString(2, strParts);
    gmDBManager.setString(3, strSetId);
    gmDBManager.setString(4, strAppfrAllcmp);
    gmDBManager.setString(5, strPartFl);
    gmDBManager.setString(6, strExcldType);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(7)));
    gmDBManager.close();
    return alResult;


  }

  /**
   * fchSerialNumVisibility - To check whether the part number is used in any trans If used, then
   * the Serial Number Needed and Lot Tracking Needed check box should be disabled
   * 
   * @param String
   * @return HashMap
   * @throws AppError
   */
  public HashMap fchSerialNumVisibility(String strPartNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strIsSerialChkEnable = "";
    String strIsLotTrckChkEnable = "";
    HashMap hmChkEnableVal = new HashMap();

    gmDBManager.setPrepareString("gm_pkg_pd_partnumber.gm_fch_checkbox_status", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.execute();
    strIsSerialChkEnable = GmCommonClass.parseNull(gmDBManager.getString(2));
    strIsLotTrckChkEnable = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    hmChkEnableVal.put("SERIALCHKFL", strIsSerialChkEnable);
    hmChkEnableVal.put("LOTTRACKCHKFL", strIsLotTrckChkEnable);

    return hmChkEnableVal;
  }

}

// end of class GmPartNumBean


