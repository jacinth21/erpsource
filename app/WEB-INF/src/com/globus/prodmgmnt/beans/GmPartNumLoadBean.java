package com.globus.prodmgmnt.beans;


import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

import com.globus.common.beans.GmLogger;

public class GmPartNumLoadBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	public void bulkUploadPartDetails(String strFileName) {
//		HashMap[] hmArray;
		GmPartNumBean gmPartNumBean = new GmPartNumBean();
		ArrayList alPartList = new ArrayList();
		
		try {
			alPartList = readFile(strFileName);
				int size = alPartList.size();
				//size = 2;
				/*log.debug(" list values "+alPartList);
			CSVParser csvParser = new CSVParser();
			hmArray = csvParser.parse(strFileName, ',');
*/
				log.debug(" ArrayList values are " +alPartList);
			for (int i = 0; i < size; i++) {
				gmPartNumBean.savePartNumber((HashMap)alPartList.get(i), "303011");
			}
		}
		catch (Exception exp) {
			exp.printStackTrace();
		}
	}
	
	
	public ArrayList readFile(String infile) throws IOException {
		String[] dataarr;

		ArrayList alHeader = new ArrayList();
		ArrayList alData = new ArrayList();
		
		
		/*bufRdr = new BufferedReader(new FileReader(file));
		data = bufRdr.readLine();
		String[] headerarr = data.split(",");
		*/
		 CSVReader reader = new CSVReader(new FileReader(infile));
		 String[] headerarr  = reader.readNext();
		 
		
		for (int i = 0; i < headerarr.length; i++){
			alHeader.add(headerarr[i]);
		}
		log.debug(" header " +alHeader);
		
		 while ((dataarr = reader.readNext()) != null){
			
			LinkedHashMap hmRow = new LinkedHashMap();
			List alRowData = new ArrayList();
			alRowData = Arrays.asList(dataarr);
			log.debug(" ArrayList val " + alRowData);
			
			for (int i=0;i < alRowData.size(); i++){
				hmRow.put(alHeader.get(i), alRowData.get(i));
			}	
		//	log.debug(" Added row " +hmRow );
			alData.add(hmRow);
		}

		return alData;
	}

}
