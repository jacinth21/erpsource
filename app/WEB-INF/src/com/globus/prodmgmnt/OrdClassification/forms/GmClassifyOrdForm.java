package com.globus.prodmgmnt.OrdClassification.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmClassifyOrdForm extends GmCommonForm {

private String systemID = "";
private String systemNM = "";
private String strXMLGrid = "";
private String status = "";	 
private String statusNM = "";	  
private String publishedBy = "";	
private String inputString = "";
private String publishedNM = "";	
private String initiatedNM = "";
private String fwdGroupType = "";
private String fwdRuleTypeID = "";
private String historyFL = "";
private String hiddenToken = "";
private String editAccessFl = "";
private String publishAccessFl = "";

private Date publishedDate = null;
private Date initiatedDate = null;

private ArrayList alSystem = new ArrayList();
private HashMap hmReturn = new HashMap();


public String getEditAccessFl() {
	return editAccessFl;
}
public void setEditAccessFl(String editAccessFl) {
	this.editAccessFl = editAccessFl;
}
public String getPublishAccessFl() {
	return publishAccessFl;
}
public void setPublishAccessFl(String publishAccessFl) {
	this.publishAccessFl = publishAccessFl;
}
public String getFwdRuleTypeID() {
	return fwdRuleTypeID;
}
public void setFwdRuleTypeID(String fwdRuleTypeID) {
	this.fwdRuleTypeID = fwdRuleTypeID;
}
public String getHiddenToken() {
	return hiddenToken;
}
public void setHiddenToken(String hiddenToken) {
	this.hiddenToken = hiddenToken;
}
public HashMap getHmReturn() {
	return hmReturn;
}
public void setHmReturn(HashMap hmReturn) {
	this.hmReturn = hmReturn;
}

public String getHistoryFL() {
	return historyFL;
}
public void setHistoryFL(String historyFL) {
	this.historyFL = historyFL;
}
public String getFwdGroupType() {
	return fwdGroupType;
}
public void setFwdGroupType(String fwdGroupType) {
	this.fwdGroupType = fwdGroupType;
}
public String getPublishedNM() {
	return publishedNM;
}
public void setPublishedNM(String publishedNM) {
	this.publishedNM = publishedNM;
}
public String getInitiatedNM() {
	return initiatedNM;
}
public void setInitiatedNM(String initiatedNM) {
	this.initiatedNM = initiatedNM;
}
public String getStatusNM() {
	return statusNM;
}
public void setStatusNM(String statusNM) {
	this.statusNM = statusNM;
}
public String getSystemNM() {
	return systemNM;
}
public void setSystemNM(String systemNM) {
	this.systemNM = systemNM;
}
public Date getInitiatedDate() {
	return initiatedDate;
}
public void setInitiatedDate(Date initiatedDate) {
	this.initiatedDate = initiatedDate;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getPublishedBy() {
	return publishedBy;
}
public void setPublishedBy(String publishedBy) {
	this.publishedBy = publishedBy;
}
public String getInputString() {
	return inputString;
}
public void setInputString(String inputString) {
	this.inputString = inputString;
}
public String getSystemID() {
	return systemID;
}
public void setSystemID(String systemID) {
	this.systemID = systemID;
}
public String getStrXMLGrid() {
	return strXMLGrid;
}
public void setStrXMLGrid(String strXMLGrid) {
	this.strXMLGrid = strXMLGrid;
}
public Date getPublishedDate() {
	return publishedDate;
}
public void setPublishedDate(Date publishedDate) {
	this.publishedDate = publishedDate;
}
public ArrayList getAlSystem() {
	return alSystem;
}
public void setAlSystem(ArrayList alSystem) {
	this.alSystem = alSystem;
}

}
