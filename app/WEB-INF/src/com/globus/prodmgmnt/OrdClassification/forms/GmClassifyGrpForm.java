package com.globus.prodmgmnt.OrdClassification.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmClassifyGrpForm extends GmCommonForm{
	
	private String systemID = "";
	private String systemNM= "";
	private String sysRuleTypeID = "";
	private String curGroupType = "";
	private String fwdRuleTypeID = "";
	private String fwdGroupType = "";
	private String screenTitle = "";
	private String hinputStr = "";
	private String strSelectedGrpType = "";
	
	private ArrayList alSetsGroup = new ArrayList();
	private ArrayList alSysRuleGroup = new ArrayList();
	private ArrayList alSysAllRuleType = new ArrayList();
	
	
	public String getFwdRuleTypeID() {
		return fwdRuleTypeID;
	}
	public void setFwdRuleTypeID(String fwdRuleTypeID) {
		this.fwdRuleTypeID = fwdRuleTypeID;
	}
	public ArrayList getAlSysAllRuleType() {
		return alSysAllRuleType;
	}
	public void setAlSysAllRuleType(ArrayList alSysAllRuleType) {
		this.alSysAllRuleType = alSysAllRuleType;
	}
	public String getSystemNM() {
		return systemNM;
	}
	public void setSystemNM(String systemNM) {
		this.systemNM = systemNM;
	}
	public String getSysRuleTypeID() {
		return sysRuleTypeID;
	}
	public void setSysRuleTypeID(String sysRuleTypeID) {
		this.sysRuleTypeID = sysRuleTypeID;
	}
	public String getHinputStr() {
		return hinputStr;
	}
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}
	public String getSystemID() {
		return systemID;
	}
	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}
	public String getScreenTitle() {
		return screenTitle;
	}
	public void setScreenTitle(String screenTitle) {
		this.screenTitle = screenTitle;
	}
	public String getCurGroupType() {
		return curGroupType;
	}
	public void setCurGroupType(String curGroupType) {
		this.curGroupType = curGroupType;
	}
	public String getFwdGroupType() {
		return fwdGroupType;
	}
	public void setFwdGroupType(String fwdGroupType) {
		this.fwdGroupType = fwdGroupType;
	}
	public String getStrSelectedGrpType() {
		return strSelectedGrpType;
	}
	public void setStrSelectedGrpType(String strSelectedGrpType) {
		this.strSelectedGrpType = strSelectedGrpType;
	}
	public ArrayList getAlSetsGroup() {
		return alSetsGroup;
	}
	public void setAlSetsGroup(ArrayList alSetsGroup) {
		this.alSetsGroup = alSetsGroup;
	}
	public ArrayList getAlSysRuleGroup() {
		return alSysRuleGroup;
	}
	public void setAlSysRuleGroup(ArrayList alSysRuleGroup) {
		this.alSysRuleGroup = alSysRuleGroup;
	}
	
}
