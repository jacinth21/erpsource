package com.globus.prodmgmnt.OrdClassification.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.OrdClassification.beans.GmClassifyOrdBean;
import com.globus.prodmgmnt.OrdClassification.forms.GmClassifyOrdForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmClassifyOrdAction extends GmAction{

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute (ActionMapping mapping , ActionForm form , HttpServletRequest request, HttpServletResponse response)throws Exception{
		log.debug("======GmClassifyOrdAction======");
		HttpSession session = request.getSession(false);
		ArrayList alGrpTypeList = new ArrayList();
		ArrayList alResult = new ArrayList();
		ArrayList alRuleType = new ArrayList();
		ArrayList alLinkedRuleType = new ArrayList();
		ArrayList alFinal = new ArrayList();
		
		HashMap hmTemp = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmResult = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmAccess = new HashMap();
		
		String strForward = "GmClassifyOrd";
		String strXMLGrid = "";
		String strSystemID = "";
		String strOpt = "";
		String strPath = "";
		String strSelectedGrpType = ""; 
		String fwdGroupType = ""; 
		String strSysRuleTypeID = "";
		String strStatus= "";
		String strFwdRuleTypeID = "";
		String strAccessFl = "";
		String strPartyId = "";
		
		GmProjectBean gmProjectBean = new GmProjectBean();
		GmClassifyOrdBean gmClassifyOrdBean = new GmClassifyOrdBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		
		GmClassifyOrdForm gmClassifyOrdForm = (GmClassifyOrdForm) form;
		gmClassifyOrdForm.loadSessionParameters(request);
		
		strSystemID = gmClassifyOrdForm.getSystemID();
		strOpt = gmClassifyOrdForm.getStrOpt();
		fwdGroupType = gmClassifyOrdForm.getFwdGroupType();
		strFwdRuleTypeID = gmClassifyOrdForm.getFwdRuleTypeID();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmClassifyOrdForm); 
		strOpt = validateRequest(hmParam, session); 
		if(strOpt.equals("save")|| strOpt.equals("next")||strOpt.equals("publish")){
			if(strOpt.equals("publish")){
				hmParam.put("PUBLISHEDBY", gmClassifyOrdForm.getUserId());
				hmParam.put("PUBLISHEDDATE", GmCommonClass.getCurrentDate(gmClassifyOrdForm.getApplnDateFmt()));
			}
			gmClassifyOrdBean.saveSystemRule(hmParam);
			if(strOpt.equals("next")){
				ActionForward actionForward = mapping.findForward("savenext"); 
	        	ActionForward nextForward = new ActionForward(actionForward);
	        	
	        	hmReturn = GmCommonClass.parseNullHashMap((HashMap)gmClassifyOrdBean.fetchSystemRuleTypeByGroupType(hmParam));
	        	strSelectedGrpType = GmCommonClass.parseNull((String)hmReturn.get("STRSELECTEDGRPTYPE"));
	            
	        	strPath ="/gmClassifyGrp.do?fwdGroupType="+fwdGroupType+"&strSelectedGrpType="+strSelectedGrpType+"&systemID="+strSystemID+"&fwdRuleTypeID="+strFwdRuleTypeID;
	            nextForward.setPath(strPath);
	            nextForward.setRedirect(true);
	            return nextForward;
			}else if(strOpt.equals("save")){
				strOpt = "edit";
			}
		}
		
		
		if(strOpt.equals("load") || strOpt.equals("reload") || strOpt.equals("edit")){
			hmResult = GmCommonClass.parseNullHashMap((HashMap)gmClassifyOrdBean.fetchSystemRule(strSystemID));
			strStatus = GmCommonClass.parseNull((String)hmResult.get("STATUS"));
			if(!strStatus.equals("") && strOpt.equals("reload")){
				strOpt = "publish";
			}else{
				hmResult = GmCommonClass.parseNullHashMap((HashMap)gmClassifyOrdBean.fetchSystemRuleType(strSystemID));
				alResult = GmCommonClass.parseNullArrayList(linkRuleGroups(hmResult));
				
				hmTemp.put("alResult", alResult);
				alGrpTypeList =  GmCommonClass.parseNullArrayList(gmClassifyOrdBean.getCodeList("GRPTYP"));
				hmTemp.put("alGrpTypeList", alGrpTypeList);
				String strSessCompanyLocale = GmCommonClass.parseNull((String)request.getSession().getAttribute("strSessCompanyLocale"));
				hmTemp.put("strSessCompanyLocale", strSessCompanyLocale);
				strXMLGrid = GmCommonClass.parseNull(getXmlGridData(hmTemp));
				gmClassifyOrdForm.setStrXMLGrid(strXMLGrid);
				gmClassifyOrdForm.setAlSystem(GmCommonClass.parseNullArrayList(gmProjectBean.loadSetMap("GROUPPARTMAPPING")));
				strOpt = (!strOpt.equals("load"))?"edit":strOpt;
			}
		}
		if(strOpt.equals("confirm") || strOpt.equals("publish")){
			hmResult = GmCommonClass.parseNullHashMap((HashMap)gmClassifyOrdBean.fetchSystemRule(strSystemID));
			gmClassifyOrdForm = (GmClassifyOrdForm)GmCommonClass.getFormFromHashMap(gmClassifyOrdForm, hmResult);
			alRuleType = GmCommonClass.parseNullArrayList((ArrayList)gmClassifyOrdBean.fetchSystemAllRuleType(strSystemID));
			for(int i=0;i<alRuleType.size();i++){
				HashMap hmLoop = new HashMap();
				ArrayList alLoop = new ArrayList();
				hmLoop = (HashMap)alRuleType.get(i);
				strSysRuleTypeID = GmCommonClass.parseNull((String)hmLoop.get("SYSRULETYPEID"));
				alLoop=GmCommonClass.parseNullArrayList(gmClassifyOrdBean.fetchSystemRuleGroup(strSysRuleTypeID));
				hmLoop.put("ALRULEGRP", alLoop);
				alFinal.add(hmLoop);
			}
			
			hmReturn.put("hmSysRule", hmResult);
			hmReturn.put("ALRESULT", alFinal);
			gmClassifyOrdForm.setHmReturn(hmReturn);
			strForward = "GmPublishSys";
		}
		strPartyId = gmClassifyOrdForm.getSessPartyId();
 	    hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"DO_CLASSIFY_EDIT"));
 	    strAccessFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
 	    gmClassifyOrdForm.setEditAccessFl(strAccessFl);
 	    hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"DO_CLASSIFY_PUBLISH"));
	    strAccessFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
	    gmClassifyOrdForm.setPublishAccessFl(strAccessFl);
		gmClassifyOrdForm.setStrOpt(strOpt);
		return mapping.findForward(strForward);	
	}

	private String getXmlGridData(HashMap hmTemplate) throws AppError {
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		ArrayList alResult = new ArrayList();
		ArrayList alGrpTypeList = new ArrayList();
		alResult = GmCommonClass.parseNullArrayList((ArrayList)hmTemplate.get("alResult"));
		alGrpTypeList = GmCommonClass.parseNullArrayList((ArrayList)hmTemplate.get("alGrpTypeList"));
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmTemplate.get("STRSESSCOMPANYLOCALE"));
		
		templateUtil.setDataList("alGrpTypeList",alGrpTypeList);
		templateUtil.setDataList("alResult",alResult);
		templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
		        "properties.labels.prodmgmnt.OrdClassification.GmClassifyOrd", strSessCompanyLocale));
		templateUtil.setTemplateName("GmClassifyOrd.vm");
		templateUtil.setTemplateSubDir("prodmgmnt/OrdClassification/templates");
		return templateUtil.generateOutput(); 
	}
	
	private ArrayList linkRuleGroups(HashMap hmReturn) throws AppError {
		ArrayList alResult = new ArrayList();
		ArrayList alGrpType = new ArrayList();
		ArrayList alLinkGrpType = new ArrayList();
		String strLinkID = "";
		String strRuleTypeID = "";
		
		alGrpType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("RULETYPELIST"));
		alLinkGrpType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("LINKEDRULETYPELIST"));
		log.debug("=====alGrpType======"+alGrpType);
		log.debug("=====alLinkGrpType======"+alLinkGrpType);
		for(int i = 0;i<alLinkGrpType.size();i++){
			HashMap hmTempLinked = new HashMap();
			hmTempLinked = GmCommonClass.parseNullHashMap((HashMap)alLinkGrpType.get(i));
			strLinkID = GmCommonClass.parseNull((String)hmTempLinked.get("LINKEDRULETYPEID"));
			for(int j=0;j<alGrpType.size();j++){
				HashMap hmTempRule = new HashMap();
				hmTempRule = GmCommonClass.parseNullHashMap((HashMap)alGrpType.get(j));
				strRuleTypeID = GmCommonClass.parseNull((String)hmTempRule.get("SYSRULETYPEID"));
				if(strLinkID.equals(strRuleTypeID)){
					if(hmTempRule.containsKey("LSYSRULETYPEID")){
						hmTempRule.put("LSYSRULETYPEID2", GmCommonClass.parseNull((String)hmTempLinked.get("LSYSRULETYPEID")));
						hmTempRule.put("LGRPTYPEID2", GmCommonClass.parseNull((String)hmTempLinked.get("LGRPTYPEID")));
					}else{
						hmTempRule.putAll(hmTempLinked);
					}
					alGrpType.set(j,hmTempRule);
					break;
				}
			}
		}
		log.debug("=====Final Result======"+alGrpType);
		return alGrpType; 
	}
	public String validateRequest(HashMap hmParam,HttpSession session)
	{
		String strToken= GmCommonClass.parseNull((String) session.getAttribute("Token"));
		String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		String strHiddenToken = GmCommonClass.parseNull((String) hmParam.get("HIDDENTOKEN"));
		log.debug("session token ="+strToken);
		log.debug("strHiddenToken token ="+strHiddenToken);
		UUID uniqueKey = UUID.randomUUID(); 
		if(!strToken.equals("") && !strToken.equals(strHiddenToken)){
			if(strOpt.equals("save")){
				strOpt = "edit";
			}
		}
		session.setAttribute("Token",uniqueKey.toString());
		return strOpt;
	}
}
