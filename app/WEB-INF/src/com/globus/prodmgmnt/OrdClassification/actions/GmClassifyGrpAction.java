package com.globus.prodmgmnt.OrdClassification.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.OrdClassification.beans.GmClassifyOrdBean;
import com.globus.prodmgmnt.OrdClassification.forms.GmClassifyGrpForm;

public class GmClassifyGrpAction extends GmAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute (ActionMapping mapping , ActionForm form , HttpServletRequest request, HttpServletResponse response)throws Exception{
		
		String strForward = "GmClassifyGrp"; 
		String strCurGroupType = ""; 
		String strFwdGroupType = "";
		String strFwdRuleTypeID = "";
		String strOpt = "";
		String strSysRuleTypeID = "";
		String strPath = "";
		String strSystemID = "";
		
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		
		ArrayList alSetsGroup = new ArrayList();
		ArrayList alSysRuleGroup = new ArrayList();
		ArrayList alSysAllRuleType = new ArrayList();  
		
		GmClassifyOrdBean gmClassifyOrdBean = new GmClassifyOrdBean();
		
		GmClassifyGrpForm gmClassifyGrpForm= (GmClassifyGrpForm) form;
		gmClassifyGrpForm.loadSessionParameters(request);
		
		strFwdGroupType = gmClassifyGrpForm.getFwdGroupType();
		strCurGroupType = gmClassifyGrpForm.getCurGroupType();
		strFwdRuleTypeID = gmClassifyGrpForm.getFwdRuleTypeID();
		
		strOpt = gmClassifyGrpForm.getStrOpt();
		log.debug("====strFwdGroupType======="+strFwdGroupType);
		hmParam = GmCommonClass.getHashMapFromForm(gmClassifyGrpForm); 
		strSystemID = gmClassifyGrpForm.getSystemID();
		if(strOpt.equals("save") || strOpt.equals("next") || strOpt.equals("confirm"))
		{
			gmClassifyOrdBean.saveSystemRuleGroup(hmParam);
		}
		if(strOpt.equals("confirm")){
			ActionForward actionForward = mapping.findForward("ConfirmPage"); 
        	ActionForward nextForward = new ActionForward(actionForward);
        	strPath ="/gmClassifyOrd.do?strOpt=confirm&systemID="+strSystemID+"&fwdGroupType="+strCurGroupType+"&fwdRuleTypeID="+strFwdRuleTypeID;
            nextForward.setPath(strPath);
            nextForward.setRedirect(true);
            return nextForward;
		}
		
		hmReturn= GmCommonClass.parseNullHashMap(gmClassifyOrdBean.fetchSystemRuleTypeByGroupType(hmParam));
		gmClassifyGrpForm = (GmClassifyGrpForm) GmCommonClass.getFormFromHashMap(gmClassifyGrpForm, hmReturn);
		strSysRuleTypeID = gmClassifyGrpForm.getSysRuleTypeID();
		alSysRuleGroup =GmCommonClass.parseNullArrayList(gmClassifyOrdBean.fetchSystemRuleGroup(strSysRuleTypeID));
		alSetsGroup = GmCommonClass.parseNullArrayList(gmClassifyOrdBean.fetchSetsMapGroup(hmParam));
		alSysAllRuleType = GmCommonClass.parseNullArrayList(gmClassifyOrdBean.fetchSystemAllRuleType(strSystemID));
		gmClassifyGrpForm.setAlSysRuleGroup(alSysRuleGroup);
		gmClassifyGrpForm.setAlSetsGroup(alSetsGroup);
		gmClassifyGrpForm.setAlSysAllRuleType(alSysAllRuleType);
		return mapping.findForward(strForward);
	}	
}
