package com.globus.prodmgmnt.OrdClassification.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmClassifyOrdBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

public GmClassifyOrdBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	  public GmClassifyOrdBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	public HashMap fetchSystemRule(String strSystemID) throws AppError{
		log.debug("========strSystemID========="+strSystemID);
		ArrayList alRuleType = new ArrayList();
		ArrayList alLinkedRuleType = new ArrayList();
		HashMap hmReturn = new HashMap();
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_rpt.gm_fch_system_rule",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSystemID);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;
	}
	
	public HashMap fetchSystemRuleType(String strSystemID) throws AppError{
		log.debug("========strSystemID========="+strSystemID);
		ArrayList alRuleType = new ArrayList();
		ArrayList alLinkedRuleType = new ArrayList();
		HashMap hmReturn = new HashMap();
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_rpt.gm_fch_sys_rule_type",3);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSystemID);
		gmDBManager.execute();
		alRuleType = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		alLinkedRuleType = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		hmReturn.put("RULETYPELIST", alRuleType);
		hmReturn.put("LINKEDRULETYPELIST", alLinkedRuleType);
		return hmReturn;
	}

	public ArrayList fetchSystemAllRuleType(String strSystemID) throws AppError{
		log.debug("========strSystemID========="+strSystemID);
		ArrayList alAllRuleType = new ArrayList();
		ArrayList alLinkedRuleType = new ArrayList();
		HashMap hmReturn = new HashMap();
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_rpt.gm_fch_sys_all_rule_type",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSystemID);
		gmDBManager.execute();
		alAllRuleType = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alAllRuleType;
	}	
	
	public void saveSystemRule(HashMap hmParam) throws AppError {
		String strSystemID = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strPublishedBy = GmCommonClass.parseNull((String) hmParam.get("PUBLISHEDBY"));
		Date dtPublishedDate = (Date) hmParam.get("PUBLISHEDDATE");
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strUserId = (String) hmParam.get("USERID"); 

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_txn.gm_sav_system_rule", 6);
		gmDBManager.setString(1, strSystemID);
		gmDBManager.setString(2, strStatus); 
		gmDBManager.setString(3, strPublishedBy); 
		gmDBManager.setDate(4, dtPublishedDate);
		gmDBManager.setString(5, strInputString); 
		gmDBManager.setString(6, strUserId); 
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	public ArrayList fetchSetsMapGroup(HashMap hmParam) throws AppError {
		ArrayList alReturn = new ArrayList();
		String strSystemID = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
		String strFwdGroupType= GmCommonClass.parseNull((String) hmParam.get("FWDGROUPTYPE"));
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_rpt.gm_fch_mapped_sets_grp", 3);
		gmDBManager.setString(1, strSystemID);
		gmDBManager.setString(2, strFwdGroupType);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return alReturn;
	}
	
	public HashMap fetchSystemRuleTypeByGroupType(HashMap hmParam) throws AppError{
		HashMap hmReturn = new HashMap();
		String strSystemID = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
		String strFwdRuleTypeID = GmCommonClass.parseNull((String) hmParam.get("FWDRULETYPEID"));
		String strCurGroupType= GmCommonClass.parseNull((String) hmParam.get("FWDGROUPTYPE"));
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_rpt.gm_fch_sys_ruletype_by_grptype", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSystemID);
		gmDBManager.setString(2, strFwdRuleTypeID);
		gmDBManager.setString(3, strCurGroupType);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(4));
		gmDBManager.close();
		return hmReturn;
	}

	public ArrayList fetchSystemRuleGroup(String strSysRuleTypeID) throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_rpt.gm_fch_sys_rule_grp", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSysRuleTypeID));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alReturn;
	}
	
	public void saveSystemRuleGroup(HashMap hmParam) throws AppError {
		log.debug("========hmParam========="+hmParam);
		String strSystemID = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
		String strSysRuleTypeID= GmCommonClass.parseNull((String) hmParam.get("SYSRULETYPEID"));
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID")); 
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_classify_ord_txn.gm_sav_sys_rule_grp", 4);
		gmDBManager.setString(1, strSystemID);
		gmDBManager.setString(2, strSysRuleTypeID); 
		gmDBManager.setString(3, strInputString); 
		gmDBManager.setString(4, strUserId); 
		gmDBManager.execute();
		gmDBManager.commit();
	}

	public  ArrayList getCodeList(String strCodeGrp) throws AppError {
		ArrayList arList = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager();
		sbQuery.append("SELECT  C901_CODE_ID CODEID, C901_CODE_NM CODENM ");
		sbQuery.append(",C902_CODE_NM_ALT CODENMALT, C901_CONTROL_TYPE CONTROLTYP,C901_CODE_SEQ_NO CODESEQNO");
		sbQuery.append(" FROM  T901_CODE_LOOKUP ");
		sbQuery.append(" WHERE  C901_CODE_GRP IN ('" + strCodeGrp + "')");
		sbQuery.append(" AND C901_ACTIVE_FL = '1' ");
		sbQuery.append(" ORDER BY C901_CODE_NM ");
		log.debug("sbQuery.toString()" + sbQuery.toString());
		arList = gmDBManager.queryMultipleRecords(sbQuery.toString());
		gmDBManager.close();
		return arList;
	}
}
