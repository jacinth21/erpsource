package com.globus.prodmgmnt.mrktcollateral.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.mrktcollateral.beans.GmArtifactBean;
import com.globus.prodmgmnt.mrktcollateral.forms.GmKeywordDtlForm;

public class GmKeywordDtlAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError {
    log.debug("inside Action");
    String strOpt = "";

    String strRows = "";
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    GmArtifactBean gmArtifactBean = new GmArtifactBean();
    GmKeywordDtlForm gmKeywordDtlForm = (GmKeywordDtlForm) form;
    gmKeywordDtlForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmKeywordDtlForm);

    strOpt = GmCommonClass.parseNull(gmKeywordDtlForm.getStrOpt());

    strRows = GmCommonClass.parseNull(GmCommonClass.getRuleValue("KEYWORDS", "PAGESIZE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    gmKeywordDtlForm.setRows(strRows);

    if (strOpt.equals("Reload")) {
      String strxmlGridData = fetchKeywords(hmParam);
      gmKeywordDtlForm.setXmlStringData(strxmlGridData);
    }
    if (strOpt.equals("Save")) {
      // This method used to save the uploaded files
      return saveKeywords(hmParam);
    }
    return mapping.findForward("GmKeywordDtl");
  }

  /**
   * saveKeywords - The below method is used to save keywords.In this screen we are validating file
   * level keywords and system level keywords.
   * 
   * @param HashMap hmParam
   * @return ActionRedirect
   * @throws AppError
   */
  private ActionRedirect saveKeywords(HashMap hmParam) throws AppError {
    String strRedirectURL = "";
    GmArtifactBean gmArtifactBean = new GmArtifactBean();
    if (!hmParam.get("INPUTSTRING").toString().equals("")) {
      gmArtifactBean.saveKeywordDetail(hmParam);
    }

    strRedirectURL =
        "/gmKeywordDtl.do?strOpt=Reload&refId=" + hmParam.get("REFID") + "&type="
            + hmParam.get("TYPE") + "&systemId=" + hmParam.get("SYSTEMID");
    ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
    return actionRedirect;
  }

  /**
   * fetchKeywords - The below method is fetch keywords.In this screen we are fetching keywords
   * which are reftype 4000410.
   * 
   * @param HashMap hmParam
   * @return String
   * @throws AppError
   */
  private String fetchKeywords(HashMap hmParam) throws AppError {
    String strApplDateFmt = "";
    String strRedirectURL = "";
    HashMap hmReturn = new HashMap();
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    GmArtifactBean gmArtifactBean = new GmArtifactBean();
    ArrayList alReturn = new ArrayList();
    alReturn = GmCommonClass.parseNullArrayList(gmArtifactBean.fetchKeywordsDtl(hmParam));
    hmReturn.put("KEYWORDDATA", alReturn);
    hmReturn.put("APPLNDATEFMT", strApplDateFmt);
    hmReturn.put("STRSESSCOMPANYLOCALE",
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE")));
    return generateXMLString(hmReturn);

  }

  private String generateXMLString(HashMap hmParam) throws AppError { // Processing HashMap into XML
                                                                      // data
    GmTemplateUtil templateUtil = new GmTemplateUtil();

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    log.debug("the value inside HashMap***** in template" + hmParam);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("prodmgmnt/mrktcollateral/templates");
    templateUtil.setResourceBundle(GmCommonClass
        .getResourceBundleBean(
            "properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest",
            strSessCompanyLocale));
    templateUtil.setTemplateName("GmKeywordDetail.vm");
    return templateUtil.generateOutput();
  }

}
