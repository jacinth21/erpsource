package com.globus.prodmgmnt.mrktcollateral.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.prodmgmnt.mrktcollateral.beans.GmArtifactBean;
import com.globus.prodmgmnt.mrktcollateral.forms.GmArtifactsForm;
import com.globus.sales.beans.GmSalesReportBean;

public class GmArtifactsAction extends GmAction {

  private static String strComnPath = GmCommonClass.getString("GMCOMMON");
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError {
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();

    GmArtifactsForm gmArtifactsForm = (GmArtifactsForm) form;
    gmArtifactsForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmArtifactsForm);

    String strOpt = GmCommonClass.parseNull(gmArtifactsForm.getStrOpt());
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(
            gmArtifactsForm.getUserId(), "FILE_SHARE"));
    gmArtifactsForm.setSubmitAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(
            gmArtifactsForm.getUserId(), "KEYWORD_ACCESS"));
    gmArtifactsForm.setKeywordAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    // This method used to populate the Dropdown list
    populateControls(gmArtifactsForm);

    if (strOpt.equals("Save")) {
      // This method used to save the uploaded files
      return saveArtifacts(hmParam);
    }
    if (strOpt.equals("Reload")) {
      // This method used to fetch the uploaded files
      String strxmlGridData = fetchArtifacts(hmParam);
      gmArtifactsForm.setXmlStringData(strxmlGridData);
    }
    return mapping.findForward("GmArtifatcs");
  }// End of execute

  /**
   * populateControls - used to populate the Controls on Screen
   * 
   * @param GmArtifactsForm gmArtifactsForm
   * @return void
   * @throws AppError
   */
  private void populateControls(GmArtifactsForm gmArtifactsForm) throws AppError {

    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean();
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
    GmArtifactBean gmArtifactBean = new GmArtifactBean();
    HashMap hmKey = new HashMap();
    HashMap hmParam = new HashMap();
    String strUploadType = gmArtifactsForm.getUploadType();
    String strRefId = gmArtifactsForm.getRefId();
    String strKeywordType = gmArtifactsForm.getKeywordType();

    gmArtifactsForm.setAlUploadTypes(GmCommonClass.getCodeList("MREFTY"));
    gmArtifactsForm.setAlArtifactsType(GmCommonClass.getCodeList("FTYPE"));
    gmArtifactsForm.setAlSubType(GmCommonClass.getCodeList("FSBTYP"));
    // 103092- System
    if (strUploadType.equals("103092")) {
      gmArtifactsForm.setAlRefId(gmSalesReportBean.loadProjectGroup());
      hmKey.put("ID", "ID");
      hmKey.put("NAME", "NAME");
      hmKey.put("LABEL", "System List");
      gmArtifactsForm.setHmKey(hmKey);

      if (!strRefId.equals("")) {
        // load system keywords
        hmParam.put("REFID", strRefId);
        hmParam.put("TYPE", strKeywordType);
        gmArtifactsForm.setSystemKeywords(gmArtifactBean.fetchSystemKeywords(hmParam));
      }
      // 103094- Group
    } else if (strUploadType.equals("103094")) {
      hmParam.put("STROPT", "40045");
      gmArtifactsForm.setAlRefId(gmDemandSetupBean.loadDemandGroupList(hmParam));
      hmKey.put("ID", "CODEID");
      hmKey.put("NAME", "CODENM");
      hmKey.put("LABEL", "Group List");
      gmArtifactsForm.setHmKey(hmKey);
      // 103090- PartNumber
    } else if (strUploadType.equals("103090")) {
      hmKey.put("LABEL", "Part");
      gmArtifactsForm.setHmKey(hmKey);
      // 103091- Set
    } else if (strUploadType.equals("103091")) {
      hmKey.put("LABEL", "Set ID");
      gmArtifactsForm.setHmKey(hmKey);
    }

  }// End of loadRefList

  /**
   * save - To save the Uploaded files Attributes.
   * 
   * @param HashMap hmParam
   * @return ActionRedirect
   * @throws AppError
   */
  private ActionRedirect saveArtifacts(HashMap hmParam) throws AppError {

    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();

    if (!hmParam.get("INPUTSTRING").toString().equals("")) {
      // saveFileAttributes is used to save uploaded files details
      gmCommonUploadBean.saveFileAttributes(hmParam);
    }
    ActionRedirect actionRedirect =
        new ActionRedirect("/gmShareArtifacts.do?strOpt=Reload&refId=" + hmParam.get("REFID")
            + "&subType=" + hmParam.get("SUBTYPE") + "&artifactsType="
            + hmParam.get("ARTIFACTSTYPE") + "&uploadType=" + hmParam.get("UPLOADTYPE"));
    return actionRedirect;
  }// End of save

  /**
   * reLoad - To fetch the Uploaded files on grid.
   * 
   * @param HashMap hmParam
   * @return String
   * @throws AppError
   */
  private String fetchArtifacts(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmShareRule = new HashMap();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    GmCommonBean gmCommonBean = new GmCommonBean();

    // fetchUploadInfo is used to fetch all uploaded Information
    hmReturn.put("SHAREABLEDATA", gmCommonUploadBean.fetchUploadInfo(hmParam));
    hmReturn.put("APPLNDATEFMT", hmParam.get("APPLNDATEFMT"));
    hmReturn.put("STRSESSCOMPANYLOCALE",
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE")));

    hmShareRule.put("RULEID", "SHAREGRP");
    // fetchTransactionRules is used to get Shareable Access values
    hmShareRule = gmCommonBean.fetchTransactionRules(hmShareRule);

    hmReturn.put("HMSHARERULE", hmShareRule);
    return generateXMLString(hmReturn);
  }// End of reLoad

  private String generateXMLString(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("prodmgmnt/mrktcollateral/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.prodmgmnt.mrktcollateral.GmArtifacts", strSessCompanyLocale));
    templateUtil.setTemplateName("GmArtifacts.vm");
    return templateUtil.generateOutput();
  }// End of generateXMLString
}
