package com.globus.prodmgmnt.mrktcollateral.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmArtifactsForm extends GmCommonForm{
	
	private String xmlStringData = "";
	private String inputString= "";
	private String uploadType = "";
	private String artifactsType = "";
	private String refId = "";
	private String subType = "";
	private String submitAccess ="";
	private String systemKeywords ="";
	private String keywordType ="";
	private String keywordAccess ="";
	
	private ArrayList alUploadTypes = new ArrayList();
	private ArrayList alArtifactsType = new ArrayList();
	private ArrayList alSubType = new ArrayList();
	private ArrayList alRefId = new ArrayList();
	private HashMap hmKey= new HashMap();
	
	
	public String getKeywordType() {
		return keywordType;
	}
	public void setKeywordType(String keywordType) {
		this.keywordType = keywordType;
	}
	public String getSystemKeywords() {
		return systemKeywords;
	}
	public void setSystemKeywords(String systemKeywords) {
		this.systemKeywords = systemKeywords;
	}
	public String getXmlStringData() {
		return xmlStringData;
	}
	public void setXmlStringData(String xmlStringData) {
		this.xmlStringData = xmlStringData;
	}
	public String getInputString() {
		return inputString;
	}
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	public String getUploadType() {
		return uploadType;
	}
	public void setUploadType(String uploadType) {
		this.uploadType = uploadType;
	}
	public String getArtifactsType() {
		return artifactsType;
	}
	public void setArtifactsType(String artifactsType) {
		this.artifactsType = artifactsType;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public ArrayList getAlUploadTypes() {
		return alUploadTypes;
	}
	public void setAlUploadTypes(ArrayList alUploadTypes) {
		this.alUploadTypes = alUploadTypes;
	}
	public ArrayList getAlArtifactsType() {
		return alArtifactsType;
	}
	public void setAlArtifactsType(ArrayList alArtifactsType) {
		this.alArtifactsType = alArtifactsType;
	}
	public ArrayList getAlSubType() {
		return alSubType;
	}
	public void setAlSubType(ArrayList alSubType) {
		this.alSubType = alSubType;
	}
	public ArrayList getAlRefId() {
		return alRefId;
	}
	public void setAlRefId(ArrayList alRefId) {
		this.alRefId = alRefId;
	}
	public HashMap getHmKey() {
		return hmKey;
	}
	public void setHmKey(HashMap hmKey) {
		this.hmKey = hmKey;
	}
	public String getSubmitAccess() {
		return submitAccess;
	}
	public void setSubmitAccess(String submitAccess) {
		this.submitAccess = submitAccess;
	}
	public String getKeywordAccess() {
		return keywordAccess;
	}
	public void setKeywordAccess(String keywordAccess) {
		this.keywordAccess = keywordAccess;
	}
	
}
