package com.globus.prodmgmnt.mrktcollateral.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmKeywordDtlForm extends GmCommonForm{
	
	private String strOpt = "";
	private String xmlStringData = "";
	private String inputString = "";
	private String type = "";
	private String refId = "";
	private String systemId = "";
	private String hDisplayNm = "";
	private String hRedirectURL = "";
	private String rows = "";
	
	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public String gethDisplayNm() {
		return hDisplayNm;
	}

	public void sethDisplayNm(String hDisplayNm) {
		this.hDisplayNm = hDisplayNm;
	}

	public String gethRedirectURL() {
		return hRedirectURL;
	}

	public void sethRedirectURL(String hRedirectURL) {
		this.hRedirectURL = hRedirectURL;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getXmlStringData() {
		return xmlStringData;
	}

	public void setXmlStringData(String xmlStringData) {
		this.xmlStringData = xmlStringData;
	}

	public String getStrOpt() {
		return strOpt;
	}

	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}

	public String getInputString() {
		return inputString;
	}

	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	
	
	
	
}