package com.globus.prodmgmnt.mrktcollateral.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.prodmgmnt.GmKeywordRefVO;

public class GmArtifactBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchKeywords This method is used in webservice which is used to fetch the Keywords Details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public List<GmKeywordRefVO> fetchKeywords(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    List<GmKeywordRefVO> listGmKeywordRefVO;
    GmKeywordRefVO gmKeywordRefVO = (GmKeywordRefVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_mc_artifact_rpt.gm_fch_all_keywords", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmKeywordRefVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmKeywordRefVO,
            gmKeywordRefVO.getFileInfoProperties());
    gmDBManager.close();
    return listGmKeywordRefVO;
  }

  /**
   * fetchKeywords This method is used in portal which is used to fetch the Keywords Details here
   * input parameter is ref id and ref type(4000410)
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchKeywordsDtl(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_mc_artifact_rpt.gm_fch_keywords", 3);
    gmDBManager.setString(1, (String) hmParams.get("REFID"));
    gmDBManager.setString(2, (String) hmParams.get("TYPE"));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchKeywords This method will fetch the System Keywords
   * 
   * @param HashMap
   * @return String
   * @exception AppError
   */
  public String fetchSystemKeywords(HashMap hmParams) throws AppError {
    StringBuffer strKeywords = new StringBuffer();
    String strSeparator = ",";
    int alResultSize;
    ArrayList alResult = new ArrayList();
    HashMap hmTemp = new HashMap();
    alResult = fetchKeywordsDtl(hmParams);
    alResultSize = alResult.size();
    for (int i = 0; i < alResultSize; i++) {
      hmTemp = GmCommonClass.parseNullHashMap((HashMap) alResult.get(i));
      strKeywords.append((String) hmTemp.get("KEYWORDNM"));
      if (i != (alResultSize - 1)) {
        strKeywords.append(strSeparator);
      }
    }
    return strKeywords.toString();
  }

  /**
   * saveKeywordDetail - To save the Keyword Name. inputString = keyword_id^keyword name|
   * 
   * @param HashMap hmparam
   * @return void
   * @throws AppError
   * @author Anilkumar
   */
  public void saveKeywordDetail(HashMap hmparam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_mc_artifact_txn.gm_sav_keyword_dtl", 5);
    gmDBManager.setString(1, (String) hmparam.get("REFID"));
    gmDBManager.setString(2, (String) hmparam.get("TYPE"));
    gmDBManager.setString(3, (String) hmparam.get("SYSTEMID"));
    gmDBManager.setString(4, (String) hmparam.get("INPUTSTRING"));
    gmDBManager.setString(5, (String) hmparam.get("USERID"));
    gmDBManager.execute();
    gmDBManager.commit();
  }// END of saveShareableArtifacts

}
