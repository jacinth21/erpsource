package com.globus.custservice.actions;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDDTBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.forms.GmProformaInvoiceForm;

public class GmProformaInvoiceAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmProformaInvoiceForm gmProformaInvoiceForm = (GmProformaInvoiceForm) form;
    gmProformaInvoiceForm.loadSessionParameters(request);
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDDTBean gmDDTBean = new GmDDTBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmJasperHeaderParams = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strApplDateFmt = "";
    String strCurrSin = "";
    String strTodayDt = "";
    String strSurgeryDt = "";
    String strJPSurgeryDt = "";
    String strUserId = "";
    String strOrderID = "";
    String strCurrSign = "";
    String strTotalAmt = "";
    String strPatient = "";
    String strSurgeon = "";
    String strCodeID = "";
    String strCodeNm = "";
    String strBillAddress = "";
    String strVATExcl = "";
    Date dtToday = null;
    Date dtSurgery = null;
    Date dtSurgeryOperation = null;

    String strCompName = "";
    String strCompAdd = "";
    String strPostBox = "";
    String strCompphone = "";
    String strCompFax = "";
    String strCompLogo = "";
    String strImagePath = "";
    String strCompanyLocale = "";
    String strAccCurrSYMB = "";
    String strCompSeal = "";
    String strDivisionId = "";
    String strComments = "";
    String strDefaultDoHoldCmt ="";
    String strHoldReason = "";
    String strDOHeldOrderComment ="";
    String strHoldCancelReason ="";
    String strAccVATTaxVal = "";

    ArrayList alQuoteDetails = new ArrayList();
    HashMap hmRuleValue = new HashMap();
    strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
    strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    GmResourceBundleBean gmPaperWorkResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    hmParam = GmCommonClass.getHashMapFromForm(gmProformaInvoiceForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strRptName = "";
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strCurrSign = GmCommonClass.parseNull((String) hmParam.get("APPLNCURRSIGN"));
    strCompName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
    strCompAdd = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYADDRESS"));
    strPostBox = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYPOST"));
    strCompphone = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYPHONE"));
    strCompFax = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYFAX"));
    strCompLogo = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COMP_LOGO"));
    strCompLogo = strImagePath + strCompLogo + ".gif";
    strCompSeal = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COMP_SEAL"));
    strCompSeal = strImagePath + strCompSeal + ".gif";
    strDefaultDoHoldCmt =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("DO_AUTO_COMMENTS", "DO_AUTO_HOLD")); //DO on Hold Status 
    strHoldReason =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("DO_CANCEL_REASON", "DO_AUTO_HOLD")); //100020-code id
    strHoldCancelReason = GmCommonClass.getCodeNameFromCodeId(strHoldReason); //PO price discrepancy
    strDOHeldOrderComment= strHoldCancelReason +": "+strDefaultDoHoldCmt;
     log.debug("hmParam>>>>>>>>>>>>" + hmParam);  

    if (strTodayDt.equals("")) {
      strTodayDt = GmCommonClass.parseNull(gmCal.getCurrentDate(strApplDateFmt));
      dtToday = GmCommonClass.getStringToDate(strTodayDt, strApplDateFmt);
    }


    hmJasperHeaderParams = GmCommonClass.parseNullHashMap(gmDOBean.fetchOrderDetails(strOrderID));
    strDivisionId = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("DIVISION_ID"));
    if(strDivisionId.equals("2006")){
    	strCompName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".COMPNAME"));
    	strCompAdd = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".COMPADDRESS"));
    	strCompphone = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".PH"));
    	strCompFax = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".FAX"));
    }
    if (strOpt.equals("PROFORMA")) {
      strRptName =
          GmCommonClass.getRuleValueByCompany(getGmDataStoreVO().getCmpid(), "PROFORMA_JASPER",
              getGmDataStoreVO().getCmpid());
      hmJasperHeaderParams.put("ADDWATERMARK", "Proforma Invoice");
    } else if (strOpt.equals("COMMERCIAL_INV")) {
      strRptName =
          GmCommonClass.getRuleValueByCompany(getGmDataStoreVO().getCmpid(), "COMMERICAL_JASPER",
              getGmDataStoreVO().getCmpid());
      hmJasperHeaderParams.put("ADDWATERMARK", "Commercial Invoice");
      hmJasperHeaderParams.put("SHOW_BANK", "YES");
    } else if (strOpt.equals("BOD")) {
      String strBODCopyFl=(String)request.getParameter("BOD_COPY_FL");
      if(strBODCopyFl.equals("Y")){
    	  strRptName =
    	          GmCommonClass.getRuleValueByCompany(getGmDataStoreVO().getCmpid(), "BILL_OF_DELIVERY_CPY",
    	              getGmDataStoreVO().getCmpid());
      }else{
    	  strRptName =
    	          GmCommonClass.getRuleValueByCompany(getGmDataStoreVO().getCmpid(), "BILL_OF_DELIVERY",
    	              getGmDataStoreVO().getCmpid());
      }

    } else if (strOpt.equals("DORECEIPT")) {
      strRptName =
          GmCommonClass.getRuleValueByCompany(getGmDataStoreVO().getCmpid(), "DORECEIPT",
              getGmDataStoreVO().getCmpid());

    }



    String strOrderDate =
        GmCommonClass.getStringFromDate((Date) hmJasperHeaderParams.get("ODT"), getGmDataStoreVO()
            .getCmpdfmt());


    String strShipperName =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMNA.COMPNAME"));
    String strShipperAddress =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMNA.COMPADDRESS"));
    String strShipperPhone = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMNA.PH"));
    String strShipAdd = strShipperName + "/" + strShipperAddress + "/" + strShipperPhone;
    hmJasperHeaderParams.put("GMADDRESS", strShipAdd);

    String strAccCurrId = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("ACC_CURR_ID"));

    hmJasperHeaderParams.put("SHIPTAXID",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONSIGNSHIPTAXID")));
    hmJasperHeaderParams.put("CONSIGNEEPHONE",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONSIGNEEPHONE")));

    String strShowBankByCurr =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOWBANKBYCURR"));

    if (strShowBankByCurr.equals("YES")) {

      hmJasperHeaderParams.put("IBAN",
          gmResourceBundleBean.getProperty(strAccCurrId + ".GMCOMPANYIBAN"));
      hmJasperHeaderParams.put("BANKADD",
          gmResourceBundleBean.getProperty(strAccCurrId + ".GMCOMPANYBANK"));
      hmJasperHeaderParams.put("SWIFTCODE",
          gmResourceBundleBean.getProperty(strAccCurrId + ".GMCOMPANYBANK_CODE"));
    } else {
      hmJasperHeaderParams.put("IBAN", gmResourceBundleBean.getProperty("GMCOMPANYIBAN"));
      hmJasperHeaderParams.put("BANKADD", gmResourceBundleBean.getProperty("GMCOMPANYBANK"));
      hmJasperHeaderParams.put("SWIFTCODE", gmResourceBundleBean.getProperty("GMCOMPANYBANK_CODE"));
    }
    hmJasperHeaderParams
        .put("CONSIGNEEREGION", gmResourceBundleBean.getProperty("CONSIGNEEREGION"));
    hmJasperHeaderParams.put("HARMCODE", gmResourceBundleBean.getProperty("HARMCODE"));
    hmJasperHeaderParams.put("COMPANYDFMT", getGmDataStoreVO().getCmpdfmt());

    strBillAddress = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("BILLADD"));
    strTotalAmt = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("TOTAL_SALES"));
    strVATExcl = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("VATEXCL"));
    strAccCurrSYMB = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("ACC_CURR_SYMB"));
    strCurrSign = strAccCurrSYMB.equals("") ? strCurrSign : strAccCurrSYMB;

    // if Commercial invoice - only show the order details - To avoid the child orders
    if (strOpt.equals("COMMERCIAL_INV")) {
      HashMap hmInParams = new HashMap();
      hmInParams.put("ORDERID", strOrderID);
      hmInParams.put("gmDataStoreVO", getGmDataStoreVO());
      alReturn = gmDOBean.fetchCartDetails(hmInParams);
    } else if ((strOpt.equals("BOD")) || (strOpt.equals("DORECEIPT"))) {
      hmJasperHeaderParams.put("STROPT", strOpt);
      hmJasperHeaderParams.put("ORDERID", strOrderID);
      alReturn =
          GmCommonClass.parseNullArrayList(gmDDTBean.fetchBODOrderDetails(hmJasperHeaderParams));
      hmJasperHeaderParams.put("ALSIZE", alReturn.size());
    } else {
      alReturn = GmCommonClass.parseNullArrayList(gmDOBean.fetchOrderSummary(strOrderID));
    }



    alQuoteDetails = gmCustomerBean.fetchOrderAttributeInfo(strOrderID, "SURATB");
    int intSize = alQuoteDetails.size();
    HashMap hcboVal = new HashMap();
    for (int i = 0; i < intSize; i++) {
      hcboVal = (HashMap) alQuoteDetails.get(i);
      strCodeID = (String) hcboVal.get("TRTYPEID");
      strCodeNm = (String) hcboVal.get("ATTRVAL");
      // 10304731:Patient Name of Poland
      if (strCodeID.equals("400145") || strCodeID.equals("10304731")) // Patient Name
      {
        strPatient = strCodeNm;
      }
      if (strCodeID.equals("400146")) // Surgeon Name
      {
        strSurgeon = strCodeNm;
      }
      if (strCodeID.equals("400151")) // AU Date of Surgery
      {
        strSurgeryDt = strCodeNm;
        dtSurgery = GmCommonClass.getStringToDate(strSurgeryDt, strApplDateFmt);
      }
      /*
       * if (strCodeID.equals("26230740")) // JP Date of Surgery { strSurgeryDt = strCodeNm; }
       */
    }



    hmRuleValue.put("RULEID", "AU");
    hmRuleValue.put("RULEGROUP", "PROFORMA");
    strAccVATTaxVal = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("ACCVATTAX"));
    String strVATper = !strAccVATTaxVal.equals("")?strAccVATTaxVal:GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleValue));

    strVATper = strVATExcl.equals("Y") ? "0" : strVATper;

    String strUserName = gmCommonBean.getUserName(strUserId);
    String strUserTitle =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strUserId, "USERTITLE"));
    // For Japan paperwork date format always should be YYYY/MM/DD
    strJPSurgeryDt =
        GmCommonClass.getStringFromDate((Date) hmJasperHeaderParams.get("ORD_SURG_DT"),
            "yyyy/MM/dd");
    String strCountryCode =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
  if(strCountryCode.equals("au")&&hmJasperHeaderParams.get("ORD_SURG_DT")!=null){
   dtSurgery=(Date)hmJasperHeaderParams.get("ORD_SURG_DT"); 		
  } 
  
  if (strOpt.equals("COMMERCIAL_INV")) {
	    String strLUserID = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("LUSERID"));
	  	   strUserName = gmCommonBean.getUserName(strLUserID);
	  	   hmJasperHeaderParams.put("USERID", strLUserID);
	  	  }

    hmJasperHeaderParams.put("TODAYDT", strTodayDt);
    hmJasperHeaderParams.put("DOID", strOrderID);
    hmJasperHeaderParams.put("CURRSIGN", strCurrSign);
    hmJasperHeaderParams.put("TOTALAMT", Double.parseDouble(strTotalAmt));
    hmJasperHeaderParams.put("BILLADD", strBillAddress);
    hmJasperHeaderParams.put("PATIENT", strPatient);
    hmJasperHeaderParams.put("SURGEON", strSurgeon);
    hmJasperHeaderParams.put("SURGERYDT", dtSurgery);
    hmJasperHeaderParams.put("DATEOFSUROPERTION", strJPSurgeryDt);
    hmJasperHeaderParams.put("VATPER", strVATper);
    hmJasperHeaderParams.put("COMPNAME", strCompName);
    hmJasperHeaderParams.put("COMPADD", strCompAdd);
    hmJasperHeaderParams.put("COMPPOST", strPostBox);
    hmJasperHeaderParams.put("COMPPHONE", strCompphone);
    hmJasperHeaderParams.put("COMPFAX", strCompFax);
    hmJasperHeaderParams.put("COMPLOGO", strCompLogo);
    hmJasperHeaderParams.put("COMPSEAL", strCompSeal);
    hmJasperHeaderParams.put("VARIABLECURRENCY", strAccCurrSYMB);
    hmJasperHeaderParams.put("LUSERNM", strUserName);
    hmJasperHeaderParams.put("UTITLE", strUserTitle);
    hmJasperHeaderParams.put("IMAGEPATH", strImagePath);
    hmJasperHeaderParams.put("DO_HELD_ORDER_COMMENT", strDOHeldOrderComment);

    // part details lable
    hmJasperHeaderParams.put("PARTNMLBL",
        GmCommonClass.parseNull(gmPaperWorkResourceBundleBean.getProperty("PART")));
    hmJasperHeaderParams.put("DESCLBL",
        GmCommonClass.parseNull(gmPaperWorkResourceBundleBean.getProperty("DESCRIPTION")));
    hmJasperHeaderParams.put("QTYLBL",
        GmCommonClass.parseNull(gmPaperWorkResourceBundleBean.getProperty("QTY")));
    hmJasperHeaderParams.put("CONTROLLBL",
        GmCommonClass.parseNull(gmPaperWorkResourceBundleBean.getProperty("CONTROL_NUMBER")));
    hmJasperHeaderParams.put("PRICELBL",
        GmCommonClass.parseNull(gmPaperWorkResourceBundleBean.getProperty("PRICE_EA")));
    hmJasperHeaderParams.put("AMOUNTLBL",
        GmCommonClass.parseNull(gmPaperWorkResourceBundleBean.getProperty("PRICE_EXT")));
    hmJasperHeaderParams.put("EXPIRYDATELBL",
            GmCommonClass.parseNull(gmPaperWorkResourceBundleBean.getProperty("EXPIRY_DATE")));

    hmJasperHeaderParams.put("JASPER_NAME", strRptName);
    hmJasperHeaderParams.put("SETLOAD", alReturn);
    gmProformaInvoiceForm.setHmJasperDetails(hmJasperHeaderParams);
    gmProformaInvoiceForm.setAlReturn(alReturn);


    return mapping.findForward("GmProformaInvoice");
  }
}
