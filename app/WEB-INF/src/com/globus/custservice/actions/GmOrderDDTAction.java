package com.globus.custservice.actions;


import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmDDTBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmOrderDDTBean;
import com.globus.custservice.forms.GmOrderDDTForm;

public class GmOrderDDTAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());
 
  /**
   * loadDDTNumber -  This method will fetch the order details, ship details and cart details
   * 
   * @author Paveethra Pandiyan
   * @return ActionForward
   */

  public ActionForward loadDDTNumber(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDDTBean gmDDTBean = new GmDDTBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmOrderDDTForm gmOrderDDTForm = (GmOrderDDTForm) form;
    gmOrderDDTForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmOrderDDTForm);
    String strMAPUpdFl = "";
    String strinvoiceId = "";
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    String strOrderID = gmOrderDDTForm.getOrderId();

    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    HashMap hmResult = gmDOBean.fetchOrderDetails(strOrderID);
    hmReturn.put("ORDERDETAILS", hmResult);
    hmResult = gmDOBean.fetchShipDetails(strOrderID);
    hmReturn.put("SHIPDETAILS", hmResult);
    ArrayList alCartDetails = gmDDTBean.fetchDDTCartDetails(strOrderID);
    hmReturn.put("CARTDETAILS", alCartDetails);

    gmOrderDDTForm.setHmResult(hmReturn);
    strinvoiceId = GmCommonClass.parseNull((String) hmReturn.get("INVOICEID"));
    hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "ORDER_DDT_ACCESS"));
      strMAPUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
   

    gmOrderDDTForm.setAccessFL(strMAPUpdFl);

    ArrayList alLog = gmCommonBean.getLog(strOrderID, "1200");
    gmOrderDDTForm.setAlLogReasons(alLog);
    return mapping.findForward("gmOrderDDTUpdate");
  }
  
  /**
   * loadDDTNumber -  This method will update DDT and redirect to loadDDTNumber method
   * @author Paveethra Pandiyan
   * @return ActionRedirect
   */

  public ActionForward saveDDTNumber(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmOrderDDTForm gmOrderDDTForm = (GmOrderDDTForm) form;
    gmOrderDDTForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmOrderDDTForm);

    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    hmParam.put("SCREEN", "UPDATE_DDT");
    gmDOBean.updateControlNumberDetail(hmParam, null);
    String strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(strOrderID, strComments, strUserId, "1200");
    }

    String strForward = "/gmOrderDDTAction.do?method=loadDDTNumber&orderId=" + strOrderID;

    return actionRedirect(strForward, request);
  }
  
  /** PMT-55577 - Record DDT # for Parts used in Surgery
   * loadLocLotTxnList - this method used to fetch the lot transaction ids 
 * @param mapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws Exception
 */
public ActionForward loadLocLotTxnList(ActionMapping mapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {
	    instantiate(request, response);
	    GmOrderDDTForm gmOrderDDTForm = (GmOrderDDTForm) form;
	    gmOrderDDTForm.loadSessionParameters(request);
	    GmOrderDDTBean gmOrderDDTBean = new GmOrderDDTBean(getGmDataStoreVO());
	    
	    HashMap hmParam = new HashMap();
	    String strDDTTxnId="";
	    hmParam = GmCommonClass.getHashMapFromForm(gmOrderDDTForm);
	    
	    strDDTTxnId = gmOrderDDTBean.loadLocationLotTxnList(hmParam);
	    
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strDDTTxnId.equals("")) {
			pw.write(strDDTTxnId);
		}
		pw.flush();
		return null;
	    
  }
	    
}
