package com.globus.custservice.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonApplicationBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmDODocumentUploadBean;
import com.globus.custservice.forms.GmDODocumentUploadForm;

/**
 * To upload the DO documents for revenue tracking (audit)
 * 
 * @author mmuthusamy
 *
 */
public class GmDODocumentUploadAction extends GmDispatchAction {

  private static String strComnPath = GmCommonClass.getString("GMCOMMON");
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger


  // Class.



  /**
   * loadOrderUpload - used to load the DO document details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadOrderUpload(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDODocumentUploadForm gmDODocumentUploadForm = (GmDODocumentUploadForm) actionForm;
    gmDODocumentUploadForm.loadSessionParameters(request);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    GmDODocumentUploadBean gmDODocumentUploadBean = new GmDODocumentUploadBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmDODocumentUploadForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("STRORDERID"));
    // String strFwdAction = GmCommonClass.parseNull(gmDODocumentUploadForm.getForwardAction());
    ArrayList alFilePath = new ArrayList();
    HashMap hmAccess = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmFileProp = new HashMap();
    String accessFlag = "";
    String strForwardPage = "GmDODocumentUpload";
    String strUploadTypeId = "";
    String strDcoedFlag = "";
    String strNonDcoedFlag = "";
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "UPLOAD_FILES_BTN"));
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug(" Permissions Flag = " + hmAccess);
    if (accessFlag.equals("Y")) {
      gmDODocumentUploadForm.setStrBtnAccessFl("Y");
    }
    request.setAttribute("hRedirectURL",
        GmCommonClass.parseNull(request.getParameter("hRedirectURL")));
    log.debug("strOpt= " + strOpt + " ----strAction= " + strAction);

    // to set the Revenue tracking type
    gmDODocumentUploadForm.setAlFileType(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("REVTP")));

    if (strOpt.equalsIgnoreCase("Load")) {
      // to validate the Order ID
      gmDODocumentUploadBean.validateDO(hmParam);
      gmDODocumentUploadForm.setRefId(strOrderId);
      gmDODocumentUploadForm.setStrOrderId(strOrderId);

    }
    strUploadTypeId = GmCommonClass.parseNull(gmDODocumentUploadForm.getUploadTypeId());
    log.debug(" strUploadTypeId " + strUploadTypeId);
    if (!strUploadTypeId.equals("")) {
      if (!strOrderId.equals("")) {
        hmValues.put("STRORDERID", strOrderId);
        hmValues.put("UPLOADTYPEID", strUploadTypeId);
        hmValues.put("DCOEDFL", strDcoedFlag);
        hmValues.put("NONDCOEDFL", strNonDcoedFlag);
        fetchUploadedFiles(actionMapping, actionForm, request, response, hmValues);
      }
    }
    gmDODocumentUploadForm.setStrOpt(strOpt);
    log.debug(" strForwardPage " + strForwardPage);
    return actionMapping.findForward(strForwardPage);
  }

  /**
   * UploadDODocument - Method used to upload the DO document
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward UploadDODocument(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDODocumentUploadForm gmDODocumentUploadForm = (GmDODocumentUploadForm) actionForm;
    gmDODocumentUploadForm.loadSessionParameters(request);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    GmDODocumentUploadBean gmDODocumentUploadBean = new GmDODocumentUploadBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmDODocumentUploadForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("STRORDERID"));
    String strRefId = "";
    String strSubType = "";
    String strTitleType = "";
    String strDcoedFlag = "";
    String strNonDcoedFlag = "";
    String strUploadHomeDir = "";
    String strUploadTypeNm = "";
    String strRefID = "";
    String strMessage = "";
    String strFileTypeList = "";
    String strFileNm = "";
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    String strUploadTypeId = "";
    String strFileIds = "";
    request.setAttribute("hRedirectURL",
        GmCommonClass.parseNull(request.getParameter("hRedirectURL")));


    // to get the Upload directory
    strUploadHomeDir =
        GmCommonClass.parseNull(GmCommonApplicationBean.getApplicationConfig("DO_DOC_UPLOAD"));

    if (strOpt.equals("UploadFiles")) {

      strUploadTypeId = GmCommonClass.parseNull(gmDODocumentUploadForm.getUploadTypeId());
      strUploadTypeNm =
          GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(gmDODocumentUploadForm
              .getUploadTypeId()));
      strRefID = GmCommonClass.parseNull(gmDODocumentUploadForm.getRefId());
      // File path DO home Dir + company locale + Order ID + File Name
      strUploadHomeDir = strUploadHomeDir + "\\" + strCompanyLocale + "\\" + strOrderId + "\\";

      log.debug("strUploadHomeDir::" + strUploadHomeDir + "::strUploadTypeNm::" + strUploadTypeNm
          + "::strRefID::" + strRefID);
      ArrayList alFileNmLists = gmDODocumentUploadForm.getListFile();
      log.debug(" alFileNmLists " + alFileNmLists);
      strFileTypeList = gmDODocumentUploadForm.getFileTypeList();
      log.debug(" alFileNmLists " + alFileNmLists.size());
      /*
       * convertStringToList method is used to get all the values from the input string for
       * FileType, FileTitle and FileName
       */
      ArrayList alFileTypeList = convertInputStringToList(strFileTypeList);

      // to validate the files
      validateFile(alFileNmLists);

      /*
       * In the below loop we are itrating the fileType, fileTitle, fileName from the screen and
       * getting the values upload the files at perticular loaction into the system and also store
       * the information into database table.
       */

      for (int i = 0; i < alFileNmLists.size(); i++) {// Get file from the form
        FormFile formFile = (FormFile) alFileNmLists.get(i);
        strFileNm = GmCommonClass.parseNull(formFile.getFileName());
        if (strFileNm.equals("")) { // alFileNmLists having empty values.So, to continue this loop
          // added continue here
          continue;
        }

        int fileSize = formFile.getFileSize();
        String strFileSize = Integer.toString(fileSize);
        String strFileType = "";
        String strFileTitle = "";
        String strFileId = "";

        for (int j = 0; j < alFileTypeList.size(); j++) {
          HashMap hmInputParams = GmCommonClass.parseNullHashMap((HashMap) alFileTypeList.get(j));
          log.debug("hmInputParams " + hmInputParams);

          String strFileNam = GmCommonClass.parseNull((String) hmInputParams.get("FILENAME"));
          log.debug(strFileNam + " " + strFileNm);
          if (strFileNam.equals(strFileNm)) {
            strFileType = GmCommonClass.parseNull((String) hmInputParams.get("TYPE"));
            strFileTitle = GmCommonClass.parseNull((String) hmInputParams.get("TITLE"));
            strSubType = GmCommonClass.parseNull((String) hmInputParams.get("SUBTYPE"));
            strTitleType = GmCommonClass.parseNull((String) hmInputParams.get("TITLETYPE"));
            strFileId = GmCommonClass.parseNull((String) hmInputParams.get("FILEID"));
          }
        }


        // to upload the files
        gmCommonUploadBean.uploadFile(formFile, strUploadHomeDir, strUploadHomeDir + "\\"
            + strFileNm);

        hmValues.put("FILEID", strFileId);
        hmValues.put("REFID", strOrderId);
        hmValues.put("REFTYPE", strFileType);
        hmValues.put("FILENAME", strFileNm);
        hmValues.put("USERID", strUserID);
        hmValues.put("FILETITLE", strFileTitle);
        hmValues.put("FILETYPE", strFileType);
        hmValues.put("FILESIZE", strFileSize);
        hmValues.put("FILESEQNO", "");
        hmValues.put("REFGRP", strUploadTypeId);
        hmValues.put("SUBTYPE", strSubType);
        hmValues.put("TITLETYPE", strTitleType);

        // saveUploadUniqueInfo method will store the information for the files into database table.
        strFileId = gmCommonUploadBean.saveUploadUniqueInfo(hmValues);
        strFileIds += strFileId + ",";
        // to update the DO Document flag
        hmParam.put("DO_DOC_UPLOAD_FL", "Y");
        gmDODocumentUploadBean.updateDODocumentUploadFlag(hmParam);
        // to store the document level comments to - Order audit trial log (PBUG-2700)
        if (!strFileTitle.equals("")) {
          gmCommonBean.saveLog(strOrderId, strFileTitle, strUserID, "1200");
        }

        strMessage = "File Uploaded Successfully";
        gmDODocumentUploadForm.setMessage(strMessage);

        strOpt = "Load";
      }
      // if (strOpt.equals("FETCHUPLOADFILES")) {

      gmDODocumentUploadForm.setUploadTypeId(strUploadTypeId);
      gmDODocumentUploadForm.setStrOrderId(strOrderId);
      // }

    }

    return loadOrderUpload(actionMapping, gmDODocumentUploadForm, request, response);
  }

  /**
   * fetchUploadedFiles - Method used to load the uploaded files information
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @param hmParam
   * @throws AppError
   */
  private void fetchUploadedFiles(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response, HashMap hmParam) throws AppError {

    GmDODocumentUploadForm gmDODocumentUploadForm = (GmDODocumentUploadForm) form;
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());

    ArrayList alReport = new ArrayList();

    // Getting Template column names from properties files
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("STRORDERID"));
    String strUploadTypeId = GmCommonClass.parseNull((String) hmParam.get("UPLOADTYPEID"));
    String strRBPath = "properties.labels.custservice.GmDODocumentUpload";
    hmParam.put("COMPANYLOCALE", strCompanyLocale);
    hmParam.put("RBPATH", strRBPath);

    // get order id and pass into fetchUpload info hmparm.get(strOrderId);
    alReport =
        GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo("", strOrderID,
            strUploadTypeId));
    log.debug("alReport === " + alReport.size());
    gmDODocumentUploadForm.setIntResultSize(alReport.size());
    hmParam.put("ALRESULT", alReport);
    String strXmlGridData = generateOutPut(hmParam);
    log.debug(" strXmlGridData " + strXmlGridData);
    gmDODocumentUploadForm.setGridXmlData(strXmlGridData);

  }

  /**
   * generateOutPut - Method used to generate the XML string
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  private String generateOutPut(HashMap hmParam) throws AppError {

    String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"));
    String strRBPath = GmCommonClass.parseNull((String) hmParam.get("RBPATH"));

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alResult", (ArrayList) hmParam.get("ALRESULT"));
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strRBPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName("GmDODocumentUpload.vm");
    return templateUtil.generateOutput();
  }


  /*
   * convertInputStringToList method is used to get all the values from the input string for
   * FileType, FileTitle and FileName inputSting like:
   * FileType^FileTitle^FileName|FileType^FileTitle^FileNam|
   */
  private ArrayList convertInputStringToList(String strInputStr) throws AppError {

    String strTemp, strType, strTitle, strFileName = "";
    String strSubType = "";
    String strTitleType = "";
    String strFileId = "";
    String[] strInnerTok;
    int intInputSize;
    StringTokenizer strTok = new StringTokenizer(strInputStr, "|");

    ArrayList alList = new ArrayList();
    HashMap hmParam = null;

    while (strTok.hasMoreTokens()) {
      hmParam = new HashMap();
      strTemp = strTok.nextToken();
      strInnerTok = strTemp.split("\\^");
      intInputSize = strInnerTok.length;
      strType = strInnerTok[0];
      strTitle = strInnerTok[1];
      strFileName = strInnerTok[2];
      strSubType = strInnerTok[3];
      // Title Type will come when DCOed doc upload/edit
      // FileId will come when edit existing file
      strTitleType = (intInputSize > 4) ? strInnerTok[4] : "";
      strFileId = (intInputSize > 5) ? strInnerTok[5] : "";

      hmParam.put("TYPE", strType);
      hmParam.put("TITLE", strTitle);
      hmParam.put("FILENAME", strFileName);
      hmParam.put("SUBTYPE", strSubType);
      hmParam.put("TITLETYPE", strTitleType);
      hmParam.put("FILEID", strFileId);
      alList.add(hmParam);
    }
    return alList;
  }

  /**
   * downloadFiles - Method used to download the files
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward downloadFiles(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDODocumentUploadForm gmDODocumentUploadForm = (GmDODocumentUploadForm) actionForm;
    gmDODocumentUploadForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();

    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    hmParam = GmCommonClass.getHashMapFromForm(gmDODocumentUploadForm);
    File file = null;
    FileInputStream istr = null;
    String strFileName = "";
    String strFileId = GmCommonClass.parseNull(request.getParameter("fileID"));
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("STRORDERID"));
    String strUploadHomeDir =
        GmCommonClass.parseNull(GmCommonApplicationBean.getApplicationConfig("DO_DOC_UPLOAD"));

    // File path DO home Dir + company locale + Order ID + File Name
    strUploadHomeDir = strUploadHomeDir + "\\" + strCompanyLocale + "\\" + strOrderId + "\\";

    strFileName = gmCommonUploadBean.fetchFileName(strFileId);
    ServletOutputStream out = response.getOutputStream();
    response.setContentType("application/binary");
    response.setHeader("Content-disposition", "attachment;filename=" + strFileName);
    log.debug("file name......" + strUploadHomeDir + strFileName);
    try {
      file = new File(strUploadHomeDir + strFileName);
      istr = new FileInputStream(file);
    } catch (FileNotFoundException ffe) {
      log.debug("CATCH EXE" + ffe);
      response.sendRedirect(response.encodeRedirectUrl(strErrorFileToDispatch));
      throw new AppError("", "20676");
    }
    response.setContentLength((int) file.length());
    int curByte = -1;
    while ((curByte = istr.read()) != -1)
      out.write(curByte);
    istr.close();
    out.flush();
    out.close();

    return null;
  }

  /**
   * validateFile -
   * 
   * @param alFileNmLists
   * @throws AppError
   */
  private void validateFile(ArrayList alFileNmLists) throws AppError {
    String strWhiteList =
        GmCommonClass.parseNull(GmCommonApplicationBean.getApplicationConfig("DO_DOC_WHITELIST"));
    for (int j = 0; j < alFileNmLists.size(); j++) {// Get file from the form
      FormFile formFile = (FormFile) alFileNmLists.get(j);

      String strFileName = formFile.getFileName();
      log.debug("strFilename" + strFileName);
      if (!strFileName.equals("")) {
        String strfileExt = strFileName.substring(strFileName.lastIndexOf("."));
        String strUploadFileExt = "*" + strfileExt;
        String strExtList[] = strWhiteList.split(",");
        // to get the max size
        int intMaxFileSize =
            Integer.parseInt(GmCommonClass.parseZero(GmCommonApplicationBean
                .getApplicationConfig("DO_DOC_MAXFILESIZE")));
        int intFileSize = formFile.getFileSize();
        boolean validFlag = false;

        for (int i = 0; i < strExtList.length; i++) {
          String strAllowedExt = strExtList[i];
          if (strAllowedExt.equalsIgnoreCase(strUploadFileExt)) {
            validFlag = true;
            break;
          }
        }
        if (!validFlag) {
          throw new AppError(
              "Upload not successful, as the file type(extension) is not supported.<br>Allowed extension(s):"
                  + strWhiteList, "", 'E');
        } else if (intFileSize == 0) {
          throw new AppError("", "20680");
        } else if (intFileSize > intMaxFileSize) {
          throw new AppError("", "20681");
        }
      }
    }
  }

}
