package com.globus.custservice.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.forms.GmEmailTrackRptForm;
import com.globus.custservice.beans.GmEmailTrackRptBean;

public class GmEmailTrackRptAction extends GmDispatchAction {
  
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code
	 
  public ActionForward loadEmailTrackRpt(ActionMapping actionMapping, ActionForm actionForm,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	  instantiate(request, response);
	 
	  GmEmailTrackRptForm gmEmailTrackRptForm = (GmEmailTrackRptForm)actionForm;		    
	    gmEmailTrackRptForm.loadSessionParameters(request);
	    GmEmailTrackRptBean gmEmailTrackRptBean = new GmEmailTrackRptBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    HashMap hmResults = new HashMap();
	    ArrayList alResult = new ArrayList();
	    String strSessCompanyLocale =GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
	    hmParam = GmCommonClass.getHashMapFromForm(gmEmailTrackRptForm);
	    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
	    if(strOpt.equals("Load")){
	    alResult =
	        GmCommonClass.parseNullArrayList(gmEmailTrackRptBean.fetchEmailTrackingDetails(hmParam));
	    }
	    String strResult =GmCommonClass.parseNull(getXmlGridData(alResult,strSessCompanyLocale));
	    gmEmailTrackRptForm.setgridXmlData(strResult);
	    return actionMapping.findForward("GmEmailTrackReport");
	  
	   } 

  /**
   * Email tracking- This method will fetch the email details
   * @return String
   * @param array list and company locale
   **/
public String getXmlGridData(ArrayList alResult,String strSessCompanyLocale) throws AppError {

		    GmTemplateUtil templateUtil = new GmTemplateUtil();
		    templateUtil.setDataList("alResult", alResult);
		    templateUtil.setTemplateSubDir("custservice/templates");
		    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmEmailTrackReport", strSessCompanyLocale));
		    templateUtil.setTemplateName("GmEmailTrackReport.vm");
		    return templateUtil.generateOutput();	 
		   
	  }

/**
 * fetchEmailMessage
 * @param actionMapping
 * @param actionForm
 * @param request
 * @param response
 * @return org.apache.struts.action.ActionForward
 */

public ActionForward fetchEmailMessage(ActionMapping actionMapping, ActionForm actionForm,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	  instantiate(request, response);
	  
	  GmEmailTrackRptForm gmEmailTrackRptForm = (GmEmailTrackRptForm)actionForm;		    
	  gmEmailTrackRptForm.loadSessionParameters(request);
	  GmEmailTrackRptBean gmEmailTrackRptBean = new GmEmailTrackRptBean(getGmDataStoreVO());
	  String strEmailMsg = "";
	  String strEmailLogId = GmCommonClass.parseNull(gmEmailTrackRptForm.getEmailLogId());
	    
      strEmailMsg = gmEmailTrackRptBean.fetchEmailMessage(strEmailLogId);
      gmEmailTrackRptForm.setStrEmailMsg(strEmailMsg);
	  return actionMapping.findForward("GmPopUpEmail");
   }
}


