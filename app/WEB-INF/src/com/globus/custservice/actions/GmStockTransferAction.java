package com.globus.custservice.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmStockTransferRptBean;
import com.globus.custservice.beans.GmStockTransferTransBean;
import com.globus.custservice.forms.GmStockTransferForm;
import com.globus.custservice.shipping.beans.GmShippingTransBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.operations.requests.beans.GmStockInitiateBean;

public class GmStockTransferAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * stockTransferReport - Will fetch the stock transfer requests
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward stockTransferReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    String strXmlData = "";

    ArrayList alDateType = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alReqCompList = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();

    GmStockTransferForm gmStockTransferForm = (GmStockTransferForm) form;
    gmStockTransferForm.loadSessionParameters(request);
    GmStockTransferRptBean gmStockTransferRptBean = new GmStockTransferRptBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alReqCompList = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchAssocCompMap());
    alDateType =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("STDTTY", getGmDataStoreVO()));
    alStatus =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RQSTS", getGmDataStoreVO()));

    gmStockTransferForm.setAlReqCompList(alReqCompList);
    gmStockTransferForm.setAlDateType(alDateType);
    gmStockTransferForm.setAlStatus(alStatus);

    hmParam = GmCommonClass.getHashMapFromForm(gmStockTransferForm);
    alResult = GmCommonClass.parseNullArrayList(gmStockTransferRptBean.fetchStockTransfer(hmParam));

    hmParam.put("fileName", "GmStockTransferReport.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.custservice.GmStockTransfer");
    strXmlData = generateOutPut(hmParam, alResult);
    gmStockTransferForm.setGridXmlData(strXmlData);
    log.debug("strXmlData>>>>>" + strXmlData);

    return mapping.findForward("GmStockTransferReport");


  }

  /**
   * stockTransferReleaseControl - To Process the stock transfer request
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward stockTransferReleaseControl(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    String strTransid = "";
    String strOpt = "";
    String strFGInputStr = "";
    String strBLInputStr = "";
    String strUserId = "";
    String strChildRequestId = "";

    HashMap hmParam = new HashMap();
    HashMap hmReqDetails = new HashMap();
    HashMap hmChildReqDtls = new HashMap();
    ArrayList alChildRQList = new ArrayList();
    ArrayList alRQItemList = new ArrayList();
    ArrayList alReleaseFrom = new ArrayList();
    ArrayList alReleaseTransDtls = new ArrayList();

    GmStockTransferForm gmStockTransferForm = (GmStockTransferForm) form;
    gmStockTransferForm.loadSessionParameters(request);
    GmStockTransferTransBean gmStockTransferTransBean =
        new GmStockTransferTransBean(getGmDataStoreVO());
    GmStockTransferRptBean gmStockTransferRptBean = new GmStockTransferRptBean(getGmDataStoreVO());
    GmStockInitiateBean gmStockInitiateBean = new GmStockInitiateBean(getGmDataStoreVO());
    GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(getGmDataStoreVO());
    GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());

    hmParam = GmCommonClass.getHashMapFromForm(gmStockTransferForm);

    strTransid = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strFGInputStr = GmCommonClass.parseNull((String) hmParam.get("FGINPUTSTR"));
    strBLInputStr = GmCommonClass.parseNull((String) hmParam.get("BLINPUTSTR"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    if (strOpt.equals("save")) {
      // Create the stock transfer consignment for quanties fetched from FG
      if (!strFGInputStr.equals("")) {
        gmStockTransferTransBean.reconfig_RQSTToConsign(strTransid, strFGInputStr, strUserId);
      }
      // Create the stock transfer consignment for quanties fetched from Bulk
      if (!strBLInputStr.equals("")) {
        // Create child request if the parts are released from different warehouse
        if (!strFGInputStr.equals("")) {
          strChildRequestId =
              gmStockTransferTransBean.createChildReqFromMasterReq(strTransid, strUserId);
        }
        strChildRequestId = strChildRequestId.equals("") ? strTransid : strChildRequestId;
        gmStockTransferTransBean
            .reconfig_RQSTToConsign(strChildRequestId, strBLInputStr, strUserId);
      }
    }
    // fetch stock transfer request details
    hmReqDetails = gmStockInitiateBean.fetchRequestShipDetails(strTransid);
    // fetch stock transfer request part details
    alRQItemList =
        GmCommonClass.parseNullArrayList(gmRequestReportBean.fetchRequestInfoForEdit(strTransid));
    // fetch stock transfer request transaction details
    alReleaseTransDtls =
        GmCommonClass.parseNullArrayList(gmStockTransferRptBean.fetchReleasedSTTrans(strTransid));

    gmStockTransferForm =
        (GmStockTransferForm) GmCommonClass.getFormFromHashMap(gmStockTransferForm, hmReqDetails);

    gmStockTransferForm.setAlrqitemdetails(alRQItemList);
    gmStockTransferForm.setAlreleasetransdtls(alReleaseTransDtls);

    alReleaseFrom =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("STRLFM", getGmDataStoreVO()));

    gmStockTransferForm.setAlReleaseFrm(alReleaseFrom);

    return mapping.findForward("GmSTProcessRelease");

  }

  /**
   * stockTransferDetailReport - will fetch the stock transfer request with part,qty in detail
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward stockTransferDetailReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    String strGridXmlData = "";
    ArrayList alResult = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alReqCompList = new ArrayList();
    ArrayList alDateType = new ArrayList();
    ArrayList alQtyVariance = new ArrayList();
    HashMap hmParam = new HashMap();

    GmStockTransferForm gmStockTransferForm = (GmStockTransferForm) form;
    gmStockTransferForm.loadSessionParameters(request);
    GmStockTransferRptBean gmStockTransferRptBean = new GmStockTransferRptBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alDateType =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("STDTTY", getGmDataStoreVO()));
    alStatus =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RQSTS", getGmDataStoreVO()));
    gmStockTransferForm.setAlDateType(alDateType);
    gmStockTransferForm.setAlStatus(alStatus);
    alQtyVariance =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("OPERS", getGmDataStoreVO()));
    gmStockTransferForm.setAlQtyVariance(alQtyVariance);
    alReqCompList = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchAssocCompMap());
    gmStockTransferForm.setAlReqCompList(alReqCompList);


    hmParam = GmCommonClass.getHashMapFromForm(gmStockTransferForm);

    alResult =
        GmCommonClass.parseNullArrayList(gmStockTransferRptBean
            .fetchStockTransferDetailReport(hmParam));

    hmParam.put("fileName", "GmStockTransferDetailsReport.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.custservice.GmStockTransfer");

    if (alResult.size() != 0) {
      // search option for qty variance
      alResult = applySearchCriteria(alResult, gmStockTransferForm);
      strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));
      hmParam.put("COMPANYLOCALE", strSessCompanyLocale);
      strGridXmlData = generateOutPut(hmParam, alResult);
      // remove the special character
      strGridXmlData = GmCommonClass.replaceForXML(strGridXmlData);
    }

    gmStockTransferForm.setGridXmlData(strGridXmlData);

    return mapping.findForward("GmStockTransferDetailReport");
  }

  /**
   * getXmlGridData - This method will be used to generate Output for grid data.
   * 
   * @return String - will be used by dhtmlx grid
   * @exception AppError
   */

  private String generateOutPut(HashMap hmParam, ArrayList alResult) throws AppError {

    String strSessCompanyLocale = "";
    String strVmPropPath = "";
    String strFileName = "";
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    // ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT"));
    strFileName = GmCommonClass.parseNull((String) hmParam.get("fileName"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setTemplateSubDir("custservice/templates");
    return templateUtil.generateOutput();
  }

  // This Method used for Appyling the Search Criteria.
  public ArrayList applySearchCriteria(ArrayList alResult, GmStockTransferForm gmStockTransferForm)
      throws AppError {

    GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
    String strQtyValue = "";
    String strVarianceOpr = "";

    strQtyValue = GmCommonClass.parseNull(gmStockTransferForm.getVarianceQty());
    strVarianceOpr = GmCommonClass.parseNull(gmStockTransferForm.getVariance());

    if (!strVarianceOpr.equals("0") && !strQtyValue.equals("")) {
      gmSearchCriteria.addSearchCriteria("VARIANCE_QTY", strQtyValue, strVarianceOpr);
    }

    gmSearchCriteria.query(alResult);

    return alResult;
  }

  /**
   * stockTransferDetailReport - will fetch the stock transfer request with part,qty in detail
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward stockTransferInvoicePrint(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);

    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
    GmStockTransferRptBean gmStockTransferRptBean = new GmStockTransferRptBean(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean();
    GmStockTransferForm gmStockTransferForm = (GmStockTransferForm) form;
    gmStockTransferForm.loadSessionParameters(request);

    HashMap hmConsignData = new HashMap();
    HashMap hmReturnData = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strConsignId = GmCommonClass.parseNull(gmStockTransferForm.getFulfillTrnasctionId());
    String strUserId = GmCommonClass.parseNull(gmStockTransferForm.getUserId());

    hmConsignData = gmCustomerBean.loadConsignDetails(strConsignId, strUserId);
    HashMap hmCompDetails = gmStockTransferRptBean.loadCompanyDetails(strConsignId);
    String strConsignCompId = GmCommonClass.parseNull((String) hmCompDetails.get("CONS_COMP_ID"));

    hmConsignData.putAll(hmCompDetails);
    hmConsignData.put("CONSIGNID", strConsignId);
    hmConsignData.put("INVPRINTLBL", GmCommonClass.parseNull(gmStockTransferForm.getInvPrintLbl()));

    hmReturnData = gmStockTransferRptBean.loadStockTransferItemDetails(strConsignId);// SETLOAD
    alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmReturnData.get("SETLOAD"));
    HashMap hmResult = gmStockTransferRptBean.filterConsignData(hmConsignData, alReturn);

    hmResult.putAll(hmConsignData);
    hmReturn.put("HMORDERDETAILS", hmResult);
    hmReturn.put("ALORDERLIST", alReturn);
    request.setAttribute("CompanyId", strConsignCompId);
    request.setAttribute("INVRPTDATA", hmReturn);
    request.setAttribute("INVJASNAME", "/GmINStockTransferInvoicePrint.jasper");

    return mapping.findForward("GmOUSInvoicePrint");
  }

}
