package com.globus.custservice.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.forms.GmAccountShipSetupForm;


public class GmAccountShipSetupAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmAccountShipSetupForm gmAccountShipSetupForm = (GmAccountShipSetupForm) form;
    gmAccountShipSetupForm.loadSessionParameters(request);

    GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());

    ArrayList alState = new ArrayList();
    ArrayList alCountry = new ArrayList();
    ArrayList alCarrier = new ArrayList();

    HashMap hmAcctShipDtls = new HashMap();
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAccountShipSetupForm);

    String strOpt = "";
    String strAccId = "";
    // PMR-4359 -- Code change
    String strPrefCarrier = "";
    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
    String strRulePrefCarrier = "";

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strAccId = GmCommonClass.parseNull(gmAccountShipSetupForm.getAccID());

    alState = GmCommonClass.getCodeList("STATE", getGmDataStoreVO());
    gmAccountShipSetupForm.setAlState(alState);

    alCountry = GmCommonClass.getCodeList("CNTY");
    gmAccountShipSetupForm.setAlCountry(alCountry);

    // PMT-4359 -- Getting the Code Lookup values for DCAR Code group to form the alCarrier.
    alCarrier = GmCommonClass.getCodeList("DCAR", getGmDataStoreVO());
    gmAccountShipSetupForm.setAlCarrier(alCarrier);

    log.debug("strOpt = " + strOpt + " == strAccId = " + strAccId);
    if (strOpt.equals("reload")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmAccountShipSetupForm);
      hmAcctShipDtls = gmCustSalesSetupBean.fetchAcctShipDtls(hmParam);
      gmAccountShipSetupForm =
          (GmAccountShipSetupForm) GmCommonClass.getFormFromHashMap(gmAccountShipSetupForm,
              hmAcctShipDtls);
    }
    if (strOpt.equals("save")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmAccountShipSetupForm);
      gmCustSalesSetupBean.saveAcctShipSetup(hmParam);
      strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
      String strRedirectURL = "/gmAccountShipSetup.do?strOpt=reload&accID=" + strAccId;

      return actionRedirect(strRedirectURL, request);
    }

    // PMT-4359 Getting Carrier value from the form and from the Rule table Separately
    // If Form carrier value is empty then set Rule Carrier value to the form or set the form
    // carrier value as it is.
    strPrefCarrier = GmCommonClass.parseNull(gmAccountShipSetupForm.getCarrier());
    strRulePrefCarrier =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCountryCode, "PREFCARRIER"));
    if (strPrefCarrier.equals("")) {
      gmAccountShipSetupForm.setCarrier(strRulePrefCarrier);
    } else {
      gmAccountShipSetupForm.setCarrier(strPrefCarrier);
    }
    return mapping.findForward("gmAccountShipSetup");
  }
}
