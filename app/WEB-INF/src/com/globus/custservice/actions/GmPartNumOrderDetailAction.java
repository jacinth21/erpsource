package com.globus.custservice.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.forms.GmPartNumOrderDetailForm;



public class GmPartNumOrderDetailAction extends GmAction {

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48CED39B3454353450023
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    GmPartNumOrderDetailForm gmPartNumOrderDetailForm = (GmPartNumOrderDetailForm) form;
    gmPartNumOrderDetailForm.loadSessionParameters(request);
    ArrayList alDistReturn = new ArrayList();
    ArrayList alRepReturn = new ArrayList();
    ArrayList alOrdType = new ArrayList();

    HashMap hmParam = new HashMap();
    RowSetDynaClass rdReport = null;
    String strJsonData="";
    String strDispatchTo="GmPartNumOrderDetail";
    String strOpt = gmPartNumOrderDetailForm.getStrOpt();

    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

    alDistReturn = gmCustomerBean.getDistributorList("Active");
    gmPartNumOrderDetailForm.setAlDistList(alDistReturn);

    alRepReturn = gmCustomerBean.getRepList("Active");
    gmPartNumOrderDetailForm.setAlRepList(alRepReturn);

    alOrdType = GmCommonClass.getAllCodeList("ORDTP");
    gmPartNumOrderDetailForm.setAlOrdTypeList(alOrdType);
    GmCalenderOperations gmCal = new GmCalenderOperations();


    if (gmPartNumOrderDetailForm.getFromDate().equals("")
        && gmPartNumOrderDetailForm.getToDate().equals("")) {
      gmPartNumOrderDetailForm.setFromDate(gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt()));
      gmPartNumOrderDetailForm.setToDate(gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt()));
    }
    log.debug("strOpt=="+strOpt);
    if (strOpt.equals("RELOAD") || strOpt.equals("RPT")) {
      gmPartNumOrderDetailForm.setStrOpt("CloseBtn");
    }
    //PC-3968 - display tag to dhtmlx grid 
    hmParam = GmCommonClass.getHashMapFromForm(gmPartNumOrderDetailForm);
   // gmPartNumOrderDetailForm.setRdReport(rdReport);
    if (strOpt.equals("RELOAD")) {
    	strJsonData = gmCustomerBean.getOrderDetailsForParts(hmParam);
    	 
        if(!strJsonData.equals("")) {
	    log.debug("strJsonData=="+strJsonData);
	    response.setContentType("text/xml");
	    PrintWriter pw = response.getWriter();
	    pw.write(strJsonData);
	    pw.flush();
	    pw.close();
	   
	    }
        strDispatchTo=null;
    }
    return mapping.findForward(strDispatchTo);
  }

}
