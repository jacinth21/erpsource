/**
 * FileName : GmLotErrorReportAction.java, Description : Lot Error Report, Author : N Raja
 */
package com.globus.custservice.actions;
import java.io.PrintWriter;  
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.custservice.beans.GmLotErrorRptBean;
import com.globus.custservice.beans.GmLotErrorTxnBean;
import com.globus.custservice.forms.GmLotErrorReportForm;
import com.globus.operations.beans.GmLotCodeRptBean;
import com.globus.common.util.GmTemplateUtil;

public class GmLotErrorReportAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * @description Load the lot error report records
	 * @param actionMapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLotReport(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		 // call the instantiate method
		    instantiate(request, response);
		    GmLotErrorReportForm gmLotErrorReportForm = (GmLotErrorReportForm)form;
		    gmLotErrorReportForm.loadSessionParameters(request);
		    GmLotErrorRptBean gmLotCodeRptBean =new GmLotErrorRptBean(getGmDataStoreVO());
		    gmLotErrorReportForm.setAlTransType(GmCommonClass.parseNullArrayList(GmCommonClass
		           .getCodeList("LOTTRN", getGmDataStoreVO())));
		    gmLotErrorReportForm.setAlResolutionType(GmCommonClass.parseNullArrayList(GmCommonClass
		            .getCodeList("RESTYP", getGmDataStoreVO())));
		    gmLotErrorReportForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
		            .getCodeList("SMOST", getGmDataStoreVO())));
		    gmLotErrorReportForm.setUnResolvedStatus("60471");
		    String strFlag = GmCommonClass.parseNull(request.getParameter("strFlag"));
			gmLotErrorReportForm.setStrFlag(strFlag);
			return actionMapping.findForward("gmLotErrorReport");
	 }

	/**
	 * @description fetch the lot error report records
	 * @param mapping,form,request,response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotErrorReportInfo(ActionMapping mapping, ActionForm form, 
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmLotErrorReportForm gmLotErrorReportForm = (GmLotErrorReportForm)form;
		    gmLotErrorReportForm.loadSessionParameters(request);
		    GmLotErrorRptBean gmLotErrorRptBean = new GmLotErrorRptBean(getGmDataStoreVO());
			HttpSession session = request.getSession(false);
			HashMap hmParam = new HashMap();
			String strSessCompanyLocale = "";
			String strLotErrorRptJsonString ="";
			hmParam =  GmCommonClass.getHashMapFromForm(gmLotErrorReportForm);
			log.debug("hmParam"+hmParam);
			strLotErrorRptJsonString = GmCommonClass.parseNull(gmLotErrorRptBean.fetchLotErrorInfo(hmParam));  
	        response.setContentType("text/json");
		    PrintWriter pw = response.getWriter();
		    if (!strLotErrorRptJsonString.equals("")) {
	             pw.write(strLotErrorRptJsonString);
		     }
	             pw.flush();
		     return null;
	}
	
	/**
	 * @description Fetch Lot Error Detail
	 * @param mapping,form,request,response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotErrorDetail(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmLotErrorReportForm gmLotErrorReportForm = (GmLotErrorReportForm)form;
		    gmLotErrorReportForm.loadSessionParameters(request);
		    GmLotErrorRptBean gmLotErrorRptBean = new GmLotErrorRptBean(getGmDataStoreVO());
		    ArrayList lotErrorDetail = new ArrayList();
		    ArrayList alResType = new ArrayList();
		    ArrayList alDropDown = new ArrayList();
		    String strErrorInfoID = GmCommonClass.parseNull(gmLotErrorReportForm.getStrErrorInfoID());
		    lotErrorDetail =  GmCommonClass.parseNullArrayList(gmLotErrorRptBean.fetchLotErrorDetail(strErrorInfoID));
		    gmLotErrorReportForm.setAlList(lotErrorDetail);
		    if(lotErrorDetail.size()>0){
	    	HashMap hmReturn = new HashMap();
	    	hmReturn = (HashMap)lotErrorDetail.get(0);
	    	
	      	request.setAttribute("HMRETURN", hmReturn);
		    }
		    request.setAttribute("ALRESTYPE", GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RESTYP", getGmDataStoreVO())));
			return actionMapping.findForward("gmLotErrorDetail");
	 }
	
	/**
	 * @description Used to Save Error Detail with same lot
	 * @param mapping,form,request,response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveExistLot(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
			instantiate(request, response);
		    GmLotErrorReportForm gmLotErrorReportForm = (GmLotErrorReportForm)form;
		    gmLotErrorReportForm.loadSessionParameters(request);
		    GmLotCodeRptBean gmLotCodeRptBean =new GmLotCodeRptBean(getGmDataStoreVO());
		    GmLotErrorTxnBean gmLotErrorTxnBean = new GmLotErrorTxnBean(getGmDataStoreVO());
		    HashMap hmParam = new HashMap();
		    
		    String lotNum = GmCommonClass.parseNull(gmLotErrorReportForm.getLotNumber());
		    String partNum = GmCommonClass.parseNull(gmLotErrorReportForm.getPartNumber());
		    String comments = GmCommonClass.parseNull(gmLotErrorReportForm.getComments());
		    String resolutionsts = GmCommonClass.parseNull(gmLotErrorReportForm.getResolutionType());
		    String userid = GmCommonClass.parseNull(gmLotErrorReportForm.getUserId());
		    String errorInfoId = GmCommonClass.parseNull(gmLotErrorReportForm.getStrErrorInfoID());
		   
		    String strLotErrorRptJsonString ="";
		    hmParam.put("LOTNUM", lotNum);
		    hmParam.put("PARTNUM", partNum);
		    hmParam.put("COMMENTS", comments);
		    hmParam.put("RESOLUTIONSTS", resolutionsts);
		    hmParam.put("USERID", userid);
		    hmParam.put("ERRORINFOID", errorInfoId);
		    strLotErrorRptJsonString = gmLotErrorTxnBean.saveExistLot(hmParam);
		    
		    gmLotErrorReportForm.setStrLotErrorRptJsonString(strLotErrorRptJsonString);
		    
		    response.setContentType("text/plain");
		      PrintWriter pw = response.getWriter();
		      if (!strLotErrorRptJsonString.equals("")){
		      pw.write(strLotErrorRptJsonString);//return error message if validations throws while saving exist lot
		      }
		      pw.flush();
		      return null;
	 }
	
	/**
	 * @description Save Lot Error Detail using New Lot
	 * @param mapping,form,request,response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveNewLot(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		instantiate(request, response);
	    GmLotErrorReportForm gmLotErrorReportForm = (GmLotErrorReportForm)form;
	    gmLotErrorReportForm.loadSessionParameters(request);
	    GmLotCodeRptBean gmLotCodeRptBean =new GmLotCodeRptBean(getGmDataStoreVO());
	    GmLotErrorTxnBean gmLotErrorTxnBean = new GmLotErrorTxnBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    
	    String updLotNum = GmCommonClass.parseNull(gmLotErrorReportForm.getUpdLotNumber());
	    String partNum = GmCommonClass.parseNull(gmLotErrorReportForm.getPartNumber());
	    String comments = GmCommonClass.parseNull(gmLotErrorReportForm.getComments());
	    String resolutionsts = GmCommonClass.parseNull(gmLotErrorReportForm.getResolutionType());
	    String userid = GmCommonClass.parseNull(gmLotErrorReportForm.getUserId());
	    String errorInfoId = GmCommonClass.parseNull(gmLotErrorReportForm.getStrErrorInfoID());
	   
	    String strLotErrorRptJsonString ="";
	    hmParam.put("UPDLOTNUM", updLotNum);
	    hmParam.put("PARTNUM", partNum);
	    hmParam.put("COMMENTS", comments);
	    hmParam.put("RESOLUTIONSTS", resolutionsts);
	    hmParam.put("USERID", userid);
	    hmParam.put("ERRORINFOID", errorInfoId);
	    strLotErrorRptJsonString = gmLotErrorTxnBean.saveNewLot(hmParam);
	    
	    gmLotErrorReportForm.setStrLotErrorRptJsonString(strLotErrorRptJsonString);
	    
	    response.setContentType("text/plain");
	      PrintWriter pw = response.getWriter();//return error message if validations throws while saving new lot
	      if (!strLotErrorRptJsonString.equals("")){
	      pw.write(strLotErrorRptJsonString);
	      }
	      pw.flush();
	      return null;
	    
	 }
	
	
	/**
	 * @description Save Lot Error Detail as resolved
	 * @param mapping,form,request,response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveAsResolved(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		instantiate(request, response);
	    GmLotErrorReportForm gmLotErrorReportForm = (GmLotErrorReportForm)form;
	    gmLotErrorReportForm.loadSessionParameters(request);
	    GmLotCodeRptBean gmLotCodeRptBean =new GmLotCodeRptBean(getGmDataStoreVO());
	    GmLotErrorTxnBean gmLotErrorTxnBean = new GmLotErrorTxnBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    
	    String lotNum = GmCommonClass.parseNull(gmLotErrorReportForm.getLotNumber());
	    String partNum = GmCommonClass.parseNull(gmLotErrorReportForm.getPartNumber());
	    String comments = GmCommonClass.parseNull(gmLotErrorReportForm.getComments());
	    String resolutionsts = GmCommonClass.parseNull(gmLotErrorReportForm.getResolutionType());
	    String userid = GmCommonClass.parseNull(gmLotErrorReportForm.getUserId());
	    String errorInfoId = GmCommonClass.parseNull(gmLotErrorReportForm.getStrErrorInfoID());
	   
	    String strLotErrorRptJsonString ="";
	    hmParam.put("LOTNUM", lotNum);
	    hmParam.put("PARTNUM", partNum);
	    hmParam.put("COMMENTS", comments);
	    hmParam.put("RESOLUTIONSTS", resolutionsts);
	    hmParam.put("USERID", userid);
	    hmParam.put("ERRORINFOID", errorInfoId);
	    strLotErrorRptJsonString = gmLotErrorTxnBean.saveAsResolved(hmParam);
	    
	    gmLotErrorReportForm.setStrLotErrorRptJsonString(strLotErrorRptJsonString);
	    
	    response.setContentType("text/plain");
	      PrintWriter pw = response.getWriter();//return error message if validations throws while saving mark as resolved
	      if (!strLotErrorRptJsonString.equals("")){
	      pw.write(strLotErrorRptJsonString);
	      }
	      pw.flush();
	      return null;
	 }
}