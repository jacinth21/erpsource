package com.globus.custservice.actions;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmOrderUsageLotReportBean;
import com.globus.custservice.forms.GmOrderUsageLotReportForm;
import com.globus.common.actions.GmDispatchAction;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author tramasamy
 *
 */
public class GmOrderUsageLotReportAction extends GmDispatchAction {
	
	/**
	 *  this is get the logger properties
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	  /**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward orderUsageLotRpt(ActionMapping mapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		  GmOrderUsageLotReportForm gmOrderUsageLotRptForm = (GmOrderUsageLotReportForm) form;
		  gmOrderUsageLotRptForm.loadSessionParameters(request);
		  GmOrderUsageLotReportBean gmSalesOrderUsageLotRptBean = new GmOrderUsageLotReportBean(getGmDataStoreVO());
		  ArrayList alDateType = new ArrayList();
		  ArrayList alResult = new ArrayList();
		  HashMap hmApplnParam = new HashMap();
		  HashMap hmParam = new HashMap();
		  HashMap hmRegStr = new HashMap();
		  String strOpt ="";
		  String strPartNumFormat = "";
		  String strXmlData = "";
		  String strSessCompanyLocale = "";
		  String strHideOrdUsageRptFl = "";
		
		  hmParam = GmCommonClass.getHashMapFromForm(gmOrderUsageLotRptForm);
		  strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		  alDateType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ORDTTY","106480"));
		  gmOrderUsageLotRptForm.setAlDateType(alDateType);
		  String strCompanyLocale =
			        GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
			    GmResourceBundleBean gmResourceBundleBean =
			        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
		 strHideOrdUsageRptFl =
			        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("HIDE_ORDER_USAGE_FL"));
		 gmOrderUsageLotRptForm.setStrHideOrdUsageRptFl(strHideOrdUsageRptFl);
		//PC-3894- before 6 month added to Fromdate and Current date to Todate - changes
		 String strApplnDateFmt =getGmDataStoreVO().getCmpdfmt();
		 int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
 	     String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth-5,strApplnDateFmt);
 	     String strToDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
   // if strOpt value load -- fetch order usage lot details
	if(strOpt.equals("Load")){
	      hmRegStr.put("PARTNUM", gmOrderUsageLotRptForm.getPartNumber());
	      hmRegStr.put("SEARCH", gmOrderUsageLotRptForm.getPnumSuffix());
	      strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
	      hmParam.put("PARTNUM", strPartNumFormat);
		  alResult= GmCommonClass.parseNullArrayList(gmSalesOrderUsageLotRptBean.fetchOrderLotDetails(hmParam));
		  strSessCompanyLocale =
		          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
		      hmApplnParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
		      hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
		      hmApplnParam.put("TEMPLATE_NM", "GmOrderUsageLotReport.vm");
		      hmApplnParam.put("LABEL_PROPERTIES_NM",
		        "properties.labels.custservice.GmOrderUsageLotReport");
		      hmApplnParam.put("HIDEORDERUSAGERPTFL", strHideOrdUsageRptFl);  //Used strHideOrdUsageRptFl in PC-3894 - Sterile - Order usage report
		  
		      // based on array list to get the Grid xml string
		      strXmlData = gmSalesOrderUsageLotRptBean.generateOutput(alResult, hmApplnParam);
		      gmOrderUsageLotRptForm.setXmlString(strXmlData);
		      //PC-3894- load the data we need to updating the filter FromDate and ToDate changes
		      strMonthFirstDay =  GmCommonClass.parseNull((String)gmOrderUsageLotRptForm.getFromDate());
		      strToDate  = GmCommonClass.parseNull((String)gmOrderUsageLotRptForm.getToDate());
	  }
	    request.setAttribute("hFrmDt",strMonthFirstDay);
        request.setAttribute("hToDt",strToDate);
	    return mapping.findForward("success");
	}
	
	/**
	 * fetchOrderUsageDtls - this method used to fetch the order usage lot details based on parts
	 * 						Called from Issue Credit/Debit and Initiate Return screen.
	 * 						(PMT-45387: Provision to record DDT)
	 * 
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchOrderUsageDtls(ActionMapping mapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		  GmOrderUsageLotReportForm gmOrderUsageLotRptForm = (GmOrderUsageLotReportForm) form;
		  GmOrderUsageLotReportBean gmSalesOrderUsageLotRptBean = new GmOrderUsageLotReportBean(getGmDataStoreVO());
		
		  HashMap hmParam = new HashMap();
		  hmParam = GmCommonClass.getHashMapFromForm(gmOrderUsageLotRptForm);
		  String strOrderUsageDtls =
		          GmCommonClass.parseNull(gmSalesOrderUsageLotRptBean.fetchOrderUsageLotByPart(GmCommonClass.parseNull((String) hmParam.get("ORDERID")),
		        		  GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER")), GmCommonClass.parseNull((String) hmParam.get("INVOICEID"))));
		   
		        response.setContentType("text/json");
		        PrintWriter pw = response.getWriter();
		        if (!strOrderUsageDtls.equals("")) {
		        pw.write(strOrderUsageDtls);
		        }
		        pw.flush();
		      return null;  
	}
	
}
