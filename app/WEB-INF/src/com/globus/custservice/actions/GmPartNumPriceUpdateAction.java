package com.globus.custservice.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.forms.GmPartNumPriceUpdateForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPartNumPriceUpdateAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmPartNumPriceUpdateForm gmPartNumPriceUpdateForm = (GmPartNumPriceUpdateForm) form;
    gmPartNumPriceUpdateForm.loadSessionParameters(request);

    // bean declare
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());

    // local variable declare
    String strUserId = "";
    String strMode = "";
    String strGridData = "";
    String strOpt = "";
    String strProjId = "";
    String strInputString = "";
    String strColumn = "";
    String strAction = "";
    String strLoad = "";
    String strUploadParts = "";

    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmString = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alProjectList = new ArrayList();
    ArrayList alResult = new ArrayList();

    // set the form values to hash map
    hmParam = GmCommonClass.getHashMapFromForm(gmPartNumPriceUpdateForm);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strProjId = GmCommonClass.parseNull((String) hmParam.get("HPROJECTID"));
    strProjId = strProjId.equals("") ? "1" : strProjId;
    strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    strColumn = GmCommonClass.parseNull((String) hmParam.get("HCOLUMN"));
    strColumn = (strColumn == null) ? "ID" : strColumn;
    strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    strMode = GmCommonClass.parseNull((String) hmParam.get("HMODE"));
    strLoad = GmCommonClass.parseNull((String) hmParam.get("HLOAD"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301");
    alProjectList = GmCommonClass.parseNullArrayList(gmProject.reportProject(hmTemp));
    hmReturn.put("PROJLIST", alProjectList);
    gmPartNumPriceUpdateForm.setHmReturn(hmReturn);

    // save the part price using By Project screen.
    if (strMode.equals("Save")) {

      hmString.put("INPSTR", strInputString);
      hmString.put("PROJECTID", strProjId);
      hmString.put("USERID", strUserId);
      hmString.put("HMODE", strMode);
      hmResult = GmCommonClass.parseNullHashMap(gmSales.savePartNumPricingUpdatation(hmString));
      strAction = "report";
    } else if (strMode.equals("Batch")) {
      // save the part price using Batch Update screen.
      strInputString = strInputString.replaceAll("\t", "^");
      strInputString = strInputString.replaceAll("\r\n", "|");
      strInputString = strInputString.replaceAll(",,,,,", "");
      strInputString = strInputString.concat("|");

      hmValues.put("PROJECTID", strProjId);
      hmValues.put("INPSTR", strInputString);
      hmValues.put("USERID", strUserId);
      hmValues.put("HMODE", strMode);
      hmResult = GmCommonClass.parseNullHashMap(gmSales.savePartNumPricingUpdatation(hmValues));
      strUploadParts = GmCommonClass.parseNull((String) hmResult.get("BATCH_UPLOAD_PARTS"));
      hmValues.put("BATCH_UPLOAD_PARTS", strUploadParts);
      // to fetch the upload parts details
      alResult = GmCommonClass.parseNullArrayList(gmSales.fetchUploadBatchParts(hmValues));
      gmPartNumPriceUpdateForm.setAlResult(alResult);
      strAction = "Batch";
      gmPartNumPriceUpdateForm.setStrOpt("Batch");
    }
    gmPartNumPriceUpdateForm.setHmResult(hmResult);

    if (strAction.equals("report")) {
      alReturn =
          GmCommonClass.parseNullArrayList(gmSales.reportPartNumPricing(strColumn, strProjId));
      log.debug("alReturn size=="+alReturn.size());
      if(alReturn.size() > 0) {
      strGridData = GmCommonClass.parseNull(getXmlGridData(alReturn, strSessCompanyLocale));
      }
      gmPartNumPriceUpdateForm.setStrGridData(strGridData);
      GmWSUtil gmWSUtil = new GmWSUtil();
      String  strJSON = GmCommonClass.parseNull((String) gmWSUtil.parseArrayListToJson(alReturn));
      log.debug("strJSON=="+strJSON);
      gmPartNumPriceUpdateForm.setStrJSONData(strJSON);
      gmPartNumPriceUpdateForm.setAlPartList(alReturn);
    }
    gmPartNumPriceUpdateForm.sethProjectId(strProjId);
    gmPartNumPriceUpdateForm.sethAction(strAction);
    gmPartNumPriceUpdateForm.sethMode(strMode);

    return mapping.findForward("GmPartPriceUpdate");
  }

  public String getXmlGridData(ArrayList alReturn, String strSessCompanyLocale) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alReturn);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmPartPricing", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPartPricing.vm");
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    return templateUtil.generateOutput();
  }
}
