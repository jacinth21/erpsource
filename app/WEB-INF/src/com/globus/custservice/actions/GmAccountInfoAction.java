package com.globus.custservice.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.forms.GmAccountInfoForm;

public class GmAccountInfoAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * getAccountInfo This method is used to Check if Account Id is available or not.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * 
   */
  public ActionForward getAccountInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    String strAccountId = GmCommonClass.parseNull(request.getParameter("AccId"));
    instantiate(request, response);
    GmAccountInfoForm gmAccountInfoForm = (GmAccountInfoForm) actionForm;
    gmAccountInfoForm.loadSessionParameters(request);
    GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
    strJSON = gmSalesBean.checkAccountDtls(strAccountId);
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return null;

  }

  public ActionForward validateAccountInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmAccountInfoForm gmAccountInfoForm = (GmAccountInfoForm) actionForm;
    gmAccountInfoForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAccountInfoForm);
    String strJSON = "";
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strSourceType = GmCommonClass.parseNull((String) hmParam.get("SOURCEID"));
    String strKey = "";
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strSearchDelimiter = gmAutoCompleteReportBean.strAutoSearchDelimiter;
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    strAccountId =
        ":" + strAccountId + strSearchDelimiter.substring(0, strSearchDelimiter.length() - 1);//
    // :11334[#]

    if (strSourceType.equals("50256")) { // ICT (Distributor)
      // 70103 - Inter-Company Transfer (ICT)
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("AR_DIST_LIST", "Y");

    } else if (strSourceType.equals("26240213")) { // Dealer
      // 26230725 Dealer
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("AR_DEALER_LIST", "Y");
      //

    } else {
      strKey =
          gmAutoCompleteReportBean.getAutoCompleteKeys("AR_REP_ACCOUNT_LIST" + strSourceType, "Y");
    }
    //
    log.debug(" strAccountId " + strAccountId + " Keys " + strKey);
    strJSON = gmCacheManager.returnAutoCompleteLikeSearchJSON(strKey, strAccountId, 100);
    log.debug(" Verify the data " + strJSON);


    if (!strJSON.equals("")) {
      response.setContentType("text/json");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return null;

  }
}
