/*----------------------------------------------
 * FileName : GmOrderTagRecordAction.java 
 * Description : Load OrderTag Record details,Save
 * Author      : N Raja
 *----------------------------------------------
 */


package com.globus.custservice.actions;

import org.apache.log4j.Logger;  
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.custservice.forms.GmOrderTagRecordForm;
import com.globus.custservice.beans.GmOrderTagRecordBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.util.GmTemplateUtil;
import com.globus.quality.forms.GmSetBundleMappingForm;


public class GmOrderTagRecordAction extends GmDispatchAction {
	
	// Code to Initialize the Logger Class.
	Logger log = GmLogger.getInstance(this.getClass().getName());

	 
	/**  fetchTagRecordInfo:
	   * @description fetchTagRecordInfo method to fetch Tag details
	   * @param actionMapping, form, request, response
	   * throws AppError
	   */
	  public ActionForward fetchTagRecordInfo(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
			  log.debug("Inside the fetchTagRecordInfo");
			  instantiate(request, response);
			  GmOrderTagRecordForm gmOrderTagRecordForm =(GmOrderTagRecordForm)actionForm;
			  gmOrderTagRecordForm.loadSessionParameters(request);
			  GmOrderTagRecordBean gmOrderTagRecordBean=new GmOrderTagRecordBean(getGmDataStoreVO());
			  ArrayList alFetchTagOrders = new ArrayList();
				HashMap hmParam = new HashMap();
				String strTagRefId = "";
				String strGridXmlData = "";
				String strSessCompanyLocale = "";
				String tempId = "";
				strSessCompanyLocale = GmCommonClass.parseNull((String) session
						.getAttribute("strSessCompanyLocale"));
				log.debug("strSessCompanyLocale==>> " + strSessCompanyLocale);
				strTagRefId = GmCommonClass.parseNull(gmOrderTagRecordForm
						.getTagRefId());
				gmOrderTagRecordForm.setTagRefId(strTagRefId);
				String strAccountId = gmOrderTagRecordForm.getAccountId();
				gmOrderTagRecordForm.setAccountId(strAccountId);
				if(strTagRefId.equals("")) {
					tempId =  GmCommonClass
							.parseNull(gmOrderTagRecordForm.getTempId());
					gmOrderTagRecordForm.setTempId(tempId);
					strTagRefId = tempId;
					}
					alFetchTagOrders = GmCommonClass
							.parseNullArrayList(gmOrderTagRecordBean
									.fetchTagOrders(strTagRefId));
				log.debug("alFetchTagOrders==>> " + alFetchTagOrders);
				
				hmParam.put("REPORT", alFetchTagOrders);
				hmParam.put("gmDataStoreVO", getGmDataStoreVO());
				hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
				hmParam.put("STRTAGREFID", strTagRefId);
				strGridXmlData = getXmlGridData(hmParam);
				request.setAttribute("GRIDXMLDATA", strGridXmlData);
			  return actionMapping.findForward("GmFetchOrderTag");
	   }	  
	  
	  /**
	   * @description cancelTagDetails method to remvoe tag details
	   * @param actionMapping, form, request, response
	   * throws AppError
	   */
	  public ActionForward cancelTagDetails(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
				  log.debug("Inside the cancelTagDetails");
				  instantiate(request, response);
				  GmOrderTagRecordForm gmOrderTagRecordForm =(GmOrderTagRecordForm)actionForm;
				  gmOrderTagRecordForm.loadSessionParameters(request);
				  GmOrderTagRecordBean gmOrderTagRecordBean=new GmOrderTagRecordBean(getGmDataStoreVO());
				  String strRemoveTagId = "";
		    	  String strUserId = "";
		    	  HashMap hmValues = new HashMap();
				  strRemoveTagId = GmCommonClass.parseNull((String)request.getParameter("tagInfoId"));
				  strUserId = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
				  hmValues.put("USERID", strUserId);
				  hmValues.put("TAGDOINFOID",strRemoveTagId);
				  gmOrderTagRecordBean.cancelTagDetails(hmValues);
				  return null;
				  
		   }
	  
	  
	  /**
	   * @description getXmlGridData for fetch AddTags data
	   * @param hmParam
	   * @return templateUtil
	   * @throws AppError
	   */
	  public String getXmlGridData(HashMap hmParam) throws AppError {
		  log.debug("hmParam "+hmParam);
			GmTemplateUtil templateUtil = new GmTemplateUtil();
			String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
			templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("REPORT"));
			
			templateUtil.setDataMap("hmParam", hmParam);
			templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.custservice.GmOrderTagRecord", strSessCompanyLocale));
			templateUtil.setTemplateName("GmAddingTagRecord.vm");
			templateUtil.setTemplateSubDir("custservice/templates");
			
			return templateUtil.generateOutput();
	  }
	  
	  /**
	   * @description processRecordTags method to save Tags details
	   * @param actionMapping, form, request, response
	   * @throws AppError
	   */
	  public ActionForward processRecordTags(ActionMapping actionMapping,
				ActionForm actionForm, HttpServletRequest request,
				HttpServletResponse response) throws AppError, Exception {

			log.debug("Inside the processRecordTags");
			 instantiate(request, response);
			GmOrderTagRecordForm gmOrderTagRecordForm = (GmOrderTagRecordForm) actionForm;
			gmOrderTagRecordForm.loadSessionParameters(request);
			GmOrderTagRecordBean gmOrderTagRecordBean = new GmOrderTagRecordBean(
					getGmDataStoreVO());
			 String strAccountId = "";
			  String strRefDoId = "";
			  String strSetId = "";
			  String strForward = "";
			  String strUserId = "";
			  String strOrderTagId = "";
			  String strTagId = "";
			  String strTgId = "";
			  String tempId = "";
			ArrayList alFetchTagOrders = new ArrayList();
			HashMap hmParam = new HashMap();
			HashMap hmOutParam = new HashMap();
			strTagId = GmCommonClass.parseNull(gmOrderTagRecordForm.getTagId());
			strTgId = strTagId + ",";
			 strAccountId = GmCommonClass.parseNull(gmOrderTagRecordForm.getAccountId());
			 strRefDoId = GmCommonClass.parseNull(gmOrderTagRecordForm.getTagRefId());
			 strSetId = GmCommonClass.parseNull(gmOrderTagRecordForm.getSetId());
			 strUserId = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
			 if(strRefDoId.equals("")) {
					tempId =  GmCommonClass
							.parseNull(gmOrderTagRecordForm.getTempId());
					log.debug("tempId"+tempId);
					strRefDoId = tempId;
					log.debug("strRefDoId"+strRefDoId);
					}
			 hmParam =(HashMap) GmCommonClass.getHashMapFromForm(gmOrderTagRecordForm);
			 
			 hmParam.put("REF_ID",strRefDoId);
			 hmParam.put("ACCOUNTID",strAccountId);
			 hmParam.put("USERID", strUserId);
			 hmParam.put("SETID", strSetId);
			    hmOutParam = gmOrderTagRecordBean.saveOrderTagDetails(hmParam);
			    strOrderTagId = GmCommonClass.parseNull((String)hmOutParam.get("ORDARTAGID"));
			    strAccountId = GmCommonClass.parseNull((String)hmOutParam.get("ACCOUNTID"));
			   	strForward = "/gmOrderTagRecordAction.do?method=fetchTagRecordInfo&tagRefId="+strOrderTagId+"&accountId="+strAccountId;
			    log.debug("strForward"+strForward);
			    return actionRedirect(strForward, request);
			} 
	  
	  /**
	   * @description updateTagDetails method to update Y for valid Tags
	   * @param actionMapping, form, request, response
	   * throws Exception
	   */
	  public ActionForward updateTagDetails(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
				  log.debug("Inside the updateTagDetails");
				  instantiate(request, response);
				  GmOrderTagRecordForm gmOrderTagRecordForm =(GmOrderTagRecordForm)actionForm;
				  gmOrderTagRecordForm.loadSessionParameters(request);
				  GmOrderTagRecordBean gmOrderTagRecordBean=new GmOrderTagRecordBean(getGmDataStoreVO());
				  HashMap hmValues = new HashMap();
				  String strRefDoId = GmCommonClass.parseNull(gmOrderTagRecordForm.getTagRefId());
				  String strUserId = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
				  hmValues.put("ORDERID", strRefDoId);
				  hmValues.put("USERID", strUserId);
				  gmOrderTagRecordBean.updateTagDetails(hmValues);
				  return null;
				  
		   }
	  
	  /**
	   * @description updateTagDetails method to update Y for valid Tags
	   * @param actionMapping, form, request, response
	   * throws Exception
	   */
	  public ActionForward getOrderId(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
				  log.debug("Inside the getOrderId");
				  instantiate(request, response);
				  GmOrderTagRecordForm gmOrderTagRecordForm =(GmOrderTagRecordForm)actionForm;
				  gmOrderTagRecordForm.loadSessionParameters(request);
				  GmOrderTagRecordBean gmOrderTagRecordBean=new GmOrderTagRecordBean(getGmDataStoreVO());
				  HashMap hmValues = new HashMap();
				  String tempId = gmOrderTagRecordBean.getOrderId(hmValues);
				  gmOrderTagRecordForm.setTempId(tempId);
				  String strRefDoId = GmCommonClass.parseNull(gmOrderTagRecordForm.getTempId());
				  response.setContentType("text/json");
				    PrintWriter pw = response.getWriter();
				    if (!tempId.equals("")) {
			             pw.write(tempId);
				     }
			             pw.flush();
			             return null;
				  
		   }
		 
}
