package com.globus.custservice.actions;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.forms.GmDOReportForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmDOReportAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
  public static final String TEMPLATE_NAME = "GmDeliveredOrderForm";
  HashMap hmParam = new HashMap();

  public ActionForward loadDODashboard(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDOReportForm gmDOReportForm = (GmDOReportForm) form;
    gmDOReportForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    ArrayList alResult = new ArrayList();
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strAccessCondition = getAccessFilter(request, response);
    hmParam = GmCommonClass.getHashMapFromForm(gmDOReportForm);
    hmParam.put("DEPTID", strDepartMentID);
    hmParam.put("AccessFilter", strAccessCondition);
    hmParam.put("APPDATEFMT", strApplDateFmt);
    alResult = gmDOBean.fetchDODashboard(hmParam);
    log.debug("alResult size" + alResult.size());
    gmDOReportForm.setAlResult(alResult);
    return mapping.findForward("success");
  }

  public ActionForward openEmailOrderSummary(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDOReportForm gmDOReportForm = (GmDOReportForm) form;
    gmDOReportForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmDOReportForm);

    String strOrderID = GmCommonClass.parseNull(gmDOReportForm.getOrderID());

    return mapping.findForward("email");

  }

  public ActionForward emailOrderSummary(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmDOReportForm gmDOReportForm = (GmDOReportForm) form;
    instantiate(request, response);
    HashMap hmResult = new HashMap();
    HashMap hmOrderDetails = new HashMap();
    HashMap hmOrderSummary = new HashMap();
    StringBuffer sbMessage = new StringBuffer();
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    HttpSession session = request.getSession(false);
    gmDOReportForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmDOReportForm);

    String strOpt = GmCommonClass.parseNull(gmDOReportForm.getStrOpt());

    if (strOpt.equals("reload")) {
      log.debug("insidereload");
      String strOrderID = GmCommonClass.parseNull(gmDOReportForm.gethOrderID());
      String strToAdd = GmCommonClass.parseNull((String) hmParam.get("TO"));
      String strCcAdd = GmCommonClass.parseNull((String) hmParam.get("CC"));
      String strCaseID = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
      String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
      String strAccountNm = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTNM"));
      sbMessage.append("DO#:  " + strOrderID + "<br>");
      sbMessage.append("Case ID:  " + strCaseID + "<br>");
      sbMessage.append("Account ID:  " + strAccountID + "<br>");
      sbMessage.append("Account Name:  " + strAccountNm + "<br>");
      String strAddMoreMessage = GmCommonClass.parseNull(gmDOReportForm.getAddMoreMessage());
      String strMessage = sbMessage.toString().concat(strAddMoreMessage);
      log.debug("strMessage==>" + strMessage);
      hmResult = gmCustomerBean.loadOrderDetails(strOrderID);
      String strFilePath = GmCommonClass.getString("UPLOADHOME");
      String fileName = strFilePath + strOrderID + ".pdf";
      File newFile = new File(strFilePath);
      if (!newFile.exists()) {
        newFile.mkdir();
      }

      hmOrderSummary = gmDOBean.fetchDOMasterDetails(hmResult);
      hmOrderSummary.put(
          "SUBREPORT_DIR",
          request.getSession().getServletContext()
              .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
              + "\\");
      String strhidePratPrice = GmCommonClass.parseNull(gmDOReportForm.getHaction());
      String strDeptID = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
      log.debug("strhidePratPrice" + strhidePratPrice);
      hmOrderSummary.put("HIDEPRICE", strhidePratPrice);
      hmOrderSummary.put("DEPTID", strDeptID);
      GmJasperReport gmJasperReport = new GmJasperReport();
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setJasperReportName("/GmDeliveredOrderForm.jasper");
      gmJasperReport.setHmReportParameters(hmOrderSummary);
      gmJasperReport.setReportDataList(null);
      gmJasperReport.exportJasperReportToPdf(fileName);
      log.debug("fileName = " + fileName);
      GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
      String strSubject = gmEmailProperties.getSubject();
      strSubject = GmCommonClass.replaceAll(strSubject, "#<ORDER_NO>", strOrderID);
      gmEmailProperties.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
          + GmEmailProperties.FROM));
      gmEmailProperties.setSubject(strSubject);
      gmEmailProperties.setMessage(strMessage);
      gmEmailProperties.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
          + GmEmailProperties.MIME_TYPE));
      gmEmailProperties.setAttachment(fileName);
      gmEmailProperties.setRecipients(strToAdd);
      gmEmailProperties.setCc(strCcAdd);
      GmCommonClass.sendMail(gmEmailProperties);
      File newPDFFile = new File(fileName);
      newPDFFile.delete();
      request.setAttribute("hType", "S");
      throw new AppError("", "20688");
    }
    return mapping.findForward("email");
  }

  public GmEmailProperties getEmailProperties(String strTemplate) {
    GmEmailProperties emailProps = new GmEmailProperties();
    emailProps
        .setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
    emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    // emailProps.setMessageFooter(GmCommonClass.getEmailProperty(strTemplate + "." +
    // GmEmailProperties.MESSAGE_FOOTER));
    return emailProps;
  }

  public ActionForward printDOBooking(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmDOReportForm gmDOReportForm = (GmDOReportForm) form;
    instantiate(request, response);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    gmDOReportForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    HashMap hmResult = new HashMap();
    HashMap hmOrderDetails = new HashMap();
    HashMap hmOrderSummary = new HashMap();
    String strOrdId = gmDOReportForm.getOrderID();
    hmResult = gmCustomerBean.loadOrderDetails(strOrdId);
    hmOrderSummary = gmDOBean.fetchDOMasterDetails(hmResult);
    log.debug("hmOrderSummary==>" + hmOrderSummary);
    hmOrderSummary.put(
        "SUBREPORT_DIR",
        request.getSession().getServletContext()
            .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
            + "\\");
    String strhidePratPrice = GmCommonClass.parseNull(gmDOReportForm.getHaction());
    String strDeptID = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    log.debug("strhidePratPrice" + strhidePratPrice);
    hmOrderSummary.put("HIDEPRICE", strhidePratPrice);
    hmOrderSummary.put("DEPTID", strDeptID);
    log.debug("begin  of the report");
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    ServletOutputStream servletOutputStream = response.getOutputStream();
    InputStream reportStream;
    JasperPrint jp = null;
    response.setHeader("Content-Disposition", "inline; filename=\"file2.pdf\"");
    response.setContentType("application/pdf");
    reportStream =
        request.getSession().getServletContext()
            .getResourceAsStream(strJasperPath + "/GmDeliveredOrderForm.jasper");
    JRPdfExporter exporter = new JRPdfExporter();
    jp = JasperFillManager.fillReport(reportStream, hmOrderSummary, new JREmptyDataSource());
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
    exporter.setParameter(JRPdfExporterParameter.METADATA_TITLE,
        "GlobusOne Enterprise Portal");
    exporter.exportReport();
    servletOutputStream.close();
    return mapping.findForward("");
  }

}
