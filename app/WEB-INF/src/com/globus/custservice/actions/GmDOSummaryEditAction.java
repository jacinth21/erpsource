package com.globus.custservice.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.forms.GmDOSummaryEditForm;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.party.beans.GmAddressBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;


public class GmDOSummaryEditAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing



  public ActionForward editTagDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDOSummaryEditForm gmDOSummaryEditForm = (GmDOSummaryEditForm) form;
    GmDORptBean gmDORptBean = new GmDORptBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    ArrayList alTagID = new ArrayList();
    HashMap hmResult = new HashMap();
    gmDOSummaryEditForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmDOSummaryEditForm);
    String strOrderID = GmCommonClass.parseNull(gmDOSummaryEditForm.getOrderID());
    String strOpt = GmCommonClass.parseNull(gmDOSummaryEditForm.getStrOpt());
    String strFwd = "EditTag";
    String strAccountID = GmCommonClass.parseNull(gmDOSummaryEditForm.getAccountID());
    String strCaseInfoID = GmCommonClass.parseNull(gmDOSummaryEditForm.getCaseInfoID());

    // Fetching hte order source attribute value.
    hmResult.put("ORDERID", strOrderID);
    hmResult.put("ATTRIBTYPE", "103560"); // 103560 - Order source attribute type
    String strOrderSrc = GmCommonClass.parseNull(gmDORptBean.fetchOrderAttribute(hmResult));

    ArrayList alTagDetails =
        GmCommonClass.parseNullArrayList(gmDOBean.fetchDOTagDetails(strOrderID));

    // When DO booked from DO App, we will not have any tag details saved using the case id
    // reference.
    // So taking the tag details by passing the order id reference.
    if (strOrderSrc.equals("103521") && strCaseInfoID.equals("")) {
      alTagID = GmCommonClass.parseNullArrayList(gmDOBean.fetchCaseTagID(strOrderID));
    } else {
      alTagID = GmCommonClass.parseNullArrayList(gmDOBean.fetchCaseTagID(strCaseInfoID));
    }
    HashMap hmReturn = gmCustomerBean.getAccSummary(strAccountID);

    if (strOpt.equals("Save")) {
      // update the Tag Details
      gmDOBean.updateDOTagDetail(hmParam);

      request.setAttribute("hOrdId", strOrderID);
      request.setAttribute("hParantForm", "CASE");
      return mapping.findForward("gmEditOrderServlet");
    }
    gmDOSummaryEditForm.setAlTagDetails(alTagDetails);
    gmDOSummaryEditForm.setAlTagID(alTagID);
    request.setAttribute("hmResult", hmReturn);
    return mapping.findForward(strFwd);
  }

  public ActionForward editShipDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDOSummaryEditForm gmDOSummaryEditForm = (GmDOSummaryEditForm) form;
    gmDOSummaryEditForm.loadSessionParameters(request);
    GmDOBean gmDOBean = new GmDOBean();
    ArrayList alAddress = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAddressBean gmAddressBean = new GmAddressBean(getGmDataStoreVO());
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    HashMap hmOrdDetails = new HashMap();
    HashMap hmShipInfo = new HashMap();
    HashMap hmNames = new HashMap();
    HashMap hmShipModes = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alShipDetails = new ArrayList();
    HashMap hmValues = new HashMap();
    String strInactiveFl = "";
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmDOSummaryEditForm);
    String strOpt = GmCommonClass.parseNull(gmDOSummaryEditForm.getStrOpt());
    String strOrderID = GmCommonClass.parseNull(gmDOSummaryEditForm.getOrderID());
    String strParentOrderID = GmCommonClass.parseNull(gmDOSummaryEditForm.getParentOrderID());
    String strAccountID = GmCommonClass.parseNull(gmDOSummaryEditForm.getAccountID());
    String strCarrier = "";
    String strMode = "";
    String strDefMode = "";
    HashMap hmReturn = gmCustomerBean.getAccSummary(strAccountID);
    if (strOpt.equals("save")) {
      gmDOBean.saveDOShippInfo(hmParam);
      // After saving the address for child orders, we need to load the parent order summary
      // details.
      // So setting the respective order id's in request attribute.
      if (!strParentOrderID.equals("")) {
        request.setAttribute("hOrdId", strParentOrderID);
      } else {
        request.setAttribute("hOrdId", strOrderID);
      }
      request.setAttribute("hParantForm", "CASE");
      return mapping.findForward("gmEditOrderServlet");
    }
    hmResult = gmCustomerBean.loadOrderDetails(strOrderID);
    hmOrdDetails = (HashMap) hmResult.get("ORDERDETAILS");
    alShipDetails = (ArrayList) hmResult.get("SHIPSUMMARY");
    for (Iterator iter = alShipDetails.iterator(); iter.hasNext();) {
      hmValues = (HashMap) iter.next();
      strCarrier = GmCommonClass.parseNull((String) hmValues.get("SCARRID"));
      strMode = GmCommonClass.parseNull((String) hmValues.get("SMODEID"));
    }
    String strRepId = GmCommonClass.parseNull((String) hmOrdDetails.get("REPID"));

    alAddress = gmAddressBean.fetchPartyAddress(strRepId, strInactiveFl);
    log.debug("alAddress ==>" + alAddress);
    gmDOSummaryEditForm.setAlRepAdd(alAddress);

    hmShipInfo = gmShippingInfoBean.loadShipping();
    gmDOSummaryEditForm.setAlShipTo((ArrayList) hmShipInfo.get("SHIPTO"));
    if (!strCarrier.equals("")) {
      hmShipModes =
          GmCommonClass.parseNullHashMap(gmShippingInfoBean.fetchCarrierBasedModes(strCarrier, ""));
      gmDOSummaryEditForm.setAlShipMode((ArrayList) hmShipModes.get("SHIMODELIST"));
      strDefMode = GmCommonClass.parseNull((String) hmShipModes.get("DEFAULTMODE"));
      strDefMode = strMode.equals("") ? strDefMode : strMode;
      gmDOSummaryEditForm.setShipMode(strDefMode);
    } else {
      gmDOSummaryEditForm.setAlShipMode((ArrayList) hmShipInfo.get("MODE"));
    }
    hmNames = (HashMap) hmShipInfo.get("NAMES");
    gmDOSummaryEditForm.setHmNames(hmNames);
    gmDOSummaryEditForm.setAlShipToId((ArrayList) hmNames.get("REPLIST"));
    alLogReasons = gmCommonBean.getLog(strOrderID, "1200");
    gmDOSummaryEditForm.setAlLogReasons(alLogReasons);
    request.setAttribute("hmResult", hmResult);
    request.setAttribute("hmReturn", hmReturn);
    return mapping.findForward("EditShip");
  }

  public ActionForward editMultipleShipDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDOSummaryEditForm gmDOSummaryEditForm = (GmDOSummaryEditForm) form;
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmDOSummaryEditForm);
    String strAccountID = GmCommonClass.parseNull(gmDOSummaryEditForm.getAccountID());
    String strOrderID = GmCommonClass.parseNull(gmDOSummaryEditForm.getOrderID());
    String strOpt = GmCommonClass.parseNull(gmDOSummaryEditForm.getStrOpt());
    if (strOpt.equals("save")) {
      gmDOBean.saveMultipleShippInfo(hmParam);
      request.setAttribute("hOrdId", strOrderID);
      request.setAttribute("hParantForm", "CASE");
      return mapping.findForward("gmEditOrderServlet");
    }
    HashMap hmReturn = gmCustomerBean.getAccSummary(strAccountID);
    hmResult = gmCustomerBean.loadOrderDetails(strOrderID);
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();
    GmAddressBean gmAddressBean = new GmAddressBean();
    HashMap hmShipInfo = new HashMap();
    HashMap hmNames = new HashMap();
    hmShipInfo = gmShippingInfoBean.loadShipping();
    gmDOSummaryEditForm.setAlShipTo((ArrayList) hmShipInfo.get("SHIPTO"));// sales rep
    hmNames = (HashMap) hmShipInfo.get("NAMES");
    gmDOSummaryEditForm.setAlShipToId((ArrayList) hmNames.get("REPLIST"));
    alResult = gmDOBean.fetchPartDetails(strOrderID);
    request.setAttribute("hmResult", hmResult);
    request.setAttribute("hmReturn", hmReturn);
    return mapping.findForward("EditMultipleShip");
  }

  public ActionForward editControlNumberDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDOSummaryEditForm gmDOSummaryEditForm = (GmDOSummaryEditForm) form;
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean=new GmInvLocationBean();
    gmDOSummaryEditForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmAcctParam = new HashMap ();	
    hmParam = GmCommonClass.getHashMapFromForm(gmDOSummaryEditForm);
    String strErrorMsg = "";
    String strOrderID = GmCommonClass.parseNull(gmDOSummaryEditForm.getOrderID());
    String strOpt = GmCommonClass.parseNull(gmDOSummaryEditForm.getStrOpt());
    String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    String strFormName = GmCommonClass.parseNull(request.getParameter("FRMNAME"));
    HashMap hmLotOverrideAccess = new HashMap();
    String strLotOverrideUpdAccess = "";
    String strLotOrderideAcess = "";
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strUserId = GmCommonClass.parseNull(gmDOSummaryEditForm.getUserId());
    hmLotOverrideAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "LOT_OVERRIDE_CHECK"));
    strLotOverrideUpdAccess = GmCommonClass.parseNull((String) hmLotOverrideAccess.get("UPDFL"));
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
    
    if (strLotOverrideUpdAccess.equals("Y")) {
      strLotOrderideAcess = "Y";
    }
    request.setAttribute("LOTOVERRIDEACCESS", strLotOrderideAcess);

    log.debug("strHaction---------7777777" + strFormName);
    String strAccountID = GmCommonClass.parseNull(gmDOSummaryEditForm.getAccountID());
    String strCaseInfoID = GmCommonClass.parseNull(gmDOSummaryEditForm.getCaseInfoID());
    hmResult = gmCustomerBean.loadOrderDetails(strOrderID);
    HashMap hmReturn = gmCustomerBean.getAccSummary(strAccountID);
    ArrayList alControlNumberDetails = new ArrayList();

    if (strFormName.equals("frmVendor")) {
      alControlNumberDetails =
          GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("ALFILTEREDCNUMDETAILS"));
    } else {
      alControlNumberDetails =
          GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("ALCONTROLNUMDETAILS"));
    }

    gmDOSummaryEditForm.setAlCtrlNmDetails(alControlNumberDetails);

    if (strOpt.equals("Save") || strOpt.equals("EditOrder")) {
      try {
        gmDOBean.updateControlNumberDetail(hmParam, null);
        request.setAttribute("hOrdId", strOrderID);
        request.setAttribute("hParantForm", "CASE");
      
        // JMS call to update the lot number from the Edit Lot screen in Do summary page.
        // PMT 14376
        hmAcctParam.put("TRANS_ID", strOrderID);	 	 	 
        hmAcctParam.put("TRANS_TYPE", "50180");	 	 	 
        hmAcctParam.put("WAREHOUSE_TYPE", "5");	
        hmAcctParam.put("USER_ID", strUserId);	   
        hmAcctParam.put("COMPANY_INFO",strCompanyInfo);
        gmInvLocationBean.updateLoanerStockSheetInfo(hmAcctParam); 
    
        
      } catch (Exception e) {
        // MNTTASK-8623 - If any error validate qty procedure and set error MSG to Control Number
        // Edit screen.
        // if(e.getMessage().indexOf("20999") != -1){
        strErrorMsg =
            e.getMessage().substring(e.getMessage().indexOf("^") + 1, e.getMessage().length());
        request.setAttribute("ERRMSG", strErrorMsg);
        request.setAttribute("hmReturn", hmResult);
        return mapping.findForward("EditControlNumber");
        // }
      }
      if (strOpt.equals("EditOrder")) {
        request.setAttribute("hMsgFl", "Y");
        return mapping.findForward("EditOrder");
      } else {
        String strForward =
            "//GmEditOrderServlet?hMode=PrintPrice&hParantForm=CASE&hOrdId=" + strOrderID;
        return actionRedirect(strForward,request);
      }
    }
    request.setAttribute("hmResult", hmReturn);
    request.setAttribute("hmReturn", hmResult);
    return mapping.findForward("EditControlNumber");
  }
}
