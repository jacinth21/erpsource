package com.globus.custservice.actions;


import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.actions.GmDispatchAction;
import com.globus.custservice.beans.GmOrderTissuePartDtlsBean;
import com.globus.custservice.forms.GmOrderTissuePartDtlsForm;
import com.globus.prodmgmnt.beans.GmSystemPublishReportBean;
import com.globus.prodmgmnt.forms.GmSystemDetailForm;


/**
 * @author T.S. Ramachandiran
 *
 */

public class GmOrderTissuePartDtlsAction extends GmDispatchAction {
	
	/**
	 *  This used to get the instance of this class for enabling logging 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**  Method - fetchOrderTissueAddressDtls:
	   * @description fetchOrderTissueAddressDtls method to fetch hospital address details if order has tissue parts
	   * @param actionMapping, form, request, response
	   * throws Exception
	   */
	
	  public ActionForward fetchOrderTissueAddressDtls(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		 
			  instantiate(request, response);
			  GmOrderTissuePartDtlsForm gmOrderTissuePartDtlsForm = (GmOrderTissuePartDtlsForm) actionForm;
			  gmOrderTissuePartDtlsForm.loadSessionParameters(request);
			  GmOrderTissuePartDtlsBean gmOrderTissuePartDtlsBean = new GmOrderTissuePartDtlsBean(getGmDataStoreVO());
			 
			  String accid = "";
			  String strResponse = "";
			  String strAddress  = "";
			  String strMessage = "";
			  HashMap hmparam = new HashMap();
	         
			  accid = GmCommonClass.parseNull((String) gmOrderTissuePartDtlsForm
	  				.getStrAccountId());
			  hmparam = gmOrderTissuePartDtlsBean.getOrderTissueAddressDtls(accid);
			  strAddress = GmCommonClass.parseNull((String)hmparam.get("STRADDRESS"));
			  strMessage = GmCommonClass.parseNull((String)hmparam.get("STRMESSAGE"));
			  strResponse = strMessage+"^^"+strAddress;
	          response.setContentType("text/plain");
			  if (!strAddress.equals("")) {
				  PrintWriter pw = response.getWriter();
				  pw.write(strResponse);
	    	      pw.flush();
	    	      return null;
			   }
			 return null;
	  }
	  
	  
	  /**  Method - fetchDOConfirmTissueAddressDtls:
	   * @description fetchDOConfirmTissueAddressDtls method to fetch hospital address details if DO has tissue parts
	   * @param actionMapping, form, request, response
	   * throws Exception
	   */
	
	  public ActionForward fetchDOConfirmTissueAddressDtls(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			  HttpServletResponse response)throws Exception{
		  
		  instantiate(request, response);
		  GmOrderTissuePartDtlsForm gmOrderTissuePartDtlsForm = (GmOrderTissuePartDtlsForm) actionForm;
		  gmOrderTissuePartDtlsForm.loadSessionParameters(request);
		  GmOrderTissuePartDtlsBean gmOrderTissuePartDtlsBean = new GmOrderTissuePartDtlsBean(getGmDataStoreVO());
		  
		  String strTxnId = "";
		  String strResponse = "";
		  String strMessage = "";
		  String strAddress  = "";
		  HashMap hmparam = new HashMap();	  
		  strTxnId = GmCommonClass.parseNull((String) gmOrderTissuePartDtlsForm
	  				.getStrTxnId());
		  hmparam = gmOrderTissuePartDtlsBean.getDOTissueAddress(strTxnId);
		  strAddress = GmCommonClass.parseNull((String)hmparam.get("STRADDRESS"));
		  strMessage = GmCommonClass.parseNull((String)hmparam.get("STRMESSAGE"));
		  strResponse = strMessage+"^^"+strAddress;
		  response.setContentType("text/plain");
		  if (!strAddress.equals("")) {
			  PrintWriter pw = response.getWriter();
			  pw.write(strResponse);
    	      pw.flush();
    	      return null;
		   }
		  return null;
	  }
	  
}
