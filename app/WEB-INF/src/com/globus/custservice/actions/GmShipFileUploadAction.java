package com.globus.custservice.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmShipFileUploadForm;
import com.globus.custservice.beans.GmCustomerBean;

public class GmShipFileUploadAction extends GmDispatchAction {

  private static String strComnPath = GmCommonClass.getString("GMCOMMON");

  /**
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward uploadFiles(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    HttpSession session = request.getSession(false);
    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    GmShipFileUploadForm gmShipFileUploadForm = (GmShipFileUploadForm) actionForm;
    gmShipFileUploadForm.loadSessionParameters(request);
    String strOpt = GmCommonClass.parseNull(gmShipFileUploadForm.getStrOpt());
    String strRefID = GmCommonClass.parseNull(gmShipFileUploadForm.getStrRefID());
    String strRefType = GmCommonClass.parseNull(gmShipFileUploadForm.getRefType());
    String strUserID = GmCommonClass.parseNull(gmShipFileUploadForm.getUserId());
    String strUploadHome =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("FILE", "MAILATTACHMENT"));
    String strApplnDateFmt =
        GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    String strForward = "UploadFiles";
    String strMessage = "";
    String strFileName = "";
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alDetails = new ArrayList();
    ArrayList alLogReason = new ArrayList();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    hmParam = GmCommonClass.getHashMapFromForm(gmShipFileUploadForm);

    // When got common error from upload screen and click back to previous screen then it should go
    // to upload screen.
    String strRedirectURL = GmCommonClass.parseNull(request.getParameter("hRedirectURL"));
    String strDisplayNm = GmCommonClass.parseNull(request.getParameter("hDisplayNm"));
    request.setAttribute("hRedirectURL", strRedirectURL);
    request.setAttribute("hDisplayNm", strDisplayNm);

    if (strOpt.equals("Validate")) {
      strFileName = gmShipFileUploadForm.getFileName();
      strFileName = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
      String strFlag = gmCommonUploadBean.validateFileNameSts(strRefID, strRefType, strFileName);
      hmParam.put("FILENAME", strFileName);
      hmParam.put("STRREFID", strRefID);
      hmParam.put("REFTYPE", strRefType);
      GmCommonClass.getFormFromHashMap(gmShipFileUploadForm, hmParam);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strFlag);
      pw.flush();
      pw.close();
      return null;
    }
    if (strOpt.equals("Upload")) {
      FormFile strFile = gmShipFileUploadForm.getFile();
      strFileName = strFile.getFileName();
      if (!strFileName.equals("")) {
        gmCommonUploadBean.validateFile(strFile);
        try {
          strUploadHome = strUploadHome + strRefID;
          String strFileName1 = strUploadHome + "\\" + strFileName;
          gmCommonUploadBean.uploadFile(strFile, strUploadHome, strFileName1);
        } catch (Exception e) {
          throw new AppError("", "20675");
        }
        strFileName = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
        gmCommonUploadBean.saveUploadUniqueInfo(strRefID, "4000715", strFileName, strUserID);
        if (!strRefID.equals("")) {
          strMessage = "File Uploaded Successfully";
        }
        gmShipFileUploadForm.setMessage(strMessage);
      }
      hmParam.put("MESSAGE", strMessage);
    }
    GmCommonClass.getFormFromHashMap(gmShipFileUploadForm, hmParam);
    return actionMapping.findForward(strForward);
  }

  public ActionForward attachFiles(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    HttpSession session = request.getSession(false);
    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmShipFileUploadForm gmShipFileUploadForm = (GmShipFileUploadForm) actionForm;
    gmShipFileUploadForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmShipFileUploadForm);
    String strOpt = GmCommonClass.parseNull(gmShipFileUploadForm.getStrOpt());
    String strRefID = GmCommonClass.parseNull(gmShipFileUploadForm.getStrRefID());
    String strForward = "UploadFiles";
    HashMap hmAccess = new HashMap();
    String strPartyId = (String) session.getAttribute("strSessPartyId");
    if (strOpt.equals("validateTrans")) {
      GmCustomerBean gmCust = new GmCustomerBean();
      StringBuffer sbXML = new StringBuffer();
      String strOrderCount = "";
      strRefID = GmCommonClass.parseNull(gmShipFileUploadForm.getStrRefID());
      strOrderCount = GmCommonClass.parseNull(gmCust.checkOrderId(strRefID));
      sbXML.append(strOrderCount);
      response.setContentType("text/xml;charset=utf-8");
      response.setHeader("Cache-Control", "no-cache");
      PrintWriter writer = response.getWriter();
      writer.println(sbXML.toString());
      log.debug("sbXML.toString()::" + sbXML.toString());
      writer.close();
    }
    return actionMapping.findForward(strForward);
  }
}
