package com.globus.custservice.actions;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmCreditHoldEngine;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmCustomerExtnBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmDOReportBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.custservice.forms.GmOrderDetailsForm;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmOrderDetailsAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
  Logger logOrder = GmLogger.getInstance("globusapporder");
  HashMap hmParam = new HashMap();
  HashMap hmResult = new HashMap();
  ArrayList alReturn = new ArrayList();


  public ActionForward saveUsageDeatils(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
    gmOrderDetailsForm.loadSessionParameters(request);
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    //GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmCustomerExtnBean gmCustExtn = new GmCustomerExtnBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmTxnSplitBean txnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());

    HttpSession session = request.getSession(false);
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strClientSysType =
        GmCommonClass.parseNull((String) session.getAttribute("strSessClientSysType"));
    String strOrderId = "";
    String strParentOrderID = "";
    String strOpt = GmCommonClass.parseNull(gmOrderDetailsForm.getStrOpt());
    String strTodaysDate = GmCalenderOperations.getCurrentDate("yyMMdd");
    String strTodayDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
    String strAccountID = GmCommonClass.parseNull(gmOrderDetailsForm.getAccountID());
    String strRepId = GmCommonClass.parseNull(gmOrderDetailsForm.getRepID());
    String strCaseInfiID = gmOrderDetailsForm.getCaseInfoID();
    String strUserID = GmCommonClass.parseNull(gmOrderDetailsForm.getUserId());
    String strEGPSUsage = GmCommonClass.parseNull(gmOrderDetailsForm.getCboegpsusage());  //strEGPSUsage in PC-4892 - EGPS in Edit order and Add EGPS in case booking
    log.debug("strEGPSUsage==>" + strEGPSUsage);
    log.debug("strCaseInfiID==>" + strCaseInfiID);
    if (!strOpt.equals("save")) {
      hmResult = GmCommonClass.parseNullHashMap(gmDOBean.validateNewOrder(strCaseInfiID));
      log.debug("hmResult==>" + hmResult);
      strParentOrderID = GmCommonClass.parseNull((String) hmResult.get("PARENTID"));
      if (!strParentOrderID.equals("")) {
        gmOrderDetailsForm.setParentOrderID(strParentOrderID);
        strOrderId = GmCommonClass.parseNull((String) hmResult.get("ORDERID"));
        gmOrderDetailsForm.setOrderID(strOrderId);
      }
    }
    alReturn = GmCommonClass.getCodeList("SRNO");
    gmOrderDetailsForm.setAlSurgeryNo(alReturn);
    alReturn = GmCommonClass.getCodeList("USAGE");
    gmOrderDetailsForm.setAlUsage(alReturn);
    alReturn = GmCommonClass.getCodeList("ORDTP");
    gmOrderDetailsForm.setAlSetType(alReturn);
    
    alReturn = GmCommonClass.getCodeList("EGPUSE");  //EGPUSE  in PC-4892 - EGPS in Edit order and Add EGPS in case booking
    gmOrderDetailsForm.setAlEGPSUsage(alReturn);
    
    // gmOrderDetailsForm.setSurgeryDt(GmCalenderOperations.getDate(strTodayDate));
    //HashMap hmReturn = gmCustomerBean.getAccSummary(strAccountID);
    HashMap hmReturn = gmCustExtn.getAccSummary(strAccountID);
    log.debug("hmReturn==>" + hmReturn);
    if (hmReturn == null || hmReturn.size() == 0) {
      throw new AppError("", "20689");
    }
    gmOrderDetailsForm.setHmResult(hmReturn);
    // toArray Search
    alReturn = gmDOBean.fetchCasePartDetails(strCaseInfiID);
    log.debug("alReturn==>" + alReturn);
    gmOrderDetailsForm.setAlReturn(alReturn);
    // end
    alReturn = txnSplitBean.fetchRulesLookup("NONUSAGE");
    gmOrderDetailsForm.setAlNonUsage(alReturn);

    hmParam = GmCommonClass.getHashMapFromForm(gmOrderDetailsForm);

    if (strOpt.equals("save")) {
      strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
      String strQuoteStr = "";//strQuoteStr  in PC-4892 - EGPS in Edit order and Add EGPS in case booking
      strQuoteStr = strQuoteStr +  "^" + 26241315 + "^" + strEGPSUsage + "|";
      hmParam.put("ORDERID", strOrderId);
      hmParam.put("USERID", strUserID);
      hmParam.put("ORDERATTRSTR", strQuoteStr);
      
      gmDOBean.saveOrderUsageDetails(hmParam);
      if (strClientSysType.equals("IPAD")) {
        // Redirect to next page instead of forwarding. to avoid refresh issue after save.
        String url = "/gmOrderDetails.do?method=loadDOActions&&hOrdId=" + strOrderId;
        return actionRedirect(url, request);
      } else {
        request.setAttribute("hOrdId", strOrderId);
        request.setAttribute("hParantForm", "CASE");
        request.setAttribute("hFwdFlag", "YES");
        request.setAttribute("hidePartPrice", "Y");
        return mapping.findForward("orderSummary");
      }
    }
    return mapping.findForward("saveUsageDetails");
  }

  public ActionForward editUsageDeatils(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
    gmOrderDetailsForm.loadSessionParameters(request);
    ArrayList alLogReasons = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmTxnSplitBean txnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    HttpSession session = request.getSession(false);
    alReturn = GmCommonClass.getCodeList("SRNO");
    gmOrderDetailsForm.setAlSurgeryNo(alReturn);
    alReturn = GmCommonClass.getCodeList("USAGE");
    gmOrderDetailsForm.setAlUsage(alReturn);
    alReturn = GmCommonClass.getCodeList("ORDTP");
    gmOrderDetailsForm.setAlSetType(alReturn);
  //code change for PMT-39389 rule values to make NPI Details Mandatory based on order type and company
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strNPIMandatoryFl = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("COMPANY", "NPIMANDATORY", strCompanyId));
    gmOrderDetailsForm.setNpiMandatoryFl(strNPIMandatoryFl);
    String strAccountID = GmCommonClass.parseNull(gmOrderDetailsForm.getAccountID());

    String strOrderID = gmOrderDetailsForm.getOrderID();
    String strRepNm = GmCommonClass.parseNull((String) gmOrderDetailsForm.getCbo_repName());
    log.debug("strOrderID==>" + strOrderID);
    String strCaseInfiID = gmOrderDetailsForm.getCaseInfoID();
    alReturn = gmDOBean.fetchPartDetails(strOrderID);
    log.debug("alReturn==>" + alReturn.size());
    gmOrderDetailsForm.setAlResult(alReturn);
    HashMap hmReturn = gmCustomerBean.getAccSummary(strAccountID);
    log.debug("hmReturn==>" + hmReturn);
    gmOrderDetailsForm.setHmResult(hmReturn);
    // Array Search
    alReturn = gmDOBean.fetchCasePartDetails(strCaseInfiID);
    log.debug("alReturn==>" + alReturn);
    gmOrderDetailsForm.setAlReturn(alReturn);
    // end
    alReturn = txnSplitBean.fetchRulesLookup("NONUSAGE");
    gmOrderDetailsForm.setAlNonUsage(alReturn);

    alLogReasons = gmCommonBean.getLog(strOrderID, "1200");
    gmOrderDetailsForm.setAlLogReasons(alLogReasons);
    gmOrderDetailsForm.setStrOpt("edit");
    return mapping.findForward("saveUsageDetails");
  }

  public ActionForward confirmDO(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
    gmOrderDetailsForm.loadSessionParameters(request);
    GmCreditHoldEngine gmCreditHoldEngine = new GmCreditHoldEngine(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmTxnSplitBean txnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    GmCommonCancelBean gmCommonCancel = new GmCommonCancelBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmOrderDetailsForm);
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
    hmParam.put("COMPANYINFO", strCompanyInfo);
    HashMap hmReturn = new HashMap();
    logOrder.error("hmParam details while Confirming DO GmOrderDetailsAction.confirmDO()*****" + hmParam);
    gmDOBean.saveDOConfirmation(hmParam);
    String strDepID = GmCommonClass.parseNull(gmOrderDetailsForm.getDeptId());
    String strApplnDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strHoldFl = GmCommonClass.parseNull(request.getParameter("hHoldFl"));
    String strUserID = GmCommonClass.parseNull(gmOrderDetailsForm.getUserId());


    if (!strDepID.equals("2005")) {
      HashMap hmEmailData = new HashMap();
      hmReturn = gmCustomerBean.loadOrderDetails(strOrderId);
      hmEmailData = gmCustomerBean.sendPhoneOrderEmail(hmReturn);
      hmEmailData.put(
          "SUBREPORT_DIR",
          request.getSession().getServletContext()
              .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
      hmEmailData.put("APPLNDATEFMT", strApplnDateFmt);

      GmJasperMail jasperMail = new GmJasperMail();
      jasperMail.setJasperReportName("/GmDeliveredOrderSummary.jasper");
      jasperMail.setAdditionalParams(hmEmailData);
      jasperMail.setReportData(null);
      jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));

      HashMap hmjasperReturn = jasperMail.sendMail();
      log.debug("Sent the Phone order Mail");
      txnSplitBean.sendVendorEmail(request, response, strApplnDateFmt, strOrderId);
    }
    request.setAttribute("hOrdId", strOrderId);
    gmDOBean.syncDOFile(strOrderId, "");
    /*
     * Validate Account is on Credit Hold. if Account is Credit Hold Type1 then send Email concern
     * persons.
     */
    hmParam.put("EMAILTYPE", "ORDER");
    hmParam.put("ACCOUNTID", strAccId);
    hmParam.put("TXNID", strOrderId);
    gmCreditHoldEngine.validateCreditHoldAndSendEmail(hmParam);
    return mapping.findForward("confirm");
  }

  public ActionForward validateDOAjax(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
    gmOrderDetailsForm.loadSessionParameters(request);
    String strErrMessage = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmOrderDetailsForm);
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());

    strErrMessage = gmDOBean.validateDO(hmParam);
    log.debug("strErrMessage==>" + strErrMessage);
    response.setContentType("text/html");
    PrintWriter writer = response.getWriter();
    writer.println(strErrMessage);
    writer.flush();
    return null;
  }

  public ActionForward loadCustPO(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
    gmOrderDetailsForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    HashMap hmReturn = new HashMap();
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());

    hmParam = GmCommonClass.getHashMapFromForm(gmOrderDetailsForm);
    String strOpt = GmCommonClass.parseNull(gmOrderDetailsForm.getStrOpt());
    String strOrderID = GmCommonClass.parseNull(gmOrderDetailsForm.getOrderID());
    if (strOpt.equals("load")) {
      String strAccessCondition = getAccessFilter(request, response);
      String strDeptID = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
      hmParam.put("DEPTID", strDeptID);
      hmParam.put("AccessFilter", strAccessCondition);
      hmReturn = gmDOBean.loadCustPODetails(hmParam);
      gmOrderDetailsForm.setHmResult(hmReturn);
    }
    return mapping.findForward("updatePO");
  }

  public ActionForward editCustPO(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
    gmOrderDetailsForm.loadSessionParameters(request);
    HashMap hmReturn = new HashMap();
    HashMap hmParams = new HashMap();
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

    hmParam = GmCommonClass.getHashMapFromForm(gmOrderDetailsForm);
    String strOrderID = GmCommonClass.parseNull(gmOrderDetailsForm.getOrderID());
    String strCustPo = GmCommonClass.parseNull(gmOrderDetailsForm.getCustPO());
    String strUserID = GmCommonClass.parseNull(gmOrderDetailsForm.getUserId());
    log.debug(strOrderID + "===" + strCustPo + "=====" + strUserID);


    hmParams.put("ORDERID", strOrderID);
    hmParams.put("PO", strCustPo);
    hmParams.put("USERID", strUserID);

    hmReturn = gmCustomerBean.saveCustPO(hmParams);
    hmReturn = gmDOBean.loadCustPODetails(hmParam);
    gmOrderDetailsForm.setHmResult(hmReturn);
    gmOrderDetailsForm.setStrOpt("load");
    return mapping.findForward("updatePO");
  }

  public ActionForward loadDOActions(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    String strSessOrderId = "";
    HashMap hmReturn = new HashMap();
    HashMap hmOrderDetails = new HashMap();
    GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
    gmOrderDetailsForm.loadSessionParameters(request);
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());

    strSessOrderId = GmCommonClass.parseNull(request.getParameter("hOrdId"));
    if (strSessOrderId.equals("")) {
      strSessOrderId = GmCommonClass.parseNull((String) request.getAttribute("hOrdId"));
    }

    hmReturn = gmCust.loadOrderDetails(strSessOrderId);
    ArrayList alControlNumDetails =
        GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALCONTROLNUMDETAILS"));
    String strPartNumInputStr = "";
    for (int i = 0; i < alControlNumDetails.size(); i++) {
      HashMap hmVal = (HashMap) alControlNumDetails.get(i);
      String strControlNum = GmCommonClass.parseNull((String) hmVal.get("CNUM"));
      String strPartNum = GmCommonClass.parseNull((String) hmVal.get("PNUM"));
      if (strControlNum.equals(""))
        strPartNumInputStr = strPartNumInputStr + strPartNum + ",";
    }
    hmOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
    hmOrderDetails.put("PARTNUMINPUTSTR", strPartNumInputStr);
    gmOrderDetailsForm.setHmOrderDetails(hmOrderDetails);
    return mapping.findForward("GmDOActions");
  }
  
  /**
   * fetchProductFamily - To fetch the product family of the part number
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchProductFamily(ActionMapping mapping, ActionForm form,
	  HttpServletRequest request, HttpServletResponse response) throws Exception {
	  instantiate(request, response);
	  GmDOReportBean gmDOReportBean = new GmDOReportBean(getGmDataStoreVO());//PMT-32396
	  String strProdFmly= "";
      String strPartNum="";
      
      strPartNum = GmCommonClass.parseNull((String)request.getParameter("strPartNum"));
	  strProdFmly=gmDOReportBean.fetchProductFamily(strPartNum);
	  
	  PrintWriter writer = response.getWriter();
	  writer.write(strProdFmly);
	  writer.flush();
	  writer.close();
	  return null;
  }
  /**
   * validateRepIdAjax - To validate repid is mapped to account or not
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward validateRepIdAjax(ActionMapping mapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {  

	  instantiate(request, response);
	  GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
	  GmOrderDetailsForm gmOrderDetailsForm = (GmOrderDetailsForm) form;
	  gmOrderDetailsForm.loadSessionParameters(request);
	  HashMap hmParam = new HashMap();
	  
	  String strAccId = GmCommonClass.parseNull((String)gmOrderDetailsForm.getAccountID());
	  String strRepId = GmCommonClass.parseNull((String)gmOrderDetailsForm.getRepID());
	  
	  hmParam.put("ACCID", strAccId);
	  hmParam.put("REPID", strRepId);
	  
	  String strResponse = gmDOBean.validateRepId(hmParam);  

	  PrintWriter writer = response.getWriter();
	  writer.write(strResponse);
	  writer.flush();
	  writer.close();
	  return null;
	  
  }

}
