package com.globus.custservice.shipping.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmMasterBarcodeBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

	public GmMasterBarcodeBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**fetchGeneratePDFDetails - This method used to fetch the order or consignmnet details and generate the PDF
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public HashMap fetchGeneratePDFDetails(String strOrderId) throws Exception{
		
		ArrayList alReturn = new ArrayList();
		HashMap hmHeader = new HashMap();
		//String strOrderId =GmCommonClass.parseNull((String) strOrderId.get("ORDERID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    	ArrayList alResult = new ArrayList();
    	gmDBManager.setPrepareString("gm_pkg_cs_master_barcode_rpt.gm_fetch_txn_dtls",3);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
        gmDBManager.setString(1, strOrderId);
        gmDBManager.execute();
        
        alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
       
        hmHeader = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
        gmDBManager.close();
        log.debug("fetchGeneratePDFDetails alResult:"+alResult  +  "hmHeader=="+hmHeader);
        if(alResult.size() > 0) {
        hmHeader.put("TRANSDTLS", alResult);
        }else {
        	hmHeader.put("ERRMSG", "Order/Consignment should be in controlled or shipped Status");
        }
        return hmHeader;   
	}
	
}
