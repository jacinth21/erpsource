/*
 * Module:       GmShippingReportBean.java
 * Author:       Dhinakaran James
 * Date-Written: 12 Oct 2006
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */

package com.globus.custservice.shipping.beans;

import com.globus.common.beans.*;
import com.globus.common.db.GmDBManager;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

public class GmShippingReportBean
{

    public ArrayList getShippingReport(HashMap hmParam)  throws AppError
    {
		ArrayList alReturn = new ArrayList();

        String strType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
        String strFrmDt = GmCommonClass.parseNull((String)hmParam.get("FRMDT"));
        String strToDt = GmCommonClass.parseNull((String)hmParam.get("TODT"));
        String strRefId = GmCommonClass.parseNull((String)hmParam.get("REFID"));
        String strSource = GmCommonClass.parseNull((String)hmParam.get("SOURCE"));
        String strMode = GmCommonClass.parseNull((String)hmParam.get("MODE"));
        String strCarr = GmCommonClass.parseNull((String)hmParam.get("CARR"));
        
		try{
			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();
			StringBuffer sbQuery = new StringBuffer();

			sbQuery.append(" select c907_shipping_id sid, c907_ref_id refid, get_code_name(c901_source) source ");
			sbQuery.append(" ,get_cs_ship_name(c901_ship_to,c907_ship_to_id) shiptonm, to_char(c907_ship_to_dt,'mm/dd/yyyy') sdate ");
			sbQuery.append(" ,get_code_name(c901_delivery_mode) smode,get_code_name(c901_delivery_carrier) scarr ");
			sbQuery.append(" ,c907_tracking_number track, to_char(c907_created_date,'mm/dd/yyy') cdate, get_user_name(c907_created_by) cuser ");
			sbQuery.append(" ,get_user_name(c907_last_updated_by) lname, to_char(c907_last_updated_date,'mm/dd/yyyy') luser ");
			sbQuery.append(" ,c901_delivery_mode smodeid, c901_delivery_carrier scarrid ");
			sbQuery.append(" from t907_shipping_info ");
			sbQuery.append(" where c907_void_fl is null ");
			
            if(strType.equals("PEND"))
            {
            	sbQuery.append(" and c907_tracking_number is null ");
            }
            
            sbQuery.append(" order by c907_shipping_id ");
            alReturn = dbCon.queryMultipleRecords(sbQuery.toString());
        }
        catch(Exception e)
        {
            GmLogError.log("Exception in GmShippingReportBean:getShippingReport", "Exception is:" + e);
        }
    	return alReturn;
    } // End of getShippingReport


    /**
     * Fetch Refid by GUid in t907 table
     * 
     * @param String strGUID
     * @return strRefID
     * @throws AppError the app error
     */
    public String getRefIDbyGUID(String strGUID) throws AppError {
      String strRefID = "";
      GmDBManager gmDBManager = new GmDBManager();
      gmDBManager.setFunctionString("gm_pkg_ship_email.get_ship_refid_by_guid", 1);
      gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR); 
      gmDBManager.setString(2, strGUID);
      gmDBManager.execute();
      strRefID = gmDBManager.getString(1);
      gmDBManager.close();
      return strRefID;
    }
    
} // End of GmShippingReportBean
