/*
 * Module:       GmShippingTransBean.java
 * Author:       Dhinakaran James
 * Date-Written: 12 Oct 2006
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */

package com.globus.custservice.shipping.beans;

import com.globus.common.beans.*;
import com.globus.common.db.GmDBManager;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Date;

public class GmShippingTransBean {

	public String saveShippingMultipleRequests(String strInputStr, String strUserName) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_CS_SAV_SHIP_MULTIPLE", 2);

		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserName);

		gmDBManager.execute();
		gmDBManager.commit();

		return "";
	}// End of saveShippingMultipleRequests

	/**
	 * saveLoanerShipInfo - This Method is used to Saving Ship out info when
	 * "Loaner Extension w/Replenish"
	 * 
	 * @param GmDBManager
	 *            gmDBManager
	 * @param Hash
	 *            Map hmParas
	 * @return void
	 * @exception AppError
	 */
	public void saveLoanerShipInfo(GmDBManager gmDBManager, HashMap hmParas) throws AppError {

		String strRefId = GmCommonClass.parseNull((String) hmParas.get("REFID"));
		String strSource = GmCommonClass.parseNull((String) hmParas.get("SOURCE"));
		String strShipTo = GmCommonClass.parseNull((String) hmParas.get("EXTSHIPTO"));
		String strShipToId = GmCommonClass.parseNull((String) hmParas.get("EXTSHIPTOID"));
		String strShipDT = GmCommonClass.parseNull((String) hmParas.get("SHIPDT"));
		String strShipCarr = GmCommonClass.parseNull((String) hmParas.get("SHIPCARR"));
		String strShipMode = GmCommonClass.parseNull((String) hmParas.get("SHIPMODE"));
		String strTrackNo = GmCommonClass.parseNull((String) hmParas.get("TRACKNO"));
		String strVoidFl = GmCommonClass.parseNull((String) hmParas.get("VOIDFL"));
		String strUserId = GmCommonClass.parseNull((String) hmParas.get("USERID"));
		String strMode = GmCommonClass.parseNull((String) hmParas.get("MODE"));
		String strShipId = GmCommonClass.parseNull((String) hmParas.get("SHIPID"));

		gmDBManager.setPrepareString("GM_CS_SAV_SHIP_INFO", 12);

		gmDBManager.setString(1, strRefId);
		gmDBManager.setInt(2, Integer.parseInt(GmCommonClass.parseZero(strSource)));
		gmDBManager.setInt(3, Integer.parseInt(GmCommonClass.parseZero(strShipTo)));
		gmDBManager.setString(4, strShipToId);
		gmDBManager.setString(5, strShipDT);
		gmDBManager.setInt(6, Integer.parseInt(GmCommonClass.parseZero(strShipCarr)));
		gmDBManager.setInt(7, Integer.parseInt(GmCommonClass.parseZero(strShipMode)));
		gmDBManager.setString(8, strTrackNo);
		gmDBManager.setString(9, strVoidFl);
		gmDBManager.setString(10, strUserId);
		gmDBManager.setString(11, strMode);
		gmDBManager.setInt(12, Integer.parseInt(GmCommonClass.parseZero(strShipId)));

		gmDBManager.execute();
	}
} // End of GmShippingTransBean
