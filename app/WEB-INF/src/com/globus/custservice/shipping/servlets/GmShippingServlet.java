/*****************************************************************************
 * File			 : GmShippingServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.custservice.shipping.servlets;
import com.globus.common.beans.*;
import com.globus.custservice.shipping.beans.*;
import com.globus.common.servlets.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class GmShippingServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strShipPath = GmCommonClass.getString("GMSHIP");
		String strDispatchTo = "";

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			String strAction = request.getParameter("hAction");
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("hAction");
			}
			strAction = (strAction == null)?"Load":strAction;
			String strOpt = request.getParameter("hOpt");
				
			if (strOpt == null)
			{
				strOpt = (String)session.getAttribute("strSessOpt");
			}
			strOpt = (strOpt == null)?"":strOpt;
			
			
			GmCommonClass gmCommon = new GmCommonClass();
			GmShippingReportBean gmReport = new GmShippingReportBean();
			GmShippingTransBean gmTrans = new GmShippingTransBean();
			
			HashMap hmReturn = new HashMap();
			HashMap hmParam = new HashMap();
			
			ArrayList alReturn = new ArrayList();
			String strInputString = "";
			String strUserId = 	(String)session.getAttribute("strSessUserId");
			String strTemp = "";
			if (strAction.equals("Load"))
			{
				if (strOpt.equals("TRANS"))
				{
					hmParam.put("TYPE","PEND");
					strDispatchTo = strShipPath.concat("/GmShippingTrans.jsp");	
				}else if(strOpt.equals("REPORT"))
				{
					strDispatchTo = strShipPath.concat("/GmShippingReport.jsp");
				}
				alReturn = gmReport.getShippingReport(hmParam);
				hmReturn.put("REPORT",alReturn);
			}
			else if (strAction.equals("SaveShip"))
			{
				strInputString = GmCommonClass.parseNull(request.getParameter("hInputStr"));
				strTemp = gmTrans.saveShippingMultipleRequests(strInputString,strUserId);
				hmParam.put("TYPE","PEND");
				alReturn = gmReport.getShippingReport(hmParam);
				hmReturn.put("REPORT",alReturn);
				strDispatchTo = strShipPath.concat("/GmShippingTrans.jsp");				
			}
			request.setAttribute("hmReturn",hmReturn);
			request.setAttribute("hAction",strAction);
			request.setAttribute("hOpt",strOpt);
			dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
			session.setAttribute("hAppErrorMessage",e.getMessage());
			strDispatchTo = strComnPath + "/GmError.jsp";
			gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmShippingServlet