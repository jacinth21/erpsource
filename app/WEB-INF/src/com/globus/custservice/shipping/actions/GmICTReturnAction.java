package com.globus.custservice.shipping.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.shipping.forms.GmICTReturnForm;
import com.globus.operations.beans.GmOperationsBean;

public class GmICTReturnAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	  /**
	   * fetchInhouseTransItems This method used to fetch the Inhouse Return transaction details and consignment details 
	   */
	
	public ActionForward fetchInhouseTransItems(ActionMapping mapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
			GmICTReturnForm gmICTReturnForm = (GmICTReturnForm) form;
			gmICTReturnForm.loadSessionParameters(request);
		    instantiate(request, response);
		    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
		    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
		    GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
		    ArrayList alReturn = new ArrayList();
		    HashMap hmParam = new HashMap();
		    HashMap hmICTDetails = new HashMap();
		    HashMap hmTxnHeaderDetails = new HashMap();
		    HashMap hmCurrency = new HashMap();
		    HashMap hmResult = new HashMap();
		    String strImagePath="";
		    String strCompanyLocale="";
		    String strCompLogo = "";
		    String strRuleGrpId = "";
		    String strReqId = "";
		    String strRefId="";
		    String strUserId="";
		    hmParam = GmCommonClass.getHashMapFromForm(gmICTReturnForm);
		    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
		    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		    strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
		    strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
		    GmResourceBundleBean gmResourceBundleBean =
		        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
		    strCompLogo = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COMP_LOGO")); 
		    strCompLogo = strImagePath + strCompLogo + ".gif";
		    hmParam.put("COMPLOGO", strCompLogo);
		    hmParam.put("IMAGEPATH", strImagePath);
		    hmParam.put("VARIABLECURRENCY", GmCommonClass.parseNull((String) hmCurrency.get("CMPCURRSMB")));
		    hmParam.put("COMPANYDFMT", getGmDataStoreVO().getCmpdfmt());
		    hmParam.put("GMCOMPANYADDRESS", GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMADDRESS")));
		    strReqId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
		    hmParam.put("USERNAME", gmCommonBean.getUserName(strUserId));
		    hmParam.put("UTITLE", GmCommonClass.parseNull(GmCommonClass.getRuleValue(strUserId, "USERTITLE")));
		    hmTxnHeaderDetails = gmCust.loadConsignDetails(strReqId, strUserId);
		    strRefId=GmCommonClass.parseNull((String) hmTxnHeaderDetails.get("DISTID"));
		    strRuleGrpId=GmCommonClass.parseNull((String) hmTxnHeaderDetails.get("PARTYID"));
		    hmResult = gmOper.loadSavedSetMaster(strReqId);
		    alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("SETLOAD"));
		    hmParam.put("SETLOAD", alReturn);
		    hmParam.put("JASPERNAME", "/GmICTReturn.jasper");

		    hmParam.putAll(hmTxnHeaderDetails);
			hmICTDetails = (HashMap) gmPartyBean.fetchPartyRuleParam(strRuleGrpId, "ICPTY", "PRINT");
		    hmParam.putAll(hmICTDetails);
		    hmParam.put("ALSIZE", alReturn.size());
		    gmICTReturnForm.setAlReturn(alReturn);
		    gmICTReturnForm.setHmJasperDetails(hmParam);
		    return mapping.findForward("GmICTReturn"); 
	}
}
