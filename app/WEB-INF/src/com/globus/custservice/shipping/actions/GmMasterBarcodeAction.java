package com.globus.custservice.shipping.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;
import org.apache.log4j.Logger;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;

import com.globus.custservice.shipping.beans.GmMasterBarcodeBean;
import com.globus.custservice.shipping.forms.GmMasterBarcodeForm;


/**
 * PC-3900 -  Master Barcode Generate for spineIT Orders/Consignment
 * @author tramasamy
 *
 */

public class GmMasterBarcodeAction extends GmDispatchAction{
	
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	
	 
	/**loadMasterBarcode - this method used to load masre barcode screen
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadMasterBarcode(ActionMapping mapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {
	    instantiate(request, response);
	    GmMasterBarcodeForm gmMasterBarcodeForm = (GmMasterBarcodeForm) form;
	    gmMasterBarcodeForm.loadSessionParameters(request);
	   
	    return mapping.findForward("GmMasterBarcode");
	  }
	
	  
	/**fetchGeneratePDFDetails - This method used to fetch the order or consignmnet details and generate the PDF
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchGeneratePDFDetails(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		  
		    instantiate(request, response);
		    GmMasterBarcodeForm gmMasterBarcodeForm = (GmMasterBarcodeForm) actionForm;
		    gmMasterBarcodeForm.loadSessionParameters(request);
		    GmJasperReport gmJasperReport = new GmJasperReport();
		    HashMap hmParam = new HashMap();
		    HashMap hmResult = new HashMap();
		    HashMap hmHeader = new HashMap();
		    
		    hmParam = GmCommonClass.getHashMapFromForm(gmMasterBarcodeForm);
		    String strOrderId =GmCommonClass.parseNull((String)hmParam.get("ORDERID"));
		    GmMasterBarcodeBean gmMasterBarcodeBean = new GmMasterBarcodeBean(getGmDataStoreVO()); 
		    hmResult = gmMasterBarcodeBean.fetchGeneratePDFDetails(strOrderId);
		    
		    String strErrMsg = GmCommonClass.parseNull((String)hmResult.get("ERRMSG"));
		    
		    ArrayList alReturn = new ArrayList();
		    
		 if(strErrMsg.equals("")) {
		    String strJasperName = "/GmMasterBarCode.jasper";
		    
			 String fileName = "";
			
			
		 
		      String strFilePath = GmCommonClass.getString("MASTER_BARCODE");
		      fileName = strFilePath + strOrderId + ".pdf";
		      File newFile = new File(strFilePath);
		      alReturn = (ArrayList) hmResult.get("TRANSDTLS");
		     
		      gmJasperReport.setRequest(request); gmJasperReport.setResponse(response);
		      gmJasperReport.setJasperReportName(strJasperName);
		      gmJasperReport.setHmReportParameters(hmResult);
		      gmJasperReport.setReportDataList(alReturn);
		      gmJasperReport.exportJasperReportToPdf(fileName);
		 }
			   response.setContentType("text/json");
		        PrintWriter pw = response.getWriter();
		        if (!strErrMsg.equals("")) {
		        pw.write(strErrMsg);
		        }
		        pw.flush();
		 
	 return null;
	  }
}
