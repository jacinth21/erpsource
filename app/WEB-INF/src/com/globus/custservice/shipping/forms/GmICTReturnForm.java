package com.globus.custservice.shipping.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmICTReturnForm extends GmLogForm{
	
	private String requestId="";
	private String sourceType="";
	private ArrayList alReturn = new ArrayList();
	private HashMap hmJasperDetails = new HashMap();
	/**
	 * @return the sourceType
	 */
	public String getSourceType() {
		return sourceType;
	}

	/**
	 * @param sourceType the sourceType to set
	 */
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}

	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}
	
	/**
	 * @return the hmJasperDetails
	 */
	public HashMap getHmJasperDetails() {
		return hmJasperDetails;
	}

	/**
	 * @param hmJasperDetails the hmJasperDetails to set
	 */
	public void setHmJasperDetails(HashMap hmJasperDetails) {
		this.hmJasperDetails = hmJasperDetails;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}

	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	
}
