package com.globus.custservice.beans;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author HReddi
 * Interface created for write method to print the Consign Version for US and BBA.	 *
 */	
public interface GmProcessBasePackSLipInterface {
	public ArrayList fetchCartDetails (HashMap hmParams);	
}
