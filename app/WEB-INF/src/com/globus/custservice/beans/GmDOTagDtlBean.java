/**
 * FileName    : GmDOTagDtlBean.java 
 * Author      : ppandiyan
 * Copyright   : Globus Medical Inc
 */
package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

import com.globus.custservice.forms.GmOrderTagRecordForm;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDOTagDtlBean extends GmCommonBean {
	
	// Code to Initialize the Logger Class.
	Logger log = GmLogger.getInstance(this.getClass().getName());

	  public GmDOTagDtlBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }

		  /**
		   * Constructor will populate company info.
		   * 
		   * @param gmDataStore
		   */
		  public GmDOTagDtlBean(GmDataStoreVO gmDataStore) {
		    super(gmDataStore);
		  }
		  
		  /*
		   * fetchDOTagDetails(): This method is used to fetch the record DO tag
		   * details for the order id
		   * Author: ppandiyan
		   */
	
	public String fetchDOTagDetails(String strOrderId) throws AppError {
		
		String strReturn = "";
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_order_tag_record.gm_fetch_DO_tag_dtls", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strOrderId);
		gmDBManager.execute();
		strReturn = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		log.debug("fetchDOTagDetails::"+strReturn);
	    return strReturn;
	}

	
}
