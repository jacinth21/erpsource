package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOrderUsageBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmOrderUsageBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }


  /**
   * fetchUsedLotNumber - this method used to fetch the used lot numbers
   * 
   * @param strOrderId
   * @return Array List
   * @throws AppError
   */
  public ArrayList fetchUsedLotNumber(String strOrderId) throws AppError {
    ArrayList alReturn = new ArrayList();
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cs_usage_lot_number.gm_fch_used_lot_number", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    Iterator itrUsedLotDetails = alResult.iterator();
    while (itrUsedLotDetails.hasNext()) {
      HashMap hmUsedLotMap = (HashMap) itrUsedLotDetails.next();
      String strPnum = GmCommonClass.parseNull((String) hmUsedLotMap.get("PNUM"));

      ArrayList alUsedLot =
          fetchUsageLot(GmCommonClass.parseNull(strOrderId), strPnum, gmDBManager);
      hmUsedLotMap.put("LOT_DTLS", alUsedLot);
      alReturn.add(hmUsedLotMap);
    }
    gmDBManager.close();
    log.debug(" alReturn --> " + alReturn.size());

    return alReturn;
  }

  /**
   * fetchUsageLot This method used to fetch the group of lot number details
   * 
   * @param strOrderId
   * @param strPartNumber
   * @param gmDBManager
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchUsageLot(String strOrderId, String strPartNumber, GmDBManager gmDBManager)
      throws AppError {
    gmDBManager.setPrepareString("gm_pkg_cs_usage_lot_number.gm_fch_lot_number_dtls", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strPartNumber);
    gmDBManager.execute();
    ArrayList alReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    return alReport;
  }

  /**
   * returnUsedLot - This method used to save the returns lot number details
   * 
   * @param strOrderId
   * @param strRAId
   * @param strInputStr
   * @param strUserId
   * @param gmDBManager
   * @throws AppError
   */
  public void returnUsedLot(String strOrderId, String strRAId, String strInputStr,
      String strUserId, GmDBManager gmDBManager) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_cs_usage_lot_number.gm_return_lot_number_dtls", 4);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strRAId);
    gmDBManager.setString(3, strInputStr);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
  }
  
	/**
	 * This method used to save the used lot number details based on issue
	 * credit
	 * 
	 * @param strOrderId
	 * @param strUsedLotStr
	 * @param strUserId
	 * @throws AppError
	 */
	public void saveUsageLotDtls(String strOrderId, String strUsedLotStr,
			String strUserId) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_cs_usage_lot_number.gm_sav_order_used_lot_bulk", 3);
		    gmDBManager.setString(1, strOrderId);
		    gmDBManager.setString(2, strUsedLotStr);
		    gmDBManager.setString(3, strUserId);
		    gmDBManager.execute();
		    gmDBManager.commit();
	}
}
