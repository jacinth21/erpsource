/*
 * GmINCustomerBean: This is company based customer bean calling using interface
 */

package com.globus.custservice.beans;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmINCustomerBean extends GmBean implements GmPlaceOrderInterface {


  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmINCustomerBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  public GmINCustomerBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * saveItemOrder - This method is to save the order details while placing sales order in India
   * Company
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   */
  @Override
  public HashMap saveItemOrder(HashMap hmParam) throws AppError {
    String strMsg = "";
    String strPrepareString = null;
    String strCtrlNumberStr = "";
    HashMap hmReturn = new HashMap();
    Date dtSurgDt = null;

    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    GmCommonBean gmCommonBean = new GmCommonBean(gmDataStoreVO);
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(gmDataStoreVO);
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(gmDataStoreVO);
    GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(gmDataStoreVO);

    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strTotal = GmCommonClass.parseNull((String) hmParam.get("TOTAL"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strAdjInputStr = GmCommonClass.parseNull((String) hmParam.get("ADJINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrdType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strBillTo = GmCommonClass.parseNull((String) hmParam.get("BILLTO"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("MODE"));
    String strPerson = GmCommonClass.parseNull((String) hmParam.get("PERSON"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strPO = GmCommonClass.parseNull((String) hmParam.get("PO"));
    String strConCode = GmCommonClass.parseNull((String) hmParam.get("CONCODE"));
    String strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENTORDID"));

    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));
    String strShipCarr = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strAddressId = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID"));
    // String strSalesRep = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
    String strQuoteStr = GmCommonClass.parseNull((String) hmParam.get("QUOTESTR"));
    String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
    strCtrlNumberStr = GmCommonClass.parseNull((String) hmParam.get("CTRLNUMBERSTR"));
    String strHardCpPO = GmCommonClass.parseNull((String) hmParam.get("HARDCPPO"));
    String strOverrideAttnTo = GmCommonClass.parseNull((String) hmParam.get("OVERRIDEATTNTO"));
    String strShipInstruction = GmCommonClass.parseNull((String) hmParam.get("SHIPINSTRUCTION"));
    String strOrderComments = GmCommonClass.parseNull((String) hmParam.get("ORDER_COMMENTS"));
    String strRefOrderId = GmCommonClass.parseNull((String) hmParam.get("REFORDID"));
    String strReqDate = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDT"));
    String strShipCharge = GmCommonClass.parseNull((String) hmParam.get("SHIPCHARGE"));
    strShipCharge =
        strShipCharge.equalsIgnoreCase("N/A") ? "" : GmCommonClass.replaceAll(strShipCharge, ",",
            "");
    String strLotOverrideFl = GmCommonClass.parseNull((String) hmParam.get("LOTOVERRIDEFL"));
    String strUpdateDDTFl = GmCommonClass.parseNull((String) hmParam.get("UPDATE_DDT_FL"));

    if (!strOrderComments.equals("")) {
      strComments = strOrderComments;
    }
    strHardCpPO = (strHardCpPO.equals("on")) ? "Y" : "";
    String strShipId = "";
    strPO = gmCustomerBean.returnCustomerPO(strOrderId, strPO);
    dtSurgDt = (java.util.Date) hmParam.get("SURGERYDT");

    log.debug("calling GM_SAVE_ITEM_ORDER *** " + hmParam);

    gmDBManager.setPrepareString("GM_SAVE_ITEM_ORDER", 25);
    gmDBManager.registerOutParameter(18, java.sql.Types.CHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strBillTo);
    gmDBManager.setInt(3, Integer.parseInt(GmCommonClass.parseZero(strMode)));
    gmDBManager.setString(4, strPerson);
    gmDBManager.setString(5, strOrderComments);
    gmDBManager.setString(6, strPO);
    gmDBManager.setString(7, strTotal);
    gmDBManager.setString(8, strUserId);
    gmDBManager.setString(9, strInputStr);
    gmDBManager.setInt(10, Integer.parseInt(strOrdType));
    gmDBManager.setInt(11, Integer.parseInt(GmCommonClass.parseZero(strConCode)));
    gmDBManager.setString(12, strParentOrderId);
    gmDBManager.setInt(13, Integer.parseInt(GmCommonClass.parseZero(strShipTo)));
    gmDBManager.setString(14, strShipToId);
    gmDBManager.setInt(15, Integer.parseInt(GmCommonClass.parseZero(strShipCarr)));
    gmDBManager.setInt(16, Integer.parseInt(GmCommonClass.parseZero(strShipMode)));
    gmDBManager.setInt(17, Integer.parseInt(GmCommonClass.parseZero(strAddressId)));
    gmDBManager.setString(19, strCaseId);
    gmDBManager.setString(20, strHardCpPO);
    gmDBManager.setString(21, strOverrideAttnTo);
    gmDBManager.setString(22, strShipInstruction);
    gmDBManager.setString(23, strRefOrderId);
    gmDBManager.setString(24, strShipCharge);
    gmDBManager.setDate(25, dtSurgDt);
    gmDBManager.execute();

    strShipId = gmDBManager.getString(18);
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strOrderId, strComments, strUserId, "1200");
    }
    // To save BBA order
    if (!strRefOrderId.equals("")) {
      gmCustomerBean.saveBBAOrder(gmDBManager, strOrderId, strUserId);
    }

    if (!strQuoteStr.equals("") || !strReqDate.equals("") || !strLotOverrideFl.equals("")) {
      if (!strReqDate.equals(""))
        strQuoteStr = strQuoteStr + "^" + 103926 + "^" + strReqDate + "|";
      if (!strLotOverrideFl.equals(""))
        strQuoteStr = strQuoteStr + "^" + 103950 + "^" + strLotOverrideFl + "|";
      gmCustomerBean.saveOrderAttribute(gmDBManager, strOrderId, strUserId, strQuoteStr);
    }
    // MNTTASK-8623 - CSM DO Process control # from S Part
    // Italy company - always save the order info and item order usage table
    if (!strCtrlNumberStr.equals("") || "Y".equals(strUpdateDDTFl)) {
      hmParam.put("INPUTSTR", strCtrlNumberStr);
      hmParam.put("ORDERID", strOrderId);
      hmParam.put("SCREEN", "NEW_ORDER");

      gmDOBean.updateControlNumberDetail(hmParam, gmDBManager);
    }
    // hmParam.put("SOURCE", "50180"); // orders
    // hmParam.put("REFID", strOrderId);
    // hmParam.put("STATUSFL", "15");
    // gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);

    gmTxnSplitBean.checkAndSplitDO(gmDBManager, strOrderId, strUserId);

    /* For Updating the Unit Price Adjustment Details for the Sales Order in T502_ITEM_ORDER table */
    if (!strAdjInputStr.equals("")) {
      gmCustomerBean.updateAdjustmentDetails(gmDBManager, strOrderId, strAdjInputStr, strUserId);
    }

    // update the adjustment details for BBA order
    gmCustomerBean.updateAdjustmentDetailsBBA(gmDBManager, strOrderId, strUserId);

    gmCustomerBean.syncAccAttribute(gmDBManager, strOrderId, strBillTo, "ORDER", strUserId);

    hmReturn.put("MSG", strMsg);
    hmReturn.put("strShipId", strShipId);

    gmDBManager.commit();
    return hmReturn;
  }

  /**
   * saveOrderDetails - This method is to save the order details while editing sales order in India
   * Company
   * 
   * @param hmParam, strOrderId, strUsername
   * @return String
   * @exception AppError
   */
  @Override
  public String saveOrderDetails(HashMap hmParam, String strOrderId, String strUsername)
      throws AppError {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);

    String strMsg = "";

    String strAccId = "";
    String strMode = "";
    String strPerson = "";
    String strComments = "";
    String strPO = "";
    String strParentOrderId = "";
    String strInvoiceId = "";

    String strSalesRepId = "";
    String strRmQtyStr = "";
    String strBOQtyStr = "";
    String strQuoteStr = "";
    String strCnumStr = "";
    String strHardCpPO = "";
    String strUsedLotStr = "";

    String strRmQtyMsgStr = "";
    String strBOQtyMsgStr = "";

    Date dtSurgDt = null;
    Date dtOrderDt = null;

    HashMap hmReturn = new HashMap();

    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strMode = GmCommonClass.parseNull((String) hmParam.get("OMODE"));
    strPerson = GmCommonClass.parseNull((String) hmParam.get("OPERSON"));
    strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    strPO = GmCommonClass.parseNull((String) hmParam.get("CPO"));
    strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENTORDERID")); // parentid
    strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
    strSalesRepId = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
    strRmQtyStr = GmCommonClass.parseNull((String) hmParam.get("RMQTYSTR"));
    strBOQtyStr = GmCommonClass.parseNull((String) hmParam.get("BOQTYSTR"));
    strQuoteStr = GmCommonClass.parseNull((String) hmParam.get("QUOTESTR"));
    strCnumStr = GmCommonClass.parseNull((String) hmParam.get("CNUMSTR"));
    strUsedLotStr = GmCommonClass.parseNull((String) hmParam.get("USEDLOTSTR"));
    strHardCpPO = GmCommonClass.parseNull((String) hmParam.get("HARDCPPO"));
    strHardCpPO = (strHardCpPO.equals("on")) ? "Y" : "";
    dtSurgDt = (java.util.Date) hmParam.get("SURGERYDT");
    dtOrderDt = (java.util.Date) hmParam.get("ORDER_DATE");

    gmDBManager.setPrepareString("GM_UPDATE_ORDER", 19);
    gmDBManager.registerOutParameter(18, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(19, java.sql.Types.CHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strAccId);
    gmDBManager.setString(3, strPO);
    gmDBManager.setString(4, strUsername);
    gmDBManager.setInt(5, Integer.parseInt(strMode));
    gmDBManager.setString(6, strPerson);
    gmDBManager.setString(7, strComments); // Comments and other
    // information
    gmDBManager.setString(8, strParentOrderId);
    gmDBManager.setString(9, strInvoiceId);
    gmDBManager.setString(10, strSalesRepId);
    gmDBManager.setString(11, strRmQtyStr);
    gmDBManager.setString(12, strBOQtyStr);
    gmDBManager.setString(13, strHardCpPO);
    gmDBManager.setString(14, strQuoteStr);
    gmDBManager.setString(15, strUsedLotStr);
    gmDBManager.setDate(16, dtSurgDt);
    gmDBManager.setDate(17, dtOrderDt);//to update order date for Month End Reclassification
    gmDBManager.execute();
    strRmQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(18));
    strBOQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(19));
    if (!strCnumStr.equals("")) {
      hmParam.put("INPUTSTR", strCnumStr);
      hmParam.put("ORDERID", strOrderId);
      hmParam.put("USERID", strUsername);
      hmParam.put("SCREEN", "EDIT_ORDER");
      gmDOBean.updateControlNumberDetail(hmParam, gmDBManager);
    }


    strMsg = strRmQtyMsgStr + "<br>" + strBOQtyMsgStr;
    hmReturn.put("MSG", strMsg);
    gmDBManager.commit();
    return strMsg;

  }


}// end of class GmINCustomerBean
