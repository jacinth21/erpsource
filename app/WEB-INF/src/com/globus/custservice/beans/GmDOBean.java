package com.globus.custservice.beans;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.util.GmWSUtil;
import com.globus.jms.consumers.beans.GmOrderBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.party.beans.GmNPIProcessBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.sales.GmDONPIDetailVO;
import com.globus.valueobject.sales.GmDORecordTagIDVO;
import com.globus.valueobject.sales.GmDOSurgeryDetailVO;
import com.globus.valueobject.sales.GmItemOrderAttrVO;
import com.globus.valueobject.sales.GmOrderAttrVO;
import com.globus.valueobject.sales.GmOrderItemVO;
import com.globus.valueobject.sales.GmOrderVO;
import com.globus.valueobject.sales.GmTagUsageVO;

public class GmDOBean extends GmSalesFilterConditionBean implements GmCartDetailsInterface {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  Logger logOrder = GmLogger.getInstance("globusapporder");

  // this will be removed all place changed with GmDataStoreVO constructor

  public GmDOBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDOBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public ArrayList fetchDODashboard(HashMap hmParam) throws AppError {

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    String strOrdStatus = GmCommonClass.parseNull((String) hmParam.get("ORDSTATUS"));
    String strCompPlantFilter = "";
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "DOPENDINGCONFIRM")); // DOPENDINGCONFIRM-Rule
                                                                                              // Value:=
                                                                                              // plant.
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

    sbQuery
        .append("SELECT t7100.c7100_case_id caseid, get_rep_name (t501.c703_sales_rep_id) rep_name, gm_pkg_op_do_process_report.get_order_status_desc(t501.c501_order_id) status");
    sbQuery
        .append(", get_account_name (t501.c704_account_id) acc_name, GET_ACCOUNT_CURR_SYMB(t501.c704_account_id)  ACC_CURRENCY ,");
    sbQuery.append(" t7100.c7100_surgery_date sdate, t501a.c501a_attribute_value requested_date, "); // requested_date-Handling
                                                                                                     // dateformat
                                                                                                     // in
                                                                                                     // DTDODashWrapper.java
                                                                                                     // file.
    sbQuery
        .append(" to_number(NVL(TRIM(get_total_do_amt(t501.c501_order_id,'')),0)) total_cost , t501.c501_order_id order_id ,get_code_name(t501.c901_order_type) order_type , DECODE(T502a.C501_ORDER_ID,'','','Y') REPLN");
    sbQuery.append(" FROM t501_order t501");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(", t7100_case_information t7100,");
    sbQuery.append(" ( SELECT t501a.c501a_attribute_value , t501a.c501_order_id  ");
    sbQuery.append(" FROM  t501a_order_attribute t501a ");
    sbQuery.append(" WHERE  t501a.c501a_order_attribute_id IN ( ");
    sbQuery.append(" SELECT MAX (t501a.c501a_order_attribute_id)");
    sbQuery.append(" FROM t501a_order_attribute t501a ");
    sbQuery.append(" WHERE  c901_attribute_type = 11240");
    sbQuery.append(" AND c501a_void_fl IS NULL ");
    sbQuery.append(" GROUP BY t501a.c501_order_id)) t501a , ");
    sbQuery.append(" ( SELECT DISTINCT T502A.C501_ORDER_ID FROM T502A_ITEM_ORDER_ATTRIBUTE T502A ");
    sbQuery.append(" WHERE T502A.C901_ATTRIBUTE_TYPE ='101732' AND T502A.C502A_VOID_FL IS NULL ");
    sbQuery.append(" AND T502A.C502A_ATTRIBUTE_VALUE ='Y' )t502a ");
    sbQuery.append(" WHERE t7100.c7100_case_info_id(+) = t501.c7100_case_info_id");
    sbQuery.append(" AND t501.c501_order_id = t501a.c501_order_id ");
    sbQuery.append(" AND T501.C501_ORDER_ID = T502a.C501_ORDER_ID(+) ");
    // sbQuery.append(" AND t501.c501_parent_order_id is NULL");
    sbQuery.append(" AND t501.c901_order_type IN (2518,2519)");// Rep Draft
    // ,Pending
    // CS
    // Confirmation
    if (!strOrdStatus.equals("")) {
      sbQuery.append(" AND t501.c501_status_fl = " + strOrdStatus);
    }
    sbQuery.append(" AND c501_delete_fl IS NULL");
    sbQuery.append(" AND c501_void_fl IS NULL");
    sbQuery
        .append(" AND (t501.c501_parent_order_id IS NULL OR get_order_attribute_value(t501.c501_order_id,4000536) = 4000537) ");
    sbQuery.append(" AND t7100.c7100_void_fl(+) IS NULL");
    sbQuery.append(" AND t7100.c901_type(+) = 1006503 ");
    sbQuery.append(" AND V700.REP_ID = T501.C703_SALES_REP_ID ");
    // Fetch records based company/plant
    sbQuery.append(" AND ( t501.C1900_COMPANY_ID = " + strCompPlantFilter);
    sbQuery.append(" OR t501.C5040_PLANT_ID = " + strCompPlantFilter);
    sbQuery.append(") ");
    sbQuery.append(" ORDER BY requested_date DESC, sdate DESC, UPPER(rep_name) ");

    log.debug("fetchDODashboard:::::::" + sbQuery.toString());
    rdResult = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    alReturn = (ArrayList) rdResult.getRows();

    return alReturn;
  }

  /* Save DO Confirmation */
  public void saveDOConfirmation(HashMap hmParam) throws Exception {
    log.debug("hmParam: " + hmParam);
    logOrder.error("hmParam in GmDOBean.saveDOConfirmation() " + hmParam);
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    GmOrderBean gmOrderBean = new GmOrderBean(getGmDataStoreVO());
    GmDORptBean gmDoRptBean = new GmDORptBean(getGmDataStoreVO());
    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33510
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    HashMap hmHoldFl = new HashMap();
    HashMap hmOrdAtt = new HashMap();
    HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33510
    String strAction = "CS_CONFIRM";
    String strConsumerClass =
        GmCommonClass.parseNull(GmCommonClass.getString("NOTIFICATION_CONSUMER_CLASS"));
    String strQueueName =
        GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_NOTIFICATION_RMT_QUEUE"));
    if (strDeptId.equals("2005")) {
      strAction = "SALES_CONFIRM";
    } else {
      strAction = "CS_CONFIRM";
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_sav_confirmation", 3);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strAction);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();

    if (strAction.equals("CS_CONFIRM")) {
      gmTxnSplitBean.checkAndSplitDO(gmDBManager, strOrderId, strUserId);
      
     /* Adding a new method call to perform posting for ipad order types  Bill only sales Consignment
        PC-2280 Aug 22-2020
    */     
      gmTxnSplitBean.checkAndUpdatePosting(gmDBManager, strOrderId, strUserId);
      gmUpdateLotStatus(gmDBManager, strOrderId, strUserId);
    }
    // gmUpdateNetQty(gmDBManager, strOrderId, strUserId);
    fetchInputStringForDOConfirm(gmDBManager, strOrderId);
    gmDBManager.commit();

    // to call the JMS for hold order update status.
    if (!strDeptId.equals("2005")) {
      hmHoldFl.put("ORDERID", strOrderId);
      hmHoldFl.put("IPADCOMMENTS", strOrderId + " On Hold from CSM");// CSM Comments for hold
      hmHoldFl.put("USERID", strUserId);
      hmHoldFl.put("COMPANY_INFO", strCompanyInfo);

      gmOrderBean.updateHoldOrderStatus(hmHoldFl);
    }
    // To send push notification while confirming the order
    hmParam.put("REFID", strOrderId);
    hmParam.put("MESSAGE", "DO# " + strOrderId + " confirmed by C/S");
    hmParam.put("NOTIFICATION", "ORDER");

    hmOrdAtt.put("ORDERID", strOrderId);
    hmOrdAtt.put("ATTRIBTYPE", "107802");// 107802 ipad order type 
    
    // fetching ipad order type selected in order type dropdown
    String strOrdType = GmCommonClass.parseNull(gmDoRptBean.fetchOrderAttribute(hmOrdAtt));
    
    // 2532 bill only from sales consignment
    // This is JMS call to update account net qty by lot report
    if (strOrdType.equals("2532")) {
      HashMap hmAcctParam = new HashMap();
      hmAcctParam.put("TRANS_ID", strOrderId);
      hmAcctParam.put("TRANS_TYPE", "50180");
      hmAcctParam.put("USER_ID", strUserId);
      hmAcctParam.put("WAREHOUSE_TYPE", "5");
      hmAcctParam.put("COMPANY_INFO", strCompanyInfo);
      gmInvLocationBean.updateLoanerStockSheetInfo(hmAcctParam);
    }

    // This is JMS Code which will process the notification, jms/gmnotificationqueue is the JMS
    // queue using for notification
    GmQueueProducer qprod = new GmQueueProducer();
    GmMessageTransferObject tf = new GmMessageTransferObject();
    tf.setMessageObject(hmParam);
    tf.setConsumerClass(strConsumerClass);
    qprod.sendMessage(tf, strQueueName);

    // To Save the Insert part number details in DO Pending Confirmation screen Using JMS
    String strTxnType = "50261";
    HashMap hmJMSParam = new HashMap();
    hmJMSParam.put("TXNID", strOrderId);
    hmJMSParam.put("TXNTYPE", strTxnType);
    hmJMSParam.put("STATUS", "93342");
    hmJMSParam.put("USERID", strUserId);
    hmJMSParam.put("COMPANYINFO", strCompanyInfo);
    gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);
    
    // To call web service for NPI number for Ipad orders in Do Pending Confirmation screen Using JMS
    String strNPIQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_NPI_QUEUE"));
    String strNPIConsumerClass =   GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("NPI_CONSUMER_CLASS"));
    hmParam.put("QUEUE_NAME", strNPIQueueName);
    hmParam.put("TRANSID", strOrderId);
    hmParam.put("USERID", strUserId);
    hmParam.put("NPINO", "");

    hmParam.put("CONSUMERCLASS", strNPIConsumerClass); 
    log.debug("hmParam============== "+hmParam);
    gmConsumerUtil.sendMessage(hmParam);

    
    //JMS call for Storing Building Info PMT-33510  
    hmStorageBuildInfo.put("TXNID", strOrderId);
    hmStorageBuildInfo.put("TXNTYPE", "ORDER");
    gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo); 
  }

  /**
   * Description : This method is used to set the data (DO summary Email). Order Details and Order
   * summary information set to Hashmap.
   * 
   * Author :
   */
  public HashMap fetchDOMasterDetails(HashMap hmResult) throws AppError {
    HashMap hmOrderDetails = new HashMap();
    HashMap hmOrderSummary = new HashMap();

    hmOrderDetails = (HashMap) hmResult.get("ORDERDETAILS");

    hmOrderSummary.put("REPID", GmCommonClass.parseNull((String) hmOrderDetails.get("REPID")));
    hmOrderSummary.put("ACCID", GmCommonClass.parseNull((String) hmOrderDetails.get("ACCID")));
    hmOrderSummary.put("ANAME", GmCommonClass.parseNull((String) hmOrderDetails.get("ANAME")));
    hmOrderSummary.put("PO", GmCommonClass.parseNull((String) hmOrderDetails.get("PO")));
    hmOrderSummary.put("ID", GmCommonClass.parseNull((String) hmOrderDetails.get("ID")));
    hmOrderSummary.put("SURGERYDT", hmOrderDetails.get("SDT"));

    hmOrderSummary.put("SHIPPADD_OLD",
        GmCommonClass.parseNull((String) hmOrderDetails.get("SHIPADD_OLD")));
    String strRepName = GmCommonClass.parseNull((String) hmOrderDetails.get("REPNM"));
    StringTokenizer strRepToken = new StringTokenizer(strRepName, " ");
    if (strRepToken.hasMoreTokens()) {
      hmOrderSummary.put("REPFNM", GmCommonClass.parseNull(strRepToken.nextToken()));
    } else {
      hmOrderSummary.put("REPFNM", "");
      hmOrderSummary.put("REPLNM", "");
    }

    if (strRepToken.hasMoreTokens()) {
      hmOrderSummary.put("REPLNM", GmCommonClass.parseNull(strRepToken.nextToken()));
    } else {
      hmOrderSummary.put("REPLNM", "");
    }
    ArrayList alShipSummary = (ArrayList) hmResult.get("SHIPSUMMARY");
    String stAddress1 = "";
    String stAddress2 = "";
    boolean bMultiAddr = false;
    int cnt = 0;
    for (int i = 0; i < alShipSummary.size(); i++) {
      HashMap hmShippSummary = (HashMap) alShipSummary.get(i);
      stAddress1 = GmCommonClass.parseNull((String) hmShippSummary.get("ADDRESS1"));
      stAddress2 = GmCommonClass.parseNull((String) hmShippSummary.get("ADDRESS2"));
      if (stAddress1.equals(stAddress2)) {
        bMultiAddr = true;
        hmOrderSummary.put("REP_NAME",
            GmCommonClass.parseNull((String) hmShippSummary.get("REP_NAME")));
      } else {
        hmOrderSummary.put("ADDR1", stAddress1);
        hmOrderSummary.put("CITY", GmCommonClass.parseNull((String) hmShippSummary.get("CITY"))
            .concat(","));
        hmOrderSummary.put("STATE", GmCommonClass.parseNull((String) hmShippSummary.get("STATE"))
            .concat(","));
        hmOrderSummary.put("ZIP", GmCommonClass.parseNull((String) hmShippSummary.get("ZIPCODE")));
        hmOrderSummary.put("REP_NAME",
            GmCommonClass.parseNull((String) hmShippSummary.get("REP_NAME")));
      }
    }
    if (bMultiAddr) {
      hmOrderSummary.put("ADDR1", "Multiple");
      hmOrderSummary.put("CITY", "");
      hmOrderSummary.put("STATE", "");
      hmOrderSummary.put("ZIP", "");

    }

    hmOrderSummary.put("ORDERSUMMARY", hmResult.get("ORDERSUMMARY"));
    return hmOrderSummary;
  }

  public void saveOrderUsageDetails(HashMap hmParam) throws Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    log.debug("hmParam: " + hmParam);

    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strOrderTotal = GmCommonClass.parseNull((String) hmParam.get("TOTAL"));
    double dTotal = Double.parseDouble(strOrderTotal);
    log.debug("" + dTotal);
    log.debug("strCaseInfoID: " + strCaseInfoID);
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    String strCustPO = GmCommonClass.parseNull((String) hmParam.get("CUSTPO"));
    String strParentDOID = GmCommonClass.parseNull((String) hmParam.get("PARENTORDERID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("ORDCOMMENTS"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strOrderAttrb = GmCommonClass.parseNull((String) hmParam.get("ORDERATTRSTR"));
    Date dtSurdDt = null;
    dtSurdDt = (java.util.Date) hmParam.get("SURGERYDT");

    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_sav_usage_details_main", 12);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strParentDOID);
    gmDBManager.setString(3, strCaseInfoID);
    gmDBManager.setString(4, strInputStr);
    gmDBManager.setString(5, strAccountID);
    gmDBManager.setString(6, strCustPO);
    gmDBManager.setDouble(7, dTotal);
    gmDBManager.setString(8, strComments);
    gmDBManager.setString(9, strAction);
    gmDBManager.setString(10, strUserID);
    gmDBManager.setString(11, strRepId);
    gmDBManager.setDate(12, dtSurdDt);

    gmDBManager.execute();
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strOrderID, strLog, strUserID, "1200"); // 1200 is Order
    }
    if(!strOrderAttrb.equals("")) {   //strOrderAttrb added changes for PC-4892-CaseBooking
      updateOrderAttrMaster(hmParam, gmDBManager);
    }
    // TO UPDATE THE NPI TRANSACTION STATUS
    gmNPIProcessBean.saveNPITransactionStatus(gmDBManager, strOrderID, strUserID);
    gmDBManager.commit();
  }

  public ArrayList fetchPartDetails(String strOrderID) throws Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_do_process_report.gm_fetch_part_details", 2);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  public ArrayList fetchCasePartDetails(String strCaseInfoId) throws Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_do_process_report.gm_fetch_case_parts", 2);
    gmDBManager.setString(1, strCaseInfoId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchDOTagDetails - This method will fetch the DO Tag Details
   * 
   * @param String strOrderID
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchDOTagDetails(String strSessOrderId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT order_id, pnum, pdesc, DECODE (itype, 50300, 'C', 50301, 'L') itype, ");
    sbQuery.append(" NVL(t5012.c5012_qty,t502.iqty) iqty, t5012.c5010_tag_id tag_id ");
    sbQuery
        .append(" FROM (SELECT   NVL(t501.c501_parent_order_id,t501.c501_order_id) porder_id, t501.c501_order_id order_id, t501.c7100_case_info_id, ");
    sbQuery.append(" t502.c205_part_number_id pnum, ");
    sbQuery.append(" get_partnum_desc (t502.c205_part_number_id) pdesc, ");
    sbQuery.append(" t502.c901_type itype, SUM (NVL (t502.c502_item_qty, 0)) iqty ");
    sbQuery.append(" FROM t501_order t501, t502_item_order t502 ");
    sbQuery.append(" WHERE (t501.c501_order_id = '" + strSessOrderId);
    sbQuery.append("' OR t501.c501_parent_order_id ='" + strSessOrderId
        + "')  AND t501.c501_order_id = t502.c501_order_id ");
    sbQuery.append(" AND t501.c501_delete_fl IS NULL ");
    sbQuery.append(" AND t501.c501_void_fl IS NULL ");
    sbQuery.append(" AND t502.c502_void_fl IS NULL ");
    sbQuery.append(" GROUP BY t501.c501_order_id, ");
    sbQuery.append(" t501.c7100_case_info_id, ");
    sbQuery.append(" t502.c205_part_number_id, ");
    sbQuery.append(" t502.c901_type, t501.c501_parent_order_id) t502, ");
    sbQuery.append(" t5012_tag_usage t5012 ");
    sbQuery
        .append(" WHERE  NVL( to_char(t502.c7100_case_info_id) ,t502.porder_id)  = t5012.c5012_ref_id(+) ");
    sbQuery.append(" AND t5012.c901_ref_type(+) = 2570 ");
    sbQuery.append(" AND t502.pnum = t5012.c205_part_number_id(+) ");
    sbQuery.append(" AND t5012.c5012_void_fl(+) IS NULL ");
    sbQuery.append(" ORDER BY pnum, pdesc, itype ");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug("fetchDOTagDetails " + sbQuery.toString());
    return alReturn;
  }

  /**
   * fetchDOControlNumDetails - This method will fetch the DO Control number Details
   * 
   * @param String strOrderID
   * @return ArrayList
   * @exception AppError
   */

  // MNTTASK-8623 - CSM DO Process control # from S Part - changed query
  public ArrayList fetchDOControlNumDetails(String strOrderId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_fch_do_ctrl_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strOrderId);

    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    log.debug("alReturn.size()::" + alReturn.size());
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchDOReturnsDetails - This method will fetch the DO Return Details
   * 
   * @param String strOrderID
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchDOReturnsDetails(String strOrderID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT t506.c506_rma_id ra_id, get_code_name (c506_type) ra_type, ");
    sbQuery.append("  c506_return_date return_date, c506_credit_date op_credit_dt,  ");
    sbQuery.append(" c506_ac_credit_dt acc_credit_dt, c506_credit_memo_id credit_memo_id,  ");
    sbQuery.append(" c503_invoice_id invoice_id   FROM t506_returns t506 ");
    sbQuery.append("  WHERE t506.c501_order_id ");
    sbQuery.append("  IN (select C501_ORDER_ID from T501_ORDER where C501_ORDER_ID ='" + strOrderID
        + "' ");
    sbQuery.append(" OR C501_PARENT_ORDER_ID='" + strOrderID + "' ");
    sbQuery.append(" )");
    sbQuery.append(" AND t506.c506_void_fl IS NULL ");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    // log.debug("fetchDOReturnsDetails  "+sbQuery.toString());
    return alReturn;
  }

  public HashMap validateNewOrder(String strCaseInfoId) throws Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strParentID = "";
    String strOrderID = "";
    gmDBManager.setPrepareString("gm_pkg_op_do_process_report.gm_validate_newdo", 3);
    gmDBManager.setString(1, strCaseInfoId);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strParentID = gmDBManager.getString(2);
    strOrderID = gmDBManager.getString(3);
    gmDBManager.close();
    hmReturn.put("PARENTID", strParentID);
    hmReturn.put("ORDERID", strOrderID);
    return hmReturn;
  }

  public void saveDOShippInfo(HashMap hmParam) throws Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strSource = GmCommonClass.parseZero((String) hmParam.get("SOURCE"));
    String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPID"));
    String strShipCarr = GmCommonClass.parseZero((String) hmParam.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseZero((String) hmParam.get("SHIPMODE"));
    String strTrackNo = GmCommonClass.parseNull((String) hmParam.get("TRACKNO"));
    String strAddressId = GmCommonClass.parseZero((String) hmParam.get("REPADDRESSID"));
    String strFreight = GmCommonClass.parseNull((String) hmParam.get("FREIGHT"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_update_ship_info", 11);

    gmDBManager.setString(1, strOrderID);
    gmDBManager.setInt(2, Integer.parseInt(strSource));
    gmDBManager.setInt(3, Integer.parseInt(strShipTo));
    gmDBManager.setString(4, strShipToId);
    gmDBManager.setInt(5, Integer.parseInt(strShipCarr));
    gmDBManager.setInt(6, Integer.parseInt(strShipMode));
    gmDBManager.setString(7, strTrackNo);
    gmDBManager.setInt(8, Integer.parseInt(strAddressId));
    gmDBManager.setString(9, strFreight);
    gmDBManager.setString(10, strFreight);
    gmDBManager.setString(11, strUserId);
    gmDBManager.execute();
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strOrderID, strLog, strUserId, "1200"); // 1200 is Order
    }
    gmDBManager.commit();
  }

  public ArrayList fetchCaseTagID(String strCaseInfoID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_op_do_process_report.gm_fch_case_tags", 2);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  public void updateDOTagDetail(HashMap hmParam) throws AppError {
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_update_tagid", 4);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strCaseInfoID);
    gmDBManager.setString(3, strInputStr);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();
  }

  public void saveMultipleShippInfo(HashMap hmParam) throws Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strSource = GmCommonClass.parseZero((String) hmParam.get("SOURCE"));
    String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTOID"));// sales rep
    String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPID"));// salesrepid
    String strShipCarr = GmCommonClass.parseZero((String) hmParam.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseZero((String) hmParam.get("SHIPMODE"));
    String strTrackNo = GmCommonClass.parseNull((String) hmParam.get("TRACKNO"));
    String strAddressId = GmCommonClass.parseZero((String) hmParam.get("REPADDRESSID"));
    String strFreight = GmCommonClass.parseNull((String) hmParam.get("FREIGHT"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));// partnumbers

    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_save_multiple_shippinfo", 11);

    gmDBManager.setString(1, strOrderID);
    gmDBManager.setInt(2, Integer.parseInt(strSource));
    gmDBManager.setInt(3, Integer.parseInt(strShipTo));
    gmDBManager.setString(4, strShipToId);
    gmDBManager.setInt(5, Integer.parseInt(strShipCarr));
    gmDBManager.setInt(6, Integer.parseInt(strShipMode));
    gmDBManager.setString(7, strTrackNo);
    gmDBManager.setInt(8, Integer.parseInt(strAddressId));
    gmDBManager.setString(9, strFreight);
    gmDBManager.setString(10, strInputStr);
    gmDBManager.setString(11, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public HashMap fetchOrderDetails(String strOrdId) throws AppError {
    String strDelNote = GmCommonClass.parseNull(GmCommonClass.getString("CREATE_DELNOT"));
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());

    String strAliasName = "SHIPCOST";
    strAliasName =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("US", "COUNTRY")).equalsIgnoreCase("Y") ? strAliasName
            : "";

    // log.debug("sbOrderIdQuery..........."+sbOrderIdQuery.toString());

    sbQuery
        .append(" SELECT	T501.C501_ORDER_ID ID,gm_pkg_op_return.get_return_reason(T501.C501_ORDER_ID,'orders') rareason, get_cs_ship_email (4121, T501.c703_sales_rep_id) repemail, GET_ACCOUNT_NAME(T501.C704_ACCOUNT_ID) ANAME,T501.C501_DO_DOC_UPLOAD_FL DO_FLAG, ");
    sbQuery
        .append(" get_account_address(T501.C704_ACCOUNT_ID) ACCADD ,t501.c101_dealer_id DEALERID,GET_PARTY_NAME(t501.c101_dealer_id) DEALERNM, get_dealer_address(t501.c101_dealer_id) DEALERADD, ");
    sbQuery
        .append(" GET_DIST_REP_NAME(T501.C703_SALES_REP_ID) REPDISTNM,GET_REP_NAME(T501.C703_SALES_REP_ID) REPNM, ");
    sbQuery
        .append(" GET_SHIP_ADD(T501.C501_SHIP_TO,T501.C704_ACCOUNT_ID,T501.C703_SALES_REP_ID,T501.C501_SHIP_TO_ID) SHIPADD_OLD, ");
    sbQuery.append(" gm_pkg_cm_shipping_info.get_ship_add('");
    sbQuery.append(strOrdId);
    sbQuery.append("', '50180') SHIPADD, "); // source orders 50180
    sbQuery
        .append(" T501.C501_ORDER_DATE ODT, T501.c501_surgery_date ORD_SURG_DT, T7100.c7100_surgery_date SDT, T501.C501_CUSTOMER_PO PO, GET_BILL_ADD(T501.C704_ACCOUNT_ID) BILLADD, ");
    sbQuery
        .append(" T501.C501_TOTAL_COST + NVL(T501.C501_SHIP_COST,0) SALES, NVL(T501.C501_SHIP_COST, 0) "
            + strAliasName + " ,");
    sbQuery
        .append(" t501.c501_shipping_date SHIPPING_DT, T501.C501_COMMENTS COMMENTS, GET_SORTED_LOG_COMMENTS('"
            + strOrdId
            + "','1200','106981') LOGCOMMENTS,GET_USER_NAME(T501.C501_CREATED_BY) UNAME, GET_CS_SHIP_NAME(T501.C501_SHIP_TO,T501.C501_SHIP_TO_ID) SHIPTONAME,");
    sbQuery.append(" to_char(T501.C501_SHIPPING_DATE,'" + getCompDateFmt()
        + " HH24:MI')  SHIPTIME, ");
    // sbQuery.append(" C501_SHIP_TO SHIPTO, C501_SHIP_TO_ID SHIPTOID,
    // C704_ACCOUNT_ID ACCID, ");
    // should call carrier,mode,tracking no
    sbQuery.append(" gm_pkg_cm_shipping_info.get_shipto('");
    sbQuery.append(strOrdId);
    sbQuery.append("', DECODE(T501.C901_ORDER_TYPE,101260,'11390','50180')) SHIPTO,");
    sbQuery.append(" gm_pkg_cm_shipping_info.get_shipto_id('");
    sbQuery.append(strOrdId);
    sbQuery.append("', DECODE(T501.C901_ORDER_TYPE,101260,'11390','50180')) SHIPTOID,");
    sbQuery.append(" gm_pkg_cm_shipping_info.get_track_no('");
    sbQuery.append(strOrdId);
    sbQuery.append("', DECODE(T501.C901_ORDER_TYPE,101260,'11390','50180')) TRACKNO,");
    sbQuery.append(" gm_pkg_cm_shipping_info.get_carrier('");
    sbQuery.append(strOrdId);
    sbQuery.append("', DECODE(T501.C901_ORDER_TYPE,101260,'11390','50180')) CARRIER,");
    sbQuery.append(" gm_pkg_cm_shipping_info.get_ship_mode('");
    sbQuery.append(strOrdId);
    sbQuery.append("', DECODE(T501.C901_ORDER_TYPE,101260,'11390','50180')) SHIPMODE,");
    sbQuery
        .append(" get_account_curr_symb(T501.C704_ACCOUNT_ID) ACC_CURR_SYMB, GET_CODE_NAME_ALT(T704.C901_CURRENCY) ACC_CURRN_NM, ");
    sbQuery.append(" T704.C901_CURRENCY ACC_CURR_ID, ");
    sbQuery
        .append(" T501.C704_ACCOUNT_ID ACCID,T704.C704_CUSTOMER_REF_ID CUSTREFID, get_order_attribute_value('"
            + strOrdId + "','103560') ORDERSRC, "); // 103560 - order source
    // attribute type
    sbQuery
        .append(" T501.C901_ORDER_TYPE ORDERTYPE, T501.C901_REASON_TYPE REASONTYPE, T501.C501_PARENT_ORDER_ID PARENTORDID, T501.C503_INVOICE_ID INVOICEID, "); // ..
    sbQuery
        .append(" GET_CODE_NAME (T501.C901_ORDER_TYPE) ORDERTYPENAME, T7100.C7100_CASE_ID CASE_ID, T7100.c7100_case_info_id caseinfo_id ,");
    sbQuery
        .append(" T501.C501_VOID_FL VOIDFL, T501.C501_RECEIVE_MODE RMODE , T501.C501_STATUS_FL SFL, T501.C501_UPDATE_INV_FL INVFL, ");
    sbQuery
        .append(" T501.C501_RECEIVED_FROM RFROM, T501.C501_HARD_PO_FL HARDPOFL,  gm_pkg_op_do_process_report.get_order_status_desc (t501.C501_ORDER_ID) STATUSFL");
    sbQuery.append(" , GET_TOTAL_SALES_AMT ('");
    sbQuery.append(strOrdId);
    sbQuery.append("') total_sales");
    sbQuery.append(" , T501.c703_sales_rep_id repid");

    sbQuery.append(" , GET_CODE_NAME(T704.C704_PAYMENT_TERMS) PMTTERM ");// Getting
    // the
    // Payment
    // term
    // of
    // the
    // account
    // to
    // "Quotation"
    // print
    if (strDelNote.equalsIgnoreCase("yes")) {
      sbQuery.append(" , C501_REF_ID ACKREFID ");
    }
    sbQuery.append(" , get_order_attribute_value('" + strOrdId + "','103926') REQDATE ");
    sbQuery.append(" , get_ac_company_id(T501.C704_ACCOUNT_ID) COMPID ");
    sbQuery.append(" ,GET_TOTAL_ORD_INV_AMT(T501.C501_ORDER_ID) total_invoice ");
    sbQuery
        .append(" ,DECODE (get_distributor_type (get_account_dist_id (t501.c704_account_id)), 70106,");
    sbQuery
        .append(" NVL (get_rule_value (50218, get_distributor_inter_party_id (get_account_dist_id ( t501.c704_account_id))), get_comp_curr_symbol(t501.c1900_company_id)), get_comp_curr_symbol(t501.c1900_company_id)) CURRENCY ,t501.c501_hold_fl hold_fl, C501_HOLD_ORDER_PROCESS_FL HOLD_ORDER_PROCESS_FL, DECODE(gm_pkg_op_do_process_trans.gm_sav_order_hold_flag('"
            + strOrdId
            + "', T501.C501_CREATED_BY),1,'Y','') HOLD_FL_ACK, GET_ACCOUNT_ATTRB_VALUE(T704.C704_ACCOUNT_ID, 101183) VATEXCL, GET_ACCOUNT_ATTRB_VALUE(T704.C704_ACCOUNT_ID, 101188) ACCVATTAX ");
    sbQuery
        .append(" ,get_rule_value('DOMAINDB','DOMAINPACKSLIP') RULEDOMAIN , get_rule_value('RETURNSUMMARY','LOGO') RULEIMAGE, t704.c704_ship_attn_to ATTENTO , get_account_type(T501.C704_ACCOUNT_ID) ACCOUNTTYPE ,T501.C501_CREATED_BY USERID ,get_rule_value('PARTSIZE','SHOWPARTSIZE') SHOWSIZE ");
    sbQuery
        .append(" ,T501.c1910_division_id division_id, get_company_dtfmt(T501.c1900_company_id) CMPDFMT, t501.c1900_company_id COMPANYCD, get_company_code (t501.C1900_COMPANY_ID) comp_code ");
    sbQuery.append(",T501.c5040_plant_id plantid, get_plant_name (T501.c5040_plant_id) plantName ");
    sbQuery
        .append(", GET_ACCOUNT_ATTRB_VALUE (T704.C704_ACCOUNT_ID, 10306124) SHIPPURPOSE, GET_ACCOUNT_ATTRB_VALUE (T704.C704_ACCOUNT_ID, 10306125) CONSIGNEETAXID, GET_ACCOUNT_ATTRB_VALUE (T704.C704_ACCOUNT_ID, 10306126) CONSIGNEEPERSON");
    sbQuery.append(" ,GET_ACCOUNT_ATTRB_VALUE (T704.C704_ACCOUNT_ID, 106642) VENDORID ");
    sbQuery.append(", gm_pkg_op_ict.get_ship_date (T501.C501_ORDER_ID) ship_date ");
    sbQuery
        .append(",   GET_CODE_NAME(DECODE(t501.c901_contract,0,'',t501.c901_contract)) CONTRACTTP ");

    sbQuery.append(" ,get_order_attribute_value('" + strOrdId + "','103560') IPADORDER ");
    sbQuery.append(" ,T501.c501_po_amount POAMT ");
    sbQuery.append(" ,T501.c501_copay COPAY ");
    sbQuery
        .append(" ,T501.c501_cap_amount CAPAMOUNT,NVL(get_order_attribute_value(T501.C501_ORDER_ID,'400152'),1) EXCH_RATE ");
    sbQuery
        .append(", get_code_name(t501.c501_receive_mode) MODEOFORDER, GET_ACCOUNT_ATTRB_VALUE(T704.C704_ACCOUNT_ID,106100) OVRIDE_CURR");// 106100:
    sbQuery
        .append(", T501.C501_ORDER_DATE_HIS_FL ORD_DT_HIST_FL , GET_RULE_VALUE('ORDER_DATE_EDIT_DAYS','DO_ORDER')  ORD_DATE_EDIT_DAYS ");// to
                                                                                                                                         // get
                                                                                                                                         // the
                                                                                                                                         // editable
                                                                                                                                         // days
                                                                                                                                         // range
                                                                                                                                         // and
                                                                                                                                         // history
                                                                                                                                         // flag
                                                                                                                                         // of
                                                                                                                                         // the
                                                                                                                                         // order
    sbQuery
        .append(", DECODE (SIGN(NVL(GET_RULE_VALUE('ORDER_DATE_EDIT_DAYS','DO_ORDER'),0)-(TRUNC(current_date)-TRUNC(C501_ORDER_DATE))),-1,'N','Y') ORD_DATE_EDIT_FLG ");// to
                                                                                                                                                                        // check
                                                                                                                                                                        // whether
                                                                                                                                                                        // the
                                                                                                                                                                        // order
                                                                                                                                                                        // date
                                                                                                                                                                        // is
                                                                                                                                                                        // more
                                                                                                                                                                        // than
                                                                                                                                                                        // editable
                                                                                                                                                                        // date
                                                                                                                                                                        // range
                                                                                                                                                                        // or
                                                                                                                                                                        // not
    sbQuery
        .append(", get_party_name(t704d.C101_GPO) GPONAME, get_party_name(get_account_gpo_id (T704.C704_ACCOUNT_ID)) GPBNAME");
    // Override
    // Currency
    
    sbQuery
    .append(", C501_Last_Updated_By LUSERID");
    sbQuery
    .append(", gm_pkg_op_storage_building.get_building_short_name(T501.c5057_building_id) BUILDINGNAME");//for PMT-36675
    sbQuery
    .append(", T501.C501_CUSTOMER_PO_DATE PODATE ");//PC-3880 New field in record PO Date in GM Italy
    sbQuery
        .append(" FROM T501_ORDER T501,  T7100_CASE_INFORMATION T7100, T704_ACCOUNT T704, T704D_ACCOUNT_AFFLN t704d ");// Payment
    // term
    // is
    // getting
    // from
    // the
    // account
    // table
    sbQuery.append(" WHERE T501.C501_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery.append("' AND T501.C7100_CASE_INFO_ID =  T7100.C7100_CASE_INFO_ID(+)");
    sbQuery.append(" AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID");
    sbQuery.append(" AND t704.C704_ACCOUNT_ID = t704d.C704_ACCOUNT_ID (+) ");
    sbQuery.append(" AND t704d.C704D_VOID_FL(+) IS NULL ");
    sbQuery
        .append(" AND (T501.C501_DELETE_FL IS NULL OR ( T501.C501_DELETE_FL = 'Y' AND T501. c901_order_type= '102080')) AND T7100.C7100_VOID_FL IS NULL");
    sbQuery.append(" AND T704.C704_VOID_FL IS NULL AND t501.c501_void_fl      IS NULL");

    log.debug(" Query for order details " + sbQuery.toString());

    return dbManager.querySingleRecord(sbQuery.toString());
  }

  /**
   * fetchCartDetails - This method will fetch the part number details to the cart
   * 
   * @param String , String.
   * @return ArrayList
   * @exception AppError
   */
  @Override
  public ArrayList fetchCartDetails(HashMap hmParams) throws AppError {
    String strOrdId = GmCommonClass.parseNull((String) hmParams.get("ORDERID"));
    String strProjectType = GmCommonClass.parseNull((String) hmParams.get("PROJECTTYPE"));
    String strAction = GmCommonClass.parseNull((String) hmParams.get("ACTIONTYPE"));
    String strDelNote = GmCommonClass.parseNull(GmCommonClass.getString("CREATE_DELNOT"));
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParams.get("gmDataStoreVO");
    String strCompanyId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(gmDataStoreVO);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    sbQuery.setLength(0);

    sbQuery
        .append(" SELECT t501.C501_ORDER_ID ORDERID,t502.C205_PART_NUMBER_ID ID,t502.C205_PART_NUMBER_ID PNUM ,get_rule_value_by_company('"
            + strCompanyId
            + "_DOMAINDB','DOMAINPACKSLIP',"
            + strCompanyId
            + ") RULEDOMAIN, C502_ITEM_ORDER_ID IORDID, get_partdesc_by_company(t502.C205_PART_NUMBER_ID) PDESC, ");
    sbQuery
        .append(" C502_ITEM_QTY QTY, C502_CONTROL_NUMBER CNUM, C502_ITEM_PRICE PRICE, C502_ITEM_ORDER_ID ITEMID ,C502_BASE_PRICE BASE_PRICE ");
    sbQuery
        .append(" ,(C502_ITEM_QTY * C502_ITEM_PRICE) TOTAL_VALUE, GET_COUNT_PARTID(t501.C501_ORDER_ID,t502.C205_PART_NUMBER_ID) PARTQTY");
    sbQuery
        .append(" ,C901_TYPE ITEMTYPE , GET_CODE_NAME_ALT(C901_TYPE) ITEMTYPEDESC, GET_CODE_NAME(C901_TYPE) ITEMTYPEFULLDESC ");
    sbQuery.append(" ,c502_construct_fl CONSFL ");
    if (strDelNote.equalsIgnoreCase("yes")) {
      sbQuery
          .append(" ,get_ack_released_qty(t502.C205_PART_NUMBER_ID,'"
              + strOrdId
              + "',c502_item_price,c502_unit_price_adj_value,c901_unit_price_adj_code) shipqty, NVL(get_qty_in_stock(t502.C205_PART_NUMBER_ID),0) shelfqty");
      sbQuery
          .append(" ,C502_ITEM_QTY-get_ack_released_qty(t502.C205_PART_NUMBER_ID,'"
              + strOrdId
              + "',c502_item_price,c502_unit_price_adj_value,c901_unit_price_adj_code) pendqty,((C502_ITEM_QTY-get_ack_released_qty(t502.C205_PART_NUMBER_ID,'"
              + strOrdId
              + "',c502_item_price,c502_unit_price_adj_value,c901_unit_price_adj_code)) * C502_ITEM_PRICE) TOTAL_AMT ");
    }
    sbQuery
        .append(" ,GET_CS_PENDSHIP_QTY(t501.C501_ORDER_ID,t502.C205_PART_NUMBER_ID,C502_ITEM_ORDER_ID) PENDSHIPQTY ");
    sbQuery
        .append(" ,c502_ref_id refid , NVL(get_account_part_pricing (t501.c704_account_id, t502.c205_part_number_id, get_account_gpo_id (t501.c704_account_id)), 0) CURR_PRICE ");
    sbQuery
        .append(" , get_part_attr_value_by_comp (t502.C205_PART_NUMBER_ID,200001, t501.c1900_company_id) PCODE ");
    sbQuery
        .append(" , NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t502.c205_part_number_id, '");
    sbQuery.append(strOrdId);
    sbQuery
        .append("' , DECODE (t501.c501_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location (t502.c205_part_number_id, '')) SUGLOCATION  ");
    // Need to get the location cd for Pick slip
    sbQuery.append(" , gm_pkg_op_item_control_rpt.get_location_cd (t502.c205_part_number_id, '");
    sbQuery.append(strOrdId);
    sbQuery
        .append("', DECODE (t501.c501_status_fl, '2', 'pick', '3', 'put', 'pick'), '','',t502.c502_control_number) SUGLOCATIONCD , NVL(gm_pkg_op_inv_warehouse.GET_RESERVED_QTY('"
            + strOrdId
            + "',t502.c205_part_number_id),0) RESERVQTY ,get_rule_value('PARTRESERVE','RESERVEMESSAGE') RULEMESSAGE , get_part_size (t502.c205_part_number_id, t502.c502_control_number ) partsize  ");
    sbQuery
        .append(", gm_pkg_op_ack_order.get_reserved_qty(t501.C501_ORDER_ID,t502.c205_part_number_id) RESVQTY ");
    sbQuery
        .append(" ,DECODE(t502.c502_unit_price_adj_value ,NULL,'','0','','Y') UADJFL ,NVL(t502.C502_UNIT_PRICE,'0') unitprice , NVL(t502.c502_unit_price_adj_value,'0') UnitpriceAdj");
    sbQuery
        .append(" , t502.c901_unit_price_adj_code UnitpriceAdjCodeID,DECODE(t502.c901_unit_price_adj_code,NULL,'N/A',GET_CODE_NAME (t502.c901_unit_price_adj_code)) unitpriceAdjCode, t502.c502_ITEM_PRICE netunitprice ");
    sbQuery
        .append(" ,  NVL(t502.c502_item_qty*t502.C502_UNIT_PRICE,0) beforePrice  ,  NVL(t502.c502_item_qty*t502.C502_ITEM_PRICE,0) afterPrice ");
    sbQuery
        .append(" ,  get_rule_value (t502.C205_PART_NUMBER_ID, 'NONUSAGE') UFREE , DECODE (t501.C901_ORDER_TYPE, NULL, 2521, t501.C901_ORDER_TYPE) ORDTYP, t501.C901_ORDER_TYPE ORDERTYPE");
    sbQuery
        .append(" , NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice(t501.c704_account_id,t502.c205_part_number_id, (SELECT t740.c101_party_id from t740_gpo_account_mapping t740 where t740.c704_account_id = t501.c704_account_id)),0) CURRPORTPRICE ");
    sbQuery
        .append(" , t502.C502_DO_UNIT_PRICE ACCOUNTPRICE, t502.c502_adj_code_value CODEADJUST, NVL(GET_ORDER_ATTRIBUTE_VALUE(t501.c501_order_id,400152),1) exch_rate ");// 400152:
                                                                                                                                                                        // Exchange
    sbQuery
        .append(", GET_LOT_EXPIRY_DATE (t502.c205_part_number_id, t502.C502_CONTROL_NUMBER) PRODEXPIRY , get_partnum_product(t502.c205_part_number_id) PRODFAMILY"); //PC-5584 - Exclude usage code or construct code from PSFG

    sbQuery
    .append(", to_char(t2550.C2550_EXPIRY_DATE, '"+strApplDateFmt+"') EXPIRYDATE ");
    // rate
    sbQuery.append(" FROM T502_ITEM_ORDER T502, t501_order t501, t2550_part_control_number t2550"); //  Join t2550 lot master and get expiry date for the lot # from t502_item_order in PC-4081

    if (!strAction.equals("AdjPopup")) {
      sbQuery.append(" WHERE t501.C501_ORDER_ID = '");
      sbQuery.append(strOrdId);
      sbQuery
          .append("' AND C502_DELETE_FL IS NULL AND t501.c501_void_fl       IS NULL AND t502.c502_void_fl IS NULL AND t501.C501_ORDER_ID = t502.C501_ORDER_ID");
    } else {
      sbQuery.append(" WHERE (t501.C501_ORDER_ID = '");
      sbQuery.append(strOrdId);
      sbQuery.append("' OR t501.c501_parent_order_id = '");
      sbQuery.append(strOrdId);
      sbQuery
          .append("') AND C502_DELETE_FL IS NULL AND t501.c501_void_fl       IS NULL AND t502.c502_void_fl IS NULL AND t501.C501_ORDER_ID = t502.C501_ORDER_ID");
    }
    if (strProjectType != null && !strProjectType.equals("")) {
      sbQuery.append(" AND t502.C205_PART_NUMBER_ID IN (SELECT T205.C205_PART_NUMBER_ID FROM ");
      sbQuery.append(" T205_PART_NUMBER T205, T202_PROJECT T202");
      sbQuery.append(" WHERE T205.C202_PROJECT_ID = T202.C202_PROJECT_ID");
      // 1500 maps to Globus Project
      if (strProjectType.equals("1500")) {
        sbQuery.append(" AND T202.C901_PROJECT_TYPE = ");
        sbQuery.append(strProjectType);
      } else {
        sbQuery.append(" AND T202.C901_PROJECT_TYPE = ");
        sbQuery.append(strProjectType);
      }
      sbQuery.append(" ) ");

    }
    sbQuery.append(" AND t502.C205_PART_NUMBER_ID = t2550.C205_PART_NUMBER_ID(+) ");  
    sbQuery.append(" AND t502.C502_CONTROL_NUMBER = t2550.C2550_CONTROL_NUMBER(+) ");		
    sbQuery.append(" ORDER BY t501.C501_ORDER_ID,  T502.C205_PART_NUMBER_ID");

    log.debug(" Query for CARTDETAILS " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }

  public HashMap fetchShipDetails(String strOrdId) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT	C501_SHIPPING_DATE SDT, C501_DELIVERY_CARRIER SCAR, C501_DELIVERY_MODE SMODE,  ");
    sbQuery.append(" C501_TRACKING_NUMBER STRK , GET_CODE_NAME(C501_DELIVERY_CARRIER) SCARNM, ");
    sbQuery.append(" GET_CODE_NAME(C501_DELIVERY_MODE) SMODENM, C501_SHIP_COST SCOST, ");
    sbQuery.append(" c501_ship_to SHIPTO, c501_ship_to_id SHIPTOID ");
    sbQuery.append(" FROM T501_ORDER ");
    sbQuery.append(" WHERE C501_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery.append("' AND ( C501_DELETE_FL IS NULL");
    sbQuery.append(" or ( c501_delete_fl    = 'Y' ");
    sbQuery.append(" AND c901_order_type   = '102080')) ");
    log.debug(" Query for SHIPDETAILS " + sbQuery.toString());
    return dbManager.querySingleRecord(sbQuery.toString());
  }

  public ArrayList fetchRelatedOrderId(String strOrdId) throws AppError {
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    return dbManager.queryMultipleRecords(getOrderIDQuery(strOrdId));
  }

  private String getOrderIDQuery(String strOrdId) {
    StringBuffer sbOrderIdQuery = new StringBuffer();
    sbOrderIdQuery.append("SELECT C501_ORDER_ID SBID FROM T501_ORDER ");
    sbOrderIdQuery.append("WHERE C501_ORDER_ID =  '");
    sbOrderIdQuery.append(strOrdId);
    sbOrderIdQuery.append("'  OR C501_PARENT_ORDER_ID = '");
    sbOrderIdQuery.append(strOrdId);
    sbOrderIdQuery.append("'");
    return sbOrderIdQuery.toString();
  }

  public ArrayList getBackOrderDetails(String strOrdId, String strProjectType) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    /*******************************************************************
     * Below code to get Back Order details
     ******************************************************************/
    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT A.C205_PART_NUMBER_ID ID, A.C502_ITEM_ORDER_ID IORDID, GET_PARTNUM_DESC(A.C205_PART_NUMBER_ID) PDESC,");
    sbQuery
        .append(" A.C502_ITEM_QTY QTY, A.C502_CONTROL_NUMBER CNUM, A.C502_ITEM_PRICE PRICE, A.C502_ITEM_ORDER_ID ITEMID ");
    sbQuery
        .append(" ,(A.C502_ITEM_QTY * A.C502_ITEM_PRICE) TOTAL_VALUE, B.C501_ORDER_ID ORDID, B.C501_SHIPPING_DATE SDATE");
    sbQuery.append(" ,A.C901_TYPE ITEMTYPE , GET_CODE_NAME_ALT(C901_TYPE) ITEMTYPEDESC ");
    sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B ");
    sbQuery
        .append(" WHERE A.C501_ORDER_ID IN (SELECT  C501_ORDER_ID FROM T501_ORDER  WHERE C501_PARENT_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery
        .append("' ) AND A.C502_DELETE_FL IS NULL AND A.C501_ORDER_ID = B.C501_ORDER_ID AND A.C502_VOID_FL IS NULL ");
    // To check if its not a globus part
    if (strProjectType != null && !strProjectType.equals("")) {
      sbQuery.append(" AND A.C205_PART_NUMBER_ID IN (SELECT T205.C205_PART_NUMBER_ID FROM ");
      sbQuery.append(" T205_PART_NUMBER T205, T202_PROJECT T202");
      sbQuery.append(" WHERE T205.C202_PROJECT_ID = T202.C202_PROJECT_ID");
      // 1500 maps to Globus Project
      if (strProjectType.equals("1500")) {
        sbQuery.append(" AND T202.C901_PROJECT_TYPE = ");
        sbQuery.append(strProjectType);
      } else {
        sbQuery.append(" AND T202.C901_PROJECT_TYPE = ");
        sbQuery.append(strProjectType);
      }
      sbQuery.append(" ) ");

    }
    sbQuery.append(" AND B.C901_ORDER_TYPE = 2525 ");
    sbQuery.append(" ORDER BY  B.C501_ORDER_ID, A.C205_PART_NUMBER_ID ");
    // log.debug(" Query for BACKORDER " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }

  public ArrayList fetchConstructDetails(String strOrdId) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    // To get the Construct Pricing Info for the Order
    sbQuery.setLength(0);
    sbQuery.append(" SELECT B.C741_CONSTRUCT_NM CONID, B.C741_CONSTRUCT_DESC CONNM, ");
    sbQuery.append(" C512_CONSTRUCT_VALUE CVALUE, A.C501_ORDER_ID ORDID ");
    sbQuery.append(" FROM T512_ORDER_CONSTRUCT A, T741_CONSTRUCT B ");
    sbQuery.append(" WHERE A.C741_CONSTRUCT_ID = B.C741_CONSTRUCT_ID AND C501_ORDER_ID IN ( ");
    sbQuery.append(getOrderIDQuery(strOrdId));
    sbQuery.append(" ) AND C512_VOID_FL IS NULL");
    // log.debug(" Query for CONSTRUCTDETAILS " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }

  public ArrayList fetchOrderSummary(String strOrdId) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("ORDERID", strOrdId);
    return fetchOrderSummary(hmParam);
  }

  /**
   * fetchOrderSummary - This method will fetch the order summary details
   * 
   * @param String strOrdId
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchOrderSummary(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    GmDORptBean gmDORptBean = new GmDORptBean(getGmDataStoreVO());

    String strOrdId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    // Added for PMT-51170, to exclude discount orders for sales rep in DO summary
    String strExclOrdType = GmCommonClass.parseNull((String) hmParam.get("EXCLORDTYPE"));

    String strAllOrders = GmCommonClass.parseNull(gmDORptBean.getAllOrderIds(strOrdId,strExclOrdType));
    // Use this for displaying Order summary in the Order Summary Screen
    // - Part of Capitated Pricing changes
    sbQuery.setLength(0);
    sbQuery.append(" SELECT B.C501_ORDER_ID ORDID, A.C205_PART_NUMBER_ID ID, ");
    if (strOpt.equals("BOD") || strOpt.equals("DORECEIPT")) {
      sbQuery.append(" get_partdesc_by_company(A.C205_PART_NUMBER_ID) PDESC, ");
      sbQuery.append(" t502b.c502b_usage_lot_num USAGELOT ,");

    } else {
      sbQuery.append(" GET_PARTNUM_DESC(A.C205_PART_NUMBER_ID) PDESC, ");
    }

    sbQuery.append(" A.C502_ITEM_QTY QTY, A.C502_CONTROL_NUMBER CNUM, A.C502_ITEM_PRICE PRICE ");

    sbQuery
        .append(" ,(A.C502_ITEM_QTY * A.C502_ITEM_PRICE) EXT_PRICE, B.C501_SHIPPING_DATE SDATE ");
    sbQuery
        .append(" ,DECODE(B.C901_ORDER_TYPE,null,2521,B.C901_ORDER_TYPE) ORDTYP, B.C501_ORDER_DATE ORDDT,B.C501_SURGERY_DATE SURG_DT");
    sbQuery.append(" ,A.C901_TYPE ITEMTYPE , GET_CODE_NAME_ALT(C901_TYPE) ITEMTYPEDESC ");
    sbQuery.append(" ,gm_pkg_op_do_process_report.get_order_status_desc(B.C501_ORDER_ID) SFL ");
    sbQuery
        .append(" ,A.C502_CONSTRUCT_FL CONFL, NVL(get_account_part_pricing (b.c704_account_id, a.c205_part_number_id, get_account_gpo_id (b.c704_account_id)), 0) CURR_PRICE ");
    sbQuery
        .append(" , get_rule_value (DECODE (b.c901_order_type, NULL, 2521, b.c901_order_type), 'DOEXORDERTYPE') DOEXORDERTYPE ");
    sbQuery
        .append(" , get_part_attr_value_by_comp (A.C205_PART_NUMBER_ID,200001, B.c1900_company_id) PCODE ,gm_pkg_op_do_process_trans.get_item_attribute_val('"
            + strOrdId
            + "',A.c205_part_number_id) attr , get_rule_value(A.C205_PART_NUMBER_ID,'NONUSAGE') UFREE");
    // sbQuery.append(" , get_item_order_attribute_value('"+strOrdId+"',A.C205_PART_NUMBER_ID,101732) replenishreq ");
    sbQuery
        .append(" , REPLENISH.ATR_VAL replenishreq, NVL(A.c502_tax_amt, ROUND (NVL (A.C502_ITEM_QTY,0) * NVL (NVL(A.C502_EXT_ITEM_PRICE,A.C502_ITEM_PRICE), 0) * NVL (A.c502_vat_rate, 0) / 100,2))  tax_cost ");

    sbQuery
        .append(" ,  NVL( ROUND (NVL (A.C502_ITEM_QTY,0) * NVL (NVL(A.C502_EXT_ITEM_PRICE,A.C502_ITEM_PRICE), 0) * NVL (A.C502_IGST_RATE, 0) / 100,2),0) IGST_COST");

    sbQuery
        .append(" ,  NVL( ROUND (NVL (A.C502_ITEM_QTY,0) * NVL (NVL(A.C502_EXT_ITEM_PRICE,A.C502_ITEM_PRICE), 0) * NVL (A.C502_SGST_RATE, 0) / 100,2),0) SGST_COST");

    sbQuery
        .append(" ,  NVL( ROUND (NVL (A.C502_ITEM_QTY,0) * NVL (NVL(A.C502_EXT_ITEM_PRICE,A.C502_ITEM_PRICE), 0) * NVL (A.C502_CGST_RATE, 0) / 100,2),0) CGST_COST");

    // added new key for jasper :key -
    // UPRICEORDERDTL,UPRICEADJORDERDTL,EXT_UNIT_PRICE_ORDERDTL
    sbQuery
        .append(" , NVL(A.C502_UNIT_PRICE,0) UPRICE, get_formatted_number(NVL(A.C502_UNIT_PRICE,0),2) UPRICEORDERDTL,A.c502_unit_price_adj_value UPRICEADJ ,get_formatted_number(A.c502_unit_price_adj_value,2) UPRICEADJORDERDTL, A.c901_unit_price_adj_code ADJCODEID, get_code_name(A.c901_unit_price_adj_code) ADJCODE");

    sbQuery
        .append(" , A.c901_unit_price_adj_code ADJCODEID, get_code_name(A.c901_unit_price_adj_code) ADJCODE");
    sbQuery
        .append(" , DECODE(A.c502_unit_price_adj_value ,NULL,'','0','','Y') UADJFL,GET_CODE_NAME_alt(c901_unit_price_adj_code) adjcodename ");
    sbQuery
        .append(" ,  NVL((A.C502_ITEM_QTY * A.C502_UNIT_PRICE),0) EXT_UNIT_PRICE ,get_formatted_number(NVL((A.C502_ITEM_QTY * A.C502_UNIT_PRICE),0),2) EXT_UNIT_PRICE_ORDERDTL ,A.c502_ITEM_PRICE netunitprice , A.c502_adj_code_value CODEADJUST,A.C502_DO_UNIT_PRICE ACCOUNTPRICE ");
    sbQuery
        .append(",A.c502_ITEM_PRICE netunitprice , A.c502_adj_code_value CODEADJUST,A.C502_DO_UNIT_PRICE ACCOUNTPRICE ");
    sbQuery
        .append(" , NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice(B.c704_account_id,A.c205_part_number_id, (SELECT t740.c101_party_id from t740_gpo_account_mapping t740 where t740.c704_account_id = B.c704_account_id)),0) CURRPORTPRICE ");
    sbQuery.append(" , NVL(A.C502_EXT_ITEM_PRICE,A.C502_ITEM_PRICE) EXT_ITEM_PRICE ");
    sbQuery.append("  FROM T502_ITEM_ORDER A, T501_ORDER B ");

    // To get the replenish req value from the attribute table, 101732 =
    // Replenish req attribute
    // type
    sbQuery
        .append(" , ( SELECT DISTINCT C502A_ATTRIBUTE_VALUE ATR_VAL, C205_PART_NUMBER_ID PART_NUM, C501_ORDER_ID ODR_ID  ");
    sbQuery.append(" FROM t502a_item_order_attribute WHERE c502a_void_fl IS NULL ");
    sbQuery.append(" AND C901_ATTRIBUTE_TYPE = 101732 AND C501_ORDER_ID IN (" + strAllOrders
        + " ) ) REPLENISH ");
    if (strOpt.equals("BOD") || strOpt.equals("DORECEIPT")) {
      sbQuery.append(" ,t502b_item_order_usage t502b, t500_order_info t500 ");
    }

    sbQuery.append(" WHERE B.C501_ORDER_ID IN ( ");
    sbQuery.append(strAllOrders);
    sbQuery.append(" ) AND A.C502_DELETE_FL IS NULL AND A.C502_VOID_FL IS NULL ");
    sbQuery.append(" AND B.C501_VOID_FL IS NULL AND A.C501_ORDER_ID = B.C501_ORDER_ID ");
    if (strOpt.equals("BOD") || strOpt.equals("DORECEIPT")) {
      sbQuery
          .append(" AND B.C501_ORDER_ID = t502b.c501_order_id AND B.C501_ORDER_ID = T500.c501_order_id AND T500.c500_order_info_id = A.c500_order_info_id");
      sbQuery.append(" AND A.c500_order_info_id = t502b.c500_order_info_id ");
      sbQuery.append("AND t502b.c502b_void_fl IS NULL ");

    }
    sbQuery.append(" AND A.C501_ORDER_ID  = REPLENISH.ODR_ID(+)  ");
    sbQuery.append(" AND A.C205_PART_NUMBER_ID  = REPLENISH.PART_NUM(+) ");
    sbQuery.append(" ORDER BY A.C501_ORDER_ID, A.C205_PART_NUMBER_ID ");
    log.debug(" Query for ORDERSUMMARY " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }

  public ArrayList fetchShipSummary(String strOrdId) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    // Use this for displaying Shipping summary in the Order Summary
    // Screen - Part of Capitated Pricing changes
    sbQuery.setLength(0);

    sbQuery.append(" SELECT T501.C501_ORDER_ID ORDID, T501.C501_SHIPPING_DATE SDT ");
    sbQuery.append(" ,T907.c907_tracking_number STRK ");
    sbQuery
        .append(" ,get_code_name (T907.c901_delivery_carrier) SCARNM ,  t907.c907_override_attn_to ATTENTO ");
    sbQuery.append(" ,T907.c901_delivery_carrier SCARRID,T907.C901_DELIVERY_MODE SMODEID ");
    sbQuery.append(" ,get_code_name (T907.c901_delivery_mode) SMODENM ");
    // added new key for jasper
    sbQuery
        .append(" ,NVL(C501_SHIP_COST,0) SCOST,get_formatted_number(NVL(C501_SHIP_COST,0),2) SCOSTORDERDTL, get_cs_ship_name (T907.c901_ship_to, T907.c907_ship_to_id) REP_NAME ");
    sbQuery.append(" ,T106.C106_ADD1 ADDRESS1, T106.C106_ADD2 ADDRESS2 ");
    sbQuery
        .append(" ,T106.C106_CITY CITY, get_code_name (T106.C901_STATE) STATE, T106.C106_ZIP_CODE ZIPCODE ,T907.c901_ship_to SHIPTO, T907.c907_ship_to_id SHIPTOID, T907.c106_address_id ADDRESSID");
    sbQuery.append(" ,T907.C907_STATUS_FL SSTATUSFL ");//PC-5584 - Exclude usage code or construct code from PSFG
    sbQuery.append("  FROM T501_ORDER T501, T907_SHIPPING_INFO T907, T106_ADDRESS T106 ");
    sbQuery.append(" WHERE T501.C501_ORDER_ID   = T907.C907_REF_ID (+) ");
    sbQuery.append(" AND T907.C106_ADDRESS_ID = T106.C106_ADDRESS_ID (+) ");
    sbQuery.append(" AND C501_ORDER_ID IN ( ");
    sbQuery.append(getOrderIDQuery(strOrdId));
    sbQuery.append(" ) AND (T501.C501_DELETE_FL IS NULL ");
    sbQuery.append(" or ( t501.c501_delete_fl  = 'Y' ");
    sbQuery.append(" and t501.c901_order_type  = '102080'))");
    sbQuery.append("AND T501.C501_VOID_FL IS NULL ");
    sbQuery.append(" AND T907.C907_VOID_FL IS NULL AND T106.C106_VOID_FL IS NULL");
    sbQuery.append(" ORDER BY  T501.C501_ORDER_ID ");
    log.debug(" Query for SHIPSUMMARY " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }

  public ArrayList fetchInvoiceDetails(String strOrdId) throws AppError {
    StringBuffer sbInvoiceIdQuery = new StringBuffer();
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());

    sbInvoiceIdQuery
        .append("SELECT t501.c503_invoice_id sbid, c501_order_id ordid, c501_customer_po custpo, to_char(T503.C503_CREATED_DATE,'"
            + strApplDateFmt + "') INVCREATEDDATE ,  ");
    sbInvoiceIdQuery.append(" GET_USER_NAME (T503.C503_CREATED_BY) CREATEDBY ");
    sbInvoiceIdQuery.append(" FROM T501_ORDER t501, T503_INVOICE t503 WHERE C501_ORDER_ID IN (");
    sbInvoiceIdQuery.append(getOrderIDQuery(strOrdId));
    sbInvoiceIdQuery.append(") AND C501_DELETE_FL IS NULL AND C501_VOID_FL IS NULL ");
    sbInvoiceIdQuery
        .append(" AND T501.C503_INVOICE_ID = T503.C503_INVOICE_ID(+) AND T503.C503_VOID_FL is null ");
    sbInvoiceIdQuery.append(" ORDER BY  C501_ORDER_ID");

    return dbManager.queryMultipleRecords(sbInvoiceIdQuery.toString());
  }

  public String validateDO(HashMap hmParam) throws Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strErrMessage = "";
    log.debug("hmParam: " + hmParam);
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strTagID = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_order_validation", 5);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strTagID);
    gmDBManager.setString(3, strPartNum);
    gmDBManager.setString(4, strOpt);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strErrMessage = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.close();
    return strErrMessage;
  }

  /**
   * updateControlNumberDetail - This method will save the DO Control number Details
   * 
   * @param HashMap , GmDBManager.
   * @return void
   * @exception AppError
   */

  public void updateControlNumberDetail(HashMap hmParam, GmDBManager gmDBManager) throws AppError {

    boolean blFlg = false;
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strScreen = GmCommonClass.parseNull((String) hmParam.get("SCREEN")); // To know from
    // which screen the
    // procedure is
    // being called
    if (gmDBManager == null) {
      gmDBManager = new GmDBManager(getGmDataStoreVO());
      blFlg = true;
    }
    gmDBManager.setPrepareString("gm_pkg_cs_usage_lot_number.gm_sav_usage_ddt_dtls ", 4);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strScreen);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    if (blFlg)
      gmDBManager.commit();
  }

  public HashMap loadCustPODetails(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));

    sbQuery
        .append(" SELECT t501.c501_order_id ID, t7100.c7100_case_id CASE_ID, t501.c704_account_id ACCID, ");
    sbQuery.append(" get_account_name(t501.c704_account_id) ANAME ,c501_customer_po PO ");
    sbQuery.append(" FROM t501_order t501, t7100_case_information t7100 ");
    if (strDeptId.equals("S")) {
      sbQuery.append(getAccessFilterClauseWithRepID());
    }
    sbQuery.append(" WHERE t501.c501_order_id = '" + strOrderID + "'");
    sbQuery
        .append(" AND t7100.c7100_case_info_id = t501.c7100_case_info_id AND t7100.c901_type = 1006503 ");
    if (strDeptId.equals("S")) {
      sbQuery.append(" AND t501.c703_sales_rep_id = V700.rep_id");
    }
    log.debug("PO Details :" + sbQuery.toString());
    hmReturn = dbManager.querySingleRecord(sbQuery.toString());
    return hmReturn;
  }

  public ArrayList filterControlNumberDetails(String strOrderId, ArrayList alControlNumberDetails)
      throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    sbQuery.setLength(0);

    sbQuery.append(" SELECT C205_PART_NUMBER_ID ID, SUM(C502_ITEM_QTY) QTY ");
    sbQuery.append(" FROM T502_ITEM_ORDER T502, t501_order t501 ");
    sbQuery.append(" WHERE t501.C501_ORDER_ID = '" + strOrderId + "'");
    sbQuery
        .append(" AND C502_DELETE_FL IS NULL AND t501.C501_ORDER_ID = t502.C501_ORDER_ID AND T502.C502_VOID_FL IS NULL ");
    sbQuery.append(" GROUP BY T502.C205_PART_NUMBER_ID ");
    sbQuery.append(" ORDER BY T502.C205_PART_NUMBER_ID ");

    ArrayList alCartDetails = dbManager.queryMultipleRecords(sbQuery.toString());
    Iterator itrCartDetails = alCartDetails.iterator();
    ArrayList alReturn = new ArrayList();

    while (itrCartDetails.hasNext()) {
      HashMap hmCartMap = (HashMap) itrCartDetails.next();
      String strCartDetailsPnum = GmCommonClass.parseNull((String) hmCartMap.get("ID"));
      Iterator itrCnumDetails = alControlNumberDetails.iterator();

      while (itrCnumDetails.hasNext()) {
        HashMap hmCnumMap = (HashMap) itrCnumDetails.next();
        String strCnumDetailsPnum = GmCommonClass.parseNull((String) hmCnumMap.get("PNUM"));

        if (strCartDetailsPnum.equals(strCnumDetailsPnum)) {
          alReturn.add(hmCnumMap);
        }
      }
    }
    return alReturn;
  }

  /**
   * fetchOpenReturnfl - This method will return open return flag
   * 
   * @param String strOrderId
   * @exception AppError
   */
  public String fetchOpenReturnfl(String strOrderId) throws AppError {

    String strOpenReturnfl = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_open_return_fl", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strOrderId);
    gmDBManager.execute();
    strOpenReturnfl = gmDBManager.getString(1);
    gmDBManager.close();

    return strOpenReturnfl;
  }

  /**
   * populateDOFromJSON - This method will Get The array From VO and preparing the input string for
   * each array.putting the input string into hash map. Finally adding that hash map to array list.
   * After Preparing input string value will be saved into DB.
   * 
   * @param Array alOrderVO,gmOrderVO
   * @return type ArrayList
   * @exception AppError
   */
  public ArrayList populateDOFromJSON(GmOrderVO[] alOrderVO, GmOrderVO gmOrderVO) throws AppError {
    ArrayList alResult = new ArrayList();
    int length = alOrderVO.length;
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmOrderAttrVO gmOrderAttrVO = new GmOrderAttrVO();
    GmItemOrderAttrVO gmItemOrderAttrVO = new GmItemOrderAttrVO();
    GmOrderItemVO gmOrderItemVO = new GmOrderItemVO();
    GmTagUsageVO gmTagUsageVO = new GmTagUsageVO();
    GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    for (int i = 0; i < length; i++) {

      String strErrMsg = "";
      String strOpt = "";
      GmOrderVO gmOrderparamVO = alOrderVO[i];
      // Validating For Duplicate Order.
      String strOrderId =
          gmCustomerBean.loadNextOrderId(gmOrderparamVO.getOrderid(), "",
              gmOrderparamVO.getAcctid(), "");
      if (strOrderId.equals("DUPLICATE") || strOrderId.equals("NOPARENT")) {
        strErrMsg =
            strOrderId.equals("DUPLICATE") ? (strOpt.equals("QUOTE") ? "Quote already entered in the System"
                : "DO already entered in the System")
                : "Incorrect Parent DO for this Account";
        throw new AppError(strErrMsg, "20938", 'E');
      }

      HashMap hmOrder =
          GmCommonClass.parseNullHashMap(populateOrderDetails(gmOrderparamVO, gmOrderVO));
      gmOrderVO.setOrderid(strOrderId);
      hmOrder.put("ORDERID", strOrderId);

      GmItemOrderAttrVO[] arrGmOrderItemAttrVO = gmOrderparamVO.getArrGmOrderItemAttrVO();
      String strOrdItemAttrInputstr =
          gmWSUtil.getDbInputStringFromVO(arrGmOrderItemAttrVO, "^", "|",
              gmItemOrderAttrVO.getOrderItemAttrProperties(), "getOrderItemAttrProperties");
      hmOrder.put("ORDERITEMATTRSTR", strOrdItemAttrInputstr);
      log.debug("the value inside ORDERITEMATTRSTR populateDOFromJSON *****"
          + strOrdItemAttrInputstr);

      GmOrderAttrVO[] arrGmOrderAttrVO = gmOrderparamVO.getArrGmOrderAttrVO();
      String strOrdAttrInputstr =
          gmWSUtil.getDbInputStringFromVO(arrGmOrderAttrVO, "^", "|",
              gmOrderAttrVO.getOrderAttrProperties(), "getOrderAttrProperties");
      hmOrder.put("ORDERATTRSTR", strOrdAttrInputstr);

      log.debug("the value inside ORDERATTRSTR populateDOFromJSON *****" + strOrdAttrInputstr);

      GmOrderItemVO[] arrGmOrderItemVO = gmOrderparamVO.getArrGmOrderItemVO();
      String strOrdItemInputstr =
          gmWSUtil.getDbInputStringFromVO(arrGmOrderItemVO, "^", "|",
              gmOrderItemVO.getOrderItemProperties(), "getOrderItemProperties");
      hmOrder.put("ORDERITEMSTR", strOrdItemInputstr);

      log.debug("the value inside ORDERITEMSTR populateDOFromJSON *****" + strOrdItemInputstr);
      GmTagUsageVO[] arrGmTagUsageVO = gmOrderparamVO.getArrGmTagUsageVO();
      String strTagUsageInputstr =
          gmWSUtil.getDbInputStringFromVO(arrGmTagUsageVO, "^", "|",
              gmTagUsageVO.getTagUsageProperties(), "getTagUsageProperties");
      hmOrder.put("INPUTSTR", strTagUsageInputstr);

      log.debug("the value inside ORDERITEMSTR populateDOFromJSON *****" + strTagUsageInputstr);

      // Appending the Billing tab and surgery info tab details in the same key
      // To update the orderattribute details in the method: updateOrderAttrMaster()
      GmDOSurgeryDetailVO[] arrSurgeryDetailVO = gmOrderparamVO.getArrDoSurgeryDetailVO();
      String strSurgeryAttrbInputstr =
          gmWSUtil.getDbInputStringFromVO(arrSurgeryDetailVO, "^", "|",
              GmDOSurgeryDetailVO.getOrderAttrProperties(), "getOrderAttrProperties");
      strOrdAttrInputstr = strOrdAttrInputstr + strSurgeryAttrbInputstr;
      hmOrder.put("ORDERATTRSTR", strOrdAttrInputstr);

      // NPI Details from GLobusApp
      GmDONPIDetailVO[] arrDoNPIDetailVO = gmOrderparamVO.getArrDoNPIDetailVO();
      // Check Null for the arrDoNPIDetailVO object,if it's coming as null from the input then it
      // won't stop to Book DO From IPAD
      if (arrDoNPIDetailVO != null) {
        String strDONPIInputstr =
            gmWSUtil.getDbInputStringFromVO(arrDoNPIDetailVO, "^", "|",
                GmDONPIDetailVO.getDONPIProperties(), "getDONPIProperties");
        hmOrder.put("ORDERNPISTR", strDONPIInputstr);

        log.debug("the value inside ORDERNPISTR populateDOFromJSON  *****" + strDONPIInputstr);
      }
      
      // DO Record tag tab details
      GmDORecordTagIDVO[] arrDORecordTagIDVO = gmOrderparamVO.getArrDORecordTagIDVO();
      // Check Null for the arrDORecordTagIDVO object,if it's coming as null from the input then it
      // won't stop to Book DO From IPAD
      if (arrDORecordTagIDVO != null) {
        String strDORecordTagInputstr =
            gmWSUtil.getDbInputStringFromVO(arrDORecordTagIDVO, "^", "|",
            		GmDORecordTagIDVO.getDORecordTagProperties(), "getDORecordTagProperties");
        hmOrder.put("ORDERRECORDTAGDOSTR", strDORecordTagInputstr);

        log.debug("the value inside ORDERRECORDTAGDOSTR populateDOFromJSON  *****" + strDORecordTagInputstr);
      }
      
      gmTxnShipInfoVO = gmOrderparamVO.getGmTxnShipInfoVO();

      hmOrder.put("SOURCE", GmCommonClass.parseNull(gmTxnShipInfoVO.getSource()));
      hmOrder.put("SHIPTO", GmCommonClass.parseNull(gmTxnShipInfoVO.getShipto()));
      hmOrder.put("SHIPCARRIER", GmCommonClass.parseNull(gmTxnShipInfoVO.getShipcarrier()));
      hmOrder.put("SHIPMODE", GmCommonClass.parseNull(gmTxnShipInfoVO.getShipmode()));
      hmOrder.put("SHIPID", GmCommonClass.parseNull(gmTxnShipInfoVO.getShip_to_id()));
      hmOrder.put("REPADDRESSID", GmCommonClass.parseNull(gmTxnShipInfoVO.getAddressid()));
      hmOrder.put("SHIPATTN", GmCommonClass.parseNull(gmTxnShipInfoVO.getAttnto()));
      hmOrder.put("SHIPINST", GmCommonClass.parseNull(gmTxnShipInfoVO.getShipinstruction()));
      alResult.add(hmOrder);
      log.debug("the value inside hmOrder populateDOFromJSON *****" + hmOrder);
    }
    return alResult;
  }

  public HashMap populateOrderDetails(GmOrderVO gmOrderparamVO, GmOrderVO gmOrderVO) {

    HashMap hmOrder = new HashMap();

    gmOrderVO.setAcctid(gmOrderparamVO.getAcctid());
    gmOrderVO.setOrderdate(gmOrderparamVO.getOrderdate());
    gmOrderVO.setOrdertype(gmOrderparamVO.getOrdertype());
    gmOrderVO.setRepid(gmOrderparamVO.getRepid());
    gmOrderVO.setCustpo(gmOrderparamVO.getCustpo());
    gmOrderVO.setCustpodate(gmOrderparamVO.getCustpodate());
    gmOrderVO.setShipdate(gmOrderparamVO.getShipdate());
    gmOrderVO.setTotal(gmOrderparamVO.getTotal());
    gmOrderVO.setStatusfl(gmOrderparamVO.getStatusfl());
    gmOrderVO.setShipcost(gmOrderparamVO.getShipcost());
    gmOrderVO.setRmaid(gmOrderparamVO.getRmaid());
    gmOrderVO.setInvoiceid(gmOrderparamVO.getInvoiceid());
    gmOrderVO.setCaseinfoid(gmOrderparamVO.getCaseinfoid());
    gmOrderVO.setInvupdfl(gmOrderparamVO.getInvupdfl());
    gmOrderVO.setHoldfl(gmOrderparamVO.getHoldfl());
    gmOrderVO.setVoidfl(gmOrderparamVO.getVoidfl());
    gmOrderVO.setSkipvalidatefl(gmOrderparamVO.getSkipvalidatefl());
    gmOrderVO.setParentorderid(gmOrderparamVO.getParentorderid());
    gmOrderVO.setOrdercomments(gmOrderparamVO.getOrdercomments());
    gmOrderVO.setShipto(gmOrderparamVO.getShipto());
    gmOrderVO.setShipmode(gmOrderparamVO.getShipmode());
    gmOrderVO.setShipcarrier(gmOrderparamVO.getShipcarrier());
    gmOrderVO.setStropt(gmOrderparamVO.getStropt());

    hmOrder.put("ORDERDATE", gmOrderparamVO.getOrderdate());
    hmOrder.put("ORDERTYPE", gmOrderparamVO.getOrdertype());
    hmOrder.put("SHIPMODE", gmOrderparamVO.getShipmode());
    hmOrder.put("SHIPCARRIER", gmOrderparamVO.getShipcarrier());
    hmOrder.put("SHIPTOORDER", gmOrderparamVO.getShipto());
    hmOrder.put("CASEINFOID", gmOrderparamVO.getCaseinfoid());
    hmOrder.put("USERID", gmOrderVO.getUserid());
    hmOrder.put("ACCOUNTID", gmOrderparamVO.getAcctid());
    hmOrder.put("REPID", gmOrderparamVO.getRepid());
    hmOrder.put("TOTAL", gmOrderparamVO.getTotal());
    hmOrder.put("CUSTPO", gmOrderparamVO.getCustpo());
    hmOrder.put("PARENTORDERID", gmOrderparamVO.getParentorderid());
    hmOrder.put("ORDCOMMENTS", gmOrderparamVO.getOrdercomments());
    hmOrder.put("CUSTPO", gmOrderparamVO.getCustpo());
    hmOrder.put("SKIPVALIDATEFL", gmOrderparamVO.getSkipvalidatefl());
    hmOrder.put("SHIPCOST", gmOrderparamVO.getShipcost());
    hmOrder.put("STATUS", gmOrderparamVO.getStatusfl());
    hmOrder.put("STROPT", gmOrderparamVO.getStropt());

    log.debug("the value inside hmOrder populateOrderDetails *****" + hmOrder);
    return hmOrder;
  }

  /*
   * Method saveOrderUsageDetails This method is overloaded for DO App. Once the order is getting
   * saved, the same db object will be used to save other information.
   */
  public void saveOrderUsageDetails(HashMap hmParam, GmDBManager gmDBManager) throws AppError,
      Exception {
    log.debug("the value inside coming hmOrder saveOrderUsageDetails *****" + hmParam);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("ORDERITEMSTR"));// ORDER ITEM
    String strOrderTotal = GmCommonClass.parseNull((String) hmParam.get("TOTAL"));
    double dTotal = Double.parseDouble(strOrderTotal);
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    // strAction = "NEWDO";
    String strCustPO = GmCommonClass.parseNull((String) hmParam.get("CUSTPO"));
    String strParentDOID = GmCommonClass.parseNull((String) hmParam.get("PARENTORDERID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("ORDCOMMENTS"));

    // Capturing surgery date from DO(Changing the procedure not on front end , when it required for
    // DO app, need to the surgery date value from JSON).
    String strSurgeryDate = GmCommonClass.parseNull((String) hmParam.get("SURGERYDATE"));
    Date dtSurgeryDate = null;
    dtSurgeryDate = GmCommonClass.getStringToDate(strSurgeryDate, strApplDateFmt);
    logOrder.debug("ORDERITEMSTR while raising order GmDOBean.saveOrderUsageDetails *****"
        + strInputStr);


    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_sav_usage_details_main", 12);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strParentDOID);
    gmDBManager.setString(3, strCaseInfoID);
    gmDBManager.setString(4, strInputStr);
    gmDBManager.setString(5, strAccountID);
    gmDBManager.setString(6, strCustPO);
    gmDBManager.setDouble(7, dTotal);
    gmDBManager.setString(8, strComments);
    gmDBManager.setString(9, strAction);
    gmDBManager.setString(10, strUserID);
    gmDBManager.setString(11, strRepID);
    gmDBManager.setDate(12, dtSurgeryDate);
    gmDBManager.execute();

    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strOrderID, strComments, strUserID, "1200"); // 1200 is
      // Order
    }
    log.debug("the value inside after hmOrder saveOrderUsageDetails *****" + hmParam);

  }

  /*
   * Method updateOrderMaster This method is used to to save other information for orders becuase
   * few values are hardcoded in saveOrderUsageDetails
   */
  public void updateOrderMaster(HashMap hmParam, GmDBManager gmDBManager) throws AppError,
      Exception {

    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    String strOrderDate = GmCommonClass.parseNull((String) hmParam.get("ORDERDATE"));
    Date dtOrderDate = null;
    dtOrderDate = GmCommonClass.getStringToDate(strOrderDate, strApplDateFmt);
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strOrderTotal = GmCommonClass.parseNull((String) hmParam.get("TOTAL"));
    double dTotal = Double.parseDouble(strOrderTotal);
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strAction = "EDITDO";// Since while updating the Order Table Action
    // Needs to be Edit DO.
    String strCustPO = GmCommonClass.parseNull((String) hmParam.get("CUSTPO"));
    String strParentDOID = GmCommonClass.parseNull((String) hmParam.get("PARENTORDERID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("ORDCOMMENTS"));
    String strmode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strCarrier = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strshipto = GmCommonClass.parseNull((String) hmParam.get("SHIPTOORDER"));
    String strshipcost = GmCommonClass.parseNull((String) hmParam.get("SHIPCOST"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strrmaid = GmCommonClass.parseNull((String) hmParam.get("RMAID"));
    String strinvupfl = GmCommonClass.parseNull((String) hmParam.get("INVUPDFL"));
    // Capturing surgery date from DO(Changing the procedure not on front end , when it required for
    // DO app, need to the surgery date value from JSON).
    String strSurgeryDate = GmCommonClass.parseNull((String) hmParam.get("SURGERYDATE"));
    Date dtSurgeryDate = null;
    dtSurgeryDate = GmCommonClass.getStringToDate(strSurgeryDate, strApplDateFmt);

    gmDBManager.setPrepareString("gm_pkg_op_do_master.gm_sav_order_master", 31);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strAccountID);
    gmDBManager.setString(3, strRepID);
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, strCustPO);
    gmDBManager.setDate(6, dtOrderDate); // order date should be
    // CURRENT_DATE.
    gmDBManager.setString(7, ""); // Shipp Date
    gmDBManager.setDouble(8, dTotal);
    gmDBManager.setString(9, strCarrier); // delivery carrier
    gmDBManager.setString(10, strmode);// delivery mode
    gmDBManager.setString(11, "");// Tracking Number
    gmDBManager.setString(12, strshipto);
    gmDBManager.setString(13, "8"); // Pending CS confirmation
    gmDBManager.setString(14, "26230683"); // receivemode ipad
    gmDBManager.setString(15, ""); // Ship Charge Flag
    gmDBManager.setString(16, "");// P_Comments
    gmDBManager.setString(17, strComments);
    gmDBManager.setString(18, strRepID);
    gmDBManager.setString(19, ""); // ship Cost
    gmDBManager.setString(20, "");// update Invoice Flag
    gmDBManager.setString(21, ""); // order date should be CURRENT_DATE.
    gmDBManager.setString(22, "2518"); // Draft
    gmDBManager.setString(23, "");// Reason Type
    gmDBManager.setString(24, strParentDOID);// parent Order id
    gmDBManager.setString(25, ""); // rma id
    gmDBManager.setString(26, ""); // invoice id
    gmDBManager.setString(27, ""); // hold flag
    gmDBManager.setString(28, strCaseInfoID);// case info id
    gmDBManager.setString(29, strUserID);
    gmDBManager.setString(30, strAction);
    gmDBManager.setDate(31, dtSurgeryDate);

    gmDBManager.execute();

  }

  /*
   * Method updateOrderAttrMaster This method is used to to save the order attribute information.
   */
  public void updateOrderAttrMaster(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("ORDERATTRSTR"));
    gmDBManager.setPrepareString("gm_pkg_op_do_master.gm_sav_order_attribute", 3);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strInputStr); // ordid^atttype^attrval|ordid^atttype^attrval|//ORDER
    // ATTRIBUTE
    gmDBManager.execute();
  }

  /*
   * Method updateNPIDetails This method is used to to save other information for orders becuase few
   * values are hardcoded in saveNPIDetails
   */
  public void updateNPIDetails(HashMap hmParam, GmDBManager gmDBManager) throws AppError, Exception {


    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("ORDERNPISTR"));

    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_all_npi_transaction", 4);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strAccountID);
    gmDBManager.setString(4, strUserID);

    gmDBManager.execute();

  }

  /*
   * Method saveDeliveredOrder This method is used to to save the order information details when the
   * order is raised from mobile App.
   */
  public void saveDeliveredOrder(ArrayList alReturn, HashMap hmValue) throws AppError, Exception {
    log.debug("the value inside array list in saveDeliveredOrder *****" + hmValue);
    logOrder.error("hmValue in GmDOBean.saveDeliveredOrder() " + hmValue);
    logOrder.error("alReturn in GmDOBean.saveDeliveredOrder() " + alReturn);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmDOTxnBean gmDOTxnBean = new GmDOTxnBean(getGmDataStoreVO());

    String skipvalidatefl = GmCommonClass.parseNull((String) hmValue.get("SKIPVALIDATEFL"));
    int length = alReturn.size();
    for (int i = 0; i < length; i++) {
      HashMap hmParam = (HashMap) alReturn.get(i);
      saveOrderUsageDetails(hmParam, gmDBManager);
      
      /* PMT-33368: 
       * 
       * iPad comments stored in T902 table. If we store the same comments in Order table (T501), Invoice print also showing the same comments.
       * We avoid to store the comments in T501 table.
       * 
       */
      String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
      String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
      String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
      hmParam.put("ORDCOMMENTS", "");

      updateOrderMaster(hmParam, gmDBManager);
      updateOrderAttrMaster(hmParam, gmDBManager);
      saveDOShippInfo(hmParam, gmDBManager);
      updateControlNumber(hmParam, gmDBManager);
      updateDOTagDetail(hmParam, gmDBManager);
      updateDOSurgeryDate(hmParam,gmDBManager);
      //PC-3271: To save Account based Order attribute values
      gmCustomerBean.syncAccAttribute(gmDBManager,strOrderId,strAccountID,"ORDER",strUserId);
      String strNPIString = GmCommonClass.parseNull((String) hmParam.get("ORDERNPISTR"));
      if (!strNPIString.equals("")) {
        updateNPIDetails(hmParam, gmDBManager);
      }
      String strRecordDoString = GmCommonClass.parseNull((String) hmParam.get("ORDERRECORDTAGDOSTR")); 
      if (!strRecordDoString.equals("")) {
    	  gmDOTxnBean.saveDORecordTagDetails(hmParam, gmDBManager);
        }
    }
    if (skipvalidatefl.equals("")) {
      validateduplicateOrderUsage(hmValue, gmDBManager);
    }
    //PMT#- Track DO's Placed from IPhone/Android devices
    gmDOTxnBean.saveOrderDeviceInfo(hmValue, gmDBManager);

    gmDBManager.commit();
    log.debug("the value inside array list after saving data saveDeliveredOrder *****" + hmValue);
  }

  /*
   * Method validateduplicateOrderUsage This method is used to to validate the order with existing
   * order it needs to throw exception if order already exist for same account,part and qty .
   */
  public void validateduplicateOrderUsage(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_fch_validate_dousage", 4);

    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strAccountID);

    gmDBManager.execute();

    String validatedofl = GmCommonClass.parseNull((gmDBManager.getString(3)));
    String existing_order_id = GmCommonClass.parseNull((gmDBManager.getString(4)));
    if (validatedofl.equals("Y")) {
      gmDBManager.close();
      String strErrMsg =
          "A DO with the same Account-Part-Quantity combination already exists with ID "
              + existing_order_id;
      throw new AppError(strErrMsg, "", 'S');
    }
  }

  /*
   * Method updateDOTagDetail This method is used to to save the Tag information for an order raised
   * from do app.
   */
  public void updateDOTagDetail(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    log.debug("the value inside array list in updateDOTagDetail *****" + hmParam);
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_update_tagid", 4);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strCaseInfoID);
    gmDBManager.setString(3, strInputStr);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    log.debug("the value inside array list afetr in updateDOTagDetail *****" + hmParam);
  }

  /*
   * Method saveDOShippInfo This method is used to to save the shipping information for an order
   * raised from do app.
   */
  public void saveDOShippInfo(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    log.debug("the value inside array list afetr in saveDOShippInfo *****" + hmParam);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strSource = GmCommonClass.parseZero((String) hmParam.get("SOURCE"));
    String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPID"));
    String strShipCarr = GmCommonClass.parseZero((String) hmParam.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseZero((String) hmParam.get("SHIPMODE"));
    String strTrackNo = GmCommonClass.parseNull((String) hmParam.get("TRACKNO"));
    String strAddressId = GmCommonClass.parseZero((String) hmParam.get("REPADDRESSID"));
    String strFreight = GmCommonClass.parseNull((String) hmParam.get("FREIGHT"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAttnto = GmCommonClass.parseNull((String) hmParam.get("SHIPATTN"));
    String strInstruction = GmCommonClass.parseNull((String) hmParam.get("SHIPINST"));

    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_update_ship_info", 13);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setInt(2, Integer.parseInt(strSource));
    gmDBManager.setInt(3, Integer.parseInt(strShipTo));
    gmDBManager.setString(4, strShipToId);
    gmDBManager.setInt(5, Integer.parseInt(strShipCarr));
    gmDBManager.setInt(6, Integer.parseInt(strShipMode));
    gmDBManager.setString(7, "");// track no.
    gmDBManager.setInt(8, Integer.parseInt(strAddressId));
    gmDBManager.setString(9, "");// Frieght
    gmDBManager.setString(10, strCaseInfoID);// case info id
    gmDBManager.setString(11, strUserId);
    gmDBManager.setString(12, strAttnto);
    gmDBManager.setString(13, strInstruction);
    gmDBManager.execute();
    log.debug("the value inside array list afetr after in saveDOShippInfo *****" + hmParam);

  }

  /*
   * Method updateControlNumber This method is used to to save the item order attribute information
   * for an order raised from do app.
   */
  public void updateControlNumber(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    log.debug("the value inside array list afetr before in updateControlNumber *****" + hmParam);
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("ORDERITEMATTRSTR"));
    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_sav_control_number", 3);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    log.debug("the value inside array list afetr after in updateControlNumber *****" + hmParam);
  }

  /**
   * sendOrderExceptionEmail
   * 
   * @throws AppError ,Exception
   * @method If any Exception Thrown,while saving the order a mail need to be sent.
   */
  public void sendOrderExceptionEmail(HashMap hmParams) throws AppError, Exception {
    String strRepEmail = "";
    String strMessage = "";
    String strMessageHeader = "";
    String strMessageBody = "";
    String strMessageFooter = "";
    String strsubject = "";
    strRepEmail = GmCommonClass.parseNull((String) hmParams.get("REPEMAIL"));
    log.debug("strRepEmail in bean" + strRepEmail);
    String TEMPLATE_NAME = "GmOrderExcp";
    GmEmailProperties emailProps = new GmEmailProperties();

    emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.FROM));
    emailProps.setRecipients(strRepEmail);
    emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.MIME_TYPE));

    strsubject = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);

    strsubject =
        GmCommonClass.replaceAll(strsubject, "#<ORDERID>", (String) hmParams.get("ORDERID"));
    log.debug("strsubject in bean" + strsubject);
    emailProps.setSubject(strsubject);
    strMessage = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE);
    strMessageHeader =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_HEADER);
    strMessageHeader =
        GmCommonClass.replaceAll(strMessageHeader, "#<REPNAME>", (String) hmParams.get("REPNAME"));
    strMessageBody =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_BODY);
    strMessageBody =
        GmCommonClass
            .replaceAll(strMessageBody, "#<ORDERDATE>", (String) hmParams.get("ORDERDATE"));
    strMessageBody =
        GmCommonClass
            .replaceAll(strMessageBody, "#<ERRORMESS>", (String) hmParams.get("EXCEPTION"));
    strMessageFooter =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_FOOTER);
    strMessageFooter =
        GmCommonClass.replaceAll(strMessageFooter, "#<ACCOUNTID>",
            (String) hmParams.get("ACCOUNTID"));
    strMessageFooter =
        GmCommonClass.replaceAll(strMessageFooter, "#<ORDERID>", (String) hmParams.get("ORDERID"));
    strMessageFooter =
        GmCommonClass.replaceAll(strMessageFooter, "#<TOTAL>", (String) hmParams.get("TOTAL"));
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageHeader>", strMessageHeader);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageBody>", strMessageBody);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageFooter>", strMessageFooter);
    log.debug("strMessage in bean" + strMessage);

    emailProps.setMessage(strMessage);
    GmCommonClass.sendMail(emailProps);
  }

  /**
   * Fetch latest do.
   * 
   * @param hmParam the hm param
   * @throws AppError the app error
   */
  public String fetchLatestDO(HashMap hmParam) throws AppError {

    String strLatestDO = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_do_process_report.gm_fch_latest_do", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("ORDERDATE")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("PREFIXORDID")));
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();

    strLatestDO = GmCommonClass.parseNull((gmDBManager.getString(3)));
    gmDBManager.close();

    return strLatestDO;
  }

  /**
   * Fetch Total Before Adj, Adjustments and Total After Adjustment details for the Order id.
   * 
   * @param String strOrderID
   * @throws AppError the app error
   */
  public HashMap fetchOrderTotalDetails(String strOrderID) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDOBean gmDoBean = new GmDOBean();
    ArrayList alOrderSUmmary = new ArrayList();
    alOrderSUmmary = GmCommonClass.parseNullArrayList(gmDoBean.fetchOrderSummary(strOrderID));
    double intQty = 0;
    int intPreQty = 0;
    double dbBeforeItemTotal = 0.0;
    double dbBeforePriceTotal = 0.0;
    double dbAfterItemTotal = 0.0;
    double dbAfterPriceTotal = 0.0;
    double dbBeforePrice = 0.0;
    double dbAfterPrice = 0.0;
    double dbAdjustPrice = 0.0;
    String strAfterPrice = "";
    String strBeforeTotal = "";
    String strAdjustAmount = "";
    double dbTaxTotal = 0.0;
    double dbTaxGrandTotal = 0.0;
    String strTaxTotal = "";
    String strTaxGrandTotal = "";
    String strExOrderType = "";

    if (alOrderSUmmary.size() > 0) {
      for (int i = 0; i < alOrderSUmmary.size(); i++) {
        HashMap hmResult = (HashMap) alOrderSUmmary.get(i);
        String strUnitPrice = "";
        String strItemPrice = "";
        String strItemQty = "";
        strUnitPrice = (String) hmResult.get("UPRICE");
        strItemPrice = (String) hmResult.get("PRICE");
        strItemQty = (String) hmResult.get("QTY");
        intQty = Double.parseDouble(strItemQty) + intPreQty;
        strItemQty = "" + intQty;
        // Caliculating the Total Before Adjustment
        dbBeforeItemTotal = Double.parseDouble(strUnitPrice);
        dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply by
        // Qty
        dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal;
        strBeforeTotal = "" + dbBeforePriceTotal;

        // Caliculating the Total After Adjustment
        dbAfterItemTotal = Double.parseDouble(strItemPrice);
        dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply by Qty
        dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal;
        strAfterPrice = "" + dbAfterPriceTotal;

        // Caliculating the Adjustment Amount
        dbBeforePrice = Double.parseDouble(strBeforeTotal);
        dbAfterPrice = Double.parseDouble(strAfterPrice);
        dbAdjustPrice = dbAfterPrice - dbBeforePrice;
        strAdjustAmount = "" + dbAdjustPrice;

        // Calculating the Tax Total
        strExOrderType = GmCommonClass.parseNull((String) hmResult.get("DOEXORDERTYPE"));
        strTaxTotal = GmCommonClass.parseZero((String) hmResult.get("TAX_COST"));
        dbTaxTotal = Double.parseDouble(strTaxTotal);
        if (strExOrderType.equals("")) {
          dbTaxGrandTotal = dbTaxGrandTotal + dbTaxTotal;
        }
      }
    }
    strTaxGrandTotal = "" + dbTaxGrandTotal;
    // converting into string ,As we are not parse bigdecimal in jasper
    strBeforeTotal = GmCommonClass.getStringWithCommas(strBeforeTotal, 2);
    strAfterPrice = GmCommonClass.getStringWithCommas(strAfterPrice, 2);
    strAdjustAmount = GmCommonClass.getStringWithCommas(strAdjustAmount, 2);
    hmReturn.put("BEFORETOTAL", strBeforeTotal);
    hmReturn.put("ADJAMOUNT", dbAdjustPrice);
    hmReturn.put("ADJUSTMENTAMT", strAdjustAmount);
    hmReturn.put("AFTERTOTAL", strAfterPrice);
    hmReturn.put("TAXTOTAL", strTaxGrandTotal);

    return hmReturn;
  }

  /**
   * syncDOFile - This is the method is used to do below things. 1. copy[DO PUSH LOC to AR ACS LOC]
   * & Delete[DO PUSH LOC] the DO PDF file on DO confirm action. 2. Delete[DO PUSH LOC] the source
   * file on DO void action.
   * 
   * @param strOrderID, strOpt
   * @throws AppError, Exception
   */
  public void syncDOFile(String strOrderID, String strOpt) throws AppError, Exception {

    HashMap hmParam = new HashMap();
    GmDORptBean gmDORptBean = new GmDORptBean(getGmDataStoreVO());
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());

    hmParam.put("ORDERID", strOrderID);
    hmParam.put("ATTRIBTYPE", "103560"); // 103560 - Order source Attribute type
    String strOrderSrc = GmCommonClass.parseNull(gmDORptBean.fetchOrderAttribute(hmParam));
    String strCompId = getGmDataStoreVO().getCmpid();
    String strTransCompID =
        GmCommonClass.parseNull(gmShippingTransBean.getTransactionCompanyId(strCompId, strOrderID,
            "DOPENDINGCONFIRM", "ORDER"));

    // Getting the CompanyLocale and Make it as subfolder in AR
    String strCompanyLocale = GmCommonClass.getCompanyLocale(strTransCompID);

    strCompanyLocale = strCompanyLocale.equals("") ? "US" : strCompanyLocale;
    // Forming the file name:
    String strSourcePath = GmCommonClass.parseNull(GmCommonClass.getString("DOFILEPUSHLOC"));
    strSourcePath = strSourcePath + strCompanyLocale + "\\";
    String strTargetPath = GmCommonClass.parseNull(GmCommonClass.getString("DOFILEARACSLOC"));
    strTargetPath = strTargetPath + strCompanyLocale + "\\";

    // String strNewTargetPath =
    // GmCommonClass.parseNull(GmCommonClass.getString("DONEWFILEARACSLOC"));
    String strDOFileExtn = GmCommonClass.parseNull(GmCommonClass.getString("DOFILEXTN"));

    String strPDFFileNm = strOrderID + strDOFileExtn;
    // On confirm DO action (strOpt is empty),should move the PDF file from DO PUSH Location to AR
    // ACS Location.
    // When the DO is voided(strOpt is deletefile),we should delete the PDF file from the DO PUSH
    // LOC Location.
    if (strOrderSrc.equals("103521")) {
      File srcFile = new File(strSourcePath + strPDFFileNm);
      // Checking the file existence, if it is not available, then no need to call below part.
      if (srcFile.exists()) {
        if (!strOpt.equals("deletefile")) {
          // copyFiles from DO PUSH LOC to AR ACS LOC
          gmCommonUploadBean.copyFile(strSourcePath + strPDFFileNm, strTargetPath + strPDFFileNm);
        }
        // if new archive Path exists it will be taking it
        // if(strNewTargetPath !="" ){
        // gmCommonUploadBean.copyFile(strSourcePath + strPDFFileNm, strNewTargetPath +
        // strPDFFileNm);
        // }
        // deleteFiles from DO PUSH LOC .commented for PMT-12763
        // gmCommonUploadBean.deleteFile(strSourcePath + strPDFFileNm);

      }
    }
  }

  /**
   * gmUpdateLotStatus - Procedure to update the lot status to sold if the lot is used in surgery -
   * Will not have a commit as this method is using connection object from different method.
   * 
   * @param strOrderID, strUserId
   * @throws AppError, Exception
   */
  public void gmUpdateLotStatus(GmDBManager gmDBManager, String strOrderID, String strUserId)
      throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_lot_track.gm_update_lot_status", 2);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
  }

  /**
   * gmUpdateNetQty - To update Part Qty does in Sales Net Qty (PMT-17705) -
   * 
   * @param strOrderID, strUserId
   * @throws AppError, Exception
   */
  public void gmUpdateNetQty(GmDBManager gmDBManager, String strOrderID, String strUserId)
      throws AppError {

    log.debug("123>>>>>>" + strOrderID);
    log.debug("strUserId123>>>>>>" + strUserId);
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_trans.gm_sav_order_ship", 4);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setInt(2, Integer.parseInt("0"));
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();

  }

  /**
   * getGSTStartFlag - This method is to get the GST start date
   * 
   * @param String,String
   * @return String
   * @exception AppError
   */
  public String getGSTStartFlag(String strRefId, String strType) throws AppError {
    String strGSTStartFl = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setFunctionString("gm_pkg_ac_invoice_info.get_gst_start_flag", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strRefId);
    gmDBManager.setString(3, strType);
    gmDBManager.execute();
    strGSTStartFl = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    log.debug("strGSTStartFl>>>" + strGSTStartFl);
    return strGSTStartFl;
  }

  /**
   * fetchInputStringForDOConfirm : fetch the input string for parts while confirming the IPAD
   * order's
   * 
   * @param hmParam the hm param
   * @throws AppError the app error
   */
  public void fetchInputStringForDOConfirm(GmDBManager gmDBManager, String strOrderID)
      throws AppError {

    String strDOInputString = "";

    gmDBManager.setPrepareString("gm_pkg_op_do_process_report.gm_fch_part_string", 2);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strDOInputString = GmCommonClass.parseNull((gmDBManager.getString(2)));
    logOrder.error("strDOInputString GmDOBean.fetchInputStringForDOConfirm() " + strDOInputString);
  }
  /*
   * Method updateSurgeryDate: This method is used to to save the surgerydate information for IPAD.
   */
  public void updateDOSurgeryDate(HashMap hmParam, GmDBManager gmDBManager) throws AppError {

    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("ORDERATTRSTR"));
    gmDBManager.setPrepareString("gm_pkg_op_do_master.gm_sav_surgery_date", 3);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strInputStr); //ordid^atttype^attrval|ordid^atttype^attrval|//ORDER
    // ATTRIBUTE
    gmDBManager.execute();
  }
  /**
   * validateRepId : This method is used to validate the rep is mapped to account or not
   * 
   * @param hmParam
   * @throws AppError the app error
   */
  public String validateRepId(HashMap hmParam) throws AppError{
	  GmDBManager gmDBManager = new GmDBManager();
	  String strAccId = GmCommonClass.parseNull((String)hmParam.get("ACCID"));
	  String strRepId = GmCommonClass.parseNull((String)hmParam.get("REPID"));
	  String strCompId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
	  
	  gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_validate_rep_id", 4);
	  gmDBManager.setString(1, strAccId);
	  gmDBManager.setString(2, strRepId);
	  gmDBManager.setString(3, strCompId);
	  gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	  gmDBManager.execute();
	  
	  String strResponse = GmCommonClass.parseNull((gmDBManager.getString(4)));
	  gmDBManager.close();
	  return strResponse;
  }
}
