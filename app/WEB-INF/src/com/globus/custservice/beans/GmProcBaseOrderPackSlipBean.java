package com.globus.custservice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmProcBaseOrderPackSlipBean implements GmProcessBasePackSLipInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
		
    public ArrayList fetchCartDetails(HashMap hmParam) throws AppError{
        String strDelNote = GmCommonClass.parseNull(GmCommonClass.getString("CREATE_DELNOT"));
        String strOrdId = GmCommonClass.parseNull((String)hmParam.get("ORDERID"));
        StringBuffer sbQuery = new StringBuffer();
        GmDBManager dbManager = new GmDBManager();
        ArrayList alReturn = new ArrayList();
        sbQuery.setLength(0);               
        sbQuery.append("select t502.c205_part_number_id productno");
        sbQuery.append(" ,get_partnum_desc(t502.c205_part_number_id) proddescrption");
        sbQuery.append(" ,to_char(t2550.c2550_expiry_date,get_rule_value('DATEFMT','DATEFORMAT')) prodexpiry");
        sbQuery.append(" ,c502_control_number allograftno");
        sbQuery.append(" ,get_part_size(t502.c205_part_number_id,t2550.c2550_control_number)  prodsize");
        sbQuery.append(" ,t2550.c2540_donor_number proddonor , t502.c502_item_qty IQTY , t502.c502_item_price PRICE ");
        sbQuery.append(" from");
        sbQuery.append(" (select t2550.c205_part_number_id");
        sbQuery.append(" ,t2550.c2550_control_number");
        sbQuery.append(" ,t2550.c2550_expiry_date");
        sbQuery.append(" ,t2550.c2550_custom_size");
        sbQuery.append(" ,t2540.c2540_donor_number");
        sbQuery.append(" ,t2550.c2540_donor_id");
        sbQuery.append(" from t2550_part_control_number t2550 ,t2540_donor_master t2540");
        sbQuery.append(" where NVL(t2550.c2540_donor_id,-999) = t2540.c2540_donor_id (+)");
        sbQuery.append(" and t2550.c205_part_number_id in ( select t502.c205_part_number_id");
        sbQuery.append(" from t502_item_order t502  where  t502.c501_order_id = '"+strOrdId+"') ) t2550,t502_item_order t502");
        sbQuery.append(" where t502.c501_order_id = '"+strOrdId+"'");
        sbQuery.append(" and t502.c205_part_number_id  = t2550.c205_part_number_id (+)");
        sbQuery.append(" and t502.c502_control_number = t2550.c2550_control_number (+)");
        sbQuery.append(" and t502.c502_void_fl is null ORDER BY productno , allograftno ");        
        alReturn = GmCommonClass.parseNullArrayList(dbManager.queryMultipleRecords(sbQuery.toString()));
        log.debug("The Cart Details Query is ******* "+sbQuery.toString());
        return alReturn;
  }
	
}
