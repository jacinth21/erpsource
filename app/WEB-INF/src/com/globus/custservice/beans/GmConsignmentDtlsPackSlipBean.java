/*
 * Module: GmConsignmentDtlsPackSlipBean.java Author: N RAJA Project: Globus Medical App Date-Written:
 * 22 January 2020 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.custservice.beans;

import java.util.HashMap; 




import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;



public class GmConsignmentDtlsPackSlipBean extends GmBean implements GmConsignmentPackSlipInterface {	

  GmCustomerBean gmCustomerBean = new GmCustomerBean();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                // Class.
 /**
  * default constructor 
  */
  public GmConsignmentDtlsPackSlipBean() {
	  super(GmCommonClass.getDefaultGmDataStoreVO());
  }
  
  /**
   * 
   * @param gmDataStore
   */
  public GmConsignmentDtlsPackSlipBean(GmDataStoreVO gmDataStore) {
	  super(gmDataStore);
  }


  /**
   * loadConsignDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadConsignDetails(String strConsignId, String strLUserId,GmDataStoreVO gmDataStore) throws AppError {
	//PMT-57366  Loan detail sheet displaying Hospital name in English
	  GmCustomerBean gmCustomerBean = new GmCustomerBean(gmDataStore);
	  return gmCustomerBean.loadConsignDetails(strConsignId, strLUserId);
  }

}// end of class GmConsignmentDtlsPackSlipBean
