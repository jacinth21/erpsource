package com.globus.custservice.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.jms.consumers.beans.GmOrderBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

public class GmDOTxnBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmDOTxnBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDOTxnBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveAckOrderDetails - This method is to save the edited details of Acknowledgment order
   * 
   * @param HashMap hmParam -- contains entire order information
   * @param String strOrderId
   * @param String strUsername
   * @return String
   * @exception AppError
   */
  public String saveAckOrderDetails(HashMap hmParam, String strOrderId, String strUsername)
      throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strAccId = "";
    String strMode = "";
    String strComments = "";
    String strPO = "";
    String strParentOrderId = "";
    String strQuoteStr = "";
    String strSalesRepId = "";
    String strRmQtyStr = "";
    String strHardCpPO = "";
    String strPerson = "";

    String strRmQtyMsgStr = "";

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strMode = GmCommonClass.parseNull((String) hmParam.get("OMODE"));
    strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENTORDERID")); // parentid
    strSalesRepId = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
    strQuoteStr = GmCommonClass.parseNull((String) hmParam.get("QUOTESTR"));
    strRmQtyStr = GmCommonClass.parseNull((String) hmParam.get("RMQTYSTR"));
    strPerson = GmCommonClass.parseNull((String) hmParam.get("OPERSON"));
    strHardCpPO = GmCommonClass.parseNull((String) hmParam.get("HARDCPPO"));
    strHardCpPO = (strHardCpPO.equals("on")) ? "Y" : "";

    gmDBManager.setPrepareString("gm_pkg_op_order_txn.gm_sav_ack_order", 13);
    gmDBManager.registerOutParameter(13, java.sql.Types.CHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strAccId);
    gmDBManager.setString(3, strPO);
    gmDBManager.setString(4, strUsername);
    gmDBManager.setInt(5, Integer.parseInt(strMode));
    gmDBManager.setString(6, strPerson);
    gmDBManager.setString(7, strComments); // Comments and other information
    gmDBManager.setString(8, strParentOrderId);
    gmDBManager.setString(9, strSalesRepId);
    gmDBManager.setString(10, strRmQtyStr);
    gmDBManager.setString(11, strHardCpPO);
    gmDBManager.setString(12, strQuoteStr);
    gmDBManager.execute();
    strRmQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(13));

    gmDBManager.commit();
    hmReturn.put("MSG", strRmQtyMsgStr);

    return strRmQtyMsgStr;

  } // end of saveAckOrderDetails
  
  /**
   * saveOusDistOrder - TO save and generate order for OUS distributors in Ack-Edit Screen
   * 
   * 
   * @param strRefId, strOrderType , strUserId , strFreightamt,strPoid,strInput
   * @author Suganthi
   * @exception AppError
   */
  public String saveOusDistOrder(HashMap hmParam) throws AppError
  {
 	GmDBManager gmDBManager = new GmDBManager();
     String strRefId = GmCommonClass.parseNull((String)hmParam.get("REFORDID"));
     String strOrderType = "102080";
     String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
     String strFreightamt = GmCommonClass.parseNull((String)hmParam.get("SHIPCHARGE"));
     String strPoid = GmCommonClass.parseNull((String)hmParam.get("PO"));
     String strInput = GmCommonClass.parseNull((String)hmParam.get("INPUTSTR"));
     String strOrderId = "";
 	gmDBManager.setPrepareString("gm_pkg_op_ict.gm_op_sav_ous_dist_order", 7);
 	 gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
 	gmDBManager.setString(1, strRefId);
 	gmDBManager.setString(2, strOrderType);
 	gmDBManager.setString(3, strUserId);
 	gmDBManager.setString(4, strFreightamt);
 	gmDBManager.setString(5, strPoid);
 	gmDBManager.setString(6, strInput);
 	gmDBManager.execute();
 	strOrderId = GmCommonClass.parseNull(gmDBManager.getString(7));
 	gmDBManager.commit();
 	gmDBManager.close();
 	return strOrderId;
  }
  
  /**
   * saveOusPartDtls - TO save part details for the generated OUS dist order in Ack-Edit Screen
   * 
   * 
   * @param strOrdId, strAccntId , strInput
   * @author Suganthi
   * @exception AppError
   */
  public String saveOusPartDtls(HashMap hmParam) throws AppError
  {
	  log.debug("saveOusPartDtls");
 	GmDBManager gmDBManager = new GmDBManager();
     String strOrdId = GmCommonClass.parseNull((String)hmParam.get("RELEASEORDERID"));
     String strAccntId = GmCommonClass.parseNull((String)hmParam.get("ACCID"));
     String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
     String strInput = GmCommonClass.parseNull((String)hmParam.get("INPUTSTR"));
     String strRowid = "";
 	gmDBManager.setPrepareString("gm_pkg_op_ict.gm_op_sav_ous_dist_part_detail",5);
 	//gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
 	gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
 	gmDBManager.setString(1, strOrdId);
 	gmDBManager.setString(2, strAccntId);
 	gmDBManager.setString(3, strUserId);
 	gmDBManager.setString(4, strInput);
 	gmDBManager.execute();
 	strRowid = GmCommonClass.parseNull(gmDBManager.getString(5));
 	gmDBManager.commit();
 	gmDBManager.close();
 	return strRowid;
  }
  
  /**
   * releaseOusDistOrder - TO release the generated order for OUS distributors in Ack-Edit Screen
   * 
   * 
   * @param strRefId, strOrderType , strUserId , strFreightamt,strPoid,strInput
   * @author Suganthi
   * @exception AppError
   */
  public void releaseOusDistOrder(HashMap hmParam) throws AppError
  {
 	GmDBManager gmDBManager = new GmDBManager();
 	GmOrderBean gmOrderBean = new GmOrderBean(getGmDataStoreVO());
 	GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33510
    HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33510
 	
     String strOrderId = GmCommonClass.parseNull((String)hmParam.get("RELEASEORDERID"));
     String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
     gmDBManager.setPrepareString("gm_pkg_op_ict.gm_release_ous_dist_order", 2);
 	gmDBManager.setString(1, strOrderId);
 	gmDBManager.setString(2, strUserId);
 	gmDBManager.execute();
 	gmDBManager.commit();

 	// Moving Order to hold Status if is there any Price Discrepancy
 	gmOrderBean.updateHoldOrderStatus(hmParam);
 	
 	//JMS call for Storing Building Info PMT-33510  
    hmStorageBuildInfo.put("TXNID", strOrderId);
    hmStorageBuildInfo.put("TXNTYPE", "ORDER");
    gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);


  }
  
  /**
   * validateOusDistAck - TO validate OUS Distributors in Ack-Edit Screen
   * 
   * 
   * @param strOrderId
   * @author Suganthi
   * @exception AppError
   */
  public String validateOusDistAck(HashMap hmParam) throws AppError
  {
	  GmDBManager gmDBManager = new GmDBManager();
	  String strOrderId = GmCommonClass.parseNull((String)hmParam.get("RELEASEORDERID"));
	  String strFlag = "";
	  gmDBManager.setFunctionString("gm_pkg_op_ack_order.gm_validate_ack_ous_dist", 1);
	  gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	  gmDBManager.setString(2, strOrderId);
	  gmDBManager.execute();
	  strFlag = GmCommonClass.parseNull(gmDBManager.getString(1));
	  gmDBManager.close();
	  return strFlag;
  }
  
  /**
   * removeAckOrderQty - TO validate OUS Distributors in Ack-Edit Screen
   * 
   * 
   * @param strOrderId
   * @author Suganthi
   * @exception AppError
   */
  public String removeAckOrderQty(HashMap hmParam) throws AppError
  {
	  log.debug("removeAckOrderQty-Bean "+hmParam);
	  GmDBManager gmDBManager = new GmDBManager();
	  String strOrderId = GmCommonClass.parseNull((String)hmParam.get("RELEASEORDERID"));
	  String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
	  String strInput = GmCommonClass.parseNull((String)hmParam.get("INPUTSTR"));
	  String strRmQtyMsgStr = "";
	  gmDBManager.setPrepareString("gm_pkg_op_order_txn.gm_sav_order_qty", 4);
	  gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
	  gmDBManager.setString(1, strOrderId);
	  gmDBManager.setString(2, strUserId);
	  gmDBManager.setString(3, strInput);
	  gmDBManager.execute();
	  strRmQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(4));
	  gmDBManager.commit();
	  gmDBManager.close();
	  return strRmQtyMsgStr;
  }
  
  /*
   * saveDORecordTagDetails(): This method is used to save the record DO tag 
   * details for the order id
   * Author: Karthik
   */
  public void saveDORecordTagDetails(HashMap hmParam, GmDBManager gmDBManager) throws AppError, Exception {

	  
    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("ORDERRECORDTAGDOSTR"));
    log.debug(" strInputStr??? " + strInputStr);
    gmDBManager.setPrepareString("gm_pkg_op_order_txn.gm_save_DO_record_tag_list",3 );
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
  }
  
  /*
   * saveOrderDeviceInfo(): This method is used to save the device UUID 
   * details for the order id
   */
  
  public void saveOrderDeviceInfo(HashMap hmParamDeviceInfo, GmDBManager gmDBManager) throws AppError, Exception{
	  
	  String strUserid = GmCommonClass.parseNull((String)hmParamDeviceInfo.get("USERID"));
	  String strOrderid = GmCommonClass.parseNull((String)hmParamDeviceInfo.get("ORDERID"));
	  String strUuid = GmCommonClass.parseNull((String)hmParamDeviceInfo.get("UUID"));
	  
	  log.debug("hmParamDeviceInfo==>"+hmParamDeviceInfo);
	  gmDBManager.setPrepareString("gm_pkg_op_order_dtl.gm_track_DO_device_typ",3);
	  gmDBManager.setString(1, strUuid);
	  gmDBManager.setString(2, strOrderid);
	  gmDBManager.setString(3, strUserid);
	  gmDBManager.execute();
	  
  }
  /*
   * saveDOTags(): This method is used to save the record DO tag 
   * details for the order id
   * Author: ppandiyan
   */
  public void saveDOTags(HashMap hmParam) throws AppError {

	    GmDBManager gmDBManager = new GmDBManager();

	    String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
	    String strTagID = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
	    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
	    String strTagUsageFl = GmCommonClass.parseNull((String) hmParam.get("TAGUSAGEFL"));
	    String strLatitude = GmCommonClass.parseNull((String) hmParam.get("LATITUDE"));
	    String strLongitude = GmCommonClass.parseNull((String) hmParam.get("LONGITUDE"));
	    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    
	    gmDBManager.setPrepareString("gm_pkg_op_order_txn.gm_save_do_record_tag_dtls",7 );
	    gmDBManager.setString(1, strTagID);
	    gmDBManager.setString(2, strSetID);
	    gmDBManager.setString(3, strOrderID);
	    gmDBManager.setString(4, strTagUsageFl);
	    gmDBManager.setString(5, strLatitude);
	    gmDBManager.setString(6, strLongitude);
	    gmDBManager.setString(7, strUserID);
	    gmDBManager.execute();
	    gmDBManager.commit();
	  }
  
  
}
