/*
 * Module: GmCustomerBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11
 * Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed 03/11/10
 * HKP Subpart with one dot should displayed for part number search
 */

package com.globus.custservice.beans;

import java.sql.ResultSet; 
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.custservice.transfer.beans.GmTransferBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.party.beans.GmNPIProcessBean;
import com.globus.prodmgmnt.beans.GmPartNumSearchBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.sales.pricing.beans.GmPriceRequestRptBean;
import com.globus.custservice.beans.GmOrderTagRecordBean;

public class GmCustomerExtnBean extends GmBean implements GmPlaceOrderInterface {

  GmCommonClass gmCommon = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
  // Initialize
  // the
  // Logger
  // Class.
 
  // this will be removed all place changed with GmDataStoreVO constructor
  public GmCustomerExtnBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCustomerExtnBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * loadItemOrderLists - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadItemOrderLists(String strAtrbCodeGrp) throws AppError {
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmcommon = new GmCommonBean(getGmDataStoreVO());
    GmCustomerBean gmCustBean = new GmCustomerBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    ArrayList alMode = new ArrayList();
    ArrayList alShipTo = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alAccList = new ArrayList();
    ArrayList alOrderType = new ArrayList();
    ArrayList alAllRepList = new ArrayList(); // Active and inactive.
    ArrayList alQuoteDetails = new ArrayList();
    ArrayList alEGPSUsage = new ArrayList();  //
    HashMap hmruleShipVal = new HashMap();
    HashMap hmPutGroupId = new HashMap();
   
    try {
      alMode = gmCommon.getCodeList("ORDMO", getGmDataStoreVO());
      alShipTo = gmCommon.getCodeList("CONSP", getGmDataStoreVO());
      // alEGPSUsage - PC-4659-Add EGPS Usage in DO - SpineIT New order
      alEGPSUsage = gmCommon.getCodeList("EGPUSE", getGmDataStoreVO());
      alRepList = gmCustBean.getRepList("Active");
      alAccList = gmSales.reportAccount();
      alOrderType = gmCommon.getCodeList("ODTYP", getGmDataStoreVO());
      alAllRepList = gmCustBean.getRepList("");
      alQuoteDetails = gmCustBean.fetchOrderAttributeInfo("", strAtrbCodeGrp);
      hmPutGroupId.put("RULEID", "SHIP_CARR_MODE_VAL");
      hmruleShipVal = gmcommon.fetchTransactionRules(hmPutGroupId, getGmDataStoreVO());// ship
                                                                                       // carrier
                                                                                       // and mode
                                                                                       // default
      // value get from rule

      hmReturn.put("DEFAULTSHIPMODECARRVAL", hmruleShipVal);
      hmReturn.put("ORDERMODE", alMode);
      hmReturn.put("SHIPTO", alShipTo);
      hmReturn.put("REPLIST", alRepList);
      hmReturn.put("ACCLIST", alAccList);
      hmReturn.put("ORDTYPE", alOrderType);
      hmReturn.put("ALLREPLIST", alAllRepList);
      hmReturn.put("QUOTELIST", alQuoteDetails);
      hmReturn.put("EGPSUSAGE", alEGPSUsage);
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadItemOrderLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadItemOrderLists
  
  /**
   * getAccSummary - This method will return the summary info for the Account. Currently used in
   * Order Screen
   * 
   * @param String strAccId
   * @return HashMap
   * @exception AppError
   */
  public HashMap getAccSummary(String strAccId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    String strGpoId = "";
    HashMap hmParam = new HashMap();
    ArrayList alConstructs = new ArrayList();
    ArrayList alRules = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT AD_NAME ADNAME, REGION_NAME RGNAME,DECODE( '" + getComplangid()
        + "', '103097',NVL(D_NAME_EN, D_NAME), D_NAME) DNAME, D_ID DISTID, TER_NAME TRNAME,  ");
    sbQuery.append(" get_party_name(t704d.c101_rpc) rpcname , get_party_name(t704d.c101_health_system) hlthname, ");//PC-2376  Add new fields in Account Pricing Report
    sbQuery.append("DECODE( '" + getComplangid()
        + "', '103097',NVL(REP_NAME_EN, REP_NAME), REP_NAME) RPNAME, ");
    sbQuery.append(" REP_ID REPID , T740.C101_PARTY_ID PRID ");
    sbQuery.append(" ,get_party_name(T740.C101_PARTY_ID) PRNAME, T740.C740_GPO_CATALOG_ID GPOID ");
    sbQuery.append(" ,get_account_name(V700.AC_ID) ANAME ,V700.AC_ID ACCID ");
    sbQuery.append(" ,nvl(GET_ACCOUNT_ATTRB_VALUE (AC_ID,'91982'),'0') SHIPMODE");
    sbQuery.append(" ,nvl(GET_ACCOUNT_ATTRB_VALUE (AC_ID,'105080'),'0') SHIPCARR");
    sbQuery.append(" ,get_party_name(t704d.c101_idn) GPRAFF ");
    sbQuery
        .append(" ,get_party_name(t704d.c101_gpo) HLCAFF , get_account_type(V700.AC_ID) ACCTYPE ");
    sbQuery.append(" ,GET_CODE_NAME(T704.c901_CURRENCY)  ACC_CURRENCY ");

    // sbQuery
    // .append(" ,NVL(TRIM(GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE (V700.AC_ID, '5507'))), '') ACC_CURRENCY ");
    // // 5507
    // -
    // Currency
    // Value,
    // based
    // on
    // account,
    // getting
    // currency
    sbQuery.append(" , T704.C704_COMMENTS ACCCOMMENTS ");
    // sbQuery.append(" , GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE(AC_ID,'903103')) contract ");//
    // 903103:
    // Contract;
    // 903100:
    // Yes
    sbQuery
        .append(", GET_CODE_NAME(DECODE(t704d.c901_contract,0,'',t704d.c901_contract)) CONTRACT ");
    sbQuery.append(" ,GET_ACCOUNT_ATTRB_VALUE (T704.c704_account_id, 10306194) CIG ");

    sbQuery.append(" , T704.C101_PARTY_ID partyId ");
    // NET NUMBER CHANGE
    sbQuery.append(" , T704.c101_DEALER_ID DEALERID");
    sbQuery.append(" , DECODE( '" + getComplangid()
        + "', '103097',NVL(DEALER_NM_EN, DEALER_NM), DEALER_NM) DEALERNM");
    sbQuery.append(" , GET_CODE_NAME(t101_inv.c901_invoice_closing_date) CLOSINGDATE, get_rule_value_by_company(V700.compid,'ACC_DIV_EGPS','"+getCompId()+"') EGPSFL");
    sbQuery
        .append(" FROM V700_TERRITORY_MAPPING_DETAIL V700, T740_GPO_ACCOUNT_MAPPING T740 ,  T704_Account T704, t704d_account_affln t704d, t101_party t101");
    sbQuery.append(" , t101_party_invoice_details T101_INV");
    sbQuery.append(" WHERE AC_ID = '");
    sbQuery.append(strAccId);
    sbQuery.append("' AND V700.AC_ID = T740.C704_ACCOUNT_ID (+) ");
    sbQuery.append(" AND V700.AC_ID = T704.C704_ACCOUNT_ID (+) ");
    sbQuery.append(" AND t704.C704_ACCOUNT_ID = t704d.C704_ACCOUNT_ID (+) ");
    sbQuery.append(" AND v700.dealer_id     = t101_inv.c101_party_id (+) ");
    sbQuery.append(" AND v700.dealer_id     = t101.c101_party_id (+) ");
    sbQuery.append(" AND NVL(t101.c101_party_id,-999)   = NVL(T101_INV.c101_party_id,-999) ");
    sbQuery.append(" AND t704d.C704D_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND t740.C740_VOID_FL (+) is null ");
    sbQuery.append(" AND t704.C704_VOID_FL (+) is null ");
    sbQuery.append(" AND t101_inv.c101_void_fl (+) IS NULL ");
    sbQuery.append(" AND t101.c101_void_fl (+) IS NULL ");
    sbQuery.append(" AND T704.c1900_company_id = ");
    sbQuery.append(getCompId());

    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("ACCSUM", hmResult);
    strGpoId = GmCommonClass.parseNull((String) hmResult.get("PRID"));

    if (!strGpoId.equals("")) {
      hmParam.put("PID", strGpoId);
      alConstructs = gmSales.reportGPOConstructList(hmParam);
      hmReturn.put("CONSTRUCTS", alConstructs);
    }
    gmDBManager.close();
    return hmReturn;
  } // End of getAccSummary


/**
 * saveItemOrder - This method will
 * 
 * @return HashMap
 * @exception AppError
 */
@Override
public HashMap saveItemOrder(HashMap hmParam) throws AppError {

  String strMsg = "";
  String strPrepareString = null;
  String strCtrlNumberStr = "";
  HashMap hmReturn = new HashMap();
  Date dtSurgDt = null;

  GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
  GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
  GmCommonBean gmCommonBean = new GmCommonBean(gmDataStoreVO);
  GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(gmDataStoreVO);
  GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(gmDataStoreVO);
  GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);
  GmNPIProcessBean GmNPIProcessBean = new GmNPIProcessBean(gmDataStoreVO);
  GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(gmDataStoreVO);
  GmOrderTagRecordBean gmOrderTagRecordBean = new GmOrderTagRecordBean(getGmDataStoreVO());
  GmCustomerBean gmCustBean = new GmCustomerBean(getGmDataStoreVO());
  
  String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
  String strTempId = GmCommonClass.parseNull((String) hmParam.get("STRTEMPID"));
  String strTotal = GmCommonClass.parseNull((String) hmParam.get("TOTAL"));
  String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
  String strAdjInputStr = GmCommonClass.parseNull((String) hmParam.get("ADJINPUTSTR"));
  String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
  String strOrdType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
  String strBillTo = GmCommonClass.parseNull((String) hmParam.get("BILLTO"));
  String strMode = GmCommonClass.parseNull((String) hmParam.get("MODE"));
  String strPerson = GmCommonClass.parseNull((String) hmParam.get("PERSON"));
  String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
  String strPO = GmCommonClass.parseNull((String) hmParam.get("PO"));
  String strConCode = GmCommonClass.parseNull((String) hmParam.get("CONCODE"));
  String strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENTORDID"));

  String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
  String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));
  String strShipCarr = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
  String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
  String strAddressId = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID"));
  // String strSalesRep = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
  String strQuoteStr = GmCommonClass.parseNull((String) hmParam.get("QUOTESTR"));
  String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
  strCtrlNumberStr = GmCommonClass.parseNull((String) hmParam.get("CTRLNUMBERSTR"));
  String strHardCpPO = GmCommonClass.parseNull((String) hmParam.get("HARDCPPO"));
  String strOverrideAttnTo = GmCommonClass.parseNull((String) hmParam.get("OVERRIDEATTNTO"));
  String strShipInstruction = GmCommonClass.parseNull((String) hmParam.get("SHIPINSTRUCTION"));
  String strOrderComments = GmCommonClass.parseNull((String) hmParam.get("ORDER_COMMENTS"));
  String strRefOrderId = GmCommonClass.parseNull((String) hmParam.get("REFORDID"));
  String strReqDate = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDT"));
  String strShipCharge = GmCommonClass.parseNull((String) hmParam.get("SHIPCHARGE"));
  String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
  strShipCharge =
      strShipCharge.equalsIgnoreCase("N/A") ? "" : GmCommonClass.replaceAll(strShipCharge, ",",
          "");
  String strLotOverrideFl = GmCommonClass.parseNull((String) hmParam.get("LOTOVERRIDEFL"));
  String strUpdateDDTFl = GmCommonClass.parseNull((String) hmParam.get("UPDATE_DDT_FL"));
  String strUsageLotFl = GmCommonClass.parseNull((String) hmParam.get("USAGE_LOT_FL"));
  String strEGPSUsage = GmCommonClass.parseNull((String) hmParam.get("EGPSUSAGE"));

  if (!strOrderComments.equals("")) {
    strComments = strOrderComments;
  }
  strHardCpPO = (strHardCpPO.equals("on")) ? "Y" : "";
  String strShipId = "";
  strPO = gmCustBean.returnCustomerPO(strOrderId, strPO);
  dtSurgDt = (java.util.Date) hmParam.get("SURGERYDT");

  log.debug("calling GM_SAVE_ITEM_ORDER *** " + hmParam);

  gmDBManager.setPrepareString("GM_SAVE_ITEM_ORDER", 25);
  gmDBManager.registerOutParameter(18, java.sql.Types.CHAR);
  gmDBManager.setString(1, strOrderId);
  gmDBManager.setString(2, strBillTo);
  gmDBManager.setInt(3, Integer.parseInt(GmCommonClass.parseZero(strMode)));
  gmDBManager.setString(4, strPerson);
  gmDBManager.setString(5, strOrderComments);
  gmDBManager.setString(6, strPO);
  gmDBManager.setString(7, strTotal);
  gmDBManager.setString(8, strUserId);
  gmDBManager.setString(9, strInputStr);
  gmDBManager.setInt(10, Integer.parseInt(strOrdType));
  gmDBManager.setInt(11, Integer.parseInt(GmCommonClass.parseZero(strConCode)));
  gmDBManager.setString(12, strParentOrderId);
  gmDBManager.setInt(13, Integer.parseInt(GmCommonClass.parseZero(strShipTo)));
  gmDBManager.setString(14, strShipToId);
  gmDBManager.setInt(15, Integer.parseInt(GmCommonClass.parseZero(strShipCarr)));
  gmDBManager.setInt(16, Integer.parseInt(GmCommonClass.parseZero(strShipMode)));
  gmDBManager.setInt(17, Integer.parseInt(GmCommonClass.parseZero(strAddressId)));
  gmDBManager.setString(19, strCaseId);
  gmDBManager.setString(20, strHardCpPO);
  gmDBManager.setString(21, strOverrideAttnTo);
  gmDBManager.setString(22, strShipInstruction);
  gmDBManager.setString(23, strRefOrderId);
  gmDBManager.setString(24, strShipCharge);
  gmDBManager.setDate(25, dtSurgDt);
  log.debug("1");
  gmDBManager.execute();
  log.debug("2");

  strShipId = gmDBManager.getString(18);
  log.debug("3");
  if (!strComments.equals("")) {
    gmCommonBean.saveLog(gmDBManager, strOrderId, strComments, strUserId, "1200");
  }
  // To save BBA order
  if (!strRefOrderId.equals("")) {
	  gmCustBean.saveBBAOrder(gmDBManager, strOrderId, strUserId);
  }

  if (!strQuoteStr.equals("") || !strReqDate.equals("") || !strLotOverrideFl.equals("")) {
    if (!strReqDate.equals(""))
      strQuoteStr = strQuoteStr + "^" + 103926 + "^" + strReqDate + "|";
    if (!strLotOverrideFl.equals("")) 
      strQuoteStr = strQuoteStr + "^" + 103950 + "^" + strLotOverrideFl + "|";
    if(!strEGPSUsage.equals(""))
  	  strQuoteStr = strQuoteStr + "^" + 26241315 + "^" + strEGPSUsage + "|";	
      gmCustBean.saveOrderAttribute(gmDBManager, strOrderId, strUserId, strQuoteStr);
  }
  // MNTTASK-8623 - CSM DO Process control # from S Part
  // Italy company - always save the order info and item order usage table
  
  // PMT-45387: Provision to record DDT and usage LOT
  // Currently Used LOT section store (T502b) based on DDT flag. Going to change based on Usage LOT flag
  if (!strCtrlNumberStr.equals("") || "Y".equals(strUsageLotFl)) {
    hmParam.put("INPUTSTR", strCtrlNumberStr);
    hmParam.put("ORDERID", strOrderId);
    hmParam.put("SCREEN", "NEW_ORDER");

    log.debug(" StrCtrlNumberStr >>>>>" + hmParam);
    gmDOBean.updateControlNumberDetail(hmParam, gmDBManager);
  }
  // hmParam.put("SOURCE", "50180"); // orders
  // hmParam.put("REFID", strOrderId);
  // hmParam.put("STATUSFL", "15");
  // gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
  log.debug("strShipId " + strShipId);

  gmTxnSplitBean.checkAndSplitDO(gmDBManager, strOrderId, strUserId);
  
  /* added for PMT-24435 - To Update Order id when order created without DOID*/
  gmOrderTagRecordBean.updateOrderId(gmDBManager, strOrderId, strUserId, strTempId);

  /* For Updating the Unit Price Adjustment Details for the Sales Order in T502_ITEM_ORDER table */
  if (!strAdjInputStr.equals("")) {
	  gmCustBean.updateAdjustmentDetails(gmDBManager, strOrderId, strAdjInputStr, strUserId);
  }

  // update the adjustment details for BBA order
  gmCustBean.updateAdjustmentDetailsBBA(gmDBManager, strOrderId, strUserId);

  gmCustBean.syncAccAttribute(gmDBManager, strOrderId, strBillTo, "ORDER", strUserId);
  // TO UPDATE THE NPI TRANSACTION STATUS
  GmNPIProcessBean.saveNPITransactionStatus(gmDBManager, strOrderId, strUserId);

  gmDBManager.commit();
  hmReturn.put("MSG", strMsg);
  hmReturn.put("strShipId", strShipId);

  // To Save the Insert part number details in New order screen Using JMS
  String strTxnType = "50261";
  HashMap hmJMSParam = new HashMap();
  hmJMSParam.put("TXNID", strOrderId);
  hmJMSParam.put("TXNTYPE", strTxnType);
  hmJMSParam.put("STATUS", "93342");
  hmJMSParam.put("USERID", strUserId);
  hmJMSParam.put("COMPANYINFO", strCompanyInfo);
  gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);

  return hmReturn;
} // End of saveItemOrder

/**
 * loadOrderDetails - This method is a function override method of loadOrderDetails and it only
 * passes order id as parameter and based on the value it fecthes all order information
 * 
 * @param String strOrdId
 * @return HashMap
 * @exception AppError
 */
public HashMap loadOrderDetails(String strOrdId) throws AppError {
  return loadOrderDetails(strOrdId, null);
}
/**
 * loadOrderDetails - This method will be used to get the order information
 * 
 * @param String strOrdId
 * @param String strProjectType
 * @return HashMap
 * @exception AppError
 */
public HashMap loadOrderDetails(String strOrdId, String strProjectType) throws AppError {
  HashMap hmReturn = new HashMap();
  HashMap hmPayment = new HashMap();

  ArrayList alReturn = new ArrayList();
  ArrayList alCartDetails = new ArrayList();
  ArrayList alReturnDetail = new ArrayList();
  ArrayList alOrderId = new ArrayList();
  ArrayList alInvoiceId = new ArrayList();
  ArrayList alQuoteDetails = new ArrayList();
  ArrayList alTagDetails = new ArrayList();
  ArrayList alControlNumDetails = new ArrayList();
  ArrayList alOrderAtbDetails = new ArrayList();
  ArrayList alPaymentDetails = new ArrayList();
  ArrayList alPriceReqDtl = new ArrayList();
  ArrayList alEGPSUsage = new ArrayList();  //PC-4892 EGPS in Edit order
  ArrayList alOrdSurAtbDetails = new ArrayList();  //PC-4659-Add EGPS Usage in DO - SpineIT New order
  GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
  GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
  GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(getGmDataStoreVO());
  GmCustomerBean gmCustBean = new GmCustomerBean(getGmDataStoreVO());
  String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
  // Locale
  GmResourceBundleBean gmResourceBundleBean =
      GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

  String strOrderType = "";
  String strShowSurgAtr = "NO";
  strShowSurgAtr =
      GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
  hmPayment.put("ORDERID", strOrdId);
  hmPayment.put("INVID", "");
  HashMap hmResult = gmDOBean.fetchOrderDetails(strOrdId);
  hmReturn.put("ORDERDETAILS", hmResult);
  strOrderType = GmCommonClass.parseNull((String) hmResult.get("ORDERTYPE"));
  HashMap hmInParams = new HashMap();
  hmInParams.put("ORDERID", strOrdId);
  hmInParams.put("gmResourceBundleBean", gmResourceBundleBean);
  hmInParams.put("PROJECTTYPE", strProjectType);
  hmInParams.put("gmDataStoreVO", getGmDataStoreVO());
  // BBA-41 Dynamic Class Loading.
  if (GmCommonClass.parseNull(strProjectType).equals("PACKSLIP")) {
    strProjectType = null;
    String strClassName = "com.globus.custservice.beans.GmProcBaseOrderPackSlipBean";
    GmProcessBasePackSLipInterface packSlipClient =
        GmProcessBasePackSlip.getInstance(strClassName);
    alCartDetails = packSlipClient.fetchCartDetails(hmInParams);
  } else {
    GmCartDetailsInterface gmCartDetailsInterface =
        (GmCartDetailsInterface) GmCommonClass.getSpringBeanClass("xml/Cart_Details_Beans.xml",
            getGmDataStoreVO().getCmpid());
    alCartDetails = gmCartDetailsInterface.fetchCartDetails(hmInParams);
  }
  hmReturn.put("CARTDETAILS", alCartDetails);

  hmResult = new HashMap();
  hmResult = gmDOBean.fetchShipDetails(strOrdId);
  hmReturn.put("SHIPDETAILS", hmResult);

  alReturn = gmDOBean.getBackOrderDetails(strOrdId, strProjectType);
  hmReturn.put("BACKORDER", alReturn);

  alReturn = gmDOBean.fetchConstructDetails(strOrdId);
  hmReturn.put("CONSTRUCTDETAILS", alReturn);

  alReturn = gmDOBean.fetchOrderSummary(strOrdId);
  hmReturn.put("ORDERSUMMARY", alReturn);

  alReturn = gmDOBean.fetchShipSummary(strOrdId);
  hmReturn.put("SHIPSUMMARY", alReturn);

  alOrderId = gmDOBean.fetchRelatedOrderId(strOrdId);
  hmReturn.put("ALORDERID", alOrderId);

  alTagDetails = gmDOBean.fetchDOTagDetails(strOrdId);
  hmReturn.put("ALTAGDETAILS", alTagDetails);

  alReturnDetail = gmDOBean.fetchDOReturnsDetails(strOrdId);
  hmReturn.put("ALRETURNDETAILS", alReturnDetail);

  alControlNumDetails = gmDOBean.fetchDOControlNumDetails(strOrdId);
  hmReturn.put("ALCONTROLNUMDETAILS", alControlNumDetails);

  alControlNumDetails = gmDOBean.filterControlNumberDetails(strOrdId, alControlNumDetails);
  hmReturn.put("ALFILTEREDCNUMDETAILS", alControlNumDetails);

  alInvoiceId = gmDOBean.fetchInvoiceDetails(strOrdId);
  hmReturn.put("ALINVOICEID", alInvoiceId);
  // For PMT-5022 There is a section newly added in DO Summary Section named payment Details which
  // will display payment amount and who initiated the payment.
  alPaymentDetails = GmCommonClass.parseNullArrayList(gmARBean.fetchPaymentDetails(hmPayment));
  hmReturn.put("ALPAYMENTDETAILS", alPaymentDetails);

  alPriceReqDtl = gmPriceRequestRptBean.fetchOrderPriceRequest(strOrdId);
  hmReturn.put("ALPRICEREQDTL", alPriceReqDtl);

  if (strOrderType.equals("2520")) {
    alQuoteDetails = gmCustBean.fetchOrderAttributeInfo(strOrdId, "QUOATB");
    hmReturn.put("QUOTEDETAILS", alQuoteDetails);
  } else if (strShowSurgAtr.equals("YES")) {
    alQuoteDetails = gmCustBean.fetchOrderAttributeInfo(strOrdId, "SURATB");
    hmReturn.put("QUOTEDETAILS", alQuoteDetails);
  }
  alOrderAtbDetails = gmCustBean.fetchOrderAttributeInfo(strOrdId, "ORDATB");
  hmReturn.put("ORDATBDETAILS", alOrderAtbDetails);
  
  alOrdSurAtbDetails = gmCustBean.fetchOrderAttributeInfo(strOrdId, "EGPUSE");
  hmReturn.put("ALORDSURATBDETAILS", alOrdSurAtbDetails);
  log.debug("alOrdSurAtbDetails************* "+alOrdSurAtbDetails);
  
  alEGPSUsage = gmCommon.getCodeList("EGPUSE", getGmDataStoreVO());
  hmReturn.put("EGPSUSAGE", alEGPSUsage);
  return hmReturn;
} // End of loadOrderDetails

/**
 * saveShipOrderDetails - This method will be used to save the order details Both Order and
 * shipping information
 * 
 * @param HashMap hmParam -- contains entire order information
 * @param String strOrderId
 * @param String strUsername
 * @return HashMap
 * @exception AppError
 */
@Override
public String saveOrderDetails(HashMap hmParam, String strOrderId, String strUsername)
    throws AppError {

  GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
  GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
  GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);
  GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(gmDataStoreVO);

  // CallableStatement gmDBManager = null;

  String strBeanMsg = "";
  String strMsg = "";
  String strPrepareString = null;

  String strAccId = "";
  String strMode = "";
  String strPerson = "";
  String strComments = "";
  String strPO = "";
  String strParentOrderId = "";
  String strInvoiceId = "";

  String strShipDate = "";
  String strShipCarrName = "";
  String strSalesRepId = "";
  String strRmQtyStr = "";
  String strBOQtyStr = "";
  String strQuoteStr = "";
  String strCnumStr = "";
  String strHardCpPO = "";
  String strUsedLotStr = "";

  String strRmQtyMsgStr = "";
  String strBOQtyMsgStr = "";

  Date dtSurgDt = null;
  Date dtOrderDt = null;

  HashMap hmResult = new HashMap();
  HashMap hmReturn = new HashMap();

  try {

    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strMode = GmCommonClass.parseNull((String) hmParam.get("OMODE"));
    strPerson = GmCommonClass.parseNull((String) hmParam.get("OPERSON"));
    // strShipTo = (String) hmParam.get("SHIPTO");
    // strShipToId = (String) hmParam.get("SHIPID");
    strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    strPO = GmCommonClass.parseNull((String) hmParam.get("CPO"));
    strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENTORDERID")); // parentid
    strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
    strSalesRepId = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
    strRmQtyStr = GmCommonClass.parseNull((String) hmParam.get("RMQTYSTR"));
    strBOQtyStr = GmCommonClass.parseNull((String) hmParam.get("BOQTYSTR"));
    strQuoteStr = GmCommonClass.parseNull((String) hmParam.get("QUOTESTR"));
    strCnumStr = GmCommonClass.parseNull((String) hmParam.get("CNUMSTR"));
    strUsedLotStr = GmCommonClass.parseNull((String) hmParam.get("USEDLOTSTR"));
    strHardCpPO = GmCommonClass.parseNull((String) hmParam.get("HARDCPPO"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    strHardCpPO = (strHardCpPO.equals("on")) ? "Y" : "";
    dtSurgDt = (java.util.Date) hmParam.get("SURGERYDT");
    dtOrderDt = (java.util.Date) hmParam.get("ORDER_DATE");

    gmDBManager.setPrepareString("GM_UPDATE_ORDER", 19);
    gmDBManager.registerOutParameter(18, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(19, java.sql.Types.CHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strAccId);
    gmDBManager.setString(3, strPO);
    gmDBManager.setString(4, strUsername);
    gmDBManager.setInt(5, Integer.parseInt(strMode));
    gmDBManager.setString(6, strPerson);
    gmDBManager.setString(7, strComments); // Comments and other
    // information
    gmDBManager.setString(8, strParentOrderId);
    gmDBManager.setString(9, strInvoiceId);
    gmDBManager.setString(10, strSalesRepId);
    gmDBManager.setString(11, strRmQtyStr);
    gmDBManager.setString(12, strBOQtyStr);
    gmDBManager.setString(13, strHardCpPO);
    gmDBManager.setString(14, strQuoteStr);
    gmDBManager.setString(15, strUsedLotStr);
    gmDBManager.setDate(16, dtSurgDt);
    gmDBManager.setDate(17, dtOrderDt);// to update order date for Month End Reclassification
    gmDBManager.execute();
    strRmQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(18));
    strBOQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(19));
    if (!strCnumStr.equals("")) {
      hmParam.put("INPUTSTR", strCnumStr);
      hmParam.put("ORDERID", strOrderId);
      hmParam.put("USERID", strUsername);
      hmParam.put("SCREEN", "EDIT_ORDER");
      log.debug(" StrCtrlNumberStr >>>>>" + hmParam);
      gmDOBean.updateControlNumberDetail(hmParam, gmDBManager);
    }

    gmDBManager.commit();
    log.debug("Remove String =" + strRmQtyMsgStr);
    log.debug("BackOrder String =" + strBOQtyMsgStr);
    strMsg = strRmQtyMsgStr + "<br>" + strBOQtyMsgStr;
    hmReturn.put("MSG", strMsg);

    // To Save the Insert part number details in New order screen Using JMS
    String strTxnType = "50261";
    HashMap hmJMSParam = new HashMap();
    hmJMSParam.put("TXNID", strOrderId);
    hmJMSParam.put("TXNTYPE", strTxnType);
    hmJMSParam.put("STATUS", "93342");
    hmJMSParam.put("USERID", strUsername);
    hmJMSParam.put("COMPANYINFO", strCompanyInfo);
    gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);

  } catch (Exception e) {
    e.printStackTrace();
    GmLogError.log("Exception in GmCustomerBean:save Order details ", "Exception is:" + e);
    throw new AppError(e);
  }

  return strMsg;

} // end of saveShipOrderDetails

/**
 * fetchOrderAttributeValues - fetch order attribute values
 * @param strOrderId
 * @param strAttrType
 * @return
 * @throws AppError
 */
public HashMap fetchOrderAttributeValues(String strOrderId, String strAttrType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_fch_order_attrib_values", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setInt(2, Integer.parseInt(strAttrType));
    gmDBManager.execute();
    hmReturn =
            GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
                .getObject(3)));
    gmDBManager.close();
return hmReturn;
}


 }// end of class GmCustomerExtnBean
