package com.globus.custservice.beans;

import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmStockTransferTransBean extends GmBean {

  public GmStockTransferTransBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * reconfig_RQSTToConsign - This method will create the stock transfer consignments
   * 
   * @param String requestID
   * @param String strInputString
   * @param String strUserId
   * @return void
   * @exception AppError
   **/
  public void reconfig_RQSTToConsign(String strRequestID, String strInputString, String strUserId)
      throws AppError {

    HashMap hm = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_request_fulfill.gm_sav_reconfig_RQST_Consign", 3);
    gmDBManager.setString(1, strRequestID);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * createChildReqFromMasterReq - This method will create child request using master request
   * 
   * @param strMasterRequestId ,strUserId
   * @return String
   * @exception AppError
   */
  public String createChildReqFromMasterReq(String strMasterRequestId, String strUserId)
      throws AppError {
    String strChildReqId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_create_child_ST_request", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strMasterRequestId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    strChildReqId = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.commit();

    return strChildReqId;
  }


}
