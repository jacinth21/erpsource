package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDOReportBean extends GmBean {
	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmDOReportBean(GmDataStoreVO gmDataStoreVO) throws Exception {
		super(gmDataStoreVO);
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchProductFamily - This method will Fetch all the distinct part numbers
	 * and product family
	 * 
	 * @exception AppError
	 **/
	public String fetchProductFamily(String strPartNum) throws AppError {
		String strProdFmly = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setFunctionString("get_partnum_product", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.execute();
		strProdFmly = GmCommonClass.parseNull((gmDBManager.getString(1)));
		gmDBManager.close();
		return strProdFmly;
	}

	/**
	 * fetchProductFamilyByTxnId - This method will Fetch the part numbers and
	 * product family for transaction
	 * 
	 * @exception AppError
	 **/
	public ArrayList fetchProductFamilyByTxnId(String strRefId, String strSource)
			throws AppError {
		ArrayList alProdFmlyDtls = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString(
				"gm_pkg_op_order_rpt.gm_fch_partprod_fmly", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strRefId);
		gmDBManager.setString(2, strSource);
		gmDBManager.execute();
		alProdFmlyDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return alProdFmlyDtls;
	}
	/**
	 * fetchPopulatedCartDetails - This method will Fetch the cart details
	 * removing the usage part code
	 * 
	 * @exception AppError
	 **/
	public ArrayList fetchPopulatedCartDetails(ArrayList alCartDetails,
			String strRefId, String strSource) throws AppError {
		ArrayList alProdFmlyDtls = new ArrayList();
		HashMap hmCartDetails = new HashMap();
		HashMap hprodFmly = new HashMap();
		Iterator itrCartDetails = alCartDetails.iterator();
		String strCartPartNum = "";
		String strPartNum = "";
		String strProdFmly = "";
		while (itrCartDetails.hasNext()) {
			hmCartDetails = GmCommonClass
					.parseNullHashMap((HashMap) itrCartDetails.next());
			if(strSource.equals("50180"))
			{
				strCartPartNum = GmCommonClass.parseNull((String) hmCartDetails
						.get("ID"));
			}else
			{
				strCartPartNum = GmCommonClass.parseNull((String) hmCartDetails
						.get("PNUM"));
			}
			
			alProdFmlyDtls = fetchProductFamilyByTxnId(strRefId, strSource);
			Iterator itrProdFmlyDtls = alProdFmlyDtls.iterator();
			while (itrProdFmlyDtls.hasNext()) {
				hprodFmly = GmCommonClass
						.parseNullHashMap((HashMap) itrProdFmlyDtls.next());
				strPartNum = GmCommonClass.parseNull((String) hprodFmly
						.get("PNUM"));
				strProdFmly = GmCommonClass.parseNull((String) hprodFmly
						.get("PDTFMLY"));
				if (strCartPartNum.equals(strPartNum)
						&& strProdFmly.equals("26240096")) {
					itrCartDetails.remove();
				}
			}
		}
		return alCartDetails;
	}
	/**
	 * this is used fetch parent order id from order id
	 * @param strOrderId
	 * @return
	 */
	public String fetchParentOrderId(String strOrderId)throws AppError {
		String strParentId = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setFunctionString("gm_pkg_op_order_rpt.gm_parent_order_id", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strOrderId);
		gmDBManager.execute();
		strParentId = GmCommonClass.parseNull((gmDBManager.getString(1)));
		gmDBManager.close();
		return strParentId;
	}
	
}
