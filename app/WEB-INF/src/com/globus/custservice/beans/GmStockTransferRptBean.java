package com.globus.custservice.beans;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmStockTransferRptBean extends GmBean {

  public GmStockTransferRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchStockTransfer - This method will fetch the stock transfer request
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchStockTransfer(HashMap hmParam) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strReqComp = GmCommonClass.parseNull((String) hmParam.get("REQCOMPID"));
    String strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strTranID = GmCommonClass.parseNull((String) hmParam.get("TRNASCTIONID"));
    String strFulFillTranID = GmCommonClass.parseNull((String) hmParam.get("FULFILLTRNASCTIONID"));
    String strInvoiceID = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strStatusAlt = "";
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();


    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append("SELECT T520.C520_Request_Id Transid ,");
    sbQuery.append(" to_char(T520.C520_Created_Date,'" + getCompDateFmt() + "') Createddt, ");
    sbQuery.append(" to_char(T520.C520_Required_Date ,'" + getCompDateFmt() + "') Req_Date, ");
    sbQuery.append(" DECODE('" + getCompId() + "',T5201.C1900_Company_Id,'Y','N') proc_req_fl, ");
    sbQuery
        .append(" get_user_name(T520.C520_Request_By) Req_By, Get_Company_Name(T5201.C1900_Company_Id) fullfill_company, ");
    sbQuery
        .append(" gm_pkg_op_request_summary.get_request_status(T520.C520_Status_Fl) status_fl,T520.C520_Master_Request_Id master_req_id, ");
    sbQuery
        .append(" t504.c504_consignment_id fullfill_trans_id,  t504.c504_stock_transfer_invoice_id ST_Invoice_ID ");
    sbQuery.append(" FROM T520_Request T520,T5201_Requested_Company T5201,t504_consignment t504 ");
    sbQuery.append(" WHERE T520.C520_Request_Id = T5201.C520_Request_Id ");
    sbQuery.append(" AND t520.c520_request_id = t504.c520_request_id(+)");
    sbQuery.append(" AND (T5201.C1900_Company_Id = '" + getCompId()
        + "' OR T520.C1900_Company_Id = '" + getCompId() + "') ");
    sbQuery.append(" AND T520.C520_Void_Fl  IS NULL ");
    sbQuery.append(" AND T5201.C5201_Void_Fl  IS NULL ");
    sbQuery.append(" AND t504.c504_void_fl(+) IS NULL ");
    sbQuery.append(" AND T520.C520_Request_For  IN ('26240420') "); // only stock transfer(26240420)
                                                                    // requests

    if ((!strReqComp.equals("0")) && (!strReqComp.equals(""))) {
      sbQuery.append(" AND T520.C1900_Company_Id = " + strReqComp);

    }
    if ((!strStatus.equals("0")) && (!strStatus.equals(""))) {
      strStatusAlt = GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strStatus));
      sbQuery.append(" AND T520.C520_Status_Fl  = '");
      sbQuery.append(strStatusAlt);
      sbQuery.append("'");

    }

    if ((!strFromDt.equals("")) && (!strToDt.equals(""))) {

      if (strDateType.equals("106680")) {

        sbQuery.append(" AND TRUNC(T520.C520_Created_Date) between ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFromDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");

      } else if (strDateType.equals("106681")) {
        sbQuery.append(" AND TRUNC(T520.C520_Required_Date) between ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFromDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");
      }


    }

    if ((!strTranID.equals("")) && (strTranID != null)) {
      sbQuery.append(" AND  T520.C520_Request_Id  = UPPER('");
      sbQuery.append(strTranID);
      sbQuery.append("')");

    }

    if ((!strFulFillTranID.equals("")) && (strFulFillTranID != null)) {

      sbQuery.append(" AND  t504.c504_consignment_id  = UPPER('");
      sbQuery.append(strFulFillTranID);
      sbQuery.append("')");

    }

    if ((!strInvoiceID.equals("")) && (strInvoiceID != null)) {

      sbQuery.append(" AND t504.c504_stock_transfer_invoice_id  = UPPER('");
      sbQuery.append(strInvoiceID);
      sbQuery.append("')");

    }

    sbQuery.append(" order by T520.C520_Created_Date desc");
    log.debug(" Query for Stock " + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;
  }

  /**
   * fetchStockTransferDetailReport - This method will fetch the stock transfer request in details
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchStockTransferDetailReport(HashMap hmParam) {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();

    String strFullfillComp = GmCommonClass.parseNull((String) hmParam.get("FULFILLCOMPANY"));
    String strReqId = GmCommonClass.parseNull((String) hmParam.get("TRNASCTIONID"));
    String strFulfillId = GmCommonClass.parseNull((String) hmParam.get("FULFILLTRNASCTIONID"));
    String strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    String strStatusAlt = "";


    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append("SELECT NVL(req_id,cons_req_id) trans_id,");
    sbQuery
        .append(" cons_id fullfill_id,NVL(cons_part_num,part_num) pnum,get_partnum_desc (NVL(cons_part_num,part_num)) part_desc ,");
    sbQuery
        .append(" NVL(qty,cons_qty) requested_qty,NVL(cons_qty,0) processed_qty,NVL((NVL(qty,cons_qty) - cons_qty),qty) variance_qty ");
    sbQuery
        .append(" FROM  (SELECT t520.c520_request_id req_id,t521a.c205_part_number_id part_num ,t521a.c521a_qty qty");
    sbQuery.append(" FROM t520_request t520,t521a_request_log t521a ");
    sbQuery.append(" WHERE t520.c520_request_id       = t521a.c520_request_id");
    sbQuery.append(" AND t520.c520_request_for = '26240420' ");
    sbQuery.append(" AND t520.c1900_company_id =" + getCompId());

    if ((!strReqId.equals("")) && (strReqId != null)) {
      sbQuery.append(" AND  t520.c520_request_id = UPPER('");
      sbQuery.append(strReqId);
      sbQuery.append("')");
    }

    if ((!strFromDt.equals("")) && (!strToDt.equals(""))) {

      if (strDateType.equals("106680")) {

        sbQuery.append(" AND TRUNC(T520.C520_Created_Date) between ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFromDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");

      } else if (strDateType.equals("106681")) {
        sbQuery.append(" AND TRUNC(T520.C520_Required_Date) between ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFromDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");
      }


    }

    if ((!strStatus.equals("0")) && (!strStatus.equals(""))) {
      strStatusAlt = GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strStatus));
      sbQuery.append(" AND T520.C520_Status_Fl  = '");
      sbQuery.append(strStatusAlt);
      sbQuery.append("'");

    }

    sbQuery.append(" AND t520.c520_void_fl IS NULL  AND t521a.c521a_void_fl IS NULL  ) t520_req  ");
    sbQuery.append(" full outer join ");
    sbQuery.append(" (SELECT t504.c520_request_id cons_req_id, t504.c504_consignment_id cons_id ,");
    sbQuery.append(" t505.c205_part_number_id cons_part_num,SUM(t505.c505_item_qty) cons_qty ");
    sbQuery.append(" FROM t504_consignment t504,t505_item_consignment t505");
    sbQuery.append(" WHERE t505.c504_consignment_id = t504.c504_consignment_id");
    sbQuery.append(" AND t504.c520_request_id IN (SELECT c520_request_id FROM t520_request ");
    sbQuery.append(" WHERE  c520_request_for = '26240420' ");

    if ((!strFromDt.equals("")) && (!strToDt.equals(""))) {

      if (strDateType.equals("106680")) {

        sbQuery.append(" AND TRUNC(C520_Created_Date) between ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFromDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");

      } else if (strDateType.equals("106681")) {
        sbQuery.append(" AND TRUNC(C520_Required_Date) between ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFromDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");
      }


    }

    if ((!strStatus.equals("0")) && (!strStatus.equals(""))) {
      strStatusAlt = GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strStatus));
      sbQuery.append(" AND C520_Status_Fl  = '");
      sbQuery.append(strStatusAlt);
      sbQuery.append("'");

    }

    sbQuery.append(" AND c1900_company_id = " + getCompId());
    sbQuery.append(" ) AND t504.c504_void_fl IS NULL AND t505.c505_void_fl IS NULL");

    if ((!strFullfillComp.equals("0")) && (!strFullfillComp.equals(""))) {
      sbQuery.append(" AND t504.c1900_company_id = " + strFullfillComp);
    }

    if ((!strReqId.equals("")) && (strReqId != null)) {
      sbQuery.append(" AND  T504.c520_request_id = UPPER('");
      sbQuery.append(strReqId);
      sbQuery.append("')");
    }

    if ((!strFulfillId.equals("")) && (strFulfillId != null)) {
      sbQuery.append(" AND  T504.c504_consignment_id = UPPER('");
      sbQuery.append(strFulfillId);
      sbQuery.append("')");
    }

    if ((!strPartNum.equals("")) && (strPartNum != null)) {
      sbQuery.append(" AND  T505.c205_part_number_id ='");
      sbQuery.append(strPartNum);
      sbQuery.append("'");
    }



    sbQuery
        .append(" GROUP BY  t504.c520_request_id,t504.c504_consignment_id, t505.c205_part_number_id ) t504_cons");
    sbQuery.append(" on t520_req.req_id = t504_cons.cons_req_id");
    sbQuery.append(" AND t520_req.part_num = t504_cons.cons_part_num");
    sbQuery.append(" ORDER BY trans_id,fullfill_id,pnum");


    log.debug(" Query for Stock " + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReturn;

  }

  /**
   * filterConsignData - This method will be used to fetch stock transfer invoice details
   * 
   * 
   * @param HashMap,ArrayList
   * @return HashMap
   * @exception AppError
   */
  public HashMap filterConsignData(HashMap hmInData, ArrayList alParam) throws AppError {

    GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
    GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmOutData = new HashMap();
    HashMap hmAtbData = new HashMap();
    HashMap hmReqAtbData = new HashMap();
    HashMap hmCompanyAddress = new HashMap();
    String strInvComments = "";
    String strInvPrintlbl = "";
    String strPrintPurposelbl = "";
    int intYear;
    int intMonth;
    String strNxtYear;
    String strFinYear = "";
    java.sql.Date dtCreatedDt = null;
    String strConsignId = GmCommonClass.parseNull((String) hmInData.get("CONSIGNID"));
    String strRequestId = GmCommonClass.parseNull((String) hmInData.get("REQUEST_ID"));
    String strConsignCompId =
        GmCommonClass.parseNull(gmShippingTransBean.getTransactionCompanyId(getCompId(),
            strConsignId, "DOPENDINGCONFIRM", "CONSIGNMENT"));
    String strReqCompId =
        GmCommonClass.parseNull(gmShippingTransBean.getTransactionCompanyId(getCompId(),
            strRequestId, "DOPENDINGCONFIRM", "CONSIGNMENTREQUEST"));

    strConsignCompId = strConsignCompId.equals("") ? "100800" : strConsignCompId;
    String strDivisionId = strConsignCompId.equals("100801") ? "2001" : "2000";
    String strCompanyLocale =
        GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(strConsignCompId));
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean gmCompResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

    hmOutData.put("GMCOMPANYEMAIL",
        GmCommonClass.parseNull(gmCompResourceBundleBean.getProperty("GMCOMPANYEMAIL")));
    hmOutData.put("GMCOMPANYURL",
        GmCommonClass.parseNull(gmCompResourceBundleBean.getProperty("GMCOMPANYURL")));
    hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strConsignCompId, strDivisionId);
    hmReturn.putAll(hmCompanyAddress);

    String strCompLogo =
        GmCommonClass.parseNull(GmCommonClass.parseNull((String) hmCompanyAddress.get("LOGO")));
    String strSealImage =
        GmCommonClass.parseNull(gmCompResourceBundleBean.getProperty("COMP_SEAL"));
    String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
    hmOutData.put("SEALIMAGE", strImagePath + strSealImage + ".gif");
    hmOutData.put("COMPLOGO", strImagePath + strCompLogo + ".gif");

    strInvPrintlbl = GmCommonClass.parseNull((String) hmInData.get("INVPRINTLBL"));

    if (strInvPrintlbl.equals("ORG_RECIPIENT")) {
      strPrintPurposelbl =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ORG_RECIPIENT"));
    } else if (strInvPrintlbl.equals("DUP_TRANSPORTER")) {
      strPrintPurposelbl =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DUP_TRANSPORTER"));
    } else if (strInvPrintlbl.equals("TRIP_SUPPLIER")) {
      strPrintPurposelbl =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("TRIP_SUPPLIER"));
    } else {
      strPrintPurposelbl = "";
    }

    hmAtbData = gmOperationsBean.loadConsignAttribute(strConsignId);
    hmReturn.putAll(hmAtbData);
    hmReqAtbData = gmRequestTransBean.loadRequestAttribute(strRequestId);
    hmReturn.put("LOCALDRUGLIC",
        GmCommonClass.parseNull((String) hmReqAtbData.get("GMDRUGLOCALLIC")));
    hmReturn.put("EXPORTDRUGLIC",
        GmCommonClass.parseNull((String) hmReqAtbData.get("GMDRUGEXPORTLIC")));
    dtCreatedDt = (java.sql.Date) hmInData.get("CDATE");
    hmReturn.put("CREATEDDATE",
        GmCommonClass.getStringFromDate(dtCreatedDt, getGmDataStoreVO().getCmpdfmt()));

    hmReturn.put("SHIPDATE", GmCommonClass.getStringFromDate((java.sql.Date) hmInData.get("SDATE"),
        getGmDataStoreVO().getCmpdfmt()));
    // to get the financial year for invoice date
    String strFinMnthVal =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("FIN_MNTH", "FIN_YEAR_CALC",
            getGmDataStoreVO().getCmpid()));
    strFinMnthVal = strFinMnthVal.equals("") ? "0" : strFinMnthVal;
    int intMnthVal = Integer.parseInt(strFinMnthVal);
    intMonth = dtCreatedDt.getMonth();
    // add 1900 to get the exact 4 digit year
    intYear = dtCreatedDt.getYear() + 1900;
    if (intMonth > intMnthVal) {
      strNxtYear = Integer.toString(intYear + 1);
    } else {
      strNxtYear = Integer.toString(intYear - 1);
    }
    strFinYear = strFinYear + intYear + "-" + strNxtYear.substring(2, strNxtYear.length());
    hmReturn.put("FINYEAR", strFinYear);

    int intSize = alParam.size();
    int intLoop = 0;
    ArrayList alLoop = new ArrayList();
    ArrayList alHashMap = new ArrayList();
    HashMap hmLoop = new HashMap();
    String strIGSTPER = GmCommonClass.parseNull((String) hmAtbData.get("IGST"));
    String strCGSTPER = GmCommonClass.parseNull((String) hmAtbData.get("CGST"));
    String strSGSTPER = GmCommonClass.parseNull((String) hmAtbData.get("SGST"));


    double igstPer = 0.0;
    double cgstPer = 0.0;
    double sgstPer = 0.0;
    int intQty = 0;
    double dbItemTotal = 0.0;
    double dbTotalItemIGST = 0.0;
    double dbTotalItemCGST = 0.0;
    double dbTotalItemSGST = 0.0;
    double dbGrossTotal = 0.0;
    double dbRoundedGrossTotal = 0.0;
    double dbTotal = 0.0;


    String strItemTotal = "";
    String strTotal = "";
    String strPrice = "";
    String strQty = "";
    String strNumInWords = "";
    String strOrderComments = "";
    String strPartDesc = "";
    String strPart = "";


    String strCurrency = "";
    if (intSize == 0) {

      strCurrency = "$";
      hmReturn.put("CURRENCY", strCurrency);
      hmReturn.put("GROSSTOTAL", dbGrossTotal);
      hmReturn.put("TOTALIGST", dbTotalItemIGST);
      hmReturn.put("TOTALCGST", dbTotalItemCGST);
      hmReturn.put("TOTALSGST", dbTotalItemSGST);
      hmReturn.put("NETTOTAL", dbTotal);

    }
    for (int i = 0; i < intSize; i++) {

      hmLoop = (HashMap) alParam.get(i);
      strPart = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmLoop.get("PNUM")));

      strPartDesc =
          GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmLoop.get("PDESC")));

      if (!strIGSTPER.equals("")) {
        igstPer = Double.parseDouble(strIGSTPER) / 100;
      }
      if (!strCGSTPER.equals("")) {
        cgstPer = Double.parseDouble(strCGSTPER) / 100;
      }
      if (!strSGSTPER.equals("")) {
        sgstPer = Double.parseDouble(strSGSTPER) / 100;
      }
      strPrice = GmCommonClass.parseNull((String) hmLoop.get("PRICE"));
      strQty = GmCommonClass.parseNull((String) hmLoop.get("IQTY"));
      intQty = Integer.parseInt(strQty);
      dbItemTotal = Double.parseDouble(strPrice);
      dbItemTotal = GmCommonClass.roundDigit((intQty * dbItemTotal), 2); // Multiply by Qty

      dbTotalItemIGST =
          !strIGSTPER.equals("") ? dbTotalItemIGST + (dbItemTotal * igstPer) : dbTotalItemIGST;
      dbTotalItemCGST =
          !strCGSTPER.equals("") ? dbTotalItemCGST + (dbItemTotal * cgstPer) : dbTotalItemCGST;
      dbTotalItemSGST =
          !strSGSTPER.equals("") ? dbTotalItemSGST + (dbItemTotal * sgstPer) : dbTotalItemSGST;
      strItemTotal = "" + dbItemTotal;
      dbTotal = dbTotal + dbItemTotal;
      strTotal = "" + dbTotal;
      hmLoop.put("ITEMTOTAL", strItemTotal);
      hmLoop.put("PDESC", strPartDesc);
      hmLoop.put("ACTUALPRICE", strPrice);

      dbGrossTotal = dbTotalItemIGST + dbTotalItemCGST + dbTotalItemSGST + dbTotal;

      DecimalFormat df = new DecimalFormat("####");
      String strFormattedTotal = df.format(Math.abs(dbGrossTotal));
      strNumInWords = GmCommonClass.convertToWords(Integer.valueOf(strFormattedTotal));
      strNumInWords = dbRoundedGrossTotal < 0 ? " Minus" + strNumInWords : strNumInWords;

      HashMap hmCurrency = new HashMap();
      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getCompId());
      strCurrency = GmCommonClass.parseNull((String) hmCurrency.get("CMPCURRSMB"));

      hmReturn.put("GROSSTOTAL", dbGrossTotal);
      hmReturn.put("TOTALIGST", dbTotalItemIGST);
      hmReturn.put("TOTALCGST", dbTotalItemCGST);
      hmReturn.put("TOTALSGST", dbTotalItemSGST);
      hmReturn.put("IGST", strIGSTPER.equals("") ? "-" : strIGSTPER);
      hmReturn.put("CGST", strCGSTPER.equals("") ? "-" : strCGSTPER);
      hmReturn.put("SGST", strSGSTPER.equals("") ? "-" : strSGSTPER);

      dbRoundedGrossTotal = Math.round(dbGrossTotal);
      hmReturn.put("ROUNDEDGROSSTOTAL", dbRoundedGrossTotal);
      hmReturn.put("ROUNDOFFAMOUNT", Math.round(dbGrossTotal) - dbGrossTotal);
      hmReturn.put("CURRSIGN", strCurrency);
      hmReturn.put("NETTOTAL", dbTotal);
      hmReturn.put("GRANDTOTALINWORDS", strNumInWords);
    }


    if (strInvComments.equalsIgnoreCase("")) {
      hmReturn.put("COMMENTS", strOrderComments);
    } else {
      hmReturn.put("COMMENTS", strInvComments);
    }

    hmReturn.put("RECIPIENTLABEL", strPrintPurposelbl);
    hmReturn.put("INVTITLE", "Stock Transfer Invoice");
    hmOutData.putAll(hmReturn);
    return hmOutData;
  }

  /**
   * loadCompanyDetails - This method will be used to fetch snapshot info for request and
   * consignment .
   * 
   * @param
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadCompanyDetails(String strConsignId) throws AppError {

    HashMap hmCompDtls = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT T520.C1900_COMPANY_ID REQ_COMP_ID, t504.C1900_COMPANY_ID CONS_COMP_ID, GET_RULE_VALUE_BY_COMPANY('GMCOMPANYGST','INVOICE_ATTRIB',T520.C1900_COMPANY_ID) GSTNUM, ");
    sbQuery
        .append(" GET_RULE_VALUE_BY_COMPANY('GMCOMPANYCIN','INVOICE_ATTRIB',t504.C1900_COMPANY_ID) GMCOMPANYCIN, GET_RULE_VALUE_BY_COMPANY('GMCOMPANYPAN','INVOICE_ATTRIB',t504.C1900_COMPANY_ID) GMCOMPANYPAN, ");
    sbQuery
        .append(" GET_RULE_VALUE_BY_COMPANY('GMCOMPANYGST','INVOICE_ATTRIB',t504.C1900_COMPANY_ID) GMCOMPANYGST ");
    sbQuery.append(" FROM T504_CONSIGNMENT t504,T520_REQUEST t520 ");
    sbQuery.append(" WHERE T504.C520_REQUEST_ID=T520.C520_REQUEST_ID ");
    sbQuery.append(" AND C504_CONSIGNMENT_ID='" + strConsignId + "'");

    hmCompDtls = gmDBManager.querySingleRecord(sbQuery.toString());

    return hmCompDtls;
  }

  /**
   * fetchReleasedSTTrans - This method will be used to fetch release stock transfer transactions
   * request.
   * 
   * @param
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchReleasedSTTrans(String strRequestId) throws AppError {

    ArrayList alReleasedTransDtls = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" Select T504.C504_Consignment_Id Cons_Id,T520.C520_Request_Id Req_Id  ");
    sbQuery
        .append(" ,Gm_Pkg_Op_Request_Summary.Get_Request_Status (T520.C520_Status_Fl) Status_Fl  ");
    sbQuery.append(" ,To_Char(Nvl(T520.C520_Last_Updated_Date, T520.C520_Created_Date),'"
        + getCompDateFmt() + "') Updated_Date  ");
    sbQuery
        .append(" ,get_user_name(Nvl(T520.C520_Last_Updated_By, T520.C520_Created_By)) Updated_By  ");
    sbQuery.append(" FROM t520_request t520, T504_Consignment T504 ");
    sbQuery.append(" WHERE (T520.C520_Request_Id    = '" + strRequestId
        + "' OR T520.C520_Master_Request_Id = '" + strRequestId + "' ) ");
    sbQuery.append(" AND t520.c520_request_id       = t504.c520_request_id(+) ");
    sbQuery.append(" And T504.C504_Void_Fl         Is Null ");
    sbQuery.append(" AND t520.c520_void_fl         IS NULL ");
    sbQuery.append(" ORDER BY Cons_Id ");

    log.debug("Query inside fetchReleasedSTTrans is " + sbQuery.toString());
    alReleasedTransDtls =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alReleasedTransDtls;
  }

  /**
   * loadStockTransferItemDetails - This method will be used to fetch stock transfer invoice item
   * details request.
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadStockTransferItemDetails(String strConsignId) throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append("SELECT  T205.C205_PART_NUMBER_ID PNUM ");
    sbQuery.append(", t205.c205_part_num_desc PDESC");
    sbQuery.append(", NVL(T505.C505_ITEM_QTY,0) IQTY");
    sbQuery.append(", T505.C505_CONTROL_NUMBER CNUM");
    sbQuery.append(", NVL(T505.C505_ITEM_PRICE,0) PRICE ");
    sbQuery.append(", GET_RULE_VALUE_BY_COMPANY(t205.c205_product_family,'HSN_GRP',1009) HSNCODE ");
    sbQuery.append(" FROM    T504_CONSIGNMENT T504 ");
    sbQuery.append(", T505_ITEM_CONSIGNMENT T505 ,t205_part_number t205");
    sbQuery.append(" WHERE  T504.C504_CONSIGNMENT_ID = '");
    sbQuery.append(strConsignId);
    sbQuery.append("' AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID");
    sbQuery.append(" AND T505.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T504.C504_VOID_FL is NULL");
    sbQuery.append(" AND T505.C505_VOID_FL is NULL");
    sbQuery.append(" ORDER BY T505.C205_PART_NUMBER_ID ");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("SETLOAD", alReturn);

    return hmReturn;
  }



}
