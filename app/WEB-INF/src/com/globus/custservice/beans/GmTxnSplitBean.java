package com.globus.custservice.beans;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmTxnSplitBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /*
   * Initialize the Logger Class.This will be removed all place changed with GmDataStoreVO
   * constructor
   */

  public GmTxnSplitBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmTxnSplitBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public HashMap fetchSplitOrderDetails(String strParentOrderID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alProjectIds = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmProjectId = new HashMap();
    HashMap hmOrderSummary = new HashMap();
    HashMap hmReturn = new HashMap();
    String strProjectId = "";
    alProjectIds = fetchRulesLookup("EXVND");
    log.debug("alProjectIds==>" + alProjectIds);

    for (int i = 0; i < alProjectIds.size(); i++) {

      hmProjectId = (HashMap) alProjectIds.get(i);
      strProjectId = GmCommonClass.parseZero((String) hmProjectId.get("ID"));
      gmDBManager.setPrepareString("gm_pkg_op_split.gm_fch_order_details", 4);
      gmDBManager.setString(1, strParentOrderID);
      gmDBManager.setString(2, strProjectId);
      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
      gmDBManager.execute();
      alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
      hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));

      if (alReturn.size() > 0)
        hmOrderSummary.put(strProjectId, alReturn);
      if (!hmReturn.isEmpty())
        hmOrderSummary.put(strProjectId + "Details", hmReturn);
    }
    gmDBManager.close();
    return hmOrderSummary;
  }

  public void checkAndSplitDO(GmDBManager gmDBManager, String strOrderID, String strUserId)
      throws AppError {

    gmDBManager.setPrepareString("gm_pkg_op_split.gm_sav_do_split", 2);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();

    // Will not have a commit as this method is using connection object from different method.

  }
  
/* Adding a new method call to perform posting for ipad order types  Bill only sales Consignment
  PC-2280 Aug 22-2020
*/
 
  public void checkAndUpdatePosting(GmDBManager gmDBManager, String strOrderID, String strUserId)
	      throws AppError {

	    gmDBManager.setPrepareString("gm_pkg_op_split.gm_sav_txn_posting", 2);
	    gmDBManager.setString(1, strOrderID);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.execute();

	    // Will not have a commit as this method is using connection object from different method.

	  }
  
  

  public void sendMailToVendor(ArrayList alOrderSummary, HashMap hmOrderDeatils,
      GmEmailProperties gmEmailProperties, String strJasperName) {

    hmOrderDeatils.put("SUBRAITEMDETAILLIST", alOrderSummary);
    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName(strJasperName);
    jasperMail.setAdditionalParams(hmOrderDeatils);
    jasperMail.setReportData(null);
    jasperMail.setEmailProperties(gmEmailProperties);
    HashMap hmjasperReturn = jasperMail.sendMail();
  }

  public ArrayList fetchRulesLookup(String strRuleGrpId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_split.gm_fch_rules_lookup", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRuleGrpId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    log.debug("Test");
    return alResult;
  }

  public HashMap fetchEmailProperties(String strEmailFileName, String strToEmailId,
      String strOrderId, String strTxtReplace) {

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    GmEmailProperties emailProps = new GmEmailProperties();
    HashMap hmReturn = new HashMap();
    StringTokenizer strEmailtokens = new StringTokenizer(strToEmailId, "^^");
    log.debug("tokens Count==>" + strEmailtokens.countTokens());
    if (strEmailtokens.countTokens() > 1) {
      String strToEmail = strEmailtokens.nextToken();
      emailProps.setRecipients(strToEmail);
      emailProps.setCc(strEmailtokens.nextToken());
    } else {
      emailProps.setRecipients(strToEmailId);
      emailProps.setCc(gmResourceBundleBean.getProperty(strEmailFileName + "."
          + GmEmailProperties.CC));
    }
    emailProps.setSender(gmResourceBundleBean.getProperty(strEmailFileName + "."
        + GmEmailProperties.FROM));
    emailProps.setMimeType(gmResourceBundleBean.getProperty(strEmailFileName + "."
        + GmEmailProperties.MIME_TYPE));
    String strSubject =
        gmResourceBundleBean.getProperty(strEmailFileName + "." + GmEmailProperties.SUBJECT);
    strSubject = GmCommonClass.replaceAll(strSubject, strTxtReplace, strOrderId);
    emailProps.setSubject(strSubject);
    hmReturn.put("EMAILPROPS", emailProps);
    return hmReturn;
  }

  public void checkAndSplitRA(GmDBManager gmDBManager, String strRAID, String strUserId)
      throws AppError {
    log.debug("strOrderID: " + strRAID + " strUserId: " + strUserId);


    gmDBManager.setPrepareString("gm_pkg_op_split.gm_sav_ra_split", 2);
    gmDBManager.setString(1, strRAID);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();

    // Will not have a commit as this method is using connection object from different method.

  }

  public HashMap fetchSplitRADetails(String strParentRAId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alProjectIds = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmProjectId = new HashMap();
    HashMap hmOrderSummary = new HashMap();
    HashMap hmReturn = new HashMap();
    String strProjectId = "";
    alProjectIds = fetchRulesLookup("EXVND");
    log.debug("alProjectIds==>" + alProjectIds);
    for (int i = 0; i < alProjectIds.size(); i++) {
      hmProjectId = (HashMap) alProjectIds.get(i);
      strProjectId = GmCommonClass.parseZero((String) hmProjectId.get("ID"));
      gmDBManager.setPrepareString("gm_pkg_op_split.gm_fch_ra_details", 4);
      gmDBManager.setString(1, strParentRAId);
      gmDBManager.setString(2, strProjectId);
      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
      gmDBManager.execute();
      alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
      hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
      log.debug("Size:" + alReturn.size());
      if (alReturn.size() > 0)
        hmOrderSummary.put(strProjectId, alReturn);
      if (!hmReturn.isEmpty())
        hmOrderSummary.put(strProjectId + "Details", hmReturn);
    }
    gmDBManager.close();
    return hmOrderSummary;
  }

  public void checkAndSplitConsignamnet(GmDBManager gmDBManager, String strRequestID,
      String strUserId) throws AppError {
    log.debug("strRequestID: " + strRequestID + " strUserId: " + strUserId);
    gmDBManager.setPrepareString("gm_pkg_op_split.gm_sav_multi_consg_split", 2);
    gmDBManager.setString(1, strRequestID);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    // Will not have a commit as this method is using connection object from different method.
  }

  public HashMap fetchSplitConsignRequestDetails(String strRequestId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alProjectIds = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmProjectId = new HashMap();
    HashMap hmOrderSummary = new HashMap();
    HashMap hmReturn = new HashMap();
    String strProjectId = "";
    alProjectIds = fetchRulesLookup("EXVND");
    log.debug("alProjectIds==>" + alProjectIds);
    for (int i = 0; i < alProjectIds.size(); i++) {
      hmProjectId = (HashMap) alProjectIds.get(i);
      strProjectId = GmCommonClass.parseZero((String) hmProjectId.get("ID"));
      gmDBManager.setPrepareString("gm_pkg_op_split.gm_fch_consg_req_details", 4);
      gmDBManager.setString(1, strRequestId);
      gmDBManager.setString(2, strProjectId);
      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
      gmDBManager.execute();
      alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
      hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
      gmDBManager.commit();
      log.debug("Size:" + alReturn.size());
      if (alReturn.size() > 0 && !hmReturn.isEmpty())
        hmOrderSummary.put(strProjectId, alReturn);
      if (!hmReturn.isEmpty()) {
        hmOrderSummary.put(strProjectId + "Details", hmReturn);
      }
    }
    return hmOrderSummary;
  }

  public void sendVendorEmail(String strRequestId, String strApplnDateFmt,
      String strJasPerLoaction, GmJasperReport gmJasperReport) throws AppError {
    ArrayList alResult = new ArrayList();
    String strVendorMail = "";
    String strConsgnId = "";
    HashMap hmEmailProps = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTxnSplit = new HashMap();
    hmTxnSplit = fetchSplitConsignRequestDetails(strRequestId);
    Set keySet = hmTxnSplit.keySet();
    for (Iterator keys = keySet.iterator(); keys.hasNext();) {
      String key = (String) keys.next();
      if (!key.contains("Details")) {
        alResult = (ArrayList) hmTxnSplit.get(key);
        hmResult = (HashMap) hmTxnSplit.get(key + "Details");
        hmResult.put("SUBREPORT_DIR", strJasPerLoaction);
        hmResult.put("APPLNDATEFMT", strApplnDateFmt);
        strVendorMail = GmCommonClass.parseNull(GmCommonClass.getRuleValue(key, "VENDOREMAIL"));
        strConsgnId = GmCommonClass.parseNull((String) hmResult.get("RQID"));
        hmEmailProps =
            fetchEmailProperties("GmSplitConsignment", strVendorMail, strConsgnId, "#<RQID>");
        String strFileName =
            createPDFFile(strConsgnId, gmJasperReport, new HashMap(hmResult), new ArrayList(
                alResult), "/GmVendorDeliverSetForm.jasper");
        GmEmailProperties gmEmailProps = (GmEmailProperties) hmEmailProps.get("EMAILPROPS");
        gmEmailProps.setAttachment(strFileName);
        sendMailToVendor(alResult, hmResult, gmEmailProps, "/GmVendorDeliverSetForm.jasper");
        removeFile(strFileName);
      }
    }
  }

  public String createPDFFile(String strDoId, GmJasperReport gmJasperReport,
      HashMap hmOrderSummary, ArrayList alOrderSummary, String strJasperName) throws AppError {
    String fileName = "";
    try {
      hmOrderSummary.put("SUBRAITEMDETAILLIST", alOrderSummary);

      String strFilePath = GmCommonClass.getString("UPLOADHOME");
      fileName = strFilePath + strDoId + ".pdf";
      File newFile = new File(strFilePath);
      if (!newFile.exists()) {
        newFile.mkdir();
      }
      gmJasperReport.setJasperReportName(strJasperName);
      gmJasperReport.setHmReportParameters(hmOrderSummary);
      gmJasperReport.setReportDataList(null);
      gmJasperReport.exportJasperReportToPdf(fileName);
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex.getMessage(), "", 'E');
    }
    return fileName;
  }

  public void removeFile(String strFileName) {
    File newPDFFile = new File(strFileName);
    newPDFFile.delete();
  }

  public void sendVendorEmail(HttpServletRequest request, HttpServletResponse response,
      String strApplnDateFmt, String strOrderId) throws AppError {
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean();
    GmDOBean gmDOBean = new GmDOBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    ArrayList alResult = new ArrayList();
    String strVendorMail = "";
    String strDoId = "";
    String strOrderType = "";
    String strColHeader = "";
    String strLot = "";
    String strHeader = "";
    String strExperiDt = "";
    String strComments = "";
    int intArLength = 0;
    HashMap hmEmailProps = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTxnSplit = new HashMap();

    ArrayList alLog = new ArrayList();
    ArrayList alDOCnum = new ArrayList();
    ArrayList alList = new ArrayList();
    HashMap hmLog = new HashMap();

    alDOCnum = gmDOBean.fetchDOControlNumDetails(strOrderId);
    log.debug("alDOCnum====" + alDOCnum);
    intArLength = alDOCnum.size();
    log.debug("intArLength===" + intArLength);
    if (intArLength > 0) {
      HashMap hmTempLoop = new HashMap();
      HashMap hmLoop = new HashMap();
      String strNextId = "";
      int intCount = 0;

      for (int i = 0; i < intArLength; i++) {
        hmLoop = (HashMap) alDOCnum.get(i);
        strNextId = GmCommonClass.parseNull((String) hmLoop.get("CNUM"));
        log.debug("strNextId===" + strNextId);
        if (strNextId.equals("")) {
          intCount++;
        } else {
          intCount = 0;
          break;
        }
      }
      log.debug("intCount===" + intCount);
      if (intArLength == intCount) {
        hmLoop.put("PNUM", "");
        hmLoop.put("CNUM", "");
        hmLoop.put("IQTY", "");
        hmLoop.put("PDESC", "");
        alList.add(hmLoop);
        log.debug("IF alList===" + alList);
      } else {
        alList.add(hmLoop);
        log.debug("Else alList===" + alList);
      }
    }

    hmTxnSplit = gmTxnSplitBean.fetchSplitOrderDetails(strOrderId);
    Set keySet = hmTxnSplit.keySet();

    alLog = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strOrderId, "1200"));
    if (alLog.size() > 0) {
      hmLog = GmCommonClass.parseNullHashMap((HashMap) alLog.get(0));
      strComments = GmCommonClass.parseNull((String) hmLog.get("COMMENTS"));
    }

    for (Iterator keys = keySet.iterator(); keys.hasNext();) {
      String key = (String) keys.next();
      if (!key.contains("Details")) {
        alResult = (ArrayList) hmTxnSplit.get(key);
        hmResult = (HashMap) hmTxnSplit.get(key + "Details");
        hmResult.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        hmResult.put("APPLNDATEFMT", strApplnDateFmt);
        strVendorMail = GmCommonClass.parseNull(GmCommonClass.getRuleValue(key, "VENDOREMAIL"));
        strDoId = GmCommonClass.parseNull((String) hmResult.get("ID"));
        strOrderType = GmCommonClass.parseNull((String) hmResult.get("ORDERTYPE"));
        if (strOrderType.equals("2532")) {
          hmResult.put("EMAILTEXT",
              GmCommonClass.parseNull(GmCommonClass.getString("VENDORORDERDTEXT2")));
          strColHeader = GmCommonClass.parseNull(GmCommonClass.getString("VENDOR_HEADER1"));
          strHeader = GmCommonClass.parseNull(GmCommonClass.getString("VENDORORDERHEAD1"));
        } else {
          hmResult.put("EMAILTEXT",
              GmCommonClass.parseNull(GmCommonClass.getString("VENDORORDERDTEXT1")));
          strColHeader = GmCommonClass.parseNull(GmCommonClass.getString("VENDOR_HEADER2"));
          strHeader = GmCommonClass.parseNull(GmCommonClass.getString("VENDORORDERHEAD2"));
          strLot = GmCommonClass.parseNull(GmCommonClass.getString("VENDORORDLOT"));
          strExperiDt = GmCommonClass.parseNull(GmCommonClass.getString("VENDOREXPERIDT"));
        }
        hmResult.put("COLHEADER", strColHeader);
        hmResult.put("HEADER", strHeader);
        hmResult.put("COMMENTS", strComments);
        hmResult.put("LOT", strLot);
        hmResult.put("EXPERIDATE", strExperiDt);
        hmResult.put("SUBDOCNUMLIST", alList);
        hmEmailProps =
            gmTxnSplitBean.fetchEmailProperties("GmSplitOrder", strVendorMail, strDoId, "#<ORDID>");
        GmJasperReport gmJasperReport = new GmJasperReport();
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        String strFileName =
            gmTxnSplitBean.createPDFFile(strDoId, gmJasperReport, new HashMap(hmResult),
                new ArrayList(alResult), "/GmVendorDeliverOrderForm.jasper");
        GmEmailProperties gmEmailProps = (GmEmailProperties) hmEmailProps.get("EMAILPROPS");
        gmEmailProps.setAttachment(strFileName);
        gmTxnSplitBean.sendMailToVendor(alResult, hmResult, gmEmailProps,
            "/GmVendorDeliverOrderForm.jasper");
        gmTxnSplitBean.removeFile(strFileName);
      }
    }
  }
}
