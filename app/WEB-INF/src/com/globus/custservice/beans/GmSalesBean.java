/*
 * Module: GmSalesBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11
 * Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmParentRepAccountBean;
import com.globus.common.db.GmDBManager;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.pricing.beans.GmPricingBatchBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesBean extends GmSalesFilterConditionBean

{


  GmCommonClass gmCommon = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  // this will be removed all place changed with GmDataStoreVO constructor

  public GmSalesBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  ArrayList alRegion = new ArrayList();
  ArrayList alState = new ArrayList();
  ArrayList alCountry = new ArrayList();
  ArrayList alDevClass = new ArrayList();

  /**
   * reportPartNumPricing - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportPartNumPricing(String strColumn, String strProjId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());



    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    HashMap hmTemp = new HashMap();
    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301"); // Filter all approved projects

    if (strProjId.equals("0")) {
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      alReturn = gmProject.reportProject(hmTemp);
    } else {


      gmDBManager.setPrepareString("GM_REPORT_PARTNUMBER_PRICING", 4);

      /*
       * register out parameter and set input parameters
       */
      gmDBManager.setString(1, strColumn);
      gmDBManager.setString(2, strProjId);
      gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
      gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

      gmDBManager.execute();
      strBeanMsg = gmDBManager.getString(3);
      rs = (ResultSet) gmDBManager.getObject(4);
      alReturn = gmDBManager.returnArrayList(rs);
      gmDBManager.close();

    }
    return alReturn;
  }

  /**
   * loadPartNumPricing - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPartNumPricing(String strId) throws AppError {
    HashMap hmReturn = new HashMap();
    String strCompanyID = getCompId();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T205.C205_PART_NUMBER_ID ID, T2052.C2052_EQUITY_PRICE EPRICE, T2052.C2052_LOANER_PRICE LPRICE, ");
    sbQuery
        .append(" T205.C205_PART_NUM_DESC PDESC, T2052.C2052_CONSIGNMENT_PRICE CPRICE, T2052.C2052_LIST_PRICE LIPRICE  ");
    sbQuery.append(" FROM T205_PART_NUMBER T205,T2052_PART_PRICE_MAPPING T2052");
    sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID = '");
    sbQuery.append(strId);
    sbQuery.append("'");
    sbQuery.append(" AND T205.C205_PART_NUMBER_ID  = T2052.C205_PART_NUMBER_ID(+) ");
    sbQuery.append(" AND T2052.C1900_COMPANY_ID(+) = '");
    sbQuery.append(strCompanyID);
    sbQuery.append("'");
    sbQuery.append(" AND t2052.c2052_void_fl(+) is null ");
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("PARTDETAILS", hmResult);


    return hmReturn;
  } // End of loadPartNumPricing

  /**
   * savePartNumPricing - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String savePartNumPricing(HashMap hmParam, String strPartNum, String strUserId)
      throws AppError {

    String strMsg = "";

    String strEPrice = (String) hmParam.get("EPrice");
    String strLPrice = (String) hmParam.get("LPrice");
    String strCPrice = (String) hmParam.get("CPrice");
    String strLiPrice = (String) hmParam.get("LiPrice");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_SAVE_PARTNUMBER_PRICE", 6);

    gmDBManager.setDouble(1, Double.parseDouble(strEPrice));
    gmDBManager.setDouble(2, Double.parseDouble(strLPrice));
    gmDBManager.setDouble(3, Double.parseDouble(strCPrice));
    gmDBManager.setDouble(4, Double.parseDouble(strLiPrice));
    gmDBManager.setString(5, strPartNum);
    gmDBManager.setString(6, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();
    return strMsg;
  } // end of savePartNumPricing

  /**
   * loadAccountPricing - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadAccountPricing() throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301"); // Filter all approved projects

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      ArrayList alAccountList = reportAccount();
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
      ArrayList alProjectList = gmProject.reportProject(hmTemp);
      ArrayList alSystemList = gmPricingRequestBean.loadSystemsList();

      hmReturn.put("ACCOUNTLIST", alAccountList);
      hmReturn.put("PROJLIST", alProjectList);
      hmReturn.put("SYSTEMLIST", alSystemList);

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesBean:loadAccountPricing", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadAccountPricing

  /**
   * reportAccount - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportAccount() throws AppError {
	String StrAcctType=""; 
log.debug("StrAcctType11========== "+StrAcctType);
	return reportAccount(StrAcctType);
  }
  public ArrayList reportAccount(String StrAcctType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("StrAcctType========== "+StrAcctType);

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;



    gmDBManager.setPrepareString("GM_REPORT_ACCOUNT", 3);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(3, StrAcctType);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    strBeanMsg = gmDBManager.getString(1);
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    log.debug("alReturn---------------- "+alReturn);
    return alReturn;
  } // End of reportAccount
  
  public ArrayList reportAccountCodeName(String StrCodeGrp) throws AppError { 
	  StringBuffer sbQuery = new StringBuffer();
	  ArrayList alResult = new ArrayList();
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  
	  log.debug("alReturn---------enty------- ");
	  sbQuery
	  		.append("SELECT c901_code_id CODEID,c901_code_nm CODENM ");
	  sbQuery
	  		.append( " FROM t901_code_lookup");
	  sbQuery
		.append(" WHERE c901_code_grp = '"+StrCodeGrp+"' AND c901_code_id   IN "); 
	  sbQuery.append(" (");
	  sbQuery.append(" SELECT c906_rule_value FROM t906_rules WHERE c906_rule_grp_id = 'ACT_TYP' AND c906_void_fl IS NULL");
	  sbQuery.append(" )");
	  sbQuery.append(" AND c901_active_fl = '1' AND c901_void_fl  IS NULL");
	   log.debug("sbQuery " + sbQuery.toString());
	   alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
	  
	   log.debug("hmReturn " + alResult);
  return alResult;
  }
  
  public ArrayList fetchReportAccount(String StrAcctType) throws AppError { 
	  StringBuffer sbQuery = new StringBuffer();
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strBeanMsg = "";
	    String strMsg = "";
	    String strPrepareString = null;

	    ArrayList alReturn = new ArrayList();
	    ResultSet rs = null;



	    gmDBManager.setPrepareString("GM_REPORT_ACCOUNT", 3);
	    /*
	     * register out parameter and set input parameters
	     */
	    gmDBManager.setString(3, StrAcctType);
	    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

	    gmDBManager.execute();
	    strBeanMsg = gmDBManager.getString(1);
	    rs = (ResultSet) gmDBManager.getObject(2);
	    alReturn = gmDBManager.returnArrayList(rs);
	    gmDBManager.close();
	    log.debug("alReturn---------------- "+alReturn);
	    return alReturn;
	  } // End of reportAccount

  
  /**
   * reportAccountPricing - This method is used to fetch detail of the saved account price during
   * batch update of price for an account
   * 
   * @see last modified by PMT-8227
   * @param hmParam the hm param
   * @return the hash map
   * @throws AppError the app error
   */
  public HashMap reportAccountPricing(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdCurrconv = null;
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strProjId = GmCommonClass.parseNull((String) hmParam.get("PROJID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strQueryBy = GmCommonClass.parseNull((String) hmParam.get("QUERYBY"));
    String strLikeOption = GmCommonClass.parseNull((String) hmParam.get("LIKE_OPTION"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strVoidFl = GmCommonClass.parseNull((String) hmParam.get("VOIDFL"));

    strProjId = strProjId.equals("") ? "0" : strProjId;

    if (strLikeOption.equals("90181")) {
      strPartNum = ("^").concat(strPartNum);
      strPartNum = strPartNum.replaceAll(",", "|^");
    } else if (strLikeOption.equals("90182")) {
      strPartNum = strPartNum.replaceAll(",", "\\$|");
      strPartNum = strPartNum.concat("$");
    } else {
      strPartNum = strPartNum.replaceAll(",", "|");
    }

    log.debug("strProjId: " + strProjId);
    log.debug("strAccId: " + strAccId);
    log.debug("strType: " + strType);
    log.debug("strVoidFl: " + strVoidFl);

    HashMap hmReturn = new HashMap();


    StringBuffer sbQuery = new StringBuffer();

    // sbQuery.append(" SELECT * FROM (");
    sbQuery
        .append("SELECT  t705.c205_part_number_id ID, decode(0,0,'','') grouptype, t705.c705_unit_price price, t205.C205_PART_NUM_DESC pdesc, t705.c705_discount_offered disc,");
    sbQuery
        .append(" t705.c705_account_pricing_id apid, t705.c705_created_date cdate,  t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME cuser , t705.c705_last_updated_date ldate, ");
    sbQuery
        .append(" get_user_name(t705.c705_last_updated_by) luser, t705.c705_history_fl hisfl,1 type ");
    sbQuery.append(" ,1019 audit_id,t7500.c7500_account_price_request_id price_req_id ,t705.c705_void_fl void_fl");
    if (strQueryBy.equals("System Name")) {
      sbQuery
          .append(" FROM t705_account_pricing t705, T205_PART_NUMBER t205,t101_user t101,t207_set_master t207, t208_set_details t208, t7500_account_price_request t7500, t7501_account_price_req_dtl t7501 ");
    } else {
      sbQuery
          .append(" FROM t705_account_pricing t705, T205_PART_NUMBER t205,t101_user t101, t7500_account_price_request t7500, t7501_account_price_req_dtl t7501 ");
      if (!strProjId.equals("0")) {
        sbQuery
            .append(" ,(SELECT c205_part_number_id, c202_project_id FROM t2021_part_project_mapping ");
        sbQuery.append("  WHERE  C202_PROJECT_ID IN ('");
        sbQuery.append(strProjId);
        sbQuery.append("')");
        sbQuery.append(" AND c2021_void_fl  IS NULL ");
        sbQuery.append(" )t2021 ");
      }
    }
    sbQuery.append(" WHERE");
    if (strType.equals("Account")) {
      sbQuery.append(" t705.c101_party_id = ");
      sbQuery.append(" (SELECT c101_party_id FROM t704_account WHERE c704_account_id ='");
      sbQuery.append(strAccId);
      sbQuery.append("')");
    } else {
      sbQuery.append(" t705.c101_party_id =");
      sbQuery.append(strAccId);
    }
    sbQuery.append(" AND t705.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND t705.C705_CREATED_BY     = t101.C101_USER_ID ");
    if (!strProjId.equals("0")) {
      if (strQueryBy.equals("System Name")) {
        sbQuery.append(" AND T207.C207_SET_ID IN ('");
        sbQuery.append(strProjId);
        sbQuery.append("')");
      } else {
        sbQuery.append(" AND (T2021.C202_PROJECT_ID IN ('");
        sbQuery.append(strProjId);
        sbQuery.append("')");
        sbQuery.append(" OR T705.C202_PROJECT_ID IN ('");
        sbQuery.append(strProjId);
        sbQuery.append("'))");
        sbQuery.append(" AND T705.c205_part_number_id  = t2021.c205_part_number_id(+) ");
      }
    }
    if (strQueryBy.equals("System Name")) {
      sbQuery.append(" AND t207.c901_set_grp_type   = 1600 ");
      sbQuery.append(" AND T207.C207_SET_ID         = T208.C207_SET_ID ");
      sbQuery.append(" AND T208.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID ");
    }
    if (strType.equals("Account") && !strPartNum.equals("")) {
    	sbQuery.append(" AND REGEXP_LIKE (T205.C205_PART_NUMBER_ID,REGEXP_REPLACE('" + strPartNum + "','[+]','\\+'))");
    	
    }
    sbQuery
        .append(" AND t705.c7501_account_price_req_dtl_id = t7501.c7501_account_price_req_dtl_id(+) ");
    sbQuery
        .append(" AND t7501.c7500_account_price_request_id = t7500.c7500_account_price_request_id(+) ");
    sbQuery.append(" AND t7500.c7500_void_fl IS NULL ");
    sbQuery.append(" AND t7501.c7501_void_fl IS NULL ");
    if(!strVoidFl.equals("80130")) {
    	sbQuery.append(" AND t705.c705_void_fl is null ");
    }else {
    	sbQuery.append(" AND t705.c705_void_fl = 'Y' ");
    }
    
    // Removed code related to Group pricing
    sbQuery.append(" ORDER BY type,id ");
    log.debug(" sbQuery.toString() " + sbQuery.toString());
    rdCurrconv = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    ArrayList alReturnList = (ArrayList) rdCurrconv.getRows();
    hmReturn.put("REPORT", alReturnList);
    return hmReturn;

  } // End of reportAccountPricing

  /**
   * batchLoadAccountPricing - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String batchLoadAccountPricing(String strAccId, String strProjId, String strUserId)
      throws AppError {


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;



    gmDBManager.setPrepareString("GM_LOAD_ACCOUNT_PRICING", 4);

    gmDBManager.setInt(1, Integer.parseInt(strAccId));
    // csmt.setString(1,strAccId);
    gmDBManager.setString(2, strProjId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);

    gmDBManager.execute();
    strBeanMsg = gmDBManager.getString(4);
    gmDBManager.close();


    return strBeanMsg;
  } // End of batchLoadAccountPricing

  /**
   * loadAccPartNumPricing - This method will fetch Account price based on Account and Price
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadAccPartNumPricing(HashMap hmParam) throws AppError {
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    log.debug("strAccId " + strAccId);
    log.debug("strPartNum " + strPartNum);
    log.debug("strType " + strType);
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T705.C205_PART_NUMBER_ID ID, nvl(T705.C705_UNIT_PRICE,0) PRICE, nvl(T705.C705_DISCOUNT_OFFERED,0) DISC, ");
    sbQuery.append(" t205.C205_PART_NUM_DESC PDESC ");
    sbQuery.append(" FROM T705_ACCOUNT_PRICING T705, t205_part_number t205 ");
    sbQuery.append(" WHERE T705.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID  ");
    if (strType.equals("Account")) {
      sbQuery.append(" AND T705.c101_party_id = ");
      sbQuery.append(" (SELECT c101_party_id FROM t704_account WHERE c704_account_id ='");
    } else {
      sbQuery.append(" AND T705.C101_PARTY_ID IN ( '");
    }
    sbQuery.append(strAccId);
    sbQuery.append("') AND T705.C205_PART_NUMBER_ID ='");

    sbQuery.append(strPartNum);
    sbQuery.append("'");
    log.debug("sbQuery " + sbQuery.toString());
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

    hmReturn.put("ACCPARTDETAILS", hmResult);

    return hmReturn;
  } // End of loadAccPartNumPricing

  /**
   * saveAccPartNumPricing - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String saveAccPartNumPricing(HashMap hmParam) throws AppError, Exception {
    GmPricingBatchBean gmPricingBatchBean = new GmPricingBatchBean(getGmDataStoreVO());
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo"));
    String strPrice = (String) hmParam.get("Price");
    String strDiscount = (String) hmParam.get("Discount");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_SAVE_ACCOUNT_PARTNUM_PRICE", 6);

    gmDBManager.setDouble(1, Double.parseDouble(strPrice));
    gmDBManager.setDouble(2, Double.parseDouble(strDiscount));
    gmDBManager.setString(3, strPartNum);
    gmDBManager.setString(4, strAccId);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strType);

    gmDBManager.execute();

    gmDBManager.commit();

    // create a record in t9600_batch and JMS
    HashMap hmParams = new HashMap();
    hmParam.put("BATCHTYPE", "18753");
    String strPartyId = "";
    if (strType.equals("Account")) {
      GmParentRepAccountBean gmParentRepAccountBean =
          new GmParentRepAccountBean(getGmDataStoreVO());
      strPartyId = gmParentRepAccountBean.fetchPartyId(strAccId);
      hmParam.put("REFID", strPartyId);
    } else {
      hmParam.put("REFID", strAccId); // Party Id is passed as strAccId in this bean
    }
    hmParam.put("COMPANYINFO", strCompanyInfo);
    String strBatchId = gmPricingBatchBean.updateImplemetedPrice(hmParam);
    return strPartNum;

  } // end of saveAccPartNumPricing

  /**
   * reportDistributorAccount - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportDistributorAccount(HashMap hmParam) throws AppError {
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("OPT"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("ACCESS"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("SALESFILTER"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    if (strOpt.equals("ALL")) {
      sbQuery
          .append(" SELECT	T703.C701_DISTRIBUTOR_ID DID, GET_DISTRIBUTOR_NAME(T703.C701_DISTRIBUTOR_ID) DNAME, ");
      sbQuery
          .append(" T704.C704_ACCOUNT_ID ID, DECODE('"
              + getComplangid()
              + "','103097',NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM)  NAME, T704.c704_ship_add1|| ''||c704_ship_add2 SHIPPINGADDRESS,"); //103097:English
      sbQuery.append(" T704.C704_SHIP_ZIP_CODE SHIP_ZIP_CODE ,T703.C703_SALES_REP_ID REPID, ");
      sbQuery
          .append(" DECODE('"
              + getComplangid()
              + "','103097',NVL(T703.C703_SALES_REP_NAME_EN,T703.C703_SALES_REP_NAME),T703.C703_SALES_REP_NAME) REPNAME, t704.c704_SHIP_CITY || ', ' || GET_CODE_NAME_ALT(t704.c704_SHIP_STATE) LOC, get_party_name(c101_dealer_id)  DEALERNAME,t704.c101_dealer_id DEALERID");
      sbQuery
          .append(" , GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE(T704.C704_ACCOUNT_ID,103050)) COLLECTOR ,DECODE('"
              + getComplangid()
              + "','103097',NVL(t101.C101_PARTY_NM_EN,t101.c101_party_nm),t101.c101_party_nm)  PARENTACCTNAME, T704.C704_ACCOUNT_SH_NAME ASHRTNAME , T704.C704_CUSTOMER_REF_ID CUSTREFID"); // 103050
      // -
      // Collector
      // Attribute
      sbQuery
          .append(" FROM T704_ACCOUNT T704, T703_SALES_REP T703 , T740_GPO_ACCOUNT_MAPPING T740 ,t101_party t101 ");
      sbQuery.append(" , (SELECT DISTINCT ac_id FROM v700_territory_mapping_detail v700 ");
      if (!strSalesFilter.equals("")) {
        sbQuery.append(" WHERE ");
        sbQuery.append(strSalesFilter);
      }
      sbQuery.append(" ) v700 ");
      sbQuery
          .append(" WHERE T703.C703_SALES_REP_ID <> 00 AND T704.C704_ACCOUNT_ID <> '00' AND T704.C704_ACCOUNT_ID <> '01' ");
      sbQuery.append(" AND t704.c101_party_id = t101.c101_party_id ");
      sbQuery.append(" AND t704.C704_ACCOUNT_ID = v700.AC_ID ");
      sbQuery.append(" AND T704.C703_SALES_REP_ID = T703.C703_SALES_REP_ID ");
      sbQuery.append(" AND t704.C704_ACCOUNT_ID = t740.C704_ACCOUNT_ID(+) ");
      sbQuery.append(" AND t740.C740_VOID_FL (+) is null ");
      sbQuery.append(" AND t704.c1900_company_id = '" + getCompId() + "' ");
      if (!strDistId.equals("")) {
        sbQuery.append(" AND T703.C701_DISTRIBUTOR_ID = ");
        sbQuery.append(strDistId);
      }

      if (!strGpoId.equals("") && !strGpoId.equals("0")) {
        sbQuery.append(" AND t740.C101_PARTY_ID = ");
        sbQuery.append(strGpoId);
      }

      sbQuery.append(" ORDER BY DNAME, C704_ACCOUNT_NM ");

    } else if (strOpt.equals("REPALL")) {
      sbQuery
          .append(" SELECT\tGET_DISTRIBUTOR_ID(C703_SALES_REP_ID) DID, GET_DISTRIBUTOR_NAME(GET_DISTRIBUTOR_ID(C703_SALES_REP_ID)) DNAME, ");
      sbQuery
          .append(" C703_SALES_REP_ID ID, GET_REP_NAME(C703_SALES_REP_ID) NAME, C703_SHIP_CITY  || ', ' || GET_CODE_NAME_ALT(C703_SHIP_STATE) LOC, ");
      sbQuery
          .append(" C703_PHONE_NUMBER PHONE , C703_PAGER_NUMBER PAGER , C703_EMAIL_ID EMAILID, C703_ACTIVE_FL AFL");
      sbQuery
          .append(" ,get_code_name_alt(C901_DESIGNATION) DESGN, C703_START_DATE STARTDT, C703_END_DATE ENDDT ");
      sbQuery.append(" FROM T703_SALES_REP T703");
      sbQuery.append(" , (SELECT DISTINCT rep_id FROM v700_territory_mapping_detail v700 ");
      if (!strSalesFilter.equals("")) {
        sbQuery.append(" WHERE ");
        sbQuery.append(strSalesFilter);
      }
      sbQuery.append(" ) v700 ");
      sbQuery.append(" WHERE C703_SALES_REP_ID <> 00 AND C703_ACTIVE_FL = 'Y'  ");
      sbQuery.append(" AND T703.C703_SALES_REP_ID = V700.REP_ID ");
      sbQuery.append(" AND T703.C1900_COMPANY_ID = '" + getCompId() + "' ");

      if (!strDistId.equals("")) {
        sbQuery.append(" AND C701_DISTRIBUTOR_ID = ");
        sbQuery.append(strDistId);
      }

      sbQuery.append(" ORDER BY DNAME , NAME ");
    } else {
      sbQuery.append(" SELECT  T701.C701_DISTRIBUTOR_ID DID ");
      sbQuery.append(" ,T701.C701_DISTRIBUTOR_NAME DNAME ");
      sbQuery.append(" ,T703.C703_SALES_REP_ID ");
      sbQuery.append(" ,T703.C703_SALES_REP_NAME REPNAME ");
      sbQuery.append(" ,T704.C704_ACCOUNT_ID ID ");
      sbQuery.append(" ,T704.C704_ACCOUNT_NM NAME ");
      sbQuery
          .append(" ,T704.C704_BILL_CITY || ', ' || GET_CODE_NAME_ALT(T704.C704_BILL_STATE) LOC ");
      sbQuery.append(" ,GET_ACCOUNT_REP_DAY_SALES(T704.C704_ACCOUNT_ID,T703.C703_SALES_REP_ID, '"
          + strCurrType + "' ) DAYSALES ");
      sbQuery.append(" ,GET_ACCOUNT_REP_MONTH_SALES(T704.C704_ACCOUNT_ID,T703.C703_SALES_REP_ID, '"
          + strCurrType + "' ) MONSALES ");
      sbQuery
          .append(" ,GET_ACCT_REP_LAST_MONTH_SALES(T704.C704_ACCOUNT_ID,T703.C703_SALES_REP_ID, '"
              + strCurrType + "' ) LASTMONSALES ");
      sbQuery
          .append(" , GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE(T704.C704_ACCOUNT_ID,103050)) COLLECTOR "); // 103050
                                                                                                        // -
                                                                                                        // Collector
                                                                                                        // Attribute
      sbQuery.append(" FROM 	T701_DISTRIBUTOR T701 ");
      sbQuery.append(" ,T703_SALES_REP T703 ");
      sbQuery.append(" ,T704_ACCOUNT T704 ");
      sbQuery
          .append(" ,( SELECT DISTINCT C704_ACCOUNT_ID, C703_SALES_REP_ID FROM T501_ORDER T501 ");
      sbQuery.append(" WHERE  T501.C501_ORDER_DATE  >= ADD_MONTHS(TRUNC(CURRENT_DATE,'Mon') ,-1) ");
      sbQuery.append(" AND 	T501.C501_ORDER_DATE  < CURRENT_DATE ");
      sbQuery.append(getSalesFilterClause());
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append("  AND t501.c1900_company_id ='" + getCompId() + "'");
      sbQuery.append(" ) TEMP_TABLE_1 ");
      sbQuery.append(" WHERE 	T701.C701_DISTRIBUTOR_ID 	   = T703.C701_DISTRIBUTOR_ID ");
      sbQuery.append(" AND 	TEMP_TABLE_1.C703_SALES_REP_ID = T703.C703_SALES_REP_ID  ");
      sbQuery.append(" AND	TEMP_TABLE_1.C704_ACCOUNT_ID   = T704.C704_ACCOUNT_ID  ");
      sbQuery.append(" AND t704.c1900_company_id = '" + getCompId() + "' ");
      sbQuery
          .append(" ORDER BY T701.C701_DISTRIBUTOR_NAME, T704.C704_ACCOUNT_NM, T703.C703_SALES_REP_NAME ");
    }

    log.debug(" Query for reportDistributorAccount " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;

  } // End of reportDistributorAccount

  /**
   * reportAccountByType - This method will return arrayList of the account
   * 
   * @param String strAccountType
   * @exception AppError
   */
  public ArrayList reportAccountByType(String strAccountType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_op_fch_account_bytype", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAccountType);

    gmDBManager.execute();

    ArrayList hmReturnList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturnList;
  }

  /**
   * saveBatchAcctPricing - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveBatchAcctPricing(HashMap hmParam) throws AppError, Exception {
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo"));
    GmPricingBatchBean gmPricingBatchBean = new GmPricingBatchBean(getGmDataStoreVO());

    log.debug("strAccId " + strAccId);
    log.debug("strInputStr " + strInputStr);
    log.debug("strUserId " + strUserId);
    log.debug("strType " + strType);
    HashMap hmReturn = new HashMap();
    String strinvalidPartNum = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_SAVE_BATCH_ACCT_PRICING", 6);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strType);

    gmDBManager.execute();
    RowSetDynaClass rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
    strinvalidPartNum = GmCommonClass.parseNull(gmDBManager.getString(6));

    hmReturn.put("REPORT", rdResult.getRows());

    log.debug("strinvalidPartNum " + strinvalidPartNum);
    if (!strinvalidPartNum.equals("")) {
      gmDBManager.close();
      throw new AppError(
          "The following part number(s) are invalid parts: ( "
              + strinvalidPartNum
              + " ).  Please remove the part number(s) mentioned from your excel in order to save.  For parts unable to be loaded, please check with the Project Manager.",
          "", 'E');
    }
    gmDBManager.commit();

    // create a record in t9600_batch and JMS
    HashMap hmParams = new HashMap();
    hmParam.put("BATCHTYPE", "18753");
    String strPartyId = "";
    if (strType.equals("Account")) {
      GmParentRepAccountBean gmParentRepAccountBean =
          new GmParentRepAccountBean(getGmDataStoreVO());
      strPartyId = gmParentRepAccountBean.fetchPartyId(strAccId);
      hmParam.put("REFID", strPartyId);
    } else {
      hmParam.put("REFID", strAccId); // Party Id is passed as strAccId in this bean
    }
    hmParam.put("COMPANYINFO", strCompanyInfo);
    String strBatchId = gmPricingBatchBean.updateImplemetedPrice(hmParam);
    return hmReturn;

  } // end of saveBatchAcctPricing

  /**
   * reportGPOConstructList -
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportGPOConstructList(HashMap hmParam) throws AppError {
    String strPartyID = GmCommonClass.parseNull((String) hmParam.get("PID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT c741_construct_id CID, c741_construct_nm CNAME ");
    sbQuery.append(" , c101_party_id PID, c741_construct_ref_id CREFID");
    sbQuery.append(" , c741_construct_desc CDESC , c741_construct_value CVALUE ");
    sbQuery.append(" FROM t741_construct ");

    if (!strPartyID.equals("")) {
      sbQuery.append(" WHERE c101_party_id = ");
      sbQuery.append(strPartyID);
    }
    sbQuery.append(" ORDER BY c741_construct_nm");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;

  } // End of reportGPOConstructList

  /**
   * fetchAccountsReport - This method will return arrayList of the account report
   * 
   * @param HashMap
   * 
   * @exception AppError
   */
  public ArrayList fetchAccountsReport(HashMap hmParam) throws AppError {

    String strAccId = GmCommonClass.parseZero((String) hmParam.get("ACCID"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("SALESFILTER"));
    String strParentFl = GmCommonClass.parseNull((String) hmParam.get("PARENTFL"));
    String strAccCurrId = GmCommonClass.parseZero((String) hmParam.get("ACC_CURR_ID"));
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T703.C701_DISTRIBUTOR_ID DID, DECODE('"
            + getComplangid()
            + "','103097',NVL(T701.C701_DISTRIBUTOR_NAME_EN,T701.C701_DISTRIBUTOR_NAME),T701.C701_DISTRIBUTOR_NAME) DNAME, T704.C704_ACCOUNT_ID ID ");
    sbQuery
        .append(" , DECODE('"
            + getComplangid()
            + "','103097',NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM) NAME, T704.C704_SHIP_ADD1|| ''||C704_SHIP_ADD2 SHIPPINGADDRESS, T704.C704_SHIP_ZIP_CODE SHIP_ZIP_CODE ");
    sbQuery
        .append(" , T703.C703_SALES_REP_ID REPID, DECODE('"
            + getComplangid()
            + "','103097',NVL(T703.C703_SALES_REP_NAME_EN,T703.C703_SALES_REP_NAME),T703.C703_SALES_REP_NAME) REPNAME ");
    sbQuery
        .append(" , T704.C704_SHIP_CITY || ', ' || GET_CODE_NAME_ALT (T704.C704_SHIP_STATE) LOC ");
    sbQuery.append(" , GET_CODE_NAME(T704A.COLLECTORID) COLLECTOR ");
    sbQuery.append(" , GET_CODE_NAME (T704A. CREDITTYPE) CREDITTYPE ");
    sbQuery.append(" ,T704A.CREDITLIMIT CREDITLIMIT ");
    sbQuery.append(" ,V700.VP_NAME,V700.AD_NAME,ASSREP.ASSOCREPNM ");
    sbQuery
    .append(" ,t101.c101_party_nm partentacctnm  ,get_account_curr_symb(T704.C704_ACCOUNT_ID) ARCURRSYM, T704.C704_ACCOUNT_SH_NAME ASHRTNAME , T704.C704_CUSTOMER_REF_ID CUSTREFID");
    sbQuery.append(" ,get_code_name(t704.c901_account_type) ACCT_TYPE ");
    sbQuery
        .append(" FROM T704_ACCOUNT T704, T703_SALES_REP T703, T701_DISTRIBUTOR T701,t101_party  t101 ");

    sbQuery
        .append("    ,  (SELECT RTRIM (XMLAGG (XMLELEMENT (N, T703.C703_SALES_REP_NAME || ',')) .EXTRACT ('//text()'), ', ') ASSOCREPNM, ");
    sbQuery.append("     t704.c704_account_id acctid ");
    sbQuery
        .append("    FROM t704a_account_rep_mapping t704a, t704_account t704, t703_sales_rep t703 ");
    sbQuery.append(" WHERE T704A.C704_ACCOUNT_ID   = T704.C704_ACCOUNT_ID ");
    sbQuery.append("     AND t704a.c703_sales_rep_id = T703.C703_SALES_REP_ID ");
    sbQuery.append("     AND T704A.C704A_VOID_FL    IS NULL ");
    sbQuery.append(" AND T704.C704_VOID_FL      IS NULL ");
    sbQuery.append(" GROUP BY t704.c704_account_id)assrep ");

    sbQuery.append(" ,(SELECT t704A.C704_ACCOUNT_ID ");
    sbQuery
        .append("   , MAX (DECODE (C901_ATTRIBUTE_TYPE, 103050,C704A_ATTRIBUTE_VALUE))  COLLECTORID ");
    sbQuery
        .append("   , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101182', C704A_ATTRIBUTE_VALUE)) CREDITLIMIT ");
    sbQuery
        .append("   , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101184', C704A_ATTRIBUTE_VALUE)) CREDITTYPE ");
    sbQuery.append("  FROM t704a_account_attribute t704a ");
    sbQuery.append("  WHERE c704a_void_fl  IS NULL ");
    // The below rule entry condition is added as part of PMT-8430 to resolve time out issue
    sbQuery
        .append(" AND C901_ATTRIBUTE_TYPE IN (SELECT t906.c906_rule_value FROM t906_rules t906 ");
    sbQuery
        .append(" WHERE t906.c906_rule_grp_id = 'ACCNT_ATTR' AND c906_rule_id = 'INCLUDE_ATTR') ");
    sbQuery.append("  GROUP BY C704_ACCOUNT_ID )t704a ");

    sbQuery
        .append(" , ( SELECT DISTINCT ac_id , VP_NAME, AD_NAME  FROM v700_territory_mapping_detail v700");
    if (!strSalesFilter.equals("")) {
      sbQuery.append(" WHERE ");
      sbQuery.append(strSalesFilter);
    }
    sbQuery.append(" ) v700 ");
    sbQuery.append(" WHERE T703.C703_SALES_REP_ID <> 00 ");
    sbQuery.append(" AND T704.C704_ACCOUNT_ID   <> '00' ");
    sbQuery.append(" AND T704.C704_ACCOUNT_ID   <> '01' ");
    sbQuery.append(" AND t704.C704_ACCOUNT_ID    = v700.AC_ID ");
    sbQuery.append(" AND t704.C704_ACCOUNT_ID    = assrep.ACCTID(+) ");
    sbQuery.append(" AND t704.C704_ACCOUNT_ID     = t704A.C704_ACCOUNT_ID(+) ");
    sbQuery.append(" AND T704.C703_SALES_REP_ID  = T703.C703_SALES_REP_ID ");
    sbQuery.append(" AND T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID ");
    sbQuery.append(" AND t704.c101_party_id = t101.c101_party_id  ");
    // If show parent account check box is checked,All rep accounts that belongs to Parent account
    // should needs to show.
    if (!strAccId.equals("") && !strAccId.equals("0") && !strParentFl.equals("")) {
      sbQuery
          .append(" AND T704.c101_party_id  In (SELECT c101_party_id from t704_account where c704_account_id ="
              + strAccId + ") ");
    }
    if (!strAccCurrId.equals("")) {
      sbQuery.append(" AND t704.c901_currency = " + strAccCurrId);
    }
    sbQuery.append(" AND t704.c1900_company_id = " + getCompId() + "");
    if (!strAccId.equals("0") && strParentFl.equals("")) {
      sbQuery.append(" AND T704.C704_ACCOUNT_ID = '");
      sbQuery.append(strAccId);
      sbQuery.append("' ");
    }
    sbQuery.append("  Order By UPPER(Dname), UPPER(C704_Account_Nm) ");

    log.debug(" Query for fetchAccountsReport " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alReturn;
  }// end of fetchAccountsReport

  /**
   * savePartNumPricingUpdatation - method to save/update the price values for both scenarios[By
   * project/BatchUpdate]
   * 
   * @param HashMap
   * 
   * @return HashMap
   * 
   * @exception AppError
   */
  public HashMap savePartNumPricingUpdatation(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strProjID = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPSTR"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("HMODE"));
    String strUploadParts = "";

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("gm_pkg_pd_part_pricing.gm_save_part_num_price", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strProjID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserID);
    gmDBManager.setString(4, strMode);

    gmDBManager.execute();
    strUploadParts = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.commit();
    log.debug(" upload parts --> " + strUploadParts);
    hmReturn.put("BATCH_UPLOAD_PARTS", strUploadParts);
    return hmReturn;
  }// end of savePartNumPricingUpdatation method

  /**
   * fetchUploadBatchParts - method to fetch the part number details (Batch upload screen).
   * 
   * @param HashMap
   * 
   * @return Array List
   * 
   * @exception AppError
   */
  public ArrayList fetchUploadBatchParts(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();
    String strProjID = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
    String strParts = GmCommonClass.parseNull((String) hmParam.get("BATCH_UPLOAD_PARTS"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_part_pricing.gm_fch_upload_batch_parts", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strProjID);
    gmDBManager.setString(2, strParts);
    gmDBManager.setString(3, strUserID);

    gmDBManager.execute();
    RowSetDynaClass rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) rdResult.getRows());

    return alResult;
  }// end of fetchUploadBatchParts method

  /**
   * Get Validate Account - This method will be used to check account Id.
   * 
   * @param String string -  Contain the Account ID to fetch the details.
   * @return String strflag - Having the Account Id Status.
   * @exception AppError
   */
  public String checkAccountDtls(String strAccId) throws AppError{ 
    String strFlag = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ar_accounts_rpt.gm_check_account", 2);
    gmDBManager.setString(1, strAccId);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strFlag = gmDBManager.getString(2);
    gmDBManager.close();
    return strFlag;
  }


}// end of class GmSalesBean



