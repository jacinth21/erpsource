package com.globus.custservice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

/**
 * GmOrderUsageLotReportBean - This is fetch the Order Lot Details and Usage LotDetails
 * @author rajan
 *
 */
public class GmOrderUsageLotReportBean extends GmBean {
	
	/**
	 * GmOrderUsageLotReportBean - Constructor
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmOrderUsageLotReportBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	/**
	 *  This is logger properties
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * fetchOrderLotDetails - this method will Fetch orders UsageLot Details
	 * 
	 * @param hmParam
	 * @return ArrayList
	 * @throws AppError
	 */
	public ArrayList fetchOrderLotDetails (HashMap hmParam) throws AppError{
		
		ArrayList alResult = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String strDealerId =  GmCommonClass.parseNull((String) hmParam.get("DEALER"));
		String strAccountId =  GmCommonClass.parseNull((String) hmParam.get("ACCOUNT"));
		String strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
		String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
		String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
		String strMissingLot = GmCommonClass.parseNull((String) hmParam.get("MISSINGLOT"));
	    String strPartnumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
	    String strFieldSales = GmCommonClass.parseNull((String) hmParam.get("FIELDSALES"));
	    String strSalesRep =  GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
	    String strProject =  GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
	    String strLotNumber =  GmCommonClass.parseNull((String) hmParam.get("LOTNUMBER"));
	    
		String strLangId = GmCommonClass.parseNull(getGmDataStoreVO().getCmplangid());
		String strComId =  getGmDataStoreVO().getCmpid();
	    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
	
		sbQuery.append(" select t501.c501_order_id ORDID,t502b.PARTNUM,t205.c205_part_num_desc PARTDESC,");
		sbQuery.append("  t501.c501_order_date ORDDATE,t501.c501_surgery_date SURGERYDATE,t502b.LOTNUM,t502b.USAGEQTY,");
		sbQuery.append(" DECODE('"+ strLangId + "','103097',NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM) ACCOUNTNAME,");
		sbQuery.append(" DECODE('"+ strLangId + "','103097',NVL(t101.c101_party_nm_en,t101.c101_party_nm),t101.c101_party_nm) DEALERNAME,");
		sbQuery.append(" get_order_part_qty(t501.c501_order_id,t502b.PARTNUM) ORDQTY ,");
		sbQuery.append(" get_code_name(NVL(T501.C901_ORDER_TYPE,'2521')) ORDERTYPEDESC ,");
		sbQuery.append(" get_user_name(t501.c501_created_by) CREATEDBY ,");
		sbQuery.append(" t501.c501_created_date CREATEDDATE");
		sbQuery.append(", NVL(C701_DISTRIBUTOR_NAME_EN,C701_DISTRIBUTOR_NAME) DISTNM ");
		sbQuery.append(", NVL(C703_SALES_REP_NAME_EN,C703_SALES_REP_NAME) REPNM ");	
		sbQuery.append(" FROM t501_order t501, t704_account t704, t101_party t101, t205_part_number t205,(");
		sbQuery.append(" SELECT NVL(t501.c501_parent_order_id,t501.c501_order_id) ORDID, t502b.c205_part_number_id PARTNUM,");
		sbQuery.append(" SUM(t502b.c502b_item_qty) USAGEQTY, t502b.c502b_usage_lot_num LOTNUM ");
		sbQuery.append(" FROM t501_order t501, t502b_item_order_usage t502b ");
		sbQuery.append(" WHERE t501.c501_order_id = t502b.c501_order_id ");
		sbQuery.append(" AND (t501.c501_order_id       = t502b.c501_order_id ");
		sbQuery.append(" OR t501.c501_parent_order_id  = t502b.c501_order_id )");  
		//Exclude the Duplicate and Price Adjustment order
		sbQuery.append(" AND NVL(t501.c901_order_type,-999) NOT IN ( ");
		sbQuery.append(" SELECT t906.c906_rule_value ");
	    sbQuery.append(" FROM t906_rules t906 ");
	    sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPELOT' ");
	    sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
		sbQuery.append(" AND t502b.c502b_void_fl  IS NULL");
		sbQuery.append(" AND t501.c501_void_fl  IS NULL ");
		//if from date not empty
		if(!strFromDt.equals("")&& (!strToDt.equals(""))){
		       if (strDateType.equals("106480")) {
		    	sbQuery.append(" AND t501.c501_order_date between to_date('" +strFromDt + "' ,'"+strDateFormat +"') and to_date('" +strToDt + "' ,'"+strDateFormat +"')");
		       }
		       else if (strDateType.equals("106481")) {
		    	sbQuery.append(" AND t501.c501_surgery_date  between to_date('" +strFromDt + "' ,'"+strDateFormat +"') and to_date('" +strToDt + "','"+strDateFormat +"')");
		       }
		    }
		sbQuery.append(" AND t501.c1900_company_id='"+ strComId +"'");
		sbQuery.append(" GROUP BY NVL(t501.c501_parent_order_id,t501.c501_order_id), t502b.c205_part_number_id,t502b.c502b_usage_lot_num) t502b");
		
		sbQuery.append(", t701_distributor t701 ");	
		if(!strProject.equals("")) {
		sbQuery.append(", t202_project t202 ");	
		}
		sbQuery.append(", t703_sales_rep t703 ");	
		sbQuery.append(" WHERE t501.c501_order_id  = t502b.ORDID");
		sbQuery.append(" AND t501.c704_account_id     = t704.c704_account_id"); 
		sbQuery.append(" AND t501.c101_dealer_id      = t101.c101_party_id(+)");
		sbQuery.append(" AND t205.c205_part_number_id = t502b.PARTNUM");
		sbQuery.append(" AND t501.c501_distributor_id = t701.c701_distributor_id ");
		sbQuery.append(" AND t701.C701_VOID_FL IS NULL ");
		sbQuery.append(" AND t501.c703_sales_rep_id = t703.c703_sales_rep_id ");
		sbQuery.append(" AND t703.C703_VOID_FL IS NULL ");
		if(!strProject.equals("")) {
			 sbQuery.append(" AND t205.c202_project_id = t202.c202_project_id ");
			 sbQuery.append(" AND t202.C202_VOID_FL IS NULL ");
		}
		sbQuery.append(" AND t101.c101_void_fl(+)    IS NULL");
	    sbQuery.append(" AND t704.c704_void_fl       IS NULL ");
	    sbQuery.append(" AND t502b.USAGEQTY <> 0 ");
	   
	    
		//if Account id not empty
	    if(!strAccountId.equals("")){ 
	    	 sbQuery.append(" AND t501.c704_account_id= ");
	         sbQuery.append(strAccountId);	
	    }
	    //if part number not equal to empty
	    if (!strPartnumber.equals("")) {
	           sbQuery.append(" AND regexp_like (t502b.PARTNUM, '" + strPartnumber + "') ");
        }
	   //if dealer id not empty
	    if(!strDealerId.equals("")){
	    	 sbQuery.append(" AND t501.c101_dealer_id= ");
	         sbQuery.append(strDealerId);	
	    }
          //if missing lot equal to checked
	    if(!strMissingLot.equals("") ){
	    	sbQuery.append(" AND t502b.LOTNUM is null");
	    } 
	    if(!strLotNumber.equals("")) {
	        sbQuery.append(" AND UPPER(t502b.LOTNUM) LIKE ('%");
		    sbQuery.append(strLotNumber.toUpperCase());
		    sbQuery.append("%')");
	    }
	    if(!strSalesRep.equals("")) {
	    	sbQuery.append(" AND t501.c703_sales_rep_id ='"+strSalesRep+"'");
	    	
	    }
	    if(!strFieldSales.equals("")) {
	    	sbQuery.append(" AND t501.c501_distributor_id ='"+strFieldSales+"'");
	    }
	    if(!strProject.equals("")) {
	    	sbQuery.append(" AND t202.c202_project_id ='"+strProject+"'");
	    }
	    sbQuery.append(" AND t501.c1900_company_id='"+ strComId +"'");
	    sbQuery.append(" ORDER BY ORDDATE DESC,ACCOUNTNAME,ORDID,t502b.PARTNUM,t502b.LOTNUM ASC");
	    log.debug("alResult======= " + sbQuery.toString());
	    alResult = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
		return alResult;
	
	}
	

	/**generateOutput - this method will load xml String 
	 * 
	 * @param alGridData
	 * @param hmDtls
	 * @return
	 * @throws AppError
	 */
	public String generateOutput(ArrayList alGridData, HashMap hmDtls) throws AppError {
	    GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
	    String strTemplateName = GmCommonClass.parseNull((String) hmDtls.get("TEMPLATE_NM"));
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) hmDtls.get("STRSESSCOMPANYLOCALE"));
	   String strLabelProperties = GmCommonClass.parseNull((String) hmDtls.get("LABEL_PROPERTIES_NM"));
	    gmTemplateUtil.setDataList("alGridData", alGridData);
	    gmTemplateUtil.setDataMap("hmApplnParam", hmDtls);
	    gmTemplateUtil.setTemplateName(strTemplateName);
	    gmTemplateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strLabelProperties,
	      strSessCompanyLocale));
	    gmTemplateUtil.setTemplateSubDir("custservice/templates");
	    return gmTemplateUtil.generateOutput();
	  }
	
	/**
	 * fetchOrderUsageLotByPart - this method will fetch usage lot number
	 * @param strOrderId, strPartNumber
	 * @return
	 * @throws AppError
	 */
	public String fetchOrderUsageLotByPart (String strOrderId, String strPartNumber, String strInvoiceId) throws AppError{
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmDORptBean gmDORptBean = new GmDORptBean (getGmDataStoreVO());
		// to get the orders ids.
		String strAllOrders = GmCommonClass.parseNull(gmDORptBean.getAllOrderIds(strOrderId));
		// to replace the single quote
		strAllOrders = strAllOrders.replaceAll("'", "");
        String strOrderUsageLotStr = "";
	    gmDBManager.setPrepareString("gm_pkg_cs_ord_usage_lot_rpt.gm_fch_order_used_lot_number_by_part", 4);
		gmDBManager.registerOutParameter(4,OracleTypes.CLOB);
		gmDBManager.setString(1, strAllOrders);
		gmDBManager.setString(2, strPartNumber);
		gmDBManager.setString(3, strInvoiceId);
		gmDBManager.execute();
		strOrderUsageLotStr = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();
     log.debug(" strOrderUsageLotStr ==> "+strOrderUsageLotStr);
    return strOrderUsageLotStr;
	}
}
