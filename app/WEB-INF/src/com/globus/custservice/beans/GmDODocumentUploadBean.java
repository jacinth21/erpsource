package com.globus.custservice.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * This class used to fetch/update the DO document information.
 * 
 * @author mmuthusamy
 *
 */
public class GmDODocumentUploadBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger


  /**
   * Cons - method
   * 
   * @param gmDataStoreVO
   * @throws AppError
   */
  public GmDODocumentUploadBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * validateDO - Before upload the document validate the DO
   * 
   * @param hmParam
   * @throws AppError
   */
  public void validateDO(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("STRORDERID"));
    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_validate_do", 1);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.execute();
    gmDBManager.close();

  }


  /**
   * updateDODocumentUploadFlag - Method used to update DO document flag to order table
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public void updateDODocumentUploadFlag(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("STRORDERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUploadFl = GmCommonClass.parseNull((String) hmParam.get("DO_DOC_UPLOAD_FL"));

    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_sav_do_document_fl", 3);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strUploadFl);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }
}
