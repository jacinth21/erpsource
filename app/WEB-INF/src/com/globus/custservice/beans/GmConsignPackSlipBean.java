package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.sql.SQLException;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.requests.beans.GmRequestReportBean;

public class GmConsignPackSlipBean implements GmConsignPackSLipInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
	
	
	public HashMap loadSavedSetMaster(String strConsignId) throws AppError{
		HashMap hmParam = new HashMap();
		hmParam.put("CONSIGNID", strConsignId);
		return loadSavedSetMaster(hmParam);
	}
		
	
	/**
 	  * loadSavedSetMaster - This method will 
 	  * @param String strUsername
	  * @param String strPassword
 	  * @return HashMap
	  * @exception AppError
	**/
	public HashMap loadSavedSetMaster(HashMap hmParam)  throws AppError{
		ArrayList alReturn = new ArrayList();
		HashMap hmReturn = new HashMap();
		HashMap hmResult = new HashMap();
		HashMap hmTemp = new HashMap();
		
				GmDBManager gmDBManager = new GmDBManager();
				StringBuffer sbQuery = new StringBuffer();
				String strConsignId = GmCommonClass.parseNull((String)hmParam.get("CONSIGNID"));
				String strRemVoidFl = GmCommonClass.parseNull((String)hmParam.get("REMVOIDFL"));
								
				sbQuery.append(" select t505.c205_part_number_id PRODUCTNO ");
				sbQuery.append(" ,get_partnum_desc(t505.c205_part_number_id) PRODDESCRPTION ");
				sbQuery.append(" ,to_char(t2550.c2550_expiry_date,get_rule_value('DATEFMT','DATEFORMAT')) PRODEXPIRY ");
				sbQuery.append(" ,t505.c505_control_number ALLOGRAFTNO ");
				sbQuery.append(" ,get_part_size(t505.c205_part_number_id,t2550.c2550_control_number) PRODSIZE ");
				sbQuery.append(" ,t2550.c2540_donor_number PRODDONOR ");
				sbQuery.append(" , NVL (T505.C505_ITEM_QTY, 0) IQTY ");
				sbQuery.append(" , NVL (T505.C505_ITEM_PRICE, 0) PRICE ");
				sbQuery.append(" from ");
				sbQuery.append(" (select t2550.c205_part_number_id ");
				sbQuery.append(" ,t2550.c2550_control_number ");
				sbQuery.append(" ,t2550.c2550_expiry_date ");
				sbQuery.append(" ,t2550.c2550_custom_size ");
				sbQuery.append(" ,t2540.c2540_donor_number ");
				sbQuery.append(" ,t2550.c2540_donor_id ");
				sbQuery.append(" from t2550_part_control_number t2550 ,t2540_donor_master t2540 ");
				sbQuery.append(" where NVL(t2550.c2540_donor_id,-999) = t2540.c2540_donor_id (+) ");
				sbQuery.append(" and t2550.c205_part_number_id in ( select t505.c205_part_number_id ");
		        sbQuery.append(" from t505_item_consignment t505  where  t505.c504_consignment_id = '"+strConsignId+"') ) t2550,t505_item_consignment t505 ");
		        sbQuery.append(" where t505.c504_consignment_id = '"+strConsignId+"' ");
		        sbQuery.append(" and t505.c205_part_number_id  = t2550.c205_part_number_id (+) ");
		        sbQuery.append(" and t505.c505_control_number = t2550.c2550_control_number (+) ");
		        sbQuery.append(" and t505.c505_void_fl IS NULL  ORDER BY PRODUCTNO , ALLOGRAFTNO ");
				
		        log.debug(" SETLOAD query " + sbQuery.toString());
				alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
				hmReturn.put("SETLOAD",alReturn);
				
				  if( alReturn.size() == 0){
	                   throw new AppError(new SQLException("Error Occured",null,20072));	                      
	              }	
					  
               String strSetId = "";//(String)hmResult.get("SETID"); todo Commented on May 08th..  Need to verify this later
               
               // Adding the SetId to the return HashMap. This is done for Rollback Set CSG.
               // The Rollback button would be displayed in GmConsignShip.jsp only if the SetId is not null
               // hmReturn.put("SETID",strSetId);
               
				if (!strSetId.equals("")){
					sbQuery.setLength(0);
					sbQuery.append(" SELECT	C.C205_PART_NUMBER_ID , GET_PARTNUM_DESC(C.C205_PART_NUMBER_ID), C.C208_SET_QTY, 'Add to Set' ");
					sbQuery.append(" FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C  ");
					sbQuery.append(" WHERE A.C207_SET_ID = '");
					sbQuery.append(strSetId);
					sbQuery.append("' AND A.C207_SET_ID = C.C207_SET_ID AND A.C504_CONSIGNMENT_ID = '");
					sbQuery.append(strConsignId);
					sbQuery.append("' AND C.C208_SET_QTY <> 0 AND C208_INSET_FL = 'Y' ");
                    sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
					sbQuery.append(" AND C.C205_PART_NUMBER_ID NOT IN (SELECT  C205_PART_NUMBER_ID FROM ");
					sbQuery.append(" T505_ITEM_CONSIGNMENT WHERE C504_CONSIGNMENT_ID = '");
					sbQuery.append(strConsignId);
					sbQuery.append("')");
					sbQuery.append("UNION");
					sbQuery.append(" SELECT C205_PART_NUMBER_ID, GET_PARTNUM_DESC(C205_PART_NUMBER_ID), C505_ITEM_QTY , 'Remove from Set' ");
					sbQuery.append(" FROM T505_ITEM_CONSIGNMENT WHERE C504_CONSIGNMENT_ID ='");
					sbQuery.append(strConsignId);
					sbQuery.append("' AND C205_PART_NUMBER_ID IN ( SELECT C.C205_PART_NUMBER_ID");
					sbQuery.append(" FROM  T504_CONSIGNMENT A, T208_SET_DETAILS C ");
					sbQuery.append(" WHERE A.C207_SET_ID = '");
					sbQuery.append(strSetId);
					sbQuery.append("' AND A.C207_SET_ID = C.C207_SET_ID AND A.C504_CONSIGNMENT_ID = '");
					sbQuery.append(strConsignId);
					sbQuery.append("' AND C.C208_SET_QTY = 0 AND A.C504_VOID_FL IS NULL )");

                    log.debug(" MISSINGPARTS query " + sbQuery.toString());             
					hmReturn.put("MISSINGPARTS",new ArrayList());

					if (!strSetId.equals("")){
						sbQuery.setLength(0);
						sbQuery.append(" SELECT C504_CONSIGNMENT_ID CONID ");
						sbQuery.append(" FROM T504_CONSIGNMENT ");
						sbQuery.append(" WHERE C207_SET_ID = '");
						sbQuery.append(strSetId);
						sbQuery.append("' AND C504_STATUS_FL < 2 ");
	                    sbQuery.append(" AND C504_VOID_FL IS NULL ");
                        log.debug(" SIMILARSETS query " + sbQuery.toString());                       
					//	alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString()); Commented on May 08th.. to verify this sql
						hmReturn.put("SIMILARSETS",new ArrayList());		                    
					}
				}		
		return hmReturn;
	} //End of loadSavedSetMaster	
}
