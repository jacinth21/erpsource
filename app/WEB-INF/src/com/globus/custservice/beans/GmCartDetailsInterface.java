package com.globus.custservice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.AppError;

public interface GmCartDetailsInterface {   
    public ArrayList fetchCartDetails(HashMap hmParams) throws AppError;
}