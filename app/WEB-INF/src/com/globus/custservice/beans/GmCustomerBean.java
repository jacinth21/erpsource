/*
 * Module: GmCustomerBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11
 * Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed 03/11/10
 * HKP Subpart with one dot should displayed for part number search
 */

package com.globus.custservice.beans;

import java.sql.ResultSet; 
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.custservice.transfer.beans.GmTransferBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.party.beans.GmNPIProcessBean;
import com.globus.prodmgmnt.beans.GmPartNumSearchBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.sales.pricing.beans.GmPriceRequestRptBean;
import com.globus.custservice.beans.GmOrderTagRecordBean;

public class GmCustomerBean extends GmBean implements GmPlaceOrderInterface {

  GmCommonClass gmCommon = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
  // Initialize
  // the
  // Logger
  // Class.

  ArrayList alRegion = new ArrayList();
  ArrayList alState = new ArrayList();
  
  ArrayList alCountry = new ArrayList();

  ArrayList alDevClass = new ArrayList();

  // this will be removed all place changed with GmDataStoreVO constructor

  public GmCustomerBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCustomerBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * loadEditProject - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadEditProject(String strId) throws AppError {
    HashMap hmReturn = new HashMap();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT  C202_PROJECT_ID ID, C202_PROJECT_NM NAME, C202_DEVICE_CLASS DEVCLASS, ");
      sbQuery.append(" C202_CREATED_BY PMAN, C202_PROJECT_DESC PDESC, C201_IDEA_ID IDEAID ");
      sbQuery.append(" FROM T202_PROJECT ");
      sbQuery.append(" WHERE C202_PROJECT_ID = '");
      sbQuery.append(strId);
      sbQuery.append("'");
      HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmReturn.put("PROJECTDETAILS", hmResult);

      alDevClass = gmCommon.getCodeList("DEVCL");

      hmReturn.put("DEVCLASS", alDevClass);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadEditProject", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadEditProject

  /**
   * reportProject - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportProject(String strColumn) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;

    gmDBManager.setPrepareString("GM_REPORT_PROJECT", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strColumn);

    gmDBManager.execute();
    strBeanMsg = gmDBManager.getString(2);
    rs = (ResultSet) gmDBManager.getObject(3);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();

    return alReturn;
  }

  /**
   * getDistributorList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getDistributorList(HashMap hmParam) throws AppError {
    String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
    String strDistTyp = GmCommonClass.parseNull((String) hmParam.get("DISTTYP"));
    String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));
    String strExclDistTyp = GmCommonClass.parseNull((String) hmParam.get("EXCL_DISTTYP"));
    String strScreenfrm = GmCommonClass.parseNull((String) hmParam.get("SCREEN"));
    ArrayList alReturn = new ArrayList();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT  C701_DISTRIBUTOR_ID ID,DECODE('"
              + getComplangid()
              + "', '103097',NVL(C701_DISTRIBUTOR_NAME_EN, C701_DISTRIBUTOR_NAME), C701_DISTRIBUTOR_NAME) NAME , 'D' || C701_DISTRIBUTOR_ID DID, NVL(t710.c901_division_id,'100823') DDIV , LOWER(C701_DISTRIBUTOR_NAME) distname");
      sbQuery.append(" FROM T701_DISTRIBUTOR ");
      // Instead of function we are getting the division id from master table
      sbQuery.append(" ,(SELECT DISTINCT t710_tmp.c901_area_id, t710_tmp.c901_division_id ");
      sbQuery.append(" FROM t710_sales_hierarchy t710_tmp ");
      sbQuery.append(" WHERE t710_tmp.c710_active_fl = 'Y' ");
      sbQuery.append(" AND t710_tmp.c710_void_fl IS NULL) t710 ");
      if(strScreenfrm.equals("4000338") || strScreenfrm.equals("ReloadRpt")){   //Field Sales
    	  sbQuery.append(" WHERE (C701_VOID_FL  IS NULL OR C701_DISTRIBUTOR_ID = get_rule_value('DISTRIBUTOR','TAGST')) AND (c901_ext_country_id is NULL ");
      }else{
    	  sbQuery.append(" WHERE C701_VOID_FL IS NULL AND (c901_ext_country_id is NULL ");
      }     
      sbQuery.append(" OR c901_ext_country_id  in (select country_id from v901_country_codes))");
      if (strFilter.equals("Active")) {
        sbQuery.append(" AND ");
        sbQuery.append(" C701_ACTIVE_FL = 'Y'");
      }
      if (!strExclDistTyp.equals("")) {
        sbQuery.append(" AND c901_distributor_type NOT IN ( ");
        sbQuery.append(strExclDistTyp);
        sbQuery.append(" ) ");
      }

      if (!strDistTyp.equals("")) {
        sbQuery.append(" AND c901_distributor_type = ");
        sbQuery.append(strDistTyp);
      }
      if (!strShipTyp.equals("")) {
        sbQuery.append(" AND C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120," + strRepID
            + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='" + strRepID
            + "'))) ");

      }
      sbQuery.append(" AND c701_region = t710.c901_area_id (+) ");
      // Filtering Distributor based on all the companies associated with the plant.
      if (strDistFilter.equals("COMPANY-PLANT")) {
        sbQuery
            .append(" AND ( C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = '"
                + getCompPlantId() + "'  AND C5041_VOID_FL IS NULL )");
        sbQuery.append(" OR C1900_COMPANY_ID IS NULL )  ");
        sbQuery
            .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
        sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
            + getCompId() + "') ");

      } else {
        sbQuery.append("  AND ( C1900_COMPANY_ID IS NULL OR C1900_COMPANY_ID = '" + getCompId()
            + "') ");
        sbQuery
            .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
        sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
            + getCompId() + "') ");
      }
      sbQuery.append(" ORDER BY distname ");
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      log.debug(" Query to get the Distributor List is " + sbQuery.toString() + "size of AList "
          + alReturn.size());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:getDistributorList", "Exception is:" + e);
    }
    return alReturn;
  } // End of getDistributorList

  /**
   * getDistributorList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getDistributorList(String strFilter) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("FILTER", strFilter);
    return getDistributorList(hmParam);
  } // End of getDistributorList

  /**
   * getDistributorList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getDistributorList() throws AppError {
    return getDistributorList("");
  } // End of getDistributorList

  /**
   * getRegionDistTerrMapping - This method will
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getRegionDistTerrMapping(String strYear) throws AppError {
    ArrayList alReturn = new ArrayList();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT D.C701_REGION RID, GET_CODE_NAME(D.C701_REGION) RGNAME, D.C701_DISTRIBUTOR_ID DID, D.C701_DISTRIBUTOR_NAME DNAME, ");
      sbQuery
          .append(" A.C702_TERRITORY_ID TID, A.C702_TERRITORY_NAME TNAME, C.C703_SALES_REP_ID REPID, C.C703_SALES_REP_NAME RNAME, ");
      sbQuery.append(" GET_AD_REP_NAME(C.C703_SALES_REP_ID) ADNAME ");
      sbQuery.append(" FROM T702_TERRITORY A, T703_SALES_REP C, T701_DISTRIBUTOR D ");
      sbQuery.append(" WHERE A.C702_TERRITORY_ID = C.C702_TERRITORY_ID ");
      sbQuery.append(" AND C.C701_DISTRIBUTOR_ID = D.C701_DISTRIBUTOR_ID ");
      sbQuery.append(" ORDER BY RGNAME, DNAME, A.C702_TERRITORY_ID ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:getRegionDistTerrMapping", "Exception is:" + e);
    }
    return alReturn;
  } // End of getRegionDistTerrMapping

  /**
   * getRepList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getRepList() throws AppError {
    return getRepList("");
  } // End of getRepList

  public ArrayList getRepList(String strFilter) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("FILTER", strFilter);
    return getRepList(hmParam);
  }

  /**
   * getRepList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getRepList(HashMap hmParam) throws AppError {

    log.debug(" hmParam ::: >>>" + hmParam);

    ArrayList alReturn = new ArrayList();
    String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
    String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strDivID = GmCommonClass.parseNull((String) hmParam.get("DIVID"));
    String strAccountSrc = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTSRC"));
    String strDirectRepType = GmCommonClass.parseNull((String) hmParam.get("DIRECTREPTYPE"));
    String strRepFilter = GmCommonClass.parseNull((String) hmParam.get("REPFILTER"));
    String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));

    /*
     * When Company ID is passed in along with the HashMap then we have to filter the Rep based on
     * the passed in company . Use the Company from Context only when the HashMap does not have the
     * Company ID.
     */
    strCompanyID = strCompanyID.equals("") ? getCompId() : strCompanyID;

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C703_SALES_REP_ID REPID,C703_SALES_REP_ID ID, 'S' || C703_SALES_REP_ID SID, t701.C701_DISTRIBUTOR_ID DID,  DECODE('"
              + getComplangid()
              + "' , '103097',NVL(C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME), C703_SALES_REP_NAME) NAME,  ");
      sbQuery.append(" t701.c701_distributor_name DNAME ");
      sbQuery
          .append(" , T101.C101_PARTY_ID  PARTYID ,  T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNAME  "); // Party
      sbQuery.append(" FROM T703_SALES_REP  T703 , T101_PARTY T101, t701_distributor t701 ");
      sbQuery.append(" WHERE  T703.C101_PARTY_ID = T101.C101_PARTY_ID  AND  C703_VOID_FL IS NULL ");
      sbQuery.append(" AND t703.c701_distributor_id = t701.c701_distributor_id ");
      sbQuery.append(" AND (t701.c701_void_fl       IS NULL  OR t701.C701_DISTRIBUTOR_ID = get_rule_value('DISTRIBUTOR','TAGST')) ");
      if (strDivID.equals("")) {
        // Filtering Distributor based on all the companies associated with the plant.
        if (strRepFilter.equals("COMPANY-PLANT")) {
          sbQuery
              .append(" AND  T703.C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
                  + getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
        } else {
          sbQuery.append(" AND  T703.C1900_COMPANY_ID = " + strCompanyID);
        }
      } else {
        if (!strDivID.equals("")) {
          sbQuery.append(" AND T703.c901_ext_country_id is NOT NULL ");
        } else {
          sbQuery.append(" AND (T703.c901_ext_country_id is NULL ");
          sbQuery
              .append(" OR T703.c901_ext_country_id  in (select country_id from v901_country_codes))");
        }
      }
      if (strFilter.equals("Active")) {
        sbQuery.append(" AND ");
        sbQuery.append(" C703_ACTIVE_FL = 'Y' ");
      }
      if (!strShipTyp.equals("")) {
        sbQuery.append(" AND t701.C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120,"
            + strRepID + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='"
            + strRepID + "'))) ");

      }
      // If Source dropdown is showing in UI, loading Direct SalesReps in Dropdown
      if (!strDirectRepType.equals("")) {
        sbQuery.append(" AND t701.c901_distributor_type IN(" + strDirectRepType + ")");
      }

      sbQuery.append(" ORDER BY UPPER(C703_SALES_REP_NAME) ");
      log.debug(" Query to fetch Rep Details is >>>>  " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:getRepList", "Exception is:" + e);
    }
    return alReturn;
  } // End of getRepList



  /**
   * loadOrderAccount - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOrderAccount(String strId) throws AppError {
    HashMap hmReturn = new HashMap();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT  C704_ACCOUNT_ID ID,  C704_ACCOUNT_NM ANAME, C703_SALES_REP_ID REPID, ");
      sbQuery.append(" GET_DIST_REP_NAME(C703_SALES_REP_ID) REPDISTNM, ");
      sbQuery
          .append(" C704_ACCOUNT_SH_NAME SNAME, C704_CONTACT_PERSON CPERSON, C704_PHONE PHONE, ");
      sbQuery.append(" C704_FAX FAX, C704_PAYMENT_TERMS PTERM, ");
      sbQuery.append(" C704_BILL_NAME BNAME, C704_BILL_ADD1 BADD1, C704_BILL_ADD2 BADD2, ");
      sbQuery
          .append(" C704_BILL_CITY BCITY, GET_CODE_NAME(C704_BILL_STATE) BSTATE, GET_CODE_NAME(C704_BILL_COUNTRY) BCOUNTRY, ");
      sbQuery
          .append(" C704_BILL_ZIP_CODE BZIP, to_char(C704_INCEPTION_DATE,get_rule_value('DATEFMT','DATEFORMAT')) INCDATE,");
      sbQuery
          .append(" to_char(sysdate,get_rule_value('DATEFMT','DATEFORMAT')) CDATE FROM T704_ACCOUNT ");
      sbQuery.append(" WHERE C704_ACCOUNT_ID = '");
      sbQuery.append(strId);
      sbQuery.append("'");

      HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmReturn.put("ACCOUNTDETAILS", hmResult);

      ArrayList alShip = gmCommon.getCodeList("SHIP");
      hmReturn.put("SHIP", alShip);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadOrderAccount", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadOrderAccount

  /**
   * initiateOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap initiateOrder(HashMap hmParam, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());



    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;
    String strOrdId = "";

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();


    String strAccId = (String) hmParam.get("ACCID");
    String strRepId = (String) hmParam.get("REPID");
    String strShipTo = (String) hmParam.get("SHIPTO");
    String strPO = (String) hmParam.get("PO");


    gmDBManager.setPrepareString("GM_INITIATE_ORDER", 8);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);

    gmDBManager.setString(1, strAccId);
    gmDBManager.setInt(2, Integer.parseInt(strRepId));
    gmDBManager.setInt(3, Integer.parseInt(strShipTo));
    gmDBManager.setString(4, strPO);
    gmDBManager.setString(5, strUsername);

    gmDBManager.execute();

    strOrdId = gmDBManager.getString(6);
    strShipTo = gmDBManager.getString(7);
    strMsg = gmDBManager.getString(8);
    gmDBManager.commit();
    hmReturn.put("ORDID", strOrdId);
    hmReturn.put("SHIPADD", strShipTo);
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of initiateOrder

  /**
   * getShipAddress - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String getShipAddress(String strBillTo, String strPerson, String strShipTo,
      String strShipToId) throws AppError {
    String strShipAdd = "";
    HashMap hmResult = new HashMap();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT   GET_SHIP_ADD('");
      sbQuery.append(strShipTo);
      sbQuery.append("','");
      sbQuery.append(strBillTo);
      sbQuery.append("','");
      sbQuery.append(strPerson);
      sbQuery.append("','");
      sbQuery.append(strShipToId);
      sbQuery.append("') SHIPADD FROM DUAL ");

      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      strShipAdd = (String) hmResult.get("SHIPADD");

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:getShipAddress", "Exception is:" + e);
    }
    return strShipAdd;
  } // End of getShipAddress

  // new method to fetch the shipping address when clicked on the address look
  // up button
  public String getAddress(String strShipTo, String strShipToId) throws AppError {
    String strShipAdd = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_cm_shipping_info.get_address", 2);

    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strShipTo);
    gmDBManager.setString(3, strShipToId);
    gmDBManager.execute();
    strShipAdd = gmDBManager.getString(1);
    log.debug("strShipAdd to display is   " + strShipAdd);
    gmDBManager.close();

    return strShipAdd;
  } // End of getShipAddress

  /**
   * loadCart - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadCart(String strPartNum, String strAccId, String strRepId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    StringTokenizer strTok = new StringTokenizer(strPartNum, ",");

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      while (strTok.hasMoreTokens()) {
        strPartNum = strTok.nextToken();
        StringBuffer sbQuery = new StringBuffer();

        sbQuery.append(" SELECT c205_part_number_id ID, c205_part_num_desc PDESC");
        sbQuery.append(" ,get_qty_in_stock(c205_part_number_id) STOCK ");
        sbQuery.append(" ,get_account_part_pricing(");
        sbQuery.append(strAccId);
        sbQuery.append(",'");
        sbQuery.append(strPartNum);
        sbQuery.append("') PRICE ");
        sbQuery.append(" ,get_cs_fch_loaner_status('");
        sbQuery.append(strPartNum);
        sbQuery.append("','");
        sbQuery.append(strRepId);
        sbQuery.append("') LOANERSTATUS ");
        sbQuery.append(" ,c205_list_price LPRICE ");
        sbQuery.append(" FROM t205_part_number ");
        sbQuery.append(" WHERE c205_part_number_id = '");
        sbQuery.append(strPartNum);
        sbQuery.append("'");

        hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
        hmResult.put(strPartNum, hmReturn);

      }
      log.debug("LOAD CART:" + hmResult);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadCart", "Exception is:" + e);
    }
    return hmResult;
  } // End of loadCart

  /**
   * placeOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap placeOrder(HashMap hmOrder, HashMap hmCart, String strPartNums, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();


    String strAccId = (String) hmOrder.get("ACCID");
    String strRepId = (String) hmOrder.get("REPID");
    String strShipTo = (String) hmOrder.get("SHIPTO");
    String strOrdId = (String) hmOrder.get("ORDID");
    String strPO = (String) hmOrder.get("PO");
    strPO = returnCustomerPO(strOrdId, strPO);

    String strTotal = (String) hmOrder.get("TOTAL");
    String strOMode = (String) hmOrder.get("OMODE");
    String strPName = (String) hmOrder.get("PNAME");

    gmDBManager.setPrepareString("GM_PLACE_ORDER", 10);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(10, java.sql.Types.CHAR);

    gmDBManager.setString(1, strOrdId);
    gmDBManager.setString(2, strAccId);
    gmDBManager.setInt(3, Integer.parseInt(strRepId));
    gmDBManager.setInt(4, Integer.parseInt(strShipTo));
    gmDBManager.setString(5, strPO);
    // gmDBManager.setDouble(6,222);
    gmDBManager.setDouble(6, Double.parseDouble(strTotal));
    gmDBManager.setString(7, strUsername);
    gmDBManager.setInt(8, Integer.parseInt(strOMode));
    gmDBManager.setString(9, strPName);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(10);
    gmDBManager.commit();
    placeItemOrder(hmCart, strOrdId, strPartNums, strUsername);

    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of placeOrder

  /**
   * placeItemOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap placeItemOrder(HashMap hmCart, String strOrdId, String strPartNums,
      String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    StringTokenizer strTok = new StringTokenizer(strPartNums, ",");
    String strPartNum = "";
    HashMap hmLoop = new HashMap();
    String strQty = "";
    String strPrice = "";
    int i = 0;



    while (strTok.hasMoreTokens()) {
      strPartNum = strTok.nextToken();
      hmLoop = new HashMap();
      hmLoop = (HashMap) hmCart.get(strPartNum);
      strQty = (String) hmLoop.get("QTY");
      strPrice = (String) hmLoop.get("PRICE");
      gmDBManager.setPrepareString("GM_PLACE_ITEM_ORDER", 5);
      /*
       * register out parameter and set input parameters
       */
      gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

      gmDBManager.setString(1, strOrdId);
      gmDBManager.setString(2, strPartNum);
      gmDBManager.setInt(3, Integer.parseInt(strQty));
      gmDBManager.setDouble(4, Double.parseDouble(strPrice));
      gmDBManager.execute();
      strMsg = gmDBManager.getString(5);
      gmDBManager.commit();
      hmReturn.put("MSG", strMsg);

    } // ENd of WHILE


    return hmReturn;
  } // End of placeItemOrder

  /**
   * saveShipOrderDetails - This method will be used to save the order details Both Order and
   * shipping information
   * 
   * @param HashMap hmParam -- contains entire order information
   * @param String strOrderId
   * @param String strUsername
   * @return HashMap
   * @exception AppError
   */
  @Override
  public String saveOrderDetails(HashMap hmParam, String strOrderId, String strUsername)
      throws AppError {

    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(gmDataStoreVO);

    // CallableStatement gmDBManager = null;

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    String strAccId = "";
    String strMode = "";
    String strPerson = "";
    String strComments = "";
    String strPO = "";
    String strParentOrderId = "";
    String strInvoiceId = "";

    String strShipDate = "";
    String strShipCarrName = "";
    String strSalesRepId = "";
    String strRmQtyStr = "";
    String strBOQtyStr = "";
    String strQuoteStr = "";
    String strCnumStr = "";
    String strHardCpPO = "";
    String strUsedLotStr = "";

    String strRmQtyMsgStr = "";
    String strBOQtyMsgStr = "";

    Date dtSurgDt = null;
    Date dtOrderDt = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    try {

      strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
      strMode = GmCommonClass.parseNull((String) hmParam.get("OMODE"));
      strPerson = GmCommonClass.parseNull((String) hmParam.get("OPERSON"));
      // strShipTo = (String) hmParam.get("SHIPTO");
      // strShipToId = (String) hmParam.get("SHIPID");
      strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
      strPO = GmCommonClass.parseNull((String) hmParam.get("CPO"));
      strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENTORDERID")); // parentid
      strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
      strSalesRepId = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
      strRmQtyStr = GmCommonClass.parseNull((String) hmParam.get("RMQTYSTR"));
      strBOQtyStr = GmCommonClass.parseNull((String) hmParam.get("BOQTYSTR"));
      strQuoteStr = GmCommonClass.parseNull((String) hmParam.get("QUOTESTR"));
      strCnumStr = GmCommonClass.parseNull((String) hmParam.get("CNUMSTR"));
      strUsedLotStr = GmCommonClass.parseNull((String) hmParam.get("USEDLOTSTR"));
      strHardCpPO = GmCommonClass.parseNull((String) hmParam.get("HARDCPPO"));
      String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
      strHardCpPO = (strHardCpPO.equals("on")) ? "Y" : "";
      dtSurgDt = (java.util.Date) hmParam.get("SURGERYDT");
      dtOrderDt = (java.util.Date) hmParam.get("ORDER_DATE");

      gmDBManager.setPrepareString("GM_UPDATE_ORDER", 19);
      gmDBManager.registerOutParameter(18, java.sql.Types.CHAR);
      gmDBManager.registerOutParameter(19, java.sql.Types.CHAR);
      gmDBManager.setString(1, strOrderId);
      gmDBManager.setString(2, strAccId);
      gmDBManager.setString(3, strPO);
      gmDBManager.setString(4, strUsername);
      gmDBManager.setInt(5, Integer.parseInt(strMode));
      gmDBManager.setString(6, strPerson);
      gmDBManager.setString(7, strComments); // Comments and other
      // information
      gmDBManager.setString(8, strParentOrderId);
      gmDBManager.setString(9, strInvoiceId);
      gmDBManager.setString(10, strSalesRepId);
      gmDBManager.setString(11, strRmQtyStr);
      gmDBManager.setString(12, strBOQtyStr);
      gmDBManager.setString(13, strHardCpPO);
      gmDBManager.setString(14, strQuoteStr);
      gmDBManager.setString(15, strUsedLotStr);
      gmDBManager.setDate(16, dtSurgDt);
      gmDBManager.setDate(17, dtOrderDt);// to update order date for Month End Reclassification
      gmDBManager.execute();
      strRmQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(18));
      strBOQtyMsgStr = GmCommonClass.parseNull(gmDBManager.getString(19));
      if (!strCnumStr.equals("")) {
        hmParam.put("INPUTSTR", strCnumStr);
        hmParam.put("ORDERID", strOrderId);
        hmParam.put("USERID", strUsername);
        hmParam.put("SCREEN", "EDIT_ORDER");
        log.debug(" StrCtrlNumberStr >>>>>" + hmParam);
        gmDOBean.updateControlNumberDetail(hmParam, gmDBManager);
      }

      gmDBManager.commit();
      log.debug("Remove String =" + strRmQtyMsgStr);
      log.debug("BackOrder String =" + strBOQtyMsgStr);
      strMsg = strRmQtyMsgStr + "<br>" + strBOQtyMsgStr;
      hmReturn.put("MSG", strMsg);

      // To Save the Insert part number details in New order screen Using JMS
      String strTxnType = "50261";
      HashMap hmJMSParam = new HashMap();
      hmJMSParam.put("TXNID", strOrderId);
      hmJMSParam.put("TXNTYPE", strTxnType);
      hmJMSParam.put("STATUS", "93342");
      hmJMSParam.put("USERID", strUsername);
      hmJMSParam.put("COMPANYINFO", strCompanyInfo);
      gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);

    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmCustomerBean:save Order details ", "Exception is:" + e);
      throw new AppError(e);
    }

    return strMsg;

  } // end of saveShipOrderDetails

  /**
   * updateOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateOrder(String strOrdId, String strTotal, HashMap hmCart, String strPartNums,
      String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_UPDATE_ORDER", 4);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);

    gmDBManager.setString(1, strOrdId);
    gmDBManager.setDouble(2, Double.parseDouble(strTotal));
    gmDBManager.setString(3, strUsername);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(4);
    gmDBManager.commit();

    placeItemOrder(hmCart, strOrdId, strPartNums, strUsername);

    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of updateOrder

  /**
   * reportDashboard - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportDashboard(String strOpt) throws AppError {
    GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmSales = new HashMap();
    ArrayList alReturnControl = new ArrayList();

    GmTransferBean gmTransferBean = new GmTransferBean(getGmDataStoreVO());
    ArrayList alPendingTransfers = new ArrayList();

    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    ArrayList alDummyConsignment = new ArrayList();

    ArrayList alReturnSets = new ArrayList();
    ArrayList alReturns = new ArrayList();
    ArrayList alLoaners = new ArrayList();

    alReturnControl = reportOrdDashboardCtrl(); // Orders Dashboard
    hmReturn.put("CNTRL", alReturnControl);

    hmSales = todaysSales(); // Todays and Month to Date sales figures
    hmReturn.put("SALES", hmSales);

    alReturnSets = reportDashboardSets(); // Sets Dashboard
    hmReturn.put("SETS", alReturnSets);

    if (strOpt.equals("CUST")) {
      alPendingTransfers = gmTransferBean.loadPendingTransfer(); // Return
      // the
      // pending
      // transfers
      hmReturn.put("TRANSFERS", alPendingTransfers);

      // alReturns = gmOper.getReturnsDashboard("CUSTSER"); // Returns
      // Dashboard
      // hmReturn.put("RETURNS",alReturns);

      alDummyConsignment = gmCustomerBean.loadPendingDummyConsignmnet(); // Return
      // the
      // pending
      // dummy
      // consignments
      hmReturn.put("DUMMYCONSG", alDummyConsignment);

      alLoaners = loadLoanerReconDashboard(); // Returns the unreconciled
      // Loaner Transactions
      // (missing transactions
      // 50159)
      hmReturn.put("LOANERS", alLoaners);
    }
    return hmReturn;

  } // ENd of reportDashboard

  /**
   * reportOrdDashboardCtrl - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportOrdDashboardCtrl() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;


    gmDBManager.setPrepareString("GM_REPORT_ORDER_CONTROL", 2);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    strBeanMsg = gmDBManager.getString(1);
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();

    return alReturn;
  } // End of reportOrdDashboardCtrl

  /**
   * reportPendingOrders - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportPendingOrders(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strCondition = GmCommonClass.parseNull((String) hmParam.get("COND"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("OPT"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    Date dtFrom = (Date) hmParam.get("FRMDT");
    Date dtTo = (Date) hmParam.get("TODT");
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strDistType = GmCommonClass.parseNull((String) hmParam.get("DTYPE"));
    String strReportType = GmCommonClass.parseNull((String) hmParam.get("REPORTTYPE"));
    String strOrderType = GmCommonClass.parseNull((String) hmParam.get("ORDERTYPE"));
    String strCollectorId = GmCommonClass.parseZero((String) hmParam.get("COLLECTORID"));
    String strMoreFitlerCompID = GmCommonClass.parseZero((String) hmParam.get("COMPANYID"));
    String strMasterID = GmCommonClass.parseZero((String) hmParam.get("MASTERID"));
    String strCompanyID = getGmDataStoreVO().getCmpid();
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strARCurrId = GmCommonClass.parseNull((String) hmParam.get("ARCURRID"));
    String strACKCurrId = GmCommonClass.parseNull((String) hmParam.get("ACKCURRSYM"));
    String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strARCurrId);

    log.debug(hmParam);
    StringBuffer sbQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    log.debug("strType" + strType);
    // By Dealer 26240245 , 103286 by Parent Account
    if (strReportType.equals("103286") || strReportType.equals("26240245")) {
      sbQuery
          .append(" SELECT t101p.c101_party_id ACCID , DECODE('"
              + getComplangid()
              + "','103097',NVL(t101p.C101_PARTY_NM_EN,t101p.C101_PARTY_NM),t101p.C101_PARTY_NM) NAME ,");

    } else {
      sbQuery
          .append(" SELECT T501.C704_ACCOUNT_ID ACCID, DECODE('"
              + getComplangid()
              + "','103097',NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM) NAME ,"); // 103097:English
    }

    sbQuery
        .append(" T501.C501_ORDER_ID ORDID,t501.C501_DO_DOC_UPLOAD_FL DO_FLAG,t501.c901_order_type ORDTYPE, ");
    sbQuery.append(" T501.C501_ORDER_DATE ORDDT, ");
    sbQuery.append(" T501.C501_ORDER_DATE_TIME ORDDTREPORT, ");
    sbQuery.append(" T101.C101_USER_F_NAME || ' ' || T101.C101_USER_L_NAME  CS, ");
    sbQuery.append(" GET_TOTAL_ORDER_AMT(T501.C501_ORDER_ID,'" + strCurrType + "') COST, ");
    sbQuery.append(" decode(trunc((CURRENT_DATE- T501.C501_ORDER_DATE)/21),0,'','Y') FL ");
    sbQuery
        .append(" ,DECODE('"
            + getComplangid()
            + "','103097',NVL(T703.C703_SALES_REP_NAME_EN,T703.C703_SALES_REP_NAME),T703.C703_SALES_REP_NAME) rname");
    sbQuery.append(", t703.C703_SALES_REP_ID rid");
    sbQuery
        .append(" ,T501.C501_PARENT_ORDER_ID PARENTORDID, DECODE('"
            + getComplangid()
            + "','103097',NVL(t701.c701_distributor_name_en,t701.c701_distributor_name),t701.c701_distributor_name) DNAME, t701.c701_distributor_id DID, T501.C501_CUSTOMER_PO CUSTOMERPO "); // 103097:English
    // AR2 -Added held flag column for PO Email Remainder screen.
    sbQuery
        .append(" ,T501.C501_HOLD_FL HOLD_FL , T704a.Attribute_Name COLLECTOR_NAME,  DECODE(T501.C501_PARENT_ORDER_ID,NULL,'N','Y') CHILD_ORDER_FL "); // AR2
                                                                                                                                                       // -
                                                                                                                                                       // Added
                                                                                                                                                       // held
                                                                                                                                                       // flag
                                                                                                                                                       // column
                                                                                                                                                       // for
                                                                                                                                                       // PO
                                                                                                                                                       // Email
                                                                                                                                                       // Remainder
    sbQuery.append(" ,get_code_name(t704.c901_currency) currsymbol"); // screen.
    sbQuery
        .append(" FROM T501_ORDER T501 , T701_DISTRIBUTOR t701, T703_SALES_REP t703, T704_ACCOUNT T704, T101_USER T101, ");
    sbQuery
        .append(" (SELECT T704a.C704_Account_Id, T704a.C704a_Attribute_Value , T901.C901_Code_nm attribute_name ");
    sbQuery.append(" FROM T704a_Account_Attribute T704a, T901_Code_Lookup t901");
    sbQuery.append(" WHERE T704a.c704a_void_fl is null ");
    sbQuery
        .append(" AND T704a.C704a_Attribute_Value = T901.C901_Code_Id AND T704a.C901_Attribute_Type = '103050') t704a "); // 103050
                                                                                                                          // -
                                                                                                                          // Collector
    // By Dealer 26240245 , 103286 by Parent Account
    if (strReportType.equals("103286") || strReportType.equals("26240245")) {
      sbQuery.append(" ,t101_party t101p ");
    }
    sbQuery.append(" WHERE   T501.C501_STATUS_FL IS NOT NULL");
    // currency filter for account
    if (!strOpt.equals("PROFORMA")) {
      sbQuery.append(" AND T704.C901_CURRENCY ='" + strARCurrId + "'");
    } else if (!strACKCurrId.equals("0") && strOpt.equals("PROFORMA")) {
      sbQuery.append(" AND T704.C901_CURRENCY ='" + strACKCurrId + "'");
    }
    // Commenting the below line due to Credit Memo changes. We would
    // not need to check the Order Status for "Pending PO".
    // An order has Pending PO as long as the C501_CUSTOMER_PO is NULL
    // sbQuery.append(" AND T501.C501_STATUS_FL = 2 ");
    if (strOpt.equals("HELDRPT") || strOrderType.equals("101783")) { // 101783 - Held Order
      sbQuery.append(" AND c501_hold_fl = 'Y' ");
    } else if (strOpt.equals("PROFORMA")) {
      sbQuery.append(" AND T501.C901_order_type = 101260 ");
      sbQuery.append(" AND T501.c501_status_fl <> '3'"); // Shipped
    } else {
      // TO fix, BUG-4998 Order created with PO was not loaded,
      // when the Report Type choosen as 'Order with PO' in Email Remainder report
      if (strOrderType.equals("") || strOrderType.equals("0")) {
        sbQuery.append(" AND C501_CUSTOMER_PO IS NULL ");
      }
      sbQuery.append(" AND C503_INVOICE_ID IS NULL ");
    }

    if (strOrderType.equals("101781")) {
      // Filter for orders with PO
      sbQuery.append(" AND T501.C501_CUSTOMER_PO IS NOT NULL ");
    } else if (strOrderType.equals("101782")) {
      // Filter for orders without PO
      sbQuery.append(" AND T501.C501_CUSTOMER_PO IS NULL ");
    }

    sbQuery.append(" AND t501.C703_SALES_REP_ID = t703.C703_SALES_REP_ID ");
    sbQuery.append(" AND t701.C701_DISTRIBUTOR_ID = t703.C701_DISTRIBUTOR_ID ");
    sbQuery.append(" And T501.C704_Account_Id  = T704a.C704_Account_Id (+) ");
    sbQuery.append(" AND T704.C704_ACCOUNT_ID = T501.C704_ACCOUNT_ID ");
    sbQuery.append(" AND T501.C501_CREATED_BY = T101.C101_USER_ID ");
    sbQuery.append(" AND T501.c1900_company_id = '" + getGmDataStoreVO().getCmpid() + "' ");
    if (strReportType.equals("103286")) {
      sbQuery.append(" AND t704.c101_party_id = t101p.c101_party_id");
      sbQuery.append(" AND t101p.C901_PARTY_TYPE = 4000877");
    }
    // By Dealer 26240245
    if (strReportType.equals("26240245")) {
      sbQuery.append(" AND t501.c101_dealer_id = t101p.c101_party_id");
      sbQuery.append(" AND t101p.C901_PARTY_TYPE = '26230725'");
    }

    if (strReportType.equals("103283") && !strMasterID.equals("") && !strMasterID.equals("0")) // By
                                                                                               // Account
    {
      sbQuery.append(" AND T501.C704_ACCOUNT_ID =  '");
      sbQuery.append(strMasterID);
      sbQuery.append("' ");
    } else if (strReportType.equals("103284") && !strMasterID.equals("")
        && !strMasterID.equals("0")) // By Rep
    {
      sbQuery.append(" AND t703.C703_SALES_REP_ID =  '");
      sbQuery.append(strMasterID);
      sbQuery.append("' ");
    } else if (strReportType.equals("103285") && !strMasterID.equals("")
        && !strMasterID.equals("0")) // By Dist
    {
      sbQuery.append(" AND t701.C701_DISTRIBUTOR_ID =  '");
      sbQuery.append(strMasterID);
      sbQuery.append("' ");
    } else if (strReportType.equals("103286") && !strMasterID.equals("")
        && !strMasterID.equals("0")) { // By Parent Account
      sbQuery.append(" AND  T501.C704_Account_Id IN ");
      sbQuery
          .append(" ( SELECT C704_Account_Id FROM T704_ACCOUNT WHERE C704_VOID_FL IS NULL AND   ");
      sbQuery.append(" C101_PARTY_ID =  ");
      sbQuery.append("'" + strMasterID + "')");
    }

    if (!strCollectorId.equals("0")) {
      sbQuery.append(" AND T704a.C704a_Attribute_Value ='" + strCollectorId + "'");
      // sbQuery.append("strCollectorId");
    }
    sbQuery.append(" AND (t701.c901_ext_country_id is NULL ");
    sbQuery.append(" OR t701.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND (t703.c901_ext_country_id is NULL ");
    sbQuery.append(" OR t703.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND (t501.c901_ext_country_id is NULL ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");

    if (!strDistType.equals("0")) {
      sbQuery.append(" AND t701.C901_DISTRIBUTOR_TYPE = ");
      sbQuery.append(strDistType);
    }

    if (strType.equals("103290")) {// 0-14 days
      sbQuery
          .append(" AND T501.C501_ORDER_DATE BETWEEN trunc(CURRENT_DATE - 14) and trunc(CURRENT_DATE) ");
    } else if (strType.equals("103291")) { // 21 days
      sbQuery
          .append(" AND T501.C501_ORDER_DATE BETWEEN trunc(CURRENT_DATE - 21) and trunc(CURRENT_DATE-15) ");
    } else if (strType.equals("103292")) { // 21x days
      sbQuery.append(" AND T501.C501_ORDER_DATE < trunc(CURRENT_DATE-21) ");
    } else if (strType.equals("0") || strType.equals("103289")) { // When Aging type is All, choose
                                                                  // one need to filter it by To
                                                                  // Date.
      if (dtFrom != null) {
        sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('");
        sbQuery.append(GmCommonClass.getStringFromDate(dtFrom, strDateFmt));
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("' )");
      }
      if (dtTo != null) {
        sbQuery.append(" AND T501.C501_ORDER_DATE <= TO_DATE('");
        sbQuery.append(GmCommonClass.getStringFromDate(dtTo, strDateFmt));
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("' ) ");
      }
    }

    // sbQuery.append(" AND C901_ORDER_TYPE IS NULL ");

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    // PMT-4434 company id added for PO reminder/commission mail screen, From other reports we will
    // be getting this condition via the "strCondition" variable.
    if (!strMoreFitlerCompID.equals("0") && !strMoreFitlerCompID.equals("")) {
      sbQuery
          .append(" AND t501.c704_account_id IN (SELECT ac_id  FROM v700_territory_mapping_detail  WHERE compid = '");
      sbQuery.append(strMoreFitlerCompID);
      sbQuery.append("') ");
    }
    sbQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    if (!strOpt.equals("PROFORMA")) {
      sbQuery.append(" AND nvl(T501.C901_order_type,-9999) <> 2535 ");
    }
    sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
    sbQuery.append(" SELECT t906.c906_rule_value ");
    sbQuery.append(" FROM t906_rules t906 ");
    sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
    sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
    // sbQuery.append(" AND (T501.C901_order_type <> 2535 or t501.c901_order_type is null) ");
    if (strReportType.equals("103284")) { // BYREP
      sbQuery.append(" ORDER BY  RNAME, ORDDTREPORT DESC ");
    } else if (strReportType.equals("103283") || strReportType.equals("103286")
        || strReportType.equals("26240245")) { // BYACC OR PAR ,By Dealer 26240245
      // ACC
      sbQuery.append(" ORDER BY  NAME, ORDDTREPORT DESC ");
    } else {
      sbQuery.append(" ORDER BY  DNAME, ORDDTREPORT DESC ");
    }

    log.debug(" Query for Pending PO is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    hmReturn.put("REPORT", alReturn);
    return hmReturn;
  } // End of reportPendingOrders

  /**
   * reportDashboardSets - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportDashboardSets() throws AppError {
    ArrayList alReturn = new ArrayList();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C504_CONSIGNMENT_ID CID, C504_STATUS_FL,GET_SET_NAME(C207_SET_ID) SNAME,");
      sbQuery.append(" GET_USER_NAME(C504_CREATED_BY) PER, GET_USER_NAME(C504_SHIP_TO_ID) UNAME, ");
      sbQuery.append(" decode(C701_DISTRIBUTOR_ID,'',C704_ACCOUNT_ID,C701_DISTRIBUTOR_ID) DID, ");
      sbQuery
          .append(" decode(C701_DISTRIBUTOR_ID,'',GET_ACCOUNT_NAME(C704_ACCOUNT_ID),GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID)) DNAME, ");
      sbQuery
          .append(" to_char(C504_CREATED_DATE,get_rule_value('DATEFMT','DATEFORMAT') || HH:MI AM') IDATE, C504_COMMENTS COMMENTS , ");
      sbQuery.append(" decode(C504_STATUS_FL,2,'Y','-') CONSFL, C504_SHIP_REQ_FL SHIPREQFL ");
      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE C504_STATUS_FL >= 2 AND C504_STATUS_FL < 4 ");
      sbQuery
          .append(" AND  (C701_DISTRIBUTOR_ID IS NOT NULL OR C704_ACCOUNT_ID = '01') AND C504_TYPE IN (4110,4111,4112) ");
      sbQuery.append(" AND C504_VOID_FL IS NULL ");
      sbQuery.append(" ORDER BY DNAME, C504_CONSIGNMENT_ID DESC ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:reportDashboardSets", "Exception is:" + e);
    }
    return alReturn;
  } // End of reportDashboardSets

  /**
   * reportDashboardItems - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  /*
   * Commented Because its not used anywhere.. public ArrayList reportDashboardItems(String strType)
   * throws AppError { ArrayList alReturn = new ArrayList(); try{
   * 
   * DBgmDBManagerectionWrapper gmDBManager = null; gmDBManager = new DBgmDBManagerectionWrapper();
   * 
   * StringBuffer sbQuery = new StringBuffer(); sbQuery.append(" SELECT C504_CONSIGNMENT_ID CID,
   * "); sbQuery.append(" decode(C701_DISTRIBUTOR_ID,
   * '',GET_ACCOUNT_NAME(C704_ACCOUNT_ID),GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID)) NAME,
   * "); sbQuery.append(" decode(C504_STATUS_FL,2,'Y','-') CONTROLFL, C504_TYPE CTYPE,
   * GET_CODE_NAME(C504_TYPE) TNAME, "); sbQuery.append(" C504_SHIP_REQ_FL SHIPREQFL, C504_COMMENTS
   * COMMENTS, GET_USER_NAME(C504_SHIP_TO_ID) UNAME "); sbQuery.append(" FROM T504_CONSIGNMENT
   * "); sbQuery.append(" WHERE C504_STATUS_FL > 1 AND C504_STATUS_FL < 4 AND C207_SET_ID IS NULL
   * "); sbQuery.append(" AND C504_VOID_FL IS NULL "); sbQuery.append(" AND C504_TYPE <> 4129 "); if
   * (strType.equals("Customer")) { sbQuery.append(" AND C701_DISTRIBUTOR_ID IS NOT NULL
   * "); sbQuery.append(" ORDER BY NAME, C504_CONSIGNMENT_ID DESC "); } alReturn =
   * gmDBManager.queryMultipleRecords(sbQuery.toString());
   * 
   * }catch (Exception e) { GmLogError.log("Exception in
   * GmCustomerBean:reportDashboardItems","Exception is:"+e); } return alReturn; } //End of
   * reportDashboardItems
   */

  /**
   * todaysSale - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap todaysSales() throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    String strDateFormat = "";
    GmCommonBean gmCommon = new GmCommonBean(getGmDataStoreVO());
    HashMap hmRuleParams = new HashMap();
    hmRuleParams.put("RULEID", "DATEFMT");
    hmRuleParams.put("RULEGROUP", "DATEFORMAT");
    strDateFormat = GmCommonClass.parseNull(gmCommon.getRuleValue(hmRuleParams));
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());



      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT SUM(C501_TOTAL_COST) SALES ");
      sbQuery.append(" FROM T501_ORDER ");
      sbQuery.append(" WHERE to_char(C501_ORDER_DATE,'" + strDateFormat + "') = to_char(SYSDATE,'"
          + strDateFormat + "') ");
      sbQuery.append(" AND NVL (C901_ORDER_TYPE, - 9999) NOT IN ('2533') ");
      sbQuery.append(" AND NVL (c901_order_type,      - 9999) NOT IN ");
      sbQuery
          .append(" (SELECT t906.c906_rule_value FROM t906_rules t906 WHERE t906.c906_rule_grp_id = 'ORDTYPE'  AND c906_rule_id = 'EXCLUDE')");
      sbQuery.append(" AND NVL (c901_order_type,      - 9999) NOT IN ");
      sbQuery
          .append(" (SELECT t906.c906_rule_value FROM t906_rules t906 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'  AND c906_rule_id = 'EXCLUDE')");
      sbQuery.append(" AND C501_VOID_FL IS NULL AND C501_DELETE_FL IS NULL ");
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmReturn.put("DAYSALES", hmResult);

      Calendar cal = new GregorianCalendar();

      int year = cal.get(Calendar.YEAR);
      int month = cal.get(Calendar.MONTH);
      month = month + 1;
      String strMonth = "" + month;
      if (strMonth.length() == 1) {
        strMonth = "0".concat(strMonth);
      }
      String strYear = "" + year;
      String strDate = strMonth.concat("/01/");
      strDate = strDate.concat(strYear);

      hmResult = new HashMap();
      sbQuery.setLength(0);
      sbQuery.append(" SELECT SUM(C501_TOTAL_COST) SALES ");
      sbQuery.append(" FROM T501_ORDER ");
      sbQuery.append(" WHERE C501_ORDER_DATE >= to_date('");
      sbQuery.append(strDate);
      sbQuery.append("','" + strDateFormat + "') ");
      sbQuery.append(" AND NVL (C901_ORDER_TYPE, - 9999) NOT IN ('2533') ");
      sbQuery.append(" AND NVL (c901_order_type,      - 9999) NOT IN ");
      sbQuery
          .append(" (SELECT t906.c906_rule_value FROM t906_rules t906 WHERE t906.c906_rule_grp_id = 'ORDTYPE'  AND c906_rule_id = 'EXCLUDE')");
      sbQuery.append(" AND NVL (c901_order_type,      - 9999) NOT IN ");
      sbQuery
          .append(" (SELECT t906.c906_rule_value FROM t906_rules t906 WHERE t906.c906_rule_grp_id = 'ORDERTYPE'  AND c906_rule_id = 'EXCLUDE')");
      sbQuery.append(" AND C501_VOID_FL IS NULL AND C501_DELETE_FL IS NULL ");

      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

      hmReturn.put("MONTHSALES", hmResult);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:todaysSales", "Exception is:" + e);
    }
    return hmReturn;
  } // End of todaysSale

  /**
   * todaysSalesReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap todaysSalesReport() throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT GET_DISTRIBUTOR_ID(A.C703_SALES_REP_ID) DID, GET_DISTRIBUTOR_NAME(GET_DISTRIBUTOR_ID(A.C703_SALES_REP_ID)) DNAME, ");
      sbQuery
          .append(" B.C704_ACCOUNT_NM NAME, SUM(A.C501_TOTAL_COST) + sum(decode(A.C501_SHIP_COST,'',0,A.C501_SHIP_COST)) SALES ");
      sbQuery.append(" FROM T501_ORDER A, T704_ACCOUNT B ");
      sbQuery
          .append(" WHERE to_char(A.C501_ORDER_DATE,get_rule_value('DATEFMT','DATEFORMAT')) = to_char(SYSDATE,get_rule_value('DATEFMT','DATEFORMAT')) ");
      sbQuery.append(" AND A.C501_VOID_FL IS NULL AND A.C501_DELETE_FL IS NULL ");
      sbQuery.append(" AND A.C704_ACCOUNT_ID = B.C704_ACCOUNT_ID ");
      sbQuery.append(" GROUP BY B.C704_ACCOUNT_NM , A.C703_SALES_REP_ID ");
      sbQuery.append(" ORDER BY DNAME, B.C704_ACCOUNT_NM ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("SALESREPORT", alReturn);
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:todaysSalesReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of todaysSalesReport

  /**
   * monthSalesReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap monthSalesReport(String strFromDate, String strToDate) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT GET_DISTRIBUTOR_ID(A.C703_SALES_REP_ID) DID, GET_DISTRIBUTOR_NAME(GET_DISTRIBUTOR_ID(A.C703_SALES_REP_ID)) DNAME, ");
      sbQuery
          .append(" B.C704_ACCOUNT_NM NAME, SUM(A.C501_TOTAL_COST) + sum(decode(A.C501_SHIP_COST,'',0,A.C501_SHIP_COST)) SALES ");
      sbQuery.append(" FROM T501_ORDER A, T704_ACCOUNT B ");
      sbQuery
          .append(" WHERE to_char(A.C501_ORDER_DATE,get_rule_value('DATEFMT','DATEFORMAT')) >='");
      sbQuery.append(strFromDate);
      sbQuery.append("' AND to_char(A.C501_ORDER_DATE,get_rule_value('DATEFMT','DATEFORMAT')) <='");
      sbQuery.append(strToDate);
      sbQuery.append("' AND A.C501_VOID_FL IS NULL AND A.C501_DELETE_FL IS NULL ");
      sbQuery.append(" AND A.C704_ACCOUNT_ID = B.C704_ACCOUNT_ID ");
      sbQuery.append(" GROUP BY B.C704_ACCOUNT_NM , A.C703_SALES_REP_ID ");
      sbQuery.append(" ORDER BY DNAME, B.C704_ACCOUNT_NM ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("SALESREPORT", alReturn);
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:monthSalesReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of monthSalesReport

  /**
   * dailySalesReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap dailySalesReport() throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      Calendar cal = new GregorianCalendar();

      int year = cal.get(Calendar.YEAR);
      int month = cal.get(Calendar.MONTH);
      month = month + 1;
      String strMonth = "" + month;
      if (strMonth.length() == 1) {
        strMonth = "0".concat(strMonth);
      }
      String strYear = "" + year;
      String strDate = strMonth.concat("/01/");
      strDate = strDate.concat(strYear);

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT to_char(C501_ORDER_DATE,get_rule_value('DATEFMT','DATEFORMAT')) SDATE, to_char(C501_ORDER_DATE,'Day') DAY, ");
      sbQuery
          .append(" SUM(C501_TOTAL_COST) + sum(decode(C501_SHIP_COST,'',0,C501_SHIP_COST)) SALES ");
      sbQuery.append(" FROM T501_ORDER ");
      sbQuery.append(" WHERE C501_ORDER_DATE > to_date('");
      sbQuery.append(strDate);
      sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT')) ");
      sbQuery.append(" AND C501_VOID_FL IS NULL AND C501_DELETE_FL IS NULL ");
      sbQuery
          .append(" GROUP BY to_char(C501_ORDER_DATE,get_rule_value('DATEFMT','DATEFORMAT')),to_char(C501_ORDER_DATE,'Day') ");
      sbQuery.append(" ORDER BY to_char(C501_ORDER_DATE,get_rule_value('DATEFMT','DATEFORMAT')) ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("SALESREPORT", alReturn);
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:todaysSalesReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of dailySalesReport

  /**
   * loadOrderDetails - This method is a function override method of loadOrderDetails and it only
   * passes order id as parameter and based on the value it fecthes all order information
   * 
   * @param String strOrdId
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOrderDetails(String strOrdId) throws AppError {
    return loadOrderDetails(strOrdId, null);
  }

  /**
   * loadOrderDetails - This method will be used to get the order information
   * 
   * @param String strOrdId
   * @param String strProjectType
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOrderDetails(String strOrdId, String strProjectType) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmPayment = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alCartDetails = new ArrayList();
    ArrayList alReturnDetail = new ArrayList();
    ArrayList alOrderId = new ArrayList();
    ArrayList alInvoiceId = new ArrayList();
    ArrayList alQuoteDetails = new ArrayList();
    ArrayList alTagDetails = new ArrayList();
    ArrayList alControlNumDetails = new ArrayList();
    ArrayList alOrderAtbDetails = new ArrayList();
    ArrayList alPaymentDetails = new ArrayList();
    ArrayList alPriceReqDtl = new ArrayList();
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(getGmDataStoreVO());

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    // Locale
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

    String strOrderType = "";
    String strShowSurgAtr = "NO";
    strShowSurgAtr =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_SURG_ATRB_DET"));
    hmPayment.put("ORDERID", strOrdId);
    hmPayment.put("INVID", "");
    HashMap hmResult = gmDOBean.fetchOrderDetails(strOrdId);
    hmReturn.put("ORDERDETAILS", hmResult);
    strOrderType = GmCommonClass.parseNull((String) hmResult.get("ORDERTYPE"));
    HashMap hmInParams = new HashMap();
    hmInParams.put("ORDERID", strOrdId);
    hmInParams.put("gmResourceBundleBean", gmResourceBundleBean);
    hmInParams.put("PROJECTTYPE", strProjectType);
    hmInParams.put("gmDataStoreVO", getGmDataStoreVO());
    // BBA-41 Dynamic Class Loading.
    if (GmCommonClass.parseNull(strProjectType).equals("PACKSLIP")) {
      strProjectType = null;
      String strClassName = "com.globus.custservice.beans.GmProcBaseOrderPackSlipBean";
      GmProcessBasePackSLipInterface packSlipClient =
          GmProcessBasePackSlip.getInstance(strClassName);
      alCartDetails = packSlipClient.fetchCartDetails(hmInParams);
    } else if (GmCommonClass.parseNull(strProjectType).equals("CHANGEPRICE")){//PMT-38606
    	strProjectType = null;
    	GmCartDetailsInterface gmCartDetailsInterface = (GmCartDetailsInterface) GmCommonClass.getSpringBeanClass("xml/Cart_Details_Beans.xml", "DOCART");
    	  hmInParams.replace("PROJECTTYPE", strProjectType);
    	          alCartDetails = gmCartDetailsInterface.fetchCartDetails(hmInParams); 
    } else {
      GmCartDetailsInterface gmCartDetailsInterface =
          (GmCartDetailsInterface) GmCommonClass.getSpringBeanClass("xml/Cart_Details_Beans.xml",
              getGmDataStoreVO().getCmpid());
      alCartDetails = gmCartDetailsInterface.fetchCartDetails(hmInParams);
    }
    hmReturn.put("CARTDETAILS", alCartDetails);

    hmResult = new HashMap();
    hmResult = gmDOBean.fetchShipDetails(strOrdId);
    hmReturn.put("SHIPDETAILS", hmResult);

    alReturn = gmDOBean.getBackOrderDetails(strOrdId, strProjectType);
    hmReturn.put("BACKORDER", alReturn);

    alReturn = gmDOBean.fetchConstructDetails(strOrdId);
    hmReturn.put("CONSTRUCTDETAILS", alReturn);

    alReturn = gmDOBean.fetchOrderSummary(strOrdId);
    hmReturn.put("ORDERSUMMARY", alReturn);

    alReturn = gmDOBean.fetchShipSummary(strOrdId);
    hmReturn.put("SHIPSUMMARY", alReturn);

    alOrderId = gmDOBean.fetchRelatedOrderId(strOrdId);
    hmReturn.put("ALORDERID", alOrderId);

    alTagDetails = gmDOBean.fetchDOTagDetails(strOrdId);
    hmReturn.put("ALTAGDETAILS", alTagDetails);

    alReturnDetail = gmDOBean.fetchDOReturnsDetails(strOrdId);
    hmReturn.put("ALRETURNDETAILS", alReturnDetail);

    alControlNumDetails = gmDOBean.fetchDOControlNumDetails(strOrdId);
    hmReturn.put("ALCONTROLNUMDETAILS", alControlNumDetails);

    alControlNumDetails = gmDOBean.filterControlNumberDetails(strOrdId, alControlNumDetails);
    hmReturn.put("ALFILTEREDCNUMDETAILS", alControlNumDetails);

    alInvoiceId = gmDOBean.fetchInvoiceDetails(strOrdId);
    hmReturn.put("ALINVOICEID", alInvoiceId);
    // For PMT-5022 There is a section newly added in DO Summary Section named payment Details which
    // will display payment amount and who initiated the payment.
    alPaymentDetails = GmCommonClass.parseNullArrayList(gmARBean.fetchPaymentDetails(hmPayment));
    hmReturn.put("ALPAYMENTDETAILS", alPaymentDetails);

    alPriceReqDtl = gmPriceRequestRptBean.fetchOrderPriceRequest(strOrdId);
    hmReturn.put("ALPRICEREQDTL", alPriceReqDtl);

    if (strOrderType.equals("2520")) {
      alQuoteDetails = fetchOrderAttributeInfo(strOrdId, "QUOATB");
      hmReturn.put("QUOTEDETAILS", alQuoteDetails);
    } else if (strShowSurgAtr.equals("YES")) {
      alQuoteDetails = fetchOrderAttributeInfo(strOrdId, "SURATB");
      hmReturn.put("QUOTEDETAILS", alQuoteDetails);
    }
    alOrderAtbDetails = fetchOrderAttributeInfo(strOrdId, "ORDATB");
    hmReturn.put("ORDATBDETAILS", alOrderAtbDetails);
    return hmReturn;
  } // End of loadOrderDetails

  /**
   * sendPhoneOrderEmail - This method will called to send mail to the rep after the phone order is
   * generated
   * 
   * @param HashMap hmReturn
   * @return HashMap
   * @exception AppError
   */
  public HashMap sendPhoneOrderEmail(HashMap hmReturn) {
    HashMap hmEmailData = new HashMap();
    String strCountryCode = GmCommonClass.countryCode;
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    String strCompanyCode = "";
    String strCompCurrSymb = "";
    String strAccCurrSymb = "";

    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    HashMap hmOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
    strCompanyCode = GmCommonClass.parseNull((String) hmOrderDetails.get("COMP_CODE"));// Get the
                                                                                       // company
                                                                                       // code
    String strOrdType = (String) hmOrderDetails.get("ORDERTYPE");
    GmEmailProperties emailProps = new GmEmailProperties();
    String strEmailKey = "GmOrder.";
    String strOrderSrc = GmCommonClass.parseNull((String) hmOrderDetails.get("ORDERSRC"));
    // if order raised by DO App, strEmailKey should have the respective information to send mail.
    strEmailKey = strOrderSrc.equals("103521") ? "GmDOAppOrder." : strEmailKey;


    emailProps.setRecipients((String) hmOrderDetails.get("REPEMAIL"));// reps email id to be fetched
    emailProps.setSender(gmResourceBundleBean.getProperty(strEmailKey + GmEmailProperties.FROM));
    emailProps.setCc(gmResourceBundleBean.getProperty(strEmailKey + GmEmailProperties.CC));
    emailProps.setMimeType(gmResourceBundleBean.getProperty(strEmailKey
        + GmEmailProperties.MIME_TYPE));
    String strSubject = gmResourceBundleBean.getProperty(strEmailKey + GmEmailProperties.SUBJECT);
    strSubject =
        GmCommonClass.replaceAll(strSubject, "#<ORDID>", (String) hmOrderDetails.get("ID"));
    emailProps.setSubject(strSubject);
    emailProps.setEmailHeaderName(gmResourceBundleBean.getProperty(strEmailKey + GmEmailProperties.EMAIL_HEADER_NM));
    if (!strCountryCode.equals("en")) {
      generateMailHeaders(strOrdType, hmReturn, hmEmailData);
    }
    hmEmailData.put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
    hmEmailData.put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSEmail"));
    log.debug("repemail " + (String) hmOrderDetails.get("REPEMAIL"));

    // ****** Header Information ********/
    hmEmailData.put("ID", hmOrderDetails.get("ID"));
    hmEmailData.put("ODT", GmCommonClass.getStringFromDate(
        (java.sql.Date) hmOrderDetails.get("ODT"), getCompDateFmt()));
    hmEmailData.put("SHIPADD", hmOrderDetails.get("SHIPADD"));
    hmEmailData.put("BILLADD", hmOrderDetails.get("BILLADD"));
    hmEmailData.put("PO", hmOrderDetails.get("PO"));
    hmEmailData.put("REPDISTNM", hmOrderDetails.get("REPDISTNM"));
    hmEmailData.put("CASE_ID", hmOrderDetails.get("CASE_ID"));
    hmEmailData.put("STATUS", hmOrderDetails.get("STATUSFL"));
    hmEmailData.put("ORDERTYPE", hmOrderDetails.get("ORDERTYPE"));
    hmEmailData.put("REQDATE", hmOrderDetails.get("REQDATE"));

    strAccCurrSymb = GmCommonClass.parseNull((String) hmOrderDetails.get("ACC_CURR_SYMB"));
    strCompCurrSymb =
        strAccCurrSymb.equals("") ? GmCommonClass.parseNull(GmCommonClass.getRuleValue(
            strCompanyCode, "HOLD_ORDER_CURRENCY")) : strAccCurrSymb;
    hmEmailData.put("STRSESSCURRSYMBOL", strCompCurrSymb); // passing
                                                           // currency
                                                           // symbol
                                                           // key
                                                           // to
                                                           // hashmap

    // Temp Fix and need to work on the same later
    log.debug("Total Sales " + hmOrderDetails.get("TOTAL_SALES"));
    hmEmailData.put("TOTAL_SALES",
        GmCommonClass.getStringWithCommas((String) hmOrderDetails.get("TOTAL_SALES"), 2));

    log.debug("ORDERSUMMARY " + hmReturn.get("ORDERSUMMARY"));
    log.debug("alShipDetails  " + hmReturn.get("SHIPSUMMARY"));


    // alShipDetails.add((HashMap)hmReturn.get("SHIPSUMMARY"));

    /*
     * ArrayList alOrderSUmmary = new ArrayList(); alOrderSUmmary =
     * GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ORDERSUMMARY")); int intQty = 0;
     * int intPreQty = 0; double dbBeforeItemTotal = 0.0; double dbBeforePriceTotal = 0.0; double
     * dbAfterItemTotal = 0.0; double dbAfterPriceTotal = 0.0; double dbBeforePrice = 0.0; double
     * dbAfterPrice = 0.0; double dbAdjustPrice = 0.0; String strAfterPrice = ""; String
     * strBeforeTotal = ""; String strAdjustAmount = "";
     * 
     * if(alOrderSUmmary.size() > 0){ for(int i=0; i<alOrderSUmmary.size();i++){ HashMap hmResult =
     * (HashMap)alOrderSUmmary.get(i); String strUnitPrice = ""; String strItemPrice = ""; String
     * strItemQty = ""; strUnitPrice = (String)hmResult.get("UPRICE"); strItemPrice =
     * (String)hmResult.get("PRICE"); strItemQty = (String)hmResult.get("QTY"); intQty =
     * Integer.parseInt(strItemQty)+ intPreQty; strItemQty = ""+intQty;
     * 
     * // Caliculating the Total Before Adjustment dbBeforeItemTotal =
     * Double.parseDouble(strUnitPrice); dbBeforeItemTotal = intQty * dbBeforeItemTotal; // Multiply
     * by Qty dbBeforePriceTotal = dbBeforePriceTotal + dbBeforeItemTotal; strBeforeTotal = "" +
     * dbBeforePriceTotal;
     * 
     * // Caliculating the Total After Adjustment dbAfterItemTotal =
     * Double.parseDouble(strItemPrice); dbAfterItemTotal = intQty * dbAfterItemTotal; // Multiply
     * by Qty dbAfterPriceTotal = dbAfterPriceTotal + dbAfterItemTotal; strAfterPrice = "" +
     * dbAfterPriceTotal;
     * 
     * // Caliculating the Adjustment Amount dbBeforePrice = Double.parseDouble(strBeforeTotal);
     * dbAfterPrice = Double.parseDouble(strAfterPrice); dbAdjustPrice = dbAfterPrice -
     * dbBeforePrice; strAdjustAmount = ""+dbAdjustPrice; } }
     * 
     * hmEmailData.put("BEFORETOTAL", strBeforeTotal); hmEmailData.put("ADJUSTMENTAMT",
     * strAdjustAmount); hmEmailData.put("AFTERTOTAL", strAfterPrice);
     */
    // ******** Sub Report Information ******* //
    hmEmailData.put("SUBORDERDETAILLIST", hmReturn.get("ORDERSUMMARY"));
    hmEmailData.put("SUBSHIPDETAILLIST", hmReturn.get("SHIPSUMMARY"));

    log.debug("SUBCONSTDETAILLIST: "
        + GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("CONSTRUCTDETAILS")));
    hmEmailData.put("SUBCONSTDETAILLIST",
        GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("CONSTRUCTDETAILS")));
    // hmEmailData.put("SUBCONSTDETAILLIST",null);

    // log.debug("gmJasperReport.getJasperRealLocation()" +jasperMail. .getJasperRealLocation());
    // hmResult.put("SUBREPORT_DIR", gmJasperReport.getJasperRealLocation());

    hmEmailData.put("EMAILPROPS", emailProps);
    return hmEmailData;
  }

  /**
   * generateMailHeaders This method will generate Mail headers for DO order & Ack order
   * 
   * @param strOrdType String
   * @param hmReturn HashMap
   * @param hmEmailData HashMap
   * @return HashMap
   */
  private HashMap generateMailHeaders(String strOrdType, HashMap hmReturn, HashMap hmEmailData) {
    HashMap hmOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
    if (strOrdType.equals("101260")) {
      hmEmailData.put("HEADERLBL", "Acknowledgement Order Summary");
      hmEmailData.put("ODTLBL", "Ack Date");
      hmEmailData.put("IDLBL", "Ack #");
    } else {
      hmEmailData.put("HEADERLBL", "Delivered Order Summary");
      hmEmailData.put("ODTLBL", "DO Date");
      hmEmailData.put("IDLBL", "DO #");
      hmEmailData.put("ACKLBL", "Ack #");
      hmEmailData.put("ACKREFID", hmOrderDetails.get("ACKREFID"));
      hmEmailData.put("SUBSHIPDETAILLIST", hmReturn.get("SHIPSUMMARY"));
    }
    return hmEmailData;
  }

  /**
   * saveControlOrderDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveControlOrderDetails(HashMap hmParam, String strItemsId, String strOrderId,
      String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());



    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    String strItemId = "";
    String strContNum = "";

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmLoop = new HashMap();

    StringTokenizer strTok = new StringTokenizer(strItemsId, ",");



    while (strTok.hasMoreTokens()) {
      strItemId = strTok.nextToken();
      hmLoop = new HashMap();
      hmLoop = (HashMap) hmParam.get(strItemId);
      strContNum = (String) hmLoop.get("CNTRL");

      gmDBManager.setPrepareString("GM_UPDATE_ITEM_ORDER", 5);

      /*
       * register out parameter and set input parameters
       */
      gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

      gmDBManager.setString(1, strItemId);
      gmDBManager.setString(2, strContNum);
      gmDBManager.setString(3, strOrderId);
      gmDBManager.setString(4, strUsername);

      gmDBManager.execute();
      strMsg = gmDBManager.getString(5);
      gmDBManager.commit();
      hmReturn.put("MSG", strMsg);

    } // ENd of WHILE


    return hmReturn;
  } // end of saveControlOrderDetails

  /**
   * loadShipping - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadShipping() throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alCarrier = new ArrayList();
    ArrayList alMode = new ArrayList();
    ArrayList alShipTo = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alAccList = new ArrayList();
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());

    try {
      alCarrier = gmCommon.getCodeList("DCAR", getGmDataStoreVO());
      alMode = gmCommon.getCodeList("DMODE", getGmDataStoreVO());
      alShipTo = gmCommon.getCodeList("CONSP");
      alRepList = getRepList("Active");
      hmReturn.put("CARRIER", alCarrier);
      hmReturn.put("MODE", alMode);
      hmReturn.put("SHIPLIST", alShipTo);
      hmReturn.put("REPLIST", alRepList);
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadShipping", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadShipping

  /**
   * saveShipOrderDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */

  /*
   * public HashMap saveShipOrderDetails(HashMap hmParam, String strOrderId, String strUsername)
   * throws AppError { DBgmDBManagerectionWrapper gmDBManager = null; gmDBManager = new
   * DBgmDBManagerectionWrapper();
   * 
   * CallableStatement gmDBManager = null; gmDBManagerection gmDBManager = null;
   * 
   * String strBeanMsg = ""; String strMsg = ""; String strPrepareString = null;
   * 
   * String strShipDate = ""; String strShipCarr = ""; String strShipMode = ""; String strTrack =
   * ""; String strShipCost = ""; String strShipCarrName = ""; String strOrderAmt = ""; String
   * strAccName = "";
   * 
   * HashMap hmResult = new HashMap(); HashMap hmReturn = new HashMap();
   * 
   * try { gmDBManager = gmDBManager.getgmDBManagerection(); gmDBManager.setAutoCommit(false);
   * strShipDate = (String) hmParam.get("SDT"); strShipCarr = (String) hmParam.get("SCAR");
   * strShipMode = (String) hmParam.get("SMODE"); strTrack = (String) hmParam.get("STRK");
   * strShipCost = (String) hmParam.get("SCOST"); strOrderAmt = (String) hmParam.get("ORDAMT");
   * strAccName = (String) hmParam.get("ACCNM");
   * 
   * strShipCarrName = gmCommon.getCodeName(strShipCarr);
   * 
   * strPrepareString = gmDBManager.getStrPrepareString("GM_UPDATE_SHIP_ITEM_ORDER", 8); gmDBManager
   * = gmDBManager.prepareCall(strPrepareString); // register out parameter and set input parameters
   * 
   * gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);
   * 
   * gmDBManager.setString(1, strShipDate); gmDBManager.setInt(2, Integer.parseInt(strShipCarr));
   * gmDBManager.setInt(3, Integer.parseInt(strShipMode)); gmDBManager.setString(4, strTrack);
   * gmDBManager.setString(5, strOrderId); gmDBManager.setString(6, strUsername);
   * gmDBManager.setDouble(7, Double.parseDouble(strShipCost));
   * 
   * gmDBManager.execute(); gmDBManager.commit(); strMsg = gmDBManager.getString(8); }
   * 
   * catch (Exception e) { e.printStackTrace(); GmLogError.log("Exception in
   * GmCustomerBean:saveshipdetails", "Exception is:" + e); try { gmDBManager.rollback(); } catch
   * (Exception sqle) { sqle.printStackTrace(); throw new AppError(sqle); } throw new AppError(e); }
   * finally { try { if (gmDBManager != null) { gmDBManager.close();
   * 
   * }// Enf of if (gmDBManager != null) if (gmDBManager != null) { gmDBManager.close(); // closing
   * the gmDBManagerections } } catch (Exception e) { throw new AppError(e); }// End of catch
   * finally { gmDBManager = null; gmDBManager = null; gmDBManager = null; } }
   * 
   * try { String strMailIds = "djames@globusmedical.com"; StringBuffer sbMailMsg = new
   * StringBuffer();
   * 
   * sbMailMsg.append("Hi,"); sbMailMsg.append("\n\nYour Order has been processed and is ready for
   * Shipping. Following are the details:"); sbMailMsg.append(" \n\n\tHospital Name: ");
   * sbMailMsg.append(strAccName); sbMailMsg.append(" \n\tOrder Number: ");
   * sbMailMsg.append(strOrderId); sbMailMsg.append(" \n\tOrder Amt: $");
   * sbMailMsg.append(GmCommonClass.getStringWithCommas(strOrderAmt));
   * sbMailMsg.append(" \n\tExpected Ship Date: "); sbMailMsg.append(strShipDate);
   * sbMailMsg.append(" \n\tShipped Via: "); sbMailMsg.append(strShipCarrName); sbMailMsg.append("
   * \n\tTracking Number: "); sbMailMsg.append(strTrack); sbMailMsg.append(" \n\n\n**Please DO NOT
   * respond to this mail**"); sbMailMsg.append(" \n\nFor Questions/Clarifications regarding this
   * order, contact Customer Service at (610)415-9000 xtn 222");
   * 
   * gmCommon.sendMail("Globus Medical Inc.<System@globusmedical.com>",
   * GmCommonClass.StringtoArray(strMailIds, ";"), GmCommonClass.StringtoArray( strMailIds, ";"),
   * "Order Confirmation", sbMailMsg.toString()); }
   * 
   * catch (Exception e) { e.printStackTrace(); GmLogError.log("Exception in
   * GmCustomerBean:saveshipdetails", "Exception is:" + e); try { gmDBManager.rollback(); } catch
   * (Exception sqle) { sqle.printStackTrace(); throw new AppError(sqle); } throw new AppError(e); }
   * finally { try { if (gmDBManager != null) { gmDBManager.close();
   * 
   * }// Enf of if (gmDBManager != null) if (gmDBManager != null) { gmDBManager.close(); // closing
   * the gmDBManagerections } } catch (Exception e) { throw new AppError(e); }// End of catch
   * finally { gmDBManager = null; gmDBManager = null; gmDBManager = null; } }
   * 
   * return hmReturn; } // end of saveShipOrderDetails
   */

  /**
   * 
   * reportAccountOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  /**
   * @param hmParam
   * @return
   * @throws AppError
   */
  public ArrayList reportAccountOrder(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company",
            GmCommonClass.getCompanyLocale(getCompId()));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strCustPO = GmCommonClass.parseNull((String) hmParam.get("CUSTPO"));
    String strTrack = GmCommonClass.parseNull((String) hmParam.get("TRACK"));
    String strTotal = GmCommonClass.parseNull((String) hmParam.get("TOTAL"));
    String strShipFrmDt = GmCommonClass.parseNull((String) hmParam.get("SHIPFRM"));
    String strShipToDt = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strOrderBy = GmCommonClass.parseNull((String) hmParam.get("ORDERBY"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("ORDERFRM"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("ORDERTO"));
    String strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
    String strParentOrdId = GmCommonClass.parseNull((String) hmParam.get("PARENTID"));
    String strHoldFl = GmCommonClass.parseNull((String) hmParam.get("HOLDFL"));
    String strDtFormat = GmCommonClass.parseNull((String) hmParam.get("DTFORMAT"));
    String strFormat = GmCommonClass.parseNull((String) hmParam.get("FORMAT"));
    String strParentFL = GmCommonClass.parseNull((String) hmParam.get("PARENTFL"));
    String strAccCurrId = GmCommonClass.parseNull((String) hmParam.get("ACC_CURR_ID"));
    String strIsQuote = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.QUOTE"));// Checking

    // whether "Quote"
    // is enabled or
    // not
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
    String strFilter = "";
    // GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "ORDFILTER"));
    String strCompPlantFilter =
        strFilter.equals("") ? strCompanyId : getGmDataStoreVO().getPlantid();
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT ");
    sbQuery.append(" C501_ORDER_ID ID, c501_REF_ID REFID, to_char(C501_ORDER_DATE_TIME,'");
    sbQuery.append(strFormat);
    sbQuery.append(" hh:mi AM') ODT, ");
    sbQuery
        .append(" gm_pkg_cm_shipping_info.get_track_no(c501_order_id,50180) TRACK, GET_TOTAL_ORDER_AMT(C501_ORDER_ID,'') COST, ");
    sbQuery
        .append(" C501_STATUS_FL SFL, DECODE('"
            + getComplangid()
            + "','103097',NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM)ANAME, "); // 103097:English
    sbQuery.append(" GET_USER_NAME(C501_CREATED_BY) NM, ");
    sbQuery.append(" SUBSTR(C501_CUSTOMER_PO,0,20) PO, "); // only 20 character of customer ref
                                                           // should display on modify order screen
    sbQuery.append(" T503.C503_INVOICE_ID INVID, ");
    sbQuery.append(" GET_LOG_FLAG(C501_ORDER_ID,1200) ORD_CALL_FLAG, ");
    sbQuery
        .append(" gm_pkg_op_order_rpt.GET_PARTSWAP_RA_STATUS(c501_parent_order_id, C901_ORDER_TYPE) PARTSWAP_RA_STATUS, "); // To
                                                                                                                            // Get
                                                                                                                            // the
                                                                                                                            // RA
                                                                                                                            // status
                                                                                                                            // Flag
    sbQuery.append(" GET_LOG_FLAG(T503.C503_INVOICE_ID,1201) INV_CALL_FLAG ");
    sbQuery.append(" ,C901_ORDER_TYPE ORDERTYPE , get_code_name(C901_ORDER_TYPE) ORDERTYPEDESC ");
    sbQuery
        .append(" ,C501_PARENT_ORDER_ID PARENTOID, C501_SHIPPING_DATE SDATE, C501_UPDATE_INV_FL INVFL ");
    sbQuery.append(" ,C501_HOLD_FL HOLDFL ");
    sbQuery
        .append(" ,GET_ACCOUNT_ATTRB_VALUE (T704.c704_ACCOUNT_ID, 91983) EMAILREQ  , GET_ACCOUNT_ATTRB_VALUE (T704.c704_ACCOUNT_ID, 91984) EVERSION ");
    sbQuery
        .append(" ,T503.C903_PDF_FILE_ID PDFFILEID,T503.C903_CSV_FILE_ID CSVFILEID ");
    sbQuery
        .append(" , c501_delivery_carrier CARRIER, GET_ACCOUNT_ATTRB_VALUE(T704.c704_ACCOUNT_ID,106100) OVRIDE_CURR,C501_DO_DOC_UPLOAD_FL DO_FLAG ");// 106100:
    // Override
    // Currency
    sbQuery.append(" ,t501.c5506_robot_quotation_id QUOTEID ");
    sbQuery.append(" FROM T501_ORDER T501, T704_ACCOUNT T704,T503_INVOICE T503 ");

    if (!strTrack.equals("")) {
      sbQuery.append(" ,T907_SHIPPING_INFO T907");
    }
    sbQuery.append(" WHERE C501_VOID_FL IS NULL");
    sbQuery.append(" AND T501.C503_INVOICE_ID = T503.C503_INVOICE_ID(+)");
    sbQuery.append(" AND T503.C503_VOID_FL (+)  IS NULL");
    sbQuery.append(" AND T501.C704_ACCOUNT_id = T704.C704_ACCOUNT_id ");
    if (!strParentFL.equals("") && !strAccId.equals("") && !strAccId.equals("0")) {
      sbQuery.append(" AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID");
      sbQuery
          .append(" AND T704.C101_PARTY_ID  In (SELECT C101_PARTY_ID from T704_ACCOUNT where C704_ACCOUNT_ID = "
              + strAccId + ") ");
    }
    if (!strAccId.equals("") && !strAccId.equals("0") && strParentFL.equals("")) {
      sbQuery.append(" AND T704.C704_ACCOUNT_ID = ");
      sbQuery.append(strAccId);
    }

    if (!strAccCurrId.equals("")) {
      sbQuery.append(" AND T704.C901_CURRENCY = ");
      sbQuery.append(strAccCurrId);
    }


    sbQuery
        .append(" AND (C501_DELETE_FL IS NULL OR (C901_ORDER_TYPE = '102080' and C501_DELETE_FL='Y' )) AND ( T501.C901_ext_country_id IS NULL  OR T501.C901_ext_country_id IN (SELECT country_id FROM V901_COUNTRY_CODES)) ");
    if (!strFromDate.equals("") && !strToDate.equals("")) {
      sbQuery.append(" AND C501_ORDER_DATE BETWEEN to_date('");
      sbQuery.append(strFromDate);
      sbQuery.append("','");
      sbQuery.append(strDtFormat);
      sbQuery.append("') AND to_date('");
      sbQuery.append(strToDate);
      sbQuery.append("','");
      sbQuery.append(strDtFormat);
      sbQuery.append("') ");
    }

    if (!strShipFrmDt.equals("") && !strShipToDt.equals("")) {
      sbQuery.append(" AND trunc(C501_SHIPPING_DATE) BETWEEN trunc(to_date('");
      sbQuery.append(strShipFrmDt);
      sbQuery.append("','");
      sbQuery.append(strDtFormat);
      sbQuery.append("')) AND trunc(to_date('");
      sbQuery.append(strShipToDt);
      sbQuery.append("','");
      sbQuery.append(strDtFormat);
      sbQuery.append("')) ");
    }



    if (!strRepId.equals("") && !strRepId.equals("0")) {
      sbQuery.append(" AND T501.C703_SALES_REP_ID = ");
      sbQuery.append(strRepId);
    }

    if (!strOrderId.equals("")) {
      sbQuery.append(" AND C501_ORDER_ID = '");
      sbQuery.append(strOrderId);
      sbQuery.append("'");
    }

    if (!strCustPO.equals("")) {
      if (!strCountryCode.equals("en")) {
        sbQuery.append(" AND C501_CUSTOMER_PO LIKE '%");
        sbQuery.append(strCustPO);
        sbQuery.append("%'");
      } else {
        sbQuery.append(" AND C501_CUSTOMER_PO ='");
        sbQuery.append(strCustPO);
        sbQuery.append("'");
      }
    }
    if (!strTrack.equals("")) {
      sbQuery
          .append(" AND T501.C501_ORDER_ID = T907.C907_REF_ID AND T907.C901_SOURCE = 50180 AND T907.C907_TRACKING_NUMBER='");
      sbQuery.append(strTrack);
      sbQuery.append("'");
    }


    if (!strTotal.equals("")) {
      sbQuery.append(" AND C501_TOTAL_COST >= ");
      sbQuery.append(strTotal);
    }

    if (!strOrderBy.equals("") && !strOrderBy.equals("0")) {
      sbQuery.append(" AND C501_CREATED_BY = ");
      sbQuery.append(strOrderBy);
    }

    if (!strInvoiceId.equals("")) {
      sbQuery.append(" AND T503.C503_INVOICE_ID = '");
      sbQuery.append(strInvoiceId);
      sbQuery.append("' ");
    }

    if (!strParentOrdId.equals("")) {
        sbQuery.append(" AND (C501_PARENT_ORDER_ID = '");
        sbQuery.append(strParentOrdId);
        sbQuery.append("'");
        sbQuery.append(" OR C501_REF_ID = '");
        sbQuery.append(strParentOrdId);
        sbQuery.append("' )");
      }
    if (!strHoldFl.equals("")) {
      sbQuery.append(" AND C501_HOLD_FL = 'Y'");
    }

    if (!strIsQuote.equals("YES")) {// This condition is needed if the "Quote" is enabled, otherwise
                                    // "Quote" related records will not fetch to modify order screen
      sbQuery.append(" AND NVL (c901_order_type, - 9999) NOT IN( SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' AND c906_rule_id = 'EXCLUDE') ");
    }
    sbQuery.append(" AND  (T501.C1900_COMPANY_ID =" + strCompPlantFilter);
    sbQuery.append(" OR  T501.C5040_PLANT_ID =" + strCompPlantFilter);
    sbQuery.append(" ) ORDER BY C501_ORDER_DATE_TIME DESC");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug(" Query in reportAccountOrder is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of reportAccountOrder

  /**
   * loadItemOrderDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadItemOrderDetails(String strItemOrdId) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT  C502_ITEM_ORDER_ID IID, C501_ORDER_ID ID, C205_PART_NUMBER_ID PNUM, ");
      sbQuery.append(" C502_ITEM_PRICE PRICE, C502_ITEM_QTY QTY, C502_CONTROL_NUMBER CNO  ");
      sbQuery.append(" FROM T502_ITEM_ORDER ");
      sbQuery.append(" WHERE C502_ITEM_ORDER_ID = '");
      sbQuery.append(strItemOrdId);
      sbQuery.append("' AND C502_VOID_FL IS NULL ");
      HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmReturn.put("ITEMORDERDETAILS", hmResult);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadItemOrderDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadItemOrderDetails

  /**
   * saveSplitItemOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveSplitItemOrder(ArrayList alList, String strOrderId, String strItemOrdId,
      String strPartNum, String strPrice, String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    String strgmDBManagerum = "";
    String strQty = "";
    String strInputString = "";

    HashMap hmLoop = new HashMap();
    HashMap hmReturn = new HashMap();

    int intSize = 0;



    intSize = alList.size();

    for (int i = 0; i < intSize; i++) {
      hmLoop = (HashMap) alList.get(i);
      strgmDBManagerum = (String) hmLoop.get("CNTRL");
      strgmDBManagerum = strgmDBManagerum.concat(",");
      strQty = (String) hmLoop.get("QTY");
      strQty = strQty.concat("|");
      strInputString = strInputString + strgmDBManagerum.concat(strQty);
      hmLoop = new HashMap();
    }

    gmDBManager.setPrepareString("GM_SPLIT_ITEM_ORDER", 7);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);

    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strOrderId);
    gmDBManager.setString(3, strItemOrdId);
    gmDBManager.setString(4, strPartNum);
    gmDBManager.setDouble(5, Double.parseDouble(strPrice));
    gmDBManager.setString(6, strUsername);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(7);
    gmDBManager.commit();

    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // end of saveSplitItemOrder

  /**
   * saveCustPO - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveCustPO(HashMap hmdetails) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";
    String strPrepareString = null;
    String strOrderId = GmCommonClass.parseNull((String) hmdetails.get("ORDERID"));
    String strPO = GmCommonClass.parseNull((String) hmdetails.get("PO"));
    String strOrderComments = GmCommonClass.parseNull((String) hmdetails.get("ORDERCOMMENTS"));
    String strInputString = GmCommonClass.parseNull((String) hmdetails.get("INPUTSTRING"));
    String strPOAmt = GmCommonClass.parseNull((String) hmdetails.get("POAMT"));
    String strCopay = GmCommonClass.parseNull((String) hmdetails.get("COPAY"));
    String strCapAmount = GmCommonClass.parseNull((String) hmdetails.get("CAPAMOUNT"));
    String strCopayCapAction = GmCommonClass.parseNull((String) hmdetails.get("COPAYCAPACTION"));
    String strUserid = GmCommonClass.parseNull((String) hmdetails.get("USERID"));
    String strPOStatus = GmCommonClass.parseNull((String) hmdetails.get("POSTATUS"));
    String strDOStatus = GmCommonClass.parseNull((String) hmdetails.get("DOSTATUS"));
    String strPODate = GmCommonClass.parseNull((String) hmdetails.get("PODATE"));//PC-3880 New field in record PO Date in GM Italy
    HashMap hmReturn = new HashMap();
    if (!strPO.equals("")) {
      strPO = returnCustomerPO(strOrderId, strPO);
    }
    // log.debug(" Customer PO is " + strPO + " Order Id is " + strOrderId);

    // gmDBManager.setAutoCommit(false);
    gmDBManager.setPrepareString("GM_UPDATE_PO", 13);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);//PC-3880 New field in record PO Date in GM Italy

    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strPO);
    gmDBManager.setString(3, strUserid);
    gmDBManager.setString(4, strInputString);
    gmDBManager.setString(5, strOrderComments);
    gmDBManager.setString(6, strPOAmt);
    gmDBManager.setString(7, strCopay);
    gmDBManager.setString(8, strCapAmount);
    gmDBManager.setString(9, strCopayCapAction);
    gmDBManager.setString(10, strPOStatus);
    gmDBManager.setString(11, strDOStatus);
    gmDBManager.setString(13, strPODate);//PC-3880 New field in record PO Date in GM Italy
    gmDBManager.execute();
    strMsg = gmDBManager.getString(12);//PC-3880 New field in record PO Date in GM Italy
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // end of saveCustPO

  /**
   * getPartNumsByOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String getPartNumsByOrder(String strOrdId) throws AppError {
    ArrayList alResult = new ArrayList();
    String strPartNums = "";
    HashMap hmLoop = null;
    String strTemp = "";
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT   C205_PART_NUMBER_ID ID ");
      sbQuery.append(" FROM T502_ITEM_ORDER ");
      sbQuery.append(" WHERE C501_ORDER_ID ='");
      sbQuery.append(strOrdId);
      sbQuery.append("' AND C502_DELETE_FL IS NULL ");
      sbQuery.append("  AND C502_VOID_FL IS NULL");
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
      for (int i = 0; i < alResult.size(); i++) {
        hmLoop = new HashMap();
        hmLoop = (HashMap) alResult.get(i);
        strTemp = (String) hmLoop.get("ID");
        strPartNums = strPartNums.concat(strTemp);
        strPartNums = strPartNums.concat(",");
      }
      if (!strPartNums.equals("")) {
        strPartNums = strPartNums.substring(0, strPartNums.length() - 1);
      }
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:getPartNumsByOrder", "Exception is:" + e);
    }
    return strPartNums;
  } // End of getPartNumsByOrder

  /**
   * loadSetConsignLists - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadSetConsignLists(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmDistType = new HashMap();

    ArrayList alType = new ArrayList();
    ArrayList alShipTo = new ArrayList();
    ArrayList alPurpose = new ArrayList();
    ArrayList alDistributor = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alAccList = new ArrayList();
    ArrayList alEmpList = new ArrayList();
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));
    String strScreentype = GmCommonClass.parseNull((String) hmParam.get("SCREENTYPE"));
    String strSelectedAction = GmCommonClass.parseNull((String) hmParam.get("SELECTEDACTION"));
    StringBuffer sbQuery = new StringBuffer();

    ArrayList alConPurpose = new ArrayList();

    try {
      alType = gmCommon.getCodeList("CONTY");
      alShipTo = gmCommon.getCodeList("CONSP");
      alPurpose = gmCommon.getCodeList("CONPU"); // inhouse consinment purpose
      alConPurpose = gmCommon.getCodeList("COPUR"); // consinment purpose

      // Get Distributor List from Owner Company
      if (strSelectedAction.equals("BSTR")) {
        hmDistType.put("DISTTYP", "26240143"); // 26240143:Owner Company
        alDistributor = getDistributorList(hmDistType);
      } else {
        alDistributor =
            strScreentype.equals("") ? getDistributorList("Active") : getDistributorList(hmParam);
      }
      // Below code to fetch distributor information

      alRepList = strScreentype.equals("") ? getRepList("Active") : getRepList(hmParam);
      alAccList = gmSales.reportAccount();
      alEmpList = gmLogon.getEmployeeList();

      hmReturn.put("CONTYPE", alType);
      hmReturn.put("SHIPTO", alShipTo);
      hmReturn.put("PURPOSE", alPurpose);
      hmReturn.put("CONPURPOSE", alConPurpose);

      hmReturn.put("DISTRIBUTORLIST", alDistributor);
      hmReturn.put("REPLIST", alRepList);
      hmReturn.put("ACCLIST", alAccList);
      hmReturn.put("EMPLIST", alEmpList);

      sbQuery.append(" SELECT C701_DISTRIBUTOR_ID DISTACCID, C504_SHIP_TO SHIPTO,  ");
      sbQuery
          .append(" C504_SHIP_TO_ID SHIPTOID, C504_SHIP_REQ_FL SHIPFL, C504_FINAL_COMMENTS COMMENTS, ");
      sbQuery.append(" C504_INHOUSE_PURPOSE PURPOSE, C504_TYPE TYPE , C504_STATUS_FL STATUS, ");
      sbQuery.append(" C526_PRODUCT_REQUEST_DETAIL_ID LINKLNREQID ");
      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("'");
      sbQuery.append(" AND C504_VOID_FL IS NULL ");
      log.debug(" Query to loadSetConsignLists details === " + sbQuery.toString());
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      log.debug("hmResult1***********" + hmResult);
      // Set Default Value in Type dropdown if selected Action is Built Set to Return
      if (strSelectedAction.equals("BSTR")) {
        hmResult.put("TYPE", "26240144"); // 26240144-Return to Owner Company
      }
      hmReturn.put("OTHERDETAILS", hmResult);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadSetConsignLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadSetConsignLists

  /**
   * updateSetConsign - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateSetConsign(String strConsignId, HashMap hmParam, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    String strMsg = "";

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    String strType = (String) hmParam.get("TYPE");
    if (!(strType.equals("4119") || strType.equals("4127") || strType.equals("40050"))) {
      gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
      // doing a release here as during process consign the status shud be
      // 30
      // hmParam.put("SOURCE", "50181");
      // gmShippingTransBean.releaseShipping(gmDBManager, hmParam);
    }

    String strPurpose = GmCommonClass.parseZero((String) hmParam.get("PURPOSE"));
    String strBillTo = GmCommonClass.parseZero((String) hmParam.get("DISTACCID"));
    String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTO"));
    String strShipToId = (String) hmParam.get("SHIPTOID");
    log.debug("strShipTo " + strShipTo);
    log.debug("strShipToId " + strShipToId);
    String strFinalComments = (String) hmParam.get("COMMENTS");
    String strShipFl = (String) hmParam.get("SHIPFL");
    String strStatusFl = GmCommonClass.parseNull((String) hmParam.get("STATUSFL"));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strSalesRepId = GmCommonClass.parseNull((String) hmParam.get("SALESREPID"));
    String strIctNotes = GmCommonClass.parseNull((String) hmParam.get("ICTNOTES"));
    String strLinkReqId = GmCommonClass.parseNull((String) hmParam.get("LINKREQID"));

    gmDBManager.setPrepareString("GM_UPDATE_SET_CONSIGN", 15);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(14, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setInt(2, Integer.parseInt(strType));
    gmDBManager.setInt(3, Integer.parseInt(strPurpose));
    gmDBManager.setString(4, strBillTo);
    gmDBManager.setInt(5, Integer.parseInt(strShipTo));
    gmDBManager.setString(6, strShipToId);
    gmDBManager.setString(7, strFinalComments);
    gmDBManager.setString(8, strShipFl);
    gmDBManager.setString(9, strStatusFl);
    gmDBManager.setString(10, strUsername);
    gmDBManager.setString(11, strAccId);
    gmDBManager.setString(12, strSalesRepId);
    gmDBManager.setString(13, strIctNotes);
    gmDBManager.setString(15, strLinkReqId);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(14);

    /* save shipping params */

    gmDBManager.commit();

    hmReturn.put("MSG", strMsg);

    // GmLogError.log("Exception in
    // GmCustomerBean:updateSetConsign","Exception is:" + e);

    return hmReturn;
  } // End of updateSetConsign

  /**
   * loadConsignDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignDetails(String strConsignId, String strLUserId) throws AppError {
    // strLUserId is user id from session
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmReturn = new HashMap();

    try {
      sbQuery
          .append(" SELECT C504_CONSIGNMENT_ID CID,NVL(T504.C701_DISTRIBUTOR_ID,t504.c704_account_id) DISTID, C504_REF_ID REFID,GET_CONSIGN_BILL_ADD(C504_CONSIGNMENT_ID");
      // sbQuery.append(strConsignId);
      sbQuery
          .append(") BILLADD , C504_CREATED_DATE CDATE, C504_LAST_UPDATED_DATE UDATE, gm_pkg_cm_shipping_info.get_ship_add(C504_CONSIGNMENT_ID,decode(c504_type,400085,4000518,400086,4000518,9110,50186,106703,26240435,106704,26240435,50181)");
      // sbQuery.append(strConsignId);
      sbQuery
          .append(") SHIPADD , GET_USER_NAME(C504_LAST_UPDATED_BY) NAME, C504_RETURN_DATE EDATE,GET_USER_NAME(C504_CREATED_BY) CREATEDBY,");
      sbQuery.append(" GET_DISTRIBUTOR_NAME(T504.C701_DISTRIBUTOR_ID) DISTNM, ");
      sbQuery.append(" GET_ACCOUNT_NAME(T504.C704_ACCOUNT_ID) ACCNM , ");
      sbQuery.append(" get_distributor_type (T504.C701_DISTRIBUTOR_ID) disttype, ");
      sbQuery
          .append(" GET_DISTRIBUTOR_INTER_PARTY_ID(T504.C701_DISTRIBUTOR_ID) PARTYID, t520.C520_REQUEST_ID REQUEST_ID, c504_stock_transfer_invoice_id INVOICEID, ");
      sbQuery
          .append(" GET_CODE_NAME(C504_DELIVERY_CARRIER) SCARR, GET_CODE_NAME(C504_DELIVERY_MODE) SMODE,C504_DELIVERY_CARRIER SCARRCODEID, C504_DELIVERY_MODE SMODECODEID, ");
      sbQuery
          .append(" C504_TRACKING_NUMBER TRACK, C504_STATUS_FL CONSIGNSTATUS, C504_SHIP_DATE SDATE, ");
      sbQuery.append(" GET_LOG_COMMENTS('" + strConsignId + "','1221') LOGCOMMENTS,");
      sbQuery
          .append(" C504_type TYPE,get_transaction_status(C504_STATUS_FL, '4110') STATUSVALUE, ");
      sbQuery.append(" NVL(C504_SHIP_COST,0)  SHIPCOST ,C504_COMMENTS ICTNOTES, ");
      sbQuery
          .append(" decode(T504.C504_STATUS_FL,'4',decode( GET_DISTRIBUTOR_INTER_PARTY_ID(T504.C701_DISTRIBUTOR_ID),NULL, get_comp_curr_symbol(t504.c1900_company_id),get_rule_value (50218, get_distributor_inter_party_id (T504.c701_distributor_id))),get_comp_curr_symbol(t504.c1900_company_id)) CURRENCY");
      sbQuery
          .append(" ,decode(GET_DISTRIBUTOR_INTER_PARTY_ID(T504.C701_DISTRIBUTOR_ID),NULL, get_comp_curr_symbol(t504.c1900_company_id),get_rule_value (50218, get_distributor_inter_party_id (T504.c701_distributor_id))) NEWCURRENCY,");
      sbQuery.append(" get_compid_from_distid(T504.C701_DISTRIBUTOR_ID) COMPANYID ");
      sbQuery
          .append(" , GM_PKG_OP_REQUEST_MASTER.GET_TAG_ID(decode(C901_REF_TYPE,26240144,C504_REF_ID,C504_CONSIGNMENT_ID)) TAGID, GM_PKG_OP_REQUEST_MASTER.GET_REQUEST_ATTRIBUTE(t504.C520_REQUEST_ID,'1006420') CSTPO, T504.C504_SHIP_TO SHIPTO ");
      sbQuery
          .append(" , get_set_id_from_cn(C504_CONSIGNMENT_ID) SETID, get_set_name_from_cn(C504_CONSIGNMENT_ID) SETNAME,get_set_id_from_cn(C504_CONSIGNMENT_ID)||' '||get_set_name_from_cn(C504_CONSIGNMENT_ID) SETIDNAME");
      sbQuery
          .append(" , GET_USER_NAME("
              + strLUserId
              + ") LUSERNM , get_rule_value("
              + strLUserId
              + ",'USERTITLE') UTITLE,t704.c704_ship_attn_to ATTENTIONOVER, t701.c701_ship_country shipcountry");
      sbQuery.append(", t504.c1910_division_id division_id ");
      sbQuery
          .append(", get_company_dtfmt(t504.c1900_company_id) CMPDFMT, t504.c1900_company_id COMPANYCD ");
      sbQuery
          .append(",T504.C704_LOCATION_ACCOUNT_ID LOCACCID, T504.C704_ACCOUNT_ID ACCID,GET_CS_SHIP_NAME(T504.C504_SHIP_TO,T504.C504_SHIP_TO_ID) SHIPTONAME ,t520.C520_REQUIRED_DATE REQ_DATE ");
      sbQuery.append(",t520.C525_Product_Request_Id LOANERREQID");
      sbQuery
          .append(" FROM T504_CONSIGNMENT T504, t704_account t704, t701_distributor t701, T520_REQUEST t520 ");
      sbQuery.append(" WHERE C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery.append("'");
      sbQuery
          .append(" AND t504.c704_account_id = t704.c704_account_id(+)  AND t504.C701_DISTRIBUTOR_ID = t701.C701_DISTRIBUTOR_ID(+) ");
      sbQuery.append(" AND T504.C520_REQUEST_ID = t520.C520_REQUEST_ID (+) ");
      sbQuery
          .append(" AND C504_VOID_FL IS NULL AND t704.c704_void_fl IS NULL  AND (t701.c701_void_fl IS NULL OR t504.c701_distributor_id = get_rule_value('DISTRIBUTOR','TAGST')) AND t520.c520_void_fl(+) IS NULL ");
      log.debug("sbQuery ::: " + sbQuery);
      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadConsignDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignDetails

  /**
   * loadOUSConsignDetails - This method will return GOP OUS consign item details.
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOUSConsignDetails(HashMap hmParam) throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    HashMap hmValue = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    String strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String struserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_oppr_request_summary.gm_fch_cn_dtls", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignmentId);
    gmDBManager.setString(2, struserId);

    gmDBManager.execute();
    hmValue =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();

    return hmValue;
  } // End of loadOUSConsignDetails

  /**
   * loadConsignAckDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignAckDetails(String strConsignId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmReturn = new HashMap();

    try {
      sbQuery
          .append(" SELECT A.C504_CONSIGNMENT_ID CID, DECODE(A.c701_distributor_id, NULL,C.c704_account_nm, B.C701_DISTRIBUTOR_NAME) NAME, ");
      sbQuery.append(" to_char(A.C504_LAST_UPDATED_DATE,'Day Monthdd,yyyy' ) UDATE, ");
      sbQuery.append(" to_char(A.C504_LAST_UPDATED_DATE+1,'Day Monthdd,yyyy') NDATE, ");
      sbQuery.append(" get_compid_from_distid(A.C701_DISTRIBUTOR_ID) COMPANYID, ");
      sbQuery.append(" A.c1910_division_id division_id ");
      sbQuery.append(" FROM T504_CONSIGNMENT A, T701_DISTRIBUTOR B , t704_account C");
      sbQuery.append(" WHERE A.C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConsignId);
      sbQuery
          .append("' AND A.C701_DISTRIBUTOR_ID = B.C701_DISTRIBUTOR_ID(+) AND A.c704_account_id     = C.c704_account_id(+) ");
      sbQuery
          .append(" AND A.C504_VOID_FL IS NULL  AND  C.c704_void_fl(+) IS NULL AND B.c701_void_fl(+) IS NULL ");

      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadConsignAckDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignAckDetails

  /**
   * loadConsignLists - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignLists(String strOpt, HashMap hmTransRules) throws AppError {
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmDistType = new HashMap();
    ArrayList alPurpose = new ArrayList();
    ArrayList alEmpList = new ArrayList();
    ArrayList alDistList = new ArrayList();

    hmTransRules = GmCommonClass.parseNullHashMap(hmTransRules);
    String strCodeList = "";

    try {
      if (strOpt.equals("InHouse")) {
        alEmpList = gmLogon.getEmployeeList();
        alPurpose = gmCommon.getCodeList("CONPU");
        hmReturn.put("EMPLIST", alEmpList);
        hmReturn.put("PURPOSE", alPurpose);
      } else if (strOpt.equals("Repack")) {
        alEmpList = gmLogon.getEmployeeList();
        alPurpose = gmCommon.getCodeList("CONPP");
        hmReturn.put("EMPLIST", alEmpList);
        hmReturn.put("PURPOSE", alPurpose);
      } else if (strOpt.equals("QuaranIn")) {
        alEmpList = gmLogon.getEmployeeList();
        alPurpose = gmCommon.getCodeList("CONQI");
        hmReturn.put("EMPLIST", alEmpList);
        hmReturn.put("PURPOSE", alPurpose);
      } else if (strOpt.equals("QuaranOut")) {
        alEmpList = gmLogon.getEmployeeList();
        alPurpose = gmCommon.getCodeList("CONSC");
        hmReturn.put("EMPLIST", alEmpList);
        hmReturn.put("PURPOSE", alPurpose);
      } else if (strOpt.equals("InvenIn")) {
        alEmpList = gmLogon.getEmployeeList();
        alPurpose = gmCommon.getCodeList("CONIN");
        hmReturn.put("EMPLIST", alEmpList);
        hmReturn.put("PURPOSE", alPurpose);
      } else if (strOpt.equals("DummyTransfer")) {
        ArrayList alType = new ArrayList();
        ArrayList alShipTo = new ArrayList();
        ArrayList alDistributor = new ArrayList();
        ArrayList alRepList = new ArrayList();
        ArrayList alAccList = new ArrayList();

        alType = gmCommon.getCodeList("CONTD");
        alShipTo = gmCommon.getCodeList("CONSP");
        alDistributor = getDistributorList();
        alRepList = getRepList();
        alAccList = gmSales.reportAccount();

        hmReturn.put("CONTYPE", alType);
        hmReturn.put("SHIPTO", alShipTo);
        hmReturn.put("DISTRIBUTORLIST", alDistributor);
        hmReturn.put("REPLIST", alRepList);
        hmReturn.put("ACCLIST", alAccList);
      } else {
        ArrayList alType = new ArrayList();
        ArrayList alShipTo = new ArrayList();
        ArrayList alDistributor = new ArrayList();
        ArrayList alRepList = new ArrayList();
        ArrayList alAccList = new ArrayList();

        alType = gmCommon.getCodeList("CONTY");
        alShipTo = gmCommon.getCodeList("CONSP");
        alDistributor = getDistributorList("Active");
        alRepList = getRepList("Active");
        alAccList = gmSales.reportAccount();

        hmReturn.put("CONTYPE", alType);
        hmReturn.put("SHIPTO", alShipTo);
        hmReturn.put("DISTRIBUTORLIST", alDistributor);
        hmReturn.put("REPLIST", alRepList);
        hmReturn.put("ACCLIST", alAccList);
      }
      if (hmTransRules.size() > 0) {

        strCodeList = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_CODELIST"));
        if (!strCodeList.equals("")) {
          alEmpList = gmLogon.getEmployeeList();
          alPurpose = gmCommon.getCodeList(strCodeList);
          hmDistType.put("DISTTYP", "26240143");
          alDistList = getDistributorList(hmDistType);// 26240143:Get Distributor List from Owner
                                                      // Company
          hmReturn.put("EMPLIST", alEmpList);
          hmReturn.put("PURPOSE", alPurpose);
          hmReturn.put("DISTLIST", alDistList); // Distributor List - Owner Company
        }
      }
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadConsignLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignLists

  /**
   * loadNextConsignId - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String loadNextConsignId(String strOpt) throws AppError {
    HashMap hmReturn = new HashMap();
    String strConsignId = "";
    StringBuffer sbQuery = new StringBuffer();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      sbQuery.append(" SELECT   GET_NEXT_CONSIGN_ID('");
      sbQuery.append(strOpt);
      sbQuery.append("') CID FROM DUAL ");
      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
      strConsignId = (String) hmReturn.get("CID");

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadNextConsignId", "Exception is:" + e);
    }
    return strConsignId;
  } // End of loadNextConsignId

  /**
   * saveItemConsign - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveItemConsign(String strConsignId, HashMap hmParam, String strInputStr,
      String strUsername) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    log.debug("saveItemConsginMM");
    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();


    // log.debug("6.1");
    String strType = GmCommonClass.parseZero((String) hmParam.get("TYPE"));
    String strPurpose = GmCommonClass.parseZero((String) hmParam.get("PURPOSE"));
    String strBillTo = (String) hmParam.get("BILLTO");
    String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTO"));
    String strShipToId = (String) hmParam.get("SHIPTOID");
    String strFinalComments = (String) hmParam.get("COMMENTS");
    String strShipFl = (String) hmParam.get("SHIPFL");
    // log.debug("6.3");


    gmDBManager.setPrepareString("GM_SAVE_ITEM_CONSIGN", 10);

    // log.debug("6.5");
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setInt(2, Integer.parseInt(strType));
    gmDBManager.setInt(3, Integer.parseInt(strPurpose));
    gmDBManager.setString(4, strBillTo);
    gmDBManager.setInt(5, Integer.parseInt(strShipTo));
    gmDBManager.setString(6, strShipToId);
    gmDBManager.setString(7, strFinalComments);
    gmDBManager.setString(8, strShipFl);
    gmDBManager.setString(9, strUsername);
    gmDBManager.setString(10, strInputStr);

    // log.debug("6.5");
    gmDBManager.execute();
    // log.debug("6.7");;
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of saveItemConsign

  /**
   * updateItemConsign - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateItemConsign(String strConsignId, String strInputStr, String strShipFl,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();


    gmDBManager.setPrepareString("GM_SAVE_ITEM_BUILD", 4);


    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strShipFl);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of updateItemConsign

  /**
   * saveConsignShipDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */

  // this prc GM_SAVE_CONSIGN_SHIP is not called from here
  public void saveConsignShipDetails(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";
    String strMode = GmCommonClass.parseNull((String) hmParam.get("MODE"));

    gmDBManager.setPrepareString("GM_SAVE_CONSIGN_SHIP", 8);
    // gmDBManager.registerOutParameter(7,java.sql.Types.CHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("CSGID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("SHIPDATE")));
    gmDBManager.setInt(3, Integer.parseInt(GmCommonClass.parseZero(GmCommonClass
        .parseNull((String) hmParam.get("SHIPCARR")))));
    gmDBManager.setInt(4, Integer.parseInt(GmCommonClass.parseZero(GmCommonClass
        .parseNull((String) hmParam.get("SHIPMODE")))));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("TRACK#")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("FREIGHTAMT")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("PONUM")));
    gmDBManager.execute();
    // strMsg = gmDBManager.getString(7);

    if (strMode.equals("DummyVerify")) {
      strMsg =
          "Dummy Consignment ".concat(GmCommonClass.parseNull((String) hmParam.get("CSGID")))
              .concat(" updated ");
      throw new AppError(strMsg, "", 'S');
    }
    gmDBManager.commit();
  } // End of saveConsignShipDetails

  /**
   * loadItemOrderLists - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadItemOrderLists(String strAtrbCodeGrp) throws AppError {
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmcommon = new GmCommonBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    ArrayList alMode = new ArrayList();
    ArrayList alShipTo = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alAccList = new ArrayList();
    ArrayList alOrderType = new ArrayList();
    ArrayList alAllRepList = new ArrayList(); // Active and inactive.
    ArrayList alQuoteDetails = new ArrayList();
    HashMap hmruleShipVal = new HashMap();
    HashMap hmPutGroupId = new HashMap();
    try {
      alMode = gmCommon.getCodeList("ORDMO", getGmDataStoreVO());
      alShipTo = gmCommon.getCodeList("CONSP", getGmDataStoreVO());
      alRepList = getRepList("Active");
      alAccList = gmSales.reportAccount();
      alOrderType = gmCommon.getCodeList("ODTYP", getGmDataStoreVO());
      alAllRepList = getRepList("");
      alQuoteDetails = fetchOrderAttributeInfo("", strAtrbCodeGrp);
      hmPutGroupId.put("RULEID", "SHIP_CARR_MODE_VAL");
      hmruleShipVal = gmcommon.fetchTransactionRules(hmPutGroupId, getGmDataStoreVO());// ship
                                                                                       // carrier
                                                                                       // and mode
                                                                                       // default
      // value get from rule

      hmReturn.put("DEFAULTSHIPMODECARRVAL", hmruleShipVal);
      hmReturn.put("ORDERMODE", alMode);
      hmReturn.put("SHIPTO", alShipTo);
      hmReturn.put("REPLIST", alRepList);
      hmReturn.put("ACCLIST", alAccList);
      hmReturn.put("ORDTYPE", alOrderType);
      hmReturn.put("ALLREPLIST", alAllRepList);
      hmReturn.put("QUOTELIST", alQuoteDetails);
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadItemOrderLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadItemOrderLists

  /**
   * loadNextOrderId - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String loadNextOrderId(String strOrderId, String strParentOrderId, String strAccId,
      String strOrdPrefix) throws AppError {
    HashMap hmReturn = new HashMap();
    StringBuffer sbQuery = new StringBuffer();
    String strError = "";
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      if (!strOrderId.equals("")) {
        sbQuery.append(" SELECT count(*) CNT FROM T501_ORDER WHERE C501_ORDER_ID = '");
        sbQuery.append(strOrderId);
        sbQuery.append("' AND C501_DELETE_FL IS NULL");
        hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
        String strCount = GmCommonClass.parseNull((String) hmReturn.get("CNT"));
        if (strCount.equals("1")) {
          strError = "DUPLICATE";
        }
      }

      if (!strParentOrderId.equals("")) {
        sbQuery.setLength(0);
        sbQuery.append(" SELECT count(*) CNT FROM T501_ORDER WHERE C501_ORDER_ID = '");
        sbQuery.append(strParentOrderId);
        sbQuery.append("' AND C501_DELETE_FL IS NULL AND C704_ACCOUNT_ID = '");
        sbQuery.append(strAccId);
        sbQuery.append("'");
        hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
        String strCount = GmCommonClass.parseNull((String) hmReturn.get("CNT"));
        if (strCount.equals("0")) {
          strError = "NOPARENT";
        }
      }

      if (strError.equals("")) {
        sbQuery.setLength(0);
        sbQuery.append(" SELECT GET_NEXT_ORDER_ID('");
        sbQuery.append(strOrderId);
        sbQuery.append("','" + strOrdPrefix + "') ORDID FROM DUAL ");
        hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
        strOrderId = (String) hmReturn.get("ORDID");
      } else {
        strOrderId = strError;
      }

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadNextOrderId", "Exception is:" + e);
    }
    return strOrderId;
  } // End of loadNextOrderId

  /**
   * saveItemOrder - This method will
   * 
   * @return HashMap
   * @exception AppError
   */
  @Override
  public HashMap saveItemOrder(HashMap hmParam) throws AppError {

    String strMsg = "";
    String strPrepareString = null;
    String strCtrlNumberStr = "";
    HashMap hmReturn = new HashMap();
    Date dtSurgDt = null;

    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    GmCommonBean gmCommonBean = new GmCommonBean(gmDataStoreVO);
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(gmDataStoreVO);
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(gmDataStoreVO);
    GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);
    GmNPIProcessBean GmNPIProcessBean = new GmNPIProcessBean(gmDataStoreVO);
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(gmDataStoreVO);
    GmOrderTagRecordBean gmOrderTagRecordBean = new GmOrderTagRecordBean(getGmDataStoreVO());

    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strTempId = GmCommonClass.parseNull((String) hmParam.get("STRTEMPID"));
    String strTotal = GmCommonClass.parseNull((String) hmParam.get("TOTAL"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strAdjInputStr = GmCommonClass.parseNull((String) hmParam.get("ADJINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrdType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strBillTo = GmCommonClass.parseNull((String) hmParam.get("BILLTO"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("MODE"));
    String strPerson = GmCommonClass.parseNull((String) hmParam.get("PERSON"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strPO = GmCommonClass.parseNull((String) hmParam.get("PO"));
    String strConCode = GmCommonClass.parseNull((String) hmParam.get("CONCODE"));
    String strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENTORDID"));

    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));
    String strShipCarr = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strAddressId = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID"));
    // String strSalesRep = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
    String strQuoteStr = GmCommonClass.parseNull((String) hmParam.get("QUOTESTR"));
    String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
    strCtrlNumberStr = GmCommonClass.parseNull((String) hmParam.get("CTRLNUMBERSTR"));
    String strHardCpPO = GmCommonClass.parseNull((String) hmParam.get("HARDCPPO"));
    String strOverrideAttnTo = GmCommonClass.parseNull((String) hmParam.get("OVERRIDEATTNTO"));
    String strShipInstruction = GmCommonClass.parseNull((String) hmParam.get("SHIPINSTRUCTION"));
    String strOrderComments = GmCommonClass.parseNull((String) hmParam.get("ORDER_COMMENTS"));
    String strRefOrderId = GmCommonClass.parseNull((String) hmParam.get("REFORDID"));
    String strReqDate = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDT"));
    String strShipCharge = GmCommonClass.parseNull((String) hmParam.get("SHIPCHARGE"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    strShipCharge =
        strShipCharge.equalsIgnoreCase("N/A") ? "" : GmCommonClass.replaceAll(strShipCharge, ",",
            "");
    String strLotOverrideFl = GmCommonClass.parseNull((String) hmParam.get("LOTOVERRIDEFL"));
    String strUpdateDDTFl = GmCommonClass.parseNull((String) hmParam.get("UPDATE_DDT_FL"));
    String strUsageLotFl = GmCommonClass.parseNull((String) hmParam.get("USAGE_LOT_FL"));

    if (!strOrderComments.equals("")) {
      strComments = strOrderComments;
    }
    strHardCpPO = (strHardCpPO.equals("on")) ? "Y" : "";
    String strShipId = "";
    strPO = returnCustomerPO(strOrderId, strPO);
    dtSurgDt = (java.util.Date) hmParam.get("SURGERYDT");

    log.debug("calling GM_SAVE_ITEM_ORDER *** " + hmParam);

    gmDBManager.setPrepareString("GM_SAVE_ITEM_ORDER", 25);
    gmDBManager.registerOutParameter(18, java.sql.Types.CHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strBillTo);
    gmDBManager.setInt(3, Integer.parseInt(GmCommonClass.parseZero(strMode)));
    gmDBManager.setString(4, strPerson);
    gmDBManager.setString(5, strOrderComments);
    gmDBManager.setString(6, strPO);
    gmDBManager.setString(7, strTotal);
    gmDBManager.setString(8, strUserId);
    gmDBManager.setString(9, strInputStr);
    gmDBManager.setInt(10, Integer.parseInt(strOrdType));
    gmDBManager.setInt(11, Integer.parseInt(GmCommonClass.parseZero(strConCode)));
    gmDBManager.setString(12, strParentOrderId);
    gmDBManager.setInt(13, Integer.parseInt(GmCommonClass.parseZero(strShipTo)));
    gmDBManager.setString(14, strShipToId);
    gmDBManager.setInt(15, Integer.parseInt(GmCommonClass.parseZero(strShipCarr)));
    gmDBManager.setInt(16, Integer.parseInt(GmCommonClass.parseZero(strShipMode)));
    gmDBManager.setInt(17, Integer.parseInt(GmCommonClass.parseZero(strAddressId)));
    gmDBManager.setString(19, strCaseId);
    gmDBManager.setString(20, strHardCpPO);
    gmDBManager.setString(21, strOverrideAttnTo);
    gmDBManager.setString(22, strShipInstruction);
    gmDBManager.setString(23, strRefOrderId);
    gmDBManager.setString(24, strShipCharge);
    gmDBManager.setDate(25, dtSurgDt);
    log.debug("1");
    gmDBManager.execute();
    log.debug("2");

    strShipId = gmDBManager.getString(18);
    log.debug("3");
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strOrderId, strComments, strUserId, "1200");
    }
    // To save BBA order
    if (!strRefOrderId.equals("")) {
      saveBBAOrder(gmDBManager, strOrderId, strUserId);
    }

    if (!strQuoteStr.equals("") || !strReqDate.equals("") || !strLotOverrideFl.equals("")) {
      if (!strReqDate.equals(""))
        strQuoteStr = strQuoteStr + "^" + 103926 + "^" + strReqDate + "|";
      if (!strLotOverrideFl.equals(""))
        strQuoteStr = strQuoteStr + "^" + 103950 + "^" + strLotOverrideFl + "|";
      log.debug("calling saveOrderAttribute");
      saveOrderAttribute(gmDBManager, strOrderId, strUserId, strQuoteStr);
    }
    // MNTTASK-8623 - CSM DO Process control # from S Part
    // Italy company - always save the order info and item order usage table
    
    // PMT-45387: Provision to record DDT and usage LOT
    // Currently Used LOT section store (T502b) based on DDT flag. Going to change based on Usage LOT flag
    if (!strCtrlNumberStr.equals("") || "Y".equals(strUsageLotFl)) {
      hmParam.put("INPUTSTR", strCtrlNumberStr);
      hmParam.put("ORDERID", strOrderId);
      hmParam.put("SCREEN", "NEW_ORDER");

      log.debug(" StrCtrlNumberStr >>>>>" + hmParam);
      gmDOBean.updateControlNumberDetail(hmParam, gmDBManager);
    }
    // hmParam.put("SOURCE", "50180"); // orders
    // hmParam.put("REFID", strOrderId);
    // hmParam.put("STATUSFL", "15");
    // gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
    log.debug("strShipId " + strShipId);

    gmTxnSplitBean.checkAndSplitDO(gmDBManager, strOrderId, strUserId);
    
    /* added for PMT-24435 - To Update Order id when order created without DOID*/
    gmOrderTagRecordBean.updateOrderId(gmDBManager, strOrderId, strUserId, strTempId);

    /* For Updating the Unit Price Adjustment Details for the Sales Order in T502_ITEM_ORDER table */
    if (!strAdjInputStr.equals("")) {
      updateAdjustmentDetails(gmDBManager, strOrderId, strAdjInputStr, strUserId);
    }

    // update the adjustment details for BBA order
    updateAdjustmentDetailsBBA(gmDBManager, strOrderId, strUserId);

    syncAccAttribute(gmDBManager, strOrderId, strBillTo, "ORDER", strUserId);
    // TO UPDATE THE NPI TRANSACTION STATUS
    GmNPIProcessBean.saveNPITransactionStatus(gmDBManager, strOrderId, strUserId);

    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);
    hmReturn.put("strShipId", strShipId);

    // To Save the Insert part number details in New order screen Using JMS
    String strTxnType = "50261";
    HashMap hmJMSParam = new HashMap();
    hmJMSParam.put("TXNID", strOrderId);
    hmJMSParam.put("TXNTYPE", strTxnType);
    hmJMSParam.put("STATUS", "93342");
    hmJMSParam.put("USERID", strUserId);
    hmJMSParam.put("COMPANYINFO", strCompanyInfo);
    gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);

    return hmReturn;
  } // End of saveItemOrder

  /**
   * updateItemOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateItemOrder(String strOrderId, String strInputStr, String strShipFl,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_SAVE_ITEM_ORD_ITEMS", 7);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strShipFl);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, "UPDATE");
    gmDBManager.setString(6, "");
    gmDBManager.setString(7, "");
    gmDBManager.execute();
    gmDBManager.commit();

    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // End of updateItemOrder

  /**
   * updateItemOrderPrice - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateItemOrderPrice(String strOrderId, String strInputStr, String strShipCost,
      String strTotal, String strUserId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());

    String strMsg = "";
    // To update Ack order price when price changed for bba order
    updateAckItemOrderPrice(gmDBManager, strOrderId, strInputStr, strUserId);

    gmDBManager.setPrepareString("GM_SAVE_ITEM_ORD_ITEMS_PRICE", 6);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setDouble(3, Double.parseDouble(strShipCost));
    gmDBManager.setDouble(4, Double.parseDouble(strTotal));
    gmDBManager.setString(5, strUserId);
    gmDBManager.execute();
    strMsg = gmDBManager.getString(6);
    log.debug("strMsg = " + strMsg);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);
    return hmReturn;
  }// End of updateItemOrderPrice

  /**
   * loadConsignReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadConsignReport(String strType, String strDistId, String strSetId,
      String strFromDt, String strToDt) throws AppError {
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    // String strDateFotmat = getGmDataStoreVO().getCmpdfmt();
    HashMap hmReturn = new HashMap();
    ArrayList alDistributor = new ArrayList();
    ArrayList alSets = new ArrayList();
    ArrayList alReturn = new ArrayList();
    String strIdString = "";

    try {

      alSets = gmProject.loadSetMap("1601");
      hmReturn.put("SETLIST", alSets);

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      log.debug("strType in line 3430 is : " + strType);
      if (strType.equals("50230") || strType.equals("50231") || strType.equals("50234")
          || strType.equals("50235") || strType.equals("50236") || strType.equals("50237")) // add
      // the
      // "ICT"-50234
      {
        alDistributor = getDistributorList();
        hmReturn.put("DISTRIBUTORLIST", alDistributor);
        strIdString = "C701_DISTRIBUTOR_ID";
      } else if (strType.equals("INHOUSE") || strType.equals("50232") || strType.equals("50233"))// (strType.equals("INHOUSE"))
      {
        alDistributor = gmCommon.getCodeList("CONPU");
        hmReturn.put("DISTRIBUTORLIST", alDistributor);
        strIdString = "C504_INHOUSE_PURPOSE";
      }

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C504_CONSIGNMENT_ID CID, C504_COMMENTS COMMENTS, GET_SET_NAME(C207_SET_ID) SNAME, ");
      sbQuery.append(" to_char(C504_SHIP_DATE,GET_RULE_VALUE('DATEFMT','DATEFORMAT')) SDATE ");
      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE ");
      sbQuery.append(strIdString);
      sbQuery.append(" ='");
      sbQuery.append(strDistId);
      sbQuery.append("'");
      if (!strSetId.equals("00")) {
        sbQuery.append(" AND C207_SET_ID ='");
        sbQuery.append(strSetId);
        sbQuery.append("'");
      }
      sbQuery.append(" AND C504_VOID_FL IS NULL ");
      sbQuery.append(" AND C207_SET_ID IS NOT NULL ");
      sbQuery.append(" AND C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT','DATEFORMAT')) AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("',GET_RULE_VALUE('DATEFMT','DATEFORMAT'))");
      sbQuery.append("ORDER BY C504_SHIP_DATE");

      log.debug(" Query for loadConsignReport " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("REPORT", alReturn);

    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmCustomerBean:loadConsignReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignReport

  /**
   * loadConsignReportNew - This method will
   * 
   * @param String strType
   * @param String strFromDt
   * @return RowSetDynaClass
   * @exception AppError
   */
  public RowSetDynaClass loadConsignReportNew(HashMap hmParam) throws AppError {
    RowSetDynaClass rsConsignReturn = null;
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
      String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
      String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
      String strDistId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
      String strDataFmt = getGmDataStoreVO().getCmpdfmt();
      String strCompPlantFilter = "";
      strCompPlantFilter =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "CONSIGN_RPT_SETS")); // CONSIGN_RPT_SETS-Rule
                                                                                                // Value:=
                                                                                                // plant.
      strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT t504.C207_SET_ID ID, GET_SET_NAME(t504.C207_SET_ID) SNAME, GET_SET_NAME(t504.C207_SET_ID) SNAME1, GM_PKG_OP_REQUEST_MASTER.GET_TAG_ID(t504.C504_CONSIGNMENT_ID) TAGID, ");
      if (strType.equals("50231") || strType.equals("50230") || strType.equals("50234")
          || strType.equals("50235") || strType.equals("50236") || strType.equals("50237"))// set
      // 50231
      // dist
      // 50230
      {
        sbQuery
            .append(" nvl(t504.C701_DISTRIBUTOR_ID,0) DID, GET_DISTRIBUTOR_NAME(t504.C701_DISTRIBUTOR_ID) DNAME, GET_DISTRIBUTOR_NAME(t504.C701_DISTRIBUTOR_ID) DNAME1, ");
        /*
         * sbQuery.append(
         * " to_number(GET_DIST_SET_COUNT(t504.C207_SET_ID,t504.C701_DISTRIBUTOR_ID,'DIST','");
         * sbQuery.append(strFromDt); sbQuery.append("','"); sbQuery.append(strToDt);
         * sbQuery.append("')) COUNT ");
         */
        sbQuery
            .append(" t504.c504_consignment_id CNID, GET_CODE_NAME (C504_INHOUSE_PURPOSE) PURPOSE, get_user_name(t504.c504_created_by) cby, t504.c504_created_date cdate , t504.c504_type ctype ");
      } else if (strType.equals("50232") || strType.equals("50233"))// inset
      // 50232
      // intype
      // 50233
      {
        sbQuery
            .append(" nvl(t504.C504_INHOUSE_PURPOSE,0) DID, get_account_name(t504.C704_ACCOUNT_ID) DNAME, GET_CODE_NAME(t504.C504_INHOUSE_PURPOSE) DNAME1,");
        /*
         * sbQuery.append(
         * " to_number(GET_DIST_SET_COUNT(t504.C207_SET_ID,t504.C504_INHOUSE_PURPOSE,'INHOUSE','");
         * sbQuery.append(strFromDt); sbQuery.append("','"); sbQuery.append(strToDt);
         * sbQuery.append("')) COUNT ");
         */
        sbQuery
            .append(" t504.c504_consignment_id CNID, GET_CODE_NAME (C504_INHOUSE_PURPOSE) PURPOSE, get_user_name(t504.c504_created_by) cby, t504.c504_created_date cdate , t504.c504_type ctype ");
      }

      sbQuery.append(" FROM T504_CONSIGNMENT t504 "); // removed
      // t701_distributor
      sbQuery.append(" WHERE t504.C207_SET_ID IS NOT NULL AND t504.C504_STATUS_FL = '4' ");
      sbQuery.append(" AND t504.C504_VOID_FL IS NULL ");
      // Fetch records based company/plant for EDC countries
      sbQuery.append(" AND ( t504.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbQuery.append(" OR t504.C5040_PLANT_ID = " + strCompPlantFilter);
      sbQuery.append(") ");
      sbQuery.append(" AND t504.C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDataFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDataFmt + "')");
      if (!strDistId.equals("0") && !strDistId.equals("")) {
        sbQuery.append(" AND t504.c701_distributor_id = " + strDistId + " ");
      }
      if (strType.equals("50231") || strType.equals("50230") || strType.equals("50234")
          || strType.equals("50235") || strType.equals("50236") || strType.equals("50237"))
      // "SET" - 50231; "DIST" - 50230; adding " 50234" - "ICT" 7/3/2008
      {
        if (strType.equals("50234")) { // "ICT"
          sbQuery.append(" AND t504.C504_TYPE = '40057' ");
          sbQuery.append(" AND t504.c701_distributor_id IN ( SELECT t701.C701_DISTRIBUTOR_ID");
          sbQuery
              .append("  FROM  T701_DISTRIBUTOR t701 WHERE t701.c901_distributor_type = 70103 ) ");
        } else if (strType.equals("50235")) { // "FD"
          sbQuery.append(" AND t504.C504_TYPE = '40057' ");
          sbQuery.append(" AND t504.c701_distributor_id IN ( SELECT t701.C701_DISTRIBUTOR_ID");
          sbQuery
              .append("  FROM  T701_DISTRIBUTOR t701 WHERE t701.c901_distributor_type = 70104 ) ");
        } else if (strType.equals("50236")) { // "ICS"
          sbQuery.append(" AND t504.C504_TYPE = '40057' ");
          sbQuery.append(" AND t504.c701_distributor_id IN ( SELECT t701.C701_DISTRIBUTOR_ID");
          sbQuery
              .append("  FROM  T701_DISTRIBUTOR t701 WHERE t701.c901_distributor_type = 70105 ) ");
        } else if (strType.equals("50237")) { // "Swiss"
          sbQuery.append(" AND t504.C504_TYPE = '40057' ");
          sbQuery.append(" AND t504.c701_distributor_id IN ( SELECT t701.C701_DISTRIBUTOR_ID");
          sbQuery
              .append("  FROM  T701_DISTRIBUTOR t701 WHERE t701.c901_distributor_type = 70106 ) ");
        } else {
          sbQuery.append(" AND t504.C504_TYPE <> '40057' ");
          // now for chemtech type wont be 40057 so it will pull up under US. so to avoid it adding
          // below condition
          sbQuery.append(" AND t504.c701_distributor_id not IN ( SELECT t701.C701_DISTRIBUTOR_ID");
          sbQuery
              .append("  FROM  T701_DISTRIBUTOR t701 WHERE t701.c901_distributor_type = 70104 ) ");
        }
        sbQuery.append(" AND t504.C701_DISTRIBUTOR_ID IS NOT NULL ");
        // sbQuery.append(" GROUP BY t504.C207_SET_ID, t504.C701_DISTRIBUTOR_ID ");

        if (strType.equals("50231"))// "SET" - 50231
        {
          sbQuery.append(" ORDER BY SNAME ");
        } else if (strType.equals("50230") || strType.equals("50234") || strType.equals("50235")
            || strType.equals("50236") || strType.equals("50237"))// "DIST",
        // adding
        // "50234"
        // -
        // ICT
        {
          sbQuery.append(" ORDER BY DNAME, ID ");
        }
      } else if (strType.equals("50232") || strType.equals("50233")) // "INSET"-50232;
      // "INTYPE"-50233
      {
        sbQuery.append(" AND t504.C701_DISTRIBUTOR_ID IS NULL AND t504.C704_ACCOUNT_ID = '01'");
        // sbQuery.append(" GROUP BY t504.C207_SET_ID , t504.C504_INHOUSE_PURPOSE ");

        if (strType.equals("50232")) // "INSET"
        {
          sbQuery.append(" ORDER BY SNAME ");
        } else if (strType.equals("50233")) // "INTYPE"
        {
          sbQuery.append(" ORDER BY DNAME, SNAME ");
        }
      }
      // else if (strType.equals("50234")){ // "ICT"
      // sbQuery.append(" AND C504_TYPE = '40057' ");
      // }
      log.debug(" Query for loadConsignReportNew " + sbQuery.toString());
      rsConsignReturn = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
      log.debug("Size ...... " + rsConsignReturn.getRows().size());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadConsignReportNew", "Exception is:" + e);
    }
    return rsConsignReturn;
  } // End of loadConsignReportNew

  /**
   * loadItemConsignReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadItemConsignReport(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();

      String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
      String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
      String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
      String strDistId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
      // String strDataFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFMT"));
      String strDateFormat = getGmDataStoreVO().getCmpdfmt();
      String strCompPlantFilter = "";
      strCompPlantFilter =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "CONSIGN_RPT_ITEMS")); // CONSIGN_RPT_ITEMS-Rule
                                                                                                 // Value:=
                                                                                                 // plant.
      strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

      sbQuery.append(" SELECT t504.C701_DISTRIBUTOR_ID DID,");
      if (strType.equals("50232") || strType.equals("50233")) { // 50232 - In-House - By Sets /
                                                                // Items
        sbQuery.append(" get_account_name(t504.C704_ACCOUNT_ID) DNAME ,");
      } else {
        sbQuery.append(" GET_DISTRIBUTOR_NAME(t504.C701_DISTRIBUTOR_ID) DNAME,");
      }
      sbQuery
          .append(" C504_CONSIGNMENT_ID CONID, C504_SHIP_DATE SDATE, C504_SHIP_TO_ID SHIPTO, C504_INHOUSE_PURPOSE PID, ");
      sbQuery
          .append(" GET_CODE_NAME(C504_INHOUSE_PURPOSE) PURPOSE, GET_CONSIGN_SHIP_ADD(C504_CONSIGNMENT_ID) EMP, ");
      sbQuery.append(" NVL(C504_SHIP_COST,0) SHIPCOST, ");
      sbQuery.append(" C504_COMMENTS COMMENTS ");
      sbQuery
          .append(" ,c504_consignment_id CNID, GET_CODE_NAME (C504_INHOUSE_PURPOSE) PURPOSE, get_user_name(c504_created_by) cby, c504_created_date cdate , c504_type ctype ");
      sbQuery.append(" FROM T504_CONSIGNMENT t504");
      sbQuery.append(" WHERE C207_SET_ID IS NULL AND C504_STATUS_FL = '4' ");
      sbQuery.append(" AND C504_VOID_FL IS NULL ");
      // Fetch records based company/plant for EDC countries
      sbQuery.append(" AND ( t504.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbQuery.append(" OR t504.C5040_PLANT_ID = " + strCompPlantFilter);
      sbQuery.append(") ");
      sbQuery.append(" AND C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFormat + "')");
      if (!strDistId.equals("0") && !strDistId.equals("")) {
        sbQuery.append(" AND t504.c701_distributor_id = " + strDistId + " ");
      }
      if (strType.equals("50230") || strType.equals("50234") || strType.equals("50235")
          || strType.equals("50236") || strType.equals("50237")) // DIST
      // or
      // ICT
      {
        // Distributor - ICT
        if (strType.equals("50234")) {
          sbQuery.append(" AND C504_TYPE = '40057' ");
          sbQuery.append(" AND  t504.c701_distributor_id IN ");
          sbQuery.append(" ( SELECT c701_distributor_id from t701_distributor  ");
          sbQuery.append(" WHERE C901_DISTRIBUTOR_TYPE = '70103'  ) ");
        }
        // Distributor - FD
        else if (strType.equals("50235")) {
          sbQuery.append(" AND C504_TYPE = '40057' ");
          sbQuery.append(" AND  t504.c701_distributor_id IN ");
          sbQuery.append(" ( SELECT c701_distributor_id from t701_distributor  ");
          sbQuery.append(" WHERE C901_DISTRIBUTOR_TYPE = '70104'  ) ");

        } else if (strType.equals("50236")) { // ICS
          sbQuery.append(" AND C504_TYPE = '40057' ");
          sbQuery.append(" AND  t504.c701_distributor_id IN ");
          sbQuery.append(" ( SELECT c701_distributor_id from t701_distributor  ");
          sbQuery.append(" WHERE C901_DISTRIBUTOR_TYPE = '70105'  ) ");

        } else if (strType.equals("50237")) {// Swiss
          sbQuery.append(" AND C504_TYPE = '40057' ");
          sbQuery.append(" AND  t504.c701_distributor_id IN ");
          sbQuery.append(" ( SELECT c701_distributor_id from t701_distributor  ");
          sbQuery.append(" WHERE C901_DISTRIBUTOR_TYPE = '70106'  ) ");

        } else {
          sbQuery.append(" AND C504_TYPE NOT IN ('4129', '40057')  ");
          // now for chemtech type wont be 40057 so it will pull up under US. so to avoid it adding
          // below condition
          sbQuery.append(" AND t504.c701_distributor_id not IN ( SELECT t701.C701_DISTRIBUTOR_ID");
          sbQuery
              .append("  FROM  T701_DISTRIBUTOR t701 WHERE t701.c901_distributor_type = 70104 ) ");
        }
        sbQuery.append(" AND t504.C701_DISTRIBUTOR_ID IS NOT NULL ");
        sbQuery.append(" ORDER BY DNAME,C504_CONSIGNMENT_ID DESC");
      } else {
        sbQuery.append(" AND t504.C701_DISTRIBUTOR_ID IS NULL ");
        sbQuery.append(" AND C704_ACCOUNT_ID = '01' ");
        sbQuery.append(" AND C504_TYPE = '4112' ");// For InHouse
        // Consignments,
        // Hardcode // for
        // 4114 also
        sbQuery.append(" ORDER BY C504_INHOUSE_PURPOSE,C504_CONSIGNMENT_ID DESC ");
      }

      log.debug(" Query for loadItemConsignReport " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("REPORT", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadItemConsignReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadItemConsignReport

  /**
   * loadPartNumSearchReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPartNumSearchReport(String strPartNum, String strControl, String strFrmDt,
      String strToDt, String strDtFmt, String strCompanyId) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strDateFmt =
        GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt()).equals("") ? strDtFmt
            : getGmDataStoreVO().getCmpdfmt();
    String strCompnyId =
        GmCommonClass.parseNull(getCompId()).equals("") ? strCompanyId : getCompId();
  //PC-4148 Remove Try Catch for Email Exception (TSK-23801)
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT A.C205_PART_NUMBER_ID PARTNUM, GET_PARTNUM_DESC(A.C205_PART_NUMBER_ID) PDESC, ");
      sbQuery.append(" B.C704_ACCOUNT_ID ACCID, GET_ACCOUNT_NAME(B.C704_ACCOUNT_ID) ANAME,  ");
      sbQuery
          .append(" SUM (decode(NVL (b.c901_order_type, -9999),2528,decode(sign(a.c502_item_qty),-1,a.c502_item_qty,0),a.c502_item_qty)) QTY ");
      sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B ");
      sbQuery.append(" WHERE A.C205_PART_NUMBER_ID IN (");
      sbQuery.append(strPartNum);
      sbQuery
          .append(") AND A.C501_ORDER_ID = B.C501_ORDER_ID AND B.C501_VOID_FL IS NULL AND A.C502_DELETE_FL IS NULL ");
      sbQuery.append("  AND A.C502_VOID_FL IS NULL ");
      sbQuery.append(" AND NVL(B.C901_ORDER_TYPE,-9999) NOT IN (2524) ");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");

      if (!strControl.equals("")) {
        sbQuery.append(" AND A.C502_CONTROL_NUMBER = '");
        sbQuery.append(strControl);
        sbQuery.append("'");
      }
      sbQuery.append(" AND B.C501_ORDER_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFrmDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "')");
      sbQuery.append(" AND B.C1900_COMPANY_ID =" + strCompnyId);
      sbQuery.append(" GROUP BY B.C704_ACCOUNT_ID, A.C205_PART_NUMBER_ID ");
      sbQuery.append(" ORDER BY PARTNUM,ANAME ");
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("SALESREPORT", alReturn);
      sbQuery.setLength(0);

      // Starts here -- MNTTASK-2796 Part Number Transaction Report duplicating (Customer Service)
      sbQuery.append("SELECT * FROM (");
      sbQuery
          .append(" SELECT B.C205_PART_NUMBER_ID PARTNUM, GET_PARTNUM_DESC(B.C205_PART_NUMBER_ID) PDESC, ");
      sbQuery.append(" A.C704_ACCOUNT_ID DID, GET_ACCOUNT_NAME(A.C704_ACCOUNT_ID) DNAME, ");
      sbQuery.append(" SUM(B.C505_ITEM_QTY) QTY,'ACCOUNT' REPORT_BY ");
      sbQuery.append(" FROM T504_CONSIGNMENT A, T505_ITEM_CONSIGNMENT B ");
      sbQuery.append(" WHERE B.C205_PART_NUMBER_ID IN (");
      sbQuery.append(strPartNum);
      sbQuery.append(") AND A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID ");
      if (!strControl.equals("")) {
        sbQuery.append(" AND B.C505_CONTROL_NUMBER = '");
        sbQuery.append(strControl);
        sbQuery.append("'");
      } else {
        sbQuery.append(" AND trim(B.C505_CONTROL_NUMBER) IS NOT NULL ");
      }
      sbQuery.append(" AND A.C504_TYPE <> 4129 ");
      sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND A.C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFrmDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "')");
      sbQuery.append(" AND A.C504_STATUS_FL = '4' ");
      sbQuery.append(" AND A.C701_DISTRIBUTOR_ID IS NULL ");
      sbQuery.append(" AND A.C1900_COMPANY_ID =" + strCompnyId);
      sbQuery.append(" GROUP BY B.C205_PART_NUMBER_ID,A.C704_ACCOUNT_ID ");
      sbQuery.append(" UNION ");
      sbQuery
          .append(" SELECT B.C205_PART_NUMBER_ID PARTNUM, GET_PARTNUM_DESC(B.C205_PART_NUMBER_ID) PDESC, A.C701_DISTRIBUTOR_ID DID,");
      sbQuery
          .append(" GET_DISTRIBUTOR_NAME(A.C701_DISTRIBUTOR_ID) DNAME ,SUM(B.C505_ITEM_QTY) QTY,'DIST' REPORT_BY ");
      sbQuery.append(" FROM T504_CONSIGNMENT A, T505_ITEM_CONSIGNMENT B");
      sbQuery.append(" WHERE B.C205_PART_NUMBER_ID IN (");
      sbQuery.append(strPartNum);
      sbQuery.append(") AND A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID ");
      if (!strControl.equals("")) {
        sbQuery.append(" AND B.C505_CONTROL_NUMBER = '");
        sbQuery.append(strControl);
        sbQuery.append("'");
      } else {
        sbQuery.append(" AND trim(B.C505_CONTROL_NUMBER) IS NOT NULL ");
      }
      sbQuery.append(" AND A.C504_TYPE <> 4129 ");
      sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND A.C504_SHIP_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFrmDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "')");
      sbQuery.append(" AND A.C504_STATUS_FL = '4' ");
      sbQuery.append(" AND A.C701_DISTRIBUTOR_ID IS NOT NULL");
      sbQuery.append(" AND A.C1900_COMPANY_ID =" + strCompnyId);
      sbQuery.append(" GROUP BY A.C701_DISTRIBUTOR_ID, B.C205_PART_NUMBER_ID) ");
      sbQuery.append(" ORDER BY PARTNUM,DNAME ");
      log.debug("Query:::" + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("CONSIGNREPORT", alReturn);
      // Ends Here ---- MNTTASK-2796 Part Number Transaction Report duplicating (Customer Service)
      sbQuery.setLength(0);
      sbQuery.append(" SELECT SUM(T507.C507_ITEM_QTY) QTY, T507.C205_PART_NUMBER_ID PARTNUM, ");
      sbQuery.append(" GET_PARTNUM_DESC(T507.C205_PART_NUMBER_ID) PDESC, ");
      sbQuery
          .append(" T506.C701_DISTRIBUTOR_ID DID, GET_DISTRIBUTOR_NAME(T506.C701_DISTRIBUTOR_ID) DNAME , ");
      sbQuery.append(" T506.C704_ACCOUNT_ID ACCID, GET_ACCOUNT_NAME(T506.C704_ACCOUNT_ID) ANAME ");
      sbQuery.append(" FROM T506_RETURNS T506, T507_RETURNS_ITEM T507 ");
      sbQuery.append(" WHERE T506.C506_RMA_ID = T507.C506_RMA_ID ");
      sbQuery.append(" AND T506.C506_VOID_FL IS NULL ");
      sbQuery.append(" AND T507.C205_PART_NUMBER_ID IN (");
      sbQuery.append(strPartNum);
      sbQuery.append(") AND T506.C506_CREDIT_DATE BETWEEN");
      sbQuery.append(" to_date('");
      sbQuery.append(strFrmDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "')");
      sbQuery.append(" AND T506.C506_STATUS_FL = 2 AND T507.C507_STATUS_FL = 'C' ");
      sbQuery.append(" AND T506.C1900_COMPANY_ID =" + strCompnyId);
      if (!strControl.equals("")) {
        sbQuery.append(" AND T507.C507_CONTROL_NUMBER = '");
        sbQuery.append(strControl);
        sbQuery.append("'");
      }
      sbQuery
          .append(" GROUP BY T506.C701_DISTRIBUTOR_ID, T507.C205_PART_NUMBER_ID, T506.C704_ACCOUNT_ID ");
      sbQuery.append(" ORDER BY PARTNUM,DNAME ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("RETURNREPORT", alReturn);

      sbQuery.setLength(0);
      sbQuery
          .append(" SELECT B.C205_PART_NUMBER_ID PARTNUM, GET_PARTNUM_DESC(B.C205_PART_NUMBER_ID) PDESC, ");
      sbQuery
          .append(" sum(B.C505_ITEM_QTY) QTY, GET_CODE_NAME(A.C504_INHOUSE_PURPOSE) TNAME, A.C504_INHOUSE_PURPOSE PID ");
      sbQuery.append(" FROM T504_CONSIGNMENT A, T505_ITEM_CONSIGNMENT B ");
      sbQuery.append(" WHERE B.C205_PART_NUMBER_ID IN (");
      sbQuery.append(strPartNum);
      sbQuery.append(") AND A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID ");
      if (!strControl.equals("")) {
        sbQuery.append(" AND B.C505_CONTROL_NUMBER = '");
        sbQuery.append(strControl);
        sbQuery.append("'");
      }
      sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND A.C504_VERIFIED_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFrmDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "')");
      sbQuery.append(" AND A.C504_STATUS_FL = '4' AND A.C504_TYPE = '4113' ");
      sbQuery.append(" AND A.C1900_COMPANY_ID =" + strCompnyId);
      sbQuery.append(" GROUP BY A.C504_INHOUSE_PURPOSE , B.C205_PART_NUMBER_ID ");
      sbQuery.append(" ORDER BY PARTNUM,TNAME ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("QUARANTINEREPORT", alReturn);

    return hmReturn;
  } // End of loadPartNumSearchReport

  /**
   * loadPartNumSearchDrillDown - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPartNumSearchDrillDown(String strId, String strType, String strPartNum,
      String strControl, String strFrmDt, String strToDt) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      if (strType.equals("S")) {
        sbQuery
            .append(" SELECT B.C501_ORDER_ID CID, to_char(B.C501_SHIPPING_DATE,get_rule_value('DATEFMT','DATEFORMAT')) SDATE, ");
        sbQuery.append(" A.C502_CONTROL_NUMBER CNUM  ");
        sbQuery
            .append(" ,SUM (decode(NVL (b.c901_order_type, -9999),2528,decode(sign(a.c502_item_qty),-1,a.c502_item_qty,0),a.c502_item_qty)) QTY ");
        sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B ");
        sbQuery.append(" WHERE A.C205_PART_NUMBER_ID ='");
        sbQuery.append(strPartNum);
        sbQuery
            .append("' AND A.C501_ORDER_ID = B.C501_ORDER_ID AND A.C502_VOID_FL IS NULL AND B.C501_VOID_FL IS NULL AND A.C502_DELETE_FL IS NULL");
        sbQuery.append(" AND NVL(B.C901_ORDER_TYPE,-9999) NOT IN (2524) ");
        sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
        sbQuery.append(" SELECT t906.c906_rule_value ");
        sbQuery.append(" FROM t906_rules t906 ");
        sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
        sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");

        if (!strControl.equals("")) {
          sbQuery.append(" AND A.C502_CONTROL_NUMBER = '");
          sbQuery.append(strControl);
          sbQuery.append("'");
        }
        sbQuery.append(" AND B.C501_ORDER_DATE BETWEEN ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFrmDt);
        sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT')) AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT'))");
        sbQuery.append(" AND B.C704_ACCOUNT_ID ='");
        sbQuery.append(strId);
        sbQuery
            .append("' GROUP BY B.C501_ORDER_ID, B.C501_SHIPPING_DATE, A.C502_CONTROL_NUMBER, b.c901_order_type ");
        sbQuery.append(" ORDER BY B.C501_ORDER_ID DESC ");
      } else if (strType.equals("C")) // For Consignment
      {
        sbQuery
            .append(" SELECT A.C504_CONSIGNMENT_ID CID, sum(B.C505_ITEM_QTY) QTY, to_char(A.C504_SHIP_DATE,get_rule_value('DATEFMT','DATEFORMAT')) SDATE, ");
        sbQuery.append(" A.C207_SET_ID SETID, B.C505_CONTROL_NUMBER CNUM ");
        sbQuery.append(" FROM T504_CONSIGNMENT A, T505_ITEM_CONSIGNMENT B ");
        sbQuery.append(" WHERE B.C205_PART_NUMBER_ID = '");
        sbQuery.append(strPartNum);
        sbQuery.append("' AND A.C504_CONSIGNMENT_ID = B.C504_CONSIGNMENT_ID ");
        if (!strControl.equals("")) {
          sbQuery.append(" AND B.C505_CONTROL_NUMBER = '");
          sbQuery.append(strControl);
          sbQuery.append("'");
        } else {
          sbQuery.append(" AND trim(B.C505_CONTROL_NUMBER) IS NOT NULL ");
        }
        sbQuery.append(" AND A.C504_TYPE <> 4129 ");
        sbQuery.append(" AND A.C504_VOID_FL IS NULL ");
        sbQuery.append(" AND A.C504_SHIP_DATE BETWEEN ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFrmDt);
        sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT')) AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT'))");
        sbQuery.append(" AND A.C504_STATUS_FL = '4' ");
        if (strId.equals("01")) {
          sbQuery.append(" AND A.C704_ACCOUNT_ID ='");
        } else {
          sbQuery.append(" AND A.C701_DISTRIBUTOR_ID ='");
        }
        sbQuery.append(strId);
        sbQuery
            .append("' GROUP BY A.C504_CONSIGNMENT_ID, A.C504_SHIP_DATE, A.C207_SET_ID, B.C505_CONTROL_NUMBER ");
        sbQuery.append(" ORDER BY A.C504_CONSIGNMENT_ID DESC ");
      } else if (strType.equals("R")) // For Returns
      {
        sbQuery
            .append(" SELECT A.C506_RMA_ID CID, sum(B.C507_ITEM_QTY) QTY, to_char(A.C506_CREDIT_DATE,get_rule_value('DATEFMT','DATEFORMAT')) SDATE, ");
        sbQuery.append(" A.C207_SET_ID SETID, B.C507_CONTROL_NUMBER CNUM ");
        sbQuery.append(" FROM T506_RETURNS A, T507_RETURNS_ITEM B ");
        sbQuery.append(" WHERE B.C205_PART_NUMBER_ID ='");
        sbQuery.append(strPartNum);
        sbQuery.append("' AND A.C506_RMA_ID = B.C506_RMA_ID ");
        sbQuery.append(" AND A.C506_VOID_FL IS NULL ");
        if (!strControl.equals("")) {
          sbQuery.append(" AND B.C507_CONTROL_NUMBER = '");
          sbQuery.append(strControl);
          sbQuery.append("'");
        }
        sbQuery.append(" AND A.C506_CREDIT_DATE BETWEEN ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFrmDt);
        sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT')) AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT'))");
        sbQuery.append(" AND A.C506_STATUS_FL > 0 AND B.C507_STATUS_FL <> 'N'");
        if (strId.equals("01")) {
          sbQuery.append(" AND A.C704_ACCOUNT_ID ='");
        } else {
          sbQuery.append(" AND A.C701_DISTRIBUTOR_ID ='");
        }
        sbQuery.append(strId);
        sbQuery
            .append("' GROUP BY A.C506_RMA_ID, A.C506_CREDIT_DATE, A.C207_SET_ID, B.C507_CONTROL_NUMBER ");
        sbQuery.append(" ORDER BY A.C506_RMA_ID DESC ");
      }
      log.debug("DRILLDOWN:" + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("DRILLREPORT", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadPartNumSearchDrillDown", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadPartNumSearchDrillDown

  /**
   * loadReturnsDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadReturnsDetails() throws AppError {
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    ArrayList alReturnType = new ArrayList();
    ArrayList alReturnReasons = new ArrayList();
    ArrayList alDistributors = new ArrayList();
    ArrayList alSets = new ArrayList();
    ArrayList alUserSet = new ArrayList();

    try {
      alReturnType = gmCommon.getCodeList("RETTY");
      alReturnReasons = gmCommon.getCodeList("RETRE");
      alDistributors = getDistributorList();
      alSets = gmProject.loadSetMap("1601");
      alUserSet = gmLogon.getEmployeeList("300", "");

      hmReturn.put("RETTYPES", alReturnType);
      hmReturn.put("RETREASONS", alReturnReasons);
      hmReturn.put("DISTLIST", alDistributors);
      hmReturn.put("SETLIST", alSets);
      hmReturn.put("EMPNAME", alUserSet);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadReturnsDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadReturnsDetails

  public ArrayList fetchReportTypes() throws AppError {
    ArrayList alReturn = new ArrayList();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C901_CODE_ID CODEID, C901_CODE_NM CSRPTTYPES  FROM T901_CODE_LOOKUP ");
      sbQuery.append(" WHERE  C901_CODE_GRP = 'CSRPT' ");
      sbQuery.append(" ORDER BY C901_CODE_NM ");
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:fetchReportTypes()", "Exception is:" + e);
    }

    return alReturn;
  }

  /**
   * viewReturnSetDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  /*
   * public HashMap viewReturnSetDetails(String strRAId) throws AppError { HashMap hmReturn = new
   * HashMap(); try{ DBgmDBManagerectionWrapper gmDBManager = null; gmDBManager = new
   * DBgmDBManagerectionWrapper();
   * 
   * StringBuffer sbQuery = new StringBuffer(); sbQuery.append(" SELECT C506_RMA_ID RAID,
   * C506_COMMENTS COMMENTS, GET_SET_NAME(C207_SET_ID) SNAME, "); sbQuery.append("
   * GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID) DNAME, GET_ACCOUNT_NAME(C704_ACCOUNT_ID) ANAME,
   * "); sbQuery.append(" GET_CODE_NAME(C506_TYPE) TYPE, GET_CODE_NAME(C506_REASON) REASON, ");
   * sbQuery.append(" to_char(C506_RETURN_DATE,'mm/dd/yyyy') RETDATE, C506_STATUS_FL SFL,
   * "); sbQuery.append(" to_char(C506_CREATED_DATE,'mm/dd/yyyy') CDATE,
   * get_user_name(C506_CREATED_BY) CUSER, "); sbQuery.append("
   * to_char(C506_EXPECTED_DATE,'mm/dd/yyyy') EDATE, C501_ORDER_ID ORDID,
   * to_char(C506_CREDIT_DATE,'mm/dd/yyyy') CRDATE "); sbQuery.append(" ,C506_TYPE TYPEID,
   * C506_REASON REASONID , get_user_name(C101_USER_ID) UNAME "); sbQuery.append(" FROM T506_RETURNS
   * "); sbQuery.append(" WHERE C506_RMA_ID = '"); sbQuery.append(strRAId); sbQuery.append("'");
   * sbQuery.append(" AND C506_VOID_FL IS NULL ");
   * 
   * log.debug(" Query to Fetch Details based on RAID in Customer Bean is " + sbQuery.toString());
   * hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
   * 
   * if(hmReturn.isEmpty()) { throw new AppError(AppError.APPLICATION, "0003"); } }
   * 
   * catch (Exception e) { GmLogError.log("Exception in
   * GmCustomerBean:viewReturnSetDetails","Exception is:"+e); throw new AppError(e); } return
   * hmReturn; } //End of viewReturnSetDetails
   */

  /**
   * returnCustomerPO - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String returnCustomerPO(String strOrderId, String POId) throws AppError {
    HashMap hmReturn = new HashMap();
    String strCount = "";
    int intCnt = 0;
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT COUNT(*) CNT FROM T501_ORDER ");
      sbQuery.append(" WHERE C501_CUSTOMER_PO LIKE '");
      sbQuery.append(POId);
      sbQuery
          .append("%' AND C501_DELETE_FL IS NULL AND C704_ACCOUNT_ID = (SELECT C704_ACCOUNT_ID FROM T501_ORDER WHERE C501_ORDER_ID='");
      sbQuery.append(strOrderId);
      sbQuery.append("') AND C501_STATUS_FL IS NOT NULL AND C501_STATUS_FL = 4 ");

      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
      strCount = (String) hmReturn.get("CNT");
      intCnt = Integer.parseInt(strCount);
      if (intCnt > 0) {
        for (int i = 0; i < intCnt; i++) {
          POId = POId + ".";
        }
      }

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:returnCustomerPO", "Exception is:" + e);
    }
    return POId;
  } // End of returnCustomerPO

  /**
   * initiateItemReturns - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  /*
   * public HashMap initiateItemReturns(HashMap hmParam)throws AppError { DBgmDBManagerectionWrapper
   * gmDBManager = null; gmDBManager = new DBgmDBManagerectionWrapper();
   * 
   * CallableStatement gmDBManager = null; gmDBManagerection gmDBManager = null;
   * 
   * String strMsg = ""; String strPrepareString = null;
   * 
   * HashMap hmReturn = new HashMap(); HashMap hmResult = new HashMap(); //
   * log.debug(" Values inside hmParam is " + hmParam);
   * 
   * String strRAId = ""; String strType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
   * String strReason = GmCommonClass.parseNull((String)hmParam.get("REASON")); String strDistId =
   * GmCommonClass.parseNull((String)hmParam.get("ID")); String strComments =
   * GmCommonClass.parseNull((String)hmParam.get("COMMENTS")); String strExpDate =
   * GmCommonClass.parseNull((String)hmParam.get("EDATE")); String strOrderId =
   * GmCommonClass.parseNull((String)hmParam.get("ORDERID")); String strParentOrderId =
   * GmCommonClass.parseNull((String)hmParam.get("PARENT_ORDERID")); String strInputString =
   * GmCommonClass.parseNull((String)hmParam.get("INPUTSTR")); String strFlag =
   * GmCommonClass.parseNull((String)hmParam.get("FLAG")); String strUsername =
   * GmCommonClass.parseNull((String)hmParam.get("USERID")); String strEmpName =
   * GmCommonClass.parseZero((String)hmParam.get("EMPNAME"));
   * 
   * log.debug(" Values inside strInputString is " + strInputString);
   * 
   * try { gmDBManager = gmDBManager.getgmDBManagerection(); gmDBManager.setAutoCommit(false);
   * strPrepareString = gmDBManager.getStrPrepareString("GM_INITIATE_ITEM_RETURN",11); gmDBManager =
   * gmDBManager.prepareCall(strPrepareString); /* register out parameter and set input parameters
   * 
   * gmDBManager.registerOutParameter(10,java.sql.Types.CHAR);
   * gmDBManager.registerOutParameter(11,java.sql.Types.CHAR);
   * 
   * gmDBManager.setInt(1,Integer.parseInt(strType));
   * gmDBManager.setInt(2,Integer.parseInt(strReason)); gmDBManager.setString(3,strDistId);
   * gmDBManager.setString(4,strComments); gmDBManager.setString(5,strExpDate);
   * gmDBManager.setString(6,strOrderId); gmDBManager.setInt(7,Integer.parseInt(strUsername));
   * gmDBManager.setString(8,strParentOrderId); gmDBManager.setInt(9,Integer.parseInt(strEmpName));
   * gmDBManager.execute(); gmDBManager.commit(); strRAId = gmDBManager.getString(10); strMsg =
   * gmDBManager.getString(11);
   * 
   * if (strMsg != null) { throw new AppError(AppError.APPLICATION,strMsg); }
   * 
   * GmOperationsBean gmOper = new GmOperationsBean();
   * 
   * hmReturn = gmOper.saveReturnsSetMaster(strRAId,strInputString,"",strFlag,strUsername);
   * 
   * hmReturn = loadReturnsDetails(strRAId,""); //hmResult = viewReturnSetDetails(strRAId);
   * //hmReturn.put("RETURNDETAILS",hmResult);
   * 
   * 
   * }catch(Exception e) { try { gmDBManager.rollback(); } catch (Exception sqle) {
   * sqle.printStackTrace(); throw new AppError(sqle); }
   * 
   * GmLogError.log("Exception in GmOperationsBean:initiateItemReturns","Exception is:"+e); throw
   * new AppError(e); } finally { try { if (gmDBManager != null) { gmDBManager.close(); }//Enf of if
   * (gmDBManager != null) if(gmDBManager!=null) { gmDBManager.close(); closing the
   * gmDBManagerections } } catch(Exception e) { throw new AppError(e); }//End of catch finally {
   * gmDBManager = null; gmDBManager = null; gmDBManager = null; } } return hmReturn; } //end of
   * initiateItemReturns
   */

  /**
   * loadReturnsConsignForCredit - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  /*
   * public HashMap loadReturnsConsignForCredit(String strRAId) throws AppError {
   * DBgmDBManagerectionWrapper gmDBManager = null; gmDBManager = new DBgmDBManagerectionWrapper();
   * 
   * CallableStatement gmDBManager = null; gmDBManagerection gmDBManager = null;
   * 
   * String strBeanMsg = ""; String strPrepareString = null;
   * 
   * ArrayList alReturn = new ArrayList(); HashMap hmReturn = new HashMap(); HashMap hmResult = new
   * HashMap(); ResultSet rs = null;
   * 
   * try { gmDBManager = gmDBManager.getgmDBManagerection(); strPrepareString =
   * gmDBManager.getStrPrepareString("GM_LOAD_CONSIGN_RETURN",3); gmDBManager =
   * gmDBManager.prepareCall(strPrepareString); /* register out parameter and set input parameters
   * 
   * gmDBManager.registerOutParameter(2,java.sql.Types.CHAR);
   * gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
   * 
   * gmDBManager.setString(1,strRAId);
   * 
   * gmDBManager.execute();
   * 
   * strBeanMsg = gmDBManager.getString(2); rs = (ResultSet)gmDBManager.getObject(3); alReturn =
   * gmDBManager.returnArrayList(rs); hmReturn.put("RETLOAD",alReturn);
   * 
   * hmResult = viewReturnSetDetails(strRAId); hmReturn.put("RETURNDETAILS",hmResult); }
   * catch(Exception e) { e.printStackTrace(); GmLogError.log("Exception in
   * GmCustomerBean:loadReturnConsignForCredit","Exception is:"+e); try { gmDBManager.rollback(); }
   * catch (Exception sqle) { sqle.printStackTrace(); throw new AppError(sqle); } throw new
   * AppError(e); } finally { try { if (gmDBManager != null) { gmDBManager.close();
   * 
   * }//Enf of if (gmDBManager != null) if(gmDBManager!=null) { gmDBManager.close(); /* closing the
   * gmDBManagerections } } catch(Exception e) { throw new AppError(e); }//End of catch finally {
   * gmDBManager = null; gmDBManager = null; gmDBManager = null; } } return hmReturn; } // End of
   * loadReturnsConsignForCredit
   */

  /**
   * rollConsignmentToInventory - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap rollConsignmentToInventory(String strConsignId, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_ROLL_CONSIGN_INV", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strUserId);

    gmDBManager.execute();

    strBeanMsg = gmDBManager.getString(3);
    gmDBManager.commit();
    hmReturn.put("MSG", strBeanMsg);


    return hmReturn;
  } // End of rollConsignmentToInventory

  /**
   * loadReturnsReport - This method will
   * 
   * @param String strType
   * @param String strFromDt,strToDt
   * @return RowSetDynaClass
   * @exception AppError
   */
  public ArrayList loadReturnsReport(String strType, String strFromDt, String strToDt)
      throws AppError {
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    ArrayList alReturn = new ArrayList();
  //PC-4148 Remove Try Catch for Email Exception (TSK-23806)

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
     
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT T506.C207_SET_ID ID, DECODE(T506.C207_SET_ID, '', 'Multiple Sets',GET_SET_NAME(T506.C207_SET_ID)) SNAME, DECODE(T506.C207_SET_ID, '', 'Multiple Sets',GET_SET_NAME(T506.C207_SET_ID)) SNAME1 ");
      sbQuery
          .append(" ,nvl(T506.C701_DISTRIBUTOR_ID,0) DID, GET_DISTRIBUTOR_NAME(T506.C701_DISTRIBUTOR_ID) DNAME, GET_DISTRIBUTOR_NAME(T506.C701_DISTRIBUTOR_ID) DNAME1,");
      sbQuery.append("  count  FROM  t506_returns t506, ");
      sbQuery.append(" ( SELECT COUNT(1) COUNT, c207_set_id, c701_distributor_id   FROM t506_returns WHERE c506_status_fl = '2' ");
      sbQuery.append(" AND c506_credit_date BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "')");      
      sbQuery.append(" AND c506_void_fl IS NULL  GROUP BY  c207_set_id,c701_distributor_id) t506_cnt ");
      sbQuery.append(" WHERE T506.C506_STATUS_FL = '2' AND  t506.c506_void_fl IS NULL ");
      sbQuery.append(" AND T506.C506_CREDIT_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "')");     
      sbQuery.append(" AND t506.c207_set_id = t506_cnt.c207_set_id ");
      sbQuery.append(" AND t506.c701_distributor_id = t506_cnt.c701_distributor_id AND ");
      //PC-4419 User request cancel (GmCustomerBean loadReturnsReport) changed the query to improve the performance
      if (strType.equals("50231") || strType.equals("50230")) {
        sbQuery.append(" T506.C506_TYPE IN (3301,3306)");
      } else if (strType.equals("50234")) {
        sbQuery.append(" T506.C506_TYPE IN (3307,26240178)");
      }
      sbQuery.append(" AND T506.C5040_PLANT_ID = " + getCompPlantId());
      sbQuery.append(" and T506.C207_SET_ID is not null ");

      if (strType.equals("50231") || strType.equals("50230") || strType.equals("50234")) {
        sbQuery.append(" AND T506.C701_DISTRIBUTOR_ID IS NOT NULL ");
        sbQuery.append(" GROUP BY T506.C207_SET_ID, T506.C701_DISTRIBUTOR_ID,COUNT ");

        if (strType.equals("50231")) {
          sbQuery.append(" ORDER BY SNAME ");
        } else if (strType.equals("50230") || strType.equals("50234")) {
          sbQuery.append(" ORDER BY DNAME, SNAME ");
        }
      }
      log.debug("sbQuery" + sbQuery.toString());

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      log.debug("Size .... " + alReturn.size());

    return alReturn;
  } // End of loadReturnsReport

  /**
   * loadReturnsReportDrilldown - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadReturnsReportDrilldown(String strSetId, String strId, String strFromDt,
      String strToDt) throws AppError {
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    strSetId = strSetId.equals("") ? "0" : strSetId;
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT C506_RMA_ID RAID, C506_CREDIT_DATE CDATE ,");
      sbQuery.append(" C506_COMMENTS COMMENTS");
      sbQuery.append(" FROM T506_RETURNS ");
      sbQuery.append(" WHERE C506_STATUS_FL = '2' ");
      sbQuery.append(" AND C506_VOID_FL IS NULL ");
      sbQuery.append(" AND C5040_PLANT_ID = '" + getCompPlantId() + "'");
      sbQuery.append(" AND C506_CREDIT_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strDateFmt + "') AND DECODE(C207_SET_ID,NULL,'0',C207_SET_ID) ='");
      sbQuery.append(strSetId);
      sbQuery.append("' AND C701_DISTRIBUTOR_ID ='");
      sbQuery.append(strId);
      sbQuery.append("' ORDER BY C506_RMA_ID ");

      // log.debug("sbQuery"+sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("REPORT", alReturn);
      alReturn = getDistributorList();
      hmReturn.put("DISTRIBUTORLIST", alReturn);
      alReturn = gmProject.loadSetMap("1601");
      hmReturn.put("SETLIST", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadReturnsReportDrilldown", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadReturnsReportDrilldown

  /**
   * loadPartSearchReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPartSearchReport(String strPartNum, String strDesc, String strDepartMentID)
      throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT t205.C205_PART_NUMBER_ID PNUM, t205.C205_PART_NUM_DESC PDESC,");
      sbQuery.append(" t205.C202_PROJECT_ID PROJID, get_project_name(t205.C202_PROJECT_ID) PNAME ");
      sbQuery.append(" ,get_code_name(t205.C205_PRODUCT_FAMILY) PFLY, t205.C205_REV_NUM CURREV ");
      sbQuery.append(" ,t205.C205_ACTIVE_FL AFL, get_code_name(t205.C901_STATUS_ID) STATUS ");
      sbQuery
          .append(" ,get_code_name(t205.C205_PRODUCT_FAMILY) PFLY, get_code_name(t205.C205_PRODUCT_MATERIAL) PMAT ");
      sbQuery
          .append(" ,t2052.C2052_LIST_PRICE LPRICE,get_part_attr_value_by_comp (t205.C205_PART_NUMBER_ID,'200001', "
              + getCompId() + ") NAPPICODE ");
      sbQuery.append(" FROM T205_PART_NUMBER t205, T2052_PART_PRICE_MAPPING t2052 ");
      // Field Sales should not see the parts that are not released for sale
      if (strDepartMentID.equals("S")) {
        sbQuery.append(" , V205D_PART_ATTR ");
      }
      sbQuery.append(" WHERE INSTR (t205.C205_PART_NUMBER_ID, '.', 1, 2) = 0 ");

      if (!strPartNum.equals("")) {
        sbQuery.append(" AND t205.C205_PART_NUMBER_ID like '%");
        sbQuery.append(strPartNum);
        sbQuery.append("%'");
      } else {
        sbQuery.append(" AND upper(t205.C205_PART_NUM_DESC) like upper('%");
        sbQuery.append(strDesc);
        sbQuery.append("%')");
      }
      // Field Sales should not see the parts that are not released for sale
      if (strDepartMentID.equals("S")) {
        // sbQuery.append(" AND gm_pkg_pd_partnumber.get_rfs_info(c205_part_number_id) is not null ");
        sbQuery.append(" AND t205.C205_PART_NUMBER_ID = V205D_PART_ATTR.PNUM ");
      }
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID  = t2052.C205_PART_NUMBER_ID(+) ");
      sbQuery.append(" AND t2052.C1900_COMPANY_ID(+) = " + getCompId());

      sbQuery.append(" ORDER BY t205.C205_PART_NUMBER_ID");

      log.debug("Query for loadPartSearchReport is " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("SEARCHREPORT", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadPartNumSearchReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadPartSearchReport

  /**
   * loadReturnsDetails - This method will return values for an RMA
   * 
   * @param String strRAId
   * @return HashMap
   * @exception AppError
   */
  /*
   * public HashMap loadReturnsDetails(String strRAId, String strAction) throws AppError { HashMap
   * hmReturn = new HashMap(); HashMap hmResult = new HashMap(); GmOperationsBean gmOper = new
   * GmOperationsBean();
   * 
   * PreparedStatement pstmt = null; DBgmDBManagerectionWrapper gmDBManager = new
   * DBgmDBManagerectionWrapper(); gmDBManagerection gmDBManager = null; ResultSet rs = null;
   * HashMap hmTemp = new HashMap(); String strSetId = "";
   * 
   * try{ hmResult = viewReturnSetDetails(strRAId); hmReturn.put("RADETAILS",hmResult);
   * 
   * hmResult = gmOper.loadReturnsSavedSetMaster(strRAId,strAction);
   * hmReturn.put("RAITEMDETAILS",hmResult.get("SETLOAD"));
   * 
   * gmDBManager = gmDBManager.getgmDBManagerection(); StringBuffer sbQuery = new StringBuffer();
   * sbQuery.append(" SELECT NVL(C207_SET_ID,0) SETID "); sbQuery.append(" FROM T506_RETURNS
   * "); sbQuery.append(" WHERE C506_RMA_ID = "); sbQuery.append(" ? " );
   * 
   * sbQuery.append(" AND C506_VOID_FL IS NULL "); log.debug("Query to check Set ID is " +
   * sbQuery.toString());
   * 
   * pstmt = gmDBManager.prepareStatement(sbQuery.toString()); pstmt.setString(1,strRAId); hmTemp =
   * (HashMap)gmDBManager.querySingleRecord(pstmt); strSetId = (String)hmTemp.get("SETID");
   * hmReturn.put("SETID",strSetId); }catch(Exception e) { GmLogError.log("Exception in
   * GmCustomerBean:loadReturnsDetails","Exception is:"+e); throw new AppError(e); } finally { try {
   * if (pstmt != null) { pstmt.close(); }//Enf of if (gmDBManager != null) if(gmDBManager!=null) {
   * gmDBManager.close(); /* closing the gmDBManagerections } } catch(Exception e) { throw new
   * AppError(e); }//End of catch finally { pstmt = null; gmDBManager = null; gmDBManager = null; }
   * } return hmReturn; } //End of loadReturnsDetails
   */

  /**
   * processBackOrder - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap processBackOrder(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    String strBeanMsg = "";
    String strPrepareString = null;
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParam.get("TRANSTYPE"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    HashMap hmReturn = new HashMap();


    gmDBManager.setPrepareString("GM_PROCESS_BACKORDER", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);

    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strUserId);

    gmDBManager.execute();

    strBeanMsg = gmDBManager.getString(3);
    gmDBManager.commit();
    // log.debug("strBeanMsg:"+strOrderId);

    hmReturn.put("MSG", strBeanMsg);

    // To Save the Insert part number details in Back order screen using JMS
    HashMap hmJMSParam = new HashMap();
    hmJMSParam.put("TXNID", strOrderId);
    hmJMSParam.put("TXNTYPE", strTxnType);
    hmJMSParam.put("STATUS", "93342");
    hmJMSParam.put("USERID", strUserId);
    hmJMSParam.put("COMPANYINFO", strCompanyInfo);
    gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);
    return hmReturn;
  } // End of processBackOrder

  /**
   * loadPendingTransfer - This method will
   * 
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList loadPendingDummyConsignmnet() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    ResultSet rs = null;


    gmDBManager.setPrepareString("GM_CS_FCH_PENDING_DUMMYCONSG", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR); // register out
    // parameter
    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(1);
    alResult = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    return alResult;
  } // End of loadPendingTransfer

  /**
   * loadLoanerReconDashboard - This method will
   * 
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList loadLoanerReconDashboard() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    ResultSet rs = null;

    gmDBManager.setPrepareString("GM_CS_FCH_LOANER_RECON_DASH", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR); // register out
    // parameter
    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(1);
    alResult = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    return alResult;
  } // End of loadLoanerReconDashboard

  /**
   * loadCartForConsign - This method will use to view the part details in item consignment cart
   * Added get_partnumber_qty to get available qty from inventory
   * 
   * @param String strPartNum
   * @return HashMap
   * @exception AppError
   * @see AppError
   */
  public HashMap loadCartForConsign(String strPartNum) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("PARTNUM", strPartNum);
    return loadCartForConsign(hmParam);
  }

  /**
   * loadCartForConsign - This method will use to view the part details in item consignment cart
   * Added get_partnumber_qty to get available qty from inventory
   * 
   * @param String strPartNum
   * @return HashMap
   * @exception AppError
   * @see AppError
   */
  public HashMap loadCartForConsign(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strBillToPrntCompId = GmCommonClass.parseNull((String) hmParam.get("PARENTCOMPID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    StringTokenizer strTok = new StringTokenizer(strPartNum, ",");
    String strPartCompFiltr = "";
    String strCompanyID = getCompId();
    GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
    String strICTDistId = "";



    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      while (strTok.hasMoreTokens()) {
        strPartNum = strTok.nextToken();
        StringBuffer sbQuery = new StringBuffer();
        strPartCompFiltr = gmPartSearchBean.getPartCompanyFilter(strPartNum, "T205");
        sbQuery.append(" SELECT T205.C205_PART_NUMBER_ID ID ");
        if (strType.equals("400085") || strType.equals("400086")) {
          strICTDistId =
              GmCommonClass.getRuleValueByCompany(strBillToPrntCompId, "PRIMARY-DIST", getCompId());
          sbQuery
              .append(", NVL(get_account_part_pricing (NULL, T205.c205_part_number_id, get_distributor_inter_party_id ('"
                  + strICTDistId + "')),0) PRICE, ");
        } else {
          sbQuery.append(", T2052.C2052_EQUITY_PRICE PRICE, ");
        }
        sbQuery
            .append(" T205.C205_PART_NUM_DESC PDESC , GET_QTY_IN_STOCK(T205.C205_PART_NUMBER_ID) STOCK, ");
        sbQuery.append(" GET_QTY_IN_QUARANTINE(T205.C205_PART_NUMBER_ID) QSTOCK, ");
        // BSTOCK is a duplicat value for BLK_STOCK, need to fix this in JSPs, modified this for a
        // production move as a temporary fix
        sbQuery
            .append("  GET_QTY_IN_BULK(T205.C205_PART_NUMBER_ID) BLK_STOCK, GET_QTY_IN_BULK(T205.C205_PART_NUMBER_ID) BSTOCK,");
        sbQuery
            .append(" (get_partnumber_qty(T205.C205_PART_NUMBER_ID,90812) - GET_QTY_ALLOCATED(T205.C205_PART_NUMBER_ID,50542)) RTN_STOCK, ");
        sbQuery.append(" GET_QTY_IN_PACKAGING(T205.C205_PART_NUMBER_ID) REPACK_STOCK, gm_pkg_op_process_inhouse_txn.get_part_own_compid(t205.c205_part_number_id) OWN_COMP"); //own_comp added for PC-5635 
        sbQuery.append(" FROM T205_PART_NUMBER T205 ,T2052_PART_PRICE_MAPPING T2052 ");
        sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID = '");
        sbQuery.append(strPartNum);
        sbQuery.append("'");
        sbQuery.append(" AND T205.C205_PART_NUMBER_ID  = T2052.C205_PART_NUMBER_ID(+) ");
        sbQuery.append(strPartCompFiltr);
        sbQuery.append(" AND T2052.C1900_COMPANY_ID(+) = '");
        sbQuery.append(strCompanyID);
        sbQuery.append("'");
        sbQuery.append(" AND t2052.c2052_void_fl(+) is null ");
        log.debug(" Query for Loading Consignment cart " + sbQuery.toString());
        hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
        hmResult.put(strPartNum, hmReturn);
      }

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadCartForConsign", "Exception is:" + e);
    }
    return hmResult;
  } // End of loadCartForConsign

  /**
   * getAccSummary - This method will return the summary info for the Account. Currently used in
   * Order Screen
   * 
   * @param String strAccId
   * @return HashMap
   * @exception AppError
   */
  public HashMap getAccSummary(String strAccId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    String strGpoId = "";
    HashMap hmParam = new HashMap();
    ArrayList alConstructs = new ArrayList();
    ArrayList alRules = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT AD_NAME ADNAME, REGION_NAME RGNAME,DECODE( '" + getComplangid()
        + "', '103097',NVL(D_NAME_EN, D_NAME), D_NAME) DNAME, D_ID DISTID, TER_NAME TRNAME,  ");
    sbQuery.append(" get_party_name(t704d.c101_rpc) rpcname , get_party_name(t704d.c101_health_system) hlthname, ");//PC-2376  Add new fields in Account Pricing Report
    sbQuery.append("DECODE( '" + getComplangid()
        + "', '103097',NVL(REP_NAME_EN, REP_NAME), REP_NAME) RPNAME, ");
    sbQuery.append(" REP_ID REPID , T740.C101_PARTY_ID PRID ");
    sbQuery.append(" ,get_party_name(T740.C101_PARTY_ID) PRNAME, T740.C740_GPO_CATALOG_ID GPOID ");
    sbQuery.append(" ,get_account_name(V700.AC_ID) ANAME ,V700.AC_ID ACCID ");
    sbQuery.append(" ,nvl(GET_ACCOUNT_ATTRB_VALUE (AC_ID,'91982'),'0') SHIPMODE");
    sbQuery.append(" ,nvl(GET_ACCOUNT_ATTRB_VALUE (AC_ID,'105080'),'0') SHIPCARR");
    sbQuery.append(" ,get_party_name(t704d.c101_idn) GPRAFF ");
    sbQuery
        .append(" ,get_party_name(t704d.c101_gpo) HLCAFF , get_account_type(V700.AC_ID) ACCTYPE ");
    sbQuery.append(" ,GET_CODE_NAME(T704.c901_CURRENCY)  ACC_CURRENCY ");

    // sbQuery
    // .append(" ,NVL(TRIM(GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE (V700.AC_ID, '5507'))), '') ACC_CURRENCY ");
    // // 5507
    // -
    // Currency
    // Value,
    // based
    // on
    // account,
    // getting
    // currency
    sbQuery.append(" , T704.C704_COMMENTS ACCCOMMENTS ");
    // sbQuery.append(" , GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE(AC_ID,'903103')) contract ");//
    // 903103:
    // Contract;
    // 903100:
    // Yes
    sbQuery
        .append(", GET_CODE_NAME(DECODE(t704d.c901_contract,0,'',t704d.c901_contract)) CONTRACT ");
    sbQuery.append(" ,GET_ACCOUNT_ATTRB_VALUE (T704.c704_account_id, 10306194) CIG ");

    sbQuery.append(" , T704.C101_PARTY_ID partyId ");
    // NET NUMBER CHANGE
    sbQuery.append(" , T704.c101_DEALER_ID DEALERID");
    sbQuery.append(" , DECODE( '" + getComplangid()
        + "', '103097',NVL(DEALER_NM_EN, DEALER_NM), DEALER_NM) DEALERNM");
    sbQuery.append(" , GET_CODE_NAME(t101_inv.c901_invoice_closing_date) CLOSINGDATE");
    sbQuery
        .append(" FROM V700_TERRITORY_MAPPING_DETAIL V700, T740_GPO_ACCOUNT_MAPPING T740 ,  T704_Account T704, t704d_account_affln t704d, t101_party t101");
    sbQuery.append(" , t101_party_invoice_details T101_INV");
    sbQuery.append(" WHERE AC_ID = '");
    sbQuery.append(strAccId);
    sbQuery.append("' AND V700.AC_ID = T740.C704_ACCOUNT_ID (+) ");
    sbQuery.append(" AND V700.AC_ID = T704.C704_ACCOUNT_ID (+) ");
    sbQuery.append(" AND t704.C704_ACCOUNT_ID = t704d.C704_ACCOUNT_ID (+) ");
    sbQuery.append(" AND v700.dealer_id     = t101_inv.c101_party_id (+) ");
    sbQuery.append(" AND v700.dealer_id     = t101.c101_party_id (+) ");
    sbQuery.append(" AND NVL(t101.c101_party_id,-999)   = NVL(T101_INV.c101_party_id,-999) ");
    sbQuery.append(" AND t704d.C704D_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND t740.C740_VOID_FL (+) is null ");
    sbQuery.append(" AND t704.C704_VOID_FL (+) is null ");
    sbQuery.append(" AND t101_inv.c101_void_fl (+) IS NULL ");
    sbQuery.append(" AND t101.c101_void_fl (+) IS NULL ");
    sbQuery.append(" AND T704.c1900_company_id = ");
    sbQuery.append(getCompId());

    log.debug(sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("ACCSUM", hmResult);
    strGpoId = GmCommonClass.parseNull((String) hmResult.get("PRID"));

    if (!strGpoId.equals("")) {
      hmParam.put("PID", strGpoId);
      alConstructs = gmSales.reportGPOConstructList(hmParam);
      hmReturn.put("CONSTRUCTS", alConstructs);
    }
    gmDBManager.close();
    return hmReturn;
  } // End of getAccSummary

  /**
   * getPartNCMRs - This method will return Order Details.
   * 
   * @param String PartNum
   * 
   * @return RowSetDynaClass
   * @exception AppError
   **/

  public String getOrderDetailsForParts(HashMap hmParam) throws AppError {

    RowSetDynaClass rdResult = null;
    String strJsonData="";
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strOrderToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strOrderFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strOrderType = GmCommonClass.parseZero((String) hmParam.get("ORDTYPE"));
    String strRepId = GmCommonClass.parseZero((String) hmParam.get("REP"));
    String strDistId = GmCommonClass.parseZero((String) hmParam.get("DIST"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_order_master.gm_fch_part_order_det", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.CLOB);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strOrderToDt);
    gmDBManager.setString(3, strOrderFromDt);
    gmDBManager.setInt(4, Integer.parseInt(strOrderType));
    gmDBManager.setInt(5, Integer.parseInt(strRepId));
    gmDBManager.setInt(6, Integer.parseInt(strDistId));
    gmDBManager.execute();
  //PC-3968 - display tag to dhtmlx grid 
    strJsonData =  GmCommonClass.parseNull(gmDBManager.getString(7));
    gmDBManager.close();
log.debug("strJsonData===="+strJsonData);
    return strJsonData;

  }// End of getPartNCMRs

  /**
   * Description : This procedure is used to update the Notes on paperwork When the action from
   * Inhouse Set Status has been selected as Edit on Paperwork Author : Rajkumar
   */
  public void updateNotes(String strConsignID, String strComments, String strUserid)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_consignment.gm_sav_consign_notes", 3);
    gmDBManager.setString(1, strConsignID);
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strUserid);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * <b>Description</b>: This method is used to fetch Active Employees who have placed orders.
   * 
   * @author Rick Schmid
   * @return <b>ArrayList</b> - Active Employees who have placed orders.
   */
  public ArrayList fetchEmployeeWithOrders() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alActive = new ArrayList();
    ArrayList alSeperator = new ArrayList();
    ArrayList alInActive = new ArrayList();
    ArrayList alResult = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_common.gm_fch_order_by_user", 3);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();

    // Load Array lists
    alActive = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    alSeperator = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    alInActive = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    alResult.addAll(alActive);
    alResult.addAll(alSeperator);
    alResult.addAll(alInActive);

    gmDBManager.close();

    return alResult;
  }

  /**
   * Description : This method is used to fetch data from the Order Attribute table. For the Revenue
   * Recognition change. Since this was already in production, decision was taken not to obsolete
   * this Author : Mihir
   */
  public ArrayList fetchRevenuetrackInfo(String strOrderID) throws AppError {
	 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    //This Changes added for PMT-53710 fetch the PO and DO details  
    gmDBManager.setPrepareString("gm_pkg_ac_ar_revenue_sampling.gm_fch_revenue_sampling_dtls", 2);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); 
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * Description : This method is used to fetch data from the Order Attribute table. It outer-joins
   * the Code Lookup table and Order ID can also be sent as null or "" to get the rows for
   * displaying on the screens for insert mode Author : James
   */
  public ArrayList fetchOrderAttributeInfo(String strOrderID, String strCodeGroup) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("GM_PKG_AC_ORDER.gm_fch_order_attrib_info", 3);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strCodeGroup);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // header
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alResult;
  }

  /**
   * updateItemOrderPrice - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public void saveOrderAttribute(GmDBManager gmDBManager, String strOrderId, String strUserId,
      String strInputStr) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_sav_rev_sampling", 3);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strInputStr);
    gmDBManager.execute();

  }// End of saveOrderAttribute

  /* THIS METHOD IS USED TO CHECK THE AVAIABLITY OF DO-ID */
  public HashMap checkDoId(String strOrderId) throws AppError {
    HashMap hmDoValues = new HashMap();
    String strCheckDOID = "";
    String strDateOfSurg = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_CHK_DO_ORDERID", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.execute();
    strCheckDOID = GmCommonClass.parseNull(gmDBManager.getString(2));
    strDateOfSurg = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    hmDoValues.put("DOMESSAGE", strCheckDOID);
    hmDoValues.put("SURGDATE", strDateOfSurg);
    return hmDoValues;
  }

  /**
   * checkCustomerPoId - THIS METHOD IS USED TO CHECK THE AVAIABLITY OF CUSTOMER POID
   * 
   * @param String strCustemorPoId
   * @return String
   * @exception AppError
   */
  public String checkCustomerPoId(String strCustemorPoId) throws AppError {

    String strCheckPOID = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_chk_custpoid", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strCustemorPoId);
    gmDBManager.execute();
    strCheckPOID = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();

    return strCheckPOID;
  }

  /**
   * getRepList This method will fetch the SalesRep List
   * 
   * @param No Param
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getSalesRepList(HashMap hmParam) throws AppError {
    ArrayList alReport = new ArrayList();
    String strFilter = GmCommonClass.parseNull((String) hmParam.get("SALESREPFILTER"));
    String strsalesRepList = GmCommonClass.parseNull((String) hmParam.get("SALESREPLIST"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append("SELECT C703_SALES_REP_ID REPID,C703_SALES_REP_ID ID,'S'|| C703_SALES_REP_ID SID");
    sbQuery
        .append(",C701_DISTRIBUTOR_ID DID,C703_SALES_REP_NAME NAME,GET_DIST_REP_NAME(C703_SALES_REP_ID) DNAME ,T101.C101_PARTY_ID PARTYID,T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNAME");
    sbQuery.append(" FROM T703_SALES_REP T703 , T101_PARTY T101");
    sbQuery.append(" WHERE T703.C101_PARTY_ID = T101.C101_PARTY_ID AND T703.C703_VOID_FL IS NULL ");
    if (strFilter.equals("Active")) {
      sbQuery.append(" AND T703.C703_ACTIVE_FL = 'Y' ");
      sbQuery.append("  ");
    }
    if (strsalesRepList.equalsIgnoreCase("ALLSALESREP")) {
      sbQuery
          .append(" AND  T703.C1900_COMPANY_ID  IN (SELECT C1900_COMPANY_ID FROM T1900_COMPANY WHERE C1900_VOID_FL IS NULL");
      sbQuery.append(") ");
    } else {
      sbQuery.append(" AND  T703.C1900_COMPANY_ID = " + getCompId());
    }
    sbQuery.append(" ORDER BY UPPER(C703_SALES_REP_NAME)");
    log.debug("getSalesRepList()>>>" + sbQuery.toString());
    alReport = gmDBManager.queryMultipleRecords(sbQuery.toString());

    gmDBManager.close();
    return alReport;
  }

  /**
   * fetchOrderHoldPer - To Find the hold status of order
   * 
   * @param String strOrderID
   * @return String
   * @exception AppError
   */
  public String fetchOrderHoldPer(String strOrderID) throws AppError {
    String strOrdHoldFl = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_cs_fch_order_hold_per", 2);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strOrdHoldFl);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strOrdHoldFl = GmCommonClass.parseNull((String) gmDBManager.getObject(2));
    gmDBManager.close();

    return strOrdHoldFl;
  }

  /**
   * @method fetchDeliveryNotes
   * @param strOrderId
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchDeliveryNotes(String strOrderId) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_ack_order.gm_fch_delivery_notes", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  public String getOrderAttributeString(String strOrderId) throws AppError {
    String strAttribValue = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_op_ack_order.get_order_attribute_string", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strOrderId);
    gmDBManager.execute();
    strAttribValue = gmDBManager.getString(1);
    gmDBManager.close();
    return strAttribValue;
  }

  /**
   * loadOrderAckDetails - This method will load the order Ack item details
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList loadOrderAckItemDetails(String strOrdId) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT A.C205_PART_NUMBER_ID ID,A.C501_ORDER_ID ORDERID, GET_PARTNUM_DESC(A.C205_PART_NUMBER_ID) PDESC, ");
    sbQuery.append(" A.C502_ITEM_QTY QTY, A.C502_CONTROL_NUMBER CNUM, ");

    sbQuery
        .append(" (CASE WHEN TO_NUMBER(GET_QTY_IN_STOCK(A.C205_PART_NUMBER_ID)) < (TO_NUMBER( A.C502_ITEM_QTY) -TO_NUMBER( get_released_qty (A.C205_PART_NUMBER_ID, A.C501_ORDER_ID, A.C502_ITEM_PRICE)))");
    sbQuery
        .append(" THEN to_char(to_number(A.C502_ITEM_QTY)-to_number(get_released_qty (A.C205_PART_NUMBER_ID, A.C501_ORDER_ID, A.C502_ITEM_PRICE)))");
    sbQuery.append(" ELSE '' end) BCKQTY,");
    sbQuery.append(" get_order_partqty(A.C501_ORDER_ID, A.C205_PART_NUMBER_ID) ORDERQTY, ");
    sbQuery.append(" get_ackord_partqty(A.C501_ORDER_ID, A.C205_PART_NUMBER_ID) ACKORDERQTY, ");
    sbQuery
        .append(" get_released_qty(A.C205_PART_NUMBER_ID,A.C501_ORDER_ID,A.C502_ITEM_PRICE) SHIPQTY, ");
    sbQuery
        .append(" A.C502_ITEM_PRICE PRICE,NVL(A.C502_BASE_PRICE,0) ACTUALPRICE, A.C502_CONSTRUCT_FL CONFL,");
    sbQuery
        .append(" DECODE(A.C502_CONSTRUCT_FL,'Y',nvl(get_rule_value('CONSTRUCT','VATRATE'),'0'),'',nvl(get_rule_value('NON_CONSTRUCT','VATRATE'),'0')) VAT ");
    sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B ");
    sbQuery.append(" WHERE A.C501_ORDER_ID IN ('");
    sbQuery.append(strOrdId);
    sbQuery.append("') AND A.C502_VOID_FL IS NULL ");
    sbQuery.append(" AND A.C502_DELETE_FL IS NULL ");
    sbQuery.append(" AND A.C501_ORDER_ID = B.C501_ORDER_ID ");
    sbQuery.append(" ORDER BY CONFL,SHIPQTY");
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug(" Query to get the order item details is " + sbQuery.toString() + "size of AList "
        + alReturn.size() + "arrarylist data " + alReturn);

    return alReturn;
  }

  /**
   * loadOrderAckDetails - This method will load the order Ack details
   * 
   * @return ArrayList
   * @exception AppError
   */
  public HashMap loadOrderAckDetails(String strOrdId) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT A.C501_ORDER_ID ID ,A.C704_ACCOUNT_ID ACCID");
    sbQuery.append(" ,A.C501_COMMENTS CMENT,A.C501_CUSTOMER_PO PO");
    sbQuery.append(" ,A.C501_ORDER_DATE ORDERDATE, A.C501_REF_ID ACKREFID ");
    sbQuery.append(" ,GET_USER_NAME(A.C501_CREATED_BY) CREATEDBY ");
    sbQuery.append(" ,GET_CODE_NAME(A.C501_DELIVERY_MODE) SHIPMODE ");
    sbQuery.append(" ,GET_CODE_NAME(A.C501_RECEIVE_MODE) ORDERMODE ");
    sbQuery.append(" ,GET_BILL_ADD(A.C704_ACCOUNT_ID) BILLADD");
    sbQuery.append(" ,GET_SHIP_ADD('5021',A.C704_ACCOUNT_ID,NULL,NULL) SHIPADD");
    sbQuery
        .append(" ,DECODE(GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE(A.C704_ACCOUNT_ID,5507)),' ',GET_RULE_VALUE('CURRSYMBOL','CURRSYMBOL'), GET_CODE_NAME(GET_ACCOUNT_ATTRB_VALUE(A.C704_ACCOUNT_ID,5507))) CURRSIGN");
    sbQuery.append(" ,GET_CODE_NAME(B.C704_PAYMENT_TERMS) PAYNM");
    sbQuery.append(" FROM T501_ORDER A,T704_ACCOUNT B");
    sbQuery.append(" WHERE C501_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery.append("' AND A.C704_ACCOUNT_ID=B.C704_ACCOUNT_ID ");
    sbQuery.append(" AND C501_STATUS_FL IS NOT NULL ");
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
    log.debug(" Query to get the order details is " + sbQuery.toString() + "size of AList "
        + hmReturn.size() + "arrarylist data " + hmReturn);

    return hmReturn;
  }

  /**
   * @return ArrayList
   * @throws AppError
   * @author rjayakumar
   * @param None
   * @Purpose To fetch the Acknowledgement Order BO Details
   */
  public ArrayList fetchOrdAckBODet(String strAccId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_backorder_rpt.gm_fch_ord_ack_bo", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAccId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * loadInvoiceAttribute - This method will return the hashmap for the input rule grp id.
   * 
   * @exception AppError
   */
  public HashMap loadInvoiceAttribute(String strRuleGrpId) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = null;
    HashMap hmTemp = null;
    HashMap hmReturn = new HashMap();
    sbQuery
        .append(" SELECT C906_RULE_ID NAME,C906_RULE_VALUE VALUE,C906_RULE_GRP_ID GRPVALUE FROM t906_rules ");
    sbQuery.append(" where c906_rule_grp_id = '");
    sbQuery.append(strRuleGrpId);
    sbQuery.append("' ");

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    if (alReturn != null) {
      for (Iterator it = alReturn.iterator(); it.hasNext();) {
        hmTemp = (HashMap) it.next();
        hmReturn.put(hmTemp.get("NAME"), hmTemp.get("VALUE"));
      }
    }
    return hmReturn;
  }// end of loadInvoiceAttribute

  /**
   * loadOrderAttribute - This method is used to load the attribute values of an order
   * 
   * @param String strOrdId
   * @param String strOrdType
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOrderAttribute(String strOrdId, String strOrdType) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = null;
    HashMap hmTemp = null;
    HashMap hmReturn = new HashMap();
    HashMap hmParentOrderId = new HashMap();
    String strParentOrderId = "";
    /*
     * If the order type is back order, then we are getting the parent order id to get the attribute
     * type and value corresponding to the order id 2525: Back Order
     */
    if (strOrdType.equals("2525")) {
      sbQuery.append(" select c501_parent_order_id parentorderid from T501_ORDER ");
      sbQuery.append(" where c501_order_id = '");
      sbQuery.append(strOrdId);
      sbQuery.append("' AND c501_void_fl IS NULL ");
      hmParentOrderId = gmDBManager.querySingleRecord((sbQuery.toString()));
      strParentOrderId = GmCommonClass.parseNull((String) hmParentOrderId.get("PARENTORDERID"));
      sbQuery.setLength(0);
      // For the complete back order(insufficient shelf qty), parent order Id will be null.
      strOrdId = strParentOrderId.equals("") ? strOrdId : strParentOrderId;
    }

    sbQuery
        .append(" select get_code_name_alt(c901_attribute_type) NAME, c501A_attribute_value VALUE from T501A_ORDER_ATTRIBUTE ");
    sbQuery.append(" where c501_order_id = '");
    sbQuery.append(strOrdId);
    sbQuery.append("' AND c501a_void_fl IS NULL ");

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    // Iterating each records in the result and getting the values of keys, "NAME" and "VALUE" to
    // hmTemp
    if (alReturn != null) {
      for (Iterator it = alReturn.iterator(); it.hasNext();) {
        hmTemp = (HashMap) it.next();
        hmReturn.put(hmTemp.get("NAME"), hmTemp.get("VALUE"));
      }
    }
    return hmReturn;
  }

  /*
   * To fetch the order change price flag
   * 
   * @return String
   * 
   * @param order id
   */
  public String fetchOrderChangePriceFl(String strOrderID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strChangePriceFl = "";
    gmDBManager.setFunctionString("gm_pkg_op_do_process_report.get_order_change_price_status", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strOrderID);
    gmDBManager.execute();
    strChangePriceFl = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strChangePriceFl;
  }

  /*
   * To Update the order as hold due to Price Descrepency
   * 
   * @param HashMap
   */
  public void updateHoldStatus(HashMap hmParams) throws AppError {
    
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO());
    String strPriceDisc = "";
    String strHoldReason = "";
    String strDefaultDoHoldCmt = "";
    String strHoldFl = GmCommonClass.parseNull((String) hmParams.get("HOLDFLAG"));
    String strOrderId = GmCommonClass.parseNull((String) hmParams.get("ORDERID"));
    String strOpt = GmCommonClass.parseNull((String) hmParams.get("STROPT"));
    String strUserId = GmCommonClass.parseZero((String) hmParams.get("USERID"));
    String strSpecificComments = GmCommonClass.parseNull((String) hmParams.get("SPECIFIC_COMMENTS"));
      
    // to call the hold flag information.
    // For Quote, not need to hold the order by default. It should happen only if we click on Hold
    // button

    if (!strOpt.equals("QUOTE")) {
      strPriceDisc = fetchOrderHoldPer(strOrderId);// To get the hold status
      if (strPriceDisc.equals("Y")) {
        // get the default comments and reason from rules.
        strDefaultDoHoldCmt = strSpecificComments.equals("")?
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("DO_AUTO_COMMENTS", "DO_AUTO_HOLD")):strSpecificComments;
        strHoldReason =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("DO_CANCEL_REASON", "DO_AUTO_HOLD"));
         
        gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_cs_sav_order_hold", 5);
        gmDBManager.setString(1, strOrderId); 
        gmDBManager.setString(2, strHoldReason);
        gmDBManager.setInt(3, 414553); 
        gmDBManager.setString(4, strDefaultDoHoldCmt); 
        gmDBManager.setString(5, strUserId); 
        gmDBManager.execute();
        gmDBManager.commit();
      }
    }
  }

  /*
   * To save the BBA Order(Ack to BBA)
   * 
   * @param gmDBManager , strOrderId ,strUserId
   */
  public void saveBBAOrder(GmDBManager gmDBManager, String strOrderId, String strUserId)
      throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_ack_order.gm_op_save_bba_order", 2);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
  }


  /**
   * @method fetchReservedQty - To Fetch the Reserved Quantity for the Part
   * @param strOrderId
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchReservedQty(String strOrderId) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse.gm_fch_reserved_qty", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * checkOrderId - THIS METHOD IS USED TO CHECK THE AVAIABLITY OF ORDER ID
   * 
   * @param String strOrderId
   * @return String
   * @exception AppError
   */
  public String checkOrderId(String strOrderId) throws AppError {
    String strOrderCount = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_chk_orderId", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.execute();
    strOrderCount = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strOrderCount;
  }

  /**
   * updateAckOrderPrice - To update ack order price when bba order price changed
   * 
   * @param gmDBManager , strOrderId , strInputStr ,strUserId
   * @exception AppError
   */
  public void updateAckItemOrderPrice(GmDBManager gmDBManager, String strOrderId,
      String strInputStr, String strUserId) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_ack_order.gm_save_ack_item_ord_price", 3);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
  }


  /**
   * updateAdjustmentDetails - To update Unit Price, Unit Price Adj Value and Adjustment COde value
   * for Sales Order *
   * 
   * @param gmDBManager , strOrderId , strAdjInputStr ,strUserId
   * @author hreddy
   * @exception AppError
   */
  public void updateAdjustmentDetails(GmDBManager gmDBManager, String strOrderId,
      String strAdjInputStr, String strUserId) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_sm_adj_txn.gm_sav_adj_details", 3);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strAdjInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
  }

  /**
   * syncAccAttribute - This method will Timestamp the Account attribute data to the
   * Order/consingment Attribute.
   * 
   * @param String strTransID,strAccID,strTransType,strUserID
   * @return void
   * @exception AppError
   */
  public void syncAccAttribute(GmDBManager gmDBManager, String strTransID, String strAccID,
      String strTransType, String strUserID) throws AppError {
    log.debug(" strTransID --> " + strTransID + " strAccID " + strAccID);
    gmDBManager.setPrepareString("gm_pkg_op_order_master.gm_sav_sync_acc_attribute", 4);
    gmDBManager.setString(1, strTransID);
    gmDBManager.setString(2, strAccID);
    gmDBManager.setString(3, strTransType);
    gmDBManager.setString(4, strUserID);
    gmDBManager.execute();
  }// End of syncAccAttribute

  /**
   * updateAdjustmentDetailsBBA - To update Unit Price, Unit Price Adj Value and Adjustment COde
   * value for BBA Order *
   * 
   * @param gmDBManager , strOrderId , strUserId
   * @author hreddy
   * @exception AppError
   */
  public void updateAdjustmentDetailsBBA(GmDBManager gmDBManager, String strOrderId,
      String strUserId) throws AppError {

    gmDBManager.setPrepareString("gm_pkg_sm_adj_txn.gm_sav_adj_details_bba", 2);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();

  }

  /**
   * validateFieldsalesLot - TO validate the lot in new order screen while tab out
   * 
   * 
   * @param strOrderId, strPartNum , strCntrlNum , strRepID
   * @author karthik
   * @exception AppError
   */

  public String validateFieldsalesLot(String strOrderId, String strPartNum, String strCntrlNum,
      String strRepID) throws AppError {
    HashMap hmReturn = null;
    log.debug("validateFieldsalesLot...." + strPartNum + "::" + strCntrlNum + "::" + strRepID
        + "::" + strOrderId);
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_field_sales_lot", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setString(3, strCntrlNum);
    gmDBManager.setString(4, strRepID);
    gmDBManager.execute();
    String strErrorMessage = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.close();
    return strErrorMessage;
  }

  
  /**
   * loadOUSOrdDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOUSOrdDetails(String strOrdId, String strLUserId) throws AppError {
    // strLUserId is user id from session
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbquery = new StringBuffer();
    HashMap hmReturn = new HashMap();

    try {
     
    sbquery.append("SELECT C501_ORDER_ID CID, nvl(t501.C501_DISTRIBUTOR_ID,t701.c701_distributor_id) DISTID,c501_ref_id refid,GET_BILL_ADD(t501.c704_account_id) BILLADD ,C501_CREATED_DATE CDATE,");
    sbquery.append("C501_LAST_UPDATED_DATE UDATE,gm_pkg_cm_shipping_info.get_ship_add(C501_ORDER_ID,DECODE(C901_ORDER_TYPE,400085,4000518,400086,4000518,9110,50186,106703,26240435,106704,26240435,50181)) SHIPADD ,");
    sbquery.append(" get_user_name(c501_last_updated_by) name, NULL EDATE,GET_USER_NAME(C501_CREATED_BY) CREATEDBY,get_distributor_name(t701.c701_distributor_id) distnm, GET_ACCOUNT_NAME(T704.C704_ACCOUNT_ID) ACCNM ,");
    sbquery.append("get_distributor_type (T701.C701_DISTRIBUTOR_ID) disttype,get_distributor_inter_party_id(t701.c701_distributor_id) partyid,t501.C501_ORDER_ID REQUEST_ID, C503_INVOICE_ID INVOICEID,GET_CODE_NAME(C501_DELIVERY_CARRIER) SCARR,");
    sbquery.append("GET_CODE_NAME(C501_DELIVERY_MODE) SMODE, C501_DELIVERY_CARRIER SCARRCODEID,C501_DELIVERY_MODE SMODECODEID, C501_TRACKING_NUMBER TRACK, '4' CONSIGNSTATUS, C501_SHIPPING_DATE SDATE,");
    sbquery.append(" GET_LOG_COMMENTS('" + strOrdId + "','1221') LOGCOMMENTS,");
    sbquery.append("T501.C901_ORDER_TYPE TYPE,get_transaction_status(C501_STATUS_FL, '50261') STATUSVALUE,NVL(C501_SHIP_COST,0) SHIPCOST ,c501_comments ictnotes,");
    sbquery.append("DECODE(T501.C501_STATUS_FL,'3',DECODE( GET_DISTRIBUTOR_INTER_PARTY_ID(T701.C701_DISTRIBUTOR_ID),NULL, get_comp_curr_symbol(t501.c1900_company_id),get_rule_value (50218, get_distributor_inter_party_id (T701.c701_distributor_id))),get_comp_curr_symbol(t501.c1900_company_id)) CURRENCY ,");
    sbquery.append("DECODE(GET_DISTRIBUTOR_INTER_PARTY_ID(T701.C701_DISTRIBUTOR_ID),NULL, get_comp_curr_symbol(t501.c1900_company_id),get_rule_value (50218, get_distributor_inter_party_id (T701.c701_distributor_id))) NEWCURRENCY,get_compid_from_distid(T701.C701_DISTRIBUTOR_ID) COMPANYID ,");
    sbquery.append("gm_pkg_op_request_master.get_tag_id(decode(NULL,26240144,c501_ref_id,C501_ORDER_ID)) tagid,C501_CUSTOMER_PO CSTPO,T501.C501_SHIP_TO SHIPTO ,get_set_id_from_cn(C501_ORDER_ID) SETID,get_set_name_from_cn(C501_ORDER_ID) SETNAME,get_set_id_from_cn(C501_ORDER_ID)||' '||get_set_name_from_cn(C501_ORDER_ID) SETIDNAME ,");
    sbquery
    	          .append(" 	GET_USER_NAME("
    	              + strLUserId
    	              + ") LUSERNM , get_rule_value("
    	              + strLUserId
    	              + ",'USERTITLE') UTITLE,t704.c704_ship_attn_to ATTENTIONOVER, t701.c701_ship_country shipcountry,");
    	 sbquery.append("t501.c1910_division_id division_id ,get_company_dtfmt(t501.c1900_company_id) CMPDFMT, t501.c1900_company_id companycd , NULL LOCACCID,T704.C704_ACCOUNT_ID ACCID,GET_CS_SHIP_NAME(T501.C501_SHIP_TO,T501.C501_SHIP_TO_ID) SHIPTONAME ,t501.C501_SHIPPING_DATE req_date ");
    	sbquery.append(" FROM T501_ORDER T501,t704_account t704,t701_distributor t701 where C501_ORDER_ID    = '");
    	  sbquery.append(strOrdId);
    	      sbquery.append("'");
    	      sbquery.append("and t501.c704_account_id     = t704.c704_account_id(+)");
    	      sbquery.append(" AND T501.C501_DISTRIBUTOR_ID = t701.c701_distributor_id ");
    	sbquery.append(" AND C501_VOID_FL            IS NULL AND t704.c704_void_fl       IS NULL and t701.c701_void_fl       is null");
    	
      log.debug("sbQuery ::: " + sbquery);
      hmReturn = gmDBManager.querySingleRecord(sbquery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in loadOUSOrdDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadOUSOrdDetails
}// end of class GmCustomerBean
