package com.globus.custservice.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmITConsignmentDtlsPackSlipBean extends GmBean implements GmConsignmentPackSlipInterface {

	/**
	 *  Code to logger properties
	 */
    Logger log = GmLogger.getInstance(this.getClass().getName());
  
    /**
     * Code for create object common class
     */
    GmCommonClass gmCommon = new GmCommonClass();
 
  public GmITConsignmentDtlsPackSlipBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
  
	/**
	 * default constructor
	 */
  public GmITConsignmentDtlsPackSlipBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	   }
  /**
   * loadConsignDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  @Override
  public HashMap loadConsignDetails(String strConsignId, String strLUserId, GmDataStoreVO gmDataStore) throws AppError {
	////PMT-57366  Loan detail sheet displaying Hospital name in English 
	    // strLUserId is user id from session
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	    HashMap hmReturn = new HashMap();

	    try {
	      sbQuery
	          .append(" SELECT C504_CONSIGNMENT_ID CID,NVL(T504.C701_DISTRIBUTOR_ID,t504.c704_account_id) DISTID, C504_REF_ID REFID,GET_CONSIGN_BILL_ADD(C504_CONSIGNMENT_ID");
	      // sbQuery.append(strConsignId);
	      sbQuery
	          .append(") BILLADD , C504_CREATED_DATE CDATE, C504_LAST_UPDATED_DATE UDATE, gm_pkg_cm_shipping_info.get_ship_add(C504_CONSIGNMENT_ID,decode(c504_type,400085,4000518,400086,4000518,9110,50186,106703,26240435,106704,26240435,50181)");
	      // sbQuery.append(strConsignId);
	      sbQuery
	          .append(") SHIPADD , GET_USER_NAME(C504_LAST_UPDATED_BY) NAME, C504_RETURN_DATE EDATE,GET_USER_NAME(C504_CREATED_BY) CREATEDBY,");
	      sbQuery.append(" GET_DISTRIBUTOR_NAME(T504.C701_DISTRIBUTOR_ID) DISTNM, ");
	      sbQuery.append(" GET_ACCOUNT_NAME(T504.C704_ACCOUNT_ID) ACCNM , ");
	      sbQuery.append(" get_distributor_type (T504.C701_DISTRIBUTOR_ID) disttype, ");
	      sbQuery
	          .append(" GET_DISTRIBUTOR_INTER_PARTY_ID(T504.C701_DISTRIBUTOR_ID) PARTYID, t520.C520_REQUEST_ID REQUEST_ID, c504_stock_transfer_invoice_id INVOICEID, ");
	      sbQuery
	          .append(" GET_CODE_NAME(C504_DELIVERY_CARRIER) SCARR, GET_CODE_NAME(C504_DELIVERY_MODE) SMODE,C504_DELIVERY_CARRIER SCARRCODEID, C504_DELIVERY_MODE SMODECODEID, ");
	      sbQuery
	          .append(" C504_TRACKING_NUMBER TRACK, C504_STATUS_FL CONSIGNSTATUS, C504_SHIP_DATE SDATE, ");
	      sbQuery.append(" GET_LOG_COMMENTS('" + strConsignId + "','1221') LOGCOMMENTS,");
	      sbQuery
	          .append(" C504_type TYPE,get_transaction_status(C504_STATUS_FL, '4110') STATUSVALUE, ");
	      sbQuery.append(" NVL(C504_SHIP_COST,0)  SHIPCOST ,C504_COMMENTS ICTNOTES, ");
	      sbQuery
	          .append(" decode(T504.C504_STATUS_FL,'4',decode( GET_DISTRIBUTOR_INTER_PARTY_ID(T504.C701_DISTRIBUTOR_ID),NULL, get_comp_curr_symbol(t504.c1900_company_id),get_rule_value (50218, get_distributor_inter_party_id (T504.c701_distributor_id))),get_comp_curr_symbol(t504.c1900_company_id)) CURRENCY");
	      sbQuery
	          .append(" ,decode(GET_DISTRIBUTOR_INTER_PARTY_ID(T504.C701_DISTRIBUTOR_ID),NULL, get_comp_curr_symbol(t504.c1900_company_id),get_rule_value (50218, get_distributor_inter_party_id (T504.c701_distributor_id))) NEWCURRENCY,");
	      sbQuery.append(" get_compid_from_distid(T504.C701_DISTRIBUTOR_ID) COMPANYID ");
	      sbQuery
	          .append(" , GM_PKG_OP_REQUEST_MASTER.GET_TAG_ID(decode(C901_REF_TYPE,26240144,C504_REF_ID,C504_CONSIGNMENT_ID)) TAGID, GM_PKG_OP_REQUEST_MASTER.GET_REQUEST_ATTRIBUTE(t504.C520_REQUEST_ID,'1006420') CSTPO, T504.C504_SHIP_TO SHIPTO ");
	      sbQuery
	          .append(" , get_set_id_from_cn(C504_CONSIGNMENT_ID) SETID, get_set_name_from_cn(C504_CONSIGNMENT_ID) SETNAME,get_set_id_from_cn(C504_CONSIGNMENT_ID)||' '||get_set_name_from_cn(C504_CONSIGNMENT_ID) SETIDNAME");
	      sbQuery
	          .append(" , GET_USER_NAME("
	              + strLUserId
	              + ") LUSERNM , get_rule_value("
	              + strLUserId
	              + ",'USERTITLE') UTITLE,t704.c704_ship_attn_to ATTENTIONOVER, t701.c701_ship_country shipcountry");
	      sbQuery.append(", t504.c1910_division_id division_id ");
	      sbQuery
	          .append(", get_company_dtfmt(t504.c1900_company_id) CMPDFMT, t504.c1900_company_id COMPANYCD ");
	      sbQuery
	          .append(",T504.C704_LOCATION_ACCOUNT_ID LOCACCID, T504.C704_ACCOUNT_ID ACCID,GET_CS_SHIP_NAME(T504.C504_SHIP_TO,T504.C504_SHIP_TO_ID) SHIPTONAME ,t520.C520_REQUIRED_DATE REQ_DATE ");
	      sbQuery.append(",t520.C525_Product_Request_Id LOANERREQID");
	      
	      sbQuery.append(", DECODE(T504.C504_SHIP_TO, 4122,T504.c504_ship_to_id,'') SHIPACCNTID  ");   
	      sbQuery.append(", CASE WHEN T504.C504_TYPE = 4110 AND T504.C704_ACCOUNT_ID IS NOT NULL THEN T504.C704_ACCOUNT_ID ELSE '' END CONSIGNACCID ");   
	      sbQuery
	          .append(" FROM T504_CONSIGNMENT T504, t704_account t704, t701_distributor t701, T520_REQUEST t520 ");
	      sbQuery.append(" WHERE C504_CONSIGNMENT_ID = '");
	      sbQuery.append(strConsignId);
	      sbQuery.append("'");
	      sbQuery
	          .append(" AND t504.c704_account_id = t704.c704_account_id(+)  AND t504.C701_DISTRIBUTOR_ID = t701.C701_DISTRIBUTOR_ID(+) ");
	      sbQuery.append(" AND T504.C520_REQUEST_ID = t520.C520_REQUEST_ID (+) ");
	      sbQuery
	          .append(" AND C504_VOID_FL IS NULL AND t704.c704_void_fl IS NULL  AND (t701.c701_void_fl IS NULL OR t504.c701_distributor_id = get_rule_value('DISTRIBUTOR','TAGST')) AND t520.c520_void_fl(+) IS NULL ");
	      log.debug("sbQuery ::: " + sbQuery);
	      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
	    } catch (Exception e) {
	      GmLogError.log("Exception in GmITConsignmentDtlsPackSlipBean : loadConsignDetails", "Exception is:" + e);
	    }
	    return hmReturn;
	  } // End of loadConsignDetails

}
