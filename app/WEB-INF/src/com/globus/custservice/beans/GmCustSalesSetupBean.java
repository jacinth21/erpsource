/*
 * Module: GmCustSalesSetupBean Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 14 Mar 2007 Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.sales.beans.GmTerritoryBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCustSalesSetupBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.
  // this will be removed all place changed with GmDataStoreVO constructor

  public GmCustSalesSetupBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCustSalesSetupBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  ArrayList alRegion = new ArrayList();
  ArrayList alState = new ArrayList();
  ArrayList alCountry = new ArrayList();
  ArrayList alDistTyp = new ArrayList();
  ArrayList alPartyPric = new ArrayList();
  ArrayList alCommTyp = new ArrayList();
  ArrayList alDsgn = new ArrayList(); // designation
  ArrayList alAccountList = new ArrayList(); // ICT Accounts
  ArrayList alDevClass = new ArrayList();

  /**
   * loadDistributor - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadDistributor() throws AppError {
    HashMap hmReturn = new HashMap();
    String strAccountType =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("50257", "ACCOUNTTYPE"));// 50257:
                                                                                    // Inter-Company
                                                                                    // Account
    strAccountType = strAccountType.equals("") ? "70112" : strAccountType;

    GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    alRegion = gmAutoCompleteReportBean.getCodeList("REGN", getGmDataStoreVO());
    alState = gmAutoCompleteReportBean.getCodeList("STATE", getGmDataStoreVO());
    alCountry = gmAutoCompleteReportBean.getCodeList("CNTY", null);
    alDistTyp = gmAutoCompleteReportBean.getCodeList("DSTYP", null);
    alPartyPric = gmAutoCompleteReportBean.reportPartyNameList("7004");
    alCommTyp = gmAutoCompleteReportBean.getCodeList("CNTYP", null);// COMMISSION TYPE
    alAccountList = gmAutoCompleteReportBean.reportAccountByType(strAccountType);

    hmReturn.put("REGION", alRegion);
    hmReturn.put("STATE", alState);
    hmReturn.put("COUNTRY", alCountry);
    hmReturn.put("DISTTYP", alDistTyp);
    hmReturn.put("PARTYPRIC", alPartyPric);
    hmReturn.put("COMNTYPE", alCommTyp);
    hmReturn.put("ACCLIST", alAccountList);
    return hmReturn;
  } // End of loadDistributor

  /**
   * saveDistributor - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveDistributor(HashMap hmParam, String strUsername, String strAction)
      throws AppError {
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";
    String strSavedDistId = "";
    HashMap hmReturn = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strDistNm = (String) hmParam.get("DNAME");
    String strDistNmEn = GmCommonClass.parseNull((String) hmParam.get("DNAMEEN"));
    // Distributor Name (EN) is null then set the distributor name for it
    strDistNmEn = strDistNmEn.equals("") ? strDistNm : strDistNmEn;
    String strDistShortNm = GmCommonClass.parseNull((String) hmParam.get("SHNAME"));
    String strRegion = (String) hmParam.get("REGION");
    String strDistNames = (String) hmParam.get("DNAMES");
    String strContactPerson = (String) hmParam.get("CPERSON");
    String strComnPerc = GmCommonClass.parseNull((String) hmParam.get("COMNPERC"));
    String strDistTyp = (String) hmParam.get("DISTTYP");
    String strPartyPric = (String) hmParam.get("PARTYPRIC");
    String strBillName = (String) hmParam.get("BNAME");
    String strShipName = (String) hmParam.get("SNAME");
    String strBillAdd1 = (String) hmParam.get("BADD1");
    String strShipAdd1 = (String) hmParam.get("SADD1");
    String strBillAdd2 = (String) hmParam.get("BADD2");
    String strShipAdd2 = (String) hmParam.get("SADD2");
    String strBillCity = (String) hmParam.get("BCITY");
    String strShipCity = (String) hmParam.get("SCITY");
    String strBillState = (String) hmParam.get("BSTATE");
    String strShipState = (String) hmParam.get("SSTATE");
    String strBillCountry = (String) hmParam.get("BCOUNTRY");
    String strShipCountry = (String) hmParam.get("SCOUNTRY");
    String strBillZipCode = (String) hmParam.get("BZIP");
    String strShipZipCode = (String) hmParam.get("SZIP");
    String strPhone = (String) hmParam.get("PHONE");
    String strFax = (String) hmParam.get("FAX");
    Date dStartDate = (Date) hmParam.get("START");
    Date dEndDate = (Date) hmParam.get("END");
    String strComments = (String) hmParam.get("COMMENTS");
    String strActiveFl = (String) hmParam.get("AFLAG");
    String strDistId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
    String strCommissionType = (String) hmParam.get("COMMISSIONTYPE");
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));
    String strICTAccountID = GmCommonClass.parseNull((String) hmParam.get("ICTACCID"));
    String strRuleGrpID = "ICTACCOUNTMAP";
    String strInputString = "";

    gmDBManager.setPrepareString("GM_SAVE_DISTRIBUTOR", 35);

    gmDBManager.registerOutParameter(34, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(35, java.sql.Types.CHAR);

    gmDBManager.setString(1, strDistNm);
    gmDBManager.setString(2, strDistNmEn);
    gmDBManager.setInt(3, Integer.parseInt(strRegion));
    gmDBManager.setString(4, strDistNames);
    gmDBManager.setString(5, strContactPerson);
    gmDBManager.setString(6, strBillName);
    gmDBManager.setString(7, strShipName);
    gmDBManager.setString(8, strBillAdd1);
    gmDBManager.setString(9, strShipAdd1);
    gmDBManager.setString(10, strBillAdd2);
    gmDBManager.setString(11, strShipAdd2);
    gmDBManager.setString(12, strBillCity);
    gmDBManager.setString(13, strShipCity);
    gmDBManager.setInt(14, Integer.parseInt(strBillState));
    gmDBManager.setInt(15, Integer.parseInt(strShipState));
    gmDBManager.setInt(16, Integer.parseInt(strBillCountry));
    gmDBManager.setInt(17, Integer.parseInt(strShipCountry));
    gmDBManager.setString(18, strBillZipCode);
    gmDBManager.setString(19, strShipZipCode);
    gmDBManager.setString(20, strPhone);
    gmDBManager.setString(21, strFax);
    gmDBManager.setDate(22, dStartDate);
    gmDBManager.setDate(23, dEndDate);
    gmDBManager.setString(24, strComments);
    gmDBManager.setString(25, strActiveFl);
    gmDBManager.setInt(26, Integer.parseInt(strDistId));
    gmDBManager.setString(27, strUsername);
    gmDBManager.setString(28, strAction);
    if (strComnPerc.equals("")) {
      gmDBManager.setString(29, null);
    } else {
      gmDBManager.setDouble(29, GmCommonClass.parseDouble(Double.parseDouble(strComnPerc)));
    }
    gmDBManager.setString(30, strDistTyp);
    gmDBManager.setString(31, strPartyPric);
    gmDBManager.setString(32, strDistShortNm);
    gmDBManager.setInt(33, Integer.parseInt(strCommissionType));
    gmDBManager.execute();

    strMsg = GmCommonClass.parseNull(gmDBManager.getString(34));
    strSavedDistId = GmCommonClass.parseNull(gmDBManager.getString(35));
    hmReturn.put("DID", strSavedDistId);
    // gmCommonBean.loadDistributorList(); // This will load the
    // distributor list and save it
    // the ArrayList to be used by
    // custservice/Transfer(Commented for OP-793 , as the distributors are fetched based on company
    // ID)
    if (!strLog.equals("")) {
      gmCom.saveLog(gmDBManager, strSavedDistId, strLog, strUsername, "1210");
    }
    // if type is ICS then - we are update the ICS account details.
    if (strDistTyp.equals("70105")) {
      // to form the input String - Party id and selected ICS account id
      strInputString = strPartyPric + "^" + strICTAccountID + "|";
      saveIctAccountMapDtls(gmDBManager, strRuleGrpID, strInputString, strUsername);
    }
    gmDBManager.commit();

    // PC-3178 New Customer not displayed in IML
    hmParam.put("LOCID","");
    hmParam.put("LOCREFID",strSavedDistId);
    hmParam.put("LOCREFTYPE","109461");
    hmParam.put("STRCOMPID",getCompId());
    hmParam.put("STRUSERID",strUsername);
    
    GmAddAccountBean gmAddAccountBean = new GmAddAccountBean(getGmDataStoreVO());
    gmAddAccountBean.updateAccount(hmParam);
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    
    // To remove the Field Sales list from cache server
    gmCacheManager.removePatternKeys("DIST_LIST*" + getCompId() + "*");
    gmCacheManager.removePatternKeys("PARENT_ACCOUNT_LIST*" + getCompId() + "*");
    gmCacheManager.removePatternKeys("LOT_LOCATION_LIST*" + getCompId() + "*");
    gmCacheManager.removePatternKeys("LOT_LOCATION_LIST_109461*" + getCompId() + "*");
    
    return hmReturn;
  }

  /**
   * loadSalesRep - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadSalesRep() throws AppError {
    HashMap hmReturn = new HashMap();
    GmTerritoryBean gmTerr = new GmTerritoryBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
    try {
      alState = gmCommon.getCodeList("STATE", getGmDataStoreVO());
      alCountry = gmCommon.getCodeList("CNTY");
      ArrayList alDistributor = new ArrayList();
      ArrayList alCategory = new ArrayList();
      alCategory = gmCommon.getCodeList("SCAT");
      ArrayList alTerritory = new ArrayList();
      alTerritory = gmTerr.getTerritoryList("UnAssigned");
      alDsgn = gmCommon.getCodeList("DSGTP");
      ArrayList alCompDivList = new ArrayList();
      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));
      ArrayList alDivMapTypeList = new ArrayList();
      alDivMapTypeList = gmCommon.getCodeList("DIVTYP");

      hmReturn.put("DISTRIBUTORLIST", alDistributor);
      hmReturn.put("CATEGORY", alCategory);
      hmReturn.put("STATE", alState);
      hmReturn.put("COUNTRY", alCountry);
      hmReturn.put("TERRITORY", alTerritory);
      hmReturn.put("DSGN", alDsgn);
      hmReturn.put("COMPDIVLIST", alCompDivList);
      hmReturn.put("DIVMAPTYPELIST", alDivMapTypeList);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadSalesRep", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadSalesRep

  /**
   * saveSalesRep - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String saveSalesRep(HashMap hmParam, String strUserId, String strAction) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    String RepId = "";
    String strFirstName = GmCommonClass.parseNull((String) hmParam.get("FIRSTNAME"));
    String strLastName = GmCommonClass.parseNull((String) hmParam.get("LASTNAME"));
    // Rep Name (EN) is null then set the Rep name for it
    String strRepName = strFirstName + " " + strLastName;
    String strRepNameEn = GmCommonClass.parseNull((String) hmParam.get("REPNAMEEN"));
    strRepNameEn = strRepNameEn.equals("") ? strRepName : strRepNameEn;
    String strDistId = (String) hmParam.get("DID");
    String strCatId = (String) hmParam.get("CATID");
    String strRepId = (String) hmParam.get("REPID");
    String strTerrId = (String) hmParam.get("TID");
    String strActiveFl = (String) hmParam.get("AFLAG");
    log.debug("strActiveFl " + strActiveFl);
    String strPartyId = (String) hmParam.get("PARTYID");
    Date dtStartDt = (Date) hmParam.get("STDATE");
    Date dtEndDt = (Date) hmParam.get("ENDDATE");
    String strDsgn = GmCommonClass.parseZero((String) hmParam.get("DSGN"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNAME"));
    String StrLoginCreden = GmCommonClass.parseNull((String) hmParam.get("LOGINCREDEN"));
    String strPrimaryDivId = GmCommonClass.parseNull((String) hmParam.get("PRIMARYDIVID"));

    log.debug(":  hmParam : " + hmParam);
    gmDBManager.setPrepareString("GM_SAVE_SALESREP", 17);

    gmDBManager.setString(1, strFirstName);
    gmDBManager.setString(2, strLastName);
    gmDBManager.setString(3, strRepNameEn);
    gmDBManager.setString(4, strDistId);
    gmDBManager.setInt(5, Integer.parseInt(strCatId));
    gmDBManager.setString(6, strRepId);
    gmDBManager.setInt(7, Integer.parseInt(strUserId));
    gmDBManager.setString(8, strAction);
    gmDBManager.setInt(9, Integer.parseInt(strTerrId));
    gmDBManager.setString(10, strActiveFl);
    gmDBManager.setInt(11, Integer.parseInt(strPartyId));
    gmDBManager.setDate(12, dtStartDt);
    gmDBManager.setDate(13, dtEndDt);
    gmDBManager.setInt(14, Integer.parseInt(strDsgn));
    gmDBManager.setString(15, strUserName);
    gmDBManager.setString(16, StrLoginCreden);
    gmDBManager.setString(17, strPrimaryDivId);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.execute();
    strRepId = gmDBManager.getString(6);
    if (!strLog.equals("")) {
      gmCom.saveLog(gmDBManager, strRepId, strLog, strUserId, "1211");
    }
    gmDBManager.commit();

    log.debug("RepId inside bean " + RepId);
    return strRepId;

  } // end of saveSalesRep

  /**
   * loadEditSalesRep - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadEditSalesRep(String strRepId) throws AppError {
    HashMap hmReturn = new HashMap();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();

      sbQuery
          .append(" SELECT c703_sales_rep_id ID,t102.c102_login_username userloginname, t101p.C101_FIRST_NM FNAME,t101p.C101_LAST_NM LNAME,t703.C703_SALES_REP_NAME_EN REPNAMEEN,	 ");
      sbQuery
          .append(" t703.c701_distributor_id DID,DECODE('"
              + getComplangid()
              + "', '103097', NVL(t701.c701_distributor_name_en, t701.c701_distributor_name), t701.c701_distributor_name) DNAME, t703.c703_phone_number PHONE, ");
      sbQuery
          .append(" t703.c703_rep_category CAT, t703.c702_territory_id TERR_ID,t101p.C101_PARTY_ID PARTYID,");
      sbQuery
          .append(" get_territory_name (t703.c702_territory_id) TERR_NAME, t703.c703_active_fl AFLAG, ");
      sbQuery
          .append(" t703.c703_START_DATE STARTDT,t703.c703_END_DATE ENDDT, t703.c901_DESIGNATION DSGN, get_rep_quota_count(t703.c703_sales_rep_id) QUOTACNT,  get_party_primary_contact(t101p.C101_PARTY_ID,'90452') EMAIL  ");
      sbQuery
          .append("   FROM t703_sales_rep t703,t101_party t101p ,t102_user_login  t102 , t101_user t101 ,t701_distributor t701  ");
      sbQuery.append(" WHERE C703_SALES_REP_ID = '");
      sbQuery.append(strRepId);
      sbQuery.append("' AND t101p.C101_PARTY_ID = t703.C101_PARTY_ID ");
      sbQuery.append(" AND t101p.c101_party_id = t101.c101_party_id(+)");
      sbQuery.append(" AND t101.c101_user_id = t102.c101_user_id(+) ");
      sbQuery.append(" AND t703.c701_distributor_id = t701.c701_distributor_id ");
      log.debug("sbQuery - editload " + sbQuery.toString());
      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadEditSalesRep", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadEditSalesRep

  /**
   * loadEditDistributor - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadEditDistributor(String strDistId) throws AppError {
    HashMap hmReturn = new HashMap();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C701_DISTRIBUTOR_ID ID,  C701_DISTRIBUTOR_NAME NAME,C701_DISTRIBUTOR_NAME_EN NAMEEN, C701_DIST_SH_NAME SHNAME,  ");
      sbQuery
          .append(" C701_REGION REGION, C701_DISTRIBUTOR_INCHARGE INCHARGE, C701_CONTACT_PERSON CPERSON, ");
      sbQuery
          .append(" C701_BILL_ADD1 BADD1, C701_COMMISSION_PERCENT CPERCENT, C701_BILL_NAME BNAME, C701_BILL_ADD2 BADD2,  ");
      sbQuery
          .append(" C701_BILL_STATE BSTATE, C701_BILL_COUNTRY BCOUNTRY, C701_BILL_CITY BCITY, C701_BILL_ZIP_CODE BZIP, ");
      sbQuery
          .append(" C701_FAX FAX, C701_PHONE_NUMBER PHONE,C701_CONTRACT_START_DATE STARTDATE, C701_SHIP_NAME SNAME, ");
      sbQuery
          .append(" C701_CONTRACT_END_DATE ENDDATE, C701_SHIP_ADD1 SADD1, C701_SHIP_ADD2 SADD2, C701_ACTIVE_FL AFL, ");
      sbQuery
          .append(" C701_COMMENTS COMMENTS, C701_SHIP_CITY SCITY, C701_SHIP_STATE SSTATE, C701_SHIP_COUNTRY SCOUNTRY,C1900_PARENT_COMPANY_ID PARENTCOMPID, ");
      sbQuery
          .append(" C701_SHIP_ZIP_CODE SZIP, C701_EMAIL_ID EMAIL ,C901_DISTRIBUTOR_TYPE DISTTYP, ");
      sbQuery.append(" C101_PARTY_INTRA_MAP_ID PARTYPRIC, C901_COMMISSION_TYPE COMNTYPE, ");
      sbQuery.append(" get_rule_value (C101_PARTY_INTRA_MAP_ID, 'ICTACCOUNTMAP') ICTACCID ");
      sbQuery.append(" FROM T701_DISTRIBUTOR ");
      sbQuery.append(" WHERE C701_DISTRIBUTOR_ID = '");
      sbQuery.append(strDistId);
      sbQuery.append("'");

      hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadEditDistributor", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadEditDistributor



  /**
   * loadAccount - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadAccount() throws AppError {
    HashMap hmReturn = new HashMap();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmPartyBean gmParty = new GmPartyBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());

    ArrayList alRepList = gmCust.getRepList();
    ArrayList alPriceCategory = new ArrayList();
    ArrayList alAcc = new ArrayList();
    ArrayList alContFl = new ArrayList();
    ArrayList alContFlAtt = new ArrayList();
    ArrayList alCompId = new ArrayList();
    ArrayList alCollector = new ArrayList();
    ArrayList alPrefCarrier = new ArrayList();
    ArrayList alCategory = new ArrayList();
    ArrayList alTaxType = new ArrayList();
    ArrayList alCompCurrency = new ArrayList();
    ArrayList alInvCloseDt = new ArrayList();

    alState = gmCommon.getCodeList("STATE", getGmDataStoreVO());
    alCountry = gmCommon.getCodeList("CNTY", getGmDataStoreVO());
    ArrayList alPayment = gmCommon.getCodeList("PAYM", getGmDataStoreVO());
    alPriceCategory = gmCommon.getCodeList("PRICE");
    alContFl = gmCommon.getCodeList("CONTFL");
    alCollector = GmCommonClass.getCodeList("COLLTR", getGmDataStoreVO());
    alCategory = GmCommonClass.getCodeList("CATGRY");
    alTaxType = GmCommonClass.getCodeList("TAXTP", getGmDataStoreVO());

    // Account Affiliation in Account Setup
    alAcc = gmCommon.getCodeList("ACTYP", getGmDataStoreVO());
    alContFlAtt = gmCommon.getCodeList("CONATT");
    alCompId = gmCommon.getCodeList("COMP", getGmDataStoreVO());
    // PMT-4359 -- Getting the Code lookup values for DCAR code group to form alPrefCarrier
    alPrefCarrier = gmCommon.getCodeList("DCAR", getGmDataStoreVO());

    alCompCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    alInvCloseDt = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INCLDT"));
    hmReturn.put("REPLIST", alRepList);
    hmReturn.put("STATE", alState);
    hmReturn.put("COUNTRY", alCountry);
    hmReturn.put("PAYMENT", alPayment);
    hmReturn.put("PRICE", alPriceCategory);
    hmReturn.put("ACCTTYPE", alAcc);
    hmReturn.put("ALCONTFL", alContFl);
    hmReturn.put("ALCONTFLATT", alContFlAtt);
    hmReturn.put("ALCOMPID", alCompId);
    hmReturn.put("COLLECTORS", alCollector);
    // PMT-4359 -- Setting alPrefCarrier to the hmReturn object.
    hmReturn.put("PREFERCARRIER", alPrefCarrier);
    hmReturn.put("ACCTCATGRY", alCategory);
    hmReturn.put("ALTAXTYPE", alTaxType);


    // Account Affiliation in Account Setup
    hmReturn.put("ALCOMPCURRENCY", alCompCurrency);
    hmReturn.put("ALINVCLOSEDT", alInvCloseDt);
    return hmReturn;
  } // End of loadAccount

  /**
   * saveAccount - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String saveAccount(HashMap hmParam, String strUsername, String strAction) throws Exception {
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strSavedAcNum = "";

    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));
    String strAccAttInputStr = GmCommonClass.parseNull((String) hmParam.get("ACCATTINPUTSTR"));
    String strRepAccountStr = GmCommonClass.parseNull((String) hmParam.get("REP_ACC_INPUT_STR"));
    String strParentAccountStr =
        GmCommonClass.parseNull((String) hmParam.get("PARENT_ACC_INPUT_STR"));
    //
    String strRepAccountId = "";
    String strParentAccountId = "";

    gmDBManager.setPrepareString("GM_SAVE_ACCOUNT", 6);

    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);

    gmDBManager.setString(1, strRepAccountStr);
    gmDBManager.setString(2, strParentAccountStr);
    gmDBManager.setString(3, strAction);
    gmDBManager.setString(4, strUsername);
    gmDBManager.execute();
    strRepAccountId = GmCommonClass.parseNull(gmDBManager.getString(5));
    strParentAccountId = GmCommonClass.parseNull(gmDBManager.getString(6));
    log.debug(" strRepAccountId ::" + strRepAccountId);
    log.debug(" strParentAccountId ::" + strParentAccountId);
    if (!strLog.equals("")) {
      gmCom.saveLog(gmDBManager, strRepAccountId, strLog, strUsername, "1209");
    }
    if (strAccAttInputStr.length() > 0) {
      saveAccountAttribute(gmDBManager, strRepAccountId, strUsername, strAccAttInputStr, strAction);
      // to sync the account setup screen parameters
      saveSyncAccountAttr(gmDBManager, strRepAccountId, "ACC_SETUP_PARAM", strUsername);
    }
    gmDBManager.commit();
    strSavedAcNum = strRepAccountId;
    
    // PC-3178 New Customer not displayed in IML
    hmParam.put("LOCID","");
    hmParam.put("LOCREFID",strSavedAcNum);
    hmParam.put("LOCREFTYPE","109460");
    hmParam.put("STRCOMPID",getCompId());
    hmParam.put("STRUSERID",strUsername);
    
    GmAddAccountBean gmAddAccountBean = new GmAddAccountBean(getGmDataStoreVO());
    gmAddAccountBean.updateAccount(hmParam);
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    
    // To remove the Account list from cache server
    gmCacheManager.removePatternKeys("REP_ACCOUNT_LIST*" + getCompId() + "*");
    gmCacheManager.removePatternKeys("PARENT_ACCOUNT_LIST*" + getCompId() + "*");
    gmCacheManager.removePatternKeys("LOT_LOCATION_LIST*" + getCompId() + "*");
    gmCacheManager.removePatternKeys("LOT_LOCATION_LIST_109460*" + getCompId() + "*");
    
    return strSavedAcNum;
  } // end of saveAccount

  /**
   * saveAccountAttribute - This method will save account attribute details.
   * 
   * @param gmDBManager, strAccId, strUserId, strAccAttInputStr, strAction
   * @return void
   * @exception AppError
   */
  public void saveAccountAttribute(GmDBManager gmDBManager, String strAccId, String strUserId,
      String strAccAttInputStr, String strAction) throws Exception {
    log.debug(strAccId + ":::" + strUserId + ":::" + strAccAttInputStr + ":::" + strAction);
    gmDBManager.setPrepareString("GM_SAVE_ACCOUNT_ATTRIBUTE", 4);
    gmDBManager.setInt(1, Integer.parseInt(strAccId));
    gmDBManager.setString(2, strAccAttInputStr);
    gmDBManager.setString(3, strAction);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
  } // end of saveAccount


  /**
   * loadEditAccount - This method is to load the details of the account
   * 
   * @param String strId
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadEditAccount(String strId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    String strAcctPricing = GmCommonClass.parseNull(GmCommonClass.getString("LOAD_LIST_PRICE"));
    String strUploadAcctPrice =
        GmCommonClass.parseNull(GmCommonClass.getString("UPLOAD_ACCT_PRICE"));

    HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmRepAccout = fetchRepAccountInfo(strId);
    String strPartyId = GmCommonClass.parseNull((String) hmRepAccout.get("PARTYID"));
    HashMap hmParentAccount = fetchParentAccountInfo(strPartyId);
    // to get the default company avaTax flag
    if (strId.equals("")) {
      String strCompanyAvaTaxFl =
          GmCommonClass.parseNull((GmCommonClass.getRuleValueByCompany(getCompId(), "AVATAX",
              getCompId())));
      hmRepAccout.put("COMPANY_AVATAX_FL", strCompanyAvaTaxFl);
    }
    // to set the default company currency id
    hmRepAccout.put("COMPANY_CURR_ID", strCompanyCurrId);
    hmReturn.put("REP_ACCOUNTDETAILS", hmRepAccout);
    hmReturn.put("PARENT_ACCOUNTDETAILS", hmParentAccount);
    hmResult = loadAccount();

    hmReturn.put("ACCLIST", hmResult.get("ACCLIST"));
    hmReturn.put("REPLIST", hmResult.get("REPLIST"));
    hmReturn.put("DISTRIBUTORLIST", hmResult.get("DISTRIBUTORLIST"));
    hmReturn.put("STATE", hmResult.get("STATE"));
    hmReturn.put("COUNTRY", hmResult.get("COUNTRY"));
    hmReturn.put("PAYMENT", hmResult.get("PAYMENT"));
    hmReturn.put("PRICE", hmResult.get("PRICE"));
    hmReturn.put("GPOLIST", hmResult.get("GPOLIST"));
    hmReturn.put("ACCTTYPE", hmResult.get("ACCTTYPE"));
    hmReturn.put("ALCONTFL", hmResult.get("ALCONTFL"));
    hmReturn.put("ALCONTFLATT", hmResult.get("ALCONTFLATT"));
    hmReturn.put("ALCOMPID", hmResult.get("ALCOMPID"));
    hmReturn.put("ALGPRAFF", hmResult.get("ALGPRAFF"));
    hmReturn.put("ALHLCAFF", hmResult.get("ALHLCAFF"));
    hmReturn.put("COLLECTORS", hmResult.get("COLLECTORS"));
    hmReturn.put("ALTAXTYPE", hmResult.get("ALTAXTYPE"));
    // PMT-4359 -- Setting the resulted HmResult value to the hmReturn object to use in JSP
    hmReturn.put("PREFERCARRIER", hmResult.get("PREFERCARRIER"));
    hmReturn.put("ALPARENTACC", hmResult.get("ALPARENTACC"));
    hmReturn.put("ACCTCATGRY", hmResult.get("ACCTCATGRY"));
    hmReturn.put("ALCOMPCURRENCY", hmResult.get("ALCOMPCURRENCY"));
    hmReturn.put("ALINVCLOSEDT", hmResult.get("ALINVCLOSEDT"));
    return hmReturn;
  } // End of loadEditAccount

  public void sendEmailToNewSalesRep(HashMap hmParam) throws AppError {

    HashMap hmReturn = new HashMap();
    String strCc = "";
    String strTo = "";

    GmEmailProperties emailProps = new GmEmailProperties();
    GmJasperMail jasperMail = new GmJasperMail();
    hmParam.put("EMAILFILENM", "GmNewSalesRepEmail");
    hmParam.put("JASPERNAME", "/GmNewSalesRepEmail.jasper");
    String strEmailPropName = GmCommonClass.parseNull((String) hmParam.get("EMAILFILENM"));
    String strJasperName = GmCommonClass.parseNull((String) hmParam.get("JASPERNAME"));
    String strFirstName = GmCommonClass.parseNull((String) hmParam.get("FNAME"));
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERLOGINNAME"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("ID"));

    hmParam.put("FIRSTNAME", strFirstName + " , welcome to Globus Medical!");
    hmParam.put("REPID", strRepID);
    hmParam.put("USERNAME", strUserName);
    hmParam.put("EMAILID", strUserName + "@globusmedical.com");

    String strSubject =
        GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.SUBJECT);
    strCc = GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.CC);
    strTo = GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.TO);
    emailProps.setCc(strCc);
    emailProps.setRecipients(strTo);
    emailProps.setMimeType(GmCommonClass.getEmailProperty(strEmailPropName + "."
        + GmEmailProperties.MIME_TYPE));
    emailProps.setSender(GmCommonClass.getEmailProperty(strEmailPropName + "."
        + GmEmailProperties.FROM));
    emailProps.setSubject(strSubject);
    jasperMail.setJasperReportName(strJasperName);
    jasperMail.setAdditionalParams(hmParam);
    jasperMail.setReportData(null);
    jasperMail.setEmailProperties(emailProps);
    hmReturn = jasperMail.sendMail();
  }

  /**
   * saveIctAccountMapDtls - This method will save ICT account mapping details for ICS type.
   * 
   * @param gmDBManager, strRuleGroupID, strAccICTAcctInputStr, strUserid
   * @return void
   * @exception AppError
   */
  public void saveIctAccountMapDtls(GmDBManager gmDBManager, String strRuleGrpId,
      String strIctAccInputString, String strUserID) throws AppError {
    log.debug("strRuleGrpId --> " + strRuleGrpId + " strInputString --> " + strIctAccInputString);
    gmDBManager.setPrepareString("gm_pkg_cm_rule_params.gm_save_ruleparams", 3);
    gmDBManager.setString(1, strRuleGrpId);
    gmDBManager.setString(2, strIctAccInputString);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
  } // end of saveIctAccountMapDtls

  /**
   * fetchAcctShipDtls - This method will fetch the Account shipping Details *
   * 
   * @param HashMap - hmParam
   * @return RowSetDynaClass
   * @exception AppError , Exception
   **/
  public HashMap fetchAcctShipDtls(HashMap hmParam) throws AppError, Exception {
    HashMap hmReturn = new HashMap();
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_report_account_ship_setup", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAccId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  } // End of fetchAcctShipDtls

  /**
   * saveAcctShipSetup - This Method is used to save Account Shipping Details
   * 
   * @param HashMap hmParam
   * @return void
   * @exception AppError
   **/
  public void saveAcctShipSetup(HashMap hmParam) throws AppError {
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strShipAttnTo = GmCommonClass.parseNull((String) hmParam.get("SHIPATTNTO"));
    String strShipName = GmCommonClass.parseNull((String) hmParam.get("SHIPNAME"));
    String strShipAdd1 = GmCommonClass.parseNull((String) hmParam.get("SHIPADD1"));
    String strShipAdd2 = GmCommonClass.parseNull((String) hmParam.get("SHIPADD2"));
    String strShipCity = GmCommonClass.parseNull((String) hmParam.get("SHIPCITY"));
    String strShipState = GmCommonClass.parseNull((String) hmParam.get("SHIPSTATE"));
    String strShipCountry = GmCommonClass.parseNull((String) hmParam.get("SHIPCOUNTRY"));
    String strShipZip = GmCommonClass.parseNull((String) hmParam.get("SHIPZIP"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPrefCarrier = GmCommonClass.parseNull((String) hmParam.get("CARRIER"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // PMT-4359 -- Need to save preferred Carrier value along with these values for an account in
    // t704_account table as 11th parameter
    gmDBManager.setPrepareString("gm_sav_account_ship_setup", 11);
    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strShipAttnTo);
    gmDBManager.setString(3, strShipName);
    gmDBManager.setString(4, strShipAdd1);
    gmDBManager.setString(5, strShipAdd2);
    gmDBManager.setString(6, strShipCity);
    gmDBManager.setInt(7, Integer.parseInt(strShipState));
    gmDBManager.setInt(8, Integer.parseInt(strShipCountry));
    gmDBManager.setString(9, strShipZip);
    gmDBManager.setString(10, strUserId);
    gmDBManager.setString(11, strPrefCarrier);
    gmDBManager.execute();
    gmDBManager.commit();
  }// End of saveAcctShipSetup

  /**
   * fetchAccountAttribute - This method will return Account attribute
   * 
   * @param HashMap
   * 
   * @exception AppError
   */
  public HashMap fetchAccountAttribute(HashMap hmParam) throws AppError {

    String strAccId = GmCommonClass.parseZero((String) hmParam.get("ACCID"));
    String strAccAttrType = GmCommonClass.parseNull((String) hmParam.get("ACCATTRTYPE"));
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT C704A_ATTRIBUTE_VALUE ATTRI_VALUE,  get_code_name(C704A_ATTRIBUTE_VALUE)  ATTRI_NAME ");
    sbQuery.append(" FROM T704A_ACCOUNT_ATTRIBUTE ");
    sbQuery.append(" WHERE C704A_VOID_FL IS NULL ");
    if (!strAccAttrType.equals("")) {
      sbQuery.append(" AND C901_ATTRIBUTE_TYPE = ");
      sbQuery.append(strAccAttrType);
    }
    if (!strAccId.equals("0")) {
      sbQuery.append(" AND C704_ACCOUNT_ID = '");
      sbQuery.append(strAccId);
      sbQuery.append("' ");
    }
    log.debug(" Query for fetchAccountAttribute " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
    gmDBManager.close();
    return hmReturn;
  }// end of fetchAccountAttribute

  /**
   * saveAccAttributes - This method will save account attribute details in account_attribute
   * table..
   * 
   * @param String strAccId
   * @param String strUserId
   * @param String strInputString
   * @param String strAction
   * @return
   */
  public void saveAccAttributes(String strAccId, String strUserId, String strInputString,
      String strAction) throws Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveAccountAttribute(gmDBManager, strAccId, strUserId, strInputString, strAction);
    gmDBManager.commit();
  }

  /**
   * loadMapAccountAttribute - This method will load code group details for an account from account
   * attribute table.
   * 
   * @param String strAccId
   * @param String strAccGrp
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList loadMapAccountAttribute(String strAccId, String strAccGrp) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append("  SELECT  T901.C901_CODE_ID  ATRBTYPE, t704a.C704A_ATTRIBUTE_VALUE ATRBVALUE");
    sbQuery
        .append(" FROM (select C901_ATTRIBUTE_TYPE,C704A_ATTRIBUTE_VALUE from T704A_ACCOUNT_ATTRIBUTE WHERE C704_ACCOUNT_ID = '");
    sbQuery.append(strAccId);
    sbQuery.append("' AND C704A_VOID_FL IS NULL) t704a,T901_CODE_LOOKUP T901 ");
    sbQuery.append(" WHERE T901.C901_CODE_ID = T704A.C901_ATTRIBUTE_TYPE (+)");
    sbQuery.append(" AND t901.C901_CODE_GRP = '");
    sbQuery.append(strAccGrp);
    sbQuery.append("' AND T901.C901_ACTIVE_FL != 0");

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  /**
   * This method used to fetch the account information details
   * 
   * @param strRepAccountId
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchRepAccountInfo(String strRepAccountId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_acct_trans.gm_fch_rep_account_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRepAccountId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * This method used to fetch the parent account information details
   * 
   * @param strPartyId
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchParentAccountInfo(String strPartyId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_acct_trans.gm_fch_parent_account_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * This method is used to sync the account attribute details to all other party
   * 
   * @param gmDBManager
   * @param strAccId
   * @param strRuleId
   * @param strUserId
   * @throws AppError
   */
  public void saveSyncAccountAttr(GmDBManager gmDBManager, String strAccId, String strRuleId,
      String strUserId) throws AppError {
    String strAccountAttributeType = "";
    strAccountAttributeType = GmCommonClass.getRuleValue(strRuleId, "ACC_ATTRIBUTE_TYPE");
    gmDBManager.setPrepareString("gm_pkg_sm_acct_trans.gm_sav_sync_acct_attr", 3);
    gmDBManager.setInt(1, Integer.parseInt(strAccId));
    gmDBManager.setString(2, strAccountAttributeType);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
  }

  /**
   * savSalesRepDivMap - This method is used to save Sales Rep Devision Mapping details.
   * 
   * @param HashMap hmParam Holds the input string(Division, Type combination as string), Sales Rep
   *        Id and Party Id
   * @throws AppError
   * 
   */
  public void savSalesRepDivMap(HashMap hmParam) throws AppError {

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_sav_salesrep_div_map", 3);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strRepID);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * This method used to fetch the sales rep division mapping details
   * 
   * @param strPartyId
   * @return HashMap
   * @throws AppError
   */
  public ArrayList fetchSalesRepDivMap(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("ID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_fch_salesrep_div_map", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRepId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alResult;
  }

  /**
   * This method will fetch the account type for the corresponding Account
   * 
   * @author
   * @param String
   */
  public String getAccountType(String strAccountId) throws AppError {
    String strAccType = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_account_type", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strAccountId);
    gmDBManager.execute();
    strAccType = gmDBManager.getString(1);
    gmDBManager.close();

    return strAccType;
  }
  
  /**
   * This method will fetch and send Email to the new account details
   * 
   * @param String 
   */
  public void sendRepAcctEmail(HttpServletRequest request, HttpServletResponse response,String strId) {
	String strID = "";
	String strName = "";
	String strSubject = ""; 
	ArrayList alResult = new ArrayList();
	ArrayList alResultRep = new ArrayList();
	ArrayList alResultRepAcc = new ArrayList();
	HashMap hmReturn=new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_acct_trans.gm_sav_fch_rep_account_info",3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    alResultRep = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3)); 
    
    hmReturn.put("invbatchdetails", alResult);
    hmReturn.put("ALREULTREP", alResultRep);
    gmDBManager.close();
    String TEMPLATE_NAME = "GmRepAccountCreate";
	HashMap hmRepDet = new HashMap();
	  if (alResult.size() > 0){
		  for (Iterator iter = alResult.iterator(); iter.hasNext();) {
	          HashMap hmRuleDetails = new HashMap();
	          hmRuleDetails = (HashMap) iter.next();
	          strID = GmCommonClass.parseNull((String) hmRuleDetails.get("ID"));
	          strName = GmCommonClass.parseNull((String) hmRuleDetails.get("NAME"));
		  }
	  }
	try {
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME+ "." + GmEmailProperties.FROM));
		emailProps.setRecipients(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.TO));
		strSubject = (GmCommonClass.getEmailProperty(TEMPLATE_NAME+ "." + GmEmailProperties.SUBJECT));
		strSubject = strSubject +" : "+strID+" - "+strName;
		emailProps.setSubject(strSubject);
		emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME+ "." + GmEmailProperties.MIME_TYPE));
		GmJasperMail jasperMail = new GmJasperMail();
		jasperMail.setJasperReportName("/GmRepAccountEmailDetails.jasper");
		jasperMail.setAdditionalParams(hmReturn);
		jasperMail.setReportData(alResult);
		jasperMail.setEmailProperties(emailProps);
		hmRepDet = jasperMail.sendMail();		
	} catch (Exception e) {
		e.printStackTrace();
	}
	
}

}// end of class GmCustSalesSetupBean
