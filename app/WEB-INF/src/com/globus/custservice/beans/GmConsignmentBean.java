/*
 * Module: GmConsignmentBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 16 June 2006 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;



public class GmConsignmentBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  GmCustomerBean gmCust = new GmCustomerBean();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmConsignmentBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmConsignmentBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * loadConsignReprocessLists - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadConsignReprocessLists(String strDistId, String strSetId) throws AppError {
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    ArrayList alDistributor = new ArrayList();
    ArrayList alSets = new ArrayList();
    ArrayList alReturn = new ArrayList();
    String strIdString = "";

    try {

      alSets = gmProject.loadSetMap("1601");
      hmReturn.put("SETLIST", alSets);

      alDistributor = gmCust.getDistributorList();
      hmReturn.put("DISTLIST", alDistributor);

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();

      sbQuery
          .append(" SELECT C504_CONSIGNMENT_ID CID, C504_COMMENTS COMMENTS, GET_SET_NAME(C207_SET_ID) SNAME, ");
      sbQuery
          .append(" to_char(C504_SHIP_DATE,'mm/dd/yyyy') SDATE, GET_USER_NAME(C504_LAST_UPDATED_BY) UPDBY ");
      sbQuery.append(" FROM T504_CONSIGNMENT ");
      sbQuery.append(" WHERE C701_DISTRIBUTOR_ID =");
      sbQuery.append(strDistId);
      sbQuery.append(" AND C207_SET_ID ='");
      sbQuery.append(strSetId);
      sbQuery.append("'");
      sbQuery.append(" AND C207_SET_ID IS NOT NULL AND C504_VOID_FL IS NULL ");
      sbQuery.append(" ORDER BY C504_SHIP_DATE");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("CONSIGN", alReturn);

      sbQuery.setLength(0);
      sbQuery.append(" SELECT C506_RMA_ID RAID, C506_COMMENTS COMMENTS, ");
      sbQuery
          .append(" to_char(C506_CREDIT_DATE,'mm/dd/yyyy') SDATE, GET_USER_NAME(C506_LAST_UPDATED_BY) UPDBY ");
      sbQuery.append(" FROM T506_RETURNS ");
      sbQuery.append(" WHERE C701_DISTRIBUTOR_ID =");
      sbQuery.append(strDistId);
      sbQuery.append(" AND C207_SET_ID ='");
      sbQuery.append(strSetId);
      sbQuery.append("'");
      sbQuery.append(" AND C207_SET_ID IS NOT NULL AND C506_STATUS_FL = '2' ");
      sbQuery.append(" AND C506_VOID_FL IS NULL ");
      sbQuery.append(" ORDER BY C506_CREDIT_DATE");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("RETURNS", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmConsignmentBean:loadConsignReprocessLists", "Exception is:"
          + e);
    }
    return hmReturn;
  } // End of loadConsignReprocessLists


  /**
   * loadConsignReprocessData - This method will
   * 
   * @param String strConId
   * @param String strSetIds
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadConsignReprocessData(String strConId, String strSetIds) throws AppError {
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();

    HashMap hmReturn = new HashMap();
    ArrayList alConData = new ArrayList();
    ArrayList alSets = new ArrayList();
    ArrayList alReturn = new ArrayList();
    String strSetId = "";

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();

      sbQuery
          .append(" SELECT	C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC, ");
      sbQuery.append(" C505_ITEM_QTY IQTY, C505_CONTROL_NUMBER CNUM , C505_ITEM_QTY RQTY ");
      sbQuery.append(" FROM T505_ITEM_CONSIGNMENT ");
      sbQuery.append(" WHERE C504_CONSIGNMENT_ID = '");
      sbQuery.append(strConId);
      sbQuery.append("' AND C505_ITEM_QTY <> 0 ");
      sbQuery.append(" ORDER BY C205_PART_NUMBER_ID");

      alConData = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("CONSIGN", alConData);

      StringTokenizer strTok = new StringTokenizer(strSetIds, ",");

      while (strTok.hasMoreTokens()) {
        // System.out.println("STRTOK"+strTok.toString());
        strSetId = strTok.nextToken();

        alReturn = gmProject.fetchSetMaping(strSetId, "SET");
        hmReturn.put(strSetId, alReturn);
      }

    } catch (Exception e) {
      GmLogError
          .log("Exception in GmConsignmentBean:loadConsignReprocessData", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadConsignReprocessData

  /**
   * saveConsignReprocess - This method will
   * 
   * @param String strConId
   * @param String strSetIds
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveConsignReprocess(String strConId, String strSetIds, HashMap hmParam,
      String strUserId) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alConData = new ArrayList();
    String strSetId = "";
    String strSetTemp = "";
    String strInputStr = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());



    String strBeanIds = "";
    String strConsignId = "";
    String strReturnId = "";
    String strPrepareString = null;
    String strIds = "";


    hmReturn.put("CONID", strConId);
    hmReturn.put("SETIDS", strSetIds);

    gmDBManager.setPrepareString("GM_SAVE_REPROCESS_CONSIGN", 6);


    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConId);
    strInputStr = (String) hmParam.get("RETURNS");
    gmDBManager.setString(2, strInputStr);
    strInputStr = (String) hmParam.get("CONSIGN");
    gmDBManager.setString(3, strInputStr);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();

    strReturnId = gmDBManager.getString(5);
    strConsignId = gmDBManager.getString(6);

    hmReturn.put("RETURNID", strReturnId);
    hmReturn.put("CONSIGNID", strConsignId);

    StringTokenizer strTok = new StringTokenizer(strSetIds, ",");
    while (strTok.hasMoreTokens()) {
      strSetId = strTok.nextToken();
      strInputStr = (String) hmParam.get(strSetId);

      gmDBManager.setPrepareString("GM_SAVE_REPROCESS_SWAP_SETS", 5);


      gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

      gmDBManager.setString(1, strConId);
      gmDBManager.setString(2, strSetId);
      gmDBManager.setString(3, strInputStr);
      gmDBManager.setString(4, strUserId);

      gmDBManager.execute();

      strBeanIds = gmDBManager.getString(5);
      strIds = strIds.concat(strBeanIds).concat(",");
    }

    gmDBManager.commit();

    StringBuffer sbQuery = new StringBuffer();
    strIds = strIds.replaceAll(",", "','");

    sbQuery
        .append(" SELECT	C504_CONSIGNMENT_ID CONID, GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID) DNAME, ");
    sbQuery.append(" C504_COMMENTS COMMENTS, C207_SET_ID SETID, GET_SET_NAME(C207_SET_ID) SNAME, ");
    sbQuery
        .append(" C504_PARENT_CONSIGNMENT_ID PCONID, GET_USER_NAME(C504_CREATED_BY) CUSER, to_char(C504_CREATED_DATE,'mm/dd/yyyy') CDATE ");
    sbQuery.append(" FROM T504_CONSIGNMENT ");
    sbQuery.append(" WHERE C504_CONSIGNMENT_ID IN('");
    sbQuery.append(strIds);
    sbQuery.append("')");
    sbQuery.append(" AND C504_VOID_FL IS NULL ");
    sbQuery.append(" ORDER BY C504_CONSIGNMENT_ID");

    alConData = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("IDSLIST", alConData);


    return hmReturn;
  } // End of saveConsignReprocess

  /**
   * loadConsignNetReport - This method will
   * 
   * @param String strConId
   * @param String strSetIds
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadConsignNetReport(String strDistId) throws AppError {
    ArrayList alReturn = new ArrayList();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();

      sbQuery.append(" SELECT  T207.C207_SET_ID SETID, T207.C207_SET_NM SNAME, ");
      sbQuery.append(" GET_DIST_SET_CONSIGN_COUNT(T207.C207_SET_ID,");
      sbQuery.append(strDistId);
      sbQuery.append(") NETCNT ");
      sbQuery.append(" FROM T207_SET_MASTER T207");
      sbQuery.append(" WHERE T207.C901_SET_GRP_TYPE = 1601 ");
      sbQuery.append(" ORDER BY T207.C207_SET_ID ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmConsignmentBean:loadConsignNetReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of loadConsignNetReport

  /**
   * loadLoanerPartRepSummary - This method will load the loaner information present for that part
   * and rep.
   * 
   * @param String strPartNum
   * @param String strRepId
   * @return RowSetDynaClass
   * @exception AppError
   **/
  public HashMap loadLoanerPartRepSummary(String strPartNum, String strRepId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    String strPrepareString = null;

    RowSetDynaClass resultSet = null;
    ResultSet rs = null;
    String strPartDesc = "";
    HashMap hmResult = new HashMap();



    gmDBManager.setPrepareString("gm_cs_fch_loaner_partrep_sum", 4);


    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strRepId);

    gmDBManager.registerOutParameter(3, OracleTypes.CHAR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.execute();

    strPartDesc = (String) gmDBManager.getObject(3);
    rs = (ResultSet) gmDBManager.getObject(4);
    resultSet = gmDBManager.returnRowSetDyna(rs);
    gmDBManager.close();
    hmResult.put("PARTDESC", strPartDesc);
    hmResult.put("RSLOANERINFO", resultSet);
    log.debug("values inside hmResult " + hmResult);


    return hmResult;
  } // End of methodName

  /**
   * loadLoanerReconReport - This method will load the loaner information for reconciliation
   * 
   * @param String strDistId
   * @return List
   * @exception AppError velu changed Aug 23 10
   **/
  public List loadLoanerReconReport(HashMap hmParam) throws AppError {
    List lResult = null;

    String strDistId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strLnTyp = GmCommonClass.parseZero((String) hmParam.get("LNTYP"));
    String strLnDtFrom = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strLnDtTo = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    // String strReconTyp = GmCommonClass.parseZero((String) hmParam.get("RECONTYP"));

    log.debug("strDistId " + strDistId);
    log.debug("strPartNum " + strPartNum);
    log.debug("strLnTyp " + strLnTyp);
    log.debug("strLnDtFrom " + strLnDtFrom);
    log.debug("strLnDtTo " + strLnDtTo);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmResult = new HashMap();

    gmDBManager.setPrepareString("gm_cs_rpt_loaner_recon_by_part", 6);

    gmDBManager.setInt(1, Integer.parseInt(GmCommonClass.parseZero(strDistId)));
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setInt(3, Integer.parseInt(GmCommonClass.parseZero(strLnTyp)));
    gmDBManager.setString(4, strLnDtFrom);
    gmDBManager.setString(5, strLnDtTo);

    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);

    gmDBManager.execute();
    lResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));

    gmDBManager.close();
    log.debug("total recs are " + lResult.size());
    return lResult;
  } // End of loadLoanerReconReport

  public String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = new ArrayList();
    HashMap hReturn = new HashMap();
    String strSessCompanyLocale= (String) hmParam.get("STRSESSCOMPANYLOCALE");
    String strTemplate = (String) hmParam.get("TEMPLATE");
    String strTemplatePath = (String) hmParam.get("TEMPLATEPATH");
    hReturn = (HashMap) hmParam.get("HRETURN");
    alResult = (ArrayList) hmParam.get("LRESULT");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hReturn", hReturn);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.custservice.GmLoanerReconReport",
            strSessCompanyLocale));
    return templateUtil.generateOutput();
  }

  /**
   * loanerByDist - This method will load the loaner information By Distributor
   * 
   * @param String strDistId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loanerByDist(HashMap hmParam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    StringBuffer sbSetNameQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    HashMap hmSetDetails = new HashMap();
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strSetName = "";

    String strFromMonth = GmCommonClass.parseNull((String) hmParam.get("FROMMONTH"));
    String strFromYear = GmCommonClass.parseNull((String) hmParam.get("FROMYEAR"));
    String strToMonth = GmCommonClass.parseNull((String) hmParam.get("TOMONTH"));
    String strToYear = GmCommonClass.parseNull((String) hmParam.get("TOYEAR"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("LNTYP"));

    if (!strSetId.equals("") && strSetId != null) {
      sbSetNameQuery.append(" SELECT C207_SET_NM NAME  ");
      sbSetNameQuery.append(" FROM T207_SET_MASTER  ");
      sbSetNameQuery.append(" WHERE C207_SET_ID ='");
      sbSetNameQuery.append(strSetId);
      sbSetNameQuery.append("' ");
      hmSetDetails = gmDBManager.querySingleRecord(sbSetNameQuery.toString());
      strSetName = (String) hmSetDetails.get("NAME");
    }


    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (!strFromMonth.equals("") && !strFromYear.equals("")) {
      sbWhereCondtion.append(" T504A.C504A_LOANER_DT >= TO_DATE('");
      sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
      sbWhereCondtion.append("','MM/YYYY') ");
    }

    if (!strToMonth.equals("") && !strToYear.equals("")) {
      if (sbWhereCondtion != null) {
        sbWhereCondtion.append(" AND");
      }
      sbWhereCondtion.append(" T504A.C504A_LOANER_DT <= LAST_DAY(TO_DATE('");
      sbWhereCondtion.append(strToMonth + "/" + strToYear);
      sbWhereCondtion.append("','MM/YYYY')) ");
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T504A.C504A_LOANER_DT,'Mon YY') L_DATE ");
    sbHeaderQuery.append("FROM T504A_LOANER_TRANSACTION T504A WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(" AND T504A.C5040_PLANT_ID =" + getCompPlantId());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(T504A.C504A_LOANER_DT,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery.append("SELECT   T504A.C504A_CONSIGNED_TO_ID ID ");
    sbDetailQuery.append(" ,GET_DISTRIBUTOR_NAME(T504A.C504A_CONSIGNED_TO_ID) NAME ");
    sbDetailQuery.append(" ,TO_CHAR(T504A.C504A_LOANER_DT,'Mon YY') L_DATE ");
    sbDetailQuery.append(" ,COUNT(1) COUNT ");
    sbDetailQuery.append(" FROM  T504A_LOANER_TRANSACTION T504A ");
    sbDetailQuery.append(", T504_CONSIGNMENT T504, T526_Product_Request_Detail t526 ");
    sbDetailQuery.append(" WHERE C901_CONSIGNED_TO = 50170  "); // 50170 is loan to (LONTO) Group -
                                                                // value is Distributor.
    sbDetailQuery.append(" AND T504.C504_TYPE = ");
    sbDetailQuery.append(strType);
    sbDetailQuery
        .append(" And t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id ");
    sbDetailQuery.append(" AND t526.c526_status_fl  = 30");
    sbDetailQuery.append(" AND t526.C526_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND T504.C504_VOID_FL IS NULL  ");
    sbDetailQuery.append(" AND T504A.C504A_VOID_FL IS NULL  ");
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID =" + getCompPlantId());
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID = T504A.C5040_PLANT_ID  ");
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID = t526.C5040_PLANT_ID  ");

    if (!strSetId.equals("") && strSetId != null) {
      sbDetailQuery.append(" AND T504.C207_SET_ID = '");
      sbDetailQuery.append(strSetId);
      sbDetailQuery.append("'");
    }

    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(sbWhereCondtion);
    sbDetailQuery
        .append(" GROUP BY to_char(C504A_LOANER_DT,'Mon YY'), T504A.C504A_CONSIGNED_TO_ID ");
    sbDetailQuery.append(" ORDER BY NAME");

    log.debug(" details  query for loaner by dist " + sbDetailQuery.toString());
    log.debug(" header  query for loaner by dist " + sbHeaderQuery.toString());

    hmapFinalValue =
        gmCrossTabRep.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("SETNAME", strSetName);
    return hmapFinalValue;

  }


  /**
   * loanerByDistBySet - This method will load the loaner information By Distributor and it is a
   * drill down to the set level
   * 
   * @param String strDistId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loanerByDistBySet(HashMap hmParam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    StringBuffer sbDistNameQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    HashMap hmDistVal = new HashMap();
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strDistributorName = "";

    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));

    String strFromMonth = GmCommonClass.parseNull((String) hmParam.get("FROMMONTH"));
    String strFromYear = GmCommonClass.parseNull((String) hmParam.get("FROMYEAR"));
    String strToMonth = GmCommonClass.parseNull((String) hmParam.get("TOMONTH"));
    String strToYear = GmCommonClass.parseNull((String) hmParam.get("TOYEAR"));
    String strLnTyp = GmCommonClass.parseZero((String) hmParam.get("LNTYP"));

    if (!strDistId.equals("") && strDistId != null) {
      sbDistNameQuery.append(" SELECT C701_DISTRIBUTOR_NAME  NAME ");
      sbDistNameQuery.append(" FROM T701_DISTRIBUTOR  ");
      sbDistNameQuery.append(" WHERE C701_DISTRIBUTOR_ID ='");
      sbDistNameQuery.append(strDistId);
      sbDistNameQuery.append("' ");

      hmDistVal = gmDBManager.querySingleRecord(sbDistNameQuery.toString());
      strDistributorName = (String) hmDistVal.get("NAME");
    }

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (!strFromMonth.equals("") && !strFromYear.equals("")) {
      sbWhereCondtion.append(" T504A.C504A_LOANER_DT >= TO_DATE('");
      sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
      sbWhereCondtion.append("','MM/YYYY') ");
    }

    if (!strToMonth.equals("") && !strToYear.equals("")) {
      if (sbWhereCondtion != null) {
        sbWhereCondtion.append(" AND");
      }
      sbWhereCondtion.append(" T504A.C504A_LOANER_DT <= LAST_DAY(TO_DATE('");
      sbWhereCondtion.append(strToMonth + "/" + strToYear);
      sbWhereCondtion.append("','MM/YYYY')) ");
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T504A.C504A_LOANER_DT,'Mon YY') L_DATE ");
    sbHeaderQuery.append("FROM T504A_LOANER_TRANSACTION T504A WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(" AND T504A.C5040_PLANT_ID =" + getCompPlantId());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(T504A.C504A_LOANER_DT,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery.append("SELECT T504.C207_SET_ID ID ");
    sbDetailQuery.append(" ,T504.C207_SET_ID || ' / ' || GET_SET_NAME(T504.C207_SET_ID) NAME ");
    sbDetailQuery.append(" ,TO_CHAR(T504A.C504A_LOANER_DT,'Mon YY') L_DATE ");
    sbDetailQuery.append(" ,COUNT(1) COUNT ");
    sbDetailQuery
        .append(" FROM  T504_CONSIGNMENT T504, T504A_LOANER_TRANSACTION T504A ,t526_product_request_detail t526 ");
    sbDetailQuery.append(" WHERE  T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND T504.C504_TYPE = ");
    sbDetailQuery.append(strLnTyp);
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID =" + getCompPlantId());
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID = T504A.C5040_PLANT_ID  ");
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID = t526.C5040_PLANT_ID  ");
    sbDetailQuery
        .append(" AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id ");
    sbDetailQuery.append(" AND t526.c526_status_fl = 30 ");
    sbDetailQuery.append(" AND t526.C526_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND T504.C504_VOID_FL IS NULL  ");
    sbDetailQuery.append(" AND T504a.C504a_VOID_FL IS NULL  ");

    if (!strDistId.equals("") && strDistId != null) {
      sbDetailQuery.append(" AND T504A.C504A_CONSIGNED_TO_ID = '");
      sbDetailQuery.append(strDistId);
      sbDetailQuery.append("'");
    }

    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(sbWhereCondtion);
    sbDetailQuery.append(" GROUP BY to_char(C504A_LOANER_DT,'Mon YY'), T504.C207_SET_ID ");
    sbDetailQuery.append(" ORDER BY NAME");


    log.debug(" details  query for loaner by dist by set " + sbDetailQuery.toString());
    log.debug(" header  query for loaner by dist by set " + sbHeaderQuery.toString());

    hmapFinalValue =
        gmCrossTabRep.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("DISTNAME", strDistributorName);
    return hmapFinalValue;

  }

  /**
   * loanerStatusBySet - This method will load the status of loaner set
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loanerStatusBySet(String strLnType) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
    String strFilter = 
    		GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_OVERALLBY_ST"));/*LOANER_OVERALLBY_ST- To fetch the records for EDC entity countries based on plant*/
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    sbHeaderQuery.append("SELECT get_rule_value(c902_code_nm_alt,'LOANS')   NAME ");
    sbHeaderQuery.append("FROM T901_CODE_LOOKUP WHERE ");
    sbHeaderQuery.append("C901_CODE_GRP = 'LOANS' ");
    sbHeaderQuery.append("and C901_CODE_ID != '50198' ");
    sbHeaderQuery.append(" and C901_ACTIVE_FL =1 ");
    sbHeaderQuery.append(" order by C901_CODE_SEQ_NO  ");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // changing this query to use a union as on the screen the open_request column doesnt sum up the
    // total.
    // the current cross tab code doesnt handle the sum up for fixed columns. so changing the query
    // ***********************************************************
    sbDetailQuery.append("SELECT T504.C207_SET_ID ID ");
    sbDetailQuery.append(" ,T504.C207_SET_ID || ' / ' || GET_SET_NAME(T504.C207_SET_ID) SETNAME ");
    // sbDetailQuery.append(" ,T504A.C504A_STATUS_FL  STATUS ");
    sbDetailQuery.append(" ,get_rule_value(T504A.C504A_STATUS_FL,'LOANS') NAME ");
    sbDetailQuery.append(" ,COUNT(1) COUNT ");
    sbDetailQuery.append(" FROM  T504_CONSIGNMENT T504, T504A_CONSIGNMENT_LOANER T504A ");
    sbDetailQuery.append(" WHERE  T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND T504.C504_TYPE = ");
    sbDetailQuery.append(strLnType);
    sbDetailQuery.append(" AND T504.C504_VOID_FL IS NULL  ");
    sbDetailQuery.append(" AND T504A.c504a_status_fl !=60");// inactive
    sbDetailQuery.append(" AND T504.C207_SET_ID IS NOT NULL  ");
    sbDetailQuery.append(" AND ( T504.C1900_company_id = " + strFilter);
    sbDetailQuery.append(" OR T504.C5040_plant_id = " + strFilter);
    sbDetailQuery.append(") ");
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID = T504A.C5040_PLANT_ID  ");
    sbDetailQuery.append(" GROUP BY T504.C207_SET_ID, T504A.C504A_STATUS_FL ");
    sbDetailQuery.append(" UNION ALL");
    sbDetailQuery.append(" SELECT T504.C207_SET_ID ID ");
    sbDetailQuery.append(" ,T504.C207_SET_ID || ' / ' || GET_SET_NAME(T504.C207_SET_ID) SETNAME, ");
    sbDetailQuery.append(" 'Opn Req' NAME,");
    sbDetailQuery.append(" open_count.open_sum count ");
    sbDetailQuery.append(" FROM  T504_CONSIGNMENT T504, T504A_CONSIGNMENT_LOANER T504A ");
    sbDetailQuery
        .append(" ,(SELECT c207_set_id,	SUM (c526_qty_requested) open_sum FROM t526_product_request_detail t526 WHERE  c901_request_type = ");
    sbDetailQuery.append(strLnType);
    sbDetailQuery
        .append(" AND c526_status_fl = 10 AND c526_void_fl IS NULL ");
    sbDetailQuery.append(" AND ( t526.C1900_company_id = " + strFilter);
    sbDetailQuery.append(" OR t526.C5040_plant_id = " + strFilter); 
    sbDetailQuery.append(") ");
    sbDetailQuery.append(" GROUP BY c207_set_id ) open_count ");
    sbDetailQuery.append(" WHERE  T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND T504.C504_TYPE = ");
    sbDetailQuery.append(strLnType);
    sbDetailQuery.append(" AND T504.C504_VOID_FL IS NULL  ");
    sbDetailQuery.append(" AND T504.C207_SET_ID = open_count.c207_set_id(+)  ");
    sbDetailQuery.append(" AND T504A.c504a_status_fl !=60");// inactive
    sbDetailQuery.append(" AND T504.C207_SET_ID IS NOT NULL  ");
    sbDetailQuery.append(" AND ( T504.C1900_company_id = " + strFilter);
    sbDetailQuery.append(" OR T504.C5040_plant_id = " + strFilter); 
    sbDetailQuery.append(") ");
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID = T504A.C5040_PLANT_ID  ");
    sbDetailQuery.append(" GROUP BY T504.C207_SET_ID , open_count.open_sum "); // ,
                                                                               // T504A.C504A_STATUS_FL
                                                                               // ");
    sbDetailQuery.append(" UNION ALL");
    sbDetailQuery.append(" SELECT T504.C207_SET_ID ID ");
    sbDetailQuery.append(" ,T504.C207_SET_ID || ' / ' || GET_SET_NAME(T504.C207_SET_ID) SETNAME, ");
    sbDetailQuery.append(" 'Pnd Apr Req' NAME,");
    sbDetailQuery.append(" pnd_count.pnd_sum count ");
    sbDetailQuery.append(" FROM  T504_CONSIGNMENT T504, T504A_CONSIGNMENT_LOANER T504A ");
    sbDetailQuery
        .append(" ,( SELECT c207_set_id,	SUM (c526_qty_requested) pnd_sum FROM t526_product_request_detail t526  WHERE  c901_request_type =");
    sbDetailQuery.append(strLnType);
    sbDetailQuery
        .append(" AND c526_status_fl = 5 AND c526_void_fl IS NULL ");
    sbDetailQuery.append(" AND ( t526.C1900_company_id = " + strFilter);
    sbDetailQuery.append(" OR t526.C5040_plant_id = " + strFilter); 
    sbDetailQuery.append(") ");
    sbDetailQuery.append(" GROUP BY c207_set_id ) pnd_count ");
    sbDetailQuery.append(" WHERE  T504A.C504_CONSIGNMENT_ID = T504.C504_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND T504.C504_TYPE = ");
    sbDetailQuery.append(strLnType);
    sbDetailQuery.append(" AND T504.C504_VOID_FL IS NULL  ");
    sbDetailQuery.append(" AND T504.C207_SET_ID = pnd_count.c207_set_id(+) ");
    sbDetailQuery.append(" AND T504A.c504a_status_fl !=60");// inactive
    sbDetailQuery.append(" AND T504.C207_SET_ID IS NOT NULL  ");
    sbDetailQuery.append(" AND ( T504.C1900_company_id = " + strFilter);
    sbDetailQuery.append(" OR T504.C5040_plant_id = " + strFilter); 
    sbDetailQuery.append(") ");
    sbDetailQuery.append(" AND T504.C5040_PLANT_ID = T504A.C5040_PLANT_ID  ");
    sbDetailQuery.append(" GROUP BY T504.C207_SET_ID, pnd_count.pnd_sum "); // ,
                                                                            // T504A.C504A_STATUS_FL
                                                                            // ");
    sbDetailQuery.append(" ");
    sbDetailQuery.append(" ORDER BY SETNAME ");


    log.debug(" details  query for loanerStatusBySet " + sbDetailQuery.toString());
    log.debug(" header  query for loanerStatusBySet " + sbHeaderQuery.toString());

    // gmCrossTabRep.setFixedColumn(true);
    // gmCrossTabRep.setFixedParams(5,"Open Requests");

    hmapFinalValue =
        gmCrossTabRep.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());

    return hmapFinalValue;

  }

  
  /**
   * loanerStatusBySet - This method will load the status of loaner set
   * 
   * @return String 
   * @exception AppError
   **/
  public String  loanerStsBySet(String strLnType) throws AppError  {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strJSONDashData = "";
	    gmDBManager.setPrepareString("gm_pkg_loaner_overall_status.gm_fch_loaner_overall", 2);
	    gmDBManager.setInt(1, Integer.parseInt(GmCommonClass.parseZero(strLnType)));
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.execute();
		log.debug("strLnType is"+strLnType);
		strJSONDashData = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strJSONDashData;
  } 
}// end of class GmConsignmentBean