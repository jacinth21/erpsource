/**
 * 
 */
package com.globus.custservice.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sswetha
 *
 */
public class GmPriceBean extends GmBean {

	
	/**
	 *  Constructor
	 */
		  public GmPriceBean(GmDataStoreVO gmDataStoreVO) throws AppError {
			    super(gmDataStoreVO);
	}
		  
	/**
	 *  Constructor
	*/  
		public GmPriceBean() {
			    super(GmCommonClass.getDefaultGmDataStoreVO());
       }

			  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to initialize logger
			  
			  
			  
		     /**
			   * updateItemOrderPrice - This method will update the price for all orders from Change Pricing screen for PMT-38606
			   * 
			   * @param String strUsername
			   * @param String strPassword
			   * @return HashMap
			   * @exception AppError
			   */
			  public HashMap updateItemOrderPrice(String strOrderId, String strInputStr, String strShipCost,
			      String strTotal, String strUserId) throws AppError {
			    HashMap hmReturn = new HashMap();
			    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
			    String strMsg = "";
			    // To update Ack order price when price changed for bba order
			    updateAckItemOrderPrice(gmDBManager, strOrderId, strInputStr, strUserId);

			    gmDBManager.setPrepareString("gm_update_order_price_for_all_orders", 6);
			    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
			    gmDBManager.setString(1, strOrderId);
			    gmDBManager.setString(2, strInputStr);
			    gmDBManager.setDouble(3, Double.parseDouble(strShipCost));
			    gmDBManager.setDouble(4, Double.parseDouble(strTotal));
			    gmDBManager.setString(5, strUserId);
			    gmDBManager.execute();
			    strMsg = gmDBManager.getString(6);
			    log.debug("strMsg = " + strMsg);
			    gmDBManager.commit();
			    hmReturn.put("MSG", strMsg);
			    return hmReturn;
			  }// End of updateItemOrderPrice
			  
			  
			  
			  /**
			   * updateAckOrderPrice - To update ack order price for all orders when bba order price changed  for PMT-38606
			   * 
			   * @param gmDBManager , strOrderId , strInputStr ,strUserId
			   * @exception AppError
			   */
			  public void updateAckItemOrderPrice(GmDBManager gmDBManager, String strOrderId,
			      String strInputStr, String strUserId) throws AppError {
			    gmDBManager.setPrepareString("gm_pkg_op_ack_order.gm_save_ack_item_ord_price_for_all_orders", 3);
			    gmDBManager.setString(1, strOrderId);
			    gmDBManager.setString(2, strInputStr);
			    gmDBManager.setString(3, strUserId);
			    gmDBManager.execute();
			  }
		
}
