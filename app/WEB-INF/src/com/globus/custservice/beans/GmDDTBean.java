package com.globus.custservice.beans;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDDTBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  public GmDDTBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * fetchDDTCartDetails - This method will fetch the Cart Details
   * 
   * @param String strOrderId
   * @return ArrayList
   * @exception AppError
   */


  public ArrayList fetchDDTCartDetails(String strOrderId) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cs_usage_lot_number.gm_fch_ddt_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return alReturn;
  }

  /**
   * ; fetchBODOrderDetails - This method will fetch the order summary details for Bill of Delivery
   * and DO Receipt
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList fetchBODOrderDetails(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());

    String strOrdId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    sbQuery.setLength(0);
    sbQuery.append("select  ORDID,ID,PDESC,USAGELOT,SUM(QTY) QTY,UPRICE from  ");
    sbQuery.append(" ( SELECT T501.C501_ORDER_ID ORDID,  T502B.C205_PART_NUMBER_ID ID, ");
    sbQuery.append(" get_partdesc_by_company(T502B.C205_PART_NUMBER_ID) PDESC, ");
    sbQuery
        .append(" T502b.C502b_Usage_Lot_Num Usagelot,  T502b.C502b_Item_Qty Qty,  t502.c502_item_price UPRICE ");
    sbQuery.append(" , t502.c502_item_qty ITEMQTY ");
    sbQuery.append(" From T501_Order T501,  T502b_Item_Order_Usage T502b,T500_Order_Info t500 ");
    sbQuery.append(" ,  t502_item_order t502 ");
    sbQuery.append(" WHERE T501.C501_ORDER_ID IN  (SELECT C501_ORDER_ID SBID ");
    sbQuery.append(" FROM T501_ORDER WHERE C501_ORDER_ID = '" + strOrdId + "' ");
    sbQuery.append("  OR C501_PARENT_ORDER_ID = '" + strOrdId + "'  )");
    sbQuery.append(" AND T501.C501_Order_Id      = T502.C501_Order_Id ");
    sbQuery.append(" AND t502.c500_order_info_id = t500.c500_order_info_id ");
    sbQuery.append(" AND T500.C500_Order_Info_Id = T502b.C500_Order_Info_Id ");
    sbQuery.append(" AND T502B.C502B_VOID_FL    IS NULL ");
    sbQuery.append(" AND T501.C501_VOID_FL      IS NULL ");
    sbQuery.append(" AND T502.C502_VOID_FL      IS NULL ");
    sbQuery.append(" AND T500.C500_VOID_FL      IS NULL ) ");
    sbQuery.append(" GROUP BY ORDID,ID,PDESC,USAGELOT,UPRICE ");
    sbQuery.append(" ORDER BY ORDID,ID ");
    log.debug(" Query for Bill of Delivery Order Details " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }

}
