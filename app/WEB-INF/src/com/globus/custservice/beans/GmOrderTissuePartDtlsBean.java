/**
 *  FileName : GmOrderTissuePartDtlsBean.java 
 *  Author   : T.S. Ramachandiran 
 */

package com.globus.custservice.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import java.util.HashMap;

public class GmOrderTissuePartDtlsBean extends GmBean{
	
	/**
	 *  This used to get the instance of this class for enabling logging 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
    
	/**
	 * constructor - GmOrderTissuePartDtlsBean
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmOrderTissuePartDtlsBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {

		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**getOrderTissueAddressDtls method used to fetch hospital address details
	 * @param hmParam
	 * @throws AppError
	 */
	  public HashMap getOrderTissueAddressDtls(String strAccountId) throws AppError{	    
		  
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			String strAddress = "";
		  	String strMessage = "";
		  	String strConcat = "";
		  	HashMap hmparam = new HashMap();
			gmDBManager.setPrepareString("gm_pkg_op_tissue_part_dtls.get_tissue_shipout_address", 3);
			gmDBManager.setString(1, strAccountId);
			gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
			gmDBManager.execute();
			strAddress = GmCommonClass.parseNull(gmDBManager.getString(2));
			strMessage = GmCommonClass.parseNull(gmDBManager.getString(3));
			gmDBManager.close();
			hmparam.put("STRMESSAGE", strMessage);
			hmparam.put("STRADDRESS", strAddress);
			log.debug("hmparam==>"+hmparam);
			return hmparam;	 
		 }
	  
	  
	  /**getDOTissueAddress method used to fetch hospital address details
		 * @param hmParam
		 * @throws AppError
		 */
	  public HashMap getDOTissueAddress(String strOrderId)throws AppError{	    
		  
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			String strAddress = "";
		  	String strMessage = "";
		  	String strConcat = "";
		  	HashMap hmparam = new HashMap();
			gmDBManager.setPrepareString("gm_pkg_op_tissue_part_dtls.gm_fetch_do_tissue_shipout_address", 3);
			gmDBManager.setString(1, strOrderId);
			gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
			gmDBManager.execute();
			strAddress = GmCommonClass.parseNull(gmDBManager.getString(2));
			strMessage = GmCommonClass.parseNull(gmDBManager.getString(3));
			gmDBManager.close();
			hmparam.put("STRMESSAGE", strMessage);
			hmparam.put("STRADDRESS", strAddress);
			log.debug("hmparam==>"+hmparam);
			return hmparam;
			 
		 }
	
}
