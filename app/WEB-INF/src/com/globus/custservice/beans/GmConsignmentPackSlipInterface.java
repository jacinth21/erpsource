package com.globus.custservice.beans;

import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.valueobject.common.GmDataStoreVO;

public interface GmConsignmentPackSlipInterface {
	////PMT-57366  Loan detail sheet displaying Hospital name in English
	public HashMap loadConsignDetails(String strConsignId, String strLUserId, GmDataStoreVO gmDataStore) throws AppError;
}