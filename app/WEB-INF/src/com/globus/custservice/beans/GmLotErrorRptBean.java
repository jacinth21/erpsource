/*
 * Module: GmSalesBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11
 * Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmParentRepAccountBean;
import com.globus.common.db.GmDBManager;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.pricing.beans.GmPricingBatchBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLotErrorRptBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  // this will be removed all place changed with GmDataStoreVO constructor

  public GmLotErrorRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmLotErrorRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * This is fetch Lot Tracking Details
   * @param hmParams
   * @return
   * @throws AppError
   */
  public String fetchLotErrorInfo(HashMap hmParams) throws AppError {
	    ArrayList alResult = new ArrayList();
	    StringBuffer sbQuery = new StringBuffer();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	   
	    String strFieldSales = GmCommonClass.parseZero((String) hmParams.get("FIELDSALES")); 
	    String strPartNumber = GmCommonClass.parseZero((String) hmParams.get("PARTNUMBER")); 
	    String strLotNumber = GmCommonClass.parseZero((String) hmParams.get("LOTNUMBER")); 
	    String strTransactionId = GmCommonClass.parseZero((String) hmParams.get("TRANSACTIONID"));
	    String strTransactionType = GmCommonClass.parseZero((String) hmParams.get("TRANSACTIONTYPE")); 
	    String strTransactionFromDate = GmCommonClass.parseNull((String) hmParams.get("TRANSFROMDATE"));     
	    String strTransactionToDate = GmCommonClass.parseNull((String) hmParams.get("TRANSTODATE"));  
	    String strStatus = GmCommonClass.parseZero((String) hmParams.get("STATUS")); 
	    String strResolutionType = GmCommonClass.parseZero((String) hmParams.get("RESOLUTIONTYPE")); 
	    String strResolutionFromDate = GmCommonClass.parseNull((String) hmParams.get("RESOLUTIONFROMDATE"));   
	    String strResolutionToDate = GmCommonClass.parseNull((String) hmParams.get("RESOLUTIONTODATE"));  
	    String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
	    String strCompId = GmCommonClass.parseNull(getCompId());
        String strPlantId = GmCommonClass.parseNull(getCompPlantId());
	  
	    sbQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT('ERRORINFOID' VALUE T5063.C5063_LOT_ERROR_INFO_ID, 'PARTNM' VALUE T5063.C205_PART_NUMBER_ID , 'LOTNM' VALUE T5063.C2550_CONTROL_NUMBER, ");
	    sbQuery.append(" 'TRANSID' VALUE T5063.C5063_TRANS_ID, 'TRANSDATE' VALUE TO_CHAR (T5063.C5063_TRANS_DATE,'"+strCompDateFmt+"'),");
	    sbQuery.append(" 'TRANSTYPE' VALUE GET_CODE_NAME(T5063.C5063_TRANS_TYPE),'PARTDESC' VALUE REGEXP_REPLACE(GET_PARTNUM_DESC(T5063.C205_PART_NUMBER_ID),'[^ ,0-9A-Za-z]', ''), ");
	    sbQuery.append(" 'RESOLVEDBY' VALUE GET_USER_NAME(T5063.C5063_RESOLVED_BY), 'RESOLVEDDATE' VALUE TO_CHAR (T5063.C5060_RESOLVED_DATE,'"+strCompDateFmt+"'),");  
	    sbQuery.append(" 'RESOLTYPE' VALUE GET_CODE_NAME(T5063.C901_RESOLUTION_TYPE), ");
	    sbQuery.append(" 'ERRORTYPE' VALUE GET_CODE_NAME(T5063.C901_ERROR_TYPE), 'ERTRANSID' VALUE T5063.C5063_REASON, ");
	    sbQuery.append(" 'STATUS' VALUE GET_CODE_NAME(T5063.C901_STATUS), 'FIELDSALES' VALUE GET_DISTRIBUTOR_NAME(T5063.C701_TRANS_DISTRIBUTOR_ID),'CONSIGNEDFS' VALUE GET_DISTRIBUTOR_NAME(T5063.C701_CONSIGNED_DISTRIBUTOR_ID),'UPDATEDLOT' VALUE T5063.C5063_UPDATED_CONTROL_NUMBER,'COMMENTS' VALUE T5063.C5063_COMMENTS) ORDER BY T5063.C205_PART_NUMBER_ID RETURNING CLOB) ");
	    sbQuery.append(" FROM T5063_LOT_ERROR_INFO T5063  ");
	    
	    sbQuery.append(" WHERE T5063.C5063_VOID_FL(+) IS NULL  ");
        
         if (!strPartNumber.equals("0")) { // If part number is given in filter
        	 sbQuery.append(" AND UPPER(T5063.C205_PART_NUMBER_ID) = UPPER('" + strPartNumber + "') ");
          }
        if (!strFieldSales.equals("0") ) { // If Field Sales is given in filter
        	 sbQuery.append(" AND UPPER(T5063.C701_TRANS_DISTRIBUTOR_ID) = UPPER('" + strFieldSales + "') ");
         }
         if (!strLotNumber.equals("0")) { // If Lot Number is given in filter
        	 sbQuery.append(" AND UPPER(T5063.C2550_CONTROL_NUMBER) = UPPER('" + strLotNumber + "') ");
          }
         if (!strTransactionId.equals("0")) { // If Trans.Id is given in filter
        	 sbQuery.append(" AND UPPER(T5063.C5063_TRANS_ID) = UPPER('" + strTransactionId + "') ");
          }
         if (!strTransactionType.equals("0")) { // If Trans.Type is given in filter
        	 sbQuery.append(" AND UPPER(T5063.C5063_TRANS_TYPE) = UPPER('" + strTransactionType + "') ");
          }
         if (!strTransactionFromDate.equals("") && !strTransactionToDate.equals("") ) { // If Trans.Date is given in filter
        	 sbQuery.append(" AND TRUNC( T5063.C5063_TRANS_DATE) BETWEEN TRUNC(TO_DATE('" +strTransactionFromDate + "' ,'"+strCompDateFmt +"')) AND TRUNC(TO_DATE('" +strTransactionToDate + "' ,'"+strCompDateFmt +"'))");
          }
         if (!strStatus.equals("0")) { // If Status is given in filter
        	 sbQuery.append(" AND UPPER(T5063.C901_STATUS) = UPPER('" + strStatus + "') ");
          }
         if (!strResolutionType.equals("0")) { // If Resolution Type is given in filter
        	 sbQuery.append(" AND UPPER(T5063.C901_RESOLUTION_TYPE) = UPPER('" + strResolutionType + "') ");
          }
         if (!strResolutionFromDate.equals("") && !strResolutionToDate.equals("")) { // If Resolution Date is given in filter
        	 sbQuery.append(" AND TRUNC(T5063.C5060_RESOLVED_DATE) BETWEEN TRUNC(TO_DATE('" +strResolutionFromDate + "' ,'"+strCompDateFmt +"')) AND TRUNC(TO_DATE('" +strResolutionToDate + "' ,'"+strCompDateFmt +"'))");
          }
         if (!strCompId.equals("")) { // company id
        	 sbQuery.append(" AND C1900_COMPANY_ID ='" + strCompId + "'");
          }
         if (!strPlantId.equals("")) { // plant id
        	 sbQuery.append(" AND C5040_PLANT_ID ='" + strPlantId + "'");
          }
         log.debug(" Query For LotcodeRptBean fetchLotTrackingDetails() : " + sbQuery.toString());
         String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
	   return strReturn;
}
  
  /**
   * This method is used to fetch Lot Error Details
   * @param String error info ID
   * @return
   * @throws AppError
   */
  public ArrayList fetchLotErrorDetail(String strErrorInfoID) {
	    ArrayList alList = new ArrayList();
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_error_rpt.gm_fch_lot_error_detail", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strErrorInfoID);			
		gmDBManager.execute();
		
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alList;
	}


}// end of class GmSalesBean


