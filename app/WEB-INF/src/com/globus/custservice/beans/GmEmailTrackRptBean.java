package com.globus.custservice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class  GmEmailTrackRptBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmEmailTrackRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmEmailTrackRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * fetch email tracking report details
   *  @return ArrayList
   * @param HashMap
  **/
  public ArrayList fetchEmailTrackingDetails(HashMap hmParams) throws AppError {
	    ArrayList alResult = new ArrayList();
	    StringBuffer sbQuery = new StringBuffer();
	  	   
	    String strMailTo = GmCommonClass.parseNull((String) hmParams.get("TOEMAILID"));
	    String strMailSub = GmCommonClass.parseNull((String) hmParams.get("SUBJECT"));
	    String strFrmDate = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
	    String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
	    String strCompanyDateFormat = getGmDataStoreVO().getCmpdfmt();
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    
	    sbQuery.append("select c916_email_log_id EMAILLOGID,c916_email_from EMAILFORM,c916_email_to EMAILTO,c916_email_cc EMAILCC,");
	    sbQuery.append("c916_email_subject EMAILSUB, TO_CHAR(c916_email_sent_date,'"+strCompanyDateFormat+" HH:MI:SS AM') EMAILSENTDATE from t916_email_log where ");
	    sbQuery.append("c916_void_fl IS NULL "); 
	    if (strMailTo!=null && !strMailTo.equals("")) {
	        sbQuery.append(" AND  UPPER(c916_email_to) like UPPER('%"+ strMailTo+"%')");
	    }
	    if (strMailSub!=null && !strMailSub.equals("")) {
	        sbQuery.append(" AND  UPPER(c916_email_subject) like UPPER('%"+ strMailSub+"%')");  
	    }
	    if (!strFrmDate.equals("") && strFrmDate!=null) {
	        sbQuery.append(" AND TRUNC(c916_email_sent_date) >= TO_DATE('");
	        sbQuery.append(strFrmDate);
	        sbQuery.append("', '"+strCompanyDateFormat+"')");
	    }
	    if (!strToDate.equals("") && strToDate!=null) {
	        sbQuery.append(" AND TRUNC(c916_email_sent_date) <= TO_DATE('");
	        sbQuery.append(strToDate);
	        sbQuery.append("', '"+strCompanyDateFormat+"')");
	    }
	    
	    sbQuery.append(" order by EMAILSENTDATE desc "); 
	   
	    log.debug(" Query For fetchEmailTrackingDetails() : " + sbQuery.toString());

	    gmDBManager.setPrepareString(sbQuery.toString());
	    alResult =  GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecord());
	    return alResult;
	  }
  
  
  /**
   * fetchEmailMessage
   * @param strEmailLogId
   * @param hmResult
   * @param strEmailMsg
   * @return strEmailMsg
  */
  public String  fetchEmailMessage(String strEmailLogId) throws AppError {
	    StringBuffer sbQuery = new StringBuffer();
	    HashMap hmResult = new HashMap();
	    String strEmailMsg = "";
        GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    
        sbQuery.append("select c916_email_msg EMAILMSG ");
        sbQuery.append("from t916_email_log where ");
        sbQuery.append("c916_email_log_id='"+strEmailLogId+"' ");  
        sbQuery.append("AND c916_void_fl is null");
        
        log.debug(" Query For loadpopupemail() : " + sbQuery.toString());
        gmDBManager.setPrepareString(sbQuery.toString());
        hmResult =gmDBManager.querySingleRecord();
        strEmailMsg = GmCommonClass.parseNull((String)hmResult.get("EMAILMSG"));
	    return strEmailMsg;
       }
  }
	    