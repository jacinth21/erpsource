package com.globus.custservice.beans;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * @author HReddi
 * Interface created for write method to print the Consign Version for US and BBA.	 *
 */	
public interface GmConsignPackSLipInterface {
	public HashMap loadSavedSetMaster (String strConsignID);	
}
