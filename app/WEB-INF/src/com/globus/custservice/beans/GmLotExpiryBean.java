package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
/**
 * GmLotExpiryBean - This bean class used to fetch and return sterile part Details for given input orderid
 * 					
 * 								 
 * 
 * @author rselvaraj
 *
 */
public class GmLotExpiryBean extends GmBean{
	
	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmLotExpiryBean(GmDataStoreVO gmDataStoreVO) throws Exception {
		super(gmDataStoreVO);
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * fetchSterilePartOrderTxn - This method used to fetch and return the sterile parts details
	 * and product family
	 * 
	 * @exception AppError
	 **/
	
	public ArrayList fetchSterilePartOrder(String strOrderId) throws AppError {
		ArrayList alResult = new ArrayList();
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_sterile_parts.gm_fch_sterile_part_order", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strOrderId);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		Iterator itrSterileDetails = alResult.iterator();
	    while (itrSterileDetails.hasNext()) {
	      HashMap hmSterilePart = (HashMap) itrSterileDetails.next();
	      String strPnum = GmCommonClass.parseNull((String) hmSterilePart.get("PNUM"));
	      alReturn.add(strPnum);
	    }
		gmDBManager.close();
	    return alReturn;
		
	}
	
	
	/**
	 * fetchSterileParConsignment - This method used to fetch and return the sterile parts details for consignment id
	 * and product family
	 * 
	 * @exception AppError
	 **/
	
	public ArrayList fetchSterileParConsignment(String strConsigmentId) throws AppError {
		ArrayList alResult = new ArrayList();
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_sterile_parts.gm_fch_sterile_part_consignment", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strConsigmentId);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		Iterator itrSterileDetails = alResult.iterator();
	    while (itrSterileDetails.hasNext()) {
	      HashMap hmSterilePart = (HashMap) itrSterileDetails.next();
	      String strPnum = GmCommonClass.parseNull((String) hmSterilePart.get("PNUM"));
	      alReturn.add(strPnum);
	    }
		gmDBManager.close();
		log.debug("alReturn Size=>"+alReturn.size());
	    return alReturn;
		
	}
	


}
