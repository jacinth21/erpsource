package com.globus.custservice.beans;
import com.globus.common.beans.AppError;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

public abstract class GmConsignPackSlipInstance implements GmConsignPackSLipInterface {
	/**
	 * This method returns the object dynamically. 
	 * @param strClassName
	 * @return
	 * @throws Exception
	 */
	
	public static GmConsignPackSLipInterface getInstance(String strClassName) throws AppError	{	
//		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
		GmConsignPackSLipInterface clientInterface = null;
	//	log.debug("The Class Name Comming from Servlet ****** "+strClassName);
		try {
			Class packSlipClient = Class.forName(strClassName);
			clientInterface = (GmConsignPackSLipInterface) packSlipClient
					.newInstance();
		} catch (Exception ex) {
			throw new AppError(ex);
		}
		return clientInterface;	  
	}
}
