package com.globus.custservice.beans;

import java.util.HashMap;

import com.globus.common.beans.AppError;

public interface GmPlaceOrderInterface {
  public HashMap saveItemOrder(HashMap hmParam) throws AppError;

  public String saveOrderDetails(HashMap hmParam, String strOrderId, String strUsername)
      throws AppError;
}
