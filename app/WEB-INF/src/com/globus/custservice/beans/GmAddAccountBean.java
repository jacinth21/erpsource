package com.globus.custservice.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.excel.GmExcelManipulation;
import org.quartz.JobDataMap;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * This Class will pass information using Calling the JMS
 * @author Mkosalram
 *
 */

public class GmAddAccountBean extends GmBean{

	public GmAddAccountBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	GmCommonClass gmCommonClass = new GmCommonClass();
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
	
	
	// PC-3178 New Customer not displayed in IML
	/**
	 * updateAccount:This method update the New Account
	 * @param hmParam
	 * @throws AppError
	 */
	public void updateAccount(HashMap hmParam) throws AppError {
		
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		HashMap hmMessageObj = new HashMap();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmAddAccountBean gmAddAccountBean = new GmAddAccountBean(gmDataStoreVO);
		// to get the queue name
		String strAddAccountQueue = GmCommonClass.parseNull((String) GmCommonClass.getJmsConfig("JMS_ADD_ACCOUNT_IML_QUEUE"));
		// to set the JMS class name and queue name
		String strAddAccountQueueConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("ADD_ACCOUNT_IML_CONSUMER_CLASS"));
		
		hmMessageObj.put("CONSUMERCLASS", strAddAccountQueueConsumerClass);
		hmMessageObj.put("QUEUE_NAME", strAddAccountQueue);
		hmMessageObj.put("HMACCVALUES", hmParam);
		gmConsumerUtil.sendMessage(hmMessageObj);
		log.debug("JMS Message sent");

	}
	
	// PC-3178 New Customer not displayed in IML
	/**
	 * addAccountIML method update lot expiry using jms configuration
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	
	public void addAccountIML(Object objMessageObject, JobDataMap jobDataMap) throws Exception{
		
	    HashMap hmMessageObj = (HashMap) objMessageObject;
	    HashMap hmAccValues = GmCommonClass.parseNullHashMap((HashMap) hmMessageObj.get("HMACCVALUES"));
	    
		String strLocId = GmCommonClass.parseNull((String) hmAccValues.get("LOCID"));
		String strLocRefId = GmCommonClass.parseNull((String) hmAccValues.get("LOCREFID"));
		String strLocType = GmCommonClass.parseNull((String) hmAccValues.get("LOCREFTYPE"));
		String strCompanyID = GmCommonClass.parseNull((String) hmAccValues.get("STRCOMPID"));
		String strUserID = GmCommonClass.parseNull((String) hmAccValues.get("STRUSERID"));
		
		try {

			if (strCompanyID.equals("1020")) {

				GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

				gmDBManager.setPrepareString("gm_pkg_lt_lottrack_txn.gm_sav_lot_location", 5);
				gmDBManager.setString(1, strLocId);
				gmDBManager.setString(2, strLocRefId);
				gmDBManager.setString(3, strLocType);
				gmDBManager.setString(4, strCompanyID);
				gmDBManager.setString(5, strUserID);
				gmDBManager.execute();
				gmDBManager.commit();
			}
		} catch (Exception ex) {
			log.error("General exception: " + ex.getMessage(), ex);
			throw ex;
		}
	}
}
