package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmSalesCreditBean - This is used Sales Credit fetch Credit Report and save the UsageLotDtls
 * @author rajan
 *
 */
public class GmSalesCreditBean extends GmBean {
  /**
   *  Code to Initialize the Logger Class.
   */
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Constructor will populate company info.
   * this will be removed all place changed with GmDataStoreVO constructor
   * @param gmDataStore
   */
  public GmSalesCreditBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveCreditforReturns - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public String saveCreditforReturns(String strRAId, Date dtCreditDate, String strUserComments,
      String strUserId, String strCreditString) throws AppError {
    String strOutInvoiceId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_SAVE_CREDIT_ORDER", 6);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.setString(1, strRAId);
    gmDBManager.setDate(2, dtCreditDate);
    gmDBManager.setString(3, strUserComments);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strCreditString);
    gmDBManager.execute();
    strOutInvoiceId = GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.commit();
    return strOutInvoiceId;
  }// End of saveCreditforReturns

  /**
   * loadOrderDetails - This method will return the order information based on the order id. This
   * Method included return information as part of the return
   * 
   * @param String strOrdId
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadOrderDetailsWithReturn(String strOrdId) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	C501_ORDER_ID ID, GET_ACCOUNT_NAME(C704_ACCOUNT_ID) ANAME, GET_DIST_REP_NAME(C703_SALES_REP_ID) REPDISTNM, ");
      sbQuery
          .append(" GET_SHIP_ADD(C501_SHIP_TO,C704_ACCOUNT_ID,C703_SALES_REP_ID,C501_SHIP_TO_ID) SHIPADD, ");
      sbQuery
          .append(" C501_ORDER_DATE ODT,C501_SURGERY_DATE SURG_DT, C501_CUSTOMER_PO PO, GET_BILL_ADD(C704_ACCOUNT_ID) BILLADD, ");
      sbQuery.append(" C501_TOTAL_COST + NVL(C501_SHIP_COST,0) SALES, C501_SHIP_COST, ");
      sbQuery.append(" C501_COMMENTS COMMENTS, GET_USER_NAME(C501_CREATED_BY) UNAME, ");
      sbQuery.append(" C501_SHIP_TO SHIPTO, C501_SHIP_TO_ID SHIPTOID, C704_ACCOUNT_ID ACCID, ");
      sbQuery.append(" get_account_curr_symb(C704_ACCOUNT_ID) ACC_CURR_SYMB,");
      sbQuery
          .append(" C901_ORDER_TYPE ORDERTYPE, C901_REASON_TYPE REASONTYPE, C501_PARENT_ORDER_ID PARENTORDID, ");
      sbQuery
          .append(" C501_VOID_FL VOIDFL, C501_STATUS_FL SFL, C501_UPDATE_INV_FL INVFL,C501_SHIPPING_DATE SDATE, GET_ACCOUNT_ATTRB_VALUE(C704_ACCOUNT_ID,106100) OVRIDE_CURR ");// 106100:
                                                                                                                                                                               // Override
                                                                                                                                                                               // Currency
      sbQuery.append(" FROM T501_ORDER ");
      sbQuery.append(" WHERE C501_ORDER_ID = '");
      sbQuery.append(strOrdId);
      sbQuery.append("' AND C501_DELETE_FL IS NULL");

      HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmReturn.put("ORDERDETAILS", hmResult);
      sbQuery.setLength(0);

      sbQuery.append(" SELECT t502.C205_PART_NUMBER_ID ID,  ");
      sbQuery.append(" GET_PARTNUM_DESC(t502.C205_PART_NUMBER_ID) PDESC, ");
      sbQuery.append(" t502.C502_CONTROL_NUMBER CNUM, ");
      sbQuery.append(" t502.C502_ITEM_QTY QTY,  ");
      sbQuery.append(" t502.C502_ITEM_PRICE PRICE,   ");
      sbQuery.append("  NVL(T507.C507_ITEM_QTY,0) RTN_ITEM_QTY ");
      sbQuery.append(" ,T502.C901_TYPE ITEMTYPE , GET_CODE_NAME_ALT(C901_TYPE) ITEMTYPEDESC ");
      sbQuery
          .append(" ,get_account_part_pricing (t502.c704_account_id, t502.c205_part_number_id, get_account_gpo_id (t502.c704_account_id)) CURR_PRICE  , T502.unitprice unitprice");
      sbQuery
          .append(" , T502.UnitpriceAdj UnitpriceAdj , T502.unitpriceAdjCode unitpriceAdjCode , T502.netunitprice netunitprice , T502.UNITPRICEADJCODEID UNITPRICEADJCODEID");
      sbQuery
          .append(" FROM ( SELECT T502.C501_ORDER_ID, T502.C205_PART_NUMBER_ID, T502.C502_CONTROL_NUMBER, T502.C502_ITEM_PRICE , T501.C704_ACCOUNT_ID, T502.C502_ITEM_SEQ_NO, T502.C901_TYPE, SUM(T502.C502_ITEM_QTY) C502_ITEM_QTY  , NVL(t502.c502_unit_price, '0') unitprice , t502.c502_unit_price_adj_value  UnitpriceAdj , DECODE (t502.c901_unit_price_adj_code, NULL, 'N/A', GET_CODE_NAME (t502.c901_unit_price_adj_code)) unitpriceAdjCode ,t502.c901_unit_price_adj_code UNITPRICEADJCODEID  , t502.C502_ITEM_PRICE netunitprice ");
      sbQuery
          .append("  FROM T501_ORDER T501, T502_ITEM_ORDER T502 WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID AND T502.C502_VOID_FL   IS NULL AND T502.C502_DELETE_FL IS NULL ");
      sbQuery.append(" AND  T501.C501_ORDER_ID = '");
      sbQuery.append(strOrdId);
      sbQuery
          .append("' AND T501.C501_VOID_FL   IS NULL GROUP BY T502.C501_ORDER_ID, T501.C704_ACCOUNT_ID, T502.C205_PART_NUMBER_ID, T502.C502_CONTROL_NUMBER, T502.C502_ITEM_PRICE, T502.C502_ITEM_SEQ_NO , T502.C901_TYPE , t502.c502_unit_price , t502.c502_unit_price_adj_value , t502.c901_unit_price_adj_code ,t502.C502_ITEM_PRICE ) T502 ");
      sbQuery
          .append(" , (SELECT T506.C501_ORDER_ID, T507.C205_PART_NUMBER_ID, T507.C507_ORG_CONTROL_NUMBER, T507.C507_ITEM_PRICE, SUM(T507.C507_ITEM_QTY) C507_ITEM_QTY  ");
      sbQuery.append(" FROM T507_RETURNS_ITEM T507, T506_RETURNS T506 ");
      sbQuery
          .append(" WHERE T506.C506_RMA_ID = T507.C506_RMA_ID  AND T507.C507_STATUS_FL <> 'Q'  AND T506.C506_VOID_FL IS NULL AND T506.C501_ORDER_ID ='");
      sbQuery.append(strOrdId);
      sbQuery.append("' AND T506.c1900_company_id = '" + getCompId() + "'");
      sbQuery
          .append(" GROUP BY T506.C501_ORDER_ID, T507.C205_PART_NUMBER_ID, T507.C507_ORG_CONTROL_NUMBER, T507.C507_ITEM_PRICE) T507 ");
      sbQuery.append(" WHERE T502.C501_ORDER_ID = T507.C501_ORDER_ID(+) ");
      sbQuery.append(" AND T502.C205_PART_NUMBER_ID = T507.C205_PART_NUMBER_ID(+) ");
      sbQuery.append(" AND T502.C502_CONTROL_NUMBER = T507.C507_ORG_CONTROL_NUMBER(+) ");
      sbQuery.append(" AND T502.C502_ITEM_PRICE = T507.C507_ITEM_PRICE(+) ");
      sbQuery.append(" ORDER BY T502.C502_ITEM_SEQ_NO, T502.C205_PART_NUMBER_ID");

      log.debug("Query for CARTDETAILS is " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("CARTDETAILS", alReturn);

      hmResult = new HashMap();
      sbQuery.setLength(0);

      sbQuery
          .append(" SELECT	C501_SHIPPING_DATE SDT, C501_DELIVERY_CARRIER SCAR, C501_DELIVERY_MODE SMODE,  ");
      sbQuery.append(" C501_TRACKING_NUMBER STRK , GET_CODE_NAME(C501_DELIVERY_CARRIER) SCARNM, ");
      sbQuery.append(" GET_CODE_NAME(C501_DELIVERY_MODE) SMODENM, C501_SHIP_COST SCOST ");
      sbQuery.append(" FROM T501_ORDER ");
      sbQuery.append(" WHERE C501_ORDER_ID = '");
      sbQuery.append(strOrdId);
      sbQuery.append("' AND C501_DELETE_FL IS NULL");

      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmReturn.put("SHIPDETAILS", hmResult);

      alReturn = loadOrderReturnsReport(strOrdId);
      hmReturn.put("RETURNS", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesCreditBean:loadOrderDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadOrderDetails

  /**
   * loadCreditReportList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadCreditReportList(String strType) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmCommonClass gmCommon = new GmCommonClass();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());

    try {
      alReturn = gmCust.getDistributorList();
      hmReturn.put("DISTLIST", alReturn);
      // alReturn = gmCommon.getCodeList("RETTY");
      alReturn = gmCommon.getCodeList("RETTY");
      hmReturn.put("CODELIST", alReturn);

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesCreditBean:loadCreditReportList", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadCreditReportList

  /**
   * reportCreditsByDistributor - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList reportCreditsByDistributor(String strDistId, String strType, String strFrmDate,
      String strToDate) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	C506_RMA_ID RAID, C701_DISTRIBUTOR_ID DID, GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID) DNAME, ");
      sbQuery.append(" C506_COMMENTS COMMENTS, C506_CREATED_DATE CDATE, ");
      sbQuery.append(" GET_USER_NAME(C506_CREATED_BY) PER, GET_CODE_NAME(C506_TYPE) TYPE, ");
      sbQuery.append(" GET_CODE_NAME(C506_REASON) REASON, C207_SET_ID SETID, ");
      sbQuery.append(" C506_CREDIT_DATE CRDATE, C506_RETURN_DATE RDATE ");
      sbQuery.append(" ,GET_RETURNED_PARTNUM_QTY(C506_RMA_ID) RAQTY ");
      sbQuery.append(" FROM T506_RETURNS ");
      sbQuery.append(" WHERE C506_STATUS_FL = '2' ");
      sbQuery.append(" AND C506_VOID_FL IS NULL ");
      sbQuery.append(" AND C207_SET_ID is null ");
      sbQuery.append(" AND C506_TYPE IN (");
      sbQuery.append(strType);
      sbQuery.append(") ");
      sbQuery.append("  AND C5040_PLANT_ID = '" + getCompPlantId() + "' ");
      if (!GmCommonClass.parseZero(strDistId).equals("0")) {
        sbQuery.append(" AND C701_DISTRIBUTOR_ID = ");
        sbQuery.append(strDistId);
      }
      sbQuery.append(" AND C506_CREDIT_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFrmDate);
      sbQuery.append("','" + strDateFormat + "') AND to_date('");
      sbQuery.append(strToDate);
      sbQuery.append("','" + strDateFormat + "')");
      sbQuery.append(" ORDER BY C506_CREDIT_DATE");

      log.debug(" ** sbQuery reportCreditsByDistributor " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesCreditBean:reportCreditsByDistributor", "Exception is:"
          + e);
    }
    return alReturn;
  } // End of reportCreditsByDistributor



  /**
   * loadCreditReport - This method will price adjusted invoice (Part Return)
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadCashCreditReport(String strOrderID) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT 'N/A' RAID,");
      sbQuery.append(" 'N/A' DID, ");
      sbQuery.append(" 'N/A' DNAME, ");
      sbQuery.append(" 'N/A' COMMENTS, ");
      sbQuery.append(" T501.C501_CREATED_DATE CDATE, ");
      sbQuery.append(" T501.C501_ORDER_DATE CREDITDATE, ");
      sbQuery.append(" GET_USER_NAME(T501.C501_CREATED_BY) PER, ");
      sbQuery.append(" 'N/A' TYPE, ");
      sbQuery.append(" 'N/A' MEMOID, ");
      sbQuery.append(" 'N/A'  REASON, ");
      sbQuery.append(" 'N/A'  SETID, ");
      sbQuery.append(" T501.C501_CUSTOMER_PO PO, ");
      sbQuery.append(" GET_BILL_ADD(T501.C704_ACCOUNT_ID) ACCADD, ");
      sbQuery.append(" T501.C501_LAST_UPDATED_DATE UDATE, ");
      sbQuery.append(" T503.C503_INVOICE_ID INVID,  ");
      sbQuery.append(" T501.C501_COMMENTS ORDER_COMMENTS, ");
      sbQuery.append(" to_date(null,'" + getCompDateFmt() + "') RETDATE ");
      sbQuery.append(" , get_account_curr_symb (t501.c704_account_id) ACCOUNT_CURR_NM ");
      // sbQuery.append(" --to_char(T501C501_CREDIT_DATE,get_rule_value('DATEFMT','DATEFORMAT')) CREDITDATE ");
      sbQuery.append(" FROM 	T501_ORDER T501, ");
      sbQuery.append(" T503_INVOICE T503 ");
      sbQuery.append(" WHERE   T501.C501_ORDER_ID = '");
      sbQuery.append(strOrderID);
      sbQuery.append("' ");
      sbQuery.append(" AND T501.C501_CUSTOMER_PO  = T503.C503_CUSTOMER_PO(+)");
      sbQuery.append(" AND T503.C503_VOID_FL IS NULL ");

      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

      hmReturn.put("RADETAILS", hmResult);

      sbQuery.setLength(0);
      sbQuery.append(" SELECT	 SUM(C502_ITEM_QTY) QTY, C502_ITEM_PRICE PRICE, 'N/A' SFL, ");
      sbQuery.append(" C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC ");
      sbQuery.append(" FROM T502_ITEM_ORDER   ");
      sbQuery.append(" WHERE C501_ORDER_ID = '");
      sbQuery.append(strOrderID);
      sbQuery.append("' ");
      sbQuery.append(" AND C502_VOID_FL IS NULL ");
      sbQuery.append(" GROUP BY C502_ITEM_PRICE,C205_PART_NUMBER_ID ");
      sbQuery.append(" ORDER BY C205_PART_NUMBER_ID ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("RAITEMDETAILS", alReturn);


    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesCreditBean:loadCreditReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadCreditReport

  /**
   * getRAID - This method will return the RAID for the passed Order ID
   * 
   * @param String strRAID
   * 
   * @return HashMap
   * @exception AppError
   **/
  public String getRAID(String strOrderID) throws AppError {
    HashMap hmResult = new HashMap();
    String strRAID = "";

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT	T501.C506_RMA_ID RAID");
      sbQuery.append(" FROM 	T501_ORDER T501");
      sbQuery.append(" WHERE 	T501.C501_ORDER_ID = '");
      sbQuery.append(strOrderID);
      sbQuery.append("'");

      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      if (hmResult != null) {
        strRAID = (String) hmResult.get("RAID");
      }

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesCreditBean:loadCreditReport", "Exception is:" + e);
    }
    return strRAID;
  } // End of loadCreditReport

  /**
   * saveCreditforSales - This method will credit the payment when the system tries to credit the
   * invoice
   * 
   * @param String strInvIV
   * @param String strAccId
   * @param String strInputStr
   * @param String strComments
   * @param String strUserId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveCreditforSales(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    String strRAId = "";
    String strNewInvoiceId = "";

    String strInvId = GmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPO = GmCommonClass.parseNull((String) hmParam.get("PO"));
    String strInvType = GmCommonClass.parseNull((String) hmParam.get("INVTYPE"));

    log.debug(" Values in Hashmap " + hmParam);

    gmDBManager.setPrepareString("GM_SAVE_CREDIT_SALES", 9);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);


    gmDBManager.setString(1, strInvId);
    gmDBManager.setString(2, strAccId);
    gmDBManager.setString(3, strInputStr);
    gmDBManager.setString(4, strComments);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strPO);
    gmDBManager.setString(7, strInvType);

    gmDBManager.execute();
    strRAId = gmDBManager.getString(8);
    strNewInvoiceId = gmDBManager.getString(9);
    gmDBManager.commit();
    log.debug("RAID created is " + strRAId + " New Invoice ID is " + strNewInvoiceId);

    hmReturn.put("RAID", strRAId);
    hmReturn.put("NEWINVID", strNewInvoiceId);

    return hmReturn;
  } // End of saveCreditforSales


  /**
   * reportInvoiceByAccount - This method will load the unpaid invoice for the selected Account only
   * fetchs the record where invoice are generated and waiting for the payment
   * 
   * @param String strAccId
   * @param String strFormat
   * @return Object
   * @exception AppError
   **/
  public Object loadUnpaidInvoice(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strFormat = GmCommonClass.parseNull((String) hmParam.get("FORMAT"));
    String strInvId = GmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strCustPO = GmCommonClass.parseNull((String) hmParam.get("CUSTPO"));
    String strInvAmt = GmCommonClass.parseNull((String) hmParam.get("INVAMT"));
    String strInvoType = GmCommonClass.parseNull((String) hmParam.get("INVOTYP"));
    String strSort = GmCommonClass.parseNull((String) hmParam.get("SORT"));
    String strSortType = GmCommonClass.parseNull((String) hmParam.get("SORTTYPE"));
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    String strModifyInv = GmCommonClass.parseNull((String) hmParam.get("SRC"));
    String strFromPage = GmCommonClass.parseNull((String) hmParam.get("FROM"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("hFrom"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("hTo"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("strType"));
    String strGridSortField = GmCommonClass.parseNull((String) hmParam.get("strGridSortField"));
    String strGridSortType = GmCommonClass.parseNull((String) hmParam.get("strGridSortType"));
    String strRepOpenType = GmCommonClass.parseNull((String) hmParam.get("strRepOpenType"));
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    String strParentFL = GmCommonClass.parseNull((String) hmParam.get("PARENTFL"));
    String strAccCurrId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNT_CURR_ID"));
    String strStatusType = GmCommonClass.parseNull((String) hmParam.get("STATUSTYPE"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
    String strInvamtReplace=strInvAmt.replaceAll(",", "");
    //PMT-41835 Division Filter in the Invoice Report
    String strDivision = GmCommonClass.parseZero((String) hmParam.get("DIVISIONID"));
    //PMT-41835
    if (strGridSortType.equals("des")) {
      strGridSortType = "desc";
    }
    if (strType.equals("Group")) {
      strInvSource = "0";
    }
    ArrayList alReturn = new ArrayList();
    RowSetDynaClass rdReturnRowSet = null;
    log.debug("strInvSource " + strInvSource);

    if (strSort.equals("")) {
      strSort = "INVDT";
    }
//PC-4148 Remove Try Catch for Email Exception (TSK-23805)
      StringBuffer sbQuery = new StringBuffer();
      if (strFormat.equals("L10")) {
        sbQuery.append(" SELECT * FROM ( ");
      } if((strFormat.equals("OPEN")) && (!strRepOpenType.equals("SO"))) {
    	  sbQuery.append(" SELECT * FROM ( ");
      } else if (strFormat.equals("ALL") || strFormat.equals("OPEN")) {
        if (strRepOpenType.equals("SO")) {
          sbQuery.append(" SELECT * FROM (");
        }
      }

      sbQuery.append(" SELECT ");
      sbQuery.append(" B.C503_INVOICE_ID INVID, B.C503_CUSTOMER_PO PO,  ");
      sbQuery.append(" C503_INVOICE_DATE INVDT, ");

      if (strInvSource.equals("26240213")) {

        sbQuery.append(" get_payment_terms(B.C503_invoice_date,t101p.c901_payment_terms)  PAYDT,");
        sbQuery
            .append(" (TRUNC (CURRENT_DATE) - (TRUNC (get_payment_terms(B.C503_invoice_date,t101p.c901_payment_terms))))  OVERDUE,");
        sbQuery.append(" B.c101_dealer_id ACCID, ");
        sbQuery.append(" DECODE('" + getComplangid()
            + "','103097',NVL(c101_party_nm_en,c101_party_nm),c101_party_nm) NAME ");
        sbQuery.append(" ,get_acct_curr_symb_by_dealer(B.c101_dealer_id) CURRENCY ");
        sbQuery.append(" ,GET_DEALER_BILL_ADD(B.c101_dealer_id) BILLADD ");
        sbQuery
            .append(" , t101p.C101_EMAIL_FL EMAILREQ  , t101p.C901_EMAIL_FILE_FMT EVERSION , t101p.c101_vendor_id VENDOR ");

      } else {
        sbQuery.append("  get_payment_terms(B.C503_invoice_date,t704.C704_PAYMENT_TERMS)  PAYDT,");
        sbQuery
            .append(" (TRUNC (CURRENT_DATE) - (TRUNC (get_payment_terms(B.C503_invoice_date,t704.C704_PAYMENT_TERMS))))  OVERDUE,");
        sbQuery
            .append(" DECODE('"
                + getComplangid()
                + "','103097',NVL(t704.C704_ACCOUNT_NM_EN,t704.C704_ACCOUNT_NM),t704.C704_ACCOUNT_NM)  NAME, B.C704_ACCOUNT_ID ACCID ");
        sbQuery
            .append(" ,DECODE (b.C901_INVOICE_SOURCE,50256, get_rule_value (50218, get_distributor_inter_party_id (b.c701_distributor_id)),50253,get_rule_value (50218, get_distributor_inter_party_id (b.c701_distributor_id)),50257, ");
        sbQuery
            .append(" get_rule_value (50218, get_distributor_inter_party_id (get_account_dist_id (b.c704_account_id))),get_code_name(t704.c901_currency)) CURRENCY ");
        sbQuery.append(" ,GET_BILL_ADD(B.C704_ACCOUNT_ID) BILLADD ");
        sbQuery
            .append(" , DECODE( B.C901_INVOICE_SOURCE,'50253',GET_RULE_VALUE ('50217',GET_DISTRIBUTOR_INTER_PARTY_ID (GET_ACCOUNT_DIST_ID (B.C704_ACCOUNT_ID))),'') ICSHEADADD ");
        sbQuery
            .append(" ,GET_ACCOUNT_ATTRB_VALUE (B.c704_ACCOUNT_ID, 91983) EMAILREQ  , GET_ACCOUNT_ATTRB_VALUE (B.c704_ACCOUNT_ID, 91984) EVERSION , GET_ACCOUNT_ATTRB_VALUE (B.c704_ACCOUNT_ID, 106642) VENDOR");
      }

      sbQuery.append(" ,C503_INVOICE_DATE INVDTREPORT, ");
      sbQuery.append(" NVL(B.C503_INV_AMT, 0) INVAMT, ");
      sbQuery.append(" GET_LOG_FLAG(B.C503_INVOICE_ID,1201) CALL_FLAG, ");
      sbQuery.append(" NVL(B.C503_INV_PYMNT_AMT, 0) AMTPAID ");
      sbQuery.append(" ,C901_INVOICE_TYPE INVTPID ");
      sbQuery
          .append(" ,DECODE(C901_INVOICE_TYPE,50200,'regular',50201,'obo',50202,'credit',50203,'debit') INVTP ");


      sbQuery
      		.append(" ,B.C903_PDF_FILE_ID PDFFILEID, B.C903_CSV_FILE_ID  CSVFILEID ");
      sbQuery.append(" ,B.C903_RTF_FILE_ID RTFFILEID ");
      sbQuery.append(" ,TO_CHAR (B.C503_INVOICE_DATE, 'YYYY') INVYEAR ");

      sbQuery.append(" FROM T503_INVOICE B , t901_code_lookup t901 , t1900_company t1900 ");

      if (strInvSource.equals("26240213")) {

        sbQuery.append(" ,t101_party t101,t101_party_invoice_details t101p");

      } else {
        sbQuery.append(" , t704_account t704 ");
      }


      sbQuery
          .append(" WHERE B.C503_VOID_FL IS NULL AND (B.c901_ext_country_id is NULL OR B.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)) ");// To
                                                                                                                                                                // include
                                                                                                                                                                // ext
                                                                                                                                                                // country
                                                                                                                                                                // id
                                                                                                                                                                // for
                                                                                                                                                                // GH
      if (strInvSource.equals("26240213")) {
        sbQuery.append(" AND t101.c101_void_fl IS NULL ");
        sbQuery.append(" AND t101p.c101_void_fl IS NULL ");
        sbQuery.append(" AND B.c101_dealer_id = t101.c101_party_id ");
        sbQuery.append(" AND t101.c101_party_id = t101p.c101_party_id ");
      } else {
        sbQuery.append(" AND B.C704_ACCOUNT_ID = t704.C704_ACCOUNT_ID ");
        sbQuery.append(" AND t704.c901_currency = " + strAccCurrId);
      }
      // 26240439 - invoice statement , to avoid displaying invoice statement types
      sbQuery
          .append(" AND NVL (B.c901_INVOICE_TYPE, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906"
              + "  WHERE t906.c906_rule_grp_id = 'INVOICETYPE' AND C906_RULE_ID = 'EXCLUDE' AND C906_VOID_FL IS NULL) ");
      sbQuery.append(" AND B.C1900_COMPANY_ID = ");
      sbQuery.append(getCompId());
      sbQuery.append(" AND b.c1900_company_id = t1900.c1900_company_id ");
      sbQuery.append(" AND t901.c901_code_id = NVL(B.C503_TERMS,-9999) ");

      if (!strAccId.equals("") && !strAccId.equals("0") && !strParentFL.equals("")
          && (!strInvSource.equals("26240213"))) {
        if (!strType.equals("Group"))// For Group Statement Report
        {
          sbQuery
              .append(" AND T704.c101_party_id  In (SELECT c101_party_id from t704_account where c704_account_id ="
                  + strAccId + ") ");
        }
      }

      // sbQuery.append(" and NVL (get_ac_fch_invoice_amt (b.c503_invoice_id), 0) <> 0 ");

      if (!strAccId.equals("") && !strAccId.equals("0") && strParentFL.equals("")) {

        if (strInvSource.equals("50255") || strInvSource.equals("50257")
            || strInvSource.equals("50253")) {
          /*
           * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
           * the data fetched should be based on Accounts.
           */
          sbQuery.append(" AND B.C704_ACCOUNT_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");

        } else if (strInvSource.equals("50256")) {
          sbQuery.append(" AND B.C701_distributor_id = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        } else if (strInvSource.equals("26240213")) {
          sbQuery.append(" AND B.c101_dealer_id = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        }
      }

      if (strType.equals("Group") && (!strInvSource.equals("26240213")))// For Group Statement
                                                                        // Report
      {
        sbQuery.append(" AND EXISTS");
        sbQuery
            .append("(SELECT t704a.c704_account_id   FROM t704a_account_attribute t704a WHERE t704a.c704a_attribute_value = '"
                + strAccId
                + "' AND t704a.c901_attribute_type='7008' and t704a.c704_account_id = B.C704_ACCOUNT_ID)");
      }

      if (!strInvId.equals("")) {
        if (!strAccId.equals("") && !strAccId.equals("0")) {
          sbQuery.append(" AND B.C503_INVOICE_ID = '");
          sbQuery.append(strInvId);
          sbQuery.append("'");
        } else {
          sbQuery.append(" AND B.C503_INVOICE_ID = '");
          sbQuery.append(strInvId);
          sbQuery.append("'");
        }
      }
      // if (strFromPage.equals("POSTPAYMENT"))
      if (strFormat.equals("OPEN")) {
        sbQuery.append(" AND B.C503_STATUS_FL IN('0','1') ");
      }

      if (strStatusType.equals("1")) {
        // sbQuery.append(" AND INVSFL IN (4,5) ");
        sbQuery.append(" AND B.C503_STATUS_FL < = ");
        sbQuery.append(strStatusType);
      } else if (strStatusType.equals("2")) {
        sbQuery.append(" AND B.C503_STATUS_FL = ");
        sbQuery.append(strStatusType);
      }

      // Filter By Order
      if (!strOrderId.equals("")) {
        sbQuery.append(" AND (B.C503_INVOICE_ID) IN ");
        sbQuery.append(" ( SELECT C503_INVOICE_ID ");
        sbQuery.append(" FROM  T501_ORDER ");
        sbQuery.append(" WHERE C501_ORDER_ID  =  '");
        sbQuery.append(strOrderId);
        sbQuery.append("' AND nvl(C901_order_type,-9999) NOT IN ('2535','101260')  ");
        sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
        sbQuery.append(" SELECT t906.c906_rule_value ");
        sbQuery.append(" FROM t906_rules t906 ");
        sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
        sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ) ");
      }

      if (!strCustPO.equals("")) {
        sbQuery.append(" AND B.C503_CUSTOMER_PO = '");
        sbQuery.append(strCustPO);
        sbQuery.append("' ");
      }
      if (!strInvamtReplace.equals("")) {
        sbQuery.append(" AND NVL(B.C503_INV_AMT, 0)= '");
        sbQuery.append(strInvamtReplace);
        sbQuery.append("'");
      }

      if (!strInvoType.equals("")) {
        sbQuery.append(" AND B.C901_INVOICE_TYPE IN (");
        sbQuery.append(strInvoType);
        sbQuery.append(")");
      }

      if (!strInvSource.equals("0")) {
        sbQuery.append(" AND B.C901_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);

      }
    //PMT-41835 Division Filter in the Invoice Report
      if(!strDivision.equals("0")){
      	sbQuery.append(" AND B.C1910_DIVISION_ID = ");
      	sbQuery.append(strDivision);
      	sbQuery.append("");
      }//PMT-41835


      /*
       * sbQuery.append("' AND B.C503_CUSTOMER_PO IN ");
       * sbQuery.append(" ( SELECT A.C501_CUSTOMER_PO "); sbQuery.append(" FROM T501_ORDER A ");
       * sbQuery.append(" WHERE A.C501_STATUS_FL IS NOT NULL ");
       * 
       * if (strFormat.equals("ALL")) { sbQuery.append(" AND A.C501_STATUS_FL >= 4 "); } else if
       * (strFormat.equals("OPEN")) { sbQuery.append(" AND A.C501_STATUS_FL = 4 "); }
       * 
       * sbQuery.append(" AND A.C704_ACCOUNT_ID = '"); sbQuery.append(strAccId);
       * sbQuery.append("' AND A.C501_DELETE_FL IS NULL )");
       */


      /* sbQuery.append(" ORDER BY NAME ASC,INVDTREPORT DESC "); */
      // sbQuery.append("ORDER BY INVDT");

      // if (strFromPage.equals("POSTPAYMENT")){

      if (!strFromDt.equals("") && !strToDt.equals("")) {
        if(strDateType.equals("106461")){
            sbQuery.append(" AND TRUNC(C503_INVOICE_DATE) between ");
            sbQuery.append(" to_date('");
            sbQuery.append(strFromDt);
            sbQuery.append("','" + strDateFormat + "') AND to_date('");
            sbQuery.append(strToDt);
            sbQuery.append("','" + strDateFormat + "')");
        }else if(strDateType.equals("106460") && strModifyInv.equals("MODIFYINV")){
            sbQuery.append(" AND TRUNC(C503_CREATED_DATE) between ");
            sbQuery.append(" to_date('");
            sbQuery.append(strFromDt);
            sbQuery.append("','" + strDateFormat + "') AND to_date('");
            sbQuery.append(strToDt);
            sbQuery.append("','" + strDateFormat + "')");
        }
      }
      if (strFormat.equals("ALL") || strFormat.equals("OPEN")) {
        if (strRepOpenType.equals("SO")) {
          sbQuery.append(" )  WHERE OVERDUE >0 AND INVAMT != 0");
        }
      }

      if (strFormat.equals("OPEN")) {


        if (!strGridSortField.equals("") && !strGridSortType.equals(""))// Sort the Data while
                                                                        // Printing Statement
        {
          sbQuery.append(" ORDER BY " + strGridSortField + " " + strGridSortType);
        } else {
          sbQuery.append(" ORDER BY ");
          sbQuery.append(strSort);
          // sbQuery.append("");

          if (strSortType.equals("1"))
            sbQuery.append(" DESC");
          else
            sbQuery.append(" ASC ");
        }
      } else {
        sbQuery.append(" ORDER BY INVID DESC ");
      }
      if (strFormat.equals("L10")) {
        sbQuery.append(") WHERE ROWNUM < 11 ");
      }
      else if ((strFormat.equals("OPEN")) && (!strRepOpenType.equals("SO"))) {
          sbQuery.append(") WHERE INVAMT != 0 ");
       }
      log.debug(strModifyInv + "Query for loadUnpaidInvoice is " + sbQuery.toString());
      // log.debug("Values: "+gmDBManager.queryMultipleRecords(sbQuery.toString()));

      if (strModifyInv.equals("MODIFYINV")) {
        rdReturnRowSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
        return rdReturnRowSet;
      } else {
        alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      }
      
    return alReturn;
  } // End of reportInvoiceByAccount

  /**
   * loadOrderReturnsReport - This method returns all RA's for a Sales Order
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadOrderReturnsReport(String strOrderId) throws AppError {
    ArrayList alReturn = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	C506_RMA_ID RAID, C506_COMMENTS COMMENTS, GET_CODE_NAME(C506_TYPE) TYPE, GET_CODE_NAME(C506_REASON) REASON,");
      sbQuery
          .append(" C506_RETURN_DATE RETDATE, DECODE(C506_STATUS_FL,0,'Initiated',1,'Returned',2,'Credited') SFL, ");
      sbQuery.append(" C506_CREATED_DATE CDATE, get_user_name(C506_CREATED_BY) CUSER, ");
      sbQuery.append(" C506_EXPECTED_DATE EDATE, C506_CREDIT_DATE CRDATE ");
      sbQuery.append(" FROM T506_RETURNS ");
      sbQuery.append(" WHERE C501_ORDER_ID = '");
      sbQuery.append(strOrderId);
      sbQuery.append("' AND C506_VOID_FL IS NULL");
      sbQuery.append(" ORDER BY REGEXP_REPLACE(C506_RMA_ID,'\\D') ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesCreditBean:loadOrderReturnsReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of loadOrderReturnsReport

  /**
   * getPamentTerm - This method will return the Payment Term for the passed Account ID
   * 
   * @param String Account Id
   * 
   * @return String
   * @exception AppError
   **/
  public String getPaymentTerm(String strAccountId) throws AppError {
    HashMap hmResult = new HashMap();
    String strPaymentTerm = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT GET_CODE_NAME(C704_PAYMENT_TERMS) PAYMENTTERM ");
    sbQuery.append(" FROM 	T704_ACCOUNT T704");
    sbQuery.append(" WHERE 	T704.C704_ACCOUNT_ID = '");
    sbQuery.append(strAccountId);
    sbQuery.append("'");

    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    if (hmResult != null) {
      strPaymentTerm = (String) hmResult.get("PAYMENTTERM");
    }
    return strPaymentTerm;
  } // End of getPaymentTerm

  /**
   * getPamentTermForDealer - This method will return the Payment Term for the passed Dealer Id
   * 
   * @param String Dealer Id
   * 
   * @return String
   * @exception AppError
   **/
  public String getPaymentTermForDelaer(String strDealerId) throws AppError {
    HashMap hmResult = new HashMap();
    String strPaymentTerm = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT t901.c901_code_nm PAYMENTTERM ");
    sbQuery.append(" FROM t101_party_invoice_details t101,t901_code_lookup t901");
    sbQuery
        .append("  WHERE t101.c901_payment_terms = t901.c901_code_id  AND t101.c101_party_id = '");
    sbQuery.append(strDealerId);
    sbQuery.append("'");
    sbQuery.append(" AND t901.c901_void_fl IS NULL AND t101.C101_VOID_FL IS NULL ");

    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    if (hmResult != null) {
      strPaymentTerm = (String) hmResult.get("PAYMENTTERM");
    }
    return strPaymentTerm;
  } // End of getPaymentTermForDealer

  /**
   * fetchAccountActivityRpt - This method will return the Account Activity reports
   * 
   * @param HashMap
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchAccountActivityRpt(HashMap hmParam) throws AppError, ParseException {
    ArrayList alResult = new ArrayList();
    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    String strSource = GmCommonClass.parseZero((String) hmParam.get("INVSOURCE"));
    String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
    String strFromDt = GmCommonClass.parseZero((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseZero((String) hmParam.get("TODT"));
    String strAccIds = GmCommonClass.parseZero((String) hmParam.get("ACCID"));
    String strInvTypes = GmCommonClass.parseZero((String) hmParam.get("INVOTYP"));
    String strDateFmt = GmCommonClass.parseZero((String) hmParam.get("APPLNDATEFMT"));
    String strOpt = GmCommonClass.parseZero((String) hmParam.get("STROPT"));
    String strARCurrSymbol = GmCommonClass.parseZero((String) hmParam.get("ARCURRENCY"));
    Date dtFromDt = null;
    Date dtToDt = null;
    dtFromDt = GmCommonClass.getStringToDate(strFromDt, strDateFmt);
    dtToDt = GmCommonClass.getStringToDate(strToDt, strDateFmt);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_fch_activity_rpt", 10);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
    gmDBManager.setString(1, strSource);
    gmDBManager.setString(2, strAccIds);
    gmDBManager.setString(3, strInvTypes);
    gmDBManager.setString(4, strStatus);
    gmDBManager.setDate(5, new java.sql.Date(dtFromDt.getTime()));
    gmDBManager.setDate(6, new java.sql.Date(dtToDt.getTime()));
    gmDBManager.setString(7, strOpt);
    gmDBManager.setInt(8, Integer.parseInt(strARCurrSymbol));
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(9));
    hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(10));
    gmDBManager.close();
    hmReturn.put("ALRESULT", alResult);
    hmReturn.put("HEADER", hmResult);
    return hmReturn;
  }

  /**
   * fetchInvoiceHeaderDtls - This method will Fetch the Invoice Statement Header Details
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchInvoiceHeaderDtls(HashMap hmParam) throws AppError, ParseException {
    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    String strInvSource = GmCommonClass.parseZero((String) hmParam.get("INVSOURCE"));
    String strAccountID = GmCommonClass.parseZero((String) hmParam.get("ACCID"));
    String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_fch_inv_header_dtls", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInvSource);
    gmDBManager.setString(2, strAccountID);
    gmDBManager.setString(3, strStatus);
    gmDBManager.execute();
    hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    return hmResult;
  }




} // End of GmSalesCreditBean
