package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDORptBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmDORptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDORptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public ArrayList fetchDODashboard(HashMap hmParam) throws AppError {

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));

    sbQuery
        .append("SELECT t7100.c7100_case_id caseid, get_rep_name (t501.c703_sales_rep_id) rep_name,");
    sbQuery.append("get_account_name (t501.c704_account_id) acc_name,");
    sbQuery
        .append(" to_char(t7100.c7100_surgery_date,'mm/dd/yyyy') sdate, to_char(TO_DATE(t501a.c501a_attribute_value,'dd/mm/yyyy HH:MI:SS'),'mm/dd/yyyy') requested_date, ");
    sbQuery
        .append(" total_cost , t501.c501_order_id order_id ,get_code_name(t501.c901_order_type) order_type");
    sbQuery.append(" FROM t501_order t501");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(", t7100_case_information t7100,");


    sbQuery.append(" ( SELECT t501a.c501a_attribute_value , t501a.c501_order_id  ");
    sbQuery.append(" FROM  t501a_order_attribute t501a ");
    sbQuery.append(" WHERE  t501a.c501a_order_attribute_id IN ( ");
    sbQuery.append(" SELECT MAX (t501a.c501a_order_attribute_id)");
    sbQuery.append(" FROM t501a_order_attribute t501a ");
    sbQuery.append(" WHERE  c901_attribute_type = 11240");
    sbQuery.append(" AND c501a_void_fl IS NULL ");
    sbQuery.append(" GROUP BY t501a.c501_order_id)) t501a ,");
    sbQuery.append(" (SELECT ord_id, SUM (price) total_cost ");
    sbQuery.append(" FROM (SELECT NVL (t501.c501_parent_order_id, t501.c501_order_id) ord_id,");
    sbQuery.append(" SUM (t502.c502_item_price * t502.c502_item_qty) price");
    sbQuery.append(" FROM t501_order t501, t502_item_order t502 ");
    sbQuery.append(" WHERE ( t501.c501_order_id = t502.c501_order_id");
    sbQuery.append(" OR t501.c501_parent_order_id = t502.c501_order_id)");
    sbQuery.append(" AND t501.c901_order_type IN (2518,2519)");// Rep Draft ,Pending CS Confirmation
    sbQuery.append(" AND c501_delete_fl IS NULL");
    sbQuery.append(" AND c501_void_fl IS NULL");
    sbQuery.append(" AND t502.C502_VOID_FL IS NULL");
    sbQuery.append(" GROUP BY t501.c501_order_id, t501.c501_parent_order_id)");
    sbQuery.append(" GROUP BY ord_id) order_value");
    sbQuery.append(" WHERE t7100.c7100_case_info_id = t501.c7100_case_info_id");
    sbQuery.append(" AND t501.c501_order_id = t501a.c501_order_id ");
    sbQuery.append(" AND t501.c501_order_id = order_value.ord_id ");
    sbQuery.append(" AND t501.c501_parent_order_id is NULL");
    sbQuery.append(" AND t501.c901_order_type IN (2518,2519)");// Rep Draft ,Pending CS Confirmation
    sbQuery.append(" AND c501_delete_fl IS NULL");
    sbQuery.append(" AND c501_void_fl IS NULL");
    sbQuery.append(" AND t7100.c7100_void_fl IS NULL");
    sbQuery.append(" AND t7100.c901_type = 1006503 ");
    sbQuery.append(" AND V700.REP_ID = T501.C703_SALES_REP_ID ");

    log.debug(sbQuery.toString());
    rdResult = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    alReturn = (ArrayList) rdResult.getRows();

    return alReturn;
  }


  /**
   * fetchOrderAttribute - To get the order attribute values or the input attrib type
   * 
   * @param hmParam
   * @return String
   * @throws AppError, Exception
   * @author Elango
   */
  public String fetchOrderAttribute(HashMap hmParam) throws AppError {
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strAttribType = GmCommonClass.parseNull((String) hmParam.get("ATTRIBTYPE"));
    String strAttribValue = "";
    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("get_order_attribute_value", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strOrderId);
    gmDBManager.setString(3, strAttribType);
    gmDBManager.execute();

    strAttribValue = GmCommonClass.parseNull(gmDBManager.getString(1));

    gmDBManager.close();
    return strAttribValue;
  }

  /**
   * validateLotNum - To validate the lot number based on the part string
   * 
   * @param hmParam
   * @return String
   * @throws AppError
   */
  public String validateLotNum(HashMap hmParam) throws AppError {
    String strErrMsg = new String();
    String strPnumStr = GmCommonClass.parseNull((String) hmParam.get("PNUMSTR"));
    String strCnum = GmCommonClass.parseNull((String) hmParam.get("CNUM"));
    String strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    if (!strPnumStr.equals("")) {
      // Call below method if the part number string is available. This one will be getting called
      // while doing submit
      strErrMsg = validateLotNumOnSubmit(hmParam);
    } else if (!strCnum.equals("") && !strPnum.equals("")) {
      // This method is getting called to validate each row
      strErrMsg = validatePartNumLot(hmParam);
    }
    return strErrMsg;
  }

  /**
   * validateLotNum - To validate the lot number while submitting the screen
   * 
   * @param hmParam
   * @return String
   * @throws AppError
   */
  public String validateLotNumOnSubmit(HashMap hmParam) throws AppError {
    String strErrMsg = new String();
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lot_track.gm_validate_input_data", 9);
    gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("REFTYPE")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("DOID")));
    gmDBManager.setString(3, "93343");// pick
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("PNUM")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("CNUM")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("QTY")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("PNUMSTR")));
    gmDBManager.execute();

    strErrMsg = GmCommonClass.parseNull(gmDBManager.getString(9));
    gmDBManager.close();
    return strErrMsg;
  }

  /**
   * validateLotNum - To validate the lot number on ajax call
   * 
   * @param hmParam
   * @return String
   * @throws AppError
   */
  public String validatePartNumLot(HashMap hmParam) throws AppError {
    String strErrMsg = new String();
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_lot_track.gm_validate_input_data", 8);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("REFTYPE")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("DOID")));
    gmDBManager.setString(3, "93343");// pick
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("PNUM")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("CNUM")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("QTY")));
    gmDBManager.execute();

    strErrMsg = GmCommonClass.parseNull(gmDBManager.getString(8));
    gmDBManager.close();
    return strErrMsg;
  }

  /**
   * fetchAckCartDetails - This method will fetch the part number details of Acknowledgment order to
   * the cart
   * 
   * @param String.
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchAckCartDetails(String strOrdId) throws AppError {
    String strCompanyId = GmCommonClass.parseNull(getCompId());
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager(getGmDataStoreVO());
    sbQuery.setLength(0);

    sbQuery
        .append("SELECT ID, GET_PARTDESC_BY_COMPANY(id) PDESC, ORDERID, QTY, PRICE, UNITPRICEADJ, UNITPRICEADJCODEID,");
    sbQuery.append(" ((qty-GET_ACK_RELEASED_QTY(id,'" + strOrdId
        + "',price,unitpriceadj,unitpriceadjcodeid)) * price) TOTAL_AMT , ");
    sbQuery.append(" GET_ACK_RELEASED_QTY(id,'" + strOrdId
        + "',price,unitpriceadj,unitpriceadjcodeid) SHIPQTY, CONSFL, BASE_PRICE,");
    sbQuery
        .append(" NVL(GET_QTY_IN_STOCK(ID),0) SHELFQTY, NVL(GM_PKG_OP_INV_WAREHOUSE.GET_RESERVED_QTY('"
            + strOrdId + "',id),0) RESERVQTY, ");
    sbQuery
        .append(" GET_RULE_VALUE('PARTRESERVE','RESERVEMESSAGE') RULEMESSAGE, GM_PKG_OP_ACK_ORDER.GET_RESERVED_QTY(orderid,id) RESVQTY, ");
    sbQuery.append(" qty - GET_ACK_RELEASED_QTY(id,'" + strOrdId
        + "',price,unitpriceadj,unitpriceadjcodeid) PENDQTY ");
    sbQuery.append(" FROM ( ");
    sbQuery
        .append(" SELECT c205_part_number_id ID, t501.c501_order_id ORDERID, SUM(c502_item_qty) QTY, c502_item_price PRICE, ");
    sbQuery
        .append(" c502_base_price BASE_PRICE, c502_construct_fl CONSFL, NVL(t502.c502_unit_price_adj_value,'0') UNITPRICEADJ, ");
    sbQuery.append(" t502.c901_unit_price_adj_code UNITPRICEADJCODEID ");
    sbQuery.append(" FROM t502_item_order T502, t501_order t501  ");
    sbQuery.append(" WHERE t501.C501_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery
        .append("' AND C502_DELETE_FL IS NULL AND T502.C502_VOID_FL IS NULL AND T501.C501_VOID_FL IS NULL AND T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbQuery
        .append(" GROUP BY c205_part_number_id, t501.c501_order_id, c502_item_price, c502_base_price, c502_construct_fl, t502.c502_unit_price_adj_value, t502.c901_unit_price_adj_code ");
    sbQuery.append(" ORDER BY T501.C501_ORDER_ID, T502.C205_PART_NUMBER_ID )");

    log.debug(" Query for ACK CARTDETAILS " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }

  /**
   * validatePartNumPlant - This method is to validate the plant selected based on the part number
   * 
   * @param HashMap.
   * @return String
   * @exception AppError
   */
  public String validatePartNumPlant(HashMap hmParam) throws AppError {
    String strErrMsg = new String();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_order_rpt.gm_validate_pnum_plant", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("PNUM")));
    gmDBManager.execute();

    strErrMsg = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strErrMsg;
  }

  /**
   * fetchOrderAttributeWithCodeNmInfo - This fetch method is revenue tracking details should
   * display on the DO summary screen
   * 
   * @param String.
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchOrderAttributeWithCodeNmInfo(String strOrderID, String strCodeGroup)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("GM_PKG_AC_ORDER.gm_fch_order_attrib_code_info", 3);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strCodeGroup);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // header
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchDeviceTokens - this method is to fetch the device tokens of the sales rep and associated
   * sales rep as a string
   * 
   * @param String
   * @return String
   * @exception AppError
   */
  public String fetchDeviceTokens(String strRefId) throws AppError {

    String strTokenString = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_it_user_token.gm_fch_device_token_str", 2);
    gmDBManager.setString(1, strRefId);
    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
    gmDBManager.execute();
    strTokenString = GmCommonClass.parseNull((gmDBManager.getString(2)));
    gmDBManager.close();
    return strTokenString;

  }

  /**
   * validateBilledLot - this method is to fetch the error message for the lot is already billed
   * 
   * @param String strPartNum , strCntrlNum
   * @return String
   * @exception AppError
   */
  public String validateBilledLot(String strPartNum, String strCntrlNum) throws AppError {
    HashMap hmReturn = null;

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_billed_lot", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strCntrlNum);
    gmDBManager.execute();
    String strErrorMessage = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    return strErrorMessage;

  }

  /**
   * getAllOrderIDsQuery - This method used to form the order id string for DO summary Order Details
   * section - Created the method for PMT-28394 (Rebate Management)
   * 
   * @param strOrdId
   * @return String
   */

  public String getAllOrderIDsQuery(String strOrdId) throws AppError {
    StringBuffer sbOrderIdQuery = new StringBuffer();
    sbOrderIdQuery.append("SELECT C501_ORDER_ID SBID FROM T501_ORDER ");
    sbOrderIdQuery.append("WHERE C501_ORDER_ID IN ( ");
    sbOrderIdQuery.append(getAllOrderIds(strOrdId));
    sbOrderIdQuery.append(" ) ");
    return sbOrderIdQuery.toString();
  }


  /**
   * getAllOrderIds - This method used to fetch all the orders (Child/Grand child orders)
   * 
   * @param strOrdId
   * @return String
   */

  public String getAllOrderIds(String strOrdId) throws AppError {
    
	String strAllOrderIds = getAllOrderIds(strOrdId,"");
    return strAllOrderIds;
  }
  
  /**
   * getAllOrderIds - This method used to fetch all the orders (Child/Grand child orders)
   *                  and exclude the mentioned order type from input parameter
   * Added for PMT-51170 DO Summary print - Hide discount Orders for Sales Rep
   * @param strOrdId,strExclOrdType
   * @return String
   */

  public String getAllOrderIds(String strOrdId,String strExclOrdType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strAllOrderIds;
    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_fch_all_child_orders", 3);
    gmDBManager.setString(1, strOrdId);
    gmDBManager.setString(2, strExclOrdType);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strAllOrderIds = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    log.debug(" all the order IDs " + strAllOrderIds);

    return strAllOrderIds;
  }
  

  /**
   * fetchDOTagList: Fetch the set id and set name details associated to the tag id
   * 
   * @param Hashmap
   * @throws AppError the app error
   */
  public HashMap fetchDOTagList(HashMap hmparam) throws AppError {
	  	HashMap hmReturn = new HashMap();
	  	String strTagId = GmCommonClass.parseNull((String) hmparam.get("TAGID"));
        GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_op_order_rpt.gm_fch_DO_tag_list", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setString(1, strTagId);
        gmDBManager.execute();
        hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
        gmDBManager.close();
    return hmReturn;
  }
  
}
