package com.globus.custservice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmHQDOBean extends GmBean implements GmCartDetailsInterface {

  public GmHQDOBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  public GmHQDOBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  /**
   * fetchFranceCartDetails - This method will fetch the part number details to the cart
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  @Override
  public ArrayList fetchCartDetails(HashMap hmParams) throws AppError {

    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParams.get("gmDataStoreVO");
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(gmDataStoreVO);
    String strOrdId = GmCommonClass.parseNull((String) hmParams.get("ORDERID"));
    String strAction = GmCommonClass.parseNull((String) hmParams.get("ACTIONTYPE"));
    String strProjectType = GmCommonClass.parseNull((String) hmParams.get("PROJECTTYPE"));
    GmResourceBundleBean gmResourceBundleBean =
        (GmResourceBundleBean) hmParams.get("gmResourceBundleBean");
    String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
    GmResourceBundleBean gmResourceBundleBeanPaperwork =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strSortFlag =
        GmCommonClass.parseNull(gmResourceBundleBeanPaperwork.getProperty("SORT_BY_LOT_FL"));
    String strDelNote = GmCommonClass.parseNull(GmCommonClass.getString("CREATE_DELNOT"));
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager dbManager = new GmDBManager((GmDataStoreVO) hmParams.get("gmDataStoreVO"));
    sbQuery.setLength(0);

    sbQuery
        .append(" SELECT  t501.C501_ORDER_ID ORDERID     , T502.C205_PART_NUMBER_ID ID , get_rule_value_by_company ('DOMAINDB', 'DOMAINPACKSLIP', 1000) RULEDOMAIN ");
    sbQuery
        .append("   , C502_ITEM_ORDER_ID IORDID , get_partdesc_by_company (T502.C205_PART_NUMBER_ID) PDESC, '' NTFTCODE ,0 VAT, C502_ITEM_QTY QTY , C502_CONTROL_NUMBER CNUM ,  t2550.c2550_expiry_date EXPDT, C502_ITEM_PRICE PRICE ");
    sbQuery
        .append(" ,decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T502.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,T502.C502_CONTROL_NUMBER),'','10' || T502.C502_CONTROL_NUMBER,'17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,C502_CONTROL_NUMBER) || '10' || T502.C502_CONTROL_NUMBER),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,T502.C502_CONTROL_NUMBER),'','01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T502.C205_PART_NUMBER_ID)  || '10' || T502.C502_CONTROL_NUMBER,'01' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T502.C205_PART_NUMBER_ID) || '17' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,C502_CONTROL_NUMBER) || '10' || T502.C502_CONTROL_NUMBER)) BARCODEEXP");
    sbQuery
        .append(" ,decode(GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T502.C205_PART_NUMBER_ID),'',decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,T502.C502_CONTROL_NUMBER),'','(10)' || T502.C502_CONTROL_NUMBER,'(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,C502_CONTROL_NUMBER) || '(10)' || T502.C502_CONTROL_NUMBER),decode(GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,T502.C502_CONTROL_NUMBER),'','(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T502.C205_PART_NUMBER_ID)  || '(10)' || T502.C502_CONTROL_NUMBER,'(01)' || GM_PKG_PD_RPT_UDI.GET_PART_DI_NUMBER(T502.C205_PART_NUMBER_ID) || '(17)' || GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,C502_CONTROL_NUMBER) || '(10)' || T502.C502_CONTROL_NUMBER)) BARCODEEXPVAL");
    sbQuery
        .append(" , GM_PKG_OP_UDI_LABEL.GET_EXPIRY_DATE(T502.C205_PART_NUMBER_ID,T502.C502_CONTROL_NUMBER) EXPDATE");
    sbQuery
        .append("   , C502_ITEM_ORDER_ID ITEMID  , C502_BASE_PRICE BASE_PRICE , (C502_ITEM_QTY * C502_ITEM_PRICE) TOTAL_VALUE, GET_COUNT_PARTID (t501.C501_ORDER_ID, T502.C205_PART_NUMBER_ID) PARTQTY ");
    sbQuery
        .append("   , C901_TYPE ITEMTYPE, GET_CODE_NAME_ALT ( C901_TYPE) ITEMTYPEDESC , GET_CODE_NAME (C901_TYPE) ITEMTYPEFULLDESC , c502_construct_fl CONSFL, get_released_qty (T502.C205_PART_NUMBER_ID, '"
            + strOrdId + "', c502_item_price) shipqty ");
    sbQuery
        .append("   , NVL (get_qty_in_stock (T502.C205_PART_NUMBER_ID), 0) shelfqty  , C502_ITEM_QTY - get_released_qty (T502.C205_PART_NUMBER_ID, '"
            + strOrdId + "', c502_item_price) pendqty ");
    sbQuery.append("   , ( (C502_ITEM_QTY       - get_released_qty (T502.C205_PART_NUMBER_ID, '"
        + strOrdId + "', c502_item_price))  * C502_ITEM_PRICE) TOTAL_AMT ");
    sbQuery
        .append("   , GET_CS_PENDSHIP_QTY (t501.C501_ORDER_ID, T502.C205_PART_NUMBER_ID, C502_ITEM_ORDER_ID) PENDSHIPQTY  , c502_ref_id refid ");
    sbQuery
        .append("   , NVL (get_account_part_pricing (t501.c704_account_id, t502.c205_part_number_id, get_account_gpo_id (t501.c704_account_id) ), 0) CURR_PRICE ");
    sbQuery
        .append("   , get_part_attr_value_by_comp (T502.C205_PART_NUMBER_ID, 200001, t501.c1900_company_id) PCODE ");
    sbQuery
        .append("   , NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t502.c205_part_number_id, '"
            + strOrdId
            + "', DECODE ( t501.c501_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location ( t502.c205_part_number_id, '')) SUGLOCATION ");
    sbQuery
        .append("   , gm_pkg_op_item_control_rpt.get_location_cd (t502.c205_part_number_id, '"
            + strOrdId
            + "', DECODE (t501.c501_status_fl, '2', 'pick', '3', 'put', 'pick'), '', '', t502.c502_control_number) SUGLOCATIONCD ");
    sbQuery
        .append("   , NVL (gm_pkg_op_inv_warehouse.GET_RESERVED_QTY ('"
            + strOrdId
            + "', t502.c205_part_number_id), 0)  RESERVQTY , get_rule_value ('PARTRESERVE', 'RESERVEMESSAGE') RULEMESSAGE ");
    sbQuery
        .append("   , get_part_size (t502.c205_part_number_id, t502.c502_control_number) partsize , gm_pkg_op_ack_order.get_reserved_qty (t501.C501_ORDER_ID, t502.c205_part_number_id) RESVQTY ");
    sbQuery
        .append("   , DECODE (t502.c502_unit_price_adj_value, NULL, '', '0', '', 'Y') UADJFL , NVL ( t502.C502_UNIT_PRICE, '0') unitprice , NVL (t502.c502_unit_price_adj_value, '0') UnitpriceAdj ");
    sbQuery
        .append("   , t502.c901_unit_price_adj_code UnitpriceAdjCodeID , DECODE (t502.c901_unit_price_adj_code, NULL, 'N/A', GET_CODE_NAME (t502.c901_unit_price_adj_code)) unitpriceAdjCode ");
    sbQuery
        .append("   , t502.c502_ITEM_PRICE netunitprice, NVL (t502.c502_item_qty *  t502.C502_UNIT_PRICE, 0) beforePrice , NVL (t502.c502_item_qty *  t502.C502_ITEM_PRICE, 0) afterPrice ");
    sbQuery
        .append("   , get_rule_value (t502.C205_PART_NUMBER_ID, 'NONUSAGE') UFREE , DECODE (t501.C901_ORDER_TYPE, NULL, 2521, t501.C901_ORDER_TYPE) ORDTYP ");
    sbQuery
        .append("   , t501.C901_ORDER_TYPE ORDERTYPE , NVL (gm_pkg_sm_adj_rpt.get_account_part_unitprice (t501.c704_account_id, t502.c205_part_number_id, ( ");
    sbQuery
        .append("    SELECT t740.c101_party_id   FROM t740_gpo_account_mapping t740  WHERE t740.c704_account_id = t501.c704_account_id ) ), 0) CURRPORTPRICE , t502.C502_DO_UNIT_PRICE ACCOUNTPRICE , t502.c502_adj_code_value CODEADJUST ");
    sbQuery
        .append("   FROM T502_ITEM_ORDER T502, t501_order t501, t2550_part_control_number t2550 ");
    sbQuery.append("   WHERE t501.C501_ORDER_ID = '" + strOrdId
        + "'   AND C502_DELETE_FL    IS NULL ");
    sbQuery.append("   AND T502.c205_part_number_id = t2550.c205_part_number_id(+) ");
    sbQuery.append("   AND t502.c502_control_number = t2550.c2550_control_number (+) ");
    sbQuery.append("   AND t2550.c2550_expiry_date(+) IS NOT NULL ");
    sbQuery
        .append("   AND t501.c501_void_fl IS NULL   AND t501.C501_ORDER_ID = t502.C501_ORDER_ID ");
    if (strSortFlag.equals("YES")) {
      sbQuery.append(" ORDER BY ORDERID,ID, CNUM");
    } else {
      sbQuery.append("  ORDER BY  ORDERID,ID ");
    }

    log.debug(" Query for FRANCE CARTDETAILS " + sbQuery.toString());
    return dbManager.queryMultipleRecords(sbQuery.toString());
  }
}
