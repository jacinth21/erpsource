package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLotErrorTxnBean extends GmBean {

	
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

	  public GmLotErrorTxnBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmLotErrorTxnBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	  /**
	   * Used to Save Lot Error Detail using New Lot
	   * @param hmParams
	   * @return
	   * @throws AppError
	   */
	  public String saveNewLot(HashMap hmReturn) throws AppError {
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strLotErrorRptJsonString = "";
		    String strLotMessageRptJsonString = "";
		    String updLotNumber = "";
		    String strUserId = "";
		    String strPartNumber = "";
		    String strResolutionStatus = "";
		    String strComments = "";
		    String strErrorInfoID = "";
		    strErrorInfoID = GmCommonClass.parseNull((String) hmReturn.get("ERRORINFOID"));
		    updLotNumber = GmCommonClass.parseNull((String) hmReturn.get("UPDLOTNUM"));
		    strUserId = GmCommonClass.parseNull((String) hmReturn.get("USERID"));
		    strResolutionStatus = GmCommonClass.parseNull((String) hmReturn.get("RESOLUTIONSTS"));
		    strComments = GmCommonClass.parseNull((String) hmReturn.get("COMMENTS"));
		    strPartNumber = GmCommonClass.parseNull((String) hmReturn.get("PARTNUM"));
		    
	        gmDBManager.setPrepareString("gm_pkg_op_lot_error_txn.gm_sav_new_lot", 8);
		    gmDBManager.registerOutParameter(7,OracleTypes.VARCHAR);
		    gmDBManager.registerOutParameter(8,OracleTypes.VARCHAR);
			gmDBManager.setString(1, strErrorInfoID);
			gmDBManager.setString(2, updLotNumber);
			gmDBManager.setString(3, strUserId);
			gmDBManager.setString(4, strResolutionStatus);
			gmDBManager.setString(5, strComments);
			gmDBManager.setString(6, strPartNumber);
			gmDBManager.execute();
			strLotMessageRptJsonString = GmCommonClass.parseNull(gmDBManager.getString(7));
			strLotErrorRptJsonString = GmCommonClass.parseNull(gmDBManager.getString(8));
			gmDBManager.commit();
			return strLotMessageRptJsonString;
		  }
	  
	  /**
	   * Used to Save Lot Error Detail using Same Lot
	   * @param hmParams
	   * @return
	   * @throws AppError
	   */
	  public String saveExistLot(HashMap hmReturn) throws AppError {
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strLotErrorRptJsonString = "";
		    String strLotMessageRptJsonString = "";
		    String strLotNumber = "";
		    String strUserId = "";
		    String strPartNumber = "";
		    String strResolutionStatus = "";
		    String strComments = "";
		    String strErrorInfoID = "";
		    strErrorInfoID = GmCommonClass.parseNull((String) hmReturn.get("ERRORINFOID"));
		    strLotNumber = GmCommonClass.parseNull((String) hmReturn.get("LOTNUM"));
		    strUserId = GmCommonClass.parseNull((String) hmReturn.get("USERID"));
		    strResolutionStatus = GmCommonClass.parseNull((String) hmReturn.get("RESOLUTIONSTS"));
		    strComments = GmCommonClass.parseNull((String) hmReturn.get("COMMENTS"));
		    strPartNumber = GmCommonClass.parseNull((String) hmReturn.get("PARTNUM"));
		    
	        gmDBManager.setPrepareString("gm_pkg_op_lot_error_txn.gm_sav_exist_lot", 8);
		    gmDBManager.registerOutParameter(7,OracleTypes.VARCHAR);
		    gmDBManager.registerOutParameter(8,OracleTypes.VARCHAR);
			gmDBManager.setString(1, strErrorInfoID);
			gmDBManager.setString(2, strLotNumber);
			gmDBManager.setString(3, strUserId);
			gmDBManager.setString(4, strResolutionStatus);
			gmDBManager.setString(5, strComments);
			gmDBManager.setString(6, strPartNumber);
			gmDBManager.execute();
			strLotMessageRptJsonString = GmCommonClass.parseNull(gmDBManager.getString(7));
			strLotErrorRptJsonString = GmCommonClass.parseNull(gmDBManager.getString(8));
			gmDBManager.commit();
			return strLotMessageRptJsonString;
		  }
	  
	  /**
	   * Used to Save Lot Error Detail as Resolved
	   * @param hmParams
	   * @return
	   * @throws AppError
	   */
	  public String saveAsResolved(HashMap hmReturn) throws AppError {
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strLotMessageRptJsonString = "";
		    String strLotNumber = "";
		    String strUserId = "";
		    String strPartNumber = "";
		    String strResolutionStatus = "";
		    String strComments = "";
		    String strErrorInfoID = "";
		    strErrorInfoID = GmCommonClass.parseNull((String) hmReturn.get("ERRORINFOID"));
		    strLotNumber = GmCommonClass.parseNull((String) hmReturn.get("LOTNUM"));
		    strUserId = GmCommonClass.parseNull((String) hmReturn.get("USERID"));
		    strResolutionStatus = GmCommonClass.parseNull((String) hmReturn.get("RESOLUTIONSTS"));
		    strComments = GmCommonClass.parseNull((String) hmReturn.get("COMMENTS"));
		    strPartNumber = GmCommonClass.parseNull((String) hmReturn.get("PARTNUM"));
		    
	        gmDBManager.setPrepareString("gm_pkg_op_lot_error_txn.gm_sav_as_resolved", 7);
	        gmDBManager.registerOutParameter(7,OracleTypes.VARCHAR);
			gmDBManager.setString(1, strErrorInfoID);
			gmDBManager.setString(2, strLotNumber);
			gmDBManager.setString(3, strUserId);
			gmDBManager.setString(4, strResolutionStatus);
			gmDBManager.setString(5, strComments);
			gmDBManager.setString(6, strPartNumber);
			gmDBManager.execute();
			strLotMessageRptJsonString = GmCommonClass.parseNull(gmDBManager.getString(7));
			gmDBManager.commit();
			return strLotMessageRptJsonString;
		  }

}
