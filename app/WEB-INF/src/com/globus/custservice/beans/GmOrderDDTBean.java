package com.globus.custservice.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**PMT-55577 - Record DDT # for Parts used in Surgery
 * 
 * GmOrderDDTBean - This class used to fetch the lot transaction id 
 * 
 * @author tramasamy
 *
 */

public class GmOrderDDTBean extends GmBean{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmOrderDDTBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
	
	}

	/**loadLocationLotTxnList - This method used to fetch the lot transaction details 
	 * based on order id , part number and lot number 
	 * @param hmParam
	 * @return  strJsonString
	 */
	public String loadLocationLotTxnList(HashMap hmParam) {
		
		GmDBManager	 gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		 String strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
		 String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		 String strLotNum = GmCommonClass.parseNull((String) hmParam.get("CONTROLNUM"));
		 String strJsonString = "";
		 
		 gmDBManager.setPrepareString("gm_pkg_cs_location_lot_tracking.gm_fch_lot_loc_list", 4);
		 
			gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
			gmDBManager.setString(1, strOrderID);
			gmDBManager.setString(2, strPartNum);
			gmDBManager.setString(3, strLotNum);
			gmDBManager.execute();
			strJsonString = GmCommonClass.parseNull(gmDBManager.getString(4));
			gmDBManager.close();
			return strJsonString;
	}

}
