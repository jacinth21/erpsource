/**
 * FileName    : GmOrderTagRecordBean.java 
 * Description : The Class is used to fetch / save the record tag details
 * Author      : Vinoth
 * Copyright   : Globus Medical Inc
 */
package com.globus.custservice.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

import com.globus.custservice.forms.GmOrderTagRecordForm;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOrderTagRecordBean extends GmCommonBean {
	
	// Code to Initialize the Logger Class.
	Logger log = GmLogger.getInstance(this.getClass().getName());

	  public GmOrderTagRecordBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }

		  /**
		   * Constructor will populate company info.
		   * 
		   * @param gmDataStore
		   */
		  public GmOrderTagRecordBean(GmDataStoreVO gmDataStore) {
		    super(gmDataStore);
		  }
      /**
       * @description saveOrderTagDetails for addTags
       * @Author: N Raja
       * @param hmData
       * @return
       * @throws AppError
       * 
       */
	public HashMap saveOrderTagDetails(HashMap hmData) throws AppError
	{
		HashMap hmvalue =new HashMap();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strTagID = GmCommonClass.parseNull((String) hmData.get("TAGID"));
	    String strTgId = strTagID + ",";
	    
	    String strSetID = GmCommonClass.parseNull((String) hmData.get("SETID"));
	    String strAccID = GmCommonClass.parseNull((String) hmData.get("ACCOUNTID"));
	    String strREFID = GmCommonClass.parseNull((String) hmData.get("REF_ID"));
	    String strUserId = GmCommonClass.parseNull((String) hmData.get("USERID")); 
	    
	      gmDBManager.setPrepareString("gm_pkg_order_tag_record.gm_save_order_tag_transaction", 5);
	      gmDBManager.registerOutParameter(3, java.sql.Types.VARCHAR);
	      gmDBManager.registerOutParameter(4, java.sql.Types.VARCHAR);
	      gmDBManager.setString(1, strTgId);
	      gmDBManager.setString(2, strSetID);
	      gmDBManager.setString(3, strREFID);
	      gmDBManager.setString(4, strAccID);
	      gmDBManager.setString(5, strUserId);
	      gmDBManager.execute();
	      hmvalue.put("ORDARTAGID", GmCommonClass.parseNull(gmDBManager.getString(3)));
	      hmvalue.put("ACCOUNTID", GmCommonClass.parseNull(gmDBManager.getString(4)));
	    gmDBManager.commit();
	    log.debug("hmValue"+hmvalue);
	    return hmvalue;
	
	}
	/**
	 * @description fetchTagOrders for addTags
	 * @Author: N Raja
	 * @param strTagRefId
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchTagOrders(String strTagRefId) throws AppError {
		
		ArrayList alReturn=new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_order_tag_record.Gm_fetch_tag_orders", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTagRefId);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
	    return alReturn;
	}
	/**
	 * @description cancelTagDetails for addTags
	 * @Author: N Raja
	 * @param hmParam
	 * @throws AppError
	 */
	public void cancelTagDetails(HashMap hmParam) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strTagInfoId = GmCommonClass.parseNull((String) hmParam.get("TAGDOINFOID"));
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

	    gmDBManager.setPrepareString("gm_pkg_order_tag_record.gm_cancel_order_tag_transaction", 2);
	    gmDBManager.setString(1, strTagInfoId);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.execute();
	    gmDBManager.commit();
	  }
	
	/**
	 * @description updateTagDetails for addTags
	 * @param hmParam
	 * @throws AppError
	 */
	public void updateTagDetails(HashMap hmParam) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strRefDoId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

	    gmDBManager.setPrepareString("gm_pkg_order_tag_record.gm_upd_DO_record_tags", 2);
	    gmDBManager.setString(1, strRefDoId);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.execute();
	    gmDBManager.commit();
	  }
	
	/**
	 * @description updateOrderId for addTags
	 * @param hmParam
	 * @throws AppError
	 */
	
	 public void updateOrderId(GmDBManager gmDBManager, String strOrderID, String strUserId, String strTempId)
		      throws AppError {

		 gmDBManager.setPrepareString("gm_pkg_order_tag_record.gm_upd_OrderId_record_tags", 3);
		    gmDBManager.setString(1, strOrderID);
		    gmDBManager.setString(2, strUserId);
		    gmDBManager.setString(3, strTempId);
		    gmDBManager.execute();

		    // Will not have a commit as this method is using connection object from different method.

		  }
	 
	 /**
	   * @description updateTagDetails method to update Y for valid Tags
	   * @param actionMapping, form, request, response
	   * throws Exception
	   */
	  public String getOrderId(HashMap hmValues) throws AppError {
		  
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_order_tag_record.gm_get_order_id", 1);
		    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		    gmDBManager.execute();
		    String strTempId = GmCommonClass.parseNull(gmDBManager.getString(1));
		    log.debug("strTempId"+strTempId);
		    gmDBManager.commit();
		    return strTempId;
		   }
}
