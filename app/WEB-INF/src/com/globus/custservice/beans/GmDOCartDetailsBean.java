package com.globus.custservice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sswetha
 *
 */
public class GmDOCartDetailsBean extends GmBean implements GmCartDetailsInterface{
	 public GmDOCartDetailsBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	 public GmDOCartDetailsBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }

	Logger log = GmLogger.getInstance(this.getClass().getName());
	  Logger logOrder = GmLogger.getInstance("globusapporder");

	  
	/**
	   * fetchCartDetails - This method will fetch the part number details to the cart
	   * 
	   * @param String , String.
	   * @return ArrayList
	   * @exception AppError
	   */
	  @Override
	  public ArrayList fetchCartDetails(HashMap hmParams) throws AppError {
		  
	    String strOrdId = GmCommonClass.parseNull((String) hmParams.get("ORDERID"));
	    String strProjectType = GmCommonClass.parseNull((String) hmParams.get("PROJECTTYPE"));
	    String strAction = GmCommonClass.parseNull((String) hmParams.get("ACTIONTYPE"));
	    String strDelNote = GmCommonClass.parseNull(GmCommonClass.getString("CREATE_DELNOT"));
	    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParams.get("gmDataStoreVO");
	    String strCompanyId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
	    StringBuffer sbQuery = new StringBuffer();
	    GmDBManager dbManager = new GmDBManager(gmDataStoreVO);
	    sbQuery.setLength(0);

	    sbQuery
	        .append(" SELECT t501.C501_ORDER_ID ORDERID,C205_PART_NUMBER_ID ID,C205_PART_NUMBER_ID PNUM, get_rule_value( NVL(t501.c901_order_type,2521), 'DISBLCHPRICINGORD') EXCLUDEORD, t501.C501_STATUS_FL ORDSTATUS, t501.C503_INVOICE_ID INVOICEID, get_rule_value_by_company('"
	            + strCompanyId
	            + "_DOMAINDB','DOMAINPACKSLIP',"
	            + strCompanyId
	            + ") RULEDOMAIN, C502_ITEM_ORDER_ID IORDID, get_partdesc_by_company(C205_PART_NUMBER_ID) PDESC, ");
	    sbQuery
	        .append(" C502_ITEM_QTY QTY, C502_CONTROL_NUMBER CNUM, C502_ITEM_PRICE PRICE, C502_ITEM_ORDER_ID ITEMID ,C502_BASE_PRICE BASE_PRICE ");
	    sbQuery
	        .append(" ,(C502_ITEM_QTY * C502_ITEM_PRICE) TOTAL_VALUE, GET_COUNT_PARTID(t501.C501_ORDER_ID,C205_PART_NUMBER_ID) PARTQTY");
	    sbQuery
	        .append(" ,C901_TYPE ITEMTYPE , GET_CODE_NAME_ALT(C901_TYPE) ITEMTYPEDESC, GET_CODE_NAME(C901_TYPE) ITEMTYPEFULLDESC ");
	    sbQuery.append(" ,c502_construct_fl CONSFL ");
	    if (strDelNote.equalsIgnoreCase("yes")) {
	      sbQuery
	          .append(" ,get_ack_released_qty(C205_PART_NUMBER_ID,'"
	              + strOrdId
	              + "',c502_item_price,c502_unit_price_adj_value,c901_unit_price_adj_code) shipqty, NVL(get_qty_in_stock(C205_PART_NUMBER_ID),0) shelfqty");
	      sbQuery
	          .append(" ,C502_ITEM_QTY-get_ack_released_qty(C205_PART_NUMBER_ID,'"
	              + strOrdId
	              + "',c502_item_price,c502_unit_price_adj_value,c901_unit_price_adj_code) pendqty,((C502_ITEM_QTY-get_ack_released_qty(C205_PART_NUMBER_ID,'"
	              + strOrdId
	              + "',c502_item_price,c502_unit_price_adj_value,c901_unit_price_adj_code)) * C502_ITEM_PRICE) TOTAL_AMT ");
	    }
	    sbQuery
	        .append(" ,GET_CS_PENDSHIP_QTY(t501.C501_ORDER_ID,C205_PART_NUMBER_ID,C502_ITEM_ORDER_ID) PENDSHIPQTY ");
	    sbQuery
	        .append(" ,c502_ref_id refid , NVL(get_account_part_pricing (t501.c704_account_id, t502.c205_part_number_id, get_account_gpo_id (t501.c704_account_id)), 0) CURR_PRICE ");
	    sbQuery
	        .append(" , get_part_attr_value_by_comp (C205_PART_NUMBER_ID,200001, t501.c1900_company_id) PCODE ");
	    sbQuery
	        .append(" , NVL (gm_pkg_op_item_control_rpt.get_initiated_location (t502.c205_part_number_id, '");
	    sbQuery.append(strOrdId);
	    sbQuery
	        .append("' , DECODE (t501.c501_status_fl, '2', 'pick', '3', 'put', 'pick')), gm_pkg_op_inv_scan.get_part_location (t502.c205_part_number_id, '')) SUGLOCATION  ");
	    // Need to get the location cd for Pick slip
	    sbQuery.append(" , gm_pkg_op_item_control_rpt.get_location_cd (t502.c205_part_number_id, '");
	    sbQuery.append(strOrdId);
	    sbQuery
	        .append("', DECODE (t501.c501_status_fl, '2', 'pick', '3', 'put', 'pick'), '','',t502.c502_control_number) SUGLOCATIONCD , NVL(gm_pkg_op_inv_warehouse.GET_RESERVED_QTY('"
	            + strOrdId
	            + "',t502.c205_part_number_id),0) RESERVQTY ,get_rule_value('PARTRESERVE','RESERVEMESSAGE') RULEMESSAGE , get_part_size (t502.c205_part_number_id, t502.c502_control_number ) partsize  ");
	    sbQuery
	        .append(", gm_pkg_op_ack_order.get_reserved_qty(t501.C501_ORDER_ID,t502.c205_part_number_id) RESVQTY ");
	    sbQuery
	        .append(" ,DECODE(t502.c502_unit_price_adj_value ,NULL,'','0','','Y') UADJFL ,NVL(t502.C502_UNIT_PRICE,'0') unitprice , NVL(t502.c502_unit_price_adj_value,'0') UnitpriceAdj");
	    sbQuery
	        .append(" , t502.c901_unit_price_adj_code UnitpriceAdjCodeID,DECODE(t502.c901_unit_price_adj_code,NULL,'N/A',GET_CODE_NAME (t502.c901_unit_price_adj_code)) unitpriceAdjCode, t502.c502_ITEM_PRICE netunitprice ");
	    sbQuery
	        .append(" ,  NVL(t502.c502_item_qty*t502.C502_UNIT_PRICE,0) beforePrice  ,  NVL(t502.c502_item_qty*t502.C502_ITEM_PRICE,0) afterPrice ");
	    sbQuery
	        .append(" ,  get_rule_value (t502.C205_PART_NUMBER_ID, 'NONUSAGE') UFREE , DECODE (t501.C901_ORDER_TYPE, NULL, 2521, t501.C901_ORDER_TYPE) ORDTYP, t501.C901_ORDER_TYPE ORDERTYPE");
	    sbQuery
	        .append(" , NVL(gm_pkg_sm_adj_rpt.get_account_part_unitprice(t501.c704_account_id,t502.c205_part_number_id, (SELECT t740.c101_party_id from t740_gpo_account_mapping t740 where t740.c704_account_id = t501.c704_account_id)),0) CURRPORTPRICE ");
	    sbQuery
	        .append(" , t502.C502_DO_UNIT_PRICE ACCOUNTPRICE, t502.c502_adj_code_value CODEADJUST, NVL(GET_ORDER_ATTRIBUTE_VALUE(t501.c501_order_id,400152),1) exch_rate ");// 400152:
	                                                                                                                                                                        // Exchange
	    sbQuery
	        .append(", GET_LOT_EXPIRY_DATE (t502.c205_part_number_id, t502.C502_CONTROL_NUMBER) PRODEXPIRY");

	    // rate
	    sbQuery.append(" FROM T502_ITEM_ORDER T502, t501_order t501 ");

	    if (!strAction.equals("AdjPopup")) {
	      sbQuery.append(" WHERE (t501.C501_ORDER_ID = '");//PMT-38606
	      sbQuery.append(strOrdId);
	    //PMT-38606 Modify the below condition in this query to fetch the child order if the input order is a parent order
	      sbQuery
          .append("' OR  t501.c501_parent_order_id IN (select c501_order_id from t501_order WHERE c501_order_id = '");
	      sbQuery
          .append(strOrdId);	
	      sbQuery
          .append("' AND c501_parent_order_id IS NULL)");
	      sbQuery
          .append(")");
	    //PMT-38606
	      sbQuery
	          .append(" AND C502_DELETE_FL IS NULL AND t501.c501_void_fl  IS NULL AND t502.c502_void_fl IS NULL AND t501.C501_ORDER_ID = t502.C501_ORDER_ID");
	    } else {
	      sbQuery.append(" WHERE (t501.C501_ORDER_ID = '");
	      sbQuery.append(strOrdId);
	      sbQuery.append("' OR t501.c501_parent_order_id = '");
	      sbQuery.append(strOrdId);
	      sbQuery
	          .append("') AND C502_DELETE_FL IS NULL AND t501.c501_void_fl  IS NULL AND t502.c502_void_fl IS NULL AND t501.C501_ORDER_ID = t502.C501_ORDER_ID");
	    }
	    if (strProjectType != null && !strProjectType.equals("")) {
	      sbQuery.append(" AND C205_PART_NUMBER_ID IN (SELECT T205.C205_PART_NUMBER_ID FROM ");
	      sbQuery.append(" T205_PART_NUMBER T205, T202_PROJECT T202");
	      sbQuery.append(" WHERE T205.C202_PROJECT_ID = T202.C202_PROJECT_ID");
	      // 1500 maps to Globus Project
	      if (strProjectType.equals("1500")) {
	        sbQuery.append(" AND T202.C901_PROJECT_TYPE = ");
	        sbQuery.append(strProjectType);
	      } else {
	        sbQuery.append(" AND T202.C901_PROJECT_TYPE = ");
	        sbQuery.append(strProjectType);
	      }
	      sbQuery.append(" ) ");

	    }
	    sbQuery.append(" ORDER BY t501.C501_ORDER_ID,  T502.C205_PART_NUMBER_ID");

	    log.debug(" Query for Loading CARTDETAILS for Change Pricing is " + sbQuery.toString());
	    return dbManager.queryMultipleRecords(sbQuery.toString());
	  }

}
