/*****************************************************************************
 * File : GmPendingOrdersPOServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.accounts.beans.GmAccountsMiscBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmPendingOrdersPOServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    GmCalenderOperations gmCal = new GmCalenderOperations();
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strOperPath.concat("/GmPendingOrderReport.jsp");
    String strTodaysDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strMonthBeginDate = strTodaysDate.replaceAll("/\\d\\d/", "/01/");
    String strAccessFilter = null;
    String strSalesFilter = null;
    String strCondition = null;
    String strInputStr = "";
    String strType = "";
    String strDefRptType = "";

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = GmCommonClass.parseNull((String) session.getAttribute("hAction"));
      }
      strAction = (strAction == "") ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
        // session.removeAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      log.debug(strAction + "====strAction======strOpt====" + strOpt);

      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
      GmPartyBean gmParty = new GmPartyBean(getGmDataStoreVO());
      GmAccountsMiscBean gmAcct = new GmAccountsMiscBean(getGmDataStoreVO());
      GmCommonClass gmCommonClass = new GmCommonClass();
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      GmCommonBean gmCommBn = new GmCommonBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmSalesFilters = new HashMap();
      HashMap hmCurrency = new HashMap();
      ArrayList alCompanyCurrency = new ArrayList();

      String strUserId = (String) session.getAttribute("strSessUserId");

      strType = request.getParameter("Cbo_Type");
      if (strType == null) {
        // Check cookie
        strType = gmCookieManager.getCookieValue(request, "cAgingType");
      }
      // Recheck for default if not found in cookie
      strType = strType == null ? "103289" : strType; // hard-coding the default Aging type as ALL,
                                                      // when it is Empty.

      String strFromDate = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
      String strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
      String strDType = GmCommonClass.parseZero(request.getParameter("Cbo_DType"));
      String strReportType = GmCommonClass.parseNull(request.getParameter("hReportType"));
      String strOrderType = GmCommonClass.parseZero(request.getParameter("Cbo_OType"));
      String strCollectorId = GmCommonClass.parseZero(request.getParameter("Cbo_CollectorId"));
      String strCompanyID = GmCommonClass.parseZero(request.getParameter("Cbo_CompanyID"));
      String strMasterID = GmCommonClass.parseZero(request.getParameter("Cbo_Master"));
      String strEmailAction = GmCommonClass.parseZero(request.getParameter("Cbo_EmailAction"));
      String strDueDays = GmCommonClass.parseZero(request.getParameter("Txt_DueDate"));
      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      String strARCurrId = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      String strACKcurrId = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr_ack"));
      strARCurrId = strARCurrId.equals("0") ? strCompanyCurrId : strARCurrId;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strARCurrId);

      strDefRptType =
          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("GRP_TYPE",
              "DEFAULT_GRP_TYPE", getGmDataStoreVO().getCmpid()));

      if (strOpt.equals("PROFORMA")) {
        strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strACKcurrId);
      }
      // Get the company id from rules
      String strRuleComp =
          GmCommonClass.parseNull((GmCommonClass.getRuleValueByCompany("PICSLIP", "EXCLTXN",
              getGmDataStoreVO().getCmpid())));
      if (strDueDays.equals("") || strDueDays.equals("0")) {
        strDueDays =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("DEFAULT_DAYS", "COMMSN_EMAIL"));
      }

      if (strReportType.equals("")) {
        // Check cookie
        strReportType =
            GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cReportType"));
      }

      // Recheck for default if not found in cookie
      if (strReportType.equals("") && strOpt.equals("EMAIL")) {
        strReportType = "103284"; // BYREP
      } else if (strReportType.equals("") && !strOpt.equals("EMAIL")
          && strDefRptType.equals("26240245")) {
        strReportType = "26240245"; // BY DEALER
      } else if (strReportType.equals("") && !strOpt.equals("EMAIL")) {
        strReportType = "103283"; // BYACC
      }

      ArrayList alDistTypes = gmCommonClass.getCodeList("DSTYP");
      ArrayList alOrderTypes = gmCommonClass.getCodeList("ORTYP");
      ArrayList alCollector = gmCommonClass.getCodeList("COLLTR");
      ArrayList alPoEmailType = gmCommonClass.getCodeList("POEMAC");
      ArrayList alGrpTypes = gmCommonClass.getCodeList("PGRPTY");
      ArrayList alAgingTyp = gmCommonClass.getCodeList("PAGNTY");
      ArrayList alCompList = gmCommBn.getCompanies("");

      ArrayList alAccountList = GmCommonClass.parseNullArrayList(gmSalesBean.reportAccount());
      ArrayList alRepList = GmCommonClass.parseNullArrayList(gmCust.getRepList());
      ArrayList alDistList = GmCommonClass.parseNullArrayList(gmCust.getDistributorList());
      alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());

      // To get the Access Level information
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommBn.getSalesFilterLists(strAccessFilter);
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
      strCondition = getAccessCondition(request, response, true);
      // PMT-4434 company id added for PO reminder/commission mail, From other reports we will be
      // getting this condition via the "strCondition" variable.
      if (strOpt.equals("EMAIL")) {
        strCondition = "";
      }
      hmParam.put("COND", strCondition);
      hmParam.put("OPT", strOpt);
      hmParam.put("TYPE", strType);
      log.debug("========date values==" + strFromDate + "  " + strToDate);
      hmParam.put("FRMDT", getDateFromStr(strFromDate));
      hmParam.put("TODT", getDateFromStr(strToDate));
      hmParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
      hmParam.put("DTYPE", strDType);
      hmParam.put("REPORTTYPE", strReportType);
      hmParam.put("ORDERTYPE", strOrderType);
      hmParam.put("COLLECTORID", strCollectorId);
      hmParam.put("COMPANYID", strCompanyID);
      hmParam.put("MASTERID", strMasterID);
      hmParam.put("ARCURRID", strARCurrId);
      hmParam.put("ACKCURRSYM", strACKcurrId);
      request.setAttribute("DTYPE", alDistTypes);
      request.setAttribute("OTYPE", alOrderTypes);
      request.setAttribute("ALCOLLECTOR", alCollector);
      request.setAttribute("ALPOEMAILTYPE", alPoEmailType);
      request.setAttribute("ALGRPTYPE", alGrpTypes);
      request.setAttribute("ALAGETYPE", alAgingTyp);
      request.setAttribute("ALCOMPLIST", alCompList);
      request.setAttribute("ALACCLIST", alAccountList);
      request.setAttribute("ALREPLIST", alRepList);
      request.setAttribute("ALDISTLIST", alDistList);
      request.setAttribute("USERID", strUserId);
      request.setAttribute("FROMDATE", strFromDate);
      request.setAttribute("TODATE", strToDate);
      request.setAttribute("DEFAULTDUEDAYS", strDueDays);
      request.setAttribute("EXCLCOMPNY", strRuleComp);

      gmFramework.getRequestParams(request, response, session, hmParam);
      // -- added below code for BUG-8755 , temporary code fix --
      // -- order price are showing in corporate currency value for secondary company users
      // -- For temporary fix, added default currency type as item currency type(105461)
      hmParam.put("CURRTYPE", "105461");

      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      request.setAttribute("ARCURRID", strARCurrId);
      request.setAttribute("ACKCURRSYM", strACKcurrId);
      request.setAttribute("ARCURRSYMBOL", strAccCurrName);
      if ((strAction.equals("Load") && !strOpt.equals("EMAIL")) || strAction.equals("Reload")) {
        hmReturn = gmCust.reportPendingOrders(hmParam);
      } else if (strAction.equals("SendMail")) {
        strInputStr =
            request.getParameter("hInputStr") == null ? "" : request.getParameter("hInputStr");
        strInputStr = strInputStr.replaceAll("\\^", "','");
        strInputStr = "'".concat(strInputStr).concat("'");
        // Passing strAccCurrName to show currency symbol based on account in a Email.
        hmReturn = gmAcct.sendEmailReminderPO(strInputStr, strUserId, "", strAccCurrName);
        hmReturn = gmCust.reportPendingOrders(hmParam);
        strAction = "Reload";
      } else if (strAction.equals("commsn_email")) {
        populateCommissionEmail(request, response);
        strDispatchTo = strOperPath.concat("/GmCommissionEmail.jsp");
      } else if (strAction.equals("send_commsn_email")) {
        processCommissionEmail(request, response);
      }

      request.setAttribute("strAction", strAction);
      request.setAttribute("hmParam", hmParam);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hmSalesFilters", hmSalesFilters);

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cAgingType", strType);
      gmCookieManager.setCookieValue(response, "cReportType", strReportType);

      log.debug("========strDispatchTo==" + strDispatchTo);
      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  /**
   * populateCommissionEmail - This method will get all the required information that is needed to
   * be populated in the commission email composition page.
   * 
   * @param request
   * @param response
   * @throws AppError
   * @throws Exception
   */
  private void populateCommissionEmail(HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {
    GmAccountsMiscBean gmAcctMiscBean = new GmAccountsMiscBean(getGmDataStoreVO());
    HashMap hmCommissionParam = new HashMap();
    String strUserId = GmCommonClass.parseNull((String) request.getAttribute("USERID"));
    String strInputStr =
        request.getParameter("hInputStr") == null ? "" : GmCommonClass.parseNull(request
            .getParameter("hInputStr"));
    // Currency symbol based on account
    String strCurrency = GmCommonClass.parseNull((String) request.getAttribute("ARCURRSYMBOL"));
    String strEmailAction = GmCommonClass.parseZero(request.getParameter("Cbo_EmailAction"));
    // String strDueDate = GmCommonClass.parseNull((String)request.getParameter("hduedays"));
    String strDueDate = GmCommonClass.parseNull(request.getParameter("Txt_DueDate"));

    // As we are having front end validation to compare the htodate to txt_todate we will always
    // have the right date in txt_todate ,so we can use that always.
    String strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));

    // change the input string 1410-140806-2^118-140806-1^632-140806-1 into
    // '1410-140806-2','118-140806-1','632-140806-1' to use it in bean query
    strInputStr = strInputStr.replaceAll("\\^", "','");
    String strInputString = "'".concat(strInputStr).concat("'");
    // To get the Orders to be sent for Commission Reminder Email.
    String strReportType = GmCommonClass.parseNull(request.getParameter("hReportType"));
    HashMap hmOrderDetails =
        GmCommonClass.parseNullHashMap(gmAcctMiscBean.listPOReminderOrder(strInputString,
            strUserId, "CommssionEmail"));
    // change the input string 1410-140806-2^118-140806-1^632-140806-1 into
    // 1410-140806-2,118-140806-1,632-140806-1 to use it in CLOB List
    strInputStr = strInputStr.replaceAll("','", ",");
    // To get the email id of the collector
    String strCollectorIDList =
        GmCommonClass.parseNull(gmAcctMiscBean.fetchCollectorEmail(strInputStr));
    hmCommissionParam.put("INPUTSTR", strInputStr);
    hmCommissionParam.put("EMAILACTION", strEmailAction);
    hmCommissionParam.put("TODATE", strToDate);
    hmCommissionParam.put("REPORTTYPE", strReportType);
    hmCommissionParam.put("DUEDATE", strDueDate);
    // To get all the required information that is needed to be populated in the commission email
    // composition page.
    HashMap hmEmailDetails =
        GmCommonClass.parseNullHashMap(gmAcctMiscBean.fetchCommissionEmailInfo(hmCommissionParam));

    String strOrderTotal = GmCommonClass.parseNull((String) hmOrderDetails.get("ORDER_TOTAL"));
    // Get the Currency Symbol or Currency Format based on the company.
    HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    String strCurrSign = (String) hmCurrency.get("CMPCURRSMB");
    String strApplCurrFmt = (String) hmCurrency.get("CMPCURRFMT");

    strOrderTotal = GmCommonClass.getStringWithCommas(strOrderTotal, strApplCurrFmt);

    String strMessage = GmCommonClass.parseNull((String) hmEmailDetails.get("EMAILMESSAGE"));
    strMessage = strMessage.replace("#@#TOTAL", strOrderTotal);
    strMessage = strMessage.replace("#@#CURR", strCurrency);
    hmEmailDetails.put("EMAILMESSAGE", strMessage);

    String strCcIds = GmCommonClass.parseNull((String) hmEmailDetails.get("EMAILCC"));
    hmEmailDetails.put("EMAILCC", strCcIds + "," + strCollectorIDList);
    // Order details
    ArrayList alPOList = GmCommonClass.parseNullArrayList((ArrayList) hmOrderDetails.get("alData"));

    request.setAttribute("EMAILDETAILS", hmEmailDetails);
    request.setAttribute("EMAILPOLIST", alPOList);
    request.setAttribute("hInputOrdStr", strInputStr);
    request.setAttribute("hEmailAction", strEmailAction);
    request.setAttribute("APPLCURRSYM", strCurrSign);
    request.setAttribute("APPLCURRFMT", strApplCurrFmt);
  }// End of populateCommissionEmail method

  /**
   * processCommissionEmail - This will send the email to sales rep's and puts the order log.
   * 
   * @param request
   * @param response
   * @throws AppError
   * @throws Exception
   */
  private void processCommissionEmail(HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {
    GmAccountsMiscBean gmAcctMiscBean = new GmAccountsMiscBean(getGmDataStoreVO());

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    String strEmailAction = GmCommonClass.parseZero(request.getParameter("hEmailAction"));
    String strUserId = GmCommonClass.parseNull((String) request.getAttribute("USERID"));
    String TEMPLATE_NAME = "GmPOCommissioningEmail";

    String strInputStr = GmCommonClass.parseNull(request.getParameter("hInputOrdStr"));
    ArrayList alPOList =
        GmCommonClass.parseNullArrayList(GmCommonClass.getListFromString(strInputStr, ","));
    String sbToEmailId = GmCommonClass.parseNull(request.getParameter("emailTo"));
    String sbCcEmailId = GmCommonClass.parseNull(request.getParameter("emailCC"));
    String strSubject = GmCommonClass.parseNull(request.getParameter("emailSubject"));
    String strEmailMessage = GmCommonClass.parseNull(request.getParameter("emailMessage"));
    String strOrderDetails = GmCommonClass.parseNull(request.getParameter("hHtmlOrderDetails"));

    if (strEmailAction.equals("4000627")) { // 4000627 - Held Order Commission E-mail
      TEMPLATE_NAME = "GmHeldOrdersCommissioningEmail";
    }

    String strMimeType =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
            + GmEmailProperties.MIME_TYPE));
    String strFrom =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
            + GmEmailProperties.FROM));

    GmEmailProperties emailProps = new GmEmailProperties();
    GmCommonBean gmCommonBean = new GmCommonBean();
    emailProps.setMimeType(strMimeType);
    emailProps.setSender(strFrom);
    emailProps.setRecipients(sbToEmailId.toString());
    emailProps.setCc(sbCcEmailId.toString());
    emailProps.setSubject(strSubject);

    HashMap hmMessageParam = new HashMap();
    hmMessageParam.put("Message", strEmailMessage);
    hmMessageParam.put("Details", strOrderDetails);

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setTemplateName("GmCommissionEmail.vm");
    templateUtil.setDataMap("hmEmailParam", hmMessageParam);
    String strEmailContent = templateUtil.generateOutput();

    HashMap hmEmailProperty = new HashMap();
    hmEmailProperty.put("strFrom", emailProps.getSender());
    hmEmailProperty.put("strTo", emailProps.getRecipients());
    hmEmailProperty.put("strCc", emailProps.getCc());
    hmEmailProperty.put("strSubject", emailProps.getSubject());
    hmEmailProperty.put("strMessageDesc", strEmailContent);
    hmEmailProperty.put("strMimeType", emailProps.getMimeType());

    String strLogMessage =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strEmailAction, "COMMSN_LOG_MSG"));
    log.debug("::" + strLogMessage + ":::hmEmailProperty:" + hmEmailProperty);
    GmCommonClass.sendMail(hmEmailProperty);

    // Saving an entry in the Log table.
    String strOrderId = "";
    int intArLength = alPOList.size();
    HashMap hmLoop = new HashMap();
    for (int i = 0; i < intArLength; i++) {
      strOrderId = GmCommonClass.parseNull((String) alPOList.get(i));
      gmCommonBean.saveLog(strOrderId, strLogMessage, strUserId, "1200");
    }

    String strhAgingType = GmCommonClass.parseNull(request.getParameter("hAgingType"));
    String strhDistType = GmCommonClass.parseNull(request.getParameter("hDistType"));
    String strhGrpType = GmCommonClass.parseNull(request.getParameter("hReportType"));
    String strhMasterId = GmCommonClass.parseNull(request.getParameter("hMasterId"));
    String strhCollecId = GmCommonClass.parseNull(request.getParameter("hCollecId"));
    String strhOdrType = GmCommonClass.parseNull(request.getParameter("hOdrType"));
    String strhCompID = GmCommonClass.parseNull(request.getParameter("hCompID"));
    String strhFrmDt = GmCommonClass.parseNull(request.getParameter("hFromDate"));
    String strhToDt = GmCommonClass.parseNull(request.getParameter("hToDate"));
    String strDueDays = GmCommonClass.parseNull(request.getParameter("hduedays"));
    String strAccountCurrency = GmCommonClass.parseNull(request.getParameter("hAccCurrency"));

    // Forwarding to PO email page.
    String strAdress =
        "/GmPendingOrdersPOServlet?hOpt=EMAIL&Cbo_Type=" + strhAgingType + "&Txt_FromDate="
            + strhFrmDt + "&Txt_ToDate=" + strhToDt + "&Cbo_DType=" + strhDistType
            + "&Cbo_ReportType=" + strhGrpType + "&Cbo_Master=" + strhMasterId
            + "&Cbo_CollectorId=" + strhCollecId + "&Cbo_OType=" + strhOdrType + "&Cbo_CompanyID="
            + strhCompID + "&Txt_DueDate=" + strDueDays + "&Cbo_Comp_Curr=" + strAccountCurrency;
    gotoPage(strAdress, request, response);
  }// End of processCommissionEmail method
}// End of GmPendingOrdersPOServlet
