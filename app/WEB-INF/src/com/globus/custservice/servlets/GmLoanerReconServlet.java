/*****************************************************************************
 * File : GmLoanerReconServlet.java Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.beans.GmLoanerReconBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;

public class GmLoanerReconServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmLoanerRecon.jsp");


    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strDateFormat = getGmDataStoreVO().getCmpdfmt();
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmLogisticsBean gmLogis = new GmLogisticsBean(getGmDataStoreVO());
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmInHouseSetsReportBean gmTrans = new GmInHouseSetsReportBean(getGmDataStoreVO());
      GmInHouseSetsTransBean gmInHouseTransBean = new GmInHouseSetsTransBean(getGmDataStoreVO());
      GmLoanerReconBean gmLoanerReconBean = new GmLoanerReconBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmCalenderOperations gmCal = new GmCalenderOperations();
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmAccess = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alRepList = new ArrayList();
      ArrayList alDistList = new ArrayList();
      ArrayList alAccList = new ArrayList();
      ArrayList alStatus = new ArrayList();
      ArrayList alResult = new ArrayList();
      ArrayList alLogReasons = new ArrayList();
      ArrayList alDateType =new ArrayList();
      String strXmlData = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strTransId = "";
      strTransId = request.getParameter("hId") == null ? "" : request.getParameter("hId");
      String strInputStr = "";
      String strRecDO = "";
      HashMap hmParam = new HashMap();
      HashMap hmFilter = new HashMap();
      String strMsg = request.getParameter("strMsg") == null ? "" : request.getParameter("strMsg");;
      String strReconXmlData = "";
      String strReconTransXmlData = "";
      String strCboAction = "";
      String strAccessFl = "";
      String strhTranId = "";
      String strLoanDt = "";
      String strType = "";
      String strTransType = "";
      String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      String strCompanyID = "";
      String strCompPlantFilter = "";

      String strSessTodaysDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());

      strType = GmCommonClass.parseNull(request.getParameter("hTxnType"));
      String strStartDate = GmCommonClass.parseNull(request.getParameter("startDate"));
      String strEndDate = GmCommonClass.parseNull(request.getParameter("endDate"));
      strStartDate = strStartDate.equals("") ? strSessTodaysDate : strStartDate;
      strEndDate = strEndDate.equals("") ? strSessTodaysDate : strEndDate;

      hmAccess = gmAccessControlBean.getAccessPermissions(strUserId, "LR_ACCESS");
      strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("ACCFL", strAccessFl);
      request.setAttribute("FROMDATE", strStartDate);
      request.setAttribute("TODATE", strEndDate);

      // alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RETNRS"));
      // request.setAttribute("alReasons",alReturn);

      // after save the report reload, we fetch the saved data.So, move this code to down.
      if (strAction.equals("SaveRecon")) {
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        hmParam.put("SOLDSTR", strInputStr);
        strRecDO = GmCommonClass.parseNull(request.getParameter("hRecDoStr"));
        hmParam.put("STRRECDO", strRecDO);
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputConStr"));
        hmParam.put("CONSTR", strInputStr);
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputWriteStr"));
        hmParam.put("WRITESTR", strInputStr);
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputRetStr"));
        hmParam.put("RETSTR", strInputStr);
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputihStr"));
        hmParam.put("IHSTR", strInputStr);
        hmParam.put("TYPEID", strType);
        if (strType.equals("9112") || strType.equals("9115") || strType.equals("1006571")) {
          strMsg = gmInHouseTransBean.saveInHouseReconTrans(strTransId, hmParam, strUserId);
        } else {
          strMsg = gmInHouseTransBean.saveLoanerReconTrans(strTransId, hmParam, strUserId);
        }
        log.debug("strMsg " + strMsg);
        request.setAttribute("strMsg", strMsg);
        request.setAttribute("hAction", "LoanRecon");
        strAction = "LoanRecon";
        request.setAttribute("hTransId", strTransId);
        String strLogReason = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strTransId, strLogReason, strUserId, "1246");
        }
        strDispatchTo =
            "/GmLoanerReconServlet?hAction=" + strAction + "&hId=" + strTransId + "&strMsg="
                + strMsg + "&companyInfo=" + URLEncoder.encode(strCompanyInfo);
        response.sendRedirect(strDispatchTo);
      }
      log.debug("strAction ==>>>>> " + strAction);
      if (strAction.equals("LoanRecon") || strAction.equals("LoadUnRecon")) {
        alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LRSTA"));
        strCboAction = GmCommonClass.parseNull(request.getParameter("Cbo_action"));
        hmResult = gmTrans.viewInHouseTransDetails(strTransId);
        strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
        strhTranId = GmCommonClass.parseNull((String) hmResult.get("TRANSID"));
        strLoanDt =  GmCommonClass.getStringFromDate((java.sql.Date) hmResult.get("LOANDT"),strDateFormat);
        request.setAttribute("LOANDT", strLoanDt);
        hmReturn.put("CONDETAILS", hmResult);
        alDateType = GmCommonClass.getCodeList("LNDT");
        if (strTransType.equals("9112") || strTransType.equals("1006571")) {
          strType = "4119";
        } else {
          strType = "4127";
        }
        // hmResult = gmLogis.viewInHouseTransItemsDetails(strTransId, strType);
        /*
         * *viewLoanerTransItemsDetails(): New method to load the Reconciliation screen details
         * [while clicking "Y" icon in loaner Reconciliation]Load the part details section based on
         * the plant
         */
        hmResult = gmLogis.viewLoanerTransItemsDetails(strTransId, strType);
        String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
        hmResult.put("SESSDATEFMT", strApplDateFmt);
        alResult = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("SETLOAD"));
        hmResult.put("CTYPE", strTransType);
        if (strTransType.equals("9112") || strTransType.equals("9115")
            || strTransType.equals("1006571")) {
          strReconXmlData = generateOutPut(hmResult, alResult, "GmInHouseReconTrans.vm");
        } else {
          strReconXmlData = generateOutPut(hmResult, alResult, "GmLoanerReconciledDetails.vm");
        }
        hmReturn.put("RECON_XMLDATA", strReconXmlData);
        alResult =
            GmCommonClass.parseNullArrayList(gmLoanerReconBean.fetchReconciledItems(strTransId));

        strReconTransXmlData = generateOutPut(hmResult, alResult, "GmReconLoaner.vm");
        hmReturn.put("RECONTRAN_XMLDATA", strReconTransXmlData);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmReturn.put("ALCBOACTION", alReturn);
        hmReturn.put("CBO_ACTION", strCboAction);
        alLogReasons = gmCommonBean.getLog(strhTranId, "1245");
        alLogReasons.addAll(gmCommonBean.getLog(strTransId, "1246"));

        log.debug("alLogReasons ==> " + alLogReasons + " strhTranId :: " + strhTranId);

        // The following code added for Getting the Distributor & Sales Rep based on the PLANT if
        // the Selected Company is EDC
        strCompanyID = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
        strCompPlantFilter =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyID, "LOANER_RECON"));// LOANER_RECON-
                                                                                              // To
                                                                                              // fetch
                                                                                              // the
                                                                                              // records
                                                                                              // for
                                                                                              // EDC
                                                                                              // entity
                                                                                              // countries
                                                                                              // based
                                                                                              // on
                                                                                              // plant
        if (strCompPlantFilter.equals("Plant")) {
          hmFilter.put("REPFILTER", "COMPANY-PLANT");
          hmFilter.put("DISTFILTER", "COMPANY-PLANT");
        }
        alRepList = gmCust.getRepList(hmFilter);
        alDistList = gmCust.getDistributorList(hmFilter);

        alAccList = gmSales.reportAccount();
        alStatus = GmCommonClass.getCodeList("LRTYP");
        request.setAttribute("DATETYPE", alDateType);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hTransId", strTransId);
        request.setAttribute("hmLog", alLogReasons);
        request.setAttribute("strMsg", strMsg);

        request.setAttribute("REPLIST", alRepList);
        request.setAttribute("DISTLIST", alDistList);
        request.setAttribute("ACCLIST", alAccList);
        request.setAttribute("ORDSTATUS", alStatus);
      }
      if (strAction.equals("LoadFilter")) {
        Enumeration enumRequest = request.getParameterNames();
        ArrayList alReason = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LRRSN"));
        HashMap hmOpt = new HashMap();
        hmOpt.put("ALREASON", alReason);
        while (enumRequest.hasMoreElements()) {
          String strNames = GmCommonClass.parseNull((String) enumRequest.nextElement());
          hmParam.put(strNames, request.getParameter(strNames));
        }
        hmParam.put("DTFMT", strDateFormat);
        log.debug("hmParams ::: " + hmParam);
        alResult = gmLoanerReconBean.fetchDoItems(hmParam);
        strXmlData = generateOutPut(hmOpt, alResult, "GmUnRecDo.vm");
        // log.debug("XMLDATA ::: " + strXmlData);
        response.setContentType("text/xml");
        PrintWriter pwOut = response.getWriter();
        pwOut.write(strXmlData);
        pwOut.close();
      } else {
        request.setAttribute("hmReturn", hmReturn);

        dispatch(strDispatchTo, request, response);
      }

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private String generateOutPut(HashMap hmParam, ArrayList alReturn, String strVmName)
      throws AppError {

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alReason = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALREASON"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alReturn", alReturn);
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setTemplateName(strVmName);
    templateUtil.setDropDownMaster("alReason", alReason);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmLoanerRecon", strSessCompanyLocale));
    return templateUtil.generateOutput();
  }

}// End of GmLoanerReconServlet
