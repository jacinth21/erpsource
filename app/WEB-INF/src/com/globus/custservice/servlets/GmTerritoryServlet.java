/*****************************************************************************
 * File : GmTerritoryServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.beans.GmTerritoryBean;
import com.globus.sales.beans.GmTerritoryReportBean;

public class GmTerritoryServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = strOperPath.concat("/GmTerritorySetup.jsp");
    RowSetDynaClass resultSet = null;
    Logger log = GmLogger.getInstance(this.getClass().getName());

    GmCommonClass gmCommon = new GmCommonClass();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
    GmCommonBean gmCommBn = new GmCommonBean(getGmDataStoreVO());
    GmTerritoryBean gmTerr = new GmTerritoryBean(getGmDataStoreVO());
    GmTerritoryReportBean gmTerrRep = new GmTerritoryReportBean(getGmDataStoreVO());
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alPartyCompany = new ArrayList();
    ArrayList alCurrType = new ArrayList();

    String strTerrId = "";
    String strId = "";
    String strTerrNm = "";
    String strInputString = "";
    String strYear = "";
    String strType = "";
    String strUserId = (String) session.getAttribute("strSessUserId");
    String strAccessFilter = "";
    String strLog = "";
    String strActiveFl = "";
    String strRepId = "";
    String strCompanyId = "";
    String strCurrTypeId = "";
    String strhDivId = "";
    String strDivId = GmCommonClass.parseNull(request.getParameter("DivId"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strCompLangId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLangId"));


    String strAction = request.getParameter("hAction");
    if (strAction == null) {
      strAction = (String) session.getAttribute("hAction");
    }
    strAction = (strAction == null) ? "Load" : strAction;
    String strOpt = request.getParameter("hOpt");

    if (strOpt == null) {
      strOpt = (String) session.getAttribute("strSessOpt");
    }
    strOpt = (strOpt == null) ? "" : strOpt;

    String strPartyID = getGmDataStoreVO().getPartyid();
    hmParam.put("PARTYID", strPartyID);
    alPartyCompany = gmPartyBean.fetchPartyCompany(hmParam);
    request.setAttribute("COMPANY_LIST", alPartyCompany);

    if (strDivId.equals("")) {
      strDivId = GmCommonClass.parseNull(request.getParameter("hDivId"));
    }
    hmParam.put("DIVID", strDivId);
    request.setAttribute("hDivId", strDivId);

    strCompanyId = GmCommonClass.parseZero(request.getParameter("Cbo_CompanyId"));
    if (strCompanyId.equals("0")) {

      strCompanyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    }
    request.setAttribute("hCompanyId", strCompanyId);

    strCurrTypeId = GmCommonClass.parseZero(request.getParameter("Cbo_CurrType"));
    request.setAttribute("hCurrTypeId", strCurrTypeId);

    if (strAction.equals("Load") && strOpt.equals("Report")) {
      alReturn = gmCust.getRegionDistTerrMapping("2006");
      hmReturn.put("REPORT", alReturn);
      request.setAttribute("hmReturn", hmReturn);
      strDispatchTo = strSalesPath.concat("/GmRegnDistTerrMapping.jsp");
    } else if (strAction.equals("Load") && strOpt.equals("MapReport")) {
      resultSet = gmTerrRep.reportRegnDistTerrMapping(strCompLangId);
      log.debug("Result Set is " + resultSet);
      ArrayList alResult = (ArrayList) resultSet.getRows();
      String strXmlData = generateOutPut(alResult, strSessCompanyLocale);
      log.debug("Xml Data : " + strXmlData);
      request.setAttribute("strXmlData", strXmlData);
      strDispatchTo = strSalesPath.concat("/GmRegnDistTerrMappingReport.jsp");
      log.debug("Str Path : " + strDispatchTo);
    } else if (strAction.equals("Reload") && strOpt.equals("")) {
      strYear = GmCommonClass.parseNull(request.getParameter("Cbo_Year"));


      strRepId = GmCommonClass.parseNull(request.getParameter("Cbo_RepId"));


      hmResult =
          GmCommonClass.parseNullHashMap(gmTerr.loadTerritoryDetails(strRepId, "", strYear,
              strCurrTypeId, strCompLangId));

      hmTemp = GmCommonClass.parseNullHashMap((HashMap) hmResult.get("TERRITORYDETAILS"));
      strTerrId = GmCommonClass.parseNull((String) hmTemp.get("TERRID"));
      hmReturn.put("TERRITORYDETAILS", hmResult.get("TERRITORYDETAILS"));
      hmReturn.put("ACCOUNTSLIST", hmResult.get("ACCOUNTSLIST"));
      hmReturn.put("QUOTA", hmResult.get("QUOTA"));
      ArrayList alQuotaReturn = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("QUOTA"));
      if (alQuotaReturn.size() == 0 && !strRepId.equals("0") && !strTerrId.equals("")) {
        strAction = "Edit";
      } else {

        hmResult = gmTerr.loadTerritory();
        // hmReturn.put("TERRITORYLIST",hmResult.get("TERRITORYLIST"));
        hmReturn.put("YEARLIST", hmResult.get("YEARLIST"));
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hYear", strYear);
        request.setAttribute("hRepId", strRepId);
        alReturn = gmCom.getLog(strTerrId, "1212");
        request.setAttribute("hmLog", alReturn);
        // setting values to GmTerritoryQuota.vm files


        request.setAttribute(
            "rsQuotaReport",
            generateOutPut(alQuotaReturn, (ArrayList) hmResult.get("YEARLIST"),
                strSessCompanyLocale));

        // Populate the Rep List.
        strAction = "LoadRepList";
      }
    } else if (strAction.equals("ShowAcc")) {
      strId = GmCommonClass.parseNull(request.getParameter("hId"));
      strType = GmCommonClass.parseNull(request.getParameter("hType"));

      // To get the Access Level information from Materialized view
      strAccessFilter = getAccessFilter(request, response);

      alReturn = gmTerr.getAccountsList(strId, strType, strAccessFilter, strCompLangId);
      hmReturn.put("ACCLIST", alReturn);
      request.setAttribute("hmReturn", hmReturn);

      strDispatchTo = strSalesPath.concat("/GmTerrAccountReport.jsp");
    }
    if (strAction.equals("Add") || strAction.equals("Edit")) {
      strTerrId = GmCommonClass.parseNull(request.getParameter("hTerrId"));
      strRepId = GmCommonClass.parseNull(request.getParameter("Cbo_RepId"));
      strTerrNm =
          request.getParameter("Txt_TerrNm") == null ? "" : request.getParameter("Txt_TerrNm");
      strYear = request.getParameter("Cbo_Year") == null ? "" : request.getParameter("Cbo_Year");
      strInputString =
          request.getParameter("inputString") == null ? "" : request.getParameter("inputString");
      strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
      strActiveFl = GmCommonClass.parseNull(request.getParameter("Chk_ActiveFl"));

      HashMap hmValues = new HashMap();
      hmValues.put("TID", strTerrId);
      hmValues.put("REPID", strRepId);
      hmValues.put("TNAME", strTerrNm);
      hmValues.put("YEAR", strYear);
      hmValues.put("INPUTSTR", strInputString);
      hmValues.put("LOG", strLog);
      hmValues.put("AFLAG", strActiveFl);
      hmValues.put("CURRTYPEID", strCurrTypeId);

      HashMap hmSaveReturn = new HashMap();
      hmSaveReturn = gmTerr.saveTerritory(hmValues, strUserId, strAction);
      strTerrId = (String) hmSaveReturn.get("TID");

      hmResult =
          gmTerr.loadTerritoryDetails(strRepId, strTerrId, strYear, strCurrTypeId, strCompLangId);
      hmReturn.put("TERRITORYDETAILS", hmResult.get("TERRITORYDETAILS"));
      hmReturn.put("ACCOUNTSLIST", hmResult.get("ACCOUNTSLIST"));
      hmReturn.put("QUOTA", hmResult.get("QUOTA"));
      ArrayList alQuotaReturn = (ArrayList) hmResult.get("QUOTA");
      hmResult = gmTerr.loadTerritory();

      strAction = "LoadRepList";

      // hmReturn.put("TERRITORYLIST",hmResult.get("TERRITORYLIST"));
      hmReturn.put("YEARLIST", hmResult.get("YEARLIST"));
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hYear", strYear);
      request.setAttribute("hAction", "Edit");
      request.setAttribute("hRepId", strRepId);
      alReturn = gmCom.getLog(strTerrId, "1212");
      request.setAttribute("hmLog", alReturn);

      // setting values to GmTerritoryQuota.vm files

      request
          .setAttribute(
              "rsQuotaReport",
              generateOutPut(alQuotaReturn, (ArrayList) hmResult.get("YEARLIST"),
                  strSessCompanyLocale));
    }

    if (strAction.equals("LoadRepList")) {

      hmParam.put("COMPANY_ID", strCompanyId);

      // Call the Sales Rep Filter only when the Company ID is coming in from the Front end.
      log.debug(" BEFORE CALLING REPLIST ))))))");
      if (!strCompanyId.equals(""))
        alReturn = gmCust.getRepList(hmParam);

      hmReturn.put("REPLIST", alReturn);
      request.setAttribute("hmReturn", hmReturn);

      alCurrType = gmCommon.getCodeList("CURRTY");
      request.setAttribute("CURRTYPE", alCurrType);

      this.getCompanyInfo(request, strCompanyId);
    }

    dispatch(strDispatchTo, request, response);

  }// End of service method

  private void getCompanyInfo(HttpServletRequest request, String strCompanyID) {
    HashMap hmCompanyInfo = null;
    String strTxnCurrency = null;
    String strReportCurrency = null;

    hmCompanyInfo = (new GmCompanyBean(getGmDataStoreVO())).fetchCompanyInfo(strCompanyID);

    strTxnCurrency = (String) hmCompanyInfo.get("TXNCURR");
    strReportCurrency = (String) hmCompanyInfo.get("TXNRPTCURR");
    request.setAttribute("TXNCURR", strTxnCurrency);
    request.setAttribute("RPTCURR", strReportCurrency);

  }

  private String generateOutPut(ArrayList alGridData, String strSessCompanyLocale) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.GmRegnDistTerrMapping", strSessCompanyLocale));
    templateUtil.setTemplateName("GmRegionDistTerrMapping.vm");
    return templateUtil.generateOutput();
  }

  private String generateOutPut(ArrayList alValues, ArrayList alYearList,
      String strSessCompanyLocale) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alValues);
    templateUtil.setDropDownMaster("alYearList", alYearList);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.GmTerrSalesToQuota", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setTemplateName("GmTerritoryQuota.vm");
    return templateUtil.generateOutput();
  }
}// End of GmTerritoryServlet
