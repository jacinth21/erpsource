/*****************************************************************************
 * 
 * File : GmAccountServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmParentRepAccountBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.sales.beans.GmSalesMappingBean;

/**
 *
 * This class used to load/save the account information.
 * Screen location : Sales Admin - Transactions - Account setup
 *
 */
public class GmAccountServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    log.debug("  GmAccountServlet loading...success");

    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    String strHideButton = "";
    String strReadFl = "";
    String strUpdFl = "";
    String strVoidFl = "";

    String strMPReadFl = "";
    String strMPUpdFl = "";
    String strMPVoidFl = "";
    String strAccAddEditAcsFl = "";
    String strAccRepEditAcsFl = "";
    String strAccAddrsEditAcsFl = "";
    String strAcctPricing = GmCommonClass.parseNull(GmCommonClass.getString("LOAD_LIST_PRICE"));
    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt"); // for DIVCONTAINER
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmCustSalesSetupBean gmCust = new GmCustSalesSetupBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean(getGmDataStoreVO());
      GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
      GmParentRepAccountBean gmParentRepAccountBean =
          new GmParentRepAccountBean(getGmDataStoreVO());
      GmAutoCompleteTransBean gmAutoCompleteTransBean =
          new GmAutoCompleteTransBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmAccSrc = new HashMap();

      ArrayList alReturn = new ArrayList();
      ArrayList alPartyList = new ArrayList();
      ArrayList alResult = new ArrayList();
      ArrayList alReturnList = new ArrayList();
      ArrayList alAcctSrc = new ArrayList();
      ArrayList alAcctInternalRep = new ArrayList();

      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      String strId = "";
      String hAccAttInputStr = "";
      String strCollectorId = "";
      String strAccountSrc = "";
      String strReloadCount = "";
      String strForwardName = "/GmAccountSetup.jsp"; // By default,forward to Basicinfo tab
      // to set Account default CollectorId value from RULES.
      strCollectorId =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("COLLECTORID", "DEFAULT"));
      request.setAttribute("DEFCOLLECTORID", strCollectorId);

      // MAP_ACC_PARAMS - This group used for set the Account params value in OUS and enable the
      // button 'Map Account Parameters'
      String strPartyId = (String) session.getAttribute("strSessPartyId");
      // Get the reload count from screen
      strReloadCount = GmCommonClass.parseNull(request.getParameter("hReloadCount"));
      request.setAttribute("RELOADCOUNT", strReloadCount);
      alPartyList =
          GmCommonClass.parseNullArrayList(gmAccessControlBean
              .getPartyList("MAPPARAMS", strPartyId));
      // Below code is to show the button "Map Account Parameters" based on access
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "MAP_ACC_PARAMS"));
      String strMAPReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      String strMAPUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("SHOW_ACC_PARAMS", strMAPUpdFl);

      log.debug("strPartyId " + strPartyId);
      // AR_ACC_INSERT - Users in this security group will have the ability to add account
      // parameters in the Account Setup Screen and shows the button 'Account Parameter Setup'
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "AR_ACC_INSERT"));
      strReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      strVoidFl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
      // MAP_SHIP_ACCESS - Users in this security group will have the ability to map shipping
      // parameters.
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "MAP_SHIP_ACCESS"));
      strMPReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      strMPUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      strMPVoidFl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
      request.setAttribute("READFL", strMPReadFl);
      // ACC_SETUP_ACCESS - Users in this security group will have the ability to view the
      // information that was setup in the Account Setup Screen.
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "ACC_SETUP_ACCESS"));
      String strCSReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      String strCSUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      String strCSVoidFl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
      // ACCOUNT_ADD_EDIT - Users in this security event having an access to create new account
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "ACCOUNT_ADD_EDIT"));
      strAccAddEditAcsFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("ACC_ADD_EDIT_ACCESS", strAccAddEditAcsFl);
      if(!strAccAddEditAcsFl.equals("Y")){
          // ACCOUNT_ADDRESS_EDIT - Users in this security event having an access to edit address account info
          hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                  "ACCOUNT_ADDRESS_EDIT"));
          strAccAddrsEditAcsFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          request.setAttribute("ACC_ADDRS_EDIT_ACCESS", strAccAddrsEditAcsFl);

          // ACCOUNT_REP_EDIT - Users in this security event having an access to edit rep account info
           hmAccess =
               GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                      "ACCOUNT_REP_EDIT"));
          strAccRepEditAcsFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          request.setAttribute("ACC_REP_EDIT_ACCESS", strAccRepEditAcsFl);
         
      }
     
      // SHIP_ATTN_ACCESS - Users in this security group will have the ability to enter Ship Attn
      // information.
      HashMap hmShipSetupAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "SHIP_ATTN_ACCESS"));
      String strShipSetupUpdFl = GmCommonClass.parseNull((String) hmShipSetupAccess.get("UPDFL"));
      request.setAttribute("ACCSHIPUPDFL", strShipSetupUpdFl);
      String strPortalCompanyId = getGmDataStoreVO().getCmpid();
      strAccountSrc = strPortalCompanyId.equals("1001") ? "Y" : "N";
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

      alAcctInternalRep= GmCommonClass.parseNullArrayList(gmParentRepAccountBean.fetchInternalRepList("ALACCTINTERNALREP")); //PMT-41644
      request.setAttribute("ALACCTINTERNALREP", alAcctInternalRep);
      // The Group drop down is showing only based on the value of SHOW_ACCOUNT_GRP,
      String strAccountGrp =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_ACCOUNT_GRP"));
      // Account source - to show only BBA
      if (strAccountSrc.equals("Y")) {
        hmAccSrc.put("ACCOUNTSRC", strAccountSrc);
        hmAccSrc.put("FILTER", "Active");
        hmAccSrc.put("DIRECTREPTYPE",
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("DIRECT_REP", "ACCT_SOURCE")));
        alAcctSrc = GmCommonClass.parseNullArrayList(gmCustomerBean.getRepList(hmAccSrc));

        request.setAttribute("ALACCTSRC", alAcctSrc);
        request.setAttribute("ACCOUNTSRC", strAccountSrc);
      }
      if (strAccountGrp.equalsIgnoreCase("YES")) {
        GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
        ArrayList alAccountGrp = gmPartyInfoBean.fetchPartyNameList("7008");
        request.setAttribute("ACCOUNT_GRP_LIST", alAccountGrp);
        request.setAttribute("SHOWACCOUNTGRP", strAccountGrp);
      }

      if (strCSReadFl.equals("Y") || strCSUpdFl.equals("Y") || strCSVoidFl.equals("Y")) {
        request.setAttribute("CSDISABLE", "disabled=disabled");
      }
      // Submit button access ,if the user is mapped under C/S as well as in A/R.
      if (strReadFl.equals("N") || strUpdFl.equals("N") || strVoidFl.equals("N")) {
        request.setAttribute("CSDISABLE", "");
      }
      if (strAction.equals("Load")) {
        if (!strReadFl.equals("Y")) {
          request.setAttribute("DISABLE", "disabled=disabled");
        } else {
          request.setAttribute("DISABLE", "");
        }

      } else if (strAction.equals("Reload")) {
        // if(strUpdFl.equals("Y")){
        request.setAttribute("DISABLE", "");
        // }
        strId = GmCommonClass.parseNull(request.getParameter("hAccountId"));
        strHideButton = GmCommonClass.parseNull(request.getParameter("hideButton"));
        hmReturn = gmCust.loadEditAccount(strId);
        hmReturn.put("PORTAL_COMP_ID", strPortalCompanyId);
        // if new account id comes ,it goes to Add orelse it goes to edit
        if (strId.equals("")) {
          request.setAttribute("hAction", "Add");
        } else {
          request.setAttribute("hAction", "Edit");
        }
        alReturn = gmCom.getLog(strId, "1209");
        request.setAttribute("hmLog", alReturn);
        request.setAttribute("HideButton", strHideButton);
      } else if (strAction.equals("Add") || strAction.equals("Edit")) {
        String strUserId = (String) session.getAttribute("strSessUserId");

        String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        hAccAttInputStr = GmCommonClass.parseNull(request.getParameter("hAccAttInputStr"));
        //
        String strRepAccountStr = GmCommonClass.parseNull(request.getParameter("hRepAccInputStr"));
        log.debug("strRepAccountStr==========="+strRepAccountStr);
        String strParentAccountStr =
            GmCommonClass.parseNull(request.getParameter("hParentAccInputStr"));
        log.debug("strParentAccountStr==========="+strParentAccountStr);
        HashMap hmValues = new HashMap();

        strId = request.getParameter("hAccountId");
        String strOldRepAccName =
            GmCommonClass.parseNull(request.getParameter("hOld_Account_Name"));
        String strNewRepAccName = GmCommonClass.parseNull(request.getParameter("Txt_Rep_AccNm"));
        String strOldParentAccName =
            GmCommonClass.parseNull(request.getParameter("hOld_Parant_Acc_Name"));
        String strNewParentAccName =
            GmCommonClass.parseNull(request.getParameter("Txt_Parent_AccNm"));
        //PMT-39047 Getting the old and new acc type
        String strOAccType = GmCommonClass.parseNull(request.getParameter("holdAccType"));
        String strAccType = GmCommonClass.parseNull(request.getParameter("Cbo_AccType"));


        hmValues.put("ACCATTINPUTSTR", hAccAttInputStr);
        hmValues.put("LOG", strLog);
        hmValues.put("ACCID", strId);
        hmValues.put("REP_ACC_INPUT_STR", strRepAccountStr);
        hmValues.put("PARENT_ACC_INPUT_STR", strParentAccountStr);
        strId = gmCust.saveAccount(hmValues, strUserId, strAction);
   
         if(strPortalCompanyId.equals("1000") && strAction.equals("Add")) {
    	gmCust.sendRepAcctEmail(request,response,strId);
    }
       
        hmReturn = gmCust.loadEditAccount(strId);
        hmReturn.put("PORTAL_COMP_ID", strPortalCompanyId);
        request.setAttribute("hmReturn", hmReturn);
        alReturn = gmCom.getLog(strId, "1209");
        request.setAttribute("hmLog", alReturn);
        request.setAttribute("hAction", "Edit");

        GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
        // View refresh JMS called
        HashMap hmViewRefresh = new HashMap();
        hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
        hmViewRefresh.put("companyInfo",
            GmCommonClass.parseNull(request.getParameter("companyInfo")));      
        String strViewConsumerClass =
            GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
        hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
        gmConsumerUtil.sendMessage(hmViewRefresh);

        // Redis JMS call
        HashMap hmRepAcc = new HashMap();
        hmRepAcc.put("ID", strId);
        hmRepAcc.put("OLD_NM", strOldRepAccName);
        hmRepAcc.put("NEW_NM", strNewRepAccName);
        hmRepAcc.put("METHOD", "RepAccount");
        hmRepAcc.put("OLD_TYPE", strOAccType);
        hmRepAcc.put("NEW_TYPE", strAccType);
        hmRepAcc.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
        gmAutoCompleteTransBean.saveDetailsToCache(hmRepAcc);


        String strPrtyId = gmParentRepAccountBean.fetchPartyId(strId);
        HashMap hmParentAcc = new HashMap();
        hmParentAcc.put("ID", strPrtyId);
        hmParentAcc.put("OLD_NM", strOldParentAccName);
        hmParentAcc.put("NEW_NM", strNewParentAccName);
        hmParentAcc.put("METHOD", "PartyList");
        hmParentAcc.put("PARTY_TYPE", "4000877");
        hmParentAcc
            .put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
        
        // PMT-52804: To fix parent account not refreshed issue
        gmAutoCompleteTransBean.saveDetailsToCache(hmParentAcc);
      
      } else if (strAction.equals("EditLoad")) {
        strId = request.getParameter("hAccountId");
        hmReturn = gmCust.loadEditAccount(strId);
        hmReturn.put("PORTAL_COMP_ID", strPortalCompanyId);
        request.setAttribute("hmReturn", hmReturn);
        alReturn = gmCom.getLog(strId, "1209");
        request.setAttribute("hmLog", alReturn);
      }
      // code to get Collector Attributes
      if (strAction.equals("fetchAccAttr")) // This action is for ajax call so need to forward to
                                            // jsp
      {
        HashMap hmParam = new HashMap();

        String strAccAttrVal = "";
        String strAccAttrNm = "";
        String strRetVal = "";
        String strAccId = request.getParameter("hAccId");
        String strAccAttrType = request.getParameter("hAccAttrType");

        hmParam.put("ACCID", strAccId);
        hmParam.put("ACCATTRTYPE", strAccAttrType);
        hmResult = gmCust.fetchAccountAttribute(hmParam);

        strAccAttrVal = GmCommonClass.parseNull((String) hmResult.get("ATTRI_VALUE"));
        strAccAttrNm = GmCommonClass.parseNull((String) hmResult.get("ATTRI_NAME"));
        strRetVal = strAccAttrVal + '^' + strAccAttrNm;
        log.debug(" strRetVal ==> " + strRetVal);
        response.setContentType("text/plain");
        PrintWriter pw = response.getWriter();
        pw.write(strRetVal);
        pw.flush();
      }
      // The below code is added for PMT-5341 This code is used to enable the account rep map.
      else if (strAction.equals("assocrepmap")) {

        HashMap hmAssoc = new HashMap();
        HashMap hmAssociateRepData = new HashMap();
        ArrayList alAssocRep = new ArrayList();
        String strAccountId = GmCommonClass.parseNull(request.getParameter("hAccountId"));
        strOpt = GmCommonClass.parseNull(request.getParameter("hstrOpt"));
        if (strOpt.equals("Save")) {
          String strInputString = GmCommonClass.parseNull(request.getParameter("hInputString"));
          String strVoidInputString = GmCommonClass.parseNull(request.getParameter("hVoidString"));
          hmAssociateRepData.put("INPUTSTRING", strInputString);
          hmAssociateRepData.put("VOIDINPUTSTRING", strVoidInputString);
          hmAssociateRepData.put("ACCTID", strAccountId);
          hmAssociateRepData.put("USERID", strUserId);
          gmSalesMappingBean.SaveAssocRepList(hmAssociateRepData);
        }

        if (!strAccountId.equals("")) { // for PAC-if account id is empty
          hmAssoc = GmCommonClass.parseNullHashMap(fetchAssociatedReps(strAccountId));
          alAssocRep = GmCommonClass.parseNullArrayList((ArrayList) hmAssoc.get("ALASSOCREP"));
          request.setAttribute("HMASSOCREPDATA", hmAssoc);
        }
        hmReturn.put("ASSOCREPDATA", alAssocRep);
        hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
        String strxmlGridData = generateOutPut(hmReturn);
        request.setAttribute("GRIDDATA", strxmlGridData);
        dispatch(strSalesPath.concat("/GmAssocRepAcctMap.jsp"), request, response);

      } else // coded added into else condition because for ajax call no need to to forward to jsp
      {
        if (alPartyList.size() != 0) {
          request.setAttribute("hShowButton", "true");
        }
        request.setAttribute("hAccountId", strId);
        request.setAttribute("hmReturn", hmReturn);

        // To save the Account Parameters
        if (strAction.equals("SaveMapAccParams")) {
          strId = GmCommonClass.parseNull(request.getParameter("hAccountId"));
          String strUserId =
              GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
          String strInputString = GmCommonClass.parseNull(request.getParameter("hInputstr"));
          gmCust.saveAccAttributes(strId, strUserId, strInputString, strAction);
          strAction = "LoadMapAccParams";
        }
        // To fetch the Account Parameters
        if (strAction.equals("LoadMapAccParams")) {
          HashMap hmTemp = new HashMap();
          strId = GmCommonClass.parseNull(request.getParameter("hAccountId"));
          strHideButton = GmCommonClass.parseNull(request.getParameter("hideSubmitButton"));
          // Below code is to get the code name alt of ACCATR
          String strCodeName =
              GmCommonClass.parseNull(GmCommonClass.getCodeAltName(GmCommonClass.getCodeID(
                  "Account Attribute", "ACCATR")));
          alResult =
              GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(strCodeName,
                  getGmDataStoreVO()));

          Iterator iter = alResult.iterator();
          while (iter.hasNext()) {
            hmResult = (HashMap) iter.next();
            String strCntrlTyp = GmCommonClass.parseNull((String) hmResult.get("CONTROLTYP"));
            String strCdNmAlt = GmCommonClass.parseNull((String) hmResult.get("CODENMALT"));
            if (strCntrlTyp.equalsIgnoreCase("COMBOX")) {
              if (!strCdNmAlt.equals("")) {
                alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(strCdNmAlt));
                hmResult.put("DROPDOWNGRP", alReturn);
              }
            }
            alReturnList.add(hmResult);
          }
          request.setAttribute("hAccountId", strId);
          request.setAttribute("hideSubmitButton", strHideButton);
          request.setAttribute("alMapAccParams", alReturnList);
          alReturnList = gmCust.loadMapAccountAttribute(strId, strCodeName);
          request.setAttribute("alAccAttributes", alReturnList);
          dispatch(strOperPath.concat("/GmAccountMappingSetup.jsp"), request, response);
        } else if (strAction.equalsIgnoreCase("loadParentInfo")) {
          String strJSON = "";
          String strParentAccountId =
              GmCommonClass.parseNull(request.getParameter("Cbo_Parent_Account_Id"));
          GmWSUtil gmWSUtil = new GmWSUtil();
          strJSON = gmWSUtil.parseHashMapToJson(gmCust.fetchParentAccountInfo(strParentAccountId));
          log.debug("strParentAccountId ::" + strParentAccountId + ":: strJSON :: " + strJSON);
          if (!strJSON.equals("")) {
            response.setContentType("text/plain; charset=UTF-8");
            response.setCharacterEncoding("UTF-8");
            PrintWriter pw = response.getWriter();
            pw.write(strJSON);
            pw.close();
          }
        } else {
          if (strOpt.equals("FETCHCONTAINER")) {
            strForwardName = "/GmAccountSetupContainer.jsp"; // forward to container Jsp
          }
          dispatch(strOperPath.concat(strForwardName), request, response); // forward to Basic Info
                                                                           // tab
        }
      }
      
    } // End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
   // The below method is added for PMT-5341 here it us used fetch the Account related information
   // ,Rep List and asoociate reps mapped to an account

  private HashMap fetchAssociatedReps(String strAccountId) {
    GmCustomerBean gmcustbean = new GmCustomerBean(getGmDataStoreVO());
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean(getGmDataStoreVO());

    HashMap hmAcct = new HashMap();
    ArrayList alRepList = new ArrayList();
    ArrayList alAssocRep = new ArrayList();
    HashMap hmAssocRepDetail = new HashMap();

    hmAcct = GmCommonClass.parseNullHashMap(gmcustbean.getAccSummary(strAccountId));

    alRepList = gmcustbean.getRepList("Active");

    alAssocRep = GmCommonClass.parseNullArrayList(gmSalesMappingBean.getAssocRepList(strAccountId));
    hmAssocRepDetail.put("ALREPLIST", alRepList);
    hmAssocRepDetail.put("HMACCOUNT", hmAcct);
    hmAssocRepDetail.put("ALASSOCREP", alAssocRep);

    return hmAssocRepDetail;

  }

  /**
 * This method used to generate the XML data for Associate Rep mapping details
 * 
 * @param hmParam
 * @return String
 * @throws AppError
 */
  
private String generateOutPut(HashMap hmParam) throws AppError { // Processing HashMap into XML
                                                                   // data
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.GmAssocRepAcctMap", strSessCompanyLocale));
    templateUtil.setTemplateName("GmAssocRepMapping.vm");
    return templateUtil.generateOutput();
  }
}// End of GmAccountServlet
