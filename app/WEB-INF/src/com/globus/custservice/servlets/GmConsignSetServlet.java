/*****************************************************************************
 * File : GmConsignSetServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmAccountingBean;
import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.printwork.consignment.beans.GmConsignmentPrintInterface;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.common.servlets.GmShippingServlet;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmConsignPackSLipInterface;
import com.globus.custservice.beans.GmConsignPackSlipInstance;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmDOReportBean;
import com.globus.custservice.beans.GmLotExpiryBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.operations.returns.beans.GmReturnsBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmConsignSetServlet extends GmShippingServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
   

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmConsignReport.jsp");
    String strOperPath = GmCommonClass.getString("GMREQUESTS");
    String strInvoicePDF = GmCommonClass.getString("BATCH_INVOICE");
    log.debug("GmConsignSetServlet");
    try {
      // checkSession(response, session); // Checks if the current session
    	log.debug("GmConsignSetServlet111");
     // instantiate(request, response);
  	log.debug("GmConsignSetServlet2222");
  	
  	 String type = GmCommonClass.parseNull(request.getParameter("type"));
  	 //String strRtfType = type;
     String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
     GmDataStoreVO tmpGmDataStoreVO = null;
     log.debug("GmConsignSetServlet//type "+type);
    /* if (type.equals("saveRTFfile")) {
    	 type = "savePDFfile";
     }*/
     if (!type.equals("savePDFfile")) {
       HttpSession session = request.getSession(false);
       checkSession(response, session); // Checks if the current session is valid, else redirecting
                                        // to SessionExpiry page
       instantiate(request, response);
     }
     Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                  // Class.

     if (type.equals("savePDFfile")) {
       // When calling the JMS some times company info came as empty values.
       // at the time we are getting the default company information.
       if (strComapnyInfo.equals("")) {
         tmpGmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
       } else {
         tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strComapnyInfo);
       }
     } else {
       tmpGmDataStoreVO = getGmDataStoreVO();
     }

      // is valid, else redirecting to
      // SessionExpiry page
   
      // to
      // Initialize
      // the
      // Logger
      // Class.

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = 
        !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session
                .getAttribute("hAction")) : "";
      }
      strAction = (strAction == null) ? "Load" : strAction;
      log.debug(" GmConsignSetServlet action is " + strAction);
      
      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(tmpGmDataStoreVO);
      GmOperationsBean gmOper = new GmOperationsBean(tmpGmDataStoreVO);
      GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(tmpGmDataStoreVO);
      GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(tmpGmDataStoreVO);
      GmRuleEngine gmRuleEngine = new GmRuleEngine(tmpGmDataStoreVO);
      GmCommonBean gmCommonBean = new GmCommonBean(tmpGmDataStoreVO);
      GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(tmpGmDataStoreVO);
      GmCommonCancelBean gmCommonCancel = new GmCommonCancelBean(tmpGmDataStoreVO);
      GmReturnsBean gmReturnBean = new GmReturnsBean(tmpGmDataStoreVO);
      GmARBean gmAR = new GmARBean(tmpGmDataStoreVO);
      GmDOBean gmDOBean = new GmDOBean(tmpGmDataStoreVO);
      GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(tmpGmDataStoreVO);
      GmARBatchBean gmARBatchBean = new GmARBatchBean(tmpGmDataStoreVO);
      GmDOReportBean gmDOReportBean = new GmDOReportBean(tmpGmDataStoreVO);
      GmLotExpiryBean gmLotExpiryBean = new GmLotExpiryBean(tmpGmDataStoreVO);
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmPartyAccess = new HashMap();
      HashMap hmPutGroupId = new HashMap();
      HashMap hmruleShipVal = new HashMap();
      HashMap hmAtbData = new HashMap();
      HashMap hmCompAtbData = new HashMap();
      HashMap hmSetMaster = new HashMap();

      ArrayList alResult = new ArrayList();
      List ltBackOrderReport = new ArrayList();
      List ltChildRequest = new ArrayList();
      ArrayList alAttrib = new ArrayList();
      ArrayList alSterilePartList = new ArrayList();

      hmPutGroupId.put("RULEID", "SHIP_CARR_MODE_VAL");
      hmruleShipVal = gmCommonBean.fetchTransactionRules(hmPutGroupId, tmpGmDataStoreVO);// ship
                                                                                           // carrier
                                                                                           // and
                                                                                           // mode
      // default value get from
      // rule
      hmReturn.put("DEFAULTSHIPMODECARRVAL", hmruleShipVal);

      String strConsignId = "";
      String stConsignId = "";
      String strType = "";
      String strDistribAccId = "";
      String strAccId = "";
      String strSalesRepId = "";
      String strPurpose = "";
      String strBillTo = "";
      String strShipTo = "";
      String strShipToId = "";
      String strFinalComments = "";
      String strShipFl = "";
      String strDelFl = "";
      String strUserId =  !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session.getAttribute("strSessUserId")) : GmCommonClass.parseNull(request.getParameter("hUserID"));

      String strShipDate = "";
      String strShipCarr = "";
      String strShipMode = "";
      String strTrack = "";
      String strFreightAmt = "";
      String strPONum = "";

      String strMode = "";

      String strStatFlag = "";
      String strVeriFlag = "";
      String strSetId = "";
      String strRequestId = "";
      String strCustPONum = "";
      String strPrnPdf = "";
      String strFilePath = "";
      String strInvType = "";

      String strruleEnforceDate = "";
      String strAdd = "";
      String strShipDt = "";
      String strTransType = "";
      String strInitiatedFrom = "";
      String strDemandSheetMonthID = "";
      String strCmnPoolAcs = "";
      String strPartyId = "";
      String strSetID = "";
      String strDSheetName = "";
      String strReqSrc = "";
      String strReqStatusFlag = "";
      String strLinkReqId = "";
      String strTermofsale = "";
      String strTermofsaleId = "";
      String strPerforma="";
      // getting the company id
      String strPortalCompanyId = tmpGmDataStoreVO.getCmpid();
      String strCompanyLocale = "";
      String strVersionForward = "";
      String strCompanyPhone = "";
      String strHidePhoneNo = "";
      String strGSTStartFl = "";
      String strOrdId = "";
      String strCompanyId = GmCommonClass.parseNull(tmpGmDataStoreVO.getCmpid());
      String strSterileExpDateRule="";
      strCompanyLocale = GmCommonClass.getCompanyLocale(strPortalCompanyId);
      // Locale
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      GmResourceBundleBean rbCompanyBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
      strHidePhoneNo =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONSIGN.HIDE_PHONE_NO"));
      
      HashMap hmITCompanyAddress = GmCommonClass.fetchCompanyAddress(strPortalCompanyId, "");
      String strITCountryCode = GmCommonClass.parseNull((String) hmITCompanyAddress.get("COUNTRYCODE"));     // added strITCountryCode need value if the company is ITALY  - PMT-42950
      boolean blEditConsignment = false;
      double dbConsignmentStatus = 0;
      int intVerifyFlag = 0;
      String strStrutsPageURL = "";
      String strRFS = "";
      ArrayList alReturn = new ArrayList();
      ArrayList alDataList = new ArrayList();
      ArrayList alPopulatedCartDtls = new ArrayList();
      strStatFlag = GmCommonClass.parseNull(request.getParameter("staInputs"));
      strVeriFlag = GmCommonClass.parseNull(request.getParameter("veriFlag"));
      strSetId = GmCommonClass.parseNull(request.getParameter("setID"));
      strConsignId = GmCommonClass.parseNull(request.getParameter("consignID"));
      strInitiatedFrom = GmCommonClass.parseNull(request.getParameter("initiatedFrom"));
      strDemandSheetMonthID = GmCommonClass.parseNull(request.getParameter("demandSheetMonthID"));
      String strCountryId = GmCommonClass.parseNull(request.getParameter("countryid"));
      String strSessCurrSymbol = !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol")) : "";
      String strSetRbObjectFl =
    	        GmCommonClass
    	            .parseNull(gmResourceBundleBean.getProperty("INVOICE.ADD_RESOURCE_BUNDLE_OBJ"));
      String strSessApplDtFmt = tmpGmDataStoreVO.getCmpdfmt();
      String strCountryCode =
          GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("COUNTRYCODE"));
      String strLoanerPaperworkFlag =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER_PAPERWORK_FLAG"));
      String strBarCodeFl =
          GmCommonClass
              .parseNull(gmResourceBundleBean.getProperty("PACK_SLIP.BARCIDE_REQUIRED_FL"));
      String strRAPaperworkFlag =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("RA_PAPERWORK_FLAG"));
       //String year = GmCalenderOperations.getCurrentDate("yyyy");
	   HashMap hmReturnData =
		          GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("INVRPTDATA"));
				   HashMap hmOrderDetails =
		          GmCommonClass.parseNullHashMap((HashMap) hmReturnData.get("HMORDERDETAILS"));
      HashMap hmValues = new HashMap();
      hmValues.put("SFL", strStatFlag);
      hmValues.put("VFL", strVeriFlag);
      hmValues.put("SETID", strSetId);
      hmValues.put("CONID", strConsignId);

      alAttrib = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CNATTB"));
      request.setAttribute("ALATTB", alAttrib);
      /* This will move the request to common pool in both DB (Prod DB & Order Planning DB) */
      if (strAction.equals("moveToCommonPool")) {
        strRequestId = GmCommonClass.parseNull(request.getParameter("hRequestId"));
        gmRequestTransBean.moveReqToCommonPool(strRequestId, strDemandSheetMonthID, strUserId);
        strAction = "EditLoad";
      }
      if (strAction.equals("Load")) {
        alResult = gmOper.getSetBuildDashboard(hmValues);
        hmReturn.put("INISETS", alResult);
        // hmReturn = gmOper.reportDashboard("1");
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("EditLoad")) {
        /*
         * strConsignId = (String) session.getAttribute("strSessConsignId"); if (strConsignId ==
         * null) {
         */
        strConsignId =
            request.getParameter("hId") == null ? "" : (String) request.getParameter("hId");
        // }
        String strSelectedAction = GmCommonClass.parseZero(request.getParameter("hCboAction"));
        log.debug("GmConsignSetServlet strSelectedAction --> " + strSelectedAction);
        strMode =
            request.getParameter("hMode") == null ? "" : (String) request.getParameter("hMode");
        strRequestId = GmCommonClass.parseNull(request.getParameter("hRequestId"));
        if (!strRequestId.equals("")) {
          strConsignId =
              GmCommonClass.parseNull(gmRequestReportBean.fetchConsignmentIdFromMR(strRequestId));
          log.debug("GmConsignSetServlet Consignment Id " + strConsignId + " From MR " + strRequestId);
        } else {
          strRequestId = gmRequestReportBean.fetchRequestId(strConsignId);
        }
        gmRequestReportBean.validateRequest(strRequestId, "ShipOut");
        log.debug(" GmConsignSetServlet out Consignment Id " + strConsignId + " From MR " + strRequestId);

        HashMap hmHeader = gmRequestReportBean.fetchRequestHeader(strRequestId);
        dbConsignmentStatus =
            Double.parseDouble(GmCommonClass.parseZero((String) hmHeader.get("STATUSFLAG")));
        strReqStatusFlag = GmCommonClass.parseNull((String) hmHeader.get("REQUESTSTATUSFLAG"));
        intVerifyFlag =
            Integer.parseInt(GmCommonClass.parseZero((String) hmHeader.get("VERIFYFLAG")));
        strSetID = GmCommonClass.parseNull((String) hmHeader.get("SETID"));
        strDSheetName = GmCommonClass.parseZero((String) hmHeader.get("REQUESTTXNID"));
        strReqSrc = GmCommonClass.parseNull((String) hmHeader.get("REQUESTSOURCEID"));

        strPartyId =  !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId")) : "";
        hmPartyAccess =
            GmCommonClass.parseNullHashMap((new GmAccessControlBean(tmpGmDataStoreVO))
                .getAccessPermissions(strPartyId, "MOVETOCOMMONPOOL"));
        strCmnPoolAcs = GmCommonClass.parseNull((String) hmPartyAccess.get("UPDFL"));

        request.setAttribute("initiatedFrom", strInitiatedFrom);
        request.setAttribute("demandSheetMonthID", strDemandSheetMonthID);

        if (strCmnPoolAcs.equals("Y") && strInitiatedFrom.equals("OP")) {
          request.setAttribute("SHW_COMPOOL_BTN", "YES");
          if (dbConsignmentStatus <= 2 && !strSetID.equals("") && !strDSheetName.equals("0") // sheet
                                                                                             // name
                                                                                             // is
                                                                                             // getting
                                                                                             // from
                                                                                             // function
                                                                                             // for
                                                                                             // no
                                                                                             // data
                                                                                             // it
                                                                                             // returns
                                                                                             // zero.
              && strReqSrc.equals("50616") && !strReqStatusFlag.equals("10")) { // 50616 - Order
                                                                                // planning source
            request.setAttribute("ENABLE_COMPOOL_BTN", "YES");
          }
        }

        if (dbConsignmentStatus <= 2 && intVerifyFlag < 1) {
          blEditConsignment = true;
          strStrutsPageURL = strOperPath.concat("/GmRequestEdit.jsp");
          gotoStrutsPage(strStrutsPageURL, request, response);
        }

        ltBackOrderReport = gmLogisticsBean.fetchBackOrderReport(strRequestId);

        if (!strConsignId.equals("")) {
          hmResult = gmOper.loadSavedSetMaster(strConsignId);
          hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        }
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);

        HashMap hmConsign = new HashMap();
        hmConsign.put("CONSIGNMENTID", strConsignId);
        hmConsign.put("SCREENTYPE", "BuiltSets");
        hmConsign.put("SELECTEDACTION", strSelectedAction);
        // The following code added for Getting the Distributor, Sales Rep and Employee List based
        // on the PLANT too if the Selected Company is EDC
        
        String strCompanyPlant =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "BUILTSETS_DIST_CON"));
        if (strCompanyPlant.equals("Plant")) {
          hmConsign.put("DISTFILTER", "COMPANY-PLANT");
          hmConsign.put("REPFILTER", "COMPANY-PLANT");
        }
        // for Other Values
        hmResult = gmCust.loadSetConsignLists(hmConsign);
        // to set the type based on selected process.
        ArrayList alType = new ArrayList();
        if (strSelectedAction.equals("PROCCSG")) {
          alType = gmCommon.getCodeList("CONTY", "102925", tmpGmDataStoreVO); // 102925 - Master
                                                                                // Process
          hmResult.put("CONTYPE", alType);

        } else if (strSelectedAction.equals("BSTR")) {
          // alType = gmCommon.getCodeList("CONTY", "102926", tmpGmDataStoreVO); // 102926 -
          // Master

          alType = gmCommon.getCodeList("CONTY", "26240144", tmpGmDataStoreVO); // Return to Owner
                                                                                  // Company
          hmResult.put("CONTYPE", alType);
        } else {
          alType = gmCommon.getCodeList("CONTY", "102926", tmpGmDataStoreVO); // 102926 - Master
          hmResult.put("CONTYPE", alType);
        }
        hmReturn.put("SETCONLISTS", hmResult);

        request.setAttribute("BACKORDERLIST", ltBackOrderReport);
        request.setAttribute("REQUESTID", strRequestId);
        request.setAttribute("CONSIGNMENTID", strConsignId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hMode", strMode);
        request.setAttribute("hSelectedAction", strSelectedAction);

        strRFS = GmCommonClass.parseNull(request.getParameter("refName"));
        if (!strRFS.equals("RFS")) {
          request.setAttribute("RE_FORWARD", "gmSetProcessConsign");
        } else {
          request.setAttribute("RE_FORWARD", "gmSetConsign");
          request.setAttribute("names", GmCommonClass.parseNull(request.getParameter("names")));
          request.setAttribute("shipTo", GmCommonClass.parseNull(request.getParameter("shipTo")));
          request.setAttribute("addressid",
              GmCommonClass.parseNull(request.getParameter("addressid")));
        }
        request.setAttribute("RE_TXN", "50920");
        request.setAttribute("txnStatus", "PROCESS");
        strDispatchTo = "/gmRuleEngine.do";

        // strDispatchTo = strCustPath.concat("/GmSetConsignSetup.jsp");
      } else if (strAction.equals("Save")) {
        String strTxn = "";
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        strRequestId = GmCommonClass.parseNull(request.getParameter("hRequestId"));
        strType = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
        strPurpose = GmCommonClass.parseZero(request.getParameter("Cbo_Purpose"));
        strDistribAccId = GmCommonClass.parseNull(request.getParameter("Cbo_BillTo"));
        strAccId = GmCommonClass.parseZero(request.getParameter("Cbo_AccId"));
        strSalesRepId = GmCommonClass.parseZero(request.getParameter("Cbo_SalesRepId"));
        strLinkReqId = GmCommonClass.parseNull(request.getParameter("Txt_LoanerReqId"));

        // strShipTo = GmCommonClass.parseNull((String)
        // request.getParameter("Cbo_ShipTo"));
        // strShipToId = GmCommonClass.parseZero((String)
        // request.getParameter("Cbo_Values"));
        strFinalComments = GmCommonClass.parseNull(request.getParameter("Txt_Comments"));
        strShipFl = request.getParameter("Chk_ShipFlag") == null ? "0" : "1";
        log.debug("GmConsignSetServlet strType " + strType);
        hmParam.putAll(loadShipReqParams(request));
        // loadShipReqParams(request, hmParam);
        hmParam.put("TYPE", strType);
        hmParam.put("PURPOSE", strPurpose);
        hmParam.put("DISTACCID", strDistribAccId);
        hmParam.put("ACCID", strAccId);
        hmParam.put("SALESREPID", strSalesRepId);
        // hmParam.put("SHIPTO", strShipTo);
        // hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("COMMENTS", strFinalComments);
        hmParam.put("SHIPFL", strShipFl);
        hmParam.put("REFID", strRequestId);
        hmParam.put("TXNID", strConsignId);
        hmParam.put("SOURCE", "50184");
        hmParam.put("USERID", strUserId);
        hmParam.put("LINKREQID", strLinkReqId);
        log.debug("REFID " + strRequestId);
        hmReturn = gmCust.updateSetConsign(strConsignId, hmParam, strUserId);

        // call to void product_request_det_id.
        if (!strLinkReqId.equals("")) {
          HashMap hmVoidPdtId = new HashMap();
          hmVoidPdtId.put("TXNID", strLinkReqId);
          hmVoidPdtId.put("TXNNAME", "");
          hmVoidPdtId.put("CANCELREASON", "90766"); //
          hmVoidPdtId.put("CANCELTYPE", "VDLON");
          hmVoidPdtId.put("COMMENTS", strFinalComments);
          hmVoidPdtId.put("USERID", strUserId);
          hmVoidPdtId.put("CANCELRETURNVAL", "");
          hmVoidPdtId.put("SHOWSUCCESSMSG", "N");
          hmVoidPdtId.put("JNDI", "");
          gmCommonCancel.UpdateCancel(hmVoidPdtId);

          String strComments = "Consignment " + strConsignId + " in place of Loaner";
          gmCommonBean.saveLog(strLinkReqId, strComments, strUserId, "1248");
        }

        gmCommonBean.loadActiveLoanerSets();// to load list of active loaner setids
        alReturn =
            GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("CONSEQUENCES"));

        log.debug("GmConsignSetServlet alReturn.................................>>>>>>>>>>>>>>>>>>>>>>>>>>>> "
            + alReturn);
        if (alReturn.size() > 0) {
          gmRuleEngine.sendRFSEmail(strRequestId, alReturn, "SET_Built");
        }

        request.setAttribute("MODE", "Save");
        request.setAttribute("PARAM", hmParam);
        // For Load
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        // for Other Values
        HashMap hmConsign = new HashMap();
        hmConsign.put("CONSIGNMENTID", strConsignId);
        hmResult = gmCust.loadSetConsignLists(hmConsign);
        hmReturn.put("SETCONLISTS", hmResult);
        // As the hmReturn is reset above, the shipping values should be set back in the hashmap.
        hmReturn.put("DEFAULTSHIPMODECARRVAL", hmruleShipVal);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");


        strDispatchTo = strCustPath.concat("/GmSetConsignSetup.jsp");
      } else if (strAction.equals("PrintVersion") || strAction.equals("PackVersion")
          || strAction.equals("PrintVersionLetter") || strAction.equals("PrintDummyVerify")) {
        GmAccountingBean gmAccountingBean = new GmAccountingBean(tmpGmDataStoreVO);
        GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean(tmpGmDataStoreVO);
        String strRefId = "";
        String strRMAId = "";

        // String strAdress =
        // "/GmConsignSetServlet?hAction=PrintVersion&hDeptID="+strDeptId+"&hInvoiceId="+strInv;
    
    
    	String strPrintPurposelbl =GmCommonClass.parseNull(request.getParameter("printPurpose"));
    	String strIntrasitPackSlip =GmCommonClass.parseNull(request.getParameter("strIntrasitPackSlip"));
        request.setAttribute("printPurpose",strPrintPurposelbl);
        request.setAttribute("INTRANSIT_PACK_SLIP",strIntrasitPackSlip);
        String strInvoiceId = GmCommonClass.parseNull(request.getParameter("hInvoiceId"));
        String strInvoiceType = GmCommonClass.parseNull(request.getParameter("hInvoiceType"));
        String strRuleDomain =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PACK_SLIP.DOMAINDB"));
        String strPackHeader =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PACK_SLIP.FDA_REGIST_VAL"));
        String strPackFooter =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PACK_SLIP.FDA_FOOTER_VAL"));
        //PC-4767 - adjustment in ous ditributor invoice template
        String strInvoiceICSNotes =
                GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.ICS_NOTES"));
        request.setAttribute("DOMAINDB", strRuleDomain);
        request.setAttribute("PACKHEADER", strPackHeader);
        request.setAttribute("PACKFOOTER", strPackFooter);
        log.debug("GmConsignSetServlet strInvoiceId " + strInvoiceId);
        log.debug("GmConsignSetServlet strInvoiceType " + strInvoiceType);
        String strDeptId = GmCommonClass.parseNull(request.getParameter("hDeptID"));
        String strPrintType = "PROFORMA";
        String strPkSlpHdr = "";
        String strDistId = "";
        String strApplnDateFmt = tmpGmDataStoreVO.getCmpdfmt();
        if (!strInvoiceId.equals("")) {
          strRefId = gmAccountingBean.fetchConsignmentIdFromInvoice(strInvoiceId);
          strPrintType = "COMMERCIAL";
        } else {
          strRefId =
              request.getParameter("hId") == null ? "" : (String) request.getParameter("hId");
        }
        strOrdId = GmCommonClass.parseNull(gmInvoiceBean.getInvoiceOrder(strInvoiceId));
        log.debug("GmConsignSetServlet strOrdId "
        		+ strOrdId);      
        // regular invoice ie CSG
        if (strInvoiceType.equals("50200") || strInvoiceId.equals("")) {
          strConsignId = strRefId;
          HashMap hmRefDetails = new HashMap();

          hmParam.put("TXNID", strConsignId);
          hmParam.put("ORDID", strOrdId);
          hmParam.put("USERID", strUserId);
          hmParam.put("COUNTRYID", strCountryId);
          hmParam.put("DATASTOREVO", tmpGmDataStoreVO);

          GmConsignmentPrintInterface gmConsignmentPrintInterface =
              (GmConsignmentPrintInterface) GmCommonClass.getSpringBeanClass(
                  "xml/Consignment_Print_Beans.xml", strPortalCompanyId);
          hmRefDetails = gmConsignmentPrintInterface.fetchConsignmentDtls(hmParam);                               
          log.debug("GmConsignSetServlet hmRefDetails "+hmRefDetails);
          strRequestId = GmCommonClass.parseNull((String) hmRefDetails.get("REQUESTID"));
          log.debug(" GmConsignSetServlet out Consignment Id " + strConsignId + " GmConsignSetServlet From MR " + strRequestId);
          ltBackOrderReport = gmLogisticsBean.fetchBackOrderReport(strRequestId);
          
          if(strOrdId.equals("")){
        	  hmAtbData = gmOper.loadConsignAttribute(strConsignId);
          }
          hmCompAtbData = gmAR.loadInvoiceAttribute("INVOICE_ATTRIB");
          ltChildRequest = gmLogisticsBean.fetchChildRequest(strRequestId);
          hmResult = GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNDETAILS"));
				          
				          
          String strCompDateFmt = GmCommonClass.parseNull((String) hmResult.get("CMPDFMT"));
          String strUpdDate =
              GmCommonClass.getStringFromDate((java.util.Date) hmResult.get("UDATE"),
                  strCompDateFmt);
          strDistId = GmCommonClass.parseNull((String) hmResult.get("DISTID"));
          strTransType = GmCommonClass.parseNull((String) hmResult.get("TYPE"));
				          
	      String strhospAccID = GmCommonClass.parseNull((String) hmResult.get("SHIPACCNTID"));  // get Accid and ConsignAccID for PMT-42950  
	      String strConsignAccID  = GmCommonClass.parseNull((String) hmResult.get("CONSIGNACCID"));
				         
				          
	      hmResult.put("INVICSNOTES", strInvoiceICSNotes);
          // hmReturn.put("SETDETAILS", hmResult);
          hmResult.put("SESSIONDTFMT", tmpGmDataStoreVO.getCmpdfmt());
          hmResult.put("UDATE", strUpdDate);
          log.debug("GmConsignSetServlet  details of CSG " + hmResult);
          hmReturn =GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNCHILDDETAILS"));
         
          log.debug("GmConsignSetServlet  "+hmResult.toString());

          alDataList = (ArrayList) hmReturn.get("SETLOAD");
         //To Fetch the cart details without usage part code-PMT-32393
          alPopulatedCartDtls  = gmDOReportBean.fetchPopulatedCartDetails(alDataList, strConsignId, "50181"); //50181-Consignment
          request.setAttribute("alPopulatedCartDtls", alPopulatedCartDtls);
          request.setAttribute("CHILDREQUESTLIST", ltChildRequest);
          request.setAttribute("BACKORDERLIST", ltBackOrderReport);// using
          // SETLOAD
          // instead
          // of
          // this
          request.setAttribute("ORDER_ATB", hmAtbData);
          request.setAttribute("COMP_ATB", hmCompAtbData);
          request.setAttribute("REQUESTID", strRequestId);
          request.setAttribute("CONSIGNMENTID", strConsignId);
	      request.setAttribute("HOSPACCID", strhospAccID);
          request.setAttribute("CONSIGNACCID", strConsignAccID);			         
				          
				          

        } else if (strInvoiceType.equals("50202")) {
          strRMAId = strRefId;
          GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(tmpGmDataStoreVO);

          HashMap hmRefDetails = gmProcessReturnsBean.loadCreditReport(strRMAId, "PRINT");
          hmResult = gmICTSummaryBean.mapAlias((HashMap) hmRefDetails.get("RADETAILS"));
          hmReturn.put("SETLOAD", hmRefDetails.get("RAITEMDETAILS"));
          request.setAttribute("CONSIGNMENTID", strRMAId);
        }
        String strPackJasNm =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CON.CONSGNPACKJAS"));
        // BBA-41 Dynamic Class Loading.
        String strClassName =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CON.CONSIGNPACKSLIP"));
        if (!strClassName.equals("")) {
          GmConsignPackSLipInterface consignpackSlipClient =
              GmConsignPackSlipInstance.getInstance(strClassName);
          hmSetMaster = consignpackSlipClient.loadSavedSetMaster(strConsignId);
          alDataList = GmCommonClass.parseNullArrayList((ArrayList) hmSetMaster.get("SETLOAD"));
        }
        if (strAction.equals("PackVersion") || strAction.equals("PrintVersionLetter")) {
          HashMap hmConsignDetail = new HashMap();
          hmConsignDetail = gmCust.loadConsignDetails(strConsignId, strUserId);
          String strRaComments = gmReturnBean.fetchCNPackslipComment(strConsignId);
                        
          //To check sterile part in order id for Company id 1000-PMT#48419
            strSterileExpDateRule = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "SHOW_STERILE_EXP_FL"));
           
            if(strSterileExpDateRule.equals("Y"))
            {
            	alSterilePartList = gmLotExpiryBean.fetchSterileParConsignment(strConsignId);
            }
             
          strGSTStartFl = gmDOBean.getGSTStartFlag(strConsignId,"1221");
          hmReturn.put("RACOMMENTS", strRaComments);
          hmConsignDetail.put("RACOMMENTS", strRaComments);
          hmReturn.put("SETDETAILS", hmConsignDetail);
          hmReturn.put("ALSTERILEPARTLIST", alSterilePartList);
          
          String strRAComments = "";
          strRAComments = GmCommonClass.parseNull((String)getTransferLog(strConsignId));
          if(!strRAComments.equals("")){
              hmReturn.put("RACOMMENTS", strRAComments);
              hmConsignDetail.put("RACOMMENTS", strRAComments); 
          }

          strPkSlpHdr = GmCommonClass.getRuleValue(strDistId, "PACKSLIP/SHIPLIST");
          strPackJasNm =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CON.CONSGNPACKJAS"));
          if (strTransType.equals("9110")) {
            strPkSlpHdr = strTransType;
          }
          hmConsignDetail.put("JASPERNAME", strPackJasNm);
          String strCompDateFmt = GmCommonClass.parseNull((String) hmConsignDetail.get("CMPDFMT"));
          String strUpdDate =
              GmCommonClass.getStringFromDate((java.util.Date) hmConsignDetail.get("UDATE"),
                  strCompDateFmt);
          hmConsignDetail.put("UDATE", strUpdDate);
          log.debug("GmConsignSetServlet strPkSlpHdr " + strPkSlpHdr);
          request.setAttribute("PackSlipHeader", strPkSlpHdr);
          request.setAttribute("hmResult", hmConsignDetail);
          request.setAttribute("GSTStartFl",strGSTStartFl);     

          strVersionForward =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CON.COSGNPRINTVER"));



          if (strLoanerPaperworkFlag.equals("YES") && strAction.equals("PackVersion")) {
            strVersionForward = "/GmPackSlipJapan.jsp";
          }
        }

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hmReturn1", hmSetMaster);

        if (strAction.equals("PrintVersion") || strAction.equals("PrintVersionLetter")) {
          // strDispatchTo =
          // strCustPath.concat("/GmSetConsignPrint.jsp");
        	
          //PMT-40968   Passes the consignment Id & gets RA packslip comment
      	  if (strRAPaperworkFlag.equals("Y")) {
          String strRaComments = gmReturnBean.fetchCNPackslipComment(strConsignId);
          hmResult.put("RACOMMENTS", strRaComments);
          }
      	
          GmJasperReport gmJasperReport = new GmJasperReport();
          gmJasperReport.setRequest(request);
          gmJasperReport.setResponse(response);

          hmResult.put("SUBCHILDREQUEST", ltChildRequest);
          //hmResult.put("SUBREPORT_DIR", gmJasperReport.getJasperRealLocation());
         // hmResult.put("SUBREPORT_DIR",GmCommonClass.parseNull(GmCommonClass.getString("GMJASPERLOCATION")));
          hmResult.put(
    	          "SUBREPORT_DIR",
    	          request.getSession().getServletContext()
    	              .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
    	              + "\\");
          hmResult.put("SETLOAD", hmReturn.get("SETLOAD"));
          // hmResult.put("SETDETAILS", (HashMap)
          // hmReturn.get("SETDETAILS"));
          String strRAComments = "";
          strRAComments = GmCommonClass.parseNull((String)getTransferLog(strConsignId));
      	  hmResult.put("ICTNOTES", strRAComments);

          String partyId = GmCommonClass.parseNull((String) hmResult.get("PARTYID"));
          String strDistType = GmCommonClass.parseNull((String) hmResult.get("DISTTYPE"));
          String strCompanyID = GmCommonClass.parseNull((String) hmResult.get("COMPANYID"));
          String strShipCountryID = GmCommonClass.parseNull((String) hmResult.get("SHIPCOUNTRY"));
          String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
          String strDivisionId = GmCommonClass.parseNull((String) hmResult.get("DIVISION_ID"));
          log.debug("GmConsignSetServlet hmResult"+hmResult);
          log.debug(" GmConsignSetServlet My division id --> " + strDivisionId);
          strDivisionId = strDivisionId.equals("") ? "2000" : strDivisionId;
          HashMap hmCompanyAddress =
              GmCommonClass.fetchCompanyAddress(strPortalCompanyId, strDivisionId);
          String strCompanyName =
              GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
          String strCompanyAddress =
              GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
          if (!strHidePhoneNo.equals("YES")) {
            strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
          }
          String strIBAN = "";
          String strSwiftCode = "";
          String strBank = "";
          String strRptName = "";
          String strNativeCurrency = "";
          String strConsignRptName = "/GmConsignPrint.jasper";
          strVersionForward = "";
          strConsignRptName =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CON.CONSGNPACKJAS"));
          strVersionForward =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CON.COSGNPACKSLIP"));
          hmResult.put("LOGOIMAGE", strImagePath + (String) hmCompanyAddress.get("LOGO") + ".gif");
          hmResult.put("COMPNAME", strCompanyName);
          hmResult.put("COMPADDRESS", strCompanyAddress + "\n" + strCompanyPhone);
          hmResult.put("GMCOMPANYNAME", strCompanyName);
          hmResult.put("GMCOMPANYADDRESS", strCompanyAddress + " / " + strCompanyPhone);
          // to get the transactions company id details
          String strTransCompanyId = GmCommonClass.parseNull((String) hmResult.get("COMPANYCD"));
          String strTransCompanyLocale = GmCommonClass.getCompanyLocale(strTransCompanyId);
          GmResourceBundleBean gmTransResourceBundleBean =
              GmCommonClass.getResourceBundleBean("properties.Paperwork", strTransCompanyLocale);
          // Header label getting based on transaction id
          hmResult.put("CONSIGNMENTLBL",
              GmCommonClass.parseNull(gmTransResourceBundleBean.getProperty("CONSIGNMENT")));

          hmResult.put("CONSIGNEDTOLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONSIGNED_TO")));
          hmResult.put("SHIPTOLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_TO")));
          hmResult
              .put("DATELBL", GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DATE")));
          hmResult.put("CONSIGNNOLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONSIGN_NO")));
          hmResult.put("CONSIGNEDBYLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONSIGNED_BY")));
          hmResult.put("SHIPMODELBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_MODE")));
          hmResult.put("SHIPVIALBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_VIA")));
          hmResult.put("TRACKINGNOLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("TRACKING")));
          hmResult.put("NOTESLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("NOTES")));
          hmResult.put("PARTNMLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PART")));
          hmResult.put("DESCLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DESCRIPTION")));
          hmResult.put("QTYLBL", GmCommonClass.parseNull(gmResourceBundleBean.getProperty("QTY")));
          // hmResult.put("EXPDATELBL",
          hmResult.put("EXPDATELBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("EXP_DATE")));
          hmResult.put("EXPDTENABLE",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PACK_SLIP.EXPDTENABLE")));
          hmResult.put("FOOTERMSG",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PACKS_LIP.FOOTERMSG")));
          hmResult.put("CONTROLLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONTROL_NUMBER")));
          hmResult.put("PRICELBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PRICE")));
          hmResult.put("AMOUNTLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AMOUNT")));
          hmResult.put("SIGNINLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SIGN_IN_LBL")));
          hmResult.put("BACKODRLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("BACK_ODR_LBL")));
          hmResult.put("STATUSLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("STATUS_LBL")));
          hmResult.put("TOTALLBL",
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty("TOTAL_LBL")));
          hmResult.put("MANFDATELBL",
                  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("MANF_DATE")));
          
          hmResult.put("COMPANYDFMT", tmpGmDataStoreVO.getCmpdfmt());
          if (strCountryCode.equals("en")) {
            hmResult.put("SIGNINLBL", GmCommonClass.parseNull((String) hmResult.get("ICTNOTES")));
          }
          log.debug("GmConsignSetServlet partyId..."+partyId);
          log.debug("GmConsignSetServlet strDistType..."+strDistType);
          log.debug(" GmConsignSetServlet strInvoiceType..."+strInvoiceType);
          if ((partyId.equals("") || strDistType.equals("26240143"))
              && !strInvoiceType.equals("50202"))// report
          // will
          // send
          // to
          // American
          // user
          // 26240143: Consignment return to Owner Company
          {
            strNativeCurrency = strSessCurrSymbol;

            hmResult.put("VARIABLECURRENCY", strNativeCurrency);

            hmResult.put("ADDWATERMARK", "");
            hmResult.put("HEADERADDRESS", strCompanyName + "," + strCompanyAddress + ","
                + strCompanyPhone);
            // For BBA, for International Shipments we need to Load Commercial Invoice
            if (!strRuleDomain.equals("") && !strShipCountryID.equals("1101")
                && !strShipCountryID.equals("")) {
              String gmAddress =
                  GmCommonClass.parseNull((String) hmCompanyAddress.get("GMADDRESS"));
              hmResult.put("GMADDRESS", strCompanyName + "/" + strCompanyAddress + "/"
                  + strCompanyPhone);
              hmResult.put("USERID", strUserId);
              if (strLoanerPaperworkFlag.equals("YES")) {
                strConsignRptName = "/GmJPConsignDetailSheet.jasper";
                hmResult.put("HEADERADDRESS", strCompanyName + "/" + strCompanyAddress);
                hmResult.put("GMCOMPANYFAX",
                    GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("GMCOMPANYFAX")));
                hmResult.put("GMCOMPANYPHONE",
                    GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("GMCOMPANYPHONE")));

              } else {
                hmResult.put("HEADERADDRESS", strCompanyName + "," + strCompanyAddress + ","
                    + strCompanyPhone);
                strConsignRptName =
                    GmCommonClass.parseNull(gmResourceBundleBean
                        .getProperty("CON.CONSGNPACKJASICS"));
              }
            }
            gmJasperReport.setJasperReportName(strConsignRptName);
            hmResult.put("JASPERNAME", strConsignRptName);
            hmResult.put("APPLNDATEFMT", tmpGmDataStoreVO.getCmpdfmt());
            log.debug("GmConsignSetServlet hmResult11..."+hmResult);
          } else {
            String flag =
                request.getParameter("flag") == null ? "false" : (String) request
                    .getParameter("flag");
            log.debug("GmConsignSetServlet flag  " + flag);
            HashMap hmEuSectonData = new HashMap();
            GmPartyBean gmPartyBean = new GmPartyBean(tmpGmDataStoreVO);
            hmResult.put("USERID", "");
            hmEuSectonData = (HashMap) gmPartyBean.fetchPartyRuleParam(partyId, "ICPTY", "PRINT");

            // Add For ICT change
            strShipDt = GmCommonClass.parseNull(gmICTSummaryBean.fetchConsignShipDt(strConsignId));
            strruleEnforceDate =
                GmCommonClass.parseNull(GmCommonClass.getRuleValue("50001", "ICTCHANGE"));
            strAdd = GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("GMADDICT"));
            if (strShipDt.equals("")) {
              strShipDt = strruleEnforceDate;
            }
            if (!strDistType.equals("70105")) { // ICS
              if (GmCommonClass.getStringToDate(strShipDt, strSessApplDtFmt).after(
                  GmCommonClass.getStringToDate(strruleEnforceDate, "MM/dd/yyyy"))) {

                if (hmEuSectonData.containsKey("HEADERADDRESS")
                    || hmEuSectonData.containsKey("CURRENCY")) {
                  hmEuSectonData.remove("HEADERADDRESS");
                  // hmEuSectonData.remove("CURRENCY");
                  hmEuSectonData.put("HEADERADDRESS", strAdd);
                  // hmEuSectonData.put("CURRENCY", "USD");
                }
              }
              log.debug("ICT CHANGE ENFORCED DATE:" + strruleEnforceDate);
            }// End ICT change

            log.debug(" hmEuSectonData " + hmEuSectonData);
            int intConsignStatus = Integer.parseInt((String) hmResult.get("CONSIGNSTATUS"));

            if (intConsignStatus < 4 && !strInvoiceType.equals("50202")) {
              hmResult.put("ADDWATERMARK", "Draft Proforma");
              if (strCountryCode.equals("en")) {
                hmResult.put("VARIABLECURRENCY", "USD");
              } else {
                hmResult.put("VARIABLECURRENCY", strSessCurrSymbol);
              }
            } else if (!strInvoiceType.equals("50202")) {
               log.debug("GmConsignSetServlet  strInvoiceType " + strInvoiceType);
              hmResult.put("VARIABLECURRENCY", hmEuSectonData.get("CURRENCY"));
              // /log.debug(" VARIABLECURRENCY " + hmEuSectonData.get("CURRENCY"));
              // if (partyId.equals("203") ||
              // strDeptId.equals("2006") ||
              // strDeptId.equals("2000"))

              
              if(strOrdId.equals("")){
            	  strInvoiceId =
                          GmCommonClass.parseNull(gmAccountingBean
                              .fetchInvoiceIdFromConsignmentId(strConsignId));
              }
              strCustPONum =
                  GmCommonClass.parseNull((gmAccountingBean.fetchCustomerPoNum(strInvoiceId)));

              if (strPrintType.equals("COMMERCIAL") || partyId.equals("203")) {

                /*
                 * //Below Condition add for ICT Changes if(strShipDt.equals("")){ strShipDt =
                 * strruleEnforceDate; } if(GmCommonClass.getStringToDate(strShipDt,
                 * "MM/dd/yyyy").after(GmCommonClass.getStringToDate(strruleEnforceDate,
                 * "MM/dd/yyyy"))) { hmResult.put("ADDWATERMARK", "Proforma Invoice");
                 * 
                 * }else{
                 */
                hmResult.put("ADDWATERMARK", "Commercial Invoice");
                hmResult.put("USERID", strUserId);
                // }//End ICT

                // if (strInvoiceId.equals("")){
                // strInvoiceId =
                // gmAccountingBean.fetchInvoiceIdFromConsignmentId(strConsignId)
                // ;
                // }
              } else {
                hmResult.put("ADDWATERMARK", "Proforma Invoice");
                hmResult.put("SHIPCOST", "0");
                hmResult.put("USERID", strUserId);
              }
              if (strDistType.equals("70105")) { // ICS
                if (hmResult.containsKey("ADDWATERMARK")) {
                  hmResult.remove("ADDWATERMARK");
                  // hmEuSectonData.remove("CURRENCY");
                  hmResult.put("ADDWATERMARK", "Invoice");
                  // hmEuSectonData.put("CURRENCY", "USD");
                }
                // to show banking details for all the ICS distributor (PMT-2841)
                if (strDistType.equals("70105")) { // 70105-Inter-Company Sale (ICS)
                  // 569 -- Dist id (Orthomedical SA)
                  // to fetch ICS account bank details
                  HashMap hmICSBankDtls = gmAccountingBean.fetchICSBankDetails("569", partyId);
                  // get the details from hash map
                  strIBAN = GmCommonClass.parseNull((String) hmICSBankDtls.get("IBAN"));
                  strSwiftCode = GmCommonClass.parseNull((String) hmICSBankDtls.get("SWIFTCODE"));
                  strBank = GmCommonClass.parseNull((String) hmICSBankDtls.get("BANK_ADD"));
                  strTermofsale =
                      GmCommonClass.parseNull((String) hmEuSectonData.get("TERMOFSALE"));
                  strTermofsaleId = gmCommon.getCodeNameFromCodeId(strTermofsale);
                  
                  String strCompAdd = GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("ICS.GMCOMPADD"));
                  if (!strCompAdd.equals("")) {
                	  hmEuSectonData.put("HEADERADDRESS",strCompAdd);
                  }
                  // to get the shipper id
                  String strImportShipperTax =
                      GmCommonClass.parseNull((String) hmICSBankDtls.get("IMPORT_SHIPPER_TAX"));
                  if (!strImportShipperTax.equals("")) {
                    hmEuSectonData.put("CONSIGNEETAXID", strImportShipperTax);
                  } // end if shipper tax

                }// end ICS dist type
                log.debug("strIBAN is " + strIBAN);
                hmEuSectonData.put("IBAN", strIBAN);
                hmEuSectonData.put("SWIFTCODE", strSwiftCode);
                hmEuSectonData.put("BANKADD", strBank);
              }

            }

            log.debug(" IC values from DB " + hmEuSectonData);
            if (strDistType.equals("70105")) { // ICS
            	strCompanyName =  GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("ICS.COMPNAME"));;
            	strCompanyAddress =  GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("ICS.COMPADDRESS"));;
            	 hmResult.put("GMADDRESS", strCompanyName + "\n" + strCompanyAddress);
            }else {
            	 String gmAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMADDRESS"));
                 hmResult.put("GMADDRESS", strCompanyName + "\n" + strCompanyAddress + "\n"
                     + strCompanyPhone);
            }
           

            String strTansactionNum = "";
            if (!strInvoiceId.equals("")) {
              strTansactionNum = strInvoiceId;
            } else {
              strTansactionNum = (String) hmResult.get("CID");
            }

            hmResult.put("TRANSACTIONUM", strInvoiceId);
            hmResult.put("CUSTPO", strCustPONum);
            hmResult.putAll(hmEuSectonData);
            hmResult.put("TERMOFSALE", strTermofsaleId);
            // to show banking details for all the ICS distributor (PMT-2841)
            strRptName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("569", "JASPERRPT"));
            if (!strDistType.equals("70105")) { // 70105-Inter-Company Sale (ICS)
              gmJasperReport.setJasperReportName("/GmEuropeConsignPrint.jasper");
              hmResult.put("JASPERNAME", "/GmEuropeConsignPrint.jasper");
            } else {
              gmJasperReport.setJasperReportName(strRptName);
              hmResult.put("JASPERNAME", strRptName);
            }


          }

          log.debug(" name " + gmJasperReport.getJasperReportName() + "  " + strRptName);
          if (strAction.equals("PrintVersion")) {
            strPrnPdf = GmCommonClass.parseNull((String) request.getAttribute("printPDF"));
            strFilePath = GmCommonClass.parseNull((String) request.getAttribute("filePath"));
            strInvType = GmCommonClass.parseNull((String) request.getAttribute("invoiceType"));
            log.debug("print pdf = " + strPrnPdf);
           // PC-3329 Invoice Word rtf Download - InterCompanySale
           if(type.equals("savePDFfile") || type.equals("saveRTFfile"))
            {
        	   HashMap hmAccountParamData = new HashMap();
               hmAccountParamData = gmARBatchBean.fetchAccountParameterDtls(strInvoiceId);
               //get the invoiced company
               String strCompanyCode =
                   GmCommonClass.parseNull((String) hmAccountParamData.get("COMP_CODE"));
               // get the invoiced year
               String year = GmCalenderOperations.getCurrentDate("yyyy");
            	String pdffileName =
            	          strInvoicePDF + strCompanyCode + "\\" + year + "\\" + strInvoiceId + ".pdf";
            	log.debug("GmConsignSetServlet pdffileName>> "+pdffileName);
            	gmJasperReport.setRequest(request);
            	gmJasperReport.setResponse(response);
                if (strSetRbObjectFl.equalsIgnoreCase("YES")) {
                  gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());
                }
                log.debug("GmConsignSetServlet hmResult>> "+hmResult);
            	gmJasperReport.setHmReportParameters(hmResult);
            	gmJasperReport.setReportDataList(null);
    			 if (type.equals("savePDFfile")) {
    	           gmJasperReport.exportJasperReportToPdf(pdffileName);
    	        } else {
    	          gmJasperReport.exportJasperReportToRTF();
    	        }

        		ObjectOutputStream sendStream = null;
        	      sendStream = new ObjectOutputStream(response.getOutputStream());
        	      sendStream.close();	
            }
            if (strPrnPdf.equals("pdfprint")) {
              final String FILEEXT = ".pdf";
              String fileName = strFilePath + strInvType + "_" + strInvoiceId + FILEEXT;

              gmJasperReport.setHmReportParameters(hmResult);
              if (!strRuleDomain.equals("")) {
                gmJasperReport.setReportDataList(alDataList);
              } else {
                gmJasperReport.setReportDataList(null);
              }
              // gmJasperReport.createJasperReport();
              gmJasperReport.exportJasperReportToPdf(fileName);
              strDispatchTo = "/prodmgmnt/GmProjectsDashboard.jsp";
              
              
            } else {
              request.setAttribute("hmResult", hmResult);
              request.setAttribute("hmReturn", hmResult);
              request.setAttribute("STRBARCODEFL", strBarCodeFl);

              strDispatchTo = strCustPath.concat(strVersionForward);
            }
            // return;
          } else {
            String strPrintVerOriginalCopy = "";
            String strPrintVerDuplocateCopy = "";
            String strCompanyLogo =
                GmCommonClass.parseNull(rbCompanyBundleBean.getProperty("COMP_LOGO"));
            hmResult.put("LOGO", strCompanyLogo);
            log.debug(" testing hmResult" + hmResult);
            request.setAttribute("hmResults", hmResult);

            // if(!strShipMode.equals("5033") && !strShipMode.equals("5034")){
            gmJasperReport.setPageHeight(900);
            gmJasperReport.setHmReportParameters(hmResult);
            if (!strRuleDomain.equals("")) {
              gmJasperReport.setReportDataList(alDataList);
            } else {
              gmJasperReport.setReportDataList(null);
            }
            gmJasperReport.setBlDisplayImage(true);

            if (strLoanerPaperworkFlag.equals("YES")) {
              strPrintVerOriginalCopy = gmJasperReport.getXHtmlReport();
            } else {
              strPrintVerOriginalCopy = gmJasperReport.getHtmlReport();
            }
            // }
            strShipTo = GmCommonClass.parseNull((String) hmResult.get("SHIPTO"));
            if (!strShipTo.equals("4121")) {
              // for duplicate copy
              gmJasperReport.setPageHeight(900);
              gmJasperReport.setHmReportParameters(hmResult);
              if (!strRuleDomain.equals("")) {
                gmJasperReport.setReportDataList(alDataList);
              } else {
                gmJasperReport.setReportDataList(null);
              }
              gmJasperReport.setBlDisplayImage(true);
              if (strLoanerPaperworkFlag.equals("YES")) {
                strPrintVerDuplocateCopy = gmJasperReport.getXHtmlReport();
              } else {
                strPrintVerDuplocateCopy = gmJasperReport.getHtmlReport();
              }
            }
            strShipToId =
                request.getParameter("hShipTo") == null ? "" : (String) request
                    .getParameter("hShipTo");
            hmReturn.put("SHIPTOID", strShipToId);
            stConsignId =
                request.getParameter("hId") == null ? "" : (String) request.getParameter("hId");
            hmResult = gmCust.loadConsignAckDetails(stConsignId);

            hmReturn.put("ACKDETAILS", hmResult);
            // to put the jasper code.
            hmReturn.put("PRINT_VER_ORIGINAL", strPrintVerOriginalCopy);
            hmReturn.put("PRINT_VER_DUPLICATE", strPrintVerDuplocateCopy);
            request.setAttribute("hmReturn", hmReturn);
            strPerforma  =request.getParameter("hstrPerforma") == null ? "" : (String) request
                    .getParameter("hstrPerforma");
           log.debug("strPerforma==>"+strPerforma); 	 
           hmReturn.put("INTRANSPERFROMA",strPerforma);
            strDispatchTo = strCustPath.concat("/GmConsignPrintVerLetter.jsp");
          }

        } else if (strAction.equals("PrintDummyVerify")) {
          request.setAttribute("printTitle", "Dummy Consignment");
          strDispatchTo = strCustPath.concat("/GmSetConsignPrint.jsp");
        } else {
          request.setAttribute("gmResourceBundleBean", gmResourceBundleBean);
          if(strITCountryCode.equals("it")){ //   if the company is ITALY  - PMT-42950
        	  strDispatchTo = strCustPath.concat("/GmITConsignPackslipPrint.jsp");
          }else{
          strDispatchTo = strCustPath.concat(strVersionForward);
          }
        }
      } else if (strAction.equals("Ack")) {
        strConsignId =
            request.getParameter("hId") == null ? "" : (String) request.getParameter("hId");

        hmResult = gmCust.loadConsignAckDetails(strConsignId);
        hmReturn.put("ACKDETAILS", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strCustPath.concat("/GmConsignAckPrint.jsp");
      } else if (strAction.equals("LoadSetShip") || strAction.equals("LoadSetDummy")) {
        // LoadSetShip is not used anymore-Part of the Portal framework
        // change.James May10,2007
        // this condition can be removed later.
        strConsignId = GmCommonClass.parseNull(request.getParameter("hId"));
        if (strAction.equals("LoadSetDummy")) {
          request.setAttribute("hMode", "DummyVerify");
        } else {
          request.setAttribute("hMode", "SHIP");
        }
        // strConsignId = (String)
        // session.getAttribute("strSessConsignId") == null ? "" :
        // (String) session.getAttribute("strSessConsignId");
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));

        // Adding the SetId to the return HashMap. This is done for
        // Rollback Set CSG.
        // The Rollback button would be displayed in GmConsignShip.jsp
        // only if the SetId is not null
        hmReturn.put("STRSETID", hmResult.get("SETID"));

        hmResult = gmCust.loadConsignDetails(strConsignId, strUserId);
        hmReturn.put("SETDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        hmResult = gmCust.loadShipping();
        request.setAttribute("hmShipLoad", hmResult);
        strDispatchTo = strCustPath.concat("/GmConsignShip.jsp");
      } else if (strAction.equals("SaveShip")) {// this code wont be called ever
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : (String) request
                .getParameter("hConsignId");
        strShipDate =
            request.getParameter("Txt_SDate") == null ? "" : (String) request
                .getParameter("Txt_SDate");
        strShipCarr =
            request.getParameter("Cbo_ShipCarr") == null ? "" : (String) request
                .getParameter("Cbo_ShipCarr");
        strShipMode =
            request.getParameter("Cbo_ShipMode") == null ? "" : (String) request
                .getParameter("Cbo_ShipMode");
        strTrack =
            request.getParameter("Txt_Track") == null ? "" : (String) request
                .getParameter("Txt_Track");
        strFreightAmt =
            request.getParameter("Txt_FreightAmt") == null ? "" : (String) request
                .getParameter("Txt_FreightAmt");
        strPONum =
            request.getParameter("Txt_PONum") == null ? "" : (String) request
                .getParameter("Txt_PONum");
        strMode =
            request.getParameter("hMode") == null ? "SHIP" : (String) request.getParameter("hMode");

        hmParam.put("CSGID", strConsignId);
        hmParam.put("SHIPDATE", strShipDate);
        hmParam.put("SHIPCARR", strShipCarr);
        hmParam.put("SHIPMODE", strShipMode);
        hmParam.put("TRACK#", strTrack);
        hmParam.put("FREIGHTAMT", strFreightAmt);
        hmParam.put("PONUM", strPONum);
        hmParam.put("MODE", strMode);
        hmParam.put("USERID", strUserId);

        gmCust.saveConsignShipDetails(hmParam);
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        log.debug("Message : " + hmResult.get("MSG"));
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmCust.loadConsignDetails(strConsignId, strUserId);
        hmReturn.put("SETDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strMode);

        hmResult = gmCust.loadShipping();
        request.setAttribute("hmShipLoad", hmResult);
        strDispatchTo = strCustPath.concat("/GmConsignShip.jsp");
      } else if (strAction.equals("LoadConsign")) {
        GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean(tmpGmDataStoreVO);
        strConsignId =
            request.getParameter("hId") == null ? "" : (String) request.getParameter("hId");
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmCust.loadConsignDetails(strConsignId, strUserId);
        hmReturn.put("SETDETAILS", hmResult);

        RowSetDynaClass ICTResult = gmICTSummaryBean.fetchICTSummary(strConsignId);
        strShipDt = GmCommonClass.parseNull(gmICTSummaryBean.fetchConsignShipDt(strConsignId));
        String strPkSlpNm =
            GmCommonClass.getRuleValue((String) hmResult.get("DISTID"), "PACKSLIP/SHIPLIST");
        request.setAttribute("ICTRESULT", ICTResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", "LOAD");
        request.setAttribute("ICTRULEDATE", strruleEnforceDate);
        request.setAttribute("SHIPDATE", strShipDt);
        request.setAttribute("PkSlpNm", strPkSlpNm);
        log.debug("ICTRULEDATE : " + strruleEnforceDate);
        log.debug("SHIPDATE : " + strShipDt);
        strDispatchTo = strCustPath.concat("/GmConsignShip.jsp");
      } else if (strAction.equals("Rollback")) {
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : (String) request
                .getParameter("hConsignId");
        hmResult = gmCust.rollConsignmentToInventory(strConsignId, strUserId);

        // For Load
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        // for Other Values
        HashMap hmConsign = new HashMap();
        hmConsign.put("CONSIGNMENTID", strConsignId);
        hmResult = gmCust.loadSetConsignLists(hmConsign);
        hmReturn.put("SETCONLISTS", hmResult);

        request.setAttribute("hAction", "EditLoad");
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", "ROLL");
        strDispatchTo = strCustPath.concat("/GmSetConsignSetup.jsp");
      }
      log.debug("String DispatchTo :" + strDispatchTo);
      if (blEditConsignment) {
        log.debug("strStrutsPageURL for consignment " + strStrutsPageURL);
        // gotoStrutsPage(strStrutsPageURL, request, response);
      } else {
        log.debug("strPrnPdf ====" + strPrnPdf);
        if (!strPrnPdf.equals("pdfprint") && type.equals("")) {
          dispatch(strDispatchTo, request, response);
        }
      }

    }// End of try
    catch (AppError e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      strDispatchTo = strComnPath + "/GmError.jsp";
      dispatch(strDispatchTo, request, response);

    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
  
 /**
   * getTransferLog - This method is used to fetch TFCN and TFRA paperwork comments 
   * @return String
   * @exception AppError
 **/  
 public String getTransferLog(String strConsignId) throws AppError{
	 GmCommonBean gmCommonBean = new GmCommonBean();
	 ArrayList AlLogReasons =GmCommonClass.parseNullArrayList((ArrayList)gmCommonBean.getLog(strConsignId, "26240618"));
     int intLog = 0;
     HashMap hmLog = new HashMap();
 	 String strCNComments="";
     if(!AlLogReasons.equals("")){  	
   		intLog = AlLogReasons.size();
   	 }
   	 if (intLog > 0) {		
   		for (int i=0;i<intLog;i++)
   		{	
   			hmLog = (HashMap)AlLogReasons.get(i);
   			strCNComments = (String) hmLog.get("COMMENTS");
   		}     	
   	}
   	 return strCNComments;
 }
}// End of GmConsignSetServlet
