/*****************************************************************************
 * File : GmOrderVoidServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmParentRepAccountBean;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;


public class GmOrderVoidServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }


  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmVoidOrder.jsp");

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strMode = request.getParameter("hMode");

      if (strMode == null) {
        strMode = (String) session.getAttribute("hMode");
        session.removeAttribute("hMode");
      }

      strMode = (strMode == null) ? "Load" : strMode;

      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      GmParentRepAccountBean gmParentRepAccountBean =
          new GmParentRepAccountBean(getGmDataStoreVO());
      GmAutoCompleteReportBean gmAutoCompleteReportBean =
          new GmAutoCompleteReportBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alCompCurrency = new ArrayList();
      HashMap hmLoadReturn = new HashMap();

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strAccId = "";
      String strAccountNm = "";
      String strRepId = "";
      String strOrderId = "";
      String strCustPO = "";
      String strTrack = "";
      String strTotal = "";
      Date dtShipFrmDt = null;
      Date dtShipTodt = null;
      Date dtFromDate = null;
      Date dtToDate = null;
      String strShipFrmDt = null;
      String strShipToDt = null;
      String strOrderBy = "";
      String strFormat = "";
      String strOrdId = "";
      String strAction = "";
      String strPartNums = "";
      String strFromDate = "";
      String strToDate = "";
      String strInvoiceId = "";
      String strParentOrdId = "";
      String strHoldFl = "";
      String strEnblUpdateLotNum = "";
      String strRuleComp = "";
      String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strParentFl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));
      HashMap hmParam = new HashMap();
      ArrayList alChooseAction = new ArrayList();
      String strAccCurrId = "";
      String strCompTxnCurrId = "";
      String strRepNm = "";
      String strOrderByNm = "";


      Calendar cal = new GregorianCalendar();
      int year = cal.get(Calendar.YEAR);
      int month = cal.get(Calendar.MONTH);
      int day = cal.get(Calendar.DATE);
      month = month + 1;
      String strMonth = "" + month;
      String strDay = "" + day;
      if (strMonth.length() == 1) {
        strMonth = "0".concat(strMonth);
      }
      if (strDay.length() == 1) {
        strDay = "0".concat(strDay);
      }
      String strYear = "" + year;
      // strFromDate = strMonth.concat("/").concat(strDay).concat("/").concat(strYear);
      // strToDate = strMonth.concat("/").concat(strDay).concat("/").concat(strYear);

      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      strFromDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
      strToDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);

      // dtFromDate = GmCalenderOperations.getDate(strFromDate);
      dtFromDate = GmCommonClass.getCurrentDate(strApplDateFmt, getGmDataStoreVO().getCmptzone());
      dtToDate = GmCommonClass.getCurrentDate(strApplDateFmt, getGmDataStoreVO().getCmptzone());
      strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
      log.debug(" Mode value is " + strMode + "::strOpt is ::" + strOpt);

      // Load Order By dropdown list which is Grouped by Cust Service, Accounting and Sales Reps
      // alReturn = gmCust.fetchEmployeeWithOrders();
      // hmReturn.put("ORDERBYLIST", alReturn);
      alChooseAction = gmAutoCompleteReportBean.getCodeList("MODACT", getGmDataStoreVO());
      hmReturn.put("ALCHOOSEACTION", alChooseAction);

      alCompCurrency = gmCompanyBean.fetchCompCurrMap();
      hmReturn.put("ALCOMPANYCURRENCY", alCompCurrency);

      HashMap hmCompCurr = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());

      strCompTxnCurrId = GmCommonClass.parseNull((String) hmCompCurr.get("TXNCURRENCYID"));


      strEnblUpdateLotNum =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERS", "ENABLE_UPDATE_LOTNUM"));
      // Get the company id from rules
      strRuleComp =
          GmCommonClass.parseNull((GmCommonClass.getRuleValueByCompany("PICSLIP", "EXCLTXN",
              getGmDataStoreVO().getCmpid())));
      if (strMode.equals("Load")) {
        hmParam.put("ORDERFRM", strFromDate);
        hmParam.put("ORDERTO", strToDate);
        hmParam.put("DTFORMAT", getGmDataStoreVO().getCmpdfmt());
        hmParam.put("FORMAT", strApplDateFmt);

        // from left link there is no account currency. So, it is default to Company txn currency
        hmParam.put("ACC_CURR_ID", strCompTxnCurrId);

        // alReturn = gmCust.reportAccountOrder(hmParam);
        hmParam.put("ORDERFRM", dtFromDate);
        hmParam.put("ORDERTO", dtToDate);
        // hmReturn.put("ACCOUNTORDERLIST", alReturn);
        request.setAttribute("EXCLCOMPNY", strRuleComp);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strMode);
        request.setAttribute("hmParam", hmParam);
        request.setAttribute("ENBL_UPDATE_LOTNUM", strEnblUpdateLotNum);
        strDispatchTo = strCustPath.concat("/GmOrderAction.jsp");
      } else if (strMode.equals("Reload")) {
        strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
        strRepId = GmCommonClass.parseNull(request.getParameter("Cbo_RepId"));
        strOrderId = GmCommonClass.parseNull(request.getParameter("Txt_OrdId"));
        strCustPO = GmCommonClass.parseNull(request.getParameter("Txt_CustPO"));
        strTrack = GmCommonClass.parseNull(request.getParameter("Txt_Track"));
        strTotal = GmCommonClass.parseNull(request.getParameter("Txt_Total"));
        strFromDate = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
        strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
        dtFromDate = getDateFromStr(request.getParameter("Txt_FromDate"));
        dtToDate = getDateFromStr(request.getParameter("Txt_ToDate"));
        dtShipFrmDt = getDateFromStr(request.getParameter("Txt_ShipFromDate"));
        dtShipTodt = getDateFromStr(request.getParameter("Txt_ShipToDate"));
        strShipFrmDt = GmCommonClass.parseNull(request.getParameter("Txt_ShipFromDate"));
        strShipToDt = GmCommonClass.parseNull(request.getParameter("Txt_ShipToDate"));
        strOrderBy = GmCommonClass.parseNull(request.getParameter("Cbo_OrderBy"));
        strInvoiceId = GmCommonClass.parseNull(request.getParameter("Txt_InvoiceId"));
        strParentOrdId = GmCommonClass.parseNull(request.getParameter("Txt_ParentOrd"));
        strHoldFl = GmCommonClass.parseNull(request.getParameter("Chk_HoldFl"));
        strAccCurrId = GmCommonClass.parseNull(request.getParameter("Cbo_AccCurrId"));
        strAccountNm = GmCommonClass.parseNull(request.getParameter("hAccNm"));
        strRepNm = GmCommonClass.parseNull(request.getParameter("hRepNm"));
        strOrderByNm = GmCommonClass.parseNull(request.getParameter("hOrderByNm"));


        hmParam.put("ACCID", strAccId);
        hmParam.put("REPID", strRepId);
        hmParam.put("ORDERID", strOrderId);
        hmParam.put("CUSTPO", strCustPO);
        hmParam.put("TRACK", strTrack);
        hmParam.put("TOTAL", strTotal);
        hmParam.put("ORDERFRM", strFromDate);
        hmParam.put("ORDERTO", strToDate);
        hmParam.put("SHIPFRM", strShipFrmDt);
        hmParam.put("SHIPTO", strShipToDt);
        hmParam.put("ORDERBY", strOrderBy);
        hmParam.put("INVOICEID", strInvoiceId);
        hmParam.put("PARENTID", strParentOrdId);
        hmParam.put("HOLDFL", strHoldFl);
        hmParam.put("DTFORMAT", strApplDateFmt);
        hmParam.put("FORMAT", strApplDateFmt);;
        hmParam.put("PARENTFL", strParentFl);
        hmParam.put("ACC_CURR_ID", strAccCurrId);
        hmParam.put("ACC_CURR_SYM", GmCommonClass.getCodeNameFromCodeId(strAccCurrId));
        hmParam.put("ACC_NAME", strAccountNm);
        hmParam.put("REP_NAME", strRepNm);
        hmParam.put("ORDER_BY_NAME", strOrderByNm);



        // Clear Accounts Cbo if Hold is checked.
        if (!strHoldFl.equals("")) {
          hmParam.put("ACCID", "0");
        }

        alReturn = gmCust.reportAccountOrder(hmParam);
        // Show parent Account check box as active when checked.Default it should be chekcked.
        strParentFl = strParentFl.equals("on") ? "true" : "false";
        request.setAttribute("PARFLAG", strParentFl);

        hmParam.put("SHIPFRM", dtShipFrmDt);
        hmParam.put("SHIPTO", dtShipTodt);
        hmParam.put("ORDERFRM", dtFromDate);
        hmParam.put("ORDERTO", dtToDate);


        hmReturn.put("ACCOUNTORDERLIST", alReturn);
        alReturn = new ArrayList();
        // alReturn = gmSales.reportAccount();
        // hmReturn.put("ACCOUNTLIST", alReturn);
        // alReturn = gmCust.getRepList();
        // hmReturn.put("REPLIST", alReturn);
        request.setAttribute("EXCLCOMPNY", strRuleComp);
        request.setAttribute("hAction", strFormat);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hmParam", hmParam);
        request.setAttribute("hMode", "Reload");
        request.setAttribute("ENBL_UPDATE_LOTNUM", strEnblUpdateLotNum);
        // log.debug(" Values inside HashMap which is returned is " + hmReturn);

        strDispatchTo = strCustPath.concat("/GmOrderAction.jsp");

        // When click L icon from Custo Po edit screen , will be displayed Order details in popup.
        if (strOpt.equals("CUSTPO")) {
          strDispatchTo = strCustPath.concat("/GmOrderReport.jsp");
        }
      } else if (strMode.equals("LoadOrder")) {
        strOrdId = request.getParameter("hOrdId") == null ? "" : request.getParameter("hOrdId");
        strAction =
            request.getParameter("Cbo_Action1") == null ? "" : request.getParameter("Cbo_Action1");
        if (strAction.equals("CQ")) {
          // Confusion starts here !!! All this because of AddCart.jsp
          strAccId =
              request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
          strPartNums = gmCust.getPartNumsByOrder(strOrdId);

          hmLoadReturn = gmCust.loadOrderAccount(strAccId);
          ArrayList alShipList = (ArrayList) hmLoadReturn.get("SHIP");
          ArrayList alCartDetails = new ArrayList();
          HashMap hmAccount = (HashMap) hmLoadReturn.get("ACCOUNTDETAILS");

          hmReturn = gmCust.loadCart(strPartNums, strAccId, ""); // "" is for rep id. Changed for
                                                                 // getting the Loaner Status
          hmLoadReturn = gmCust.loadOrderDetails(strOrdId);
          HashMap hmTemp = new HashMap();
          hmTemp = (HashMap) hmLoadReturn.get("ORDERDETAILS");
          hmLoadReturn.put("ORDID", hmTemp.get("ID"));
          hmLoadReturn.put("SHIPADD", hmTemp.get("SHIPADD"));
          hmLoadReturn.put("PO", hmTemp.get("PO"));
          hmLoadReturn.put("SHIP", alShipList);
          hmLoadReturn.put("ACCOUNTDETAILS", hmAccount);
          hmLoadReturn.put("SHIPTO", "");

          alCartDetails = (ArrayList) hmLoadReturn.get("CARTDETAILS");

          HashMap hmLoop = new HashMap();
          HashMap hmTemp1 = new HashMap();
          String strTemp1 = "";
          String strTemp2 = "";
          for (int i = 0; i < alCartDetails.size(); i++) {
            hmLoop = (HashMap) alCartDetails.get(i);
            hmTemp = new HashMap();
            strTemp1 = (String) hmLoop.get("ID");
            strTemp2 = (String) hmLoop.get("QTY");
            hmTemp.put("QTY", strTemp2);
            hmTemp1.put(strTemp1, hmTemp);
          }
          session.setAttribute("hmCart", hmTemp1);
          session.setAttribute("hmOrder", hmReturn);
          session.setAttribute("hmReturn", hmLoadReturn);
          session.setAttribute("hAction", "PopOrd");
          session.setAttribute("strPartNums", strPartNums);
          session.setAttribute("hMode", "Edit");
          strDispatchTo = strCustPath.concat("/GmAddToCart.jsp");
          // Ends here ! Finally !!

        } else if (strAction.equals("PO")) {
          strAction = "PO";
        } else if (strAction.equals("CLI")) {
          strAction = "CONTROL";
        } else if (strAction.equals("VO")) {
          strAction = "";
        } else if (strAction.equals("SI")) {
          strAction = "SHIP";
          hmLoadReturn = gmCust.loadShipping();
          request.setAttribute("hmShipLoad", hmLoadReturn);
        }
        if (!strAction.equals("CQ")) {
          hmLoadReturn = gmCust.loadOrderDetails(strOrdId);
          request.setAttribute("hmReturn", hmLoadReturn);
          request.setAttribute("hMode", strAction);
          strDispatchTo = strCustPath.concat("/GmEditOrder.jsp");
        }
      }

      if (strAction.equals("CQ")) {
        gotoPage(strDispatchTo, request, response);
      } else {
        dispatch(strDispatchTo, request, response);
      }

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmOrderVoidServlet
