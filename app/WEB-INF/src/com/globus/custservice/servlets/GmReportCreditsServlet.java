/*****************************************************************************
 * File : GmReportCreditsServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmSalesCreditBean;
import com.globus.operations.returns.beans.GmBBAProcessReturnsProcessInterface;
import com.globus.operations.returns.beans.GmBBAReturnSummaryPicSlip;
import com.globus.operations.returns.beans.GmProcessReturnsBean;


public class GmReportCreditsServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strReturnsPath = GmCommonClass.getString("GMRETURNS");
    String strDispatchTo = strCustPath.concat("/GmReturnsReport.jsp");

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");

      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
        session.removeAttribute("hAction");
      }

      strAction = (strAction == null) ? "Load" : strAction;
      String strOpt = (String) session.getAttribute("strSessOpt");
      strOpt = strOpt == null || strOpt.equals("") ? "" : strOpt;

      GmSalesCreditBean gmSalesCredit = new GmSalesCreditBean(getGmDataStoreVO());
      GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
      /*
       * The following code is added for showing the Domain based Return Summary. For BBA, we are
       * calling the GmBBAProcessReturnsBean.java and for US, we are using the
       * GmUSProcessReturnsBean.java to get the RA details.
       */
      GmBBAProcessReturnsProcessInterface gmBBAProcessReturnsProcessInterface =
          GmBBAReturnSummaryPicSlip.getInstance(GmCommonClass.parseNull(GmCommonClass.getRuleValue(
              "RAPICSLIP", "FETCH_CLASS")));

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();
      HashMap hmLoadReturn = new HashMap();

      String strDistId = "";
      String strFormat = "";
      String strType = "";
      String strRAId = "";
      String strOrderID = "";
      String strFrmDate = "";
      String strToDate = "";

      if (strAction.equals("Load")) {
        hmLoadReturn = gmSalesCredit.loadCreditReportList(strType);
        hmReturn.put("LOADLIST", hmLoadReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
      } else if (strAction.equals("Reload")) {
        strDistId =
            request.getParameter("Cbo_DistId") == null ? "" : request.getParameter("Cbo_DistId");
        strFormat =
            request.getParameter("Cbo_Action") == null ? "" : request.getParameter("Cbo_Action");
        strFrmDate =
            request.getParameter("Txt_FromDate") == null ? "" : request
                .getParameter("Txt_FromDate");
        strToDate =
            request.getParameter("Txt_ToDate") == null ? "" : request.getParameter("Txt_ToDate");
        alReturn =
            gmSalesCredit.reportCreditsByDistributor(strDistId, strFormat, strFrmDate, strToDate);

        hmReturn.put("DISTORDERLIST", alReturn);
        hmLoadReturn = gmSalesCredit.loadCreditReportList(strType);
        hmReturn.put("LOADLIST", hmLoadReturn);

        request.setAttribute("hType", strFormat);
        request.setAttribute("hDistId", strDistId);
        request.setAttribute("hFrmDt", strFrmDate);
        request.setAttribute("hToDt", strToDate);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "Reload");
      } else if (strAction.equals("ViewReturn")) {
        strRAId = request.getParameter("hRAId") == null ? "" : request.getParameter("hRAId");
        hmReturn = gmProcessReturnsBean.loadCreditReport(strRAId, strAction);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strCustPath.concat("/GmReturnsView.jsp");
      }
      // When cash adjustment is made below credit memo is called
      else if (strAction.equals("PrintCashAdj")) {
        strOrderID =
            request.getParameter("hOrderId") == null ? "" : request.getParameter("hOrderId");
        hmReturn = gmSalesCredit.loadCashCreditReport(strOrderID);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strReturnsPath.concat("/GmCreditMemoPrint.jsp");
      } else if (strAction.equals("PrintVersion")) {
        // hOrderId is passed from OrderAction.jsp if the order id is passed
        // Pass the order id and fetch the Rreturn Account Number
    	GmCommonBean gmCommonBean = new GmCommonBean();
        strOrderID =
            request.getParameter("hOrderId") == null ? "" : request.getParameter("hOrderId");
        if (strOrderID.equals("")) {
          strRAId = request.getParameter("hId") == null ? "" : request.getParameter("hId");
        } else {
          strRAId = gmSalesCredit.getRAID(strOrderID);
        }

        log.debug(" Order Id is " + strOrderID + " RAID value is " + strRAId);

        hmReturn = gmBBAProcessReturnsProcessInterface.loadCreditReport(strRAId, strAction);
        log.debug(" Values inside hmreturn is " + hmReturn);
        ArrayList AlLogReasons =GmCommonClass.parseNullArrayList((ArrayList)gmCommonBean.getLog(strRAId, "26240618"));
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("LOGREASON", AlLogReasons);
        strDispatchTo = strReturnsPath.concat("/GmCreditMemoPrint.jsp");
      }

      if (strAction.equals("CQ")) {
        gotoPage(strDispatchTo, request, response);
      } else {
        dispatch(strDispatchTo, request, response);
      }

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmReportCreditsServlet
