package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmConsignmentBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.event.beans.GmEventReportBean;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmLoanerPartRepServlet extends GmServlet {
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }  


  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");

    String strDispatchTo = strCustPath.concat("/GmLoanerPartRep.jsp");
    String strOpt = "";
    String strSetId = "";
    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";
    String strDistributorName = "";
    String strSetName = "";
    String LoadSts = "";
    instantiate(request, response);
    GmConsignmentBean gmConsignBean = new GmConsignmentBean(getGmDataStoreVO());
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());

    RowSetDynaClass rowSetLoanerReport = null;
    List lResult = null;
    String strPartDesc = "";
    String strHeader = "";
    HashMap hmResult = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmFromDate = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alLoanerType = new ArrayList();
    ArrayList alMonthDropDown = new ArrayList();
    ArrayList alYearDropDown = new ArrayList();
    
    String strInputParam = GmCommonClass.parseNull(request.getParameter("inputParam"));
    String strHInputParam = GmCommonClass.parseNull(request.getParameter("hinputparam"));
    String strStatusParam = GmCommonClass.parseNull(request.getParameter("hStatus"));
    String strSelectedStatus = GmCommonClass.parseNull(request.getParameter("selectedStatus"));
    String strHSelectedStatus = GmCommonClass.parseNull(request.getParameter("hselectedstatus"));
    String strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
    String strRepId = GmCommonClass.parseNull(request.getParameter("hRepId"));
    String strDistId = GmCommonClass.parseNull(request.getParameter("Cbo_DistId"));
    String strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
    String strStsId = GmCommonClass.parseNull(request.getParameter("Cbo_StsId"));
    String strPNum = GmCommonClass.parseNull(request.getParameter("TxtPartNum"));
    String strReconType = GmCommonClass.parseNull(request.getParameter("Cbo_ReconType"));
    String strLnType = GmCommonClass.parseNull(request.getParameter("Cbo_lnType"));
    String strHLnType = GmCommonClass.parseNull(request.getParameter("hLnTyp"));
    strLnType = (strLnType.equals("")) ? strHLnType : strLnType;
    strLnType = (strLnType.equals("")) ? "4127" : strLnType;
    String strFromDate = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
    String strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
    String strShwDet = GmCommonClass.parseNull(request.getParameter("Chk_shwDet"));
    strShwDet = strShwDet == "" ? "N" : "Y";
    String strEventId = GmCommonClass.parseNull(request.getParameter("Cbo_eventId"));
    String strEmpId = GmCommonClass.parseNull(request.getParameter("Cbo_empId"));
    String strAppDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strScreentype = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmResourceBundleBean gmResourceBundleBeanlbl =
        GmCommonClass.getResourceBundleBean("properties.labels.operations.GmLoanerStatusBySet",
            strSessCompanyLocale);

    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    try { // Merge code from OUS
      String strMonthBeginDate = "";

      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      strMonthBeginDate =
          GmCalenderOperations.getFirstDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());
      Logger log = GmLogger.getInstance(this.getClass().getName());
      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;

      strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      log.debug(" action is " + strAction + " opt os " + strOpt);
      alLoanerType = GmCommonClass.getCodeList("LNTYP");
      request.setAttribute("LOANERTYP", alLoanerType);
      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

      if (strAction.equals("Load") && strOpt.equals("")) {
        alReturn = gmCust.getDistributorList();
        request.setAttribute("DISTLIST", alReturn);

        alReturn = GmCommonClass.getCodeList("LNRTYP");
        request.setAttribute("LNTYLIST", alReturn);

        alReturn = GmCommonClass.getCodeList("LONRC");
        request.setAttribute("LNRCLIST", alReturn);

        request.setAttribute("FROMDATE", strTodaysDate);
        request.setAttribute("TODATE", strTodaysDate);

        strDispatchTo = strCustPath.concat("/GmLoanerReconReport.jsp");
      } else if (strAction.equals("Load") && strOpt.equals("Lookup")) {
        hmResult = gmConsignBean.loadLoanerPartRepSummary(strPartNum, strRepId);
        strPartDesc = (String) hmResult.get("PARTDESC");
        rowSetLoanerReport = (RowSetDynaClass) hmResult.get("RSLOANERINFO");
        request.setAttribute("PARTNUM", strPartNum);
        request.setAttribute("PARTDESC", strPartDesc);
        request.setAttribute("RSLOANERREPORT", rowSetLoanerReport);
      } else if (strAction.equals("Reload") && strOpt.equals("Recon")) {
        String strPartNumFormat = strPNum.replaceAll(",", "|");
        hmParam.put("DISTID", strDistId);
        hmParam.put("PARTNUM", strPartNumFormat);
        hmParam.put("LNTYP", strLnType);
        hmParam.put("FROMDATE", strFromDate);
        hmParam.put("TODATE", strToDate);
        hmParam.put("SHOW_LSALES_INDB",
            GmCommonClass.parseNull(GmCommonClass.getString("SHOW_4127_INDB")));
        lResult = gmConsignBean.loadLoanerReconReport(hmParam);

        HashMap hmDataMap = new HashMap();
        hmDataMap.put("SHOWDET", strShwDet);
        hmDataMap.put("RECONTYPE", strReconType);
        hmDataMap.put("SESSCURRSYMBOL", request.getSession().getAttribute("strSessCurrSymbol"));

        HashMap hmValues = new HashMap();
        hmValues.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
        hmValues.put("HRETURN", hmDataMap);
        hmValues.put("LRESULT", lResult);
        hmValues.put("TEMPLATE", "GmLoanerReconReport.vm");
        hmValues.put("TEMPLATEPATH", "/custservice/templates");

        String XmlGridData = gmConsignBean.getXmlGridData(hmValues);
        log.debug("lResult::" + lResult.size());

        rowSetLoanerReport = (RowSetDynaClass) hmResult.get("RSLOANERINFO");

        request.setAttribute("RSLOANERREPORT", rowSetLoanerReport);
        request.setAttribute("XmlGridData", XmlGridData);

        alReturn = gmCust.getDistributorList();
        request.setAttribute("DISTLIST", alReturn);
        alReturn = GmCommonClass.getCodeList("LNRTYP");
        request.setAttribute("LNTYLIST", alReturn);
        alReturn = GmCommonClass.getCodeList("LONRC");
        request.setAttribute("LNRCLIST", alReturn);
        request.setAttribute("hDistId", strDistId);

        request.setAttribute("PNUM", strPNum);
        request.setAttribute("RECONTYPE", strReconType);
        request.setAttribute("LNTYPE", strLnType);
        request.setAttribute("FROMDATE", strFromDate);
        request.setAttribute("TODATE", strToDate);
        request.setAttribute("SHWDET", strShwDet);

        strDispatchTo = strCustPath.concat("/GmLoanerReconReport.jsp");
      } else if (strOpt.equals("LoanerByDist") || strAction.equals("LoanerByDist")) {
        strFromMonth = request.getParameter("Cbo_FromMonth");
        strFromYear = request.getParameter("Cbo_FromYear");
        strToMonth = request.getParameter("Cbo_ToMonth");
        strToYear = request.getParameter("Cbo_ToYear");
        strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));


        if (strFromMonth == null && strToMonth == null) {
          hmFromDate = GmCommonClass.getMonthDiff(6, false);

          strFromMonth = (String) hmFromDate.get("FromMonth");
          strFromYear = (String) hmFromDate.get("FromYear");
          strToMonth = (String) hmFromDate.get("ToMonth");
          strToYear = (String) hmFromDate.get("ToYear");
        }

        hmParam.put("FROMMONTH", strFromMonth);
        hmParam.put("FROMYEAR", strFromYear);
        hmParam.put("TOMONTH", strToMonth);
        hmParam.put("TOYEAR", strToYear);
        hmParam.put("SETID", strSetId);
        hmParam.put("LNTYP", strLnType);

        log.debug(" hmParam values " + hmParam);
        hmResult = gmConsignBean.loanerByDist(hmParam);
        // To fetch the value for the unload condition
        strSetName =
            GmCommonClass
                .getStringWithTM(GmCommonClass.parseNull((String) hmResult.get("SETNAME")));

        hmResult.put("ALMONTHDROPDOWN", alMonthDropDown);
        hmResult.put("ALYEARDROPDOWN", alYearDropDown);
        strHeader =
            GmCommonClass.parseNull(gmResourceBundleBeanlbl
                .getProperty("LBL_LOANER_QTY_DISTRIBUTOR"));
        if (!strSetName.equals("")) {
          strHeader = strHeader + " - " + strSetName;
        }
        request.setAttribute("strHeader", strHeader);
        request.setAttribute("hmReturn", hmResult);
        request.setAttribute("Cbo_FromMonth", strFromMonth);
        request.setAttribute("Cbo_FromYear", strFromYear);
        request.setAttribute("Cbo_ToMonth", strToMonth);
        request.setAttribute("Cbo_ToYear", strToYear);
        request.setAttribute("hSetId", strSetId);
        request.setAttribute("Cbo_lnType", strLnType);
        strAction = "LoanerByDist";
        strDispatchTo = strOperPath.concat("/GmLoanerByDist.jsp");
      }

      else if (strOpt.equals("LoanerByDistBySet") || strAction.equals("LoanerByDistBySet")) {
        strFromMonth = request.getParameter("Cbo_FromMonth");
        strFromYear = request.getParameter("Cbo_FromYear");
        strToMonth = request.getParameter("Cbo_ToMonth");
        strToYear = request.getParameter("Cbo_ToYear");
        strDistId = GmCommonClass.parseNull(request.getParameter("hDistId"));

        if (strFromMonth == null && strToMonth == null) {
          hmFromDate = GmCommonClass.getMonthDiff(6, false);

          strFromMonth = (String) hmFromDate.get("FromMonth");
          strFromYear = (String) hmFromDate.get("FromYear");
          strToMonth = (String) hmFromDate.get("ToMonth");
          strToYear = (String) hmFromDate.get("ToYear");
        }

        log.debug(" values in date " + hmFromDate);

        hmParam.put("FROMMONTH", strFromMonth);
        hmParam.put("FROMYEAR", strFromYear);
        hmParam.put("TOMONTH", strToMonth);
        hmParam.put("TOYEAR", strToYear);
        hmParam.put("DISTID", strDistId);
        hmParam.put("LNTYP", strLnType);

        hmResult = gmConsignBean.loanerByDistBySet(hmParam);
        // To fetch the value for the unload condition
        strDistributorName = GmCommonClass.parseNull((String) hmResult.get("DISTNAME"));

        hmResult.put("ALMONTHDROPDOWN", alMonthDropDown);
        hmResult.put("ALYEARDROPDOWN", alYearDropDown);
        strHeader =
            GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_LOANER_QTY_BY_SET"));
        if (!strDistributorName.equals("")) {
          strHeader = strHeader + " - " + strDistributorName;
        }
        log.debug(" Header is " + strHeader + " Dist Name " + strDistributorName);
        request.setAttribute("strHeader", strHeader);
        request.setAttribute("hmReturn", hmResult);
        request.setAttribute("Cbo_FromMonth", strFromMonth);
        request.setAttribute("Cbo_FromYear", strFromYear);
        request.setAttribute("Cbo_ToMonth", strToMonth);
        request.setAttribute("Cbo_ToYear", strToYear);
        request.setAttribute("hDistId", strDistId);
        request.setAttribute("Cbo_lnType", strLnType);
        strAction = "LoanerByDistBySet";
        strDispatchTo = strOperPath.concat("/GmLoanerByDist.jsp");
      }

      else if (strOpt.equals("LoanerStatusBySet") || strAction.equals("LoanerStatusBySet")) {
    	  
          String strJsonString = "";
        // Commenting the below code since we are moving the data fetch from different method
        //  hmResult = gmConsignBean.loanerStatusBySet(strLnType);
          strJsonString = gmConsignBean.loanerStsBySet(strLnType);
          
        // To fetch the value for the unload condition
        strHeader =
            GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_STATUS_REPORT"));
        log.debug(" Header is " + strHeader);

        request.setAttribute("strHeader", strHeader);
        request.setAttribute("hmReturn", hmResult);
        request.setAttribute("Cbo_lnType", strLnType);
   //     gmBBAStagingDashForm.setStrXmlGridData(strJsonString);
        request.setAttribute("strLoaner_Data", strJsonString);

        strAction = "LoanerStatusBySet";
        strDispatchTo = strOperPath.concat("/GmLoanerStatusBySet.jsp");
      }

      else if (strOpt.equals("LoanerStatusByDist") || strAction.equals("LoanerStatusByDist")) {
        GmInHouseSetsReportBean gmInHouseSetsReportBean =
            new GmInHouseSetsReportBean(getGmDataStoreVO());
        ArrayList alResult = new ArrayList();

        strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));
        strLnType = GmCommonClass.parseNull(request.getParameter("hLnTyp"));
        strScreentype = GmCommonClass.parseNull(request.getParameter("hScreenType"));

        hmParam.put("SETID", strSetId);
        hmParam.put("TYPE", strLnType);
        hmParam.put("APPLNDATEFMT", strAppDateFmt);
        hmParam.put("SCREENTYPE", strScreentype);
        // log.debug(" values in hmParam " + hmParam);
        alResult = gmInHouseSetsReportBean.getInHouseSetStatus(hmParam);

        if (alResult.size() > 0) {
          HashMap hmTemp = (HashMap) alResult.get(0);
          strSetName = GmCommonClass.getStringWithTM((String) hmTemp.get("SNAME"));
          strSetName = strSetId + " / " + strSetName;
        }

        log.debug(" values inside result arraylist  " + alResult.size());

        hmParam.put("SETID", strSetId);
        hmParam.put("APPLNDATEFMT ", strAppDateFmt);
        alReturn = gmInHouseRptBean.reportLoanerOpenRequests(hmParam);
        request.setAttribute("LOANERREQUESTS", alReturn);

        request.setAttribute("ALRESULT", alResult);
        request.setAttribute("SETNAME", strSetName);
        request.setAttribute("hLnTyp", strLnType);

        strAction = "LoanerStatusByDist";
        strDispatchTo = strOperPath.concat("/GmLoanerStatusByDist.jsp");
      }


      else if (strOpt.equals("LoanerRequests") || strAction.equals("LoanerRequests")
          || ((strAction.equals("Reload") && strOpt.equals("Requests")))
          || (strOpt.equals("InhouseLoanerRequests"))
          || (strAction.equals("Reload") && strOpt.equals("InhouseLoanerRequests"))) {
        ArrayList alDist = new ArrayList();
        ArrayList alSet = new ArrayList();
        ArrayList alStatus = new ArrayList();
        ArrayList lrs = new ArrayList();
        ArrayList alEventName = new ArrayList();
        ArrayList alEmpList = new ArrayList();
        // The following code added for Getting the Distributor & Sales Rep based on the PLANT if
        // the Selected Company is EDC
        HashMap hmFilter = new HashMap();
        String strCompPlantFilter = "";
        String strCompanyID = "";
        strCompanyID = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
        strCompPlantFilter =
            GmCommonClass
                .parseNull(GmCommonClass.getRuleValue(strCompanyID, "LOAN_PROCESS_HEADER"));// LOAN_PROCESS_HEADER-
                                                                                            // To
                                                                                            // fetch
                                                                                            // the
                                                                                            // records
                                                                                            // for
                                                                                            // EDC
                                                                                            // entity
                                                                                            // countries
                                                                                            // based
                                                                                            // on
                                                                                            // plant
        hmFilter.put("FILTER", "Active");
        if (strCompPlantFilter.equals("Plant")) {
          hmFilter.put("DISTFILTER", "COMPANY-PLANT");
        }
        alDist = gmCust.getDistributorList(hmFilter);
        request.setAttribute("DISTLIST", alDist);

        alSet = gmProjectBean.loadSetMap("1600");
        request.setAttribute("SETLIST", alSet);

        alStatus = GmCommonClass.getCodeList("RQDTST");
        request.setAttribute("STATUSLIST", alStatus);

        alEmpList = gmLogonBean.getEmployeeList();
        request.setAttribute("EMPLIST", alEmpList);

        alEventName = GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventNameList());
        request.setAttribute("EVENTNAME", alEventName);


        strSetName = GmCommonClass.parseNull(request.getParameter("txt_SetName"));
        String strDtType = GmCommonClass.parseNull(request.getParameter("dtType"));
        String strFromDt = GmCommonClass.parseNull(request.getParameter("fromDt"));
        String strToDt = GmCommonClass.parseNull(request.getParameter("toDt"));
        String strInHouseType = "";
        
        if(strInputParam!= "" || strHInputParam!= "") {
        	if (strDtType.equals("") && strFromDt.equals("") && strToDt.equals("")) {
                strFromDt = strTodaysDate;
                strToDt = strTodaysDate;
                strDtType = "PS";    //type should be Planned shipped date
              }
        	
        }
        if (strOpt.equals("InhouseLoanerRequests")) {

          strInHouseType = GmCommonClass.parseNull(request.getParameter("hInHouseType"));
          strInHouseType = (strInHouseType.equals("")) ? "InhouseLoanerRequests" : strInHouseType;
          String strAccessFl =
              gmEventSetupBean.getEventAccess(strUserId, "EVENT", "VOID_INHOUSE_REQUEST");
          request.setAttribute("AccessFl", strAccessFl);
        }

        String strPrdReqId = GmCommonClass.parseNull(request.getParameter("PrdReqId"));
        if (strPrdReqId.equals("")) {
          strPrdReqId = GmCommonClass.parseNull((String) request.getAttribute("PrdReqId"));
        }
        String strStatus = GmCommonClass.parseNull(request.getParameter("hStatus"));
        String strCaseId = GmCommonClass.parseNull(request.getParameter("hCaseId"));

        // String strTodaysDate =(String) session.getAttribute("strSessTodaysDate");
        if (strAction.equals("Load")
            && (strOpt.equals("LoanerRequests") || strOpt.equals("InhouseLoanerRequests"))) { // set
                                                                                              // default
                                                                                              // values
                                                                                              // when
                                                                                              // left
                                                                                              // clicked

          if (strDtType.equals("") && strFromDt.equals("") && strToDt.equals("")) {
            strFromDt = strTodaysDate;
            strToDt = strTodaysDate;
            strDtType = "PS";
          }
          if (strStatus.equals("")) {
            strStatus = "10,20";
          }
        }
        if (!strCaseId.equals("")) {
          strDtType = "SG"; // Surgery Date
        }
        hmParam.put("SETNAME", strSetName);
        hmParam.put("DISTID", strDistId);
        hmParam.put("CASEID", strCaseId);
        hmParam.put("FROMDT", strFromDt);
        hmParam.put("TODT", strToDt);
        hmParam.put("DTTYPE", strDtType);
        hmParam.put("PRDREQID", strPrdReqId);
        hmParam.put("STATUS", strStatus);
        hmParam.put("TYPE", "4127"); // hardcoding the type to 4127 which is Product Loaner - CONTY
        if (strOpt.equals("InhouseLoanerRequests")) {
          hmParam.put("TYPE", "4119"); // type to 4119 which is Inhouse Loaner
        }
        hmParam.put("EVENTID", strEventId);
        hmParam.put("EMPID", strEmpId);
        hmParam.put("INPUTPARAM", strInputParam);
        hmParam.put("HINPUTPARAM", strHInputParam);
        hmParam.put("SELECTEDSTATUS", strSelectedStatus);
        hmParam.put("HSELECTEDSTATUS", strHSelectedStatus);
        hmParam.put("STATUSPARAM", strStatusParam);
        hmParam.put("TXNTYPE", strInHouseType);
        hmParam.put("DATEFMT", strAppDateFmt);
        hmParam.put("APPLNDATEFMT", strAppDateFmt);
        hmParam.put("HINHOUSETYPE", strInHouseType);
        hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
        log.debug(" values in hmParam " + hmParam);
        	
        lrs = gmInHouseRptBean.reportLoanerOpenRequests(hmParam);
        
        request.setAttribute("LOANERREQUESTS", lrs);
        request.setAttribute("hDistId", strDistId);
        request.setAttribute("hSetName", strSetName);
        request.setAttribute("CASEID", strCaseId);
        request.setAttribute("hFromDt", strFromDt);
        request.setAttribute("hToDt", strToDt);
        request.setAttribute("hDtType", strDtType);
        request.setAttribute("status", strStatus);
        request.setAttribute("hInHouseType", strInHouseType);
        request.setAttribute("hEventId", strEventId);
        request.setAttribute("hEmpId", strEmpId);
        request.setAttribute("hInputParam", strInputParam);
        request.setAttribute("hHInputParam", strHInputParam);
        request.setAttribute("hSelectedStatus", strSelectedStatus);
        hmParam.put("hhselectedstatus", strHSelectedStatus);
        request.setAttribute("htStatusParam", strStatusParam);
        request.setAttribute("REPORT", gmInHouseRptBean.getXmlGridData(lrs, hmParam)); // getXMLGRIDdata
        // strAction = "LoanerRequests";
        strDispatchTo = strOperPath.concat("/GmLoanerRequests.jsp");
      }
      // Merge code from OUS
      // From Case Summary Report or Case Id drilldown
      else if (strOpt.equals("CaseSummaryRpt") || strOpt.equals("LoadCaseSummaryRpt")) {
        ArrayList alDistList = new ArrayList();
        ArrayList alAccList = new ArrayList();
        ArrayList alStsList = new ArrayList();
        GmInHouseSetsReportBean gmInHouseSetsReportBean =
            new GmInHouseSetsReportBean(getGmDataStoreVO());

        String strDtType = "";

        String strParentCaseId = GmCommonClass.parseNull(request.getParameter("CASEID"));
        String strStatus = GmCommonClass.parseNull(request.getParameter("hStatus"));

        // Get the Active distributors list and set in request.
        alDistList = gmCust.getDistributorList("Active");
        request.setAttribute("DISTLIST", alDistList);

        alAccList = gmSales.reportAccount();
        request.setAttribute("ACCLIST", alAccList);

        alStsList = GmCommonClass.getCodeList("CSYPRT");
        request.setAttribute("STATUSLIST", alStsList);


        // Check request values and set defaults if needed
        if (strOpt.equals("CaseSummaryRpt")) {
          // Set Defaults
          // strFromDate = strTodaysDate;
          log.debug("strFromDate:" + strFromDate);
          if (strFromDate.equals("")) {
            strFromDate = strMonthBeginDate;
          }
          strToDate = strTodaysDate;
          strDtType = "SG"; // Surgery Date
          LoadSts = "NO";

        } else {
          // Load From page
          strDistId = GmCommonClass.parseNull(request.getParameter("Cbo_DistId"));
          strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
          strStsId = GmCommonClass.parseNull(request.getParameter("Cbo_StsId"));
          strDtType = GmCommonClass.parseNull(request.getParameter("dtType"));
          strFromDate = GmCommonClass.parseNull(request.getParameter("fromDt"));
          strToDate = GmCommonClass.parseNull(request.getParameter("toDt"));
        }

        strStatus = "10,20"; // Open,Allocated
        // 30,40 = Closed,Canceled TODO: not included for now as per Rajesh

        hmParam.put("DISTID", strDistId);
        hmParam.put("ACCID", strAccId);
        hmParam.put("STSID", strStsId);
        hmParam.put("DTTYPE", strDtType); // Date Search Type (Surgery Date)
        hmParam.put("FROMDT", strFromDate);
        hmParam.put("TODT", strToDate);
        hmParam.put("STATUS", strStatus);
        hmParam.put("APPLNDATEFMT", strAppDateFmt);
        // Set template file (vm)
        hmParam.put("fileName", "GmCaseSummaryReport.vm");
        if (!LoadSts.equals("NO")) {
          alReturn = gmInHouseSetsReportBean.loadCaseSummaryInfo(hmParam);
        }


        request.setAttribute("CASESUMMARYDATA", alReturn);
        request.setAttribute("hDistId", strDistId);
        request.setAttribute("hAccId", strAccId);
        request.setAttribute("hStsId", strStsId);
        request.setAttribute("hSetName", strSetName);
        request.setAttribute("hFromDt", strFromDate);
        request.setAttribute("hToDt", strToDate);
        request.setAttribute("hDtType", strDtType);
        request.setAttribute("status", strStatus);

        strDispatchTo = strCustPath.concat("/GmCaseSummaryReport.jsp");

        String strXmlData = generateOutPut(alReturn, strSessCompanyLocale);
        request.setAttribute("strXmlData", strXmlData);
      }
      request.setAttribute("hAction", strAction);
      dispatch(strDispatchTo, request, response);

    }// End of try


    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }
    // End of catch
  }// End of service method

  /**
   * generateOutPut This method will get output from template
   * 
   * @param ArrayList alGridData
   * @return String
   * @exception Exception
   */
  private String generateOutPut(ArrayList alGridData, String strSessCompanyLocale) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setResourceBundle(GmCommonClass
        .getResourceBundleBean(
            "properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest",
            strSessCompanyLocale));
    templateUtil.setTemplateName("GmCaseSummaryReport.vm");
    return templateUtil.generateOutput();
  }
}// End of Servlet

