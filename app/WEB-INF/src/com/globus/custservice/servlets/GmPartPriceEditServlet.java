/*****************************************************************************
 * File : GmPartPriceEditServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPartPriceEditServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = "";

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strPartNum = request.getParameter("PartNum");
      String strMode = request.getParameter("hMode");
      String strProjId = request.getParameter("hId");

      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmValues = new HashMap();
      HashMap hmTemp = new HashMap();

      ArrayList alProjectList = new ArrayList();
      ArrayList alAccountList = new ArrayList();

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strAccId = "";

      String strType =
          request.getParameter("hType") == null ? "Account" : request.getParameter("hType");
      log.debug("strType " + strType);
      if (strMode.equals("Save")) {
        String strEPrice = GmCommonClass.parseZero(request.getParameter("Txt_EPrice"));
        String strLPrice = GmCommonClass.parseZero(request.getParameter("Txt_LPrice"));
        String strCPrice = GmCommonClass.parseZero(request.getParameter("Txt_CPrice"));
        String strLiPrice = GmCommonClass.parseZero(request.getParameter("Txt_LiPrice"));

        hmValues.put("EPrice", strEPrice);
        hmValues.put("LPrice", strLPrice);
        hmValues.put("CPrice", strCPrice);
        hmValues.put("LiPrice", strLiPrice);

        String strMsg = gmSales.savePartNumPricing(hmValues, strPartNum, strUserId);

        ArrayList alReturn = new ArrayList();
        alReturn = gmSales.reportPartNumPricing("ID", strProjId);
        request.setAttribute("alReturn", alReturn);

        request.setAttribute("hLoad", "Part");
        request.setAttribute("hId", strProjId);
        dispatch(strCustPath.concat("/GmPartPriceReport.jsp"), request, response);
      } else if (strMode.equals("Edit")) {
        hmReturn = gmSales.loadPartNumPricing(strPartNum);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hId", strProjId);
        dispatch(strCustPath.concat("/GmPartPriceEdit.jsp"), request, response);
      } else if (strMode.equals("EditAccPrice")) {
        strAccId =
            request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
        hmValues.put("PARTNUM", strPartNum);
        hmValues.put("ACCID", strAccId);
        hmValues.put("TYPE", strType);
        hmReturn = gmSales.loadAccPartNumPricing(hmValues);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hId", strProjId);
        request.setAttribute("hAccId", strAccId);
        request.setAttribute("hType", strType);
        request.setAttribute("hMode", "EditAccPrice");
        dispatch(strCustPath.concat("/GmPartPriceEdit.jsp"), request, response);
      } else if (strMode.equals("SaveAccPrice")) {

        String strPrice =
            request.getParameter("Txt_Price") == null ? "" : request.getParameter("Txt_Price");
        String strDiscount =
            request.getParameter("Txt_Discount") == null ? "" : request
                .getParameter("Txt_Discount");
        strAccId = request.getParameter("hAccId") == null ? "" : request.getParameter("hAccId");

        hmValues.put("Price", strPrice);
        hmValues.put("Discount", strDiscount);
        hmValues.put("PARTNUM", strPartNum);
        hmValues.put("ACCID", strAccId);
        hmValues.put("USERID", strUserId);
        hmValues.put("TYPE", strType);
        // companyinfo required when updating price from JMS
        hmValues.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
        String strMsg = gmSales.saveAccPartNumPricing(hmValues);

        if (strType.equals("Account")) {
          hmReturn = gmSales.loadAccountPricing();
        } else { // if type is party
          if (strType.equals("GPO")) {
            alAccountList = gmPartyInfoBean.fetchPartyNameList("7003");
          } else if (strType.equals("ICT")) {
            alAccountList = gmPartyInfoBean.fetchPartyNameList("7004");
          }
          hmReturn.put("ACCOUNTLIST", alAccountList);
          hmTemp.put("COLUMN", "ID");
          hmTemp.put("STATUSID", "20301");
          alProjectList = gmProject.reportProject(hmTemp);
          hmReturn.put("PROJLIST", alProjectList);
        }
        ArrayList alYesNoList = new ArrayList();
        alYesNoList = GmCommonClass.getCodeList("YES/NO");
        String strVoidFl = request.getParameter("Cbo_Void");
     
        hmReturn.put("YES/NO", alYesNoList);
        hmValues.put("VOIDFL", strVoidFl);
        request.setAttribute("hMode", "Reload");
        request.setAttribute("hOpt", "Project");
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hVoidFl", strVoidFl);
        hmValues.put("PROJID", strProjId);
        hmResult = gmSales.reportAccountPricing(hmValues);

        request.setAttribute("hmResult", hmResult);
        request.setAttribute("REPORT", hmResult.get("REPORT"));
        request.setAttribute("hAccId", strAccId);
        request.setAttribute("hProjId", strProjId);
        request.setAttribute("hType", strType);
        String strSesPartyId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        HashMap hmPricingSetupAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId, "ACC_PRIC_PARAM_SUBMT"));
        String strBtnAccessFl = GmCommonClass.parseNull((String) hmPricingSetupAccess.get("UPDFL"));
        request.setAttribute("BTNACCESSFL", strBtnAccessFl);
        dispatch(strCustPath.concat("/GmAccountPriceReport.jsp"), request, response);
      }

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPartPriceEditServlet
