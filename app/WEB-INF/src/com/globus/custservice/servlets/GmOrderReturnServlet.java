/*****************************************************************************
 * File : GmOrderItemServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmOrderUsageBean;
import com.globus.custservice.beans.GmSalesCreditBean;

public class GmOrderReturnServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmSalesCreditBean gmSalesCredit = new GmSalesCreditBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    HashMap hmAccess = new HashMap();
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = "";
    String strOrderId = "";
    String strFunctionId = "";
    String strUserId = (String) session.getAttribute("strSessUserId");
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

    HashMap hmReturn = new HashMap();
    ArrayList alReturnReasons = new ArrayList();
    GmOrderUsageBean gmOrderUsageBean = new GmOrderUsageBean(getGmDataStoreVO());

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      String strShowUsageLot =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_USAGE_LOT"));
      String strShowDDTFl  = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.UPDATE_DDT_FL"));
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;
      log.debug("strAction" + strAction);
      // Below code will get the order information
      strOrderId = request.getParameter("hOrdId");
      if (strAction.equals("ReturnPart")) {
        strFunctionId = "INIT_RETURN_ACEESS";
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                strFunctionId));
        String strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        String strReadAccess = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
        String strVoidAccess = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
        if (!strUpdateAccess.equals("Y")) {
          throw new AppError("", "20685");
        }
      }
      // Modify Order Dashboard Information
      String strMode = request.getParameter("hMode");
      String StrAccID = request.getParameter("Cbo_AccId");
      String strFromDate = request.getParameter("Txt_FromDate");
      String strToDate = request.getParameter("Txt_ToDate");
      strOrderId = strOrderId == null ? "" : strOrderId;
      hmReturn = gmSalesCredit.loadOrderDetailsWithReturn(strOrderId);
      ArrayList alUsedLotdetails = gmOrderUsageBean.fetchUsedLotNumber(strOrderId);

      if (strAction.equals("ReturnPart")) {
          // Sales Item Return type
    	  // Load the Return reason
        alReturnReasons = gmCommon.getCodeList("RETRE","3300"); 
        strDispatchTo = strCustPath.concat("/GmModifyOrderInitiateReturn.jsp");
      }
      hmReturn.put("SHOW_USAGE", strShowUsageLot);
      hmReturn.put("SHOW_DDT_FL", strShowDDTFl);
      request.setAttribute("alReasons", alReturnReasons);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      request.setAttribute("ALUSEDLOTDTLS", alUsedLotdetails);

      // Modify Order Dashboard Information
      request.setAttribute("hMode", strMode);
      request.setAttribute("hAccId", StrAccID);
      request.setAttribute("hFrom", strFromDate);
      request.setAttribute("hTo", strToDate);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmOrderItemServlet
