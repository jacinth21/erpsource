/*****************************************************************************
 * File : GmConsignLoanerServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmConsignLoanerServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMOPERATIONS");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");

    String strDispatchTo = strCustPath.concat("/GmLoanerBuildSetup.jsp");

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean();

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();

      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strConsignId = "";
      String strType = "";
      String strBillTo = "";
      String strPurpose = "";
      String strShipTo = "";
      String strShipToId = "";
      String strComments = "";
      String strShipFl = "";

      String strSetId = "";
      String strMode = "";

      if (strAction.equals("Load")) {
        alReturn = gmProj.fetchSetMaping("900.003", "SET");
        hmResult.put("SETLOAD", alReturn);
        hmReturn = gmCust.loadConsignLists("", null);
        hmResult.put("CONLISTS", hmReturn);

        session.setAttribute("hmReturn", hmResult);
        session.setAttribute("hAction", strAction);
        session.setAttribute("hMode", strMode);
      } else if (strAction.equals("Initiate")) {
        hmResult = gmOper.initiateSetBuild("900.003", "LN", strUserId);
        strConsignId = (String) hmResult.get("CONSIGNID");
        hmReturn.put("CONDETAILS", hmResult.get("CONDETAILS"));

        strType = request.getParameter("Cbo_Type") == null ? "" : request.getParameter("Cbo_Type");
        strPurpose =
            request.getParameter("Cbo_Purpose") == null ? "0" : request.getParameter("Cbo_Purpose");
        strBillTo =
            request.getParameter("Cbo_BillTo") == null ? "" : request.getParameter("Cbo_BillTo");
        strShipTo =
            request.getParameter("Cbo_ShipTo") == null ? "" : request.getParameter("Cbo_ShipTo");
        strShipToId =
            request.getParameter("Cbo_Values") == null ? "0" : request.getParameter("Cbo_Values");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strShipFl = request.getParameter("Chk_ShipFlag") == null ? "0" : "1";

        hmParam.put("TYPE", strType);
        hmParam.put("PURPOSE", strPurpose);
        hmParam.put("DISTACCID", strBillTo);
        hmParam.put("SHIPTO", strShipTo);
        hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("SHIPFL", strShipFl);
        hmParam.put("STATUSFL", "2"); // Hardcoding to set sttaus so that Control Number can be
                                      // entered

        hmResult = gmCust.updateSetConsign(strConsignId, hmParam, strUserId);

        request.setAttribute("hMode", "AlloPic");

        // For Load
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        // for Other Values
        HashMap hmConsign = new HashMap();
        hmConsign.put("CONSIGNMENTID", strConsignId);
        hmResult = gmCust.loadSetConsignLists(hmConsign);
        hmReturn.put("SETCONLISTS", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
        strDispatchTo = strCustPath.concat("/GmSetConsignSetup.jsp");
      } else if (strAction.equals("PicSlip")) {
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strCustPath.concat("/GmLoanerPicSlip.jsp");
      }

      if (strAction.equals("Initiate") || strAction.equals("PicSlip")) {
        dispatch(strDispatchTo, request, response);
      } else {
        gotoPage(strDispatchTo, request, response);
      }

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmConsignLoanerServlet