/*****************************************************************************
 * File : GmReturnsReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;

public class GmReturnsReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmReturnCountReport.jsp");
    String strType = "";
    String strhAction = "";
    String strDistId = "";
    String strSetId = "";
    String strRAId = "";
    String strDistName = "";
    int currMonth = 0;
    ArrayList alReturn = new ArrayList();
    try {

      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmCalenderOperations gmCal = new GmCalenderOperations();
      HashMap hmReturn = new HashMap();
      RowSetDynaClass rsReturnsReport = null;

      strType = request.getParameter("hType") == null ? "50230" : request.getParameter("hType");
      strhAction =
          request.getParameter("hAction") == null ? "Load" : request.getParameter("hAction");
      strDistId = request.getParameter("Cbo_Dist") == null ? "" : request.getParameter("Cbo_Dist");
      strSetId = request.getParameter("Cbo_Set") == null ? "" : request.getParameter("Cbo_Set");
      strDistName =
          request.getParameter("hDistName") == null ? "" : (String) request
              .getParameter("hDistName");
      String strTodaysDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
      String firstDayofMonth = "";
      currMonth = gmCal.getCurrentMonth() - 1;
      firstDayofMonth = gmCal.getAnyDateOfMonth(currMonth, 1, strApplDateFmt);

      // strMonthBeginDate = strTodaysDate.replaceAll("/\\d\\d/","/01/");
      String strFromDt = "";
      String strToDt = "";

      strFromDt =
          request.getParameter("Txt_FromDate") == null ? firstDayofMonth : request
              .getParameter("Txt_FromDate");
      strToDt =
          request.getParameter("Txt_ToDate") == null ? strTodaysDate : request
              .getParameter("Txt_ToDate");

      String strOpt = (String) session.getAttribute("strSessOpt");
      strOpt = strOpt == null || strOpt.equals("") ? "" : strOpt;
      request.setAttribute("distributorName", strDistName);

      // to set the method parameter
      HashMap hmValue = new HashMap();
      hmValue.put("TYPE", strType);
      hmValue.put("FROMDT", strFromDt);
      hmValue.put("TODT", strToDt);
      hmValue.put("DATEFMT", strApplDateFmt);

      if (strhAction.equals("Load") || strhAction.equals("Reload")) {
        if (strOpt.equals("Sets")) {
          log.debug("In Servlet....");

          alReturn = gmCust.loadReturnsReport(strType, strFromDt, strToDt);
          request.setAttribute("hType", strType);

          HashMap hmParam = new HashMap();
          hmParam.put("hType", strType);
          request.setAttribute("rsRetReport", generateOutPut(alReturn, hmParam));
        } else if (strOpt.equals("Items")) {
          if (strhAction.equals("Load")) {
            strType = "50230";
            hmReturn = gmCust.loadItemConsignReport(hmValue);
            request.setAttribute("hType", strType);
            request.setAttribute("hmReturn", hmReturn);
            strDispatchTo = strCustPath.concat("/GmConsignItemReport.jsp");
          } else if (strhAction.equals("Reload")) {
            hmReturn = gmCust.loadItemConsignReport(hmValue);
            request.setAttribute("hType", strType);
            request.setAttribute("hmReturn", hmReturn);
            strDispatchTo = strCustPath.concat("/GmConsignItemReport.jsp");
          }
        }
      } else if (strhAction.equals("Drilldown")) {
        hmReturn = gmCust.loadReturnsReportDrilldown(strSetId, strDistId, strFromDt, strToDt);
        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strCustPath.concat("/GmReturnsDrilldownReport.jsp");
      }
      // to be removed
      /*
       * else if (strhAction.equals("ReturnDetails")) { strRAId = request.getParameter("hRAId")==
       * null?"":request.getParameter("hRAId"); hmReturn =
       * gmCust.loadReturnsDetails(strRAId,strhAction); request.setAttribute("hmReturn",hmReturn);
       * request.setAttribute("hAction",strhAction); strDispatchTo =
       * strCustPath.concat("/GmReturnsView.jsp"); }
       */

      request.setAttribute("hType", strType);
      request.setAttribute("hDistId", strDistId);
      request.setAttribute("hSetId", strSetId);
      request.setAttribute("hFrmDt", strFromDt);
      request.setAttribute("hToDt", strToDt);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method


  private String generateOutPut(ArrayList alValues, HashMap hmValues) throws Exception {

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alValues);
    templateUtil.setDataMap("hmResult", hmValues);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmReturnCountReport", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/returns/templates");
    templateUtil.setTemplateName("GmReturnsReportSet.vm");
    return templateUtil.generateOutput();
  }
}// End of GmReturnsReportServlet
