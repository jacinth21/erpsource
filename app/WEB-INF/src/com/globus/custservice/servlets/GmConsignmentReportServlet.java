/*****************************************************************************
 * File : GmConsignmentReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmConsignmentBean;
import com.globus.custservice.beans.GmCustomerBean;

public class GmConsignmentReportServlet extends GmServlet {
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strOperPath.concat("/GmConsignmentReport.jsp");
    String strType = "";
    String strhAction = "";
    String strDistId = "";
    String strSetId = "";
    String strDistName = "";

    String strScreenType = GmCommonClass.parseNull(request.getParameter("screenType"));
    String strScreenParam = "";
    String strApplDateFmt = "";


    // checkSession(response, session); // Checks if the current session is valid, else
    // redirecting
    // to SessionExpiry page
    instantiate(request, response);
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmConsignmentBean gmCons = new GmConsignmentBean(getGmDataStoreVO());
    GmCalenderOperations gmCal = new GmCalenderOperations();
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alResult = new ArrayList();
    RowSetDynaClass rsConsignReturn = null;
    strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    strhAction =
        request.getParameter("hAction") == null ? "Load" : (String) request.getParameter("hAction");
    strDistId =
        request.getParameter("Cbo_Dist") == null ? "" : (String) request.getParameter("Cbo_Dist");
    strSetId =
        request.getParameter("Cbo_Set") == null ? "" : (String) request.getParameter("Cbo_Set");
    strDistName =
        request.getParameter("hDistName") == null ? "" : (String) request.getParameter("hDistName");

    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    String strMonthBeginDate =
        GmCalenderOperations.getFirstDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());
    String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    request.setAttribute("allReportTypes", gmCust.fetchReportTypes());
    request.setAttribute("distributorName", strDistName);
    String strFromDt =
        request.getParameter("Txt_FromDate") == null ? strMonthBeginDate : request
            .getParameter("Txt_FromDate");
    String strToDt =
        request.getParameter("Txt_ToDate") == null ? strTodaysDate : request
            .getParameter("Txt_ToDate");
    String strOpt = (String) session.getAttribute("strSessOpt");
    strOpt = strOpt.equals("") || strOpt == null ? "" : strOpt;
    String strReportType =
        request.getParameter("Cbo_ReportType") == null ? "" : request
            .getParameter("Cbo_ReportType");
    if (strOpt.equalsIgnoreCase("Items")) {
      strType = request.getParameter("hType") == null ? "50230" : request.getParameter("hType");
    } else {
      strType = request.getParameter("hType") == null ? "50230" : strReportType;
    }
    if (!strScreenType.equals("")) {
      strType = strScreenType.substring(4, strScreenType.length());
      strScreenParam = strScreenType.substring(0, 3);
    }
    log.debug(" action " + strhAction + " strOpt " + strOpt + " hType " + strType
        + " strScreeen Type " + strScreenType);
    HashMap hmParam = new HashMap();
    HashMap hmValue = new HashMap();
    // set the VM parameters
    hmParam.put("STROPT", strOpt);
    hmParam.put("APPLNDATEFMT", strApplDateFmt);
    hmParam.put("SCREENTYPE", strScreenParam);
    hmParam.put("RPT_TYPE", strType);
    String strXml = "";
    // set the method parameters (Dist id is new)
    hmValue.put("TYPE", strType);
    hmValue.put("FROMDT", strFromDt);
    hmValue.put("TODT", strToDt);
    hmValue.put("DATEFMT", strApplDateFmt);
    hmValue.put("DISTID", strDistId);
    if (strOpt.equals("Sets")) {
      if (strhAction.equals("Load") || strhAction.equals("Type")) {
        log.debug("In Servlet...1.");
        request.setAttribute("ALLNEWREPORTTYPE", gmCust.fetchReportTypes());
        rsConsignReturn = gmCust.loadConsignReportNew(hmValue);

        strXml = getXmlGridData(rsConsignReturn.getRows(), hmParam);
        request.setAttribute("hType", strType);
        request.setAttribute("rsConsReport", rsConsignReturn);
        strDispatchTo = strOperPath.concat("/GmConsignCountReport.jsp");
      } else if (strhAction.equals("DrillDown")) {
        hmReturn = gmCust.loadConsignReport(strType, strDistId, strSetId, strFromDt, strToDt);
        request.setAttribute("hType", strType);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hDistId", strDistId);
        request.setAttribute("hSetId", strSetId);
        request.setAttribute("hFromDt", strFromDt);
        request.setAttribute("hToDt", strToDt);
      }
    } else if (strOpt.equals("Items")) {
      if (strhAction.equals("Load")) {
        // strType = "50230"; //DIST
        hmReturn = gmCust.loadItemConsignReport(hmValue);
        request.setAttribute("hType", strType);
        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmConsignItemReport.jsp");
      } else if (strhAction.equals("Reload")) {
        hmReturn = gmCust.loadItemConsignReport(hmValue);
        request.setAttribute("hType", strType);
        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmConsignItemReport.jsp");
      }
      alResult = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("REPORT"));
      strXml = getXmlGridData(alResult, hmParam);
    } else if (strOpt.equals("SETNET")) {
      if (strhAction.equals("Load")) {
        alReturn = gmCust.getDistributorList();
        hmReturn.put("DISTLIST", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmConsignNetReport.jsp");
      } else if (strhAction.equals("Reload")) {
        alReturn = gmCust.getDistributorList();
        hmReturn.put("DISTLIST", alReturn);
        alReturn = gmCons.loadConsignNetReport(strDistId);
        hmReturn.put("REPORT", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strOperPath.concat("/GmConsignNetReport.jsp");
      }
      request.setAttribute("hAction", strhAction);
      request.setAttribute("hOpt", strOpt);
    }
    request.setAttribute("screenType", strScreenType);
    request.setAttribute("hDistId", strDistId);
    request.setAttribute("grid_XML", strXml);

    request.setAttribute("screenType", strScreenType);
    request.setAttribute("grid_XML", strXml);
    request.setAttribute("hFrmDt", strFromDt);
    request.setAttribute("hToDt", strToDt);
    request.setAttribute("hType", strType);
    request.setAttribute("strOpt", strOpt);
    if (strScreenParam.equalsIgnoreCase("ICT")) {
      HashMap hmDisType = new HashMap();
      if (strType.equals("50234")) { // Field Sales - ICT
        hmDisType.put("DISTTYP", "70103");
      } else if (strType.equals("50235")) { // Field Sales - FD
        hmDisType.put("DISTTYP", "70104");
      } else if (strType.equals("50236")) {// Field Sales - ICS
        hmDisType.put("DISTTYP", "70105");
      } else if (strType.equals("50237")) {// Field Sales - ICA
        hmDisType.put("DISTTYP", "70106");
      }
      // The following code added for Getting the Distributor based on the PLANT if the Selected
      // Company is EDC
      String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
      String strCompanyPlant =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "CONSIGN_RPT_SETS")); // For
                                                                                                 // both
                                                                                                 // sets
                                                                                                 // and
                                                                                                 // items
                                                                                                 // [Inter
                                                                                                 // company
                                                                                                 // reports]

      if (strCompanyPlant.equals("Plant")) {
        hmDisType.put("DISTFILTER", "COMPANY-PLANT");
      }
      ArrayList alDistList = GmCommonClass.parseNullArrayList(gmCust.getDistributorList(hmDisType));
      request.setAttribute("alDistList", alDistList);
      strDispatchTo = strOperPath.concat("/GmICTConsignReport.jsp");
    }
    dispatch(strDispatchTo, request, response);


  }// End of service method

  /**
   * @getXmlGridData xml content generation for Field Sales lock grid
   * @author mmuthusamy
   * @param array list and hash map
   * @return String
   */

  private String getXmlGridData(List list, HashMap hmParam) throws AppError {

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", list);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmConsignCountReport", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName("GmConsignmentReport.vm");

    return templateUtil.generateOutput();
  }

}// End of GmConsignmentReportServlet
