/*****************************************************************************
 * File			 : GmPartNumSearchServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;
import com.globus.common.beans.*;
import com.globus.custservice.beans.*;
import com.globus.common.servlets.*;
import com.globus.prodmgmnt.beans.GmPartNumSearchBean;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmPartNumSearchServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		instantiate(request, response);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
		String strDispatchTo = strCustPath.concat("/GmPartNumSearch.jsp");

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page

				GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
				GmPartNumSearchBean gmPartNum = new GmPartNumSearchBean(getGmDataStoreVO());
				HashMap hmReturn = new HashMap();
				HashMap hmParams = new HashMap();
				
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				String strPartNum = "";
				String strControl = "";
				String strType = "";
				String strFromDate = "";
				String strToDate = "";
				String strMode = "";
				String strIds = "";
				String strDtFmt="";
				String strCompanyId="";
				String strReportBy = "";
				int intIndex = 0;

				strPartNum = request.getParameter("Txt_PartNum")==null?"":request.getParameter("Txt_PartNum");
				strControl = request.getParameter("Txt_CNum")==null?"":request.getParameter("Txt_CNum");
				strFromDate = request.getParameter("Txt_FromDate")==null?"":request.getParameter("Txt_FromDate");
				strToDate = request.getParameter("Txt_ToDate")==null?"":request.getParameter("Txt_ToDate");
				strDtFmt = request.getParameter("hDtFmt")==null?"":request.getParameter("hDtFmt"); 
				strCompanyId = request.getParameter("hCompanyId")==null?"":request.getParameter("hCompanyId"); 
				strReportBy = request.getParameter("reportType") == null ? "" : request.getParameter("reportType");
          
				if (strAction.equals("Go"))
				{
					intIndex = strPartNum.indexOf(",");
					if (intIndex == -1)
					{
						strIds = "'".concat(strPartNum).concat("'");
					}
					else
					{
						strIds = strPartNum;
						strIds = "'".concat(strIds).concat(",");
						strIds = strIds.replaceAll(",","','");
						strIds = strIds.substring(0,strIds.length()-2);
					}
					hmReturn = gmCust.loadPartNumSearchReport(strIds,strControl,strFromDate,strToDate,strDtFmt,strCompanyId);
				}
				else if (strAction.equals("Drill"))
				{
					strIds = request.getParameter("hId")==null?"":request.getParameter("hId");
					strType = request.getParameter("hType")==null?"":request.getParameter("hType");
					hmParams.put("STRIDS",strIds);
					hmParams.put("STRTYPE",strType);
					hmParams.put("STRPARTNUM",strPartNum);
					hmParams.put("STRCONTROL",strControl);
					hmParams.put("STRFROMDATE",strFromDate);
					hmParams.put("STRTODATE",strToDate);
					hmParams.put("STRDTFMT",strDtFmt);
					hmParams.put("STRREPORTBY",strReportBy);
					hmReturn = gmPartNum.loadPartNumSearchDrillDown(hmParams);
					request.setAttribute("hType",strType);
					strDispatchTo = strCustPath.concat("/GmPartNumSearchDrill.jsp");
				}
				
				if (!strAction.equals("Load"))
				{				
					request.setAttribute("hPartNum",strPartNum);
					request.setAttribute("hCNum",strControl);
					request.setAttribute("hFrom",strFromDate);
					request.setAttribute("hTo",strToDate);
					
					request.setAttribute("hmReturn",hmReturn);
				}
				
				dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmPartNumSearchServlet