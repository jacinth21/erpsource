/*****************************************************************************
 * File : GmOrderItemServlet Desc :
 * 
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.*; 

import java.util.LinkedHashSet;
import java.util.Set;



import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmCreditHoldEngine;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.common.servlets.GmShippingServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
// import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmCustomerExtnBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.beans.GmPlaceOrderInterface;
import com.globus.custservice.beans.GmPriceBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.jms.consumers.beans.GmOrderBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.logistics.beans.GmBackOrderReportBean;
import com.globus.operations.logistics.beans.GmBackOrderTransBean;
import com.globus.sales.InvAllocation.beans.GmPendAllocationBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.event.beans.GmEventSetupBean;
import com.globus.sales.pricing.beans.GmPriceRequestRptBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.custservice.beans.GmDOTxnBean;



public class GmOrderItemServlet extends GmShippingServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmOrderItemSetup.jsp");
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLoanerBean gmLoanerBean = new GmLoanerBean(getGmDataStoreVO());
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    GmARBean gmAR = new GmARBean(getGmDataStoreVO());
    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCreditHoldEngine gmCreditHoldEngine = new GmCreditHoldEngine(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean=new GmInvLocationBean();
    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean();
    GmOrderBean gmOrderBean = new GmOrderBean(getGmDataStoreVO());
    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33510
  
    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page

      String strAction = request.getParameter("hAction");
      String strChangePriceAction = GmCommonClass.parseNull(request.getParameter("hChangePrice")); //Code change for PMT-38606
      log.debug("GmOrderItemServlet..strAction  "+strAction);
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      String strProcess = request.getParameter("hMode");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      strProcess = (strProcess == null) ? "" : strProcess;
      String strMode = "";
      String strEGPSUsage ="";
      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmCustomerExtnBean gmCustExtn = new GmCustomerExtnBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
      GmBackOrderTransBean gmBackOrderTransBean = new GmBackOrderTransBean(getGmDataStoreVO());
      GmCommonCancelBean gmCommonCancel = new GmCommonCancelBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
      GmDORptBean gmDORptBean = new GmDORptBean(getGmDataStoreVO());
      GmPriceRequestRptBean gmPriceRequestRptBean = new GmPriceRequestRptBean(getGmDataStoreVO());
      GmDOTxnBean gmDOTxnBean = new GmDOTxnBean(getGmDataStoreVO());
      GmPriceBean gmPriceBean = new GmPriceBean(getGmDataStoreVO()); //for PMT-38606


      // Get Settings from cookie
      GmCookieManager gmCookieManager = new GmCookieManager();
      String strTagColumn = GmCommonClass.parseNull(request.getParameter("hTagColumn"));
      String strCapFlColumn = GmCommonClass.parseNull(request.getParameter("hCapFlColumn"));
      strTagColumn =
          strTagColumn.equals("") ? gmCookieManager.getCookieValue(request, "TagColumn")
              : strTagColumn;
      strCapFlColumn =
          strCapFlColumn.equals("") ? gmCookieManager.getCookieValue(request, "CapFlColumn")
              : strCapFlColumn;

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmShipping = new HashMap();
      HashMap hmCaseInfo = new HashMap();
      HashMap hmHoldValues = new HashMap();
      HashMap hmTempCart;
      HashMap hmLoop;
      HashMap hmAccess = new HashMap();
      HashMap hmOrderTotals = new HashMap();
      RowSetDynaClass resultSet = null;
      HashMap hmPoDetails = new HashMap();
      HashMap hmOrdDetails = new HashMap();
      HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33510

      Date dtSurgDate = null;
      
      String strTempId = "";
      String strOrderId = "";
      String strOrdType = "";
      String strBillTo = "";
      String strPO = "";
      String strAccId = "";
      String strShipTo = "";
      String strShipToId = "";
      String strComments = "";
      String strShipFl = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPartNums = "";
      String strhPartNums = "";
      String strDelPartNum = "";
      String strInputStr = "";
      String strAdjInputSrt = "";
      String strPerson = "";
      String strTotal = "";
      String strCboType = "";
      String strPartNum = "";
      String strTempPartNums = "";
      String strOrderComments = "";
      String strOrderPrefix = "";
      String strTotalSales = "";
      String strSessOrderId = "";
      String strPOAmt = "";
      String strTagId = "";
      String strAckId = "";
      String strInputString = "";
      String strQty = "";
      String strPrice = "";
      String strControl = "";
      String strItemOrdType = "";
      String strPriceDisc = "";

      HashMap hmValues = new HashMap();
      String strShipDate = "";
      String strShipCarr = "";
      String strShipMode = "";
      String strTrack = "";
      String strShipCost = "";
      String strShipAdd = "";
      String strRepNum = "";
      String strRepName = "";
      String strLogReason = "";
      ArrayList alReturn = new ArrayList();
      String strParentOrderId = "";
      String strAccountName = "";
      String strBOFlag = "";
      String strCapFlag = "";
      String strConCode = "";
      String strErrMsg = "";
      String strRptType = "";
      String strSalesRep = "";
      String strBackOrdRptAccFl = "";
      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strQuoteStr = "";
      String strUpdateAccess = "";
      String strReadAccess = "";
      String strVoidAccess = "";
      String strFunctionId = "";
      String strVendorMail = "";
      String strDoId = "";
      String strCaseId = "";
      String strCaseInfoId = "";
      String strCtrlNumberStr = "";
      String strAccessFilter = "";
      String strAgingType = "";
      String strFilters = "";
      String strCondition = "";
      String strHoldFl = "";
      String strHoldFl_Ack = "";
      String strDefaultDoHoldCmt = "";
      String strHoldReason = "";
      String strHardcp_PO = "";
      String strOverrideAttnTo = "";
      String strShipInstruction = "";
      String strAtrbCodeGrp = "";
      String strRequiredDt = "";
      String strChnPriceAccess = "N";
            
      String strEditPOAccess="N";
     
      String strBtnAccessFl = "";
      String strShipCharge = "";
      String strParentOrdId = "";
      // getting the company id
      String strCompanyId = getGmDataStoreVO().getCmpid();
      String strPlantId = getGmDataStoreVO().getPlantid();
      String strCompanyLocale = "";
      ArrayList alOrderTypeList = new ArrayList();
      ArrayList alResult = new ArrayList();
      ArrayList alOrderList = new ArrayList();
      ArrayList alSourceList = new ArrayList();
      ArrayList alTypeList = new ArrayList();
      ArrayList alCboList = new ArrayList();
      ArrayList alRevenueTrack = new ArrayList();
      HashMap hmApplnParam = new HashMap();
      HashMap hmEmailProps = new HashMap();
      ArrayList alComment = new ArrayList();
      ArrayList alAdjCodeList = new ArrayList();
      HashMap hmLotOverrideAccess = new HashMap();
      String strLotOverrideUpdAccess = "";
      String strLotOrderideAcess = "N";
      String strShowReserve = "";
      String strViaCellCmpFl = "";
      String strViaCellPlantFl = "";
      String strIncJSPNM = "";
      String strGSTstartFl = "";
      int intAddLen = 500;
      String strReturnedOrder = "";
      String strValidateFlag = "";
      String strValidateOrderId = "";
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      String strBOPartNos = "";
      int alBOResultSize = 0;
      String strOusAccessFlag ="";
      String strAccName = "";
      String strReleaseQtyFlag ="";
     
      //PC-3880 New field in record PO Date in GM Italy
      String strPODate = "";
      GmRuleEngine gmRuleEngine = new GmRuleEngine();
      strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
      // Locale
      GmResourceBundleBean rbCompany =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
      GmResourceBundleBean rbPaperwork =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      hmLotOverrideAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "LOT_OVERRIDE_CHECK"));
      strLotOverrideUpdAccess = GmCommonClass.parseNull((String) hmLotOverrideAccess.get("UPDFL"));
      if (strLotOverrideUpdAccess.equals("Y")) {
        strLotOrderideAcess = "Y";
      }
      request.setAttribute("LOTOVERRIDEACCESS", strLotOrderideAcess);
      strShowReserve = GmCommonClass.parseNull(rbCompany.getProperty("SHOW_RESERVE_LOT"));
      hmApplnParam.put("SHOW_RESERVE_LOT", strShowReserve);
      request.setAttribute("ShowReserveLot", strShowReserve);

      String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));;
      String strShowSurgAtr = "NO";
      strShowSurgAtr = GmCommonClass.parseNull(rbCompany.getProperty("DO.SHOW_SURG_ATRB_DET"));
      strBtnAccessFl = gmEventSetupBean.getEventAccess(strUserId, "EVENT", "UPDATE_TO_BILLONLY");// Get
                                                                                                 // the
                                                                                                 // access
                                                                                                 // for
                                                                                                 // Update
                                                                                                 // to
                                                                                                 // Bill
                                                                                                 // Only
                                                                                                 // button
      request.setAttribute("strBtnAccessFl", strBtnAccessFl);
      log.debug(" Action " + strAction + " Opt is " + strOpt);
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "EDIT_SHIP_COST"));
      String strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("EDITSHIPCHARGE", strUpdFl);
      strBackOrdRptAccFl =
          GmCommonClass.parseNull(gmCommonBean.checkUserExists(strUserId, "BACK_ORD_RPT_ACC"));
      // Get the viacell company flag and plant flag for validating the part number
      strViaCellCmpFl =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "VIACELLCMPNY"));
      strViaCellPlantFl =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "VIACELLPLANT"));
      request.setAttribute("VIACELLCMPNY", strViaCellCmpFl);
      request.setAttribute("VIACELLPLANT", strViaCellPlantFl);
      if (!strBackOrdRptAccFl.equals("0")) {
        request.setAttribute("BackOrdRptAccFl", "Y");
      } else {
        request.setAttribute("BackOrdRptAccFl", "N");
      }
      if (strAction.equals("EditPrice") || strAction.equals("EditPO")) {
        if (strAction.equals("EditPrice")) {
          strFunctionId = "CHPRISING_ACEESS";
        } else if (strAction.equals("EditPO")) {
          strFunctionId = "CHNPO_ACEESS";
        }
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                strFunctionId));
        strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        strReadAccess = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
        strVoidAccess = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
          
        if (strUpdateAccess.equals("Y") && strReadAccess.equals("Y") && strVoidAccess.equals("Y")) {
          strChnPriceAccess = "Y";
        }
       
        
        if(strChnPriceAccess.equals("Y")){
        	
        	strFunctionId = "CHNPO_EDIT";
        	
        	hmAccess =
                    GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                        strFunctionId));
        
        	strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
            
                if (strUpdateAccess.equals("Y")){
                	strEditPOAccess = "Y";
                }
        	
        }
       
        request.setAttribute("EDITPOACCESS",strEditPOAccess);
        request.setAttribute("CHNACCESS", strChnPriceAccess);
        
        
        if (!strUpdateAccess.equals("Y") && !strReadAccess.equals("Y")
            && !strVoidAccess.equals("Y")) {
          throw new AppError("", "20685");
        }
      }
      alCboList = GmCommonClass.getCodeList("BOCBO");
      
      //fetch order types for surgery date mandatory field  
      if(strOpt.equals("PHONE")){
    	  String strSurgeryDtInfo = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SURG_DT_MANDATORY", "SURG_DT_INFO", strCompanyId));
    	  request.setAttribute("SURGERYDTINFO", strSurgeryDtInfo);
    	  
    	  //code change for PMT-39389 rule values to make NPI Details Mandatory based on order type and company
    	  String strNPIMandatoryFl = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("COMPANY", "NPIMANDATORY", strCompanyId));
    	  String strNPIOrder = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("ORDERTYPE","NPIMANDATORY", strCompanyId));
    	  request.setAttribute("NPIMANDATORYFLAG", strNPIMandatoryFl);
    	  request.setAttribute("NPIORDERTYPES", strNPIOrder);
    }
      if ((strAction.equals("Load") && strOpt.equals("PHONE"))
          || (strAction.equals("Load") && strOpt.equals("QUOTE"))
          || (strAction.equals("Load") && strOpt.equals("PROFORMA"))) {
        // adding condition for US and OUS code migration
        if ((strOpt.equals("PHONE") || strOpt.equals("PROFORMA")) && strShowSurgAtr.equals("YES")) {
          strAtrbCodeGrp = "SURATB";
        } else {
          strAtrbCodeGrp = "QUOATB";
        }
        hmReturn = gmCustExtn.loadItemOrderLists(strAtrbCodeGrp); //PC-4659 - gmCustExtn code changes 
        // hmShipping = gmCust.loadShipping();
        ArrayList t1 = new ArrayList();
        // t1 =(ArrayList) hmShipping.get("CARRIER");
        ArrayList t2 = new ArrayList();
        // t2 = (ArrayList)hmShipping.get("MODE");
        request.setAttribute("hmReturn", hmReturn);
        // request.setAttribute("ShippingInfo",hmShipping);

        /* Below code is used to get case information when it comes from CSM */
        strCaseInfoId = GmCommonClass.parseNull(request.getParameter("caseInfoId"));
        if (!strCaseInfoId.equals("")) {
          hmCaseInfo = GmCommonClass.parseNullHashMap(gmDOBean.validateNewOrder(strCaseInfoId));
          strAccId = GmCommonClass.parseNull(request.getParameter("accId"));
          strCaseId = GmCommonClass.parseNull(request.getParameter("caseId"));
          hmCaseInfo.put("ACCTID", strAccId);
          hmCaseInfo.put("CASEID", strCaseId);
          request.setAttribute("HMCASEINFO", hmCaseInfo);

        }
        strAction = "LoadPhoneOrder";
        if (strOpt.equals("PROFORMA")) {
          strDispatchTo = strCustPath.concat("/GmOrderAcknowledge.jsp");
        } else {// Seperate JSP should be called for Quote order
          strDispatchTo =
              strOpt.equals("QUOTE") ? strCustPath.concat("/GmOrderQuote.jsp") : strCustPath
                  .concat("/GmPhoneOrder.jsp");
        }
      } else if (strOpt.equals("BOACKORD")) {
        strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
        strAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));
        strAccId = strAccId.equals("0") ? "" : strAccId;
        strAccName = strAccId.equals("") ? "" : strAccName;
        hmParam.put("ACCID", strAccId);
        hmParam.put("ACCNAME", strAccName);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hmParam", hmParam);
        if (strProcess.equalsIgnoreCase("Reload")) {
          ArrayList alBOResult =
              GmCommonClass.parseNullArrayList(gmCust.fetchOrdAckBODet(strAccId));
          String strXmlData = generateOutPut(alBOResult, "GmAckOrdBOList.vm", hmApplnParam);
          if(alBOResult != null && !alBOResult.isEmpty()) {
           alBOResultSize = alBOResult.size(); 
          }
          //Prepare backorder parts string for inventory lookup
           if(alBOResultSize > 0){
        	   strBOPartNos = generateBoParts(alBOResult);
           }
               request.setAttribute("LOOKUPPARTNOS", strBOPartNos);
               request.setAttribute("XMLDATA", strXmlData);
        }
        strDispatchTo = strCustPath.concat("/GmAckOrdBOReport.jsp");
      } else if (strAction.equals("Load") && strOpt.equals("ProcBO")) {
        /*
         * The Below code added for Matching the records from for Back Order Loaner Items screen
         * with Back Order - Sales / Loaner Items Screen
         */
        strRptType = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
        String strRegnFilters = GmCommonClass.parseNull(request.getParameter("hRegnFilter"));
        String strZoneFilters = GmCommonClass.parseNull(request.getParameter("hZoneFilter"));
        String strDistFilters = GmCommonClass.parseNull(request.getParameter("hDistFilter"));
        // String strTerrFilter =
        // GmCommonClass.parseNull((String)request.getParameter("hTerrFilter"));
        String strPgrpFilters = GmCommonClass.parseNull(request.getParameter("hPgrpFilter"));
        String strDivFilters = GmCommonClass.parseNull(request.getParameter("hDivFilter"));
        String strCompFilters = GmCommonClass.parseNull(request.getParameter("hCompFilter"));
        String strTypeVal = GmCommonClass.parseNull(request.getParameter("Cbo_Type_List"));
        String strSource = GmCommonClass.parseNull(request.getParameter("Cbo_Source_Type"));
        strTagId = GmCommonClass.parseNull(request.getParameter("Txt_TagID"));
        strAgingType = GmCommonClass.parseNull(request.getParameter("Cbo_Aging"));
        String strRefId = GmCommonClass.parseNull(request.getParameter("REFID"));
        String strScreenFrom = GmCommonClass.parseNull(request.getParameter("hFrom"));

        if (strRegnFilters.equals("") && strZoneFilters.equals("") && strDistFilters.endsWith("")
            && strPgrpFilters.equals("")) {
          strFilters = "Y";
        }
        strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
        HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
        strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
        strCondition = getAccessCondition(request, response, true);
        hmParam.put("CONDITION", strCondition);
        hmParam.put("SETID", strPgrpFilters);
        hmParam.put("STRFILTER", strFilters);
        hmParam.put("Cbo_Type_Val", strTypeVal);
        hmParam.put("Cbo_Source_Type", strSource);
        request.setAttribute("hmSalesFilters", hmSalesFilters);
        hmParam.put("AccessFilter", strAccessFilter);

        strAgingType = strAgingType == "" ? "00" : strAgingType;
        hmParam.put("AGTYPE", strAgingType);
        if (strRptType.equals("")) {
          strRptType = "50261";
        }

        // Getting Tag id using CN ID from Process Transaction screen
        if (strTagId.equals("") && strScreenFrom.equals("PROCESS_TXN_SCN")) {
          strTagId = gmBackOrderReportBean.getTagIdFromTxnId(strRefId);
        }

        hmParam.put("TAGID", strTagId);
        hmParam.put("RPTOPT", strRptType);
        hmParam.put("REFID", strRefId);
        alOrderList = GmCommonClass.getCodeList("SBOTP");
        alSourceList = GmCommonClass.getCodeList("RQSRC");
        alTypeList = GmCommonClass.getCodeList("RQBYT");
        alReturn = gmBackOrderReportBean.loadBackOrderReport(hmParam);
        hmReturn.put("CBOLIST", alCboList);
        hmReturn.put("BOREPORT", alReturn);
        hmReturn.put("ORDERLIST", alOrderList);
        hmReturn.put("SOURCELIST", alSourceList);
        hmReturn.put("TYPELIST", alTypeList);
        // log.debug("Array List : "+alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hRptType", strRptType);
        request.setAttribute("hAgType", strAgingType);
        request.setAttribute("hSource", strSource);
        request.setAttribute("hTypeVal", strTypeVal);
        request.setAttribute("strTagId", strTagId);
        request.setAttribute("hScreen", strScreenFrom);
        request.setAttribute("hRefId", strRefId);
        strDispatchTo = strCustPath.concat("/GmBackOrderProcess.jsp");
      } else if (strAction.equals("Address")) {
        strBillTo = request.getParameter("hAccId") == null ? "" : request.getParameter("hAccId");
        strPerson = request.getParameter("hRepId") == null ? "" : request.getParameter("hRepId");
        strShipTo = request.getParameter("hShip") == null ? "" : request.getParameter("hShip");
        strShipToId =
        request.getParameter("hShipId") == null ? "0" : request.getParameter("hShipId");
        strShipAdd = gmCust.getShipAddress(strBillTo, strPerson, strShipTo, strShipToId);
        request.setAttribute("hShipAdd", strShipAdd);
        strDispatchTo = strCustPath.concat("/GmShipAdd.jsp");
      }else if (strAction.equals("SAddress")) {
      	  strShipTo = request.getParameter("hShip") == null ? "" : request.getParameter("hShip");
          strShipToId =
              request.getParameter("hShipId") == null ? "0" : request.getParameter("hShipId");
          log.debug("-strShipTo--"+strShipTo+"-strShipToId--"+strShipToId);
          strShipAdd = gmPendAllocationBean.getShipAddress(strShipTo, strShipToId, intAddLen);
          request.setAttribute("hShipAdd", strShipAdd);
          strDispatchTo = strCustPath.concat("/GmShipAdd.jsp");    	
      }
      else if (strAction.equals("SAVEDELNOTE")) {
        String strRefOrderId = GmCommonClass.parseNull(request.getParameter("hOrderId"));
        strTotal = GmCommonClass.parseNull(request.getParameter("hTotal"));
        strShipCost = GmCommonClass.parseNull(request.getParameter("shipCharge"));
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputString"));
        hmReturn = gmCust.loadOrderDetails(strRefOrderId); 
        log.debug("hmReturn " + hmReturn);
        HashMap hmOrderDet = (HashMap) hmReturn.get("ORDERDETAILS");
        HashMap hmShipDetails = (HashMap) hmReturn.get("SHIPDETAILS");
        strOrderPrefix = GmCommonClass.parseNull(rbCompany.getProperty("DO.ORDER_PREFIX"));
        if (strOrderPrefix.equalsIgnoreCase("yes")) {
          strOrderId = gmCust.loadNextOrderId(strOrderId, strParentOrderId, strAccId, "DELORD");
        } else {
          strOrderId = gmCust.loadNextOrderId(strOrderId, strParentOrderId, strAccId, "");
        }
        strRepNum = GmCommonClass.parseNull((String) hmOrderDet.get("REPID"));
        strRepName = GmCommonClass.parseNull((String) hmOrderDet.get("REPDISTNM"));
        strOrdType = GmCommonClass.parseNull(request.getParameter("cbo_ack_action")); // dropdown
                                                                                      // generate
                                                                                      // order
                                                                                      // values in
                                                                                      // ack oder
                                                                                      // edit screen
        strBillTo = GmCommonClass.parseNull((String) hmOrderDet.get("ACCID"));
        strMode = GmCommonClass.parseNull((String) hmOrderDet.get("RMODE"));
        strPerson = GmCommonClass.parseNull((String) hmOrderDet.get("RFROM"));
        strComments = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        strOrderComments = GmCommonClass.parseNull(request.getParameter("Txt_OrderComments"));
        strPO = GmCommonClass.parseNull(request.getParameter("Txt_PO"));
        strAccId = GmCommonClass.parseNull((String) hmOrderDet.get("ACCID"));
        strConCode = "0";
        strHoldFl = GmCommonClass.parseNull((String) hmOrderDet.get("HOLD_FL"));
        strHoldFl_Ack = GmCommonClass.parseNull((String) hmOrderDet.get("HOLD_FL_ACK"));
        strParentOrderId = GmCommonClass.parseNull((String) hmOrderDet.get("PARENTORDID"));
        strShipTo = GmCommonClass.parseNull((String) hmShipDetails.get("SHIPTO"));
        strShipToId = GmCommonClass.parseNull((String) hmShipDetails.get("SHIPTOID"));
        strShipCarr = GmCommonClass.parseNull((String) hmShipDetails.get("SCAR"));
        strShipMode = GmCommonClass.parseNull((String) hmShipDetails.get("SMODE"));
        String strAddressId = GmCommonClass.parseNull((String) hmOrderDet.get("ADDRESSID"));
        String strShipId = GmCommonClass.parseNull((String) hmOrderDet.get("SHIPID"));
        strQuoteStr = GmCommonClass.parseNull(gmCust.getOrderAttributeString(strRefOrderId));
        hmParam.put("ORDERID", strOrderId);
        hmParam.put("REFORDID", strRefOrderId);
        hmParam.put("TOTAL", strTotal);
        hmParam.put("INPUTSTR", strInputStr);
        hmParam.put("USERID", strUserId);
        hmParam.put("REPNUM", strRepNum);
        hmParam.put("REPNAME", strRepName);
        hmParam.put("TYPE", strOrdType);
        hmParam.put("BILLTO", strBillTo);
        hmParam.put("MODE", strMode);
        hmParam.put("PERSON", strPerson);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("ORDER_COMMENTS", strOrderComments);
        hmParam.put("PO", strPO);
        hmParam.put("ACCID", strAccId);
        hmParam.put("CONCODE", strConCode);
        hmParam.put("PARENTORDID", strParentOrderId);
        strTotalSales = strSessCurrSymbol + strTotal;// for getting the $ symbol
        hmParam.put("TOTAL_SALES", strTotalSales);// for getting the $ symbol
        hmParam.put("APPLNDATEFMT", strApplnDateFmt);// for displaying the date
        // hmParam.put("SALESREP",strSalesRep); Sales Rep Changes
        hmParam.put("SHIPTO", strShipTo);
        hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("SHIPCARRIER", strShipCarr);
        hmParam.put("SHIPMODE", strShipMode);
        hmParam.put("ADDRESSID", strAddressId);
        hmParam.put("SHIPID", strShipId);
        hmParam.put("QUOTESTR", strQuoteStr);
        hmParam.put("SHIPCHARGE", strShipCost);
        hmParam.put("gmDataStoreVO", getGmDataStoreVO());
        hmParam.put("COMPANYINFO", strCompanyInfo); 
        log.debug("order params to save " + hmParam);
        // Get the procedure from the Interface to save the details based on company
        GmPlaceOrderInterface gmPlaceOrderInterface =
            (GmPlaceOrderInterface) GmCommonClass.getSpringBeanClass("xml/Place_Order_Beans.xml",
                getGmDataStoreVO().getCmpid());
        hmReturn = gmPlaceOrderInterface.saveItemOrder(hmParam); 
        // Moving Order to hold Status if is there any Price Descrepency
        hmHoldValues.put("ORDERID", strOrderId);
        hmHoldValues.put("HOLDFLAG", strHoldFl_Ack);
        hmHoldValues.put("STROPT", strOpt);
        hmHoldValues.put("USERID", strUserId);
        hmHoldValues.put("COMPANY_INFO",strCompanyInfo);
        //JMS call 
        gmOrderBean.updateHoldOrderStatus(hmHoldValues);
        
        hmReturn = gmCust.loadOrderDetails(strOrderId);
        // call method to send the email to the rep
        HashMap hmEmailData = new HashMap();
        hmEmailData = gmCust.sendPhoneOrderEmail(hmReturn);
        hmEmailData.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        hmEmailData.put("APPLNDATEFMT", strApplnDateFmt);
        hmOrderTotals = gmDOBean.fetchOrderTotalDetails(strOrderId);
        hmEmailData.putAll(hmOrderTotals);
        GmJasperMail jasperMail = new GmJasperMail();
        jasperMail.setJasperReportName("/GmDeliveredOrderSummary.jasper");
        jasperMail.setAdditionalParams(hmEmailData);
        jasperMail.setReportData(null);
        jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));

        HashMap hmjasperReturn = jasperMail.sendMail();
        alComment = gmCommonBean.getLog(strOrderId, "1200");
        request.setAttribute("hmLog", alComment);
        hmReturn.put("ALCOMMENTS", alComment);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "OrdPlaced");
        ArrayList alTemp = new ArrayList();
        alTemp = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("CONSEQUENCES"));
        if (alTemp.size() > 0) {
          gmRuleEngine.sendRFSEmail(strOrderId, alTemp, "Order");
        }
        /*
         * Validate Account is on Credit Hold. if Account is Credit Hold Type1 then send Email
         * concern persons.
         */
        hmParam.put("EMAILTYPE", "ORDER");
        hmParam.put("ACCOUNTID", strAccId);
        hmParam.put("TXNID", strOrderId);
        gmCreditHoldEngine.validateCreditHoldAndSendEmail(hmParam);
        strDispatchTo = strCustPath.concat("/GmAckPrintPrice.jsp");
        
      //JMS call for Storing Building Info PMT-33510 for BUG-11234
        hmStorageBuildInfo.put("TXNID", strOrderId);
        hmStorageBuildInfo.put("TXNTYPE", "ORDER");
        gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);    
      } else if (strAction.equals("OUSDistributor")) {
    	  log.debug("String opt is " + strOpt);
    	  strAckId = GmCommonClass.parseNull(request.getParameter("hOrderId"));
    	  strOrderId = GmCommonClass.parseNull(request.getParameter("hReleaseOrder"));
    	  strShipCost = GmCommonClass.parseNull(request.getParameter("shipCharge"));
          strInputStr = GmCommonClass.parseNull(request.getParameter("hInputString"));
          strPO = GmCommonClass.parseNull(request.getParameter("Txt_PO"));
          strAccId = GmCommonClass.parseNull(request.getParameter("hOusAccId"));     
          
          hmParam.put("REFORDID", strAckId);//Ack id
          hmParam.put("RELEASEORDERID", strOrderId);//Order id
          hmParam.put("INPUTSTR", strInputStr);
          hmParam.put("USERID", strUserId);        
          hmParam.put("PO", strPO);
          hmParam.put("ACCID", strAccId);       
          hmParam.put("SHIPCHARGE", strShipCost);
          log.debug("order params to save " + hmParam);
         
          if(strOpt.equals("Generate")){
        	 strReturnedOrder = gmDOTxnBean.saveOusDistOrder(hmParam);        
		}else{
        	  gmDOTxnBean.saveOusPartDtls(hmParam);
             }  
         
          RequestDispatcher rd = request.getRequestDispatcher("/GmOrderItemServlet?hAction=ACKORD&hOrdId="+strAckId);
          rd.forward(request,response);
            }else if (strAction.equals("ReleaseOusDistributor")) { 
       	  strAckId = GmCommonClass.parseNull(request.getParameter("hOrderId")); //Ack id
    	  strOrderId = GmCommonClass.parseNull(request.getParameter("hReleaseOrder"));        
       
	      hmParam.put("RELEASEORDERID", strOrderId);//Ack id
          hmParam.put("USERID", strUserId);         
          log.debug("order params to save " + hmParam); 
		          
          if(strOpt.equals("ReleaseDistributor")){
            hmParam.put("ORDERID", strOrderId);
            hmParam.put("STROPT", strOpt);
            hmParam.put("COMPANY_INFO",strCompanyInfo);
        	  gmDOTxnBean.releaseOusDistOrder(hmParam);
          }
         RequestDispatcher rd = request.getRequestDispatcher("/GmOrderItemServlet?hAction=ACKORD&hOrdId="+strAckId);
         rd.forward(request,response);
      }
      
      else if (strAction.equals("PlaceOrder")) {
        strTempId = GmCommonClass.parseNull(request.getParameter("tempId"));
        strRepNum = GmCommonClass.parseNull(request.getParameter("Txt_SRepNum"));
        strRepName = GmCommonClass.parseNull(request.getParameter("Txt_SRepName"));
        strAccId = GmCommonClass.parseNull(request.getParameter("Txt_AccId"));
        // strShipTo = GmCommonClass.parseNull(request.getParameter("Cbo_ShipTo"));
        strBillTo = GmCommonClass.parseNull(request.getParameter("Cbo_BillTo"));
        // strShipCarr = GmCommonClass.parseNull(request.getParameter("Cbo_ShipCarr"));
        // strShipMode = GmCommonClass.parseNull(request.getParameter("Cbo_ShipMode"));
        // strShipToId = GmCommonClass.parseNull(request.getParameter("Cbo_Rep"));
        strOrdType = GmCommonClass.parseNull(request.getParameter("Cbo_OrdType"));
        strMode = GmCommonClass.parseNull(request.getParameter("Cbo_Mode"));
        strAccountName = GmCommonClass.parseNull(request.getParameter("Txt_AccName"));
        strPO = GmCommonClass.parseNull(request.getParameter("Txt_PO"));
        strConCode = GmCommonClass.parseNull(request.getParameter("hConCode"));
        strTotal = GmCommonClass.parseNull(request.getParameter("hTotal"));
        strOrderId = GmCommonClass.parseNull(request.getParameter("Txt_OrdId"));
        strComments = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        strAdjInputSrt = GmCommonClass.parseNull(request.getParameter("hAdjInputStr"));
        strParentOrderId = GmCommonClass.parseNull(request.getParameter("Txt_ParentOrdId"));
        strPerson = GmCommonClass.parseNull(request.getParameter("Txt_ReqBy"));
        strQuoteStr = GmCommonClass.parseNull(request.getParameter("hQuoteStr"));
        strCtrlNumberStr = GmCommonClass.parseNull(request.getParameter("hCtrlNumberStr"));
        strRequiredDt = GmCommonClass.parseNull(request.getParameter("Txt_ReqDate"));
        // strSalesRep = GmCommonClass.parseNull(request.getParameter("Cbo_Rep")); Sales Rep Changes
        strCaseId = GmCommonClass.parseNull(request.getParameter("Txt_CaseId"));
        strHoldFl = GmCommonClass.parseNull(request.getParameter("hHoldFl"));
        strHardcp_PO = GmCommonClass.parseNull(request.getParameter("Chk_HardPO"));
        strOverrideAttnTo = GmCommonClass.parseNull(request.getParameter("overrideAttnTo"));
        strShipInstruction = GmCommonClass.parseNull(request.getParameter("shipInstruction"));
        String strShowDOID = GmCommonClass.parseNull(rbCompany.getProperty("DO.SHOW_DO_ID"));
        strShipCharge = GmCommonClass.parseNull(request.getParameter("shipCharge"));
        String strLotOverrideFl = GmCommonClass.parseNull(request.getParameter("Chk_LotOverride"));
        strEGPSUsage = GmCommonClass.parseNull(request.getParameter("Cbo_egpsusage"));
        strLotOverrideFl = strLotOverrideFl.equals("on") ? "Yes" : "No";
        dtSurgDate =
            getDateFromStr(request.getParameter("Txt_SurgDate") == null ? "" : request
                .getParameter("Txt_SurgDate"));
        String strCurrSign =
            GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol"));
        // The Invoice total section of DO Summary screen is included in the main jsp to get it
        // based on company
        strIncJSPNM =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("DOSUMMARY", "JSPNM",
                strCompanyId));
        request.setAttribute("INCJSPNM", strIncJSPNM);

        if (strOrdType.equalsIgnoreCase("101260")) {
          strOrderId = gmCust.loadNextOrderId(strOrderId, strParentOrderId, strAccId, "ACKORD");
        } else {
          if (strShowDOID.equals("NO")) {
            strOrderId = gmCust.loadNextOrderId(strOrderId, strParentOrderId, strAccId, "DELORD");
          } else {
            strOrderId = gmCust.loadNextOrderId(strOrderId, strParentOrderId, strAccId, "");
          }
        }


        if (strOrderId.equals("DUPLICATE") || strOrderId.equals("NOPARENT")) {
          strErrMsg =
              strOrderId.equals("DUPLICATE") ? (strOpt.equals("QUOTE") ? "Quote already entered in the System"
                  : "DO already entered in the System")
                  : "Incorrect Parent DO for this Account";
          throw new AppError(strErrMsg, "", 'S');
        }
        //Commented for PCA-271 and calling new fetch from t5003_order_revenue_sample_dtls
        //alRevenueTrack = gmDORptBean.fetchOrderAttributeWithCodeNmInfo(strSessOrderId, "REVTP");
        alRevenueTrack  = gmCust.fetchRevenuetrackInfo(strSessOrderId);
        hmReturn.put("ALREVENUETRACK", alRevenueTrack);
        // Currently record the DDT number only Italy company
        String strUpdateDDTFl =
            GmCommonClass.parseNull(rbPaperwork.getProperty("DO.UPDATE_DDT_FL"));
        
        // PMT-45387: Provision to record DDT and usage LOT
        // Currently Used LOT section store (T502b) based on DDT flag. Going to change based on Usage LOT flag
        String strUsageLotFl =
            GmCommonClass.parseNull(rbPaperwork.getProperty("DO.SHOW_USAGE_LOT"));
        
        hmParam.put("ORDERID", strOrderId);
        hmParam.put("TOTAL", strTotal);
        hmParam.put("INPUTSTR", strInputStr);
        hmParam.put("ADJINPUTSTR", strAdjInputSrt);
        hmParam.put("USERID", strUserId);
        hmParam.put("REPNUM", strRepNum);
        hmParam.put("REPNAME", strRepName);
        hmParam.put("TYPE", strOrdType);
        hmParam.put("BILLTO", strBillTo);
        hmParam.put("MODE", strMode);
        hmParam.put("PERSON", strPerson);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("PO", strPO);
        hmParam.put("ACCID", strAccId);
        hmParam.put("CONCODE", strConCode);
        hmParam.put("PARENTORDID", strParentOrderId);
        // hmParam.put("SALESREP",strSalesRep); Sales Rep Changes
        hmParam.putAll(loadShipReqParams(request));
        hmParam.put("QUOTESTR", strQuoteStr);
        hmParam.put("CASEID", strCaseId);
        hmParam.put("CTRLNUMBERSTR", strCtrlNumberStr);// MNTTASK-8623 - CSM DO Process control #
                                                       // from S Part
        hmParam.put("HARDCPPO", strHardcp_PO);
        hmParam.put("OVERRIDEATTNTO", strOverrideAttnTo);
        hmParam.put("SHIPINSTRUCTION", strShipInstruction);
        hmParam.put("REQUIREDDT", strRequiredDt);
        hmParam.put("SHIPCHARGE", strShipCharge);
        hmParam.put("LOTOVERRIDEFL", strLotOverrideFl);
        hmParam.put("UPDATE_DDT_FL", strUpdateDDTFl);
        hmParam.put("USAGE_LOT_FL", strUsageLotFl);
        hmParam.put("gmDataStoreVO", getGmDataStoreVO());
        hmParam.put("SURGERYDT", dtSurgDate);
        hmParam.put("COMPANYINFO", strCompanyInfo);
        hmParam.put("STRTEMPID", strTempId);
        hmParam.put("EGPSUSAGE", strEGPSUsage);  //PC-4659 --Add EGPS Usage in DO - SpineIT New order

        log.debug("order params to save " + hmParam);
        // Get the procedure from the Interface to save the details based on company
        GmPlaceOrderInterface gmPlaceOrderInterface =
            (GmPlaceOrderInterface) GmCommonClass.getSpringBeanClass("xml/Place_Order_Extn_Beans.xml",
                getGmDataStoreVO().getCmpid());   // Place_Order_Extn_Beans - PC-4659 --Add EGPS Usage in DO - SpineIT New order
        hmReturn = gmPlaceOrderInterface.saveItemOrder(hmParam);

        // Moving Order to hold Status if is there any Price Descrepency
        hmHoldValues.put("ORDERID", strOrderId);
        hmHoldValues.put("HOLDFLAG", strHoldFl);
        hmHoldValues.put("STROPT", strOpt);
        hmHoldValues.put("USERID", strUserId);
        hmHoldValues.put("COMPANY_INFO",strCompanyInfo);
        //JMS call 
        gmOrderBean.updateHoldOrderStatus(hmHoldValues);

        hmReturn = gmCust.loadOrderDetails(strOrderId);
        hmReturn.put("STRSESSCURRSYMBOL", strCurrSign);
        // call method to send the email to the rep
        HashMap hmEmailData = new HashMap();
        hmEmailData = gmCust.sendPhoneOrderEmail(hmReturn);
        hmEmailData.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        hmEmailData.put("APPLNDATEFMT", strApplnDateFmt);
        hmOrderTotals = gmDOBean.fetchOrderTotalDetails(strOrderId);
        hmEmailData.putAll(hmOrderTotals);
        log.debug("3" + GmCommonClass.getString("GMJASPERLOCATION"));
        
        //JMS call to update the Acct net qty for Bill only sales consignment order
        //2532 : Bill only sales consignment order
        // PMT-14376
        if(strOrdType.equals("2532"))
        	
          {
        	 HashMap hmAcctParam = new HashMap ();	 	 	 
        	 hmAcctParam.put("TRANS_ID", strOrderId);	 	 	 
        	// hmAcctParam.put("REFID", strAccId);	 	 	 
        	 hmAcctParam.put("TRANS_TYPE", "50180");	 	 	 
        	 hmAcctParam.put("USER_ID", strUserId);	
        	 hmAcctParam.put("WAREHOUSE_TYPE", "5");
        	 hmAcctParam.put("COMPANY_INFO",strCompanyInfo);
        	 gmInvLocationBean.updateLoanerStockSheetInfo(hmAcctParam);
           }
        

        String strCountryCode = GmCommonClass.countryCode;// Getting the country code
        // Getting the jasper name based on QUOTE and ORDER and also based on country code getting
        // jasper name for ORDER
        String strJasperName = "";// strOpt.equals("QUOTE")?"/GmQuoteSummary.jasper":(!strCountryCode.equals("en")?"/GmOUSDeliveredOrderSummary.jasper":"/GmDeliveredOrderSummary.jasper");
        if (strOpt.equals("QUOTE")) {
          strJasperName = "/GmQuoteSummary.jasper";
        } else if (strCountryCode.equals("en") && strOrdType.equalsIgnoreCase("101260")) { // 101260[Acknowledgment
                                                                                           // Order]
          strJasperName = "/GmOUSDeliveredOrderSummary.jasper";
        } else if (!strCountryCode.equals("en")) {
          strJasperName = "/GmOUSDeliveredOrderSummary.jasper";
        } else if (strCountryCode.equals("en")) {
          strJasperName = "/GmDeliveredOrderSummary.jasper";
        }
        GmJasperMail jasperMail = new GmJasperMail();
        jasperMail.setJasperReportName(strJasperName);
        jasperMail.setAdditionalParams(hmEmailData);
        jasperMail.setReportData(null);
        jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));

        HashMap hmjasperReturn = jasperMail.sendMail();

        gmTxnSplitBean.sendVendorEmail(request, response, strApplnDateFmt, strOrderId);
        alComment = gmCommonBean.getLog(strOrderId, "1200");
        request.setAttribute("hmLog", alComment);
        hmReturn.put("ALCOMMENTS", alComment);

        String strOpenReturnfl = gmDOBean.fetchOpenReturnfl(strOrderId);
        strGSTstartFl = gmDOBean.getGSTStartFlag(strOrderId, "1200");//--1200--Orders
        request.setAttribute("GSTStartFl", strGSTstartFl);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "OrdPlaced");
        request.setAttribute("OPENRTFL", strOpenReturnfl);

        if (strOpt.equals("PhoneOrder")) {
          request.setAttribute("FromPage", strOpt);
        }
        ArrayList alTemp = new ArrayList();
        alTemp = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("CONSEQUENCES"));
        if (alTemp.size() > 0) {
          gmRuleEngine.sendRFSEmail(strOrderId, alTemp, "Order");
        }
        HashMap hmViewAccess = new HashMap();
        hmViewAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "VIEW_ADJ_DETAILS"));
        String strAdjViewFl = GmCommonClass.parseNull((String) hmViewAccess.get("UPDFL"));
        request.setAttribute("VIEWADJDETAILFL", strAdjViewFl);

        if (strOrdType.equalsIgnoreCase("101260")) {
          strDispatchTo = strCustPath.concat("/GmAckPrintPrice.jsp");
        } else {
          /*
           * Validate Account is on Credit Hold. if Account is Credit Hold Type1 then send Email
           * concern persons.
           */
          hmParam.put("EMAILTYPE", "ORDER");
          hmParam.put("ACCOUNTID", strAccId);
          hmParam.put("TXNID", strOrderId);
          gmCreditHoldEngine.validateCreditHoldAndSendEmail(hmParam);

          strDispatchTo =
              strOpt.equals("QUOTE") ? strCustPath.concat("/GmPrintQuote.jsp") : strCustPath
                  .concat("/GmPrintPrice.jsp");// Seperate JSP should be called for Quote order

        }
        
      //JMS call for Storing Building Info PMT-33510  
      hmStorageBuildInfo.put("TXNID", strOrderId);
      hmStorageBuildInfo.put("TXNTYPE", "ORDER");
      gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);    
      } else if (strAction.equals("PICSlip")) {
        strOrderId = GmCommonClass.parseNull(request.getParameter("hId"));
        String strShowJasperPicSlip =
            GmCommonClass.parseNull(rbPaperwork.getProperty("PICK_SLIP.SHOWJASPERPICSLIP"));
        String strUserName = GmCommonClass.parseNull(gmCommonBean.getUserName(strUserId));
        strSessOrderId = request.getParameter("hId");

        String strExpiryDateFl=
                GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "EXPIRY_DATE"));

        if (!strShowJasperPicSlip.equals("")) {
          hmReturn = gmCust.loadOrderDetails(strOrderId, "PACKSLIP");
        } else {
          hmReturn = gmCust.loadOrderDetails(strOrderId, "");
        }
        hmReturn.put("strExpiryDateFl", strExpiryDateFl);
        hmResult = (HashMap) hmReturn.get("ORDERDETAILS");
        strOrdType = GmCommonClass.parseNull((String) hmResult.get("ORDERTYPE"));
        log.debug("strOrdType in Servlet" + strOrdType);
        request.setAttribute("hmReturn", hmReturn);
        alReturn = gmCommonBean.getLog(strOrderId, "1200");
        String strReforward =
            GmCommonClass.parseNull(rbPaperwork.getProperty("PICK_SLIP.ORD_REFORWARD"));

        request.setAttribute("hmLog", alReturn);
        request.setAttribute("hAction", "OrdPlaced");
        request.setAttribute("RE_FORWARD", strReforward);
        request.setAttribute("RE_TXN", "50925");
        request.setAttribute("txnStatus", "PROCESS");
        ArrayList alDataList = (ArrayList) hmReturn.get("CARTDETAILS");
        hmResult.put("SIZE", alDataList.size());
        hmResult.put("LOGINNAME", strUserName);
        hmResult.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
                + "\\");
        request.setAttribute("HASHMAPDATA", hmResult);
        request.setAttribute("ARRAYLISTDATA", alDataList);

        String strShowDoOdrSlip =
            GmCommonClass.parseNull(rbPaperwork.getProperty("PICK_SLIP.SHOW_DO_ODR_SLIP"));
        String strShowQuoteJasperPrint =
            GmCommonClass.parseNull(rbPaperwork.getProperty("PICK_SLIP.SHOW_QUOTE_JASPER_PRINT"));

        if (strOrdType.equals("2520")) {
          if (strShowQuoteJasperPrint.equals("YES")) {
            ArrayList alItemDetails = gmCust.loadOrderAckItemDetails(strOrderId);
            HashMap hmOrderDetails = gmCust.loadOrderAckDetails(strOrderId);
            HashMap hmAtbData = gmAR.loadOrderAttribute(strOrderId, strOrdType);
            request.setAttribute("ITEMDETAILS", alItemDetails);
            request.setAttribute("ORDERDETAILS", hmOrderDetails);
            request.setAttribute("ORDER_ATB", hmAtbData);
            String strAckOdrSlipNm =
                GmCommonClass.parseNull(rbPaperwork.getProperty("PICK_SLIP.ACKODR"));
            request.setAttribute("ACKODRSLIPNM", strAckOdrSlipNm);
            request.setAttribute("ORDERTYPE", strOrdType);
            strDispatchTo = strCustPath.concat("/GmOrderAckSlipPrint.jsp");
          } else {
            strDispatchTo = strCustPath.concat("/GmOrderQuotePrint.jsp");
          }
        } else if ((strOrdType.equals("101260") || strOrdType.equals("2530"))
            && (strShowDoOdrSlip.equalsIgnoreCase("YES")))// || !strAckRefId.equals(""))
        {
          ArrayList alItemDetails = gmCust.loadOrderAckItemDetails(strOrderId);
          HashMap hmOrderDetails = gmCust.loadOrderAckDetails(strOrderId);
          HashMap hmAtbData = gmAR.loadOrderAttribute(strOrderId, strOrdType);
          HashMap hmBackOrderDetails = gmCust.loadOrderDetails(strSessOrderId);
          ArrayList alBackOrder = (ArrayList) hmBackOrderDetails.get("BACKORDER");
          HashMap hmOrderDetls = (HashMap) hmBackOrderDetails.get("ORDERDETAILS");
          String strAckOdrSlipNm = "";
          request.setAttribute("ITEMDETAILS", alItemDetails);
          hmOrderDetails.put(
              "SUBREPORT_DIR",
              request.getSession().getServletContext()
                  .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
          request.setAttribute("ORDERDETAILS", hmOrderDetails);
          request.setAttribute("ORDER_ATB", hmAtbData);
          request.setAttribute("BACKORDERDETAILS", alBackOrder);
          request.setAttribute("ALORDERDETAILS", hmOrderDetls);
          // GmJasperPicSlip.jsp dispatch is used to print the Jasper Pic Slip for BBA order

          if (strOrdType.equals("101260")) {
            strAckOdrSlipNm = GmCommonClass.parseNull(rbPaperwork.getProperty("PICK_SLIP.ACKODR"));
          } else {
            strAckOdrSlipNm = GmCommonClass.parseNull(rbPaperwork.getProperty("PICK_SLIP.DELODR"));
          }
          request.setAttribute("ACKODRSLIPNM", strAckOdrSlipNm);

          strDispatchTo =
              (strShowJasperPicSlip.equals("Y")) ? strCustPath.concat("/GmJasperPicSlip.jsp")
                  : "/gmRuleEngine.do";
        } else {
          // GmJasperPicSlip.jsp dispatch is used to print the Jasper Pic Slip for BBA order
          strDispatchTo =
              (strShowJasperPicSlip.equals("Y")) ? strCustPath.concat("/GmJasperPicSlip.jsp")
                  : "/gmRuleEngine.do";
        }
      } else if (strAction.equals("EditControl") || strAction.equals("EditShip")
          || strAction.equals("EditPO") || strAction.equals("EditPrice")
          || strAction.equals("ReturnPart") || strAction.equals("ACKORD")) {
        strOrderId = request.getParameter("hOrdId");
        strParentOrdId = GmCommonClass.parseNull(request.getParameter("hParentOrdId"));
        if (strOrderId == null) {
          strOrderId = (String) session.getAttribute("strSessOrderId");
        }
        strOrderId = strOrderId == null ? "" : strOrderId;
        strParentOrdId = strParentOrdId.equals("")?strOrderId:strParentOrdId;
        // find the access to change price for order
        if (strAction.equals("EditPrice")) {
          String strChangePriceFl =
              GmCommonClass.parseNull(gmCust.fetchOrderChangePriceFl(strOrderId));
          String strChnagePriceAccess =
              GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CHANGEPRICE",
                  "MODIFYACCESS", getGmDataStoreVO().getCmpid()));
          request.setAttribute("CHANGEPRICEFL", strChangePriceFl);
          request.setAttribute("CHGPRICACCESSFL", strChnagePriceAccess);
        }
        // Adding Call Log Information
        strLogReason =
            request.getParameter("Txt_LogReason") == null ? "" : request
                .getParameter("Txt_LogReason");

        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strOrderId, strLogReason, strUserId, "1200");
        }

        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "SHIP_COST_ACCESS"));
        strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        strReadAccess = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
        strVoidAccess = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
        if (strReadAccess.equals("Y")) {
          request.setAttribute("READFL", strReadAccess);
        }
        // Retrieving Comments information
        alReturn = gmCommonBean.getLog(strOrderId, "1200");
        request.setAttribute("hmLog", alReturn);

        // Retrieving Revenue tracking info
        alReturn = gmCust.fetchRevenuetrackInfo(strOrderId);
        log.debug("alReturn size " + alReturn.size());
        hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
        String strXmlData = generateOutPut(alReturn, hmApplnParam);
        request.setAttribute("XmlData", strXmlData);

        alAdjCodeList = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("PRIADJ"));
        request.setAttribute("ADJCODEDETAILS", alAdjCodeList);

        if (strAction.equals("EditPrice") && !strChangePriceAction.equals("") && strChangePriceAction.equals("CHANGEPRICE")) { //Code Change for PMT-38606
            hmReturn = gmCust.loadOrderDetails(strOrderId, strChangePriceAction);
        }else{
            hmReturn = gmCust.loadOrderDetails(strOrderId);
        }
        //to show the parent order info for child order in PO edit screen
          if(strAction.equals("EditPO")){
            alReturn = gmDOBean.fetchOrderSummary(strParentOrdId);
            hmReturn.put("PARENTORDERSUMMARY", alReturn);
          }   

        if (strAction.equals("ACKORD")) {
          // generate Ack order and save dropdown of Order Item - Acknowledgement Order
          // Edit(GmOrderItemControl.jsp)
          alOrderTypeList =
              GmCommonClass.parseNullArrayList(gmCommon.getCodeList("ACKOPT", getGmDataStoreVO()));
          hmParam.put("ALORDRTYPELIST", alOrderTypeList);
          request.setAttribute("hmParam", hmParam);

          hmParam.put("RELEASEORDERID",strOrderId);
          strValidateFlag = gmDOTxnBean.validateOusDistAck(hmParam);
          strOusAccessFlag = GmCommonClass.getRuleValue(strCompanyId,"EDIT_ACK"); 
           strReleaseQtyFlag = GmCommonClass.getRuleValue(strCompanyId,"ACK_RELEASE_QTY");  //PMT-30867 - ACK Screen Changes - get Release Qty value 
          log.debug("strOusAccessFlag"+strOusAccessFlag);
          // ArrayList alCart = (ArrayList) hmReturn.get("CARTDETAILS");
          ArrayList alCart =
              GmCommonClass.parseNullArrayList(gmDORptBean.fetchAckCartDetails(strOrderId));
          hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
          hmApplnParam.put("COMPANYLOCALE", strSessCompanyLocale);
          hmApplnParam.put("VALIDATEFLG", strValidateFlag); 
          hmApplnParam.put("OUSACCESS", strOusAccessFlag);
          strXmlData = generateOutPut(alCart, "GmAckOrderDet.vm", hmApplnParam);
          request.setAttribute("CARTDET", strXmlData);
          ArrayList alDelNote = gmCust.fetchDeliveryNotes(strOrderId);
          strXmlData = generateOutPut(alDelNote, "GmAckOrdDelNotes.vm", hmApplnParam);
          request.setAttribute("DELNOTES", strXmlData);
          ArrayList alReservQty = gmCust.fetchReservedQty(strOrderId);
          strXmlData = generateOutPut(alReservQty, "GmAckPartReserveDtls.vm", hmApplnParam);
          request.setAttribute("PARTRESERVE", strXmlData);
          request.setAttribute("VALIDATEFLAG", strValidateFlag); 
          request.setAttribute("RELEASEQTYFL", strReleaseQtyFlag);
        }
        if (strAction.equals("EditShip")) {
          hmResult = gmCust.loadShipping();
          request.setAttribute("hmShipLoad", hmResult);
        }
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);

        if (strAction.equals("ReturnPart")) {
          ArrayList alReturnReasons = gmCommon.getCodeList("RETRE");
          request.setAttribute("alReasons", alReturnReasons);
        }

        if (strAction.equals("EditPrice") || strAction.equals("ReturnPart")) {
          strDispatchTo = strCustPath.concat("/GmOrderItemModify.jsp");
        } else if (strAction.equals("EditPO")) {
          strDispatchTo = strCustPath.concat("/GmModifyOrderEditPO.jsp");
        } else {
          strDispatchTo = strCustPath.concat("/GmOrderItemControl.jsp");
          if (strAction.equals("EditControl")) {
            request.setAttribute("RE_FORWARD", "gmOrderItemControl");
            request.setAttribute("RE_TXN", "50925");
            request.setAttribute("txnStatus", "PROCESS");
            strDispatchTo = "/gmRuleEngine.do";
          }
        }
      } else if (strAction.equals("SaveControl")) {
        String strCount = "";
        String strBoFlag = "";
        String strRefId = "";
        int intCount = 0;
        strOrderId =
            request.getParameter("hOrderId") == null ? "" : request.getParameter("hOrderId");
        strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
        strShipFl = request.getParameter("Chk_ShipFl") == null ? "1" : "2";

        strLogReason =
            request.getParameter("Txt_LogReason") == null ? "" : request
                .getParameter("Txt_LogReason");

        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strOrderId, strLogReason, strUserId, "1200");
        }

        // Retrieving Comments information
        alReturn = gmCommonBean.getLog(strOrderId, "1200");
        request.setAttribute("hmLog", alReturn);

        intCount = Integer.parseInt(strCount);
        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum"
                  + i);
          if (!strPartNum.equals("")) {
            strQty = request.getParameter("Txt_Qty" + i);
            strQty = strQty.equals("") ? "0" : strQty;
            strControl = request.getParameter("Txt_CNum" + i);
            strControl = strControl.equals("") ? " " : strControl;
            strPrice = request.getParameter("hPrice" + i);
            strItemOrdType = GmCommonClass.parseNull(request.getParameter("hType" + i));
            strCapFlag = GmCommonClass.parseNull(request.getParameter("hCapFl" + i));
            strRefId = GmCommonClass.parseNull(request.getParameter("hRefId" + i));
            strInputString =
                strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",")
                    .concat(strControl).concat(",").concat(strItemOrdType).concat(",")
                    .concat(strRefId).concat(",").concat(strCapFlag).concat(",").concat(strPrice)
                    .concat(strBoFlag).concat("|");
          }
        }

        hmReturn = gmCust.updateItemOrder(strOrderId, strInputString, strShipFl, strUserId);

        hmReturn = gmCust.loadOrderDetails(strOrderId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditControl");
        request.setAttribute("hShipFl", strShipFl);
        strDispatchTo = strCustPath.concat("/GmOrderItemControl.jsp");
      }
      /*
       * else if (strAction.equals("SaveShip")) { strOrderId =
       * request.getParameter("hOrderId")==null?"":request.getParameter("hOrderId");
       * 
       * strShipDate = request.getParameter("Txt_SDate")== null
       * ?"":request.getParameter("Txt_SDate"); strShipCarr = request.getParameter("Cbo_ShipCarr")==
       * null ?"":request.getParameter("Cbo_ShipCarr"); strShipMode =
       * request.getParameter("Cbo_ShipMode")== null ?"":request.getParameter("Cbo_ShipMode");
       * strTrack = request.getParameter("Txt_Track")== null ?"":request.getParameter("Txt_Track");
       * strShipCost = request.getParameter("Txt_SCost")== null ||
       * request.getParameter("Txt_SCost").equals("")?"0":request.getParameter("Txt_SCost");
       * 
       * String strOrderAmt = request.getParameter("hOrderAmt")== null
       * ?"":request.getParameter("hOrderAmt"); String strAccName =
       * request.getParameter("hAccName")== null ?"":request.getParameter("hAccName");
       * 
       * hmValues.put("SDT",strShipDate); hmValues.put("SCAR",strShipCarr);
       * hmValues.put("SMODE",strShipMode); hmValues.put("STRK",strTrack);
       * hmValues.put("SCOST",strShipCost); hmValues.put("ORDAMT",strOrderAmt);
       * hmValues.put("ACCNM",strAccName);
       * 
       * hmReturn = gmCust.saveShipOrderDetails(hmValues,strOrderId,strUserId); hmReturn =
       * gmCust.loadOrderDetails(strOrderId);
       * 
       * hmResult = gmCust.loadShipping(); request.setAttribute("hmShipLoad",hmResult);
       * 
       * hmReturn = gmCust.loadOrderDetails(strOrderId); request.setAttribute("hmReturn",hmReturn);
       * request.setAttribute("hAction","EditShip"); request.setAttribute("hMode","SaveShip");
       * strDispatchTo = strCustPath.concat("/GmOrderItemControl.jsp");
       * 
       * }
       */
      else if (strAction.equals("SavePO") || strAction.equals("AckSavePO")) {
        String strCopay = GmCommonClass.parseNull(request.getParameter("Txt_Copay"));
        String strCapAmount = GmCommonClass.parseNull(request.getParameter("Txt_Cap_Amount"));
        String strCopayCapAction = GmCommonClass.parseNull(request.getParameter("hCopayCap"));
        strParentOrdId = GmCommonClass.parseNull(request.getParameter("hParentOrdId"));
        strOrderId = GmCommonClass.parseNull(request.getParameter("hOrderId"));
        strPO = GmCommonClass.parseNull(request.getParameter("Txt_PO"));
        strOrderComments = GmCommonClass.parseNull(request.getParameter("Txt_OrderComments"));
        strRequiredDt = GmCommonClass.parseNull(request.getParameter("Txt_ReqDate"));
        strInputString =
            request.getParameter("hInputString") == null ? "" : request
                .getParameter("hInputString");
        strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
        strPOAmt = GmCommonClass.parseNull(request.getParameter("Txt_POAmt"));
        String strPOStatus = GmCommonClass.parseNull(request.getParameter("POStatus"));
        String strDOStatus = GmCommonClass.parseNull(request.getParameter("DOStatus"));
        strParentOrdId = strParentOrdId.equals("")?strOrderId:strParentOrdId;
        
        strEditPOAccess = GmCommonClass.parseNull(request.getParameter("hEditPOAccess"));
        //PC-3880 New field in record PO Date in GM Italy
        strPODate =   GmCommonClass.parseNull(request.getParameter("Txt_PODate"));
        hmPoDetails.put("COPAY", strCopay);
        hmPoDetails.put("CAPAMOUNT", strCapAmount);
        hmPoDetails.put("COPAYCAPACTION", strCopayCapAction);
        hmPoDetails.put("ORDERID", strOrderId);
        hmPoDetails.put("PO", strPO);
        hmPoDetails.put("INPUTSTRING", strInputString);
        hmPoDetails.put("ORDERCOMMENTS", strOrderComments);
        hmPoDetails.put("POAMT", strPOAmt);
        hmPoDetails.put("USERID", strUserId);
        hmPoDetails.put("POSTATUS", strPOStatus);
        hmPoDetails.put("DOSTATUS", strDOStatus);
        //PC-3880 New field in record PO Date in GM Italy
        hmPoDetails.put("PODATE", strPODate);
        // if (strOpt.equals("SaveComments"))--Issue 4498
        // {
        // Adding Call Log Information
        strLogReason = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strOrderId, strLogReason, strUserId, "1200");
        }
        if (!strOpt.equals("SaveComments")) {// Issue 8339 : ............. in PO field {
          hmReturn = gmCust.saveCustPO(hmPoDetails);
        }
        // Retrieving Comments information
        alReturn = gmCommonBean.getLog(strOrderId, "1200");
        request.setAttribute("hmLog", alReturn);
        // Retrieving Revenue tracking info to populate after screen loads again
        alReturn = gmCust.fetchRevenuetrackInfo(strOrderId);
        log.debug("alReturn size " + alReturn.size());
        hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
        hmApplnParam.put("COMPANYLOCALE", strSessCompanyLocale);
        String strXmlData = generateOutPut(alReturn, hmApplnParam);
        request.setAttribute("XmlData", strXmlData);
        hmReturn = gmCust.loadOrderDetails(strOrderId);
        //PC-3880 New field in record PO Date in GM Italy
        // Account Currency based date format
            hmReturn.put("PODATE", GmCommonClass.getStringFromDate(
            (java.sql.Date) hmReturn.get("PODATE"), getGmDataStoreVO().getCmpdfmt()));
        if(strAction.equals("SavePO")){
          alReturn = gmDOBean.fetchOrderSummary(strParentOrdId);
          hmReturn.put("PARENTORDERSUMMARY", alReturn);
        }  
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("EDITPOACCESS",strEditPOAccess);
        
        
        if (strAction.equals("AckSavePO")) {
          // generate Ack order and save dropdown of Order Item - Acknowledgement Order
          // Edit(GmOrderItemControl.jsp)
          alOrderTypeList =
              GmCommonClass.parseNullArrayList(gmCommon.getCodeList("ACKOPT", getGmDataStoreVO()));
          hmParam.put("ALORDRTYPELIST", alOrderTypeList);
          request.setAttribute("hmParam", hmParam);
          
          hmParam.put("RELEASEORDERID",strOrderId);
          strValidateFlag = gmDOTxnBean.validateOusDistAck(hmParam);
          strOusAccessFlag = GmCommonClass.getRuleValue(strCompanyId,"EDIT_ACK");      
          log.debug("strOusAccessFlag1 "+strOusAccessFlag);

          // ArrayList alCart = (ArrayList) hmReturn.get("CARTDETAILS");
          ArrayList alCart =
              GmCommonClass.parseNullArrayList(gmDORptBean.fetchAckCartDetails(strOrderId));
          hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
          hmApplnParam.put("COMPANYLOCALE", strSessCompanyLocale);
          hmApplnParam.put("VALIDATEFLG", strValidateFlag); 
   	      hmApplnParam.put("OUSACCESS", strOusAccessFlag);
          strXmlData = generateOutPut(alCart, "GmAckOrderDet.vm", hmApplnParam);
          request.setAttribute("CARTDET", strXmlData);
          ArrayList alDelNote = gmCust.fetchDeliveryNotes(strOrderId);
          strXmlData = generateOutPut(alDelNote, "GmAckOrdDelNotes.vm", hmApplnParam);
          request.setAttribute("DELNOTES", strXmlData);
          ArrayList alReservQty = gmCust.fetchReservedQty(strOrderId);
          strXmlData = generateOutPut(alReservQty, "GmAckPartReserveDtls.vm", hmApplnParam);
          request.setAttribute("PARTRESERVE", strXmlData);
          request.setAttribute("VALIDATEFLAG", strValidateFlag); 
          request.setAttribute("hAction", "ACKORD");
        } else {
          request.setAttribute("hAction", "EditPO");
        }

        alReturn = gmCommonBean.getLog(strOrderId, "1200");
        request.setAttribute("hmLog", alReturn);
        if (strAction.equals("SavePO")) {
          strDispatchTo = strCustPath.concat("/GmModifyOrderEditPO.jsp");
        } else {
          strDispatchTo = strCustPath.concat("/GmOrderItemControl.jsp");
        }

      } else if (strAction.equals("SavePrice")) {
        String strCount = "";
        int intCount = 0;
        String strItemId = "";
        String strAdjCode = "";
        String strUnitPrice = "";
        String strUnitPriceAdj = "";
        double dbItemTotal = 0.0;
        double dbTotal = 0.0;
        double intQty = 0.0;

        strShipCost =
            request.getParameter("Txt_SCost") == null
                || request.getParameter("Txt_SCost").equals("") ? "0" : request
                .getParameter("Txt_SCost");
        strOrderId =
            request.getParameter("hOrderId") == null ? "" : request.getParameter("hOrderId");
        strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
        intCount = Integer.parseInt(strCount);

        for (int i = 0; i <= intCount; i++) {
          strItemId =
              request.getParameter("hItemId" + i) == null ? "" : request
                  .getParameter("hItemId" + i);
          if (!strItemId.equals("")) {
            strQty = request.getParameter("hQty" + i);
            strAdjCode = GmCommonClass.parseNull(request.getParameter("Cbo_AdjCode" + i));
            strUnitPrice = GmCommonClass.parseNull(request.getParameter("hUnitPrice" + i));
            strUnitPriceAdj = GmCommonClass.parseNull(request.getParameter("hUnitPriceAdj" + i));
            strQty = strQty.equals("") ? "0" : strQty;
            strPrice = request.getParameter("hPrice" + i);
            strPrice = strPrice.replaceAll("\\,", "");

            intQty = Double.parseDouble(strQty);
            dbItemTotal = Double.parseDouble(strPrice);
            dbItemTotal = intQty * dbItemTotal; // Multiply by Qty
            dbTotal = dbTotal + dbItemTotal;
            strInputString =
                strInputString.concat(strItemId).concat(",").concat(strPrice).concat(",")
                    .concat(strUnitPrice).concat(",").concat(strUnitPriceAdj).concat(",")
                    .concat(strAdjCode).concat("|");
          }
        }
        strTotal = "" + dbTotal; 
         if (!strChangePriceAction.equals("") && strChangePriceAction.equals("CHANGEPRICE")) { //Code Change for PMT-38606
        	hmReturn =
                    gmPriceBean.updateItemOrderPrice(strOrderId, strInputString, strShipCost, strTotal,
                        strUserId);

            hmReturn = gmCust.loadOrderDetails(strOrderId, strChangePriceAction);
         }else{
        	hmReturn =
                    gmCust.updateItemOrderPrice(strOrderId, strInputString, strShipCost, strTotal,
                        strUserId);

            hmReturn = gmCust.loadOrderDetails(strOrderId);
        }
        alAdjCodeList = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("PRIADJ"));
        request.setAttribute("ADJCODEDETAILS", alAdjCodeList);
        strLogReason = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));

        if (!strLogReason.equals("")) {
          gmCommonBean.saveLog(strOrderId, strLogReason, strUserId, "1200");
        }
        alReturn = gmCommonBean.getLog(strOrderId, "1200");

        strReadAccess = GmCommonClass.parseNull(request.getParameter("hReadFL"));

        request.setAttribute("READFL", strReadAccess);
        request.setAttribute("hmLog", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditPrice");
        strDispatchTo = strCustPath.concat("/GmOrderItemModify.jsp");
      } else if (strAction.equals("ProcessBO")) {
        String strRegnFilters = GmCommonClass.parseNull(request.getParameter("hRegnFilter"));
        String strZoneFilters = GmCommonClass.parseNull(request.getParameter("hZoneFilter"));
        String strDistFilters = GmCommonClass.parseNull(request.getParameter("hDistFilter"));
        // String strTerrFilter =
        // GmCommonClass.parseNull((String)request.getParameter("hTerrFilter"));
        String strPgrpFilters = GmCommonClass.parseNull(request.getParameter("hPgrpFilter"));
        String strDivFilters = GmCommonClass.parseNull(request.getParameter("hDivFilter"));
        String strCompFilters = GmCommonClass.parseNull(request.getParameter("hCompFilter"));
        if (strRegnFilters.equals("") && strZoneFilters.equals("") && strDistFilters.endsWith("")
            && strPgrpFilters.equals("") && strDivFilters.equals("")) {
          strFilters = "Y";
        }
        strCondition = getAccessCondition(request, response, true);
        hmParam.put("CONDITION", strCondition);
        hmParam.put("SETID", strPgrpFilters);
        hmParam.put("STRFILTER", strFilters);
        strOrderId = GmCommonClass.parseNull(request.getParameter("hOrderId"));
        strRptType = GmCommonClass.parseNull(request.getParameter("hRptType"));
        strCboType = GmCommonClass.parseNull(request.getParameter("Act_Type"));
        strTagId = GmCommonClass.parseNull(request.getParameter("Txt_TagID"));
        strAgingType = GmCommonClass.parseNull(request.getParameter("Cbo_Aging"));
        strAgingType = strAgingType == "" ? "00" : strAgingType;

        log.debug("strRptType " + strRptType);
        log.debug("strOrderId " + strOrderId);

        hmParam.put("TXNID", strOrderId);
        hmParam.put("USERID", strUserId);
        hmParam.put("COMPANYINFO", strCompanyInfo); 
        hmParam.put("TRANSTYPE", strRptType); 

        if (strRptType.equals("50261")) {
          hmReturn = gmCust.processBackOrder(hmParam);
        } else if (strRptType.equals("50266")) {
          gmLoanerBean.updateInhouseLoaner(hmParam);
        }
        strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
        HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
        strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);

        request.setAttribute("hmSalesFilters", hmSalesFilters);
        hmParam.put("AccessFilter", strAccessFilter);

        // strRptType = "50261";
        hmParam.put("RPTOPT", strRptType);
        hmParam.put("AGTYPE", strAgingType);

        String strRefId = GmCommonClass.parseNull(request.getParameter("hRefId"));
        String strScreenFrom = GmCommonClass.parseNull(request.getParameter("hScreen"));
        // Getting Tag id using CN ID from Process Transaction screen
        if (strTagId.equals("") && strScreenFrom.equals("PROCESS_TXN_SCN")) {
          strTagId = gmBackOrderReportBean.getTagIdFromTxnId(strRefId);
        }


        hmParam.put("TAGID", strTagId);
        hmParam.put("REFID", strRefId);
        alReturn = gmBackOrderReportBean.loadBackOrderReport(hmParam);
        hmReturn.put("BOREPORT", alReturn);

        alOrderList = GmCommonClass.getCodeList("SBOTP");
        hmReturn.put("ORDERLIST", alOrderList);
        hmReturn.put("CBOLIST", alCboList);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "ProcBO");
        request.setAttribute("hRptType", strRptType);
        request.setAttribute("hAgType", strAgingType);
        request.setAttribute("strTagId", strTagId);
        request.setAttribute("hScreen", strScreenFrom);
        request.setAttribute("hRefId", strRefId);
        strDispatchTo = strCustPath.concat("/GmBackOrderProcess.jsp");
      } else if (strAction.equals("Load") && strOpt.equals("PartBO")) {
        resultSet = gmBackOrderReportBean.loadBackOrderByPartReport(hmParam);
        request.setAttribute("BOREPORT", resultSet);
        strDispatchTo = strCustPath.concat("/GmBackOrderPart.jsp");
      } else if (strAction.equals("UpdateToBillOnly")) {
        strOrderId = GmCommonClass.parseNull(request.getParameter("hOrderId"));
        HashMap hmOrderValues = new HashMap();
        hmOrderValues.put("ORDERID", strOrderId);
        hmOrderValues.put("USERID", strUserId);
        gmBackOrderTransBean.updateBackOrderToBillOnly(hmOrderValues);
        throw new AppError(strOrderId
            + " is Successfully converted to Bill Only -From Sales Consignment", "", 'S');
      }
      else if (strAction.equals("HELDORDERPRICEREQ")) {
          strOrderId = GmCommonClass.parseNull(request.getParameter("hORDID"));
          alResult = gmPriceRequestRptBean.fetchOrderPriceRequest(strOrderId);
          request.setAttribute("ALRESULT", alResult);
          
          String strXmlData = generateOutPut(alResult, "GmPriceRequestOrders.vm", hmApplnParam);
          log.debug("strXmlData??"+strXmlData);
          request.setAttribute("PRICEREQTDET", strXmlData);
          strDispatchTo = strCustPath.concat("/GmPriceRequestPopup.jsp");

        }
      log.debug("the value inside ACKORD finally **" + strDispatchTo);
      gmCookieManager.setCookieValue(response, "TagColumn", strTagColumn);
      gmCookieManager.setCookieValue(response, "CapFlColumn", strCapFlColumn);
      strCompanyLocale = strCompanyLocale.equals("") ? "en_US" : "en_" + strCompanyLocale;
      
      request.setAttribute("CompanyLocale", strCompanyLocale);
      request.setAttribute("TagColumn", strTagColumn);
      request.setAttribute("CapFlColumn", strCapFlColumn);
     
      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      dispatch(strComnErrorPath, request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private String generateOutPut(ArrayList alGridData, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO",
        GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"))));
    templateUtil.setTemplateName("GmIncludeRevenueTrack.vm");
    templateUtil.setDropDownMaster("alStatList", GmCommonClass.getCodeList("REVST"));
    templateUtil.setDropDownMaster("alDOStatList", GmCommonClass.getCodeList("RSTDO"));
    return templateUtil.generateOutput();
  }

  private String generateOutPut(ArrayList alGridData, String strTemplate, HashMap hmParam)
      throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmAckOrdBOReport",
        GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"))));
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName(strTemplate);
    return templateUtil.generateOutput();
  }
  
  //Added to Prepare backorder parts string for inventory lookup
  private String generateBoParts(ArrayList alBOResult)
	      throws AppError {
	  HashMap<String, String> hmPartNos = new HashMap();
      ArrayList alReturnPartNos = new ArrayList(); 
      String strPartNos = "";
      int alBOResultSize = 0;
      Set<String> alReturnWithoutDuplicates;	  
	  for(int i = 0; i < alBOResult.size(); i++){
		  hmPartNos = (HashMap) alBOResult.get(i);
		  strPartNos = GmCommonClass.parseNull((String)hmPartNos.get("PNUM")); 
		  alReturnPartNos.add(strPartNos);
	  }
	   alReturnWithoutDuplicates = new LinkedHashSet<String>(alReturnPartNos);
	   alReturnPartNos.clear();
	   alReturnPartNos.addAll(alReturnWithoutDuplicates);
	   if(alReturnPartNos.size()>0){	
		   strPartNos = (String) alReturnPartNos.toString().replaceAll("\\[|\\]", "").replaceAll(" ","");
	   }
	   return strPartNos;
  }

  

}// End of GmOrderItemServlet
