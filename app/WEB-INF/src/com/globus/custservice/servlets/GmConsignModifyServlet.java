/*****************************************************************************
 * File			 : GmConsignModifyServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesCreditBean;
import com.globus.custservice.beans.GmConsignmentBean;

public class GmConsignModifyServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
		String strDispatchTo = strCustPath.concat("/GmConsignReprocess.jsp");

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				GmCommonClass gmCommon = new GmCommonClass();
				GmCustomerBean gmCust = new GmCustomerBean();
				GmConsignmentBean gmCons = new GmConsignmentBean();
				
				HashMap hmReturn = new HashMap();
				HashMap hmParam = new HashMap();
				String strType = "";
				String strReason = "";
				String strDistId = "";
				String strUserId = 	(String)session.getAttribute("strSessUserId");
				String strSetId = "";
				String strConId = "";
				String strhSetIds = "";
				String strSetTemp = "";
				String strSetTempReformat = "";
				String strInputStr = "";

				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				String strOpt = request.getParameter("hOpt");
				
				if (strOpt == null)
				{
					strOpt = (String)session.getAttribute("strSessOpt");
				}
				strOpt = (strOpt == null)?"":strOpt;

				if (strAction.equals("Load") && strOpt.equals("Reprocess"))
				{
					hmReturn = gmCust.loadReturnsDetails();
					request.setAttribute("hAction",strAction);
					request.setAttribute("hmReturn",hmReturn);
				}
				else if (strAction.equals("Reload") && strOpt.equals("Reprocess"))
				{
					strDistId = request.getParameter("Cbo_Id")==null?"":request.getParameter("Cbo_Id");
					strSetId = request.getParameter("Cbo_SetId")==null?"":request.getParameter("Cbo_SetId");
					hmReturn = gmCons.loadConsignReprocessLists(strDistId,strSetId);
					request.setAttribute("hmReturn",hmReturn);
					request.setAttribute("Cbo_Id",strDistId);
					request.setAttribute("Cbo_SetId",strSetId);
					strDispatchTo = strCustPath.concat("/GmConsignReprocess.jsp");
				}
				else if (strAction.equals("Initiate") && strOpt.equals("Reprocess"))
				{
					strSetId = request.getParameter("Cbo_SetId")==null?"":request.getParameter("Cbo_SetId");
					strConId = request.getParameter("hConId")==null?"":request.getParameter("hConId");
					strhSetIds = request.getParameter("hSetIds")==null?"":request.getParameter("hSetIds");
					hmReturn = gmCons.loadConsignReprocessData(strConId,strhSetIds);
					request.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hSetId",strSetId);
					request.setAttribute("hSetIds",strhSetIds);
					request.setAttribute("hConId",strConId);
					strDispatchTo = strCustPath.concat("/GmConsignReprocessData.jsp");
				}
				else if (strAction.equals("Save") && strOpt.equals("Reprocess"))
				{
					strConId = request.getParameter("hConId")==null?"":request.getParameter("hConId");
					strhSetIds = request.getParameter("hSetIds")==null?"":request.getParameter("hSetIds");
					StringTokenizer strTok = new StringTokenizer(strhSetIds,",");
					
					while (strTok.hasMoreTokens())
					{
						strSetTemp = strTok.nextToken();
						strSetTempReformat = strSetTemp.replaceAll("\\.","");
						strInputStr = request.getParameter("h"+strSetTempReformat);
						hmParam.put(strSetTemp,strInputStr);
					}
					
					strInputStr = GmCommonClass.parseNull(request.getParameter("hRetStr"));
					hmParam.put("RETURNS",strInputStr);
					strInputStr = GmCommonClass.parseNull(request.getParameter("hConStr"));
					hmParam.put("CONSIGN",strInputStr);
					
					hmReturn = gmCons.saveConsignReprocess(strConId,strhSetIds,hmParam,strUserId);
									
					request.setAttribute("hmReturn",hmReturn);
					strDispatchTo = strCustPath.concat("/GmConsignReprocessResults.jsp");
				}				
				request.setAttribute("hAction",strAction);
				dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmConsignModifyServlet