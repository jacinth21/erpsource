/*****************************************************************************
 * File : GmConsignItemServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmConsignPackSLipInterface;
import com.globus.custservice.beans.GmConsignPackSlipInstance;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.beans.GmInTransitBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.returns.beans.GmReturnsBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmConsignItemServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmItemConsignSetup.jsp");
    GmDataStoreVO gmDataStoreVO = null;
    try {
      // instantiate call inside the if condition is not working so overriding the
      // setCharacterEncoding
      request.setCharacterEncoding("UTF-8");
      response.setCharacterEncoding("UTF-8");
      String type = GmCommonClass.parseNull(request.getParameter("type"));
      String ruleCall = GmCommonClass.parseNull(request.getParameter("ruleEngCall"));
      String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      if (!type.equals("savePDFfile")) {
        HttpSession session = request.getSession(false);
        // checkSession(response, session); // Checks if the current session is valid, else
        // redirecting
        // to SessionExpiry page
        instantiate(request, response);
      }

      if (type.equals("savePDFfile")) {
        // When calling the JOB company info came as empty values.
        // at the time we are getting the default company information.
        if (strComapnyInfo.equals("")) {
          gmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
        } else {
          gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strComapnyInfo);
        }
      } else {
        gmDataStoreVO = getGmDataStoreVO();
      }

      GmJasperReport gmJasperReport = new GmJasperReport();
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = !type.equals("savePDFfile") ? (String) session.getAttribute("hAction") : "";
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strMode = request.getParameter("hMode") == null ? "" : request.getParameter("hMode");

      // String strOpt = (String)session.getAttribute("hOpt");

      String strOpt =
          !type.equals("savePDFfile") ? (String) session.getAttribute("strSessOpt") : "";
      strOpt = strOpt == null || strOpt.equals("") ? "" : strOpt;

      log.debug(" Action is " + strAction + " Mode is " + strMode + " Opt is " + strOpt
          + " type is " + type);

      GmCustomerBean gmCust = new GmCustomerBean(gmDataStoreVO);
      GmOperationsBean gmOper = new GmOperationsBean(gmDataStoreVO);
      GmInTransitBean gmIntransit = new GmInTransitBean(gmDataStoreVO);
      GmCustSalesSetupBean gmCustSetup = new GmCustSalesSetupBean(getGmDataStoreVO());
      GmReturnsBean gmReturn = new GmReturnsBean(getGmDataStoreVO());
      GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33513

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33513
      ArrayList alReturn = new ArrayList();
      String strConsignId = "";
      String strType = "";
      String strBillTo = "";
      String strBillComp = "";
      String strPurpose = "";
      String strShipTo = "";
      String strShipToId = "";
      String strComments = "";
      String strShipFl = "";
      String strUserId =
          !type.equals("savePDFfile") ? (String) session.getAttribute("strSessUserId") : "";
      String strPartNums = "";
      String strAddCartType = "";
      String strhPartNums = "";
      String strDelPartNum = "";
      String strInputStr = "";

      String strPartNum = "";
      String strTempPartNums = "";

      String strInputString = "";
      String strQty = "";
      String strPrice = "";
      String strControl = "";
      String strTransType = "";
      String strReTxn = "";
      String strAccess = "";
      String strRuleOpt = "";
      String strShowJasperPicSlip = "";
      String strValidationFl = "N";
      String strCheckRule = "";
      String strRuleEntryFl = "N";
      String strTxnStatus = "";
      String strTxType = "";
      String strTransIds = "";
      String strHtransIds = "";
      String strHtransPartNums = "";
      String strRowCount = "";
      int intTransQty = 0;
      int curTransQty = 0;
      int intExQty = 0;
      String strSrcTransIds = "";

      strCheckRule = GmCommonClass.parseNull((String) request.getAttribute("chkRule"));
      strCheckRule =
          strCheckRule.equals("") ? GmCommonClass.parseNull(request.getParameter("chkRule"))
              : strCheckRule;
      strTxnStatus = GmCommonClass.parseNull(request.getParameter("txnStatus"));
      // getting the company id
      String strCompanyId = gmDataStoreVO.getCmpid();
      String strCompanyLocale = "";
      strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
      // Locale
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      log.debug(" Action is " + strAction + " Opt is " + strOpt);

      // --CARTOP - Add to cart options dropdown
      ArrayList alQDList =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CARTOP", getGmDataStoreVO()));

      GmCommonBean gmCommonBean = new GmCommonBean(gmDataStoreVO);
      String strReForward =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PICK_SLIP.ITEM_REFORWARD"));
      strShowJasperPicSlip =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PICK_SLIP.COMP_CONSIGN"));
      // -- TO GET NUMBER OF ROWS ALLOWED TO ADD IN CART.
      strRowCount =
          GmCommonClass
              .parseNull(GmCommonClass.getRuleValue("ADD_TO_CART_ROWS", "INHOUSEROWCOUNT"));

      if (!type.equals("savePDFfile")) {
        session.setAttribute("CARTROWCOUNT", strRowCount);
      }

      if (strOpt.equals("")) {
        strRuleOpt = GmCommonClass.parseNull(request.getParameter("hType"));
      }
      if (strRuleOpt.equals(""))
        strRuleOpt = strOpt;
      else
        strOpt = strRuleOpt;
      log.debug(" Action is " + strAction + " Opt is " + strRuleOpt);

      hmParam.put("RULEID", strRuleOpt);
      HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
      String strRulesOpt = "";
      String strIncludeRuleEng = "";

      strRulesOpt = GmCommonClass.parseNull((String) hmTransRules.get("STR_OPT"));
      String strTransTypeID = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_ID"));
      request.setAttribute("TRANS_RULES", hmTransRules);
      if (!type.equals("savePDFfile")) {
        session.setAttribute("TRANS_RULES", hmTransRules);
      }
      strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
      // log.debug("hmTransRules" + hmTransRules);

      if (!strOpt.equals("") && hmTransRules != null) {
        // //Access Rule COde/////
        String strOperType = "INITIATE";
        strOperType =
            GmCommonClass.parseNull(request.getParameter("hMode")).equals("") ? strOperType
                : request.getParameter("hMode");
        strOperType = "FN_" + strOperType;
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(gmDataStoreVO);
        log.debug("User ID == " + strUserId);
        strOperType = (String) hmTransRules.get(strOperType);
        log.debug("Request URI == " + strOpt + "_" + strOperType);
        if (strOperType != null) {
          HashMap hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  strOperType));
          strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          request.setAttribute("ACCESSFL", strAccess);
          if (!type.equals("savePDFfile")) {
            session.setAttribute("ACCESSFL", strAccess);
          }
          if (!strAccess.equals("Y")) {
            throw new AppError("", "20685");
          }
        }
        // ////////////
      }
      if (strAction.equals("EditLoad") || strAction.equals("GoCart")
          || strAction.equals("UpdateCart") || strAction.equals("PlaceOrder")
          || strAction.equals("RemoveCart") || strAction.equals("UpdateOrder")) {
        strType = request.getParameter("Cbo_Type") == null ? "" : request.getParameter("Cbo_Type");
        strPurpose =
            request.getParameter("Cbo_Purpose") == null ? "0" : (String) request
                .getParameter("Cbo_Purpose");
        strBillTo =
            request.getParameter("Cbo_BillTo") == null ? "" : request.getParameter("Cbo_BillTo");
        strShipTo =
            request.getParameter("Cbo_ShipTo") == null ? "" : request.getParameter("Cbo_ShipTo");
        strShipToId =
            request.getParameter("Cbo_Values") == null ? "0" : request.getParameter("Cbo_Values");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strShipFl = request.getParameter("Chk_ShipFlag") == null ? "0" : "1";

        hmParam.put("TYPE", strType);
        hmParam.put("PURPOSE", strPurpose);
        hmParam.put("BILLTO", strBillTo);
        hmParam.put("SHIPTO", strShipTo);
        hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("SHIPFL", strShipFl);
        hmParam.put("QDLIST", alQDList);
        session.setAttribute("PARAM", hmParam);
      }
      if (strAction.equals("Load") && strOpt.equals("")) {
        hmReturn = gmCust.loadConsignLists(strOpt, hmTransRules);
        hmReturn.put("CONLISTS", hmReturn);
        session.setAttribute("hmReturn", hmReturn);
        session.setAttribute("hAction", strAction);
      } else if (strAction.equals("Load") && strOpt.equals("InHouse") || strAction.equals("Load")
          && strOpt.equals("Repack") || strAction.equals("Load") && strOpt.equals("QuaranIn")
          || strAction.equals("Load") && strOpt.equals("QuaranOut") || strAction.equals("Load")
          && strOpt.equals("InvenIn") || strAction.equals("Load") && strOpt.equals(strRulesOpt)) {
        hmReturn = gmCust.loadConsignLists(strOpt, hmTransRules);
        hmReturn.put("QDLIST", alQDList);
        hmReturn.put("CONLISTS", hmReturn);
        session.setAttribute("hmReturn", hmReturn);
        session.setAttribute("hAction", strAction);
      } else if (strAction.equals("EditLoad")) {
        strConsignId = gmCust.loadNextConsignId(strOpt);
        log.debug("strConsignID : ==> " + strConsignId + "  strOpt=> " + strOpt);
        session.setAttribute("CONSIGNID", strConsignId);
        session.setAttribute("hAction", strAction);
      } else if (strAction.equals("GoCart")) {
        strAddCartType =
            request.getParameter("hAddCartType") == null ? "106457" : request
                .getParameter("hAddCartType");// -- get add cart type from dropdown , is its null
                                            // 106457- By part
        strPartNums =
            request.getParameter("Txt_PartNum") == null ? "" : request.getParameter("Txt_PartNum");
        strhPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");
        strTransType = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
        strBillTo = GmCommonClass.parseNull(request.getParameter("Cbo_BillTo"));
        strBillComp = gmIntransit.fetchOwnerCompID(strBillTo); 
        hmParam.put("BILLCOMP", strBillComp);
        log.debug("Part Num & PartNums ==>" + strPartNums + " <---> " + strhPartNums);
        if (strhPartNums != null && !strhPartNums.equals("")) {
          strPartNums = strhPartNums.concat(",").concat(strPartNums);
        }
        if ((strTransType.equals("400085") && strAddCartType.equals("106457"))
        		|| (strTransType.equals("400086") && strAddCartType.equals("106457"))) {
          HashMap hmDistDetails = gmCustSetup.loadEditDistributor(strBillTo);
          hmParam.put("PARENTCOMPID",
              GmCommonClass.parseNull((String) hmDistDetails.get("PARENTCOMPID")));
          hmParam.put("PARTNUM", strPartNums);
          hmParam.put("TYPE", strTransType);
          log.debug("<--->" + strBillTo + " <---> " + strTransType);    
          hmReturn = gmCust.loadCartForConsign(hmParam);
        } else {
          if (strAddCartType.equals("106458")) { // -- 106458 - By transaction type
            // fetch values by transaction id.
            // we have one text field for both transaction(By part and By transaction), for
            // variation using strSrcTransIds as temp variable
            strSrcTransIds = strPartNums;
            hmReturn = gmReturn.loadPartInfoFromTransId(strSrcTransIds, strTransType);
            session.setAttribute("ADDCARTTYPE", strAddCartType);
          } else {// -- 106457 - By part type
            hmReturn = gmCust.loadCartForConsign(strPartNums);
          }

        }
        session.setAttribute("hmOrder", hmReturn);
        session.setAttribute("hAction", "PopOrd");
        session.setAttribute("strPartNums", strPartNums);
      } else if (strAction.equals("UpdateCart") || strAction.equals("PlaceOrder")
          || strAction.equals("RemoveCart") || strAction.equals("UpdateOrder")) {
        // log.debug("1");
        strPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");
        strDelPartNum =
            request.getParameter("hDelPartNum") == null ? "" : request.getParameter("hDelPartNum");
        // log.debug("12");
        strAddCartType =
            request.getParameter("hAddCartType") == null ? "106457" : request
                .getParameter("hAddCartType");// -- get add cart type from dropdown , is its null
                                              // 106457- By part
        HashMap hmTempOrd = (HashMap) session.getAttribute("hmOrder");
        HashMap hmTempCart = new HashMap();

        StringTokenizer strTok = null;
        // 106458- type is transacton by
        if (strAddCartType.equals("106458")) {

          String strTransPart =
              request.getParameter("hTransPartNums") == null ? "" : request
                  .getParameter("hTransPartNums");
          // -- get transaction part details keys to iterate transaction details
          strTok = new StringTokenizer(strTransPart, ","); // --
        } else {
          strTok = new StringTokenizer(strPartNums, ",");
        }

        HashMap hmLoop = new HashMap();
        // log.debug("3");
        int i = 0;

        while (strTok.hasMoreTokens()) {
          hmLoop = new HashMap();
          i++;
          strPartNum = strTok.nextToken();
          if (strDelPartNum.equals(strPartNum)) {
            hmTempOrd.remove(strPartNum);
          } else {
            strQty =
                request.getParameter("Txt_Qty" + i) == null ? "" : request.getParameter("Txt_Qty"
                    + i);
            strPrice =
                request.getParameter("hPrice" + i) == null ? "" : request
                    .getParameter("hPrice" + i);
            strControl = "";
            hmLoop.put("QTY", strQty);
            hmLoop.put("PRICE", strPrice);
            hmLoop.put("CONTROL", strControl);
            hmTempCart.put(strPartNum, hmLoop);
            // 106458- transaction by type
            // if type is 106458 and action is PlaceOrder , getting control number and transaction
            // quantity
            if (strAddCartType.equals("106458") && strAction.equals("PlaceOrder")) {
              strPartNum =
                  request.getParameter("hPNum" + i) == null ? "" : request
                      .getParameter("hPNum" + i);
              strControl =
                  request.getParameter("hCtlNum" + i) == null ? "" : request.getParameter("hCtlNum"
                      + i);// -- get control number from transaction
              intTransQty =
                  Integer.parseInt(request.getParameter("hOldTransQty" + i) == null ? "0" : request
                      .getParameter("hOldTransQty" + i));// -- get transaction qty from transaction
              curTransQty = Integer.parseInt(strQty);// -- get cart qty
              // Checks cart qty is greater than source transaction qty
              if (curTransQty > intTransQty) {
                // -- subtraction cart qty and source transaction qty to get added qty
                intExQty = curTransQty - intTransQty;

                // seperate remaining qty with control number as empty
                strInputStr =
                    strInputStr
                        + strPartNum.concat(",").concat(Integer.toString(intTransQty)).concat(",")
                            .concat(strControl).concat(",").concat(strPrice).concat("|")
                            .concat(strPartNum).concat(",").concat(Integer.toString(intExQty))
                            .concat(",").concat("").concat(",").concat(strPrice);

              } else {
                strInputStr =
                    strInputStr
                        + strPartNum.concat(",").concat(strQty).concat(",").concat(strControl)
                            .concat(",").concat(strPrice);
              }

            } else {
              strInputStr =
                  strInputStr
                      + strPartNum.concat(",").concat(strQty).concat(",").concat(strControl)
                          .concat(",").concat(strPrice);
            }


            strInputStr = strInputStr.concat("|");
            strTempPartNums = strTempPartNums.concat(strPartNum);
            strTempPartNums = strTempPartNums.concat(",");
          }
        }
        // log.debug("4");
        if (!strTempPartNums.equals("")) {
          strTempPartNums = strTempPartNums.substring(0, strTempPartNums.length() - 1);
        }
        // log.debug("5");
        // if type is 106458 and action is RemoveCart , add transaction part keys and transaction id
        if (strAddCartType.equals("106458") && strAction.equals("RemoveCart")) {
          // strSrcTransIds is temp variable for source transaction ids
          strSrcTransIds = strPartNums;
          hmTempOrd.put("TRANSPARTNUMS", strTempPartNums);
          hmTempOrd.put("TRANSIDS", strSrcTransIds);
        }
        session.setAttribute("hmCart", hmTempCart);
        session.setAttribute("hmOrder", hmTempOrd);
        // log.debug("6");
        if (strAction.equals("RemoveCart")) {
          if (strAddCartType.equals("106458")) {
            // if type is 106458 - By transaction, add transaction partnum in session
            session.setAttribute("strPartNums", strPartNums);
          } else {
            // if type is 106457 - By part

            session.setAttribute("strPartNums", strTempPartNums);
          }
          session.setAttribute("hAction", "PopOrd");

        } else if (strAction.equals("PlaceOrder")) {
          strConsignId = (String) session.getAttribute("CONSIGNID");
          log.debug(" Consignment Id is " + strConsignId + " INput string  " + strInputStr);
          log.debug(" Params to save " + hmParam);
          hmReturn = gmCust.saveItemConsign(strConsignId, hmParam, strInputStr, strUserId);

          if (strAddCartType.equals("106458")) { // -- 106458 - By transaction
            // save copy transaction details to T414_TRANSACTION_LOG table
            // strSrcTransIds is temp variable for source transaction ids
            strSrcTransIds = strPartNums;
            gmReturn.saveItemConsignByTrans(strConsignId, strSrcTransIds, strUserId);
          }

          session.setAttribute("hAction", "OrdPlaced");
          // session.removeAttribute("strSessOpt");
        //JMS call for Storing Building Info PMT-33513 
          log.debug("Storing building information GmConsignItemServlet ");
          hmStorageBuildInfo.put("TXNID", strConsignId); //consignment id
          hmStorageBuildInfo.put("TXNTYPE", "INHOUSE504");
          gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);  
          
        } else if (strAction.equals("UpdateOrder")) {

        }
        // log.debug("7");
      } else if (strAction.equals("PrintVersion")) {
        strConsignId = request.getParameter("hId") == null ? "" : request.getParameter("hId");

        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));

        hmResult = gmCust.loadConsignDetails(strConsignId, strUserId);
        hmReturn.put("SETDETAILS", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strCustPath.concat("/GmSetConsignPrint.jsp");
        strOpt = "";
      } else if (strAction.equals("Ack")) {
        strConsignId = request.getParameter("hId") == null ? "" : request.getParameter("hId");

        hmResult = gmCust.loadConsignAckDetails(strConsignId);
        hmReturn.put("ACKDETAILS", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        strDispatchTo = strCustPath.concat("/GmConsignAckPrint.jsp");
        strOpt = "";
      } else if (strAction.equals("SplitValues")) { // Method to split the FG,QN and RP Values from
                                                    // intransit transactions (PMT-18235)
        String fgInpStr = "";
        String rqInpStr = "";
        String rpInpStr = "";

        fgInpStr =
            request.getParameter("fgInpStr") == null ? "" : (String) request
                .getParameter("fgInpStr");
               rqInpStr =
            request.getParameter("qnInpuStr") == null ? "" : (String) request
                .getParameter("qnInpuStr");
               rpInpStr =
            request.getParameter("rpInpStr") == null ? "" : (String) request
                .getParameter("rpInpStr");
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
        hmReturn.put("CONDETAILS", hmResult);
        hmResult.put("FGINPUTSTR", fgInpStr);
        hmResult.put("QNINPUTSTR", rqInpStr);
        hmResult.put("RPINPUTSTR", rpInpStr);
        hmResult.put("CONSIGNID", strConsignId);
        hmResult.put("USERID", strUserId);
        HashMap hmSuceesMsg = new HashMap();
        hmSuceesMsg = gmIntransit.intrasitSplitValues(hmResult);
        String strMsg = (String) hmSuceesMsg.get("INTRAVAL");
        if (strMsg != null) {
          session.setAttribute("hType", "S");
          throw new AppError(strMsg, "", 'S');
        } else {
          session.setAttribute("hType", "E");
          throw new AppError(strMsg, "", 'E');
        }

      } else if (strAction.equals("IntransBuildSet")) { // Method to save the set values in
                                                        // Intransit transactions tab (PMT-18235)
        HashMap hmparam = new HashMap();
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        String strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));
        hmparam.put("CONSIGNMENTID", strConsignId);
        hmparam.put("SETID", strSetId);
        hmparam.put("USERID", strUserId);
        gmIntransit.intrasitBuildSet(hmparam);
        strDispatchTo =
            "/GmConsignItemServlet?&hAction=EditControl&CTYPE=40057&HTYPEVAL=INTRA&BtnVal=Y&hConsignId="
                + strConsignId;
      } else if ((strAction.equals("EditControl"))) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
        hmReturn.put("CONDETAILS", hmResult);
        String strBtnval = GmCommonClass.parseNull(request.getParameter("BtnVal"));
        String strITQNRP = GmCommonClass.parseNull(request.getParameter("ITFGQNRP"));
        String strHType = GmCommonClass.parseNull(request.getParameter("HTYPEVAL"));
        HashMap hm = new HashMap();
        hm.put("DETAILS", strBtnval);
        hm.put("INTRAVALUES", strITQNRP);

        if ((strRuleOpt.equals("40057")) || (strTransType.equals("40057"))
            || (strTransType.equals("106703")) || (strTransType.equals("106704"))) { // This
                                                                                     // condition
          // is used to
          // fetch
          // intransit code
          // look up values
          // and dispatch
          // to Jsp page
          // (PMT-18235)

          hmResult = gmOper.loadSavedSetMasterDetails(strConsignId);
          hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
          hmResult = gmOper.viewBuiltSetDetails(strConsignId);
          strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
          hmReturn.put("CONDETAILS", hmResult);
          hmReturn.put("CONDETAILS12", hm);
          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hAction", "EditLoad");
          request.setAttribute("hConsignId", strConsignId);
          alReturn = gmCommonBean.getLog(strConsignId, "1268");
          request.setAttribute("hmLog", alReturn);
          HashMap hmIntraCodeLookup = gmIntransit.fetchIntraCodelookupValues();
          ArrayList alListreg = (ArrayList) hmIntraCodeLookup.get("INTRASET");
          ArrayList alList = (ArrayList) hmIntraCodeLookup.get("INTRACONSIGNMENT");
          request.setAttribute("INTRANSIT_CODE_DETAILS", alList);
          request.setAttribute("INTRANSIT_CODE_DETAILS_REGULAR", alListreg);
          HashMap hmReturnval = new HashMap();
          hmReturnval = (HashMap) alList.get(0);
          request.setAttribute("HMRETURNVAL", hmReturnval);
          HashMap hmReturnvalue = new HashMap();
          hmReturnvalue = (HashMap) alListreg.get(0);
          request.setAttribute("HMRETURNVALUE", hmReturnvalue);
          strDispatchTo = strCustPath.concat("/GmInTransitEdit.jsp");

        } else {


          hmResult = gmOper.loadSavedSetMaster(strConsignId);
          hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
          hmResult = gmOper.viewBuiltSetDetails(strConsignId);
          strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
          hmReturn.put("CONDETAILS", hmResult);

          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hAction", "EditLoad");
          request.setAttribute("hConsignId", strConsignId);
          alReturn = gmCommonBean.getLog(strConsignId, "1268");
          log.debug("alReturn====>" + alReturn);
          request.setAttribute("hmLog", alReturn);
          strDispatchTo = strCustPath.concat("/GmItemConsignEdit.jsp");
          log.debug("strTransType....." + strTransType);
          if (strTransType.equals("4114") || strTransType.equals("4113")
              || strTransType.equals("4116") || strTransType.equals("4110")
              || strTransType.equals("4112") || strTransType.equals("4115")
              || !strIncludeRuleEng.equalsIgnoreCase("NO")) {
            if (!strTransType.equals("")) {
              strTransType = gmRuleBean.fetchTransId(strTransType, "0");
            }
            strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;
            log.debug("strTransType = " + strTransType);
            request.setAttribute("RE_FORWARD", "gmItemConsignEdit");
            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", "PROCESS");
            request.setAttribute("TxnTypeID", strTransTypeID);
            strDispatchTo = "/gmRuleEngine.do";
          }
        }
      } else if (strAction.equals("SaveControl")) {
        String strCount = "";
        int intCount = 0;
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
        strShipFl =
            request.getParameter("Chk_ShipFl") == null ? "2" : request.getParameter("Chk_ShipFl");
        strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
        strComments = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        intCount = Integer.parseInt(strCount);
        if (strTransType.equals("")) {
          strTransType =
              request.getParameter("hConsignType") == null ? "" : request
                  .getParameter("hConsignType");
        }
        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum"
                  + i);
          log.debug("strPartnum" + strPartNum + "strComments" + strComments);
          if (!strPartNum.equals("")) {
            strQty = request.getParameter("Txt_Qty" + i);
            strQty = strQty.equals("") ? "0" : strQty;
            strControl = request.getParameter("Txt_CNum" + i);
            strControl = strControl.equals("") ? " " : strControl;
            strPrice = request.getParameter("hPrice" + i);
            strInputString =
                strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",")
                    .concat(strControl).concat(",").concat(strPrice).concat("|");
          }
          log.debug("strInputString" + strInputString);
        }
        log.debug("in item servlet SAVE.......");
        /*
         * First need to validations the lot number before calling rule engine or calling the actual
         * procedure Once all the validations are done, then call the rule engine and fi rule
         * messages are not there it will save the details in actual procedure
         */
        hmParam.put("TYPE", strTransType);
        strValidationFl = gmOper.saveItemLotNum(strConsignId, hmParam, strInputString, strShipFl);
        if (strTransType.equals("4115")) {
          strTxType = request.getParameter("RE_TXN") == null ? "" : request.getParameter("RE_TXN");
        }

        if (strValidationFl.equalsIgnoreCase("N") || strValidationFl.equals("")) {

          if (!strTransType.equals("") && !strIncludeRuleEng.equalsIgnoreCase("NO")
              && strCheckRule.equalsIgnoreCase("Yes")) {

            strRuleEntryFl = "Y";

            request.setAttribute("RE_TXN", strTxType);
            request.setAttribute("txnStatus", strTxnStatus);
            request.setAttribute("TxnTypeID", strTransType);
            request.setAttribute("RE_FORWARD", "gmConsignItemServlet");
            request.setAttribute("chkRule", "No");
            dispatch("/gmRuleEngine.do", request, response);
            return;// Should not proceed once go to rule engine
          }

          hmReturn = gmCust.updateItemConsign(strConsignId, strInputString, strShipFl, strUserId);

        } else {
          strShipFl = "0";
        }

        if (!strComments.equals("")) {
          gmCommonBean.saveLog(strConsignId, strComments, strUserId, "1268");
        }
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        alReturn = gmCommonBean.getLog(strConsignId, "1268");
        log.debug("alReturn====>" + alReturn);
        request.setAttribute("hmLog", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
        request.setAttribute("hConsignId", strConsignId);
        request.setAttribute("hShipFl", strShipFl);
        if (strShipFl.equals("3"))
          request.setAttribute("disable", "disabled=\"disable\"");
        strDispatchTo = strCustPath.concat("/GmItemConsignEdit.jsp");
        if (strTransType.equals("4114") || strTransType.equals("4113")
            || strTransType.equals("4116") || strTransType.equals("4110")
            || strTransType.equals("4112") || strTransType.equals("4115")
            || !strIncludeRuleEng.equalsIgnoreCase("NO")) {
          strTransType = gmRuleBean.fetchTransId(strTransType, "0");
          strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;
          log.debug("strTransType = " + strTransType);
          if (!strRuleEntryFl.equals("Y")) {
            request.setAttribute("RE_FORWARD", "gmItemConsignEdit");
            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", "PROCESS");
            request.setAttribute("TxnTypeID", strTransTypeID);
            strDispatchTo = "/gmRuleEngine.do";
          }
        }
      } else if (strAction.equals("LoadItemShip")) {
        // strConsignId =
        // (String)session.getAttribute("strSessConsignId")==null?"":(String)session.getAttribute("strSessConsignId");
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmCust.loadConsignDetails(strConsignId, strUserId);
        hmReturn.put("SETDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", "SHIP");
        hmResult = gmCust.loadShipping();
        request.setAttribute("hmShipLoad", hmResult);
        strDispatchTo = strCustPath.concat("/GmConsignShip.jsp");
      } else if (strAction.equals("PicSlip")) {

        ArrayList alDataList = new ArrayList();
        strReTxn = "50911"; // Re-Packaging
        String strSource = GmCommonClass.parseNull(request.getParameter("ruleSource"));
        String strAddId = GmCommonClass.parseNull(request.getParameter("addressid"));
        String strRemVoidFl = GmCommonClass.parseNull(request.getParameter("hRemVoidFl"));
        String strPlantId = getGmDataStoreVO().getPlantid();
        
        String strExpiryDateFl=
                GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "EXPIRY_DATE"));
        hmReturn.put("ExpiredShipDateFl", strExpiryDateFl);

        // hRemVoidFl parameter is coming from status log report.This report should open PicSlip for
        // voided txns also.
        log.debug("ruleSource .......= " + strSource);
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        hmParam.put("CONSIGNID", strConsignId);
        hmParam.put("REMVOIDFL", strRemVoidFl);
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
        String strClassName =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CONSIGNPACKSLIP",
                "FETCH_CLASS", strCompanyId));
        if (strShowJasperPicSlip.equalsIgnoreCase("Y") && strSource.equals("50181")) {
          GmConsignPackSLipInterface consignpackSlipClient =
              GmConsignPackSlipInstance.getInstance(strClassName);
          hmResult = consignpackSlipClient.loadSavedSetMaster(strConsignId);
          alDataList = (ArrayList) hmResult.get("SETLOAD");
        } else if (strTransType.equals("40057") || strTransType.equals("106703") // 40057 :ICT ,106703: FG-ST ,106704 : BL-ST
            || strTransType.equals("106704")) {
          hmResult = gmOper.loadSavedSetMasterDetails(hmParam);
        } else {
          hmResult = gmOper.loadSavedSetMaster(hmParam);
        }
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(hmParam);
        alReturn = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strConsignId, "1268"));
        if (alReturn.size() > 0) {
          for (int i = 0; i < alReturn.size(); i++) {
            HashMap hmLog = (HashMap) alReturn.get(i);
            strComments += GmCommonClass.parseNull((String) hmLog.get("COMMENTS"));
            strComments += "<BR>";
          }
          request.setAttribute("CRCOMMENTS", strComments);
        }
        log.debug("hmResult = " + hmResult);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("source", "50181"); // consign
        if (strSource.equals("50181")) {
          strReTxn = "SHIPOUTWRAPPER";
          request.setAttribute("ADDRESSID", strAddId);
          request.setAttribute("SOURCE", strSource);
          request.setAttribute("REFID", strConsignId);
        } else if (strSource.equals("4110") || strSource.equals("4112") || strSource.equals("4113")
            || strSource.equals("4114") || strSource.equals("4116")) {
          strReTxn = gmRuleBean.fetchTransId(strSource, "0");
        }
        // For Showing the Pic Slip in Jasper Report for BBA for Consignment
        if (strShowJasperPicSlip.equalsIgnoreCase("Y") && strSource.equals("50181")) {
          request.setAttribute("HASHMAPDATA", hmResult);
          request.setAttribute("ARRAYLISTDATA", alDataList);
          strReForward = "gmConsignJasperPicSlip";
        }

        if (type.equals("savePDFfile") && ruleCall.equals("Y")) {
          String strCNPickSlipPDFLocation = GmCommonClass.getString("BATCH_CN_PICKSLIP_PRINT");
          String strSlipHeader = "Pick Slip";
          String strdetails = "Transaction Details";
          // String strCrComments=
          // GmCommonClass.parseNull((String)request.getAttribute("CRCOMMENTS"));
          String strToHeader = "";
          boolean flg = false;

          ArrayList alSetLoad =
              GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("SETLOAD"));
          HashMap hmConsignDetails = (HashMap) hmReturn.get("CONDETAILS");
          GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(gmDataStoreVO);
          strConsignId = GmCommonClass.parseNull((String) hmConsignDetails.get("CID"));
          String strSetId = GmCommonClass.parseNull((String) hmConsignDetails.get("SETID"));
          String strSetName = GmCommonClass.parseNull((String) hmConsignDetails.get("SETNM"));
          String strAccName = GmCommonClass.parseNull((String) hmConsignDetails.get("ANAME"));
          String strDistName = GmCommonClass.parseNull((String) hmConsignDetails.get("DNAME"));
          strAccName = strAccName.equals("") ? strDistName : strAccName;
          String strUserName = GmCommonClass.parseNull((String) hmConsignDetails.get("UNAME"));
          String strIniDate = GmCommonClass.parseNull((String) hmConsignDetails.get("CDATE"));
          String strDesc = GmCommonClass.parseNull((String) hmConsignDetails.get("COMMENTS"));
          strType = GmCommonClass.parseNull((String) hmConsignDetails.get("TYPE"));
          String strCtype = GmCommonClass.parseNull((String) hmConsignDetails.get("CTYPE"));
          String strInHousePurpose = GmCommonClass.parseNull((String) hmConsignDetails.get("PURP"));
          String strBillNm = GmCommonClass.parseNull((String) hmConsignDetails.get("HISTBILLNM"));
          String strRequestorName =
              GmCommonClass.parseNull((String) hmConsignDetails.get("SHIPADD"));
          String strConsignStatusFlag =
              GmCommonClass.parseNull((String) hmConsignDetails.get("SFL"));
          String strCarrier = GmCommonClass.parseNull((String) hmConsignDetails.get("CARRIER"));
          strMode = GmCommonClass.parseNull((String) hmConsignDetails.get("SHIPMODE"));
          String strTrackno = GmCommonClass.parseNull((String) hmConsignDetails.get("TRACKNO"));
          String strEtchId = GmCommonClass.parseNull((String) hmConsignDetails.get("ETCHID"));
          strEtchId = strEtchId.equals("") ? "-" : strEtchId;
          String strRefId = GmCommonClass.parseNull((String) hmConsignDetails.get("REFID"));
          String strLog_Comments =
              GmCommonClass.parseNull((String) hmConsignDetails.get("LOG_COMMENTS"));
          strCompanyId = GmCommonClass.parseNull((String) hmConsignDetails.get("COMPANYID"));

          if (strCtype.equals("50154")) {// Move from Shelf to Loaner Extension w/Replenish
            strToHeader = "Replenished To";
          } else if (strCtype.equals("9110") || strCtype.equals("1006572")
              || strCtype.equals("1006571") || strCtype.equals("1006570")
              || strCtype.equals("1006573") || strCtype.equals("50150")
              || strCtype.equals("1006575")) {// ISMP,ISBO,BLIS,IHIS,FGIS,IHLE
            strToHeader = "Loaned To";
          } else {
            strToHeader = "Consigned To";
          }
          if (strConsignStatusFlag.equals("4") && strCtype.equals("50154")) {// Packslip
            strSlipHeader = "Packing Slip";
            strdetails = "Transaction Details";
            flg = true;
          }
          if (strCtype.equals("100062")) {
            strRequestorName = "<b><font color=\"red\">Back Order</font></b>";
          }
          if (strConsignStatusFlag.equals("2") || strConsignStatusFlag.equals("3")
              || strCtype.equals("50154")) {
            strDesc += "<BR>" + strComments + "<BR>" + strLog_Comments;
          }

          String strId = strConsignId + "$" + strSource;
          String strRuleId = strConsignId + "$" + strCtype;

          HashMap hmJasperDetails = new HashMap();
          hmJasperDetails.put("COMPID", strCompanyId);
          hmJasperDetails.put("HEADERNAME", strSlipHeader);
          hmJasperDetails.put("CONSIGNID", strConsignId);
          hmJasperDetails.put("SOURCE", strSource);
          hmJasperDetails.put("CTYPE", strCtype);
          hmJasperDetails.put("TYPE", strType);
          hmJasperDetails.put("INHOUSEPURPOSE", strInHousePurpose);
          hmJasperDetails.put("CONSIGNSTATUSFLAG", strConsignStatusFlag);
          hmJasperDetails.put("CONSIGNEDTOHDR", strToHeader);
          hmJasperDetails.put("BILLTONAME", strBillNm);
          hmJasperDetails.put("ACCNAME", strAccName);
          hmJasperDetails.put("SHIPTONAME", strRequestorName);
          hmJasperDetails.put("REQDATE", strIniDate);
          hmJasperDetails.put("REFID", strRefId);
          hmJasperDetails.put("INITIATEDBY", strUserName);
          hmJasperDetails.put("SHIPCARRIER", strCarrier);
          hmJasperDetails.put("SHIPMODE", strMode);
          hmJasperDetails.put("TRACKNO", strTrackno);
          hmJasperDetails.put("TRANSDETAILSHDR", strdetails);
          hmJasperDetails.put("SETNAME", strSetName);
          hmJasperDetails.put("SETID", strSetId);
          hmJasperDetails.put("ETCHID", strEtchId);
          hmJasperDetails.put("SHIPPINGFLAG", flg);
          hmJasperDetails.put("COMMENTS", strDesc);

          /*
           * String strPrintedBy =
           * GmCommonClass.parseNull((String)session.getAttribute("strSessShName")); String
           * strPrintedDate =
           * GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate")); String
           * strApplnDateFmt =
           * GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt")); Date
           * dtPrintDate = (Date)GmCommonClass.getStringToDate(strPrintedDate, strApplnDateFmt);
           * hmJasperDetails.put("PRINTEDBY",strPrintedBy);
           * hmJasperDetails.put("PRINTEDDATE",GmCommonClass
           * .getStringFromDate(dtPrintDate,strPrintedDate));
           */
          int intSize = alSetLoad.size();
          if (intSize > 0) {
            for (int i = 0; i < intSize; i++) {
              HashMap hcboVal = new HashMap();
              hcboVal = GmCommonClass.parseNullHashMap((HashMap) alSetLoad.get(i));
              // Get the part desc with TM symbol.
              hcboVal.put("PDESC", GmCommonClass.getStringWithTM((String) hcboVal.get("PDESC")));
              alSetLoad.set(i, hcboVal);
            }
          }
          if (intSize == 0) {
            HashMap hmTemp = new HashMap();
            hmTemp.put("PNUM", "");
            hmTemp.put("PDESC",
                "<font color='#FF0000'>Transaction does not contain any part numbers !</font>");
            hmTemp.put("IQTY", "");
            hmTemp.put("CNUM", "");
            alSetLoad.add(hmTemp);
          }

          File newPDFFile = new File(strCNPickSlipPDFLocation);
          if (!newPDFFile.exists()) {
            newPDFFile.mkdirs();
          }
          ArrayList alRuleInfo = new ArrayList();
          int alRuleInfoSize = 0;
          int alPartyListSize = 0;
          if (ruleCall.equals("Y")) {
            //
            alRuleInfo =
                GmCommonClass.parseNullArrayList((ArrayList) session.getAttribute("CONSEQUENCES"));
            alRuleInfoSize = alRuleInfo.size();
            String strPartyId =
                GmCommonClass.parseNull((String) request.getSession()
                    .getAttribute("strSessPartyId"));
            ArrayList alPartyList = gmAccessControlBean.getPartyList("CNOVERRIDE", strPartyId);
            alPartyListSize = alPartyList.size();
          }
          hmJasperDetails.put("ALRULEINFO", alRuleInfo);
          hmJasperDetails.put("ALRULEINFOSIZE", alRuleInfoSize);
          hmJasperDetails.put("ALPARTYLISTSIZE", alPartyListSize);
          hmJasperDetails.put("SUBREPORT_DIR", request.getSession().getServletContext()
              .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
              + "\\");
          // to form the file location and name.
          String strPDFFileName = strCNPickSlipPDFLocation + "\\" + strConsignId + ".pdf";
          gmJasperReport.setRequest(request);
          gmJasperReport.setResponse(response);
          gmJasperReport.setJasperReportName("/GmItemPicSlipPrint.jasper");
          gmJasperReport.setHmReportParameters(hmJasperDetails);
          gmJasperReport.setReportDataList(alSetLoad);
          gmJasperReport.exportJasperReportToPdf(strPDFFileName);
        } else {
          if (!strIncludeRuleEng.equalsIgnoreCase("NO")) {

            if (type.equals("savePDFfile")) {
              request.setAttribute("RE_FORWARD", "gmConsignItemServlet");
              strDispatchTo = "/gmRuleEngine.do?ruleEngCall=Y&hAction=PicSlip&type=savePDFfile";
            } else {
              request.setAttribute("RE_FORWARD", strReForward);
              strDispatchTo = "/gmRuleEngine.do";
            }
            request.setAttribute("RE_TXN", strReTxn);
            request.setAttribute("txnStatus", "PROCESS");
            request.setAttribute("TxnTypeID", strTransTypeID);

          } else {
            if (strReForward.equals("gmItemPicSlip")) {
              strDispatchTo = strCustPath.concat("/GmItemPicSlip.jsp");
            } else {
              strDispatchTo = strCustPath.concat("/GmItemPicSlipGlobal.jsp");
            }
          }

        }
        strOpt = "";
      } else if (strAction.equals("Load") && strOpt.equals("DummyTransfer")) {
        // log.debug("Entering Dummy Consignment Setup");
        HashMap hmTemp = new HashMap();
        hmTemp = gmCust.loadConsignLists(strOpt, hmTransRules);
        hmParam.put("TYPE", "4129");
        hmParam.put("SHIPTO", "4120");
        hmParam.put("SHIPFL", "1");
        hmReturn.put("CONLISTS", hmTemp);
        session.setAttribute("hmReturn", hmReturn);
        session.setAttribute("hAction", strAction);
        session.setAttribute("PARAM", hmParam);
        session.setAttribute("source", "Dummy");
        session.setAttribute("fieldStatus", "disabled");

        // log.debug("Exiting Dummy Consignment Setup");
      } else if (strAction.equals("ByTransaction")) {// validation call for transaction by type
        strTransIds = request.getParameter("Src_Trans_Id");
        strType = request.getParameter("Trans_Type");
        String strErrMsg = gmReturn.validateCopyTransactions(strTransIds, strType);
        strOpt = "";
        response.setContentType("text/xml");
        PrintWriter pw = response.getWriter();
        pw.write(strErrMsg);
        pw.flush();
        pw.close();
        strDispatchTo = null;
      }
      strOpt = GmCommonClass.parseNull(strOpt);

      log.debug("strOpt==>" + strOpt + "  strRulesOpt==>" + strRulesOpt);
      if (strMode.equals("InHouse") || strOpt.equals("InHouse") || strMode.equals("Repack")
          || strOpt.equals("Repack") || strMode.equals("QuaranIn") || strOpt.equals("QuaranIn")
          || strMode.equals("QuaranOut") || strOpt.equals("QuaranOut") || strOpt.equals("InvenIn")
          || (!strOpt.equals("") && strOpt.equals(strRulesOpt))) {
        strDispatchTo = strCustPath.concat("/GmInHouseItemConsignSetup.jsp");
      }

      log.debug("strDispatchTo==> " + strDispatchTo);

      if (strAction.equals("EditControl") || strAction.equals("SaveControl")
          || strAction.equals("LoadItemShip") || strAction.equals("PicSlip")) {
        if (type.equals("savePDFfile") && strAction.equals("PicSlip") && ruleCall.equals("Y")) {
          log.debug("strDispatchTo==>not applicable for job");
          ObjectOutputStream sendStream = null;
          sendStream = new ObjectOutputStream(response.getOutputStream());
          log.error("sendStream is:" + sendStream);
          sendStream.close();
          log.error("sendStream is closed");
        } else {
          if (type.equals("savePDFfile")) {
            session = request.getSession(true);
          }
          strCompanyLocale = strCompanyLocale.equals("") ? "en_US" : "en_" + strCompanyLocale;
          request.setAttribute("CompanyLocale", strCompanyLocale);
          dispatch(strDispatchTo, request, response);
        }
      } else {
        gotoPage(strDispatchTo, request, response);
      }
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

}// End of GmConsignItemServlet
