/*****************************************************************************
 * File : GmProcessCreditsServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmOrderUsageBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.beans.GmSalesCreditBean;
import com.globus.corporate.beans.GmDivisionBean;


public class GmProcessCreditsServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  /**
   * GmProcessCreditsServlet - service method
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strCustPath.concat("/GmInvoiceList.jsp");
    String strInvSource = "";
    String strGSTstartFl ="";
    String strUsedLotFl = "";
    String strShowDDTFl = "";

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");

      strInvSource =
          request.getParameter("Cbo_InvSource") == null ? "" : request
              .getParameter("Cbo_InvSource");
      if (request.getParameter("Cbo_InvSource") == null) {
        strInvSource =
            GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
                getGmDataStoreVO().getCmpid());

      }

      String strIdAccount =
          request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");

      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
        session.removeAttribute("hAction");
      }

      strAction = (strAction == null || strAction.equals("")) ? "Load" : strAction;
      log.debug(" Action value is " + strAction);

      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmSalesCreditBean gmSalesCredit = new GmSalesCreditBean(getGmDataStoreVO());
      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      GmARBean gmAR = new GmARBean(getGmDataStoreVO());
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
      GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alType = new ArrayList();
      ArrayList alDateType = new ArrayList();
      RowSetDynaClass rdReturnRowSet = null;
      HashMap hmLoadReturn = new HashMap();
      HashMap hmParam = new HashMap();

      String strSessOrderId = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strItemsId = "";
      String strAccId = "";
      String strFormat = "";
      String strInvId = "";
      String strPO = "";
      String strhStr = "";
      String strRAId = "";
      String strComments = "";
      String strCreditDate = "";
      String strAccountTpe = "";
      String strStatusType = "";
      String strCustPO = "";
      String strInvoiceID = "";
      String strOrderID = "";
      String strFromDate = "";
      String strToDate = "";
      String strDateType = GmCommonClass.parseNull(request.getParameter("Cbo_DateType"));
      HashMap hmValues = new HashMap();
      strAccId = request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
      HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      String strAccCurrencyID = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccCurrencyID = strAccCurrencyID.equals("0") ? strCompanyCurrId : strAccCurrencyID;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccCurrencyID);
      ArrayList alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();
      
      ArrayList alCompDivList = new ArrayList(); //PMT-41835

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      // Locale
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
     // to call separate JSP for Chennai/Delhi--getting the file name from property
      String strFrwdJSPName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INV_GENERATE_JSP")); 
      strUsedLotFl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_USAGE_LOT"));
      strShowDDTFl  = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.UPDATE_DDT_FL"));
      
      alDateType = GmCommonClass.getCodeList("INDTTY", getGmDataStoreVO());

      alType = GmCommonClass.getCodeList("INVSR", getGmDataStoreVO());
      request.setAttribute("ALTYPE", alType);
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));
      
      //PMT-41835 Division Filter in the Invoice Report
      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));
    //Set the alCompDivList values in request

      if (strAction.equals("Load")) {
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
      } else if (strAction.equals("Reload")) {
        strAccId =
            request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
        strFormat =
            request.getParameter("Cbo_Action") == null ? "" : request.getParameter("Cbo_Action");
        strAccountTpe =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("ACCOUNT_TYPE", "PROCESS_CREDIT"));// From
                                                                                                  // Rule
                                                                                                  // We
                                                                                                  // are
                                                                                                  // getting
                                                                                                  // Account
                                                                                                  // Type
                                                                                                  // Value
        String strActivefl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));

        strStatusType =
            request.getParameter("Cbo_Type") == null ? "0" : (String) request
                .getParameter("Cbo_Type");
        hmParam.put("STATUSTYPE", strStatusType);
        request.setAttribute("Cbo_Type", strStatusType);
        
        strCustPO = GmCommonClass.parseNull(request.getParameter("Txt_PO"));
        hmParam.put("CUSTPO", strCustPO);

        strInvoiceID = GmCommonClass.parseNull(request.getParameter("Txt_InvoiceID"));
        hmParam.put("INVID", strInvoiceID);

        strOrderID = GmCommonClass.parseNull(request.getParameter("Txt_OrderID"));
        hmParam.put("ORDERID", strOrderID);

        strFromDate = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
        hmParam.put("hFrom", strFromDate);

        strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
        hmParam.put("hTo", strToDate);
        
        //PMT-41835 Division Filter in the Invoice Report

        String strDivision = GmCommonClass.parseNull(request.getParameter("Cbo_Division"));
        hmParam.put("DIVISIONID", strDivision);
        //PMT-41835 Division Filter in the Invoice Report

        hmParam.put("PARENTFL", strActivefl);
        hmParam.put("ACCID", strAccId);
        hmParam.put("FORMAT", strFormat);
        hmParam.put("INVSOURCE", strInvSource);
        hmParam.put("SRC", "MODIFYINV");
        hmParam.put("ACCOUNT_CURR_ID", strAccCurrencyID);
        hmParam.put("DATETYPE", strDateType);
        log.debug(" hashmap values " + hmParam);
        /*
         * if(strInvSource.equals("26240213")){ rdReturnRowSet = (RowSetDynaClass)
         * gmSalesCredit.loadUnpaidDealerInvoice(hmParam); } else{
         */
        rdReturnRowSet = (RowSetDynaClass) gmSalesCredit.loadUnpaidInvoice(hmParam);
        // }


        hmReturn.put("ACCOUNTORDERLIST", rdReturnRowSet);
        alReturn = new ArrayList();
        alReturn = gmSales.reportAccountByType(strAccountTpe);
        hmReturn.put("ACCOUNTLIST", alReturn);
        // To make Check box as active or inactive after loading the report.Default it s
        strActivefl = strActivefl.equals("on") ? "true" : "false";
        
        // request.setAttribute("TYPE", strStatusType);
        request.setAttribute("CUSTPO", strCustPO);
        request.setAttribute("INVOICEID", strInvoiceID);
        request.setAttribute("ORDERID", strOrderID);
        request.setAttribute("FROMDATE", getDateFromStr(strFromDate));
        request.setAttribute("TODATE", getDateFromStr(strToDate));

        request.setAttribute("INVSOURCE", strInvSource);
        request.setAttribute("IDACCOUNT", strIdAccount);
        request.setAttribute("hMode", strFormat);
        request.setAttribute("hAccId", strAccId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "Reload");
        request.setAttribute("results", rdReturnRowSet);
        request.setAttribute("Chk_ParentFl", strActivefl);
        request.setAttribute("DIVISIONID",strDivision);
      }
      // Load Issue Credit Screen
      else if (strAction.equals("IssueCredit") || strAction.equals("IssueDebit")) {
        strInvId = request.getParameter("hInvId") == null ? "" : request.getParameter("hInvId");
        strAction =
            request.getParameter("Cbo_Action1") == null ? "" : request.getParameter("Cbo_Action1");
        strAccId = request.getParameter("hAccId") == null ? "" : request.getParameter("hAccId");
        strPO = request.getParameter("hPO") == null ? "" : request.getParameter("hPO");
        log.debug(" Invoice ID inside Issue Credit is " + strInvId);
        //getting the GST Start date flag
        strGSTstartFl = gmDOBean.getGSTStartFlag(strInvId,"1201");
        request.setAttribute("strGSTStartFl", strGSTstartFl);
        // Changing load Invoice Details to get input as Invoice ID
        hmReturn = gmAR.loadInvoiceDetails(strInvId, strAction);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strAcctPath.concat(strFrwdJSPName);
      }

      // This Condition is statisfied when the user clicks on
      // "Issue Credit". Generated the Invoice
      else if (strAction.equals("CRE")) {
        strInvId = request.getParameter("hInv") == null ? "" : request.getParameter("hInv");
        strPO = request.getParameter("hPO") == null ? "" : request.getParameter("hPO");
        strAccId = request.getParameter("hAccId") == null ? "" : request.getParameter("hAccId");
        strhStr = request.getParameter("hStr") == null ? "" : request.getParameter("hStr");
        strOpt = request.getParameter("strOpt") == null ? "" : request.getParameter("strOpt");
        strCreditDate =
            request.getParameter("Txt_Credit_Date") == null ? "" : (String) request
                .getParameter("Txt_Credit_Date");

        strComments =
            request.getParameter("Txt_Details") == null ? "" : request.getParameter("Txt_Details");
        hmParam = new HashMap();
        hmParam.put("INVID", strInvId);
        hmParam.put("ACCID", strAccId);
        hmParam.put("HINPUTSTR", strhStr);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("USERID", strUserId);
        hmParam.put("PO", strPO);
        hmParam.put("INVTYPE", strOpt);

        hmReturn = gmSalesCredit.saveCreditforSales(hmParam);

        String strNewInvoiceId = GmCommonClass.parseNull((String) hmReturn.get("NEWINVID"));
        String strAdjustmentOrderId = GmCommonClass.parseNull((String) hmReturn.get("RAID"));
        
        // to update sales tax
        GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean(getGmDataStoreVO());
        String strErrorCustPo = gmSalesTaxTransBean.updateSalesTax(strNewInvoiceId, strUserId);
        if (!strErrorCustPo.equals("")) {
          throw new AppError("", "20667");
        }
        String strGenerateXmlFl =
            GmCommonClass.parseNull(GmCommonClass.getAccountAttributeValue(strAccId, "92000"));
        // if Company is creating the XML file
        if (strGenerateXmlFl.equals("Y")) {
          GmInvoiceXmlInterface gmInvoiceXmlInterface =
              (GmInvoiceXmlInterface) GmCommonClass.getSpringBeanClass("xml/Invoice_Xml_Beans.xml",
                  getGmDataStoreVO().getCmpid());

          gmInvoiceXmlInterface.generateInvocieXML(strNewInvoiceId, strUserId);
        }
        
        // PMT-45387: Provision to record DDT number
        // to call the used lot details
        
        String strUsaedLotStr = GmCommonClass.parseNull((String)request.getParameter("hUsedLotStr"));
        
        if(! strUsaedLotStr.equals("")){
        	GmOrderUsageBean gmOrderUsageBean = new GmOrderUsageBean (getGmDataStoreVO()); 
        	gmOrderUsageBean.saveUsageLotDtls(strAdjustmentOrderId, strUsaedLotStr, strUserId);
        }
        
        //getting the GST start date flag
        strGSTstartFl = gmDOBean.getGSTStartFlag(strNewInvoiceId,"1201");
        request.setAttribute("strGSTStartFl", strGSTstartFl);
        // Changing load Invoice Details to get input as Invoice ID
        hmReturn = gmAR.loadInvoiceDetails(strNewInvoiceId, "PAY");

        request.setAttribute("hAction", "PAY");
        request.setAttribute("hmReturn", hmReturn);
      
        strDispatchTo = strAcctPath.concat(strFrwdJSPName);


        // strRAId = (String)hmReturn.get("RAID");
        // hmReturn = gmSalesCredit.loadCreditReport(strRAId,"PrintVersion");
        // request.setAttribute("hmReturn",hmReturn);
        // request.setAttribute("hAction","ViewReturn");
        // strDispatchTo = strCustPath.concat("/GmReturnsView.jsp");
      }
      // to set the value to request.
      request.setAttribute("INVSOURCE", strInvSource);
      request.setAttribute("ACC_CURR_ID", strAccCurrencyID);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      request.setAttribute("DATETYPE", alDateType);
      request.setAttribute("hDateType", strDateType);
      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
      request.setAttribute("USAGE_LOT_CODE_FL", strUsedLotFl);
      request.setAttribute("SHOW_DDT_FL", strShowDDTFl);


      request.setAttribute("ALCOMPDIVLIST",alCompDivList);//PMT-41835
      if (strAction.equals("CQ")) {
        gotoPage(strDispatchTo, request, response);
      } else {
        dispatch(strDispatchTo, request, response);
      }

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmProcessCreditsServlet
