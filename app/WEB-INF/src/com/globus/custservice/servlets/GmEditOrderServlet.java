/*****************************************************************************
 * File : GmEditOrderServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmARBean;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmDOReportBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.beans.GmLotExpiryBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmEditOrderServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strOperPath.concat("/GmEditOrder.jsp");

    try {
      String type = GmCommonClass.parseNull(request.getParameter("type"));
      String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      GmDataStoreVO tmpGmDataStoreVO = null;

      HashMap hmLotOverrideAccess = new HashMap();
      String strLotOverrideUpdAccess = "";
      String strLotOrderideAcess = "N";
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      GmRuleEngine gmRuleEngine = new GmRuleEngine();
     
      hmLotOverrideAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "LOT_OVERRIDE_CHECK"));
      strLotOverrideUpdAccess = GmCommonClass.parseNull((String) hmLotOverrideAccess.get("UPDFL"));
      if (strLotOverrideUpdAccess.equals("Y")) {
        strLotOrderideAcess = "Y";
      }
      request.setAttribute("LOTOVERRIDEACCESS", strLotOrderideAcess);
      if (!type.equals("savePDFfile")) {
        HttpSession session = request.getSession(false);
        checkSession(response, session); // Checks if the current session is valid, else redirecting
        // redirecting
        // to SessionExpiry page
        instantiate(request, response);
      }
      if (type.equals("savePDFfile")) {
        // When calling the JMS some times company info came as empty values.
        // at the time we are getting the default company information.
        if (strComapnyInfo.equals("")) {
          tmpGmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
        } else {
          tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strComapnyInfo);
        }
      } else {
        tmpGmDataStoreVO = getGmDataStoreVO();
      }
      GmDOReportBean gmDOReportBean = new GmDOReportBean(tmpGmDataStoreVO);
      GmLotExpiryBean gmLotExpiryBean = new GmLotExpiryBean(tmpGmDataStoreVO);
      String strMode = request.getParameter("hMode");

      if (strMode == null && type.equals("")) {
        strMode = (String) session.getAttribute("hMode");
        session.removeAttribute("hMode");
      }

      strMode = (strMode == null) ? "" : strMode;

      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(tmpGmDataStoreVO);
      GmCommonBean gmCommonBean = new GmCommonBean();
      GmJasperReport gmJasperReport = new GmJasperReport();
      GmARBatchBean gmARBatchBean = new GmARBatchBean(tmpGmDataStoreVO);
      GmDOBean gmDOBean = new GmDOBean(tmpGmDataStoreVO);
      GmARBean gmAR = new GmARBean(tmpGmDataStoreVO);
      GmCustomerBean gmCustomerBean = new GmCustomerBean(tmpGmDataStoreVO);

      HashMap hmReturn = new HashMap();
      HashMap hmLoadReturn = new HashMap();
      String strId = "";
      String strSessOrderId = "";
      String strUserId =
          !type.equals("savePDFfile") ? (String) session.getAttribute("strSessUserId") : "";
      String strSessDeptId =
          !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session
              .getAttribute("strSessDeptId")) : "";
      String strItemsId = "";
      String strShipDate = "";
      String strShipCarr = "";
      String strShipMode = "";
      String strTrack = "";
      String strItemOrdId = "";
      String strCounter = "";
      String strQty = "";
      String strPartNum = "";
      String strContNum = "";
      String strPrice = "";
      String strPO = "";
      String strProjectType = "";
      String strSource = "";
      String strParantForm = "";
      String strhidePratPrice = "";
      String strFwdFlag = "";
      // getting the company id
      String strCompanyId = tmpGmDataStoreVO.getCmpid();
      String strCompanyLocale = "";
      String strOrderPackForward = "";
      String strOrdPackJasName = "";
      String strRuleDOmain = "";
      String strAccountType = "";
      String strAccountId = "";
      String strCommercialInvoice = "";
      String strPlantId = "";
      String strIncJSPNM = "";
      String strCodeID = "";
      String strCodeNm = "";
      String strSurgeryDt = "";
      String strGSTStartFl = "";
      String strSterileExpDateRule = "";

      int intOrderIdSize = 0;
      int intInvoiceIdSize = 0;
      ArrayList alOrderId = new ArrayList();
      ArrayList alInvoiceId = new ArrayList();
      ArrayList alOrdComment = new ArrayList();
      ArrayList alComment = new ArrayList();
      ArrayList alList = new ArrayList();
      ArrayList alQuoteDetails = new ArrayList();
      ArrayList alRevenueTrack = new ArrayList();
      ArrayList alOrderSummary = new ArrayList();
      ArrayList alSterilePartList = new ArrayList();
      HashMap hmValues = new HashMap();
      HashMap hmLoop = new HashMap();
      HashMap hOrderId = new HashMap();
      HashMap hInvoiceId = new HashMap();
      HashMap hmSalesOrderSummary= new HashMap();

      strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
      // Locale
      GmResourceBundleBean rbCompany =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      if (strMode.equals("CONTROL") || strMode.equals("SHIP") || strMode.equals("PO")) {
        strSessOrderId =
            !type.equals("savePDFfile") ? (String) session.getAttribute("strSessOrderId") : "";
        hmReturn = gmCust.loadOrderDetails(strSessOrderId);
        if (strMode.equals("SHIP")) {
          hmLoadReturn = gmCust.loadShipping();
          request.setAttribute("hmShipLoad", hmLoadReturn);
        }

        request.setAttribute("hMode", strMode);
        request.setAttribute("hmReturn", hmReturn);

      } else if (strMode.equals("SaveControl")) {
        strSessOrderId = request.getParameter("hOrdId");
        strItemsId = request.getParameter("hItems") == null ? "" : request.getParameter("hItems");
        StringTokenizer strTok = new StringTokenizer(strItemsId, ",");
        String strItemId = "";
        HashMap hmCart = new HashMap();
        int i = 0;
        while (strTok.hasMoreTokens()) {
          hmLoop = new HashMap();
          strItemId = strTok.nextToken();
          strContNum =
              request.getParameter("Txt_CNum" + i) == null ? "" : request.getParameter("Txt_CNum"
                  + i);
          hmLoop.put("CNTRL", strContNum);
          hmCart.put(strItemId, hmLoop);
          i++;
        }

        hmReturn = gmCust.saveControlOrderDetails(hmCart, strItemsId, strSessOrderId, strUserId);
        hmReturn = gmCust.loadOrderDetails(strSessOrderId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strMode);

      }
      /*
       * else if (strMode.equals("SaveShip")) { strSessOrderId =
       * (String)request.getParameter("hOrdId");
       * 
       * strShipDate = request.getParameter("Txt_SDate")== null
       * ?"":request.getParameter("Txt_SDate"); strShipCarr = request.getParameter("Cbo_ShipCarr")==
       * null ?"":request.getParameter("Cbo_ShipCarr"); strShipMode =
       * request.getParameter("Cbo_ShipMode")== null ?"":request.getParameter("Cbo_ShipMode");
       * strTrack = request.getParameter("Txt_Track")== null ?"":request.getParameter("Txt_Track");
       * 
       * hmValues.put("SDT",strShipDate); hmValues.put("SCAR",strShipCarr);
       * hmValues.put("SMODE",strShipMode); hmValues.put("STRK",strTrack);
       * 
       * hmReturn = gmCust.saveShipOrderDetails(hmValues,strSessOrderId,strUserId); hmReturn =
       * gmCust.loadOrderDetails(strSessOrderId);
       * 
       * hmLoadReturn = gmCust.loadShipping(); request.setAttribute("hmShipLoad",hmLoadReturn);
       * 
       * request.setAttribute("SHIPDETAILS",hmValues); request.setAttribute("hmReturn",hmReturn);
       * request.setAttribute("hMode",strMode);
       * 
       * }
       */
      else if (strMode.equals("PrintPack")) {
    	  
        strSessOrderId = request.getParameter("hOrdId");
        strProjectType =
            request.getParameter("Cbo_ProjectType") == null ? "1500" : request
                .getParameter("Cbo_ProjectType");
        strSource = GmCommonClass.parseNull(request.getParameter("hSource"));
        alList = gmCommon.getCodeList("PKPRN");
        strRuleDOmain = GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.DOMAINDB"));
        String strPackHeader =
            GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.FDA_REGIST_VAL"));
        String strPackFooter =
            GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.FDA_FOOTER_VAL"));
        String strPaperworkFlag =
            GmCommonClass.parseNull(rbCompany.getProperty("LOANER_PAPERWORK_FLAG"));
        // For japan we need to show cart details by avoiding below flag
        if (!strRuleDOmain.equals("") && !strPaperworkFlag.equals("YES")) {
          strProjectType = "PACKSLIP";
        }     
        hmReturn = gmCust.loadOrderDetails(strSessOrderId, strProjectType);
        // The following code is added to print Commercial Invoice for Orders which account type is
        // International.
        HashMap hmOrderDet = (HashMap) hmReturn.get("ORDERDETAILS");
        strAccountType = GmCommonClass.parseNull((String) hmOrderDet.get("ACCOUNTTYPE"));
        strAccountId = GmCommonClass.parseNull((String) hmOrderDet.get("ACCID"));
        String strOrdType = GmCommonClass.parseNull((String) hmOrderDet.get("ORDERTYPE"));
        String strAckRefId = GmCommonClass.parseNull((String) hmOrderDet.get("ACKREFID"));
        strPlantId = GmCommonClass.parseNull((String) hmOrderDet.get("PLANTID"));
        strCommercialInvoice =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strAccountType,
                "COMMERCIALINV", strCompanyId));
        HashMap hmAtbData = gmAR.loadOrderAttribute(strSessOrderId, strOrdType);
        HashMap hmCompAtbData = gmAR.loadInvoiceAttribute("INVOICE_ATTRIB"); 
        String strAckOdrSlipNm = "";
        
        strGSTStartFl = gmDOBean.getGSTStartFlag(strSessOrderId,"1200");
        
        ArrayList alCartDetails = (ArrayList) hmReturn.get("CARTDETAILS");
        //To Fetch the cart details without usage part code-PMT-32393
        alCartDetails = gmDOReportBean.fetchPopulatedCartDetails(alCartDetails, strSessOrderId, "50180");//50180 -Order
        
        /*
         * alQuoteDetails = gmCustomerBean.fetchOrderAttributeInfo(strSessOrderId, "SURATB"); int
         * intSize = alQuoteDetails.size(); HashMap hcboVal = new HashMap(); for (int i = 0; i <
         * intSize; i++) { hcboVal = (HashMap) alQuoteDetails.get(i); strCodeID = (String)
         * hcboVal.get("TRTYPEID"); strCodeNm = (String) hcboVal.get("ATTRVAL"); if
         * (strCodeID.equals("26230740")) // JP Date of Surgery { strSurgeryDt = strCodeNm; } }
         */
        
        //To check sterile part in order id for Plant 3000-PMT#48418
        strSterileExpDateRule =  GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany
                (strPlantId,"SHOW_STERILE_EXP_FL",strCompanyId));
        if(strSterileExpDateRule.equals("Y"))
        {
        	alSterilePartList = gmLotExpiryBean.fetchSterilePartOrder(strSessOrderId);
        }      
        request.setAttribute("ALSTERILEPARTLIST",alSterilePartList);
                
        // For Japan paperwork date format should be YYYY/MM/DD
        
         String strPrintPurposelbl = request.getParameter("printPurpose");
         request.setAttribute("printPurpose", strPrintPurposelbl);
       
        strSurgeryDt =
            GmCommonClass.getStringFromDate((Date) hmOrderDet.get("ORD_SURG_DT"), "yyyy/MM/dd");

        hmReturn.put("STRSURGERYDT", strSurgeryDt);

        HashMap hmEuSectonData = new HashMap();

        GmPartyBean gmPartyBean = new GmPartyBean();
        hmEuSectonData =
            strCommercialInvoice.equals("Y") ? (HashMap) gmPartyBean.fetchPartyRuleParam(
                strAccountId, "ICPTY", "PRINT") : null;
        request.setAttribute("ICSDATA", hmEuSectonData);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("alcomboList", alList);
        request.setAttribute("ProjectType", strProjectType);
        request.setAttribute("SOURCE", strSource);
        request.setAttribute("ORDER_ATB", hmAtbData);
        request.setAttribute("COMP_ATB", hmCompAtbData);
        // PLANTCOMP, RECORDCNT,PRINTCNT values are used in GmPackSlipItalyMulti.jsp
        request.setAttribute("PLANTCOMP",
            GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "PLANTPACKSLIP")));
        request.setAttribute("RECORDCNT",
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("RECORDCNT", "PACKSLIPCNT")));
        request.setAttribute("PRINTCNT",
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("PRINTCNT", "PACKSLIPCNT")));
        request.setAttribute("ISICSACCOUNT", strCommercialInvoice);
        request.setAttribute("PACKHEADER", strPackHeader);
        request.setAttribute("PACKFOOTER", strPackFooter);
        request.setAttribute("GSTStartFl",strGSTStartFl);
        request.setAttribute("alCartDetails", alCartDetails);
        strOrderPackForward =
            GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.ORDPACKSLIP"));
        strOrdPackJasName = GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.ORDPACKJAS"));
        request.setAttribute("PACKJASPERNAME", strOrdPackJasName);
        String strShowQuoteJasperPrint =
            GmCommonClass.parseNull(rbCompany.getProperty("PICK_SLIP.SHOW_QUOTE_JASPER_PRINT"));
        String strPackSlipJasPrintNm =
            GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.PACKSLIP"));
        // if ((strOrdType.equals("101260") || !strAckRefId.equals("")) || (
        // strOrdType.equals("2520") && strShowQuoteJasperPrint.equals("YES") ) )
        if ((!strPackSlipJasPrintNm.equals(""))
            || (strOrdType.equals("2520") && strShowQuoteJasperPrint.equals("YES"))) {
          ArrayList alItemDetails = gmCust.loadOrderAckItemDetails(strSessOrderId);
         //To Fetch the cart details without usage part code-PMT-32393
     	 ArrayList alpopCartDetails = gmDOReportBean.fetchPopulatedCartDetails(alItemDetails, strSessOrderId, "50180");//50180 -Order
          HashMap hmOrderDetails = gmCust.loadOrderAckDetails(strSessOrderId);
          HashMap hmBackOrderDetails = gmCust.loadOrderDetails(strSessOrderId);
          ArrayList alBackOrder = (ArrayList) hmBackOrderDetails.get("BACKORDER");
          HashMap hmOrderDetls = (HashMap) hmBackOrderDetails.get("ORDERDETAILS");
          hmOrderDetails.put(
              "SUBREPORT_DIR",
              request.getSession().getServletContext()
                  .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
          hmOrderDetails.put("CUSTREFID",GmCommonClass.parseNull((String) hmOrderDet.get("CUSTREFID")));
          request.setAttribute("ITEMDETAILS", alItemDetails);
          request.setAttribute("alpopCartDetails", alpopCartDetails);
          request.setAttribute("ORDERDETAILS", hmOrderDetails);
          request.setAttribute("BACKORDERDETAILS", alBackOrder);
          request.setAttribute("ALORDERDETAILS", hmOrderDetls);
         
          if (!strOrdType.equals("2520")) {
            strAckOdrSlipNm = strPackSlipJasPrintNm;
          } else {
            if (strAckRefId.equals("") || strOrdType.equals("2520")) {
              strAckOdrSlipNm = GmCommonClass.parseNull(rbCompany.getProperty("PICK_SLIP.ACKODR"));
            } else {
              strAckOdrSlipNm = GmCommonClass.parseNull(rbCompany.getProperty("PICK_SLIP.DELODR"));
            }
          }
          request.setAttribute("ACKODRSLIPNM", strAckOdrSlipNm);
          request.setAttribute("ORDERTYPE", strOrdType);
          strDispatchTo = strOperPath.concat("/GmOrderAckSlipPrint.jsp");
        } else {
          strDispatchTo = strOperPath.concat(strOrderPackForward);
        }

      } else if (strMode.equals("ACKPrintPack")) {
        strSessOrderId = request.getParameter("hOrdId");
        strProjectType =
            request.getParameter("Cbo_ProjectType") == null ? "1500" : request
                .getParameter("Cbo_ProjectType");
        strSource = GmCommonClass.parseNull(request.getParameter("hSource"));
        alList = gmCommon.getCodeList("PKPRN");
        hmReturn = gmCust.loadOrderDetails(strSessOrderId);
        HashMap hmorderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
        ArrayList alCartDetails = (ArrayList) hmReturn.get("CARTDETAILS");
        String strAckRefId = GmCommonClass.parseNull((String) hmorderDetails.get("ACKREFID"));
        String strOrdType = GmCommonClass.parseNull((String) hmorderDetails.get("ORDERTYPE"));
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("alcomboList", alList);
        request.setAttribute("ProjectType", strProjectType);
        request.setAttribute("SOURCE", strSource);
        request.setAttribute("ORDERDETAILS", hmorderDetails);
        strOrderPackForward =
            GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.ORDPACKSLIP"));
        strOrdPackJasName = GmCommonClass.parseNull(rbCompany.getProperty("PACK_SLIP.ORDPACKJAS"));
        request.setAttribute("PACKJASPERNAME", strOrdPackJasName);
        String strShowQuoteJasperPrint =
            GmCommonClass.parseNull(rbCompany.getProperty("PICK_SLIP.SHOW_QUOTE_JASPER_PRINT"));
        if ((strOrdType.equals("101260") || !strAckRefId.equals(""))
            || (strOrdType.equals("2520") && strShowQuoteJasperPrint.equals("YES"))) {
          ArrayList alItemDetails = gmCust.loadOrderAckItemDetails(strSessOrderId);
          HashMap hmOrderDetails = gmCust.loadOrderAckDetails(strSessOrderId);
          HashMap hmBackOrderDetails = gmCust.loadOrderDetails(strSessOrderId);
          ArrayList alBackOrder = (ArrayList) hmBackOrderDetails.get("BACKORDER");
          HashMap hmOrderDetls = (HashMap) hmBackOrderDetails.get("ORDERDETAILS");
          hmOrderDetails.put(
              "SUBREPORT_DIR",
              request.getSession().getServletContext()
                  .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
          request.setAttribute("ITEMDETAILS", alItemDetails);
          request.setAttribute("ORDERDETAILS", hmOrderDetails);
          HashMap hmParams = new HashMap();
          hmParams.put("RULEID", "PICSLIP");
          hmParams.put("RULEGROUP", "GLOBAL");
          String strReforward = gmCommonBean.getRuleValue(hmParams);
          request.setAttribute("hAction", "OrdPlaced");
          request.setAttribute("RE_FORWARD", strReforward);
          request.setAttribute("RE_TXN", "50925");
          request.setAttribute("txnStatus", "PROCESS");
          strDispatchTo = "/gmRuleEngine.do";
        } else {
          strDispatchTo = strOperPath.concat(strOrderPackForward);
        }
      } else if (strMode.equals("PackSlipPrint")) {
        request.setAttribute("hAction", strMode);
        strSessOrderId = request.getParameter("hOrdId");
        strProjectType =
            request.getParameter("Cbo_ProjectType") == null ? "1500" : request
                .getParameter("Cbo_ProjectType");
        alList = gmCommon.getCodeList("PKPRN");
        hmReturn = gmCust.loadOrderDetails(strSessOrderId, strProjectType);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("alcomboList", alList);
        request.setAttribute("ProjectType", strProjectType);

        strDispatchTo = strOperPath.concat("/GmPackSlipPrint.jsp");
        GmDORptBean gmDORptBean = new GmDORptBean(tmpGmDataStoreVO);
      } else if (strMode.equals("PrintPrice")) {
        HashMap hmViewAccess = new HashMap();

        hmViewAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "VIEW_ADJ_DETAILS"));
        String strAdjViewFl = GmCommonClass.parseNull((String) hmViewAccess.get("UPDFL"));
        request.setAttribute("VIEWADJDETAILFL", strAdjViewFl);
        hmLotOverrideAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "LOT_OVERRIDE_CHECK"));
        strLotOverrideUpdAccess =
            GmCommonClass.parseNull((String) hmLotOverrideAccess.get("UPDFL"));
        if (strLotOverrideUpdAccess.equals("Y")) {
          strLotOrderideAcess = "Y";
        }
        log.debug("strLotOrderideAcess **** " + strLotOrderideAcess);
        request.setAttribute("LOTOVERRIDEACCESS", strLotOrderideAcess);
        // The Invoice total section of DO Summary screen is included in the main jsp to get it
        // based on company
        strIncJSPNM =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("DOSUMMARY", "JSPNM",
                strCompanyId));

        request.setAttribute("INCJSPNM", strIncJSPNM);
        GmDORptBean gmDORptBean = new GmDORptBean(tmpGmDataStoreVO);
        HashMap hmParam = new HashMap();
        strSessOrderId = GmCommonClass.parseNull(request.getParameter("hOrdId"));
        if (strSessOrderId.equals("")) {
          strSessOrderId = GmCommonClass.parseNull((String) request.getAttribute("hOrdId"));
        }
        strParantForm = GmCommonClass.parseNull(request.getParameter("hParantForm"));
        if (strParantForm.equals("")) {
          strParantForm = GmCommonClass.parseNull((String) request.getAttribute("hParantForm"));
        }
        strhidePratPrice = GmCommonClass.parseNull(request.getParameter("hidePartPrice"));
        if (strhidePratPrice.equals("")) {
          strhidePratPrice =
              GmCommonClass.parseNull((String) request.getAttribute("hidePartPrice"));
        }
        strFwdFlag = GmCommonClass.parseNull(request.getParameter("hFwdFlag"));
        if (strFwdFlag.equals("")) {
          strFwdFlag = GmCommonClass.parseNull((String) request.getAttribute("hFwdFlag"));
        }
        hmReturn = gmCust.loadOrderDetails(strSessOrderId);
       
       // < Added for PMT-51170 DO summary print- Hide discount order for sales rep
        if (strhidePratPrice.equals("Y")) {
        	hmSalesOrderSummary.put("ORDERID",strSessOrderId);
        	hmSalesOrderSummary.put("EXCLORDTYPE", "2535");// 2535 - Sales Discount
        	alOrderSummary=gmDOBean.fetchOrderSummary(hmSalesOrderSummary);
        	hmReturn.put("ORDERSUMMARY", alOrderSummary);
        }
        // > PMT-51170 code ended
        alOrderId = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALORDERID"));
        alInvoiceId = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALINVOICEID"));
        HashMap hmOrderDetails = new HashMap();
        String strDoType = "";
        String strCurrencySymbol = "";
        hmOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
        strDoType = GmCommonClass.parseNull((String) hmOrderDetails.get("ORDERTYPE"));
        strCurrencySymbol = GmCommonClass.parseNull((String) hmOrderDetails.get("CURRENCY"));
        String strOpenReturnfl = gmDOBean.fetchOpenReturnfl(strSessOrderId);
        strGSTStartFl = gmDOBean.getGSTStartFlag(strSessOrderId,"1200");//--1200--Orders
        request.setAttribute("GSTStartFl", strGSTStartFl);
        hmParam.put("ORDERID", strSessOrderId);
        hmParam.put("ATTRIBTYPE", "103560"); // 103560 - Order source attribute type
        String strOrderSrc = GmCommonClass.parseNull(gmDORptBean.fetchOrderAttribute(hmParam));
        // to put the currency symbol (hashmap)
        if (strCurrencySymbol.equals("") || strCurrencySymbol.equals("USD")) {
          strCurrencySymbol = "$";
        }
        hmOrderDetails.put("CURRENCY", strCurrencySymbol);
        intOrderIdSize = alOrderId.size();
        intInvoiceIdSize = alInvoiceId.size();
        String strOrderId[] = new String[intOrderIdSize];
        String strInvouceId[] = new String[intInvoiceIdSize];
        for (int i = 0; i < intOrderIdSize; i++) {
          hOrderId = (HashMap) alOrderId.get(i);
          strOrderId[i] = (String) hOrderId.get("SBID");
        }
        for (int i = 0; i < intInvoiceIdSize; i++) {
          hInvoiceId = (HashMap) alInvoiceId.get(i);
          strInvouceId[i] = (String) hInvoiceId.get("SBID");
        }
        String strOrderIds = GmCommonClass.createInputString(strOrderId);
        String strInvoiceIds = GmCommonClass.createInputString(strInvouceId);
        //Commented for PCA-271 and calling new fetch from t5003_order_revenue_sample_dtls
//        alRevenueTrack = gmDORptBean.fetchOrderAttributeWithCodeNmInfo(strSessOrderId, "REVTP");
        alRevenueTrack  = gmCustomerBean.fetchRevenuetrackInfo(strSessOrderId);
        hmReturn.put("ALREVENUETRACK", alRevenueTrack);
        if (!strSessDeptId.equals("S")) // Comments can not be visible to dept S
        {
          alOrdComment = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strOrderIds, "1200")); // 1200
                                                                                                     // -
                                                                                                     // for
                                                                                                     // order
          alComment = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strInvoiceIds, "1201")); // 1201
                                                                                                    // -
                                                                                                    // for
                                                                                                    // invoice
          alComment.addAll(alOrdComment);
          hmReturn.put("ALCOMMENTS", alComment);
        }

        /*
         * if(strDoType.equals("2518") && strSessDeptId.equals("C")){ throw new
         * AppError("You do not have enough permission to do this action", "", 'E'); }
         */
        if (type.equals("savePDFfile")) {
          HashMap hmOrderTotals = new HashMap();
          String strDOSummaryPDFLocation = GmCommonClass.getString("BATCH_DO_PRINT");
          HashMap hmSavePDF = new HashMap();
          hmOrderTotals = gmDOBean.fetchOrderTotalDetails(strSessOrderId);
          hmSavePDF.putAll(hmReturn);
          hmSavePDF.putAll(hmOrderDetails);
          // Order total details
          hmSavePDF.put("BEFORETOTAL",
              GmCommonClass.parseZero((String) hmOrderTotals.get("BEFORETOTAL")));
          hmSavePDF.put("ADJAMOUNT",
              GmCommonClass.parseDouble((Double) hmOrderTotals.get("ADJAMOUNT")));
          hmSavePDF.put("ADJUSTMENTAMT",
              GmCommonClass.parseZero((String) hmOrderTotals.get("ADJUSTMENTAMT")));
          hmSavePDF.put("AFTERTOTAL",
              GmCommonClass.parseZero((String) hmOrderTotals.get("AFTERTOTAL")));
          hmSavePDF
              .put("TAXTOTAL", GmCommonClass.parseZero((String) hmOrderTotals.get("TAXTOTAL")));
          hmSavePDF.put(
              "SUBREPORT_DIR",
              request.getSession().getServletContext()
                  .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
                  + "\\");
          hmSavePDF.put(
              "ODT",
              GmCommonClass.getStringFromDate((Date) hmOrderDetails.get("ODT"),
                  tmpGmDataStoreVO.getCmpdfmt()));
          // to get the file location (to store a PDF file)
          String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
          File newFile = new File(strDOSummaryPDFLocation);
          if (!newFile.exists()) {
            newFile.mkdirs();
          }
          // to form the file location and name.
          String strPDFFileName = strDOSummaryPDFLocation + "\\" + strSessOrderId + ".pdf";
          gmJasperReport.setRequest(request);
          gmJasperReport.setResponse(response);
          gmJasperReport.setJasperReportName("/GmDOBatchPrint.jasper");
          gmJasperReport.setHmReportParameters(hmSavePDF);
          gmJasperReport.setReportDataList(null);
          gmJasperReport.exportJasperToPdf(strPDFFileName);
        } else {
          request.setAttribute("OPENRTFL", strOpenReturnfl);
          request.setAttribute("hFwdFlag", strFwdFlag);
          request.setAttribute("OrderSrc", strOrderSrc);
          request.setAttribute("hParantForm", strParantForm);
          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hidePartPrice", strhidePratPrice);

          if (strDoType.equals("101260")) {
            strDispatchTo = strOperPath.concat("/GmAckPrintPrice.jsp");
          } else {
            strDispatchTo =
                strDoType.equals("2520") ? strOperPath.concat("/GmPrintQuote.jsp") : strOperPath
                    .concat("/GmPrintPrice.jsp");
          }
        }
      } else if (strMode.equals("SplitControl")) {
        strSessOrderId = request.getParameter("hOrdId");
        strItemOrdId = request.getParameter("hItemOrdId");
        hmReturn = gmCust.loadItemOrderDetails(strItemOrdId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hOrdId", strSessOrderId);
        strDispatchTo = strOperPath.concat("/GmSplitControl.jsp");
      } else if (strMode.equals("SplitSave")) {
        strSessOrderId = request.getParameter("hOrdId");
        strItemOrdId = request.getParameter("hItemOrdId");
        strPartNum = request.getParameter("hPartNum");
        strCounter = request.getParameter("hCounter");
        strPrice = request.getParameter("hPrice");
        int intCounter = Integer.parseInt(strCounter);
        for (int i = 1; i <= intCounter; i++) {
          strContNum =
              request.getParameter("Txt_CNum" + i) == null ? "" : request.getParameter("Txt_CNum"
                  + i);
          strQty =
              request.getParameter("Txt_Qty" + i) == null ? "" : request
                  .getParameter("Txt_Qty" + i);
          hmLoop.put("CNTRL", strContNum);
          hmLoop.put("QTY", strQty);
          alList.add(hmLoop);
          hmLoop = new HashMap();
        }
        hmReturn =
            gmCust.saveSplitItemOrder(alList, strSessOrderId, strItemOrdId, strPartNum, strPrice,
                strUserId);

        hmReturn = gmCust.loadOrderDetails(strSessOrderId);
        request.setAttribute("hmReturn", hmReturn);
        strMode = "SaveControl";
        request.setAttribute("hMode", strMode);

      } else if (strMode.equals("SavePO")) {
        strSessOrderId = request.getParameter("hOrdId");
        strPO = request.getParameter("Txt_PO") == null ? "" : request.getParameter("Txt_PO");


        HashMap hmParams = new HashMap();

        hmParams.put("ORDERID", strSessOrderId);
        hmParams.put("PO", strPO);
        hmParams.put("USERID", strUserId);


        hmReturn = gmCust.saveCustPO(hmParams);
        hmReturn = gmCust.loadOrderDetails(strSessOrderId);
        request.setAttribute("hmReturn", hmReturn);
        strMode = "PO";
        request.setAttribute("hMode", strMode);
      }
      if (type.equals("savePDFfile")) {
        ObjectOutputStream sendStream = null;
        sendStream = new ObjectOutputStream(response.getOutputStream());
        sendStream.close();
      } else {
        strCompanyLocale = strCompanyLocale.equals("") ? "en_US" : "en_" + strCompanyLocale;
        request.setAttribute("CompanyLocale", strCompanyLocale);
        dispatch(strDispatchTo, request, response);
      }
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmEditOrderServlet
