/*****************************************************************************
 * File : GmOrderItemServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmCustomerExtnBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.beans.GmDOTxnBean;
import com.globus.custservice.beans.GmOrderUsageBean;
import com.globus.custservice.beans.GmPlaceOrderInterface;
import com.globus.operations.beans.GmOperationsBean;


public class GmOrderEditServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmOrderItemSetup.jsp");
    HashMap hmLotOverrideAccess = new HashMap();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strLotOverrideUpdAccess = "";
    String strLotOrderideAcess = "N";
    try {

      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
      hmLotOverrideAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "LOT_OVERRIDE_CHECK"));
      strLotOverrideUpdAccess = GmCommonClass.parseNull((String) hmLotOverrideAccess.get("UPDFL"));
      if (strLotOverrideUpdAccess.equals("Y")) {
        strLotOrderideAcess = "Y";
      }
      request.setAttribute("LOTOVERRIDEACCESS", strLotOrderideAcess); // Class.
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      String strShowUsageLot =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_USAGE_LOT"));
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmDOTxnBean gmDOTxnBean = new GmDOTxnBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean();
      GmCustomerExtnBean gmCustExtn = new GmCustomerExtnBean(getGmDataStoreVO());
      GmDORptBean gmDORptBean = new GmDORptBean(getGmDataStoreVO());
      GmDOBean gmDoBean = new GmDOBean(getGmDataStoreVO());
      GmDBManager gmDBManager = new GmDBManager();
      HashMap hmAccess = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmCart = new HashMap();
      HashMap hmOrdDtls = new HashMap();
      HashMap hmComboValue = new HashMap();
      HashMap hmSummary = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alRepList = new ArrayList();
      ArrayList alOrdSurAtbDetails = new ArrayList();

      String strOrderId = "";
      String strType = "";
      String strBillTo = "";
      String strPO = "";
      String strAccId = "";
      String strMode = "";
      String strComments = "";
      String strReason = "";
      String strShipFl = "";
      String strDelFl = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPartNums = "";
      String strhPartNums = "";
      String strDelPartNum = "";
      String strInputStr = "";
      String strPerson = "";
      String strTotal = "";
      String strMsg = "";
      String strParentOrderId = "";
      String strInvoiceId = "";
      String strRmQtyStr = "";
      String strBOQtyStr = "";
      String strQuoteStr = "";
      String strCnumStr = "";
      String strUsedLotStr = "";

      String strPartNum = "";
      String strTempPartNums = "";

      String strInt = "";
      String strInputString = "";
      String strQty = "";
      String strPrice = "";
      String strControl = "";
      String strHardcp_PO = "";
      String strOrderType = "";
      String strJspName = "";
      String strAccessFlag = "";
      String strEGPSUsage = "";
      String strAccountId = "";
      String strEPGSfl = "";
      Date dtSurgDate = null;
      Date dtOrderDate = null;

      HashMap hmValues = new HashMap();
      String strShipAdd = "";
      String strSalesRepId = "";
      strOrderId = request.getParameter("hOrdId");
      if (strOrderId == null) {
        strOrderId = (String) session.getAttribute("strSessOrderId");
      }
      strOrderId = strOrderId == null ? "" : strOrderId;
      strMsg = GmCommonClass.parseNull(request.getParameter("MESSAGES"));
      // If the Order is saved then save in order information in the database
      log.debug(" Action is " + strAction + " Order ID is " + strOrderId);
      if (strAction.equals("EditOrder")) {
        String strFunctionId = "EDIT_ORDER_ACEESS";
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                strFunctionId));
        String strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

        if (!strUpdateAccess.equals("Y")) {
          throw new AppError("", "20685");
        }
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "AR_EDIT_QTY"));
        String strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        request.setAttribute("QTYEDIT", strUpdFl);
      }
      if (strAction.equals("SaveOrder") || strAction.equals("SaveAckOrder")) {
        // Order Main Information
        strAccId =
            request.getParameter("Txt_AccId") == null ? "" : request.getParameter("Txt_AccId");
        strMode = request.getParameter("Cbo_Mode") == null ? "" : request.getParameter("Cbo_Mode");
        strPerson = request.getParameter("Txt_PNm") == null ? "" : request.getParameter("Txt_PNm");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strPO = request.getParameter("Txt_PO") == null ? "" : request.getParameter("Txt_PO");
        strParentOrderId =
            request.getParameter("Txt_ParentOrdId") == null ? "" : request
                .getParameter("Txt_ParentOrdId");
        strInvoiceId =
            request.getParameter("Txt_InvoiceId") == null ? "" : request
                .getParameter("Txt_InvoiceId");
        strSalesRepId =
            request.getParameter("Cbo_SalseRepID") == null ? "" : request
                .getParameter("Cbo_SalseRepID");

        strReason =
            request.getParameter("Txt_Reason") == null ? "" : request.getParameter("Txt_Reason");
        strRmQtyStr =
            request.getParameter("hRmQtyStr") == null ? "" : request.getParameter("hRmQtyStr");
        strBOQtyStr =
            request.getParameter("hBOQtyStr") == null ? "" : request.getParameter("hBOQtyStr");
        strQuoteStr =
            request.getParameter("hQuoteStr") == null ? "" : request.getParameter("hQuoteStr");
        strCnumStr =
            request.getParameter("hBOQtyStr") == null ? "" : request.getParameter("hCnumStr");
        strHardcp_PO = GmCommonClass.parseNull(request.getParameter("Chk_HardPO"));
        strUsedLotStr = GmCommonClass.parseNull(request.getParameter("hUsedLotStr"));

        dtSurgDate =
            getDateFromStr(request.getParameter("Txt_SurgDate") == null ? "" : request
                .getParameter("Txt_SurgDate"));
        //to update order date for Month End Reclassification        
        dtOrderDate =
            getDateFromStr(request.getParameter("Txt_OrderDate") == null ? "" : request
                .getParameter("Txt_OrderDate"));
        String strCompanyDtl = GmCommonClass.parseNull(request.getParameter("companyInfo"));
        strEGPSUsage  = request.getParameter("Cbo_egpsusage") == null ? "" : request
                .getParameter("Cbo_egpsusage");
 
        hmValues.put("ACCID", strAccId);
        hmValues.put("OMODE", strMode);
        hmValues.put("OPERSON", strPerson);
        hmValues.put("COMMENTS", strComments);
        hmValues.put("CPO", strPO);
        hmValues.put("PARENTORDERID", strParentOrderId);
        hmValues.put("INVOICEID", strInvoiceId);
        hmValues.put("SALESREP", strSalesRepId);
        hmValues.put("RMQTYSTR", strRmQtyStr);
        hmValues.put("BOQTYSTR", strBOQtyStr);
        hmValues.put("QUOTESTR", strQuoteStr);
        hmValues.put("CNUMSTR", strCnumStr);
        hmValues.put("HARDCPPO", strHardcp_PO);
        hmValues.put("SCREEN", "EditOrder");
        hmValues.put("USEDLOTSTR", strUsedLotStr);
        hmValues.put("gmDataStoreVO", getGmDataStoreVO());
        hmValues.put("SURGERYDT", dtSurgDate);
        hmValues.put("ORDER_DATE", dtOrderDate);
        hmValues.put("COMPANYINFO", strCompanyDtl);
     
	  if(!strEGPSUsage.equals("") && !strOrderId.equals("")) { //PC-4892  - EGPS in Edit order and Add EGPS in case booking
		  HashMap hmOrderAttbVal = gmCustExtn.fetchOrderAttributeValues(strOrderId,"26241315");
	      String strOrderAttbId = GmCommonClass.parseNull((String) hmOrderAttbVal.get("OAID"));
		  strQuoteStr = strQuoteStr.equals("")?strOrderAttbId:strQuoteStr+""+strOrderAttbId;
		  strQuoteStr = strQuoteStr +  "^" + 26241315 + "^" + strEGPSUsage + "|";
		  hmValues.put("QUOTESTR",strQuoteStr); 
	   }
        if (strAction.equals("SaveAckOrder")) {
          strMsg = gmDOTxnBean.saveAckOrderDetails(hmValues, strOrderId, strUserId);
        } else {
          GmPlaceOrderInterface gmPlaceOrderInterface =
              (GmPlaceOrderInterface) GmCommonClass.getSpringBeanClass("xml/Place_Order_Beans.xml",
                  getGmDataStoreVO().getCmpid());
          // log.debug("Before calling saveOrderDetails hmValues is " + hmValues);
          strMsg = gmPlaceOrderInterface.saveOrderDetails(hmValues, strOrderId, strUserId);
        }
        gmCommonBean.saveLog(strOrderId, strReason, strUserId, "1200");
      }
      request.setAttribute("MESSAGES", strMsg);
      if (strAction.equals("SaveOrder")) {
        String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
        strDispatchTo =
            "/GmOrderEditServlet?hAction=EditOrder&hOrdId=" + strOrderId + "&MESSAGES=" + strMsg
                + "&companyInfo=" + URLEncoder.encode(strCompanyInfo);
        response.sendRedirect(strDispatchTo);
      }
     // hmReturn = gmCust.loadOrderDetails(strOrderId);
      hmReturn = gmCustExtn.loadOrderDetails(strOrderId);
      strOrderType = GmCommonClass.parseNull((String) hmOrdDtls.get("ORDERDETAILS"));
      hmReturn.put("SHOW_USAGE", strShowUsageLot);
      request.setAttribute("hmReturn", hmReturn);
      
      hmParam.put("ORDERID", strOrderId);
      hmParam.put("ATTRIBTYPE", "26241315"); // 26241315 - Order EGPS Surgery attribute type
      String strSurgeryType = GmCommonClass.parseNull(gmDORptBean.fetchOrderAttribute(hmParam));
      request.setAttribute("STRSURGERYTYPE", strSurgeryType);
      
      // log.debug(" values inside hmReturn after Save Order is " + hmReturn);

      // Get the order type to call the corresponding jsp based on type
      hmOrdDtls = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("ORDERDETAILS"));
      strOrderType = GmCommonClass.parseNull((String) hmOrdDtls.get("ORDERTYPE"));
      //strAccountId - PC-4659 --Add EGPS Usage in DO - SpineIT New order
      strAccountId = GmCommonClass.parseNull((String) hmOrdDtls.get("ACCID")); 
      //hmSummary - PC-4659 --Add EGPS Usage in DO - SpineIT New order
      hmSummary = gmCustExtn.getAccSummary(strAccountId);  
      hmCart = GmCommonClass.parseNullHashMap((HashMap)hmSummary.get("ACCSUM"));
      strEPGSfl = GmCommonClass.parseNull((String) hmCart.get("EGPSFL"));
     
      request.setAttribute("hstrEPGSfl", strEPGSfl);
      
      hmResult = gmCust.loadShipping();
      request.setAttribute("hmShipLoad", hmResult);

      alReturn = gmCommonBean.getLog(strOrderId, "1200"); // 1200 - Order log type
      request.setAttribute("hmLog", alReturn);
      GmOrderUsageBean gmOrderUsageBean = new GmOrderUsageBean(getGmDataStoreVO());
      ArrayList alUsedLotdetails = gmOrderUsageBean.fetchUsedLotNumber(strOrderId);
      // Get the access to edit the sales order
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "EDIT_ORDER_ACCESS"));
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("EDITORDACCESS", strAccessFlag);
      // Get the access to edit the order date for Month End Reclassification
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "ORDER_DATE_EDIT_ACS"));
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("ORDER_DATE_EDIT_ACS", strAccessFlag);
      
      // To get the combo box information

      hmComboValue = gmCust.loadItemOrderLists("QUOATB");

      request.setAttribute("hmComboValue", hmComboValue);
      request.setAttribute("hAction", "EditOrder");
      request.setAttribute("ALUSEDLOTDTLS", alUsedLotdetails);
      strJspName = strOrderType.equals("101260") ? "/GmAckOrderModify.jsp" : "/GmOrderModify.jsp";
      strDispatchTo = strCustPath.concat(strJspName);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmOrderItemServlet
