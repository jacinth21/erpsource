/*****************************************************************************
 * File : GmPartPriceReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmSalesBean;

public class GmPartPriceReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = "";
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page

      String strColumn = request.getParameter("hColumn");
      strColumn = (strColumn == null) ? "ID" : strColumn;
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("strSessOpt");
      }

      String strProjId = request.getParameter("hId");
      strProjId = (strProjId == null) ? "0" : strProjId;



      alReturn = gmSales.reportPartNumPricing(strColumn, strProjId);
      request.setAttribute("alReturn", alReturn);
      request.setAttribute("hAction", strAction);

      if (strProjId.equals("0")) {
        request.setAttribute("hLoad", "Project");
        request.setAttribute("hId", strProjId);
      } else {
        request.setAttribute("hLoad", "Part");
      }
      dispatch(strOperPath.concat("/GmPartPriceReport.jsp"), request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPartPriceReportServlet
