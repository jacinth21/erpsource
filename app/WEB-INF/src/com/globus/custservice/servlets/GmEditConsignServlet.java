/*****************************************************************************
 * File			 : GmEditConsignServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;
import com.globus.common.beans.*;
import com.globus.custservice.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmEditConsignServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
		String strDispatchTo = strOperPath.concat("/GmEditConsign.jsp");

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				String strMode = request.getParameter("hMode");

				if (strMode == null)
				{
					strMode = (String)session.getAttribute("hMode");
					session.removeAttribute("hMode");
				}

				strMode = (strMode == null)?"":strMode;

				GmCommonClass gmCommon = new GmCommonClass();
				GmCustomerBean gmCust = new GmCustomerBean();

				HashMap hmReturn = new HashMap();
				HashMap hmLoadReturn = new HashMap();
				HashMap hmValues = new HashMap();
				String strId = "";
				String strSessConsignId = "";
				String strUserId = 	(String)session.getAttribute("strSessUserId");
				String strShipDate = "";
				String strShipCarr = "";
				String strShipMode = "";
				String strTrack = "";

				if (strMode.equals("CONTROL") || strMode.equals("SHIP") || strMode.equals("PO"))
				{
					strSessConsignId = (String)session.getAttribute("strSessOrderId");
					hmReturn = gmCust.loadOrderDetails(strSessConsignId);
					if (strMode.equals("SHIP"))
					{
						hmLoadReturn = gmCust.loadShipping();
					    request.setAttribute("hmShipLoad",hmLoadReturn);
					}

					request.setAttribute("hMode",strMode);
					request.setAttribute("hmReturn",hmReturn);

				}
			/*	else if (strMode.equals("SaveShip"))
				{
					strSessConsignId = (String)request.getParameter("hOrdId");

					strShipDate = request.getParameter("Txt_SDate")== null ?"":request.getParameter("Txt_SDate");
					strShipCarr = request.getParameter("Cbo_ShipCarr")== null ?"":request.getParameter("Cbo_ShipCarr");
					strShipMode = request.getParameter("Cbo_ShipMode")== null ?"":request.getParameter("Cbo_ShipMode");
					strTrack = request.getParameter("Txt_Track")== null ?"":request.getParameter("Txt_Track");

					hmValues.put("SDT",strShipDate);
					hmValues.put("SCAR",strShipCarr);
					hmValues.put("SMODE",strShipMode);
					hmValues.put("STRK",strTrack);

					hmReturn = gmCust.saveShipOrderDetails(hmValues,strSessConsignId,strUserId);
					hmReturn = gmCust.loadOrderDetails(strSessConsignId);

					hmLoadReturn = gmCust.loadShipping();
					request.setAttribute("hmShipLoad",hmLoadReturn);

					request.setAttribute("SHIPDETAILS",hmValues);
					request.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hMode",strMode);

				}*/
					
				dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmEditConsignServlet