/*****************************************************************************
 * File			 : GmCustDashBoardServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;
import com.globus.common.beans.*;
import com.globus.custservice.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmCustDashBoardServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

	
  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
		String strDispatchTo = "";
		String strDeptId = 	(String)session.getAttribute("strSessDeptId");
		
		try
		{
			//checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
		  instantiate(request, response);	
		  String strAction = request.getParameter("hAction");
				strAction = (strAction == null)?"Load":strAction;

				/*GmCommonClass gmCommon = new GmCommonClass();
				GmCustomerBean gmCust = new GmCustomerBean();

				HashMap hmReturn = new HashMap();
				String strId = "";

				if (strAction.equals("Load"))
				{
					strDeptId = strDeptId.equals("C")|| strDeptId.equals("Z")?"CUST":"SHIP";
					hmReturn = gmCust.reportDashboard(strDeptId);
					request.setAttribute("hmReturn",hmReturn);
				}
				else if (strAction.equals("Add") || strAction.equals("Edit"))
				{
					String strUserId = 	(String)session.getAttribute("strSessUserId");

				}*/
				if(strDeptId.equals("D"))
				{
					strAction= "SHIPDASHBOARD";
				}else 
				{
					strAction= "CUSTDASHBOARD";
				}
				request.setAttribute("hAction", strAction);
				dispatch(strOperPath.concat("/GmDashBoardHome.jsp"),request,response);
		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmCustDashBoardServlet