/*****************************************************************************
 * File : GmOrderAjaxServlet Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.cart.beans.GmCommonCartBean;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmCustomerExtnBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.beans.GmDOTxnBean;

public class GmOrderAjaxServlet extends GmServlet {

  private ServletContext context;
  private HashMap hmReturn = new HashMap();
  private HashMap hmCart = new HashMap();
  private ArrayList alConstructs = new ArrayList();
  private HashMap hcboVal = null;
  private HashMap hmDOIDval = new HashMap();
  HashMap hmParam = new HashMap();
  String strComnPath = GmCommonClass.getString("GMCOMMON");

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    this.context = config.getServletContext();
  }

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    try {
      instantiate(request, response);
      GmCommonCartBean gmCart = new GmCommonCartBean(getGmDataStoreVO());
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmCustomerExtnBean gmCustExtn = new GmCustomerExtnBean(getGmDataStoreVO());
      GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());
      GmDORptBean gmDORptBean = new GmDORptBean(getGmDataStoreVO());
      GmDOTxnBean gmDOTxnBean = new GmDOTxnBean(getGmDataStoreVO());
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

      StringBuffer sbXML = new StringBuffer();

      String strOpt = GmCommonClass.parseNull(request.getParameter("opt"));
      String strAccId = GmCommonClass.parseNull(request.getParameter("acc"));

      String strRepId = GmCommonClass.parseNull(request.getParameter("rep"));
      String strDealerId = GmCommonClass.parseNull(request.getParameter("dealer"));
      String strGpoId = GmCommonClass.parseNull(request.getParameter("gpoid"));
      String strCountryCode =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));

      String strRegn = "";
      String strAdName = "";
      String strDistName = "";
      String strTerrName = "";
      String strRepNm = "";
      String strGpoNm = "";
      String strGpoCatId = "";
      String strAccName = "";
      String strDistId = "";
      String strShipMode = "";
      String strGprAff = "";
      String strHlcAff = "";
      String strCustomerPoId = "";
      String strAccType = "";
      String strAccComments = "";
      String strAccCurrSign = "";
      String strAccCurrId = "";
      String strShipCarr = "";
      int intSize = 0;
      String strContractTp = "";
      String strCIG = "";
      String strDealerName = "";
      String strClosingDate = "";
      String strRpcName = "";//PC-2376 Add new fields in Account Pricing Report
      String strHlthName = "";
      String strEGPSUsage = "";  //PC-4659 - Add EGPS Usage in DO - SpineIT New order
log.debug("GmOrderAjaxServlet-opt.."+strOpt+"user "+strUserId);
      if (strOpt.equals("GETACC")) {
        hmReturn = GmCommonClass.parseNullHashMap((HashMap)gmCustExtn.getAccSummary(strAccId));
        hmCart = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("ACCSUM"));
        alConstructs = (ArrayList) hmReturn.get("CONSTRUCTS");

        strRegn = GmCommonClass.replaceForXML((String) hmCart.get("RGNAME"));
        strAdName = GmCommonClass.replaceForXML((String) hmCart.get("ADNAME"));
        strDistName = GmCommonClass.replaceForXML((String) hmCart.get("DNAME"));
        strTerrName = GmCommonClass.replaceForXML((String) hmCart.get("TRNAME"));
        strRepNm = GmCommonClass.parseNull((String) hmCart.get("RPNAME"));
        strRepId = GmCommonClass.parseNull((String) hmCart.get("REPID"));
        strGpoNm = GmCommonClass.parseNull((String) hmCart.get("PRNAME"));
        strGpoId = GmCommonClass.parseNull((String) hmCart.get("PRID"));
        strGpoCatId = GmCommonClass.parseNull((String) hmCart.get("GPOID"));
        strAccName = GmCommonClass.replaceForXML((String) hmCart.get("ANAME"));
        strAccId = GmCommonClass.parseNull((String) hmCart.get("ACCID"));
        strDistId = GmCommonClass.parseNull((String) hmCart.get("DISTID"));
        strShipMode = GmCommonClass.parseNull((String) hmCart.get("SHIPMODE"));
        strShipCarr = GmCommonClass.parseNull((String) hmCart.get("SHIPCARR"));
        // MNTTASK - 6074:Account Affiliation
        strGprAff = GmCommonClass.parseNull((String) hmCart.get("GPRAFF"));
        strHlcAff = GmCommonClass.parseNull((String) hmCart.get("HLCAFF"));
        strAccType = GmCommonClass.parseNull((String) hmCart.get("ACCTYPE"));
        strAccComments = GmCommonClass.parseNull((String) hmCart.get("ACCCOMMENTS"));
        strAccCurrSign = GmCommonClass.parseNull((String) hmCart.get("ACC_CURRENCY")); // Currency
                                                                                       // symbol
                                                                                       // added
        strContractTp = GmCommonClass.parseNull((String) hmCart.get("CONTRACT"));
        strCIG = GmCommonClass.parseNull((String) hmCart.get("CIG"));
        // NET NUMBER
        strDealerId = GmCommonClass.parseNull((String) hmCart.get("DEALERID"));
        strDealerName = GmCommonClass.parseNull((String) hmCart.get("DEALERNM"));
        strClosingDate = GmCommonClass.parseNull((String) hmCart.get("CLOSINGDATE"));
        //PC-2376 Add new fields in Account Pricing Report
        strRpcName = GmCommonClass.parseNull((String) hmCart.get("RPCNAME"));
        strHlthName = GmCommonClass.parseNull((String) hmCart.get("HLTHNAME"));
        strEGPSUsage = GmCommonClass.parseNull((String) hmCart.get("EGPSFL"));    
        
        if (!strAccComments.equals("")) {
          //PC-4451: Account not found if Account Information field has special characters
          strAccComments = GmCommonClass.replaceForXMLLessGreaterthan((String) hmCart.get("ACCCOMMENTS"));
        }
        sbXML.append("<data>");
        sbXML.append("<acc>");
        sbXML.append("<regn>");
        sbXML.append(strRegn);
        sbXML.append("</regn>");
        sbXML.append("<adname>");
        sbXML.append(strAdName);
        sbXML.append("</adname>");
        sbXML.append("<dname>");
        sbXML.append(strDistName);
        sbXML.append("</dname>");
        sbXML.append("<trname>");
        sbXML.append(strTerrName);
        sbXML.append("</trname>");
        sbXML.append("<rpname>");
        sbXML.append("<![CDATA[");
        sbXML.append(strRepNm);
        sbXML.append("]]>");
        sbXML.append("</rpname>");
        sbXML.append("<repid>");
        sbXML.append(strRepId);
        sbXML.append("</repid>");
        sbXML.append("<prname>");
        sbXML.append("<![CDATA[");
        sbXML.append(strGpoNm);
        sbXML.append("]]>");
        sbXML.append("</prname>");
        sbXML.append("<prid>");
        sbXML.append(strGpoId);
        sbXML.append("</prid>");
        sbXML.append("<gpocatid>");
        sbXML.append(strGpoCatId);
        sbXML.append("</gpocatid>");
        sbXML.append("<aname>");
        sbXML.append(strAccName);
        sbXML.append("</aname>");
        sbXML.append("<accid>");
        sbXML.append(strAccId);   
        sbXML.append("</accid>");
        sbXML.append("<distid>");
        sbXML.append(strDistId);
        sbXML.append("</distid>");
        sbXML.append("<ShipMode>");
        sbXML.append(strShipMode);
        sbXML.append("</ShipMode>");
        sbXML.append("<GprAff>");
        sbXML.append("<![CDATA[");
        sbXML.append(strGprAff);
        sbXML.append("]]>");
        sbXML.append("</GprAff>");
        sbXML.append("<HlcAff>");
        sbXML.append(strHlcAff);
        sbXML.append("</HlcAff>");
        sbXML.append("<accType>");
        sbXML.append(strAccType);
        sbXML.append("</accType>");
        sbXML.append("<accComments>");
        sbXML.append(strAccComments);
        sbXML.append("</accComments>");
        sbXML.append("<acccurrency>");
        sbXML.append(strAccCurrSign);
        sbXML.append("</acccurrency>");

        // The below contract type XML value was added in the 18th index instead of adding at the
        // end of the XML index (20).
        // Please be cautious in handling this contract value as it is used in New Order screen,
        // Account Setup and Account pricing screen.
        sbXML.append("<contType>");
        sbXML.append(strContractTp);
        sbXML.append("</contType>");

        sbXML.append("<ShipCarr>");
        sbXML.append(strShipCarr);
        sbXML.append("</ShipCarr>");
        sbXML.append("<cig>");
        sbXML.append(strCIG);
        sbXML.append("</cig>");
        // NET NUMBER
        sbXML.append("<dealerid>");
        sbXML.append(strDealerId);
        sbXML.append("</dealerid>");
        
        sbXML.append("<dealername>");
        sbXML.append(strDealerName);
        sbXML.append("</dealername>");
        
        sbXML.append("<closingdate>");
        sbXML.append(strClosingDate);
        sbXML.append("</closingdate>");
        
        //PC-2376 Add new fields in Account Pricing Report
        sbXML.append("<rpcname>");
        sbXML.append(strRpcName);
        sbXML.append("</rpcname>");
        
        sbXML.append("<hlthname>");
        sbXML.append(strHlthName);
        sbXML.append("</hlthname>");
        //PC-2376 Add new fields in Account Pricing Report
        
        sbXML.append("<hEGPSUsage>");
        sbXML.append(strEGPSUsage);
        sbXML.append("</hEGPSUsage>");
        sbXML.append("</acc>");

        if (alConstructs != null) {
          sbXML.append("<const>");
          intSize = alConstructs.size();
          hcboVal = new HashMap();
          for (int i = 0; i < intSize; i++) {
            hcboVal = (HashMap) alConstructs.get(i);
            sbXML.append("<code>");
            sbXML.append("<id>");
            sbXML.append((String) hcboVal.get("CID"));
            sbXML.append("</id>");
            sbXML.append("<value>");
            sbXML.append((String) hcboVal.get("CVALUE"));
            sbXML.append("</value>");
            sbXML.append("<name>");
            sbXML.append((String) hcboVal.get("CNAME"));
            sbXML.append("</name>");
            sbXML.append("<desc>");
            sbXML.append((String) hcboVal.get("CDESC"));
            sbXML.append("</desc>");
            sbXML.append("</code>");
          }
          sbXML.append("</const>");
        }
        sbXML.append("</data>");
      } else if (strOpt.equals("GETACCTCURR")) {

        HashMap hmAccDtl = new HashMap();
        hmAccDtl = gmCustSalesSetupBean.fetchRepAccountInfo(strAccId);

        strAccCurrId = GmCommonClass.parseNull((String) hmAccDtl.get("ACC_CURR_ID"));
        strAccCurrSign = GmCommonClass.parseNull((String) hmAccDtl.get("ACC_CURR_SYMB"));
        sbXML.append(strAccCurrId + "|" + strAccCurrSign);

      } else if (strOpt.equals("GETADD")) {
    	  
      }else if (strOpt.equals("addParts")) { 
    	  String strOrderId = GmCommonClass.parseNull(request.getParameter("orderID"));
    	  String strInputStr = GmCommonClass.parseNull(request.getParameter("hInputString"));
    	  String strAccid = GmCommonClass.parseNull(request.getParameter("acc"));
    	  String strRowid = "";
          hmParam.put("RELEASEORDERID", strOrderId);//ACk Order id
          hmParam.put("USERID", strUserId); 
          hmParam.put("INPUTSTR", strInputStr);
          hmParam.put("ACCID", strAccid);
          strRowid = gmDOTxnBean.saveOusPartDtls(hmParam);
          log.debug("strRowid..."+strRowid);
          //sbXML.append("<response><data><pnum><itemorderid>");
          sbXML.append(strRowid);
          //sbXML.append("</itemorderid></pnum></data><unitPrice></unitPrice></response>");
      }
      else if (strOpt.equals("removeParts")) { 
    	  String strOrderId = GmCommonClass.parseNull(request.getParameter("orderID"));
    	  String strInputStr = GmCommonClass.parseNull(request.getParameter("hInputString"));
    	  String strAccid = GmCommonClass.parseNull(request.getParameter("acc"));
    	  hmParam.put("RELEASEORDERID", strOrderId);// Ack Order id
          hmParam.put("INPUTSTR", strInputStr);
          hmParam.put("USERID", strUserId);
          hmParam.put("ACCID", strAccid);
          gmDOTxnBean. removeAckOrderQty(hmParam);
      }else if (strOpt.equals("CHECKPO")) { /* THIS IS USED TO CHECK THE AVAIABLITY OF PO-ID */
        strCustomerPoId = GmCommonClass.parseNull(request.getParameter("Txt_PO"));
        strCustomerPoId = GmCommonClass.parseNull(gmCust.checkCustomerPoId(strCustomerPoId));
        log.debug("From DB strCustemorPoId:::" + strCustomerPoId);
        sbXML.append(strCustomerPoId);
      } else if (strOpt.equals("CHECKDOID")) { /* THIS IS USED TO CHECK THE AVAIABLITY OF DO-ID */

        String strOrderId = GmCommonClass.parseNull(request.getParameter("doid"));
        String strSurgDate = "";
        hmDOIDval = gmCust.checkDoId(strOrderId);
        strOrderId = GmCommonClass.parseNull((String) hmDOIDval.get("DOMESSAGE"));
        strSurgDate = GmCommonClass.parseNull((String) hmDOIDval.get("SURGDATE"));
        if (strCountryCode.equals("au")) {
          sbXML.append(strOrderId);
          sbXML.append("-");
          sbXML.append(strSurgDate);
        } else {
          sbXML.append(strOrderId);
        }
      } else if (strOpt.equals("validateControlNumber")) { // MNTTASK-8623 - Called AJAX for control
                                                           // number validation.
        String ErrorMessge = "";
        String strDispatchTo = "";
        HashMap hmReturn = new HashMap();
        

        String strOrderId = GmCommonClass.parseNull(request.getParameter("doid"));
        String strPartNums = GmCommonClass.parseNull(request.getParameter("PartNums"));
        String strPartwithQty = GmCommonClass.parseNull(request.getParameter("PartNumwithQty"));
        String strPNum = GmCommonClass.parseNull(request.getParameter("pnum"));
        String strCNum = GmCommonClass.parseNull(request.getParameter("cnum"));
        String strOrderType = GmCommonClass.parseNull(request.getParameter("orderType"));
        String shipTo = GmCommonClass.parseNull(request.getParameter("shipTo"));
        String names = GmCommonClass.parseNull(request.getParameter("names"));
        String addressid = GmCommonClass.parseNull(request.getParameter("addressid"));
        String strSubmitFl = GmCommonClass.parseNull(request.getParameter("submitFl"));
        String strRepID = GmCommonClass.parseNull(request.getParameter("rep"));
        String strErrFl = "";
        String strEditOrderID = GmCommonClass.parseNull(request.getParameter("Editdoid"));

        String strCmpnyRule =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SLNUM_VALIDATION"));

        // call the below method to validate the Lot with field sales while tabout in lot field
        String strmessage =
            gmCust.validateFieldsalesLot(strEditOrderID, strPNum, strCNum, strRepID);
        // call the below method to validate the Lot is already billed
        String strBillingmessage = gmDORptBean.validateBilledLot(strPNum, strCNum);


        // Call only at the time of submit, 101260: Ack Order; 2520: New Quote
        if (strSubmitFl.equals("Y") && !strOrderType.equals("101260")
            && !strOrderType.equals("2520") && strCmpnyRule.equals("YES")) {
          hmParam.put("DOID", strOrderId);
          hmParam.put("CNUM", strCNum);
          hmParam.put("PNUM", strPNum);
          hmParam.put("ORDERTYPE", strOrderType);
          hmParam.put("PNUMSTR", strPartwithQty);
          // 101723: Control Number Attribute
          hmParam.put("REFTYPE", "101723");
          strErrFl = GmCommonClass.parseNull(gmDORptBean.validateLotNum(hmParam));
          if (!strmessage.equals("")) {
            String strmessagecontent = strErrFl + ";" + strmessage;
            strErrFl = !strErrFl.equals("") ? strmessagecontent : strmessage;
          }
        }

        if (!strErrFl.equals("")) {
          sbXML.append("<table>");
          sbXML.append("<tr><td>");
          sbXML.append(strErrFl);
          sbXML.append("</td></tr>");
          sbXML.append("</table>");
          // sbXML.setLength(0);
        }
        if (!strSubmitFl.equals("Y") || !strCmpnyRule.equals("YES")) {
          request.setAttribute("RE_FORWARD", "GmOrderAjaxServlet");
          request.setAttribute("RE_TXN", "50925");
          request.setAttribute("txnStatus", "PROCESS");
          request.setAttribute("hAction", "PlaceOrder");
          request.setAttribute("TXN_TYPE_ID", "101723");// 101723: Control Number Attribute

          strDispatchTo =
              "/gmRuleEngine.do?pnum=" + strPNum + "&cntrlnum=" + strCNum + "&orderType="
                  + strOrderType + "&hOrderId" + strOrderId + "&PartNums="
                  + URLEncoder.encode(strPartNums, "UTF-8") + "&RefName=RFS&shipTo=" + shipTo
                  + "&names=" + names + "&addressid=" + addressid;
          dispatch(strDispatchTo, request, response);

          // Once we get CONSEQUENCES attribute from the rule add error message.
          ArrayList alResult =
              GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("CONSEQUENCES"));
          log.debug("alResult::" + alResult.size() + "::alResult::" + alResult);
          int size = alResult.size();
          boolean blErrorMessageFound = false;
          boolean blErrorMessage = false;

          sbXML.append("<table>");
          for (int i = 0; i < size; i++) {
            hmReturn = (HashMap) alResult.get(i);
            boolean blAddErrorMesssage = true;
            String strRuleId = GmCommonClass.parseNull((String) hmReturn.get("ID"));
            String strHoldFl = GmCommonClass.parseNull((String) hmReturn.get("HOLDTXN"));
            log.debug("Id:" + strRuleId + "$Hold Fl:" + strHoldFl);
            // IF Order Type is not Bill & Ship, do not add RFS/BBA rules to the error message
            if ((!strOrderType.equals("2521") && (strRuleId.equals("RFS") || strRuleId
                .equals("BBA")))) {
              blAddErrorMesssage = false;
            } else if (!strRuleId.equals("CNV") && !strHoldFl.equals("Y")) {
              blAddErrorMesssage = false;
            }

            if (blAddErrorMesssage) {
              ErrorMessge = GmCommonClass.parseNull((String) hmReturn.get("MESSAGE"));
              sbXML.append("<tr><td>");
              sbXML.append(ErrorMessge);
              sbXML.append("</td></tr>");
              log.debug("ErrorMessge::" + ErrorMessge);
              blErrorMessageFound = true;
              blErrorMessage = true;
            }
          }
          /*
           * Set the lot error message if the blErrorMessage is true then the strmessage is coming
           * as not null
           */
          if (!blErrorMessage && !strmessage.equals("")) {
            sbXML.append("<tr><td>");
            sbXML.append(strmessage);
            sbXML.append("</td></tr>");
            log.debug("ErrorMessge for fieldsales Lot::" + strmessage);
            blErrorMessageFound = true;
          }
          if (!blErrorMessage && !strBillingmessage.equals("")) {
            sbXML.append("<tr><td>");
            sbXML.append(strBillingmessage);
            sbXML.append("</td></tr>");
            log.debug("ErrorMessge for fieldsales Lot::" + strBillingmessage);
            blErrorMessageFound = true;
          }
          sbXML.append("</table>");

          if (!blErrorMessageFound)
            sbXML.setLength(0);
          log.debug("Exit Loop sbXML.toString()::" + sbXML.toString());
        }
      }
      response.setContentType("text/xml;charset=utf-8");
      response.setHeader("Cache-Control", "no-cache");

      PrintWriter writer = response.getWriter();
      writer.println(sbXML.toString());
      log.debug("sbXML.toString()::" + sbXML.toString());
      writer.close();

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      String strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }
}
