/*****************************************************************************
 * File			 : GmConsignServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;
import com.globus.common.beans.*;
import com.globus.custservice.beans.*;
import com.globus.operations.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmConsignServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
		String strDispatchTo = strCustPath.concat("/GmConsignReport.jsp");

		try
		{
			if (session == null) // Checks if the current session is the logged-in session, else redirecting to SessionExpiry page
			{
			   gotoPage(strComnPath.concat("/GmSessionExpiry.jsp"),request,response);
			   return;
		    }
			else
			{
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				GmCommonClass gmCommon = new GmCommonClass();
				GmCustomerBean gmCust = new GmCustomerBean();
				GmOperationsBean gmOper = new GmOperationsBean();

				HashMap hmReturn = new HashMap();
				HashMap hmResult = new HashMap();
				ArrayList alResult = new ArrayList();
				
				String strConsignId = "";
				
				String strStatFlag = "";
	            String strVeriFlag = "";           
	            String strSetId = "";
	            

	            strStatFlag = GmCommonClass.parseNull((String)request.getParameter("staInputs"));
	            strVeriFlag = GmCommonClass.parseNull((String) request.getParameter("veriFlag"));            
	            strSetId = GmCommonClass.parseNull((String) request.getParameter("setID"));
	            strConsignId = GmCommonClass.parseNull((String) request.getParameter("consignID"));

	            HashMap hmValues = new HashMap();
	            hmValues.put("SFL", strStatFlag);
	            hmValues.put("VFL", strVeriFlag);
	            hmValues.put("SETID", strSetId);
	            hmValues.put("CONID", strConsignId);
				if (strAction.equals("Load"))
				{
					alResult = gmOper.getSetBuildDashboard(hmValues);
					hmReturn.put("INISETS",alResult);
					//hmReturn = gmOper.reportDashboard("1");
					request.setAttribute("hmReturn",hmReturn);
				}
				else if (strAction.equals("Add") || strAction.equals("Edit"))
				{

				}
				else if (strAction.equals("EditLoad"))
				{
					strConsignId = (String)request.getParameter("hId")==null?"":(String)request.getParameter("hId");

					hmResult = gmOper.loadSavedSetMaster(strConsignId);
					hmReturn.put("SETLOAD",hmResult.get("SETLOAD"));
					hmResult = gmOper.viewBuiltSetDetails(strConsignId);
					hmReturn.put("CONDETAILS",hmResult);
					request.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hAction",strAction);
					strDispatchTo = strCustPath.concat("/GmSetPicSlip.jsp");
				}
				dispatch(strDispatchTo,request,response);
			} //End of else (session == null)
		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmConsignServlet