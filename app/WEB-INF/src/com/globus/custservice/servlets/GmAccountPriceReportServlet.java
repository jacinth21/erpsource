/*****************************************************************************
 * File : GmAccountPriceReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.pricing.beans.GmAccountPriceReportBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;

public class GmAccountPriceReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = "";
    String strType = "";
    String strOptVar1 = "";

    try {
      // checkSession(response, session); // Checks if the current session
      // is valid, else redirecting to
      // SessionExpiry page
      instantiate(request, response);
      String strMode = request.getParameter("hMode");
      strMode = (strMode == null) ? "0" : strMode;

      String strOpt = (String) session.getAttribute("strSessOpt");
      strOpt = (strOpt == null) ? "Load" : strOpt;

      if (!strOpt.equals("") && strOpt.indexOf("@") != -1) {
        strOptVar1 = strOpt.substring(0, strOpt.indexOf("@"));
        strType = strOpt.substring(strOpt.indexOf("@") + 1);
        strOpt = strOptVar1;
        log.debug(" Opt var1 is " + strOpt + " strType value is " + strType);
      } else {
        strType = request.getParameter("hType");;
      }

      log.debug("strOpt is " + strOpt);
      log.debug("strType is " + strType);
      log.debug("strMode is " + strMode);
      String strAccId = "";
      String strProjId = "";
      String strId = "";
      String strPartNum = "";
      String strQueryBy = "";
      String strLikeOption = "";
      String strhMode = "Load";
      String strAccCurrSymb = "";
      String strVoidFl = "";

      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
      GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());
      GmAccountPriceReportBean gmAccountPriceReportBean = new GmAccountPriceReportBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmValues = new HashMap();
      HashMap hmAccDtl = new HashMap();

      ArrayList alProjectList = new ArrayList();
      ArrayList alAccountList = new ArrayList();
      ArrayList alSystemList = new ArrayList();
      ArrayList alLikeOptions = new ArrayList();
      ArrayList alYesNoList= new ArrayList();

      if (strType.equals("Account")) {
        hmReturn = gmSales.loadAccountPricing();
        alLikeOptions = GmCommonClass.getCodeList("LIKES");
        alYesNoList = GmCommonClass.getCodeList("YES/NO");
        hmReturn.put("LIKEOPTIONS", alLikeOptions);
        hmReturn.put("YES/NO", alYesNoList);
      } else { // if type is party
        if (strType.equals("GPO")) {
          alAccountList = gmPartyInfoBean.fetchPartyNameList("7003");
        } else if (strType.equals("ICT")) {
          alAccountList = gmPartyInfoBean.fetchPartyNameList("7004");
        }
        
        hmReturn.put("ACCOUNTLIST", alAccountList);
        hmTemp.put("COLUMN", "ID");
        hmTemp.put("STATUSID", "20301");
        alProjectList = gmProject.reportProject(hmTemp);
        alSystemList = gmPricingRequestBean.loadSystemsList();
        hmReturn.put("SYSTEMLIST", alSystemList);
        hmReturn.put("PROJLIST", alProjectList);
        alYesNoList = GmCommonClass.getCodeList("YES/NO");
        hmReturn.put("YES/NO", alYesNoList);
      }

      if (strMode.equals("Reload")) {
        strAccId = request.getParameter("Cbo_AccId");
        strProjId = request.getParameter("Cbo_ProjId");
        strPartNum = request.getParameter("hPartNum");
        strQueryBy = request.getParameter("Cbo_QueryBy");
        strLikeOption = request.getParameter("Cbo_Search");
        strType = request.getParameter("hType");
        strVoidFl = request.getParameter("Cbo_Void");
        hmValues.put("ACCID", strAccId);
        hmValues.put("PROJID", strProjId);
        hmValues.put("TYPE", strType);
        hmValues.put("PARTNUM", strPartNum);
        hmValues.put("QUERYBY", strQueryBy);
        hmValues.put("LIKE_OPTION", strLikeOption);
        hmValues.put("VOIDFL", strVoidFl);
        hmResult = gmSales.reportAccountPricing(hmValues);
        strhMode = "Reload";
      } else if (strMode.equals("LoadPrices")) {
        strAccId = request.getParameter("Cbo_AccId");
        strProjId = request.getParameter("Cbo_ProjId");
        strId =
            (String) session.getAttribute("strSessUserId") == null ? "0" : (String) session
                .getAttribute("strSessUserId");

        String strMsg = gmSales.batchLoadAccountPricing(strAccId, strProjId, strId);
        request.setAttribute("hMode", "Reload");
        hmValues.put("ACCID", strAccId);
        hmValues.put("PROJID", strProjId);
        hmValues.put("TYPE", strType);
        hmResult = gmSales.reportAccountPricing(hmValues);
        strhMode = "Reload";
      } else if (strMode.equals("BatchUpdate")) {
        strAccId = request.getParameter("Cbo_AccId");
        strId =
            (String) session.getAttribute("strSessUserId") == null ? "0" : (String) session
                .getAttribute("strSessUserId");
        String strTemp = request.getParameter("Txt_Batch");
        strTemp = strTemp.replaceAll("\t", "");
        strTemp = strTemp.replaceAll("\r\n", "|");
        strTemp = strTemp.concat("|");
        hmValues.put("ACCID", strAccId);
        hmValues.put("INPUTSTR", strTemp);
        hmValues.put("USERID", strId);
        hmValues.put("TYPE", strType);
        // companyinfo required when updating price from JMS
        hmValues.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
        hmResult = gmSales.saveBatchAcctPricing(hmValues);
        strhMode = "Reload";
	} else if (strMode.equals("Save")) {//PC-5504 - this condition for to revert the part price from current price to previous price
		log.debug("strMode is " + strMode);
		String strInputString = request.getParameter("hInputStr");
		strAccId = request.getParameter("Cbo_AccId");
		strId = (String) session.getAttribute("strSessUserId") == null ? "0" : (String) session.getAttribute("strSessUserId");
		strType = request.getParameter("hType");
		strProjId = request.getParameter("Cbo_ProjId");
        strPartNum = request.getParameter("hPartNum");
        strQueryBy = request.getParameter("Cbo_QueryBy");
        strLikeOption = request.getParameter("Cbo_Search");
        strVoidFl = request.getParameter("Cbo_Void");
		hmValues.put("INPUTSTR", strInputString);
		hmValues.put("ACCID", strAccId);
		hmValues.put("USERID", strId);
        hmValues.put("PROJID", strProjId);
        hmValues.put("TYPE", strType);
        hmValues.put("PARTNUM", strPartNum);
        hmValues.put("QUERYBY", strQueryBy);
        hmValues.put("LIKE_OPTION", strLikeOption);
        hmValues.put("VOIDFL", strVoidFl);
		log.debug("hmValues is " + hmValues);
		gmAccountPriceReportBean.savePartpricing(hmValues);
		hmResult = gmSales.reportAccountPricing(hmValues);
		strhMode = "Reload";

	} else if (strMode.equals("Void")) { //PC-5504 - this condition for to revert the part price from current price to previous price
		String strInputString = request.getParameter("hInputStr");
		strAccId = request.getParameter("Cbo_AccId");
		strId = (String) session.getAttribute("strSessUserId") == null ? "0" : (String) session.getAttribute("strSessUserId");
		strType = request.getParameter("hType");
		strProjId = request.getParameter("Cbo_ProjId");
	    strPartNum = request.getParameter("hPartNum");
	    strQueryBy = request.getParameter("Cbo_QueryBy");
	    strLikeOption = request.getParameter("Cbo_Search");
	    strVoidFl = request.getParameter("Cbo_Void");
		hmValues.put("INPUTSTR", strInputString);
		hmValues.put("USERID", strId);
		hmValues.put("ACCID", strAccId);
        hmValues.put("PROJID", strProjId);
        hmValues.put("TYPE", strType);
        hmValues.put("PARTNUM", strPartNum);
        hmValues.put("QUERYBY", strQueryBy);
        hmValues.put("LIKE_OPTION", strLikeOption);
        hmValues.put("VOIDFL", strVoidFl);
		log.debug("hmValues is " + hmValues);
		gmAccountPriceReportBean.voidPartPricing(hmValues);
		hmResult = gmSales.reportAccountPricing(hmValues);
		strhMode = "Reload";
	}

      hmAccDtl = gmCustSalesSetupBean.fetchRepAccountInfo(strAccId);
      strAccCurrSymb = GmCommonClass.parseNull((String) hmAccDtl.get("ACC_CURR_SYMB"));
      String strSesPartyId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
      HashMap hmPricingSetupAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId, "ACC_PRIC_PARAM_SUBMT"));
      String strBtnAccessFl = GmCommonClass.parseNull((String) hmPricingSetupAccess.get("UPDFL"));
      request.setAttribute("BTNACCESSFL", strBtnAccessFl);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrSymb);
      request.setAttribute("hMode", strhMode);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hType", strType);
      request.setAttribute("hAccId", strAccId);
      request.setAttribute("hProjId", strProjId);
      request.setAttribute("hQueryBy", strQueryBy);
      request.setAttribute("hPartNum", strPartNum);
      request.setAttribute("hLikeOption", strLikeOption);
      request.setAttribute("hmResult", hmResult);
      request.setAttribute("REPORT", hmResult.get("REPORT"));
      request.setAttribute("hmodeCheck", strMode);
      request.setAttribute("hVoidFl", strVoidFl);
  	  log.debug("strVoidFl is " + strVoidFl);
  	 log.debug("strBtnAccessFl is " + strBtnAccessFl);
      dispatch(strOperPath.concat("/GmAccountPriceReport.jsp"), request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmAccountPriceReportServlet
