/*****************************************************************************
 * File			 : GmCustFrameServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import com.globus.common.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;

import org.apache.log4j.Logger;

import java.util.*;

public class GmCustFrameServlet extends GmServlet {
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
                                                                    // log4j

    public void init(ServletConfig config) throws ServletException {
        super.init(config);

    }

    public void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.debug("Enter");
        HttpSession session = request.getSession(false);

        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
        String strRootPath = GmCommonClass.getString("GMROOT");
        String strDispatchTo = "";
        String strPageToLoad = "";
        String strSubMenu = "";
        String strFrom = "";
        String strId = "";
        String strPO = "";

        try {
            log.debug("Entering Control Checks");
            checkSession(response, session); // Checks if the current session
                                                // is valid, else redirecting to
                                                // SessionExpiry page
            strFrom = request.getParameter("hFrom");

            if (strFrom != null && strFrom.equals("ProjReport")) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessProjectId", strId);
                session.setAttribute("hAction", "EditLoad");
            } else if (strFrom != null && strFrom.equals("EditControl")
                    || strFrom != null && strFrom.equals("EditShip")) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessOrderId", strId);
                session.setAttribute("hAction", strFrom);
            } else if (strFrom != null && strFrom.equals("Dashboard")) {
                strId = request.getParameter("hId");
                String strMode = request.getParameter("hMode");
                session.setAttribute("strSessOrderId", strId);
                session.setAttribute("hAction", strMode);

                if (strMode.equals("INV") || strMode.equals("PAY")) {
                    strPO = request.getParameter("hPO");
                    session.setAttribute("strSessPO", strPO);
                    session.setAttribute("strSessAccId", strId);
                    session.setAttribute("hAction", strMode);
                }
                session.setAttribute("hMode", strMode);
            } else if (strFrom != null && strFrom.equals("SetDashboard")) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessConsignId", strId);
                session.setAttribute("hAction", "EditLoad");
            } else if (strFrom != null && strFrom.equals("ItemDashboard")) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessConsignId", strId);
                session.setAttribute("hAction", "EditControl");
            } else if (strFrom != null && strFrom.equals("LoadItemShip")) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessConsignId", strId);
                session.setAttribute("hAction", "LoadItemShip");
            } else if (strFrom != null && strFrom.equals("LoadSetShip")) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessConsignId", strId);
                session.setAttribute("hAction", "LoadSetShip");
            } else if (strFrom != null && strFrom.equals("ReturnsDashboard")) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessReturnId", strId);
                session.setAttribute("hAction", "CreditReturn");
            } else if (strFrom != null && (strFrom.equals("TransferDashboard"))) {
                strId = request.getParameter("hId");
                session.setAttribute("strTransferId", strId);
                session.setAttribute("hAction", "Load");
            } else if (strFrom != null && (strFrom.equals("SaveTransfer"))) {
                strId = request.getParameter("hId");
                session.setAttribute("strTransferId", strId);
                session.setAttribute("hAction", "Save");
            } else if(strFrom != null && (strFrom.equals("DummyConsignment"))) {
                strId = request.getParameter("hId");
                session.setAttribute("strSessConsignId", strId);
                session.setAttribute("hAction", "LoadSetDummy");
            }
            log.debug("Exiting Control Checks");
            strPageToLoad = request.getParameter("hPgToLoad");

            strSubMenu = request.getParameter("hSubMenu");

            strPageToLoad = (strPageToLoad == null) ? "Blank" : strPageToLoad;
            strSubMenu = (strSubMenu == null) ? "Home" : strSubMenu;
            log.debug("Page to Load:" + strPageToLoad);
            session.setAttribute("strSessPgToLoad", strPageToLoad);
            session.setAttribute("strSessSubMenu", strSubMenu);
            log.debug("Sub Menu:" + strSubMenu);
            gotoPage(strRootPath.concat("/GmCustService.jsp"), request,
                    response);

        }// End of try
        catch (Exception e) {
            session.setAttribute("hAppErrorMessage", e.getMessage());
            strDispatchTo = strComnPath + "/GmError.jsp";
            gotoPage(strDispatchTo, request, response);
        }// End of catch
        log.debug("Exit");
    }// End of service method
}// End of GmProdFrameServlet
