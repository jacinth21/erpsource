/*****************************************************************************
 * File : GmPartSearchServlet.java Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;

public class GmPartSearchServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  /**
   * getXmlGridData - This method will return grid data.
   * 
   * @param ArrayList - list to show on grid
   * 
   * @return String
   * @exception AppError
   **/
  private String generateOutPut(ArrayList alParam) throws Exception {

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmPartNumSearch", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPartSearch.vm");
    templateUtil.setTemplateSubDir("custservice/templates");

    return templateUtil.generateOutput();
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strCustPath.concat("/GmPartSearch.jsp");
    ArrayList alSearchReport;

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);

      String strDepartMentID =
          GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));

      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strPartNum = "";
      String strDesc = "";

      strPartNum =
          request.getParameter("Txt_PartNum") == null ? "" : request.getParameter("Txt_PartNum");
      strDesc = request.getParameter("Txt_Desc") == null ? "" : request.getParameter("Txt_Desc");

      if (strAction.equals("Go")) {
        hmReturn = gmCust.loadPartSearchReport(strPartNum, strDesc, strDepartMentID);
      }

      if (!strAction.equals("Load")) {
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hDesc", strDesc);
        request.setAttribute("hmReturn", hmReturn);
      }
      alSearchReport = (ArrayList) hmReturn.get("SEARCHREPORT");
      String strGridData = generateOutPut(alSearchReport);
      request.setAttribute("alSearchReport", strGridData);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPartSearchServlet
