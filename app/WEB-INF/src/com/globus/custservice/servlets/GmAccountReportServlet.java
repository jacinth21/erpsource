/*****************************************************************************
 * File : GmAccountReportServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmAccountReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = strCustPath.concat("/GmAccountReport.jsp");
    HashMap hmParam = new HashMap();
    HashMap hmGridData = new HashMap();
    ArrayList alGpo = new ArrayList();
    String strCondition = null;
    String strGpoId = "";
    String strFilterCondition = "";
    String strParentFl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strDistId = GmCommonClass.parseNull(request.getParameter("hDistId"));

      String strColumn = request.getParameter("hColumn");
      strColumn = (strColumn == null) ? "ID" : strColumn;
      String strAccId = "";
      strAccId = request.getParameter("hId");
      strAccId = (strAccId == null) ? "0" : strAccId;
      String strAction = "";

      strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      strGpoId = GmCommonClass.parseNull(request.getParameter("Cbo_GpoId"));

      // Get cookie only on onload for the first time and not on the Go button
      strGpoId = strGpoId.equals("") ? gmCookieManager.getCookieValue(request, "cGpoId") : strGpoId;

      GmFramework gmFramework = new GmFramework();
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmPartyBean gmParty = new GmPartyBean(getGmDataStoreVO());
      alGpo = gmParty.reportPartyNameList("7003");

      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());

      HashMap hmSalesFilters = new HashMap();
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      request.setAttribute("hmSalesFilters", hmSalesFilters);

      HashMap hmApplnParam = new HashMap();
      hmApplnParam.put("SESSDATEFMT", strApplDateFmt);

      ArrayList alReturn = new ArrayList();
      ArrayList alAccountList = new ArrayList();
      ArrayList alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();

      strCondition = getAccessCondition(request, response, true);
      HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      String strAccCurrencyId = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccCurrencyId = strAccCurrencyId.equals("0") ? strCompanyCurrId : strAccCurrencyId;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccCurrencyId);
      String strCompanyLocale =
          GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
      String strARReportByDealerFlag =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));
      request.setAttribute("ARRptByDealerFlag", strARReportByDealerFlag);
      hmParam.put("OPT", strOpt);
      hmParam.put("ACCESS", strCondition);
      hmParam.put("SALESFILTER", strFilterCondition);
      hmParam.put("DISTID", strDistId);
      hmParam.put("GPOID", strGpoId);

      gmFramework.getRequestParams(request, response, session, hmParam);
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));

      if (strOpt.equals("ALL") || strOpt.equals("REPALL")) // To show all Accounts - For Customer
                                                           // Service
      {
        alReturn = gmSales.reportDistributorAccount(hmParam);
        HashMap hmStrOpt = new HashMap();
        hmStrOpt.put("strOpt", strOpt);
        hmStrOpt.put("SESSDATEFMT", hmApplnParam);
        hmStrOpt.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
        String strgrid = generateOutPut(hmStrOpt, alReturn);
        request.setAttribute("gridData", strgrid);
      } else if (strOpt.equals("ACCTRPT")) // ACCTRPT - All Account Report Left Link
      {
        strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
        hmParam.put("ACCID", strAccId);
        hmParam.put("PARENTFL", strParentFl);
        hmParam.put("ACC_CURR_ID", strAccCurrencyId);
        if (strAction.equals("Reload")) {
          strParentFl = strParentFl.equals("on") ? "true" : "false";
          alReturn = gmSales.fetchAccountsReport(hmParam);
        }
        HashMap hmStrOpt = new HashMap();
        hmStrOpt.put("ARREPORTBYDEALER", strARReportByDealerFlag);
        hmStrOpt.put("strOpt", strOpt);
        hmStrOpt.put("SESSDATEFMT", hmApplnParam);
        String strgrid = generateOutPut(hmStrOpt, alReturn);
        request.setAttribute("gridData", strgrid);
        request.setAttribute("hAccId", strAccId);
        request.setAttribute("Chk_ParentFl", strParentFl);
      } else {
        // To get the Access Level information
        alReturn = gmSales.reportDistributorAccount(hmParam);
        strDispatchTo = strSalesPath.concat("/GmAccountSalesReport.jsp");
        String strXmlData = generateOutPut(alReturn, strSessCompanyLocale);
        request.setAttribute("strXmlData", strXmlData);
      }
      request.setAttribute("alReturn", alReturn);
      request.setAttribute("alGpo", alGpo);
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hGpoId", strGpoId);
      request.setAttribute("ACC_CURR_ID", strAccCurrencyId);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
      request.setAttribute("ARREPORTBYDEALERFLAG", strARReportByDealerFlag);

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cGpoId", strGpoId);

      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private String generateOutPut(ArrayList alGridData, String strSessCompanyLocale) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.GmAccountSalesReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmAccountSalesReport.vm");
    return templateUtil.generateOutput();
  }

  /*
   * The below code is modified because the GmAccountReport template was used at two places. Which
   * was casing an issue on Sales - Field Sales - Field Sales/Account Report. So creating a separate
   * new VM file for AR - Accounts Report.
   */
  private String generateOutPut(HashMap hmStrOpt, ArrayList alParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmStrOpt.get("STRSESSCOMPANYLOCALE"));
    HashMap hm1 = new HashMap();
    hm1 = (HashMap) hmStrOpt.get("SESSDATEFMT");
    String strOpt = GmCommonClass.parseNull((String) hmStrOpt.get("strOpt"));
    String strTemplate = strOpt.equals("ACCTRPT") ? "GmAllAccountsReport.vm" : "GmAccountReport.vm";
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmAccountReport", strSessCompanyLocale));
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmStrOpt", hmStrOpt);
    templateUtil.setDataMap("hmApplnParam", hm1);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir("custservice/templates");

    return templateUtil.generateOutput();
  }

}// End of GmAccountReportServlet
