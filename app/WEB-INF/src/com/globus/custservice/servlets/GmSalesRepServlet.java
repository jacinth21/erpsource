/*****************************************************************************
 * File : GmSalesRepServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;

public class GmSalesRepServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = "";
    String strAccessFl = "";
    String strXmlGridData = "";
    /**
	 * PMT-28307 Access Driven Sales Rep Setup 
	*/
    String strSalesRepEditAcsFl = "";
    String strPartyId="";
    String strAccessPartyId="";
    try {
      // checkSession(response, session); // Checks if the current session is valid
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());
      String strAction = request.getParameter("hAction");
      String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
      strAction = (strAction == null) ? "" : strAction;
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmCustSalesSetupBean gmCustSetup = new GmCustSalesSetupBean(getGmDataStoreVO());
      GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmAutoCompleteTransBean gmAutoCompleteTransBean =
          new GmAutoCompleteTransBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      HashMap hmload = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmAccess = new HashMap();

      ArrayList alReturn = new ArrayList();
      String strRepId = "";
  
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "SLS_LOG_CRD_ACC"));
      strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      request.setAttribute("ACCESSFL", strAccessFl);
      log.debug(" strAction  is " + strAction + "  strAccessFl==>" + strAccessFl);
      
      strPartyId = GmCommonClass.parseZero(request.getParameter("hPartyId")); // request.getParameter("hPartyId").equals("")
                                                                                // ? "0" :// request.getParameter("m");
      
      /**
       * PMT-28307 Access Driven Sales Rep Setup 
       */
      strAccessPartyId=GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId"));  
      hmAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strAccessPartyId,"SALES_REP_ADD_EDIT"));
      strSalesRepEditAcsFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("strSalesRepEditAcsFl"+strSalesRepEditAcsFl);
      request.setAttribute("SALES_REP_ADD_EDIT", strSalesRepEditAcsFl);
          
      if (strAction.equals("Add") || strAction.equals("Edit")) {
        String strFirstName =
            request.getParameter("Txt_FirstNm") == null ? "" : request.getParameter("Txt_FirstNm");
        String strLastName =
            request.getParameter("Txt_LastNm") == null ? "" : request.getParameter("Txt_LastNm");
        String strRepNameEn =
            GmCommonClass.parseNull(request.getParameter("Txt_RepNm_En")) == null ? ""
                : GmCommonClass.parseNull(request.getParameter("Txt_RepNm_En"));
        String strStDate =
            request.getParameter("Txt_StDate") == null ? "" : request.getParameter("Txt_StDate");
        String strEndDate =
            request.getParameter("Txt_EndDate") == null ? "" : request.getParameter("Txt_EndDate");
        String strDistId =
            request.getParameter("Cbo_DistId") == null ? "" : request.getParameter("Cbo_DistId");
        String strCatId =
            request.getParameter("Cbo_CatId") == null ? "" : request.getParameter("Cbo_CatId");
        String strOldCatgryId = GmCommonClass.parseNull(request.getParameter("hOld_Category"));
        String strDsgId =
            request.getParameter("Cbo_Dsgn") == null ? "" : request.getParameter("Cbo_Dsgn");
        String strTerritory =
            request.getParameter("Cbo_Terr") == null ? "" : request.getParameter("Cbo_Terr");
        String strActiveFl =
            GmCommonClass.parseNull(request.getParameter("Chk_ActiveFl")).equals("") ? "N"
                : request.getParameter("Chk_ActiveFl");
        String strOldActiveFl = GmCommonClass.parseNull(request.getParameter("hOld_ActiveFl"));
        String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
                                                                          
        strRepId =
            request.getParameter("Cbo_RepId") == null ? "" : request.getParameter("Cbo_RepId");
        String strLoginUserName =
            request.getParameter("UserName") == null ? "" : request.getParameter("UserName");
        String StrLoginCreden =
            request.getParameter("Chk_Login_Cr") == null ? "" : request
                .getParameter("Chk_Login_Cr");
        String strInputString = GmCommonClass.parseNull(request.getParameter("hRepDivInputStr"));
        String strPrimaryDivId = GmCommonClass.parseNull(request.getParameter("hPrimaryDivID"));
        HashMap hmValues = new HashMap();

        hmValues.put("FIRSTNAME", strFirstName);
        hmValues.put("LASTNAME", strLastName);
        hmValues.put("REPNAMEEN", strRepNameEn);
        hmValues.put("STDATE", getDateFromStr(strStDate));
        hmValues.put("ENDDATE", getDateFromStr(strEndDate));
        hmValues.put("DSGN", strDsgId);
        hmValues.put("DID", strDistId);
        hmValues.put("CATID", strCatId);
        hmValues.put("TID", strTerritory);
        hmValues.put("AFLAG", strActiveFl);
        hmValues.put("LOG", strLog);
        hmValues.put("REPID", strRepId);
        hmValues.put("PARTYID", strPartyId);
        hmValues.put("USERNAME", strLoginUserName);
        hmValues.put("LOGINCREDEN", StrLoginCreden);
        hmValues.put("INPUTSTRING", strInputString);
        hmValues.put("PRIMARYDIVID", strPrimaryDivId);
        strRepId = gmCustSetup.saveSalesRep(hmValues, strUserId, strAction);
        hmValues.put("REPID", strRepId);
        gmCustSetup.savSalesRepDivMap(hmValues);

        hmResult = gmCustSetup.loadEditSalesRep(strRepId);
        hmReturn.put("EDITLOAD", hmResult);

        request.setAttribute("hAction", "EditLoad");
        alReturn = gmCom.getLog(strRepId, "1211");
        request.setAttribute("hmLog", alReturn);

        GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
        // View refresh JMS called
        HashMap hmViewRefresh = new HashMap();
        hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
        hmViewRefresh.put("companyInfo",
            GmCommonClass.parseNull(request.getParameter("companyInfo")));
        String strViewConsumerClass =
            GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
        hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
        gmConsumerUtil.sendMessage(hmViewRefresh);
        // Redis JMS call
        String strOldSalesName = GmCommonClass.parseNull(request.getParameter("hOld_SalesNm"));
        String strRepName = strFirstName + " " + strLastName;
        if (!strRepName.equals(strOldSalesName) || !strOldActiveFl.equals(strActiveFl)
            || !strOldCatgryId.equals(strCatId)) {
          HashMap hmParam = new HashMap();
          hmParam.put("ID", strRepId);
          hmParam.put("OLD_NM", strOldSalesName);
          hmParam.put("NEW_NM", strRepName);
          hmParam.put("METHOD", "SalesRep");
          hmParam.put("OLD_ACTIVE_FL", strOldActiveFl);
          hmParam.put("NEW_ACTIVE_FL", strActiveFl);
          hmParam.put("OLD_CATGRY", strOldCatgryId);
          hmParam.put("NEW_CATGRY", strCatId);
          hmParam.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
          gmAutoCompleteTransBean.saveDetailsToCache(hmParam);
        }
      } else if (strAction.equals("EditLoad")) {
        strRepId =
            request.getParameter("Cbo_RepId") == null ? "" : request.getParameter("Cbo_RepId");
        log.debug("sales rep id : " + strRepId + ", Party ID  " + request.getParameter("hPartyId"));
        hmResult = gmCustSetup.loadEditSalesRep(request.getParameter("Cbo_RepId"));
        hmReturn.put("EDITLOAD", hmResult);
        request.setAttribute("hAction", "EditLoad");
        alReturn = gmCom.getLog(request.getParameter("Cbo_RepId"), "1211");
        request.setAttribute("hmLog", alReturn);
      }
      log.debug("load params");
      hmload = gmCustSetup.loadSalesRep();
      hmReturn.put("REPLOAD", hmload);
      hmReturn.put("REPLIST", alReturn);
      hmload.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      request.setAttribute("hmReturn", hmReturn);

      ArrayList alResult = new ArrayList();
      alResult = GmCommonClass.parseNullArrayList(gmCustSetup.fetchSalesRepDivMap(hmResult));
      strXmlGridData = GmCommonClass.parseNull(generateOutPut(alResult, hmload));
      request.setAttribute("GRIDDATA", strXmlGridData);

      dispatch(strOperPath.concat("/GmSalesRepSetup.jsp"), request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  /**
   * getXmlGridData - This method will be used to generate Output for grid data.
   * 
   * @param HashMap
   * @return String - will be used by dhtmlx grid
   * @exception AppError
   */
  private String generateOutPut(ArrayList alParam, HashMap hmParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setTemplateName("GmSalesRepDivMap.vm");
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmSalesRepSetup", strSessCompanyLocale));
    if (hmParam != null) {
      templateUtil.setDropDownMaster("alCompDivList", (ArrayList) hmParam.get("COMPDIVLIST"));
      templateUtil.setDropDownMaster("alDivMapTypeList", (ArrayList) hmParam.get("DIVMAPTYPELIST"));
    }
    return templateUtil.generateOutput();
  }
}// End of GmSalesRepServlet
