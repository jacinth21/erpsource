/*****************************************************************************
 * File : GmDistributorServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.custservice.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;


public class GmDistributorServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = "";
    String strdispatch = "";
    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmCustSalesSetupBean gmCustSetup = new GmCustSalesSetupBean(getGmDataStoreVO());
      GmCommonBean gmCom = new GmCommonBean();
      GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean();
      GmAutoCompleteTransBean gmAutoCompleteTransBean =
          new GmAutoCompleteTransBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      ArrayList alReturn = new ArrayList();
      HashMap hmRuleData = new HashMap();
      // ArrayList alDistTyp = new ArrayList();
      // ArrayList alPartyPric = new ArrayList();
      String strInputString = "";
      String strSkipRFS = "";
      String strDistId = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      // To Validate the SKIP_RFS_FLAG.If SKIP_RFS_FLAG is 'Y' it will enable new MAPPARAMETER.
      HashMap hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "RFS_SKIP"));
      String accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("hAccess", accessFlag);
      if (strAction.equals("Load")) {
        hmResult = gmCustSetup.loadDistributor();
        hmReturn.put("DISTLOAD", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        // alDistTyp = GmCommonClass.getCodeList("DSTYP");
        /*
         * log.debug("alDistTyp  "+alDistTyp); hmReturn.put("DISTTYP",alDistTyp); alPartyPric=
         * gmPartyBean.reportPartyNameList("7004"); log.debug("PartyPric  "+alPartyPric);
         * hmReturn.put("ALPARTYPRIC",alPartyPric);
         */
        strdispatch = strOperPath.concat("/GmDistributorSetup.jsp");
      } else if (strAction.equals("Add") || strAction.equals("Edit")) {
        String strDistNm =
            request.getParameter("Txt_DistNm") == null ? "" : request.getParameter("Txt_DistNm");
        String strDistNmEn =
            GmCommonClass.parseNull(request.getParameter("Txt_DistNmEn")) == null ? ""
                : GmCommonClass.parseNull(request.getParameter("Txt_DistNmEn"));
        String strDistShortNm = GmCommonClass.parseNull(request.getParameter("Txt_Dist_Short_Nm"));
        String strRegion =
            request.getParameter("Cbo_Region") == null ? "" : request.getParameter("Cbo_Region");
        String strDistNames =
            request.getParameter("Txt_DistNames") == null ? "" : request
                .getParameter("Txt_DistNames");
        String strContactPerson =
            request.getParameter("Txt_ContPer") == null ? "" : request.getParameter("Txt_ContPer");
        String strComnPerc =
            request.getParameter("Txt_ComnPerc") == null ? "" : request
                .getParameter("Txt_ComnPerc");
        String strDistTyp =
            request.getParameter("Cbo_DistTyp") == null ? "" : request.getParameter("Cbo_DistTyp");
        log.debug("message strDistTyp " + strDistTyp);
        String strPartyPric =
            request.getParameter("Cbo_PartyPric") == null ? "" : request
                .getParameter("Cbo_PartyPric");
        log.debug("message strPartyPric " + strPartyPric);
        String strBillName =
            request.getParameter("Txt_BillName") == null ? "" : request
                .getParameter("Txt_BillName");
        String strShipName =
            request.getParameter("Txt_ShipName") == null ? "" : request
                .getParameter("Txt_ShipName");
        String strBillAdd1 =
            request.getParameter("Txt_BillAdd1") == null ? "" : request
                .getParameter("Txt_BillAdd1");
        String strShipAdd1 =
            request.getParameter("Txt_ShipAdd1") == null ? "" : request
                .getParameter("Txt_ShipAdd1");
        String strBillAdd2 =
            request.getParameter("Txt_BillAdd2") == null ? "" : request
                .getParameter("Txt_BillAdd2");
        String strShipAdd2 =
            request.getParameter("Txt_ShipAdd2") == null ? "" : request
                .getParameter("Txt_ShipAdd2");
        String strBillCity =
            request.getParameter("Txt_BillCity") == null ? "" : request
                .getParameter("Txt_BillCity");
        String strShipCity =
            request.getParameter("Txt_ShipCity") == null ? "" : request
                .getParameter("Txt_ShipCity");
        String strBillState =
            request.getParameter("Cbo_BillState") == null ? "" : request
                .getParameter("Cbo_BillState");
        String strShipState =
            request.getParameter("Cbo_ShipState") == null ? "" : request
                .getParameter("Cbo_ShipState");
        String strBillCountry =
            request.getParameter("Cbo_BillCountry") == null ? "" : request
                .getParameter("Cbo_BillCountry");
        String strShipCountry =
            request.getParameter("Cbo_ShipCountry") == null ? "" : request
                .getParameter("Cbo_ShipCountry");
        String strBillZipCode =
            request.getParameter("Txt_BillZipCode") == null ? "" : request
                .getParameter("Txt_BillZipCode");
        String strShipZipCode =
            request.getParameter("Txt_ShipZipCode") == null ? "" : request
                .getParameter("Txt_ShipZipCode");
        String strPhone =
            request.getParameter("Txt_Phn") == null ? "" : request.getParameter("Txt_Phn");
        String strFax =
            request.getParameter("Txt_Fax") == null ? "" : request.getParameter("Txt_Fax");
        String strStartDate =
            request.getParameter("Txt_StartDate") == null ? "" : request
                .getParameter("Txt_StartDate");
        String strEndDate =
            request.getParameter("Txt_EndDate") == null ? "" : request.getParameter("Txt_EndDate");
        String strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        String strActiveFl =
            request.getParameter("Chk_ActiveFl") == null ? "N" : request
                .getParameter("Chk_ActiveFl");
        String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        String strCommissionType =
            request.getParameter("Cbo_CommType") == null ? "" : request
                .getParameter("Cbo_CommType");
        String strICTAccountID = GmCommonClass.parseZero(request.getParameter("Cbo_ICT_Account"));
        strICTAccountID = strICTAccountID.equals("0") ? "" : strICTAccountID;
        HashMap hmValues = new HashMap();
        hmValues.put("DNAME", strDistNm);
        hmValues.put("DNAMEEN", strDistNmEn);
        hmValues.put("SHNAME", strDistShortNm);
        hmValues.put("REGION", strRegion);
        hmValues.put("DNAMES", strDistNames);
        hmValues.put("CPERSON", strContactPerson);
        hmValues.put("COMNPERC", strComnPerc);
        hmValues.put("DISTTYP", strDistTyp);
        hmValues.put("PARTYPRIC", strPartyPric);
        hmValues.put("BNAME", strBillName);
        hmValues.put("SNAME", strShipName);
        hmValues.put("BADD1", strBillAdd1);
        hmValues.put("SADD1", strShipAdd1);
        hmValues.put("BADD2", strBillAdd2);
        hmValues.put("SADD2", strShipAdd2);
        hmValues.put("BCITY", strBillCity);
        hmValues.put("SCITY", strShipCity);
        hmValues.put("BSTATE", strBillState);
        hmValues.put("SSTATE", strShipState);
        hmValues.put("BCOUNTRY", strBillCountry);
        hmValues.put("SCOUNTRY", strShipCountry);
        hmValues.put("BZIP", strBillZipCode);
        hmValues.put("SZIP", strShipZipCode);
        hmValues.put("PHONE", strPhone);
        hmValues.put("FAX", strFax);
        hmValues.put("START", getDateFromStr(strStartDate));
        hmValues.put("END", getDateFromStr(strEndDate));
        hmValues.put("COMMENTS", strComments);
        hmValues.put("AFLAG", strActiveFl);
        hmValues.put("LOG", strLog);
        hmValues.put("COMMISSIONTYPE", strCommissionType);
        log.debug(" Active fl " + strActiveFl);
        strDistId =
            request.getParameter("Cbo_DistId") == null ? "" : request.getParameter("Cbo_DistId");
        hmValues.put("DISTID", strDistId);
        hmValues.put("ICTACCID", strICTAccountID);

        HashMap hmSaveReturn = new HashMap();

        hmSaveReturn = gmCustSetup.saveDistributor(hmValues, strUserId, strAction);

        strDistId = (String) hmSaveReturn.get("DID");
        hmResult = gmCustSetup.loadEditDistributor(strDistId);
        hmReturn.put("EDITLOAD", hmResult);
        hmResult = gmCustSetup.loadDistributor();
        hmReturn.put("DISTLOAD", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
        request.setAttribute("hDistId", strDistId);
        alReturn = gmCom.getLog(strDistId, "1210");
        request.setAttribute("hmLog", alReturn);

        GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
        // View refresh JMS called
        HashMap hmViewRefresh = new HashMap();
        hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
        hmViewRefresh.put("companyInfo",
            GmCommonClass.parseNull(request.getParameter("companyInfo")));

        String strViewConsumerClass =
            GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
        hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
        gmConsumerUtil.sendMessage(hmViewRefresh);

        // Redis JMS call
        String strOldDistName = GmCommonClass.parseNull(request.getParameter("hOld_DistNm"));
        if (!strDistNm.equals(strOldDistName)) {
          HashMap hmParam = new HashMap();
          hmParam.put("ID", strDistId);
          hmParam.put("OLD_NM", strOldDistName);
          hmParam.put("NEW_NM", strDistNm);
          hmParam.put("METHOD", "FS");
          hmParam.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
          gmAutoCompleteTransBean.saveDetailsToCache(hmParam);
        }

        strdispatch = strOperPath.concat("/GmDistributorSetup.jsp");
      } else if (strAction.equals("EditLoad")) {
        strDistId =
            request.getParameter("Cbo_DistId") == null ? "" : request.getParameter("Cbo_DistId");
        hmResult = gmCustSetup.loadEditDistributor(strDistId);
        hmReturn.put("EDITLOAD", hmResult);
        hmResult = gmCustSetup.loadDistributor();
        hmReturn.put("DISTLOAD", hmResult);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
        request.setAttribute("hDistId", strDistId);
        alReturn = gmCom.getLog(strDistId, "1210");
        request.setAttribute("hmLog", alReturn);
        strdispatch = strOperPath.concat("/GmDistributorSetup.jsp");
      } else if (strAction.equals("MapParam") || strAction.equals("saveDistParam")) {
        strDistId = GmCommonClass.parseNull(request.getParameter("Cbo_DistId"));
        strInputString = GmCommonClass.parseNull(request.getParameter("hInputString"));
        if (strAction.equals("saveDistParam")) {
          hmRuleData.put("RULEGRPID", "SKIPRFS");
          hmRuleData.put("STRINPUTSTRING", strInputString);
          hmRuleData.put("USERID", strUserId);
          gmCommonBean.saveRuleParams(hmRuleData);
          request.setAttribute("hOpt", "Reload");
        }
        strSkipRFS = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strDistId, "SKIPRFS"));
        request.setAttribute("CHKSKIPRFS", strSkipRFS);
        request.setAttribute("hDistId", strDistId);
        strdispatch = strOperPath.concat("/GmDistParmsSetup.jsp");
      }
      dispatch(strdispatch, request, response);
    }// End of try

    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmDistributorServlet
