package com.globus.custservice.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;
/**
 *  GmOrderUsageLotReportForm - This is used for Usage Lot Fields and Properties
 * @author rajan
 *
 */
public class GmOrderUsageLotReportForm extends GmCommonForm{

	/**
	 * serialVersionUID is this class file's serial id
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 *  dealer is used String type
	 */
	private String dealer = "";
	
	/**
	 * account is used String type
	 */
	private String account = "";
	
	/**
	 * accountId is used String type
	 */
	private String accountId = "";
	
	/**
	 * datetype is used String type
	 */
	private String datetype = "";
	
	/**
	 * partNumber is used String type
	 */
	private String partNumber = "";
	
	/**
	 * fromDate is used String type
	 */
	private String fromDate = "";
	
	/**
	 * toDate is used String type
	 */
	private String toDate = "";
	
	/**
	 * missingLot is used String type
	 */
	private String missingLot = "";
	
	/**'
	 * alDateType is create ArrayList
	 */
	private ArrayList alDateType = new ArrayList();
	
	/**
	 * pnumSuffix is used String type
	 */
	private String pnumSuffix = "";
	
	/**
	 * xmlString is used String type
	 */
	private String xmlString = "";
	
	/**
	 * searchaccount is used String type
	 */
	private String searchaccount="";
	
	/**'
	 * searchDealer is used String type
	 */
	private String searchDealer="";
	
	/**
	 * locationType is used String type
	 */
	private String locationType ="";
	
	/**
	 * searchdealer is used String type
	 */
	private String searchdealer = "";

	/**
	 * orderId is used String type
	 */
	private String orderId = "";
	
	/**
	 * invoiceId is used String type
	 */
	private String invoiceId = "";
	/**
	 * fieldSales is used String type
	 */
	private String fieldSales = "";
	/**
	 * searchfieldSales is used String type
	 */
	private String searchfieldSales = "";
	/**
	 * searchsalesRep is used String type
	 */
	private String searchsalesRep = "";
	/**
	 * salesRep is used String type
	 */
	private String salesRep = "";
	
	/**
	 * lotNumber is used String type
	 */
	private String lotNumber = "";
	
	/**
	 * projectID is used String type
	 */
	private String  projectID  = "";
	
	/**
	 * searchprojectID is used String type
	 */
	private String  searchprojectID = "";
	
	/**
	 * strHideFSOrdUsageRptFl is used String type
	 */
	private String strHideOrdUsageRptFl = "";
	
	/**
	 * @return the strHideFSOrdUsageRptFl
	 */
	public String getStrHideOrdUsageRptFl() {
		return strHideOrdUsageRptFl;
	}
	/**
	 * @param strHideFSOrdUsageRptFl the strHideFSOrdUsageRptFl to set
	 */
	public void setStrHideOrdUsageRptFl(String strHideOrdUsageRptFl) {
		this.strHideOrdUsageRptFl = strHideOrdUsageRptFl;
	}
	/**
	 * @return the lotNumber
	 */
	public String getLotNumber() {
		return lotNumber;
	}
	/**
	 * @param lotNumber the lotNumber to set
	 */
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	/**
	 * @return the projectID
	 */
	public String getProjectID() {
		return projectID;
	}
	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}
	/**
	 * @return the searchprojectID
	 */
	public String getSearchprojectID() {
		return searchprojectID;
	}
	/**
	 * @param searchprojectID the searchprojectID to set
	 */
	public void setSearchprojectID(String searchprojectID) {
		this.searchprojectID = searchprojectID;
	}
	/**
	 * @return the fieldSales
	 */
	public String getFieldSales() {
		return fieldSales;
	}
	/**
	 * @param fieldSales the fieldSales to set
	 */
	public void setFieldSales(String fieldSales) {
		this.fieldSales = fieldSales;
	}
	/**
	 * @return the searchfieldSales
	 */
	public String getSearchfieldSales() {
		return searchfieldSales;
	}
	/**
	 * @param searchfieldSales the searchfieldSales to set
	 */
	public void setSearchfieldSales(String searchfieldSales) {
		this.searchfieldSales = searchfieldSales;
	}
	/**
	 * @return the searchsalesRep
	 */
	public String getSearchsalesRep() {
		return searchsalesRep;
	}
	/**
	 * @param searchsalesRep the searchsalesRep to set
	 */
	public void setSearchsalesRep(String searchsalesRep) {
		this.searchsalesRep = searchsalesRep;
	}
	/**
	 * @return the salesRep
	 */
	public String getSalesRep() {
		return salesRep;
	}
	/**
	 * @param salesRep the salesRep to set
	 */
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}
	/**
	 * @return
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return account
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	
	/**
	 * @return accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return datetype
	 */
	public String getDatetype() {
		return datetype;
	}
	/**
	 * @param datetype
	 */
	public void setDatetype(String datetype) {
		this.datetype = datetype;
	}
	/**
	 * @return partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}
	/**
	 * @param partNumber
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	/**
	 * @return fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return alDateType
	 */
	public ArrayList getAlDateType() {
		return alDateType;
	}
	/**
	 * @param alDateType
	 */
	public void setAlDateType(ArrayList alDateType) {
		this.alDateType = alDateType;
	}
	/**
	 * @return pnumSuffix
	 */
	public String getPnumSuffix() {
		return pnumSuffix;
	}
	/**
	 * @param pnumSuffix
	 */
	public void setPnumSuffix(String pnumSuffix) {
		this.pnumSuffix = pnumSuffix;
	}
	/**
	 * @return xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}
	
	/**
	 * @param xmlString
	 */
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	/**
	 * @return searchaccount
	 */
	public String getSearchaccount() {
		return searchaccount;
	}
	/**
	 * @param searchaccount
	 */
	public void setSearchaccount(String searchaccount) {
		this.searchaccount = searchaccount;
	}

	/**
	 * @return missingLot
	 */
	public String getMissingLot() {
		return missingLot;
	}
	/**
	 * @param missingLot
	 */
	public void setMissingLot(String missingLot) {
		this.missingLot = missingLot;
	}
	/**
	 * @return dealer
	 */
	public String getDealer() {
		return dealer;
	}
	/**
	 * @param dealer
	 */
	public void setDealer(String dealer) {
		this.dealer = dealer;
	}
	/**
	 * @return searchDealer
	 */
	public String getSearchDealer() {
		return searchDealer;
	}
	/**
	 * @param searchDealer
	 */
	public void setSearchDealer(String searchDealer) {
		this.searchDealer = searchDealer;
	}
	/**
	 * @return locationType
	 */
	public String getLocationType() {
		return locationType;
	}
	/**
	 * @param locationType
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	/**
	 * @return searchdealer
	 */
	public String getSearchdealer() {
		return searchdealer;
	}
	/**
	 * @param searchdealer
	 */
	public void setSearchdealer(String searchdealer) {
		this.searchdealer = searchdealer;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the invoiceId
	 */
	public String getInvoiceId() {
		return invoiceId;
	}
	/**
	 * @param invoiceId the invoiceId to set
	 */
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	
	

}
