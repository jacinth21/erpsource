
package com.globus.custservice.forms;
/**
 * FileName    : GmOrderTissuePartDtlsForm.java 
 * Description : The form is used to set / get the values for record tag screen
 * Author      : T.S. Ramachandiran
 * Copyright   : Globus Medical Inc
 */
import com.globus.common.forms.GmCommonForm;

public class GmOrderTissuePartDtlsForm extends GmCommonForm{

	
	private String strTxnId = "";
	private String strAccountId = "";
	
	
	/**
	 * @return strTxnId
	 */
	public String getStrTxnId() {
		return strTxnId;
	}
	/**
	 * @param strTxnId
	 */
	public void setStrTxnId(String strTxnId) {
		this.strTxnId = strTxnId;
	}
	/**
	 * @return strAccountId
	 */
	public String getStrAccountId() {
		return strAccountId;
	}
	/**
	 * @param strAccountId
	 */
	public void setStrAccountId(String strAccountId) {
		this.strAccountId = strAccountId;
	}
}
