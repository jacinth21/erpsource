package com.globus.custservice.forms;

import com.globus.common.forms.GmCommonForm;

public class GmAccountInfoForm extends GmCommonForm {
  private String accountId = "";
  private String sourceId = "";
  private String accountType = "";

  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  /**
   * @return the sourceId
   */
  public String getSourceId() {
    return sourceId;
  }

  /**
   * @param sourceId the sourceId to set
   */
  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  /**
   * @return the accountType
   */
  public String getAccountType() {
    return accountType;
  }

  /**
   * @param accountType the accountType to set
   */
  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }



}
