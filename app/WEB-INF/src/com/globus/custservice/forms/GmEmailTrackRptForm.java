package com.globus.custservice.forms;

import java.util.ArrayList;


import com.globus.common.forms.GmCommonForm;



public class GmEmailTrackRptForm extends GmCommonForm  {

	  private String toEmailId = "";
	  private String subject = "";
	  private String fromDate = "";
	  private String toDate = "";
	  private String message = "";
	  private String emailLogId = "";
	  private String strEmailMsg = "";
	  
	private String gridXmlData = new String();
	  

	    public String getEmailLogId() {
		     return emailLogId;
	    }

	    public void setEmailLogId(String emailLogId) {
		     this.emailLogId = emailLogId;
	    }
	  
		public String getMessage() {
		     return message;
	    }

	    public void setMessage(String message) {
		     this.message = message;
	    }

		public String getToEmailId() {
			return toEmailId;
		}

	    public void setToEmailId(String toEmailId) {
		   	this.toEmailId = toEmailId;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getFromDate() {
			return fromDate;
		}

		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}

		public String getToDate() {
			return toDate;
		}

		public void setToDate(String toDate) {
			this.toDate = toDate;
		}

		public String getGridXmlData() {
			return gridXmlData;
		}

		public void setGridXmlData(String gridXmlData) {
			this.gridXmlData = gridXmlData;
		}

        public String getgridXmlData() {
        return gridXmlData;
        }

        public void setgridXmlData(String gridXmlData) {
        this.gridXmlData = gridXmlData;
       }

		
		public String getStrEmailMsg() {
			return strEmailMsg;
		}

		public void setStrEmailMsg(String strEmailMsg) {
	        this.strEmailMsg = strEmailMsg;
		}
        
}