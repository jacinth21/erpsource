package com.globus.custservice.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

/**
 * @author tramasamy
 *
 */
public class GmPartNumPriceUpdateForm extends GmCommonForm {

	private String hMode = "";
	private String Cbo_ProjId = "";
	private String hProjectId = "";
	private String hInputString = "";
	private String Txt_Batch = "";
	private String hAction = "";
	private String hCol = "";
	private String hLoad = "";
	private String strGridData = "";
	private String CursReport = "";
	private String Report = "";
	private String hColumn = "";
	private String strJSONData = "";

	ArrayList alResult = new ArrayList();
	HashMap hmReturn = new HashMap();
	HashMap hmResult = new HashMap();
	ArrayList alPartList = new ArrayList();

	/**
	 * @return the hMode
	 */
	public String gethMode() {
		return hMode;
	}

	/**
	 * @param hMode
	 *            the hMode to set
	 */
	public void sethMode(String hMode) {
		this.hMode = hMode;
	}

	/**
	 * @return the cbo_ProjId
	 */
	public String getCbo_ProjId() {
		return Cbo_ProjId;
	}

	/**
	 * @param cbo_ProjId
	 *            the cbo_ProjId to set
	 */
	public void setCbo_ProjId(String cbo_ProjId) {
		Cbo_ProjId = cbo_ProjId;
	}

	/**
	 * @return the hProjectId
	 */
	public String gethProjectId() {
		return hProjectId;
	}

	/**
	 * @param hProjectId
	 *            the hProjectId to set
	 */
	public void sethProjectId(String hProjectId) {
		this.hProjectId = hProjectId;
	}

	/**
	 * @return the hInputString
	 */
	public String gethInputString() {
		return hInputString;
	}

	/**
	 * @param hInputString
	 *            the hInputString to set
	 */
	public void sethInputString(String hInputString) {
		this.hInputString = hInputString;
	}

	/**
	 * @return the txt_Batch
	 */
	public String getTxt_Batch() {
		return Txt_Batch;
	}

	/**
	 * @param txt_Batch
	 *            the txt_Batch to set
	 */
	public void setTxt_Batch(String txt_Batch) {
		Txt_Batch = txt_Batch;
	}

	/**
	 * @return the hAction
	 */
	public String gethAction() {
		return hAction;
	}

	/**
	 * @param hAction
	 *            the hAction to set
	 */
	public void sethAction(String hAction) {
		this.hAction = hAction;
	}

	/**
	 * @return the hCol
	 */
	public String gethCol() {
		return hCol;
	}

	/**
	 * @param hCol
	 *            the hCol to set
	 */
	public void sethCol(String hCol) {
		this.hCol = hCol;
	}

	/**
	 * @return the hLoad
	 */
	public String gethLoad() {
		return hLoad;
	}

	/**
	 * @param hLoad
	 *            the hLoad to set
	 */
	public void sethLoad(String hLoad) {
		this.hLoad = hLoad;
	}

	/**
	 * @return the strGridData
	 */
	public String getStrGridData() {
		return strGridData;
	}

	/**
	 * @param strGridData
	 *            the strGridData to set
	 */
	public void setStrGridData(String strGridData) {
		this.strGridData = strGridData;
	}

	/**
	 * @return the cursReport
	 */
	public String getCursReport() {
		return CursReport;
	}

	/**
	 * @param cursReport
	 *            the cursReport to set
	 */
	public void setCursReport(String cursReport) {
		CursReport = cursReport;
	}

	/**
	 * @return the report
	 */
	public String getReport() {
		return Report;
	}

	/**
	 * @param report
	 *            the report to set
	 */
	public void setReport(String report) {
		Report = report;
	}

	/**
	 * @return the hColumn
	 */
	public String gethColumn() {
		return hColumn;
	}

	/**
	 * @param hColumn
	 *            the hColumn to set
	 */
	public void sethColumn(String hColumn) {
		this.hColumn = hColumn;
	}

	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}

	/**
	 * @param alResult
	 *            the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}

	/**
	 * @return the hmReturn
	 */
	public HashMap getHmReturn() {
		return hmReturn;
	}

	/**
	 * @param hmReturn
	 *            the hmReturn to set
	 */
	public void setHmReturn(HashMap hmReturn) {
		this.hmReturn = hmReturn;
	}

	/**
	 * @return the hmResult
	 */
	public HashMap getHmResult() {
		return hmResult;
	}

	/**
	 * @param hmResult
	 *            the hmResult to set
	 */
	public void setHmResult(HashMap hmResult) {
		this.hmResult = hmResult;
	}

	/**
	 * @return the alPartList
	 */
	public ArrayList getAlPartList() {
		return alPartList;
	}

	/**
	 * @param alPartList the alPartList to set
	 */
	public void setAlPartList(ArrayList alPartList) {
		this.alPartList = alPartList;
	}

	/**
	 * @return the strJSONData
	 */
	public String getStrJSONData() {
		return strJSONData;
	}

	/**
	 * @param strJSONData the strJSONData to set
	 */
	public void setStrJSONData(String strJSONData) {
		this.strJSONData = strJSONData;
	}
	

}