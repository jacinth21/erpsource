package com.globus.custservice.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmOrderDetailsForm extends GmLogForm{
	
	private String repID="";
	private Date surgeryDt=null;
	private String surgeryNo="79";
	private String orderID="";
	private String caseID="";
	private String accountID="";
	private String accountNm="";
	private String usage="2560";
	private String partNum="";
	private String partDesc="";
	private String orderTypeNm="Draft";
	private String caseInfoID = "";
	private String inputStr ="";
	private String total ="";
	private String parentOrderID ="";
	private String custPO ="";
	private String ordComments="";
	private String tagId ="";
	private String setType="50300";
	private String npiMandatoryFl ="";
	private String orderMode = "";
	private String cbo_repName ="";
	private String cboegpsusage = "";
	
	private HashMap hmOrderDetails =  new HashMap();
	private ArrayList alEGPSUsage = new ArrayList();
	
	
	
    
	/**
	 * @return the cboegpsusage
	 */
	public String getCboegpsusage() {
		return cboegpsusage;
	}
	/**
	 * @param cboegpsusage the cboegpsusage to set
	 */
	public void setCboegpsusage(String cboegpsusage) {
		this.cboegpsusage = cboegpsusage;
	}
	/**
	 * @return the alEGPSUsage
	 */
	public ArrayList getAlEGPSUsage() {
		return alEGPSUsage;
	}
	/**
	 * @param alEGPSUsage the alEGPSUsage to set
	 */
	public void setAlEGPSUsage(ArrayList alEGPSUsage) {
		this.alEGPSUsage = alEGPSUsage;
	}
	/**
	 * @return the hmOrderDetails
	 */
	public HashMap getHmOrderDetails() {
		return hmOrderDetails;
	}
	/**
	 * @param hmOrderDetails the hmOrderDetails to set
	 */
	public void setHmOrderDetails(HashMap hmOrderDetails) {
		this.hmOrderDetails = hmOrderDetails;
	}
	/**
	 * @return the tagId
	 */
	public String getTagId() {
		return tagId;
	}
	/**
	 * @param tagId the tagId to set
	 */
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	
	public String getOrdComments() {
		return ordComments;
	}
	public void setOrdComments(String ordComments) {
		this.ordComments = ordComments;
	}
	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}
	ArrayList alUsage= new ArrayList();
	ArrayList alSurgeryNo= new ArrayList();
	ArrayList alResult = new ArrayList();
	ArrayList alReturn = new ArrayList();
	HashMap hmResult = new HashMap();
	ArrayList alNonUsage = new ArrayList();
	ArrayList alSetType = new ArrayList();
	/**
	 * @return the alNonUsage
	 */
	public ArrayList getAlNonUsage() {
		return alNonUsage;
	}
	/**
	 * @param alNonUsage the alNonUsage to set
	 */
	public void setAlNonUsage(ArrayList alNonUsage) {
		this.alNonUsage = alNonUsage;
	}
	/**
	 * @return the hmResult
	 */
	public HashMap getHmResult() {
		return hmResult;
	}
	/**
	 * @param hmResult the hmResult to set
	 */
	public void setHmResult(HashMap hmResult) {
		this.hmResult = hmResult;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the partDesc
	 */
	public String getPartDesc() {
		return partDesc;
	}
	/**
	 * @param partDesc the partDesc to set
	 */
	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}
	
	/**
	 * @return the repID
	 */
	public String getRepID() {
		return repID;
	}
	/**
	 * @param repID the repID to set
	 */
	public void setRepID(String repID) {
		this.repID = repID;
	}

	/**
	 * @return the surgeryNo
	 */
	public String getSurgeryNo() {
		return surgeryNo;
	}
	/**
	 * @param surgeryNo the surgeryNo to set
	 */
	public void setSurgeryNo(String surgeryNo) {
		this.surgeryNo = surgeryNo;
	}
	/**
	 * @return the orderID
	 */
	public String getOrderID() {
		return orderID;
	}
	/**
	 * @param orderID the orderID to set
	 */
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	/**
	 * @return the caseID
	 */
	public String getCaseID() {
		return caseID;
	}
	/**
	 * @param caseID the caseID to set
	 */
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}
	/**
	 * @return the accountID
	 */
	public String getAccountID() {
		return accountID;
	}
	/**
	 * @param accountID the accountID to set
	 */
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	/**
	 * @return the accountNm
	 */
	public String getAccountNm() {
		return accountNm;
	}
	/**
	 * @param accountNm the accountNm to set
	 */
	public void setAccountNm(String accountNm) {
		this.accountNm = accountNm;
	}
	/**
	 * @return the surgeryDt
	 */
	public Date getSurgeryDt() {
		return surgeryDt;
	}
	/**
	 * @param surgeryDt the surgeryDt to set
	 */
	public void setSurgeryDt(Date surgeryDt) {
		this.surgeryDt = surgeryDt;
	}
	/**
	 * @return the usage
	 */
	public String getUsage() {
		return usage;
	}
	/**
	 * @param usage the usage to set
	 */
	public void setUsage(String usage) {
		this.usage = usage;
	}
	/**
	 * @return the alUsage
	 */
	public ArrayList getAlUsage() {
		return alUsage;
	}
	/**
	 * @param alUsage the alUsage to set
	 */
	public void setAlUsage(ArrayList alUsage) {
		this.alUsage = alUsage;
	}
	/**
	 * @return the orderTypeNm
	 */
	public String getOrderTypeNm() {
		return orderTypeNm;
	}
	/**
	 * @param orderTypeNm the orderTypeNm to set
	 */
	public void setOrderTypeNm(String orderTypeNm) {
		this.orderTypeNm = orderTypeNm;
	}
	/**
	 * @return the alSurgeryNo
	 */
	public ArrayList getAlSurgeryNo() {
		return alSurgeryNo;
	}
	/**
	 * @param alSurgeryNo the alSurgeryNo to set
	 */
	public void setAlSurgeryNo(ArrayList alSurgeryNo) {
		this.alSurgeryNo = alSurgeryNo;
	}
	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}
	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}
	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	public String getParentOrderID() {
		return parentOrderID;
	}
	public void setParentOrderID(String parentOrderID) {
		this.parentOrderID = parentOrderID;
	}
	public String getCustPO() {
		return custPO;
	}
	public void setCustPO(String custPO) {
		this.custPO = custPO;
	}
	/**
	 * @return the alSetType
	 */
	public ArrayList getAlSetType() {
		return alSetType;
	}
	/**
	 * @param alSetType the alSetType to set
	 */
	public void setAlSetType(ArrayList alSetType) {
		this.alSetType = alSetType;
	}
	/**
	 * @return the setType
	 */
	public String getSetType() {
		return setType;
	}
	/**
	 * @param setType the setType to set
	 */
	public void setSetType(String setType) {
		this.setType = setType;
	}
	
	public String getNpiMandatoryFl() {
		return npiMandatoryFl;
	}
	public void setNpiMandatoryFl(String npiMandatoryFl) {
		this.npiMandatoryFl = npiMandatoryFl;
	}
	/**
	 * @return the orderMode
	 */
	public String getOrderMode() {
		return orderMode;
	}
	/**
	 * @param orderMode the orderMode to set
	 */
	public void setOrderMode(String orderMode) {
		this.orderMode = orderMode;
	}
	/**
	 * @return the cbo_repName
	 */
	public String getCbo_repName() {
		return cbo_repName;
	}
	/**
	 * @param cbo_repName the cbo_repName to set
	 */
	public void setCbo_repName(String cbo_repName) {
		this.cbo_repName = cbo_repName;
	}
	
	

}
