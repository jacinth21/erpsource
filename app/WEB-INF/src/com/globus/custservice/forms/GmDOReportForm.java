package com.globus.custservice.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmDOReportForm extends GmCommonForm{
	
	 private String orderID ="";
	 private String to = "";
	 private String message = "";
	 private String hOrderID = "";
	 private String cc="";
	 private String subject="";
	 private String addMoreMessage="";
	 private String caseID="";
	 private String accountID="";
	 private String accountNm="";
	 private String ordStatus="";
	 
	 public String getOrdStatus() {
		return ordStatus;
	}

	public void setOrdStatus(String ordStatus) {
		this.ordStatus = ordStatus;
	}

	private ArrayList alResult = new ArrayList();

	 
	/**
	 * @return the hOrderID
	 */
	public String gethOrderID() {
		return hOrderID;
	}

	/**
	 * @param hOrderID the hOrderID to set
	 */
	public void sethOrderID(String hOrderID) {
		this.hOrderID = hOrderID;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the orderID
	 */
	public String getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID the orderID to set
	 */
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	/**
	 * @return the cc
	 */
	public String getCc() {
		return cc;
	}

	/**
	 * @param cc the cc to set
	 */
	public void setCc(String cc) {
		this.cc = cc;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the addMoreMessage
	 */
	public String getAddMoreMessage() {
		return addMoreMessage;
	}

	/**
	 * @param addMoreMessage the addMoreMessage to set
	 */
	public void setAddMoreMessage(String addMoreMessage) {
		this.addMoreMessage = addMoreMessage;
	}

	/**
	 * @return the caseID
	 */
	public String getCaseID() {
		return caseID;
	}

	/**
	 * @param caseID the caseID to set
	 */
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}

	/**
	 * @return the accountID
	 */
	public String getAccountID() {
		return accountID;
	}

	/**
	 * @param accountID the accountID to set
	 */
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	/**
	 * @return the accountNm
	 */
	public String getAccountNm() {
		return accountNm;
	}

	/**
	 * @param accountNm the accountNm to set
	 */
	public void setAccountNm(String accountNm) {
		this.accountNm = accountNm;
	}

	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}

	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}

}
