package com.globus.custservice.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmLotErrorReportForm  extends GmLogForm{
	
	private static final long serialVersionUID = 1L;
	private String partNumber = "";
	private String lotNumber = "";
	private String transactionType = "";
	private String transactionId = "";
	private String transFromDate = "";
	private String transToDate = "";
	private String fieldSales = "";
	private String searchfieldSales = "";
	private String resolutionType = "";
	private String resolutionFromDate = "";
	private String resolutionToDate = "";
	private String gridXmlData = "";
	private String status = "";
	private String strLotErrorRptJsonString ="";
	private String updLotNumber = "";
	private String comments = "";
	private String strErrorInfoID = "";
	private String reason = "";
	private String errtype = "";
	private String unResolvedStatus = "";
	private String transactionDate = "";
	private String resolutionDate = "";
	private String strFlag="";
	
	public String getStrFlag() {
		return strFlag;
	}
	public void setStrFlag(String strFlag) {
		this.strFlag = strFlag;
	}
	public String getResolutionDate() {
		return resolutionDate;
	}
	public void setResolutionDate(String resolutionDate) {
		this.resolutionDate = resolutionDate;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getErrtype() {
		return errtype;
	}
	public void setErrtype(String errtype) {
		this.errtype = errtype;
	}
	private ArrayList alTransType =new ArrayList();
	private ArrayList alResolutionType =new ArrayList();
	public String getStrLotErrorRptJsonString() {
		return strLotErrorRptJsonString;
	}
	public void setStrLotErrorRptJsonString(String strLotErrorRptJsonString) {
		this.strLotErrorRptJsonString = strLotErrorRptJsonString;
	}
	public String getUpdLotNumber() {
		return updLotNumber;
	}
	public void setUpdLotNumber(String updLotNumber) {
		this.updLotNumber = updLotNumber;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStrErrorInfoID() {
		return strErrorInfoID;
	}
	public void setStrErrorInfoID(String strErrorInfoID) {
		this.strErrorInfoID = strErrorInfoID;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public ArrayList getAlList() {
		return alList;
	}
	public void setAlList(ArrayList alList) {
		this.alList = alList;
	}
	public ArrayList getAlResType() {
		return alResType;
	}
	public void setAlResType(ArrayList alResType) {
		this.alResType = alResType;
	}
	private ArrayList alStatus =new ArrayList();
	
	private ArrayList alList = new ArrayList();
	private ArrayList alResType = new ArrayList();
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getTransFromDate() {
		return transFromDate;
	}
	public void setTransFromDate(String transFromDate) {
		this.transFromDate = transFromDate;
	}
	public String getTransToDate() {
		return transToDate;
	}
	public void setTransToDate(String transToDate) {
		this.transToDate = transToDate;
	}
	public String getGridXmlData() {
		return gridXmlData;
	}
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getFieldSales() {
		return fieldSales;
	}
	public void setFieldSales(String fieldSales) {
		this.fieldSales = fieldSales;
	}
	public String getSearchfieldSales() {
		return searchfieldSales;
	}
	public void setSearchfieldSales(String searchfieldSales) {
		this.searchfieldSales = searchfieldSales;
	}
	public String getResolutionType() {
		return resolutionType;
	}
	public void setResolutionType(String resolutionType) {
		this.resolutionType = resolutionType;
	}
	
	public String getResolutionFromDate() {
		return resolutionFromDate;
	}
	public void setResolutionFromDate(String resolutionFromDate) {
		this.resolutionFromDate = resolutionFromDate;
	}
	public String getResolutionToDate() {
		return resolutionToDate;
	}
	public void setResolutionToDate(String resolutionToDate) {
		this.resolutionToDate = resolutionToDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList getAlTransType() {
		return alTransType;
	}
	public void setAlTransType(ArrayList alTransType) {
		this.alTransType = alTransType;
	}
	public ArrayList getAlResolutionType() {
		return alResolutionType;
	}
	public void setAlResolutionType(ArrayList alResolutionType) {
		this.alResolutionType = alResolutionType;
	}
	public ArrayList getAlStatus() {
		return alStatus;
	}
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	public String getUnResolvedStatus() {
		return unResolvedStatus;
	}
	public void setUnResolvedStatus(String unResolvedStatus) {
		this.unResolvedStatus = unResolvedStatus;
	}
}

