
package com.globus.custservice.forms;



import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCancelForm;
import com.globus.common.forms.GmCommonForm;
 
public class GmPartNumOrderDetailForm extends GmCommonForm {
	
	
	List alDistList = new ArrayList();
	List alRepList = new ArrayList();
	List alOrdTypeList = new ArrayList();
	String dist = "";
	String rep = "";
	String ordType ="";
	String partNum="";
	String fromDate = "";
	String toDate = "";
	
	RowSetDynaClass rdReport= null;
	
	/**
	 * @return the rdReport
	 */
	public RowSetDynaClass getRdReport() {
		return rdReport;
	}

	/**
	 * @param rdReport the rdReport to set
	 */
	public void setRdReport(RowSetDynaClass rdReport) {
		this.rdReport = rdReport;
	}

	
	/**
	 * @return the alDistList
	 */
	public List getAlDistList() {
		return alDistList;
	}

	/**
	 * @param alDistList the alDistList to set
	 */
	public void setAlDistList(List alDistList) {
		this.alDistList = alDistList;
	}

	/**
	 * @return the alRepList
	 */
	public List getAlRepList() {
		return alRepList;
	}

	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(List alRepList) {
		this.alRepList = alRepList;
	}

	/**
	 * @return the dist
	 */
	public String getDist() {
		return dist;
	}

	/**
	 * @param dist the dist to set
	 */
	public void setDist(String dist) {
		this.dist = dist;
	}

	/**
	 * @return the rep
	 */
	public String getRep() {
		return rep;
	}

	/**
	 * @param rep the rep to set
	 */
	public void setRep(String rep) {
		this.rep = rep;
	}

	/**
	 * @return the alOrdTypeList
	 */
	public List getAlOrdTypeList() {
		return alOrdTypeList;
	}

	/**
	 * @param alOrdTypeList the alOrdTypeList to set
	 */
	public void setAlOrdTypeList(List alOrdTypeList) {
		this.alOrdTypeList = alOrdTypeList;
	}

	/**
	 * @return the ordType
	 */
	public String getOrdType() {
		return ordType;
	}

	/**
	 * @param ordType the ordType to set
	 */
	public void setOrdType(String ordType) {
		this.ordType = ordType;
	}

	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}

	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	
	
}
