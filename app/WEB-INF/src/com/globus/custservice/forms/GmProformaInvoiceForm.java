package com.globus.custservice.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmProformaInvoiceForm extends GmCommonForm {

 
	private String orderid = "";
 
	private ArrayList alReturn = new ArrayList();

	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}

	/**
	 * @param alReturn
	 *            the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}

	private HashMap hmJasperDetails = new HashMap();

	/**
	 * @return the hmJasperDetails
	 */
	public HashMap getHmJasperDetails() {
		return hmJasperDetails;
	}

	/**
	 * @param hmJasperDetails
	 *            the hmJasperDetails to set
	 */
	public void setHmJasperDetails(HashMap hmJasperDetails) {
		this.hmJasperDetails = hmJasperDetails;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	/**
	 * @param fromDt
	 *            the fromDt to set
	 */
	  

}
