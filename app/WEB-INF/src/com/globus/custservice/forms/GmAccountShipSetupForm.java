package com.globus.custservice.forms;


import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmAccountShipSetupForm extends GmCommonForm{
	
	private String accID = "";
	private String shipAttnTo = "";
	private String shipName = "";
	private String shipAdd1 = "";
	private String shipAdd2 = "";
	private String shipCity = "";
	private String shipState = "";
	private String shipCountry = "";
	private String shipZip = "";
	private String carrier = "";
	
	private ArrayList alState = new ArrayList();
	private ArrayList alCountry = new ArrayList();
	private ArrayList alCarrier = new ArrayList();

	public String getAccID() {
		return accID;
	}

	public void setAccID(String accID) {
		this.accID = accID;
	}

	public String getShipAttnTo() {
		return shipAttnTo;
	}

	public void setShipAttnTo(String shipAttnTo) {
		this.shipAttnTo = shipAttnTo;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getShipAdd1() {
		return shipAdd1;
	}

	public void setShipAdd1(String shipAdd1) {
		this.shipAdd1 = shipAdd1;
	}

	public String getShipAdd2() {
		return shipAdd2;
	}

	public void setShipAdd2(String shipAdd2) {
		this.shipAdd2 = shipAdd2;
	}

	public String getShipCity() {
		return shipCity;
	}

	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}

	public String getShipState() {
		return shipState;
	}

	public void setShipState(String shipState) {
		this.shipState = shipState;
	}

	public String getShipCountry() {
		return shipCountry;
	}

	public void setShipCountry(String shipCountry) {
		this.shipCountry = shipCountry;
	}

	public String getShipZip() {
		return shipZip;
	}

	public void setShipZip(String shipZip) {
		this.shipZip = shipZip;
	}

	public ArrayList getAlState() {
		return alState;
	}

	public void setAlState(ArrayList alState) {
		this.alState = alState;
	}

	public ArrayList getAlCountry() {
		return alCountry;
	}

	public void setAlCountry(ArrayList alCountry) {
		this.alCountry = alCountry;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	/**
	 * @return the alCarrier
	 */
	public ArrayList getAlCarrier() {
		return alCarrier;
	}

	/**
	 * @param alCarrier the alCarrier to set
	 */
	public void setAlCarrier(ArrayList alCarrier) {
		this.alCarrier = alCarrier;
	}
	
}