package com.globus.custservice.forms;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

public class GmOrderDDTForm extends GmLogForm{
	
private String orderId="";
private String inputStr="";
private String accessFL="";
private String partNum="";
private String controlNum="";


public String getAccessFL() {
	return accessFL;
}

public void setAccessFL(String accessFL) {
	this.accessFL = accessFL;
}

public String getInputStr() {
	return inputStr;
}

public void setInputStr(String inputStr) {
	this.inputStr = inputStr;
}

HashMap hmResult = new HashMap();



public HashMap getHmResult() {
	return hmResult;
}

public void setHmResult(HashMap hmResult) {
	this.hmResult = hmResult;
}

public String getOrderId() {
	return orderId;
}

public void setOrderId(String orderId) {
	this.orderId = orderId;
}

/**
 * @return the partNum
 */
public String getPartNum() {
	return partNum;
}

/**
 * @param partNum the partNum to set
 */
public void setPartNum(String partNum) {
	this.partNum = partNum;
}

/**
 * @return the controlNum
 */
public String getControlNum() {
	return controlNum;
}

/**
 * @param controlNum the controlNum to set
 */
public void setControlNum(String controlNum) {
	this.controlNum = controlNum;
}
	
}