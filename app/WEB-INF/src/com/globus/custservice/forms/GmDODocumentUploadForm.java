package com.globus.custservice.forms;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.struts.upload.FormFile;

import com.globus.common.forms.GmCancelForm;

public class GmDODocumentUploadForm extends GmCancelForm {

  private String strOrderId = "";
  private String strRevenueType = "";
  private String strBtnAccessFl = "";
  private String strComments = "";

  /**
   * @return the strOrderId
   */
  public String getStrOrderId() {
    return strOrderId;
  }

  /**
   * @param strOrderId the strOrderId to set
   */
  public void setStrOrderId(String strOrderId) {
    this.strOrderId = strOrderId;
  }

  /**
   * @return the strRevenueType
   */
  public String getStrRevenueType() {
    return strRevenueType;
  }

  /**
   * @param strRevenueType the strRevenueType to set
   */
  public void setStrRevenueType(String strRevenueType) {
    this.strRevenueType = strRevenueType;
  }

  /**
   * @return the strBtnAccessFl
   */
  public String getStrBtnAccessFl() {
    return strBtnAccessFl;
  }

  /**
   * @param strBtnAccessFl the strBtnAccessFl to set
   */
  public void setStrBtnAccessFl(String strBtnAccessFl) {
    this.strBtnAccessFl = strBtnAccessFl;
  }

  /**
   * @return the strComments
   */
  public String getStrComments() {
    return strComments;
  }

  /**
   * @param strComments the strComments to set
   */
  public void setStrComments(String strComments) {
    this.strComments = strComments;
  }



  private String fileUploadSubmitAcc = "";
  private String forwardAction = "";
  private String gridXmlData = "";
  private int intResultSize;
  private String uploadTypeId = "";
  private String typeListId = "";
  private String partListId = "";
  private String partNumId = "";
  private String partNumDesc = "";
  private String setListId = "";
  private String setId = "";
  private String setDesc = "";
  private String prjListId = "";
  private String sysListId = "";
  private String grpListId = "";
  private String refId = "";
  private String voidInputStr = "";
  private String fileInputStr = "";
  private String accessDcoed = "";
  private String accessNonDcoed = "";
  private String accessFileVoid = "";


  private ArrayList alUploadTypes = new ArrayList();
  private ArrayList alPrjTypeList = new ArrayList();
  private ArrayList alSysTypeList = new ArrayList();
  private ArrayList alGrpTypeList = new ArrayList();
  private ArrayList alTypeList = new ArrayList();
  private ArrayList alSubType = new ArrayList();
  private HashMap hmFileFmt = new HashMap();



  public String getAccessFileVoid() {
    return accessFileVoid;
  }

  public void setAccessFileVoid(String accessFileVoid) {
    this.accessFileVoid = accessFileVoid;
  }

  public String getAccessDcoed() {
    return accessDcoed;
  }

  public void setAccessDcoed(String accessDcoed) {
    this.accessDcoed = accessDcoed;
  }

  public String getAccessNonDcoed() {
    return accessNonDcoed;
  }

  public void setAccessNonDcoed(String accessNonDcoed) {
    this.accessNonDcoed = accessNonDcoed;
  }

  public HashMap getHmFileFmt() {
    return hmFileFmt;
  }

  public void setHmFileFmt(HashMap hmFileFmt) {
    this.hmFileFmt = hmFileFmt;
  }

  public ArrayList getAlSubType() {
    return alSubType;
  }

  public void setAlSubType(ArrayList alSubType) {
    this.alSubType = alSubType;
  }

  public String getFileUploadSubmitAcc() {
    return fileUploadSubmitAcc;
  }

  public void setFileUploadSubmitAcc(String fileUploadSubmitAcc) {
    this.fileUploadSubmitAcc = fileUploadSubmitAcc;
  }

  public String getForwardAction() {
    return forwardAction;
  }

  public void setForwardAction(String forwardAction) {
    this.forwardAction = forwardAction;
  }

  public String getGridXmlData() {
    return gridXmlData;
  }

  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  public int getIntResultSize() {
    return intResultSize;
  }

  public void setIntResultSize(int intResultSize) {
    this.intResultSize = intResultSize;
  }

  public String getUploadTypeId() {
    return uploadTypeId;
  }

  public void setUploadTypeId(String uploadTypeId) {
    this.uploadTypeId = uploadTypeId;
  }

  public String getTypeListId() {
    return typeListId;
  }

  public void setTypeListId(String typeListId) {
    this.typeListId = typeListId;
  }

  public String getPartListId() {
    return partListId;
  }

  public void setPartListId(String partListId) {
    this.partListId = partListId;
  }

  public String getPartNumId() {
    return partNumId;
  }

  public void setPartNumId(String partNumId) {
    this.partNumId = partNumId;
  }

  public String getPartNumDesc() {
    return partNumDesc;
  }

  public void setPartNumDesc(String partNumDesc) {
    this.partNumDesc = partNumDesc;
  }

  public String getSetId() {
    return setId;
  }

  public void setSetId(String setId) {
    this.setId = setId;
  }

  public String getSetDesc() {
    return setDesc;
  }

  public void setSetDesc(String setDesc) {
    this.setDesc = setDesc;
  }

  public String getSetListId() {
    return setListId;
  }

  public void setSetListId(String setListId) {
    this.setListId = setListId;
  }

  public String getPrjListId() {
    return prjListId;
  }

  public void setPrjListId(String prjListId) {
    this.prjListId = prjListId;
  }

  public String getSysListId() {
    return sysListId;
  }

  public void setSysListId(String sysListId) {
    this.sysListId = sysListId;
  }

  public String getGrpListId() {
    return grpListId;
  }

  public void setGrpListId(String grpListId) {
    this.grpListId = grpListId;
  }

  public ArrayList getAlUploadTypes() {
    return alUploadTypes;
  }

  public void setAlUploadTypes(ArrayList alUploadTypes) {
    this.alUploadTypes = alUploadTypes;
  }

  public ArrayList getAlPrjTypeList() {
    return alPrjTypeList;
  }

  public String getRefId() {
    return refId;
  }

  public void setRefId(String refId) {
    this.refId = refId;
  }

  public String getVoidInputStr() {
    return voidInputStr;
  }

  public void setVoidInputStr(String voidInputStr) {
    this.voidInputStr = voidInputStr;
  }

  public String getFileInputStr() {
    return fileInputStr;
  }

  public void setFileInputStr(String fileInputStr) {
    this.fileInputStr = fileInputStr;
  }

  public void setAlPrjTypeList(ArrayList alPrjTypeList) {
    this.alPrjTypeList = alPrjTypeList;
  }

  public ArrayList getAlSysTypeList() {
    return alSysTypeList;
  }

  public void setAlSysTypeList(ArrayList alSysTypeList) {
    this.alSysTypeList = alSysTypeList;
  }

  public ArrayList getAlGrpTypeList() {
    return alGrpTypeList;
  }

  public void setAlGrpTypeList(ArrayList alGrpTypeList) {
    this.alGrpTypeList = alGrpTypeList;
  }

  public ArrayList getAlTypeList() {
    return alTypeList;
  }

  public void setAlTypeList(ArrayList alTypeList) {
    this.alTypeList = alTypeList;
  }



  // private String refID = "";
  private String refType = "";
  // private String uploadTypeId = "";

  private String message = "";

  private String fileTypeId = "";
  private String fileTypeList = "";

  public String getFileTypeList() {
    return fileTypeList;
  }

  public void setFileTypeList(String fileTypeList) {
    this.fileTypeList = fileTypeList;
  }

  private String fileTitle = "";
  private String fileTitleList = "";
  // private String fileInputstr = "";
  /*
   * public String getFileInputstr() { return fileInputstr; }
   * 
   * public void setFileInputstr(String fileInputstr) { this.fileInputstr = fileInputstr; }
   */
  private FormFile file;
  private String fileName = "";

  private String logType = "";
  private int intRecSize = 0;

  private FormFile theFile;
  private final ArrayList listFile = new ArrayList();
  private int index = 0;

  private ArrayList alFileType = new ArrayList();


  /*
   * public String getRefID() { return refID; }
   * 
   * public void setRefID(String refID) { this.refID = refID; }
   */
  public String getRefType() {
    return refType;
  }

  public void setRefType(String refType) {
    this.refType = refType;
  }

  /*
   * public String getUploadTypeId() { return uploadTypeId; }
   * 
   * public void setUploadTypeId(String uploadTypeId) { this.uploadTypeId = uploadTypeId; }
   */
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getFileTypeId() {
    return fileTypeId;
  }

  public void setFileTypeId(String fileTypeId) {
    this.fileTypeId = fileTypeId;
  }

  public String getFileTitle() {
    return fileTitle;
  }

  public void setFileTitle(String fileTitle) {
    this.fileTitle = fileTitle;
  }

  public ArrayList getAlFileType() {
    return alFileType;
  }

  public void setAlFileType(ArrayList alFileType) {
    this.alFileType = alFileType;
  }

  public int getIntRecSize() {
    return intRecSize;
  }

  public String getLogType() {
    return logType;
  }

  public void setLogType(String logType) {
    this.logType = logType;
  }

  public void setIntRecSize(int intRecSize) {
    this.intRecSize = intRecSize;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public FormFile getFile() {
    return file;
  }

  public void setFile(FormFile file) {
    this.file = file;
  }

  public FormFile getTheFile(int index) {
    return this.theFile;
  }

  public void setTheFile(int index, FormFile theFile) {
    this.theFile = theFile;
    setListFile(theFile);
    this.index++;
  }

  public ArrayList getListFile() {
    return this.listFile;
  }

  public void setListFile(FormFile theFile) {
    this.listFile.add(index, theFile);
  }

  public String getFileTitleList() {
    return fileTitleList;
  }

  public void setFileTitleList(String fileTitleList) {
    this.fileTitleList = fileTitleList;
  }

}
