package com.globus.custservice.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

public class GmStockTransferForm extends GmCommonForm {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  private String trnasctionId = "";
  private Date createdDate;
  private Date requiredDat;
  private String requestedBy = "";
  private String fulfillCompany = "";
  private String status = "";
  private String masterRequestId = "";
  private String fulfillTrnasctionId = "";
  private String invoiceId = "";
  private String xmlString = "";
  private String gridXmlData = "";
  private String strXMLGrid = "";
  private String reqCompID = "";
  private String dateType = "";
  private String fromDate = "";
  private String toDate = "";
  private String fullTransId = "";
  private String transId = "";
  private String requestfor = "";
  private String rqcompnm = "";
  private String rqinitby = "";
  private String requireddate = "";
  private String fgInputStr = "";
  private String blInputStr = "";
  private String requeststatusflag = "";
  private String partNum = "";
  private String partDesc = "";
  private String reqQty = "";
  private String fulfilledQty = "";
  private String varianceQty = "";
  private String variance = "";
  private String invPrintLbl = "";

  private ArrayList alDateType = new ArrayList();
  private ArrayList alReqCompList = new ArrayList();
  private ArrayList alResult = new ArrayList();
  private ArrayList alStatus = new ArrayList();
  private ArrayList alQtyVariance = new ArrayList();
  private ArrayList alrqitemdetails = new ArrayList();
  private ArrayList alReleaseFrm = new ArrayList();
  private ArrayList alreleasetransdtls = new ArrayList();


  /**
   * @return the alQtyVariance
   */
  public ArrayList getAlQtyVariance() {
    return alQtyVariance;
  }

  /**
   * @param alQtyVariance the alQtyVariance to set
   */
  public void setAlQtyVariance(ArrayList alQtyVariance) {
    this.alQtyVariance = alQtyVariance;
  }

  /**
   * @return the variance
   */
  public String getVariance() {
    return variance;
  }

  /**
   * @param variance the variance to set
   */
  public void setVariance(String variance) {
    this.variance = variance;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the partDesc
   */
  public String getPartDesc() {
    return partDesc;
  }

  /**
   * @param partDesc the partDesc to set
   */
  public void setPartDesc(String partDesc) {
    this.partDesc = partDesc;
  }

  /**
   * @return the reqQty
   */
  public String getReqQty() {
    return reqQty;
  }

  /**
   * @param reqQty the reqQty to set
   */
  public void setReqQty(String reqQty) {
    this.reqQty = reqQty;
  }

  /**
   * @return the fulfilledQty
   */
  public String getFulfilledQty() {
    return fulfilledQty;
  }

  /**
   * @param fulfilledQty the fulfilledQty to set
   */
  public void setFulfilledQty(String fulfilledQty) {
    this.fulfilledQty = fulfilledQty;
  }

  /**
   * @return the varianceQty
   */
  public String getVarianceQty() {
    return varianceQty;
  }

  /**
   * @param varianceQty the varianceQty to set
   */
  public void setVarianceQty(String varianceQty) {
    this.varianceQty = varianceQty;
  }

  /**
   * @return the requeststatusflag
   */
  public String getRequeststatusflag() {
    return requeststatusflag;
  }

  /**
   * @param requeststatusflag the requeststatusflag to set
   */
  public void setRequeststatusflag(String requeststatusflag) {
    this.requeststatusflag = requeststatusflag;
  }

  /**
   * @return the alreleasetransdtls
   */
  public ArrayList getAlreleasetransdtls() {
    return alreleasetransdtls;
  }

  /**
   * @param alreleasetransdtls the alreleasetransdtls to set
   */
  public void setAlreleasetransdtls(ArrayList alreleasetransdtls) {
    this.alreleasetransdtls = alreleasetransdtls;
  }

  /**
   * @return the fgInputStr
   */
  public String getFgInputStr() {
    return fgInputStr;
  }

  /**
   * @param fgInputStr the fgInputStr to set
   */
  public void setFgInputStr(String fgInputStr) {
    this.fgInputStr = fgInputStr;
  }

  /**
   * @return the blInputStr
   */
  public String getBlInputStr() {
    return blInputStr;
  }

  /**
   * @param blInputStr the blInputStr to set
   */
  public void setBlInputStr(String blInputStr) {
    this.blInputStr = blInputStr;
  }

  /**
   * @return the alReleaseFrm
   */
  public ArrayList getAlReleaseFrm() {
    return alReleaseFrm;
  }

  /**
   * @param alReleaseFrm the alReleaseFrm to set
   */
  public void setAlReleaseFrm(ArrayList alReleaseFrm) {
    this.alReleaseFrm = alReleaseFrm;
  }

  /**
   * @return the alrqitemdetails
   */
  public ArrayList getAlrqitemdetails() {
    return alrqitemdetails;
  }

  /**
   * @param alrqitemdetails the alrqitemdetails to set
   */
  public void setAlrqitemdetails(ArrayList alrqitemdetails) {
    this.alrqitemdetails = alrqitemdetails;
  }

  /**
   * @return the requestfor
   */
  public String getRequestfor() {
    return requestfor;
  }

  /**
   * @param requestfor the requestfor to set
   */
  public void setRequestfor(String requestfor) {
    this.requestfor = requestfor;
  }

  /**
   * @return the rqcompnm
   */
  public String getRqcompnm() {
    return rqcompnm;
  }

  /**
   * @param rqcompnm the rqcompnm to set
   */
  public void setRqcompnm(String rqcompnm) {
    this.rqcompnm = rqcompnm;
  }

  /**
   * @return the rqinitby
   */
  public String getRqinitby() {
    return rqinitby;
  }

  /**
   * @param rqinitby the rqinitby to set
   */
  public void setRqinitby(String rqinitby) {
    this.rqinitby = rqinitby;
  }

  /**
   * @return the requireddate
   */
  public String getRequireddate() {
    return requireddate;
  }

  /**
   * @param requireddate the requireddate to set
   */
  public void setRequireddate(String requireddate) {
    this.requireddate = requireddate;
  }

  /**
   * @return the transId
   */
  public String getTransId() {
    return transId;
  }

  /**
   * @param transId the transId to set
   */
  public void setTransId(String transId) {
    this.transId = transId;
  }

  /**
   * @return the fullTransId
   */
  public String getFullTransId() {
    return fullTransId;
  }

  /**
   * @param fullTransId the fullTransId to set
   */
  public void setFullTransId(String fullTransId) {
    this.fullTransId = fullTransId;
  }

  /**
   * @return the fromDate
   */
  public String getFromDate() {
    return fromDate;
  }

  /**
   * @param fromDate the fromDate to set
   */
  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  /**
   * @return the toDate
   */

  public String getToDate() {
    return toDate;
  }

  /**
   * @param toDate the toDate to set
   */
  public void setToDate(String toDate) {
    this.toDate = toDate;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the dateType
   */
  public String getDateType() {
    return dateType;
  }

  /**
   * @param dateType the dateType to set
   */
  public void setDateType(String dateType) {
    this.dateType = dateType;
  }

  /**
   * @return the alDateType
   */
  public ArrayList getAlDateType() {
    return alDateType;
  }

  /**
   * @param alDateType the alDateType to set
   */
  public void setAlDateType(ArrayList alDateType) {
    this.alDateType = alDateType;
  }

  /**
   * @return the reqCompID
   */
  public String getReqCompID() {
    return reqCompID;
  }

  /**
   * @param reqCompID the reqCompID to set
   */
  public void setReqCompID(String reqCompID) {
    this.reqCompID = reqCompID;
  }

  /**
   * @return the alReqCompList
   */
  public ArrayList getAlReqCompList() {
    return alReqCompList;
  }

  /**
   * @param alReqCompList the alReqCompList to set
   */
  public void setAlReqCompList(ArrayList alReqCompList) {
    this.alReqCompList = alReqCompList;
  }

  /**
   * @return the trnasctionId
   */
  public String getTrnasctionId() {
    return trnasctionId;
  }

  /**
   * @param trnasctionId the trnasctionId to set
   */
  public void setTrnasctionId(String trnasctionId) {
    this.trnasctionId = trnasctionId;
  }

  /**
   * @return the createdDate
   */
  public Date getCreatedDate() {
    return createdDate;
  }

  /**
   * @param createdDate the createdDate to set
   */
  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  /**
   * @return the requiredDat
   */
  public Date getRequiredDat() {
    return requiredDat;
  }

  /**
   * @param requiredDat the requiredDat to set
   */
  public void setRequiredDat(Date requiredDat) {
    this.requiredDat = requiredDat;
  }

  /**
   * @return the requestedBy
   */
  public String getRequestedBy() {
    return requestedBy;
  }

  /**
   * @param requestedBy the requestedBy to set
   */
  public void setRequestedBy(String requestedBy) {
    this.requestedBy = requestedBy;
  }

  /**
   * @return the fulfillCompany
   */
  public String getFulfillCompany() {
    return fulfillCompany;
  }

  /**
   * @param fulfillCompany the fulfillCompany to set
   */
  public void setFulfillCompany(String fulfillCompany) {
    this.fulfillCompany = fulfillCompany;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the masterRequestId
   */
  public String getMasterRequestId() {
    return masterRequestId;
  }

  /**
   * @param masterRequestId the masterRequestId to set
   */
  public void setMasterRequestId(String masterRequestId) {
    this.masterRequestId = masterRequestId;
  }

  /**
   * @return the fulfillTrnasctionId
   */
  public String getFulfillTrnasctionId() {
    return fulfillTrnasctionId;
  }

  /**
   * @param fulfillTrnasctionId the fulfillTrnasctionId to set
   */
  public void setFulfillTrnasctionId(String fulfillTrnasctionId) {
    this.fulfillTrnasctionId = fulfillTrnasctionId;
  }

  /**
   * @return the invoiceId
   */
  public String getInvoiceId() {
    return invoiceId;
  }

  /**
   * @param invoiceId the invoiceId to set
   */
  public void setInvoiceId(String invoiceId) {
    this.invoiceId = invoiceId;
  }

  /**
   * @return the alResult
   */
  public ArrayList getAlResult() {
    return alResult;
  }

  /**
   * @param alResult the alResult to set
   */
  public void setAlResult(ArrayList alResult) {
    this.alResult = alResult;
  }

  /**
   * @return the xmlString
   */
  public String getXmlString() {
    return xmlString;
  }

  /**
   * @param xmlString the xmlString to set
   */
  public void setXmlString(String xmlString) {
    this.xmlString = xmlString;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the strXMLGrid
   */
  public String getStrXMLGrid() {
    return strXMLGrid;
  }

  /**
   * @param strXMLGrid the strXMLGrid to set
   */
  public void setStrXMLGrid(String strXMLGrid) {
    this.strXMLGrid = strXMLGrid;
  }

  /**
   * @return the invPrintLbl
   */
  public String getInvPrintLbl() {
    return invPrintLbl;
  }

  /**
   * @param invPrintLbl the invPrintLbl to set
   */
  public void setInvPrintLbl(String invPrintLbl) {
    this.invPrintLbl = invPrintLbl;
  }

}
