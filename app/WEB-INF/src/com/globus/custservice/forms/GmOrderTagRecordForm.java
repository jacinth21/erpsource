/**
 * FileName    : GmOrderTagRecordForm.java 
 * Description : The form is used to set / get the values for record tag screen
 * Author      : Vinoth
 * Copyright   : Globus Medical Inc
 */
package com.globus.custservice.forms;

import com.globus.common.forms.GmCommonForm;

public class GmOrderTagRecordForm extends GmCommonForm{

	
	private static final long serialVersionUID = 1L;
	
	 private String tagRefId = "";
	 private String orderTagId="";
	 private String tagId = "";
	 private String setId = "";
	 private String accountId = "";
	 private String xmlString = "";
	 private String missing = "";
	 private String tempId = "";
	/**
	 * @return the tagRefId
	 */
	public String getTagRefId() {
		return tagRefId;
	}
	/**
	 * @param tagRefId the tagRefId to set
	 */
	public void setTagRefId(String tagRefId) {
		this.tagRefId = tagRefId;
	}
	/**
	 * @return the orderTagId
	 */
	public String getOrderTagId() {
		return orderTagId;
	}
	/**
	 * @param orderTagId the orderTagId to set
	 */
	public void setOrderTagId(String orderTagId) {
		this.orderTagId = orderTagId;
	}
	/**
	 * @return the tagId
	 */
	public String getTagId() {
		return tagId;
	}
	/**
	 * @param tagId the tagId to set
	 */
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}
	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}
	/**
	 * @param xmlString the xmlString to set
	 */
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	/**
	 * @return the missing
	 */
	public String getMissing() {
		return missing;
	}
	/**
	 * @param missing the missing to set
	 */
	public void setMissing(String missing) {
		this.missing = missing;
	}
	/**
	 * @return the tempId
	 */
	public String getTempId() {
		return tempId;
	}
	/**
	 * @param tempId the tempId to set
	 */
	public void setTempId(String tempId) {
		this.tempId = tempId;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
