package com.globus.custservice.forms;

import java.util.ArrayList;
import java.util.HashMap;
import com.globus.common.forms.GmLogForm;

public class GmDOSummaryEditForm extends GmLogForm{
private String orderID ="";
private String tagID = "";
private String shipId="";
private String names="";
private String shipCarrier = "";
private String shipMode="";
private String source ="";
private String trackNo ="";
private String shipTo="";
private String shipToID="";
private String addressid="";
private String repAddressID="";
private String freight ="";

private String dist="";
private String accountID = "";
private String accountNm = "";
private String caseID = "";
private String caseInfoID = "";
private String surgeryDt = "";
private String orderTypeNm = "";
private String custPO = "";
private String inputStr = "";
private String parentOrderID = "";


private ArrayList alRepAdd = new ArrayList();
private HashMap hmNames = new HashMap ();
private ArrayList alShipTo = new ArrayList();
private ArrayList alShipToId = new ArrayList();
private ArrayList alAddress = new ArrayList();
private ArrayList alTagID = new ArrayList();
private ArrayList alTagDetails = new ArrayList();
private ArrayList alCtrlNmDetails = new ArrayList();
private ArrayList alShipMode = new ArrayList();


public ArrayList getAlShipMode() {
	return alShipMode;
}

public void setAlShipMode(ArrayList alShipMode) {
	this.alShipMode = alShipMode;
}

/**
 * @return the parentOrderID
 */
public String getParentOrderID() {
	return parentOrderID;
}

/**
 * @param parentOrderID the parentOrderID to set
 */
public void setParentOrderID(String parentOrderID) {
	this.parentOrderID = parentOrderID;
}
/**
 * @return the alCtrlNmDetails
 */
public ArrayList getAlCtrlNmDetails() {
	return alCtrlNmDetails;
}

/**
 * @param alCtrlNmDetails the alCtrlNmDetails to set
 */
public void setAlCtrlNmDetails(ArrayList alCtrlNmDetails) {
	this.alCtrlNmDetails = alCtrlNmDetails;
}

public String getFreight() {
	return freight;
}

public void setFreight(String freight) {
	this.freight = freight;
}

public String getRepAddressID() {
	return repAddressID;
}

public void setRepAddressID(String repAddressID) {
	this.repAddressID = repAddressID;
}

public String getShipToID() {
	return shipToID;
}

public void setShipToID(String shipToID) {
	this.shipToID = shipToID;
}

public String getShipTo() {
	return shipTo;
}

public void setShipTo(String shipTo) {
	this.shipTo = shipTo;
}

public String getAddressid() {
	return addressid;
}

public void setAddressid(String addressid) {
	this.addressid = addressid;
}

public String getShipCarrier() {
	return shipCarrier;
}

public void setShipCarrier(String shipCarrier) {
	this.shipCarrier = shipCarrier;
}

public String getShipMode() {
	return shipMode;
}

public void setShipMode(String shipMode) {
	this.shipMode = shipMode;
}

public String getSource() {
	return source;
}

public void setSource(String source) {
	this.source = source;
}

public String getTrackNo() {
	return trackNo;
}

public void setTrackNo(String trackNo) {
	this.trackNo = trackNo;
}


/**
 * @return the orderID
 */
public String getOrderID() {
	return orderID;
}

/**
 * @param orderID the orderID to set
 */
public void setOrderID(String orderID) {
	this.orderID = orderID;
}

/**
 * @return the tagID
 */
public String getTagID() {
	return tagID;
}

/**
 * @param tagID the tagID to set
 */
public void setTagID(String tagID) {
	this.tagID = tagID;
}


/**
 * @return the accountID
 */
public String getAccountID() {
	return accountID;
}

/**
 * @param accountID the accountID to set
 */
public void setAccountID(String accountID) {
	this.accountID = accountID;
}

/**
 * @return the accountNm
 */
public String getAccountNm() {
	return accountNm;
}

/**
 * @param accountNm the accountNm to set
 */
public void setAccountNm(String accountNm) {
	this.accountNm = accountNm;
}

/**
 * @return the caseID
 */
public String getCaseID() {
	return caseID;
}

/**
 * @param caseID the caseID to set
 */
public void setCaseID(String caseID) {
	this.caseID = caseID;
}

/**
 * @return the caseInfoID
 */
public String getCaseInfoID() {
	return caseInfoID;
}

/**
 * @param caseInfoID the caseInfoID to set
 */
public void setCaseInfoID(String caseInfoID) {
	this.caseInfoID = caseInfoID;
}

/**
 * @return the surgeryDt
 */
public String getSurgeryDt() {
	return surgeryDt;
}

/**
 * @param surgeryDt the surgeryDt to set
 */
public void setSurgeryDt(String surgeryDt) {
	this.surgeryDt = surgeryDt;
}

/**
 * @return the orderTypeNm
 */
public String getOrderTypeNm() {
	return orderTypeNm;
}

/**
 * @param orderTypeNm the orderTypeNm to set
 */
public void setOrderTypeNm(String orderTypeNm) {
	this.orderTypeNm = orderTypeNm;
}

/**
 * @return the custPO
 */
public String getCustPO() {
	return custPO;
}

/**
 * @param custPO the custPO to set
 */
public void setCustPO(String custPO) {
	this.custPO = custPO;
}

/**
 * @return the inputStr
 */
public String getInputStr() {
	return inputStr;
}

/**
 * @param inputStr the inputStr to set
 */
public void setInputStr(String inputStr) {
	this.inputStr = inputStr;
}

/**
 * @return the alRepAdd
 */
public ArrayList getAlRepAdd() {
	return alRepAdd;
}

/**
 * @param alRepAdd the alRepAdd to set
 */
public void setAlRepAdd(ArrayList alRepAdd) {
	this.alRepAdd = alRepAdd;
}

/**
 * @return the shipId
 */
public String getShipId() {
	return shipId;
}

/**
 * @param shipId the shipId to set
 */
public void setShipId(String shipId) {
	this.shipId = shipId;
}


/**
 * @return the names
 */
public String getNames() {
	return names;
}

/**
 * @param names the names to set
 */
public void setNames(String names) {
	this.names = names;
}


/**
 * @return the dist
 */
public String getDist() {
	return dist;
}

/**
 * @param dist the dist to set
 */
public void setDist(String dist) {
	this.dist = dist;
}

/**
 * @return the hmNames
 */
public HashMap getHmNames() {
	return hmNames;
}

/**
 * @param hmNames the hmNames to set
 */
public void setHmNames(HashMap hmNames) {
	this.hmNames = hmNames;
}

/**
 * @return the alShipTo
 */
public ArrayList getAlShipTo() {
	return alShipTo;
}

/**
 * @param alShipTo the alShipTo to set
 */
public void setAlShipTo(ArrayList alShipTo) {
	this.alShipTo = alShipTo;
}

/**
 * @return the alShipToId
 */
public ArrayList getAlShipToId() {
	return alShipToId;
}

/**
 * @param alShipToId the alShipToId to set
 */
public void setAlShipToId(ArrayList alShipToId) {
	this.alShipToId = alShipToId;
}

/**
 * @return the alAddress
 */
public ArrayList getAlAddress() {
	return alAddress;
}

/**
 * @param alAddress the alAddress to set
 */
public void setAlAddress(ArrayList alAddress) {
	this.alAddress = alAddress;
}

/**
 * @return the alTagID
 */
public ArrayList getAlTagID() {
	return alTagID;
}

/**
 * @param alTagID the alTagID to set
 */
public void setAlTagID(ArrayList alTagID) {
	this.alTagID = alTagID;
}

/**
 * @return the alTagDetails
 */
public ArrayList getAlTagDetails() {
	return alTagDetails;
}

/**
 * @param alTagDetails the alTagDetails to set
 */
public void setAlTagDetails(ArrayList alTagDetails) {
	this.alTagDetails = alTagDetails;
}


	
}
