package com.globus.custservice.transfer.forms;
import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmConsignmentTransferForm extends GmLogForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String transferType="";
	private String transferMode="";
	private String transferFrom="";
	private String transferTo="";
	private String date="";
	private String accountId="";
	private String inputStr="";
	private String transferAccessFl="";
	private String consignmentId="";
	private String strxmlGridData="";
	private String searchAccountId="";
	private String transferReason="";
	//hidden form values
	private String hTransferFrom="";
	private String hTransferTo="";
	private String hTransferType="";
	private String hTransferFromLoad="";
	private String transferId="";
	

	private ArrayList alType = new ArrayList();
	private ArrayList alMode = new ArrayList();
	
	/**
	 * @return the transferType
	 */
	public String getTransferType() {
		return transferType;
	}
	/**
	 * @param transferType the transferType to set
	 */
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	/**
	 * @return the transferMode
	 */
	public String getTransferMode() {
		return transferMode;
	}
	/**
	 * @param transferMode the transferMode to set
	 */
	public void setTransferMode(String transferMode) {
		this.transferMode = transferMode;
	}
	
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	/**
	 * @return the transferAccessFl
	 */
	public String getTransferAccessFl() {
		return transferAccessFl;
	}
	/**
	 * @param transferAccessFl the transferAccessFl to set
	 */
	public void setTransferAccessFl(String transferAccessFl) {
		this.transferAccessFl = transferAccessFl;
	}
	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}
	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}
	/**
	 * @return the alMode
	 */
	public ArrayList getAlMode() {
		return alMode;
	}
	/**
	 * @param alMode the alMode to set
	 */
	public void setAlMode(ArrayList alMode) {
		this.alMode = alMode;
	}
	/**
	 * @return the consignmentId
	 */
	public String getConsignmentId() {
		return consignmentId;
	}
	/**
	 * @param consignmentId the consignmentId to set
	 */
	public void setConsignmentId(String consignmentId) {
		this.consignmentId = consignmentId;
	}
	/**
	 * @return the strxmlGridData
	 */
	public String getStrxmlGridData() {
		return strxmlGridData;
	}
	/**
	 * @param strxmlGridData the strxmlGridData to set
	 */
	public void setStrxmlGridData(String strxmlGridData) {
		this.strxmlGridData = strxmlGridData;
	}
	/**
	 * @return the searchAccountId
	 */
	public String getSearchAccountId() {
		return searchAccountId;
	}
	/**
	 * @param searchAccountId the searchAccountId to set
	 */
	public void setSearchAccountId(String searchAccountId) {
		this.searchAccountId = searchAccountId;
	}
	/**
	 * @return the hTransferFrom
	 */
	public String gethTransferFrom() {
		return hTransferFrom;
	}
	/**
	 * @param hTransferFrom the hTransferFrom to set
	 */
	public void sethTransferFrom(String hTransferFrom) {
		this.hTransferFrom = hTransferFrom;
	}
	/**
	 * @return the hTransferTo
	 */
	public String gethTransferTo() {
		return hTransferTo;
	}
	/**
	 * @param hTransferTo the hTransferTo to set
	 */
	public void sethTransferTo(String hTransferTo) {
		this.hTransferTo = hTransferTo;
	}
	/**
	 * @return the hTransferType
	 */
	public String gethTransferType() {
		return hTransferType;
	}
	/**
	 * @param hTransferType the hTransferType to set
	 */
	public void sethTransferType(String hTransferType) {
		this.hTransferType = hTransferType;
	}
	/**
	 * @return the hTransferFromLoad
	 */
	public String gethTransferFromLoad() {
		return hTransferFromLoad;
	}
	/**
	 * @param hTransferFromLoad the hTransferFromLoad to set
	 */
	public void sethTransferFromLoad(String hTransferFromLoad) {
		this.hTransferFromLoad = hTransferFromLoad;
	}
	/**
	 * @return the transferFrom
	 */
	public String getTransferFrom() {
		return transferFrom;
	}
	/**
	 * @param transferFrom the transferFrom to set
	 */
	public void setTransferFrom(String transferFrom) {
		this.transferFrom = transferFrom;
	}
	/**
	 * @return the transferTo
	 */
	public String getTransferTo() {
		return transferTo;
	}
	/**
	 * @param transferTo the transferTo to set
	 */
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the transferReason
	 */
	public String getTransferReason() {
		return transferReason;
	}
	/**
	 * @param transferReason the transferReason to set
	 */
	public void setTransferReason(String transferReason) {
		this.transferReason = transferReason;
	}
	/**
	 * @return the transferId
	 */
	public String getTransferId() {
		return transferId;
	}
	/**
	 * @param transferId the transferId to set
	 */
	public void setTransferId(String transferId) {
		this.transferId = transferId;
	}
	

}
