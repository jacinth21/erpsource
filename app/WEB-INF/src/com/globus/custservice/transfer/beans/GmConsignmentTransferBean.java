package com.globus.custservice.transfer.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;


import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.beans.GmBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmConsignmentTransferBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
    GmCommonClass gmCommon = new GmCommonClass();

		public GmConsignmentTransferBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
		
	/**loadConsignmentDtls:This method used to load consignment details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList loadConsignmentDtls(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();
		// Intialize
		String strConsignmentID = "";

		strConsignmentID = GmCommonClass.parseZero((String) hmParam.get("CONSIGNMENTID"));
		gmDBManager.setPrepareString("gm_pkg_cs_consignment_transfer.gm_load_consignment_dtls", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strConsignmentID);
		gmDBManager.execute();

		// Get failed Information
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		gmDBManager.close();


		return alReturn;

	}	
		
	/**validateConsignment:This method used to validate the consignment Id field
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String validateConsignment(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strJsonString = "";
		String strConsignmentID = "";
		
       
		strConsignmentID = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));
		
	
		gmDBManager.setPrepareString("gm_pkg_cs_consignment_transfer.gm_validate_consignment", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strConsignmentID);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strJsonString;
	}
	
	
    
  	/** reverseConsignment - to reverse the transfer if the consignment is transferred
  	 * @param hmValue
  	 * @return void
  	 * @throws AppError
  	 */
  	public String reverseConsignmentTransfer(HashMap hmValue) throws AppError {

  		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
  		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
  		String strTransferId = GmCommonClass.parseNull((String) hmValue.get("TRANSFERID"));
  		String strUserId = GmCommonClass.parseNull((String) hmValue.get("USERID"));
  		String strLogReason = GmCommonClass.parseNull((String) hmValue.get("TXT_LOGREASON"));
  		
  		String strMessage = "";
  		gmDBManager.setPrepareString("gm_pkg_cs_consignment_transfer.gm_reverse_consignment_transfer", 3);
  		gmDBManager.registerOutParameter(3, java.sql.Types.VARCHAR); 
  		gmDBManager.setString(1, strTransferId);
  		gmDBManager.setString(2, strUserId);
  		gmDBManager.execute();
  		strMessage = GmCommonClass.parseNull(gmDBManager.getString(3));
  		// Code to save the log information (comments) from the screen
  		if (!strLogReason.equals("")) {
  			gmCommonBean.saveLog(gmDBManager, strTransferId, strLogReason,strUserId, "1209");// 1209 is the code for Transfer Type in // the log table. GPLOG
  		}

  		gmDBManager.commit();
  		
  	   return strMessage;

  	}
	
	
		
	/** generateOutput - This method is used to generate xml data which needs to show in grid.
	 * @param alGridData
	 * @param hmDtls
	 * @return
	 * @throws AppError
	 */
	public String generateOutput(ArrayList alGridData, HashMap hmDtls)
			throws AppError {
		GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
		String strTemplateName = GmCommonClass.parseNull((String) hmDtls.get("TEMPLATE_NM"));
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmDtls.get("STRSESSCOMPANYLOCALE"));
		String strLabelProperties = GmCommonClass.parseNull((String) hmDtls.get("LABEL_PROPERTIES_NM"));

		gmTemplateUtil.setDataList("alGridData", alGridData);
		gmTemplateUtil.setDataMap("hmApplnParam", hmDtls);
		gmTemplateUtil.setTemplateName(strTemplateName);
		gmTemplateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strLabelProperties, strSessCompanyLocale));
		gmTemplateUtil.setTemplateSubDir("custservice/transfer/templates");
		return gmTemplateUtil.generateOutput();

	}	

}
