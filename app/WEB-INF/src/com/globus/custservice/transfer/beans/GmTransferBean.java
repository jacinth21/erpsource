

/**
 * 
 */
package com.globus.custservice.transfer.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import oracle.jdbc.driver.OracleTypes;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath 
 *
 */
public class GmTransferBean extends GmBean  {
    
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
    GmCommonClass gmCommon = new GmCommonClass();
    
	  // this will be removed all place changed with GmDataStoreVO constructor

	  public GmTransferBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmTransferBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	  }

    /**
      * loadPendingTransfer - This method will 
      * @return ArrayList
      * @exception AppError
    **/
     
    public ArrayList loadPendingTransfer()throws AppError
    {
    	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    	ArrayList alResult = new ArrayList();
    	gmDBManager.setPrepareString("GM_PKG_CS_TRANSFER.GM_CS_FCH_PENDING_TRANSFER",1);
        gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
        gmDBManager.execute();
        
        alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
        gmDBManager.close();
        log.debug("Consigned Transfer alResult:"+alResult);
        return alResult;   
    } // End of loadPendingTransfer
    
 public HashMap loadTransfer(String strTransferID) throws AppError {
     HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPrepareQuery = null;
    ArrayList alTransferDetails = new ArrayList();
    HashMap hmTransfer = new HashMap();
    ResultSet rsTransfer = null, rsTransferItems = null;
    gmDBManager.setPrepareString("GM_PKG_CS_TRANSFER.GM_CS_FCH_TRANSFER_INFO", 3);
    // register out parameter
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTransferID);
    gmDBManager.execute();

    // populate the Transfer for the transfer id in to a hashmap
    rsTransfer = (ResultSet) gmDBManager.getObject(2);
    hmTransfer = gmDBManager.returnHashMap(rsTransfer);
    hmReturn.put("TRANSFER", hmTransfer);
    // log.debug("hmReturn loaded in transfer bean");
    // populate the Transfer Details for the transfer id in to an ArrayList
    rsTransferItems = (ResultSet) gmDBManager.getObject(3);
    // log.debug("rsTransferItems Value : "+((rsTransferItems == null)?"Null":"Not Null"));
    alTransferDetails = gmDBManager.returnArrayList(rsTransferItems);
    gmDBManager.close();
    hmReturn.put("TRANSFER_ITEM_DETAILS", alTransferDetails);
    // / log.debug("alTransferDetails loaded in transfer bean : "+hmReturn.toString());
    return hmReturn;
    }
 
 
 public void  saveAcceptTransfer(String strTransferID,  String strUserId, String strCompleteFlag, String strInputStr,  String strLogReason,  String strLogNo) throws AppError{
    
	 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());     
     GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
     String strError = null;        
     
        
     	 
         gmDBManager.setPrepareString("GM_PKG_CS_TRANSFER.GM_CS_SAV_ACCEPT_TRANSFER",6);
         gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);             
         gmDBManager.setString(1,strTransferID);
         gmDBManager.setString(2,strUserId);   
         gmDBManager.setString(3,strCompleteFlag); 
         gmDBManager.setString(4,strInputStr); 
         gmDBManager.setInt(5,54011); 
         
         gmDBManager.execute();
         
         strError = GmCommonClass.parseNull(gmDBManager.getString(6));
         if (strLogReason !=null && !strLogReason.equals(""))
         {                         
             gmCommonBean.saveLog(gmDBManager,strTransferID,strLogReason,strUserId,strLogNo);          
         }
           
         log.debug("strError = "+strError);
         if (!strError.equals(""))
 		{	
        	gmDBManager.close();
 			throw new AppError(strError,"",'E');
 		}
        gmDBManager.commit();     
 } 
 /**
     *  - This method will load SetTransferInfo. It will fetch the parts associated for that set id for that Distributor / Inhouse.
     *  For detailed documentation refer procedure
     * @param String strTransferType - Transfer Type (�Distributor to Distributor� (90300), �In-house to Distributor� (90301)) - TSFTP
     * @param String strTransferId - would be either a Distributor ID or an employee ID from whom the CN / RA has to be transferred. 
     * @return ArrayList
     * @exception AppError
     **/
 	public ArrayList loadFullTransferInfo(String strTransferType, String strTransferId) throws AppError
     {
         
 		GmDBManager gmDBManager = new GmDBManager();
 		ArrayList alReturn = new ArrayList();
       
 		gmDBManager.setPrepareString("gm_pkg_cs_transfer.gm_cs_fch_full_transfer_info",3);
 		gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
 		gmDBManager.setString(1,strTransferType);
 		gmDBManager.setString(2,strTransferId);
 		gmDBManager.execute();
 		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
 		gmDBManager.close();
 		log.debug(" size of tsf list is " + alReturn.size());
 		return alReturn;
     } //End of methodName
  
  
  /**
  *  This method will load SetTransferInfo. It will fetch the parts associated for that set id for that Distributor / Inhouse.
  *  For detailed documentation refer procedure
  * @param String strTransferType - Transfer Type (�Distributor to Distributor� (90300), �In-house to Distributor� (90301)) - TSFTP
  * @param String strTransferId - would be either a Distributor ID or an employee ID from whom the CN / RA has to be transferred. 
  * @param String strSetId - Set Id for which parts needs to be pulled 
  * @return ArrayList
  * @exception AppError
  **/
  public ArrayList loadSetTransferInfo(String strTransferType, String strTransferId, String strSetId) throws AppError
  {
      
      DBConnectionWrapper dbCon = null;
      dbCon = new DBConnectionWrapper();
      
      CallableStatement csmt = null;
      Connection conn = null;
      
      String strPrepareString = null;
      
      ArrayList alReturn = new ArrayList();
      ArrayList alSetTransferInfo = new ArrayList();
                    
      ResultSet rsSetTransfer = null;
       ResultSet rsReturn = null;
       
      try
      {
          conn = dbCon.getConnection();
          strPrepareString = dbCon.getStrPrepareString("gm_pkg_cs_transfer.gm_cs_fch_set_transfer_info",4);
          csmt = conn.prepareCall(strPrepareString);
          /*
          *register out parameter and set input parameters
          */
          csmt.registerOutParameter(4,OracleTypes.CURSOR);
          csmt.setString(1,strTransferType);
          csmt.setString(2,strTransferId);
          csmt.setString(3,strSetId);
          
           csmt.execute();
           rsSetTransfer = (ResultSet)csmt.getObject(4);
           alSetTransferInfo = dbCon.returnArrayList(rsSetTransfer);
       
           log.debug(" loadSetTransferInfo size is " + alSetTransferInfo.size());
          
      }catch(Exception e)
      {
           e.printStackTrace();
          log.error("Exception is "+e);
          throw new AppError(e);
      }
      finally
      {
          try
          {
              if (csmt != null)
              {
                  csmt.close();
              }//Enf of if  (csmt != null)
                  if(conn!=null)
                  {
                      conn.close();      /* closing the Connections */
                  }
          }
          catch(Exception e)
          {
              throw new AppError(e);
          }//End of catch
          finally
          {
              csmt = null;
              conn = null;
              dbCon = null;
          }
      }
      return alSetTransferInfo;
      
  } //End of methodName
   
  
  /**
   *  This method will load PartTransferInfo. It will fetch the parts which are sent as input
   *  For detailed documentation refer procedure
   * @param String strTransferType - Transfer Type (�Distributor to Distributor� (90300), �In-house to Distributor� (90301)) - TSFTP
   * @param String strTransferId - would be either a Distributor ID or an employee ID from whom the CN / RA has to be transferred. 
   * @param String strPartNums - Part nums for which details have to be pulled
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadPartTransferInfo(String strTransferType, String strTransferId,
      String strPartNums) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ArrayList alPartTransferInfo = new ArrayList();

    ResultSet rsPartTransfer = null;
    ResultSet rsReturn = null;


    gmDBManager.setPrepareString("gm_pkg_cs_transfer.gm_cs_fch_part_transfer_info", 4);

    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTransferType);
    gmDBManager.setString(2, strTransferId);
    gmDBManager.setString(3, strPartNums);

    gmDBManager.execute();
    rsPartTransfer = (ResultSet) gmDBManager.getObject(4);
    alPartTransferInfo = gmDBManager.returnArrayList(rsPartTransfer);
    gmDBManager.close();
    log.debug(" loadPartTransferInfo size is " + alPartTransferInfo.size());


    return alPartTransferInfo;

  } // End of methodName
    
  
 
   
   /**
    * saveTransfer - This method will be used to initate a Full / Set / part transfer of (consignments / returns) from a dist / inhouse to a dist 
    * It would call the package gm_pkg_cs_transfer.gm_cs_sav_transfer and save the transfer info in T921_transfer_detail
    * @return void. Upong successful initiation, a success apperror is thrown with the success message and the CN, RA and TSF ids
    * @exception AppError
    **/
    public String saveTransfer(HashMap hmValue) throws AppError
    {
        GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
                
        String strTransferId = "";
                      
        String strTransferType = GmCommonClass.parseNull((String)hmValue.get("TRANSFERTYPE"));
        String strTransferMode = GmCommonClass.parseNull((String)hmValue.get("TRANSFERMODE"));
        String strTransferFrom = GmCommonClass.parseNull((String)hmValue.get("TRANSFERFROM"));
        String strTransferTo = GmCommonClass.parseNull((String)hmValue.get("TRANSFERTO"));
        String strTransferReason = GmCommonClass.parseNull((String)hmValue.get("TRANSFERREASON"));
        String strDate = GmCommonClass.parseNull((String)hmValue.get("DATE"));
        String strUserId = GmCommonClass.parseNull((String)hmValue.get("USERID"));
        String strRefId_inputstr = GmCommonClass.parseNull((String)hmValue.get("INPUTSTR"));
        String strSetId = GmCommonClass.parseNull((String)hmValue.get("SET"));
        String strLogReason = GmCommonClass.parseNull((String)hmValue.get("LOGVAL"));
               
        ArrayList alReturn = new ArrayList();
        ResultSet rs = null;
        
            gmDBManager.setPrepareString("gm_pkg_cs_transfer.gm_cs_sav_transfer",10);
            gmDBManager.registerOutParameter(10,java.sql.Types.CHAR);
            
            gmDBManager.setString(1,strTransferType);
            gmDBManager.setString(2,strTransferMode);
            gmDBManager.setString(3,strTransferFrom);
            gmDBManager.setString(4,strTransferTo);
            gmDBManager.setString(5,strTransferReason);
            gmDBManager.setString(6,strDate);
            gmDBManager.setString(7,strUserId);
            gmDBManager.setString(8,strSetId); 
            gmDBManager.setString(9,strRefId_inputstr);
            gmDBManager.execute();
          
          // Transfer Id will be of the form TransferId@ConsignmentId@ReturnId            
            strTransferId = gmDBManager.getString(10);
            
            // Code to save the log information (comments) from the screen 
            if (!strLogReason.equals(""))
            {
                gmCommonBean.saveLog(gmDBManager,strTransferId,strLogReason,strUserId,"1209");// 1209 is the code for Transfer Type in the log table. GPLOG
            }
            gmDBManager.commit();
            
            // strMessage = "Transaction  ".concat(strTransferId).concat(" Consignment ").concat(strConsignmentId).concat(" Return ").concat(strReturnId).concat(" successfully created ");
            log.debug(" strTransferId after initiate " + strTransferId); 
            return strTransferId;
        
    }// end of save transfer
    
    /**
    	 *  - This method will fetch the report details for Transfer
    	 * @param HashMap 
    	 * @return RowSetDynaClass
    	 * @exception AppError
    	 **/
    	 public String  fetchTransferReport(HashMap hmValue ) throws AppError
    	 {
                RowSetDynaClass resultSet = null ;
                
                String strJSONTransferRptDtls="";
                
                String strTransferType = GmCommonClass.parseZero((String)hmValue.get("TRANSFERTYPE"));
                String strTransferReason = GmCommonClass.parseZero((String)hmValue.get("TRANSFERREASON"));
                String strTransferFrom = GmCommonClass.parseZero((String)hmValue.get("TRANSFERFROM"));
                String strTransferTo = GmCommonClass.parseZero((String)hmValue.get("TRANSFERTO"));
                String strFromDate = GmCommonClass.parseNull((String)hmValue.get("FROMDATE"));
                String strToDate = GmCommonClass.parseNull((String)hmValue.get("TODATE"));
                String strStatus = GmCommonClass.parseZero((String)hmValue.get("STATUS"));
                String strCNID = GmCommonClass.getStringWithQuotes(GmCommonClass.parseNull((String) hmValue.get("CNID")));
                String strCompDateFrmt = getCompDateFmt();
                GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
                    StringBuffer sbQuery = new StringBuffer();
                    sbQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT('tsfid' VALUE C920_TRANSFER_ID " );
                    sbQuery.append(" , 'fromname' VALUE  DECODE (c901_transfer_type,'90300' ");
                    sbQuery.append(" , Get_Distributor_Name (c920_from_ref_id),'26240615',get_distributor_name (c920_from_ref_id),'26240616',get_distributor_name (c920_from_ref_id),'26240577', GET_ACCOUNT_NAME (c920_from_ref_id), Get_User_Name (c920_from_ref_id) ) ");
                    sbQuery.append(" , 'toid' VALUE DECODE (c901_transfer_type,'26240577' ");
                    sbQuery.append(" , GET_ACCOUNT_NAME (C920_TO_REF_ID),Get_Distributor_Name(C920_TO_REF_ID)) ");
                    sbQuery.append(" , 'tsfreason' VALUE Get_Code_Name(C901_TRANSFER_REASON)  " );
                    sbQuery.append(" , 'tsfmode' VALUE Get_Code_Name(C901_TRANSFER_MODE)" );
                    sbQuery.append(" , 'tsfdate' VALUE TO_CHAR(C920_TRANSFER_DATE,'"+strCompDateFrmt+"') ");
                    sbQuery.append(" , 'status' VALUE DECODE(C920_STATUS_FL,1,'Open',2,'Closed',3,'Reversed') ");
                    sbQuery.append(" , 'cnid' VALUE C504_CONSIGNMENT_ID ) RETURNING CLOB)");
                    sbQuery.append(" FROM T920_TRANSFER ");
                    sbQuery.append(" WHERE  C920_VOID_FL IS NULL  ");
                    sbQuery.append(" AND C1900_COMPANY_ID = " + getCompId());
                    if (!strTransferType.equals("0")) // 0 is the default value in dropdown tag lib 
                    {
                        sbQuery.append(" AND C901_TRANSFER_TYPE = '" ) ;
                        sbQuery.append(strTransferType) ;
                        sbQuery.append("'");
                    }           
                    
                    if (!strTransferReason.equals("0"))
                    {
                        sbQuery.append(" AND C901_TRANSFER_REASON = '" ) ;
                        sbQuery.append(strTransferReason) ;
                        sbQuery.append("'");
                    }  
                    
                    if (!strTransferFrom.equals("0")) 
                    {
                        sbQuery.append(" AND C920_FROM_REF_ID = '" ) ;
                        sbQuery.append(strTransferFrom) ;
                        sbQuery.append("'");
                    }
                    
                    if (!strTransferTo.equals("0")) 
                    {
                        sbQuery.append(" AND C920_TO_REF_ID = '" ) ;
                        sbQuery.append(strTransferTo) ;
                        sbQuery.append("'");
                    }
                    
                    if (!strFromDate.equals(""))
                    {
                        sbQuery.append(" AND C920_TRANSFER_DATE >= TO_DATE('" ) ;
                        sbQuery.append(strFromDate) ;
                        sbQuery.append("', '"+strCompDateFrmt+"') ");
                    }

                    if (!strToDate.equals(""))
                    {
                        sbQuery.append(" AND C920_TRANSFER_DATE <= TO_DATE('" ) ;
                        sbQuery.append(strToDate) ;
                        sbQuery.append("', '"+strCompDateFrmt+"') ");
                    }
                    
                    if (!strStatus.equals("0")) // Vendor ID
                    {
                        sbQuery.append(" AND C920_STATUS_FL = '" ) ;
                        sbQuery.append(strStatus) ;
                        sbQuery.append("'");
                    }

					if (!strCNID.equals("")) {
						sbQuery.append(" AND C504_CONSIGNMENT_ID IN ('");
						sbQuery.append(strCNID);
						sbQuery.append("')");
					}

                    log.debug("Transfer Report Query " + sbQuery.toString());
                    strJSONTransferRptDtls = gmDBManager.queryJSONRecord(sbQuery.toString());
                //      return alReturn;
                return strJSONTransferRptDtls;
            } //End of methodName
         
         
         /**
           *  This method will load PartTransferInfo. It will fetch the parts which are sent as input
           *  For detailed documentation refer procedure
           * @param String strTransferId - Transfer Id (Full / Set / Part transfer) 
           * @return HashMap
           * @exception AppError
           **/
           public HashMap printTransfer(String strTransferId) throws AppError
           {
               
               DBConnectionWrapper dbCon = null;
               dbCon = new DBConnectionWrapper();
               
               CallableStatement csmt = null;
               Connection conn = null;
               
               String strPrepareString = null;
               
               HashMap hmPrintTransferHeader = new HashMap();
               ArrayList alPrintTransferDetails = new ArrayList();
               HashMap hmReturn = new HashMap();
                             
               ResultSet rsPrintTransferHeader = null;
               ResultSet rsPrintTransferDetails = null;
               ResultSet rsReturn = null;
                
               try
               {
                   conn = dbCon.getConnection();
                   strPrepareString = dbCon.getStrPrepareString("gm_pkg_cs_transfer.gm_cs_fch_transfer_print",3);
                   csmt = conn.prepareCall(strPrepareString);
                   /*
                   *register out parameter and set input parameters
                   */
                   csmt.registerOutParameter(2,OracleTypes.CURSOR);
                   csmt.registerOutParameter(3,OracleTypes.CURSOR);
                   csmt.setString(1,strTransferId);
                   
                   csmt.execute();
                   rsPrintTransferHeader = (ResultSet)csmt.getObject(2);
                   rsPrintTransferDetails = (ResultSet)csmt.getObject(3);
                   hmPrintTransferHeader = dbCon.returnHashMap(rsPrintTransferHeader);
                   alPrintTransferDetails = dbCon.returnArrayList(rsPrintTransferDetails);
                
                   log.debug(" hmPrintTransferHeader values are " + hmPrintTransferHeader);
                   log.debug(" alPrintTransferDetails detail size is " + alPrintTransferDetails.size());
                   
                   hmReturn.put("HMPRINTHEADER",hmPrintTransferHeader);
                   hmReturn.put("ALPRINTDETAILS",alPrintTransferDetails);
                   
               }catch(Exception e)
               {
                    e.printStackTrace();
                   log.error("Exception is "+e);
                   throw new AppError(e);
               }
               finally
               {
                   try
                   {
                       if (csmt != null)
                       {
                           csmt.close();
                       }//Enf of if  (csmt != null)
                           if(conn!=null)
                           {
                               conn.close();      /* closing the Connections */
                           }
                   }
                   catch(Exception e)
                   {
                       throw new AppError(e);
                   }//End of catch
                   finally
                   {
                       csmt = null;
                       conn = null;
                       dbCon = null;
                   }
               }
               return hmReturn;
               
           } //End of methodName
           
       
       /**
        *  This method will load details about the transfer from T920 table.
        *  For detailed documentation refer procedure
        * @param String strTransferId - would be either a Distributor ID or an employee ID from whom the CN / RA has to be transferred. 
        * @return HashMap
        * @exception AppError
        **/
        public HashMap loadTransferHeaderInfo(String strTransferId) throws AppError
        {
            
            DBConnectionWrapper dbCon = null;
            dbCon = new DBConnectionWrapper();
            
            CallableStatement csmt = null;
            Connection conn = null;
            String strPrepareString = null;
            HashMap hmReturn = new HashMap();
            
            ResultSet rsPartTransfer = null;
            ResultSet rsReturn = null;
             
            try
            {
                conn = dbCon.getConnection();
                strPrepareString = dbCon.getStrPrepareString("gm_pkg_cs_transfer.gm_cs_fch_transfer_header",2);
                csmt = conn.prepareCall(strPrepareString);
                /*
                *register out parameter and set input parameters
                */
                csmt.registerOutParameter(2,OracleTypes.CURSOR);
                csmt.setString(1,strTransferId);
                
                 csmt.execute();
                 rsPartTransfer = (ResultSet)csmt.getObject(2);
                 hmReturn = dbCon.returnHashMap(rsPartTransfer);
             
                 log.debug(" loadPartTransferInfo size is " + hmReturn.size());
                
            }catch(Exception e)
            {
                 e.printStackTrace();
                log.error("Exception is "+e);
                throw new AppError(e);
            }
            finally
            {
                try
                {
                    if (csmt != null)
                    {
                        csmt.close();
                    }//Enf of if  (csmt != null)
                        if(conn!=null)
                        {
                            conn.close();      /* closing the Connections */
                        }
                }
                catch(Exception e)
                {
                    throw new AppError(e);
                }//End of catch
                finally
                {
                    csmt = null;
                    conn = null;
                    dbCon = null;
                }
            }
            return hmReturn;
            
        } //End of methodName
        /**
         *  This method will load details about the tag transfer information based on the Tagid.
         * @param String strTransferId - would be either a Distributor ID or an employee ID from whom the CN / RA has to be transferred. 
         * @return ArrayList
         * @exception AppError
         **/
        public ArrayList fetchTranferTagDetail(String strTransferId,String strTransferType) throws AppError {
        	
        	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
        	ArrayList alResult = new ArrayList();
        	gmDBManager.setPrepareString("GM_PKG_CS_TRANSFER.GM_CS_FCH_TAG_TRANSFER",3);
        	gmDBManager.setString(1, strTransferId);
        	gmDBManager.setString(2, strTransferType);
            gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
            gmDBManager.execute();
            alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
            gmDBManager.close();
            return alResult;   
        }
        
       
        
        /**
         *  This procedure is to fetch the tranfer Detail information will be called from Accept servlet and is used to display information Transfer
      		Detail infomation in grid for each transfer on Verify transfer Screen
         * @param String strTransferId - 
         * @return ArrayList
         * @exception AppError
         **/
        public ArrayList fetchTransferDetail(String strTransferId) throws AppError {
        	
        	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
        	ArrayList alResult = new ArrayList();
        	gmDBManager.setPrepareString("GM_PKG_CS_TRANSFER.GM_CS_FCH_TRANSFER_DETAIL_INFO",2);
        	gmDBManager.setString(1, strTransferId);
            gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
            gmDBManager.execute();
            alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
            gmDBManager.close();
            return alResult;   
        }
        
        
        /**
    	 * @param strConsignmentId
    	 * @param strAccountId
    	 * @return
    	 * @throws AppError
    	 */
    	public ArrayList loadAccConsignTransfer(String strConsignmentId,
    			String strAccountId) throws AppError {

    		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    		ArrayList alResult = new ArrayList();
    		gmDBManager.setPrepareString("GM_PKG_CS_TRANSFER.GM_CS_FCH_ACC_CONSIGN_TRANSFER", 3);
    		gmDBManager.setString(1, strConsignmentId);
    		gmDBManager.setString(2, strAccountId);
    		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    		gmDBManager.execute();
    		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    		gmDBManager.close();
    		return alResult;
    	}
             
    	/**saveConsignmentTransfer:This method used to saveConsignmentTransfer details
    	 * @param hmValue
    	 * @return
    	 * @throws AppError
    	 */
    	public String saveConsignmentTransfer(HashMap hmParam) throws AppError {

    		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    		String strConsignTransferId = "";

    		String strTransferType = GmCommonClass.parseNull((String) hmParam.get("TRANSFERTYPE"));
    		String strTransferMode = GmCommonClass.parseNull((String) hmParam.get("TRANSFERMODE"));
    		String strTransferFrom = GmCommonClass.parseNull((String) hmParam.get("TRANSFERFROM"));
    		String strTransferTo = GmCommonClass.parseNull((String) hmParam.get("TRANSFERTO"));
    		String strTransferReason = GmCommonClass.parseNull((String) hmParam.get("TRANSFERREASON"));
    		String strDate = GmCommonClass.parseNull((String) hmParam.get("DATE"));
    		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    		String strRefId_inputstr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOGVAL"));
    		String strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));
    		
    		gmDBManager.setPrepareString("gm_pkg_cs_transfer.gm_cs_sav_transfer",13);
    		gmDBManager.registerOutParameter(13, OracleTypes.VARCHAR);
    		gmDBManager.setString(1, strTransferType);
    		gmDBManager.setString(2, strTransferMode);
    		gmDBManager.setString(3, "");
    		gmDBManager.setString(4, "");
    		gmDBManager.setString(5, strTransferReason);
    		gmDBManager.setString(6, strDate);
    		gmDBManager.setString(7, strUserId);
    		gmDBManager.setString(8, "");
    		gmDBManager.setString(9, strRefId_inputstr);
    		gmDBManager.setString(10, strTransferFrom);
    		gmDBManager.setString(11, strTransferTo);
    		gmDBManager.setString(12, strConsignmentId);
    		gmDBManager.execute();

    		// Transfer Id will be returned for the consignment transfer
    		strConsignTransferId = GmCommonClass.parseNull(gmDBManager.getString(13));
    		// Code to save the log information (comments) from the screen
    		if (!strLogReason.equals("")) {
    			gmCommonBean.saveLog(gmDBManager, strConsignTransferId,
    					strLogReason, strUserId, "1209");// 1209 is the code for
    														// Transfer Type in the
    														// log table. 
    		}
    		gmDBManager.commit();
    		log.debug(" strConsignemtTransferID after initiate "+ strConsignTransferId);
    		return strConsignTransferId;

    	}
        
    	/**validateTagID:This method used to validate the tag number
    	 * @param String
    	 * @return
    	 * @throws AppError
    	 */
    	public String validateTagID(String strtagID)throws AppError {
          String strErrMsg = "";

          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setFunctionString("gm_pkg_cs_transfer.gm_pkg_cs_validate_tag",2);
          gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
          gmDBManager.setString(2, strtagID);
          gmDBManager.setString(3, "10004"); //Product Loaner
          gmDBManager.execute();
          strErrMsg = gmDBManager.getString(1);
          gmDBManager.close();
          return strErrMsg;
    	}
}

