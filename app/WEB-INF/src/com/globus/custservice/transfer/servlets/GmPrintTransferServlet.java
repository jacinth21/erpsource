package com.globus.custservice.transfer.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

import com.globus.common.servlets.GmServlet;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonBean;
import com.globus.custservice.transfer.beans.GmTransferBean;

public class GmPrintTransferServlet extends GmServlet
{
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
    }
    
    
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
        HttpSession session = request.getSession(false);
        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strCustTransferPath = GmCommonClass.getString("GMTRANSFER");
        
        String strDispatchTo = strCustTransferPath.concat("/GmPrintTransfer.jsp");
        GmCommonClass gmCommon = new GmCommonClass();
        GmCommonBean gmCommonBean = new GmCommonBean();
        GmTransferBean gmTransferBean = new GmTransferBean();
        
        HashMap hmPrintTransfer = new HashMap();
        HashMap hmAckTransfer = new HashMap();                  
        HashMap hmParam = new HashMap();
        
        String strTransferId = "";
        
        try
        {
            checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
            
            String strAction = request.getParameter("hAction");
            strAction = (strAction == null)?"Load":strAction;
            log.debug(" Action is " +strAction);
            
            strTransferId = GmCommonClass.parseNull((String)request.getParameter("hTransferId"));
            log.debug(" Transfer Id is " + strTransferId);
            
            if (strAction.equals("PrintVersion"))
            {
            hmPrintTransfer = gmTransferBean.printTransfer(strTransferId);
            request.setAttribute("HMPRINTTRANSFER",hmPrintTransfer);
            }
            
            if (strAction.equals("AckTransfer"))
            {
                hmAckTransfer = gmTransferBean.loadTransferHeaderInfo(strTransferId);
                log.debug(" HMACKTRANSFER values are  " + hmAckTransfer);
                request.setAttribute("HMACKTRANSFER",hmAckTransfer);
                strDispatchTo = strCustTransferPath.concat("/GmTransferAckPrint.jsp");
            }
            
            request.setAttribute("ACTION",strAction);
            
            
            log.debug(" Dispatch to is " + strDispatchTo);
            dispatch(strDispatchTo,request,response);
            
        }// End of try
        
               
        catch (Exception e)
        {
            e.printStackTrace();
            session.setAttribute("hAppErrorMessage",e.getMessage());
            strDispatchTo = strComnPath + "/GmError.jsp";
            gotoPage(strDispatchTo,request,response);
        }
        
        // End of catch
    }//End of service method
}// End of Servlet
