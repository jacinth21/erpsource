package com.globus.custservice.transfer.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.transfer.beans.GmTransferBean;
import com.globus.operations.logistics.beans.GmKitMappingBean;

public class GmAcceptTransferServlet extends GmServlet {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
																	// log4j

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	@Override
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		log.debug("Enter");

		HttpSession session = request.getSession(false);
		instantiate(request, response);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMTRANSFER");
		GmTransferBean gmTransferBean = new GmTransferBean(getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		HashMap hmReturn = new HashMap();
		ArrayList alComments = new ArrayList();
		HashMap hmTransferDetail = new HashMap();
		ArrayList alTransferItemDetails = new ArrayList();
		ArrayList alTransferList = new ArrayList();
		String strDispatchTo = "";
		String strTransferType = "";
		String strTransferFrom = "";
		ArrayList alTransferTagDet = new ArrayList();
		String accessFlag = " ";
		String strCompleteFlag = "";
		HashMap hmAccess = new HashMap();

		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(
				getGmDataStoreVO());
		String strStatus = GmCommonClass.parseNull(request
				.getParameter("hStatus"));
		String strBtnStatus = GmCommonClass.parseNull(request
				.getParameter("hBtnStatus"));
		String strComplteTransfer = GmCommonClass.parseNull(request
				.getParameter("Complete"));
		log.debug("strBtnStatus " + strBtnStatus);

		try {
			checkSession(response, session); // Checks if the current session is
												// valid, else redirecting to
												// SessionExpiry page
			strTransferType = GmCommonClass.parseNull(request
					.getParameter("hTransferType"));
			strTransferFrom = GmCommonClass.parseNull(request
					.getParameter("hTransferFrom"));
			String strAction = request.getParameter("hAction");
			String strTransferId = GmCommonClass.parseNull(request
					.getParameter("hTransferId"));

			String strUserId = GmCommonClass.parseNull((String) session
					.getAttribute("strSessUserId"));
			if (strAction == null) {
				strAction = (String) session.getAttribute("hAction");
			}

			/*
			 * if(strTransferId == null) { strTransferId =
			 * GmCommonClass.parseNull
			 * ((String)session.getAttribute("strTransferId")); }
			 */

			log.debug("strTransferType :" + strTransferType);
			log.debug("strTransferId :" + strTransferId);
			strAction = (strAction == null) ? "Load" : strAction;
			log.debug("Str Action :" + strAction);

			if (strAction.equalsIgnoreCase("Save")
					&& !strBtnStatus.equals("disabled")) {
				String strLogReason = GmCommonClass.parseNull(request
						.getParameter("Txt_LogReason"));
				if (strLogReason.equals("")) {
					strLogReason = GmCommonClass.parseNull(request
							.getParameter("hTxt_LogReason"));
				}
				// String strErrMessage =
				// gmTransferBean.saveAcceptTransfer(strTransferId,strLogReason,strUserId,"1209");
				strCompleteFlag = GmCommonClass.parseNull(request
						.getParameter("Chk_Complete"));
				if (strCompleteFlag.equalsIgnoreCase("on")) {
					strCompleteFlag = "Y";
				} else {
					strCompleteFlag = "N";
				}

				String strInputStr = GmCommonClass.parseNull(request
						.getParameter("hinputStr"));
				log.debug("strCompleteFlag" + strCompleteFlag);
				gmTransferBean.saveAcceptTransfer(strTransferId, strUserId,
						strCompleteFlag, strInputStr, strLogReason, "1209");
			}
			//Validate the Tag id in set transfer screen
			if (strAction.equalsIgnoreCase("ValidateTag")
                && !strBtnStatus.equals("disabled")) {
				String strtagID = GmCommonClass.parseNull(request
                  .getParameter("tagId"));
			    String strErrMsg =  gmTransferBean.validateTagID(strtagID);
			    if(strErrMsg.equals("N")){
			      request.setAttribute("hBtnStatus", "disabled");
			    }
			  if (!strErrMsg.equals("")) {
			    response.setContentType("text/xml");
		        PrintWriter pw = response.getWriter();
		        pw.write(strErrMsg);
		        pw.flush();
		        pw.close();
		        strDispatchTo = null;
			  }         
		    }


			// Load the transfer details
			hmReturn = gmTransferBean.loadTransfer(strTransferId);
			hmTransferDetail = (HashMap) hmReturn.get("TRANSFER");

			if(strCompleteFlag.equals("Y")){    //To Update redis key to show add set field (PMT-24650)
				GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
				String strFromDistId = GmCommonClass.parseNull((String) hmTransferDetail.get("FROMNAMEID"));
				String strToDistId = GmCommonClass.parseNull((String) hmTransferDetail.get("TONAMEID"));				
				gmKitMappingBean.removeSetKey(strFromDistId);
				gmKitMappingBean.removeSetKey(strToDistId);
			}    
			// Check if the status is 2 ie if the status is closed
			strStatus = GmCommonClass.parseNull((String) hmTransferDetail
					.get("STATUS"));
			log.debug("strStatus:" + strStatus);
			if (strStatus.equals("2")) {
				request.setAttribute("hBtnStatus", "disabled");
				request.setAttribute("hRevBtnStatus", "true");//PMT-32065--Account Item Consignment Transfer
			}else if(strStatus.equals("3")){
				request.setAttribute("hBtnStatus", "disabled");
			}
			request.setAttribute("hmTransfer", hmTransferDetail);
			/* ------------------------------ */

			// Load the item details for the transfer Dhtmlx report on Approve
			// transfer Screen.
			alTransferItemDetails = (ArrayList) hmReturn
					.get("TRANSFER_ITEM_DETAILS");
			request.setAttribute("ALTRANSFERLIST", alTransferItemDetails);

			log.debug("strTransferFrom :" + strTransferFrom);
			alTransferList = gmTransferBean.fetchTransferDetail(strTransferId);
			String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
			GmTemplateUtil templateUtil = new GmTemplateUtil();
			templateUtil.setDataList("alResult", alTransferList);
			templateUtil.setDataMap("hmApplnParam", hmTransferDetail);
			templateUtil.setTemplateName("GmIncludeFullTransfer.vm");
			templateUtil.setTemplateSubDir("custservice/templates");
			templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	                "properties.labels.custservice.transfer.GmFullTransfer",
	                strSessCompanyLocale));
			request.setAttribute("alResult", templateUtil.generateOutput());
			/* ------------------------- */

			// Load the log for the transfer
			alComments = gmCommonBean.getLog(strTransferId);
			request.setAttribute("hmLog", alComments);
			request.setAttribute("hAction", "Load");
			/* ------------------------- */

			alTransferTagDet = gmTransferBean.fetchTranferTagDetail(
					strTransferId, strTransferType);
			log.debug("size   -->>>" + alTransferTagDet.size());
			request.setAttribute("alTransferTagDet", alTransferTagDet);

			hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean
					.getAccessPermissions(strUserId, "3250006"));
			log.debug("Access  *" + hmAccess);
			accessFlag = GmCommonClass
					.parseNull((String) hmAccess.get("UPDFL"));
			request.setAttribute("accessFlag", accessFlag);

			log.debug("Exit Load Logic");

			dispatch(strOperPath.concat("/GmApproveTransfer.jsp"), request,
					response);
		}// End of try
		catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strDispatchTo = strComnPath + "/GmError.jsp";
			gotoPage(strDispatchTo, request, response);
		}// End of catch

	}// End of service method
}// End of GmAcceptTransferServlet