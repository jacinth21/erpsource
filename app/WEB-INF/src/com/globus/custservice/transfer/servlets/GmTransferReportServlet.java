package com.globus.custservice.transfer.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.transfer.beans.GmTransferBean;

public class GmTransferReportServlet extends GmServlet {
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }


  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustTransferPath = GmCommonClass.getString("GMTRANSFER");

    String strDispatchTo = strCustTransferPath.concat("/GmTransferReport.jsp");
    GmCommonClass gmCommon = new GmCommonClass();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmTransferBean gmTransferBean = new GmTransferBean(getGmDataStoreVO());
    GmCalenderOperations gmCal = new GmCalenderOperations();
    ArrayList alTransferReason = new ArrayList();
    ArrayList alTransferReasonAC = new ArrayList();
    ArrayList alTransferType = new ArrayList();
    ArrayList alDistributorList = new ArrayList();
    ArrayList alEmployeeList = new ArrayList();
    ArrayList alAccountList =  new ArrayList();
    RowSetDynaClass rowSetTransferReport = null;

    HashMap hmParam = new HashMap();

    String strTransferType = "";
    String strTransferFrom = "";
    String strTransferTo = "";
    String strTransferReason = "";
    String strMonthBeginDate = "";
    String strTransferFromNm = "";
    String strTransferToNm="";
    String  strTransferfm="";
    String  strTransferto="";

    //String strTodaysDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    //strMonthBeginDate = strTodaysDate.replaceAll("/\\d\\d/", "/01/");
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    int currMonth = 0;
    currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    strMonthBeginDate =
            GmCommonClass.parseNull(GmCalenderOperations.getAnyDateOfMonth(currMonth, 1,
                strApplnDateFmt));
        String strTodaysDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      log.debug(" Action is " + strAction);

      strAction = (strAction == null) ? "Load" : strAction;
      log.debug(" Action is " + strAction);

     strTransferType = GmCommonClass.parseNull(request.getParameter("Cbo_TransferType"));
     strTransferReason = GmCommonClass.parseNull(request.getParameter("hTransferReason"));
     //For Autocomplete changes
     strTransferfm = GmCommonClass.parseNull(request.getParameter("searchCbo_TransferFrom"));
     strTransferFrom = GmCommonClass.parseNull(request.getParameter("Cbo_TransferFrom")); 
     strTransferto = GmCommonClass.parseNull(request.getParameter("searchCbo_TransferTo"));
     strTransferTo = GmCommonClass.parseNull(request.getParameter("Cbo_TransferTo")); 
      String strFromDate =
          request.getParameter("Txt_FromDate") == null ? strMonthBeginDate : request
              .getParameter("Txt_FromDate");
      String strToDate =
          request.getParameter("Txt_ToDate") == null ? strTodaysDate : request
              .getParameter("Txt_ToDate");
      String strStatus = GmCommonClass.parseNull(request.getParameter("Cbo_Status"));
      String strCNID = GmCommonClass.parseNull(request.getParameter("Cbo_CNID"));
      String strMissingDist = GmCommonClass.parseNull((String)gmCommon.getRuleValue("DISTRIBUTOR","TAGST"));
      
      hmParam.put("TRANSFERTYPE", strTransferType);
      hmParam.put("TRANSFERFROM", strTransferFrom);
      hmParam.put("TRANSFERTO", strTransferTo);
      hmParam.put("TRANSFERREASON", strTransferReason);
      hmParam.put("FROMDATE", strFromDate);
      hmParam.put("TODATE", strToDate);
      hmParam.put("STATUS", strStatus);
      hmParam.put("CNID", strCNID);
     
      //Auto complete changes
      hmParam.put("TRANSFERFM", strTransferfm);
      hmParam.put("TRANSFERTOAC", strTransferto);
 

      
		String strAccessFilter =  getAccessFilter(request,response);	
		//To get the Access Level information 
		String strCondition =  getAccessCondition(request,response);
		// To get the Account information for the logged in user
		alAccountList  = gmCommonBean.getAccountList(strCondition);
      
      alDistributorList = gmCommonBean.getDistributorList();
      alEmployeeList = gmCommonBean.getEmployeeList();
      alTransferReason = gmCommon.getCodeList("TSFRN");// ACCTSF TSFRN
      alTransferType = gmCommon.getCodeList("TSFTP"); 
      alTransferReasonAC = gmCommon.getCodeList("ACCTSF");

      if (strAction.equals("reportTransfer")) {
        String strJSONString  = gmTransferBean.fetchTransferReport(hmParam);
        
     log.debug("strJSONString=="+strJSONString);
        response.setContentType("text/xml");
        PrintWriter pw = response.getWriter();
        pw.write(strJSONString);
        pw.flush();
        pw.close();
        strDispatchTo = null;
      }

      request.setAttribute("ACTION", strAction);
      request.setAttribute("STRFROMDATE", strFromDate);
      request.setAttribute("STRTODATE", strToDate);
      request.setAttribute("ALTSFTP", alTransferType);
      request.setAttribute("ALTSFRN", alTransferReason);
      request.setAttribute("ALTSFRNAC", alTransferReasonAC);
      request.setAttribute("ALDISTLIST", alDistributorList);
      request.setAttribute("ALEMPLIST", alEmployeeList);
      request.setAttribute("ALACCLIST", alAccountList);
      request.setAttribute("MISSINGDIST", strMissingDist);
      
      log.debug(" values inside Param " + hmParam);
      request.setAttribute("HMPARAM", hmParam);
      //request.setAttribute("RSTRANSFERREPORT", rowSetTransferReport);

      log.debug(" Dispatch to is " + strDispatchTo);
      dispatch(strDispatchTo, request, response);

    }// End of try


    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }

    // End of catch
  }// End of service method
}// End of Servlet
