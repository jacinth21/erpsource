package com.globus.custservice.transfer.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.transfer.beans.GmTransferBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmInitiateTransferServlet extends GmServlet {


  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }


  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strCustTransferPath = GmCommonClass.getString("GMTRANSFER");
    String strServletPath = GmCommonClass.getString("GMSERVLETPATH");

    String strDispatchTo = strCustTransferPath.concat("/GmFullTransfer.jsp");
    GmCommonClass gmCommon = new GmCommonClass();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmTransferBean gmTransferBean = new GmTransferBean(getGmDataStoreVO());

    RowSetDynaClass resultSet = null;
    ArrayList alTsfReasonDtoD = new ArrayList();
    ArrayList alTsfReasonItoD = new ArrayList();
    ArrayList alTransferType = new ArrayList();
    ArrayList alDistributorList = new ArrayList();
    ArrayList alEmployeeList = new ArrayList();
    ArrayList alComments = new ArrayList();
    //ArrayList alSet = new ArrayList();
    ArrayList alTransferList = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();

    String strLogReason = "";
    String strTransferId = "";

    String strTransferType = "";
    String strTransferFrom = "";
    String strTransferTo = "";
    String strTransferReason = "";
    String strDate = "";
    String strTransferMode = "";
    String strRefId_inputstr = "";
    String strSetId = "";
    String strPartNums = "";
    String strTransferID = "";
    String strMissingDist = "";
    String strSetIdName="";
    
    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      GmCalenderOperations gmCal = new GmCalenderOperations();
      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strTodaysDate = gmCal.getCurrentDate(strApplnDateFmt);


      String strAction = request.getParameter("hAction");
      log.debug(" Action is " + strAction);

      strAction = (strAction == null) ? "Load" : strAction;
      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      log.debug(" Action is " + strAction + " | Opt is " + strOpt);


      strTransferType = GmCommonClass.parseNull(request.getParameter("Cbo_TransferType"));
      strTransferFrom = GmCommonClass.parseNull(request.getParameter("hTransferFrom"));
      strTransferTo = GmCommonClass.parseNull(request.getParameter("Cbo_TransferTo"));
      strTransferReason = GmCommonClass.parseNull(request.getParameter("Cbo_TransferReason"));
      strDate =
          request.getParameter("Txt_Date") == null ? strTodaysDate : request
              .getParameter("Txt_Date");

      hmParam.put("TRANSFERTYPE", strTransferType);
      hmParam.put("TRANSFERFROM", strTransferFrom);
      hmParam.put("TRANSFERTO", strTransferTo);
      hmParam.put("TRANSFERREASON", strTransferReason);
      hmParam.put("DATE", strDate);

      // called from left Menu and during load in Full transfer to get the details for the drop down

      request.setAttribute("hAction", strOpt);
      // gmCommonBean.loadDistributorList(); -- This is called from CommonInitServlet. So commenting
      // here. If you perform a build, reload the property files from
      // gmCommonBean.loadEmployeeList();
      alDistributorList = gmCommonBean.getDistributorList();
      alEmployeeList = gmCommonBean.getEmployeeList();
      alTsfReasonDtoD = gmCommon.getCodeList("TSFRN");
      alTsfReasonItoD = gmCommon.getCodeList("TFRNI");
      alTransferType = gmCommon.getCodeList("TSFTP");
      strMissingDist = GmCommonClass.parseNull((String)gmCommon.getRuleValue("DISTRIBUTOR","TAGST"));
      
      log.debug(" Param values is " + hmParam);
      log.debug(" transfer type is " + strTransferType + " transfer ID is " + strTransferFrom);

      if (strOpt.equals("ViewSetTransfer")) {
    	  hmValues.put("TYPE", "1601");// 1601 is for current set list
    	  hmValues.put("SETTYPE", "4070");//PMT-13143 should load only 4070 - consignment sets
          /*alSet = gmProjectBean.loadSetMap(hmValues); 
        request.setAttribute("ALSET", alSet);*/
        strDispatchTo = strCustTransferPath.concat("/GmSetTransfer.jsp");
      }

      if (strOpt.equals("ViewPartTransfer")) {
        strDispatchTo = strCustTransferPath.concat("/GmPartTransfer.jsp");
      }

      // called when load button is clicked from full transfer
      if (strAction.equals("loadFullTransferInfo")) {
        // fetches the CN and RA info of the "From" distributor
        alTransferList = gmTransferBean.loadFullTransferInfo(strTransferType, strTransferFrom);
        String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
        GmTemplateUtil templateUtil = new GmTemplateUtil();
        templateUtil.setDataList("alResult", alTransferList);
        templateUtil.setTemplateName("GmIncludeFullTransfer.vm");
        templateUtil.setTemplateSubDir("/custservice/templates");
        
        templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
                "properties.labels.custservice.transfer.GmFullTransfer",
                strSessCompanyLocale));
        request.setAttribute("alResult", templateUtil.generateOutput());
        request.setAttribute("hAction", "LoadFullTransfer");
        strDispatchTo = strCustTransferPath.concat("/GmFullTransfer.jsp");
      }

      // called when load button is clicked from full transfer
      if (strAction.equals("loadSetTransferInfo")) {
        strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_Set"));
        strSetIdName = GmCommonClass.parseNull(request.getParameter("searchCbo_Set"));  

        hmParam.put("SETID", strSetId);
        hmParam.put("SETNAME", strSetIdName);
       

        // fetches the set related info for that D or InH
        alTransferList =
            gmTransferBean.loadSetTransferInfo(strTransferType, strTransferFrom, strSetId);

        request.setAttribute("hAction", "LoadSetTransfer");
        strDispatchTo = strCustTransferPath.concat("/GmSetTransfer.jsp");
      }

      // called when load button is clicked from full transfer
      if (strAction.equals("loadPartTransferInfo")) {
        strPartNums = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
        log.debug(" Part Nums input is " + strPartNums);
        hmParam.put("PARTNUMS", strPartNums);

        // fetches the part related info for that D or InH
        alTransferList =
            gmTransferBean.loadPartTransferInfo(strTransferType, strTransferFrom, strPartNums);

        request.setAttribute("hAction", "LoadPartTransfer");
        strDispatchTo = strCustTransferPath.concat("/GmPartTransfer.jsp");
      }

      if (strAction.equals("saveTransfer")) {

        /*
         * 90350 is Full Transfer. Grp is TSMOD 90351 is Set Transfer 90352 is Part Transfer
         */

        String strUserId = (String) session.getAttribute("strSessUserId");
        strRefId_inputstr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        strTransferMode = GmCommonClass.parseNull(request.getParameter("hTsfMode"));
        strLogReason = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        log.debug("StrLogReason is " + strLogReason + "Order Id is " + strTransferId);
        strSetId = GmCommonClass.parseNull(request.getParameter("Cbo_Set"));

        hmParam.put("INPUTSTR", strRefId_inputstr);
        hmParam.put("TRANSFERMODE", strTransferMode);
        hmParam.put("USERID", strUserId);
        hmParam.put("LOGVAL", strLogReason);
        hmParam.put("SET", strSetId);

        log.debug(" Param values initiateFullTransfer " + hmParam);
        log.debug(" transfer type is " + strTransferType + " transfer ID is " + strTransferFrom);

        strTransferID = gmTransferBean.saveTransfer(hmParam);
        String strAdress =
            "/GmAcceptTransferServlet?hAction=Load&hTransferId=" + strTransferID + "";
        gotoPage(strAdress, request, response);

      }
      // To show transfer type Missing to Distributor on set and part transfer (PMT-35524)
      if (strOpt.equals("ViewFullTransfer")) {
          alTransferType = GmCommonClass.parseNullArrayList((ArrayList) gmCommon.getCodeList("TSFTP","90300",getGmDataStoreVO()));
      }else{
          alTransferType = GmCommonClass.parseNullArrayList((ArrayList) gmCommon.getCodeList("TSFTP","26240616",getGmDataStoreVO()));
      }

      request.setAttribute("STRDATE", strTodaysDate);
      request.setAttribute("ALTSFTP", alTransferType);
      request.setAttribute("ALTSFRNDtoD", alTsfReasonDtoD);
      request.setAttribute("ALTSFRNItoD", alTsfReasonItoD);
      request.setAttribute("ALDISTLIST", alDistributorList);
      request.setAttribute("ALEMPLIST", alEmployeeList);
      request.setAttribute("MISSINGDIST",strMissingDist);
      // adding this dummy to remove NullPointerException on GmIncludeLog.jsp
      request.setAttribute("hmLog", alComments);

      request.setAttribute("HMPARAM", hmParam);
      request.setAttribute("ALTRANSFERLIST", alTransferList);



      log.debug(" Dispatch to is " + strDispatchTo);
      dispatch(strDispatchTo, request, response);

    }// End of try

    catch (AppError e) {

      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      log.debug(" Dispatch to is .............." + e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      dispatch(strDispatchTo, request, response);
    }


    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      log.debug(" Dispatch to is **********************" + e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }

    // End of catch
  }// End of service method
}// End of Servlet
