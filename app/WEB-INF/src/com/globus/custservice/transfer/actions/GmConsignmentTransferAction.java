package com.globus.custservice.transfer.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonBean;
import com.globus.custservice.transfer.forms.GmConsignmentTransferForm;
import com.globus.custservice.transfer.beans.GmConsignmentTransferBean;
import com.globus.custservice.transfer.beans.GmTransferBean;
/**
 * GmConsignmentTransferAction This class is used to perform the account consignment transfer 
 * Methods: 
 *  1. loadAccountConsignmentScreen
 *  2. loadConsignmentDtls
 *  3. validateConsignment
 *  4. saveConsignmentTransfer
 *  5. reverseConsignmentTransfer
 */
public class GmConsignmentTransferAction extends GmDispatchAction {
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	 
	 /**loadAccountConsignmentScreen -- 
	 * @param actionMapping,form,request,response
	 * @return action forwarding
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadAccountConsignmentScreen(ActionMapping actionMapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws AppError, Exception {
		instantiate(request, response);
		GmConsignmentTransferForm gmConsignmentTransferForm = (GmConsignmentTransferForm) form;
		gmConsignmentTransferForm.loadSessionParameters(request);
		GmConsignmentTransferBean gmConsignmentTransferBean = new GmConsignmentTransferBean(getGmDataStoreVO());
		GmCalenderOperations gmCal = new GmCalenderOperations();
		// Initialize
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmConsignmentTransferForm);

		// get Code Group Value
		gmConsignmentTransferForm.setAlMode(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ACCTSF",getGmDataStoreVO())));
		gmConsignmentTransferForm.setAlType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TSFTP",getGmDataStoreVO())));
		String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
		String strTodaysDate = GmCommonClass.parseNull(gmCal.getCurrentDate(strApplnDateFmt));
		gmConsignmentTransferForm.setDate(strTodaysDate);
		return actionMapping.findForward("GmConsignmentTransfer");

	}
	 
	 /**loadConsignmentDtls:This method used to load consignment details information when click load button
	 * @param actionMapping,form,request,response 
	 * @return action forwarding
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadConsignmentDtls(ActionMapping actionMapping, ActionForm form,
 HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		GmConsignmentTransferForm gmConsignmentTransferForm = (GmConsignmentTransferForm) form;
		gmConsignmentTransferForm.loadSessionParameters(request);
		GmConsignmentTransferBean gmConsignmentTransferBean = new GmConsignmentTransferBean(getGmDataStoreVO());
		GmCalenderOperations gmCal = new GmCalenderOperations();
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		// Initialize
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		ArrayList alLoadDtls = new ArrayList();
		ArrayList alLogReason = new ArrayList();
		String strxmlGridData = "";

		hmParam = GmCommonClass.getHashMapFromForm(gmConsignmentTransferForm);
		String strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));

		gmConsignmentTransferForm.setAlMode(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ACCTSF",getGmDataStoreVO())));
		gmConsignmentTransferForm.setAlType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TSFTP",getGmDataStoreVO())));
		String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
		String strTodaysDate = GmCommonClass.parseNull(gmCal.getCurrentDate(strApplnDateFmt));
		gmConsignmentTransferForm.setDate(strTodaysDate);
		
    	alLoadDtls = GmCommonClass.parseNullArrayList(gmConsignmentTransferBean.loadConsignmentDtls(hmParam));

		hmReturn.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
		hmReturn.put("TEMPLATE_NM", "GmConsignmentTransfer.vm");
		hmReturn.put("LABEL_PROPERTIES_NM","properties.labels.custservice.transfer.GmConsignmentTransfer");
		
		// based on array list to get the Grid xml string
		strxmlGridData = gmConsignmentTransferBean.generateOutput(alLoadDtls,hmReturn);
		gmConsignmentTransferForm.setStrxmlGridData(strxmlGridData);
		response.setContentType("text/xml");
		response.setCharacterEncoding("UTF-8");
		PrintWriter pw = response.getWriter();
		response.setHeader("Cache-Control", "no-cache");
	    if (!strxmlGridData.equals("")) {
	        pw.write(strxmlGridData);
	      }
	      pw.flush();
	      // pw.close();
	      return null;
}
	 
		/**validateConsignment:This method used to validateConsignment when tabout in consignment Id field in JSP
		 * @param actionMapping,form,request,response 
		 * @return action forwarding
		 * @throws AppError
		 * @throws ServletException
		 * @throws IOException
		 * @throws ParseException
		 */
		public  ActionForward  validateConsignment(ActionMapping actionMapping, ActionForm form,
			      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
			IOException, ParseException {
		// call the instantiate method
		instantiate(request, response);
		GmConsignmentTransferForm gmConsignmentTransferForm = (GmConsignmentTransferForm) form;
		gmConsignmentTransferForm.loadSessionParameters(request);
		GmConsignmentTransferBean gmConsignmentTransferBean = new GmConsignmentTransferBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();

		hmParam = GmCommonClass.getHashMapFromForm(gmConsignmentTransferForm);

		String strJsonString = GmCommonClass.parseNull(gmConsignmentTransferBean.validateConsignment(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
	}
		
/**saveConsignmentTransfer:This method used to saveConsignmentTransfer when click Initiate button in JSP
 * @param actionMapping,form,request,response 
 * @return action forwarding
 * @throws AppError
 * @throws Exception
 */
public ActionForward saveConsignmentTransfer(ActionMapping actionMapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response)
				throws AppError, Exception {
	instantiate(request, response);
	GmConsignmentTransferForm gmConsignmentTransferForm = (GmConsignmentTransferForm) form;
	gmConsignmentTransferForm.loadSessionParameters(request);
	GmTransferBean gmTransferBean = new GmTransferBean(getGmDataStoreVO());

	 String strTransferID = "";
	 String strLogVal="";
	 HashMap hmParam = new HashMap();
	 hmParam = GmCommonClass.getHashMapFromForm(gmConsignmentTransferForm);
	 strLogVal = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));	
	 hmParam.put("LOGVAL",strLogVal);
	
	 strTransferID = gmTransferBean.saveConsignmentTransfer(hmParam);
	 String strForwardPage =  "/GmAcceptTransferServlet?hAction=Load&hTransferId=" + strTransferID;
	 return actionRedirect(strForwardPage, request);
 }

/**reverseConsignmentTransfer:This Method used to reverse the consignment item.
* @param actionMapping,form,request,response 
* @return action forwarding
 * @throws AppError
 * @throws Exception
 */
public ActionForward reverseConsignmentTransfer(ActionMapping actionMapping, ActionForm form,HttpServletRequest request,HttpServletResponse response) 
			throws AppError, Exception {
		instantiate(request, response);
		GmConsignmentTransferForm gmConsignmentTransferForm = (GmConsignmentTransferForm) form;
		gmConsignmentTransferForm.loadSessionParameters(request);
		GmConsignmentTransferBean gmConsignmentTransferBean = new GmConsignmentTransferBean(
				getGmDataStoreVO());
		// Initialize
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmConsignmentTransferForm);
		String strMessage = GmCommonClass.parseNull((String)gmConsignmentTransferBean.reverseConsignmentTransfer(hmParam));
		if (!strMessage.equals("")) {
			request.setAttribute("hType", "S");
			throw new AppError("<b>"+strMessage+"</b> is reversed successfully", "S", 'S');
		}
		return null;

	}		
		
}
