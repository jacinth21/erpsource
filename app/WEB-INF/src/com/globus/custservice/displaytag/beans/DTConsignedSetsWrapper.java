package com.globus.custservice.displaytag.beans;

/**
 * FileName    : DTConsignedSetsWrapper.java 
 * Description :
 * Author      : rshah
 * Date        : Apr 9, 2009 
 * Copyright   : Globus Medical Inc
 */

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TotalTableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author rshah
 *
 */
public class DTConsignedSetsWrapper extends TotalTableDecorator{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	private HashMap hmParam = new HashMap();
	private DynaBean db;	
	private String strDname = "";
	private String strSname ="";
	private String strID = "";
	private String strDistId = "";
	private String strDID = "";
	
	
	public void getValue()
	{
		db = 	(DynaBean) this.getCurrentRowObject();
    	strDname = GmCommonClass.parseNull((String)db.get("DNAME"));
		strSname = GmCommonClass.parseNull((String)db.get("SNAME"));
		strID = GmCommonClass.parseNull((String)db.get("ID"));
		strDistId = db.get("DID").toString();
		strDID = (strDistId.equals("0"))?"-":strDistId;		
	}
	public String getDNAME1()
    {    
		StringBuffer strBufVal = new StringBuffer();
		String strRes = "";	
		getValue();
		strBufVal.append("<a href=\"javascript:fnDrillDown('"+strDID+"','"+strID+"','"+strSname+"');\">"+strDname+"</a>");		
   		strRes = strBufVal.toString(); 
   		return strRes;
    }
	
	public String getSNAME1()
    {    	
		StringBuffer strBufVal = new StringBuffer();
		String strRes = "";
		getValue();
		strBufVal.append("<a href=\"javascript:fnDrillDown('"+strDID+"','"+strID+"','"+strDname+"');\">"+strSname+"</a>");		
   		strRes = strBufVal.toString(); 
   		return strRes;
    }
	
	
}
