package com.globus.custservice.displaytag.beans;

import java.util.Date;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * FileName : DTDODashWrapper.java Description : Author : DhanaReddy
 */
public class DTDODashWrapper extends TableDecorator {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  private DynaBean db = null;
  private String strOrderid = "";
  private String strCaseid = "";
  private String strTotal = "";
  private String strAccCurrSymb = "";
  private String strrequestedDate = "";
  private String strUpdateDate = "";
  Date UpdateDate = null;



  public String getCASEID() {

    db = (DynaBean) this.getCurrentRowObject();
    strOrderid = String.valueOf(db.get("ORDER_ID"));
    strCaseid = String.valueOf(db.get("CASEID"));
    strCaseid = (strCaseid.equals("null")) ? "" : strCaseid;

    StringBuffer sbHtml = new StringBuffer();
    sbHtml
        .append(" <a href=javascript:fnFetchDOSummary('"
            + strOrderid
            + "');><img width='20' height='13' title='Click here to load Summary' style='cursor:hand' src='/images/s.gif'/> </a>");
    // sbHtml.append(" <a href=# onClick=fnEditPO('"+strOrderid+"');><img width='20' height='13' title='Click here to Edit PO' style='cursor:hand' src='/images/consignment.gif'/></a> ");
    sbHtml.append(strCaseid);
    return sbHtml.toString();

  }

  public String getTOTAL_COST_FMT() {
    db = (DynaBean) this.getCurrentRowObject();
    strTotal = String.valueOf(db.get("TOTAL_COST"));
    strAccCurrSymb = String.valueOf(db.get("ACC_CURRENCY"));

    return strAccCurrSymb + " " + GmCommonClass.getStringWithCommas(strTotal);
  }

  public String getREQUESTED_DATE_FMT() throws AppError, Exception {
    db = (DynaBean) this.getCurrentRowObject();
    GmDataStoreVO gmDataStoreVO =
        (GmDataStoreVO) getPageContext().getRequest().getAttribute("gmDataStoreVO");
    gmDataStoreVO =
        ((gmDataStoreVO == null) || ((gmDataStoreVO.getCmpdfmt()).equals(""))) ? GmCommonClass
            .getDefaultGmDataStoreVO() : gmDataStoreVO;

    String strApplDateFmt = gmDataStoreVO.getCmpdfmt();
    String strDateFormat = "MM/dd/yyyy HH:mm:ss";
    strrequestedDate = String.valueOf(db.get("REQUESTED_DATE"));
    UpdateDate = GmCommonClass.getStringToDate(strrequestedDate, strDateFormat);
    strUpdateDate = GmCommonClass.getStringFromDate(UpdateDate, strApplDateFmt);
    return strUpdateDate;

  }
}
