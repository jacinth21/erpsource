package com.globus.clinical.querysystem.forms;

import java.util.ArrayList;

import com.globus.party.forms.GmPartySetupForm;

public class GmSiteMapingForm extends GmPartySetupForm {

	private String studyListId = "";
	private String siteListId = "";
	private String primaryFlag = "on";
	private String strOpt = null;
	private String sitePartyId = "";

	private ArrayList alStudyList = new ArrayList();
	private ArrayList alSiteNameList = new ArrayList();

	/**
	 * @return the alSiteNameList
	 */
	public ArrayList getAlSiteNameList() {
		return alSiteNameList;
	}

	/**
	 * @param alSiteNameList
	 *            the alSiteNameList to set
	 */
	public void setAlSiteNameList(ArrayList alSiteNameList) {
		this.alSiteNameList = alSiteNameList;
	}

	/**
	 * @return the alStudyList
	 */
	public ArrayList getAlStudyList() {
		return alStudyList;
	}

	/**
	 * @param alStudyList
	 *            the alStudyList to set
	 */
	public void setAlStudyList(ArrayList alStudyList) {
		this.alStudyList = alStudyList;
	}

	/**
	 * @return the studyListID
	 */
	public String getStudyListId() {
		return studyListId;
	}

	/**
	 * @param studyListID
	 *            the studyListID to set
	 */
	public void setStudyListId(String studyListId) {
		this.studyListId = studyListId;
	}

	/**
	 * @return the siteListID
	 */
	public String getSiteListId() {
		return siteListId;
	}

	/**
	 * @param siteListID
	 *            the siteListID to set
	 */
	public void setSiteListId(String siteListId) {
		this.siteListId = siteListId;
	}

	/**
	 * @return the primaryFlag
	 */
	public String getPrimaryFlag() {
		return primaryFlag;
	}

	/**
	 * @param primaryFlag
	 *            the primaryFlag to set
	 */
	public void setPrimaryFlag(String primaryFlag) {
		this.primaryFlag = primaryFlag;
	}

	/**
	 * @return the strOpt
	 */
	public String getStrOpt() {
		return strOpt;
	}

	/**
	 * @param strOpt
	 *            the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}

	/**
	 * @return the sitePartyId
	 */
	public String getSitePartyId() {
		return sitePartyId;
	}

	/**
	 * @param sitePartyId
	 *            the sitePartyId to set
	 */
	public void setSitePartyId(String sitePartyId) {
		this.sitePartyId = sitePartyId;
	}

}
