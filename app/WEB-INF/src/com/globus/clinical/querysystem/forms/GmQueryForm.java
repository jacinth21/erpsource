package com.globus.clinical.querysystem.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.clinical.forms.GmStudyFilterForm;

public class GmQueryForm extends GmStudyFilterForm{
	private String queryId;
	private String patientListId;
	private String patientAnsListId;
	private String partyType;
	private String queryText;
	private String status = "";
	private String queryLevelId;
	private String comments = "";
	private String dataSaved = "";
	private String EMPTY_STRING = "";
	private String gridXmlData = EMPTY_STRING;
	private HashMap hmResult;
	private ArrayList alStatus = new ArrayList();
	private String listQueries="";
	private String fromContainer = "";
	
	/**
	 * @return the listQueries
	 */
	public String getListQueries() {
		return listQueries;
	}
	/**
	 * @param listQueries the listQueries to set
	 */
	public void setListQueries(String listQueries) {
		this.listQueries = listQueries;
	}
	/**
	 * @return the partyType
	 */
	public String getPartyType() {
		return partyType;
	}
	/**
	 * @param partyType the partyType to set
	 */
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}
	/**
	 * @return the queryText
	 */
	public String getQueryText() {
		return queryText;
	}
	/**
	 * @param queryText the queryText to set
	 */
	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the hmResult
	 */
	public HashMap getHmResult() {
		return hmResult;
	}
	/**
	 * @param hmResult the hmResult to set
	 */
	public void setHmResult(HashMap hmResult) {
		this.hmResult = hmResult;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the queryId
	 */
	public String getQueryId() {
		return queryId;
	}
	/**
	 * @param queryId the queryId to set
	 */
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	/**
	 * @return the patientListId
	 */
	public String getPatientListId() {
		return patientListId;
	}
	/**
	 * @param patientListId the patientListId to set
	 */
	public void setPatientListId(String patientListId) {
		this.patientListId = patientListId;
	}
	/**
	 * @return the patientAnsListId
	 */
	public String getPatientAnsListId() {
		return patientAnsListId;
	}
	/**
	 * @param patientAnsListId the patientAnsListId to set
	 */
	public void setPatientAnsListId(String patientAnsListId) {
		this.patientAnsListId = patientAnsListId;
	}
	/**
	 * @return the queryLevelId
	 */
	public String getQueryLevelId() {
		return queryLevelId;
	}
	/**
	 * @param queryLevelId the queryLevelId to set
	 */
	public void setQueryLevelId(String queryLevelId) {
		this.queryLevelId = queryLevelId;
	}
	/**
	 * @return the dataSaved
	 */
	public String getDataSaved() {
		return dataSaved;
	}
	/**
	 * @param dataSaved the dataSaved to set
	 */
	public void setDataSaved(String dataSaved) {
		this.dataSaved = dataSaved;
	}
	/**
	 * @return the fromContainer
	 */
	public String getFromContainer() {
		return fromContainer;
	}
	/**
	 * @param fromContainer the fromContainer to set
	 */
	public void setFromContainer(String fromContainer) {
		this.fromContainer = fromContainer;
	}
}
