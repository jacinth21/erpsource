package com.globus.clinical.querysystem.forms;

import java.util.HashMap;

import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.forms.GmCommonForm;

public class GmQueryReportForm extends  GmStudyFilterForm {
	
	 private HashMap QueryReportDetailsMap = new HashMap();
	 private String EMPTY_STRING = "";
	 private String gridXmlData = EMPTY_STRING;
	 private String msg = "";
	 
	 private String type;
	 private String strstudy_id;
	 private String strsite_id;
	 private String strreportid;
	 
	 private String patientId;
	 private String PATIENT_IDE_NO;
	 private String STUDY_PERIOD;
	 private String FORM_DS;
	 private String QUERY_ID;
	 private String ISSUE_DATE;
	 private String RESOLVED_DATE;
	 private String OUTSTANDING_DAYS;
	 private String patientIDE = "";
	 
	 
	 
	 public String getPatientIDE() {
		return patientIDE;
	}

	public void setPatientIDE(String patientIDE) {
		this.patientIDE = patientIDE;
	}

	/**
	     * @return Returns the patientId.
	     */
	    public String getPatientId()
	    {
	        return patientId;
	    }

	    /**
	     * @param msg
	     *            The patientId to set.
	     */
	    public void setPatientId(String patientId)
	    {
	        this.patientId = patientId;
	    }
	 	 
	 
	 /**
	     * @return Returns the strreportid.
	     */
	    public String getStrreportid()
	    {
	        return strreportid;
	    }

	    /**
	     * @param msg
	     *            The strreportid to set.
	     */
	    public void setStrreportid(String strreportid)
	    {
	        this.strreportid = strreportid;
	    }
	 	 
	 
	 /**
	     * @return Returns the strsite_id.
	     */
	    public String getStrsite_id()
	    {
	        return strsite_id;
	    }

	    /**
	     * @param msg
	     *            The strsite_id to set.
	     */
	    public void setStrsite_id(String strsite_id)
	    {
	        this.strsite_id = strsite_id;
	    }
	 

	 /**
	     * @return Returns the strstudy_id.
	     */
	    public String getStrstudy_id()
	    {
	        return strstudy_id;
	    }

	    /**
	     * @param msg
	     *            The strstudy_id to set.
	     */
	    public void setStrstudy_id(String strstudy_id)
	    {
	        this.strstudy_id = strstudy_id;
	    }
	 
	 /**
	     * @return Returns the type.
	     */
	    public String getType()
	    {
	        return type;
	    }

	    /**
	     * @param msg
	     *            The msg to set.
	     */
	    public void setType(String type)
	    {
	        this.type = type;
	    }
	
	 
	 private HashMap hmCrossTabReport = new HashMap();
	   
	    /**
	     * @return Returns the hmCrossTabReport.
	     */
	    public HashMap getHmCrossTabReport()
	    {
	        return hmCrossTabReport;
	    }
	    /**
	     * @param hmCrossTabReport The hmCrossTabReport to set.
	     */
	    public void setHmCrossTabReport(HashMap hmCrossTabReport)
	    {
	        this.hmCrossTabReport = hmCrossTabReport;
	    }
	 
	 /**
	     * @return Returns the msg.
	     */
	    public String getMsg()
	    {
	        return msg;
	    }

	    /**
	     * @param msg
	     *            The msg to set.
	     */
	    public void setMsg(String msg)
	    {
	        this.msg = msg;
	    }
	
	 
	 /**
		 * @return the growthDetailsMap
		 */
		public HashMap getQueryReportDetailsMap() {
			return QueryReportDetailsMap;
		}

		/**
		 * @param QueryDetailsMap the QueryDetailsMap to set
		 */
		public void setQueryDetailsMap(HashMap queryReportDetailsMap) {
			QueryReportDetailsMap = queryReportDetailsMap;
		}
		
		
		/**
		 * @return the gridXmlData
		 */
		public String getGridXmlData() {
			return gridXmlData;
		}

		/**
		 * @param gridXmlData the gridXmlData to set
		 */
		public void setGridXmlData(String gridXmlData) {
			this.gridXmlData = gridXmlData;
		}

		/**
		 * @return the pATIENT_IDE_NO
		 */
		public String getPATIENT_IDE_NO() {
			return PATIENT_IDE_NO;
		}

		/**
		 * @param patient_ide_no the pATIENT_IDE_NO to set
		 */
		public void setPATIENT_IDE_NO(String patient_ide_no) {
			PATIENT_IDE_NO = patient_ide_no;
		}

		/**
		 * @return the sTUDY_PERIOD
		 */
		public String getSTUDY_PERIOD() {
			return STUDY_PERIOD;
		}

		/**
		 * @param study_period the sTUDY_PERIOD to set
		 */
		public void setSTUDY_PERIOD(String study_period) {
			STUDY_PERIOD = study_period;
		}

		/**
		 * @return the qUERY_ID
		 */
		public String getQUERY_ID() {
			return QUERY_ID;
		}

		/**
		 * @param query_id the qUERY_ID to set
		 */
		public void setQUERY_ID(String query_id) {
			QUERY_ID = query_id;
		}

		/**
		 * @return the iSSUE_DATE
		 */
		public String getISSUE_DATE() {
			return ISSUE_DATE;
		}

		/**
		 * @param issue_date the iSSUE_DATE to set
		 */
		public void setISSUE_DATE(String issue_date) {
			ISSUE_DATE = issue_date;
		}

		/**
		 * @return the rESOLVED_DATE
		 */
		public String getRESOLVED_DATE() {
			return RESOLVED_DATE;
		}

		/**
		 * @param resolved_date the rESOLVED_DATE to set
		 */
		public void setRESOLVED_DATE(String resolved_date) {
			RESOLVED_DATE = resolved_date;
		}

		/**
		 * @return the oUTSTANDING_DAYS
		 */
		public String getOUTSTANDING_DAYS() {
			return OUTSTANDING_DAYS;
		}

		/**
		 * @param outstanding_days the oUTSTANDING_DAYS to set
		 */
		public void setOUTSTANDING_DAYS(String outstanding_days) {
			OUTSTANDING_DAYS = outstanding_days;
		}
}