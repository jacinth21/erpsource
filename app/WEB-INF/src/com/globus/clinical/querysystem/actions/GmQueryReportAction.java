package com.globus.clinical.querysystem.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.querysystem.beans.GmQueryBean;
import com.globus.clinical.querysystem.forms.GmQueryForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;

public class GmQueryReportAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName()); 
	// Code to Initialize the Logger Class.

	
	/**
	 * fetchQueryReport - This method will
	 * 
	 * @param ActionMapping mapping
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @exception AppError
	 */
	public ActionForward fetchQueryReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("Enter");
		GmCommonBean gmCommonBean = new GmCommonBean();
		
		GmQueryForm gmQueryForm = (GmQueryForm) form;
		gmQueryForm.loadSessionParameters(request);
		String strUserId = gmQueryForm.getUserId(); 
		
		log.debug("pkey id : " + gmQueryForm.getSessPatientPkey());
		
		String strPatientListId = "";
		String strPatientAnsListId = "";
		String strQueryLevelId = "";
		String strFilterCondition = getAccessFilter(request, response); 
		String strListQueries = GmCommonClass.parseNull((String)gmQueryForm.getListQueries());
		String strSessStudyId = GmCommonClass.parseNull(gmQueryForm.getSessStudyId());
		String strSessStudySiteId = GmCommonClass.parseNull(gmQueryForm.getSessStudySiteId());

		/*
		log.debug("strListQueries: "+strListQueries);
		log.debug("strPatientListId: "+GmCommonClass.parseNull(gmQueryForm.getPatientListId()));
		log.debug("strPatientAnsListId: "+GmCommonClass.parseNull(gmQueryForm.getPatientAnsListId()));
		log.debug("strQueryLevelId: "+gmQueryForm.getQueryLevelId());
			*/	
				
		if(!strListQueries.equalsIgnoreCase("showAll")){ 
			strPatientListId = GmCommonClass.parseNull(gmQueryForm.getPatientListId());
			strPatientAnsListId = GmCommonClass.parseNull(gmQueryForm.getPatientAnsListId());
			strQueryLevelId = GmCommonClass.parseNull(gmQueryForm.getQueryLevelId());
		}else{
			strPatientListId = "";
			strPatientAnsListId = "";
			strQueryLevelId = "";
		}
		
		HashMap hmParam = new HashMap();
		hmParam.put("patientListId", strPatientListId);
		hmParam.put("patientAnsListId", strPatientAnsListId);
		hmParam.put("levelId", strQueryLevelId);
		hmParam.put("STUDYID", strSessStudyId);
		hmParam.put("STUDYSITEID", strSessStudySiteId);
		
		
		log.debug("hmParam: "+ hmParam);
		
		GmQueryBean gmQueryBean = new GmQueryBean();
		List lResult = new ArrayList();
		lResult = gmQueryBean.fetchQueryReport(hmParam, strFilterCondition);

        HashMap hmParamV = new HashMap();
        hmParamV.put("patientListId",strPatientListId);
        hmParamV.put("patientAnsListId",strPatientAnsListId);
        hmParamV.put("queryLevelId",strQueryLevelId);
        hmParamV.put("listQueries",strListQueries);
        hmParamV.put("TEMPLATE","GmQuery.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",lResult);

        gmQueryForm.setGridXmlData(gmQueryBean.getXmlGridData(hmParamV));
        String queryAccess = gmCommonBean.checkUserExists(strUserId,"17");
        request.setAttribute("queryAccess", queryAccess);
        
        log.debug("strListQueries: "+strListQueries);
        
        log.debug("Exit");
		
		return mapping.findForward("gmQueryReport");
	}
	
	/**
	 * fetchQueryDetails - This method will
	 * 
	 * @param ActionMapping mapping
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @exception AppError
	 */
	public ActionForward fetchQueryDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("Enter");
		GmQueryForm gmQueryForm = (GmQueryForm) form;
		GmCommonBean gmCommonBean = new GmCommonBean();
		gmQueryForm.loadSessionParameters(request);
		String strFilterCondition = getAccessFilter(request, response); 
		String userId = gmQueryForm.getUserId();
		
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmQueryForm);
		String strOpt = gmQueryForm.getStrOpt();
		GmQueryBean gmQueryBean = new GmQueryBean();
		String queryAccess = gmCommonBean.checkUserExists(userId,"17");
		request.setAttribute("queryAccess",queryAccess);
		
		HashMap hmReturn = new HashMap();
		String strQueryId = "";    
		if (strOpt.equals("save"))
        {
			hmReturn = gmQueryBean.saveQueryDetails(hmParam);
			strQueryId = (String)hmReturn.get("QUERYID");
			gmQueryForm.setQueryId(strQueryId);
			hmParam = GmCommonClass.getHashMapFromForm(gmQueryForm);	
        }
		
		HashMap hmResult = new HashMap();
		hmResult = gmQueryBean.fetchQueryDetails(hmParam, strFilterCondition);

		request.setAttribute("hmResult", hmResult);
        gmQueryForm.setHmResult(hmResult);

        gmQueryForm.setAlStatus(GmCommonClass.getCodeList("QRYSS"));
        
        gmQueryForm.setFromContainer((String)hmParam.get("FROMCONTAINER"));
        
        log.debug("Exit");
		
		return mapping.findForward("gmQueryDetails");
	}

	/**
	 * fetchQueryContainer - This method will
	 * 
	 * @param ActionMapping mapping
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @exception AppError
	 */	
	public ActionForward fetchQueryContainer(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {		
		GmQueryForm gmQueryForm = (GmQueryForm) form;
		GmCommonBean gmCommonBean = new GmCommonBean();	
		gmQueryForm.loadSessionParameters(request);
		String strUserId = gmQueryForm.getUserId(); 
        String queryAccess = gmCommonBean.checkUserExists(strUserId,"17");
        request.setAttribute("queryAccess", queryAccess);
		return mapping.findForward("gmQueryContainer");
	}

	/**
	 * fetchQueryWindowClose - This method will
	 * 
	 * @param ActionMapping mapping
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @exception AppError
	 */
	public ActionForward fetchQueryWindowClose(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("Enter");
		return mapping.findForward("gmQueryWindowClose");
	}
	
}
