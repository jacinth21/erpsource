package com.globus.clinical.querysystem.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.querysystem.beans.GmSiteMappingBean;

import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.querysystem.forms.GmSiteMapingForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.forms.GmContactSetupForm;

public class GmSiteMapingAction extends GmDispatchAction{
      
	  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
    
	  public ActionForward loadSiteMapingSetup (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
		  log.debug("Enter");
		  
		  GmPatientBean gmPatientBean = new GmPatientBean();
		  GmSiteMappingBean gmSiteMappingBean = new GmSiteMappingBean();
		  GmSiteMapingForm gmSiteMapingForm = (GmSiteMapingForm) form;
		  ArrayList alLogReasons = new ArrayList();
		  GmCommonBean gmCommonBean = new GmCommonBean();
		  gmSiteMapingForm.loadSessionParameters(request);
		  
		  String strOpt = gmSiteMapingForm.getStrOpt();
		  
		  String strSitePartyId = gmSiteMapingForm.getSitePartyId();
		  String strURL="";
		  HashMap hmValues = new HashMap();
		  HashMap hmParam = new HashMap ();
		  
		  if (strOpt.equals("edit"))
			{
			  	
				hmValues = gmSiteMappingBean.fetchEditSiteMappingInfo(Integer.parseInt(strSitePartyId));
				log.debug(" values fetched for edit " +hmValues);
				gmSiteMapingForm = (GmSiteMapingForm)GmCommonClass.getFormFromHashMap(gmSiteMapingForm,hmValues);
			}else if (strOpt.equals("save"))
			{
					strURL="/gmPartySetup.do?&partyType=7007&strOpt=fetchcontainer&partyId="+gmSiteMapingForm.getPartyId();
				    request.setAttribute("hRedirectURL", strURL);
				 hmParam = GmCommonClass.getHashMapFromForm(gmSiteMapingForm);
				log.debug(" values to save " + hmParam);
				gmSiteMappingBean.saveSiteMappingInfo(hmParam);
				// Reseting the form
				gmSiteMapingForm.reset();
			}
		  
		  gmSiteMapingForm.setAlStudyList(gmPatientBean.loadStudyList("ALLSDL",""));
		  gmSiteMapingForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmSiteMapingForm.getStudyListId(),"")); 
		  log.debug("Enter" +gmSiteMapingForm.getSiteListId() );
		  log.debug("exit");
		  
			// Displaying comments
			
			alLogReasons = gmCommonBean.getLog(strSitePartyId+"", "92091");
			gmSiteMapingForm.setAlLogReasons(alLogReasons);

		  return mapping.findForward("gmSiteMappingInfo");
	   }

	  public ActionForward loadSiteMappingSetupReport (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
			try {
				GmSiteMapingForm gmContactSetupForm = (GmSiteMapingForm)form;
				GmSiteMappingBean gmSiteMappingBean = new GmSiteMappingBean();
				String strPartyId = gmContactSetupForm.getPartyId();
				
				if (!strPartyId.equals("")){
					log.debug("strPartyId = " +strPartyId);
					List lResult = gmSiteMappingBean.fetchSiteMappingSummary(strPartyId).getRows();
					log.debug("Size of lResult " + lResult.size());
					gmContactSetupForm.setLresult(lResult);
					
					// As there is not search hyperlinks for Contact page
					//request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("NA"));
				}
				
			}  catch (Exception e) {
				e.printStackTrace();
				throw new AppError(e);
			}
			   return mapping.findForward("gmIncludeSiteMappingReport");
	   }

	
}
