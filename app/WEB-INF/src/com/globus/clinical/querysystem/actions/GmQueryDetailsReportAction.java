package com.globus.clinical.querysystem.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.querysystem.beans.GmQueryBean;
import com.globus.clinical.querysystem.forms.GmQueryReportForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmQueryDetailsReportAction extends GmDispatchAction {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 
	public ActionForward fetchQueryByStudy(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
		 
		String strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));
		String strreportid = "BYSTUDY";
		process(mapping, form, request, response, strreportid);
       
        return mapping.findForward("rptClinicalQueryperStudyMap"); 
    }
	
public ActionForward fetchQueryBySite(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception 
{
        String strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));
        String strreportid = "BYSITE";
         process(mapping, form, request, response, strreportid);
              
        return mapping.findForward("rptClinicalQueryperStudyMap"); 
}

public ActionForward fetchQueryByPatient(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
    
    String strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));  
    String strreportid = "BYPATIENT";    
    process(mapping, form, request, response, strreportid);
     
    return mapping.findForward("rptClinicalQueryperStudyMap"); 
}

private void process(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response, String strreportid) throws Exception 
	{
	String strAccessFilter = "";
	
		 try {
	        	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	        	
	        	GmQueryBean gmQueryBean  = new GmQueryBean();
	        	GmQueryReportForm gmQueryReportForm = (GmQueryReportForm) form;
	        	gmQueryReportForm.loadSessionParameters(request);
	        	
	        	String strStudyId  = GmCommonClass.parseNull(gmQueryReportForm.getSessStudyId());
	        	String strSiteId =  GmCommonClass.parseNull(gmQueryReportForm.getStrsite_id());	  
	        	String strPatientId = GmCommonClass.parseNull(gmQueryReportForm.getSessPatientPkey());

	        	strSiteId = strSiteId.equals("") ? gmQueryReportForm.getSessSiteId() : strSiteId ;
	        	strSiteId = strSiteId.equals("") ? "NULL" : strSiteId;
	        	
	        	log.debug("strSiteId: "+strSiteId);
	        	
	        	gmQueryReportForm.setStrstudy_id(strStudyId);
	        	gmQueryReportForm.setStrsite_id(strSiteId);
	        	gmQueryReportForm.setStrreportid(strreportid);
	        	
	        	strAccessFilter =  getAccessFilter(request, response);
	        	
		        HashMap hmResult = new HashMap ();
	            hmResult = gmQueryBean.fetchQueryReportDetails(strStudyId, strSiteId,strPatientId, strreportid, strAccessFilter);
	            gmQueryReportForm.setHmCrossTabReport(hmResult);
	            gmQueryReportForm.setType(strreportid);                       
	        }
	        catch(Exception exp)
	        {
	            exp.printStackTrace();
	            throw new AppError(exp);
	        }
}
	
	public ActionForward fetchQueryPerPatient(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    {
		String strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));
		String strXMLData = " ";
		String patientId = " ";
		
		GmQueryReportForm gmQueryReportForm = (GmQueryReportForm) form;
		gmQueryReportForm.loadSessionParameters(request);
		GmQueryBean gmQueryBean = new GmQueryBean();
		ArrayList alReturn = new ArrayList();
		patientId = GmCommonClass.parseNull(request.getParameter("patientId"));
		gmQueryReportForm.setPatientId(patientId);
		
		String strAccessFilter  =  this.getAccessFilter(request, response);
		alReturn = gmQueryBean.fetchQueryPerPatient(patientId, strAccessFilter);

		HashMap hmParamV = new HashMap();
	    hmParamV.put("TEMPLATE","GmClinicalQueryperPatient.vm");
	    hmParamV.put("TEMPLATEPATH","clinical/templates");
	    hmParamV.put("RLIST",alReturn);
	    strXMLData = gmQueryBean.getXmlGridData(hmParamV);
	    
	    gmQueryReportForm.setGridXmlData(strXMLData);
	    
	    
		
        return mapping.findForward("rptClinicalQueryperPatientMap");
	}
	
	public ActionForward fetchQueryByTP(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    {
			GmQueryReportForm gmQueryReportForm = (GmQueryReportForm) form;
			gmQueryReportForm.loadSessionParameters(request);

		    String strStudyId  = GmCommonClass.parseNull(gmQueryReportForm.getSessStudyId());
    	    String strSiteId =  GmCommonClass.parseNull(gmQueryReportForm.getStrsite_id());	        	
    	    strSiteId = strSiteId.equals("") ? gmQueryReportForm.getSessSiteId() : strSiteId ;
    	    strSiteId = strSiteId.equals("") ? "NULL" : strSiteId;
    	
			String strreportid = "Timeout";
			String patientId = " ";
			String strAccessFilter = getAccessFilter(request, response);
			GmQueryBean gmQueryBean = new GmQueryBean();
			
			String strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));
			patientId = GmCommonClass.parseNull(request.getParameter("patientId"));
			gmQueryReportForm.setHmCrossTabReport(GmCommonClass.parseNullHashMap(((GmQueryBean) gmQueryBean).fetchQueryByTP(strStudyId, strSiteId, patientId, strAccessFilter))) ;
			gmQueryReportForm.setType(strreportid);
						
			if (strExcel.equalsIgnoreCase("Excel"))
	    	{

	    		response.setContentType("application/vnd.ms-excel");
	    		response.setHeader("Content-disposition","attachment;filename=QueryByTP.xls");			
	    		return mapping.findForward("excel");
	    	}		
    		log.debug(" Inside reporting.. setting values ");
    	
    		 return mapping.findForward("rptClinicalQueryperTimeoutMap");
    	
    }
}