package com.globus.clinical.querysystem.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmQueryBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public HashMap fetchQueryReportDetails(String strstudy_id, String strsite_id, String strPatientId, String strreportid, String strAccessFilter) throws Exception {
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		GmDBManager gmDBManager = new GmDBManager();
		StringBuffer sbHeaderQuery = new StringBuffer();

		StringBuffer sbDetailsQuery = new StringBuffer();
		StringBuffer sbLinkQuery = new StringBuffer();
		ArrayList alThdQuery = new ArrayList();

		HashMap hmFinalValue = new HashMap();
		HashMap hmThdValue = new HashMap();

		sbHeaderQuery.append("	SELECT T612.C612_STUDY_FORM_DS FORM_DS ");
		sbHeaderQuery.append("	FROM T612_STUDY_FORM_LIST T612 ");
		sbHeaderQuery.append("	WHERE T612.C611_STUDY_ID = '" + strstudy_id + "'");		
		sbHeaderQuery.append("	ORDER BY T612.C612_SEQ ");

		log.debug("Header Query :  " + sbHeaderQuery.toString());

		sbDetailsQuery.append(" SELECT ");
		sbDetailsQuery.append(getID(strstudy_id, strsite_id, strreportid));
		sbDetailsQuery.append(" ID, ");
		sbDetailsQuery.append(getName(strstudy_id, strsite_id, strreportid));
		sbDetailsQuery.append(" NAME, ");
		sbDetailsQuery.append(" v621.c612_study_form_ds form_ds, COUNT (v621.c6510_query_id) count");
		sbDetailsQuery.append(" FROM ");
		sbDetailsQuery.append(" (SELECT t6510.c611_study_id, t611.c611_study_nm, t614.c614_site_id, ");
		sbDetailsQuery.append(" t612.c612_study_form_ds, t621.c621_patient_id, ");
		sbDetailsQuery.append("	t621.c621_patient_ide_no, c6510_query_id, t612.c612_seq, ");
		sbDetailsQuery.append("	t6510.c704_account_id ");
		sbDetailsQuery.append(" FROM t6510_query t6510, ");
		sbDetailsQuery.append("	t611_clinical_study t611, ");
		sbDetailsQuery.append("	t612_study_form_list t612, ");
		sbDetailsQuery.append(" t614_study_site t614, ");
		sbDetailsQuery.append("	t621_patient_master t621 ");
		sbDetailsQuery.append(" WHERE t6510.c611_study_id = t611.c611_study_id "); 
		sbDetailsQuery.append(" AND t611.C611_DELETE_FL = 'N' ");
		sbDetailsQuery.append(" AND t612.C612_DELETE_FL = 'N' ");
		sbDetailsQuery.append(" AND t614.C614_VOID_FL IS NULL ");
		sbDetailsQuery.append(" AND t621.C621_DELETE_FL = 'N' ");	   
		sbDetailsQuery.append(" AND t6510.c6510_void_fl IS NULL ");
		sbDetailsQuery.append("	AND t6510.c901_status IN (92441, 92443) ");
		sbDetailsQuery.append("	AND t612.c611_study_id = t6510.c611_study_id ");
		sbDetailsQuery.append("	AND t612.c601_form_id = t6510.c601_form_id ");
		sbDetailsQuery.append("	AND t614.c614_study_site_id = t6510.c614_study_site_id ");
		sbDetailsQuery.append("	AND t621.c621_patient_id = t6510.c621_patient_id) v621 "); 
		sbDetailsQuery.append(" WHERE v621.c611_study_id = NVL ('" + strstudy_id + "', v621.c611_study_id) ");
		sbDetailsQuery.append(" AND v621.c614_site_id = NVL (" + strsite_id+ " , v621.c614_site_id) ");
		sbDetailsQuery.append(" AND v621.c621_patient_id IN DECODE ('"+strPatientId+"','0',v621.c621_patient_id,'',v621.c621_patient_id,'"+strPatientId+"')");
		
		
		if (!strAccessFilter.equals("")) {
			sbDetailsQuery.append(" AND " + strAccessFilter + "  ");
		}
		sbDetailsQuery.append(" GROUP BY ");
		sbDetailsQuery.append(getID(strstudy_id, strsite_id, strreportid));
		sbDetailsQuery.append(",");
		sbDetailsQuery.append(getName(strstudy_id, strsite_id, strreportid));
		sbDetailsQuery.append(",v621.c612_study_form_ds,v621.c612_seq ORDER BY ");
		sbDetailsQuery.append(getID(strstudy_id, strsite_id, strreportid));
		sbDetailsQuery.append(",");
		sbDetailsQuery.append(getName(strstudy_id, strsite_id, strreportid));
		sbDetailsQuery.append(", v621.c612_seq ");

		log.debug("Details Query :  " + sbDetailsQuery.toString());

		hmFinalValue = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailsQuery.toString());

		sbLinkQuery.append("SELECT " + getID(strstudy_id, strsite_id, strreportid) + " ID ");
		sbLinkQuery.append(" , NVL (SUM (outstanding_issue), 0) OUT_ISSUE, ");
		sbLinkQuery.append(" ROUND(NVL (AVG (avg_days), 0),1) avg_days ");		
		sbLinkQuery.append(" FROM (SELECT DISTINCT T621.C611_STUDY_ID, T614.C614_SITE_ID, ");
		sbLinkQuery.append(" T614.C704_ACCOUNT_ID, T621.C621_PATIENT_ID ");
		sbLinkQuery.append(" from T621_PATIENT_MASTER T621, T614_STUDY_SITE T614 ");
		sbLinkQuery.append(" WHERE T621.C611_STUDY_ID = T614.C611_STUDY_ID ");
		sbLinkQuery.append(" AND T621.C704_ACCOUNT_ID = T614.C704_ACCOUNT_ID ");
		sbLinkQuery.append(" AND T621.C611_STUDY_ID = ");
		sbLinkQuery.append(" NVL ('" + strstudy_id + "', t621.c611_study_id) ");
		sbLinkQuery.append(" AND t621.c621_patient_id IN DECODE ('"+strPatientId+"','0',t621.c621_patient_id,'',t621.c621_patient_id,'"+strPatientId+"')");
		sbLinkQuery.append(" AND T614.c614_site_id = NVL (" + strsite_id + ", T614.c614_site_id)) v621, ");
		sbLinkQuery.append(" (SELECT   t6510.c611_study_id, t614.c614_site_id, ");
		sbLinkQuery.append(" t6510.c621_patient_id, COUNT (1) outstanding_issue ");
		sbLinkQuery.append(" FROM t6510_query t6510, t614_study_site t614 ");
		sbLinkQuery.append(" WHERE t6510.c614_study_site_id = t614.c614_study_site_id ");
		sbLinkQuery.append(" AND t6510.c6510_void_fl IS NULL ");
		sbLinkQuery.append(" AND t614.C614_VOID_FL IS NULL ");
		sbLinkQuery.append(" AND t6510.c901_status IN (92441, 92443) ");
		sbLinkQuery.append(" AND t6510.c611_study_id = NVL ( '" + strstudy_id + "' , t6510.c611_study_id) ");
		sbLinkQuery.append(" AND t614.c614_site_id = NVL ( " + strsite_id + " , t614.c614_site_id) ");
		sbLinkQuery.append(" AND t6510.c621_patient_id IN DECODE ('"+strPatientId+"','0',t6510.c621_patient_id,'',t6510.c621_patient_id,'"+strPatientId+"')");
		sbLinkQuery.append(" GROUP BY t6510.c611_study_id, ");
		sbLinkQuery.append(" t614.c614_site_id, ");
		sbLinkQuery.append(" t6510.c621_patient_id) oissue, ");
		sbLinkQuery.append(" (SELECT   t6510.c611_study_id, t614.c614_site_id, ");
		sbLinkQuery.append(" t6510.c621_patient_id, ");
		sbLinkQuery.append(" NVL (AVG (  get_weekdaysdiff (TRUNC(t6510.c6510_reported_date), TRUNC(SYSDATE)) ");	
		sbLinkQuery.append("  ), ");
		sbLinkQuery.append(" 0 ");
		sbLinkQuery.append(" ) avg_days ");
		sbLinkQuery.append(" FROM t6510_query t6510, t614_study_site t614 ");
		sbLinkQuery.append(" WHERE t6510.c614_study_site_id = t614.c614_study_site_id ");
		sbLinkQuery.append(" AND t6510.c6510_void_fl IS NULL");
		sbLinkQuery.append(" AND t614.C614_VOID_FL IS NULL ");		
		sbLinkQuery.append(" AND t6510.c901_status IN (92441, 92443) ");
		sbLinkQuery.append(" AND t6510.c611_study_id = NVL ('" + strstudy_id + "', t6510.c611_study_id) ");
		sbLinkQuery.append(" AND t614.c614_site_id = NVL (" + strsite_id + ", t614.c614_site_id) ");
		sbLinkQuery.append(" AND t6510.c621_patient_id IN DECODE ('"+strPatientId+"','0',t6510.c621_patient_id,'',t6510.c621_patient_id,'"+strPatientId+"')");
		sbLinkQuery.append(" AND TRUNC (t6510.c6510_reported_date) < TRUNC (SYSDATE) ");
		sbLinkQuery.append(" GROUP BY t6510.c611_study_id, ");
		sbLinkQuery.append(" t614.c614_site_id, ");
		sbLinkQuery.append(" t6510.c621_patient_id) avg_days ");
		sbLinkQuery.append(" WHERE v621.c611_study_id = oissue.c611_study_id(+) ");
		sbLinkQuery.append(" AND v621.c614_site_id = oissue.c614_site_id(+) ");
		sbLinkQuery.append(" AND v621.c621_patient_id = oissue.c621_patient_id(+) ");
		sbLinkQuery.append(" AND v621.c611_study_id = avg_days.c611_study_id(+) ");
		sbLinkQuery.append(" AND v621.c614_site_id = avg_days.c614_site_id(+) ");
		sbLinkQuery.append(" AND v621.c621_patient_id = avg_days.c621_patient_id(+) ");
		if (!strAccessFilter.equals("")) {
			sbLinkQuery.append(" AND " + strAccessFilter + "  ");
		}
		sbLinkQuery.append(" GROUP BY " + getID(strstudy_id, strsite_id, strreportid));
		sbLinkQuery.append(" HAVING NVL (SUM (outstanding_issue), 0) > 0 OR  NVL (SUM (avg_days), 0) > 0 ");
		sbLinkQuery.append(" ORDER BY ID");

		log.debug("Link Query :  " + sbLinkQuery.toString());

		alThdQuery = gmDBManager.queryMultipleRecords(sbLinkQuery.toString());

		hmThdValue = getLinkInfo("Study");
		hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alThdQuery, hmThdValue);
		gmDBManager.close();

		return hmFinalValue;
	}

	public ArrayList fetchQueryPerPatient(String patientId, String strAccessFilter) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		StringBuffer sbDetailsQuery = new StringBuffer();
		ArrayList alList = new ArrayList();
		
		  sbDetailsQuery.append("  SELECT t621.C621_PATIENT_IDE_NO patient_ide_no "); 
		  sbDetailsQuery.append("  , t613.C613_STUDY_PERIOD_DS study_period "); 
		  sbDetailsQuery.append("  , t612.C612_STUDY_FORM_DS form_ds "); 
		  sbDetailsQuery.append("  , v621.c6510_query_id query_id "); 
		  sbDetailsQuery.append(" , TO_CHAR (v621.c6510_reported_date, 'MM/DD/YYYY') issue_date "); 
		  sbDetailsQuery.append(" , DECODE (v621.c901_status, 92444, TO_CHAR (v621.c6510_last_updated_date, 'MM/DD/YYYY'), '') resolved_date "); 
		  sbDetailsQuery.append(" , DECODE "); 
		  sbDetailsQuery.append(" (v621.c901_status, "); 
		  sbDetailsQuery.append("  92444, 0, "); 
		  sbDetailsQuery.append("  get_weekdaysdiff (TRUNC (v621.c6510_reported_date), "); 
		  sbDetailsQuery.append("  TRUNC (SYSDATE) "); 
		  sbDetailsQuery.append("  ) "); 
		  sbDetailsQuery.append("  ) outstanding_days, "); 
		  sbDetailsQuery.append("  v621.c622_patient_list_id patient_list_id, "); 
		  sbDetailsQuery.append("  v621.c623_patient_ans_list_id patient_ans_list_id, "); 
		  sbDetailsQuery.append("  v621.c901_query_level_id query_level   "); 
		  sbDetailsQuery.append("  ,get_code_name(v621.C901_STATUS) STATUS ");
		  sbDetailsQuery.append("  from t6510_query v621 "); 
		  sbDetailsQuery.append("  , T621_PATIENT_MASTER T621 "); 
		  sbDetailsQuery.append("  , T613_STUDY_PERIOD T613 "); 
		  sbDetailsQuery.append("  , T612_STUDY_FORM_LIST T612  "); 
		  sbDetailsQuery.append("  where v621.C621_PATIENT_ID = T621.C621_PATIENT_ID "); 
		  sbDetailsQuery.append("  AND v621.C613_STUDY_PERIOD_ID = t613.C613_STUDY_PERIOD_ID "); 
		  sbDetailsQuery.append("  AND v621.C611_STUDY_ID = T612.C611_STUDY_ID "); 
		  sbDetailsQuery.append("  AND v621.C601_FORM_ID = t612.C601_FORM_ID "); 
		  sbDetailsQuery.append("  AND v621.c621_patient_id = " + patientId );
		  sbDetailsQuery.append("  AND v621.c6510_void_fl IS NULL "); 
		  sbDetailsQuery.append("  AND t621.C621_DELETE_FL = 'N' "); 
		  sbDetailsQuery.append("  AND t613.C613_DELETE_FL = 'N' "); 
		  sbDetailsQuery.append("  AND t612.C612_DELETE_FL = 'N' ");
		
		if (!strAccessFilter.equals("")) {
			sbDetailsQuery.append(" AND " + strAccessFilter + "  ");
		}
		
		sbDetailsQuery.append(" ORDER BY v621.c6510_query_id,  t613.C901_STUDY_PERIOD, t612.C601_FORM_ID, STATUS, issue_date, resolved_date ");
		
		log.debug("Query : " + sbDetailsQuery.toString());
		alList = gmDBManager.queryMultipleRecords(sbDetailsQuery.toString());

		gmDBManager.close();
		return alList;

	}

	private String getID(String strStudyID, String strSiteID, String strReport) {
		StringBuffer strBuffer = new StringBuffer();
		if (!strSiteID.equals("") && strReport.equals("BYPATIENT")) {
			strBuffer.append("v621.c621_patient_id");
		} else if (!strStudyID.equals("") && strReport.equals("BYSITE")) {
			strBuffer.append("v621.c614_site_id");
		} else if (!strStudyID.equals("") && strReport.equals("BYSTUDY")) {
			strBuffer.append("v621.c611_study_id");
		}
		return strBuffer.toString();
	}

	private String getName(String strStudyID, String strSiteID, String strReport) {
		StringBuffer strBuffer = new StringBuffer();
		if (!strSiteID.equals("") && strReport.equals("BYPATIENT")) {
			strBuffer.append("v621.c621_patient_ide_no");
		} else if (!strStudyID.equals("") && strReport.equals("BYSITE")) {
			strBuffer.append("v621.c614_site_id");
		} else if (!strStudyID.equals("") && strReport.equals("BYSTUDY")) {
			strBuffer.append("v621.c611_study_nm");
		}
		return strBuffer.toString();
	}

	/**
	 * getLinkInfo - This Method is used to to get the link object information
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	private HashMap getLinkInfo(String strType) {
		HashMap hmLinkDetails = new HashMap();
		HashMap hmapValue = new HashMap();
		ArrayList alLinkList = new ArrayList();

		// First parameter holds the link object name
		hmLinkDetails.put("LINKID", "ID");

		if (strType.equals("Study")) {

			hmapValue = new HashMap();
			hmapValue.put("KEY", "OUT_ISSUE");
			hmapValue.put("VALUE", "Total # of Queries Outstanding");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "AVG_DAYS");
			hmapValue.put("VALUE", "Average # of Days Outstanding");
			alLinkList.add(hmapValue);

			hmLinkDetails.put("POSITION", "BEFORE");
		}
		hmLinkDetails.put("LINKVALUE", alLinkList);
		return hmLinkDetails;
	}

	public HashMap fetchQueryByTP(String strstudy_id, String strsite_id, String patientId, String strAccessFilter) throws AppError {
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();

		StringBuffer sbHeaderQuery = new StringBuffer();
		StringBuffer sbDtlQuery = new StringBuffer();
		HashMap hmFinalValue = new HashMap();

		sbHeaderQuery.append("	SELECT T612.C612_STUDY_FORM_DS FORM_DS ");
		sbHeaderQuery.append("	FROM T612_STUDY_FORM_LIST T612 ");
		sbHeaderQuery.append("	WHERE T612.C611_STUDY_ID = '" + strstudy_id + "'");		
		sbHeaderQuery.append("	ORDER BY T612.C612_SEQ ");

		log.debug("Header Query: " + sbHeaderQuery.toString());

		
		  sbDtlQuery.append(" SELECT   v621.c613_study_period_ds ID, v621.c613_study_period_ds NAME, "); 
		  sbDtlQuery.append(" v621.c612_study_form_ds form_ds, "); 
		  sbDtlQuery.append(" COUNT (t6510.c6510_query_id) query_id, due_date "); 
		  sbDtlQuery.append(" FROM (SELECT t621.c621_patient_ide_no, t613.c613_study_period_ds, "); 
		  sbDtlQuery.append(" t612.c612_study_form_ds, "); 
		  sbDtlQuery.append(" DECODE "); 
		  sbDtlQuery.append(" (NVL (pt_sc10.c622_surgery_date, t621.c621_surgery_date), "); 
		  sbDtlQuery.append(" NULL, TO_DATE (NULL, 'MM/DD/YYYY'), "); 
		  sbDtlQuery.append(" get_cr_cal_exam_date (NVL (pt_sc10.c622_surgery_date, "); 
		  sbDtlQuery.append(" t621.c621_surgery_date "); 
		  sbDtlQuery.append(" ), "); 
		  sbDtlQuery.append(" t613.c901_period_type, "); 
		  sbDtlQuery.append(" t613.c613_duration, "); 
		  sbDtlQuery.append(" t613.c613_grace_period_after, "); 
		  sbDtlQuery.append(" c901_grace_period_type, "); 
		  sbDtlQuery.append(" 'F' "); 
		  sbDtlQuery.append(" ) "); 
		  sbDtlQuery.append(" ) due_date, "); 
		  sbDtlQuery.append(" t614.c704_account_id, t621.c621_patient_id, "); 
		  sbDtlQuery.append(" t613.c613_study_period_id, t612.c601_form_id, "); 
		  sbDtlQuery.append(" t614.c614_site_id, t611.c611_study_id "); 
		  sbDtlQuery.append(" , t613.C613_SEQ_NO ");
		  sbDtlQuery.append(" FROM t621_patient_master t621, "); 
		  sbDtlQuery.append(" t612_study_form_list t612, "); 
		  sbDtlQuery.append(" t613_study_period t613, "); 
		  sbDtlQuery.append(" t614_study_site t614, "); 
		  sbDtlQuery.append(" t611_clinical_study t611, "); 
		  sbDtlQuery.append(" (SELECT c621_patient_id, c622_date c622_surgery_date "); 
		  sbDtlQuery.append(" FROM t622_patient_list "); 
		  sbDtlQuery.append(" WHERE c601_form_id IN (10, 211)) pt_sc10 "); 
		  sbDtlQuery.append(" WHERE t621.c621_delete_fl = 'N' "); 
		  sbDtlQuery.append(" AND t621.c704_account_id IS NOT NULL "); 
		  sbDtlQuery.append(" AND t612.c611_study_id = t621.c611_study_id "); 
		  sbDtlQuery.append(" AND t612.c612_study_list_id = t613.c612_study_list_id "); 
		  sbDtlQuery.append(" AND t614.c611_study_id = t612.c611_study_id "); 
		  sbDtlQuery.append(" AND t614.c704_account_id = t621.c704_account_id "); 
		  sbDtlQuery.append(" AND t614.c614_void_fl IS NULL "); 
		  sbDtlQuery.append(" AND t621.c901_surgery_type IS NOT NULL "); 
		  sbDtlQuery.append(" AND t621.c621_patient_id = pt_sc10.c621_patient_id(+) "); 
		  sbDtlQuery.append(" AND t611.c611_study_id = t612.c611_study_id "); 
		  sbDtlQuery.append(" AND t611.c611_delete_fl <> 'Y' ) v621, "); 
		  sbDtlQuery.append(" t6510_query t6510 "); 
		  sbDtlQuery.append(" WHERE v621.c611_study_id = t6510.c611_study_id(+) "); 
		  sbDtlQuery.append(" AND v621.c704_account_id = t6510.c704_account_id(+) "); 
		  sbDtlQuery.append(" AND v621.c621_patient_id = t6510.c621_patient_id(+) "); 
		  sbDtlQuery.append(" AND v621.c613_study_period_id = t6510.c613_study_period_id(+) "); 
		  sbDtlQuery.append(" AND v621.c601_form_id = t6510.c601_form_id(+) "); 
		  sbDtlQuery.append(" AND v621.c611_study_id = '" + strstudy_id + "'" ); 
		  sbDtlQuery.append(" AND v621.c614_site_id = '" + strsite_id + "'" ); 
		  sbDtlQuery.append(" AND v621.c621_patient_id = " + patientId  );
		  sbDtlQuery.append(" AND t6510.c6510_void_fl IS NULL "); 
		  
		  if (!strAccessFilter.equals("")) {
			  sbDtlQuery.append(" AND " + strAccessFilter + "  ");
			}
		  
		  sbDtlQuery.append(" GROUP BY v621.c621_patient_ide_no, "); 
		  sbDtlQuery.append(" v621.c613_study_period_ds, "); 
		  sbDtlQuery.append(" v621.c612_study_form_ds, "); 
		  sbDtlQuery.append(" due_date, v621.C613_SEQ_NO ");
		  sbDtlQuery.append(" ORDER BY V621.C613_SEQ_NO ");

		log.debug("Details Query : " + sbDtlQuery.toString());

		gmCrossReport.setFixedColumn(true);		
		gmCrossReport.setFixedParams(4, "DUEDATE");
		hmFinalValue = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDtlQuery.toString());

		return hmFinalValue;
	}

	/**
	 * fetchQueryReport - This method will
	 * 
	 * @param HashMap hmParam
	 * @param String strCondition
	 * @return List
	 * @exception AppError
	 */
	public List fetchQueryReport(HashMap hmParam, String strCondition) throws AppError {
		ArrayList alResult = null;

		String strPatientListId = GmCommonClass.parseNull((String) hmParam.get("patientListId"));
		if (strPatientListId.equals(""))
			strPatientListId = null;
		String strPatientAnsListId = GmCommonClass.parseNull((String) hmParam.get("patientAnsListId"));
		if (strPatientAnsListId.equals(""))
			strPatientAnsListId = null;

		String strQueryLevelId = GmCommonClass.parseNull((String) hmParam.get("levelId"));
		String strSessStudyId =  GmCommonClass.parseNull((String) hmParam.get("STUDYID"));
		String strSessStudySiteId =  GmCommonClass.parseNull((String) hmParam.get("STUDYSITEID"));
		if (strQueryLevelId.equals(""))
			strQueryLevelId = null;

		log.debug("hmParam:" + hmParam);
		GmDBManager gmDBManager = new GmDBManager();

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.setLength(0);
		sbQuery.append(" SELECT t6510.c6510_query_id query_id, t6510.c6510_short_desc short_desc, ");
		sbQuery.append(" TO_CHAR(t6510.C6510_REPORTED_DATE,'mm/dd/yyyy') query_raised, ");
		sbQuery.append(" get_code_name (t6510.c901_status) status, t6510.c901_status status_id, ");
		sbQuery.append(" t6510.c622_patient_list_id patient_list_id, t6510.C623_PATIENT_ANS_LIST_ID PATIENT_ANS_LIST_ID ");
		sbQuery.append(" ,v621.c611_study_id, v621.studynm ");
		sbQuery.append(" FROM t6510_query t6510, " + " (SELECT   v621.c614_study_site_id, v621.c614_site_id, ");
		sbQuery.append(" v621.c704_account_id, v621.c611_study_id, v621.c611_study_nm studynm");
		sbQuery.append(" FROM v621_patient_study_list v621 " + " GROUP BY v621.c614_study_site_id, " + " v621.c614_site_id, ");		
		sbQuery.append(" v621.c704_account_id,  v621.c611_study_id,v621.c611_study_nm) v621 ");
		sbQuery.append("  WHERE t6510.c6510_void_fl IS NULL AND t6510.c901_query_level_id = NVL(");
		sbQuery.append(strQueryLevelId);
		sbQuery.append(",t6510.c901_query_level_id) "); // p_query_level_id
		sbQuery.append(" AND NVL (t6510.c622_patient_list_id, -999) = ");
		sbQuery.append(" NVL ");
		sbQuery.append(" (");
		sbQuery.append(strPatientListId);
		sbQuery.append(", NVL (t6510.c622_patient_list_id, -999)) "); // p_patient_list_id if form query is clicked
		sbQuery.append(" AND NVL (t6510.c623_patient_ans_list_id, -999) = " + " NVL " + " (" + strPatientAnsListId);
		sbQuery.append(", NVL (t6510.c623_patient_ans_list_id, -999)) "); // p_patient_answer_list_id if answer query is clicked
		sbQuery.append(" AND v621.c614_study_site_id = t6510.c614_study_site_id ");
		if(!strSessStudySiteId.equals(""))
		{
			sbQuery.append(" AND t6510.c614_study_site_id =  DECODE("+strSessStudySiteId+", '',t6510.c614_study_site_id, "+strSessStudySiteId+") ");
		}
		if(!strSessStudyId.equals(""))
		{
			sbQuery.append(" AND t6510.c611_study_id = DECODE('"+strSessStudyId+"', '',t6510.c611_study_id, '"+strSessStudyId+"') ");
		}
		if (!strCondition.equals("")) {
			sbQuery.append(" AND " + strCondition);
		}
		sbQuery.append(" ORDER BY  t6510.c6510_query_id DESC");
		log.debug("query===" + sbQuery.toString());
		alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
		gmDBManager.close();
		return alResult;
	}

	/**
	 * fetchQueryDetails - This method will
	 * 
	 * @param HashMap hmParam
	 * @param String strCondition
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchQueryDetails(HashMap hmParam, String strCondition) throws AppError {
		log.debug("Enter");
		HashMap hmResult = null;
		HashMap hmQueryHeaderBasic = null;
		HashMap hmQueryHeaderDetail = null;
		ArrayList alDescription = null;
		String strPatientListId = GmCommonClass.parseNull((String) hmParam.get("PATIENTLISTID"));
		String strPatientAnsListId = GmCommonClass.parseNull((String) hmParam.get("PATIENTANSLISTID"));

		String strLevelId = GmCommonClass.parseNull((String) hmParam.get("QUERYLEVELID"));
		String strQueryId = GmCommonClass.parseNull((String) hmParam.get("QUERYID"));

		log.debug("hmParam:" + hmParam);
		GmDBManager gmDBManager = new GmDBManager();

		StringBuffer sbQuery = new StringBuffer();

		// Query Info for the first part
		sbQuery.setLength(0);
		sbQuery.append(" SELECT DISTINCT t621.c621_patient_ide_no patient_ide_no, "); 
		sbQuery.append(" t621.c621_patient_id patient_id,t612.c601_form_id form_id,t613.c613_study_period_id study_period_pk,"); 
		sbQuery.append(" t613.c901_study_period study_period_id, ");
		sbQuery.append(" t613.c613_study_period_ds period, t612.c612_study_form_ds crf, ");
		sbQuery.append(" gm_pkg_cm_query_info.gm_fch_event_des "); 
		sbQuery.append(" (t613.c613_study_period_id, t612.c601_form_id, t621.c621_patient_id, ");
		sbQuery.append(" t622.c622_patient_list_id ) event "); 
		sbQuery.append(" FROM t622_patient_list t622, t621_patient_master t621, t613_study_period t613, ");
		sbQuery.append(" t612_study_form_list t612, t614_study_site t614, t623_patient_answer_list t623, "); 
		sbQuery.append(" (SELECT   v621.c614_study_site_id, v621.c614_site_id, v621.c704_account_id, v621.c611_study_id "); 
		sbQuery.append(" FROM v621_patient_study_list v621 ");
		sbQuery.append(" GROUP BY v621.c614_study_site_id, v621.c614_site_id, v621.c704_account_id, v621.c611_study_id) v621 WHERE ");

		if (!strPatientListId.equals("")) {
			sbQuery.append(" t622.C622_PATIENT_LIST_ID = " + strPatientListId);
		} else if (!strPatientAnsListId.equals("")) {
			sbQuery.append(" t623.C623_PATIENT_ANS_LIST_ID = " + strPatientAnsListId);
		}

		sbQuery.append(" AND t623.c621_patient_id = t621.c621_patient_id " + " AND t623.c601_form_id = t612.c601_form_id ");
		sbQuery.append(" AND t623.c622_patient_list_id = t622.c622_patient_list_id " + " AND t622.c621_patient_id = t621.c621_patient_id ");
		sbQuery.append(" AND t622.c613_study_period_id = t613.c613_study_period_id " + " AND t622.c601_form_id = t612.c601_form_id ");
		sbQuery.append(" AND t621.c611_study_id = t612.c611_study_id " + " AND t621.c611_study_id = t614.c611_study_id ");
		sbQuery.append(" AND t621.c704_account_id = t614.c704_account_id " + " AND t622.c622_delete_fl = 'N' AND t621.c621_delete_fl = 'N' ");
		sbQuery.append(" AND t613.c613_delete_fl = 'N' " + " AND t612.c612_delete_fl = 'N' AND t614.c614_void_fl IS NULL ");
		sbQuery.append(" AND v621.c614_study_site_id = t614.c614_study_site_id ");

		if (!strCondition.equals("")) {
			sbQuery.append(" AND " + strCondition);
		}

		log.debug("Header Query basic : " + sbQuery.toString());
		hmQueryHeaderBasic = gmDBManager.querySingleRecord(sbQuery.toString());

		if (!strQueryId.equals("")) {

			// Query Info for the second part
			sbQuery.setLength(0);
			sbQuery.append(" SELECT T6510.C6510_QUERY_ID QUERY_ID, "); 
			sbQuery.append(" C901_QUERY_LEVEL_ID QUERY_LEVEL_ID, GET_CODE_NAME(T6510.C901_STATUS) STATUS ");
			sbQuery.append(" ,GET_USER_NAME(T6510.C6510_CREATED_BY) RAISED_BY ,TO_CHAR(T6510.C6510_CREATED_DATE,'mm/dd/yyyy') RAISED_DATE ");
			sbQuery.append(" ,DECODE(T6510.C901_STATUS, 92444,GET_USER_NAME(T6510.C6510_LAST_UPDATED_BY),'') CLOSED_BY ");
			sbQuery.append(" , DECODE(T6510.C901_STATUS, 92444, TO_CHAR(T6510.C6510_LAST_UPDATED_DATE,'mm/dd/yyyy'),'') CLOSED_DATE ");
			sbQuery.append(" FROM T6510_QUERY T6510, (SELECT v621.C614_STUDY_SITE_ID, v621.C614_SITE_ID, v621.C704_ACCOUNT_ID, v621.c611_study_id ");
			sbQuery.append(" from v621_patient_study_list v621 GROUP BY v621.C614_STUDY_SITE_ID, "); 
			sbQuery.append("v621.C614_SITE_ID, v621.C704_ACCOUNT_ID, v621.c611_study_id) V621 ");
			sbQuery.append(" WHERE t6510.C6510_QUERY_ID = " + strQueryId);  // query_id
			sbQuery.append(" AND t6510.C6510_VOID_FL IS NULL " + " AND V621.C614_STUDY_SITE_ID = T6510.C614_STUDY_SITE_ID ");

			if (!strCondition.equals("")) {
				sbQuery.append(" AND " + strCondition);
			}
			log.debug("Header Query Detail : " + sbQuery.toString());
			hmQueryHeaderDetail = gmDBManager.querySingleRecord(sbQuery.toString());

			sbQuery.setLength(0);
			sbQuery.append(" SELECT gm_pkg_cm_query_info.get_display_text (t6511.c6511_query_longdesc_id) ");
			sbQuery.append(" || ' by ' || GET_USER_NAME(T6511.C6511_LAST_UPDATED_BY) ");
			sbQuery.append(" || ' on ' || TO_CHAR(t6511.C6511_LAST_UPDATED_DATE, 'Mon DD, YYYY HH:MI AM') header "); 
			sbQuery.append(" , t6511.C6511_QUERY_TEXT TEXT ");
			sbQuery.append(" from  T6511_QUERY_LONGDESC T6511 , T6510_Query T6510 ");
			sbQuery.append(" ,(SELECT v621.C614_STUDY_SITE_ID, v621.C614_SITE_ID, v621.C704_ACCOUNT_ID, v621.c611_study_id from v621_patient_study_list v621 ");
			sbQuery.append(" GROUP BY v621.C614_STUDY_SITE_ID, v621.C614_SITE_ID,v621.C704_ACCOUNT_ID, v621.c611_study_id) V621 WHERE t6511.C6510_QUERY_ID = "); 
			sbQuery.append( strQueryId );// p_query_id
			sbQuery.append(" AND T6510.C6510_QUERY_ID = t6511.C6510_QUERY_ID " + " AND t6511.C6511_VOID_FL IS NULL ");
			sbQuery.append(" AND V621.C614_STUDY_SITE_ID = T6510.C614_STUDY_SITE_ID ");

			if (!strCondition.equals("")) {
				sbQuery.append(" AND " + strCondition);
			}

			sbQuery.append(" ORDER BY t6511.c6511_query_longdesc_id ");
			log.debug("Description Query: " + sbQuery.toString());
			alDescription = gmDBManager.queryMultipleRecords(sbQuery.toString());
		}
		hmResult = new HashMap();
		hmResult.put("HeaderBasic", hmQueryHeaderBasic);
		hmResult.put("HeaderDetail", hmQueryHeaderDetail);
		hmResult.put("Description", alDescription);
		gmDBManager.close();
		return hmResult;
	}

	/**
	 * saveQueryDetails - This method will
	 * 
	 * @param HashMap hmParam
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap saveQueryDetails(HashMap hmParam) throws AppError {
		log.debug("Enter saveQueryDetails");
		HashMap hmReturn = new HashMap();
		String strPatientListId = GmCommonClass.parseNull((String) hmParam.get("PATIENTLISTID"));
		String strPatientAnsListId = GmCommonClass.parseNull((String) hmParam.get("PATIENTANSLISTID"));
		String strQueryLevelId = GmCommonClass.parseNull((String) hmParam.get("QUERYLEVELID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		log.debug(hmParam);
		log.debug("strPatientAnsListId :: " + strPatientAnsListId);
		String strQueryId = GmCommonClass.parseNull((String) hmParam.get("QUERYID"));
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_cm_query_info.gm_sav_query_details", 7);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strQueryId);
		gmDBManager.setString(2, strQueryLevelId);
		gmDBManager.setString(3, strPatientListId);
		gmDBManager.setString(4, strPatientAnsListId);
		gmDBManager.setString(5, strUserId);
		gmDBManager.setString(6, strComments);
		gmDBManager.setString(7, strStatus);
		gmDBManager.execute();
		strQueryId = gmDBManager.getString(1);
		gmDBManager.commit();
		hmReturn.put("QUERYID", strQueryId);
		log.debug("Exit saveQueryDetails");
		return hmReturn;
	}

	/**
	 * common method used to get the grid xml data
	 * 
	 * @date Feb 24, 2010
	 * @param
	 */

	public String getXmlGridData(HashMap hmParamV) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();

		ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
		String strTemplate = (String) hmParamV.get("TEMPLATE");
		String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
		String strPatientListId = (String) hmParamV.get("patientListId");
		String strPatientAnsListId = (String) hmParamV.get("patientAnsListId");
		String strQueryLevelId = (String) hmParamV.get("queryLevelId");
		String strListQueries = (String) hmParamV.get("listQueries");

		HashMap hmParam = new HashMap();
		hmParam.put("patientListId", strPatientListId);
		hmParam.put("patientAnsListId", strPatientAnsListId);
		hmParam.put("queryLevelId", strQueryLevelId);
		hmParam.put("listQueries", strListQueries);
		log.debug("patientListId===" + strPatientListId);
		log.debug("patientAnsListId===" + strPatientAnsListId);

		templateUtil.setDataList("alResult", alParam);
		templateUtil.setDataMap("hmParam", hmParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);

		return templateUtil.generateOutput();
	}	
}
