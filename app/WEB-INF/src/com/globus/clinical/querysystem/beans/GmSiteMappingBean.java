package com.globus.clinical.querysystem.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmSiteMappingBean {
		Logger log = GmLogger.getInstance(this.getClass().getName());
	   /**
	    * Fetches the contact information for the party from db
	    * @param strPartyId
	    * @return RowSetDynaClass
	    * @throws com.globus.common.beans.AppError
	    * @roseuid 48F8F334027F
	    */
	   public RowSetDynaClass fetchSiteMappingSummary(String strPartyId) throws AppError 
	   {
	       RowSetDynaClass rsDynaResult = null;
	       GmDBManager gmDBManager = new GmDBManager ();
	       
	       gmDBManager.setPrepareString("gm_pkg_cr_sitemapping.gm_fch_site_mapping_summary",2);
	       gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
	       gmDBManager.setString(1, strPartyId);
	       gmDBManager.execute();
	       rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));
	       gmDBManager.close();

	       return rsDynaResult;
	   }

	   
	   /**
	    * Fetches the contact information for the party from db when user clicks on 'Edit'
	    * @param hContactId
	    * @return java.util.HashMap
	    * @throws com.globus.common.beans.AppError
	    * @roseuid 48F8F38000BA
	    */
	   public HashMap fetchEditSiteMappingInfo(int sitePartyId) throws AppError 
	   {
	       HashMap  hmReturn = new HashMap();
	       GmDBManager gmDBManager = new GmDBManager ();
	      
	       gmDBManager.setPrepareString("gm_pkg_cr_sitemapping.gm_fch_edit_mapping_info",2);
	       
	       gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
	       gmDBManager.setInt(1, sitePartyId);
	       gmDBManager.execute();
	       hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
	       gmDBManager.close();

	       return hmReturn;
	   }
	   
	   /**
	    * Method to save the contact information for the party
	    * @param hmParam
	    * @return java.lang.String
	    * @throws com.globus.common.beans.AppError
	    * @roseuid 4908D3E3005D
	    */
	   public String saveSiteMappingInfo(HashMap hmParam) throws AppError 
	   {
		   GmCommonBean gmCommonBean = new GmCommonBean();
		   GmDBManager gmDBManager = new GmDBManager ();

		   // String strPartyId = form.getPartyId(); assign the partyId to variable strPartyId. Similar operation for other
		   // variables
		   String strPartyId = GmCommonClass.parseNull((String)hmParam.get("PARTYID"));
		   String strStudyListId = GmCommonClass.parseNull((String)hmParam.get("STUDYLISTID"));
		   String strSiteListId = GmCommonClass.parseNull((String)hmParam.get("SITELISTID"));
		   String strPrimaryFlag = GmCommonClass.getCheckBoxValue((String)hmParam.get("PRIMARYFLAG"));
		   String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		   String strLog = GmCommonClass.parseNull((String)hmParam.get("TXT_LOGREASON"));
		   String strSitePartyId = GmCommonClass.parseNull((String)hmParam.get("SITEPARTYID"));
		   
		   log.debug("strPartyId  "+ strPartyId); 
		   log.debug("strStudyListId  "+ strStudyListId);
		   log.debug("strSiteListId  "+ strSiteListId);
		   log.debug("strPrimaryFlag  "+ strPrimaryFlag);
		   log.debug("strUserId  "+ strUserId);
		   log.debug("strLog  "+ strLog);
		   
		   gmDBManager.setPrepareString("gm_pkg_cr_sitemapping.gm_sav_sitemapping", 6);
		   gmDBManager.registerOutParameter(6,java.sql.Types.VARCHAR);

		   /*
		    * register out parameter and set input parameters
		    */

		   gmDBManager.setString(1, strPartyId);
		   gmDBManager.setString(2, strStudyListId);
		   gmDBManager.setString(3, strSiteListId);
		   gmDBManager.setString(4, strUserId);
		   gmDBManager.setString(5, strPrimaryFlag);
		   gmDBManager.setString(6, strSitePartyId);
		   gmDBManager.execute();

		   strSitePartyId = gmDBManager.getString(6);

		   if (!strLog.equals(""))
		   {
			   gmCommonBean.saveLog(gmDBManager, strSitePartyId, strLog, strUserId, "92091"); // 92051 is for contact
		   }

		   gmDBManager.commit();
		   return strSitePartyId;
	   }
	}

	

