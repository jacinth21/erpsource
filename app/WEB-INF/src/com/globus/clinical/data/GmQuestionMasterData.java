/*
 * Bean: GmQuestionMasterData.java
 * Author: Richard 
 * Project: Globus Medical
 * App Date-Written: 31 Oct 2005 
 * Security: 
 * Unclassified Description: This Bean will be used to store the 
 * 		Question  Information Along with the Answer and the other 
 * 		Associated object
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- 
 * mm/dd/yy xxx What you changed
 *  
 */

package com.globus.clinical.data;

import java.util.ArrayList;


public class GmQuestionMasterData {
	
	int QuestionID = 0;
	String QuestionDS = "";
	String QuestionTips = "";
	String DefaultValue = "";
	int ParentQuestionID = 0;
	int QuestionSeqNo = 0;
	boolean ecFlag = false;
	
	String QuestionDispNo="";

	public void setEcFlag(boolean ecFlag)
	{
		this.ecFlag = ecFlag;
	}
	
	public boolean getEcFlag()
	{
		return ecFlag;
	}
	/**
	 * @return the questionDispNo
	 */
	public String getQuestionDispNo() {
		return QuestionDispNo;
	}
	/**
	 * @param questionDispNo the questionDispNo to set
	 */
	public void setQuestionDispNo(String questionDispNo) {
		QuestionDispNo = questionDispNo;
	}
	/**
	 * @return Returns the questionSeqNo.
	 */
	public int getQuestionSeqNo() {
		return QuestionSeqNo;
	}
	/**
	 * @param questionSeqNo The questionSeqNo to set.
	 */
	public void setQuestionSeqNo(int questionSeqNo) {
		QuestionSeqNo = questionSeqNo;
	}
	String ParentQuestionEnableFlag = "";
	ArrayList AnswerGrp = new ArrayList();
	
	
	/**
	 * @return Returns the answerGrp.
	 */
	public ArrayList getAnswerGrp() {
		return AnswerGrp;
	}
	/**
	 * @param answerGrp The answerGrp to set.
	 */
	public void setAnswerGrp(ArrayList answerGrp) {
		AnswerGrp = answerGrp;
	}
	/**
	 * @return Returns the defaultValue.
	 */
	public String getDefaultValue() {
		return DefaultValue;
	}
	/**
	 * @param defaultValue The defaultValue to set.
	 */
	public void setDefaultValue(String defaultValue) {
		DefaultValue = defaultValue;
	}
	/**
	 * @return Returns the parentQuestionEnableFlag.
	 */
	public String getParentQuestionEnableFlag() {
		return ParentQuestionEnableFlag;
	}
	/**
	 * @param parentQuestionEnableFlag The parentQuestionEnableFlag to set.
	 */
	public void setParentQuestionEnableFlag(String parentQuestionEnableFlag) {
		ParentQuestionEnableFlag = parentQuestionEnableFlag;
	}
	/**
	 * @return Returns the parentQuestionID.
	 */
	public int getParentQuestionID() {
		return ParentQuestionID;
	}
	/**
	 * @param parentQuestionID The parentQuestionID to set.
	 */
	public void setParentQuestionID(int parentQuestionID) {
		ParentQuestionID = parentQuestionID;
	}
	/**
	 * @return Returns the questionDS.
	 */
	public String getQuestionDS() {
		return QuestionDS;
	}
	/**
	 * @param questionDS The questionDS to set.
	 */
	public void setQuestionDS(String questionDS) {
		QuestionDS = questionDS;
	}
	/**
	 * @return Returns the questionID.
	 */
	public int getQuestionID() {
		return QuestionID;
	}
	/**
	 * @param questionID The questionID to set.
	 */
	public void setQuestionID(int questionID) {
		QuestionID = questionID;
	}
	/**
	 * @return Returns the questionTips.
	 */
	public String getQuestionTips() {
		return QuestionTips;
	}
	/**
	 * @param questionTips The questionTips to set.
	 */
	public void setQuestionTips(String questionTips) {
		QuestionTips = questionTips;
	}
}
