/*
 * Bean: GmAnswerListData.java
 * Author: Richard 
 * Project: Globus Medical
 * App Date-Written: 31 Oct 2005 
 * Security: 
 * Unclassified Description: This Bean will be used to store the 
 * 		Answer List Information and the same will be listed in the combo value
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- 
 * mm/dd/yy xxx What you changed
 *  
 */

package com.globus.clinical.data;


public class GmAnswerListData {

	
	String AnswerListID = "";
	String AnswerListName = "";
	String AnswerGrp = "";
	int SeqNo = 0;
	boolean CommentsFlag = false;
	int AnswerValue = 0;
	
	/**
	 * @return Returns the answerListID.
	 */
	public String getAnswerListID() {
		return AnswerListID;
	}
	/**
	 * @param answerListID The answerListID to set.
	 */
	public void setAnswerListID(String answerListID) {
		AnswerListID = answerListID;
	}
	/**
	 * @return Returns the answerListName.
	 */
	public String getAnswerListName() {
		return AnswerListName;
	}
	/**
	 * @param answerListName The answerListName to set.
	 */
	public void setAnswerListName(String answerListName) {
		AnswerListName = answerListName;
	}
	/**
	 * @return Returns the answerValue.
	 */
	public int getAnswerValue() {
		return AnswerValue;
	}
	/**
	 * @param answerValue The answerValue to set.
	 */
	public void setAnswerValue(int answerValue) {
		AnswerValue = answerValue;
	}
	/**
	 * @return Returns the commentsFlag.
	 */
	public boolean isCommentsFlag() {
		return CommentsFlag;
	}
	/**
	 * @param commentsFlag The commentsFlag to set.
	 */
	public void setCommentsFlag(boolean commentsFlag) {
		CommentsFlag = commentsFlag;
	}
	/**
	 * @return Returns the seqNo.
	 */
	public int getSeqNo() {
		return SeqNo;
	}
	/**
	 * @param seqNo The seqNo to set.
	 */
	public void setSeqNo(int seqNo) {
		SeqNo = seqNo;
	}
}
