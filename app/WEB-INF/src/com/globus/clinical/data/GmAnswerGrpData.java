/*
 * Bean: GmAnswerGrpData.java
 * Author: Richard 
 * Project: Globus Medical
 * App Date-Written: 31 Oct 2005 
 * Security: 
 * Unclassified Description: This Bean will be used to store the 
 * 		Answer Group Information and the same will be used to display the answer
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- 
 * mm/dd/yy xxx What you changed
 *  
 */

package com.globus.clinical.data;

import java.util.ArrayList;


public class GmAnswerGrpData {
	
	
	String AnswerGrpID = "";
	String AnswerGrpDS = "";
	String AnswerGrpCD = "";
	int	QuestionID = 0;
	String ControlTypeID = "";
	
	String DefaultValue = "";
	String AnswerSeqNo = "";
	int Answer_Length = 0;

	/**********************************************
	 * This Bean Also holds Patient Answer List 
	 * If Already Answered by the patiend
	 **********************************************/
	int Patient_AnswerList_ID = 0;
	String Patient_AnswerList = "";
	String Patinet_AnswerDs = "";
	boolean HistoryFlag = false;
	boolean MandatoryFlag = true;
	String strFormValdiateFn = "";
	String strQueryStatus = "";
	boolean ecFlag = false;
	
	public boolean getEcFlag() {
		return ecFlag;
	}
	
	public void setEcFlag(boolean ecFlag) {
		this.ecFlag = ecFlag;
	}
	/**
	 * @return Returns the mandatoryFlag.
	 */
	public boolean isMandatoryFlag() {
		return MandatoryFlag;
	}
	/**
	 * @param mandatoryFlag The mandatoryFlag to set.
	 */
	public void setMandatoryFlag(boolean mandatoryFlag) {
		MandatoryFlag = mandatoryFlag;
	}
	/**
	 * @return Returns the answer_Length.
	 */
	public int getAnswer_Length() {
		return Answer_Length;
	}
	/**
	 * @param answer_Length The answer_Length to set.
	 */
	public void setAnswer_Length(int answer_Length) {
		Answer_Length = answer_Length;
	}
	/**
	 * @return Returns the patient_AnswerList_ID.
	 */
	public int getPatient_AnswerList_ID() {
		return Patient_AnswerList_ID;
	}
	/**
	 * @param patient_AnswerList_ID The patient_AnswerList_ID to set.
	 */
	public void setPatient_AnswerList_ID(int patient_AnswerList_ID) {
		Patient_AnswerList_ID = patient_AnswerList_ID;
	}
	
	/**
	 * @return Returns the answerGrpCD.
	 */
	public String getAnswerGrpCD() {
		return AnswerGrpCD;
	}
	/**
	 * @param answerGrpCD The answerGrpCD to set.
	 */
	public void setAnswerGrpCD(String answerGrpCD) {
		AnswerGrpCD = answerGrpCD;
	}
	/**
	 * @return Returns the answerGrpDS.
	 */
	public String getAnswerGrpDS() {
		return AnswerGrpDS;
	}
	/**
	 * @param answerGrpDS The answerGrpDS to set.
	 */
	public void setAnswerGrpDS(String answerGrpDS) {
		AnswerGrpDS = answerGrpDS;
	}
	/**
	 * @return Returns the answerGrpID.
	 */
	public String getAnswerGrpID() {
		return AnswerGrpID;
	}
	/**
	 * @param answerGrpID The answerGrpID to set.
	 */
	public void setAnswerGrpID(String answerGrpID) {
		AnswerGrpID = answerGrpID;
	}
	/**
	 * @return Returns the answerList.
	 */
	public ArrayList getAnswerList() {
		return AnswerList;
	}
	/**
	 * @param answerList The answerList to set.
	 */
	public void setAnswerList(ArrayList answerList) {
		AnswerList = answerList;
	}
	/**
	 * @return Returns the controlType.
	 */
	public String getControlType() {
		return ControlType;
	}
	/**
	 * @param controlType The controlType to set.
	 */
	public void setControlType(String controlType) {
		ControlType = controlType;
	}
	/**
	 * @return Returns the controlTypeID.
	 */
	public String getControlTypeID() {
		return ControlTypeID;
	}
	/**
	 * @param controlTypeID The controlTypeID to set.
	 */
	public void setControlTypeID(String controlTypeID) {
		ControlTypeID = controlTypeID;
	}
	/**
	 * @return Returns the questionID.
	 */
	public int getQuestionID() {
		return QuestionID;
	}
	/**
	 * @param questionID The questionID to set.
	 */
	public void setQuestionID(int questionID) {
		QuestionID = questionID;
	}
	String ControlType = "";
	ArrayList AnswerList = new ArrayList();	
	/**
	 * @return Returns the historyFlag.
	 */
	public boolean isHistoryFlag() {
		return HistoryFlag;
	}
	/**
	 * @param historyFlag The historyFlag to set.
	 */
	public void setHistoryFlag(boolean historyFlag) {
		HistoryFlag = historyFlag;
	}
	/**
	 * @return Returns the patient_AnswerList.
	 */
	public String getPatient_AnswerList() {
		return Patient_AnswerList;
	}
	/**
	 * @param patient_AnswerList The patient_AnswerList to set.
	 */
	public void setPatient_AnswerList(String patient_AnswerList) {
		Patient_AnswerList = patient_AnswerList;
	}
	/**
	 * @return Returns the patinet_AnswerDs.
	 */
	public String getPatinet_AnswerDs() {
		return Patinet_AnswerDs;
	}
	/**
	 * @param patinet_AnswerDs The patinet_AnswerDs to set.
	 */
	public void setPatinet_AnswerDs(String patinet_AnswerDs) {
		Patinet_AnswerDs = patinet_AnswerDs;
	}
	/**
	 * @return Returns the answerSeqNo.
	 */
	public String getAnswerSeqNo() {
		return AnswerSeqNo;
	}
	/**
	 * @param answerSeqNo The answerSeqNo to set.
	 */
	public void setAnswerSeqNo(String answerSeqNo) {
		AnswerSeqNo = answerSeqNo;
	}
	/**
	 * @return Returns the defaultValue.
	 */
	public String getDefaultValue() {
		return DefaultValue;
	}
	/**
	 * @param defaultValue The defaultValue to set.
	 */
	public void setDefaultValue(String defaultValue) {
		DefaultValue = defaultValue;
	}
	
	/**
	 * @return Returns the defaultValue.
	 */
	public String getFormValidateFn() {
		return strFormValdiateFn;
	}
	/**
	 * @param defaultValue The defaultValue to set.
	 */
	public void setFormValidateFn(String value) {
		strFormValdiateFn = value;
	}
	/**
	 * @return the strQueryStatus
	 */
	public String getQueryStatus() {
		return strQueryStatus;
	}
	/**
	 * @param strQueryStatus the strQueryStatus to set
	 */
	public void setQueryStatus(String strQueryStatus) {
		this.strQueryStatus = strQueryStatus;
	}	
	
	
	
}
