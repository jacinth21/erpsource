/*
 * Bean: GmStudyFormData.java
 * Author: Richard 
 * Project: Globus Medical
 * App Date-Written: 31 Oct 2005 
 * Security: 
 * Unclassified Description: This Bean will be used to store the 
 * 		Study Form List and the associated object
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- 
 * mm/dd/yy xxx What you changed
 *  
 */

package com.globus.clinical.data;

import java.util.ArrayList;


public class GmStudyFormData {
	String StudyListID = "";
	String StudyFormDs = "";
	String FormID = "";
	String FormName = "";
	String StudyListTypeID = "";
	String StudyListType = "";
	String XRayNameID = "";
	String XRayName = "";
	
	ArrayList alStudyPeriod = new ArrayList();
	
	/**
	 * @return Returns the studyListID.
	 */
	public String getStudyListID() {
		return StudyListID;
	}
	/**
	 * @param studyListID The studyListID to set.
	 */
	public void setStudyListID(String studyListID) {
		StudyListID = studyListID;
	}

	/**
	 * @return Returns the alStudyPeriod.
	 */
	public ArrayList getAlStudyPeriod() {
		return alStudyPeriod;
	}
	/**
	 * @param alStudyPeriod The alStudyPeriod to set.
	 */
	public void setAlStudyPeriod(ArrayList alStudyPeriod) {
		this.alStudyPeriod = alStudyPeriod;
	}
	/**
	 * @return Returns the formID.
	 */
	public String getFormID() {
		return FormID;
	}
	/**
	 * @param formID The formID to set.
	 */
	public void setFormID(String formID) {
		FormID = formID;
	}
	/**
	 * @return Returns the formName.
	 */
	public String getFormName() {
		return FormName;
	}
	/**
	 * @param formName The formName to set.
	 */
	public void setFormName(String formName) {
		FormName = formName;
	}
	/**
	 * @return Returns the studyFormDs.
	 */
	public String getStudyFormDs() {
		return StudyFormDs;
	}
	/**
	 * @param studyFormDs The studyFormDs to set.
	 */
	public void setStudyFormDs(String studyFormDs) {
		StudyFormDs = studyFormDs;
	}
	/**
	 * @return Returns the studyListType.
	 */
	public String getStudyListType() {
		return StudyListType;
	}
	/**
	 * @param studyListType The studyListType to set.
	 */
	public void setStudyListType(String studyListType) {
		StudyListType = studyListType;
	}
	/**
	 * @return Returns the xRayName.
	 */
	public String getXRayName() {
		return XRayName;
	}
	/**
	 * @param rayName The xRayName to set.
	 */
	public void setXRayName(String rayName) {
		XRayName = rayName;
	}
	/**
	 * @return Returns the studyListTypeID.
	 */
	public String getStudyListTypeID() {
		return StudyListTypeID;
	}
	/**
	 * @param studyListTypeID The studyListTypeID to set.
	 */
	public void setStudyListTypeID(String studyListTypeID) {
		StudyListTypeID = studyListTypeID;
	}
	/**
	 * @return Returns the xRayNameID.
	 */
	public String getXRayNameID() {
		return XRayNameID;
	}
	/**
	 * @param rayNameID The xRayNameID to set.
	 */
	public void setXRayNameID(String rayNameID) {
		XRayNameID = rayNameID;
	}
}