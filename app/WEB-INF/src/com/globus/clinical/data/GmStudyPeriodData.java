/*
 * Bean: GmAnswerGrpData.java
 * Author: Richard 
 * Project: Globus Medical
 * App Date-Written: 31 Oct 2005 
 * Security: 
 * Unclassified Description: This Bean will be used to store the 
 * 		Answer Group Information and the same will be used to display the answer
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- 
 * mm/dd/yy xxx What you changed
 *  
 */

package com.globus.clinical.data;

public class GmStudyPeriodData {
	String StudyPeriodID	= "";
	String PeriodDS 		= "";
	String GroupHeading 	= "";
	
	String ExpectedDate		= ""; // Expected date of Filling
	String FormType 		= ""; // One Time or Multiple Period
	String PeriodTypeNm 	= ""; //Holds Month/Day/Year Information
	
	String StudyListID = "";
	String StudyFormDs = "";
	String FormID = "";
	String FormName = "";
	
	int StudyDuration 		= 0;
	int GracePeriodBefore 	= 0;
	int GracePeriodAfter 	= 0;
	int StudyPhase = 0 ;
	
	String GracePeriodType = "";
	String FormReceivedDate = "";
	
	boolean FormVerifiedFlag = false;
	String FormVerifiedBy = "";
	
	boolean DueFlag = false;
	
	String PatientListID = "";
    
    private String earliestDate = "";
    
    private String latestDate = "";

	public String getEarliestDate()
    {
        return earliestDate;
    }
    public void setEarliestDate(String earliestDate)
    {
        this.earliestDate = earliestDate;
    }
    public String getLatestDate()
    {
        return latestDate;
    }
    public void setLatestDate(String latestDate)
    {
        this.latestDate = latestDate;
    }
    /**
	 * @return Returns the expectedDate.
	 */
	public String getExpectedDate() {
		return ExpectedDate;
	}
	/**
	 * @param expectedDate The expectedDate to set.
	 */
	public void setExpectedDate(String expectedDate) {
		ExpectedDate = expectedDate;
	}
	/**
	 * @return Returns the formReceivedDate.
	 */
	public String getFormReceivedDate() {
		return FormReceivedDate;
	}
	/**
	 * @param formReceivedDate The formReceivedDate to set.
	 */
	public void setFormReceivedDate(String formReceivedDate) {
		FormReceivedDate = formReceivedDate;
	}
	/**
	 * @return Returns the formType.
	 */
	public String getFormType() {
		return FormType;
	}
	/**
	 * @param formType The formType to set.
	 */
	public void setFormType(String formType) {
		FormType = formType;
	}
	/**
	 * @return Returns the formVerifiedFlag.
	 */
	public boolean isFormVerifiedFlag() {
		return FormVerifiedFlag;
	}
	/**
	 * @param formVerifiedFlag The formVerifiedFlag to set.
	 */
	public void setFormVerifiedFlag(boolean formVerifiedFlag) {
		FormVerifiedFlag = formVerifiedFlag;
	}
	/**
	 * @return Returns the formVerifiedBy.
	 */
	public String getFormVerifiedBy() {
		return FormVerifiedBy;
	}
	/**
	 * @param formVerifiedBy The formVerifiedBy to set.
	 */
	public void setFormVerifiedBy(String formVerifiedBy) {
		FormVerifiedBy = formVerifiedBy;
	}	
	/**
	 * @return Returns the gracePeriodAfter.
	 */
	public int getGracePeriodAfter() {
		return GracePeriodAfter;
	}
	/**
	 * @param gracePeriodAfter The gracePeriodAfter to set.
	 */
	public void setGracePeriodAfter(int gracePeriodAfter) {
		GracePeriodAfter = gracePeriodAfter;
	}
	/**
	 * @return Returns the gracePeriodBefore.
	 */
	public int getGracePeriodBefore() {
		return GracePeriodBefore;
	}
	/**
	 * @param gracePeriodBefore The gracePeriodBefore to set.
	 */
	public void setGracePeriodBefore(int gracePeriodBefore) {
		GracePeriodBefore = gracePeriodBefore;
	}
	/**
	 * @return Returns the groupHeading.
	 */
	public String getGroupHeading() {
		return GroupHeading;
	}
	/**
	 * @param groupHeading The groupHeading to set.
	 */
	public void setGroupHeading(String groupHeading) {
		GroupHeading = groupHeading;
	}
	/**
	 * @return Returns the patientListID.
	 */
	public String getPatientListID() {
		return PatientListID;
	}
	/**
	 * @param patientListID The patientListID to set.
	 */
	public void setPatientListID(String patientListID) {
		PatientListID = patientListID;
	}
	/**
	 * @return Returns the periodDS.
	 */
	public String getPeriodDS() {
		return PeriodDS;
	}
	/**
	 * @param periodDS The periodDS to set.
	 */
	public void setPeriodDS(String periodDS) {
		PeriodDS = periodDS;
	}
	/**
	 * @return Returns the periodType.
	 */
	public String getPeriodTypeNm() {
		return PeriodTypeNm;
	}
	/**
	 * @param periodType The periodType to set.
	 */
	public void setPeriodTypeNm(String periodType) {
		PeriodTypeNm = periodType;
	}
	/**
	 * @return Returns the studyDuration.
	 */
	public int getStudyDuration() {
		return StudyDuration;
	}
	/**
	 * @param studyDuration The studyDuration to set.
	 */
	public void setStudyDuration(int studyDuration) {
		StudyDuration = studyDuration;
	}
	
	/**
	 * @return Returns the studyPeriodID.
	 */
	public String getStudyPeriodID() {
		return StudyPeriodID;
	}
	/**
	 * @param studyPeriodID The studyPeriodID to set.
	 */
	public void setStudyPeriodID(String studyPeriodID) {
		StudyPeriodID = studyPeriodID;
	}
	/**
	 * @return Returns the graceTypePeriod.
	 */
	public String getGracePeriodType() {
		return GracePeriodType;
	}
	/**
	 * @param graceTypePeriod The graceTypePeriod to set.
	 */
	public void setGracePeriodType(String graceTypePeriod) {
		GracePeriodType = graceTypePeriod;
	}
	/**
	 * @return Returns the dueFlag.
	 */
	public boolean isDueFlag() {
		return DueFlag;
	}
	/**
	 * @param dueFlag The dueFlag to set.
	 */
	public void setDueFlag(boolean dueFlag) {
		DueFlag = dueFlag;
	}
	/**
	 * @return Returns the studyPhase.
	 */
	public int getStudyPhase() {
		return StudyPhase;
	}
	/**
	 * @param studyPhase The studyPhase to set.
	 */
	public void setStudyPhase(int studyPhase) {
		StudyPhase = studyPhase;
	}
	/**
	 * @return the studyListID
	 */
	public String getStudyListID() {
		return StudyListID;
	}
	/**
	 * @param studyListID the studyListID to set
	 */
	public void setStudyListID(String studyListID) {
		StudyListID = studyListID;
	}
	/**
	 * @return the studyFormDs
	 */
	public String getStudyFormDs() {
		return StudyFormDs;
	}
	/**
	 * @param studyFormDs the studyFormDs to set
	 */
	public void setStudyFormDs(String studyFormDs) {
		StudyFormDs = studyFormDs;
	}
	/**
	 * @return the formID
	 */
	public String getFormID() {
		return FormID;
	}
	/**
	 * @param formID the formID to set
	 */
	public void setFormID(String formID) {
		FormID = formID;
	}
	/**
	 * @return the formName
	 */
	public String getFormName() {
		return FormName;
	}
	/**
	 * @param formName the formName to set
	 */
	public void setFormName(String formName) {
		FormName = formName;
	}
}