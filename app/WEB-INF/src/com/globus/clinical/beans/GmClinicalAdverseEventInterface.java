package com.globus.clinical.beans;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.AppError;
/* fetching the Adverse Event report interface method declaration*/
public interface GmClinicalAdverseEventInterface {
	
	public ArrayList loadAdverseEvent(String strStudyId, String strSiteId, String strPatientId, String strOpt, String strTimePoint) throws AppError;
	
}
