/*
 * Bean: GmQuestionBean.java
 * Author: Richard 
 * Project: Globus Medical
 * App Date-Written: 31 Oct 2005 
 * Security: 
 * Unclassified Description: This Bean will be used to store the 
 * 		Question Fetch Information
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- 
 * mm/dd/yy xxx What you changed
 *  
 */

package com.globus.clinical.beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;

//Data Structure Bean
import com.globus.clinical.data.GmAnswerGrpData;
import com.globus.clinical.data.GmAnswerListData;
import com.globus.clinical.data.GmQuestionMasterData;


public class GmQuestionBean {
	/**
 	  * loadInvoiceDetails - This Bean  gets Question  information for the selected Form and for the selected 
 	  * 					 Patient  
 	**/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	// Below 2 methos need to be removed
	//public static void main(String[] args) throws Exception {
	//	GmQuestionBean  t = new GmQuestionBean ();
	//}

	//public GmQuestionBean () throws Exception 
	//{
	//	loadQuestionInformation("1","1","1");
	//}
	
	/**
	  * loadQuestionInformation - This Method gets Question information for the selected PO and for the selected 
	  * 					 Account  
	  * @param String strPO
	  * @param String strAccId
	  * @param String strAction 
	  * @return HashMap
	  * @exception AppError
	**/
	public HashMap loadQuestionInformation(String strRuleFormId,String strPatientID, 
			String strFormID, String strPeriod, String strEventNo, String strFormDate,String strPatListId,String strIncVoidFrm) throws AppError
	{
		HashMap hmResult = new HashMap();
		ArrayList alQuestionList = new ArrayList();
		ArrayList alAnswerGrp = new ArrayList();
		ArrayList alAnswerList = new ArrayList();
		boolean bEcFlag = false;
		// Data Structure Bean
		GmQuestionMasterData gmQuestion;
		GmAnswerGrpData gmAnswerGpData;
		GmAnswerListData gmAnswerLtData;
		
		//Database connection object
		Connection conn = null;
		Statement stmt 	= null;
		Statement stmtII 	= null;
		DBConnectionWrapper dbCon = null;
		
		ResultSet rsQuestion 	= null;
		ResultSet rsAnswerGroup = null;
		
		// Flag used by the query
		boolean blAnswerGrpFlag = false;
		boolean blValueOnHand = false;
		int intQuestionID = 0;
		int intTempQuestion = 0;
		String strTemp = "";
		
		try{
				
			dbCon = new DBConnectionWrapper();
			//conn = dbCon.getOracleStaticConnection();
			conn = dbCon.getConnection();
			stmt = conn.createStatement();
			stmtII = conn.createStatement();
			
			
			StringBuffer sbQuery = new StringBuffer();
			
			// Master Question Information
			sbQuery.setLength(0);
			sbQuery.append(" SELECT T601.C601_FORM_ID " ); 
			sbQuery.append(" ,T604.C602_QUESTION_ID" );
			sbQuery.append(" ,T602.C602_QUESTION_DS" ); 
			sbQuery.append(" ,C602_QUESTION_TIPS" );
			sbQuery.append(" ,C602_PARENT_QUESTION_ID" );
			sbQuery.append(" ,C602_PARENT_QUES_ENABLE_VALUE" );
			sbQuery.append(" ,T604.C604_SEQ_NO" );		
			sbQuery.append(" ,T604.C604_DISP_QUES_NO");
			sbQuery.append(" FROM   T601_FORM_MASTER T601, " );
			sbQuery.append(" T604_FORM_QUES_LINK T604," );
			sbQuery.append(" T602_QUESTION_MASTER T602 " );
			sbQuery.append(" WHERE T601.C601_FORM_ID 	= " );
			sbQuery.append(strFormID);
			sbQuery.append(" AND	  T601.C601_FORM_ID    	= T604.C601_FORM_ID" );
			sbQuery.append(" AND	  T602.C602_QUESTION_ID = T604.C602_QUESTION_ID" );
			sbQuery.append(" AND	  T601.C601_DELETE_FL = 'N'" );
			sbQuery.append(" AND	  T602.C602_DELETE_FL = 'N'" );
			sbQuery.append(" AND	  T604.C604_DELETE_FL = 'N'" );
			sbQuery.append(" ORDER BY  T604.C604_SEQ_NO" );

			rsQuestion = stmt.executeQuery(sbQuery.toString());				
			
			// Answer Grp Infomration
			sbQuery.setLength(0);
			sbQuery.append(" SELECT  T603.C602_QUESTION_ID ");
			sbQuery.append(" ,T603.C603_ANSWER_GRP_ID ");
			sbQuery.append(" ,T603.C603_ANSWER_GRP_DS ");
			sbQuery.append(" ,T603.C904_ANSWER_GRP   ");      
			sbQuery.append(" ,T603.C603_DEFAULT_VALUE  ");
			sbQuery.append(" ,T603.C901_CONTROL_TYPE  ");
			sbQuery.append(" ,GET_CODE_NAME(T603.C901_CONTROL_TYPE)  ");
			sbQuery.append(" ,T603.C603_ANSWER_SEQ_NO  ");
			sbQuery.append(" ,TEMP.C605_ANSWER_LIST_ID ");
			sbQuery.append(" ,TEMP.C904_ANSWER_DS ");
			sbQuery.append(" ,NVL(TEMP.C623_HISTORY_FLAG,'N') ");
			sbQuery.append(" ,C603_CONTROL_LENGTH ");
			sbQuery.append(" ,TEMP.C623_PATIENT_ANS_LIST_ID ");
			sbQuery.append(" ,T603.C603_MANDATORY_FLAG ");
			sbQuery.append(" ,T603.C603_VALIDATE_FN ");
			sbQuery.append(" ,TEMP.EC_FLAG ");
			sbQuery.append(" ,gm_pkg_cm_query_info.get_query_status (TEMP.C623_PATIENT_ANS_LIST_ID, 92422) STATUS ");
			// From Section starts...
			sbQuery.append(" FROM	T603_ANSWER_GRP T603  ");
			sbQuery.append(" ,T604_FORM_QUES_LINK T604 ");
			// Query to Fetch the Answer Information
			// *************************************
			sbQuery.append(" ,(SELECT   C623_PATIENT_ANS_LIST_ID, T623.C623_EDIT_CHECK_FL EC_FLAG ");
			sbQuery.append(" ,C603_ANSWER_GRP_ID ");   
			sbQuery.append(" ,C605_ANSWER_LIST_ID  ");
			sbQuery.append(" ,C904_ANSWER_DS  ");	 
			sbQuery.append(" ,C623_HISTORY_FLAG  ");
			sbQuery.append(" FROM	 T623_PATIENT_ANSWER_LIST T623 ");
			sbQuery.append(" ,T622_PATIENT_LIST T622 ");
			sbQuery.append(" WHERE	T622.C621_PATIENT_ID = ");
			sbQuery.append(strPatientID);
			sbQuery.append(" AND  T622.C613_STUDY_PERIOD_ID = ");
			sbQuery.append(strPeriod);
			if(!strEventNo.equals("") && !strEventNo.equals("0"))
            {
                sbQuery.append(" AND T622.C622_patient_event_no = '"+strEventNo+"'");
            } else if(!strFormDate.equals(""))
            {
                sbQuery.append(" AND T622.C622_DATE = TO_DATE ('"+strFormDate+"', 'MM/DD/YYYY') ");
            }
			/*This condition will be applied to 2 Years completion form for Flexus,Secure C Study.
			 * and Study Completion form in TRIUMPH, Surgical Intervention,Patient Disposition forms in ACADIA.
			 */
            else if (strFormID.equals(strRuleFormId) &&!strPatListId.equals("")){
            	sbQuery.append(" AND  T622.C622_PATIENT_LIST_ID ='"+strPatListId+"'");
            }			
            else
            {
                sbQuery.append(" AND T622.C613_STUDY_PERIOD_ID NOT IN  (SELECT C613_STUDY_PERIOD_ID FROM T613_STUDY_PERIOD WHERE C901_STUDY_PERIOD = '6309') ");
            }
            
			sbQuery.append(" AND  T622.C622_PATIENT_LIST_ID = T623.C622_PATIENT_LIST_ID ");
			
			/* When user is coming from "voided forms" report,we need to fetch the record for the specific patient list id,
			 * as there could be multiple forms (for same patient,TP,Form) the user might have voided . This condition will
			 * ensure the exact voided form will be displayed.
			*/
			if (strIncVoidFrm.equals("Y")){
				sbQuery.append(" AND  T622.C622_PATIENT_LIST_ID ='"+strPatListId+"'");
			}
			sbQuery.append(" AND  T623.C623_DELETE_FL = '"+strIncVoidFrm+"' ");
			sbQuery.append(" AND  T622.C622_DELETE_FL = '"+strIncVoidFrm+"' ");
			
			sbQuery.append(" ) TEMP ");
			//Query Ends Here
			sbQuery.append(" WHERE	T604.C601_FORM_ID 	= ");
			sbQuery.append(strFormID );
			sbQuery.append(" AND 	T603.C602_QUESTION_ID = T604.C602_QUESTION_ID  ");
			sbQuery.append(" AND	T603.C603_ANSWER_GRP_ID  = TEMP.C603_ANSWER_GRP_ID (+)");
            sbQuery.append(" AND    T603.C603_DELETE_FL  = 'N'");
			sbQuery.append(" ORDER BY   T604.C604_SEQ_NO, T603.c603_answer_seq_no,T603.C603_ANSWER_GRP_ID ");
			log.debug("loadQuestionInformation : === "+sbQuery.toString());
			rsAnswerGroup = stmtII.executeQuery(sbQuery.toString());
			
			// Below Code will fill the Question Data Structure value
			// With the associated Data and the answer from the patient
			while (rsQuestion.next())
			{
				gmQuestion = new GmQuestionMasterData();
				
				intQuestionID = rsQuestion.getInt(2);
				gmQuestion.setQuestionID(intQuestionID);
				gmQuestion.setQuestionDS(rsQuestion.getString(3));
				gmQuestion.setQuestionTips(rsQuestion.getString(4));
				gmQuestion.setParentQuestionID(rsQuestion.getInt(5));
				gmQuestion.setParentQuestionEnableFlag(rsQuestion.getString(6));
				gmQuestion.setQuestionSeqNo(rsQuestion.getInt(7));
				gmQuestion.setQuestionDispNo(rsQuestion.getString(8));
				
				alAnswerGrp = new ArrayList();
				blAnswerGrpFlag = true;
				
				// Below While Loop will fetch the answer Information
				while (blAnswerGrpFlag)
				{
					// If the record is last row then exit from the loop
					
					if (blValueOnHand == false)
					{
						if (rsAnswerGroup.next() == false)
						{
							break;
						}
						blValueOnHand = true;
					}
					intTempQuestion = rsAnswerGroup.getInt(1);
					if (intTempQuestion == intQuestionID )
					{
						gmAnswerGpData = new GmAnswerGrpData();
						
						gmAnswerGpData.setAnswerGrpID(rsAnswerGroup.getString(2));
						gmAnswerGpData.setAnswerGrpDS(rsAnswerGroup.getString(3));
						gmAnswerGpData.setAnswerGrpCD(rsAnswerGroup.getString(4));
						gmAnswerGpData.setDefaultValue(rsAnswerGroup.getString(5));
						gmAnswerGpData.setControlTypeID(rsAnswerGroup.getString(6));
						gmAnswerGpData.setControlType(rsAnswerGroup.getString(7));
						gmAnswerGpData.setAnswerSeqNo(rsAnswerGroup.getString(8));
						// Answer replayed by the patient
						gmAnswerGpData.setPatient_AnswerList(rsAnswerGroup.getString(9));
						gmAnswerGpData.setPatinet_AnswerDs(rsAnswerGroup.getString(10));
						strTemp = rsAnswerGroup.getString(11);
						if (strTemp.equals("Y"))
						{
							gmAnswerGpData.setHistoryFlag(true);
						}
						gmAnswerGpData.setAnswer_Length(rsAnswerGroup.getInt(12));
						gmAnswerGpData.setPatient_AnswerList_ID(rsAnswerGroup.getInt(13));
						// To flag the mandatory field information		
						strTemp = rsAnswerGroup.getString(14);
						if (strTemp.equals("Y"))
						{
							gmAnswerGpData.setMandatoryFlag(false);
						}
						gmAnswerGpData.setFormValidateFn(rsAnswerGroup.getString(15));
						gmAnswerGpData.setQueryStatus(rsAnswerGroup.getString("STATUS"));
						String strEcFlag = GmCommonClass.parseNull(rsAnswerGroup.getString("EC_FLAG"));
						//System.out.println("strEcFlag : "+ strEcFlag);
						if(strEcFlag.equals("Y"))
						{
							gmQuestion.setEcFlag(true);
							gmAnswerGpData.setEcFlag(true);
						}
						alAnswerGrp.add(gmAnswerGpData);
						blValueOnHand = false;
					}
					else {
						blAnswerGrpFlag = false;
					}
					
				}
				
				gmQuestion.setAnswerGrp(alAnswerGrp);
				alQuestionList.add(gmQuestion);
			}
			hmResult.put("QUESTIONLIST",alQuestionList);
        }catch (Exception e)
        {
            // If Form ID is NULL(when Form Data Entry Left Menu link is clicked) than error will be caught
        	log.debug(e);
        }
        finally
        {
            try
            {
                if (rsQuestion != null)
                {
                    rsQuestion.close();
                }
                if (rsAnswerGroup != null)
                {
                    rsAnswerGroup.close();
                }               
                if (stmt != null)
                {
                    stmt.close();
                }
                if (stmtII != null)
                {
                    stmtII.close();
                }               
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                throw new AppError(e);
            }
        }
            
            try{
			
			//*********************************************
			//	Answer Grp List Infomration Combo Box value
			//*********************************************
            StringBuffer sbQuery = new StringBuffer();
			sbQuery.setLength(0);
			sbQuery.append(" SELECT  C605_ANSWER_LIST_ID ");
			sbQuery.append(" ,C605_ANSWER_LIST_NM  ");
			sbQuery.append(" ,C605_ANSWER_GRP  ");
			sbQuery.append(" ,C605_SEQ_NO  ");
			sbQuery.append(" ,C605_CMT_REQUIRED_FL "); 
			sbQuery.append(" ,C605_ANSWER_VALUE  ");  
			sbQuery.append(" FROM 	T605_ANSWER_LIST ");
			sbQuery.append(" WHERE  C605_ANSWER_GRP  IN  ");
			sbQuery.append(" ( SELECT  T603.C904_ANSWER_GRP  ");       
			sbQuery.append(" FROM	T603_ANSWER_GRP T603, ");
			sbQuery.append(" T604_FORM_QUES_LINK T604 ");
			sbQuery.append(" WHERE	T603.C602_QUESTION_ID  = T604.C602_QUESTION_ID "); 
			sbQuery.append(" AND 	T604.C601_FORM_ID 	= ");
			sbQuery.append(strFormID);
			sbQuery.append(" AND	T603.C603_DELETE_FL = 'N' ");
			sbQuery.append(" AND	T604.C604_DELETE_FL = 'N' ");
			sbQuery.append(" ) ");
			sbQuery.append(" ORDER BY C605_SEQ_NO ");
			//alAnswerList = dbCon.queryMultipleRecords(sbQuery.toString(),conn);
			alAnswerList = dbCon.queryMultipleRecords(sbQuery.toString());
			hmResult.put("ANSWERLIST",alAnswerList);
			
			//###############################################
			// Below code is used to test the value -- need
			// to delete the same
			//###############################################
			/* System.out.println("Final ArrayList Value");
			System.out.println("*********************");
			ArrayList alTemp = new ArrayList();
			for (int i=0;i<alQuestionList.size();i++)
			{
				gmQuestion = (GmQuestionMasterData) alQuestionList.get(i);
				System.out.println( gmQuestion.getQuestionSeqNo() + " " + gmQuestion.getQuestionDS());
				
				alTemp = gmQuestion.getAnswerGrp();
				for (int j=0;j<alTemp.size();j++)
				{
					gmAnswerGpData = (GmAnswerGrpData) alTemp.get(j);
					
					System.out.print( " -- " + gmAnswerGpData.getControlType()+  " " + gmAnswerGpData.getPatient_AnswerList() + " "  );
					if (gmAnswerGpData.getAnswerGrpCD() == null)
					{	
						System.out.println(gmAnswerGpData.getAnswerGrpDS() );
					}
					else{
						System.out.println( gmAnswerGpData.getAnswerGrpCD() );
					}
				}
				
			}*/
			
			
		}catch (Exception e)
		{
			// If Form ID is NULL(when Form Data Entry Left Menu link is clicked) than error will be caught
        	log.debug(e);
		}
		
		return hmResult;
	} //End of loadInvoiceDetails


}