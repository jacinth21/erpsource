package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.beans.GmFooterCalcuationBean;

public class GmEnrollmentRptBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public HashMap reportEnrollSiteByMonth(String strStudyId, String strFromDate, String strToDate) throws AppError {
		HashMap hmReturn = new HashMap();
		ArrayList alSiteSummary = new ArrayList();
		HashMap hmLinkDetails = new HashMap();
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		GmDBManager gmDBManager = new GmDBManager();

		try {
			gmDBManager.setPrepareString("GM_PKG_CR_ENROLL_RPT.GM_CR_FCH_ENROLL_SITE_MONTH", 6);
			gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);

			gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));
			gmDBManager.setString(2, GmCommonClass.parseNull(strFromDate));
			gmDBManager.setString(3, GmCommonClass.parseNull(strToDate));

			gmDBManager.execute();
			hmReturn = gmCrossReport.GenerateCrossTabReport((ResultSet) gmDBManager.getObject(4), (ResultSet) gmDBManager.getObject(5));
			alSiteSummary = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
			hmLinkDetails = getLinkInfo("SITE");
			hmReturn = gmCrossReport.linkCrossTab(hmReturn, alSiteSummary, hmLinkDetails);
			// hmReturn = modifyFooterColumn(hmReturn);
			gmDBManager.close();
		} catch (Exception exp) {
			gmDBManager.close();
			throw new AppError(exp);
		}

		return hmReturn;
	}

	public HashMap reportEnrollByTreatment(String strStudyId) throws AppError {
		HashMap hmReturn = new HashMap();
		ArrayList alSiteSummary = new ArrayList();
		HashMap hmLinkDetails = new HashMap();
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		GmDBManager gmDBManager = new GmDBManager();
		try {
			gmDBManager.setPrepareString("GM_PKG_CR_ENROLL_RPT.GM_CR_FCH_ENROLL_TREATMENT", 4);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

			gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));

			gmDBManager.execute();
			hmReturn = gmCrossReport.GenerateCrossTabReport((ResultSet) gmDBManager.getObject(2), (ResultSet) gmDBManager.getObject(3));
			alSiteSummary = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
			hmLinkDetails = getLinkInfo("SITE");
			hmReturn = gmCrossReport.linkCrossTab(hmReturn, alSiteSummary, hmLinkDetails);

			gmDBManager.close();
		} catch (Exception exp) {
			gmDBManager.close();
			throw new AppError(exp);
		}

		return hmReturn;
	}

	public List reportSurgeryByDate(HashMap hmParam, String strCondition) throws AppError {
		log.debug("Enter");
		ArrayList alResult = null;

		String strStudyID = GmCommonClass.parseNull((String) hmParam.get("STUDYID"));
		String strSiteNames = GmCommonClass.parseNull((String) hmParam.get("SITENAMES"));
		String strTreatement = GmCommonClass.parseNull((String) hmParam.get("TREATMENT"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));

		log.debug("hmParam:" + hmParam);
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.setLength(0);
		sbQuery.append(" SELECT t621.c621_patient_ide_no ID, t611.c611_study_id studyid, t614.c614_SITE_ID siteid,");
		sbQuery.append(" get_cr_fch_prinicipal_inv (t614.c614_study_site_id) pinv,");
		sbQuery.append(" get_party_name (c101_surgeon_id) sname,");
		sbQuery.append(" get_code_name (t621.c901_surgery_type) treatment,");
		sbQuery.append(" TO_CHAR (c621_surgery_date, 'MM/DD/YYYY') sdate,");
		sbQuery.append(" get_log_flag (t621.c621_patient_ide_no, 1229) clog");
		sbQuery.append(" FROM t621_patient_master t621, t614_study_site t614, t611_clinical_study t611 ");
		sbQuery.append(" WHERE t621.c611_study_id = t614.c611_study_id");
		sbQuery.append(" AND t621.c704_account_id = t614.c704_account_id");
		sbQuery.append(" AND t611.c611_study_id = t614.c611_study_id");
		sbQuery.append(" AND c621_surgery_date IS NOT NULL");
		sbQuery.append(" AND c621_delete_fl = 'N'");
		sbQuery.append(" AND t614.c614_void_fl IS NULL");
		sbQuery.append(" AND t621.c704_account_id IS NOT NULL");
		if(!strSiteNames.equals(""))
		{
			sbQuery.append(" AND t614.c614_study_site_id IN (" + strSiteNames);
			sbQuery.append(" )");
		}
		sbQuery.append(" AND t621.c611_study_id = '" + strStudyID + "'");
		sbQuery.append(" AND t621.c901_surgery_type =");
		sbQuery.append(" DECODE ('" + strTreatement + "',");
		sbQuery.append(" '', t621.c901_surgery_type,");
		sbQuery.append(" '" + strTreatement + "'");
		sbQuery.append(" )");
		sbQuery.append(" AND NVL (t621.c621_surgery_comp_fl, -999) =");
		sbQuery.append(" DECODE ('" + strStatus + "',");
		sbQuery.append(" '60110', 'Y',");
		sbQuery.append(" '60111', -999,");
		sbQuery.append(" NVL (t621.c621_surgery_comp_fl, -999)");
		sbQuery.append(" )");

		if (!strCondition.equals("")) {
			sbQuery.append(" and t611.C611_STUDY_ID in (select v621.C611_STUDY_ID");
			sbQuery.append(" from V621_PATIENT_STUDY_LIST v621 where");
			sbQuery.append(strCondition);
			sbQuery.append(")");
		}
		sbQuery.append(" ORDER BY t621.c621_patient_ide_no");

		log.debug("query===" + sbQuery.toString());
		alResult = dbCon.queryMultipleRecords(sbQuery.toString());
		return alResult;
	}

	/**
	 * getLinkInfo - This Method is used to to get the link object information
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	private HashMap getLinkInfo(String strType) {
		HashMap hmLinkDetails = new HashMap();
		HashMap hmapValue = new HashMap();
		ArrayList alLinkList = new ArrayList();

		// First parameter holds the link object name
		hmLinkDetails.put("LINKID", "ID");
		hmLinkDetails.put("POSITION", "AFTER");

		if (strType.equals("SITE")) {

			hmLinkDetails.put("POSITION", "BEFORE");
			// Parameter to be added to link list

			hmapValue = new HashMap();
			hmapValue.put("KEY", "SNAME");
			hmapValue.put("VALUE", "Site Name");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "PINV");
         hmapValue.put("VALUE", "Principal Investigator");    
			alLinkList.add(hmapValue);

		}
		hmLinkDetails.put("LINKVALUE", alLinkList);

		return hmLinkDetails;
	}

	private HashMap modifyFooterColumn(HashMap hmReturn) {
		GmFooterCalcuationBean calcuationBean = null;
		ArrayList alDataStore = new ArrayList();
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();

		calcuationBean = new GmFooterCalcuationBean();
		calcuationBean.setColumnNameToModify("Site ID");
		calcuationBean.setOperator("MAKENULL");
		alDataStore.add(calcuationBean);

		hmReturn = gmCrossReport.reCalculateFooter(hmReturn, alDataStore,"");

		return hmReturn;
	}

	/**
	 * common method used to get the grid xml data
	 * 
	 * @date Feb 24, 2010	
	 * @param
	 */
	public String getXmlGridData(HashMap hmParamV) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
		String strTemplate = (String) hmParamV.get("TEMPLATE");
		String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
		String strStudyList = (String) hmParamV.get("STUDYLIST");

		HashMap hmParam = new HashMap();
		hmParam.put("strStudyList", strStudyList);

		templateUtil.setDataList("alResult", alParam);
		templateUtil.setDataMap("strStudyList", hmParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}

}
