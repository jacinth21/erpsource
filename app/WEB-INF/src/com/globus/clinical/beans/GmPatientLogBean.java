/*
 * 
 *  
 */

package com.globus.clinical.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.util.GmTemplateUtil;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmPatientLogBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.
    
    /**
	 * Method to save the site monitoring records to db
	 * 
	 * @param hmParam
	 * @return java.lang.String - 
	 * @throws com.globus.common.beans.AppError
	 */
	public String savePatientLog(HashMap hmParam) throws AppError {
		//GmCommonBean gmCommonBean = new GmCommonBean();
		GmDBManager gmDBManager = new GmDBManager();
		
		
    	String strPatientLId = GmCommonClass.parseZero((String) hmParam.get("PATIENTLID"));
    	String strPatienId = GmCommonClass.parseZero((String) hmParam.get("PATIENTID"));
    	String strStudyPeriod = GmCommonClass.parseZero((String) hmParam.get("STUDYPERIOD"));
		String strLogDetail = GmCommonClass.parseNull((String) hmParam.get("LOGDETAIL"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strStudyForm = GmCommonClass.parseZero((String) hmParam.get("STUDYFORM"));
		
		gmDBManager.setPrepareString("gm_pkg_cr_patient_log.gm_sav_patient_log", 6);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		
		gmDBManager.setString(1, strPatientLId);
		gmDBManager.setString(2, strPatienId);
		gmDBManager.setString(3, strStudyPeriod);
		gmDBManager.setString(4, strLogDetail);
		gmDBManager.setString(5, strUserId);
		gmDBManager.setString(6, strStudyForm);
		
		gmDBManager.execute();
		strPatientLId = gmDBManager.getString(1);
		
		gmDBManager.commit();
		
		return strPatientLId;
	}
	
	/**
	 * fetchPatientLogSummary - This method will be used for retrieving all the
	 * comments for that site and patient.
	 * 
	 * @param hmParam
	 * @return ArrayList - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchPatientLogSummary(HashMap hmParam) throws AppError {
		ArrayList alList = new ArrayList();
		
		String strPatienId = GmCommonClass.parseZero((String) hmParam.get("PATIENTID"));
    	String strStudyPeriod = GmCommonClass.parseZero((String) hmParam.get("STUDYPERIOD"));
		String strStudyForm = GmCommonClass.parseZero((String) hmParam.get("STUDYFORM"));
		String strStudy = GmCommonClass.parseNull((String) hmParam.get("STUDY_ID"));
		String strStudySiteId = GmCommonClass.parseZero((String) hmParam.get("STUDYSITEID"));
		String strLogFrom = GmCommonClass.parseNull((String) hmParam.get("LOGFROM"));;
		String strLogTo = GmCommonClass.parseNull((String) hmParam.get("LOGTO"));
		strPatienId = strPatienId.equals("0")? GmCommonClass.parseNull((String) hmParam.get("SESSPATIENTPKEY")) :strPatienId;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_patient_log.gm_fch_patient_log_summary", 8);
		gmDBManager.setString(1, strPatienId);
		gmDBManager.setInt(2, Integer.parseInt(strStudyPeriod));
		gmDBManager.setInt(3, Integer.parseInt(strStudyForm));
		gmDBManager.setString(4, strStudy);
		gmDBManager.setInt(5, Integer.parseInt(strStudySiteId));
		gmDBManager.setString(6, strLogFrom);
		gmDBManager.setString(7, strLogTo);
		gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
		gmDBManager.close();

		return alList;
	}
	
	/**
     * fetchPatientLog - This method will be fetch patient comment detail
     * @param strPatientLId 
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchPatientLog(String strPatientLId) throws AppError
    {
        HashMap hmValues = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_patient_log.gm_fch_patient_log", 2);
		gmDBManager.setString(1, GmCommonClass.parseZero(strPatientLId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
		return hmValues;	
    }
}// end of class GmClinicalBean
