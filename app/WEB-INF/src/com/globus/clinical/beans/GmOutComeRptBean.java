package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmOutComeRptBean implements GmClinicalInterface
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

    /**
      * loadNDIScore - This Method is used to report NDI score
      * @param String strStudyListId, String strSiteId  
      * @return HashMap
      * @exception AppError
    **/
      public HashMap loadNDIScore(String strStudyListId, String strSiteId, String scoreId) throws AppError
      {
	        log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId+ " strSiteId -- " +scoreId);
	        HashMap hmFinalValue = new HashMap();
	            
	        ResultSet rsHeader = null;
	        ResultSet rsDetails = null;
	        GmCrossTabReport gmCrossReport = new GmCrossTabReport();
	        GmDBManager gmDBManager = new GmDBManager ();        
        
        try{
	        // To fetch the transaction information
	        gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_ndi_score",5);
	        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
	        gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
	        gmDBManager.setString(1,strStudyListId);
	        gmDBManager.setString(2,strSiteId);
	        gmDBManager.setString(5,scoreId);
	        gmDBManager.execute();
	        rsHeader = (ResultSet)gmDBManager.getObject(3);
	        rsDetails = (ResultSet)gmDBManager.getObject(4);
	        gmCrossReport.setGroupFl(true);
	        gmCrossReport.setAcceptExtraColumn(true);       
	        gmCrossReport.setExtraColumnParams(4,"  <br>Avg");
	        gmCrossReport.setExtraColumnParams(6," <br>"+(char)177+"SD");
	        hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
	        gmDBManager.close();
        }
        catch(Exception exp){
        	gmDBManager.close();
        	throw new AppError(exp);
        }
       
        return hmFinalValue;
      }
      
      /**
       * loadNDIPerImprove - This Method is used to report NDI score
       * @param String strStudyListId, String strSiteId  
       * @return HashMap
       * @exception AppError
     **/
       public HashMap loadNDIPerImprove(String strStudyListId, String strSiteId, String scoreId) throws AppError
       {
           log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId+ " scoreId --" +scoreId+"--");
         HashMap hmFinalValue = new HashMap();
         ResultSet rsHeader = null;
         ResultSet rsDetails = null;
         GmCrossTabReport gmCrossReport = new GmCrossTabReport();
         GmDBManager gmDBManager = new GmDBManager ();        
         
         try{
         // To fetch the transaction information
         gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_ndi_per_improve",5);
         gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
         gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
         gmDBManager.setString(1,strStudyListId);
         gmDBManager.setString(2,strSiteId);
         gmDBManager.setString(3,scoreId);
         gmDBManager.execute();
         rsHeader = (ResultSet)gmDBManager.getObject(4);
         rsDetails = (ResultSet)gmDBManager.getObject(5);
         gmCrossReport.setGroupFl(true);
         gmCrossReport.setAcceptExtraColumn(true);
         gmCrossReport.setExtraColumnParams(7," <BR> n");
         gmCrossReport.setExtraColumnParams(6," <BR> N");
         gmCrossReport.setExtraColumnParams(4," <BR> n/N%");
         hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
         gmDBManager.close();
         }
       
         catch(Exception exp){
           gmDBManager.close();
           throw new AppError(exp);
       	 }
         return hmFinalValue;
       }
       
       /**
        * loadNDIImproveDetail - This Method is used to report NDI improvement detail
        * @param String strStudyListId, String strSiteId  
        * @return HashMap
        * @exception AppError
      **/
        public HashMap loadNDIImproveDetail(String strStudyListId,String strSiteId, String strTreatmentId) throws AppError
        {
            log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strTreatmentId);
          HashMap hmFinalValue = new HashMap();
          HashMap hmLinkDetails = new HashMap();
          HashMap hmapValue = new HashMap();
          ArrayList alLinkList = new ArrayList();
          ArrayList alPreOp = new ArrayList();
          ResultSet rsHeader = null;
          ResultSet rsDetails = null;
          ResultSet rsPreOp = null;
          GmCrossTabReport gmCrossReport = new GmCrossTabReport();
          GmDBManager gmDBManager = new GmDBManager ();        
          
          try{
          // To fetch the transaction information
          gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_ndi_improve_detail",6);
          gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
          gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
          gmDBManager.registerOutParameter(6,OracleTypes.CURSOR);
          gmDBManager.setString(1,strStudyListId);
          gmDBManager.setString(2,strSiteId);
          gmDBManager.setString(3,strTreatmentId);
          gmDBManager.execute();
          rsHeader = (ResultSet)gmDBManager.getObject(4);
          rsDetails = (ResultSet)gmDBManager.getObject(5);
         // rsPreOp = (ResultSet)gmDBManager.getObject(6);
         // alPreOp = gmDBManager.returnArrayList(rsPreOp); 
              
          gmCrossReport.setGroupFl(true);
          gmCrossReport.setAcceptExtraColumn(true);
          gmCrossReport.setExtraColumnParams(6," <br>%imp");
          gmCrossReport.setExtraColumnParams(7," >=25%");

          gmCrossReport.setFixedColumn(true);
          gmCrossReport.setFixedParams(4,"Treatment");
          hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
          gmDBManager.close();
          
          /*// First parameter holds the link object name         
          hmLinkDetails.put("LINKID", "ID");
          hmapValue = new HashMap();
          hmapValue.put("KEY","PREOPCAL_VALUE");
          hmapValue.put("VALUE", "PreOpt");
          alLinkList.add(hmapValue);
          hmLinkDetails.put("POSITION","AFTER");
          hmLinkDetails.put("LINKVALUE", alLinkList);
      
          hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue,alPreOp , hmLinkDetails);*/
          }
          
          catch(Exception exp){
            gmDBManager.close();
            throw new AppError(exp);
           }
          return hmFinalValue;
        }
      
        /**
         * loadNDIScore - This Method is used to report NDI score
         * @param String strStudyListId, String strSiteId  
         * @return HashMap
         * @exception AppError
       **/
         public HashMap loadOutComeScore(String strStudyListId, String strSiteId, String strType) throws AppError
         {
           log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId);
           log.debug(" strType " + strType );
           HashMap hmFinalValue = new HashMap();
           ResultSet rsHeader = null;
           ResultSet rsDetails = null;
           GmCrossTabReport gmCrossReport = new GmCrossTabReport();
           GmDBManager gmDBManager = new GmDBManager ();        
           
           try{
        	// To fetch the transaction information  
        	// PC-43699: Based on study to Get the dynamic procedure name
        	   
        	   // PC-1809: Reflect Study - Dynamic procedure changes
       		   gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_dynamic_outcome_score",5);

           gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
           gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
           gmDBManager.setString(1,strStudyListId);
           gmDBManager.setString(2,strSiteId);
           gmDBManager.setString(3,strType);
           gmDBManager.execute();
           rsHeader = (ResultSet)gmDBManager.getObject(4);
           rsDetails = (ResultSet)gmDBManager.getObject(5);
           gmCrossReport.setGroupFl(true);
           gmCrossReport.setAcceptExtraColumn(true);
           gmCrossReport.setExtraColumnParams(4,"  <br>Avg");
           gmCrossReport.setExtraColumnParams(6," <br>"+(char)177+"SD");
           hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
           gmDBManager.close();
           }
           
           catch(Exception exp){
             gmDBManager.close();
             throw new AppError(exp);
           }
           return hmFinalValue;
         }
         
         /**
          * loadPatientSatisfaction - This Method is used to report patient satisfaction
          * @param String strStudyListId, String strPeriod  
          * @return HashMap
          * @exception AppError
        **/
          public RowSetDynaClass loadPatientSatisfaction(String strStudyListId, String strSiteId, String strPeriod) throws AppError
          {
              log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId);
              log.debug(" strPeriod " + strPeriod + " strSiteId -- " +strSiteId);
              HashMap hmFinalValue = new HashMap();
              HashMap hmTemp = new HashMap();
              ResultSet rsHeader = null;
              ResultSet rsDetails = null;
              GmCrossTabReport gmCrossReport = new GmCrossTabReport();
              GmDBManager gmDBManager = new GmDBManager ();
              RowSetDynaClass rdResult = null;
              /*
              hmTemp.put("3","6 wk");
              hmTemp.put("4","3 mo");
              hmTemp.put("5","6 mo");
              hmTemp.put("6","12 mo");
              hmTemp.put("7","24 mo");
              hmTemp.put("8","36 mo");
              hmTemp.put("9","48 no");
              */
              // PMT-50759: Based on study to Get the dynamic procedure name
              // To fetch the transaction information
              
              if(strStudyListId.equalsIgnoreCase("GPR009")){
            	  gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_amnios_patient_satisfaction",4);
              }else{
            	  gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_patient_satisfaction",4);
              }
              
              //gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
              gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
              gmDBManager.setString(1,strStudyListId);
              gmDBManager.setString(2,strSiteId);
              gmDBManager.setString(3,(String)strPeriod);
              //gmDBManager.setString(3,(String)hmTemp.get(strPeriod));
              gmDBManager.execute();
              /*rsHeader = (ResultSet)gmDBManager.getObject(4);
              rsDetails = (ResultSet)gmDBManager.getObject(5); */
              
              
             /* 
              gmCrossReport.setGroupFl(true);
              gmCrossReport.setFixedColumn(true);
              gmCrossReport.setFixedParams(4,"Def True n");
              gmCrossReport.setFixedParams(9,"Def True %");
              gmCrossReport.setFixedParams(10,"Mostly True n");
              gmCrossReport.setFixedParams(11,"Mostly True %");
              gmCrossReport.setFixedParams(12,"Don't know n");
              gmCrossReport.setFixedParams(13,"Don't know %");
              gmCrossReport.setFixedParams(14,"Mostly False n");
              gmCrossReport.setFixedParams(15,"Mostly False %");
              gmCrossReport.setFixedParams(16,"Def False n");
              gmCrossReport.setFixedParams(17,"Def False %");
              hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
              gmDBManager.close();
              return hmFinalValue;
              */
              
              rdResult  = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(4));
              gmDBManager.close();
              return rdResult;
              
            }

          /**
           * loadPeriod - This Method is used to load the period for drop down - based on Study
           * 			Study id parameter added - PMT-50759: Amnios study Patient Satisfaction report changes
           * 
           * @param strStudyId
           * @return ArrayList
           * @exception AppError
         **/
          public ArrayList loadPeriod(String strStudyId) throws AppError
          {
              ArrayList alResult = new ArrayList();
              GmDBManager gmDBManager = new GmDBManager ();        
              
              // To fetch the transaction information
              // based on study to get dynamic procedure call (PMT-50759)
              
              if(strStudyId.equalsIgnoreCase("GPR009")){
            	  gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_amnios_period_ddown",1);
              }else{
            	  gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_period_ddown",1); 
              }              
              gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
              gmDBManager.execute();
              alResult  = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
              gmDBManager.close();
              return alResult;
              
          }
          
          /**
           * loadNDIPerImprove - This Method is used to report NDI score
           * @param String strStudyListId, String strSiteId  
           * @return HashMap
           * @exception AppError
         **/
           public HashMap loadNeurologicalStatus(String strStudyListId, String strSiteId, String scoreId) throws AppError
           {
               log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId+ " scoreId --" +scoreId+"--");
             HashMap hmFinalValue = new HashMap();
             ResultSet rsHeader = null;
             ResultSet rsDetails = null;
             GmCrossTabReport gmCrossReport = new GmCrossTabReport();
             GmDBManager gmDBManager = new GmDBManager ();        
             
             try{
             // To fetch the transaction information
             gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_neurological_status",5);
             gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
             gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
             gmDBManager.setString(1,strStudyListId);
             gmDBManager.setString(2,strSiteId);
             gmDBManager.setString(3,scoreId);
             gmDBManager.execute();
             rsHeader = (ResultSet)gmDBManager.getObject(4);
             rsDetails = (ResultSet)gmDBManager.getObject(5);
             gmCrossReport.setGroupFl(true);
             gmCrossReport.setAcceptExtraColumn(true);
             gmCrossReport.setExtraColumnParams(7," <BR> n");
             gmCrossReport.setExtraColumnParams(6," <BR> N");
             gmCrossReport.setExtraColumnParams(4," <BR> n/N%");
             hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
             
             log.debug("hmFinalValue "+hmFinalValue);
             
            
             gmDBManager.close();
             }
           
             catch(Exception exp){
               gmDBManager.close();
               throw new AppError(exp);
           	 }
             return hmFinalValue;
           }
           
           /**
            * loadNDIPerImprove - This Method is used to report NDI score
            * @param String strStudyListId, String strSiteId  
            * @return HashMap
            * @exception AppError
          **/
            public HashMap loadZCQSuccess(String strStudyListId, String strSiteId, String scoreId) throws AppError
            {
                log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId+ " scoreId --" +scoreId+"--");
              HashMap hmFinalValue = new HashMap();
              ResultSet rsHeader = null;
              ResultSet rsDetails = null;
              GmCrossTabReport gmCrossReport = new GmCrossTabReport();
              GmDBManager gmDBManager = new GmDBManager ();        
              
              try{
              // To fetch the transaction information
              gmDBManager.setPrepareString("gm_pkg_cr_outcome_rpt.gm_cr_fch_zcq_success",5);
              gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
              gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
              gmDBManager.setString(1,strStudyListId);
              gmDBManager.setString(2,strSiteId);
              gmDBManager.setString(3,scoreId);
              gmDBManager.execute();
              rsHeader = (ResultSet)gmDBManager.getObject(4);
              rsDetails = (ResultSet)gmDBManager.getObject(5);
              gmCrossReport.setGroupFl(true);
              gmCrossReport.setAcceptExtraColumn(true);
              gmCrossReport.setExtraColumnParams(7," <BR> n");
              gmCrossReport.setExtraColumnParams(6," <BR> N");
              gmCrossReport.setExtraColumnParams(4," <BR> n/N%");
              hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
              gmDBManager.close();
              }
            
              catch(Exception exp){
                gmDBManager.close();
                throw new AppError(exp);
            	 }
              return hmFinalValue;
            }

            /**
         	 * common method used to get the grid xml data
         	 * 
         	 * @date June 06, 2010
         	 * @param
         	 */

         	public String getXmlGridData(HashMap hmParamV) throws AppError {

         		GmTemplateUtil templateUtil = new GmTemplateUtil();

         		ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
         		String strTemplate = (String) hmParamV.get("TEMPLATE");
         		String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
         		

         		HashMap hmParam = new HashMap();
         		
         		templateUtil.setDataList("alResult", alParam);
         		templateUtil.setTemplateName(strTemplate);
         		templateUtil.setTemplateSubDir(strTemplatePath);

         		return templateUtil.generateOutput();
         	}

         	
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.globus.clinical.beans.GmClinicalInterface#loadPrimaryEndpointRpt(
			 * java.lang.String, java.lang.String, java.lang.String)
			 * 
			 * This method used to call the existing method and returns the primary end
			 * point report Study - SECURE-C and TRIUMPH
			 */
			@Override
			public HashMap loadPrimaryEndpointRpt(String strstudyId,
					String strStudySiteIds, String strScoreType) throws AppError {
				// TODO Auto-generated method stub
		
				// to call the existing method (SECURE-C and TRIUMPH study)
				HashMap hmPrimaryEndpointDtls = loadNDIScore(strstudyId,
						strStudySiteIds, strScoreType);
		
				return hmPrimaryEndpointDtls;
			}
			
}
