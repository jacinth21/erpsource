/**
 * 
 */
package com.globus.clinical.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;

/**
 * @author rshah
 *
 */
public class GmCrtlEndPointRptBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
     * loadCrtlEndPointDetail - This Method is used to get crosstab details for Critical End Point
     * @param hmValue  
     * @return HashMap
     * @exception AppError
   **/
	public HashMap loadCrtlEndPointDetail(HashMap hmValues) throws AppError
    {        
		StringBuffer sbHeaderQuery = new StringBuffer();
		StringBuffer sbDtlQuery = new StringBuffer();
		
		HashMap hmReturn = new HashMap();
		
		String strStudyId = (String)hmValues.get("STUDYID");
		
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		
		sbHeaderQuery.append(" SELECT DISTINCT c613_study_period_ds || ' <BR> n' period, c613_seq_no, 1 a ");
		sbHeaderQuery.append(" FROM t613_study_period ");
		sbHeaderQuery.append(" WHERE c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
		sbHeaderQuery.append(" and c612_study_list_id in (select c612_study_list_id from t612_study_form_list where c611_study_id = '"+strStudyId+"') ");
		//sbHeaderQuery.append(" AND c613_seq_no < 10 ");
		sbHeaderQuery.append(" UNION ALL ");
		sbHeaderQuery.append(" SELECT DISTINCT c613_study_period_ds || ' <BR> N' period, c613_seq_no, 2 a ");
		sbHeaderQuery.append(" FROM t613_study_period ");
		sbHeaderQuery.append(" WHERE c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
		sbHeaderQuery.append(" and c612_study_list_id in (select c612_study_list_id from t612_study_form_list where c611_study_id = '"+strStudyId+"') ");
		//sbHeaderQuery.append(" AND c613_seq_no < 10 ");
		sbHeaderQuery.append(" UNION ALL ");
		sbHeaderQuery.append(" SELECT DISTINCT c613_study_period_ds || ' <BR> n/N%' period, c613_seq_no, 3 a ");
		sbHeaderQuery.append(" FROM t613_study_period ");
		sbHeaderQuery.append(" WHERE c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
		sbHeaderQuery.append(" and c612_study_list_id in (select c612_study_list_id from t612_study_form_list where c611_study_id = '"+strStudyId+"') ");
		//sbHeaderQuery.append(" AND c613_seq_no < 10 ");		
		sbHeaderQuery.append(" ORDER BY c613_seq_no, a ");
		
		sbDtlQuery = fetchCrtlEndPointDtl(hmValues);
		
		log.debug("sbHeaderQuery==="+sbHeaderQuery.toString());
		log.debug("sbDtlQuery==="+sbDtlQuery.toString());
		
		gmCrossReport.setGroupFl(true);
        gmCrossReport.setAcceptExtraColumn(true);
        gmCrossReport.setExtraColumnParams(4," <BR> n");
        gmCrossReport.setExtraColumnParams(5," <BR> N");
        gmCrossReport.setExtraColumnParams(6," <BR> n/N%");
		
		hmReturn = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDtlQuery.toString());
		
		log.debug("hmReturn==="+hmReturn);
		
		return hmReturn;
    }
	/**
     * fetchCrtlEndPointDtl - This Method is used to fetch Critical End Point detail
     * @param hmValue  
     * @return HashMap
     * @exception AppError
   **/
	public StringBuffer fetchCrtlEndPointDtl(HashMap hmValues) throws AppError
	{
		StringBuffer sbDtlQuery = new StringBuffer();
 		String strStudyId = (String)hmValues.get("STUDYID");
 		String strSiteId = (String)hmValues.get("SITEID");
 		String strStudyStiteId = (String)hmValues.get("SITELIST");
 		String strCondition = (String)hmValues.get("CONDITION");
 		
 		log.debug("strCondition===="+strCondition);
 		//Should come from rule value
 		String strform5stdlstid = GmCommonClass.getRuleValue(strStudyId, "ZCQ_FORM");//"2505";
 		String strform9stdlstid = GmCommonClass.getRuleValue(strStudyId, "NEU_FORM");//"2509";
 		String strradiostdlstid = GmCommonClass.getRuleValue(strStudyId, "RADIO_FORM");//"2510";
 		
 		String strForm5Name = "";
 		String strForm9Name = "";
 		strForm5Name = strStudyId.equals("GPR003")? "All SD5 Complete":strStudyId.equals("GPR004")?"All PST5 Complete":strStudyId.equals("GPR002")?"All SC5 Complete":strStudyId.equals("GPR005")?"ZCQ (Page 1-3 of 10) Complete":"";
 		strForm9Name = strStudyId.equals("GPR003")? "All SD9 Complete":strStudyId.equals("GPR004")?"All PST8 Complete":strStudyId.equals("GPR002")?"All SC8 Complete":strStudyId.equals("GPR005")?"All FORM 5 Complete":"";
 		
 		//Post ops
 		 sbDtlQuery.append("  SELECT NVL (Actual_Postop.tempseq, 1) ID, NVL (Actual_Postop.Name, 'All Post-Op CRFs Complete') NAME ");
 		 sbDtlQuery.append("   , Expected_Postop.study_prd_ds period, NVL (Actual_Postop.act_cnt, 0) ");
 		 sbDtlQuery.append("   , Expected_Postop.expt_cnt, TO_CHAR(ROUND ( (NVL (Actual_Postop.act_cnt, 0) / DECODE (Expected_Postop.expt_cnt, 0, 1, ");
 		 sbDtlQuery.append("     Expected_Postop.expt_cnt))                                        * 100, 2),'FM99999990D00') percentage, NVL ( ");
 		 sbDtlQuery.append("     Actual_Postop.study_prd_ds, Expected_Postop.study_prd_ds) act_studyprd ");
 		 sbDtlQuery.append("    FROM ");
 		 sbDtlQuery.append("     (SELECT 'All Post-Op CRFs Complete' NAME, 1 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(act_cnt,-1,0,act_cnt)) act_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT Floor (COUNT (1) / ");
 		 sbDtlQuery.append("             (SELECT COUNT (1) ");
 		 sbDtlQuery.append("                FROM t613_study_period t613, t612_study_form_list t612 ");
 		 sbDtlQuery.append("               WHERE t613.c613_study_period_ds = v621.c613_study_period_ds ");
 		 sbDtlQuery.append("                 AND t613.c612_study_list_id = t612.c612_study_list_id ");
 		 sbDtlQuery.append("                 AND t612.c612_study_form_ds NOT IN ('SF36') ");
 		 sbDtlQuery.append("                 AND t612.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("                 AND t612.c612_delete_fl <> 'Y' ");
 		 sbDtlQuery.append("             )) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) ");
 		 sbDtlQuery.append("             act_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621, t622_patient_list t622 ");
 		 sbDtlQuery.append("           WHERE t622.c622_date IS NOT NULL ");
 		 sbDtlQuery.append("             AND (t622.c622_missing_follow_up_fl IS NULL ");
 		 sbDtlQuery.append("             OR t622.c622_missing_follow_up_fl = 'N') ");
 		 sbDtlQuery.append("             AND t622.c601_form_id = v621.c601_form_id ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c612_study_form_ds NOT IN ('SF36') ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_id = t622.c613_study_period_id ");
 		 if(!strStudyStiteId.equals("")){
 		 sbDtlQuery.append("             AND v621.c614_study_site_id = "+strStudyStiteId);
 		 }
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c621_patient_id = t622.c621_patient_id ");
 		 sbDtlQuery.append("             AND NVL (t622.c622_delete_fl, 'N') = 'N' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Actual_Postop ");
 		 sbDtlQuery.append(" RIGHT JOIN ");
 		 sbDtlQuery.append("     (SELECT 'All Post-Op CRFs Complete' NAME, 1 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(expt_cnt,-1,0,expt_cnt)) expt_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT FLOOR (COUNT (v621.c621_patient_id) / ");
 		 sbDtlQuery.append("             (SELECT COUNT (1) ");
 		 sbDtlQuery.append("                FROM t613_study_period t613, t612_study_form_list t612 ");
 		 sbDtlQuery.append("               WHERE t613.c613_study_period_ds = v621.c613_study_period_ds ");
 		 sbDtlQuery.append("                 AND t613.c612_study_list_id = t612.c612_study_list_id ");
 		 sbDtlQuery.append("                 AND t612.c612_study_form_ds NOT IN ('SF36') ");
 		 sbDtlQuery.append("                 AND t612.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("                 AND t612.c612_delete_fl <> 'Y' ");
 		 sbDtlQuery.append("             )) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) ");
 		 sbDtlQuery.append("             expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) > TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
 		 if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		} 
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c612_study_form_ds NOT IN ('SF36') ");
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("        ");
 		 sbDtlQuery.append("       UNION ALL ");
 		 sbDtlQuery.append("           ");
 		 sbDtlQuery.append("          SELECT FLOOR (COUNT (v621.c621_patient_id) / ");
 		 sbDtlQuery.append("             (SELECT COUNT (1) ");
 		 sbDtlQuery.append("                FROM t613_study_period t613, t612_study_form_list t612 ");
 		 sbDtlQuery.append("               WHERE t613.c613_study_period_ds = v621.c613_study_period_ds ");
 		 sbDtlQuery.append("                 AND t613.c612_study_list_id = t612.c612_study_list_id ");
 		 sbDtlQuery.append("                 AND t612.c612_study_form_ds NOT IN ('SF36') ");
 		 sbDtlQuery.append("                 AND t612.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("                 AND t612.c612_delete_fl <> 'Y' ");
 		 sbDtlQuery.append("             )) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) ");
 		 sbDtlQuery.append("             expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621, t622_patient_list t622 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) BETWEEN TRUNC (v621.c621_exp_date_start) AND TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
 		 if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		} 
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c612_study_form_ds NOT IN ('SF36') ");
 		 sbDtlQuery.append("             AND t622.c621_patient_id = v621.c621_patient_id ");
 		 sbDtlQuery.append("             AND v621.c601_form_id = t622.c601_form_id ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_id = t622.c613_study_period_id ");
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Expected_Postop ");
 		 sbDtlQuery.append("      ON Actual_Postop.study_prd_ds = Expected_Postop.study_prd_ds ");
 		 sbDtlQuery.append("  ");
 		 sbDtlQuery.append(" UNION ALL ");
 		 sbDtlQuery.append("   ");
 		 sbDtlQuery.append("  SELECT NVL (Actual_SD5.tempseq, 2) ID, NVL (Actual_SD5.Name, '"+strForm5Name+"') NAME ");
 		 sbDtlQuery.append("   , Expected_SD5.study_prd_ds period, NVL (Actual_SD5.act_cnt, 0) ");
 		 sbDtlQuery.append("   , Expected_SD5.expt_cnt, TO_CHAR(ROUND ( (NVL (Actual_SD5.act_cnt, 0) / DECODE (Expected_SD5.expt_cnt, 0, 1, ");
 		 sbDtlQuery.append("     Expected_SD5.expt_cnt))                                     * 100, 2),'FM99999990D00') percentage, NVL (Actual_SD5.study_prd_ds, ");
 		 sbDtlQuery.append("     Expected_SD5.study_prd_ds) act_studyprd ");
 		 sbDtlQuery.append("    FROM ");
 		 sbDtlQuery.append("     (SELECT '"+strForm5Name+"' NAME, 2 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(act_cnt,-1,0,act_cnt)) act_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT '"+strForm5Name+"' NAME, 2 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strform5stdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) act_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621, t622_patient_list t622 ");
 		 sbDtlQuery.append("           WHERE t622.c622_date IS NOT NULL ");
 		 sbDtlQuery.append("             AND (t622.c622_missing_follow_up_fl IS NULL ");
 		 sbDtlQuery.append("             OR t622.c622_missing_follow_up_fl = 'N') ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery') ");
 		 sbDtlQuery.append("             AND t622.c601_form_id = v621.c601_form_id ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_id = t622.c613_study_period_id ");
 		 if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		} 
 		 sbDtlQuery.append("             AND v621.c612_study_list_id =  "+strform5stdlstid);
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c621_patient_id = t622.c621_patient_id ");
 		 sbDtlQuery.append("             AND NVL (t622.c622_delete_fl, 'N') = 'N' ");
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Actual_SD5 ");
 		 sbDtlQuery.append(" RIGHT JOIN ");
 		 sbDtlQuery.append("     (SELECT '"+strForm5Name+"' NAME, 2 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(expt_cnt,-1,0,expt_cnt)) expt_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT '"+strForm5Name+"' NAME, 2 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strform5stdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) > TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery') ");
 		 if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		} 
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c612_study_list_id = "+strform5stdlstid);
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("        ");
 		 sbDtlQuery.append("       UNION ALL ");
 		 sbDtlQuery.append("           ");
 		 sbDtlQuery.append("          SELECT '"+strForm5Name+"' NAME, 2 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strform5stdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621, t622_patient_list t622 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) BETWEEN TRUNC (v621.c621_exp_date_start) AND TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
 		if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		}
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND t622.c621_patient_id = v621.c621_patient_id ");
 		 sbDtlQuery.append("             AND v621.c601_form_id = t622.c601_form_id ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_id = t622.c613_study_period_id ");
 		 sbDtlQuery.append("             AND v621.c612_study_list_id = "+strform5stdlstid);
 	 	 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Expected_SD5 ");
 		 sbDtlQuery.append("      ON Actual_SD5.study_prd_ds = Expected_SD5.study_prd_ds ");
 		 sbDtlQuery.append("  ");
 		 sbDtlQuery.append(" UNION ALL ");
 		 sbDtlQuery.append("   ");
 		 sbDtlQuery.append("  SELECT NVL (Actual_SD9.tempseq, 3) ID, NVL (Actual_SD9.Name, '"+strForm9Name+"') NAME ");
 		 sbDtlQuery.append("   , Expected_SD9.study_prd_ds period, NVL (Actual_SD9.act_cnt, 0) ");
 		 sbDtlQuery.append("   , Expected_SD9.expt_cnt, TO_CHAR(ROUND ( (NVL (Actual_SD9.act_cnt, 0) / DECODE (Expected_SD9.expt_cnt, 0, 1, ");
 		 sbDtlQuery.append("     Expected_SD9.expt_cnt))                                     * 100, 2),'FM99999990D00') percentage, NVL (Actual_SD9.study_prd_ds, ");
 		 sbDtlQuery.append("     Expected_SD9.study_prd_ds) act_studyprd ");
 		 sbDtlQuery.append("    FROM ");
 		 sbDtlQuery.append("     (SELECT '"+strForm9Name+"' NAME, 3 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(act_cnt,-1,0,act_cnt)) act_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT '"+strForm9Name+"' NAME, 3 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strform9stdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) act_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621, t622_patient_list t622 ");
 		 sbDtlQuery.append("           WHERE t622.c622_date IS NOT NULL ");
 		 sbDtlQuery.append("             AND (t622.c622_missing_follow_up_fl IS NULL ");
 		 sbDtlQuery.append("             OR t622.c622_missing_follow_up_fl = 'N') ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery') ");
 		 sbDtlQuery.append("             AND t622.c601_form_id = v621.c601_form_id ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_id = t622.c613_study_period_id ");
 		if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		}
 		 sbDtlQuery.append("             AND v621.c612_study_list_id = "+strform9stdlstid);
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c621_patient_id = t622.c621_patient_id ");
 		 sbDtlQuery.append("             AND NVL (t622.c622_delete_fl, 'N') = 'N' ");
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Actual_SD9 ");
 		 sbDtlQuery.append(" RIGHT JOIN ");
 		 sbDtlQuery.append("     (SELECT '"+strForm9Name+"' NAME, 3 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(expt_cnt,-1,0,expt_cnt)) expt_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT 'All SD9 Complete' NAME, 3 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strform9stdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) > TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery') ");
 		if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		}
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c612_study_list_id = "+strform9stdlstid);
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("        ");
 		 sbDtlQuery.append("       UNION ALL ");
 		 sbDtlQuery.append("           ");
 		 sbDtlQuery.append("          SELECT '"+strForm9Name+"' NAME, 3 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strform9stdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621, t622_patient_list t622 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) BETWEEN TRUNC (v621.c621_exp_date_start) AND TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
 		if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		}
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND t622.c621_patient_id = v621.c621_patient_id ");
 		 sbDtlQuery.append("             AND v621.c601_form_id = t622.c601_form_id ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_id = t622.c613_study_period_id ");
 		 sbDtlQuery.append("             AND v621.c612_study_list_id = "+strform9stdlstid);
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Expected_SD9 ");
 		 sbDtlQuery.append("      ON Actual_SD9.study_prd_ds = Expected_SD9.study_prd_ds ");
 		 sbDtlQuery.append("  ");
 		 sbDtlQuery.append(" UNION ALL ");
 		 sbDtlQuery.append("   ");
 		 sbDtlQuery.append("  SELECT NVL (Actual_radiograph.tempseq, 4) ID, NVL (Actual_radiograph.Name, 'All Radiographs Complete') NAME ");
 		 sbDtlQuery.append("   , Expected_radiograph.study_prd_ds period, NVL (Actual_radiograph.act_cnt, 0) ");
 		 sbDtlQuery.append("   , Expected_radiograph.expt_cnt, TO_CHAR(ROUND ( (NVL (Actual_radiograph.act_cnt, 0) / DECODE (Expected_radiograph.expt_cnt, 0 ");
 		 sbDtlQuery.append("     , 1, Expected_radiograph.expt_cnt))                                       * 100, 2),'FM99999990D00') percentage, NVL ( ");
 		 sbDtlQuery.append("     Actual_radiograph.study_prd_ds, Expected_radiograph.study_prd_ds) act_studyprd ");
 		 sbDtlQuery.append("    FROM ");
 		 sbDtlQuery.append("     (SELECT 'All Radiographs Complete' NAME, 4 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(act_cnt,-1,0,act_cnt)) act_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT 'All Radiographs Complete' NAME, 4 tempseq ");
 		 sbDtlQuery.append("           , COUNT (lat_cnt) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', '"+strStudyStiteId+"', study_prd_ds, patient_id) act_cnt, ");
 		 sbDtlQuery.append("             study_prd_ds, patient_id ");
 		 sbDtlQuery.append("            FROM ");
 		 sbDtlQuery.append("             (SELECT COUNT (DECODE (v624.lateral, 'Y', 1, 0)) lat_cnt, v624.patient_id patient_id ");
 		 sbDtlQuery.append("               , v624.period study_prd_ds ");
 		 sbDtlQuery.append("                FROM ");
 		 sbDtlQuery.append("                 (SELECT v621.c621_patient_id patient_id, v621.c621_patient_ide_no patient_ide_no ");
 		 sbDtlQuery.append("                   , v621.c601_form_id, v621.c613_study_period_id ");
 		 sbDtlQuery.append("                   , v621.c613_seq_no seq_no, v621.c613_study_period_ds period ");
 		 sbDtlQuery.append("                   , DECODE (t624.c901_record_type, 60399, 'Y', 'N') lateral, DECODE (t624.c901_record_type, 60398, 'Y', ");
 		 sbDtlQuery.append("                     'N') flexion, DECODE (t624.c901_record_type, 60396, 'Y', 'N') ap ");
 		 sbDtlQuery.append("                   , DECODE (t624.c901_record_type, 60397, 'Y', 'N') extension, v621.c621_exp_date_start ");
 		 sbDtlQuery.append("                   , v621.c621_exp_date_final ");
 		 sbDtlQuery.append("                    FROM v621_patient_study_list v621, t624_patient_record t624 ");
 		 sbDtlQuery.append("                   , (SELECT c621_patient_id, c622_date c622_surgery_date ");
 		 sbDtlQuery.append("                        FROM t622_patient_list ");
 		 sbDtlQuery.append("                       WHERE c601_form_id IN (10, 211, 48, 70) ");
 		 sbDtlQuery.append("                         AND NVL (c622_delete_fl, 'N') = 'N' ");
 		 sbDtlQuery.append("                     ) pt_sc10 ");
 		 sbDtlQuery.append("                   WHERE v621.c621_patient_id = t624.c621_patient_id ");
 		 sbDtlQuery.append("                     AND v621.c901_study_period = t624.c901_study_period ");
 		if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		}
 		 sbDtlQuery.append("                     AND t624.c901_record_grp = 60391 ");
 		 sbDtlQuery.append("                     AND v621.c612_study_list_id = "+strradiostdlstid);
 		 sbDtlQuery.append("                     AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("                     AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("                     AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery') ");
 		 sbDtlQuery.append("                     AND t624.c624_void_fl IS NULL ");
 		 
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("                     AND v621.c621_patient_id = pt_sc10.c621_patient_id ");
 		 sbDtlQuery.append("                 ) v624, t622_patient_list t622 ");
 		 sbDtlQuery.append("               WHERE v624.patient_id = t622.c621_patient_id ");
 		 sbDtlQuery.append("                 AND v624.c613_study_period_id = t622.c613_study_period_id ");
 		 sbDtlQuery.append("                 AND t622.c622_date IS NOT NULL ");
 		 sbDtlQuery.append("                 AND (t622.c622_missing_follow_up_fl IS NULL ");
 		 sbDtlQuery.append("                 OR t622.c622_missing_follow_up_fl = 'N') ");
 		 sbDtlQuery.append("                 AND t622.c601_form_id = v624.c601_form_id ");
 		 sbDtlQuery.append("                 AND NVL (t622.c622_delete_fl, 'N') = 'N' ");
 		 sbDtlQuery.append("             HAVING SUM (DECODE (v624.lateral, 'Y', 1, 0)) > 0 ");
 		 sbDtlQuery.append("            GROUP BY v624.patient_id, v624.period ");
 		 sbDtlQuery.append("             ) ");
 		 sbDtlQuery.append("        GROUP BY study_prd_ds, patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Actual_radiograph ");
 		 sbDtlQuery.append(" RIGHT JOIN ");
 		 sbDtlQuery.append("     (SELECT 'All Radiographs Complete' NAME, 4 tempseq ");
 		 sbDtlQuery.append("       , SUM (DECODE(expt_cnt,-1,0,expt_cnt)) expt_cnt, study_prd_ds ");
 		 sbDtlQuery.append("        FROM ");
 		 sbDtlQuery.append("         (SELECT 'All Radiographs Complete' NAME, 4 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strradiostdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) > TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery') ");
 		 if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		}
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND v621.c612_study_list_id = "+strradiostdlstid);
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("        ");
 		 sbDtlQuery.append("       UNION ALL ");
 		 sbDtlQuery.append("           ");
 		 sbDtlQuery.append("          SELECT 'All Radiographs Complete' NAME, 4 tempseq ");
 		 sbDtlQuery.append("           , COUNT (DECODE (v621.c612_study_list_id, "+strradiostdlstid+", 1, 0)) - gm_pkg_cr_adverseevent_rpt.get_ae_count ('"+strStudyId+"', ");
 		 sbDtlQuery.append(" '"+strStudyStiteId+"', v621.c613_study_period_ds, v621.c621_patient_id) expt_cnt, v621.c613_study_period_ds study_prd_ds ");
 		 sbDtlQuery.append("            FROM v621_patient_study_list v621, t622_patient_list t622 ");
 		 sbDtlQuery.append("           WHERE TRUNC (sysdate) BETWEEN TRUNC (v621.c621_exp_date_start) AND TRUNC (v621.c621_exp_date_final) ");
 		 sbDtlQuery.append("             AND v621.c611_study_id = '"+strStudyId+"' ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_ds NOT IN ('Preoperative', 'Time of Surgery', 'N/A') ");
 		if(!strStudyStiteId.equals("")){
  			sbDtlQuery.append("          AND v621.c614_study_site_id = "+strStudyStiteId);
  	 		}
 		 sbDtlQuery.append("             AND v621.c621_surgery_comp_fl = 'Y' ");
 		 sbDtlQuery.append("             AND t622.c621_patient_id = v621.c621_patient_id ");
 		 sbDtlQuery.append("             AND v621.c601_form_id = t622.c601_form_id ");
 		 sbDtlQuery.append("             AND v621.c613_study_period_id = t622.c613_study_period_id ");
 		 sbDtlQuery.append("             AND v621.c612_study_list_id = "+strradiostdlstid);
 		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" AND "+strCondition);
		 }
 		 sbDtlQuery.append("        GROUP BY v621.c613_study_period_ds, v621.c621_patient_id ");
 		 sbDtlQuery.append("         ) ");
 		 sbDtlQuery.append("    GROUP BY study_prd_ds ");
 		 sbDtlQuery.append("     ) Expected_radiograph ");
 		 sbDtlQuery.append("      ON Actual_radiograph.study_prd_ds = Expected_radiograph.study_prd_ds ");
 		return sbDtlQuery;
	}
}
