package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmEditCheckBean {

	// Code to Initialize the Logger Class.
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Fetches the EditCheckSummary detail
	 * 
	 * 
	 * @param HashMap
	 *            hmParam
	 * @param String
	 *            strCondition
	 * @return ArrayList
	 * @throws com.globus.common.beans.AppError
	 */
	public ArrayList fetchEditCheckSummary(HashMap hmParam, String strCondition) throws AppError {

		log.debug("loadSiteMonitorReport");

		ArrayList alResult = new ArrayList();

		String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYLISTID"));
		String strFormId = GmCommonClass.parseNull((String) hmParam.get("FORMID"));
		String strCraId = GmCommonClass.parseNull((String) hmParam.get("CRAID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strReferenceId = GmCommonClass.parseNull((String) hmParam.get("REFERENCEID"));

		if (strStudyId.equals(""))
			strStudyId = "0";
		if (strFormId.equals(""))
			strFormId = "0";
		if (strCraId.equals(""))
			strCraId = "0";
		if (strStatus.equals(""))
			strStatus = "0";
		if (strReferenceId.equals(""))
			strReferenceId = null;

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_edit_check_info.gm_fch_edit_check_summary", 6);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.setString(1, strStudyId);
		gmDBManager.setString(2, strFormId);
		gmDBManager.setString(3, strStatus);
		gmDBManager.setString(4, strCraId);
		gmDBManager.setString(5, strReferenceId);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		gmDBManager.close();
		log.debug("Exit size" + alResult.size());
		return alResult;

	}

	/**
	 * Fetches the EditCheckIncidentReport detail
	 * 
	 * 
	 * @param HashMap
	 *            hmParam
	 * @param String
	 *            strCondition
	 * @return ArrayList
	 * @throws com.globus.common.beans.AppError
	 */
	public ArrayList fetchEditCheckIncidentReport(HashMap hmParam, String strCondition) throws AppError {
		log.debug("fetchEditCheckIncidentReport");

		ArrayList alTrainingReport = new ArrayList();

		String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYLISTID"));
		String strStudySiteId = GmCommonClass.parseNull((String) hmParam.get("SITELIST"));
		String strPatientId = GmCommonClass.parseNull((String) hmParam.get("PATIENTID"));
		String strPeriodId = GmCommonClass.parseNull((String) hmParam.get("PERIODID"));
		String strFormId = GmCommonClass.parseNull((String) hmParam.get("FORMID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strReferenceId = GmCommonClass.parseNull((String) hmParam.get("REFERENCEID"));
		String strReasonForClosing = GmCommonClass.parseNull((String) hmParam.get("REASONFORCLOSING"));
		String strDateFrom = GmCommonClass.parseNull((String) hmParam.get("FORMDATE"));
		String strDateTo = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
		String strCraId = GmCommonClass.parseNull((String) hmParam.get("CRAID"));
		String strAnswerGrpId = GmCommonClass.parseNull((String) hmParam.get("ANSGRPID"));
		String strPatientAnsListId = GmCommonClass.parseNull((String) hmParam.get("PATIENTANSLISTID"));
		strStudySiteId = strStudySiteId.equals("")?GmCommonClass.parseNull((String) hmParam.get("SESSSTUDYSITEID")):strStudySiteId;
		strPatientId = strPatientId.equals("")?GmCommonClass.parseNull((String) hmParam.get("SESSPATIENTPKEY")):strPatientId;
		log.debug("hmParam : " +hmParam);
		
		log.debug("strAnswerGrpId : "+strAnswerGrpId);
		

		if (strPeriodId.equals("0") | strPeriodId.equals(""))
			strPeriodId = null;
		if (strFormId.equals("0") | strFormId.equals(""))
			strFormId = null;
		if (strCraId.equals("0") | strCraId.equals(""))
			strCraId = null;
		if (strReasonForClosing.equals("0") | strReasonForClosing.equals(""))
			strReasonForClosing = null;
		if (strStatus.equals("0") | strStatus.equals(""))
			strStatus = null;
		if (strDateFrom.equals(""))
			strDateFrom = null;
		if (strDateTo.equals(""))
			strDateTo = null;

		GmDBManager gmDBManager = new GmDBManager();

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.setLength(0);
		sbQuery.append(" SELECT '1' action, t6514.c6514_ec_incident_id incident_id, ");
		sbQuery.append(" t6512.c6512_edit_ref_id reference_id, v621.c614_site_id || '-' || get_sh_account_name(v621.c704_account_id) site_id, ");
		sbQuery.append(" v621.c621_patient_id patient_id, v621.c621_patient_ide_no patient_ide, ");
		sbQuery.append(" t6514.c623_patient_ans_list_id patient_ans_list_id, ");
		sbQuery.append(" v621.c612_study_form_ds form_ds, v621.c601_form_id form_id, ");
		sbQuery.append(" v621.c901_study_period study_period_id, ");
		sbQuery.append(" v621.c613_study_period_id study_period_pk, ");
		sbQuery.append(" get_code_name (v621.c901_study_period) study_period, ");
		sbQuery.append(" t6512.c6512_edit_description description, ");
		sbQuery.append(" t6514.c6514_incident_description incident_description,  ");
		sbQuery.append(" DECODE (t6514.c6514_status, 10, 'Open', 20, 'Closed') status, ");
		sbQuery.append(" get_code_name (t6514.c901_status_reason) reason_for_closing, ");
		sbQuery.append(" NVL (query_status, 99999) query_status ");
		sbQuery.append(" , GET_USER_NAME(NVL(t6514.c6514_last_updated_by,t6514.c6514_created_by)) updated_by ");
		sbQuery.append(" FROM t6512_edit_check t6512, ");
		sbQuery.append(" t6513_edit_check_details t6513, ");
		sbQuery.append(" t6514_ec_incident t6514, ");
		sbQuery.append(" (SELECT   t6510.c623_patient_ans_list_id, ");
		sbQuery.append(" MIN (t6510.c901_status) query_status ");
		sbQuery.append(" FROM t6510_query t6510 ");
		sbQuery.append(" GROUP BY t6510.c623_patient_ans_list_id) t6510, ");
		sbQuery.append(" (SELECT t621.c611_study_id, t614.c614_site_id, t621.c621_patient_id, ");
		sbQuery.append(" t621.c621_patient_ide_no, t612.c601_form_id, ");
		sbQuery.append(" t612.c612_study_form_ds, t613.c901_study_period, ");
		sbQuery.append(" t613.c613_study_period_id, t621.c704_account_id ");
		sbQuery.append(" FROM t621_patient_master t621, ");
		sbQuery.append(" t614_study_site t614, ");
		sbQuery.append(" t612_study_form_list t612, ");
		sbQuery.append(" t613_study_period t613 ");
		sbQuery.append(" WHERE t621.c611_study_id = t614.c611_study_id ");
		sbQuery.append(" AND t621.c704_account_id = t614.c704_account_id ");
		sbQuery.append(" AND t621.c611_study_id = t612.c611_study_id ");
		sbQuery.append(" AND t612.c612_study_list_id = t613.c612_study_list_id) v621 ");
		sbQuery.append(" WHERE t6512.c6512_edit_check_id = t6513.c6512_edit_check_id ");
		sbQuery.append(" AND t6513.c6513_edit_check_details_id = t6514.c6513_edit_check_details_id ");
		sbQuery.append(" AND t6512.c611_study_id = v621.c611_study_id ");
		sbQuery.append(" AND t6514.c621_patient_id = v621.c621_patient_id ");
		sbQuery.append(" AND t6513.c601_form_id = v621.c601_form_id ");
		sbQuery.append(" AND t6514.c623_patient_ans_list_id = t6510.c623_patient_ans_list_id(+) ");
		sbQuery.append(" AND (   t6514.c613_study_period_id = v621.c613_study_period_id ");
		sbQuery.append("        OR t6514.c613_linked_study_period_id = v621.c613_study_period_id ");
		sbQuery.append("     ) ");
		// sbQuery.append(" AND t6513.c613_study_period_id =
		// v621.c613_study_period_id ");
		sbQuery.append("   AND t6514.c6514_void_fl IS NULL												");
		sbQuery.append("   AND t6512.c6512_void_fl IS NULL												");
		sbQuery.append("   AND t6513.c6513_void_fl IS NULL												");
		// add the following dynamic query based on the conditions from the
		// screen
		sbQuery.append("   AND v621.c611_study_id = DECODE('" + strStudyId + "','0',v621.c611_study_id,'',v621.c611_study_id,'" + strStudyId + "') 	");
		sbQuery.append("   AND v621.c614_site_id = DECODE('" + strStudySiteId + "','0',v621.c614_site_id,'',v621.c614_site_id,'" + strStudySiteId + "')	");
		sbQuery.append("   AND v621.c621_patient_id = DECODE('" + strPatientId + "','0',v621.c621_patient_id,'',v621.c621_patient_id,'" + strPatientId + "')	");
		sbQuery.append("   AND v621.c601_form_id = NVL(" + strFormId + ",v621.c601_form_id)								");
		sbQuery.append("   AND v621.c901_study_period = NVL(" + strPeriodId + ",v621.c901_study_period)					");
		sbQuery.append("   AND t6512.c6512_edit_ref_id = DECODE('" + strReferenceId + "','',t6512.c6512_edit_ref_id,'" + strReferenceId + "')	");
		sbQuery.append("   AND NVL (t6514.c6514_last_updated_by, t6514.c6514_created_by) = NVL(" + strCraId + ",		");
		sbQuery.append("   NVL (t6514.c6514_last_updated_by, t6514.c6514_created_by))			");
		sbQuery.append("   AND t6514.c6514_created_date >= CASE WHEN " + strDateFrom + " is not null THEN TO_DATE('" + strDateFrom + "', 'MM/DD/YYYY') ELSE t6514.c6514_created_date END	");
		sbQuery.append("   AND t6514.c6514_created_date <= CASE WHEN " + strDateTo + " is not null THEN TO_DATE('" + strDateTo + "', 'MM/DD/YYYY') ELSE t6514.c6514_created_date END		");
		sbQuery.append("   AND NVL(t6514.c6514_status,-999) = NVL(" + strStatus + ",NVL(t6514.c6514_status,-999))							");
		sbQuery.append("   AND NVL(t6514.c901_status_reason,-999) = NVL(" + strReasonForClosing + ",NVL(t6514.c901_status_reason,-999))		"); 
		
		if (!strAnswerGrpId.equals(""))
		{
			sbQuery.append(" AND  ( t6513.C603_ANSWER_GRP_ID = "+strAnswerGrpId+ " OR t6513.c603_linked_answer_grp_id = " +strAnswerGrpId + " ) " );
		}
		if(!strPatientAnsListId.equals(""))
		{
			sbQuery.append(" AND  ( t6514.C623_LINKED_PT_ANS_LIST_ID = "+strPatientAnsListId+ " OR t6514.C623_PATIENT_ANS_LIST_ID = " +strPatientAnsListId + " ) " );
		}
	
		
		if (!strCondition.equals("")) {
			sbQuery.append(" AND " + strCondition);
		}

		log.debug("query===" + sbQuery.toString());
		alTrainingReport = gmDBManager.queryMultipleRecords(sbQuery.toString());
		gmDBManager.close();
		return alTrainingReport;

	} // End of loadPatientRecord

	/**
	 * common method used to get the grid xml data
	 * 
	 * @date Feb 24, 2010
	 * @param
	 */
	public String getXmlGridData(HashMap hmParamV) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();

		ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
		String strTemplate = GmCommonClass.parseNull((String) hmParamV.get("TEMPLATE"));
		String strTemplatePath = GmCommonClass.parseNull((String) hmParamV.get("TEMPLATEPATH"));
		String strStudyId = GmCommonClass.parseNull((String) hmParamV.get("STUDYID"));
		String strSiteId = GmCommonClass.parseNull((String) hmParamV.get("SITEID"));
		String strAccess = GmCommonClass.parseNull((String) hmParamV.get("ACCESS"));
		String strFormScr = GmCommonClass.parseNull((String) hmParamV.get("FORMSCR"));
		
		HashMap hmParam = new HashMap();
		hmParam.put("STUDYID", strStudyId);
		hmParam.put("SITEID", strSiteId);
		hmParam.put("ACCESS", strAccess);
		hmParam.put("FORMSCR", strFormScr);
		
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setDataMap("hmParam", hmParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}
	public String saveEditCheck(HashMap hmParams) throws AppError {
		GmDBManager db = new GmDBManager();
		log.debug("Values ARe : "+hmParams);
		db.setPrepareString("gm_pkg_cr_edit_check.gm_sav_editcheck", 6);
		db.registerOutParameter(6, OracleTypes.CHAR);
		db.setString(1, GmCommonClass.parseNull((String)hmParams.get("REFID")));
		db.setString(2, GmCommonClass.parseNull((String)hmParams.get("DESCRIPTION")));
		db.setString(3, GmCommonClass.parseNull((String)hmParams.get("SPECIFICATION")));
		db.setString(4, GmCommonClass.parseNull((String)hmParams.get("ACTIVEFLAG")));
		db.setString(5, GmCommonClass.parseNull((String)hmParams.get("USERID")));
		db.setString(6, GmCommonClass.parseNull((String)hmParams.get("EDITID")));
		db.execute();
		Integer i = new Integer(db.getInt(6));
		String actID = i.toString();
		db.commit();
		return actID;
	}
	public HashMap getEditCheck(String strRefId)throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_edit_check.gm_fch_editcheck", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strRefId);
		gmDBManager.execute();
		return gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
	}
}
