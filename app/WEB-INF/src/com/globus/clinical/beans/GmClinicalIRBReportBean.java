/*
 * Author: Hardik Parikh Project: Globus Medical App
 *  
 */

package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalIRBReportBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.
    /**
     * @param HashMap
	 * fetchIRBSummary 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchIRBSummary(HashMap hmParam) throws AppError {           	  
          
    		log.debug(" fetchIRBSummary hmParam :"+hmParam);
    		DBConnectionWrapper dbCon = null;
    		dbCon = new DBConnectionWrapper();         
    		
    		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
	    	String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYSITEID"));
	        String strIRBTypeId = GmCommonClass.parseNull((String)hmParam.get("IRBTYPEID"));
	        String strIRBAppTypeId = GmCommonClass.parseNull((String)hmParam.get("IRBAPPTYPEID"));
	        String strDateTypeId = GmCommonClass.parseNull((String)hmParam.get("DATETYPEID"));
			String strStartDate  =  GmCommonClass.parseNull((String)hmParam.get("STARTDATE"));
			String strEndDate  =  GmCommonClass.parseNull((String)hmParam.get("ENDDATE"));
			String strSiteId=(String)hmParam.get("SITEID");
			strStudySiteId = strSiteId.equals("") ? strStudySiteId : strSiteId;
			if(strIRBTypeId.equalsIgnoreCase("0") || strIRBTypeId.equalsIgnoreCase("")){
				strIRBTypeId=null;
			}
			if(strIRBAppTypeId.equalsIgnoreCase("0") || strIRBAppTypeId.equalsIgnoreCase("")){
				strIRBAppTypeId=null;
			}	
				
			String strDateField = "";
			
		  	StringBuffer sbQuery = new StringBuffer();
			sbQuery.setLength(0);
			sbQuery.append(" SELECT c626_IRB_ID irb_id,t614.C614_STUDY_SITE_ID study_site_id,c614_site_id || '-' || get_account_name(t614.C704_ACCOUNT_ID) site_name,get_code_name(C901_IRB_TYPE) irb_type,");
			sbQuery.append(" get_code_name(C901_Approval_Type) appr_type,C901_Approval_Type appr_type_id,TO_CHAR(C626_Approval_Date,'MM/DD/YYYY') appr_date, TO_CHAR(C626_Expiry_Date,'MM/DD/YYYY') exp_date");
			sbQuery.append(" FROM T614_STUDY_SITE t614,T626_IRB t626");
			sbQuery.append(" WHERE (nvl(t626.c626_expiry_date,to_date('01/01/9999','mm/dd/yyyy')), t626.c901_approval_type,t626.c614_study_site_id) IN");
			sbQuery.append(" (SELECT nvl(MAX (c626_expiry_date),to_date('01/01/9999','mm/dd/yyyy')), c901_approval_type,c614_study_site_id FROM T626_IRB");
			sbQuery.append(" WHERE c901_status = '60530' AND c901_approval_type NOT IN ('60522')");
			sbQuery.append(" GROUP BY c901_approval_type,c614_study_site_id )");	
			sbQuery.append(" AND t614.C611_STUDY_ID = NVL('" + strStudyId+"',t614.C611_STUDY_ID)");
			sbQuery.append(" AND t614.C614_STUDY_SITE_ID = t626.C614_STUDY_SITE_ID");
			sbQuery.append(" AND t626.C614_STUDY_SITE_ID = DECODE ('"+strStudySiteId+"','0',t626.C614_STUDY_SITE_ID,'',t626.C614_STUDY_SITE_ID,'"+strStudySiteId+"')");
			sbQuery.append(" AND t614.C614_VOID_FL IS NULL");
			//sbQuery.append(" AND t626.C626_Void_fl IS NULL");
			sbQuery.append(" AND t614.C901_IRB_TYPE = NVL("+strIRBTypeId +",t614.C901_IRB_TYPE)");
			sbQuery.append(" AND t626.C901_Approval_Type=NVL("+strIRBAppTypeId +",t626.C901_Approval_Type)");
			sbQuery.append(" AND t626.C901_Status = '60530' ");
			sbQuery.append(" AND C901_Approval_Type NOT IN ('60522')");
			if( !strStartDate.equalsIgnoreCase("") && !strEndDate.equalsIgnoreCase("")){
				if (strDateTypeId.equals("0"))
	            {   
					strDateField = "C626_APPROVAL_DATE";
					
	            }else{
	            	strDateField = "C626_EXPIRY_DATE";
	            }            	
	            sbQuery.append(" AND ");sbQuery.append(strDateField); sbQuery.append(" BETWEEN to_date('");
	    		sbQuery.append(strStartDate);
	    		sbQuery.append("','MM/DD/YYYY') AND to_date('");
	    		sbQuery.append(strEndDate);
	    		sbQuery.append("','MM/DD/YYYY')");  
			}
			sbQuery.append(" GROUP BY  c626_IRB_ID,t614.C614_STUDY_SITE_ID,get_account_name(t614.C704_ACCOUNT_ID),get_code_name(C901_IRB_TYPE),c614_site_id,");
			sbQuery.append(" get_code_name(C901_Approval_Type),C901_Approval_Type,C626_Approval_Date,C626_Expiry_Date ORDER BY C626_Approval_Date DESC");

			log.debug("Summmary Report sbQuery=>"+sbQuery);
			
		    ArrayList alIRBSummaryList = dbCon.queryMultipleRecords(sbQuery.toString());         
		  
		    log.debug("alIRBSummaryList=>"+alIRBSummaryList);
            log.debug("Exit");
            
            return alIRBSummaryList;          
    } // End of fetchIRBSummary
    

    
    /**
	 * @param HashMap
	 * fetchIRBApprovalByType 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchIRBApprovalByType(HashMap hmParam) throws AppError {           	  
          
    		log.debug(" fetchIRBApprovalByType hmParam :"+hmParam);
    		DBConnectionWrapper dbCon = null;
    		dbCon = new DBConnectionWrapper();         
    		
    		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
	    	String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYSITEID"));	        
	        String strIRBAppTypeId = GmCommonClass.parseNull((String)hmParam.get("IRBAPPTYPEID"));
	        
	        String strIRBReasonId = GmCommonClass.parseNull((String)hmParam.get("IRBREASONID"));
	        String strIRBStatusId = GmCommonClass.parseNull((String)hmParam.get("IRBSTATUSID"));
	        
	        String strDateTypeId = GmCommonClass.parseNull((String)hmParam.get("DATETYPEID"));
			String strStartDate  =  GmCommonClass.parseNull((String)hmParam.get("STARTDATE"));
			String strEndDate  =  GmCommonClass.parseNull((String)hmParam.get("ENDDATE"));
			String strSiteId=(String)hmParam.get("SITEID");
			strStudySiteId = strSiteId.equals("") ? strStudySiteId : strSiteId;
			if(strIRBReasonId!=null && (strIRBReasonId.equalsIgnoreCase("0") || strIRBReasonId.equalsIgnoreCase(""))){
				strIRBReasonId=null;
			}
			if(strIRBStatusId!=null && (strIRBStatusId.equalsIgnoreCase("0") || strIRBStatusId.equalsIgnoreCase("") )){
				strIRBStatusId=null;
			}
			if(strIRBAppTypeId!=null && (strIRBAppTypeId.equalsIgnoreCase("0") || strIRBAppTypeId.equalsIgnoreCase("") )){
				strIRBAppTypeId=null;
			}		

			String strDateField = "";
		  
		  	StringBuffer sbQuery = new StringBuffer();
			sbQuery.setLength(0);
			sbQuery.append(" SELECT DISTINCT t626.c626_IRB_ID irb_id,t614.C614_STUDY_SITE_ID study_site_id,c614_site_id || '-' || get_account_name(t614.C704_ACCOUNT_ID) site_name,");
			sbQuery.append(" get_code_name(C901_Approval_Type) appr_type,C901_Approval_Type appr_type_id,get_code_name(c901_status) status_name,");
			sbQuery.append(" get_code_name(c901_approval_reason) appr_reason,c626_expiry_date,c901_approval_reason appr_reason_id,");
			sbQuery.append(" DECODE(get_code_name(c901_status),'Approved',");
			sbQuery.append(" (SELECT TO_CHAR(C627_Event_date,'MM/DD/YYYY') FROM T627_IRB_EVENT t627 WHERE t627.C626_IRB_ID=t626.C626_IRB_ID AND C901_Action_Taken='60547' AND C627_Void_Fl IS NULL),'-') appr_ltr_date,");
			
			sbQuery.append(" DECODE(get_code_name(c901_status),'Approved',");
			sbQuery.append(" TO_CHAR(C626_Expiry_Date,'MM/DD/YYYY'),'-') expr_date,");
			
			sbQuery.append(" DECODE(get_code_name(C901_Approval_Reason),'Initial',");
			sbQuery.append(" TO_CHAR(t614.c614_study_pkt_date,'MM/DD/YYYY'),'-') study_pkt_sent,");
			
			sbQuery.append(" DECODE(get_code_name(C901_Approval_Reason),'Initial',");
			sbQuery.append(" TRUNC(((SELECT C627_Event_date FROM T627_IRB_EVENT t627 WHERE t627.C626_IRB_ID=t626.C626_IRB_ID AND C901_Action_Taken='60547'AND C627_Void_Fl IS NULL) "); 
			sbQuery.append("- (SELECT C627_Event_date FROM T627_IRB_EVENT t627 WHERE t627.C626_IRB_ID=t626.C626_IRB_ID AND C901_Action_Taken='60546'AND C627_Void_Fl IS NULL))/7,2),'-1') appr_days");

			sbQuery.append(" FROM T614_STUDY_SITE t614,T626_IRB t626,T627_IRB_EVENT t627");
			sbQuery.append(" WHERE t614.C614_STUDY_SITE_ID = t626.C614_STUDY_SITE_ID");			
			sbQuery.append(" AND t626.C614_STUDY_SITE_ID =  DECODE ('"+strStudySiteId+"','0',t626.C614_STUDY_SITE_ID,'',t626.C614_STUDY_SITE_ID,'"+strStudySiteId+"')");
			sbQuery.append(" AND t614.C611_STUDY_ID = NVL('" + strStudyId+"',t614.C611_STUDY_ID)");
			sbQuery.append(" AND t626.C626_IRB_ID = t627.C626_IRB_ID(+)");		
			sbQuery.append(" AND t614.C614_VOID_FL IS NULL");
			sbQuery.append(" AND t626.C626_VOID_FL IS NULL");
			sbQuery.append(" AND t627.C627_VOID_FL IS NULL");
			sbQuery.append(" AND t626.C901_Approval_Type=NVL("+strIRBAppTypeId +",t626.C901_Approval_Type)");
			sbQuery.append(" AND t626.C901_Approval_Reason=NVL("+strIRBReasonId +",t626.C901_Approval_Reason)");
			sbQuery.append(" AND NVL(t626.c901_status,-999)=NVL("+strIRBStatusId +",NVL(t626.c901_status,-999))");
			sbQuery.append(" AND C901_Approval_Type NOT IN ('60522')");
			if( !strStartDate.equalsIgnoreCase("") && !strEndDate.equalsIgnoreCase("")){
				if (strDateTypeId.equals("0"))
	            {   
					strDateField = "C627_EVENT_DATE";
					
	            }else{
	            	strDateField = "C626_EXPIRY_DATE";
	            }            	
	            sbQuery.append(" AND ");sbQuery.append(strDateField); sbQuery.append(" BETWEEN to_date('");
	    		sbQuery.append(strStartDate);
	    		sbQuery.append("','MM/DD/YYYY') AND to_date('");
	    		sbQuery.append(strEndDate);
	    		sbQuery.append("','MM/DD/YYYY')");
	    		if (strDateTypeId.equals("0"))
	            {
	    			sbQuery.append(" AND C901_Action_Taken = '60547'");
	            }
			}	
			sbQuery.append(" GROUP BY t626.c626_irb_id,t614.c614_study_site_id,get_account_name (t614.c704_account_id),get_code_name (c901_approval_type),");
			sbQuery.append(" get_code_name (c901_status),get_code_name (c901_approval_reason),c626_expiry_date,t614.c614_study_pkt_date,");
			sbQuery.append(" c901_status,C901_Approval_Type,c901_approval_reason,c614_site_id");
			sbQuery.append(" ORDER BY C626_Expiry_Date DESC");

			log.debug("IRB Type Report sbQuery=>"+sbQuery);
			
		    ArrayList alIRBTypeList = dbCon.queryMultipleRecords(sbQuery.toString());
    	
		    log.debug("alIRBTypeList=>"+alIRBTypeList);
            log.debug("Exit");
            
            return alIRBTypeList;          
    } // End of fetchIRBApprovalByType
    
    
    /**
	 * @param HashMap
	 * fetchIRBByEvent 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchIRBByEvent(HashMap hmParam) throws AppError {           	  
          
    		log.debug(" fetchIRBByEvent hmParam :"+hmParam);
    		DBConnectionWrapper dbCon = null;
    		dbCon = new DBConnectionWrapper();         
          
    		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
	    	String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYSITEID"));	        
	        String strIRBAppTypeId = GmCommonClass.parseNull((String)hmParam.get("IRBAPPTYPEID"));
	        String strIRBReasonId = GmCommonClass.parseNull((String)hmParam.get("IRBREASONID"));
	        String strIRBActionTakenId = GmCommonClass.parseNull((String)hmParam.get("IRBACTIONTAKENID"));
			String strStartDate  =  GmCommonClass.parseNull((String)hmParam.get("STARTDATE"));
			String strEndDate  =  GmCommonClass.parseNull((String)hmParam.get("ENDDATE"));
			String strIRBId = GmCommonClass.parseNull((String)hmParam.get("IRBID"));
			String strSiteId=(String)hmParam.get("SITEID");
			strStudySiteId = strSiteId.equals("") ? strStudySiteId : strSiteId;
			
			if(strIRBAppTypeId!=null && (strIRBAppTypeId.equalsIgnoreCase("0") || strIRBAppTypeId.equalsIgnoreCase("") )){
				strIRBAppTypeId=null;
			}
			if(strIRBReasonId!=null && (strIRBReasonId.equalsIgnoreCase("0") || strIRBReasonId.equalsIgnoreCase(""))){
				strIRBReasonId=null;
			}
			if(strIRBActionTakenId!=null && (strIRBActionTakenId.equalsIgnoreCase("0") || strIRBActionTakenId.equalsIgnoreCase(""))){
				strIRBActionTakenId=null;
			}
			if(strIRBId!=null && (strIRBId.equalsIgnoreCase("0") || strIRBId.equalsIgnoreCase("") )){
				strIRBId=null;
			}
		  
		  	StringBuffer sbQuery = new StringBuffer();
			sbQuery.setLength(0);
			sbQuery.append(" SELECT distinct c627_IRB_EVENT_ID irb_event_id,t614.C614_STUDY_SITE_ID study_site_id,c614_site_id || '-' || get_account_name(t614.C704_ACCOUNT_ID) site_name,");
			sbQuery.append(" get_code_name(c901_approval_Type) appr_type,get_code_name(c901_approval_reason) appr_reason,get_code_name(c901_action_taken) action_taken,TO_CHAR(C627_Event_date,'MM/DD/YYYY') event_date,c627_event_date");
			sbQuery.append(" FROM T614_STUDY_SITE t614,T626_IRB t626,T627_IRB_EVENT t627");
			sbQuery.append(" WHERE t614.C614_STUDY_SITE_ID = t626.C614_STUDY_SITE_ID");			
			sbQuery.append(" AND t626.C614_STUDY_SITE_ID =  DECODE ('"+strStudySiteId+"','0',t626.C614_STUDY_SITE_ID,'',t626.C614_STUDY_SITE_ID,'"+strStudySiteId+"')");
			sbQuery.append(" AND t614.C611_STUDY_ID = NVL('" + strStudyId+"',t614.C611_STUDY_ID)");
			sbQuery.append(" AND t627.C626_IRB_ID =  NVL(" + strIRBId+",t627.C626_IRB_ID)");
			sbQuery.append(" AND t626.c626_irb_id = t627.c626_irb_id");			
			sbQuery.append(" AND t614.C614_VOID_FL IS NULL");
			sbQuery.append(" AND t626.C626_VOID_FL IS NULL");
			sbQuery.append(" AND t627.C627_VOID_FL IS NULL");
			sbQuery.append(" AND t626.C901_Approval_Type=NVL("+strIRBAppTypeId +",t626.C901_Approval_Type)");
			sbQuery.append(" AND t626.C901_Approval_Reason=NVL("+strIRBReasonId +",t626.C901_Approval_Reason)");
			sbQuery.append(" AND t627.C901_Action_Taken=NVL("+strIRBActionTakenId +",t627.C901_Action_Taken)");
			sbQuery.append(" AND C901_Approval_Type NOT IN ('60522')");
			if( !strStartDate.equalsIgnoreCase("") && !strEndDate.equalsIgnoreCase("")){
	            sbQuery.append(" AND ");sbQuery.append("C627_Event_date"); sbQuery.append(" BETWEEN to_date('");
	    		sbQuery.append(strStartDate);
	    		sbQuery.append("','MM/DD/YYYY') AND to_date('");
	    		sbQuery.append(strEndDate);
	    		sbQuery.append("','MM/DD/YYYY')");  
			}
			
			sbQuery.append(" GROUP BY c627_irb_event_id,t614.c614_study_site_id,c614_site_id,get_account_name (t614.c704_account_id),");
			sbQuery.append(" get_code_name (c901_approval_type),get_code_name (c901_approval_reason),get_code_name (c901_action_taken),c627_event_date");
			sbQuery.append(" ORDER BY C627_Event_date DESC");

			log.debug("Event Report sbQuery=>"+sbQuery);
			
		    ArrayList alIRBEventList = dbCon.queryMultipleRecords(sbQuery.toString());         
		  
		    log.debug("alIRBEventList=>"+alIRBEventList);
            log.debug("Exit");
            
            return alIRBEventList;          
    } // End of fetchIRBByEvent
    
    /**
	 * @param HashMap
	 * fetchIRBByMediaType 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchIRBByMediaType(HashMap hmParam) throws AppError {           	  
          
    		log.debug(" fetchIRBByMediaType hmParam :"+hmParam);
    		DBConnectionWrapper dbCon = null;
    		dbCon = new DBConnectionWrapper();         
    		
    		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
	    	String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYSITEID"));	        
	        String strIRBAppTypeId = GmCommonClass.parseNull((String)hmParam.get("IRBAPPTYPEID"));
	        String strIRBReasonId = GmCommonClass.parseNull((String)hmParam.get("IRBREASONID"));
	        String strIRBMediaTypeId = GmCommonClass.parseNull((String)hmParam.get("IRBMEDIATYPEID"));
	        String strSiteId=(String)hmParam.get("SITEID");
			strStudySiteId = strSiteId.equals("") ? strStudySiteId : strSiteId;
			
	        if(strIRBAppTypeId!=null && (strIRBAppTypeId.equalsIgnoreCase("0") || strIRBAppTypeId.equalsIgnoreCase("") )){
				strIRBAppTypeId=null;
			}
			if(strIRBReasonId!=null && (strIRBReasonId.equalsIgnoreCase("0") || strIRBReasonId.equalsIgnoreCase(""))){
				strIRBReasonId=null;
			}
			if(strIRBMediaTypeId!=null && (strIRBMediaTypeId.equalsIgnoreCase("0") || strIRBMediaTypeId.equalsIgnoreCase(""))){
				strIRBMediaTypeId=null;
			}	
		  
		  	StringBuffer sbQuery = new StringBuffer();
			sbQuery.setLength(0);
			sbQuery.append(" SELECT c628_IRB_MEDIA_ID irb_media_id,t614.C614_STUDY_SITE_ID study_site_id,c614_site_id || '-' || get_account_name(t614.C704_ACCOUNT_ID) site_name,");
			sbQuery.append(" get_code_name(c901_approval_Type) appr_type,get_code_name(c901_approval_reason) appr_reason,get_code_name(c901_media_type) media_type,c628_notes notes");
			sbQuery.append(" FROM T614_STUDY_SITE t614,T626_IRB t626,T628_IRB_MEDIA t628");
			sbQuery.append(" WHERE t614.C614_STUDY_SITE_ID = t626.C614_STUDY_SITE_ID");			
			sbQuery.append(" AND t626.C614_STUDY_SITE_ID =  DECODE ('"+strStudySiteId+"','0',t626.C614_STUDY_SITE_ID,'',t626.C614_STUDY_SITE_ID,'"+strStudySiteId+"')");
			sbQuery.append(" AND t614.C611_STUDY_ID = NVL('" + strStudyId+"',t614.C611_STUDY_ID)");
			sbQuery.append(" AND t628.C626_IRB_ID = t626.C626_IRB_ID");
			sbQuery.append(" AND t614.C614_VOID_FL IS NULL");
			sbQuery.append(" AND t626.C626_VOID_FL IS NULL");			
			sbQuery.append(" AND t628.C628_VOID_FL IS NULL");
			sbQuery.append(" AND t626.C901_Approval_Type=NVL("+strIRBAppTypeId +",t626.C901_Approval_Type)");
			sbQuery.append(" AND t626.C901_Approval_Reason=NVL("+strIRBReasonId +",t626.C901_Approval_Reason)");
			sbQuery.append(" AND t628.c901_media_type=NVL("+strIRBMediaTypeId +",t628.c901_media_type)");			
			sbQuery.append(" ORDER BY t628.c628_created_date DESC");

			log.debug("Media Report sbQuery=>"+sbQuery);
			
		    ArrayList alIRBMediaTypeList = dbCon.queryMultipleRecords(sbQuery.toString());         
		  
		    log.debug("alIRBEventList=>"+alIRBMediaTypeList);
            log.debug("Exit");
            
            return alIRBMediaTypeList;          
    } // End of fetchIRBByMediaType        
 }// end of class GmClinicalIRBReportBean
