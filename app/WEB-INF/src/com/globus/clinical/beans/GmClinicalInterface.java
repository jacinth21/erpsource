package com.globus.clinical.beans;

import java.util.HashMap;

import com.globus.common.beans.AppError;

/**
 * This class used to call the primary end point report - based on the study
 * 
 * @author mmuthusamy
 *
 */
public interface GmClinicalInterface {

	/**
	 * 
	 * This method used to load the primary end point report - based on study,
	 * site and score values
	 * 
	 * Study - SECURE-C, TRIUMPH and Amnios
	 * 
	 * @param strstudyId
	 * @param strStudySiteIds
	 * @param strScoreType
	 * @return HashMap
	 * @throws AppError
	 * 
	 */

	public HashMap loadPrimaryEndpointRpt(String strstudyId,
			String strStudySiteIds, String strScoreType) throws AppError;

}
