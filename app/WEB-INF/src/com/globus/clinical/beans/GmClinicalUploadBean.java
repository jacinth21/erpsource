package com.globus.clinical.beans;

import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmFileReader;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;

import com.globus.common.dashboard.beans.GmClinicalDashBoardBean;
import com.globus.common.db.GmDBManager;

public class GmClinicalUploadBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * readXMLMaster - This Method is used to fetch the value from XML file
	 * about location in file and save the same in the ArrayList <br>
	 * XML format is <br>
	 * XML file in below format <br>
	 * **********************************<br>
	 * <Master> <Column> <Name>patient_Id</Name> <Location>0</Location>
	 * </Column> <Column> <Name>element_no</Name> <Location>17</Location>
	 * </Column> <Column> <Name>Period</Name> <Location>3</Location> </Column>
	 * <Column> <Name>Start_Point</Name> <Location>3</Location> </Column>
	 * </Master> Saving the data in below ArrayList format<br>
	 * ************************************** <br>
	 * {patient_Id=0, Period=3, element_no=17, Start_Point=3}
	 * 
	 * @param String
	 *            strXmlFile is xml file name
	 * @param String
	 *            strXmlRoot From which root the data is coming
	 * @return HashMap Contains converted data
	 * @exception SAXException,
	 *                IOException, ParserConfigurationException
	 */
	public HashMap readXMLMaster(String strXmlFile, String strXmlRoot) throws SAXException, IOException, ParserConfigurationException {
		File file = new File(strXmlFile);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		HashMap masterhmap = new HashMap();

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = (Document) db.parse(file);
		doc.getDocumentElement().normalize();
		NodeList nodeRoot = doc.getElementsByTagName(strXmlRoot);
		for (int n = 0; n < nodeRoot.getLength(); n++) {
			Node nodeRootNode = nodeRoot.item(n);

			if (nodeRootNode.getNodeType() == Node.ELEMENT_NODE) {
				Element nodeRootNodeElem = (Element) nodeRootNode;
				NodeList nodeLst = nodeRootNodeElem.getElementsByTagName("Column");
				for (int s = 0; s < nodeLst.getLength(); s++) {
					Node fstNode = nodeLst.item(s);

					if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
						Element fstElmnt = (Element) fstNode;
						NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("Name");
						Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
						NodeList fstNm = fstNmElmnt.getChildNodes();
						NodeList lstNmElmntLst = fstElmnt.getElementsByTagName("Location");
						Element lstNmElmnt = (Element) lstNmElmntLst.item(0);
						NodeList lstNm = lstNmElmnt.getChildNodes();
						masterhmap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) lstNm.item(0)).getNodeValue());

					}
					// return the masterhmap for all column
				}
			}
		}

		return masterhmap;

	}

	/**
	 * readXMLStudy - This Method is used to fetch the value from XML file about
	 * study id and save the same in the ArrayList <br>
	 * XML format is <br>
	 * XML file in below format <br>
	 * **********************************<br>
	 * <Study_Mapping> <Study_Period> <studyname>PreOp</studyname>
	 * <studymap>6300</studymap> </Study_Period> <Study_Period>
	 * <studyname>Month 3</studyname> <studymap>6303</studymap>
	 * </Study_Period> </Study_Mapping>
	 * 
	 * Saving the data in below ArrayList format<br>
	 * ************************************** <br>
	 * {Month 6=6304, Month 3=6303}
	 * 
	 * @param String
	 *            strXmlFile is xml file name
	 * @param String
	 *            strXmlRoot From which root the data is coming
	 * @return HashMap Contains converted data
	 * @exception SAXException,
	 *                IOException, ParserConfigurationException
	 */
	public HashMap readXMLStudy(String strXmlFile, String strXmlRoot) throws SAXException, IOException, ParserConfigurationException {
		File file = new File(strXmlFile);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		HashMap masterhmap = new HashMap();

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = (Document) db.parse(file);
		doc.getDocumentElement().normalize();

		NodeList nodeRoot = doc.getElementsByTagName(strXmlRoot);
		for (int n = 0; n < nodeRoot.getLength(); n++) {
			Node nodeRootNode = nodeRoot.item(n);

			if (nodeRootNode.getNodeType() == Node.ELEMENT_NODE) {
				Element nodeRootNodeElem = (Element) nodeRootNode;
				NodeList nodeLst = nodeRootNodeElem.getElementsByTagName("Study_Period");

				for (int s = 0; s < nodeLst.getLength(); s++) {
					Node fstNode = nodeLst.item(s);

					if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
						Element fstElmnt = (Element) fstNode;
						NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("studyname");
						Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
						NodeList fstNm = fstNmElmnt.getChildNodes();
						NodeList lstNmElmntLst = fstElmnt.getElementsByTagName("studymap");
						Element lstNmElmnt = (Element) lstNmElmntLst.item(0);
						NodeList lstNm = lstNmElmnt.getChildNodes();
						masterhmap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) lstNm.item(0)).getNodeValue());

					}
					// return the Studyhmap for all column
				}
			}
		}

		return masterhmap;

	}

	/**
	 * readXMLAnswer - This Method is used to fetch the value from XML file
	 * about answer and save the same in the ArrayList <br>
	 * XML format is <br>
	 * XML file in below format <br>
	 * **********************************<br>
	 * <Answer_Mapping> <Question> <ansmap>6</ansmap>
	 *         	<ansvalue>402</ansvalue>
	 *         	<anstype>DS</anstype>
	 * 		</Question>
	 * 		<Question> 
	 *        <ansmap>12</ansmap>
	 *        <ansvalue>404</ansvalue>
	 *        <anstype>DP</anstype>
	 *		  <Dropdown>
	 *			<DropdownType>-1</DropdownType>
	 *			<DropdownValue>0</DropdownValue>
	 *		  </Dropdown>
	 *		  <Dropdown>
	 *			<DropdownType>1</DropdownType>
	 *			<DropdownValue>193</DropdownValue>
	 *		  </Dropdown>
	 *		  <Dropdown>
	 *			<DropdownType>2</DropdownType>
	 *			<DropdownValue>194</DropdownValue>
	 *		  </Dropdown>
	 *		  <Dropdown>
	 *			<DropdownType>3</DropdownType>
	 *			<DropdownValue>195</DropdownValue>
	 *		  </Dropdown>
	 *   	</Question>
	 *  </Answer_Mapping>
	 *
	 * Saving the data in below ArrayList format<br>
	 * ************************************** <br>
	 * [{anstype=DS, ansmap=6, ansvalue=402}, {anstype=DP, ansmap=12, DP={3=195, -1=0, 2=194, 1=193}, ansvalue=404}]
	 * 
	 * @param String
	 *            strXmlFile is xml file name
	 * @param String
	 *            strXmlRoot From which root the data is coming
	 * @return HashMap Contains converted data
	 * @exception SAXException, IOException, ParserConfigurationException
	 */
	public ArrayList readXMLAnswer(String strXmlFile, String strXmlRoot) throws SAXException, IOException, ParserConfigurationException {

		ArrayList studyList = new ArrayList();
		File file = new File(strXmlFile);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = (Document) db.parse(file);
		doc.getDocumentElement().normalize();

		NodeList nodeRoot = doc.getElementsByTagName(strXmlRoot);
		for (int n = 0; n < nodeRoot.getLength(); n++) {
			Node nodeRootNode = nodeRoot.item(n);

			if (nodeRootNode.getNodeType() == Node.ELEMENT_NODE) {
				Element nodeRootNodeElem = (Element) nodeRootNode;
				NodeList nodeLst = nodeRootNodeElem.getElementsByTagName("Question");

				for (int s = 0; s < nodeLst.getLength(); s++) {
					Node fstNode = nodeLst.item(s);
					HashMap studyhmap = new HashMap();
					if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
						Element fstElmnt = (Element) fstNode;
						NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("ansmap");
						Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
						NodeList fstNm = fstNmElmnt.getChildNodes();
						NodeList lstNmElmntLst = fstElmnt.getElementsByTagName("ansvalue");
						Element lstNmElmnt = (Element) lstNmElmntLst.item(0);
						NodeList lstNm = lstNmElmnt.getChildNodes();
						NodeList sstNmElmntLst = fstElmnt.getElementsByTagName("anstype");
						Element sstNmElmnt = (Element) sstNmElmntLst.item(0);
						NodeList sstNm = sstNmElmnt.getChildNodes();
						NodeList thrdNmElmntLst = fstElmnt.getElementsByTagName("questionid");
						Element thrdNmElmnt = (Element) thrdNmElmntLst.item(0);
						//Question id will not be available for SC9 XML, So handling not null scenario before proceeding.
						if(thrdNmElmnt !=null){
							NodeList thrdNm = thrdNmElmnt.getChildNodes();
							studyhmap.put("questionid", ((Node) thrdNm.item(0)).getNodeValue());
						}						
						String strType = ((Node) sstNm.item(0)).getNodeValue();						
						studyhmap.put("ansmap", ((Node) fstNm.item(0)).getNodeValue());
						studyhmap.put("ansvalue", ((Node) lstNm.item(0)).getNodeValue());
						studyhmap.put("anstype", strType);

						if (strType.equals("DP")) {
							HashMap dropdown = new HashMap();
							NodeList innerElmntLst = fstElmnt.getElementsByTagName("Dropdown");
							// System.out.println("innerElmntLst " +
							// innerElmntLst.getLength());
							for (int i = 0; i < innerElmntLst.getLength(); i++) {
								Node innerNode = innerElmntLst.item(i);

								if (innerNode.getNodeType() == Node.ELEMENT_NODE) {
									Element innerElmnt = (Element) innerNode;
									NodeList innerNmElmntLst = innerElmnt.getElementsByTagName("DropdownType");
									Element innerNmElmnt = (Element) innerNmElmntLst.item(0);
									NodeList innerNm = innerNmElmnt.getChildNodes();
									NodeList innerNmElmntLst1 = innerElmnt.getElementsByTagName("DropdownValue");
									Element innerNmElmnt1 = (Element) innerNmElmntLst1.item(0);
									NodeList innerNm1 = innerNmElmnt1.getChildNodes();
									dropdown.put(((Node) innerNm.item(0)).getNodeValue(), ((Node) innerNm1.item(0)).getNodeValue());
								}
								studyhmap.put("DP", dropdown);
							}

						}
						if (strType.equals("DS")) {
							NodeList tstNmElmntLst = fstElmnt.getElementsByTagName("datatype");
							Element tstNmElmnt = (Element) tstNmElmntLst.item(0);
							NodeList tstNm = tstNmElmnt.getChildNodes();
							studyhmap.put("datatype", ((Node) tstNm.item(0)).getNodeValue());
						}

					}

					studyList.add(studyhmap);
				}
				// return the Answerhmap for all answers
			}
		}

		return studyList;

	}

	/**
	 * getStudyListId - This Method is used to Fetching StudyListId according to
	 * StudyList And FormList
	 * 
	 * @param String
	 *            strStudyList
	 * @param String
	 *            strFormList
	 * @return String
	 * @exception AppError
	 */
	public String getStudyListId(String strStudyList, String strFormList) throws AppError {
		// Fetching StudyListId according to StudyList And FormList
		HashMap hmReturn = null;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_dataupload.gm_fetch_study_list_id", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strStudyList);
		gmDBManager.setString(2, strFormList);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();

		return (String) hmReturn.get("C612_STUDY_LIST_ID");

	}

	/**
	 * saveToForm - This Method is used to Fetching value from XML file and .csv
	 * file and row by row save it to database
	 * 
	 * @param String
	 *            strUploadHome
	 * @param String
	 *            strFile
	 * @param String
	 *            strStudyListId
	 * @param String
	 *            strType
	 * @param String
	 *            strStudy
	 * @param String
	 *            strForm
	 * @param String
	 *            xmlFileName
	 * @param String
	 *            strXmlRoot
	 * @param String
	 *            strUser
	 * 
	 * @return int
	 * @exception AppError,
	 *                IOException, SAXException,ParserConfigurationException
	 * @throws SQLException
	 * @throws SQLException
	 */
	public void saveToForm(String strUploadHome, String strFile, String strStudyListId, String strType, String strStudy, String strForm, String xmlFileName,
			String strXmlRoot, String strUser) throws IOException, SAXException, ParserConfigurationException, AppError {
		// Saving the record one by one

		log.debug("file " + strUploadHome + strFile + " xmlFileName" + xmlFileName);
		ArrayList alfileread = new ArrayList();
		GmFileReader gmFileReader = new GmFileReader();
		HashMap hmMaster = readXMLMaster(xmlFileName, strXmlRoot);
		HashMap hmStudy = readXMLStudy(xmlFileName, strXmlRoot);
		ArrayList alAnswer = readXMLAnswer(xmlFileName, strXmlRoot);
		int strElementNo = Integer.parseInt((String) hmMaster.get("element_no"));
		int strStartPoint = Integer.parseInt((String) hmMaster.get("Start_Point"));
		// Reading file
		alfileread = gmFileReader.readFile(strUploadHome + strFile, strElementNo);
		GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
		GmClinicalDashBoardBean gmClinicalDashBoardBean = new GmClinicalDashBoardBean();
		log.debug("alfileread " + alfileread);
		log.debug("Master from XML " + hmMaster);
		log.debug("hmStudy from XML " + hmStudy);
		log.debug("alAnswer from XML " + alAnswer);
		log.debug("strStartPoint " + strStartPoint);
		GmDBManager gmDBManager = new GmDBManager();
		GmCommonBean gmCommonBean = new GmCommonBean();

		String strpatientId = "";
		String strstudyID = "";

		gmCommonUploadBean.saveUploadInfo(gmDBManager, strStudyListId, strType, strFile, strUser);
		gmCommonBean.saveDeleteMyTemp(gmDBManager);
		
		
		String strvalueds = "";
		String strhmType = "";
		String strAnswerGrpid = "";
		String strvaluecodeid = "";
		// reads value from CSV loop
		for (int i = strStartPoint; i < alfileread.size(); i++) {
			ArrayList oneAnswer = (ArrayList) alfileread.get(i);

			strpatientId = String.valueOf(oneAnswer.get(Integer.parseInt((String) hmMaster.get("patient_Id"))));
			strstudyID = (String) oneAnswer.get(Integer.parseInt((String) hmMaster.get("Period")));
			String period = (String) hmStudy.get(strstudyID);
			String strPatientlistId = "";
			try {
				String strPeriodID = getPatient_Period_Id(gmDBManager, period, strForm);
				String strPatientId = getPatient_Id(gmDBManager, strpatientId, strStudy);
				strPatientlistId = getPatientListId(gmDBManager, strPatientId, strPeriodID);
			} catch (AppError e) {
				throw new AppError("Record not found for patient id: " + strpatientId + " and Study period: " + strstudyID + ", Please Validate.", "", 'E');
			}
			// loop thru answer & update all answer
			for (int j = 0; j < alAnswer.size(); j++) {
				HashMap hmAnswer = (HashMap) alAnswer.get(j);

				strhmType = (String) hmAnswer.get("anstype");
				if (strhmType.equals("DS")) {
					strvalueds = validateDS((String) oneAnswer.get(Integer.parseInt((String) hmAnswer.get("ansmap"))), (String) hmAnswer.get("datatype"));
				} else {
					strvalueds = (String) oneAnswer.get(Integer.parseInt((String) hmAnswer.get("ansmap")));
				}
				strAnswerGrpid = (String) hmAnswer.get("ansvalue");
				strvaluecodeid = "0";
				// if anstype=DP means it is about DropDown so we have to
				// save valuecodeid(Dropdown value) and not
				// dsvalue(Description)
				if (strhmType.equals("DP")) {
					HashMap DP = (HashMap) hmAnswer.get("DP");
					strvaluecodeid = GmCommonClass.parseZero((String) DP.get(strvalueds));

					strvalueds = null;
				}
				updatePatientList(gmDBManager, strPatientlistId, strAnswerGrpid, strvalueds, strvaluecodeid, strUser, strhmType);
			}
			String strSkipverify = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strStudy,"SKIP_VERIFY"));//Based on rule value we are deciding to skip verifying the forms
			HashMap hmParams = new HashMap();
			hmParams.put("PATIENTLISTID", strPatientlistId);
			hmParams.put("UPLOADEDFILENAME", strFile);
			hmParams.put("USERID", strUser);
			hmParams.put("SKIPVERIFY", strSkipverify);//setting skipverify value as 'N' and updated the verify flag value for forms based on the condition. 
			updateVerifyList(gmDBManager, hmParams);
		}
		// alfileread1 = gmClinicalDashBoardBean.rVerifyList(gmDBManager, "",
		// "", "U");
		
		//gmDBManager.commit();
		throw new AppError("Data Upload Successful.", "", 'S');

	}

	public void saveToSF36Form(HashMap hmParams) throws IOException, SAXException, ParserConfigurationException, AppError {
		String strUploadHome = GmCommonClass.parseNull((String) hmParams.get("UPLOADHOME"));
		String strUpLoadedFileName = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFILENAME"));
		String strType = GmCommonClass.parseNull((String) hmParams.get("TYPE"));
		String strStudyList = GmCommonClass.parseNull((String) hmParams.get("STUDYLIST"));
		String strStudyListId = GmCommonClass.parseNull((String) hmParams.get("STUDYLISTID"));
		String strXmlFileName = GmCommonClass.parseNull((String) hmParams.get("XMLFILENAME"));
		String strXmlRoot = GmCommonClass.parseNull((String) hmParams.get("ROOT"));
		String strUploadFileName = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFILENAME"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		String strPatientIde = "";
		String strExamDate = "";
		String strPatientId = "";
		String strTimePoint = "";
		int intPatListId = 0;
		String strAnswerGrpId = "";
		String strAnswer = "";
		String strQuesId = "";
		int intPatientAnsListId = 0;

		ArrayList alfileread = new ArrayList();

		GmFileReader gmFileReader = new GmFileReader();
		GmDBManager gmDBManager = new GmDBManager();
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();

		HashMap hmMaster = readXMLMaster(strXmlFileName, strXmlRoot);
		ArrayList alAnswer = readXMLAnswer(strXmlFileName, strXmlRoot);

		int intElementNo = Integer.parseInt((String) hmMaster.get("element_no"));
		int intStartPoint = Integer.parseInt((String) hmMaster.get("Start_Point"));

		boolean validate;
		String strFormLockValue = getFormLockValue(gmDBManager, hmParams);
		log.debug("strFormLockValue" + strFormLockValue);
		if (strFormLockValue.equalsIgnoreCase("Y")) {

			gmCommonUploadBean.saveUploadInfo(gmDBManager, strStudyListId, strType, strUploadFileName, strUserId);
			gmCommonBean.saveDeleteMyTemp(gmDBManager);

			alfileread = gmFileReader.readFile(strUploadHome + strUpLoadedFileName, intElementNo);

			log.debug("alfileread size " + alfileread.size());
			log.debug("Master from XML " + hmMaster);
			log.debug("alAnswer from XML " + alAnswer);
			for (int i = intStartPoint; i <= alfileread.size(); i++) {
				ArrayList oneAnswer = (ArrayList) alfileread.get(i - 1);
				strPatientIde = String.valueOf(oneAnswer.get(Integer.parseInt((String) hmMaster.get("patient_Id"))));
				// log.debug("strPatientIde " + strPatientIde +"\n"+" oneAnswer:
				// "+oneAnswer+"\n ExamDate From XML :"+(String)
				// hmMaster.get("ExamDate"));
				strExamDate = String.valueOf(oneAnswer.get(Integer.parseInt((String) hmMaster.get("ExamDate"))));
				if (!strExamDate.equals("")) {
					String sbExamDate[] = strExamDate.split(" ");
					strExamDate = sbExamDate[0];
				}
				log.debug("strExamDate " + strExamDate);
				try {
					strPatientId = getPatient_Id(gmDBManager, strPatientIde, strStudyList);
				} catch (AppError e) {
					throw new AppError("Record not found for patient id: " + strPatientIde + " and selected Study, Please Validate.", "",
							'E');
				}
				log.debug("strPatientId " + strPatientId);
				hmParams.put("PATIENTID", strPatientId);
				hmParams.put("EXAMDATE", strExamDate);
				
				// As per issue 9521, all data for SF-36 has to be uploaded back to the form.
				
				intPatListId = verifyPatientForm(gmDBManager, hmParams);
				log.debug("intPatListId " + intPatListId);

				strTimePoint = getPatientPeriodId(gmDBManager, hmParams);
				log.debug("strTimePoint " + strTimePoint);
				
				hmParams.put("STUDYPERIODID", strTimePoint);
				if(intPatListId==0){					
					hmParams.put("PATIENTLISTID", "0");
					intPatListId = savePatientList(gmDBManager, hmParams);					
				}
				hmParams.put("PATIENTLISTID", intPatListId + "");
				
				log.debug("strPatListId " + intPatListId);
				for (int j = 0; j < alAnswer.size(); j++) {
					HashMap hmAnswer = (HashMap) alAnswer.get(j);
					strAnswer = (String) oneAnswer.get(Integer.parseInt((String) hmAnswer.get("ansmap")));
					validate = validateAnswer(strAnswer);
					log.debug("strAnswer orig" + strAnswer);
					if (validate == false) {
						throw new AppError("Invalid Data "+strAnswer+" for PatientId " + strPatientIde + ",ExamDate"+strExamDate+",Please Validate.", "",
								'E');
					}
					strAnswerGrpId = (String) hmAnswer.get("ansvalue");
					strQuesId = (String) hmAnswer.get("questionid");
					hmParams.put("ANSDESC", strAnswer);
					hmParams.put("ANSGRPID", strAnswerGrpId);
					hmParams.put("QUESTIONID", strQuesId);

					intPatientAnsListId = savePatientAnswer(gmDBManager, hmParams);
					log.debug("intPatientAnsListId " + intPatientAnsListId);
				}
				releaseFormLock(gmDBManager, hmParams);
				hmParams.put("SKIPVERIFY", "Y");
				updateVerifyList(gmDBManager, hmParams);
			}
			if (alfileread.size() > 1) {
				//Commenting this method call as we need not display the list of patient that is missing in the Uploaded File.
				//As this scenario does not happen in real time scenario , the check is not required.
				
				//String strPatients = verifyPatientFormLock(gmDBManager, hmParams);
				
				hmParams.put("DOWNLOADTYPE", "COMPLETE");
				releaseDownloadLock(gmDBManager, hmParams);
				gmDBManager.commit();

				
				throw new AppError("Data Upload Successful.", "", 'S');
				
			} else {
				throw new AppError("No Data found for uploading ", "", 'E');
			}
		} else {
			throw new AppError("Cannot proceed with the upload as form is not locked for download ", "", 'E');
		}
	}

	private boolean validateAnswer(String ansValue) {
		/*for (int i = 0; i < ansValue.length(); i++) {
			if (Character.isDigit(ansValue.charAt(i)) == true || ansValue.charAt(i) == '.') {
				if (ansValue.charAt(i) == '.') {
					// chk the decimal digits
					String[] strValueSplit= ansValue.split("\\.");				  
					if (strValueSplit[1].length()>2){
						return false;
					}
				}
			} else
				return false;
		}*/
		String strRegExp = "\\d{0,3}(\\.\\d{0,2})?";
		return ansValue.matches(strRegExp);
		
	}

	private String validateDS(String ansValue, String datatype) {
		// TODO Auto-generated method stub
		String ansReturnValue = "";

		if (datatype.equals("NUMERIC")) {
			try {
				double i = Double.parseDouble(ansValue);
				ansReturnValue = i + "";
			} catch (Exception e) {
				ansReturnValue = "";
			}

		}
		return ansReturnValue;

	}

	/**
	 * updatePatientList - This Method is used to Updating a record in list
	 * 
	 * @param GmDBManager
	 *            gmDBManager
	 * @param String
	 *            strPatientlistId
	 * @return void
	 * @exception AppError
	 */
	private void updatePatientList(GmDBManager gmDBManager, String strPatientlistId, String strAnswerGrpid, String strvalueds, String strvaluecodeid,
			String strUser, String strhmType) throws AppError {
		// Update a record in list
		gmDBManager.setPrepareString("gm_pkg_cr_dataupload.gm_update_patient_list", 6);
		gmDBManager.setString(1, strPatientlistId);
		gmDBManager.setString(2, strAnswerGrpid);
		gmDBManager.setString(3, strvalueds);
		gmDBManager.setString(4, strvaluecodeid);
		gmDBManager.setString(5, strUser);
		gmDBManager.setString(6, strhmType);
		gmDBManager.execute();
		gmDBManager.closeCallableStatement();

	}

	/**
	 * updateVerifyList - This Method is used to insert a record in verified
	 * list
	 * 
	 * @param GmDBManager
	 *            gmDBManager
	 * @param String
	 *            strPatientlistId
	 * @return void
	 * @exception AppError
	 */
	private void updateVerifyList(GmDBManager gmDBManager, HashMap hmParams) throws AppError {
		// insert a record in verified list

		String strPatientlistId = GmCommonClass.parseNull((String) hmParams.get("PATIENTLISTID"));
		String strFile = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFILENAME"));
		String strUser = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		String strSkipVerify = GmCommonClass.parseNull((String) hmParams.get("SKIPVERIFY"));

		gmDBManager.setPrepareString("gm_pkg_cr_dataupload.GM_UPDATE_VERIFY_LIST", 4);
		gmDBManager.setString(1, strPatientlistId);
		gmDBManager.setString(2, "File Upload : " + strFile);
		gmDBManager.setString(3, strUser);
		gmDBManager.setString(4, strSkipVerify);
		gmDBManager.execute();
		gmDBManager.commit();

	}

	/**
	 * getPatient_Period_Id - This Method is used to Fetching Patient_Period_Id
	 * according to Studyperiod And FormNo
	 * 
	 * @param GmDBManager
	 *            gmDBManager
	 * @param String
	 *            studyPeriod
	 * @param String
	 *            formNo
	 * @return String
	 * @exception AppError
	 */
	private String getPatient_Period_Id(GmDBManager gmDBManager, String strStudyPeriod, String strFormNo) throws AppError {
		// Fetching Patient_Period_Id according to Studyperiod And FormNo
		String retValue = "";
		gmDBManager.setFunctionString("gm_pkg_cr_dataupload.get_study_period_id", 2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strStudyPeriod);
		gmDBManager.setString(3, strFormNo);
		gmDBManager.execute();
		retValue = gmDBManager.getString(1);
		gmDBManager.closeCallableStatement();

		return retValue;

	}

	/**
	 * getPatientListId - This Method is used to Fetching PatientListId
	 * according to strPatientId And strPeriodID
	 * 
	 * @param GmDBManager
	 *            gmDBManager
	 * @param String
	 *            strPatientId
	 * @param String
	 *            strPeriodID
	 * @return String
	 * @exception AppError
	 */
	private String getPatientListId(GmDBManager gmDBManager, String strPatientId, String strPeriodID) throws AppError {
		// Fetching PatientListId according to strPatientId And strPeriodID
		String retValue = "";
		gmDBManager.setFunctionString("gm_pkg_cr_dataupload.get_patient_list_id", 2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strPatientId);
		gmDBManager.setString(3, strPeriodID);
		gmDBManager.execute();
		retValue = gmDBManager.getString(1);
		gmDBManager.closeCallableStatement();
		return retValue;
	}

	/**
	 * getPatient_Id - This Method is used to Fetching Patient_Id according to
	 * patientId And study
	 * 
	 * @param GmDBManager
	 *            gmDBManager
	 * @param String
	 *            strPatientId
	 * @param String
	 *            strStudy
	 * @return String
	 * @exception AppError
	 * @throws SQLException
	 */
	private String getPatient_Id(GmDBManager gmDBManager, String strPatientId, String strStudy) throws AppError {
		// Fetching Patient_Id according to patientId And study
		String retValue = "";
		gmDBManager.setFunctionString("gm_pkg_cr_dataupload.get_patient_id", 2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strPatientId);
		gmDBManager.setString(3, strStudy);
		gmDBManager.execute();
		retValue = gmDBManager.getString(1);
		gmDBManager.closeCallableStatement();
		return retValue;
	}

	/**
	 * This FUNCTION is used to check if the patient has a SF36 form
	 * 
	 * @param gmDBManager
	 * @param hmParas
	 * @return
	 * @throws AppError
	 */
	private int verifyPatientForm(GmDBManager gmDBManager, HashMap hmParams) throws AppError {
		int intReturn = 0;
		String strPatientId = GmCommonClass.parseNull((String) hmParams.get("PATIENTID"));
		String strExamDate = GmCommonClass.parseNull((String) hmParams.get("EXAMDATE"));
		String strFormId = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFORMID"));
		// log.debug("strPatientId " + strPatientId);
		// log.debug("strExamDate " + strExamDate);
		// log.debug("strFormId " + strFormId);

		gmDBManager.setFunctionString("gm_pkg_cr_dataupload.GET_PATIENT_FORM", 3);
		gmDBManager.registerOutParameter(1, java.sql.Types.INTEGER);
		gmDBManager.setString(2, strPatientId);
		gmDBManager.setString(3, strFormId);
		gmDBManager.setString(4, strExamDate);
		gmDBManager.execute();

		intReturn = Integer.parseInt(gmDBManager.getString(1));
		gmDBManager.closeCallableStatement();
		return intReturn;
	}

	/**
	 * This Function is used to get the timepoint for a patient , exam date,
	 * form
	 * 
	 * @param gmDBManager
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	private String getPatientPeriodId(GmDBManager gmDBManager, HashMap hmParams) throws AppError {

		String strRetValue = "";
		String strPatientId = GmCommonClass.parseNull((String) hmParams.get("PATIENTID"));
		String strExamDate = GmCommonClass.parseNull((String) hmParams.get("EXAMDATE"));
		String strFormId = GmCommonClass.parseNull((String) hmParams.get("DOWNLOADEDFORMID"));
		String strStudyList = GmCommonClass.parseNull((String) hmParams.get("STUDYLIST"));
		String strUploadedFormId = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFORMID"));

		// log.debug("strPatientId " + strPatientId);
		// log.debug("strExamDAte " + strExamDate);
		// log.debug("strFormId " + strFormId);
		gmDBManager.setFunctionString("gm_pkg_cr_dataupload.GET_PATIENT_PERIOD_ID", 5);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strPatientId);
		gmDBManager.setString(3, strFormId);
		gmDBManager.setString(4, strExamDate);
		gmDBManager.setString(5, strStudyList);
		gmDBManager.setString(6, strUploadedFormId);
		gmDBManager.execute();
		strRetValue = gmDBManager.getString(1);
		gmDBManager.closeCallableStatement();
		return strRetValue;
	}

	/**
	 * This procedure will create/update a record in the T622_Patient_list table
	 * 
	 * @param gmDBManager
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	private int savePatientList(GmDBManager gmDBManager, HashMap hmParams) throws AppError {
		String strPatientId = GmCommonClass.parseNull((String) hmParams.get("PATIENTID"));
		String strPeriodID = GmCommonClass.parseNull((String) hmParams.get("STUDYPERIODID"));
		String strExamDate = GmCommonClass.parseNull((String) hmParams.get("EXAMDATE"));
		String strFormId = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFORMID"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		String strInitial = GmCommonClass.parseNull((String) hmParams.get("INITIAL"));
		String strCalValue = GmCommonClass.parseNull((String) hmParams.get("CALVALUE"));
		String strType = GmCommonClass.parseNull((String) hmParams.get("TYPE"));
		String strWindowFl = GmCommonClass.parseNull((String) hmParams.get("WINDOWFL"));
		String strFollowUpFl = GmCommonClass.parseNull((String) hmParams.get("FOLLOWUPFL"));
		String strEventNO = GmCommonClass.parseNull((String) hmParams.get("EVENTNO"));
		String strPatientListId = GmCommonClass.parseZero((String) hmParams.get("PATIENTLISTID"));

		gmDBManager.setPrepareString("gm_pkg_cr_common.GM_SAV_PATIENT_LIST", 12);

		gmDBManager.setString(1, strExamDate);
		gmDBManager.setString(2, strInitial);
		gmDBManager.setString(3, strCalValue);
		gmDBManager.setString(4, strFormId);
		gmDBManager.setString(5, strPatientId);
		gmDBManager.setString(6, strType);
		gmDBManager.setString(7, strPeriodID);
		gmDBManager.setString(8, strWindowFl);
		gmDBManager.setString(9, strFollowUpFl);
		gmDBManager.setString(10, strEventNO);
		gmDBManager.setString(11, strUserId);
		gmDBManager.setInt(12, Integer.parseInt(strPatientListId));
		gmDBManager.registerOutParameter(12, java.sql.Types.INTEGER);

		gmDBManager.execute();

		strPatientListId = GmCommonClass.parseNull(gmDBManager.getString(12));

		return Integer.parseInt(strPatientListId);
	}

	/**
	 * This procedure will create/update a record in the
	 * T623_PATIENT_ANSWER_LIST table
	 * 
	 * @param gmDBManager
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	private int savePatientAnswer(GmDBManager gmDBManager, HashMap hmParams) throws AppError {
		String strPatientListId = GmCommonClass.parseNull((String) hmParams.get("PATIENTLISTID"));
		String strQuestionId = GmCommonClass.parseNull((String) hmParams.get("QUESTIONID"));
		String strAnsListId = GmCommonClass.parseNull((String) hmParams.get("ANSLISTID"));
		String strAnsGrpId = GmCommonClass.parseNull((String) hmParams.get("ANSGRPID"));
		String strAnsDesc = GmCommonClass.parseNull((String) hmParams.get("ANSDESC"));
		String strFormId = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFORMID"));
		String strPatientId = GmCommonClass.parseNull((String) hmParams.get("PATIENTID"));
		String strTimePoint = GmCommonClass.parseNull((String) hmParams.get("STUDYPERIODID"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		String strPatientAnsListId = GmCommonClass.parseZero((String) hmParams.get("PATIENTANSLISTID"));
		
		log.debug("  # # # hmParams :"+hmParams);
		// log.debug("strQuestionId "+strQuestionId );
		// log.debug("strAnsListId "+ strAnsListId);
		// log.debug("strAnsGrpId "+strAnsGrpId );
		// log.debug("strAnsDesc "+strAnsDesc );
		// log.debug("strFormId "+strFormId );
		// log.debug("strPatientId "+strPatientId );

		gmDBManager.setPrepareString("gm_pkg_cr_dataupload.GM_SAVE_PATIENT_ANSWER", 10);

		gmDBManager.setString(1, strPatientListId);
		gmDBManager.setString(2, strQuestionId);
		gmDBManager.setString(3, strAnsListId);
		gmDBManager.setString(4, strAnsGrpId);
		gmDBManager.setString(5, strAnsDesc);
		gmDBManager.setString(6, strFormId);
		gmDBManager.setString(7, strPatientId);
		gmDBManager.setString(8, strTimePoint);
		gmDBManager.setString(9, strUserId);
		gmDBManager.registerOutParameter(10, java.sql.Types.INTEGER);
		gmDBManager.execute();

		strPatientAnsListId = gmDBManager.getString(10);

		return Integer.parseInt(strPatientAnsListId);

	}

	/**
	 * This procedure is used to release the form lock of the patient
	 * 
	 * @param gmDBManager
	 * @param hmParams
	 * @throws AppError
	 */
	private void releaseFormLock(GmDBManager gmDBManager, HashMap hmParams) throws AppError {
		String strPatientId = GmCommonClass.parseNull((String) hmParams.get("PATIENTID"));
		String strExamDate = GmCommonClass.parseNull((String) hmParams.get("EXAMDATE"));
		String strFormId = GmCommonClass.parseNull((String) hmParams.get("DOWNLOADEDFORMID"));
		String strPeriodID = GmCommonClass.parseNull((String) hmParams.get("STUDYPERIODID"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));

		gmDBManager.setPrepareString("gm_pkg_cr_common.GM_SAV_PATIENT_VERIFY_LIST", 5);

		gmDBManager.setString(1, strPatientId);
		gmDBManager.setString(2, strFormId);
		gmDBManager.setString(3, strPeriodID);
		gmDBManager.setString(4, strExamDate);
		gmDBManager.setString(5, strUserId);

		gmDBManager.execute();

	}

	/**
	 * This procedure is used to verify if there is any patient who is having
	 * lock set on the form (SD-6).
	 * 
	 * @param gmDBManager
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	private String verifyPatientFormLock(GmDBManager gmDBManager, HashMap hmParams) throws AppError {
		String strRetValue = "";
		ArrayList alLockedRecs = new ArrayList();
		HashMap hmfrmLckDetails = new HashMap();

		String strStudyID = GmCommonClass.parseNull((String) hmParams.get("STUDYLIST"));
		String strUpLoadFormId = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFORMID"));
		String strDownLoadFormId = GmCommonClass.parseNull((String) hmParams.get("DOWNLOADEDFORMID"));

		gmDBManager.setPrepareString("gm_pkg_cr_common.GM_FCH_PATIENT_FORM_LOCK", 4);

		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

		gmDBManager.setString(1, strStudyID);
		gmDBManager.setString(2, strUpLoadFormId);
		gmDBManager.setString(3, strDownLoadFormId);

		gmDBManager.execute();
		alLockedRecs = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		int intcount = 0;
		for (Iterator iter = alLockedRecs.iterator(); iter.hasNext();) {
			hmfrmLckDetails = (HashMap) iter.next();
			strRetValue += (String) hmfrmLckDetails.get("PAT_IDE") + ", ";
			intcount++;
			if (intcount == 10) {
				strRetValue += "<br>";
			}
		}
		return strRetValue;
	}

	/**
	 * This procedure will be used when the user is cancelling a download or
	 * when the upload is complete.
	 * 
	 * @param gmDBManager
	 * @param hmParams
	 * @throws AppError
	 */
	private void releaseDownloadLock(GmDBManager gmDBManager, HashMap hmParams) throws AppError {

		String strStudyId = GmCommonClass.parseNull((String) hmParams.get("STUDYLIST"));
		String strDownloadedFormId = GmCommonClass.parseNull((String) hmParams.get("DOWNLOADEDFORMID"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		String strReason = GmCommonClass.parseNull((String) hmParams.get("REASON"));
		String strType = GmCommonClass.parseNull((String) hmParams.get("DOWNLOADTYPE"));
		String strUploadedFormId = GmCommonClass.parseNull((String) hmParams.get("UPLOADEDFORMID"));

		gmDBManager.setPrepareString("gm_pkg_cr_datadownload.GM_CANCEL_DOWNLOAD", 6);

		gmDBManager.setString(1, strStudyId);
		gmDBManager.setString(2, strDownloadedFormId);
		gmDBManager.setString(3, strUserId);
		gmDBManager.setString(4, strReason);
		gmDBManager.setString(5, strType);
		gmDBManager.setString(6, strUploadedFormId);

		gmDBManager.execute();

	}

	/**
	 * This FUNCTION is used to check if the form is locked
	 * 
	 * @param gmDBManager
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	private String getFormLockValue(GmDBManager gmDBManager, HashMap hmParams) throws AppError {

		String retValue = "";
		String strStudyId = GmCommonClass.parseNull((String) hmParams.get("STUDYLIST"));
		String strDownloadedFormId = GmCommonClass.parseNull((String) hmParams.get("DOWNLOADEDFORMID"));
		gmDBManager.setFunctionString("gm_pkg_cr_dataupload.get_form_lock_value", 2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strStudyId);
		gmDBManager.setString(3, strDownloadedFormId);
		gmDBManager.execute();
		retValue = GmCommonClass.parseNull((String) gmDBManager.getString(1));
		return retValue;
	}
}
