package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmAdverseEventRptBean implements GmClinicalAdverseEventInterface
{

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

    /**
     * loadAdverseEvent - This Method is used to report patient satisfaction
     * @param String strStudyListId, String strSiteId, String strPatientId 
     * @return HashMap
     * @exception AppError
   **/
     public ArrayList loadAdverseEvent(String strStudyListId, String strSiteId, String strPatientId, String strOpt, String strTimePoint ) throws AppError
     {
         GmDBManager gmDBManager = new GmDBManager ();
         RowSetDynaClass rdResult = null;
         ResultSet rs = null;
         ArrayList alReturn = new ArrayList();
         

         gmDBManager.setPrepareString("gm_pkg_cr_adverseevent_rpt.gm_cr_fch_patient_adverseevent",6);
         gmDBManager.registerOutParameter(6,OracleTypes.CURSOR);
         gmDBManager.setString(1,strStudyListId);
         gmDBManager.setString(2,strSiteId);
         gmDBManager.setString(3,strPatientId);
         gmDBManager.setString(4,strOpt);
         gmDBManager.setString(5,strTimePoint);
         gmDBManager.execute();
         rs = (ResultSet)gmDBManager.getObject(6);
         alReturn = gmDBManager.returnArrayList(rs);
         log.debug(" Return list is " + alReturn);
         //rdResult  = gmDBManager.returnRowSetDyna(rs);
         gmDBManager.close();
         

         // return rdResult;
        return alReturn;
       }
     public String loadSiteId(String strStudyId) throws AppError
     {
     GmPatientBean gmPatientBean = new GmPatientBean();
     ArrayList alSiteId = new ArrayList();
     alSiteId = gmPatientBean.loadSiteFromStudy(strStudyId); 
    
     StringBuffer sbSiteId = new StringBuffer();
		int intCartSize = alSiteId.size();
		HashMap hmCart = new HashMap();;
		String strValue = "";
		 for (int i=0;i<intCartSize;i++)
		 {
		  hmCart = (HashMap)alSiteId.get(i);
			  strValue = (String)hmCart.get("ID");
			  sbSiteId.append(strValue+", ");

		 }
		 return sbSiteId.toString();
     }
 
     /**
 	 * common method used to get the grid xml data
 	 * 
 	 * @date June 06, 2010
 	 * @param
 	 */

 	public String getXmlGridData(HashMap hmParamV) throws AppError {

 		GmTemplateUtil templateUtil = new GmTemplateUtil();

 		ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
 		String strTemplate = (String) hmParamV.get("TEMPLATE");
 		String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
 		HashMap hmData = GmCommonClass.parseNullHashMap((HashMap) hmParamV.get("hmData"));

 		HashMap hmParam = new HashMap();
 		
 		templateUtil.setDataList("alResult", alParam);
 		templateUtil.setDataMap("hmData", hmData);
 		templateUtil.setTemplateName(strTemplate);
 		templateUtil.setTemplateSubDir(strTemplatePath);

 		return templateUtil.generateOutput();
 	}	
}
