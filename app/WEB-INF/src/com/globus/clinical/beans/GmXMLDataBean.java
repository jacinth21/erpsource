/*
 * Module:       GmXMLDataBean.java
 * Author:       DJames
 * Project:      Globus Medical App
 * Date-Written: Apr 2006
 * Security:     Unclassified
 * Description:  This bean contains methods that are used in the Sales Growth Reports
 *
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */

package com.globus.clinical.beans;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;


import oracle.jdbc.driver.OracleTypes;


import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;


public class GmXMLDataBean {

	public static void main(String[] args) throws Exception
	{
		GmXMLDataBean gmXml = new GmXMLDataBean();
	}
	private static final boolean bolDEBUG = false;
	public GmXMLDataBean() throws Exception
	{
		
		//loadPatientData("SC2","SC2");
		//loadPatientData("SC5","SC5");
		//loadPatientData("SC7","SC7");
		//loadPatientData("SC8","SC8");
		//loadPatientData("SC9","SC9");
		//loadPatientData("SC10","SC10");
		loadPatientData("SC11","SC11");
		//loadPatientData("SC14","SC14");
		//loadPatientList("GPR002");
		
		
	}
	
	/**
	  * loadPatientList - This Method is used to fetch data for all Groups 
	  * 						for a Growth Type report
	  * @param String strDistributorID (Contains the Selected Distributor ID) To be added 
	  * @return ArrayList
	  * @exception AppError
	**/
	 public void loadPatientList(String strStudyId) throws AppError
	 {
		DBConnectionWrapper dbCon = null;
		Connection conn = null;
		String strPrepareString = null;
		ResultSet rs = null;
		CallableStatement csmt = null;
		
		ArrayList alReturn = new ArrayList();
	  	String strPatientId = "";
	  	String strPatientIDEId = "";
	  	
	  	int intSize = 0;
	  	
		try
		{
			dbCon = new DBConnectionWrapper();
			conn = dbCon.getOracleStaticConnection();
			
			strPrepareString = dbCon.getStrPrepareString("gm_report_xml_patient_list",2);
			csmt = conn.prepareCall(strPrepareString);
			/*
			 *register out parameter and set input parameters
			 */
			csmt.registerOutParameter(2,OracleTypes.CURSOR);
			csmt.setString(1,strStudyId);
	
			csmt.execute();
			
			rs = (ResultSet)csmt.getObject(2);
			alReturn = dbCon.returnArrayList(rs);
			
		 	StringBuffer sbXMLData = new StringBuffer();
			HashMap hmLoop = new HashMap();

			intSize = alReturn.size();
			sbXMLData.append("<PROTOCOL id='");
			sbXMLData.append(strStudyId);
			sbXMLData.append("'>\n");
			if (intSize > 0)
			{
			   for (int i=0;i<intSize;i++)
			   {
			   		hmLoop = (HashMap)alReturn.get(i);
					strPatientId = (String)hmLoop.get("PID");
					strPatientIDEId = (String)hmLoop.get("PIDEID");
					//System.out.println("Patient ID:"+strPatientId);
					sbXMLData = sbXMLData.append(loadPatientData(strPatientId,strPatientIDEId));
					//System.out.println(sbXMLData.toString());
					logxml(sbXMLData,strPatientId);
					sbXMLData.setLength(0);	
			   }
				
			}
			sbXMLData.setLength(0);
			sbXMLData.append("</PROTOCOL>");
			logxml(sbXMLData,strPatientId);
			
		}catch(Exception e)
		{
			GmLogError.log("Exception in GmXMLDataBean:loadPatientList","Exception is:"+e);
			throw new AppError(e);
		}
		finally
		{
			try
			{
				if (csmt != null)
				{
					csmt.close();
				}//Enf of if  (csmt != null)
				if(conn!=null)
				{
					conn.close();		/* closing the Connections */
				}
			}
			catch(Exception e)
			{
				throw new AppError(e);
			}//End of catch
			finally
			{
				 csmt = null;
				 conn = null;
				 dbCon = null;
			}
		}
	  } // End of loadPatientList
	 
	/**
	  * loadPatientData - This Method is used to fetch data for all Groups 
	  * 						for a Growth Type report
	  * @param String strDistributorID (Contains the Selected Distributor ID) To be added 
	  * @return ArrayList
	  * @exception AppError
	**/
	 public StringBuffer loadPatientData(String strPatientId, String strPatientIDEId) throws AppError
	 {
	 	DBConnectionWrapper dbCon = null;
	 	Connection conn = null;
	 	String strPrepareString = null;
		ResultSet rs = null;
		CallableStatement csmt = null;
		
	 	StringBuffer sbXML = new StringBuffer();
		try
		{
			dbCon = new DBConnectionWrapper();
			conn = dbCon.getOracleStaticConnection();
			strPrepareString = dbCon.getStrPrepareString("gm_report_xml_ans_list_by_form",2);
			csmt = conn.prepareCall(strPrepareString);
			/*
			 *register out parameter and set input parameters
			 */
			csmt.registerOutParameter(2,OracleTypes.CURSOR);
			csmt.setString(1,strPatientId);
			csmt.execute();
			rs = (ResultSet)csmt.getObject(2);
			
			try {		
				sbXML = parseXMLData(strPatientId,strPatientIDEId,rs);
				//System.out.println(sbXML.toString());
				logxml(sbXML,strPatientId);
			}
			catch(Exception e)
			{
			   throw new AppError(e);
			}			
			return sbXML;
			
		}catch(Exception e)
		{
			GmLogError.log("Exception in GmXMLDataBean:loadPatientData","Exception is:"+e);
			throw new AppError(e);
		}
		finally
		{
			try
			{
				if (csmt != null)
				{
					csmt.close();
				}//Enf of if  (csmt != null)
				if(conn!=null)
				{
					conn.close();		/* closing the Connections */
				}
			}
			catch(Exception e)
			{
				throw new AppError(e);
			}//End of catch
			finally
			{
				 csmt = null;
				 conn = null;
				 dbCon = null;
			}
		}
	  } // End of loadPatientData
	 
 
	/**
	  * parseXMLData - Parses the Data that results from the query for a Growth Type Report.
	  * The ORder of the Select statment should be as follows:
	  * ID - COntains the ID of the entity
	  * TOTAL - COntains the sum
	  * DT - Date format
	  * NAME - NAme of Entity
	  * @param String strQuery 
	  * @return ArrayList
	  * @exception AppError
	**/
	 public StringBuffer parseXMLData(String strPatientId, String strPatientIDEId, ResultSet rs) throws AppError
	 {
		String strId = "";
		String strIdXml = "";
		String strDate = "";
		String strPeriod = "";
		String strDateXml = "";
		String strPatientIdXml = "";
		String strTag = "";
		String strAns = "";
		String strOtherAns = "";
		int intCnt = 0;
		boolean blflag = false;
		boolean blflag1 = false;
	  	StringBuffer sbXML = new StringBuffer();
	  	StringBuffer sbXMLHeader = new StringBuffer();
	  	sbXMLHeader.append("STUDY,FORM,PATIENT,VSDT,PERIOD,");
	  	
		try {
				//stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
				while (rs.next())
				{
					intCnt++;
					if (intCnt > 1) // to skip the first time
					{
						if (!strId.equals(rs.getString(1)) || !strDate.equals(rs.getString(2)))
						{
							if (!strId.equals(rs.getString(1)))
							{
								//strDateXml = "      </VSDT>\n";
								//strIdXml = "   </PATIENT>\n";
								sbXML.append("\n");
								//sbXML.append(strIdXml);
								//System.out.println(strDateXml);
								//System.out.println(strIdXml);
								blflag = true;
								blflag1 = true;
							}
							
							if (!strDate.equals(rs.getString(2)) && !strDate.equals("") && !blflag )
							{
								//strDateXml = "      </VSDT>\n";
								//sbXML.append(strDateXml);
								sbXML.append("\n");
								//System.out.println(strDateXml);
								blflag1 = true;
							}
							
						
						}
					}else
					{
						blflag = true;
						blflag1 = true;
					}

					strId = rs.getString(1);
					strDate = rs.getString(2);
					strPeriod = rs.getString(7);
					if (intCnt == 1 || blflag || blflag1)
					{
						if (intCnt == 1)
						{						
							//String strTest = rs.getMetaData().getColumnName(1);
							//strPatientIdXml = "<FORM id='".concat(strPatientIDEId).concat("'>\n");
							//sbXML.append(strPatientId);
							//sbXML.append(",");
							//System.out.println(strTest);
						}
						if (blflag || blflag1)
						{
							//strIdXml = "   <PATIENT id='".concat(strId).concat("'>\n");
							sbXML.append("GPR002");
							sbXML.append(",");
							sbXML.append(strPatientId);
							sbXML.append(",");
							sbXML.append(strId);
							sbXML.append(",");
							//System.out.println(strIdXml);
						}
						if (blflag1)
						{
							//strDateXml = "      <VSDT id='".concat(strDate).concat("' period='").concat(strPeriod).concat("'>\n");
							sbXML.append(strDate);
							sbXML.append(",");
							sbXML.append(strPeriod);
							sbXML.append(",");
							//System.out.println(strDateXml);
						}
					
						blflag = false;
						blflag1 = false;
					}
					strTag = rs.getString(3);
					strAns = rs.getString(4);
					strOtherAns = rs.getString(5);
					
					if (strOtherAns != null)
					{
						strAns = strOtherAns;
						strTag = rs.getString(6);
					}
					
					//System.out.println("strAns:"+strAns);
					
					//strAns = getXmlTag(strTag,strAns);
					//System.out.println("ANSER"+strId);
				
					strAns = parseNull(strAns);
					//System.out.println("strAns1111:"+strAns);
					
					strAns = strAns.replaceAll("\\,",".");
					sbXML.append(strAns);
					sbXML.append(",");
					//System.out.println(strAns);
					//System.out.println("strTag:"+strTag);
					sbXMLHeader.append(strTag);
					sbXMLHeader.append(",");	
				}
				
				
				//strDateXml = "      </VSDT>\n";
				//strIdXml = "   </PATIENT>\n";
				//strPatientIdXml = "</FORM>\n";
				//sbXML.append(strDateXml);
				//sbXML.append(strIdXml);
				//sbXML.append(strPatientIdXml);
				//System.out.println(strDateXml);
				//System.out.println(strIdXml);
				//System.out.println(strPatientIdXml);
				//System.out.println(sbXML.toString());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new AppError(e);
		}
		return sbXML;

	  } // End of parseXMLData

	 public String getXmlTag(String strTag, String strAns)
	 {

		StringBuffer sbXML = new StringBuffer();
		sbXML.append("\t\t");
		sbXML.append("<");
		sbXML.append(strTag);
		sbXML.append(">");
		sbXML.append(GmCommonClass.parseNull(strAns));
		sbXML.append("</");
		sbXML.append(strTag);
		sbXML.append(">\n");
	 	
	 	return sbXML.toString();
	 }
	
	 
	   /*
	     * Logs the given message with timestamp.
	     * @param  strMessage - String to be logged
	     * @param  objObject - Object to be logged (Could be a vector, hashtable, user defined bean, etc)
	     * @param  strAppName - App name determines the name of the log file to use
	     * @return None
	     * @exception None

	     */
	    public static synchronized void logxml(Object objObject, String strPatientId)
	    {
	        BufferedWriter br = null;
	        StringBuffer sb = new StringBuffer();

			String strFileName = "c:\\"+strPatientId+".csv";

	        try
	        {
	            br = new BufferedWriter(new FileWriter(strFileName,true));
	            br.write(objObject.toString());
		
	        }catch(Exception exp)
	        {
	            if(bolDEBUG)
	            {
	                System.out.println("Could not log the message..." + objObject+ "\n" + exp);
	            }
	        }
	        finally
	        {
	            try
	            {
	                if (br!=null) br.close();
	                sb = null;
	            }
	            catch (Exception exp)
	            {
	                //do nothing
	                if (bolDEBUG)
	                {
	                    System.out.println("Failure in closing the file writer...\n" + exp);
	                }
	            }
	        }
	    }//End of method log
	 
		public static String parseNull(String string) {
			// return (string == null || string.equals("") ||
			// "NULL".equalsIgnoreCase(string)) ? "" : string.trim();
			//System.out.println("In CommonClass"+string);
			return (string == null ||  string.equals("null") || string.equals("")) ? "" : string.trim();
		}
		
		
} // End of GmXMLDataBean