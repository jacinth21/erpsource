package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

/**
* GmClinicalStudyReflectBean : Contains the methods used for the  Adverse Event report datas in Reflect section
* author : MKosalram
*/

public class GmClinicalStudyReflectBean implements GmClinicalAdverseEventInterface{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
    /**
     * loadAdverseEvent - This Method is used to Adverse Event report datas
     * @param String strStudyListId, String strSiteId, String strPatientId 
     * @return ArrayList
     * @exception AppError
   **/
	
	public ArrayList loadAdverseEvent(String strStudyListId, String strSiteId, String strPatientId, String strOpt, String strTimePoint ) throws AppError
    {
        GmDBManager gmDBManager = new GmDBManager ();
        RowSetDynaClass rdResult = null;
        ResultSet rs = null;
        ArrayList alReturn = new ArrayList();

        gmDBManager.setPrepareString("gm_pkg_cr_adverseevent_rpt.gm_cr_fch_reflect_patient_adverseevent",6);
        gmDBManager.registerOutParameter(6,OracleTypes.CURSOR);
        gmDBManager.setString(1,strStudyListId);
        gmDBManager.setString(2,strSiteId);
        gmDBManager.setString(3,strPatientId);
        gmDBManager.setString(4,strOpt);
        gmDBManager.setString(5,strTimePoint);
        gmDBManager.execute();
        rs = (ResultSet)gmDBManager.getObject(6);
        alReturn = gmDBManager.returnArrayList(rs);
        log.debug(" Return list is Adverse Event " + alReturn);
        //rdResult  = gmDBManager.returnRowSetDyna(rs);
        gmDBManager.close();
        
        // return rdResult; 
       return alReturn;
      }
	
}
