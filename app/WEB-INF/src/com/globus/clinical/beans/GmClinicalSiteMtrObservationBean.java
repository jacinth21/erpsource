/*
 * Module: GmARBean.java Author: Hardik Parikh Project: Globus Medical App
 * Date-Written: 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What
 * you changed
 *  
 */

package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalSiteMtrObservationBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.
    /**
	 * fetchSiteMtrObservation 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchSiteMtrObservation(HashMap hmParam) throws AppError {           	  
          
          log.debug(" fetchSiteMtrObservation hmParam :"+hmParam);          
          
          String strObservationId=GmCommonClass.parseNull((String)hmParam.get("OBSERVATIONID"));         
                    
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_SITE_MONITOR.gm_cr_fch_site_mtr_observation", 11);
          gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);          
          gmDBManager.setString(1, strObservationId);
          gmDBManager.setString(2, null);
          gmDBManager.setString(3, null);
          gmDBManager.setString(4, null);
          gmDBManager.setString(5, null);
          gmDBManager.setString(6, null);
          gmDBManager.setString(7, null);
          gmDBManager.setString(8, null);     
          gmDBManager.setString(9, null);
          gmDBManager.setString(10, null);
          gmDBManager.execute();
          
          ArrayList alSiteMtrObservationList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(11));        
          gmDBManager.close();
          
		  log.debug("alSiteMtrObservationList=>"+alSiteMtrObservationList);

          log.debug("Exit");
         
         return alSiteMtrObservationList;          
    } // End of fetchSiteMtrObservation
    
    /**
	 * fetchNextSiteMtrObservation 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchNextSiteMtrObservation(HashMap hmParam) throws AppError {           	  
          
          log.debug(" fetchNextSiteMtrObservation hmParam :"+hmParam);          
          
          String strObservationId=GmCommonClass.parseNull((String)hmParam.get("OBSERVATIONID"));           
          String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
          String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("STUDYSITEID"));          
          String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
          String strVisitDate = GmCommonClass.parseNull((String)hmParam.get("VISITDATE"));    
          
          hmParam.put("OBSERVATIONID", strObservationId);
	      hmParam.put("STUDYID", strStudyId);
	      hmParam.put("STUDYSITEID", strStudySiteId);
	      hmParam.put("VISITDATE", strVisitDate);
	      hmParam.put("STATUS", strStatus);
                    
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_SITE_MONITOR.gm_cr_fch_next_mtr_observation", 6);
          gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);          
          gmDBManager.setString(1, strObservationId);
          gmDBManager.setString(2, strStudyId);
          gmDBManager.setString(3, strStudySiteId);
          gmDBManager.setString(4, strStatus);
          gmDBManager.setString(5, strVisitDate);                    
          gmDBManager.execute();          
          ArrayList alSiteMtrObservationList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));        
          gmDBManager.close();
          
		  log.debug("alSiteMtrObservationList=>"+alSiteMtrObservationList);
          log.debug("Exit");         
         return alSiteMtrObservationList;          
    } // End of fetchNextSiteMtrObservation   
	
    /**
	 * fetchSiteMtr 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchSiteMtr(HashMap hmParam) throws AppError {           	  
          
          log.debug(" fetchSiteMtr hmParam :"+hmParam);
          
          String strSiteMonitorId = GmCommonClass.parseNull((String)hmParam.get("SITEMONITORID"));
          String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
          String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("STUDYSITEID"));
          String strPurpose = GmCommonClass.parseNull((String)hmParam.get("PURPOSE"));
          String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
          String strFromDate = GmCommonClass.parseNull((String)hmParam.get("FROMDATE"));
          String strToDate = GmCommonClass.parseNull((String)hmParam.get("TODATE"));
          String strCraId = GmCommonClass.parseNull((String)hmParam.get("CRAID"));     
                    
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_SITE_MONITOR.gm_cr_fch_site_monitor", 8);
          gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);          
          gmDBManager.setString(1, strSiteMonitorId);
          gmDBManager.setString(2, strStudyId);
          gmDBManager.setString(3, strStudySiteId);
          gmDBManager.setString(4, strPurpose);
          gmDBManager.setString(5, strStatus);
          gmDBManager.setString(6, strFromDate);
          gmDBManager.setString(7, strToDate);                    
          gmDBManager.execute();
          
          ArrayList alSiteMtrList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));        
          gmDBManager.close();
          
		  log.debug("alSiteMtrList=>"+alSiteMtrList);

          log.debug("Exit");
         
         return alSiteMtrList;          
    } // End of fetchSiteMtr
    
    /**
	 * saveSiteMtrObservation 
     * @return ArrayList
     * @exception AppError
     */
    public String saveSiteMtrObservation(HashMap hmParam) throws AppError {           	  
          
          log.debug(" saveSiteMtrObservation hmParam :"+hmParam);
          
     	 String strSiteMonitorId = GmCommonClass.parseNull((String)hmParam.get("SITEMONITORID"));
    	 String strPatientId = GmCommonClass.parseNull((String)hmParam.get("PATIENTID"));
    	 String strPeriodId = GmCommonClass.parseNull((String)hmParam.get("PERIODID"));
    	 String strIssueDesc = GmCommonClass.parseNull((String)hmParam.get("ISSUEDESC"));
    	 String strActionRequired = GmCommonClass.parseNull((String)hmParam.get("ACTIONREQ"));
    	 String strResponsibleBy = GmCommonClass.parseNull((String)hmParam.get("RESBY"));
    	 String strTargetResDate = GmCommonClass.parseNull((String)hmParam.get("TARGETRESDATE"));
    	 String strResDesc = GmCommonClass.parseNull((String)hmParam.get("RESDESC"));
    	 String strActualResDate = GmCommonClass.parseNull((String)hmParam.get("ACTRESDATE"));
    	 String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
    	 String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    	 String strObservationId=GmCommonClass.parseNull((String)hmParam.get("OBSERVATIONID"));   
    	 
    	 if(strStatus!=null && (strStatus.equalsIgnoreCase("0") || strStatus.equalsIgnoreCase(""))){
    		 strStatus=null;
    	 }
    	 if(strResponsibleBy!=null && (strResponsibleBy.equalsIgnoreCase("0") || strResponsibleBy.equalsIgnoreCase(""))){
    		 strResponsibleBy=null;
    	 }
                    
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_SITE_MONITOR.gm_cr_sav_site_mtr_obs", 12);
          gmDBManager.registerOutParameter(12, java.sql.Types.INTEGER);          
          gmDBManager.setString(1, strSiteMonitorId);
          gmDBManager.setString(2, strPatientId);
          gmDBManager.setString(3, strPeriodId);
          gmDBManager.setString(4, strIssueDesc);
          gmDBManager.setString(5, strActionRequired);
          gmDBManager.setString(6, strResponsibleBy);
          gmDBManager.setString(7, strTargetResDate);
          gmDBManager.setString(8, strResDesc);
          gmDBManager.setString(9, strActualResDate);
          gmDBManager.setString(10, strStatus);
          gmDBManager.setString(11, strUserId);
          gmDBManager.setInt(12, Integer.parseInt(strObservationId));          
          gmDBManager.execute();                             
          strObservationId = gmDBManager.getString(12);
		  log.debug("Returning strObservationId=>"+strObservationId);
		  gmDBManager.commit();  
          log.debug("Exit");
         
         return strObservationId;          
    } // End of saveSiteMtrObservation
    
    
    }// end of class GmClinicalSiteMtrObservationBean
