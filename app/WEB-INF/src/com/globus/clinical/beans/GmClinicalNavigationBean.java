package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalNavigationBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
	/**
	 * loadTreeList - This method gets Invoice information for the selected
	 * PO and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadTreeList(HashMap hmParam) throws AppError {
		ArrayList alSnapshot = new ArrayList();
		
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		String strStudy = (String)hmParam.get("SESSSTUDYID");
		sbQuery.setLength(0);
		sbQuery.append(" select b.CTX_ID id, b.CTX_NAME name, c.CTX_DTL_DATA_NAME dataname, c.CTX_DTL_ID dtlid, c.CTX_DTL_LEVEL studyid ");
		sbQuery.append(" from tmp_gm_context a, tmp_gm_context b, TMP_GM_CONTEXT_DTL c ");
		sbQuery.append(" where a.CTX_ID = b.CTX_PARENT_ID ");
		sbQuery.append(" and b.CTX_ID = c.CTX_DTL_CTX_ID");
		if (!strStudy.equals(""))
		{
			sbQuery.append(" and c.CTX_DTL_LEVEL = '"+strStudy+"'");
		}
		sbQuery.append(" order by b.ctx_id, c.CTX_DTL_ID");
		log.debug("sbquery =="+sbQuery.toString());
		alSnapshot = dbCon.queryMultipleRecords(sbQuery.toString());
		
		return alSnapshot;
	} // End of loadPatientList

	/**
	 * loadContextMenu - This method gets the context menu for the toolbar
	 * 
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap loadContextMenu(HashMap hmParam) throws AppError {
		ArrayList alStudyList = new ArrayList();
		ArrayList alSiteList = new ArrayList();
		ArrayList alPatientList = new ArrayList();
		ArrayList alToolbarList = new ArrayList();
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		HashMap hmReturn = new HashMap();		
		HashMap hmStudyData = new HashMap();
		HashMap hmSiteData = new HashMap();
		HashMap hmPatientData = new HashMap();
		
		GmPatientBean gmPatientBean = new GmPatientBean();
		GmPatientTrackingBean gmPatientTrackingBean = new GmPatientTrackingBean();
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		String strTxnId = GmCommonClass.parseNull((String)hmParam.get("STRTXNID"));
		String strSelectedBtn = "";
		
		// Code for study list
		if (!strStudy.equals(""))
		{
			alStudyList = gmClinicalBean.loadStudyList(strCondition);
			Iterator itDetails = alStudyList.iterator();
			while (itDetails.hasNext()){
				hmStudyData = (HashMap) itDetails.next();
				if(strStudy.equals((String)hmStudyData.get("STUDYID")))
					{
						strSelectedBtn = "Y";
						hmStudyData.put("STUDYTYPEVAL", strSelectedBtn);
						break;
					}
			}
			
		}
		//log.debug("alStudyList===="+alStudyList);
		hmReturn.put("alStudyList", alStudyList);
		// code for site list
		if(!strSite.equals(""))
		{
			alSiteList = gmPatientBean.loadSiteMapDetails(strStudy, strCondition);
			Iterator itDetails = alSiteList.iterator();
			while (itDetails.hasNext()){
				hmSiteData = (HashMap) itDetails.next();
				if(strSite.equals((String)hmSiteData.get("SITEID")))
					{
						strSelectedBtn = "Y";
						hmSiteData.put("SITETYPEVAL", strSelectedBtn);
						break;
					}
			}
		}
		hmReturn.put("alSiteList", alSiteList);
		//log.debug("strPatient===="+strPatient);
		//code for patient list
		if(!strPatient.equals(""))
		{
			hmParam.put("STUDYID", strStudy);
            hmParam.put("SITEID", strSite);
            
			alPatientList = GmCommonClass.parseNullArrayList(gmPatientTrackingBean.loadPatientTrackList(hmParam, strCondition));				
			Iterator itDetails = alPatientList.iterator();
			while (itDetails.hasNext()){
				hmPatientData = (HashMap) itDetails.next();
				if(strPatient.equals((String)hmPatientData.get("IDENO")))
					{
						strSelectedBtn = "Y";
						hmPatientData.put("PATIENTTYPEVAL", strSelectedBtn);
						break;
					}
			}
		}
		hmReturn.put("alPatientList", alPatientList);
		//log.debug("alPatientList===="+alPatientList);
		
		alToolbarList = loadToolbarList(hmParam);
		if(!strTxnId.equals(""))
		{
			HashMap hmToolbarData = new HashMap();
			Iterator itDetails = alToolbarList.iterator();
			while (itDetails.hasNext()){
				hmToolbarData = (HashMap) itDetails.next();
				if(strTxnId.equals((String)hmToolbarData.get("ID")))
					{
						strSelectedBtn = "Y";
						hmToolbarData.put("TOOLBARVAL", strSelectedBtn);
						break;
					}
			}
		}
		hmReturn.put("alToolbarList", alToolbarList);
		//log.debug("alToolbarList===="+alToolbarList);
		
		return hmReturn;
	} // End of loadPatientList
	
	public String getXmlToolbarData(HashMap hmParamV) throws AppError {
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		ArrayList alStudyParam = (ArrayList)hmParamV.get("alStudyList");
		ArrayList alSiteParam = (ArrayList)hmParamV.get("alSiteList");
		ArrayList alPatientParam = (ArrayList)hmParamV.get("alPatientList");
		ArrayList alToolbarList = (ArrayList)hmParamV.get("alToolbarList");
		
		String strTemplate = (String)hmParamV.get("TEMPLATE");
		String strTemplatePath = (String)hmParamV.get("TEMPLATEPATH");

		templateUtil.setDataList("alStudyResult", alStudyParam);
		templateUtil.setDataList("alSiteResult", alSiteParam);
		templateUtil.setDataList("alPatientResult", alPatientParam);
		templateUtil.setDataList("alToolbarList", alToolbarList);
		
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}	
	
	/**
	 * loadToolbarList - This method gets list to actions for the perticular level
	 * PO and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadToolbarList_old(HashMap hmParam) throws AppError {
		ArrayList alToolbarList = new ArrayList();


		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
		String strSiteId = GmCommonClass.parseNull((String)hmParam.get("SITEID"));
		String strPatientId = GmCommonClass.parseNull((String)hmParam.get("PATIENTIDENO"));
		String strLevel = "";
		
		strLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMCRA":"STUDYLVL":"SITELVL":"PATIENTLVL";
		
		sbQuery.setLength(0);
		 
		
		
		sbQuery.append(" select toolbarlvl_id id, toolbarlvl_name name, toolbarlvl_action action ");
		sbQuery.append(" from tmp_gm_toolbar_lvl ");
		sbQuery.append(" where  toolbarlvl_lvl = '");		
		sbQuery.append(strLevel);
		sbQuery.append("'");
		log.debug("sbquery =="+sbQuery.toString());
		alToolbarList = dbCon.queryMultipleRecords(sbQuery.toString());

		return alToolbarList;
	} // End of loadToolbarList
	
	/**
	 * loadToolbarList - This method gets list to actions for the perticular level
	 * PO and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadToolbarList(HashMap hmParam) throws AppError {
		ArrayList alToolbarList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatientId = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strAccessLvl = GmCommonClass.parseNull((String)hmParam.get("SESSACCLVL"));
		String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		String strLevel = "";
		int iAccesslvl = 0;
		iAccesslvl = Integer.parseInt(strAccessLvl);
		log.debug("iAccesslvl============="+iAccesslvl);
		if(iAccesslvl >= 3)
		{
			//strLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMCRA":"STCRA":"SLCRA":"PTLCRA";
			strLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMCRARPT":"STCRARPT":"SLCRARPT":"PTLCRARPT";
		}
		else if(iAccesslvl == 1)
		{
			strLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMSC":"STSC":"SLSC":"PTLSC";
		}
		
		gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_clinical_toolbar", 3);
		gmDBManager.setString(1, GmCommonClass.parseNull(strLevel));
		gmDBManager.setString(2, GmCommonClass.parseNull(strUserId));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alToolbarList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		
		return alToolbarList;
	}// End of loadToolbarList
	
	/**
	 * loadTreeList - This method gets list to actions for the perticular level
	 * PO and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public HashMap loadTranRptTreeList(HashMap hmParam) throws AppError {
		ArrayList alReportList = new ArrayList();
		ArrayList alTransList = new ArrayList();
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatientId = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strAccessLvl = GmCommonClass.parseNull((String)hmParam.get("SESSACCLVL"));
		String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		String strRptLevel = "";
		String strTransLevel = "";
		int iAccesslvl = 0;
		iAccesslvl = Integer.parseInt(strAccessLvl);
		log.debug("iAccesslvl============="+iAccesslvl);
		if(iAccesslvl >= 3)
		{
			strRptLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMCRARPT":"STCRARPT":"SLCRARPT":"PTLCRARPT";
			strTransLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMCRATRANS":"STCRATRANS":"SLCRATRANS":"PTLCRATRANS";
		}
		else if(iAccesslvl == 1)
		{
			strRptLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMSCRPT":"STSCRPT":"SLSCRPT":"PTLSCRPT";
			strTransLevel = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"HMSCTRANS":"STSCTRANS":"SLSCTRANS":"PTLSCTRANS";
		}
		
		gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_clinical_toolbar", 3);
		gmDBManager.setString(1, GmCommonClass.parseNull(strRptLevel));
		gmDBManager.setString(2, GmCommonClass.parseNull(strUserId));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReportList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		hmReturn.put("ALREPORTTREE", alReportList);
		
		gmDBManager.setString(1, GmCommonClass.parseNull(strTransLevel));
		gmDBManager.setString(2, GmCommonClass.parseNull(strUserId));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alTransList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));		
		hmReturn.put("ALTRANSTREE", alTransList);
		
		hmReturn.put("SESSPGTOLOAD", GmCommonClass.parseNull((String)hmParam.get("SESSPGTOLOAD")));
		hmReturn.put("SESSOPT", GmCommonClass.parseNull((String)hmParam.get("SESSOPT")));
		hmReturn.put("SESSLASTCLICKED", GmCommonClass.parseNull((String)hmParam.get("SESSLASTCLICKED")));
		hmReturn.put("SESSOPTLASTCLICKED", GmCommonClass.parseNull((String)hmParam.get("SESSOPTLASTCLICKED")));
		//log.debug("loadTranRptTreeList =============="+hmReturn);
		gmDBManager.close();
		return hmReturn;
	}// End of loadToolbarList
	
	public HashMap fetchLastClickedAction (HashMap hmParam) throws AppError{
		HashMap hmReturn = new HashMap();
		ArrayList alReportList = new ArrayList();
		ArrayList alTransList = new ArrayList();
		String strSessPgToLoad = "";
		String strSessStrOpt = "";
		String strSessLastClicked = "";
		String strSessOptLastClicked = "";
		int cnt = 0;
		int iTreecnt = 0;
		alReportList = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("ALREPORTTREE"));
		alTransList = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("ALTRANSTREE"));
		strSessPgToLoad = GmCommonClass.parseNull((String)hmParam.get("SESSPGTOLOAD"));
		strSessStrOpt = GmCommonClass.parseNull((String)hmParam.get("SESSOPT"));
		strSessLastClicked = GmCommonClass.parseNull((String)hmParam.get("SESSLASTCLICKED"));
		strSessOptLastClicked = GmCommonClass.parseNull((String)hmParam.get("SESSOPTLASTCLICKED"));
		//log.debug("hmParam==============fetchLastClickedAction : "+hmParam);
		HashMap hmData = new HashMap();
		
			Iterator itDetails = alReportList.iterator();
			while (itDetails.hasNext()){
				hmData = (HashMap) itDetails.next();
				if(strSessLastClicked.equals((String)hmData.get("ACTION")))
					{
						hmReturn.put("SESSLASTCLICKED", strSessLastClicked);
						hmReturn.put("SESSOPTLASTCLICKED", (String)hmData.get("STROPT"));
						cnt++;
						break;
					}			
			}
			if(cnt == 0)
			{
				Iterator itTransDetails = alTransList.iterator();
				HashMap hmTreeData = new HashMap();
				while (itTransDetails.hasNext()){
					hmTreeData = (HashMap) itTransDetails.next();
					if(strSessLastClicked.equals((String)hmTreeData.get("ACTION")))
						{
							hmReturn.put("SESSLASTCLICKED", strSessLastClicked);
							hmReturn.put("SESSOPTLASTCLICKED", (String)hmTreeData.get("STROPT"));
							iTreecnt++;
							break;
						}			
				}
				if(iTreecnt == 0)
				{
					hmReturn.put("SESSLASTCLICKED", "");
					hmReturn.put("SESSOPTLASTCLICKED", "");
					hmReturn.put("SESSPGTOLOAD", strSessPgToLoad);
					hmReturn.put("SESSOPT", strSessStrOpt);
				}
			}			
		//log.debug("hmReturn==============fetchLastClickedAction : "+hmReturn);
		return hmReturn;
		
		
	}
}