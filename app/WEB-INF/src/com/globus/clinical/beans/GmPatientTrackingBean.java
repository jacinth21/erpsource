package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.beans.GmFooterCalcuationBean;

public class GmPatientTrackingBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());    
 	 /**
	  * 
	  * @author rshah
	  * @date Feb 1, 2010	
	  * @param strStudyId, strSiteId, strPatientIde, strCondition,strPatientId
	  * Used to get the list of perticular patient.
	  */
	 public HashMap loadPatientTrackForm(HashMap hmValues) throws AppError
	 {
		 HashMap hmParam = new HashMap();
		 GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		 StringBuffer sbHeaderQuery = new StringBuffer();
		 StringBuffer sbDtlQuery = new StringBuffer();
		 GmDBManager gmDBManager = new GmDBManager();
		 String strFormId = "";
		 String strStudyPeriodId = "";
		 String strAdvFormId ="";
		 String strAdvansGrpId = "";
		 StringBuffer sbRuleQuery = new StringBuffer();
		 ArrayList  alRules = new ArrayList();
		 HashMap hmRules = new HashMap();
		 HashMap hmRule = new HashMap();
		 HashMap hmReturn = new HashMap();
		 String strStudyId = (String)hmValues.get("STUDYID");
		 String strSiteId=(String)hmValues.get("SITEID");
		 String strPatientIde=(String)hmValues.get("PATIENTIDE");
		 String strPatientId=(String)hmValues.get("PATIENTID");
		 String strCondition=(String)hmValues.get("CONDITION");		 
		 
		 HashMap hmFinalValue = new HashMap();  
		 
		 hmParam = loadFormStudyDetail(strStudyId,"FCH_PATIENT_TRACK");
		 strFormId = GmCommonClass.parseZero((String)hmParam.get("ALTNM"));
		 strStudyPeriodId = GmCommonClass.parseNull((String)hmParam.get("NAME"));
		 hmParam = loadFormStudyDetail(strStudyId,"FCH_PATIENT_FTRACK");
		 strAdvFormId = GmCommonClass.parseNull((String)hmParam.get("ALTNM"));
		 strAdvansGrpId = GmCommonClass.parseNull((String)hmParam.get("NAME"));
		 // query for rule values to remove hard code values in decorator 
		 
		 sbRuleQuery.append(" SELECT * FROM (SELECT DISTINCT c612_study_form_ds formds, c612_seq, c601_form_id formid, get_rule_value (c601_form_id, 'PERID') perid");
		 sbRuleQuery.append(" FROM t612_study_form_list");
		 sbRuleQuery.append(" WHERE c611_study_id = '"+strStudyId+"'  AND c612_delete_fl='N'");
		 sbRuleQuery.append(" ORDER BY c612_seq )");
		 sbRuleQuery.append(" WHERE perid IS NOT NULL ");
		 alRules =  GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbRuleQuery.toString()));
		 gmDBManager.close();
		 for(int i=0 ; i<alRules.size();i++){
			 ArrayList  alRule = new ArrayList();
			hmRule = GmCommonClass.parseNullHashMap((HashMap)alRules.get(i));
			String strFormDs = GmCommonClass.parseNull((String)hmRule.get("FORMDS"));
			String strFrmid = GmCommonClass.parseNull((String)hmRule.get("FORMID"));
			String strPerId = GmCommonClass.parseNull((String)hmRule.get("PERID"));
			alRule.add(strFrmid);
			alRule.add(strPerId);
			hmRules.put(strFormDs,alRule);
		 }
		// log.debug("hmRules==>"+hmRules);
		 sbHeaderQuery.append(" SELECT DISTINCT c612_study_form_ds, c612_seq ");
		 sbHeaderQuery.append(" FROM t612_study_form_list");
		 sbHeaderQuery.append(" WHERE c611_study_id = '"+strStudyId+"'");
		 sbHeaderQuery.append(" ORDER BY c612_seq");
		 
		 //sbDtlQuery.append(" SELECT   v621.c621_patient_ide_no ID, v621.c613_study_period_ds, v621.c612_study_form_ds ");
		 sbDtlQuery.append(" SELECT   v621.c613_study_period_ds ID, v621.c613_study_period_ds, v621.c612_study_form_ds ");
		 sbDtlQuery.append(" , CASE ");
		 sbDtlQuery.append(" WHEN t622.c613_study_period_id IS NULL OR TRIM (t622.c622_missing_follow_up_fl) = 'Y' ");
		 sbDtlQuery.append(" THEN '  ' ");
		 sbDtlQuery.append(" ELSE '*' ");
		 sbDtlQuery.append(" END cnt ");
		 sbDtlQuery.append(" , v621.c612_seq, v621.c613_seq_no, v621.c621_exp_date_final duedate, v621.c901_study_period STUDYPERIODID");
		 sbDtlQuery.append(" FROM t622_patient_list t622, v621_patient_study_list v621 ");
		 sbDtlQuery.append(" WHERE v621.c621_patient_id = t622.c621_patient_id(+) ");
		 sbDtlQuery.append(" AND v621.c601_form_id = t622.c601_form_id(+) ");
		 sbDtlQuery.append(" AND v621.c613_study_period_id = t622.c613_study_period_id(+) ");
		 sbDtlQuery.append(" AND (v621.c621_exp_date_start <= SYSDATE OR v621.c621_exp_date_final <= SYSDATE) ");
		 sbDtlQuery.append(" AND v621.c621_surgery_comp_fl = 'Y' ");
		 sbDtlQuery.append(" AND v621.c611_study_id = '"+strStudyId+"'");
		 sbDtlQuery.append(" AND v621.c621_patient_ide_no = '"+strPatientIde+"'");
		 sbDtlQuery.append(" AND v621.c614_study_site_id IN DECODE ('"+strSiteId+"','0',v621.c614_study_site_id,'',v621.c614_study_site_id,'"+strSiteId+"')");
		 sbDtlQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" and "+strCondition);
		 }
		 sbDtlQuery.append(" UNION ALL");
		 //sbDtlQuery.append(" SELECT   v621.c621_patient_ide_no ID, v621.c613_study_period_ds, v621.c612_study_form_ds ");
		 sbDtlQuery.append(" SELECT   v621.c613_study_period_ds ID, v621.c613_study_period_ds, v621.c612_study_form_ds ");
		 sbDtlQuery.append(" , CASE ");
		 sbDtlQuery.append(" WHEN t622.c613_study_period_id IS NULL OR TRIM (t622.c622_missing_follow_up_fl) = 'Y' ");
		 sbDtlQuery.append(" THEN '  ' ");
		 sbDtlQuery.append(" ELSE '*' ");
		 sbDtlQuery.append(" END cnt ");
		 sbDtlQuery.append(" , v621.c612_seq, v621.c613_seq_no, to_date(null) duedate , v621.c901_study_period STUDYPERIODID");
		 sbDtlQuery.append(" FROM t622_patient_list t622, v621_patient_study_list v621 ");
		 sbDtlQuery.append(" WHERE v621.c621_patient_id = t622.c621_patient_id(+) ");
		 sbDtlQuery.append(" AND v621.c601_form_id = t622.c601_form_id(+) ");
		 sbDtlQuery.append(" AND v621.c613_study_period_id = t622.c613_study_period_id(+) ");
		 sbDtlQuery.append(" AND (v621.c621_exp_date_start > SYSDATE OR v621.c621_exp_date_final > SYSDATE) ");
		 sbDtlQuery.append(" AND v621.c621_surgery_comp_fl = 'Y' ");
		 sbDtlQuery.append(" AND v621.c611_study_id = '"+strStudyId+"'");
		 sbDtlQuery.append(" AND v621.c621_patient_ide_no = '"+strPatientIde+"'");
		 sbDtlQuery.append(" AND v621.c614_study_site_id IN DECODE ('"+strSiteId+"','0',v621.c614_study_site_id,'',v621.c614_study_site_id,'"+strSiteId+"')");
		 sbDtlQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" and "+strCondition);
		 }
		 sbDtlQuery.append(" UNION ALL");
		 //sbDtlQuery.append(" SELECT   t1.ID ID, t1.c613_study_period_ds, t1.c612_study_form_ds, cnt, t1.c612_seq, v621.c613_seq_no ");
		 sbDtlQuery.append(" SELECT   t1.c613_study_period_ds ID, t1.c613_study_period_ds, t1.c612_study_form_ds, cnt, t1.c612_seq, v621.c613_seq_no ");
		 sbDtlQuery.append(" , TO_DATE (SYSDATE) duedate, v621.c901_study_period STUDYPERIODID");
		 sbDtlQuery.append(" FROM (SELECT	t621.c621_patient_ide_no ID ");
		 sbDtlQuery.append(" , get_time_point ('"+strStudyId+"','"+ strPatientIde+"', DECODE(t623.c601_form_id,get_rule_value(t623.c601_form_id,'PTRPT'),TO_CHAR(t622.c622_date,'MM/DD/YYYY'),t623.c904_answer_ds)) c613_study_period_ds ");
		 sbDtlQuery.append(" , t612.c612_study_form_ds, TO_CHAR (COUNT (1)) cnt, t612.c612_seq ");
		 sbDtlQuery.append(" FROM t621_patient_master t621, t623_patient_answer_list t623, t612_study_form_list t612, t622_patient_list t622 ");
		 sbDtlQuery.append(" WHERE t621.c621_patient_id = t623.c621_patient_id ");
		 sbDtlQuery.append(" AND t612.c601_form_id = t623.c601_form_id ");
		 sbDtlQuery.append(" AND NVL(C622_DELETE_FL,'N')='N' ");
		 sbDtlQuery.append(" AND t621.c621_surgery_comp_fl = 'Y' ");
		 sbDtlQuery.append(" AND t621.c621_patient_ide_no = '"+strPatientIde+"'");
		 sbDtlQuery.append(" AND t623.c603_answer_grp_id in ("+strAdvansGrpId+")");
		 sbDtlQuery.append(" AND t623.c601_form_id in ( "+strAdvFormId+")");
		 sbDtlQuery.append(" AND t612.c611_study_id = '"+strStudyId+"'");
		 sbDtlQuery.append(" AND t622.C622_PATIENT_LIST_ID = t623.C622_PATIENT_LIST_ID");
		 sbDtlQuery.append(" GROUP BY t621.c621_patient_ide_no ");
		 sbDtlQuery.append(" , get_time_point ('"+strStudyId+"','"+ strPatientIde+"', DECODE(t623.c601_form_id,get_rule_value(t623.c601_form_id,'PTRPT'),TO_CHAR(t622.c622_date,'MM/DD/YYYY') ,t623.c904_answer_ds)) ");
		 sbDtlQuery.append(" , t612.c612_study_form_ds ");
		 sbDtlQuery.append(" , t612.c612_seq order by t612.c612_seq) t1 ");
		 sbDtlQuery.append(" , v621_patient_study_list v621 ");
		 sbDtlQuery.append(" WHERE v621.c613_study_period_ds = t1.c613_study_period_ds ");
		 sbDtlQuery.append(" AND v621.c621_patient_ide_no = t1.ID ");
		 sbDtlQuery.append(" AND v621.c601_form_id IN ("+strFormId+")");
		 sbDtlQuery.append(" AND v621.c614_study_site_id IN DECODE ('"+strSiteId+"','0',v621.c614_study_site_id,'',v621.c614_study_site_id,'"+strSiteId+"')");
		 if (!strCondition.equals(""))
		 {
			 sbDtlQuery.append(" and "+strCondition);
		 }
		 sbDtlQuery.append(" ORDER BY c613_seq_no, c612_seq ");
		 
		 gmCrossReport.setFixedColumn(true);
		 gmCrossReport.setFixedParams(7, "DUEDATE");
		 gmCrossReport.setFixedParams(8, "STUDYPERIODID");
		 //log.debug(" Header More query==1="+sbHeaderQuery.toString());
		 //log.debug(" Detail More query==2="+sbDtlQuery.toString());
		 hmFinalValue = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDtlQuery.toString());
		 //log.debug(" # # # hmFinalValue "+hmFinalValue);	
		 
		 sbHeaderQuery.setLength(0);
		 sbDtlQuery.setLength(0);
		 
		 sbHeaderQuery.append(" SELECT DECODE(c901_code_nm,'N/A','MRI/CT',c901_code_nm) ");
		 sbHeaderQuery.append(" FROM t901_code_lookup a");
		 sbHeaderQuery.append(" WHERE c901_code_grp IN  ('XRAY','MRI','CTSCAN','PCOM')");
		 sbHeaderQuery.append(" ORDER BY a.c901_code_id ");
		 
		 sbDtlQuery.append(" SELECT get_code_name(t624.C901_STUDY_PERIOD) ID, ");
		 sbDtlQuery.append(" t624.C901_STUDY_PERIOD name, ");
		 sbDtlQuery.append(" DECODE(get_code_name (t624.c901_record_type),'N/A',");
		 sbDtlQuery.append(" 'MRI/CT',get_code_name (t624.c901_record_type)) clinical_form, "); 
		 sbDtlQuery.append(" '*' record_exists  FROM t624_patient_record t624,t621_patient_master t621  ");
		 sbDtlQuery.append(" WHERE t621.C621_PATIENT_ID = t624.C621_PATIENT_ID  AND ");
		 sbDtlQuery.append(" t621.c621_patient_ide_no = '"+strPatientIde+"'");
		 sbDtlQuery.append(" AND t624.c611_study_id = '"+strStudyId+"' ");
		 sbDtlQuery.append(" AND t624.c624_void_fl IS NULL and t624.C901_STUDY_PERIOD <> 6309 "); 
		 sbDtlQuery.append(" UNION ALL");
		 sbDtlQuery.append(" SELECT get_code_name (t630.c901_study_period) ID, ");
		 sbDtlQuery.append(" t630.c901_study_period NAME, ");
		 sbDtlQuery.append(" 'Comments' comments, ");
		 sbDtlQuery.append(" TO_CHAR(Count(1)) record_exists ");
		 sbDtlQuery.append(" FROM   t621_patient_master t621, t630_patient_log t630 ");
		 sbDtlQuery.append(" WHERE t621.c621_patient_id = t630.c621_patient_id ");
		 sbDtlQuery.append(" AND t621.c621_patient_ide_no = '"+strPatientIde+"'");
		 sbDtlQuery.append(" AND t630.c621_void_fl IS NULL ");
		 sbDtlQuery.append(" AND t630.c901_study_period <> 6309 ");
		 sbDtlQuery.append(" GROUP BY t630.c901_study_period ");
		 sbDtlQuery.append("order by ID ");		 
		 
		// log.debug(" Header More query==3="+sbHeaderQuery.toString());
		 //log.debug(" Detail More query==4="+sbDtlQuery.toString());
		 gmCrossReport = new GmCrossTabReport();
		 HashMap hmFormValue = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDtlQuery.toString());
		 //log.debug(" # # # hmFormValue "+hmFormValue);	
		 ArrayList alRadioGraph = (ArrayList)hmFormValue.get("Details");
		 HashMap hmLinkInfo = getLinkInfo("RADIOGRAPH");
		 
		 
		 //This is a temporary fix to have the header for films when the details is blank.
		 //THis is simulated as if the data comes from DB.
		 if(alRadioGraph!=null && alRadioGraph.size()==0){
			 ArrayList alMasterList =(ArrayList) hmFinalValue.get("Details");
			 HashMap hmTempMap = new HashMap();
			 ArrayList tempList = new ArrayList();
			 
			 String strTempHeaderKey="";
			 ArrayList alFilmHeader = (ArrayList)hmFormValue.get("Header");
			 for(Iterator itr=alMasterList.iterator();itr.hasNext();){
				 hmTempMap = (HashMap)itr.next();
				 
				 for(Iterator fhitr=alFilmHeader.iterator();fhitr.hasNext();){
					 strTempHeaderKey = (String)fhitr.next();
					 
					 if(!strTempHeaderKey.equalsIgnoreCase("NAME") && !strTempHeaderKey.equalsIgnoreCase("TOTAL")){
						 hmTempMap.put(strTempHeaderKey, "");
					 }
				 }
				 tempList.add(hmTempMap);
			 }
			 
			 //log.debug(" # # # # alMasterList "+tempList +"\n"+" alMasterList ::::::::::"+alMasterList);
			 alRadioGraph = tempList;
		 }
		 hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alRadioGraph, hmLinkInfo);
		 hmReturn.put("GIRDATA",hmFinalValue);
		 hmReturn.put("RULES",hmRules);
		 //log.debug(" # # # hmFinalValue "+hmFinalValue);
		 return hmReturn;
	 }
	  /**
	  * @author rshah
	  * @param studyId, siteId, access condition
	  * @return List
	  * Used the get the patient track list based on the access permission
	  */
	 public ArrayList loadPatientTrackList(HashMap hmValues, String strCondition) throws AppError
	 {
		 ArrayList alPatientList = new ArrayList();
	     HashMap hmParam = new HashMap();
	     DBConnectionWrapper dbCon = null;
		 dbCon = new DBConnectionWrapper();
		 StringBuffer sbQuery = new StringBuffer();
		 String strFormId = "";
		 String strStudyPeriodId = "";
		 String strStudySiteId = "";
		 String strStudyId = (String)hmValues.get("STUDYID");
		 String strSiteId = (String)hmValues.get("SITEID");
		 hmParam = loadFormStudyDetail(strStudyId,"FCH_PATIENT_TRACK");
		 strFormId = GmCommonClass.parseZero((String)hmParam.get("ALTNM"));
		 strStudyPeriodId = GmCommonClass.parseNull((String)hmParam.get("NAME"));
		 strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
		 sbQuery.append(" SELECT distinct v621.c621_patient_id ID, v621.c621_patient_ide_no ideno,");
		 sbQuery.append(" get_surgical_inv_count(v621.c621_patient_ide_no) nosrginv ,");
		 sbQuery.append(" get_party_name (v621.c101_surgeon_id) sname,");
		 sbQuery.append(" get_code_name (v621.c901_surgery_type) treatment,");
		 sbQuery.append(" TO_CHAR (decode(v_621.c622_surgery_date,'',decode(v621.c621_surgery_comp_fl,'Y', v621.c621_surgery_date,v_621.c622_surgery_date),v_621.c622_surgery_date), 'mm/dd/yyyy') sdate,");
		 sbQuery.append(" v621.c611_study_id studyid, t614.c614_site_id siteid, t614.C614_STUDY_SITE_ID studysiteid, ");
		 sbQuery.append(" current_period.period, current_period.w_open, current_period.addmon,");
		 sbQuery.append(" current_period.w_close, to_char(add_months(sysdate,1),'mm/dd/yyyy') addMonthDate, ");
		 sbQuery.append(" current_period.missing_cur_fl,");
		 sbQuery.append(" (SELECT DECODE (SIGN (0 - COUNT (c622_missing_follow_up_fl)),");
		 sbQuery.append(" -1, 'Y','')");
		 sbQuery.append(" FROM t622_patient_list");
		 sbQuery.append(" WHERE c622_missing_follow_up_fl IS NOT NULL");
		 sbQuery.append(" AND c621_patient_id = v621.c621_patient_id");
		 sbQuery.append(" AND NVL(C622_DELETE_FL,'N')='N' AND c601_form_id IN ("+ strFormId +")) missing_fl, ");
		 sbQuery.append(" DECODE((SELECT DECODE (SIGN (0 - COUNT (c622_missing_follow_up_fl)), -1, 'Y','') ");
		 sbQuery.append(" FROM t622_patient_list");
		 sbQuery.append(" WHERE c622_missing_follow_up_fl IS NOT NULL");
		 sbQuery.append(" AND c621_patient_id = v621.c621_patient_id");
		 sbQuery.append(" AND NVL(C622_DELETE_FL,'N')='N' AND c601_form_id IN ("+ strFormId +")), 'Y', NULL, actual_date) actual_date ");
		 sbQuery.append(" FROM t621_patient_master v621,");
		 sbQuery.append(" t614_study_site t614,");
		 sbQuery.append(" v621_patient_study_list v_621,");
		 sbQuery.append(" (SELECT   v621.c621_patient_id, v621.c613_study_period_ds period,");
		 sbQuery.append(" TO_CHAR (v621.c621_exp_date_start, 'mm/dd/yyyy') w_open,");
		 sbQuery.append(" TO_CHAR (ADD_MONTHS (SYSDATE, 1), 'mm/dd/yyyy') addmon,");		 
		 sbQuery.append(" TO_CHAR (v621.c621_exp_date_final, 'mm/dd/yyyy') w_close,");
		 sbQuery.append(" (SELECT NVL (c622_missing_follow_up_fl, '')");
		 sbQuery.append(" FROM t622_patient_list");
		 sbQuery.append(" WHERE c622_missing_follow_up_fl IS NOT NULL");
		 sbQuery.append(" AND c621_patient_id = v621.c621_patient_id");		 
		 sbQuery.append(" AND NVL(C622_DELETE_FL,'N')='N' AND c613_study_period_id = v621.c613_study_period_id) missing_cur_fl,");
		 sbQuery.append(" (SELECT to_char(c622_date,'mm/dd/yyyy')  FROM t622_patient_list WHERE c621_patient_id = v621.c621_patient_id AND ");
		 sbQuery.append(" c613_study_period_id = v621.c613_study_period_id AND c622_missing_follow_up_fl IS NULL AND NVL(C622_DELETE_FL,'N')='N' )  actual_date");
		 sbQuery.append(" FROM v621_patient_study_list v621");
		 sbQuery.append(" WHERE (v621.c621_patient_ide_no, v621.c621_exp_date_start,v621.c621_exp_date_final) IN(");
		 sbQuery.append(" SELECT   v621.c621_patient_ide_no,");
		 sbQuery.append(" MAX (v621.c621_exp_date_start),MAX (v621.c621_exp_date_final)");		 
		 sbQuery.append(" FROM v621_patient_study_list v621");
		 sbQuery.append(" WHERE (   v621.c621_exp_date_start <= SYSDATE OR v621.c621_exp_date_final <= SYSDATE )");
		 sbQuery.append(" GROUP BY v621.c621_patient_ide_no)");
		 sbQuery.append(" AND v621.c601_form_id IN ("+strFormId +")");
		 sbQuery.append(" AND v621.c611_study_id = '"+strStudyId+"'");
		 sbQuery.append(" AND v621.c614_study_site_id IN DECODE ('"+strStudySiteId+"',");
		 sbQuery.append(" '0', v621.c614_study_site_id,'',v621.c614_study_site_id,'"+strStudySiteId+"')");
		 sbQuery.append(" ORDER BY v621.c621_patient_ide_no) current_period ");
		 sbQuery.append(" WHERE v621.c704_account_id = t614.c704_account_id ");
		 sbQuery.append(" AND v621.c621_patient_id = current_period.c621_patient_id(+)");
		 sbQuery.append(" AND v621.c611_study_id = t614.c611_study_id ");
		 sbQuery.append(" AND v621.c611_study_id = '"+strStudyId+"'");
		 sbQuery.append(" AND v621.C621_DELETE_FL not in  ('Y','D') ");
		 sbQuery.append(" AND t614.c614_study_site_id IN DECODE ('"+strStudySiteId+"',");
		 sbQuery.append(" '0', t614.c614_study_site_id,'',t614.c614_study_site_id,'"+strStudySiteId+"')");
		 sbQuery.append(" AND v_621.c611_study_id = v621.c611_study_id");
		 sbQuery.append(" AND v_621.c621_patient_id = v621.c621_patient_id");
		 sbQuery.append(" AND v_621.c704_account_id= v621.c704_account_id");
		 sbQuery.append(" AND v_621.c614_study_site_id = t614.c614_study_site_id");
		 //sbQuery.append(" AND v621.c614_site_id IN DECODE ('"+strSiteId+"',");
		 //sbQuery.append(" '0', v621.c614_site_id,'',v621.c614_site_id,'"+strSiteId+"')");
		 if (!strCondition.equals(""))
		 {
			 sbQuery.append(" and "+strCondition);
		 }
		 sbQuery.append(" UNION ALL SELECT t621.c621_patient_id, t621.c621_patient_ide_no,get_surgical_inv_count(t621.c621_patient_ide_no) nosrginv, " +
		 		"null SNAME, null TREATMENT, NULL SDATE, '0' STUDYID,'0' SITEID, null STUDYSITEID, NULL PERIOD, " +
		 		"NULL W_OPEN, NULL ADDMON, NULL W_CLOSE, NULL ADDMONTHDATE, NULL MISSING_CUR_FL, NULL MISSING_FL, " +
		 		"NULL actual_date FROM t621_patient_master t621 , t614_study_site t614 WHERE c901_surgery_type IS NULL " +
		 		"AND t614.c611_study_id = '"+strStudyId+"' AND c614_study_site_id ='"+strStudySiteId+"' and " +
		 		//Mapping is done for the table t621_patient_master & t614_study_site based on the column study id 
		 		"t614.c704_account_id = t621.c704_account_id AND t614.c611_study_id = t621.c611_study_id AND c621_delete_fl='N' ORDER BY studysiteid, siteid, IDENO ");
		 log.debug("query===123=="+sbQuery.toString());
		 alPatientList = dbCon.queryMultipleRecords(sbQuery.toString());
	     return alPatientList;
	 }
	 
	 /**
	  * 
	  * 
	  * @author rshah
	  * @date Feb 1, 2010	
	  * @param strStudyId, strSiteId, strPatientId, strCondition
	  * Used to get the list of perticular patient.
	  */
	 public ArrayList loadPatientTrackInfo(String strStudyId, String strSiteId, String strPatientId, String strCondition) throws AppError
	 {
		 ArrayList alPatientList = new ArrayList();
		 HashMap hmParam = new HashMap();
		 DBConnectionWrapper dbCon = null;
		 dbCon = new DBConnectionWrapper();
		 StringBuffer sbQuery = new StringBuffer();
		 String strFormId = "";
		 String strStudyPeriodId = "";
		 String strStudySiteId = "";
		 hmParam = loadFormStudyDetail(strStudyId,"FCH_PATIENT_TRACK");
		 strFormId = GmCommonClass.parseZero((String)hmParam.get("ALTNM"));
		 strStudyPeriodId = GmCommonClass.parseNull((String)hmParam.get("NAME"));
		 strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
		 sbQuery.append("SELECT  v621.c621_patient_id ID, v621.c621_patient_ide_no pideno, v621.c901_study_period PERIOD_ID,");
		 sbQuery.append(" v621.c611_study_id studyid, v621.c614_site_id siteid, v621.c614_study_site_id studysiteid,");
		 sbQuery.append(" v621.c613_study_period_ds PERIOD, to_char(v621.c621_exp_date_start,'mm/dd/yyyy') w_open, to_char(add_months(sysdate,1),'mm/dd/yyyy') addMonthDate, ");
		 sbQuery.append(" to_char(v621.c621_exp_date_final,'mm/dd/yyyy') w_close ");
		 sbQuery.append(" ,(select t622.c622_missing_follow_up_fl from t622_patient_list t622 where v621.c621_patient_id = t622.c621_patient_id ");
		 sbQuery.append(" AND v621.c613_study_period_id = t622.c613_study_period_id AND  NVL(t622.C622_DELETE_FL,'N')='N') missing_cur_fl ");
		 sbQuery.append(" ,(select TO_CHAR(t622.c622_date,'mm/dd/yyyy') from t622_patient_list t622 where v621.c621_patient_id = t622.c621_patient_id");
		 sbQuery.append(" AND v621.c613_study_period_id = t622.c613_study_period_id AND t622.c622_missing_follow_up_fl IS NULL AND NVL(t622.C622_DELETE_FL,'N')='N') actualDate");
		 sbQuery.append(" ,GET_FILLED_FORM_COUNT(v621.c611_study_id,v621.c621_patient_ide_no,v621.c614_study_site_id,v621.c901_study_period) FILLED_FORM ");
         sbQuery.append(" ,TO_CHAR((GET_TOTAL_FORM_COUNT(v621.c611_study_id,v621.c901_study_period ) - ");
         sbQuery.append("GET_FILLED_FORM_COUNT(v621.c611_study_id,v621.c621_patient_ide_no,v621.c614_study_site_id,v621.c901_study_period ");
         sbQuery.append("))) ");
         sbQuery.append(" MISSED_FORM, ");
         sbQuery.append(" V621.C613_SEQ_NO SEQNO ");
		 sbQuery.append(" FROM v621_patient_study_list v621");
		 sbQuery.append(" WHERE v621.c611_study_id = '"+strStudyId+"'");
		 sbQuery.append(" AND v621.c601_form_id IN ("+strFormId+")");
		 //sbQuery.append(" AND v621.c613_study_period_id NOT IN ("+strStudyPeriodId+")");
		 sbQuery.append(" AND v621.c621_patient_ide_no = '"+strPatientId+"'");
		 sbQuery.append(" AND v621.c614_study_site_id IN DECODE ('"+strStudySiteId+"','0', v621.c614_study_site_id,'',v621.c614_study_site_id,'"+strStudySiteId+"')");
		 //sbQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		 if (!strCondition.equals(""))
		 {
			 sbQuery.append(" and "+strCondition);
		 }
		 //sbQuery.append(" ORDER BY v621.c613_seq_no");
		 sbQuery.append(" UNION "); 
		 sbQuery.append(" select DISTINCT v621.C621_PATIENT_ID ID, v621.c621_patient_ide_no pideno, t613.C901_STUDY_PERIOD PERIODID,  v621.c611_study_id studyid, v621.c614_site_id siteid, v621.c614_study_site_id studysiteid, GET_CODE_NAME(t613.C901_STUDY_PERIOD) PERIOD, '-----' W_OPEN, '-----' ADDMONTHDATE, '------' W_CLOSE, '' MISSING_CUR_FL,'------' ACTUALDATE ");
		 sbQuery.append(" ,'' MISSED_FORM,'' FILLED_FORM, ");
		 sbQuery.append(" DECODE(GET_CODE_NAME(t613.C901_STUDY_PERIOD),'N/A',999,1) seqno ");
		 sbQuery.append(" from  T613_STUDY_PERIOD t613, v621_patient_study_list v621 ");
		 sbQuery.append(" where t613.C901_STUDY_PERIOD in (6301, 6309) ");
		 sbQuery.append(" AND v621.c621_patient_ide_no = '"+strPatientId+"' ");
		 sbQuery.append(" AND v621.c611_study_id = '"+strStudyId+"'");
		 sbQuery.append(" ORDER BY seqno, PERIOD_ID ");
		 
		 log.debug("query=123=="+sbQuery.toString());
		 alPatientList = dbCon.queryMultipleRecords(sbQuery.toString());
	     log.debug("patientList: "+alPatientList);	
		 return alPatientList;
	 }
	 
	 /**
	  * Method to get the form id and the study period id
	  * @author rshah
	  * @param studyId, group id
	  * @return hashMap
	  */
	 public HashMap loadFormStudyDetail(String strStudyId, String strGroup) throws AppError
	 {
		 HashMap hmReturn = new HashMap();
		 DBConnectionWrapper dbCon = null;
		 dbCon = new DBConnectionWrapper();

		 StringBuffer sbQuery = new StringBuffer();
		 sbQuery.setLength(0);
		 sbQuery.append(" SELECT t901.c902_code_nm_alt altnm, t901.c901_code_nm name");
		 sbQuery.append(" FROM t901_code_lookup t901 ");
		 sbQuery.append(" WHERE t901.c901_code_id = get_rule_value('"+strStudyId+"','"+strGroup+"')");
		 log.debug("query==="+sbQuery.toString());
		 hmReturn = dbCon.querySingleRecord(sbQuery.toString());
		 return hmReturn;
	 }
	 
	 /**
		 * common method used to get the grid xml data
		 * 
		 * @date Feb 24, 2010	
		 * @param
		 */
		public String getXmlGridData(HashMap hmParamV) throws AppError {

			GmTemplateUtil templateUtil = new GmTemplateUtil();
			HashMap hmAccess = new HashMap();
	    	
			ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
			String strTemplate = (String) hmParamV.get("TEMPLATE");
			String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
			String strAccessLvl = GmCommonClass.parseNull((String)hmParamV.get("ACCLVL"));
			hmAccess.put("ACCLVL", strAccessLvl);
			templateUtil.setDataList("alResult", alParam);
			templateUtil.setDataMap("HMACCESS", hmAccess);
			templateUtil.setTemplateName(strTemplate);
			templateUtil.setTemplateSubDir(strTemplatePath);
			return templateUtil.generateOutput();
		}
		
		 /**
		  * Method to get Link Info for a given type		  * 
		  * @param strType
		  * @return hashMap
		  */
		 public HashMap getLinkInfo(String strType) throws AppError
		 {
			 HashMap hmLinkDetails = new HashMap();
			 HashMap hmapValue = new HashMap();
			 ArrayList alLinkList = new ArrayList();
			 
			 hmLinkDetails.put("LINKID", "ID");
			 
			 if(strType.equals("RADIOGRAPH")){
				 hmapValue.put("KEY", "AP");
				 hmapValue.put("VALUE", "A P Films");
				 alLinkList.add(hmapValue);
				 
				 hmapValue = new HashMap();
				 hmapValue.put("KEY", "Lateral");
				 hmapValue.put("VALUE", "Lat Films");
				 alLinkList.add(hmapValue);
				 
				 hmapValue = new HashMap();
				 hmapValue.put("KEY", "Flexion");
				 hmapValue.put("VALUE", "Flex Films");
				 alLinkList.add(hmapValue);
				 
				 hmapValue = new HashMap();
				 hmapValue.put("KEY", "Extension");
				 hmapValue.put("VALUE", "Exten Films");
				 alLinkList.add(hmapValue);
				 
				 hmapValue = new HashMap();
				 hmapValue.put("KEY", "MRI/CT");
				 hmapValue.put("VALUE", "MRI/CT Films");
				 alLinkList.add(hmapValue);
				 
				 hmapValue = new HashMap();
				 hmapValue.put("KEY", "Comments");
				 hmapValue.put("VALUE", "Comments");
				 alLinkList.add(hmapValue);
				 
				 hmLinkDetails.put("POSITION", "AFTER");
			 }
			 hmLinkDetails.put("LINKVALUE", alLinkList);
			 
			 return hmLinkDetails;
		 }
	 

}
