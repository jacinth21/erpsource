package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalIRBApprovalBean {
	 Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
		
	 public String saveIRBApproval(HashMap hmParam) throws Exception{
		 
		 String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYID"));
         String strSiteId = GmCommonClass.parseNull((String) hmParam.get("STUDYSITEID"));
         String strApprovalType = GmCommonClass.parseNull((String) hmParam.get("APPROVALTYPE"));
         String strApprovalReason = GmCommonClass.parseNull((String) hmParam.get("APPROVALREASON"));
         String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));     
         String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
         String strIRBID=GmCommonClass.parseNull((String) hmParam.get("IRBID"));
         String strReviewTyp=GmCommonClass.parseNull((String) hmParam.get("REVIEWTYPE"));
         String strReviewPrd=GmCommonClass.parseNull((String) hmParam.get("REVIEWPERIOD"));
         String strApprovalDt=GmCommonClass.parseNull((String) hmParam.get("APPROVALDATE"));
         String strExpiryDt=GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATE"));
         String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
         
         log.debug(" # # # # hmParam "+hmParam);
         if(strStatus!=null && strStatus.equalsIgnoreCase("")){
        	 strStatus="60529";
         }
         
         GmDBManager gmDBManager = new GmDBManager();     
         gmDBManager.setPrepareString("GM_PKG_CR_IRB.GM_CR_SAV_IRB_APPROVAL",11);
         gmDBManager.registerOutParameter(11, OracleTypes.VARCHAR);          
         gmDBManager.setString(1, strStudyId);
         gmDBManager.setString(2, strSiteId);
         gmDBManager.setString(3, strApprovalType);
         gmDBManager.setString(4, strApprovalReason);
         gmDBManager.setString(5, strStatus);
         gmDBManager.setString(6, strReviewTyp);
         gmDBManager.setString(7, strReviewPrd);
         gmDBManager.setString(8, strApprovalDt);
         gmDBManager.setString(9, strExpiryDt);
         gmDBManager.setString(10, strUserId);
         gmDBManager.setString(11, strIRBID);
         
         gmDBManager.execute();
         strIRBID = gmDBManager.getString(11);
                  
         if(!strLog.equals("")){
        	 (new GmCommonBean()).saveLog(gmDBManager, strIRBID, strLog, strUserId, "1256");
         }
         gmDBManager.commit();
         
		 return strIRBID;
	 }
	 
	 

	 public HashMap loadIRBApproval(HashMap hmParam) throws Exception {
		 
		 String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYID"));
         String strStudySiteID = GmCommonClass.parseNull((String) hmParam.get("STUDYSITEID"));
         String strApprovalType = GmCommonClass.parseNull((String) hmParam.get("APPROVALTYPE"));
         String strApprovalReason = GmCommonClass.parseNull((String) hmParam.get("APPROVALREASON"));
         String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));     
         String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
         String strIRBID=GmCommonClass.parseNull((String) hmParam.get("IRBID"));
         String strReviewTyp=GmCommonClass.parseNull((String) hmParam.get("REVIEWTYPE"));
         String strReviewPrd=GmCommonClass.parseNull((String) hmParam.get("REVIEWPERIOD"));
         String strApprovalDt=GmCommonClass.parseNull((String) hmParam.get("APPROVALDATE"));
         String strExpiryDt=GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATE"));
         
         
         log.debug(" # # # hmParam"+hmParam);
         
         GmDBManager gmDBManager = new GmDBManager();     
         gmDBManager.setPrepareString("GM_PKG_CR_IRB.GM_CR_FCH_IRB_APPROVAL",6);
         gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);          
         gmDBManager.setString(1, strIRBID);
         gmDBManager.setString(2, strStudySiteID);
         gmDBManager.setString(3, strApprovalType);
         gmDBManager.setString(4, strApprovalReason);
         gmDBManager.setString(5, strStatus);
         
         gmDBManager.execute();
         ArrayList alIRBApproval = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
         
         if(strIRBID.equals("")){
        	 strIRBID = GmCommonClass.parseNull((String) hmParam.get("IRBLOGID"));
         }
         ArrayList alLog  = (new GmCommonBean()).getLog(strIRBID,"1256");
         
         HashMap hmReturn = new HashMap();
         hmReturn.put("IRBAPPROVAL", alIRBApproval);
         hmReturn.put("IRBLOG", alLog);
         
         log.debug(" # # #  "+hmReturn);
         gmDBManager.close();
		 return hmReturn;
	 }
}
