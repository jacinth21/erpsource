/*
 * Module: GmARBean.java Author: Dhinakaran James Project: Globus Medical App
 * Date-Written: 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What
 * you changed
 *  
 */

package com.globus.clinical.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.util.GmTemplateUtil;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalTrainingBean {
	
	//Code to Initialize the Logger Class.
	Logger log = GmLogger.getInstance(this.getClass().getName()); 
    
	/**
	 * loadTrainingReport - This method call gm_cr_fch_patient_record procedure to retrieve all patient record for selected criteria
     * on monitor report page. 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList loadTrainingReport(HashMap hmParam, String strCondition) throws AppError {
    	  log.debug("loadSiteMonitorReport");  
    	
    	  ArrayList alTrainingReport = new ArrayList();         
          
          String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
          String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SITELIST"));
          String strTrainingFor = GmCommonClass.parseNull((String)hmParam.get("TRAININGFOR"));
          String strTrainingReason = GmCommonClass.parseNull((String)hmParam.get("TRAININGREASON"));
          String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));

          if(strStudyId.equals("0") || strStudyId.equals("") )
        	  strStudyId = null;
          if(strStudySiteId.equals("0") || strStudySiteId.equals(""))
        	  strStudySiteId = null; 
          if(strTrainingFor.equals("0") || strTrainingFor.equals(""))
        	  strTrainingFor = null; 
          if(strTrainingReason.equals("0") || strTrainingReason.equals(""))
        	  strTrainingReason = null; 
          if(strStatus.equals("0") || strStatus.equals(""))
        	  strStatus = null; 
          
          log.debug("strStudyId " + strStudyId);  
          log.debug("strStudySiteId " + strStudySiteId);  
          log.debug("strTrainingFor " + strTrainingFor);  
          log.debug("strTrainingReason " + strTrainingReason);  
          log.debug("strStatus " + strStatus);  
          
  		log.debug("hmParam:" + hmParam);
		GmDBManager gmDBManager = new GmDBManager();

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.setLength(0);
		sbQuery.append(" SELECT t635.C635_CLINICAL_TRAINING_RCD_ID TRAINING_RCD_ID, t611.c611_study_nm study, 			"); 
		sbQuery.append("        get_account_name (t614.c704_account_id) site, 											");
		sbQuery.append("        get_code_name (t635.c901_training_for) tfor, 											");
		sbQuery.append("        get_party_name (t635.c635_trainee_ref_id) trainee, 										");
		sbQuery.append("        get_code_name (t635.c901_training_reason) reason, 										");
		sbQuery.append("        t635.c635_training_scope SCOPE, 														");
		sbQuery.append("        TO_CHAR (t635.c635_completion_date, 'MM/DD/YYYY') comp_dt, 								");
		sbQuery.append("        get_code_name (t635.c901_status) status 												");
		sbQuery.append("   FROM t635_clinical_training_record t635, 													");
		sbQuery.append("        t611_clinical_study t611, 																");
		sbQuery.append("        t614_study_site t614 																	");
		sbQuery.append("  WHERE t635.c635_void_fl IS NULL 																");
		sbQuery.append("    AND t635.c611_study_id = t611.c611_study_id 												");
		sbQuery.append("    AND t635.c614_study_site_id = t614.c614_study_site_id 										");
		sbQuery.append("    AND t635.c611_study_id = NVL ('" + strStudyId + "', t635.c611_study_id) 					");
		sbQuery.append("    AND t635.c614_study_site_id = NVL (" + strStudySiteId + ", t635.c614_study_site_id) 		");
		sbQuery.append("    AND t635.c901_training_for = NVL (" + strTrainingFor + ", t635.c901_training_for) 			");
		sbQuery.append("    AND t635.c901_training_reason = NVL (" + strTrainingReason + ", t635.c901_training_reason) 	");
		//sbQuery.append("    AND t635.c901_status = NVL (" + strStatus + " , t635.c901_status) 							");

		if (!strCondition.equals("")) {
			sbQuery.append(" AND " + strCondition);
		}

		log.debug("query===" + sbQuery.toString());
		alTrainingReport = gmDBManager.queryMultipleRecords(sbQuery.toString());
		gmDBManager.close();
		return alTrainingReport;
          
    } // End of loadPatientRecord
    
    
	public String getXmlGridData(HashMap hmParamV) throws AppError {
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();

		ArrayList alParam = (ArrayList)hmParamV.get("RLIST");
		String strTemplate = (String)hmParamV.get("TEMPLATE");
		String strTemplatePath = (String)hmParamV.get("TEMPLATEPATH");

		templateUtil.setDataList("alResult", alParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}// End of getXmlGridData

	/**
	 * loadSiteScInfo - This method call gm_pkg_cr_sitemapping.GM_FCH_SITE_SC_INFO procedure to retrieve all Site Co-ordinator names for given criteria
     * for Name of Trainee on "Training Records:Setup" page  
     * @return ArrayList
     * @exception AppError
     */
	public ArrayList loadSiteScInfo(String strStudySiteId) throws AppError {
		ArrayList alScList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_sitemapping.GM_FCH_SITE_SC_INFO", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strStudySiteId);
		gmDBManager.execute();
		alScList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("Exit");
		return alScList;
	}// End of loadSiteScInfo
	
	/**
	 * loadSurgeonInfo - This method call GM_PKG_CR_COMMON.GM_CR_FCH_SURGEON_MAP_DETAILS procedure to retrieve all Surgeon names for given criteria
     * for Name of Trainee on "Training Records:Setup" page  
     * @return ArrayList
     * @exception AppError
     */
	public ArrayList loadSurgeonInfo(String strStudySiteId,String strSurgeonType) throws AppError {
		ArrayList alSurgeonList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		
		strSurgeonType = GmCommonClass.parseNull(strSurgeonType);
		
		
		if(strSurgeonType.equals("2401")){
			strSurgeonType = "60200"; //Principal Investigator
		}else if(strSurgeonType.equals("2404")){
			strSurgeonType = "60201"; //Co- Investigator
		}
		
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_CR_FCH_SURGEON_MAP_DETAILS", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strStudySiteId));
		gmDBManager.setString(2, strSurgeonType);
		gmDBManager.execute();
		alSurgeonList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		log.debug("Exit");
		return alSurgeonList;
	}// End of loadSurgeonInfo

	/**
	 * fetchTrainingDetail - This method call gm_pkg_cr_sitemapping.gm_fch_training_detail procedure to fetch Records from Training Records table.
     * 
     * @return HashMap
     * @exception AppError
     */
	public HashMap fetchTrainingDetail(String strTrainingRecordId) throws AppError {
		HashMap hmTrainingDetail = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_sitemapping.gm_fch_training_detail", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strTrainingRecordId));
		gmDBManager.execute();
		hmTrainingDetail = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
	
		log.debug("Fetch Values - = " + hmTrainingDetail);
		gmDBManager.close();
		return hmTrainingDetail;
	}// End of fetchTrainingDetail
	
	/**
	 * saveTrainingDetail - This method call gm_pkg_cr_sitemapping.gm_sav_training_record procedure to save Training Details to the Table.
     * 
     * @return String - Training ID
     * @exception AppError
     */	
	public String saveTrainingDetail( HashMap hmParam) throws AppError {
		
		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
        String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SITENAMES"));
        String strTrainingFor = GmCommonClass.parseNull((String) hmParam.get("TRAININGFORLIST"));
        String strTraineeName = GmCommonClass.parseNull((String) hmParam.get("TRAINEENAMELIST"));
        String strTrainingReason = GmCommonClass.parseNull((String) hmParam.get("TRAININGREASONLIST"));
        String strTrainingScope = GmCommonClass.parseNull((String) hmParam.get("TRAININGSCOPE"));
        String strCompletionDate = GmCommonClass.parseNull((String) hmParam.get("COMPLETIONDATE"));
        String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strTrainingRecordId = GmCommonClass.parseNull((String)hmParam.get("STRTRAININGRECORDID"));
        String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUSLIST"));
        
        
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_sitemapping.gm_sav_training_record", 10);
		gmDBManager.registerOutParameter(10, java.sql.Types.VARCHAR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strStudySiteId));
		gmDBManager.setString(3, GmCommonClass.parseNull(strTrainingFor));
		gmDBManager.setString(4, GmCommonClass.parseNull(strTraineeName));
		gmDBManager.setString(5, GmCommonClass.parseNull( strTrainingReason));
		gmDBManager.setString(6, GmCommonClass.parseNull(strTrainingScope));
		gmDBManager.setString(7, GmCommonClass.parseNull(strCompletionDate));
		gmDBManager.setString(8, GmCommonClass.parseNull(strStatus));
		gmDBManager.setString(9, GmCommonClass.parseNull(strUserId));
		gmDBManager.setString(10, GmCommonClass.parseNull(strTrainingRecordId));
		gmDBManager.execute();
		strTrainingRecordId = gmDBManager.getString(10);
		log.debug("Save Values - = " + strTrainingRecordId);
		gmDBManager.commit();
		return strTrainingRecordId;
	}// End of saveTrainingDetail
}// end of class GmClinicalBean
