package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalIRBMediaBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
	
	public String saveIRBMedia(HashMap hmParam) throws Exception{
		 
		String strIRBID=GmCommonClass.parseNull((String) hmParam.get("IRBID"));
		String strMediaType = GmCommonClass.parseNull((String) hmParam.get("MEDIATYPE"));
        String strNotes = GmCommonClass.parseNull((String) hmParam.get("IRBMEDIANOTES"));
        String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        String strIRBMediaID = GmCommonClass.parseNull((String) hmParam.get("IRBMEDIAID"));
        
        if(!strNotes.equals("") && strNotes.length()>4000){
        	strNotes = strNotes.substring(0, 4000);
        }
        log.debug(" # # # # hmParam "+hmParam+"\n"+strNotes);
        
        GmDBManager gmDBManager = new GmDBManager();     
        gmDBManager.setPrepareString("GM_PKG_CR_IRB.GM_CR_SAV_IRB_MEDIA",5);
        gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);          
        gmDBManager.setString(1, strIRBID);
        gmDBManager.setString(2, strMediaType);
        gmDBManager.setString(3, strNotes);
        gmDBManager.setString(4, strUserId);
        gmDBManager.setString(5, strIRBMediaID);
        
        gmDBManager.execute();
        strIRBMediaID = gmDBManager.getString(5);
   
        gmDBManager.commit();
        
		 return strIRBMediaID;
	 }
	public ArrayList loadIRBMedia(HashMap hmParam) throws Exception {
		 
		 String strIRBID=GmCommonClass.parseNull((String) hmParam.get("IRBID"));
		 String strIRBMediaID = GmCommonClass.parseNull((String) hmParam.get("IRBMEDIAID"));
        
        log.debug(" # # # hmParam"+hmParam);
        
        GmDBManager gmDBManager = new GmDBManager();     
        gmDBManager.setPrepareString("GM_PKG_CR_IRB.GM_CR_FCH_IRB_MEDIA",3);
        gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);          
        gmDBManager.setString(1, strIRBID);
        gmDBManager.setString(2, strIRBMediaID);
        gmDBManager.execute();
        ArrayList alIRBMedia = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
        
        log.debug(" >>>> alIRBMedia "+alIRBMedia);
        
        gmDBManager.close();
		 return alIRBMedia;
	 }
}
