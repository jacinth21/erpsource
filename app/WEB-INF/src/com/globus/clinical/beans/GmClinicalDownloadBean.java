/*
 * Module: GmARBean.java Author: Hardik Parikh Project: Globus Medical App
 * Date-Written: 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What
 * you changed
 *  
 */

package com.globus.clinical.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalDownloadBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.    
	/**
	 * fetchDownloadHistory - This method call gm_fetch_download_history to retrieve download history.
     *  
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchDownloadHistory(String strStudyId,String strFormId) throws AppError {
    	 log.debug("Inside fetchDownloadHistory");
          ArrayList alDwnldRecords = new ArrayList();
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_DATADOWNLOAD.gm_fetch_download_history", 3);
          gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);          
          gmDBManager.setString(1, strStudyId);
          gmDBManager.setString(2,strFormId);         
          gmDBManager.execute();
          alDwnldRecords = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
          gmDBManager.close();
          log.debug("Exit");
          return alDwnldRecords;
          
    } // End of fetchDownloadHistory
    
    /**
	 * fetchDownloadForm - This method call gm_fetch_download_form procedure to retrieve all downloadable forms for selected study.  
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchDownloadForm(String strStudyId) throws AppError {
          ArrayList alDownloadForms = new ArrayList();
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_DATADOWNLOAD.gm_fetch_download_form", 2);
          gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
          gmDBManager.setString(1, strStudyId);
          gmDBManager.execute();
          alDownloadForms = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
          gmDBManager.close();
          log.debug("Exit");
          return alDownloadForms;

          
    } // End of fetchDownloadForm


    /**
	 * fetchFormData - This method call gm_fetch_form_data procedure to retrieve all patient record for selected criteria
     * on clinical data download page. 
     * @return ArrayList
     * @exception AppError
     */
    public HashMap fetchFormData(HashMap hmParam) throws AppError {           	  
          
          log.debug(" fetchFormData hmParam :"+hmParam);
          String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
          String strFormId = GmCommonClass.parseNull((String)hmParam.get("FORMID"));
          String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
          String strFileName = GmCommonClass.parseNull((String)hmParam.get("FILENAME"));  
          String strCalculatedFormId = GmCommonClass.parseNull((String)hmParam.get("SFCALCULATEDFORMID"));      
                    
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_DATADOWNLOAD.gm_fetch_form_data", 8);
          gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
          gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
          gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
          gmDBManager.setString(1, strStudyId);
          gmDBManager.setString(2, strFormId);
          gmDBManager.setString(3, strUserId);
          gmDBManager.setString(4, strFileName);
          gmDBManager.setString(5, strCalculatedFormId);          
          gmDBManager.execute();
          ArrayList alHeaderList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));        
          ResultSet rsDetails = (ResultSet) gmDBManager.getObject(7);         
          ResultSet rsHeader = (ResultSet) gmDBManager.getObject(8);
          
		 GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		 gmCrossReport.setFixedColumn(true);
		 gmCrossReport.setFixedParams(5,"RecordId");
		 gmCrossReport.setFixedParams(6,"SurveyDate");
		 HashMap hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader, rsDetails);
		 
		 hmFinalValue.put("HEADERLIST", alHeaderList);
		 
		 gmDBManager.commit();
		 log.debug("hmFinalValue=>"+hmFinalValue);

         log.debug("Exit");
         
         return hmFinalValue;          
    } // End of fetchFormData
    
    
    /**
	 * cancelDownload - This method call gm_cancel_download procedure to do cancel download for selected study and form 
     * 
     * @return ArrayList
     * @exception AppError
     */
    public void cancelDownload(HashMap hmParam) throws AppError {                  
		  String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
		  String strFomId = GmCommonClass.parseNull((String)hmParam.get("FORMID"));
		  String strUploadFileListId = GmCommonClass.parseNull((String)hmParam.get("FILEID"));
		  String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		  String strCancelId = GmCommonClass.parseNull((String)hmParam.get("CANCELID"));
		  String strCalculatedFormId = GmCommonClass.parseNull((String)hmParam.get("SFCALCULATEDFORMID"));
		  String strType="CANCEL";          
		  
		  log.debug(" Inside cancelDownload hmParam =>:"+hmParam);
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_DATADOWNLOAD.gm_cancel_download", 6);          
          gmDBManager.setString(1, strStudyId);
          gmDBManager.setString(2, strFomId);          
          gmDBManager.setString(3, strUserId);
          gmDBManager.setString(4, strCancelId);
          gmDBManager.setString(5, strType);
          gmDBManager.setString(6, strCalculatedFormId);          
          gmDBManager.execute();
          gmDBManager.commit();
          log.debug("Exit");                    
    } // End of cancelDownload

}// end of class GmClinicalBean
