package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmDynamicDataLoadBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    

    /**
      * reportDynamicData - This Method is used to report all the form data based on the filter conditions
      * @param String strStudyListId, String strSiteId  
      * @return HashMap
      * @exception AppError
    **/     
      
      public ArrayList fetchFormList(String strStudyId) throws AppError
      {
          ArrayList alFormList = new ArrayList();
           	  GmDBManager gmDBManager = new GmDBManager();
              gmDBManager.setPrepareString("GM_PKG_CR_DYNAMIC_DATA_LOAD.gm_fch_forms_list", 2);
              gmDBManager.setString(1,GmCommonClass.parseNull(strStudyId));
              gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
              gmDBManager.execute();
              alFormList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
              gmDBManager.close();      
             // log.debug("Exit");                   
              return alFormList;
      } 
      
      public ArrayList fetchTimePoints(String strStudyListID) throws AppError
      {
          ArrayList alTmptsList = new ArrayList();

        	  GmDBManager gmDBManager = new GmDBManager();
              gmDBManager.setPrepareString("GM_PKG_CR_DYNAMIC_DATA_LOAD.gm_fch_timepoints_list", 2);
              gmDBManager.setString(1,GmCommonClass.parseNull(strStudyListID));
              gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
              gmDBManager.execute();
              alTmptsList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
              gmDBManager.close();      
             // log.debug("Exit");                      
              return alTmptsList;
      } 
      
      public ArrayList fetchQuestionList(String strStudyListId) throws AppError
      {
          ArrayList alQuestionList = new ArrayList();
        	  GmDBManager gmDBManager = new GmDBManager();
              gmDBManager.setPrepareString("GM_PKG_CR_DYNAMIC_DATA_LOAD.gm_fch_questions_list", 2);
              gmDBManager.setString(1,GmCommonClass.parseNull(strStudyListId));
              gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
              gmDBManager.execute();
              alQuestionList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
              gmDBManager.close();      
             // log.debug("Exit");                      
              return alQuestionList;
      } 
      
      public ArrayList fetchAnswers(String strQuestionID,String strFormId) throws AppError
      {
          ArrayList alAnswersList = new ArrayList();
        	  GmDBManager gmDBManager = new GmDBManager();
              gmDBManager.setPrepareString("GM_PKG_CR_DYNAMIC_DATA_LOAD.gm_fch_answers_list", 3);
              gmDBManager.setString(1,GmCommonClass.parseNull(strQuestionID));
              gmDBManager.setString(2,GmCommonClass.parseNull(strFormId));
              gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
              gmDBManager.execute();
              alAnswersList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
              gmDBManager.close();      
              return alAnswersList;
      } 
      
      public HashMap fetchPeriodsInYaxis(HashMap hmParam) throws AppError
      {
    	  HashMap hmReturn = new HashMap();
    	  GmCrossTabReport gmCrossReport = new GmCrossTabReport();
    	  
    		  GmDBManager gmDBManager = new GmDBManager();
              gmDBManager.setPrepareString("GM_PKG_CR_DYNAMIC_DATA_LOAD.gm_fch_tpoints_details ", 6);
              gmDBManager.setString(1,GmCommonClass.parseNull((String)hmParam.get("InputString")));
              gmDBManager.setString(2,GmCommonClass.parseNull((String)hmParam.get("StudyListID")));
              gmDBManager.setString(3,GmCommonClass.parseNull((String)hmParam.get("QuestionID")));
              gmDBManager.setString(4,GmCommonClass.parseNull((String)hmParam.get("AnswerGroupID")));
              gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
              gmDBManager.registerOutParameter(6, OracleTypes.CURSOR); 
              gmDBManager.execute();
              ResultSet rsHeader = (ResultSet) gmDBManager.getObject(5);
              ResultSet rsDetails = (ResultSet) gmDBManager.getObject(6);            
              gmCrossReport.setFixedColumn(true);
              gmCrossReport.setFixedParams(5,"PatientId");
              gmCrossReport.setFixedParams(6,"Treatment");
              gmCrossReport.setFixedParams(7,"Surgeon");
              gmCrossReport.setFixedParams(8,"SurgeryDate"); 
    
              
              hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);                      
              gmDBManager.close(); 
              return hmReturn;
      }
      
      public HashMap fetchAnswersInYaxis(HashMap hmParam) throws AppError
      {
    	  HashMap hmReturn = new HashMap();
    	  GmCrossTabReport gmCrossReport = new GmCrossTabReport();
    	  
    		  GmDBManager gmDBManager = new GmDBManager();
              gmDBManager.setPrepareString("GM_PKG_CR_DYNAMIC_DATA_LOAD.gm_fch_answers_details ",4);
              gmDBManager.setString(1,GmCommonClass.parseNull((String)hmParam.get("InputString")));
              gmDBManager.setString(2,GmCommonClass.parseNull((String)hmParam.get("StudyListID")));              
              gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
              gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);              
              gmDBManager.execute();
              ResultSet rsHeader = (ResultSet) gmDBManager.getObject(3);
              ResultSet rsDetails = (ResultSet) gmDBManager.getObject(4);  
              
              gmCrossReport.setFixedColumn(true);
              gmCrossReport.setFixedParams(5,"PatientId");
              gmCrossReport.setFixedParams(6,"Treatment");
              gmCrossReport.setFixedParams(7,"Surgeon");
              gmCrossReport.setFixedParams(8,"SurgeryDate");
              gmCrossReport.setFixedParams(9,"ExamDate");
              gmCrossReport.setFixedParams(10,"Timepoint");
              
              hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
             // log.debug("break point 3" + hmReturn.get("Header"));
              gmDBManager.close();              
      		  return hmReturn;
    	  
      }
      
      
      
}
