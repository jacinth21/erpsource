/*
 * Bean: GmPatientBean.java Author: Richard Project: Globus Medical App Date-Written: 31 Oct 2005
 * Security: Unclassified Description: This Bean will be used to handle all the querys releated to
 * patient bean
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.clinical.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.clinical.data.GmStudyFormData;
import com.globus.clinical.data.GmStudyPeriodData;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmStudyBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.

  // Below 2 methos need to be removed
  /*
   * public static void main(String[] args) throws Exception { GmStudyBean t = new GmStudyBean (); }
   * 
   * public GmStudyBean () throws Exception { reportStudyList("0101","GPR002"); }
   */
  /**
   * reportProject - This method will fetch party information from party table
   * 
   * @param String strType -- Type for party to be retrieved
   * 
   * @return ArrayList List of party name
   * @exception AppError
   **/
  public ArrayList reportStudyNameList() throws AppError {
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    CallableStatement csmt = null;
    Connection conn = null;

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;

    try {
      conn = dbCon.getConnection();
      strPrepareString = dbCon.getStrPrepareString("GM_REPORT_STUDY", 2);
      csmt = conn.prepareCall(strPrepareString);
      /*
       * register out parameter and set input parameters
       */
      csmt.registerOutParameter(1, java.sql.Types.CHAR);
      csmt.registerOutParameter(2, OracleTypes.CURSOR);

      csmt.execute();
      strBeanMsg = csmt.getString(1);
      rs = (ResultSet) csmt.getObject(2);

      alReturn = dbCon.returnArrayList(rs);

      log.debug("alReturn in study bean:" + alReturn);

    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmStudyBean:reportStudy", "Exception is:" + e);
      throw new AppError(e);
    } finally {
      try {
        if (csmt != null) {
          csmt.close();
        }// Enf of if (csmt != null)
        if (conn != null) {
          conn.close(); /* closing the Connections */
        }
      } catch (Exception e) {
        throw new AppError(e);
      }// End of catch
      finally {
        csmt = null;
        conn = null;
        dbCon = null;
      }
    }
    return alReturn;
  }

  /**
   * reportProject - This method will fetch party / Study Associated from form/xray information
   * 
   * @param String strPatientID -- Patient ID Information
   * @param String strStudyID -- Patient Study Map Information
   * 
   * @return ArrayList List of party name
   * @exception AppError
   **/
  public ArrayList reportStudyList(String strPatientID, String strStudyID) throws AppError {
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    CallableStatement csmt = null;
    Connection conn = null;

    ArrayList alStudyList = new ArrayList();
    ArrayList alStudyPeriod = new ArrayList();
    ResultSet rs = null;

    GmStudyFormData gmStudyForm = new GmStudyFormData();
    GmStudyPeriodData gmStudyPeriod = null;

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;
    String strOldStudyID = "";
    String strCurrentStudyID = "";
    String strTemp = "";

    try {
      conn = dbCon.getConnection();
      // conn = dbCon.getOracleStaticConnection();
      strPrepareString = dbCon.getStrPrepareString("GM_REPORT_PATIENT_STUDY_LIST", 4);
      csmt = conn.prepareCall(strPrepareString);
      /*
       * register out parameter and set input parameters
       */
      csmt.registerOutParameter(3, java.sql.Types.CHAR);
      csmt.registerOutParameter(4, OracleTypes.CURSOR);

      csmt.setString(1, strPatientID);
      csmt.setString(2, strStudyID);

      csmt.execute();
      strBeanMsg = csmt.getString(3);
      rs = (ResultSet) csmt.getObject(4);



      // To add the value into study data structure object
      while (rs.next()) {
        gmStudyPeriod = new GmStudyPeriodData();
        gmStudyPeriod.setStudyListID(rs.getString(1));
        gmStudyPeriod.setFormID(rs.getString(3));
        gmStudyPeriod.setFormName(rs.getString(4));
        gmStudyPeriod.setStudyFormDs(rs.getString(5));
        // gmStudyPeriod = new GmStudyPeriodData();
        gmStudyPeriod.setStudyPeriodID(rs.getString(6));
        gmStudyPeriod.setPeriodDS(rs.getString(7));
        gmStudyPeriod.setStudyDuration(rs.getInt(8));
        gmStudyPeriod.setEarliestDate(rs.getString(9));
        gmStudyPeriod.setExpectedDate(rs.getString(10));
        gmStudyPeriod.setLatestDate(rs.getString(11));
        // Patient Information
        gmStudyPeriod.setFormReceivedDate(rs.getString(12));
        strTemp = rs.getString(13);
        if (strTemp.equals("Y")) {
          gmStudyPeriod.setFormVerifiedFlag(true);
        }
        gmStudyPeriod.setGracePeriodType(rs.getString(13));
        gmStudyPeriod.setFormVerifiedBy(rs.getString(14));
        strTemp = rs.getString(15);
        if (strTemp.equals("Y")) {
          gmStudyPeriod.setDueFlag(true);
        }
        gmStudyPeriod.setStudyPhase(rs.getInt(16));
        alStudyList.add(gmStudyPeriod);
      }


      alStudyList.add(gmStudyPeriod);


      // System.out.println("Before Bean");
      // alReturn = dbCon.returnArrayList(rs);
      // System.out.println("alReturn " + alReturn);
    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmStudyBean:reportStudy", "Exception is:" + e);
      throw new AppError(e);
    } finally {
      try {
        if (csmt != null) {
          csmt.close();
        }// Enf of if (csmt != null)
        if (conn != null) {
          conn.close(); /* closing the Connections */
        }
      } catch (Exception e) {
        throw new AppError(e);
      }// End of catch
      finally {
        csmt = null;
        conn = null;
        dbCon = null;
      }
    }
    return alStudyList;
  }

  public HashMap saveClinicalSiteMap(HashMap hmParam) throws AppError {
	String strStudySiteId = "";
    HashMap hmReturn = new HashMap();
    String strStudyId = GmCommonClass.parseNull((String) hmParam.get("SESSSTUDYID"));
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("SITENAMELIST"));
    String strSiteID = GmCommonClass.parseNull((String) hmParam.get("SITEID"));
    // String strApprovalDate = GmCommonClass.parseNull((String) hmParam.get("APPROVALDATE"));
    // Commenting approval date as its changed to label in the UI.
    String strCraName = GmCommonClass.parseNull((String) hmParam.get("CRANAME"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strIRBTypeName = GmCommonClass.parseNull((String) hmParam.get("IRBTYPENAME"));
    String strStudyPktDt = GmCommonClass.parseNull((String) hmParam.get("STUDYPKTSENT"));
    String strTinNumber = GmCommonClass.parseNull((String) hmParam.get("TINNUMBER"));
    
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_CR_SAV_SITE_MAP_INFO", 9);
    gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strStudyId);
    gmDBManager.setString(2, strAccountId);
    gmDBManager.setString(3, strSiteID);
    gmDBManager.setString(4, strCraName);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strIRBTypeName);
    gmDBManager.setString(7, strStudyPktDt);
    gmDBManager.setString(8, strTinNumber);
    gmDBManager.execute();
    strStudySiteId = gmDBManager.getString(9);
    gmDBManager.commit();
    hmReturn.put("STUDYSITEID", strStudySiteId);
    log.debug("Exit");
    return hmReturn;
  }
}
