/*
 * Module: GmARBean.java Author: Dhinakaran James Project: Globus Medical App
 * Date-Written: 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What
 * you changed
 *  
 */

package com.globus.clinical.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.util.GmTemplateUtil;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalSiteMonitorBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.
    
    /**
	 * Method to save the site monitoring records to db
	 * 
	 * @param hmParam
	 * @return java.lang.String - 
	 * @throws com.globus.common.beans.AppError
	 */
	public String saveSiteMonitor(HashMap hmParam) throws AppError {
		//GmCommonBean gmCommonBean = new GmCommonBean();
		GmDBManager gmDBManager = new GmDBManager();
		
		
    	String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYID"));
    	String strSiteId = GmCommonClass.parseNull((String) hmParam.get("SITEID"));
    	String strOtherMonitor = GmCommonClass.parseNull((String) hmParam.get("MONITORLIST"));
		String strDateVisited = GmCommonClass.parseNull((String) hmParam.get("VISITDT"));
		String strOwnerOfVisit = GmCommonClass.parseNull((String) hmParam.get("MONITOROWNER"));
		String strPurposeOfVisit = GmCommonClass.parseNull((String) hmParam.get("PURPOSEOFVISIT"));
		String strSiteMonitorStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strNotesForVisit = GmCommonClass.parseNull((String) hmParam.get("NOTESOFVISIT"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strStMtrId = GmCommonClass.parseNull((String) hmParam.get("STMTRID"));
		
		log.debug("strStudyId:"+strStudyId);
		log.debug("strSiteId:"+strSiteId);
		log.debug("strOtherMonitor:"+strOtherMonitor);
		log.debug("strDateVisited:"+strDateVisited);
		log.debug("strOwnerOfVisit:"+strOwnerOfVisit);
		log.debug("strPurposeOfVisit:"+strPurposeOfVisit);
		log.debug("strSiteMonitorStatus:"+strSiteMonitorStatus);
		log.debug("strNotesForVisit:"+strNotesForVisit);
		log.debug("strUserId:"+strUserId);
		log.debug("strStMtrId:"+strStMtrId);
		
		gmDBManager.setPrepareString("gm_pkg_cr_site_monitor.gm_cr_sav_site_monitor", 12);
		gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);
		
		gmDBManager.setInt(1, Integer.parseInt(strSiteId));
		gmDBManager.setString(2, strStudyId.equals("")?null:strStudyId);
		gmDBManager.setString(3, null);	//surgeon id
		gmDBManager.setString(4, strDateVisited.equals("")?null:strDateVisited);
		gmDBManager.setInt(5, Integer.parseInt(strPurposeOfVisit.equals("")?"0":strPurposeOfVisit));
		gmDBManager.setString(6, strNotesForVisit.equals("")?null:strNotesForVisit);
		gmDBManager.setInt(7,Integer.parseInt(strSiteMonitorStatus.equals("")?"0":strSiteMonitorStatus));
		gmDBManager.setInt(8, Integer.parseInt(strOwnerOfVisit.equals("")?"0":strOwnerOfVisit));
		gmDBManager.setString(9, strUserId);
		gmDBManager.setString(10, strOtherMonitor);
		gmDBManager.setInt(11, 0);	//no of patient
		gmDBManager.setString(12, (strStMtrId.equals("")?null:strStMtrId));	//id
		gmDBManager.execute();
		String strSiteMtrId = gmDBManager.getString(12);
		
		gmDBManager.commit();
		if(strSiteMonitorStatus.equals("60461")){
			log.debug("Inside sendEmail condition:strSiteMtrId->"+strSiteMtrId+"strSiteMonitorStatus:"+strSiteMonitorStatus);
			sendEmail(strSiteMtrId,strSiteMonitorStatus);
		}

		return strSiteMtrId;
	}
	public void saveMonitorStatus() throws AppError {
		log.debug("Inside saveMonitorStatus");
		GmDBManager gmDBManager = new GmDBManager();
		
		gmDBManager.setPrepareString("gm_pkg_cr_site_monitor.gm_cr_fch_scheduled_site_mtr");
		
		gmDBManager.execute();
		gmDBManager.commit();
		
		return;
		
	}
	public void sendReminderEmail() throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		
		gmDBManager.setPrepareString("gm_pkg_cr_site_monitor.gm_cr_reminder_email");
		
		gmDBManager.execute();
		gmDBManager.close();
		
		return;
		
	}
	public void sendEmail(String  strSiteMtrId, String status) throws AppError {
		//GmCommonBean gmCommonBean = new GmCommonBean();
		GmDBManager gmDBManager = new GmDBManager();
		
		gmDBManager.setPrepareString("gm_pkg_cr_site_monitor.gm_cr_email_cra", 3);
		//gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		String strSubject = "Site Monitoring Visit Notification";
		gmDBManager.setString(1, strSubject);
		gmDBManager.setString(2, (strSiteMtrId.equals("")?null:strSiteMtrId));	//id
		gmDBManager.setString(3, status);
		gmDBManager.execute();
		gmDBManager.close();
		
		return;
	}
	/**
	 * Fatches the currency conversion detail using currId from db 
	 * 
	 * 
	 * @param strCurrId
	 * @return java.lang.String
	 * @throws com.globus.common.beans.AppError
	 */
	public HashMap fetchSiteMonitor(String strSiteMtrId) throws AppError {
		HashMap hmReturn = new HashMap();
		HashMap hmReturnntrmap = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_cr_site_monitor.gm_cr_fch_site_monitor", 10);
			 
		gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSiteMtrId);
		gmDBManager.setString(2, null);
		gmDBManager.setString(3, null);
		gmDBManager.setString(4, null);
		gmDBManager.setString(5, null);
		gmDBManager.setString(6, null);
		gmDBManager.setString(7, null);
		gmDBManager.setString(8, null);
		
		
		gmDBManager.execute();
		//ArrayList alSiteMtr = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(9));
		 hmReturnntrmap = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(9));
		ArrayList alSiteMtrMap = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10));
		gmDBManager.close();
		log.debug("hmReturnntrmap(fetchSiteMonitor):"+alSiteMtrMap);
		String[] otherMonitor=new String[alSiteMtrMap.size()];
		HashMap hmOtherMonitor = new HashMap();
		ArrayList al = new ArrayList();
		for(int i=0;i<alSiteMtrMap.size();i++){
			hmOtherMonitor = (HashMap)alSiteMtrMap.get(i);
			log.debug("hmOtherMonitor:"+hmOtherMonitor);
			otherMonitor[i]=((String)hmOtherMonitor.get("ID"));
			al.add((String)hmOtherMonitor.get("ID"));
				
		}
		hmReturnntrmap.put("otherMonitor", otherMonitor);
		hmReturnntrmap.put("alOtherMonitor", al);
		
		log.debug("hmReturnntrmap:"+hmReturnntrmap);
		log.debug("hmReturnntrmap(fetchSiteMonitor):"+alSiteMtrMap);
		//hmReturn.put("alSiteMtr", alSiteMtr); otherMonitor
		hmReturn.put("alSiteMtrMap", alSiteMtrMap);
		

		return hmReturnntrmap;
	}
	/**
	 * loadSiteMonitorReport - This method call gm_cr_fch_patient_record procedure to retrieve all patient record for selected criteria
     * on monitor report page. 
     * @return ArrayList
     * @exception AppError
     */
    public HashMap loadSiteMonitorReport(HashMap hmParam, String strCondition) throws AppError {
    	  log.debug("loadSiteMonitorReport");  
    	
    	  HashMap hmReturn = new HashMap();
    	  ArrayList alMonitorReport = new ArrayList();         
          log.debug("***************hmparam********"+hmParam);
          String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
          String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYSITEID"));
          String strPurpose = GmCommonClass.parseNull((String)hmParam.get("PURPOSEOFVISIT"));
          String strSiteMonitorStatus = GmCommonClass.parseNull((String)hmParam.get("SITEMONITORSTATUS"));
          String strFromDate = GmCommonClass.parseNull((String)hmParam.get("FROMDATE"));
          String strToDate = GmCommonClass.parseNull((String)hmParam.get("TODATE"));

          if(strStudyId.equals("0"))
        	  strStudyId = null;
          if(strStudySiteId.equals("0"))
        	  strStudySiteId = null; 
          if(strPurpose.equals("0"))
        	  strPurpose = null; 
          if(strSiteMonitorStatus.equals("0"))
        	  strSiteMonitorStatus = null; 
          if(strFromDate.equals(""))
        	  strFromDate = null; 
          if(strToDate.equals(""))
        	  strToDate = null; 
          
          log.debug("strStudyId " + strStudyId);  
          log.debug("strStudySiteId " + strStudySiteId);  
          log.debug("strPurpose " + strPurpose);  
          log.debug("strSiteMonitorStatus " + strSiteMonitorStatus);  
          log.debug("strFromDate " + strFromDate);  
          log.debug("strToDate " + strToDate);  
          
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("gm_pkg_cr_site_monitor.gm_cr_fch_site_monitor", 8);
          gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
          gmDBManager.setString(1, "");
          gmDBManager.setString(2, strStudyId);
          gmDBManager.setString(3, strStudySiteId);
          gmDBManager.setString(4, strPurpose);
          gmDBManager.setString(5, strSiteMonitorStatus);
          gmDBManager.setString(6, strFromDate);
          gmDBManager.setString(7, strToDate);
          gmDBManager.execute();
          alMonitorReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
          gmDBManager.close();
          log.debug("Exit size" + alMonitorReport.size());
          hmReturn.put("alSiteMtr",alMonitorReport);
          return hmReturn;
          
    } // End of loadPatientRecord
    
	/**
	 * loadSiteMonitorReport - This method call gm_cr_fch_patient_record procedure to retrieve all patient record for selected criteria
     * on monitor report page. 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList loadSiteMtrObservation(HashMap hmParam, String strCondition) throws AppError {
    	  log.debug("loadSiteMonitorReport");  
    	
    	  ArrayList alMonitorReport = new ArrayList();         
          
          String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYLIST"));
          String strStudySiteId = GmCommonClass.parseNull((String)hmParam.get("SITELIST"));
          String strPurpose = GmCommonClass.parseNull((String)hmParam.get("PURPOSEOFVISIT"));
          String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
          String strFromDate = GmCommonClass.parseNull((String)hmParam.get("FROMDATE"));
          String strToDate = GmCommonClass.parseNull((String)hmParam.get("TODATE"));
          String strCra_id = GmCommonClass.parseNull((String)hmParam.get("OWNEROFVISIT"));          
          String strSiteMtrId = GmCommonClass.parseNull((String)hmParam.get("SITEMTRID"));
          String strPatientId = GmCommonClass.parseNull((String)hmParam.get("PATIENTID"));
          
          if(strStudyId.equals("0") | strStudyId.equals(""))
        	  strStudyId = null;
          if(strStudySiteId.equals("0") | strStudySiteId.equals(""))
        	  strStudySiteId = null; 
          if(strPurpose.equals("0") | strPurpose.equals(""))
        	  strPurpose = null; 
          if(strStatus.equals("0") | strStatus.equals(""))
        	  strStatus = null; 
          if(strFromDate.equals(""))
        	  strFromDate = null; 
          if(strToDate.equals(""))
        	  strToDate = null; 
          if(strCra_id.equals("0") | strCra_id.equals(""))
        	  strCra_id = null; 
          if(strSiteMtrId.equals("0") | strSiteMtrId.equals(""))
        	  strSiteMtrId = null; 

          /*
          log.debug("strStudyId " + strStudyId);  
          log.debug("strStudySiteId " + strStudySiteId);  
          log.debug("strPurpose " + strPurpose);  
          log.debug("strSiteMonitorStatus " + strStatus);  
          log.debug("strFromDate " + strFromDate);  
          log.debug("strToDate " + strToDate);  
          log.debug("strCra_id " + strCra_id);
          log.debug("strSiteMtrId " + strSiteMtrId);
          log.debug("strPatientId " + strPatientId);
          */
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("gm_pkg_cr_site_monitor.gm_cr_fch_site_mtr_observation", 11);
          gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
          gmDBManager.setString(1, null);
          gmDBManager.setString(2, strStudyId);
          gmDBManager.setString(3, strStudySiteId);
          gmDBManager.setString(4, strPurpose);
          gmDBManager.setString(5, strStatus);
          gmDBManager.setString(6, strFromDate);
          gmDBManager.setString(7, strToDate);
          gmDBManager.setString(8, strCra_id);
          gmDBManager.setString(9, strSiteMtrId);
          gmDBManager.setString(10, strPatientId);
          gmDBManager.execute();
          alMonitorReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(11));
          gmDBManager.close();
          log.debug("Exit size" + alMonitorReport.size());
          return alMonitorReport;
          
    } // End of loadPatientRecord
	
	public String getXmlGridData(HashMap hmParamV) throws AppError {
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();

		ArrayList alParam = (ArrayList)hmParamV.get("RLIST");
		String strTemplate = (String)hmParamV.get("TEMPLATE");
		String strTemplatePath = (String)hmParamV.get("TEMPLATEPATH");

		templateUtil.setDataList("alResult", alParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}

}// end of class GmClinicalBean
