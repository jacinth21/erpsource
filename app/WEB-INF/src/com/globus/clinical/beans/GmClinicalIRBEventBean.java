package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalIRBEventBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
	
	public String saveIRBEvent(HashMap hmParam) throws Exception{
		 
		 String strIRBID=GmCommonClass.parseNull((String) hmParam.get("IRBID"));
		 String strActionTaken = GmCommonClass.parseNull((String) hmParam.get("ACTIONTAKEN"));
        String strEventDt = GmCommonClass.parseNull((String) hmParam.get("EVENTDATE"));
        String strNotes = GmCommonClass.parseNull((String) hmParam.get("NOTES"));
        String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        String strReviewTyp=GmCommonClass.parseNull((String) hmParam.get("REVIEWTYPE"));
        String strReviewPrd=GmCommonClass.parseZero((String) hmParam.get("REVIEWPERIOD"));
        
        if(strReviewPrd.equals("0")){
        	strReviewPrd = null;
        }
        
        if(!strNotes.equals("") && strNotes.length()>4000){
        	strNotes = strNotes.substring(0, 4000);
        }
        
        String strIRBEventID = GmCommonClass.parseNull((String) hmParam.get("IRBEVENTID"));
        
        log.debug(" # # # # hmParam "+hmParam);
        
        GmDBManager gmDBManager = new GmDBManager();     
        gmDBManager.setPrepareString("GM_PKG_CR_IRB.GM_CR_SAV_IRB_EVENT",8);
        gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);          
        gmDBManager.setString(1, strIRBID);
        gmDBManager.setString(2, strActionTaken);
        gmDBManager.setString(3, strEventDt);
        gmDBManager.setString(4, strNotes);
        gmDBManager.setString(5, strUserId);
        gmDBManager.setString(6, strReviewTyp);
        gmDBManager.setString(7, strReviewPrd);
        gmDBManager.setString(8, strIRBEventID);
        
        gmDBManager.execute();
        strIRBEventID = gmDBManager.getString(8);
   
        gmDBManager.commit();
        
		 return strIRBEventID;
	 }
	
	 public ArrayList loadIRBEvent(HashMap hmParam) throws Exception {
		 
		 String strIRBID=GmCommonClass.parseNull((String) hmParam.get("IRBID"));
		 String strIRBEventID = GmCommonClass.parseNull((String) hmParam.get("IRBEVENTID"));
         
         log.debug(" # # # hmParam"+hmParam);
         
         GmDBManager gmDBManager = new GmDBManager();     
         gmDBManager.setPrepareString("GM_PKG_CR_IRB.GM_CR_FCH_IRB_EVENT",3);
         gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);          
         gmDBManager.setString(1, strIRBID);
         gmDBManager.setString(2, strIRBEventID);
         gmDBManager.execute();
         ArrayList alIRBEvent = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
         
         log.debug(" >>>> alIRBEvent "+alIRBEvent);
         
         gmDBManager.close();
		 return alIRBEvent;
	 }
}
