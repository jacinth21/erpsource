package com.globus.clinical.beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalSnapshotBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
	/*
	 * Below method is used to get Answered Queries to be displayed in SnapShot.
	 * 
	 */
	public ArrayList loadAnsweredTreeList(HashMap hmParam) throws AppError {
		ArrayList alAnsweredQueriesSnapshot = new ArrayList();
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		//String strTxnId = GmCommonClass.parseNull((String)hmParam.get("STRTXNID"));
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.setLength(0);
		sbQuery.append(" SELECT 40008 sid, 'Answered Queries' stype, t6510.c611_study_id studyid, ");
		sbQuery.append(" t614.c614_site_id id, get_account_name (t614.c704_account_id) name, t613.c613_study_period_ds sds, ");
		sbQuery.append(" t613.C613_STUDY_PERIOD_ID SDSID, t621.c621_patient_ide_no pideno, t621.c621_patient_id patientid, ");
		sbQuery.append(" T6510.C623_PATIENT_ANS_LIST_ID ptanslistid, T6510.C622_PATIENT_LIST_ID patientlistid, t613.C613_SEQ_NO seq, ");
		sbQuery.append(" T6510.C6510_QUERY_ID queryid, T6510.C901_QUERY_LEVEL_ID querylevelid , t6510.C6510_SHORT_DESC shortdesc ");		
		sbQuery.append(" from t6510_query t6510, ");
		sbQuery.append(" t621_patient_master t621, ");
		sbQuery.append(" t622_patient_list t622,");
		sbQuery.append(" t614_study_site t614, ");
		sbQuery.append(" t613_study_period t613, ");
		sbQuery.append(" t612_study_form_list t612 ");
		
		sbQuery.append(" WHERE t6510.c6510_void_fl IS NULL ");
		sbQuery.append(" AND t6510.c611_study_id = decode('"+strStudy+"','',t6510.c611_study_id,'"+strStudy+"') ");
		sbQuery.append(" AND t6510.c901_status IN (92442) "); // Answered Status
		sbQuery.append(" AND t621.c621_patient_id = t6510.c621_patient_id ");
		sbQuery.append(" AND t621.c621_delete_fl = 'N' ");
		sbQuery.append(" AND t622.c622_patient_list_id = t6510.c622_patient_list_id ");
		sbQuery.append(" AND t6510.c614_study_site_id = t614.c614_study_site_id	 ");
		sbQuery.append(" AND t614.c614_site_id = decode('"+strSite+"','',t614.c614_site_id,'"+strSite+"') ");
		sbQuery.append(" AND t622.c613_study_period_id = t613.c613_study_period_id ");
		sbQuery.append(" AND t612.c611_study_id = t614.c611_study_id ");
		sbQuery.append(" AND t612.c612_study_list_id = t613.c612_study_list_id ");
		sbQuery.append(" AND t621.c621_patient_ide_no = decode('"+strPatient+"','',t621.c621_patient_ide_no,'"+strPatient+"') ");
		sbQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		if (!strCondition.equals(""))
		{
			sbQuery.append(" AND t614.C614_STUDY_SITE_ID in (select v621.C614_STUDY_SITE_ID");
			sbQuery.append(" from V621_PATIENT_STUDY_LIST v621 where ");
			sbQuery.append(strCondition);
			sbQuery.append(")");
		}
		sbQuery.append(" order by ID,PIDENO,QUERYID ");
		
		//log.debug("Answered sbquery =="+sbQuery.toString());
		alAnsweredQueriesSnapshot = dbCon.queryMultipleRecords(sbQuery.toString());
		//log.debug(" alAnsweredQueriesSnapshot "+alAnsweredQueriesSnapshot);
		return alAnsweredQueriesSnapshot;
	} // End of loadQueryTreeList
	
	/*
	 * Below method is used to load the query tree
	 * 
	 */
	public ArrayList loadQueryTreeList(HashMap hmParam) throws AppError {
		ArrayList alQueriesSnapshot = new ArrayList();
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		//String strTxnId = GmCommonClass.parseNull((String)hmParam.get("STRTXNID"));
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.setLength(0);
		sbQuery.append(" SELECT 40007 sid, 'Open Queries' stype, t6510.c611_study_id studyid, ");
		sbQuery.append(" t614.c614_site_id id, get_account_name (t614.c704_account_id) name, t613.c613_study_period_ds sds, ");
		sbQuery.append(" t613.C613_STUDY_PERIOD_ID SDSID, t621.c621_patient_ide_no pideno, t621.c621_patient_id patientid, ");
		sbQuery.append(" T6510.C623_PATIENT_ANS_LIST_ID ptanslistid, T6510.C622_PATIENT_LIST_ID patientlistid, t613.C613_SEQ_NO seq, ");
		sbQuery.append(" T6510.C6510_QUERY_ID queryid, T6510.C901_QUERY_LEVEL_ID querylevelid ,t6510.C6510_SHORT_DESC shortdesc ");		
		sbQuery.append(" from t6510_query t6510, ");
		sbQuery.append(" t621_patient_master t621, ");
		sbQuery.append(" t622_patient_list t622,");
		sbQuery.append(" t614_study_site t614, ");
		sbQuery.append(" t613_study_period t613, ");
		sbQuery.append(" t612_study_form_list t612 ");
		
		sbQuery.append(" WHERE t6510.c6510_void_fl IS NULL ");
		sbQuery.append(" AND t6510.c611_study_id = decode('"+strStudy+"','',t6510.c611_study_id,'"+strStudy+"') ");
		sbQuery.append(" AND t6510.c901_status IN (92441,92443) "); // New and Re-issued.
		sbQuery.append(" AND t621.c621_patient_id = t6510.c621_patient_id ");
		sbQuery.append(" AND t621.c621_delete_fl = 'N' ");
		sbQuery.append(" AND t622.c622_patient_list_id = t6510.c622_patient_list_id ");
		sbQuery.append(" AND t6510.c614_study_site_id = t614.c614_study_site_id	 ");
		sbQuery.append(" AND t614.c614_site_id = decode('"+strSite+"','',t614.c614_site_id,'"+strSite+"') ");
		sbQuery.append(" AND t622.c613_study_period_id = t613.c613_study_period_id ");
		sbQuery.append(" AND t612.c611_study_id = t614.c611_study_id ");
		sbQuery.append(" AND t612.c612_study_list_id = t613.c612_study_list_id ");
		sbQuery.append(" AND t621.c621_patient_ide_no = decode('"+strPatient+"','',t621.c621_patient_ide_no,'"+strPatient+"') ");
		sbQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		if (!strCondition.equals(""))
		{
			sbQuery.append(" AND t614.C614_STUDY_SITE_ID in (select v621.C614_STUDY_SITE_ID");
			sbQuery.append(" from V621_PATIENT_STUDY_LIST v621 where ");
			sbQuery.append(strCondition);
			sbQuery.append(")");
		}
		sbQuery.append(" order by ID,PIDENO,QUERYID ");
		
		//log.debug("sbquery =="+sbQuery.toString());
		alQueriesSnapshot = dbCon.queryMultipleRecords(sbQuery.toString());
		return alQueriesSnapshot;
	} // End of loadQueryTreeList
	
	/*
	 * Below method is used to load the pending verification tree
	 * 
	 */
	public ArrayList loadPendVerificationTreeList(HashMap hmParam) throws AppError {
		ArrayList alVrfnSnapshot = new ArrayList();
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		//String strTxnId = GmCommonClass.parseNull((String)hmParam.get("STRTXNID"));
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.setLength(0);
		sbQuery.append(" SELECT 40009 sid, 'Pending Verification' stype, t614.c611_study_id studyid, ");
		sbQuery.append(" t614.c614_site_id id,  get_account_name (t614.c704_account_id) name,  ");
		sbQuery.append(" t613.C613_STUDY_PERIOD_ID sdsid, t613.c613_study_period_ds sds,  ");		
		sbQuery.append(" t621.c621_patient_ide_no pideno, t621.c621_patient_id pkey, ");
		sbQuery.append(" t612.c601_form_id formid, t613.C901_STUDY_PERIOD periodid, t613.C613_SEQ_NO seq, t612.c612_seq seqno,");
		sbQuery.append(" t612.c612_study_form_ds ||' - ' || get_form_name (t612.c601_form_id) fname");
		sbQuery.append(" ,t622.C622_PATIENT_LIST_ID plistid");
		sbQuery.append(" ,t622.C622_PATIENT_EVENT_NO eventno");
		sbQuery.append(" ,gm_pkg_cr_common.get_event_desc(t622.c613_study_period_id, t622.c601_form_id, t622.c621_patient_id, t622.C622_PATIENT_EVENT_NO) eventdesc");
		sbQuery.append(" FROM t621_patient_master t621, ");
		sbQuery.append(" t612_study_form_list t612, ");
		sbQuery.append(" t613_study_period t613,");
		sbQuery.append(" t614_study_site t614, ");
		sbQuery.append(" t622_patient_list t622 ");
		
		sbQuery.append(" WHERE t621.c621_delete_fl = 'N' ");
		sbQuery.append(" AND t612.c611_study_id = t621.c611_study_id ");
		sbQuery.append(" AND t621.c611_study_id = t614.c611_study_id ");
		sbQuery.append(" AND t612.c612_study_list_id = t613.c612_study_list_id");
		sbQuery.append(" AND t621.c621_patient_id = t622.c621_patient_id ");
		sbQuery.append(" AND t614.c611_study_id = decode('"+strStudy+"','',t614.c611_study_id,'"+strStudy+"') ");
		sbQuery.append(" AND t614.c614_site_id = decode('"+strSite+"','',t614.c614_site_id,'"+strSite+"') ");
		
		sbQuery.append(" AND t613.c613_study_period_id = t622.c613_study_period_id ");
		sbQuery.append(" AND t621.c704_account_id = t614.c704_account_id	 ");
		
		sbQuery.append(" AND NVL (t622.c622_verify_fl, 'N') = 'N' ");
		sbQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		sbQuery.append(" AND t621.c621_patient_ide_no = decode('"+strPatient+"','',t621.c621_patient_ide_no,'"+strPatient+"') ");
		sbQuery.append(" order by studyid, id, pideno, seq, seqno, formid  ");
		
		
		log.debug("sbquery =="+sbQuery.toString());
		alVrfnSnapshot = dbCon.queryMultipleRecords(sbQuery.toString());
		return alVrfnSnapshot;
	} // End of loadPendVerificationTreeList
	
	
	/*
	 * Below method is used to load the out of window tree
	 * 
	 */
	public ArrayList loadOutOfWindowTreeList(HashMap hmParam) throws AppError {
		ArrayList alOutOfWindowSnapshot = new ArrayList();
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		String strFormIds = GmCommonClass.getString("MISSING_FORMS");
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.setLength(0);
		sbQuery.append(" SELECT   40010 sid, 'Out of Window' stype, v621.c611_study_id studyid, ");
		sbQuery.append(" v621.c614_site_id id,  get_account_name (v621.c704_account_id) name,  ");
		sbQuery.append(" v621.C613_STUDY_PERIOD_ID sdsid, v621.c613_study_period_ds sds,  ");		
		sbQuery.append(" v621.c621_patient_ide_no pideno, v621.c621_patient_id pkey, ");
		sbQuery.append(" v621.C601_FORM_ID formid, v621.C901_STUDY_PERIOD periodid, v621.C613_SEQ_NO seq, v621.c612_seq seqno, ");
		sbQuery.append(" v621.c612_study_form_ds ||' - ' || get_form_name (v621.c601_form_id) fname");
		sbQuery.append(" FROM v621_patient_study_list v621 ");
		sbQuery.append(" WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN ( ");
		sbQuery.append(" SELECT t622.c621_patient_id, t622.c613_study_period_id ");
		sbQuery.append(" FROM t622_patient_list t622 ");
		sbQuery.append(" WHERE t622.c621_patient_id = v621.c621_patient_id AND NVL(t622.C622_DELETE_FL,'N')='N' ) ");
		sbQuery.append(" AND v621.c621_exp_date_final < SYSDATE ");
		sbQuery.append(" AND v621.c611_study_id = decode('"+strStudy+"','',v621.c611_study_id,'"+strStudy+"') ");
		sbQuery.append(" AND v621.c614_site_id = decode('"+strSite+"','',v621.c614_site_id,'"+strSite+"') ");
		
		sbQuery.append(" AND v621.c621_patient_id NOT IN ( ");
		sbQuery.append(" SELECT c621_patient_id FROM t622_patient_list WHERE c601_form_id = 15	 ");
		
		sbQuery.append(" AND c613_study_period_id = 91 AND NVL(C622_DELETE_FL,'N')='N' ) ");
		sbQuery.append(" AND v621.C621_PATIENT_IDE_NO = decode('"+strPatient+"','',v621.C621_PATIENT_IDE_NO,'"+strPatient+"') ");
		if(!strFormIds.equals(""))
		{
			sbQuery.append(" AND v621.c601_form_id not in ("+strFormIds+")");
		}
		//sbQuery.append(" AND v621.c621_patient_ide_no in ('030102', '030201')");
		//sbQuery.append(" AND v621.c614_site_id in ('01','02')");
		//sbQuery.append(" AND v621.c901_study_period in (6305, 6304)");
		sbQuery.append(" order by id, pideno, seq, seqno, formid  ");
		
		
		log.debug("alOutOfWindowSnapshot sbquery =="+sbQuery.toString());
		alOutOfWindowSnapshot = dbCon.queryMultipleRecords(sbQuery.toString());
		return alOutOfWindowSnapshot;
	} // End of loadOutOfWindowTreeList
	
	
	
	/*
	 * Below method is used to load the open edit check tree
	 * 
	 */
	public ArrayList loadOpenEditCheckTreeList(HashMap hmParam) throws AppError {
		ArrayList alOpenEditCheckSnapshot = new ArrayList();
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		//String strFormIds = GmCommonClass.getString("MISSING_FORMS");
		//String strTxnId = GmCommonClass.parseNull((String)hmParam.get("STRTXNID"));
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.setLength(0);
		
		
		
		sbQuery.append("SELECT 40012 sid, 'Open Edit Check' stype, t621.c611_study_id studyid,t614.c614_site_id id,  "); 
		sbQuery.append("get_account_name (t621.c704_account_id) name,t622.c613_study_period_id sdsid, ");
		sbQuery.append("T613.c613_study_period_ds sds,t621.c621_patient_ide_no pideno,t622.c621_patient_id pkey, ");
		sbQuery.append("t622.c601_form_id formid, t613.c901_study_period periodid,  t613.c613_seq_no seq,t612.c612_seq seqno, ");
		sbQuery.append("t612.c612_study_form_ds||' - ' || get_form_name (t622.c601_form_id) fname FROM t622_patient_list t622,  ");
		sbQuery.append("t623_patient_answer_list t623, t621_patient_master t621, ");
		sbQuery.append("t613_study_period t613,t6514_ec_incident t6514 , t601_form_master t601, t614_study_site t614, t612_study_form_list t612 ");
		sbQuery.append("WHERE t622.c621_patient_id = t623.c621_patient_id and t623.c623_edit_check_fl='Y'  and   ");
		sbQuery.append("t621.c704_account_id=t614.c704_account_id ");
		sbQuery.append(" and t622.c621_patient_id = t621.c621_patient_id ");
		sbQuery.append(" and t613.c613_study_period_id = t622.c613_study_period_id ");
		sbQuery.append(" and t6514.c621_patient_id = t622.c621_patient_id ");
		sbQuery.append(" and t6514.c623_patient_ans_list_id = t623.c623_patient_ans_list_id ");
		sbQuery.append(" and t6514.c6514_status = 10 and t621.c621_patient_ide_no=decode('"+strPatient+"','',t621.C621_PATIENT_IDE_NO,'"+strPatient+"') ");
		sbQuery.append(" and t601.c601_form_id = t622.c601_form_id ");
		sbQuery.append(" and t6514.c6514_void_fl is null ");
		sbQuery.append(" and t622.c622_patient_list_id=t623.c622_patient_list_id  ");
		sbQuery.append(" and t621.c611_study_id=decode('"+strStudy+"','',t614.c611_study_id,'"+strStudy+"')  ");
		sbQuery.append(" and t614.c614_site_id=decode('"+strSite+"','',t614.c614_site_id,'"+strSite+"')  ");
		sbQuery.append(" and t612.c612_study_list_id=t613.c612_study_list_id and t621.c611_study_id=t614.c611_study_id  and t614.c614_void_fl is null ");
		sbQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		sbQuery.append(" order by id, pideno, seq,seqno, formid ");
		
		log.debug("al sbquery =="+sbQuery.toString());
		alOpenEditCheckSnapshot = dbCon.queryMultipleRecords(sbQuery.toString());
		return alOpenEditCheckSnapshot;
	} // End of loadMissingFormsTreeList
	
	
	/*
	 * Below method is used to load the Missing form tree
	 * 
	 */
	public ArrayList loadMissingFormsTreeList(HashMap hmParam) throws AppError {
		ArrayList alMissingFormSnapshot = new ArrayList();
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		String strFormIds = GmCommonClass.getString("MISSING_FORMS");
		//String strTxnId = GmCommonClass.parseNull((String)hmParam.get("STRTXNID"));
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.setLength(0);
		sbQuery.append(" SELECT   40011 sid, 'Missing Forms' stype, v621.c611_study_id studyid, ");
		sbQuery.append(" v621.c614_site_id id,  get_account_name (v621.c704_account_id) name,  ");
		sbQuery.append(" v621.C613_STUDY_PERIOD_ID sdsid, v621.c613_study_period_ds sds,  ");		
		sbQuery.append(" v621.c621_patient_ide_no pideno, v621.c621_patient_id pkey, ");
		sbQuery.append(" v621.C601_FORM_ID formid, v621.C901_STUDY_PERIOD periodid, v621.C613_SEQ_NO seq, v621.c612_seq seqno, ");
		sbQuery.append(" v621.c612_study_form_ds ||' - ' || get_form_name (v621.c601_form_id) fname");
		sbQuery.append(" FROM v621_patient_study_list v621 ");
		sbQuery.append(" WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN ( ");
		sbQuery.append(" SELECT t622.c621_patient_id, t622.c613_study_period_id ");
		sbQuery.append(" FROM t622_patient_list t622 ");
		sbQuery.append(" WHERE t622.c621_patient_id = v621.c621_patient_id) ");
		sbQuery.append(" AND v621.c621_exp_date_final < SYSDATE ");
		sbQuery.append(" AND v621.c611_study_id = decode('"+strStudy+"','',v621.c611_study_id,'"+strStudy+"') ");
		sbQuery.append(" AND v621.c614_site_id = decode('"+strSite+"','',v621.c614_site_id,'"+strSite+"') ");
		
		sbQuery.append(" AND v621.c621_patient_id NOT IN ( ");
		sbQuery.append(" SELECT c621_patient_id FROM t622_patient_list WHERE c601_form_id = 15	 ");
		
		sbQuery.append(" AND c613_study_period_id = 91 AND NVL(C622_DELETE_FL,'N')='N' ) ");
		sbQuery.append(" AND v621.C621_PATIENT_IDE_NO = decode('"+strPatient+"','',v621.C621_PATIENT_IDE_NO,'"+strPatient+"') ");
		if(!strFormIds.equals(""))
		{
			sbQuery.append(" AND v621.c601_form_id in ("+strFormIds+")");
		}
		//sbQuery.append(" AND v621.c621_patient_ide_no in ('030102', '030201')");
		//sbQuery.append(" AND v621.c614_site_id in ('01','02')");
		//sbQuery.append(" AND v621.c901_study_period in (6305, 6304)");
		sbQuery.append(" order by id, pideno, seq, seqno, formid  ");
		
		
		log.debug("alMissingFormSnapshot sbquery =="+sbQuery.toString());
		alMissingFormSnapshot = dbCon.queryMultipleRecords(sbQuery.toString());
		return alMissingFormSnapshot;
	} // End of loadMissingFormsTreeList
	/*
	 * 
	 * Below method is called from the GmSnapShotAction (Delete in future)
	 */
	public ArrayList loadSubTreeList(HashMap hmParam) throws AppError {
		ArrayList alSubSnapshot = new ArrayList();
		GmPatientTrackingBean gmPatientTrackingBean = new GmPatientTrackingBean();
		
		String strTreeId = "";
		String strContext = "";
		String strId = "";
		int intIndex = 0;
		
		String strStudy = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSite = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatient = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITION"));
		
		strTreeId = (String)hmParam.get("TREEID");
		intIndex = strTreeId.indexOf("_");
		//log.debug("strTreeId =========== "+intIndex);
		strContext = strTreeId.substring(0, intIndex);
		//log.debug("strContext =========== "+strContext);
		strId = strTreeId.substring(intIndex+1);
		//log.debug("strId =========== "+strId);
		if(strContext.equals("SITE"))
		{
			hmParam.put("STUDYID", strStudy);
	        hmParam.put("SITEID", strSite);
	        alSubSnapshot = GmCommonClass.parseNullArrayList(gmPatientTrackingBean.loadPatientTrackList(hmParam, strCondition));
		}
		return alSubSnapshot;
	} // End of loadSubTreeList
	/*
	 * 
	 * Method is used for loading the xml data from Report and Transactions
	 */
	public String getXmlTreeData(HashMap hmParamV) throws AppError {
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		HashMap hmTree = new HashMap();
		ArrayList alParam = (ArrayList)hmParamV.get("RLIST");
		String strTemplate = (String)hmParamV.get("TEMPLATE");
		String strTemplatePath = (String)hmParamV.get("TEMPLATEPATH");
		String strTreeId = GmCommonClass.parseNull((String)hmParamV.get("TREEID"));
		hmTree.put("TREEID", strTreeId);
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setDataMap("HMTREE", hmTree);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}
	
	/*
	 * 
	 * Method to get the xml string based on the different context for snapshot
	 * 
	 */
	public String loadContextTreeXml(HashMap hmParam) throws AppError {
		HashMap hmValues = new HashMap();
		ArrayList alQuerySnapshot = new ArrayList();
		ArrayList  alAnsweredSnapshot = new ArrayList();
		ArrayList alPendVrfnSnapshot = new ArrayList();
		ArrayList alOutOfWinSnapshot = new ArrayList();
		ArrayList alMissFormSnapshot = new ArrayList();
		ArrayList alOpenEditCheckSnapshot = new ArrayList();
		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("SESSSTUDYID"));
		String strSiteId = GmCommonClass.parseNull((String)hmParam.get("SESSSITEID"));
		String strPatientId = GmCommonClass.parseNull((String)hmParam.get("SESSPATIENTID"));
		String strAccessLvl = GmCommonClass.parseNull((String)hmParam.get("SESSACCLVL"));
		String strTemplate = "";
		String strXmlSnapshotData = "";
		
		//SimpleDateFormat strFormatter1 = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss"); 
		//String strStartTime = (strFormatter1.format(new java.util.Date()));
		
		if(!strStudyId.equals(""))
		{
			alQuerySnapshot = loadQueryTreeList(hmParam);
	    	alPendVrfnSnapshot = loadPendVerificationTreeList(hmParam);
	    	alOutOfWinSnapshot = loadOutOfWindowTreeList(hmParam);
	    	alAnsweredSnapshot = loadAnsweredTreeList(hmParam);
	    	alMissFormSnapshot = loadMissingFormsTreeList(hmParam);
	    	alOpenEditCheckSnapshot = loadOpenEditCheckTreeList(hmParam);
		}
		//String strEndTime = (strFormatter1.format(new java.util.Date()));
		
		//log.debug("Start Time===="+strStartTime);
		//log.debug("End Time===="+strEndTime);
		
		strTemplate = strPatientId.equals("")?strSiteId.equals("")?strStudyId.equals("")?"GmSnapshotStudyTree.vm":"GmSnapshotStudyTree.vm":"GmSnapshotSiteTree.vm":"GmSnapshotPatientTree.vm";
		hmValues.put("TEMPLATE",strTemplate);
		hmValues.put("TEMPLATEPATH","/clinical/templates/");
		hmValues.put("ALQUERYSNAPSHOT",alQuerySnapshot);
		hmValues.put("ALPENDVRFNSNAPSHOT", alPendVrfnSnapshot);
		hmValues.put("ALOUTOFWINSNAPSHOT", alOutOfWinSnapshot);
		hmValues.put("ALANSWEREDSNAPSHOT",alAnsweredSnapshot);
		hmValues.put("ALMISSFORMSNAPSHOT",alMissFormSnapshot);
		hmValues.put("ALOPENEDITCHECKSNAPSHOT",alOpenEditCheckSnapshot);
		hmValues.put("ACCLVL", strAccessLvl);
		//strStartTime = (strFormatter1.format(new java.util.Date()));
		
		//New changes for Open Queries
		HashMap hmDetailedOpenQues = new HashMap();
		hmDetailedOpenQues = getOpen_AnsQueryList(alQuerySnapshot);
		hmValues.put("OPENQUERYMAP", hmDetailedOpenQues);
		
		//New changes for Answered Query
		HashMap hmDetailedAnsQues = new HashMap();
		hmDetailedAnsQues = getOpen_AnsQueryList(alAnsweredSnapshot);
		hmValues.put("ANSQUERYMAP", hmDetailedAnsQues);
		
		//New changes for PendVerification
		HashMap hmDetailedPendVrfn = new HashMap();
		hmDetailedPendVrfn = getPendVrfn_OutofWinList(alPendVrfnSnapshot);
		hmValues.put("PENDVRFNMAP", hmDetailedPendVrfn);
		//New Changes for Out of window
		HashMap hmDetailedOutOfWin = new HashMap();
		hmDetailedOutOfWin = getPendVrfn_OutofWinList(alOutOfWinSnapshot);
		hmValues.put("OUTOFWINMAP", hmDetailedOutOfWin);
		//Changes for Missing Form
		HashMap hmDetailedMissForm = new HashMap();
		hmDetailedMissForm = getPendVrfn_OutofWinList(alMissFormSnapshot);
		hmValues.put("MISSFORMMAP", hmDetailedMissForm);
		
		//Changes for Open Edit Check Form
		HashMap hmDetailedEditCheck = new HashMap();
		hmDetailedEditCheck = getPendVrfn_OutofWinList(alOpenEditCheckSnapshot);
		hmValues.put("EDITCHECKMAP", hmDetailedEditCheck);
		//end
		
		strXmlSnapshotData = getXmlSnapShotTreeData(hmValues);
		//log.debug("Snapshot XML====="+strXmlSnapshotData);
		//strEndTime = (strFormatter1.format(new java.util.Date()));
		//log.debug("Start Time==Generate Output=="+strStartTime);
		//log.debug("End Time====Generate Output===="+strEndTime);
		return strXmlSnapshotData;
	}
/*
 * 
 * Method used to load the data only for snapshot
 */	
	public String getXmlSnapShotTreeData(HashMap hmParamV) throws AppError {
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		
		HashMap hmAccess = new HashMap();
		
		HashMap hmDetailedOpenQues = new HashMap();
		HashMap hmDetailedAnsQues = new HashMap();
		HashMap hmDetailedPendVrfn = new HashMap();
		HashMap hmDetailOutofWin = new HashMap();	
		HashMap hmDetailMissForm = new HashMap();	
		HashMap hmDetailEditCheck = new HashMap();
		
		HashMap hmQpenQueryKeyCount = new HashMap();
		HashMap hmAnsQueryKeyCount = new HashMap();
		HashMap hmOutofWinKeyCount = new HashMap();
		HashMap hmPendVrfnKeyCount = new HashMap();
		HashMap hmMissFormKeyCount = new HashMap();
		HashMap hmEditCheckKeyCount = new HashMap();
		
		ArrayList alDetailedOpenQuery = new ArrayList();
		ArrayList alDetailedAnsQuery = new ArrayList();
		ArrayList alDetailedOutWin = new ArrayList();
		ArrayList alDetailedPendVrfn = new ArrayList();
		ArrayList alDetailedMissForm = new ArrayList();
		ArrayList alDetailedEditCheck = new ArrayList();
		//ArrayList alQuerySnapshot = (ArrayList)hmParamV.get("ALQUERYSNAPSHOT");
		//ArrayList alPendVrfnSnapshot = (ArrayList)hmParamV.get("ALPENDVRFNSNAPSHOT");
		//ArrayList alOutOfWinSnapshot = (ArrayList)hmParamV.get("ALOUTOFWINSNAPSHOT");
		//ArrayList alAnsweredSnapshot = (ArrayList)hmParamV.get("ALANSWEREDSNAPSHOT");
		
		String strTemplate = (String)hmParamV.get("TEMPLATE");
		String strTemplatePath = (String)hmParamV.get("TEMPLATEPATH");
		String strAccessLvl = GmCommonClass.parseNull((String)hmParamV.get("ACCLVL"));
		
		hmAccess.put("ACCLVL", strAccessLvl);
		
		String strSID ="";
		String strSType = "";
		
		//Code for Open Queries
		hmDetailedOpenQues =(HashMap)hmParamV.get("OPENQUERYMAP");
		hmQpenQueryKeyCount = (HashMap)hmDetailedOpenQues.get("HMKEYCOUNT");
		alDetailedOpenQuery = (ArrayList)hmDetailedOpenQues.get("ALLIST");
		String strOpenQueryCnt = (String)hmDetailedOpenQues.get("TOTCNT");
		strSID = (String)hmDetailedOpenQues.get("SID");
		strSType = (String)hmDetailedOpenQues.get("STYPE");
		
		hmAccess.put("OPENQUERYCNT", strOpenQueryCnt);
		hmAccess.put("OPENSID", strSID);
		hmAccess.put("OPENSTYPE", strSType);
		
		//Code for Answered Queries
		hmDetailedAnsQues =(HashMap)hmParamV.get("ANSQUERYMAP");
		hmAnsQueryKeyCount = (HashMap)hmDetailedAnsQues.get("HMKEYCOUNT");
		alDetailedAnsQuery = (ArrayList)hmDetailedAnsQues.get("ALLIST");
		String strAnsQueryCnt = (String)hmDetailedAnsQues.get("TOTCNT");
		strSID = (String)hmDetailedAnsQues.get("SID");
		strSType = (String)hmDetailedAnsQues.get("STYPE");
		
		hmAccess.put("ANSQUERYCNT", strAnsQueryCnt);
		hmAccess.put("ANSSID", strSID);
		hmAccess.put("ANSSTYPE", strSType);
		
		//Code for pending verification
		hmDetailedPendVrfn = (HashMap)hmParamV.get("PENDVRFNMAP");
		hmPendVrfnKeyCount = (HashMap)hmDetailedPendVrfn.get("HMKEYCOUNT");
		alDetailedPendVrfn = (ArrayList)hmDetailedPendVrfn.get("ALLIST");
		String strPendVrfnCnt = (String)hmDetailedPendVrfn.get("TOTCNT");
		strSID = (String)hmDetailedPendVrfn.get("SID");
		strSType = (String)hmDetailedPendVrfn.get("STYPE");
		
		hmAccess.put("PENDVRFNCNT", strPendVrfnCnt);
		hmAccess.put("PENDSID", strSID);
		hmAccess.put("PENDSTYPE", strSType);
		
		//Code for out of window
		
		hmDetailOutofWin = (HashMap)hmParamV.get("OUTOFWINMAP");
		hmOutofWinKeyCount = (HashMap)hmDetailOutofWin.get("HMKEYCOUNT");
		alDetailedOutWin = (ArrayList)hmDetailOutofWin.get("ALLIST");
		String strOutofWinCnt = (String)hmDetailOutofWin.get("TOTCNT");
		strSID = (String)hmDetailOutofWin.get("SID");
		strSType = (String)hmDetailOutofWin.get("STYPE");
				
		hmAccess.put("OUTOFWINCNT", strOutofWinCnt);
		hmAccess.put("OUTSID", strSID);
		hmAccess.put("OUTSTYPE", strSType);
		
		//Code for missing forms
		hmDetailMissForm = (HashMap)hmParamV.get("MISSFORMMAP");
		hmMissFormKeyCount = (HashMap)hmDetailMissForm.get("HMKEYCOUNT");
		alDetailedMissForm = (ArrayList)hmDetailMissForm.get("ALLIST");
		String strMissFormCnt = (String)hmDetailMissForm.get("TOTCNT");
		strSID = (String)hmDetailMissForm.get("SID");
		strSType = (String)hmDetailMissForm.get("STYPE");
				
		hmAccess.put("MISSFORMCNT", strMissFormCnt);
		hmAccess.put("MISSSID", strSID);
		hmAccess.put("MISSSTYPE", strSType);
		
		
		//Code for open edit check
		hmDetailEditCheck = (HashMap)hmParamV.get("EDITCHECKMAP");
		hmEditCheckKeyCount = (HashMap)hmDetailEditCheck.get("HMKEYCOUNT");
		alDetailedEditCheck = (ArrayList)hmDetailEditCheck.get("ALLIST");
		String strEditCheckCnt = (String)hmDetailEditCheck.get("TOTCNT");
		strSID = (String)hmDetailEditCheck.get("SID");
		strSType = (String)hmDetailEditCheck.get("STYPE");
				
		hmAccess.put("EDITCHECKCNT", strEditCheckCnt);
		hmAccess.put("EDITCHECKSID", strSID);
		hmAccess.put("EDITCHECKSTYPE", strSType);
		
		
		templateUtil.setDataList("alDetailedOpenQuery", alDetailedOpenQuery);
		templateUtil.setDataMap("HMOPENQUERYKEYCOUNT", hmQpenQueryKeyCount);
		
		templateUtil.setDataList("alDetailedAnsQuery", alDetailedAnsQuery);
		templateUtil.setDataMap("HMANSQUERYKEYCOUNT", hmAnsQueryKeyCount);
		//templateUtil.setDataList("alPendVrfnSnapshot", alPendVrfnSnapshot);
		//templateUtil.setDataList("alOutOfWinSnapshot", alOutOfWinSnapshot);
		templateUtil.setDataList("alDetailedPendVrfn", alDetailedPendVrfn);
		templateUtil.setDataMap("HMPENDVRFNKEYCOUNT", hmPendVrfnKeyCount);
		
		templateUtil.setDataList("alDetailedOutWin", alDetailedOutWin);
		templateUtil.setDataMap("HMOUTWINKEYCOUNT", hmOutofWinKeyCount);
		
		templateUtil.setDataList("alDetailedMissForm", alDetailedMissForm);
		templateUtil.setDataMap("HMMISSFORMKEYCOUNT", hmMissFormKeyCount);
		
		templateUtil.setDataList("alDetailedEditCheck", alDetailedEditCheck);
		templateUtil.setDataMap("HMEDITCHECKKEYCOUNT", hmEditCheckKeyCount);
		
		templateUtil.setDataMap("HMACCESS", hmAccess);
		
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}
	/*
	 * Method to split the Out of window arraylist to different arraylist;
	 * 
	 * */
	public HashMap getPendVrfn_OutofWinList (ArrayList alSnapshot) throws AppError
	{
		HashMap hmReturn = new HashMap();
		HashMap hmSnapshot = new HashMap();
		
		HashMap hmTempSites = new HashMap();
		
		HashMap hmFinalSites = new HashMap();
		HashMap hmFinalPatient = new HashMap();
		HashMap hmFinalTP = new HashMap();
		HashMap hmFinalForm = new HashMap();
		
		HashMap hmKeyCount = new HashMap();
		
		ArrayList alSiteList = new ArrayList();
		ArrayList alPatientList = new ArrayList();
		ArrayList alTimePointList = new ArrayList();
		ArrayList alFormList = new ArrayList();
		
		String strSID = "";
		String strStype = "";
		String strStudy = "";
		String strSiteId = "";
		String strSiteName = "";
		String strPkey = "";
		String strPatientIdeNo = "";
		String strSDSId = "";
		String strSDS = "";
		String strFormId = "";
		String strPeriodId = "";
		String strFormname = "";
		String strSiteKey = "";
		String strPTKey = "";
		String strTPKey = "";
		String strFormKey = "";
		String strPListId = "";
		String strEventNo = "";
		String strEventDesc = "";
		
		String strPrevSiteKey = "";
		String strPrevPTKey = "";
		String strPrevTPKey = "";
		String strPrevFormKey = "";
		
		int iTotalCnt = 0;
		int iTPCnt = 0;
		int iPTCnt = 0;
		int iSTCnt = 0;
		//log.debug("alOutOfWinSnapshot=========="+alOutOfWinSnapshot.size());
		Iterator itr = alSnapshot.iterator();
		while(itr.hasNext())
		{
			iTotalCnt++;
			hmSnapshot = (HashMap)itr.next();
			strSiteId = (String)hmSnapshot.get("ID");
			strStudy = (String)hmSnapshot.get("STUDYID");
			strSID = (String)hmSnapshot.get("SID");
			strStype = (String)hmSnapshot.get("STYPE");
			strSiteName = (String)hmSnapshot.get("NAME");
			strPkey = (String)hmSnapshot.get("PKEY");
			strPatientIdeNo = (String)hmSnapshot.get("PIDENO");
			strSDSId = (String)hmSnapshot.get("SDSID");
			strSDS = (String)hmSnapshot.get("SDS");
			strPeriodId = (String)hmSnapshot.get("PERIODID");
			strFormId = (String)hmSnapshot.get("FORMID");
			strFormname = (String)hmSnapshot.get("FNAME");
			strPListId = GmCommonClass.parseNull((String)hmSnapshot.get("PLISTID"));
			strEventNo = GmCommonClass.parseNull((String)hmSnapshot.get("EVENTNO"));
			strEventDesc = GmCommonClass.parseNull((String)hmSnapshot.get("EVENTDESC"));
			
			
			strSiteKey = strSiteId+"_"+strSID;
			strPTKey = strPkey+"_"+strSiteKey;
			strTPKey = strPeriodId+"_"+strPTKey;
			
			if(strEventNo.equals(""))
				strFormKey = strFormId+"_"+strTPKey;
			else
				strFormKey = strEventNo+"_"+strFormId+"_"+strTPKey;
			
			//First iteration time
			if (iTotalCnt == 1)
			{
				iTPCnt++;
				iPTCnt++;
				iSTCnt++;
				hmFinalForm.put("FORMID", strFormId);
				hmFinalForm.put("FORMNAME", strFormname);
				//hmFinalForm.put("FORMCNT", String.valueOf(iFormCnt));
				hmFinalForm.put("FORMKEY", strFormKey);
				hmFinalForm.put("PLISTID", strPListId);
				hmFinalForm.put("EVENTNO", strEventNo);
				hmFinalForm.put("EVENTDESC", strEventDesc);
				hmFinalForm.put("SDSID", strSDSId);
				
				alFormList.add(hmFinalForm);
				
				hmFinalTP.put(strTPKey, alFormList);
				//hmFinalTP.put("TPCNT", String.valueOf(iTPCnt));
				hmFinalTP.put("PERIODID", strPeriodId);
				hmFinalTP.put("SDS", strSDS);
				
				
				alTimePointList.add(hmFinalTP);
				
				hmFinalPatient.put(strPTKey,alTimePointList);
				//hmFinalPatient.put("PTCNT",String.valueOf(iPTCnt));
				hmFinalPatient.put("PKEY",strPkey);
				hmFinalPatient.put("PIDENO",strPatientIdeNo);
				
				alPatientList.add(hmFinalPatient);
				
				hmFinalSites.put(strSiteKey,alPatientList);
				//hmFinalSites.put("STCNT",String.valueOf(iSTCnt));
				hmFinalSites.put("ID",strSiteId);
				hmFinalSites.put("NAME",strSiteName);
				
				alSiteList.add(hmFinalSites);
				
				hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
				hmKeyCount.put("PTCNT"+strPTKey,String.valueOf(iPTCnt));
				hmKeyCount.put("TPCNT"+strTPKey, String.valueOf(iTPCnt));
			}
			if(strPrevSiteKey.equals(strSiteKey))
			{
				iSTCnt++;
				if(strPrevPTKey.equals(strPTKey))
				{
					iPTCnt++;
					if(strPrevTPKey.equals(strTPKey))
					{
						iTPCnt++;
						//open edit check(40012) should not --
						if(strPrevFormKey.equals(strFormKey) )
						{
							if(!strSID.equals("40012")){
								iSTCnt--;
								iPTCnt--;
								iTPCnt--;
							}else if (strSID.equals("40012")){
								if(hmKeyCount.containsKey("TPCNT"+strTPKey))
								{
									hmKeyCount.put("TPCNT"+strTPKey, String.valueOf(iTPCnt));
								}
								if(hmKeyCount.containsKey("PTCNT"+strPTKey))
								{
									hmKeyCount.put("PTCNT"+strPTKey,String.valueOf(iPTCnt));
								}
								if(hmKeyCount.containsKey("STCNT"+strSiteKey))
								{
									hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
								}
							}
							
						}
						else 
						{
							alFormList = addFormList(strFormId, strFormname,strFormKey, strPListId,strEventNo,strEventDesc, strSDSId, alFormList);
							
							if(hmKeyCount.containsKey("TPCNT"+strTPKey))
							{
								hmKeyCount.put("TPCNT"+strTPKey, String.valueOf(iTPCnt));
							}
							if(hmKeyCount.containsKey("PTCNT"+strPTKey))
							{
								hmKeyCount.put("PTCNT"+strPTKey,String.valueOf(iPTCnt));
							}
							if(hmKeyCount.containsKey("STCNT"+strSiteKey))
							{
								hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
							}
						}
					}
					else
					{
						alFormList = new ArrayList();
						alFormList = addFormList(strFormId, strFormname,strFormKey, strPListId,strEventNo,strEventDesc, strSDSId, alFormList);
						
						iTPCnt = 1;
						alTimePointList = addTimePointList(strTPKey, strPeriodId, strSDS, strSDSId, iTPCnt, alFormList, alTimePointList, hmKeyCount);
						hmKeyCount.put("PTCNT"+strPTKey,String.valueOf(iPTCnt));
						hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
						//hmFinalPatient.put("PTCNT",String.valueOf(iPTCnt));
						//hmFinalSites.put("STCNT",String.valueOf(iSTCnt));
					}
				}
				else
				{
					alFormList = new ArrayList();
					alFormList = addFormList(strFormId, strFormname,strFormKey, strPListId,strEventNo,strEventDesc, strSDSId, alFormList);
					
					iTPCnt = 1;
					alTimePointList = new ArrayList();
					alTimePointList = addTimePointList(strTPKey, strPeriodId, strSDS, strSDSId, iTPCnt, alFormList, alTimePointList,hmKeyCount);
					
					iPTCnt = 1;
					alPatientList = addPatientList(strPTKey, strPkey, strPatientIdeNo, iPTCnt, alTimePointList, alPatientList, hmKeyCount);
					
					hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
				}
			}
			else
			{
				if(!strPrevSiteKey.equals(strSiteKey) & iTotalCnt > 1)
				{
					alFormList = new ArrayList();
					alFormList = addFormList(strFormId, strFormname,strFormKey, strPListId,strEventNo,strEventDesc, strSDSId, alFormList);		
					
					
					iTPCnt = 1;
					alTimePointList = new ArrayList();
					alTimePointList = addTimePointList(strTPKey, strPeriodId, strSDS, strSDSId, iTPCnt, alFormList, alTimePointList,hmKeyCount);
					
					iPTCnt = 1;
					alPatientList  = new ArrayList();
					alPatientList = addPatientList(strPTKey, strPkey, strPatientIdeNo, iPTCnt, alTimePointList, alPatientList,hmKeyCount);
					
					iSTCnt = 1;
					hmTempSites = new HashMap();
					hmTempSites.put(strSiteKey,alPatientList);
					//hmTempSites.put("STCNT",String.valueOf(iSTCnt));
					hmTempSites.put("ID",strSiteId);
					hmTempSites.put("NAME",strSiteName);
					hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
					alSiteList.add(hmTempSites);
				}
			}
			strPrevSiteKey = strSiteKey;
			strPrevPTKey = strPTKey;
			strPrevTPKey = strTPKey;
			strPrevFormKey = strFormKey;
		}
		
		
		hmReturn.put("ALLIST", alSiteList);
		hmReturn.put("TOTCNT", String.valueOf(iTotalCnt));
		hmReturn.put("SID", strSID);
		hmReturn.put("STYPE", strStype);
		hmReturn.put("HMKEYCOUNT", hmKeyCount);
		//log.debug("alSiteList======1===="+alSiteList);
		//log.debug("hmKey======1===="+hmKeyCount);
		return hmReturn;
	}
	/*
	 * Method to split the Out of window arraylist to different arraylist;
	 * 
	 * */
	public HashMap getOpen_AnsQueryList (ArrayList alSnapshot) throws AppError
	{
		HashMap hmReturn = new HashMap();
		HashMap hmSnapshot = new HashMap();
		
		HashMap hmTempSites = new HashMap();
		
		HashMap hmFinalSites = new HashMap();
		HashMap hmFinalPatient = new HashMap();
		HashMap hmFinalQuery = new HashMap();
		
		HashMap hmKeyCount = new HashMap();
		
		ArrayList alSiteList = new ArrayList();
		ArrayList alPatientList = new ArrayList();
		ArrayList alQueryList = new ArrayList();
		
		String strSID = "";
		String strStype = "";
		String strStudy = "";
		String strSiteId = "";
		String strSiteName = "";
		String strPkey = "";
		String strPatientIdeNo = "";
		String strSDSId = "";
		String strSDS = "";
		String strPListId = "";
		String strPtAnsListId = "";
		String strQueryId = "";
		String strQueryLvlId = "";
		String strQueryDesc = "";
		
		String strSiteKey = "";
		String strPTKey = "";
		String strQueryKey = "";
		
		String strPrevSiteKey = "";
		String strPrevPTKey = "";
		String strPrevQueryKey = "";
		
		int iTotalCnt = 0;
		int iPTCnt = 0;
		int iSTCnt = 0;
		//log.debug("alOutOfWinSnapshot=========="+alOutOfWinSnapshot.size());
		Iterator itr = alSnapshot.iterator();
		while(itr.hasNext())
		{
			iTotalCnt++;
			hmSnapshot = (HashMap)itr.next();
			strSID = (String)hmSnapshot.get("SID");
			strStype = (String)hmSnapshot.get("STYPE");
			strSiteId = (String)hmSnapshot.get("ID");
			strStudy = (String)hmSnapshot.get("STUDYID");
			strSiteName = (String)hmSnapshot.get("NAME");
			strPkey = (String)hmSnapshot.get("PATIENTID");
			strPatientIdeNo = (String)hmSnapshot.get("PIDENO");
			strSDSId = (String)hmSnapshot.get("SDSID");
			strSDS = (String)hmSnapshot.get("SDS");
			strPListId = GmCommonClass.parseNull((String)hmSnapshot.get("PATIENTLISTID"));
			strPtAnsListId = GmCommonClass.parseNull((String)hmSnapshot.get("PTANSLISTID"));
			strQueryId =  GmCommonClass.parseNull((String)hmSnapshot.get("QUERYID"));
			strQueryLvlId =GmCommonClass.parseNull((String)hmSnapshot.get("QUERYLEVELID"));
			strQueryDesc = GmCommonClass.parseNull((String)hmSnapshot.get("SHORTDESC"));
			
			strSiteKey = strSiteId+"_"+strSID;
			strPTKey = strPkey+"_"+strSiteKey;
			strQueryKey = strQueryId+"_"+strPTKey;
				
			//First iteration time
			if (iTotalCnt == 1)
			{
				iPTCnt++;
				iSTCnt++;
				hmFinalQuery.put("QUERYID", strQueryId);
				hmFinalQuery.put("QUERYLEVELID", strQueryLvlId);
				hmFinalQuery.put("SHORTDESC", strQueryDesc);
				hmFinalQuery.put("PATIENTLISTID",strPListId);
				hmFinalQuery.put("PTANSLISTID",strPtAnsListId);
				alQueryList.add(hmFinalQuery);
				
				
				hmFinalPatient.put(strPTKey,alQueryList);
				hmFinalPatient.put("PKEY",strPkey);
				hmFinalPatient.put("PIDENO",strPatientIdeNo);
				
				alPatientList.add(hmFinalPatient);
				
				hmFinalSites.put(strSiteKey,alPatientList);
				hmFinalSites.put("ID",strSiteId);
				hmFinalSites.put("NAME",strSiteName);
				
				alSiteList.add(hmFinalSites);
				
				hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
				hmKeyCount.put("PTCNT"+strPTKey,String.valueOf(iPTCnt));				
			}
			if(strPrevSiteKey.equals(strSiteKey))
			{
				iSTCnt++;
				if(strPrevPTKey.equals(strPTKey))
				{
					iPTCnt++;
					
					if(strPrevQueryKey.equals(strQueryKey))
					{
						iSTCnt--;
						iPTCnt--;						
					}
					else
					{
						alQueryList = addQueryList(strQueryId, strQueryLvlId, strQueryDesc, strQueryKey, strPListId,strPtAnsListId, alQueryList);
						if(hmKeyCount.containsKey("PTCNT"+strPTKey))
						{
							hmKeyCount.put("PTCNT"+strPTKey,String.valueOf(iPTCnt));
						}
						if(hmKeyCount.containsKey("STCNT"+strSiteKey))
						{
							hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
						}
					}
										
				}
				else
				{
					alQueryList = new ArrayList();
					alQueryList = addQueryList(strQueryId, strQueryLvlId, strQueryDesc, strQueryKey, strPListId,strPtAnsListId, alQueryList);
					
					iPTCnt = 1;
					alPatientList = addPatientList(strPTKey, strPkey, strPatientIdeNo, iPTCnt, alQueryList, alPatientList, hmKeyCount);
					
					hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
				}
			}
			else
			{
				if(!strPrevSiteKey.equals(strSiteKey) & iTotalCnt > 1)
				{
					alQueryList = new ArrayList();
					alQueryList = addQueryList(strQueryId, strQueryLvlId, strQueryDesc, strQueryKey, strPListId,strPtAnsListId, alQueryList);	
					
					
					iPTCnt = 1;
					alPatientList  = new ArrayList();
					alPatientList = addPatientList(strPTKey, strPkey, strPatientIdeNo, iPTCnt, alQueryList, alPatientList,hmKeyCount);
					
					iSTCnt = 1;
					hmTempSites = new HashMap();
					hmTempSites.put(strSiteKey,alPatientList);
					//hmTempSites.put("STCNT",String.valueOf(iSTCnt));
					hmTempSites.put("ID",strSiteId);
					hmTempSites.put("NAME",strSiteName);
					hmKeyCount.put("STCNT"+strSiteKey,String.valueOf(iSTCnt));
					alSiteList.add(hmTempSites);
				}
			}
			strPrevSiteKey = strSiteKey;
			strPrevPTKey = strPTKey;
			strPrevQueryKey = strQueryKey;
		}
		
		
		hmReturn.put("ALLIST", alSiteList);
		hmReturn.put("TOTCNT", String.valueOf(iTotalCnt));
		hmReturn.put("SID", strSID);
		hmReturn.put("STYPE", strStype);
		hmReturn.put("HMKEYCOUNT", hmKeyCount);
		//log.debug("alSiteList======1===="+alSiteList);
		//log.debug("hmKey======1===="+hmKeyCount);
		return hmReturn;
	}
	/*
	 * Method to add the value in patient list and return it 
	 * 
	 * */
	public ArrayList addPatientList(String strPTKey, String strPkey, String strPatientIdeNo, int iPTCnt, ArrayList alTimePointList, ArrayList alList,HashMap hmKeyCount) throws AppError
	{
		HashMap hmTempPatients = new HashMap();
		hmTempPatients.put(strPTKey, alTimePointList);
		//hmTempPatients.put("PTCNT",String.valueOf(iPTCnt));
		hmTempPatients.put("PKEY",strPkey);
		hmTempPatients.put("PIDENO",strPatientIdeNo);
		
		hmKeyCount.put("PTCNT"+strPTKey,String.valueOf(iPTCnt));
		
		alList.add(hmTempPatients);
		return alList;
	}
	/*
	 * Method to add the value in time point list and return it 
	 * 
	 * */
	public ArrayList addTimePointList(String strTPKey, String strPeriodId, String strSDS, String strSDSId, int iTPCnt, ArrayList alFormList, ArrayList alList, HashMap hmKeyCount) throws AppError
	{
		HashMap hmTempTPs = new HashMap();
		hmTempTPs.put(strTPKey, alFormList);
		//hmTempTPs.put("TPCNT"+strTPKey, String.valueOf(iTPCnt));
		hmTempTPs.put("PERIODID", strPeriodId);
		hmTempTPs.put("SDS", strSDS);		
		hmKeyCount.put("TPCNT"+strTPKey, String.valueOf(iTPCnt));
		alList.add(hmTempTPs);
		return alList;
	}
	/*
	 * Method to add the value in form list and return it 
	 * 
	 * */
	public ArrayList addFormList(String strFormId, String strFormname, String strFormKey, String strPListId,String strEventNo,String strEventDesc, String strSDSId, ArrayList alList) throws AppError
	{
		HashMap hmTempForms = new HashMap();
		hmTempForms.put("FORMID", strFormId);
		hmTempForms.put("FORMNAME", strFormname);
		hmTempForms.put("FORMKEY", strFormKey);
		hmTempForms.put("PLISTID", strPListId);
		hmTempForms.put("EVENTNO", strEventNo);
		hmTempForms.put("EVENTDESC", strEventDesc);
		hmTempForms.put("SDSID", strSDSId);
		alList.add(hmTempForms);
		return alList;
	}
	
	/*
	 * Method to add the value in Query list and return it 
	 * 
	 * */
	public ArrayList addQueryList(String strQueryId, String strQueryLvlId, String strQueryDesc, String strQueryKey, String strPListId,String strPtAnsListId, ArrayList alList) throws AppError
	{
		HashMap hmTempQuery = new HashMap();
		
		hmTempQuery.put("QUERYID", strQueryId);
		hmTempQuery.put("QUERYLEVELID", strQueryLvlId);
		hmTempQuery.put("SHORTDESC", strQueryDesc);
		hmTempQuery.put("PATIENTLISTID",strPListId);
		hmTempQuery.put("PTANSLISTID",strPtAnsListId);
		hmTempQuery.put("QUERYKEY",strQueryKey);
		alList.add(hmTempQuery);
		
		return alList;
	}
	 
}