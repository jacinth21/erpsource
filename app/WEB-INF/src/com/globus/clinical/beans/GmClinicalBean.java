/*
 * Module: GmARBean.java Author: Dhinakaran James Project: Globus Medical App
 * Date-Written: 11 Mar 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What
 * you changed
 *  
 */

package com.globus.clinical.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.util.GmTemplateUtil;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	/**
	 * loadStudyList - This method gets Invoice information for the selected PO
	 * and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadStudyList() throws AppError {
		ArrayList alStudyList = new ArrayList();
		String strCondition = "";
		alStudyList = loadStudyList(strCondition);
		return alStudyList;
	} // End of loadStudyList
	/**
	 * loadStudyList - This method gets Invoice information for the selected PO
	 * and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadStudyList(String strCondition) throws AppError {
		ArrayList alStudyList = new ArrayList();

		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.setLength(0);
		sbQuery.append(" SELECT t611.c611_study_id studyid, t611.c611_study_nm studynm, COUNT(t614.c611_study_id) COUNT");
		sbQuery.append(", t611.c901_study_status status ");
		sbQuery.append(" FROM t611_clinical_study t611, t614_study_site t614");
		sbQuery.append(" WHERE t611.c611_study_id = t614.c611_study_id(+)");
		sbQuery.append(" AND t611.c611_delete_fl = 'N' ");
		sbQuery.append(" AND t614.c614_void_fl IS NULL ");
		if (!strCondition.equals(""))
		{
			sbQuery.append(" AND t614.C614_STUDY_SITE_ID in (select v621.C614_STUDY_SITE_ID");
			sbQuery.append(" from V621_PATIENT_STUDY_LIST v621 where ");
			sbQuery.append(strCondition);
			sbQuery.append(")");
		}
		sbQuery.append(" GROUP BY t611.c611_study_id, t611.c611_study_nm, t611.c901_study_status");
		sbQuery.append(" ORDER BY t611.C611_STUDY_ID");
		log.debug("query==="+sbQuery.toString());
		alStudyList = dbCon.queryMultipleRecords(sbQuery.toString());

		return alStudyList;
	} // End of loadStudyList
	
	/**
	 * loadPatientList - This method gets Invoice information for the selected
	 * PO and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadPatientList(HashMap hmParam) throws AppError {
		ArrayList alPatientList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		
		StringBuffer sbQuery = new StringBuffer();
		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDY_ID"));
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("ACCESS_CONDITION"));
		String strSiteID = GmCommonClass.parseNull((String)hmParam.get("SITE_ID"));
		String strStudySiteID = GmCommonClass.parseNull((String)hmParam.get("STUDYSITEID"));

		sbQuery.setLength(0);
		sbQuery.append(" SELECT t621.C621_PATIENT_ID PATID, C621_PATIENT_IDE_NO PATIDE, t621.C611_STUDY_ID STID, ");
		sbQuery.append(" C901_SURGERY_TYPE SURGTYPE, C621_SURGERY_DATE SURGDT ");
		sbQuery.append(" FROM T621_PATIENT_MASTER t621, T614_STUDY_SITE t614 ");
		sbQuery.append(" WHERE C621_DELETE_FL = 'N' ");
		sbQuery.append(" AND t621.C611_STUDY_ID = '" + strStudyId + "' ");
		sbQuery.append(" AND t614.c704_account_id = t621.c704_account_id ");	 
		sbQuery.append(" AND t621.C611_STUDY_ID = t614.C611_STUDY_ID AND t614.c614_void_fl IS NULL ");
		if (!strCondition.equals(""))
		{
			sbQuery.append("and t621.c704_account_id in (select v621.C704_ACCOUNT_ID ");
			sbQuery.append(" from V621_PATIENT_STUDY_LIST v621 where ");
			sbQuery.append(strCondition);
			sbQuery.append(")");
		}
		if (!strSiteID.equals(""))
		{
			sbQuery.append(" and t614.C614_SITE_ID = "+strSiteID+"");
		}
		if (!strStudySiteID.equals(""))
		{
			sbQuery.append(" AND t614.c614_study_site_id = "+strStudySiteID+"");
		}

		sbQuery.append(" ORDER BY C621_PATIENT_IDE_NO");
		log.debug("sbquery =="+sbQuery.toString());
		alPatientList = gmDBManager.queryMultipleRecords(sbQuery.toString());

		return alPatientList;
	} // End of loadPatientList

	public ArrayList loadFormsList(String strStudyId) throws AppError {
		ArrayList alFormsList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_dataupload.GM_REPORT_FORM", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strStudyId);
		gmDBManager.execute();
		alFormsList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("Exit");
		return alFormsList;
	}

	/**
	 * loadFormsList - This method gets Invoice information for the selected PO
	 * and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadFormsList(String strStudyId, String strPeriodId) throws AppError {
		ArrayList alPatientList = new ArrayList();

		try {
			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();

			StringBuffer sbQuery = new StringBuffer();

			sbQuery.setLength(0);
			sbQuery.append(" SELECT T601.C601_FORM_ID FORMID, T612.C612_STUDY_FORM_DS || ' - ' || T601.C601_FORM_NM FORMNM, ");
			sbQuery
					.append(" T612.C611_STUDY_ID STID, T612.C612_SEQ FORMSEQ, T612.C612_STUDY_LIST_ID STLISTID, C613_STUDY_PERIOD_ID STPERID, T612.C612_INITIAL_FL INIREQFL, T613.C613_Study_exam_type FLAG ");

			sbQuery.append(" FROM T601_FORM_MASTER T601, T612_STUDY_FORM_LIST T612, T613_STUDY_PERIOD T613 ");
			sbQuery.append(" WHERE T601.C601_FORM_ID = T612.C601_FORM_ID AND T612.C611_STUDY_ID ='");
			sbQuery.append(strStudyId);
			sbQuery.append("' AND T613.C612_STUDY_LIST_ID = T612.C612_STUDY_LIST_ID ");
			sbQuery.append(" AND T613.C901_STUDY_PERIOD = ");
			sbQuery.append(strPeriodId);
			sbQuery.append(" ORDER BY T612.C612_SEQ");

			alPatientList = dbCon.queryMultipleRecords(sbQuery.toString());
		} catch (Exception e) {
			GmLogError.log("Exception in GmClinicalBean:loadFormsList", "Exception is:" + e);
		}
		return alPatientList;
	} // End of loadFormsList

	/**
	 * loadFormDataEntryLists - This method gets Invoice information for the
	 * selected PO and for the selected Account
	 * 
	 * @param String
	 *            strStudyId
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap loadFormDataEntryLists(HashMap hmParam) throws AppError {
		HashMap hmReturn = new HashMap();
		ArrayList alPeriodList = new ArrayList();
		ArrayList alPatientList = new ArrayList();
		String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDY_ID"));
				
		try {
			alPeriodList = loadPeriodList(strStudyId);
			alPatientList = loadPatientList(hmParam);

			hmReturn.put("PERIODLIST", alPeriodList);
			hmReturn.put("PATIENTLIST", alPatientList);

		} catch (Exception e) {
			GmLogError.log("Exception in GmClinicalBean:loadFormDataEntryLists", "Exception is:" + e);
		}
		return hmReturn;
	} // End of loadFormDataEntryLists

	/**
	 * loadPeriodList - This method gets Invoice information for the selected PO
	 * and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadPeriodList(String strStudyId) throws AppError {
		ArrayList alPeriodList = new ArrayList();

		try {
			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();

			StringBuffer sbQuery = new StringBuffer();

			sbQuery.setLength(0);
			sbQuery
					.append(" SELECT DISTINCT B.C901_STUDY_PERIOD PERIODID, GET_CODE_NAME(B.C901_STUDY_PERIOD) PERIODDS, GET_CODE_SEQ_NO(B.C901_STUDY_PERIOD) SEQ");
			sbQuery.append(" FROM T612_STUDY_FORM_LIST A, T613_STUDY_PERIOD B ");
			sbQuery.append(" WHERE A.C612_STUDY_LIST_ID = B.C612_STUDY_LIST_ID AND A.C611_STUDY_ID ='");
			sbQuery.append(strStudyId);
			sbQuery.append("'");
			sbQuery.append(" ORDER BY SEQ");

			alPeriodList = dbCon.queryMultipleRecords(sbQuery.toString());
		} catch (Exception e) {
			GmLogError.log("Exception in GmClinicalBean:loadPeriodList", "Exception is:" + e);
		}
		return alPeriodList;
	} // End of loadPeriodList

	/**
	 * saveFormAnswers - This method will
	 * 
	 * @param HashMap
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap saveFormAnswers(HashMap hmValues, String strAction) throws AppError {
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		CallableStatement csmt = null;
		Connection conn = null;

		String strMsg = "";
		String strPrepareString = null;

		HashMap hmReturn = new HashMap();

		log.debug("hmValues: " + hmValues + " Action: " + strAction);
		//System.out.println("hmValues: " + hmValues + " Action: " + strAction);
		try {
			String strStudyId = (String) hmValues.get("STUDYID");
			String strFormId = (String) hmValues.get("FORMID");
			String strPatientId = (String) hmValues.get("PATIENTID");
			String strFormDt = (String) hmValues.get("FORMDATE");
			String strStudyPeriod = (String) hmValues.get("STUDYPERIOD");
			String strInitials = (String) hmValues.get("INITIALS");
			String strInputStr = (String) hmValues.get("INPUTSTR");
			String UserId = (String) hmValues.get("USERID");
			String strPatientListId = GmCommonClass.parseZero((String) hmValues.get("PATLISTID"));
			String strCalcValue = GmCommonClass.parseNull((String) hmValues.get("CALVAL"));
			String strVerifyType = (String) hmValues.get("VERIFYTYPE");
			String strCommentDS = (String) hmValues.get("COMMENTDS");
			String strMissingFollowup = (String) hmValues.get("MISSINGFOLLOWUP");
			String strEventNo = (String) hmValues.get("EVENTNO");
			String strFlag = (String) hmValues.get("FLAG");
			conn = dbCon.getConnection();
			log.debug("save strEventNo" + strEventNo);
			strPrepareString = dbCon.getStrPrepareString("GM_SAVE_CLINIC_FORM", 15);
			csmt = conn.prepareCall(strPrepareString);
			// csmt.setString(1,strStudyId);
			csmt.setInt(1, Integer.parseInt(strFormId));
			csmt.setInt(2, Integer.parseInt(strPatientId));
			csmt.setString(3, strFormDt);
			csmt.setInt(4, Integer.parseInt(strStudyPeriod));
			csmt.setString(5, strInitials);
			csmt.setString(6, strInputStr);
			csmt.setString(7, UserId);
			csmt.setInt(8, Integer.parseInt(strPatientListId));
			csmt.setString(9, strAction);
			csmt.setString(10, strVerifyType);
			csmt.setString(11, strCommentDS);
			csmt.setString(12, strCalcValue);
			csmt.setString(13, strMissingFollowup);
			csmt.setString(14, strEventNo);
			csmt.setString(15, strFlag);
			csmt.execute();

			// System.out.println("MSG from Oracle: "+strMsg);
			conn.commit();

		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (Exception sqle) {
				sqle.printStackTrace();
				throw new AppError(sqle);
			}
			e.printStackTrace();
			GmLogError.log("Exception in GmClinicalBean:saveFormAnswers", "Exception is:" + e);
			throw new AppError(e);
		} finally {
			try {
				if (csmt != null) {
					csmt.close();
				}// Enf of if (csmt != null)
				if (conn != null) {
					conn.close(); /* closing the Connections */
				}
			} catch (Exception e) {
				throw new AppError(e);
			}// End of catch
			finally {
				csmt = null;
				conn = null;
				dbCon = null;
			}
		}
		return hmReturn;
	} // end of saveFormAnswers

	/**
	 * loadPatientFormDetails - This method gets Invoice information for the
	 * selected PO and for the selected Account
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap loadPatientFormDetails(String strStudyId, String strPatientId, String strFormId, String strStudyPeriodId, String strEventNo,
			String strFormDate,String strPatListId,String strIncVoidFrm) throws AppError {
		HashMap hmResult = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmPatients = new HashMap();
		ArrayList alReturn = new ArrayList();
		GmCommonClass gmCommon = new GmCommonClass();
		GmPatientBean gmPatientBean = new GmPatientBean();
		StringBuffer sbQuery = new StringBuffer();
		String strRuleFormId="";
		strIncVoidFrm = GmCommonClass.parseNull(strIncVoidFrm);
		if(strIncVoidFrm.equals(""))
			strIncVoidFrm = "N";

		try {

			GmQuestionBean gmQues = new GmQuestionBean();
			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();
			hmPatients = GmCommonClass.parseNullHashMap(gmPatientBean.fetchPatientDetails(strPatientId));
			//To get the Rule value based on Formid & studyid.
			sbQuery.append(" select get_rule_value('");
			sbQuery.append(strFormId);
			sbQuery.append("','");
			sbQuery.append(strStudyId);
			sbQuery.append("')RULEFORMID from dual");
			hmResult = dbCon.querySingleRecord(sbQuery.toString());
			strRuleFormId = GmCommonClass.parseNull((String) hmResult.get("RULEFORMID"));
			/*This condition will be applied to 2 Years completion form for Flexus,Secure C Study,
			 *and Study Completion form in TRIUMPH, Surgical Intervention,Patient Disposition forms in ACADIA.
			*/	
			if(GmCommonClass.parseNull(strPatListId).equals("")&& strFormId.equals(strRuleFormId)){
				sbQuery.setLength(0);
				sbQuery.append(" SELECT C622_PATIENT_LIST_ID PATLISTID ");
				sbQuery.append(" From T622_PATIENT_LIST ");
				sbQuery.append(" WHERE C613_STUDY_PERIOD_ID ='");
				sbQuery.append(strStudyPeriodId);
				sbQuery.append("' AND C601_FORM_ID ='");
				sbQuery.append(strFormId);
				sbQuery.append("' AND C621_PATIENT_ID ='");
				sbQuery.append(strPatientId);
				sbQuery.append("'  AND NVL(C622_DELETE_FL,'N')='N' ");
				log.debug(" First COndition sbQuery "+sbQuery);
				hmResult = dbCon.querySingleRecord(sbQuery.toString());
				strPatListId = GmCommonClass.parseNull((String) hmResult.get("PATLISTID"));
			}
			hmResult = gmQues.loadQuestionInformation(strRuleFormId,strPatientId, strFormId, strStudyPeriodId, strEventNo, strFormDate,strPatListId,strIncVoidFrm);
			
			hmReturn.put("QUESTIONDETAILS", hmResult);
			
			sbQuery.setLength(0);
			sbQuery.append(" SELECT t622.C622_PATIENT_LIST_ID PATLISTID, to_char(t622.C622_DATE,'mm/dd/yyyy') FORMDATE, t622.C622_INITIALS INITIALS,  ");
			sbQuery.append(" GET_USER_NAME(t622.C622_CREATED_BY) CUSER, to_char(t622.C622_CREATED_DATE,'mm/dd/yyyy') CDATE, t622.C622_VERIFY_FL VFL, ");
			sbQuery.append(" t622.C601_FORM_ID FORMID, t622.C621_PATIENT_ID PATID, t622.C613_STUDY_PERIOD_ID STDPERID, ");
			sbQuery.append(" GET_USER_NAME(t622.C622_LAST_UPDATED_BY) LUSER, to_char(t622.C622_LAST_UPDATED_DATE,'mm/dd/yyyy') LDATE, ");
			sbQuery.append(" t622.C621_CAL_VALUE CVALUE, t622.C622_MISSING_FOLLOW_UP_FL MISSINGFOLLOWUP ,t622.C622_DATE_HISTORY_FL DATEHISTORYFL, t622.C622_INITIALS_HISTORY_FL INITIALSHISTORYFL");
			sbQuery.append(" ,gm_pkg_cm_query_info.get_query_status(t622.C622_PATIENT_LIST_ID, 92421) FORM_STATUS ");
			sbQuery.append(" ,gm_pkg_cm_query_info.get_query_status(t622.C622_PATIENT_LIST_ID, 92423) EXAM_STATUS ");
			sbQuery.append(" ,t621.C621_PATIENT_IDE_NO PATIENT_IDE,t622.c622_delete_fl DELETEFL");
			sbQuery.append(" FROM T622_PATIENT_LIST t622, t621_patient_master t621 ");
			sbQuery.append(" WHERE t622.C613_STUDY_PERIOD_ID ='");
			sbQuery.append(strStudyPeriodId);
			sbQuery.append("' AND t622.C601_FORM_ID ='");
			sbQuery.append(strFormId);
			sbQuery.append("' AND t622.C621_PATIENT_ID =");
			sbQuery.append(strPatientId);
			sbQuery.append("");
			sbQuery.append(" AND t621.C621_PATIENT_ID = t622.C621_PATIENT_ID ");
			if (!strEventNo.equals("")&& !strEventNo.equals("0")) {
				sbQuery.append(" AND t622.C622_patient_event_no = '" + strEventNo + "'");
			} else if (!strFormDate.equals("")) {
				sbQuery.append(" AND t622.C622_DATE = TO_DATE ('" + strFormDate + "', 'MM/DD/YYYY') ");
			}
			/*This condition will be applied to 2 Years completion form for Flexus,Secure C Study.
			 *and Study Completion form in TRIUMPH, Surgical Intervention,Patient Disposition forms in ACADIA.
			 */
			else if (!strFormId.equals(strRuleFormId)){
				sbQuery.append(" AND t622.C613_STUDY_PERIOD_ID NOT IN  (SELECT C613_STUDY_PERIOD_ID FROM T613_STUDY_PERIOD WHERE C901_STUDY_PERIOD = '6309') ");
			}
			sbQuery.append("  AND t622.C622_PATIENT_LIST_ID = NVL('"+strPatListId+"',t622.C622_PATIENT_LIST_ID) ");

			sbQuery.append(" AND T622.C622_DELETE_FL = '"+strIncVoidFrm+"' ");
			
			log.debug("QUESTIONDETAILS toString():" + sbQuery.toString());
			hmResult = dbCon.querySingleRecord(sbQuery.toString());
			
			hmReturn.put("PATIENTFORMDETAILS", hmResult);
			strPatListId = GmCommonClass.parseNull((String) hmResult.get("PATLISTID"));

			sbQuery.setLength(0);
			sbQuery.append(" SELECT GET_FORM_CALC_REQ_FL('");
			sbQuery.append(strStudyId);
			sbQuery.append("','");
			sbQuery.append(strFormId);
			sbQuery.append("') CALREQFL FROM DUAL");

			hmResult = dbCon.querySingleRecord(sbQuery.toString());
			String strCalReqFl = (String) hmResult.get("CALREQFL");
			hmReturn.put("CALREQFL", strCalReqFl);

			sbQuery.setLength(0);
			sbQuery.append(" SELECT T625.C622_PATIENT_LIST_ID PATLISTID ");
			sbQuery.append(" ,to_char(T625.C625_DATE,'MM/DD/YYYY') VERDATE ");
			sbQuery.append(" ,GET_USER_NAME(T625.C101_USER_ID) CUSER ");
			sbQuery.append(" ,GET_CODE_NAME(T625.C901_VERIFY_LEVEL) VLEVEL ");
			sbQuery.append(" ,T902.C902_COMMENTS CMNTS , T625.C901_VERIFY_LEVEL VLEVEL_ID ");
			sbQuery.append(" FROM T625_PATIENT_VERIFY_LIST T625 ");
			sbQuery.append(" ,T902_LOG T902 ");
			sbQuery.append(" WHERE T625.C622_PATIENT_LIST_ID = '");
			sbQuery.append(strPatListId);
			sbQuery.append("' AND T902.C902_REF_ID (+) = TO_CHAR(T625.C625_PAT_VERIFY_LIST_ID)");
			sbQuery.append(" AND C902_TYPE(+) = '1204' ");
			sbQuery.append("  and NVL(t625.c625_delete_fl,'N') ='"+strIncVoidFrm+"'");
			sbQuery.append(" ORDER BY C625_PAT_VERIFY_LIST_ID");
			log.debug("verified details" + sbQuery.toString());
			alReturn = dbCon.queryMultipleRecords(sbQuery.toString());
			hmReturn.put("VERIFYDETAILS", alReturn);

			hmReturn.put("VERIFYVALUE", gmCommon.getCodeList("FMVRF"));
			hmReturn.put("PATIENTDETAILS", hmPatients);

		} catch (Exception e) {
			GmLogError.log("Exception in GmClinicalBean:loadPatientFormDetails", "Exception is:" + e);
		}
		return hmReturn;
	} // End of loadPatientFormDetails

	/**
	 * loadPatientAnsHistory - This method gets Invoice information for the
	 * selected PO and for the selected Account
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadPatientAnsHistory(String strPatAnsListId) throws AppError {
		ArrayList alAnsHistory = new ArrayList();
		try {
			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.setLength(0);
			sbQuery.append(" SELECT   DECODE (t623a.c901_status,");
			sbQuery.append(" 92441, 'Query Raised',");
			sbQuery.append(" 92442, 'Query' || get_code_name (t623a.c901_status),");
			sbQuery.append(" 92443, 'Query' || get_code_name (t623a.c901_status),");
			sbQuery.append(" 92444, 'Query' || get_code_name (t623a.c901_status),");
			sbQuery.append(" get_code_name (t623a.c901_status)");
			sbQuery.append(" ) display_text,");
			sbQuery.append(" DECODE (t623a.C6514_EC_INCIDENT_ID, NULL, ");
			sbQuery.append(" DECODE (t623a.c6510_query_id,");
			sbQuery.append(" NULL, DECODE (c605_answer_list_id,");
			sbQuery.append(" NULL, c904_answer_ds,");
			sbQuery.append(" get_answer_list_name (c605_answer_list_id)");
			sbQuery.append(" ),");
			sbQuery.append(" t623a.c6511_query_text");
			sbQuery.append(" ),  t623a.c6511_query_text");
			sbQuery.append(" ) anslistid,");
			sbQuery.append(" TO_CHAR (c623_created_date, 'mm/dd/yyyy') cdate,");
			sbQuery.append(" DECODE (c605_answer_list_id, NULL, '', c904_answer_ds) ansds,");
			sbQuery.append(" get_user_name (c623_created_by) cuser");
			sbQuery.append(" FROM t623a_patient_answer_history t623a");
			sbQuery.append(" WHERE c623_patient_ans_list_id = " + strPatAnsListId);
			sbQuery.append(" AND NVL(t623a.C623_DELETE_FL,'N')='N'");
			sbQuery.append(" ORDER BY c623a_patient_ans_his_id");
			
			log.debug("Query: "+ sbQuery.toString()); 

			alAnsHistory = dbCon.queryMultipleRecords(sbQuery.toString());
		} catch (Exception e) {
			GmLogError.log("Exception in GmClinicalBean:loadPatientAnsHistory", "Exception is:" + e);
		}
		return alAnsHistory;
	} // End of loadPatientAnsHistory

	/**
	 * reportDashboard - This method will fetch pending arraylist information
	 * and will pass the same to the screen
	 * 
	 * @return ArrayList List of party name
	 * @exception AppError
	 */
	public HashMap reportDashboard(String strStudyID, String strCraId) throws AppError {

		HashMap hmDashBoard = new HashMap();
		ArrayList alPedingForm = new ArrayList();
		ArrayList alOutOfWindow = new ArrayList();
		ArrayList alVerifyList = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("GM_REPORT_FORM_DUE_VERIFY_LIST", 5);

		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);

		gmDBManager.setString(1, GmCommonClass.parseNull(strStudyID));
		gmDBManager.setString(2, GmCommonClass.parseNull(strCraId));
		gmDBManager.execute();

		alPedingForm = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		alOutOfWindow = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		alVerifyList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));

		hmDashBoard.put("PENDINGFORM", alPedingForm);
		hmDashBoard.put("OUTOFWINDOW", alOutOfWindow);
		hmDashBoard.put("PENDINGVERFICATION", alVerifyList);

		log.debug("alPedingForm " + alPedingForm.size());
		log.debug("alOutOfWindow " + alOutOfWindow.size());
		log.debug("alVerifyList " + alVerifyList.size());

		gmDBManager.close();

		log.debug("Exit");
		return hmDashBoard;
	}

	public void saveClinicalSurgeonSiteMap(HashMap hmParam) throws AppError {

		log.debug("Enter");
		HashMap hmReturn = new HashMap();

		String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYLIST"));
		String strAccountId = GmCommonClass.parseNull((String) hmParam.get("SITENAMELIST"));
		String strfirstName = GmCommonClass.parseNull((String) hmParam.get("FIRSTNAME"));
		String strlastName = GmCommonClass.parseNull((String) hmParam.get("LASTNAME"));
		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("SURGEONNAMELIST"));
		String strSurgeonType = GmCommonClass.parseNull((String) hmParam.get("SURGEONTYPE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

		log.debug(hmParam);

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_CR_SAV_SURGEON_MAP_INFO", 7);
		gmDBManager.setString(1, strStudyId);
		gmDBManager.setString(2, strAccountId);
		gmDBManager.setString(3, strfirstName);
		gmDBManager.setString(4, strlastName);
		gmDBManager.setString(5, strPartyId);
		gmDBManager.setString(6, strSurgeonType);
		gmDBManager.setString(7, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

		log.debug("Exit");
	}

	public RowSetDynaClass loadSurgeonMapDetails(String strStudySiteId) throws AppError {
		log.debug("Enter");
		RowSetDynaClass rdReturn = null;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_CR_FCH_SURGEON_MAP_DETAILS", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strStudySiteId));
		gmDBManager.execute();
		rdReturn = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("Size :" + rdReturn.getRows().size());
		log.debug("Exit");
		return rdReturn;
	}

	public HashMap loadSurgeonMapInfo(String strSurgeonSiteId) throws AppError {
		log.debug("Enter" + strSurgeonSiteId);
		HashMap hmReturn = null;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_CR_FCH_SURGEON_MAP_INFO", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSurgeonSiteId));
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("Exit");
		return hmReturn;
	}

	public ArrayList loadNoOfEvents(String strPatientId, String strFormId, String strStudyPeriodId) throws AppError {

		ArrayList alNoOfEvents = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.gm_load_no_of_event", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strStudyPeriodId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strFormId));
		gmDBManager.setString(3, GmCommonClass.parseNull(strPatientId));
		gmDBManager.execute();
		alNoOfEvents = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		log.debug("Exit");
		return alNoOfEvents;

	}
	
	/**
	 * loadFormListForPeriod - This method used to load the forms for study
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadFormListForPeriod(String strStudyId, String strPeriodId) throws AppError {
		ArrayList alFormsList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.gm_fch_prd_study_form", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strStudyId);
		gmDBManager.setString(2, strPeriodId);
		gmDBManager.execute();
		alFormsList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return alFormsList;
	}
	
	
	/**
	 * loadStudyFormList - This method used to load the forms for study
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public static ArrayList loadStudyFormList(String strStudyId) throws AppError {
		ArrayList alFormsList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.gm_study_form_list", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strStudyId);
		gmDBManager.execute();
		alFormsList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alFormsList;
	}
	
	public ArrayList loadNoOfEventsDesc(String strPatientId, String strFormId, String strStudyPeriodId,String strVoidFl) throws AppError {

		ArrayList alNoOfEvents = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.gm_load_no_of_event_des", 5);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strStudyPeriodId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strFormId));
		gmDBManager.setString(3, GmCommonClass.parseNull(strPatientId));
		gmDBManager.setString(4, strVoidFl);
		gmDBManager.execute();
		alNoOfEvents = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();
		log.debug("Exit");
		return alNoOfEvents;
	}

	/**
	 * loadAllFormsList - This method used to load the forms for study
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadAllFormsList(String strStudyId) throws AppError {
		ArrayList alFormsList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.gm_all_study_form", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strStudyId);
		gmDBManager.execute();
		alFormsList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alFormsList;
	}
	
	
	public String getXmlGridData(HashMap hmParamV) throws AppError {
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();

		ArrayList alParam = GmCommonClass.parseNullArrayList((ArrayList)hmParamV.get("RLIST"));
		String strTemplate = GmCommonClass.parseNull((String)hmParamV.get("TEMPLATE"));
		String strTemplatePath = GmCommonClass.parseNull((String)hmParamV.get("TEMPLATEPATH"));
		HashMap hmApplnParam = GmCommonClass.parseNullHashMap((HashMap) hmParamV.get("hmApplnParam"));
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setDataMap("hmApplnParam", hmApplnParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}

	/**
	 * This method saves the patient details
	 * @param gmDBManager
	 * @param hmParas
	 * @throws AppError
	 */
	public int savePatientRecord(HashMap hmParas) throws AppError {

		GmDBManager gmDBManager = new GmDBManager ();
		String strPatientId = GmCommonClass.parseNull((String) hmParas.get("PATIENT_ID"));
		String strTimepoint = GmCommonClass.parseNull((String) hmParas.get("TIME_POINT_ID"));
		String strType = GmCommonClass.parseNull((String) hmParas.get("TYPE_ID"));
		String strView = GmCommonClass.parseNull((String) hmParas.get("VIEW_ID"));		
		String strFilename = GmCommonClass.parseNull((String) hmParas.get("FILENAME"));
		String strUserId = GmCommonClass.parseNull((String) hmParas.get("USERID"));
		String strStudyId = GmCommonClass.parseNull((String) hmParas.get("STUDYID"));
		String strRadioGrphDt = GmCommonClass.parseNull((String) hmParas.get("RADIOGRPHDT"));
		String strPatientRecId = GmCommonClass.parseZero((String) hmParas.get("PAT_REC_ID"));
		
		gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_SAV_PATIENT_RECORD", 9);

		log.debug("strView " + strView);
		log.debug("strRadioGrphDt " + strRadioGrphDt);
		
		gmDBManager.setString(1, strPatientId);
		gmDBManager.setString(2, strTimepoint);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strView);
		gmDBManager.setString(5,strFilename);
		gmDBManager.setString(6, strUserId);
		gmDBManager.setString(7, strStudyId);
		gmDBManager.setString(8, strRadioGrphDt);
		gmDBManager.setInt(9, Integer.parseInt(strPatientRecId));
		gmDBManager.registerOutParameter(9, java.sql.Types.INTEGER);

		gmDBManager.execute();
		log.debug("11");		
		
		strPatientRecId = gmDBManager.getString(9);
		gmDBManager.commit();
		return Integer.parseInt(strPatientRecId);
	}

	/**
	 * loadPatientRecord - This method call gm_cr_fch_patient_record procedure to retrieve all patient record for selected criteria
     * on raidograph report page. 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList loadPatientRecord(HashMap hmParam) throws AppError {
          ArrayList alPatientRecords = new ArrayList();         
          
          String strStudyId = GmCommonClass.parseNull((String)hmParam.get("STUDYID"));
          String strSiteId = GmCommonClass.parseNull((String)hmParam.get("SITEID"));
          String strPatientId = GmCommonClass.parseNull((String)hmParam.get("PATIENT_ID"));
          String strTimePointId = GmCommonClass.parseNull((String)hmParam.get("TIME_POINT_ID"));
          String strTypeId = GmCommonClass.parseNull((String)hmParam.get("TYPE_ID"));
          String strViewId = GmCommonClass.parseNull((String)hmParam.get("VIEW_ID"));
          String strPatientRecordId = GmCommonClass.parseNull((String)hmParam.get("PAT_REC_ID"));       
          
          GmDBManager gmDBManager = new GmDBManager();
          gmDBManager.setPrepareString("GM_PKG_CR_COMMON.gm_cr_fch_patient_record", 8);
          gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
          gmDBManager.setString(1, strPatientId);
          gmDBManager.setString(2, strTimePointId);
          gmDBManager.setString(3, strViewId);
          gmDBManager.setString(4, strTypeId);
          gmDBManager.setString(5, strPatientRecordId);
          gmDBManager.setString(6, strStudyId);
          gmDBManager.setString(7, strSiteId);
          gmDBManager.execute();
          alPatientRecords = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
          gmDBManager.close();
          log.debug("Exit loadPatientRecord size " +alPatientRecords.size());
          return alPatientRecords;

          
    } // End of loadPatientRecord

	/**
	 * This method saves the patient details
	 * @param gmDBManager
	 * @param hmParam
	 * @throws AppError
	 */
	public ArrayList loadSiteCoordinatorReport(HashMap hmParam) throws AppError {
		ArrayList alResult = new ArrayList();         
		GmDBManager gmDBManager = new GmDBManager ();
		
		String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYLISTID"));
		
		gmDBManager.setPrepareString("gm_pkg_cr_sitemapping.gm_fch_sc_report", 2);
		log.debug("strStudyId " + strStudyId);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setString(1, strStudyId);

        gmDBManager.execute();
        alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
        gmDBManager.close();
        return alResult;
	}


	/**
	 * This method get the clinical form void report details
	 * @param hmParam
	 * @throws AppError
	 * @return ArrayList
	 */
	
	public ArrayList  fetchVoidedReportFroms(HashMap hmParam) throws AppError {
			ArrayList alResult= new ArrayList();
			String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYLISTID"));
			String strStudySiteId = GmCommonClass.parseNull((String) hmParam.get("SITELIST"));
			String strPatientId = GmCommonClass.parseNull((String) hmParam.get("PATIENTID"));
			String strPeriodId = GmCommonClass.parseNull((String) hmParam.get("PERIODID"));
			String strFormId = GmCommonClass.parseNull((String) hmParam.get("FORMID"));
			String strReasonForClosing = GmCommonClass.parseNull((String)hmParam.get("REASONFORCLOSING"));
			String strCraId = GmCommonClass.parseNull((String)hmParam.get("CRAID"));
			String strDateFrom = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
			String strDateTo = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
			strStudySiteId = strStudySiteId.equals("")?GmCommonClass.parseNull((String) hmParam.get("SESSSTUDYSITEID")):strStudySiteId;
			strPatientId = strPatientId.equals("0")?GmCommonClass.parseNull((String) hmParam.get("SESSPATIENTPKEY")):strPatientId;
			GmDBManager gmDBManager = new GmDBManager();
			
			gmDBManager.setPrepareString("gm_pkg_cr_void_clinical_form.gm_fch_void_report_form",10);
			gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
			gmDBManager.setString(1, strStudyId);
			gmDBManager.setString(2, strStudySiteId);
			gmDBManager.setString(3, strPatientId);
			gmDBManager.setString(4, strPeriodId); 
			gmDBManager.setString(5, strFormId);
			gmDBManager.setString(6, strDateFrom);
			gmDBManager.setString(7, strDateTo);
			gmDBManager.setString(8, strReasonForClosing);
			gmDBManager.setString(9, strCraId);
			gmDBManager.execute();
			alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10));

			gmDBManager.close();
			return alResult ;
		} //
	
	/**
	 * This method is for Un-Voiding the Adverse Event Form as part of the MNTTASK-3766
	 * @param gmDBManager
	 * @param String
	 * @throws AppError
	 */
	public void unVoidReportForm(String strPatListId,String strPatientId, String strFormId , String strUserId) throws AppError {
	    
		GmDBManager gmDBManager = new GmDBManager ();		
		gmDBManager.setPrepareString("gm_pkg_cr_void_clinical_form.gm_un_void_report_form", 4);		
		gmDBManager.setString(1, strPatListId);
		gmDBManager.setString(2, strPatientId);
		gmDBManager.setString(3, strFormId);
		gmDBManager.setString(4, strUserId);
		gmDBManager.execute(); 
		gmDBManager.commit();
		gmDBManager.close();              
	}
	
	/**
	 * fetchStudyDtls This method used to fetch Study details
	 * 
	 * @param String
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchStudyDtls() throws AppError {
		ArrayList alStudyList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_common.gm_fch_study_dtls", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alStudyList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alStudyList;
	}

	/**
	 * fetchStudyHistory This method used to fetch the Study History
	 * 
	 * @param String
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchStudyHistory(String strStudyId) throws AppError {
		ArrayList alHistory = null;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cr_common.gm_fch_study_history", 2);
		log.debug("strStudyId :" + strStudyId);
		gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alHistory = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alHistory;
	}

	/**
	 * getStudyStatus This method used to fetch the Study status
	 * 
	 * @param String
	 * @return String
	 * @exception AppError
	 */
	public String getStudyStatus(String strStudyId) throws AppError {

		String strStatusId = "";

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setFunctionString("gm_pkg_cr_common.get_study_status", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.NUMBER);
		gmDBManager.setString(2, strStudyId);
		gmDBManager.execute();
		strStatusId = GmCommonClass.parseNull((String) gmDBManager.getString(1));
		gmDBManager.close();
		log.debug(" strStatusId --> " + strStatusId);
		return strStatusId;
	}
	
	/**
	 * getFormEditAccess This method used to return the form edit access
	 * 
	 * @param HashMap
	 * @return String
	 * @exception AppError
	 */

	public String getFormEditAccess(HashMap hmParam) throws AppError {
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

		String strStudyId = "";
		String strUserId = "";
		String strStudyStatus = "";
		String strAccessFlag = "";
		String strDisable = "";
		HashMap hmAccess = new HashMap();

		strStudyId = GmCommonClass.parseNull((String) hmParam.get("SESSSTUDYID"));
		strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		log.debug(" strStudyId --> " + strStudyId);
		if (!strStudyId.equals("")) {
			strStudyStatus = getStudyStatus(strStudyId);
		}

		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "LOCKED_FORM_EDIT"));
		strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		log.debug(" Access permissions Flag = " + hmAccess);

		// check the Study status is locked and user have access to edit form
		// 3351 - Lock
		if (strStudyStatus.equals("3351") && !strAccessFlag.equals("Y")) {
			strDisable = "true";
		}
		return strDisable;
	}
	
	/**
	   * This method returns an ArrayList containing Account list for the specified companies
	   * 
	   * @param strCompanyIds - Company ids to which we need accounts. 
	   * @exception AppError
	   * @return ArrayList object
	   */
	  public ArrayList getSiteList(String strCompanyIds) throws AppError {
	    ArrayList alSiteList = new ArrayList();
	    StringBuffer sbQuery = new StringBuffer();
	    GmDBManager gmDBManager = new GmDBManager();

	    sbQuery.append("SELECT  T704.C704_ACCOUNT_ID ID, ");
	    sbQuery.append(" T704.C704_ACCOUNT_NM NAME ");
	    sbQuery.append(" FROM  T704_ACCOUNT  T704 ");
	    sbQuery.append(" WHERE T704.C1900_COMPANY_ID IN ( " +strCompanyIds+ ")");
	    sbQuery.append("   AND T704.C704_VOID_FL IS NULL ");
	    sbQuery.append(" ORDER BY UPPER(NAME) ");
	    log.debug(" getAccountList inside GmCommonBean is  " + sbQuery.toString());
	    alSiteList = gmDBManager.queryMultipleRecords(sbQuery.toString());

	    return alSiteList;
	  }

}// end of class GmClinicalBean
