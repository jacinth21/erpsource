/*
 * Bean: GmPatientBean.java Author: Richard Project: Globus Medical App Date-Written: 31 Oct 2005
 * Security: Unclassified Description: This Bean will be used to handle all the querys releated to
 * patient bean
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.clinical.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmSalesBean;

public class GmPatientBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.

  /**
   * loadSalesRep - This method will be used to fecth all the combo value to be displayed in the
   * screen
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPatientComboValue(String strStudyID, String strStudySiteId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();

    ArrayList alStudyName = new ArrayList();

    GmCommonClass gmCommon = new GmCommonClass();
    GmSalesBean gmSales = new GmSalesBean();
    GmPartyBean gmParty = new GmPartyBean();
    GmStudyBean gmStudy = new GmStudyBean();
    GmClinicalBean gmClinicalBean = new GmClinicalBean();

    log.debug("strStudySiteId :" + strStudySiteId);

    try {

      // alStudyName = gmStudy.reportStudyNameList();


      hmReturn.put("SURGERYTYPELIST", gmCommon.getCodeList(strStudyID));
      hmReturn.put("ACCOUNTLIST", loadSiteFromStudy(strStudyID));
      hmReturn.put("SURGEONNAME", getSurgeonList(strStudySiteId));
      // hmReturn.put("SURGEONNAME", gmParty.reportPartyNameList("7000"));// 7000 ID maps to Party
      // Information
      // hmReturn.put("STUDYLIST", alStudyName);
      hmReturn.put("PATIENTLIST", reportPatientList(strStudyID, strStudySiteId));



    } catch (Exception e) {
      GmLogError.log("Exception in PatientBean:loadPatientComboValue", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadSalesRep

  /**
   * getSurgeonList - loads the surgeons for the given study_site id
   * 
   * @param strStudySiteId
   * @return
   * @throws AppError
   */
  public List getSurgeonList(String strStudySiteId) throws AppError {
    GmClinicalBean gmClinicalBean = new GmClinicalBean();
    RowSetDynaClass rdSurgeonDetails = gmClinicalBean.loadSurgeonMapDetails(strStudySiteId);
    return rdSurgeonDetails.getRows();
  }

  /**
   * loadSalesRep - This method will be used to fecth all the patient and the hospital associated
   * with the study to be displayed in the screen
   * 
   * @param String strStudyID
   * @return HashMap
   * @exception AppError
   */
  public ArrayList reportPatientList(String strStudyID, String strStudySiteID) throws AppError {
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    CallableStatement csmt = null;
    Connection conn = null;

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;


    try {
      conn = dbCon.getConnection();
      strPrepareString = dbCon.getStrPrepareString("GM_REPORT_STUDY_PATIENT", 4);
      csmt = conn.prepareCall(strPrepareString);
      /*
       * register out parameter and set input parameters
       */
      csmt.setString(1, strStudyID);
      csmt.setString(2, strStudySiteID);
      csmt.registerOutParameter(3, java.sql.Types.CHAR);
      csmt.registerOutParameter(4, OracleTypes.CURSOR);

      csmt.execute();

      strBeanMsg = csmt.getString(3);
      rs = (ResultSet) csmt.getObject(4);
      alReturn = dbCon.returnArrayList(rs);



    } catch (Exception e) {
      GmLogError.log("Exception in GmPatientBean:reportPatientList", "Exception is:" + e);
      throw new AppError(e);
    } finally {
      try {
        if (csmt != null) {
          csmt.close();
        }// Enf of if (csmt != null)
        if (conn != null) {
          conn.close(); /* closing the Connections */
        }
      } catch (Exception e) {
        throw new AppError(e);
      }// End of catch
      finally {
        csmt = null;
        conn = null;
        dbCon = null;
      }
    }
    return alReturn;
  } // End of reportPatientListValue

  /**
   * getRepList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap LoadPatientDetails(String strPatientID, String strStudyID) throws AppError {
    HashMap hmResult = new HashMap();
    try {
      log.debug("Enter");
      DBConnectionWrapper dbCon = null;
      dbCon = new DBConnectionWrapper();

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT	T621.C621_PATIENT_IDE_NO PID ");
      sbQuery.append(" ,T621.C901_SURGERY_TYPE S_TYPE ");
      sbQuery.append(" ,TO_CHAR(T621.C621_SURGERY_DATE,'MM/DD/YYYY') S_DATE ");
      sbQuery.append(" ,T614.C614_STUDY_SITE_ID A_ID  ");
      sbQuery.append(" ,T621.C101_SURGEON_ID S_ID ");
      sbQuery.append(" ,T621.C621_PATIENT_ID PKEY_ID ");
      sbQuery.append(" ,GET_CR_FCH_PRINICIPAL_INV(T614.C614_STUDY_SITE_ID) PINV ");
      sbQuery.append(" ,T621.C621_SURGERY_COMP_FL CFLAG ");
      sbQuery.append(" FROM T621_PATIENT_MASTER T621, T614_STUDY_SITE T614 ");
      sbQuery.append(" WHERE ");
      sbQuery.append(" T621.C704_ACCOUNT_ID = T614.C704_ACCOUNT_ID ");
      sbQuery.append(" AND T621.C611_STUDY_ID = T614.C611_STUDY_ID ");
      sbQuery.append(" AND T621.C621_PATIENT_IDE_NO = '");
      sbQuery.append(strPatientID);
      sbQuery.append("' AND T621.C611_STUDY_ID = '");
      sbQuery.append(strStudyID);
      sbQuery.append("' AND T621.C621_DELETE_FL = 'N' ");
      sbQuery.append(" AND T614.C614_VOID_FL IS NULL ");
      log.debug("sbquery====" + sbQuery.toString());
      hmResult = dbCon.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmPatientBean:LoadPatientDetails", "Exception is:" + e);
    }
    log.debug("Exit");
    return hmResult;
  } // End of getRepList

  /**
   * savePatientInfo - This method will be used to save patient information
   * 
   * @param HashMap hmParam -- Holds information that will be used to save patient information
   * @param String strUsername
   * @return HashMap
   * @exception AppError
   */
  public String savePatientInfo(HashMap hmParam, String strUserId) throws AppError {
    String strPatientID = GmCommonClass.parseNull((String) hmParam.get("PID"));
    String strPatientMasterId = GmCommonClass.parseNull((String) hmParam.get("PKEY_ID"));
    String strStudyID = GmCommonClass.parseNull((String) hmParam.get("SID"));
    String strSurgeryType = GmCommonClass.parseNull((String) hmParam.get("S_TYPE"));
    String strSurgeryDate = GmCommonClass.parseNull((String) hmParam.get("S_DATE"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("A_ID"));
    String strSurgeonID = GmCommonClass.parseNull((String) hmParam.get("S_ID"));
    String strSurgeryCompleteFl = GmCommonClass.parseNull((String) hmParam.get("S_FLAG"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    log.debug("Log Reason : " + strLogReason);
    GmDBManager gmDBManager = new GmDBManager();
    GmCommonBean gmCommonBean = new GmCommonBean();
    String strMessage = "";

    strSurgeryType = strSurgeryType.equals("0") ? "" : strSurgeryType;
    strSurgeonID = strSurgeonID.equals("0") ? "" : strSurgeonID;

    try {
      gmDBManager.setPrepareString("GM_UPDATE_PATIENT", 9);
      gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
      gmDBManager.setString(1, strPatientID);
      gmDBManager.setString(2, strStudyID);
      gmDBManager.setString(3, strSurgeryType);
      gmDBManager.setString(4, strSurgeryDate);
      gmDBManager.setString(5, strAccountID);
      gmDBManager.setString(6, strSurgeonID);
      gmDBManager.setString(7, strSurgeryCompleteFl);
      gmDBManager.setString(8, strUserId);
      gmDBManager.setString(9, strPatientMasterId);

      gmDBManager.execute();
      strPatientMasterId = gmDBManager.getString(9);

      log.debug("strPatientMasterId: " + strPatientMasterId);

      if (!strLogReason.equals("")) {
        gmCommonBean.saveLog(gmDBManager, strPatientMasterId, strLogReason, strUserId, "1230"); // 1230
                                                                                                // -
                                                                                                // Log
                                                                                                // Code
        // Number for Clinical
      }
      gmDBManager.commit();
    } catch (Exception ex) {
      log.error("Message : " + ex.getMessage());
      String strException = ex.getMessage();

      if (strException.indexOf("20053") > 0) {
        strMessage = "Patient IDE Number " + strPatientID + " already exists";
      } else {
        ex.printStackTrace();
        throw new AppError(ex);
      }
    }

    return strMessage;

  } // end of saveSalesRep

  public HashMap loadSiteMapInfo(String strStudySiteID, String strCondition) throws AppError {
    log.debug("Enter");
    HashMap hmReturn = new HashMap();

    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT t614.c614_study_site_id hsitemapid, t614.c611_study_id studylist, t614.c614_site_id siteid");
    sbQuery
        .append(" , t614.c704_account_id sitenamelist, TO_CHAR (t614.c614_irb_approval_date, 'MM/DD/YYYY') approvaldate");
    sbQuery
        .append(" , t614.c101_user_id craname, t614.c901_irb_type irbTypeName,TO_CHAR(t614.C614_STUDY_PKT_DATE,'MM/DD/YYYY') studyPktSent, t614.c614_tin_number tinnumber ");
    sbQuery.append(" FROM t614_study_site t614, t611_clinical_study t611");
    sbQuery.append(" WHERE t611.c611_study_id = t614.c611_study_id ");
    sbQuery.append(" AND t614.c614_study_site_id = '" + strStudySiteID
        + "' AND t614.c614_void_fl IS NULL  ");
    if (!strCondition.equals("")) {
      sbQuery.append(" and t611.C611_STUDY_ID in (select v621.C611_STUDY_ID");
      sbQuery.append(" from V621_PATIENT_STUDY_LIST v621 where");
      sbQuery.append(strCondition);
      sbQuery.append(")");
    }
    log.debug("query===" + sbQuery.toString());
    hmReturn = dbCon.querySingleRecord(sbQuery.toString());
    log.debug("Exit");
    return hmReturn;
  }

  public ArrayList loadSiteMapDetails(String strStudyID, String strCondition) throws AppError {
    log.debug("Enter");
    ArrayList alAccountList = new ArrayList();

    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT t614.c614_study_site_id ID, t614.c614_site_id siteid,  utl_i18n.raw_to_char(utl_raw.cast_to_raw(get_account_name (t614.c704_account_id)), 'utf8') sitename");
    sbQuery
        .append(" , TO_CHAR (t614.c614_irb_approval_date, 'MM/DD/YYYY') irbapprovaldate, t611.c611_study_id studyid");
    sbQuery
        .append(" , get_user_name (t614.c101_user_id) craname, get_code_name(t614.c901_irb_type) irbTypeName,TO_CHAR(t614.C614_STUDY_PKT_DATE,'MM/DD/YYYY') studyPktSent, t614.c614_tin_number tinNumber");
    sbQuery.append(" FROM t614_study_site t614, t611_clinical_study t611 ");
    sbQuery.append(" WHERE t611.c611_study_id = t614.c611_study_id ");
    sbQuery.append(" AND t611.C611_DELETE_FL = 'N'  ");
    sbQuery.append(" AND t611.c611_study_id = '" + strStudyID + "' AND t614.c614_void_fl IS NULL ");
    if (!strCondition.equals("")) {
      sbQuery.append(" AND t614.C614_STUDY_SITE_ID in (select v621.C614_STUDY_SITE_ID");
      sbQuery.append(" from V621_PATIENT_STUDY_LIST v621 where ");
      sbQuery.append(strCondition);
      sbQuery.append(")");
    }
    sbQuery.append(" ORDER BY siteid");
    log.debug("query===" + sbQuery.toString());
    alAccountList = dbCon.queryMultipleRecords(sbQuery.toString());

    return alAccountList;

  }

  /**
   * loadSiteFromStudy - This method will be used to load site list from the study
   * 
   * @param strStudyId - Study for which the site has to be fetched
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList loadSiteFromStudy(String strStudyId) throws AppError {
    ArrayList alSiteList = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_CR_FCH_SITE_LIST", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));
    gmDBManager.execute();
    alSiteList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alSiteList;
  }

  /**
   * loadSiteFromStudy - This method will be used to load site list from the study based on the
   * access condition
   * 
   * @param strStudyId - Study for which the site has to be fetched
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList loadSiteFromStudy(String strStudyId, String strCondition) throws AppError {
    ArrayList alSiteList = new ArrayList();
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append("SELECT distinct t614.c614_site_id siteid, t614.c614_study_site_id ID, t614.c704_account_id account_id, t614.c614_site_id ||'-' || get_account_name (t614.c704_account_id) NAME");
    sbQuery.append(" FROM t614_study_site t614");
    if (!strCondition.equals("")) {
      sbQuery.append(" , V621_PATIENT_STUDY_LIST v621");
    }
    sbQuery.append(" WHERE  t614.c611_study_id = '" + strStudyId + "'");
    sbQuery.append(" AND t614.c614_void_fl IS NULL ");
    if (!strCondition.equals("")) {
      sbQuery.append("and t614.C704_ACCOUNT_ID = v621.C704_ACCOUNT_ID and");
      sbQuery.append(strCondition);
    }
    // Fix for issue # 9748
    sbQuery.append(" ORDER BY t614.c614_site_id ");
    // log.debug("sbQuery.toString()" + sbQuery.toString());
    alSiteList = dbCon.queryMultipleRecords(sbQuery.toString());

    return alSiteList;
  }

  /**
   * loadStudyList - This method gets Invoice information for the selected PO and for the selected
   * Account
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList loadStudyList(String strCodeGrpParsed, String strCondition) throws AppError {
    ArrayList alStudyList = new ArrayList();
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.setLength(0);
    sbQuery.append("SELECT  C901_CODE_ID CODEID, C901_CODE_NM CODENM ");
    sbQuery.append(",C902_CODE_NM_ALT CODENMALT, C901_CONTROL_TYPE CONTROLTYP ");
    sbQuery.append(" FROM  T901_CODE_LOOKUP ");
    sbQuery.append(" WHERE  C901_CODE_GRP IN ('" + strCodeGrpParsed + "')");
    sbQuery.append(" AND C901_ACTIVE_FL = '1' ");
    if (!strCondition.equals("")) {
      sbQuery
          .append(" AND c902_code_nm_alt in (select v621.C611_STUDY_ID from V621_PATIENT_STUDY_LIST v621 where ");
      sbQuery.append(strCondition);
      sbQuery.append(")");
    }
    sbQuery.append(" ORDER BY C901_CODE_SEQ_NO ");
    log.debug("sbQuery.toString()" + sbQuery.toString());
    alStudyList = dbCon.queryMultipleRecords(sbQuery.toString());

    return alStudyList;
  } // End of loadStudyList

  public HashMap loadAccountDetails(String strStudySiteId) {

    HashMap hmResult = new HashMap();
    try {
      DBConnectionWrapper dbCon = null;
      dbCon = new DBConnectionWrapper();

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT C614_SITE_ID PID, GET_PARTY_NAME (C101_PARTY_ID) PINV ");
      sbQuery.append(" ,GET_RULE_VALUE(T614.C611_STUDY_ID,'CLINICALPATIENTSETUP') PDEFAULT ");
      sbQuery.append(" FROM T614_STUDY_SITE T614, T615_SITE_SURGEON T615 ");
      sbQuery.append(" WHERE T614.C614_STUDY_SITE_ID = T615.C614_STUDY_SITE_ID(+) ");
      sbQuery.append(" AND C901_SURGEON_TYPE(+) = '60200' "); // 60200 - principal investigator
      sbQuery.append(" AND T614.C614_STUDY_SITE_ID = '");
      sbQuery.append(strStudySiteId);
      sbQuery.append("'");

      log.debug(" Apple sbQuery.toString()" + sbQuery.toString());

      hmResult = dbCon.querySingleRecord(sbQuery.toString());

    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmPatientBean:loadAccountDetails", "Exception is:" + e);
    }
    log.debug("Exit");
    return hmResult;
  }

  /**
   * common method used to get the grid xml data
   * 
   * @date Feb 24, 2010
   * @param
   */

  public String getXmlGridData(HashMap hmParamV) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();

    ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
    String strStudyList = (String) hmParamV.get("STUDYLIST");
    String strDhtmlx = (String) hmParamV.get("DHTMLX");

    HashMap hmParam = new HashMap();
    hmParam.put("STUDYLIST", strStudyList);
    hmParam.put("DHTMLX", strDhtmlx);

    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);

    return templateUtil.generateOutput();

  }


  /**
   * fetchPatientDetail - This method will be used to select the patient details.
   * 
   * 
   * @exception AppError
   */

  public HashMap fetchPatientDetail(String strPatientIDE) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_cr_patient_track.gm_fch_patientmapping", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPatientIDE);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * savePatientDetail - This method will be used to update the patient details.
   * 
   * 
   * @exception AppError
   */


  public void savePatientDetail(HashMap hmValues) throws AppError {
    String strPatientId = GmCommonClass.parseNull((String) hmValues.get("PATIENTID"));
    String strFirstName = GmCommonClass.parseNull((String) hmValues.get("FIRSTNAME"));
    String strLastName = GmCommonClass.parseNull((String) hmValues.get("LASTNAME"));
    String strUserId = GmCommonClass.parseNull((String) hmValues.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_cr_patient_track.gm_sav_patient_details", 4);
    gmDBManager.setString(1, strPatientId);
    gmDBManager.setString(2, strFirstName);
    gmDBManager.setString(3, strLastName);
    gmDBManager.setString(4, strUserId);


    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchPatientDetail - This method will be used to select the patient details.
   * 
   * 
   * @exception AppError
   */

  public HashMap fetchPatientDetails(String strPatientId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_cr_common.gm_fch_patient_details", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPatientId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchSurgicalDetails - This method is used to get Surgical Intervention details.
   * 
   * @exception AppError
   */
  public HashMap fetchSurgicalDetails(String strSurgInvnId, String strPatientListId)
      throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cr_surg_intervention.gm_fch_surgical_inv", 4);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strSurgInvnId);
    gmDBManager.setString(2, strPatientListId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    hmReturn.put("VERIFYDETAILS", alReturn);
    return hmReturn;

  }

  /**
   * saveSurgicalDetails - This method is used to save Surgical Intervention details, to send email
   * to creation and verification emails
   * 
   * @exception AppError
   */
  public void saveSurgicalDetails(HashMap hmParam) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager();


    String strStudyName = "";
    String strMessage = "";
    String strUserName = "";
    String strCreatedDt = "";
    String strTreatment = "";
    String strSubject = "";

    String strSurgInvnId = GmCommonClass.parseNull((String) hmParam.get("SURGINVID"));
    String strPatientIdeNo = GmCommonClass.parseNull((String) hmParam.get("PATIENT_IDE_NO"));
    String strSurgType = GmCommonClass.parseNull((String) hmParam.get("SURGTYPE"));
    String strSurgOrgLvl = GmCommonClass.parseNull((String) hmParam.get("SURGORGLVL"));
    String strSurgDt = GmCommonClass.parseNull((String) hmParam.get("SURGDT"));
    String strVerifyFlg = GmCommonClass.parseNull((String) hmParam.get("VRFLG"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("SESSDATEFMT"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    Date surgDt = GmCommonClass.getStringToDate(strSurgDt, strDateFmt);
    gmDBManager.setPrepareString("gm_pkg_cr_surg_intervention.gm_sav_surgical_inv", 12);
    gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(11, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(12, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strSurgInvnId);
    gmDBManager.setString(2, strPatientIdeNo);
    gmDBManager.setString(3, strSurgType);
    gmDBManager.setString(4, strSurgOrgLvl);
    gmDBManager.setDate(5, surgDt);
    gmDBManager.setString(6, strVerifyFlg);
    gmDBManager.setString(7, strComments);
    gmDBManager.setString(8, strUserId);
    gmDBManager.execute();
    strUserName = GmCommonClass.parseNull(gmDBManager.getString(9));
    strCreatedDt = GmCommonClass.parseNull(gmDBManager.getString(10));
    strTreatment = GmCommonClass.parseNull(gmDBManager.getString(11));
    strStudyName = GmCommonClass.parseNull(gmDBManager.getString(12));

    gmDBManager.commit();
    if (strSurgInvnId.equals("") || (!strSurgInvnId.equals("") && strVerifyFlg.equals("Y"))) {
      GmEmailProperties emailProps = new GmEmailProperties();
      emailProps.setSender(GmCommonClass.getEmailProperty("GmSurgicalIntervention."
          + GmEmailProperties.FROM));
      emailProps.setRecipients(GmCommonClass.getEmailProperty("GmSurgicalIntervention."
          + GmEmailProperties.TO));
      emailProps.setCc(GmCommonClass.getEmailProperty("GmSurgicalIntervention."
          + GmEmailProperties.CC));
      emailProps.setMimeType(GmCommonClass.getEmailProperty("GmSurgicalIntervention."
          + GmEmailProperties.MIME_TYPE));
      emailProps.setSubject(GmCommonClass.getEmailProperty("GmSurgicalIntervention."
          + GmEmailProperties.SUBJECT));

      strSubject =
          GmCommonClass.getEmailProperty("GmSurgicalIntervention." + GmEmailProperties.SUBJECT);
      if (strSurgInvnId.equals(""))
        strMessage =
            GmCommonClass.getEmailProperty("GmSurgicalInterventionCR." + GmEmailProperties.MESSAGE);
      if (!strSurgInvnId.equals("") && strVerifyFlg.equals("Y"))
        strMessage =
            GmCommonClass.getEmailProperty("GmSurgicalInterventionVR." + GmEmailProperties.MESSAGE);

      strMessage = GmCommonClass.replaceAll(strMessage, "#<USER_NAME>", strUserName);
      strMessage = GmCommonClass.replaceAll(strMessage, "#<PATIENT_ID>", strPatientIdeNo);
      strMessage = GmCommonClass.replaceAll(strMessage, "#<DATE>", strCreatedDt);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<STUDY_NAME>", strStudyName);
      strMessage = GmCommonClass.replaceAll(strMessage, "#<TREATMENT>", strTreatment);
      emailProps.setMessage(strMessage);
      emailProps.setSubject(strSubject);
      GmCommonClass.sendMail(emailProps);
    }
  }

  /**
   * fetchSurgicalInvRpt - This method is used to get study/site level surgical intervention report.
   * 
   * @exception AppError
   */
  public ArrayList fetchSurgicalInvRpt(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    String strStudyId = "";
    String strSiteId = "";
    String strPtientIdeNo = "";
    String strVerifyFl = "";
    strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYID"));
    strSiteId = GmCommonClass.parseNull((String) hmParam.get("SITEID"));
    strPtientIdeNo = GmCommonClass.parseNull((String) hmParam.get("PATIENTIDENO"));
    strVerifyFl = GmCommonClass.parseNull((String) hmParam.get("VERIFYFL"));
    gmDBManager.setPrepareString("gm_pkg_cr_surg_intervention.gm_fch_surgical_inv_rpt", 5);

    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strStudyId);
    gmDBManager.setString(2, strSiteId);
    gmDBManager.setString(3, strPtientIdeNo);
    gmDBManager.setString(4, strVerifyFl);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();

    return alReturn;
  }
}
