package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmDemographicRptBean
{

    Logger log = GmLogger.getInstance(this.getClass().getName());
    /**
     * reportDGraph - This method used to fetch the Demographics Form details
     * 				Based on report name to get the different procedure and call
     * 
     * @param strStudyId
     * @param strSiteNames
     * @param param
     * @return
     * @throws AppError
     */
    public List reportDGraph(String strStudyId, String strSiteNames, String param) throws AppError
    {        
        log.debug("Enter" + strStudyId+" $ "+ strSiteNames );
        RowSetDynaClass rdPatientinfo = null;         
            GmDBManager gmDBManager = new GmDBManager();
            // PMT-50723: Amnios study Demographics form changes.
            if(strStudyId.equals("GPR009")){
            	gmDBManager.setPrepareString(getAmniosDGraphProcedure(param), 3);
            // PC-1802: Reflect study patient Data changes	
            }else if(strStudyId.equals("GPR010")){
            	gmDBManager.setPrepareString(getReflectDGraphProcedure(param), 3);
            }else{
            	gmDBManager.setPrepareString(getDGraphProcedure(param), 3);
            }
            
            gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
            gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));
            gmDBManager.setString(2, GmCommonClass.parseNull(strSiteNames));        

            gmDBManager.execute();                
            rdPatientinfo = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(3));        
            gmDBManager.close();
        log.debug("Exit" + rdPatientinfo.getRows());     
        return rdPatientinfo.getRows();
      }
    
    public List reportLevelTreated(String strStudyId, String strSiteNames, String param, String ptype) throws AppError
    {        
        log.debug("Enter" + strStudyId+" $ "+ strSiteNames );
        RowSetDynaClass rdPatientinfo = null;         
            GmDBManager gmDBManager = new GmDBManager();
            gmDBManager.setPrepareString(getDGraphProcedure(param), 4);
            gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
            gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));
            gmDBManager.setString(2, GmCommonClass.parseNull(strSiteNames));
            gmDBManager.setString(4, GmCommonClass.parseNull(ptype));
            gmDBManager.execute();                
            rdPatientinfo = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(3));        
            gmDBManager.close();
        log.debug("Exit" + rdPatientinfo.getRows());
        return rdPatientinfo.getRows();
      }
    
    public HashMap reportDiagnosis(String strStudyListId, String strSiteId) throws AppError
    {
        log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId);
      HashMap hmFinalValue = new HashMap();
      ResultSet rsHeader = null;
      ResultSet rsDetails = null;
      GmCrossTabReport gmCrossReport = new GmCrossTabReport();
      GmDBManager gmDBManager = new GmDBManager ();        
      
      try {
      // To fetch the transaction information
      gmDBManager.setPrepareString("GM_PKG_CR_DGRAPH_RPT.GM_CR_FCH_DIAGNOSIS_INFO",4);
      gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
      gmDBManager.setString(1,strStudyListId);
      gmDBManager.setString(2,strSiteId);
      gmDBManager.execute();
      rsHeader = (ResultSet)gmDBManager.getObject(3);
      rsDetails = (ResultSet)gmDBManager.getObject(4);
      gmCrossReport.setGroupFl(true);
     // gmCrossReport.setAcceptExtraColumn(true);
     // gmCrossReport.setExtraColumnParams(4," Avg");
     // gmCrossReport.setExtraColumnParams(6," <br>SD");
      hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetails);
      log.debug("Values finally formed " + hmFinalValue);
      gmDBManager.close();
      }
      
      catch(Exception exp){
          gmDBManager.close();
          throw new AppError(exp);
      }
      return hmFinalValue;
    }
    public List reportImplantInfo(String strStudyId, String strSiteNames, String strPartType) throws AppError
    {        
       
        
        
        RowSetDynaClass rdPatientinfo = null;         
            GmDBManager gmDBManager = new GmDBManager();
            gmDBManager.setPrepareString("GM_PKG_CR_DGRAPH_RPT.GM_CR_FCH_IMPLANT_INFO", 4);
            gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
            log.debug("1" + GmCommonClass.parseNull(strStudyId));
            gmDBManager.setString(1, GmCommonClass.parseNull(strStudyId));
            log.debug("2" + GmCommonClass.parseNull(strSiteNames));
            gmDBManager.setString(2, GmCommonClass.parseNull(strSiteNames));  
            log.debug("3" + GmCommonClass.parseNull(strPartType) );
            gmDBManager.setString(3, GmCommonClass.parseNull(strPartType));
            gmDBManager.execute();                
            rdPatientinfo = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(4));        
            gmDBManager.close();
        log.debug("Exit" + rdPatientinfo.getRows());
        return rdPatientinfo.getRows();
      }
    
    private String getDGraphProcedure(String param)
    {
        if(param.equals("rptPatientInfo"))
        {
            return "GM_PKG_CR_DGRAPH_RPT.GM_CR_FCH_PATIENT_INFO";
        }
        else if(param.equals("rptLevelTreated"))
        {
            return "GM_PKG_CR_DGRAPH_RPT.GM_CR_FCH_LEVEL_TREATED";
        }
        else if(param.equals("rptSurgicalResults"))
        {
            return "GM_PKG_CR_DGRAPH_RPT.GM_CR_FCH_SURGICAL_RESULT";  
        }
       return ""; 
    }
    
    /**
	 * common method used to get the grid xml data
	 * 
	 * @date June 06, 2010
	 * @param
	 */
	
	public String getXmlGridData(HashMap hmParamV) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();

		ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
		String strTemplate = (String) hmParamV.get("TEMPLATE");
		String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
		String strPartyType = (String) hmParamV.get("PARTYTYPE");
		

		HashMap hmParam = new HashMap();
		
		hmParam.put("PARTYTYPE", strPartyType);
		
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setDataMap("hmParam", hmParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);

		return templateUtil.generateOutput();
	}	
	
	
	/**
	 * getAmniosDGraphProcedure - This method used to return the Amnios study
	 * Demographic procedure. Based on the strOpt
	 * 
	 * @param strOpt
	 * @return
	 */

	private String getAmniosDGraphProcedure(String strOpt) {
		if (strOpt.equals("rptPatientInfo")) {
			return "gm_pkg_cr_dgraph_rpt.gm_cr_fch_amnios_patient_info";
		} else if (strOpt.equals("rptLevelTreated")) {
			return "gm_pkg_cr_dgraph_rpt.gm_cr_fch_level_treated";
		} else if (strOpt.equals("rptSurgicalResults")) {
			return "gm_pkg_cr_dgraph_rpt.gm_cr_fch_surgical_result";
		}
		return "";
	}
	
	/**
	 * getReflectDGraphProcedure - This method used to return the Reflect study
	 * Demographic procedure. Based on the strOpt
	 * 
	 * @param strOpt
	 * @return
	 */

	private String getReflectDGraphProcedure(String strOpt) {
		if (strOpt.equals("rptPatientInfo")) {
			return "gm_pkg_cr_dgraph_rpt.gm_cr_fch_reflect_patient_info";
		} else if (strOpt.equals("rptLevelTreated")) {
			return "gm_pkg_cr_dgraph_rpt.gm_cr_fch_level_treated";
		} else if (strOpt.equals("rptSurgicalResults")) {
			return "gm_pkg_cr_dgraph_rpt.gm_cr_fch_surgical_result";
		}
		return "";
	}
}
