/*
 * Author: Hardik Parikh Project: Globus Medical App
 */

package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalPaymentBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to

  // Initialize
  // the
  // Logger
  // Class.

  /**
   * loadSiteByStudySiteId
   * 
   * @param strCodeNmAlt String
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList loadSiteByStudySiteId(String strStudySiteId) throws AppError {
    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    sbQuery
        .append("SELECT  c704_account_id ID,get_account_name (c704_account_id) name from t614_study_site where c614_study_site_id='");
    sbQuery.append(strStudySiteId);
    sbQuery.append("' and c614_void_fl is NULL");

    log.debug("sbQuery.toString()" + sbQuery.toString());
    arList = dbCon.queryMultipleRecords(sbQuery.toString());
    return arList;
  }

  /**
   * getCodeListByAlt
   * 
   * @param strCodeNmAlt String
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList getCodeListByAlt(String strCodeNmAlt) throws AppError {
    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    sbQuery.append("SELECT  C901_CODE_ID CODEID, C901_CODE_NM CODENM ");
    sbQuery.append(",C902_CODE_NM_ALT CODENMALT, C901_CONTROL_TYPE CONTROLTYP ");
    sbQuery.append(" FROM  T901_CODE_LOOKUP ");
    sbQuery.append(" WHERE  C902_CODE_NM_ALT ='");
    sbQuery.append(strCodeNmAlt);
    sbQuery.append("'");
    sbQuery.append("AND C901_ACTIVE_FL='1' ORDER BY C901_CODE_SEQ_NO ");
    log.debug("sbQuery.toString()" + sbQuery.toString());
    arList = dbCon.queryMultipleRecords(sbQuery.toString());

    return arList;
  }

  /**
   * loadSiteCordinator
   * 
   * @param strStudySiteId String
   * @exception AppError
   * @return ArrayList object
   */

  public ArrayList loadSiteCordinator(String strStudySiteId) throws AppError {
    log.debug("Enter loadSiteCordinator");
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_CR_COMMON.gm_fch_site_coordinator", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(strStudySiteId));
    gmDBManager.execute();
    ArrayList alSiteCordinatorList =
        gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alSiteCordinatorList :" + alSiteCordinatorList);
    log.debug("Exit loadSiteCordinator");
    return alSiteCordinatorList;
  }

  /**
   * loadSurgeonMapDetails
   * 
   * @param strStudySiteId String
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList loadSurgeonMapDetails(String strStudySiteId) throws AppError {
    log.debug("Enter loadSurgeonMapDetails");
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_CR_COMMON.GM_CR_FCH_SURGEON_MAP_DETAILS", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(strStudySiteId));
    gmDBManager.execute();
    ArrayList alSurgonList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alSurgonList :" + alSurgonList);
    log.debug("Exit loadSurgeonMapDetails");
    return alSurgonList;
  }

  /**
   * fetchPayment
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchPayment(HashMap hmParam) throws AppError {

    log.debug("Enter fetchPayment");
    log.debug(" fetchPayment hmParam :" + hmParam);

    HashMap hmReturn = new HashMap();

    String strPaymentId = GmCommonClass.parseNull((String) hmParam.get("PAYMENTREQUESTID"));
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_CR_PYT.GM_CR_FCH_PAYMENT", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPaymentId);
    gmDBManager.execute();
    ArrayList alPaymentList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    ArrayList alPaymentDetailList =
        gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    log.debug("alPaymentList :" + alPaymentList);
    log.debug("alPaymentDetailList :" + alPaymentDetailList);

    hmReturn.put("PAYMENT", alPaymentList);
    hmReturn.put("PAYMENTDETAIL", alPaymentDetailList);
    log.debug("Exit fetchPayment");
    return hmReturn;

  } // End of fetchPayment


  /**
   * saveClinicalPayment
   * 
   * @param HashMap
   * @return String
   * @exception AppError
   */
  public String saveClinicalPayment(HashMap hmParam) throws AppError {

    

    String strStudyId = GmCommonClass.parseNull((String) hmParam.get("SESSSTUDYID"));
    log.debug("saveClinicalPayment Study ID is :" + strStudyId);
    String strStudySiteId = GmCommonClass.parseNull((String) hmParam.get("SESSSTUDYSITEID"));
    String strRequestTypeId = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPEID"));
    String strPaymentToId = GmCommonClass.parseNull((String) hmParam.get("PAYMENTTOID"));
    String strPayable = GmCommonClass.parseNull((String) hmParam.get("PAYABLE"));
    String strPaymentDate = GmCommonClass.parseNull((String) hmParam.get("PAYMENTDATE"));
    log.debug("saveClinicalPayment strPaymentToId is :" + strPaymentToId);
    String strCheckNumber = GmCommonClass.parseNull((String) hmParam.get("CHECKNUMBER"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strStatusId = GmCommonClass.parseNull((String) hmParam.get("STATUSID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPaymentDetailData =
        GmCommonClass.parseNull((String) hmParam.get("PAYMENTDETAILDATA"));
    String strPaymentRequestId = GmCommonClass.parseNull((String) hmParam.get("PAYMENTREQUESTID"));
    String strTinNumber = GmCommonClass.parseNull((String) hmParam.get("TINNUMBER"));
    String strTinReason = GmCommonClass.parseNull((String) hmParam.get("TINREASON"));

    if (strPaymentRequestId != null && strPaymentRequestId.equalsIgnoreCase("")) {
      strPaymentRequestId = null;
    }
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_CR_PYT.gm_cr_sav_payment", 14);
    gmDBManager.registerOutParameter(14, java.sql.Types.VARCHAR);
    gmDBManager.setString(1, strStudyId);
    gmDBManager.setString(2, strStudySiteId);
    gmDBManager.setString(3, strRequestTypeId);
    gmDBManager.setString(4, strPaymentToId);
    gmDBManager.setString(5, strPayable);
    gmDBManager.setString(6, strPaymentDate);
    gmDBManager.setString(7, strCheckNumber);
    gmDBManager.setString(8, strComments);
    gmDBManager.setString(9, strStatusId);
    gmDBManager.setString(10, strUserId);
    gmDBManager.setString(11, strPaymentDetailData);
    gmDBManager.setString(12, strTinNumber);
    gmDBManager.setString(13, strTinReason);
    gmDBManager.setString(14, strPaymentRequestId);


    gmDBManager.execute();
    strPaymentRequestId = gmDBManager.getString(14);
    log.debug("Returning strPaymentRequestId=>" + strPaymentRequestId);
    gmDBManager.commit();
    log.debug("Exit saveClinicalPayment");

    return strPaymentRequestId;
  } // End of saveSiteMtrObservation


  /**
   * Fetches the loadPaymentTrackingDetailsReport detail
   * 
   * 
   * @param HashMap hmParam
   * @param String strCondition
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList loadPaymentTrackingDetailsReport(HashMap hmParam, String strCondition)
      throws AppError {

    log.debug("loadPaymentTrackingReport");

    ArrayList alResult = new ArrayList();

    

    String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYLISTID"));
    String strSiteList = GmCommonClass.parseNull((String) hmParam.get("SITELIST"));
    String strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPE"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strReasonForPayment = GmCommonClass.parseNull((String) hmParam.get("REASONFORPAYMENT"));
    String strPaymentId = GmCommonClass.parseNull((String) hmParam.get("PAYMENTID"));
   // String strTinNumber = GmCommonClass.parseNull((String) hmParam.get("TINNUMBER"));

    if (strSiteList.equals("") | strSiteList.equals("0"))
      strSiteList = null;
    if (strRequestType.equals("") | strRequestType.equals("0"))
      strRequestType = null;
    if (strStatus.equals("") | strStatus.equals("0"))
      strStatus = null;
    if (strReasonForPayment.equals("") | strReasonForPayment.equals("0"))
      strReasonForPayment = null;
    if (strPaymentId.equals("") | strPaymentId.equals("0"))
      strPaymentId = null;


    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cr_pyt.gm_cr_fch_payment_detail_rpt", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.setString(1, strStudyId);
    gmDBManager.setString(2, strSiteList);
    gmDBManager.setString(3, strRequestType);
    gmDBManager.setString(4, strReasonForPayment);
    gmDBManager.setString(5, strStatus);
    gmDBManager.setString(6, strPaymentId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    gmDBManager.close();
    log.debug("Exit size loadPaymentTrackingDetailsReport==========>" + alResult);
    return alResult;

  }

  /**
   * Fetches the loadPaymentTrackingReport detail
   * 
   * 
   * @param HashMap hmParam
   * @param String strCondition
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList loadPaymentTrackingReport(HashMap hmParam, String strCondition) throws AppError {

    log.debug("loadPaymentTrackingReport");

    ArrayList alResult = new ArrayList();
    
    String strStudyId = GmCommonClass.parseNull((String) hmParam.get("STUDYLISTID"));
    String strSiteList = GmCommonClass.parseNull((String) hmParam.get("SITELIST"));
    String strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPE"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
   // String strTinNumber = GmCommonClass.parseNull((String) hmParam.get("TINNUMBER"));

    if (strSiteList.equals("") | strSiteList.equals("0"))
      strSiteList = null;
    if (strRequestType.equals("") | strRequestType.equals("0"))
      strRequestType = null;
    if (strStatus.equals("") | strStatus.equals("0"))
      strStatus = null;


    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cr_pyt.gm_cr_fch_payment_rpt", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strStudyId);
    gmDBManager.setString(2, strSiteList);
    gmDBManager.setString(3, strRequestType);
    gmDBManager.setString(4, strStatus);
   
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    
    log.debug("alresult=============>"+alResult);
    gmDBManager.close();
    log.debug("Exit size" + alResult);
    return alResult;

  }


  /**
   * common method used to get the grid xml data
   * 
   * @date Feb 24, 2010
   * @param
   */
  public String getXmlGridData(HashMap hmParamV) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();

    ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");

    templateUtil.setDataList("alResult", alParam);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    return templateUtil.generateOutput();
  }

  public HashMap fetchPaymentPrintDetails(String strPaymentRequestId) throws Exception {
    ArrayList alResult = new ArrayList();
    HashMap hmHeader = new HashMap();
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cr_pyt.gm_cr_fch_payment_print", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPaymentRequestId);

    gmDBManager.execute();
    hmHeader = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    hmReturn.put("HEADER", hmHeader);
    hmReturn.put("DETAILS", alResult);

    log.debug("Exit size" + alResult.size());

    return hmReturn;
  }

  /**
   * Fetches the loadAccountList detail
   * 
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */

  public ArrayList loadAccountList() throws AppError {
    ArrayList alAccountList = new ArrayList();
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT C902_CODE_NM_ALT CODEID, C902_CODE_NM_ALT || ' - '|| C901_CODE_NM CODENM  ");
    sbQuery.append(" FROM T901_CODE_LOOKUP WHERE C901_CODE_GRP='CLRMB' ");
    alAccountList = dbCon.queryMultipleRecords(sbQuery.toString());
    log.debug("GlAccount Query: " + sbQuery.toString());
    return alAccountList;
  } // End of loadAccountList
}// end of class GmClinicalIRBReportBean
