package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 


public class GmDataCollectionRptBean
{
Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

    /**
      * loadExpActByTreatment - This Method is used to fetch Expected and Actual Follw-up by treatment
      * @param String strStudyListId, String strSiteId  
      * @return HashMap
      * @exception AppError
    **/
      public HashMap reportCrossTabSetDetails(String strStudyListId, String strSiteId) throws AppError
      {
          log.debug(" strStudyListId " + strStudyListId + " strSiteId -- " +strSiteId);
        HashMap hmExpected = new HashMap();    
        HashMap hmActual = new HashMap();   
        HashMap hmReturn = new HashMap();
        ResultSet rsHeader = null;
        ResultSet rsHeaderActual = null;
        ResultSet rsDetailsExpected = null;
        ResultSet rsDetailsActual = null;
        GmCrossTabReport gmCrossReport = new GmCrossTabReport();
        GmDBManager gmDBManager = new GmDBManager ();        
        
        try {
	        // To fetch header information
	        gmDBManager.setPrepareString("gm_pkg_cr_dcoll_rpt.gm_cr_fch_header_exp_act",2);
	        gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
	        gmDBManager.setString(2,strStudyListId);
	        gmDBManager.execute();
	        rsHeader = (ResultSet)gmDBManager.getObject(1);
	        
	        gmDBManager.setPrepareString("gm_pkg_cr_dcoll_rpt.gm_cr_fch_header_exp_act",2);
	        gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
	        gmDBManager.setString(2,strStudyListId);	        
	        gmDBManager.execute();
	        rsHeaderActual = (ResultSet)gmDBManager.getObject(1);
	        
	        // To fetch the transaction information
	        gmDBManager.setPrepareString("gm_pkg_cr_dcoll_rpt.gm_cr_fch_exp_act_treatment",4);
	        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
	        gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
	        gmDBManager.setString(1,strStudyListId);
	        gmDBManager.setString(2,strSiteId);
	        gmDBManager.execute();
	        rsDetailsExpected = (ResultSet)gmDBManager.getObject(3);
	        rsDetailsActual = (ResultSet)gmDBManager.getObject(4);
	        gmCrossReport.setGroupFl(true);
	        hmExpected = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);
	        hmActual = gmCrossReport.GenerateCrossTabReport(rsHeaderActual,rsDetailsActual);
	        log.debug("Values finally formed " + hmExpected);
	        gmDBManager.close();
	        hmReturn.put("EXPECTED",hmExpected);
	        hmReturn.put("ACTUAL",hmActual);
        }
        
        catch(Exception exp){
            gmDBManager.close();
            throw new AppError(exp);
        }
        return hmReturn;
        
      }
      
      /**
       * loadOutOfWindow - This Method is used to Load Out of Window
       * @param String strSiteId  
       * @return HashMap
       * @exception AppError
     **/
       public HashMap loadOutOfWindow(String strStudyListId,String strSiteId) throws AppError
       {
          
         log.debug( " strSiteId -- " +strSiteId);
         HashMap hmFinalValue = new HashMap();  
         HashMap hmLinkInfo = new HashMap();
         ArrayList alPatientInfo = new ArrayList();
         ResultSet rsHeader = null;
         ResultSet rsPeriod = null;
         GmCrossTabReport gmCrossReport = new GmCrossTabReport();
         GmDBManager gmDBManager = new GmDBManager ();  
         
         try {
         // To fetch the transaction information
         gmDBManager.setPrepareString("gm_pkg_cr_dcoll_rpt.gm_cr_fch_out_of_window",5);
         gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
         gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
         gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
         gmDBManager.setString(1,strStudyListId);
         gmDBManager.setString(2,strSiteId);
         gmDBManager.execute();
         rsHeader = (ResultSet)gmDBManager.getObject(3);
         rsPeriod = (ResultSet)gmDBManager.getObject(5);
         alPatientInfo = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
         
         hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsPeriod);
         gmDBManager.close();
         hmLinkInfo = getLinkInfo();
         gmCrossReport.setSkipExtraColumn(true);
         hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alPatientInfo, hmLinkInfo);
           }
           
           catch(Exception exp){
               gmDBManager.close();
               throw new AppError(exp);
           }
         return hmFinalValue;
       }

       /**
        * getLinkInfo - This Method is used to to get the link object information    
        * @return HashMap
        * @exception AppError
        **/
        private HashMap getLinkInfo()
        {
            HashMap hmLinkDetails = new HashMap();
            HashMap hmapValue = new HashMap();
            ArrayList alLinkList = new ArrayList();
            
            // First parameter holds the link object name         
            hmLinkDetails.put("LINKID", "ID");
            
            // Parameter to be added to link list 
           /* hmapValue = new HashMap();
            hmapValue.put("KEY","NAME");
            hmapValue.put("VALUE", "Pt #");
            alLinkList.add(hmapValue); */
            
            hmapValue = new HashMap();
            hmapValue.put("KEY","SURGEONNAME");
            hmapValue.put("VALUE", "Surgeon Name");
            alLinkList.add(hmapValue);
            
            hmapValue = new HashMap();
            hmapValue.put("KEY","TREATMENT");
            hmapValue.put("VALUE", "Treatment");
            alLinkList.add(hmapValue);
            hmLinkDetails.put("POSITION","BEFORE");
    
            hmLinkDetails.put("LINKVALUE", alLinkList);
            
            return hmLinkDetails;
        }   
        
        /**
         * loadMissingFollowUp - This Method is used to load Missing follow up data
         * @param String strSiteId  
         * @return HashMap
         * @exception AppError
       **/
         public HashMap loadMissingFollowUp(String strStudyListId,String strSiteId) throws AppError
         {
             GmDBManager gmDBManager = new GmDBManager ();
             HashMap hmFinalValue = new HashMap();
             try {
                 log.debug( " strSiteId -- " +strSiteId);
           HashMap hmLinkInfo = new HashMap();
           ArrayList alPatientInfo = new ArrayList();
           ResultSet rsHeader = null;
           ResultSet rsPeriod = null;
           GmCrossTabReport gmCrossReport = new GmCrossTabReport();
                   
         
           // To fetch the transaction information
           gmDBManager.setPrepareString("gm_pkg_cr_dcoll_rpt.gm_cr_fch_missing_follow_up",5);
           gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
           gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
           gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
           gmDBManager.setString(1,strStudyListId);
           gmDBManager.setString(2,strSiteId);
           gmDBManager.execute();
           rsHeader = (ResultSet)gmDBManager.getObject(3);
           rsPeriod = (ResultSet)gmDBManager.getObject(5);
           alPatientInfo = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
           
           hmFinalValue = gmCrossReport.GenerateCrossTabReport(rsHeader,rsPeriod);
           gmDBManager.close();
           hmLinkInfo = getLinkInfo();
           gmCrossReport.setSkipExtraColumn(true);
           hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alPatientInfo, hmLinkInfo);
             }
             
             catch(Exception exp){
                 gmDBManager.close();
                 throw new AppError(exp);
             }
           return hmFinalValue;
         }
}
