package com.globus.clinical.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;

/**
 * This class used to load Amnios study primary end point details Called from
 * spring bean interface
 * 
 * @author mmuthusamy
 *
 */
public class GmAmniosOutComeRptBean implements GmClinicalInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.globus.clinical.beans.GmClinicalInterface#loadPrimaryEndpointRpt(
	 * java.lang.String, java.lang.String, java.lang.String)
	 * 
	 * This method used to load the primary end point report - based on study,
	 * site and score values
	 * 
	 * Study - SECURE-C, TRIUMPH and Amnios
	 * 
	 * @param strstudyId
	 * @param strStudySiteIds
	 * @param strScoreType
	 * @return HashMap
	 * @throws AppError
	 * 
	 */

	@Override
	public HashMap loadPrimaryEndpointRpt(String strstudyId,
			String strStudySiteIds, String strScoreType) throws AppError {
		// TODO Auto-generated method stub

		log.debug(" Amnios strStudyListId " + strstudyId + " strSiteId -- "
				+ strStudySiteIds + " strSiteId -- " + strScoreType);
		HashMap hmFinalValue = new HashMap();

		ResultSet rsHeader = null;
		ResultSet rsDetails = null;
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		GmDBManager gmDBManager = new GmDBManager();

		// To fetch the transaction information
		gmDBManager.setPrepareString(
				"gm_pkg_cr_outcome_rpt.gm_cr_fch_amnios_primary_endpoint_dtls", 5);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.setString(1, strstudyId);
		gmDBManager.setString(2, strStudySiteIds);
		gmDBManager.setString(3, strScoreType);
		gmDBManager.execute();
		rsHeader = (ResultSet) gmDBManager.getObject(4);
		rsDetails = (ResultSet) gmDBManager.getObject(5);
		gmCrossReport.setGroupFl(true);
		gmCrossReport.setAcceptExtraColumn(true);
		gmCrossReport.setExtraColumnParams(4, "  <br>Avg");
		gmCrossReport.setExtraColumnParams(6, " <br>" + (char) 177 + "SD");
		hmFinalValue = gmCrossReport
				.GenerateCrossTabReport(rsHeader, rsDetails);
		gmDBManager.close();

		return hmFinalValue;
	}

}
