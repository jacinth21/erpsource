package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalSnapshotBean;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalNavigationBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalAction extends GmAction
{

	// Code to Initialize the Logger Class.
    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strCondition = "";
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    		{   
    			GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    			gmStudyFilterForm.loadSessionParameters(request);
    			HttpSession session = request.getSession(false);
    			String strNodeId = "";
    			String strPgToLd = "";
    			String strAccrd = "";
    			String strContext = "";
    			String strNodeOpt = "";
    			String strSessPgToLoad = "";
		    	String strSessAccLvl = (String)session.getAttribute("strSessAccLvl");
		    	strSessPgToLoad = (String)session.getAttribute("strSessPgToLoad");
		    	strNodeId = (String) request.getParameter("strNodeId");
		    	strPgToLd = (String) request.getParameter("strPgToLoad");
		    	strAccrd = (String) request.getParameter("strAccrd");
		    	strContext = (String) request.getParameter("strContext");
		    	strNodeOpt = (String) request.getParameter("strOpt");
		    	/*
		    	log.debug("strSessAccLvl==="+strSessAccLvl);
		    	log.debug("strSessPgToLoad==="+strSessPgToLoad);
		    	log.debug("strNodeId==="+strNodeId);
		    	log.debug("strPgToLd==="+strPgToLd);
		    	log.debug("strAccrd==="+strAccrd);
		    	log.debug("strContext==="+strContext);
		    	*/
		    	session.setAttribute("strSessNodeId", strNodeId);
		    	session.setAttribute("strSessLastClicked", strPgToLd);
		    	session.setAttribute("strSessAccrd", strAccrd);	
		    	session.setAttribute("strSessContext", strContext);
		    	session.setAttribute("strSessOptLastClicked", strNodeOpt);
		    	session.setAttribute("strSessClickedUrl", "");
		    	if (strSessAccLvl.equals("4") &&  strPgToLd.indexOf("GmEnterprisePortal")> 0)
		    	{
		    		session.setAttribute("strSessPgToLoad", "");
		    		session.setAttribute("strSessSubMenu", "");
		    		session.setAttribute("strSessLastClicked", "");
		    		session.setAttribute("strSessNodeId", "");
		    	}
		    	return null;
    		}  
}
