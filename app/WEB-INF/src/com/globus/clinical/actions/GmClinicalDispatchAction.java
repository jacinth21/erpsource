package com.globus.clinical.actions;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import com.globus.common.beans.AppError;

import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmCommonClinicalRptForm;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


public class GmClinicalDispatchAction extends GmDispatchAction
{

    String strStudyId = "";
    String strOpt = "Load";
    ArrayList alStudyList = new ArrayList();
    ArrayList alSiteListFromStudy = new ArrayList();
    ArrayList alQuestionnaire = new ArrayList();
    ArrayList alScore = new ArrayList();
    GmStudyFilterForm gmStudyFilterForm = null;
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    GmCommonBean gmCommonBean = new GmCommonBean();
    /**
     * 
     * 
     * @author rshah
     * @date Feb 1, 2010	
     * @param ActionForm, String
     * @return void
     * @description To load the study and site list.
     */
    
    public void setIncludeInfo(ActionForm actionForm, String strCondition) throws AppError
    {
    	gmStudyFilterForm = (GmStudyFilterForm)actionForm;
        strOpt = gmStudyFilterForm.getStrOpt();
        strStudyId = gmStudyFilterForm.getSessStudyId();
        GmPatientBean gmPatientBean = new GmPatientBean();
        gmStudyFilterForm.setAlStudyList(gmPatientBean.loadStudyList(gmStudyFilterForm.getStudyListGrp(),strCondition));
        alStudyList = gmStudyFilterForm.getAlStudyList();
        if(!strStudyId.equals("")){
            alSiteListFromStudy = gmPatientBean.loadSiteFromStudy(strStudyId,strCondition); 
           
            gmStudyFilterForm.setAlSiteNameList(alSiteListFromStudy);
            
            //Fetching Question Group
            String questionairreGrp = GmCommonClass.getRuleValue(strStudyId,"PEP_QUESTIONNAIRE");
            gmStudyFilterForm.setAlQuestionnairre(GmCommonClass.getCodeList(questionairreGrp));
            
           	//Fetching Score Group
            String scoreGrp = GmCommonClass.getRuleValue(gmStudyFilterForm.getQuestionnairre(),"PEP_SCORE");
            gmStudyFilterForm.setAlScore(GmCommonClass.getCodeList(scoreGrp));
            
            if(gmStudyFilterForm.getQuestionnairre().equalsIgnoreCase("60803") ){
            	gmStudyFilterForm.setAlTreatment(GmCommonClass.getCodeList("SURTP,SUEXT"));
            }else if(gmStudyFilterForm.getQuestionnairre().equalsIgnoreCase("60903") ){
            	gmStudyFilterForm.setAlTreatment(GmCommonClass.getCodeList("SUTRI"));
            }
        }        
    }
    public void setIncludeInfo(ActionForm actionForm) throws AppError
    {
    	String strCondition = "";
    	setIncludeInfo(actionForm,strCondition);
       
    }    
    /**
     * setDefaultDate - Load the month and year details and set the Default date
     * @param gmCommonClinicalRptForm - set the default values in this object
     * @param iSubFromYear - subtract the value in this param from the current year and set the 'from year' value
     */
    protected void setDefaultDate(GmCommonClinicalRptForm gmCommonClinicalRptForm, int iSubFromYear)
    {
        if(gmCommonClinicalRptForm.getFromMonth().equals(""))
        {
            int curMonth = GmCalenderOperations.getCurrentMonth();
            String strCurMonth = curMonth < 10 ? "0"+curMonth : ""+curMonth;            
            gmCommonClinicalRptForm.setFromMonth(strCurMonth);
            gmCommonClinicalRptForm.setToMonth(strCurMonth);
            gmCommonClinicalRptForm.setFromYear(String.valueOf(GmCalenderOperations.getCurrentYear() -iSubFromYear ));
            gmCommonClinicalRptForm.setToYear(String.valueOf(GmCalenderOperations.getCurrentYear()));                
        }
        gmCommonClinicalRptForm.setAlMonthList(GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()));
        gmCommonClinicalRptForm.setAlYearList(GmCommonClass.parseNullArrayList(gmCommonBean.getYearList()));        
    } 
}
