package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;

import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;

public class GmSiteCoordinatorReportAction extends GmClinicalDispatchAction{

	/**
	 * Fetches the fetchSiteCoordinatorReport detail 
	 * 
	 * 
	 * @param ActionMapping mapping 
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws com.globus.common.beans.AppError
	 */
    public ActionForward fetchSiteCoordinatorReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
		log.debug("Inside fetchSiteCoordinatorReport");	
		String strForward = "gmSiteCoordinatorReport";
		String strStudyId = "";
 	    String strXmlGridData = "";
		HashMap hmParam = new HashMap();
		log.debug("strForward  " + strForward);	
		String strCondition = getAccessFilter(request, response);
		GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm) form;
		gmStudyFilterForm.loadSessionParameters(request);

        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
		log.debug("strStudyId  " + strStudyId);
        gmStudyFilterForm.setStudyListId(strStudyId);
		
		hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		HashMap hmParamV = new HashMap();
		ArrayList alResult;
		alResult = (ArrayList)gmClinicalBean.loadSiteCoordinatorReport(hmParam);
        
        hmParamV.put("TEMPLATE","GmSiteCoordinatorReport.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);

        strXmlGridData = gmClinicalBean.getXmlGridData(hmParamV);
        gmStudyFilterForm.setGridXmlData(strXmlGridData);


		return mapping.findForward(strForward);
    }

}
