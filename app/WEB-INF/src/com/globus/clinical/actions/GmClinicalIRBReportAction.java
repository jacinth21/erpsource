package com.globus.clinical.actions;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.globus.clinical.beans.GmClinicalIRBReportBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmClinicalIRBReportForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalIRBReportAction extends GmAction
{

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   	
        
        GmClinicalIRBReportForm gmClinicalIRBReportForm = (GmClinicalIRBReportForm)form;
        gmClinicalIRBReportForm.loadSessionParameters(request);        
        String strOpt = gmClinicalIRBReportForm.getStrOpt();
        log.debug("strOpt :"+strOpt);
        gmClinicalIRBReportForm.setMsg("");    
        String strStudyId=(String)request.getSession().getAttribute("strSessStudyId");
        log.debug("Selected Study Id is : "+strStudyId);
        
        if(strOpt.equals("load_irb_sumry_rpt") || strOpt.equals("search_summary_rpt")){        	
        	log.debug("Search IRB Summary Report");
        	loadIRBSummaryReport(gmClinicalIRBReportForm,strStudyId);
        	ArrayList alIRBSummaryList= searchIRBSummaryReport(gmClinicalIRBReportForm);        	
        	String strIRBVmFile = "GmClinicalIRBSummaryReport.vm";
        	generateOutPut(gmClinicalIRBReportForm,alIRBSummaryList,strIRBVmFile);
            return mapping.findForward("loadIRBSumRpt");
        }else if(strOpt.equals("load_irb_type_rpt") || strOpt.equals("search_irb_type_rpt")){        	
        	log.debug("Search IRB Type Report");
        	loadIRBTypeReport(gmClinicalIRBReportForm,strStudyId);
        	ArrayList alIRBTypeList = searchIRBTypeReport(gmClinicalIRBReportForm);
        	String strIRBVmFile = "GmClinicalIRBTypeReport.vm";
        	generateOutPut(gmClinicalIRBReportForm,alIRBTypeList,strIRBVmFile);
        	return mapping.findForward("loadIRBTypeRpt");        
        }else if(strOpt.equals("load_irb_event_rpt") || strOpt.equals("search_irb_event_rpt")){
        	log.debug("Search IRB Event Report");
        	loadIRBEventReport(gmClinicalIRBReportForm,strStudyId);
        	ArrayList alIRBTypeList = searchIRBEventReport(gmClinicalIRBReportForm);
        	String strIRBVmFile = "GmClinicalIRBEventReport.vm";
        	generateOutPut(gmClinicalIRBReportForm,alIRBTypeList,strIRBVmFile);
        	return mapping.findForward("loadIRBEventRpt");
        }else if(strOpt.equals("load_irb_media_rpt") || strOpt.equals("search_irb_media_rpt")){
        	log.debug("Search IRB Event Report");
        	loadIRBMediaReport(gmClinicalIRBReportForm,strStudyId);
        	ArrayList alIRBTypeList = searchIRBMediaReport(gmClinicalIRBReportForm);
        	String strIRBVmFile = "GmClinicalIRBMediaReport.vm";
        	generateOutPut(gmClinicalIRBReportForm,alIRBTypeList,strIRBVmFile);
        	return mapping.findForward("loadIRBMediaRpt");
        }else{
        	loadIRBSummaryReport(gmClinicalIRBReportForm,strStudyId);
        	return mapping.findForward("loadIRBSumRpt");
        }    
        
    }   
    /**
	 * searchIRBTypeReport
	 * @param GmClinicalIRBReportForm  
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList searchIRBTypeReport(GmClinicalIRBReportForm gmClinicalIRBReportForm) throws AppError{
    	
	    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalIRBReportForm);
	    log.debug("searchIRBTypeReport getHashMapFromForm hmParam :"+hmParam);    	    
	    GmClinicalIRBReportBean gmClinicalIRBReportBean = new GmClinicalIRBReportBean();	    
	    ArrayList alIRBTypeList = gmClinicalIRBReportBean.fetchIRBApprovalByType(hmParam);	    
		return alIRBTypeList;
    }
    /**
	 * searchIRBTypeReport
	 * @param GmClinicalIRBReportForm 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList searchIRBSummaryReport(GmClinicalIRBReportForm gmClinicalIRBReportForm) throws AppError{
	    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalIRBReportForm);
	    log.debug("searchIRBTypeReport getHashMapFromForm hmParam :"+hmParam);   	        	    
	    GmClinicalIRBReportBean gmClinicalIRBReportBean = new GmClinicalIRBReportBean();
	    ArrayList alIRBSummaryList = gmClinicalIRBReportBean.fetchIRBSummary(hmParam);
		return alIRBSummaryList;
    }
    /**
	 * searchIRBEventReport
	 * @param GmClinicalIRBReportForm 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList searchIRBEventReport(GmClinicalIRBReportForm gmClinicalIRBReportForm) throws AppError{
	    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalIRBReportForm);
	    log.debug("searchIRBEventReport getHashMapFromForm hmParam :"+hmParam);    	    
	    GmClinicalIRBReportBean gmClinicalIRBReportBean = new GmClinicalIRBReportBean();
	    ArrayList alIRBEventList = gmClinicalIRBReportBean.fetchIRBByEvent(hmParam);	    
		return alIRBEventList;
    }
    /**
	 * searchIRBMediaReport 
	 * @param GmClinicalIRBReportForm
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList searchIRBMediaReport(GmClinicalIRBReportForm gmClinicalIRBReportForm) throws AppError{
    	
	    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalIRBReportForm);
	    log.debug("searchIRBMediaReport getHashMapFromForm hmParam :"+hmParam);    	    
	    GmClinicalIRBReportBean gmClinicalIRBReportBean = new GmClinicalIRBReportBean();	    
	    ArrayList alIRBTypeList = gmClinicalIRBReportBean.fetchIRBByMediaType(hmParam);	    
		return alIRBTypeList;
    }
    /**
     * loadIRBSummaryReport
	 * @param GmClinicalIRBReportForm
	 * @param String studyId
     * @exception AppError
     */
    public void loadIRBSummaryReport(GmClinicalIRBReportForm gmClinicalIRBReportForm,String strStudyId) throws AppError{    	
   		
    	log.debug("Inside loadIRBSummaryReport");
   		GmPatientBean gmPatientBean = new GmPatientBean();
   		ArrayList alSiteList = gmPatientBean.loadSiteFromStudy(strStudyId);
   		gmClinicalIRBReportForm.setAlSiteList(alSiteList);
   		ArrayList alIRBTypeList=GmCommonClass.getCodeList("IRBTYP");
   		gmClinicalIRBReportForm.setAlIRBTypeList(alIRBTypeList);
   		ArrayList alIRBApprovalTypeList=GmCommonClass.getCodeList("IRBATY");
   		
   		Iterator alIRBApprovalTypeListIter = alIRBApprovalTypeList.iterator();
    
		while (alIRBApprovalTypeListIter.hasNext()) {
			HashMap hmCodeList = (HashMap) alIRBApprovalTypeListIter.next();
			String strCodeId = (String) hmCodeList.get("CODEID");
			if(strCodeId.equalsIgnoreCase("60522")){
				alIRBApprovalTypeListIter.remove();
			}
		}
		
   		gmClinicalIRBReportForm.setAlIRBApprovalTypeList(alIRBApprovalTypeList);
   		ArrayList alDateTypeList=new ArrayList();   		
   		HashMap hmDateType = new HashMap();
   		hmDateType.put("CODEID", "0");
   		hmDateType.put("CODENM", "Appr Date");
   		alDateTypeList.add(hmDateType);
   		hmDateType = new HashMap();
   		hmDateType.put("CODEID", "1");
   		hmDateType.put("CODENM", "Expiration Date");
   		alDateTypeList.add(hmDateType);
   		gmClinicalIRBReportForm.setAlDateTypeList(alDateTypeList);
   		log.debug("Exit loadIRBSummaryReport");         
    }
    /**
     * loadIRBTypeReport
	 * @param GmClinicalIRBReportForm
	 * @param String studyId
     * @exception AppError
     */  
    public void loadIRBTypeReport(GmClinicalIRBReportForm gmClinicalIRBReportForm,String strStudyId) throws AppError{    	
   		
    	log.debug("Inside loadIRBTypeReport");
   		GmPatientBean gmPatientBean = new GmPatientBean();
   		ArrayList alSiteList = gmPatientBean.loadSiteFromStudy(strStudyId);
   		gmClinicalIRBReportForm.setAlSiteList(alSiteList);
   		
   		ArrayList alIRBApprovalTypeList=GmCommonClass.getCodeList("IRBATY");   		
   		Iterator alIRBApprovalTypeListIter = alIRBApprovalTypeList.iterator();
    
		while (alIRBApprovalTypeListIter.hasNext()) {
			HashMap hmCodeList = (HashMap) alIRBApprovalTypeListIter.next();
			String strCodeId = (String) hmCodeList.get("CODEID");
			if(strCodeId.equalsIgnoreCase("60522")){
				alIRBApprovalTypeListIter.remove();
			}
		}		
   		gmClinicalIRBReportForm.setAlIRBApprovalTypeList(alIRBApprovalTypeList);
   		
   		ArrayList alIRBApprovalReasonList=GmCommonClass.getCodeList("IRBARS");
   		gmClinicalIRBReportForm.setAlIRBApprovalReasonList(alIRBApprovalReasonList);
   		
   		ArrayList alIRBApprovalStatusList=GmCommonClass.getCodeList("IRBSTA");
   		gmClinicalIRBReportForm.setAlIRBApprovalStatusList(alIRBApprovalStatusList);

   		
   		ArrayList alDateTypeList=new ArrayList();   		
   		HashMap hmDateType = new HashMap();
   		hmDateType.put("CODEID", "0");
   		hmDateType.put("CODENM", "Appr Ltr Date");
   		alDateTypeList.add(hmDateType);
   		hmDateType = new HashMap();
   		hmDateType.put("CODEID", "1");
   		hmDateType.put("CODENM", "Expiration Date");
   		alDateTypeList.add(hmDateType);
   		gmClinicalIRBReportForm.setAlDateTypeList(alDateTypeList);
   		log.debug("Exit loadIRBTypeReport");         
    }
    /**
     * generateOutPut
	 * @param GmClinicalIRBReportForm
	 * @param ArrayList alIRBTypeList
	 * @param String strIRBVmFile
     * @exception AppError
     */  
    public void generateOutPut(GmClinicalIRBReportForm gmClinicalIRBReportForm,ArrayList alIRBTypeList,String strIRBVmFile) throws AppError{
    
    	GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setTemplateName(strIRBVmFile);
		templateUtil.setTemplateSubDir("clinical/templates");
		templateUtil.setDataList("alResult", alIRBTypeList);
		String gridXmlData = templateUtil.generateOutput();
		log.debug(" gridXmlData :"+gridXmlData);
		gmClinicalIRBReportForm.setGridXmlData(gridXmlData);
    }
    /**
     * loadIRBEventReport
	 * @param GmClinicalIRBReportForm
	 * @param String studyId
     * @exception AppError
     */
    public void loadIRBEventReport(GmClinicalIRBReportForm gmClinicalIRBReportForm,String strStudyId) throws AppError{

    	log.debug("Inside loadIRBEventReport");
   		GmPatientBean gmPatientBean = new GmPatientBean();
   		ArrayList alSiteList = gmPatientBean.loadSiteFromStudy(strStudyId);
   		gmClinicalIRBReportForm.setAlSiteList(alSiteList);
   		
   		ArrayList alIRBApprovalTypeList=GmCommonClass.getCodeList("IRBATY");   		
   		Iterator alIRBApprovalTypeListIter = alIRBApprovalTypeList.iterator();
    
		while (alIRBApprovalTypeListIter.hasNext()) {
			HashMap hmCodeList = (HashMap) alIRBApprovalTypeListIter.next();
			String strCodeId = (String) hmCodeList.get("CODEID");
			if(strCodeId.equalsIgnoreCase("60522")){
				alIRBApprovalTypeListIter.remove();
			}
		}		
   		gmClinicalIRBReportForm.setAlIRBApprovalTypeList(alIRBApprovalTypeList);
   		
   		ArrayList alIRBApprovalReasonList=GmCommonClass.getCodeList("IRBARS");
   		gmClinicalIRBReportForm.setAlIRBApprovalReasonList(alIRBApprovalReasonList);   		
   		
   		ArrayList alIRBActionTakenList=GmCommonClass.getCodeList("IRBATK");
   		gmClinicalIRBReportForm.setAlIRBActionTakenList(alIRBActionTakenList);   		
   		
   		log.debug("Exit loadIRBEventReport");       

    }
    /**
     * loadIRBMediaReport
	 * @param GmClinicalIRBReportForm
	 * @param String studyId
     * @exception AppError
     */
    public void loadIRBMediaReport(GmClinicalIRBReportForm gmClinicalIRBReportForm,String strStudyId) throws AppError{

    	log.debug("Inside loadIRBMediaReport");
   		GmPatientBean gmPatientBean = new GmPatientBean();
   		ArrayList alSiteList = gmPatientBean.loadSiteFromStudy(strStudyId);
   		gmClinicalIRBReportForm.setAlSiteList(alSiteList);
   		String strApprTypeName="";
   		
   		ArrayList alIRBApprovalTypeList=GmCommonClass.getCodeList("IRBATY");   		
   		Iterator alIRBApprovalTypeListIter = alIRBApprovalTypeList.iterator();
    
		while (alIRBApprovalTypeListIter.hasNext()) {
			HashMap hmCodeList = (HashMap) alIRBApprovalTypeListIter.next();
			String strCodeId = (String) hmCodeList.get("CODEID");
			if(strCodeId.equalsIgnoreCase("60522")){
				strApprTypeName=(String) hmCodeList.get("CODENM");
				break;
			}
		}		
		gmClinicalIRBReportForm.setApprTypeName(strApprTypeName);
   		gmClinicalIRBReportForm.setAlIRBApprovalTypeList(alIRBApprovalTypeList);
   		
   		ArrayList alIRBApprovalReasonList=GmCommonClass.getCodeList("IRBARS");
   		gmClinicalIRBReportForm.setAlIRBApprovalReasonList(alIRBApprovalReasonList);   		
   		
   		ArrayList alIRBMediaTypeList=GmCommonClass.getCodeList("IRBMTY");
   		gmClinicalIRBReportForm.setAlIRBMediaTypeList(alIRBMediaTypeList);
   		
   		log.debug("Exit loadIRBMediaReport");       

    }  
    
}
