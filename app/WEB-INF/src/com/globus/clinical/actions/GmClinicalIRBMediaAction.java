package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalIRBApprovalBean;
import com.globus.clinical.beans.GmClinicalIRBMediaBean;
import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.forms.GmClinicalIRBMediaForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalIRBMediaAction extends GmAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception
	{
	
		HttpSession session =request.getSession(false);
		String strStudyID=(String)session.getAttribute("strSessStudyId");
		
		GmClinicalIRBMediaForm gmClinicalIRBMediaForm = (GmClinicalIRBMediaForm) form;
		gmClinicalIRBMediaForm.loadSessionParameters(request);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		String strReturnTo="gmclinicalirbmedia";
		String strHidden="";
		String strButtonDisabled = "";
		String strAction = GmCommonClass.parseNull(gmClinicalIRBMediaForm.getStrOpt());		
		HashMap hmParamVal = new HashMap();
		hmParamVal = GmCommonClass.getHashMapFromForm(gmClinicalIRBMediaForm);
		hmParamVal.put("SESSSTUDYID", strStudyID);
		//disabling the submit button based on the study id
		strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParamVal);
		
		if(strAction.equals("save_irb_media")){
			saveIRBMedia(gmClinicalIRBMediaForm,request);
			strHidden =strAction ; 
			strAction = "load_irb_media";
			//strReturnTo="loadirbmedia";
			
		}
		if(strAction.equals("edit_irb_media")){
			HashMap hmParam = new HashMap();
			hmParam.put("IRBMEDIAID",gmClinicalIRBMediaForm.getIrbMediaID());
			ArrayList alIRBMedia = fetchIRBMedia(hmParam);
			
			if(alIRBMedia!=null&& alIRBMedia.size()>0){
				HashMap hmIRBMedia = (HashMap)alIRBMedia.get(0);
				GmCommonClass.getFormFromHashMap(gmClinicalIRBMediaForm, hmIRBMedia);
			}
			
			strAction = "load_irb_media";
		}
		if(strAction.equals("load_irb_media")){
			
			loadIRBMedia(gmClinicalIRBMediaForm,request);
			
			HashMap hmParam = new HashMap();
			hmParam.put("IRBID",gmClinicalIRBMediaForm.getIrbid() );
			String strGirdData = getGridData(fetchIRBMedia(hmParam));
			gmClinicalIRBMediaForm.setGridData(strGirdData);
			
		}
		if(!strHidden.equals("") && strHidden.equals("save_irb_media")){
			gmClinicalIRBMediaForm.setMediaType("");
			gmClinicalIRBMediaForm.setIrbMediaID("");
			gmClinicalIRBMediaForm.setIrbMediaNotes("");
		}
		gmClinicalIRBMediaForm.setStrOpt("load_irb_media");
		
		gmClinicalIRBMediaForm.setLockedStudyDisable(strButtonDisabled);
	return mapping.findForward(strReturnTo);
	}
	
   public void saveIRBMedia(GmClinicalIRBMediaForm gmClinicalIRBMediaForm, HttpServletRequest request) throws Exception{
		
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalIRBMediaForm);
		GmClinicalIRBMediaBean gmClinicalIRBMediaBean = new GmClinicalIRBMediaBean();
		String strIrbMediaid = gmClinicalIRBMediaBean.saveIRBMedia(hmParam);
		
		gmClinicalIRBMediaForm.setIrbMediaID(strIrbMediaid);
		log.debug(" # # # irbMediaid"+strIrbMediaid);
	}
   private String getGridData(ArrayList alIRBMedia) throws Exception{
		
		GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
		gmTemplateUtil.setDataList("alResult", alIRBMedia);
		gmTemplateUtil.setTemplateName("GmClinicalIRBMedia.vm");
		gmTemplateUtil.setTemplateSubDir("clinical/templates");
		String gridData = gmTemplateUtil.generateOutput();
		log.debug(" >>>>> gridData "+gridData);
		return gridData;
	}
   public ArrayList fetchIRBMedia(HashMap hmParam) throws Exception{
		GmClinicalIRBMediaBean gmClinicalIRBMediaBean = new GmClinicalIRBMediaBean();
		ArrayList alIRBMedia = gmClinicalIRBMediaBean.loadIRBMedia(hmParam);
		return alIRBMedia;
	}
   public void loadIRBMedia(GmClinicalIRBMediaForm gmClinicalIRBMediaForm, HttpServletRequest request) throws Exception{
		

		HashMap hmParam = new HashMap();
		hmParam.put("IRBID", gmClinicalIRBMediaForm.getIrbid());
		
		GmClinicalIRBApprovalBean gmClinicalIRBBean = new GmClinicalIRBApprovalBean();
		
		HashMap hmIRBApproval = gmClinicalIRBBean.loadIRBApproval(hmParam);
		
		if(hmIRBApproval!=null){
			ArrayList alIRBApproval = (ArrayList)hmIRBApproval.get("IRBAPPROVAL");
			
			if(alIRBApproval!=null && alIRBApproval.size()>0){
				hmIRBApproval = (HashMap)alIRBApproval.get(0);
			}
			//log.debug(" hmIRBApproval ????? 0"+hmIRBApproval);
			gmClinicalIRBMediaForm = (GmClinicalIRBMediaForm)GmCommonClass.getFormFromHashMap(gmClinicalIRBMediaForm, hmIRBApproval);			
			
			HashMap temp = GmCommonClass.getHashMapFromForm(gmClinicalIRBMediaForm);
		}
		gmClinicalIRBMediaForm.setMediaTypeList(GmCommonClass.getCodeList("IRBMTY"));
	}
}
