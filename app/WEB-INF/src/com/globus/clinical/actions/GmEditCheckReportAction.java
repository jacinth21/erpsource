package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmEditCheckBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;

public class GmEditCheckReportAction extends GmClinicalDispatchAction {

	
	/**
	 * Fetches the EditCheckIncidentReport detail 
	 * 
	 * 
	 * @param ActionMapping mapping 
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws com.globus.common.beans.AppError
	 */
    public ActionForward fetchEditCheckSummary(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
		log.debug("Inside fetchEditCheckSummary");	
		String strForward = "";
		String strSiteId = "";
		String strStudyId = "";
 	    String strXmlGridData = "";
 	    String strExcel = "";
 	   GmCommonBean gmCommonBean = new GmCommonBean();
		HashMap hmParam = new HashMap();
		log.debug("strForward  " + strForward);	
		String strCondition = getAccessFilter(request, response);
		GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm) form;
        GmStudyBean gmStudyBean = new GmStudyBean();
        GmPatientBean gmPatientBean = new GmPatientBean();
        gmStudyFilterForm.loadSessionParameters(request);

		String strOpt = gmStudyFilterForm.getStrOpt();
		gmStudyFilterForm.setAlStudyList(gmStudyBean.reportStudyNameList());
		gmStudyFilterForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmStudyFilterForm.getStudyListId(),"")); // To discuss about the static and access condition

        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
		
        gmStudyFilterForm.setStudyListId(strStudyId);
		gmStudyFilterForm.setAlStudyFormList(GmClinicalBean.loadStudyFormList(strStudyId));
		gmStudyFilterForm.setAlStatus((ArrayList)GmCommonClass.getCodeList("ECHKS",strCondition));
		gmStudyFilterForm.setAlCraNames(gmCommonBean.loadEmployeeList("2007", (int)3,(int)3));
		
		hmParam.put("STUDYID", strStudyId);
		hmParam.put("SITENAMES", strSiteId);
		
		if(strOpt.equalsIgnoreCase("loadReport")){
			hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
	    	
			GmEditCheckBean gmEditCheckBean = new GmEditCheckBean();
			ArrayList alResult = (ArrayList)gmEditCheckBean.fetchEditCheckSummary(hmParam, strCondition);
			
			strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));
		    
			if (strExcel.equalsIgnoreCase("Excel")){
		    	request.setAttribute("ALRETURN",alResult);
		    	strForward = "gmEditCheckSummaryExcel";
			}else{
		        HashMap hmParamV = new HashMap();
		        
		        hmParamV.put("TEMPLATE","GmEditCheckSummary.vm");
		        hmParamV.put("TEMPLATEPATH","clinical/templates");
		        hmParamV.put("RLIST",alResult);
		        String strAccess =  gmCommonBean.checkUserExists(gmStudyFilterForm.getUserId(), "ECGRP");
		        hmParamV.put("ACCESS",strAccess);

		        strXmlGridData = gmEditCheckBean.getXmlGridData(hmParamV);
		        gmStudyFilterForm.setGridXmlData(strXmlGridData);
		        strForward = "gmEditCheckSummary";
			}
		}

		return mapping.findForward(strForward);
    }

	/**
	 * Fetches the fetchEditCheckIncidentReport detail 
	 * 
	 * 
	 * @param ActionMapping mapping 
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws com.globus.common.beans.AppError
	 */
    public ActionForward fetchEditCheckIncidentReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
		log.debug("Inside fetchEditCheckIncidentReport");	
		String strForward = "";
		String strStudyId = "";
		String strSiteId = "";
	    String patientIde = "";
	    String patientId = "";
	    String periodId = "";
 	    String strXmlGridData = "";
 	    String strExcel = "";
		String strIncidentStatus = "";
		HashMap hmTemp = new HashMap();
		HashMap hmParam = new HashMap();
		strForward = "gmEditCheckIncidentReport";
		GmClinicalBean gmClinic = new GmClinicalBean();

		GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm) form;
        GmPatientBean gmPatientBean = new GmPatientBean();
        gmStudyFilterForm.loadSessionParameters(request);

		String strCondition= getAccessFilter(request, response);
		
        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
/*      periodId = GmCommonClass.parseNull(gmStudyFilterForm.getSessPeriodId());
	    patientId = GmCommonClass.parseNull(gmStudyFilterForm.getSessPatientPkey());;
*/	    

        gmStudyFilterForm.setStudyListId(strStudyId);
        gmStudyFilterForm.setSiteList(strSiteId);
        periodId = GmCommonClass.parseNull(gmStudyFilterForm.getPeriodId());
		patientId = GmCommonClass.parseNull(gmStudyFilterForm.getPatientId());
		strIncidentStatus = GmCommonClass.parseNull(gmStudyFilterForm.getIncidentStatus());
		
		hmParam.put("STUDY_ID", strStudyId);
		hmParam.put("ACCESS_CONDITION", strCondition);

		hmTemp = gmClinic.loadFormDataEntryLists(hmParam);
		gmStudyFilterForm.setAlStudyList(gmClinic.loadStudyList(strCondition));
		
		if(strStudyId!=null && !strStudyId.equals("")){
			gmStudyFilterForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmStudyFilterForm.getStudyListId(),""));
			gmStudyFilterForm.setAlPeriodList((ArrayList)hmTemp.get("PERIODLIST"));
			gmStudyFilterForm.setAlPatientList((ArrayList)hmTemp.get("PATIENTLIST"));
		}
		if(periodId!=null && !periodId.equals("0") && !periodId.equals("")){
			gmStudyFilterForm.setAlStudyFormList(gmClinic.loadFormListForPeriod(strStudyId, periodId));
		}else{
			gmStudyFilterForm.setAlStudyFormList(GmClinicalBean.loadStudyFormList(strStudyId));
		}
		log.debug("Inside fetchEditCheckSummarysfdgfgfsdgfsgdfs");	
		gmStudyFilterForm.setAlStatus((ArrayList)GmCommonClass.getCodeList("ECHSS",strCondition));
		gmStudyFilterForm.setAlReasonForClosing((ArrayList)GmCommonClass.getCodeList("ECISS",strCondition));
		gmStudyFilterForm.setAlCraNames(gmCommonBean.loadEmployeeList("2007", (int)3,(int)3));

		String strOpt = gmStudyFilterForm.getStrOpt();
		String strStatus = GmCommonClass.parseNull(gmStudyFilterForm.getStatus());
		
		if(strStatus.equals("93055") || strIncidentStatus.equals("93055") ){
			gmStudyFilterForm.setStatus("10");
		}else if(strStatus.equals("93056") || strIncidentStatus.equals("93056")){
			gmStudyFilterForm.setStatus("20");
		}
		
		if(strOpt.equalsIgnoreCase("loadReport")){

			
			hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
	    	
			GmEditCheckBean gmEditCheckBean = new GmEditCheckBean();
			ArrayList alResult = (ArrayList)gmEditCheckBean.fetchEditCheckIncidentReport(hmParam, strCondition);
			
			strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));
		    
			if (strExcel.equalsIgnoreCase("Excel")){
		    	request.setAttribute("ALRETURN",alResult);
		    	strForward = "gmEditCheckIncidentReportExcel";
			}else{
		        HashMap hmParamV = new HashMap();
		        hmParamV.put("STUDYID",strStudyId);
		        hmParamV.put("SITEID",strSiteId);
		        hmParamV.put("TEMPLATE","GmEditCheckIncident.vm");
		        hmParamV.put("TEMPLATEPATH","clinical/templates");
		        hmParamV.put("RLIST",alResult);
		        hmParamV.put("FORMSCR",GmCommonClass.parseNull(gmStudyFilterForm.getFormDataScr()));
		        String strAccess =  gmCommonBean.checkUserExists(gmStudyFilterForm.getUserId(), "ECGRP");
		        hmParamV.put("ACCESS",strAccess);
		        strXmlGridData = gmEditCheckBean.getXmlGridData(hmParamV);
		        gmStudyFilterForm.setGridXmlData(strXmlGridData);
		        
			}     
		}
		
		if(strStatus.equals("93055")){
			gmStudyFilterForm.setStatus("93055");
		}else if(strStatus.equals("93056")){
			gmStudyFilterForm.setStatus("93056");
		}
		return mapping.findForward(strForward);
    }

    
    
	
}
