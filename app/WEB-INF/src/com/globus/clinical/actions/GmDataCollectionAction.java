package com.globus.clinical.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmDataCollectionRptBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmDataCollectionAction extends GmClinicalDispatchAction
{

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    String strSiteId ="";
    String strStudySiteId = "";
    public ActionForward expectedFollowUpReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   
    	String strSiteId ="";
    	log.debug(" inside demandSheetReport ");
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);
        setIncludeInfo(form);        
        GmDataCollectionRptBean gmDataCollectionRptBean = new GmDataCollectionRptBean();
        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
        gmStudyFilterForm.setStrSiteIds(strSiteId);
        
        try{
            //if(strOpt.equals("Report")){
            HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmDataCollectionRptBean.reportCrossTabSetDetails(strStudyId,strSiteId)) ;
            log.debug(" Inside reporting.. setting values ");
            request.setAttribute("HMEXPECTED",hmFinalResult.get("EXPECTED"));
            request.setAttribute("HMACTUAL",hmFinalResult.get("ACTUAL"));
            //}
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
            
        return mapping.findForward("expectedFollowUpReport");
    }
    
    public ActionForward outOfWindow(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   
        log.debug(" inside outOfWindow ");
        GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
        gmStudyFilterForm.loadSessionParameters(request);
        setIncludeInfo(form);
        GmDataCollectionRptBean gmDataCollectionRptBean = new GmDataCollectionRptBean();
        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
        gmStudyFilterForm.setStrSiteIds(strSiteId);
        
        try{
            log.debug(" strOpt is " + strOpt);
            //if(strOpt.equals("Report"))
            {
            	HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmDataCollectionRptBean.loadOutOfWindow(strStudyId,strSiteId)) ;
            	gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);
            }
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
            
        return mapping.findForward("outOfWindow");
    }

    public ActionForward missingFollowUp(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   
        log.debug(" inside missingFollowUp ");
        GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
        gmStudyFilterForm.loadSessionParameters(request);
        setIncludeInfo(form);
        
        GmDataCollectionRptBean gmDataCollectionRptBean = new GmDataCollectionRptBean();
        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
        
        gmStudyFilterForm.setStrSiteIds(strSiteId);
        
        try{
            //if(strOpt.equals("Report"))
            {
            	HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmDataCollectionRptBean.loadMissingFollowUp(strStudyId,strSiteId)) ;
            	gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);
            }
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
            
        return mapping.findForward("missingFollowUp");
    }
}
