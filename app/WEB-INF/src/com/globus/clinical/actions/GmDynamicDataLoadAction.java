package com.globus.clinical.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmDynamicDataLoadBean;
import com.globus.clinical.beans.GmQuestionBean;
import com.globus.clinical.forms.GmDynamicDataLoadForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmDynamicDataLoadAction extends GmClinicalDispatchAction
{

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    public ActionForward reportDynamicData(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   
    	String strStudylistid = "";
    	String strQuestionList="";
    	String strAnswerGroupID="";
    	String strInputString="";
    	String strSiteId ="";
    	String strStudySiteId = "";
    	
        HashMap hmParam = new HashMap();
        HashMap hmFinalResult = new HashMap(); 
        GmDynamicDataLoadBean gmDynamicDataLoadBean = new GmDynamicDataLoadBean();
        GmDynamicDataLoadForm gmDynamicDataLoadForm = (GmDynamicDataLoadForm) form;
        
        gmDynamicDataLoadForm.loadSessionParameters(request);
        setIncludeInfo(form);    
        String strExcel = GmCommonClass.parseNull((String)request.getParameter("hExcel"));
       
        
        
        strStudyId = GmCommonClass.parseNull(gmDynamicDataLoadForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmDynamicDataLoadForm.getSessSiteId());
        strStudySiteId = GmCommonClass.parseNull(gmDynamicDataLoadForm.getSessStudySiteId());
        
        strInputString = strSiteId.equals("")? GmCommonClass.createInputStringWithType(gmStudyFilterForm.getCheckSiteName(),"SITE"):strStudySiteId;
        gmDynamicDataLoadForm.setStrSiteIds(strInputString);
       // strInputString = GmCommonClass.createInputStringWithType(gmStudyFilterForm.getCheckSiteName(),"SITE");        
       //  log.debug("the list of site list "+ hmSiteId );
        
        gmDynamicDataLoadForm.setAlDynamicColumns(GmCommonClass.getCodeList("DYNCL"));           
        gmDynamicDataLoadForm.setAlFormList(gmDynamicDataLoadBean.fetchFormList(strStudyId));
        
                    
        strStudylistid = gmDynamicDataLoadForm.getFormList();  //form id selected
        
        if(strStudylistid != null){
        	gmDynamicDataLoadForm.setAlTimePeriodList(gmDynamicDataLoadBean.fetchTimePoints(strStudylistid));
            gmDynamicDataLoadForm.setAlQuestionList(gmDynamicDataLoadBean.fetchQuestionList(strStudylistid));
            strInputString += GmCommonClass.createInputStringWithType(gmDynamicDataLoadForm.getCheckTimePeriods(),"TP"); //periods selected
            strQuestionList = GmCommonClass.createInputString(gmDynamicDataLoadForm.getCheckQuestions()); //questions selected
            strInputString += GmCommonClass.createInputStringWithType(gmDynamicDataLoadForm.getCheckQuestions(),"QUES"); //questions selected
        }
        
        if (strQuestionList != null){
        	gmDynamicDataLoadForm.setAlAnswersList(gmDynamicDataLoadBean.fetchAnswers(strQuestionList,strStudylistid));
            strAnswerGroupID = GmCommonClass.createInputString(gmDynamicDataLoadForm.getCheckAnswers()); //answers selected          	
            strInputString += GmCommonClass.createInputStringWithType(gmDynamicDataLoadForm.getCheckAnswers(),"ANS"); //answers selected
        }
        
        if(gmDynamicDataLoadForm.getStrOpt().equals("60270")){ // timepoints	
            hmParam.put("InputString", strInputString);
        	hmParam.put("StudyListID", strStudylistid);
        	hmParam.put("QuestionID", strQuestionList);
        	hmParam.put("AnswerGroupID",strAnswerGroupID);
            hmFinalResult = GmCommonClass.parseNullHashMap(gmDynamicDataLoadBean.fetchPeriodsInYaxis(hmParam)) ;
            gmDynamicDataLoadForm.setHmCrossTabReport(hmFinalResult);
        }
        if(gmDynamicDataLoadForm.getStrOpt().equals("60271")){ // Answers
            hmParam.put("InputString", strInputString);
        	hmParam.put("StudyListID", strStudylistid);
        	hmFinalResult = GmCommonClass.parseNullHashMap(gmDynamicDataLoadBean.fetchAnswersInYaxis(hmParam)) ;
            gmDynamicDataLoadForm.setHmCrossTabReport(hmFinalResult);
        }
          
        
        if (strExcel.equalsIgnoreCase("Excel"))
		{
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-disposition","attachment;filename=Dynamic_Data_Load.xls");			
			return mapping.findForward("excell");
		}
        return mapping.findForward("success");
    }

}
