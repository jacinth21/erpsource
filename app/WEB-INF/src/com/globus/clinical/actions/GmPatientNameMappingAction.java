package com.globus.clinical.actions;


import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmPatientNameMappingForm;




public class GmPatientNameMappingAction extends GmAction
{

	
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   	
    	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    	String strPatientIDE="";
    	String strPatientId = "";
    	GmPatientNameMappingForm gmPatientNameMappingForm = (GmPatientNameMappingForm)form;
    	
    	gmPatientNameMappingForm.loadSessionParameters(request);        
        String strOpt = GmCommonClass.parseNull(gmPatientNameMappingForm.getStrOpt());        
        GmPatientBean gmPatientNameBean = new GmPatientBean();
        
        if(strOpt.equals("fetch"))
        {
        	strPatientIDE = GmCommonClass.parseNull(gmPatientNameMappingForm.getPatientIde());
        	HashMap hmReturn =gmPatientNameBean.fetchPatientDetail(strPatientIDE);
        	GmCommonClass.getFormFromHashMap(gmPatientNameMappingForm, hmReturn);
        }
        if(strOpt.equals("edit")){
        	HashMap hmParam = GmCommonClass.getHashMapFromForm(gmPatientNameMappingForm);
        	gmPatientNameBean.savePatientDetail(hmParam);
    	}        
    	return mapping.findForward("gmPatientNameMapping");
    }   
    
   
}