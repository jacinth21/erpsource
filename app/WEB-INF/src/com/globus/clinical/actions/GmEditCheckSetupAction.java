package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmEditCheckBean;
import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.forms.GmEditCheckForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmEditCheckSetupAction extends GmAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws AppError{
		GmEditCheckForm gmEditCheckForm=(GmEditCheckForm)form;
		GmEditCheckBean gmEditCheckBean=new GmEditCheckBean();
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		String strOpt=gmEditCheckForm.getStrOpt();
		String strStudyId = (String)request.getSession().getAttribute("strSessStudyId");
		log.debug("strStudyId::"+strStudyId);
		gmEditCheckForm.loadSessionParameters(request);
		String strButtonDisabled = "";
		HashMap hmParamVal = new HashMap();
		hmParamVal = GmCommonClass.getHashMapFromForm(gmEditCheckForm);
		hmParamVal.put("SESSSTUDYID", strStudyId);
		//disabling the submit button based on the study id
		strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParamVal);
		
		if(strOpt==null || strOpt.equals("")){
			strOpt=GmCommonClass.parseNull(request.getParameter("strOpt"));
		}
		String strRefId=GmCommonClass.parseNull(request.getParameter("editId"));
		String strDispatchTo="GmEditCheckSetup";
		gmEditCheckForm.loadSessionParameters(request);
		if(strOpt.equals("Save")){
			HashMap hmParam = GmCommonClass.getHashMapFromForm(gmEditCheckForm);
			String strEditId=gmEditCheckBean.saveEditCheck(hmParam);
			HttpSession ses = request.getSession();
			ses.setAttribute("editID", strEditId);
		}else if(strOpt.equals("edit")){
			HashMap hmReturn= gmEditCheckBean.getEditCheck(strRefId);
			GmCommonClass.getFormFromHashMap(gmEditCheckForm, hmReturn);
		}
		gmEditCheckForm.setLockedStudyDisable(strButtonDisabled);
		return mapping.findForward(strDispatchTo);
	}
}
