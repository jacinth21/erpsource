package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmPatientLogBean;
import com.globus.clinical.forms.GmPatientLogSummaryForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonCurrConvForm;
import com.globus.etrain.forms.GmEtrSessionSetupForm;

public class GmPatientLogAction extends GmClinicalDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
    HashMap hmReturn = new HashMap();   
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    { 
		String strSiteId = "";
		String strStudySiteId = "";
		String strStudyId = "";
		String strAccLvl="";
		String strButtonDisabled = "";
		
	    HashMap hmReturn = new HashMap();
	    HashMap hmParamN = new HashMap(); 
	    HashMap hmValues = new HashMap();
	    GmPatientLogSummaryForm gmPatientLogForm = (GmPatientLogSummaryForm)form;
		gmPatientLogForm.loadSessionParameters(request);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		GmPatientLogBean gmPatientLogBean = new GmPatientLogBean();
		String strPatientIdHash = gmPatientLogForm.getPatientId();
		
		
		HttpSession session = request.getSession();
		String strPatientId = GmCommonClass.parseNull((String)session.getAttribute("strSessPatientPkey"));
		strPatientId = strPatientIdHash.equals("")?strPatientId:strPatientIdHash;
		
		gmPatientLogForm.setPatientId(strPatientId);
		
		log.debug(" Patient ID is "+ strPatientId);
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmPatientLogForm);
		
		String strOpt = gmPatientLogForm.getStrOpt();		
		//Get study and site from the session.
		strStudySiteId = GmCommonClass.parseNull(gmPatientLogForm.getSessStudySiteId()); 
		strSiteId = GmCommonClass.parseNull(gmPatientLogForm.getSessSiteId());
		strStudyId = GmCommonClass.parseNull(gmPatientLogForm.getSessStudyId());
		strAccLvl = GmCommonClass.parseNull(gmPatientLogForm.getSessAccLvl());
	
		strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParam);
	
		hmReturn.put("STUDYID", strStudyId);
		hmReturn.put("SITEID", strSiteId);      
		hmParamN.put("STUDY_ID", strStudyId);
		//hmParam.put("ACCESS_CONDITION", strAccLvl);
		hmParamN.put("SITE_ID", strSiteId);
		hmParamN.put("STUDYSITEID", strStudySiteId);
		/*
		log.debug("strStudyId:"+strStudyId);
		log.debug("strSiteId:"+strSiteId);
		log.debug("strStudySiteId:"+strStudySiteId);
		log.debug("strOpt:"+strOpt);
		log.debug("gmPatientLogForm.setPatientLId:"+gmPatientLogForm.getPatientLId());
		log.debug("strPatientLId:"+hmParam.get("patientLId"));
		*/
		if(strOpt.equals("edit"))
		{
			hmValues = gmPatientLogBean.fetchPatientLog(gmPatientLogForm.getPatientLId());
			gmPatientLogForm = (GmPatientLogSummaryForm) GmCommonClass.getFormFromHashMap(gmPatientLogForm, hmValues);	
		}
		if(strOpt.equals("Save"))
		{
        	String strPatientLId = GmCommonClass.parseNull(gmPatientLogBean.savePatientLog(hmParam));
			gmPatientLogForm.setPatientLId(strPatientLId);        	
        }
		gmPatientLogForm.setAlStudyPeriod(GmCommonClass.parseNullArrayList(gmClinicalBean.loadPeriodList(strStudyId)));
		gmPatientLogForm.setAlPatientId(GmCommonClass.parseNullArrayList(gmClinicalBean.loadPatientList(hmParamN)));
		gmPatientLogForm.setAlFormList(GmCommonClass.parseNullArrayList(gmClinicalBean.loadStudyFormList(strStudyId)));
		gmPatientLogForm.setLockedStudyDisable(strButtonDisabled);
		
		return mapping.findForward("gmPatientLog");
    } 
        
    
}
