package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmDataCollectionRptBean;
import com.globus.clinical.beans.GmPatientTrackingBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmPatientTrackingAction extends GmClinicalDispatchAction
{

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.
    String strCondition = "";
    public ActionForward reportPatientTrackList(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    {   
    	String strXmlGridData = "";
    	HashMap hmParam = new HashMap();
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	strCondition = getAccessFilter(request, response);
    	gmStudyFilterForm.loadSessionParameters(request);
    	setIncludeInfo(form, strCondition);    	
    	//String strSiteId = GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName());
    	String strSiteId = gmStudyFilterForm.getSessSiteId();
    	String strStudyId = gmStudyFilterForm.getSessStudyId();
    	String strAccessLvl = gmStudyFilterForm.getSessAccLvl();
    	
    	hmParam.put("STUDYID", strStudyId);
        hmParam.put("SITEID", strSiteId);

    	
		GmPatientTrackingBean gmPatientTrackingBean = new GmPatientTrackingBean();
		ArrayList alResult = GmCommonClass.parseNullArrayList(gmPatientTrackingBean.loadPatientTrackList(hmParam, strCondition));
		HashMap hmParamV = new HashMap();
        hmParamV.put("STUDYLIST",strStudyId);
        hmParamV.put("TEMPLATE","GmPatientTracking.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);
        hmParamV.put("ACCLVL", strAccessLvl);
       // log.debug("List===="+alResult);
        strXmlGridData = gmPatientTrackingBean.getXmlGridData(hmParamV);
        log.debug("xml data ===Patient Tracking===="+strXmlGridData);
        gmStudyFilterForm.setGridXmlData(strXmlGridData);
    	

    	return mapping.findForward("gmPatientTracking");
    }
    
    public ActionForward reportPatientTrackInfo(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    {   
    	HttpSession session = request.getSession();
    	String strPatId=GmCommonClass.parseNull(request.getParameter("hPatientID"));  
    	String strPKey=GmCommonClass.parseNull(request.getParameter("hPKeyID"));
    	if(!strPatId.equals("") && !strPKey.equals(""))
    	{
    		session.setAttribute("strSessPatientId", strPatId);
    		session.setAttribute("strSessPatientPkey", strPKey);
    	}
    	
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	ArrayList alPatientList = new ArrayList();
    	GmPatientTrackingBean gmPatientTrackingBean = new GmPatientTrackingBean();
    	HashMap hmParamV = new HashMap();
    	String strXmlGridData = "";
    	strCondition = getAccessFilter(request, response);
    	gmStudyFilterForm.loadSessionParameters(request);
    	setIncludeInfo(form, strCondition);
    	String strSiteId = gmStudyFilterForm.getSessSiteId();
    	String strStudyId = gmStudyFilterForm.getSessStudyId();
    	String strPatientId = gmStudyFilterForm.getSessPatientId();
    	String strAccessLvl = gmStudyFilterForm.getSessAccLvl();
    	//String strSiteId = GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName());
    	//String strPatientId = request.getParameter("patient_Ide");

    	//if(strOpt.equals("Report")){
    	
		alPatientList = GmCommonClass.parseNullArrayList(gmPatientTrackingBean.loadPatientTrackInfo(strStudyId, strSiteId, strPatientId,strCondition));
		hmParamV.put("STUDYLIST",strStudyId);
        hmParamV.put("TEMPLATE","GmPatientTrackingInfo.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alPatientList);
        hmParamV.put("ACCLVL", strAccessLvl);
        //log.debug("List===="+alResult);
        strXmlGridData = gmPatientTrackingBean.getXmlGridData(hmParamV);
        //log.debug("xml data ===Patient Tracking===="+strXmlGridData);
        gmStudyFilterForm.setGridXmlData(strXmlGridData);	
    		
    	//}
    	return mapping.findForward("gmPatientTrackInfo");
    }

    
    public ActionForward reportPatientTrackForm(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    {   
    	String strPatId=GmCommonClass.parseNull(request.getParameter("hStrPatientID"));
    	String strPKey=GmCommonClass.parseNull(request.getParameter("hPKeyID"));
    	strCondition = getAccessFilter(request, response);
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);
    	setIncludeInfo(form, strCondition);
    	//String strSiteId = GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName());
    	String strSiteId = gmStudyFilterForm.getSessStudySiteId();
    	String strStudyId = gmStudyFilterForm.getSessStudyId();
    
    	if(!strPatId.equals("")&&!strPKey.equals(""))
    	{
    	gmStudyFilterForm.setSessPatientId(strPatId);
    	gmStudyFilterForm.setSessPatientPkey(strPKey);
    	}
    	
    	String strPatientIde = gmStudyFilterForm.getSessPatientId();
    	String strPatientId = gmStudyFilterForm.getSessPatientPkey();
    	String strExcel = GmCommonClass.parseNull(request.getParameter("strExcel"));
    	HashMap hmParam = new HashMap();    
    	hmParam.put("STUDYID", strStudyId);
        hmParam.put("SITEID", strSiteId);
        hmParam.put("PATIENTIDE", strPatientIde);
        hmParam.put("PATIENTID", strPatientId);
        hmParam.put("CONDITION", strCondition);    	
    	//if(strOpt.equals("Report")){
    		GmPatientTrackingBean gmPatientTrackingBean = new GmPatientTrackingBean();
    		log.debug("hmParam===================="+hmParam);
    		hmParam = GmCommonClass.parseNullHashMap(gmPatientTrackingBean.loadPatientTrackForm(hmParam));
    		gmStudyFilterForm.setHmCrossTabReport((HashMap)hmParam.get("GIRDATA")) ;
    		gmStudyFilterForm.setHmRules((HashMap)hmParam.get("RULES"));
    		log.debug(" Inside reporting.. setting values ");
    	//}
    	if (strExcel.equalsIgnoreCase("Excel"))
    	{

    		response.setContentType("application/vnd.ms-excel");
    		response.setHeader("Content-disposition","attachment;filename=FormTrack.xls");			
    		return mapping.findForward("excel");
    	}
    	return mapping.findForward("gmPatientFormTracker");
    }
}
