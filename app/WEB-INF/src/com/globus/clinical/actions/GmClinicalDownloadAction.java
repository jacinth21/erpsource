package com.globus.clinical.actions;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalDownloadBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.clinical.forms.GmClinicalDownloadForm;
import com.globus.clinical.forms.GmClinicalSiteMapForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.excel.GmExcelManipulation;

public class GmClinicalDownloadAction extends GmAction
{

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   	
        
        GmClinicalDownloadForm gmClinicalDownloadForm = (GmClinicalDownloadForm) form;
        gmClinicalDownloadForm.loadSessionParameters(request);
        GmClinicalBean gmClinicalBean = new GmClinicalBean();
        GmClinicalDownloadBean gmClinicalDownloadBean = new GmClinicalDownloadBean();
        GmAccessControlBean	gmAccessControlBean = new GmAccessControlBean();
        String strAction = gmClinicalDownloadForm.getHaction();
        String strButtonDisabled = "";
        log.debug("strAction :"+strAction);
        gmClinicalDownloadForm.setMsg("");
        gmClinicalDownloadForm.setAlStudyList(gmClinicalBean.loadStudyList());
        gmClinicalDownloadForm.setStrUploadHome(GmCommonClass.getString("UPLOADHOME"));
        
    	String strPartyId = (String)request.getSession().getAttribute("strSessPartyId");
    	log.debug("strPartyId:"+strPartyId);
    	ArrayList alPartyList = gmAccessControlBean.getPartyList("18",strPartyId);
    	log.debug("alPartyList:"+alPartyList);
    	String strStudyId = (String)request.getSession().getAttribute("strSessStudyId");
    	gmClinicalDownloadForm.setSessStudyId(strStudyId);
    	log.debug("Selected Study Id:>>>>"+strStudyId);      
    	HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmClinicalDownloadForm);
		hmParam.put("SESSSTUDYID", strStudyId);
		//disabling the submit button based on the study id
		strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParam);
		
    	if(alPartyList.size()>0)
    		gmClinicalDownloadForm.setLimitedAccess("true");
    	else
    		gmClinicalDownloadForm.setLimitedAccess("false");
        
           	
    	if(strStudyId!=null){
    		ArrayList alDownloadFormsList = gmClinicalDownloadBean.fetchDownloadForm(strStudyId); 
    		gmClinicalDownloadForm.setAlFormList(alDownloadFormsList);
    	}       	
        
        if(strAction.equals("load_download_history")){
        	log.debug("Load Download History ");    
        	downloadHistory( gmClinicalDownloadForm, gmClinicalDownloadBean);
        }
        if(strAction.equals("download_file")){
        	log.debug("Download File");
        	String strUserId = (String)request.getSession().getAttribute("strSessUserId");  
        	downloadFile( gmClinicalDownloadForm, gmClinicalDownloadBean, strUserId );   
        	downloadHistory( gmClinicalDownloadForm, gmClinicalDownloadBean);
        	gmClinicalDownloadForm.setHaction("load_download_history");
        }
        if(strAction.equals("cancel_download")){
        	log.debug("Cancel Download");        	
        	cancelDownload( gmClinicalDownloadForm, gmClinicalDownloadBean,request);
        	downloadHistory( gmClinicalDownloadForm, gmClinicalDownloadBean);
        	gmClinicalDownloadForm.setHaction("load_download_history");
        }
        gmClinicalDownloadForm.setLockedStudyDisable(strButtonDisabled);
        return mapping.findForward("load");
    }   
    
	public void writeFile(String path,String fileName,String fileFormat,String data) {
		try{
		/* create a fileOutputStream object */
		FileOutputStream fileOutputStream = new FileOutputStream(path+fileName + "." + fileFormat);

		/* Encapsulate within printWriter */
		PrintWriter out = new PrintWriter(new BufferedOutputStream(fileOutputStream));

		/* if file Format is CSV */
		if (fileFormat.equals("CSV")) {
			writeCSV(out,	data);
		} else {
			out.print(data);
			/* flush the output stream to write data */
			out.flush();
		}
		}catch (Exception e) {
			e.printStackTrace();
		}

	} // End of write Function
	
	public void writeCSV(PrintWriter out,String data) {
		try {
			/* write the data */
			out.print(data);
			/* carriage return appended */
			out.print("\r");
			/* flush the output stream to write data */
			out.flush();
			// fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	} // End of writeCSV function
    
    public void downloadHistory(GmClinicalDownloadForm gmClinicalDownloadForm,GmClinicalDownloadBean gmClinicalDownloadBean) throws AppError{    	
    	String strStudyId = gmClinicalDownloadForm.getSessStudyId();
    	String strFormId = gmClinicalDownloadForm.getFormName();    	    	
    	ArrayList alDownloadFormsList = gmClinicalDownloadBean.fetchDownloadForm(strStudyId);
    	gmClinicalDownloadForm.setAlFormList(alDownloadFormsList);
    	ArrayList alDownloadHistoryList = gmClinicalDownloadBean.fetchDownloadHistory(strStudyId, strFormId);
    	gmClinicalDownloadForm.setAlDownloadHistoryList(alDownloadHistoryList);    	
    	ArrayList alCancelReasonList= GmCommonClass.getCodeList("DWCNL");  
    	gmClinicalDownloadForm.setAlCancelReasonList(alCancelReasonList);
    	if(alDownloadHistoryList.size()>0){
    		HashMap hmHistory = (HashMap)alDownloadHistoryList.get(0);
    		if(((String)hmHistory.get("LOCK_FL")).equals("Y")){
    			gmClinicalDownloadForm.setStrLockExist("true");
    		}
    	}
    }
    
    public void downloadFile(GmClinicalDownloadForm gmClinicalDownloadForm,GmClinicalDownloadBean gmClinicalDownloadBean,String strUserId ) throws AppError,Exception{    	
    	
    	String strStudyId = gmClinicalDownloadForm.getSessStudyId();
    	String strFormId = gmClinicalDownloadForm.getFormName();        	
    	ArrayList alDownloadFormsList = gmClinicalDownloadBean.fetchDownloadForm(strStudyId);
    	gmClinicalDownloadForm.setAlFormList(alDownloadFormsList);    	 
    	String strSelFormName = gmClinicalDownloadForm.getSelFormName();        	
    	Calendar cal = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");   	
		String strTime = cal.getTimeInMillis()+"";
		log.debug("milsecs........................."+strTime);
		log.debug("milsecs........................."+(strTime.substring(10, strTime.length())));
    	String strFileName=formatter.format(cal.getTime())+"_"+strSelFormName+"_Download"+strTime.substring(10, strTime.length());
    	
    	String strIntenalocKey=strStudyId+"_INTERNAL_"+strSelFormName+"_DOWNLOADLOC";
    	String strInternalPath=GmCommonClass.getString(strIntenalocKey);
    	
    	String strExtLocKey=strStudyId+"_"+strSelFormName+"_DOWNLOADLOC";
    	String strExtPath=GmCommonClass.getString(strExtLocKey);
    	
    	String strCalculatedFormId = GmCommonClass.getString("SFCALCULATEDFORMID");
    	
    	HashMap hmParam = new HashMap();        	
    	hmParam.put("STUDYID",strStudyId);
        hmParam.put("FORMID",strFormId);
        hmParam.put("USERID",strUserId);
        hmParam.put("FILENAME",strFileName+".csv");
        hmParam.put("SFCALCULATEDFORMID", strCalculatedFormId);
    	
        log.debug("download_file : hmParam ==> "+hmParam);
        HashMap hmReturn = gmClinicalDownloadBean.fetchFormData(hmParam);  
        
        ArrayList alDetailList = (ArrayList)hmReturn.get("Details");
        ArrayList alHeaderList = (ArrayList)hmReturn.get("HEADERLIST");
        if(alDetailList.size()>0){        	            
	            String strDetail = extractDetail(alHeaderList,alDetailList);	            
	            log.debug("strFileName ==> "+strFileName);
	            File fileDir = new File(strInternalPath);
	        	if(!fileDir.isDirectory()){
	        		fileDir.mkdirs();
	        	}
	        	fileDir = new File(strExtPath);
	        	if(!fileDir.isDirectory()){
	        		fileDir.mkdirs();
	        	}
	        	String strFileExtn=GmCommonClass.getString("CLINICAL_DOWNLOAD_FILEEXTN");
	        	writeFile(strInternalPath,strFileName,strFileExtn,strDetail);
	            String strIn = strInternalPath+strFileName+"."+strFileExtn;
	            String strOut = strExtPath+strFileName+"."+strFileExtn;	           
	            GmExcelManipulation.copyFile(strIn, strOut);
	            File file=new File(strOut);
	            file.setReadOnly();
	            gmClinicalDownloadForm.setDownloadPath(strExtPath);
	            gmClinicalDownloadForm.setMsg("Data file has successfully been saved .");
        }else{            	
        	gmClinicalDownloadForm.setMsg("There is no data available for download.");
        }
    }
    
    public void cancelDownload(GmClinicalDownloadForm gmClinicalDownloadForm,GmClinicalDownloadBean gmClinicalDownloadBean, HttpServletRequest request) throws AppError{
    	
    	String strStudyId = gmClinicalDownloadForm.getSessStudyId();    	
    	String strFormId = gmClinicalDownloadForm.getFormName();
    	ArrayList alDownloadFormsList = gmClinicalDownloadBean.fetchDownloadForm(strStudyId);
    	gmClinicalDownloadForm.setAlFormList(alDownloadFormsList);
    	String strUserId = (String)request.getSession().getAttribute("strSessUserId");
    	String strCancelFileId = request.getParameter("cancelCheck");
    	String strCancelId = gmClinicalDownloadForm.getCancelId();
    	String strCalculatedFormId = GmCommonClass.getString("SFCALCULATEDFORMID");
    	
    	HashMap hmParam = new HashMap();        	
    	hmParam.put("STUDYID",strStudyId);
        hmParam.put("FORMID",strFormId);
        hmParam.put("USERID",strUserId);
        hmParam.put("FILEID",strCancelFileId);
        hmParam.put("CANCELID",strCancelId);
        hmParam.put("SFCALCULATEDFORMID", strCalculatedFormId);
        
        log.debug("Cancel hmParam:==>"+hmParam);
        gmClinicalDownloadBean.cancelDownload(hmParam);
    }
    
    public String extractDetail(ArrayList alHeaderList,ArrayList alDetailList){
    	StringBuffer sb = new StringBuffer(); 
    	ArrayList alOrderedHeaderList = new ArrayList();
    	
    	for(int i=0;i<alHeaderList.size();i++){  
    		HashMap hmRecord = (HashMap)alHeaderList.get(i);
    		String strHeader = (String)hmRecord.get("ANSGRPID");
    		alOrderedHeaderList.add(strHeader);
    		sb.append(strHeader).append(",");
    	}
    	sb.replace(sb.length()-1, sb.length(), "\r");
    	
    	for(int i=0;i<alDetailList.size();i++){        		
        	HashMap hmRecord = (HashMap)alDetailList.get(i);
        	
        	for(int j=0;j<alOrderedHeaderList.size();j++){
        		sb.append((String)hmRecord.get(alOrderedHeaderList.get(j))+",");
        	}
        	sb.replace(sb.length()-1, sb.length(), "\r");
    	}    
        return sb.toString();
    }
   
    
}
