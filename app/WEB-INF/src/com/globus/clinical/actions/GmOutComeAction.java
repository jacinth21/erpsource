package com.globus.clinical.actions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalInterface;
import com.globus.clinical.beans.GmOutComeRptBean;
import com.globus.clinical.forms.GmEfficacyReportForm;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * 
 * This class used to load all the outcome reports 
 * 
 * @author 
 *
 */
public class GmOutComeAction extends GmClinicalDispatchAction
{

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    String strStudySiteId = "";
    
    /**
     *
     * This method used to load the primary endpoint details 
     * called only below study - SECURE-C, TRIUMPH and Amnios
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws AppError
     */
    
    public ActionForward reportNDIScore(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws AppError
    {   
    	String strMapping = "reportPEDScore";
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);
    	setIncludeInfo(form);
    	setAcadiaScore(gmStudyFilterForm);
        setNDIScore(gmStudyFilterForm);
        GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
        String strScore = GmCommonClass.parseNull(gmStudyFilterForm.getScore());

        //  60922 : Acadia score
        if(strScore.equals("60922")) 	strMapping = "reportAcadiaScore";
        
        String surgeryGrpID = GmCommonClass.getRuleValue(strStudyId,"SURGERY_GRP");
        gmStudyFilterForm.setAlType(GmCommonClass.getCodeList(surgeryGrpID));
        
        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strStudySiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudySiteId());
        strStudySiteId = strStudySiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
    	gmStudyFilterForm.setStrSiteIds(strStudySiteId);
        String scoreId = GmCommonClass.parseNull(gmStudyFilterForm.getScore());
        if(strOpt.equals("Report"))
        {
        	// PMT-43698: Amnios RT primary end point changes
        	// based on study id to get the dynamic method using Spring bean XML mapping
        	
        	GmClinicalInterface gmClinicalInterface = (GmClinicalInterface) GmCommonClass.getSpringBeanClass("xml/springBean/Clinical_Primary_Endpoint_Beans.xml",
                    strStudyId);
        	HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmClinicalInterface.loadPrimaryEndpointRpt(strStudyId,strStudySiteId,scoreId)) ;
        	gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);
        }        
    	return mapping.findForward(strMapping);
    }
    
    public ActionForward reportNDIPerImprove(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws AppError
    {   
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);
    	String strMapping = "reportPEDScore";
    	setIncludeInfo(form);
    	setAcadiaScore(gmStudyFilterForm);
        setNDIScore(gmStudyFilterForm);
        GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
        String strScore = GmCommonClass.parseNull(gmStudyFilterForm.getScore());
        
        //  60922 : Acadia score
        if(strScore.equals("60922")) 	strMapping = "reportAcadiaScore";
        
        String surgeryGrpID = GmCommonClass.getRuleValue(strStudyId,"SURGERY_GRP");
        gmStudyFilterForm.setAlType(GmCommonClass.getCodeList(surgeryGrpID));

        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strStudySiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudySiteId());
        strStudySiteId = strStudySiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
    	gmStudyFilterForm.setStrSiteIds(strStudySiteId);
        
        String scoreId = GmCommonClass.parseNull(gmStudyFilterForm.getScore());
        
        if(strOpt.equals("Report"))
        {
        	HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmOutComeRptBean.loadNDIPerImprove(strStudyId,strStudySiteId,scoreId)) ;
        	gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);
        }
        
    	return mapping.findForward(strMapping);
    }
    
    public ActionForward reportNDIImproveDetail(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {          
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);
    	String strMapping = "reportPEDScore";
    	setIncludeInfo(form);
    	setAcadiaScore(gmStudyFilterForm);
        setNDIScore(gmStudyFilterForm);
        GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
        String strScore = GmCommonClass.parseNull(gmStudyFilterForm.getScore());
        
        // 	60922 : Acadia score
        if(strScore.equals("60922")) 	strMapping = "reportAcadiaScore";
        
        String surgeryGrpID = GmCommonClass.getRuleValue(strStudyId,"SURGERY_GRP");
        gmStudyFilterForm.setAlType(GmCommonClass.getCodeList(surgeryGrpID));
        
        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strStudySiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudySiteId());
        strStudySiteId = strStudySiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
    	gmStudyFilterForm.setStrSiteIds(strStudySiteId);
    	
        try{
            
            if(strOpt.equals("Report")){
            String strTreatmentId = gmStudyFilterForm.getTreatmentId();
            HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmOutComeRptBean.loadNDIImproveDetail(strStudyId,strStudySiteId,strTreatmentId)) ;
            gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);
            }
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        return mapping.findForward(strMapping);
        
    }
    
    public ActionForward reportOutComeScore(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   
        GmEfficacyReportForm gmEfficacyReportForm = (GmEfficacyReportForm) form;
        gmEfficacyReportForm.loadSessionParameters(request);
        setIncludeInfo(gmEfficacyReportForm);
        GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
       
        
        strStudyId = GmCommonClass.parseNull(gmEfficacyReportForm.getSessStudyId());
        setAcadiaScore(gmEfficacyReportForm);
        String strSiteId = GmCommonClass.parseNull(gmEfficacyReportForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmEfficacyReportForm.getCheckSiteName()):strStudySiteId;
    	
    	gmEfficacyReportForm.setStrSiteIds(strSiteId);
        
        try{
        	//Fetching the Surgery Group from Rule Table
        	String surgeryGrpID = GmCommonClass.getRuleValue(strStudyId,"SURGERY_GRP");
            //gmEfficacyReportForm.setAlType(GmCommonClass.getCodeList("EFFRT"));
            gmEfficacyReportForm.setAlType(GmCommonClass.getCodeList(surgeryGrpID));
            
            if(strOpt.equals("Report")){
                    String strType = gmEfficacyReportForm.getTypeId();
                    HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmOutComeRptBean.loadOutComeScore(strStudyId,strSiteId,strType)) ;
                    gmEfficacyReportForm.setHmCrossTabReport(hmFinalResult);
            }
            
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
            
        return mapping.findForward("reportOutComeScore");
    }
    
    public ActionForward reportNeurologicalStatus(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception
	{   
		GmEfficacyReportForm gmEfficacyReportForm = (GmEfficacyReportForm) form;
		gmEfficacyReportForm.setStudyListId("GPR003");
		gmEfficacyReportForm.setStudyListGrp("NSRSDL");
		gmEfficacyReportForm.loadSessionParameters(request);
		setIncludeInfo(gmEfficacyReportForm);
		
		strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
		
		
		String strSiteId = GmCommonClass.parseNull(gmEfficacyReportForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmEfficacyReportForm.getCheckSiteName()):strStudySiteId;
    	
    	gmEfficacyReportForm.setStrSiteIds(strSiteId);
    	
		GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
		
		try{
	    
		    //if(strOpt.equals("Report"))
			{
	            String strType = gmEfficacyReportForm.getTypeId();
	            log.debug("strStudy- Neurological============="+strStudyId);
	            HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmOutComeRptBean.loadNeurologicalStatus(strStudyId,strSiteId,strType)) ;
	            gmEfficacyReportForm.setHmCrossTabReport(hmFinalResult);
	            
		    }
		    
		}
		catch(Exception exp){
		    exp.printStackTrace();
		    throw new AppError(exp);
		}
	    
		return mapping.findForward("reportNeurologicalStatus");
	}
    
    public ActionForward reportPatientSatisfaction(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   
        GmEfficacyReportForm gmEfficacyReportForm = (GmEfficacyReportForm) form;
        gmEfficacyReportForm.loadSessionParameters(request);
        setIncludeInfo(gmEfficacyReportForm);
        GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
        
        
        strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        
        String strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
    	
        
    	gmEfficacyReportForm.setStrSiteIds(strSiteId);
        
        try{
        	// PMT-50759: Amnios study Patient Satisfaction changes
        	// based on study to load the correct period
       		gmEfficacyReportForm.setAlType(gmOutComeRptBean.loadPeriod(strStudyId));
            
            
            if(strOpt.equals("Report")){
            List alResult = new ArrayList();
            String strType = gmEfficacyReportForm.getTypeId();
            alResult = gmOutComeRptBean.loadPatientSatisfaction(strStudyId,strSiteId,strType).getRows();
            gmEfficacyReportForm.setLdtResult(alResult); 
            
            log.debug("Start" );
        	HashMap hmParam = new HashMap();
    	    hmParam.put("TEMPLATE","GmClinicalRptPatientStatisfication.vm");
    	    hmParam.put("TEMPLATEPATH","clinical/templates");

    	    hmParam.put("RLIST", gmEfficacyReportForm.getLdtResult());
    	   String strXMLData =  gmOutComeRptBean.getXmlGridData(hmParam);
    	   log.debug("XML Data  : "+ strXMLData);
    	   gmStudyFilterForm.setGridXmlData(strXMLData);
            log.debug("Exit");
            }
            
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
            
        return mapping.findForward("reportPatientSatisfaction");
    }
    
    public ActionForward reportPEDScore(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception
    {   
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);
        setIncludeInfo(form);
        setNDIScore((GmStudyFilterForm)form);    	
    	GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
    	
    	strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
    	
    	String strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
    	
    	gmStudyFilterForm.setStrSiteIds(strSiteId);
    	try{
    		if(strOpt.equals("Report"))
    		{
    			HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmOutComeRptBean.loadNDIScore(strStudyId,strSiteId,"")) ;
    			gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);
    		}
    	}
    	catch(Exception exp){
    		exp.printStackTrace();
    		throw new AppError(exp);
    	}
    
    	return mapping.findForward("reportPEDScore");
    }
    
    public ActionForward reportZCQSuccess(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws AppError
	{   
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);	
		setIncludeInfo(form);
		GmOutComeRptBean gmOutComeRptBean = new GmOutComeRptBean();
		
		strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
        strStudySiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudySiteId());
        strStudySiteId = strStudySiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
    	gmStudyFilterForm.setStrSiteIds(strStudySiteId);
		
		
		String scoreId = GmCommonClass.parseNull(gmStudyFilterForm.getScore());
	    if(strOpt.equals("Report"))
	    {
	    	HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmOutComeRptBean.loadZCQSuccess(strStudyId,strStudySiteId,scoreId)) ;
	    	gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);
	    }
		
		return mapping.findForward("reportPEDScore");
	}

    
    public void setNDIScore(GmStudyFilterForm form) throws AppError 
	{   
    	if(form.getScore().trim().equalsIgnoreCase("0")){
    	String strStudyId  = GmCommonClass.parseNull(form.getSessStudyId());
    	form.setScore(GmCommonClass.parseNull(GmCommonClass.getRuleValue(strStudyId, "OUTNDISCORE")));
    	}
   	}
    public void setAcadiaScore(GmStudyFilterForm form) throws AppError 
	{   
    	String strTypeId = GmCommonClass.parseNull(form.getTypeId());
    	String scoreGrp = GmCommonClass.getRuleValue(form.getTypeId(),"PEP_SCORE");
        if(strTypeId.equals("61107") || strTypeId.equals("61108") || strTypeId.equals("61109")){
        	form.setAlScore(GmCommonClass.getCodeList(scoreGrp));
        	form.setScore(GmCommonClass.parseNull(GmCommonClass.getRuleValue(strStudyId, "OUTNDISCORE")));
        	if(strTypeId.equals("61109")){ // 61109:ODI% Improve Details, 60922:ACADIA
        		form.setAlTreatment(GmCommonClass.getCodeList(strStudyId));
        	}
        }
   	}
}
