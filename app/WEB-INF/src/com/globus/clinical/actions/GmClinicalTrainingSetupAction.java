package com.globus.clinical.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalTrainingBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.clinical.forms.GmClinicalTrainingForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;

public class GmClinicalTrainingSetupAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		// to
		// Initialize
		// the
		// Logger
		// Class.
		ArrayList alReportList = new ArrayList();
		ArrayList alStatusList = new ArrayList();
		ArrayList alTrainingForList = new ArrayList();
		ArrayList alTrainingReasonList = new ArrayList();
		ArrayList alTraineeNameList = new ArrayList();
		HashMap hmParams = new HashMap();
		HashMap hmFetchValue = new HashMap();
		String strUserId = " ";
		String strStudyId = " ";
		String strStudySiteId = " ";
		String strTrainingFor = " ";
		String strTraineeName = " ";
		String strTrainingReason = " ";
		String strTrainingScope = " ";
		String strCompletionDate = " ";
		String strStatus = " ";
		String strTrainingRecordId = " ";
		String strButtonDisabled = "";

		log.debug("Entered setup");

		GmCommonClass gmCommonClass = new GmCommonClass();
		GmCommonBean gmCommonBean= new GmCommonBean();
		GmClinicalTrainingBean gmClinicalTrainingBean = new GmClinicalTrainingBean();
		GmClinicalBean gmClinicalBean = new GmClinicalBean();

		GmClinicalTrainingForm gmClinicalTrainingForm = (GmClinicalTrainingForm) form;
		gmClinicalTrainingForm.loadSessionParameters(request);

		strUserId = gmClinicalTrainingForm.getUserId();
		strStudyId = gmClinicalTrainingForm.getSessStudyId();
		strStudySiteId = gmClinicalTrainingForm.getSessStudySiteId();

		log.debug("strStudyId from session " + strStudyId);
		log.debug("strStudySiteId from session " + strStudySiteId); 
		String strOpt = gmClinicalTrainingForm.getStrOpt();
		hmParams = GmCommonClass.getHashMapFromForm(gmClinicalTrainingForm);
		hmParams.put("STUDYID", strStudyId);
		hmParams.put("SITENAMES", gmClinicalTrainingForm.getSessStudySiteId());

		log.debug("hmParams Values ===== " + hmParams);
		
		strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParams);
		
		strTrainingRecordId = gmClinicalTrainingForm.getStrTrainingRecordId();
		gmClinicalTrainingForm.setStrTrainingRecordId(strTrainingRecordId);

		alTrainingForList = GmCommonClass.getCodeList("CRTPE");
		alTrainingReasonList = GmCommonClass.getCodeList("CRRFT");
		alStatusList = GmCommonClass.getCodeList("CRSTA");

		strTrainingFor = GmCommonClass.parseNull((String) hmParams.get("TRAININGFORLIST"));

		gmClinicalTrainingForm.setAlTrainingForList(alTrainingForList);
		//gmClinicalTrainingForm.setAlTrainingReasonList(alTrainingReasonList);
		gmClinicalTrainingForm.setAlStatusList(alStatusList);

		if (strOpt.equals("save")) {
			strTrainingRecordId = gmClinicalTrainingForm.getStrTrainingRecordId();
			// call SaveTrainingDetail Method - which saves the Training Details
			strTrainingRecordId = gmClinicalTrainingBean.saveTrainingDetail(hmParams);
			gmClinicalTrainingForm.setStrTrainingRecordId(strTrainingRecordId);			
		}

		if (gmClinicalTrainingForm.getStrTrainingRecordId() != null && !strOpt.equals("reload")) {
			// Fetch the Records
			hmFetchValue = (HashMap) gmClinicalTrainingBean.fetchTrainingDetail(gmClinicalTrainingForm.getStrTrainingRecordId());

			gmClinicalTrainingForm.setTrainingForList(gmCommonClass.parseNull((String) hmFetchValue.get("TRAININGFOR")));
			gmClinicalTrainingForm.setTraineeNameList(gmCommonClass.parseNull((String) hmFetchValue.get("TRAININGNAME")));
			gmClinicalTrainingForm.setTrainingReasonList(gmCommonClass.parseNull((String) hmFetchValue.get("TRAININGREASON")));
			gmClinicalTrainingForm.setTrainingScope(gmCommonClass.parseNull((String) hmFetchValue.get("TRAININGSCOPE")));
			gmClinicalTrainingForm.setCompletionDate(gmCommonClass.parseNull((String) hmFetchValue.get("COMPLETIONDATE")));
			gmClinicalTrainingForm.setStatusList(gmCommonClass.parseNull((String) hmFetchValue.get("STATUS")));
			strTrainingFor = gmCommonClass.parseNull((String) hmFetchValue.get("TRAININGFOR"));
			strOpt = "reload";
		}

		if (strOpt.equals("reload")) {
		//	log.debug("strStudySiteId " + strStudySiteId+" strTrainingFor   "+strTrainingFor);
			if (strTrainingFor.equals("2400")) {				
				alTraineeNameList = (ArrayList) gmClinicalTrainingBean.loadSiteScInfo(strStudySiteId);
				gmClinicalTrainingForm.setAlTraineeNameList(alTraineeNameList);
				alTrainingReasonList = GmCommonClass.getCodeList("TRRFSC");
				gmClinicalTrainingForm.setAlTrainingReasonList(alTrainingReasonList);
			}else if (strTrainingFor.equals("2401")) {
				alTraineeNameList = (ArrayList) gmClinicalTrainingBean.loadSurgeonInfo(strStudySiteId,strTrainingFor);
				gmClinicalTrainingForm.setAlTraineeNameList(alTraineeNameList);
				alTrainingReasonList = GmCommonClass.getCodeList("TRRFPI");
				gmClinicalTrainingForm.setAlTrainingReasonList(alTrainingReasonList);
			}else if (strTrainingFor.equals("2404")) {
				alTraineeNameList = (ArrayList) gmClinicalTrainingBean.loadSurgeonInfo(strStudySiteId,strTrainingFor);
				gmClinicalTrainingForm.setAlTraineeNameList(alTraineeNameList);
				alTrainingReasonList = GmCommonClass.getCodeList("TRRFSU");
				gmClinicalTrainingForm.setAlTrainingReasonList(alTrainingReasonList);
			}
			else if (strTrainingFor.equals("2402")){//CRA
				gmClinicalTrainingForm.setAlTraineeNameList(gmCommonBean.loadEmployeeList("2007", 3, 3));
				alTrainingReasonList = GmCommonClass.getCodeList("TRRFCD");
				gmClinicalTrainingForm.setAlTrainingReasonList(alTrainingReasonList);				
			}
			else if (strTrainingFor.equals("2403")){//DM
				gmClinicalTrainingForm.setAlTraineeNameList(gmCommonBean.loadEmployeeList("2007", 4, 4));
				alTrainingReasonList = GmCommonClass.getCodeList("TRRFCD");
				gmClinicalTrainingForm.setAlTrainingReasonList(alTrainingReasonList);
			}
		}
		
		Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String currentDate = dateFormat.format(cal.getTime());
        gmClinicalTrainingForm.setCurrentDate(currentDate);
        gmClinicalTrainingForm.setLockedStudyDisable(strButtonDisabled);
        
		log.debug("Exit");
		return mapping.findForward("success");

	}
}
