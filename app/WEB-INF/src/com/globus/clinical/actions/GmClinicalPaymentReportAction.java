package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalPaymentBean;

import com.globus.clinical.forms.GmClinicalPaymentReportForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;

public class GmClinicalPaymentReportAction extends GmClinicalDispatchAction {

	/**
	 * Fetches the PaymentTrackingReport detail 
	 * 
	 * 
	 * @param ActionMapping mapping 
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws com.globus.common.beans.AppError
	 */
    public ActionForward fetchPaymentTrackingReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
		log.debug("Inside fetchPaymentTrackingReport");	
		String strForward = "gmClinicalPaymentReport";
		String strSiteId = "";
		String strStudyId = "";
 	    String strXmlGridData = "";
		HashMap hmParam = new HashMap();
		log.debug("strForward  " + strForward);	
		String strCondition = getAccessFilter(request, response);
		GmClinicalPaymentReportForm gmClinicalPaymentReportForm = (GmClinicalPaymentReportForm) form;
        gmClinicalPaymentReportForm.loadSessionParameters(request);

		String strOpt = gmClinicalPaymentReportForm.getStrOpt();

        strStudyId = GmCommonClass.parseNull(gmClinicalPaymentReportForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmClinicalPaymentReportForm.getSessStudySiteId());
		
        gmClinicalPaymentReportForm.setStudyListId(strStudyId);
        gmClinicalPaymentReportForm.setSiteList(strSiteId);
        gmClinicalPaymentReportForm.setAlStatus((ArrayList)GmCommonClass.getCodeList("PTSTA",strCondition));
        gmClinicalPaymentReportForm.setAlRequestType((ArrayList)GmCommonClass.getCodeList("PTREQ",strCondition));
        gmClinicalPaymentReportForm.setAlReasonForPayment((ArrayList)GmCommonClass.getCodeList("PTRES",strCondition));
		
		log.debug("Inside gmClinicalPaymentReportForm " + gmClinicalPaymentReportForm.getShowDetails());
		
		if(strOpt.equalsIgnoreCase("loadReport")){
			hmParam = GmCommonClass.getHashMapFromForm(gmClinicalPaymentReportForm);
			GmClinicalPaymentBean gmClinicalPaymentBean = new GmClinicalPaymentBean();
			HashMap hmParamV = new HashMap();
			ArrayList alResult;
			alResult = (ArrayList)gmClinicalPaymentBean.loadPaymentTrackingReport(hmParam, strCondition);
	        
	        hmParamV.put("TEMPLATE","GmClinicalPaymentReport.vm");
	        hmParamV.put("TEMPLATEPATH","clinical/templates");
	        hmParamV.put("RLIST",alResult);

	        strXmlGridData = gmClinicalPaymentBean.getXmlGridData(hmParamV);
	        gmClinicalPaymentReportForm.setGridXmlData(strXmlGridData);
		}

		return mapping.findForward(strForward);
    }
    
	/**
	 * Fetches the PaymentTrackingReport detail 
	 * 
	 * 
	 * @param ActionMapping mapping 
	 * @param ActionForm form
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @return ActionForward
	 * @throws com.globus.common.beans.AppError
	 */
    public ActionForward fetchPaymentTrackingDetailsReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
		log.debug("Inside fetchPaymentTrackingReport");	
		String strForward = "gmClinicalPaymentDetailsReport";
		String strSiteId = "";
		String strStudyId = "";
 	    String strXmlGridData = "";
		HashMap hmParam = new HashMap();
		log.debug("strForward  " + strForward);	
		String strCondition = getAccessFilter(request, response);
		GmClinicalPaymentReportForm gmClinicalPaymentReportForm = (GmClinicalPaymentReportForm) form;
        gmClinicalPaymentReportForm.loadSessionParameters(request);

		String strOpt = gmClinicalPaymentReportForm.getStrOpt();

        strStudyId = GmCommonClass.parseNull(gmClinicalPaymentReportForm.getSessStudyId());
        strSiteId = GmCommonClass.parseNull(gmClinicalPaymentReportForm.getSessStudySiteId());
		
        gmClinicalPaymentReportForm.setStudyListId(strStudyId);
        gmClinicalPaymentReportForm.setSiteList(strSiteId);
        gmClinicalPaymentReportForm.setAlStatus((ArrayList)GmCommonClass.getCodeList("PTSTA",strCondition));
        gmClinicalPaymentReportForm.setAlRequestType((ArrayList)GmCommonClass.getCodeList("PTREQ",strCondition));
        gmClinicalPaymentReportForm.setAlReasonForPayment((ArrayList)GmCommonClass.getCodeList("PTRES",strCondition));
		
		if(strOpt.equalsIgnoreCase("loadReport")){
			hmParam = GmCommonClass.getHashMapFromForm(gmClinicalPaymentReportForm);
			GmClinicalPaymentBean gmClinicalPaymentBean = new GmClinicalPaymentBean();
			HashMap hmParamV = new HashMap();
			ArrayList alResult;

			alResult = (ArrayList)gmClinicalPaymentBean.loadPaymentTrackingDetailsReport(hmParam, strCondition);
	        
	        hmParamV.put("TEMPLATE","GmClinicalPaymentDetailsReport.vm");
	        hmParamV.put("TEMPLATEPATH","clinical/templates");
	        hmParamV.put("RLIST",alResult);

	        strXmlGridData = gmClinicalPaymentBean.getXmlGridData(hmParamV);
	        gmClinicalPaymentReportForm.setGridXmlData(strXmlGridData);
		}

		return mapping.findForward(strForward);
    }

	
}
