package com.globus.clinical.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmDemographicRptBean;
import com.globus.clinical.forms.GmRptImplantInfoForm;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


public class GmDemographicAction extends GmClinicalDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.

    public ActionForward reportDgraph(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {       
        String strOptLocal = "";
        String strStudySiteId = "";
        GmDemographicRptBean gmDemographicRptBean = new GmDemographicRptBean();
        log.debug("Enter 1");
        GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
        gmStudyFilterForm.loadSessionParameters(request);
        setIncludeInfo(form);
        
        strStudyId = gmStudyFilterForm.getSessStudyId();
        String strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId()); 
        strOptLocal = gmStudyFilterForm.getStrOpt();
        log.debug(" OPT LOCAL " + strOptLocal);
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        if(strOptLocal.indexOf("Link") == -1) 
        {
        	strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
        	gmStudyFilterForm.setStrSiteIds(strSiteId);
        	gmStudyFilterForm.setLdtResult(gmDemographicRptBean.reportDGraph(strStudyId,  strSiteId, strOptLocal));

        }            
        log.debug("Exit");
        // PMT-50723: Based on study to forward different JSP
        // PC-1802: Reflect study redirect to new JSP
        strOptLocal = strStudyId.equalsIgnoreCase("GPR009")? strOptLocal.concat("Amnios"): strStudyId.equalsIgnoreCase("GPR010")? strOptLocal.concat("Reflect"): strOptLocal;
        
        return mapping.findForward(strOptLocal);
    }
    
    
    public ActionForward reportLevelTreated(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {       
        String strOptLocal = "";
        String strStudySiteId ="";
        log.debug("Enter");
        GmDemographicRptBean gmDemographicRptBean = new GmDemographicRptBean();
        GmRptImplantInfoForm gmRptImplantInfoForm = (GmRptImplantInfoForm) form;
        gmRptImplantInfoForm.loadSessionParameters(request);
        setIncludeInfo(gmRptImplantInfoForm);        
        strStudyId = gmRptImplantInfoForm.getSessStudyId();
        String strSiteId = GmCommonClass.parseNull(gmRptImplantInfoForm.getSessSiteId()); 
        String pGrpID = GmCommonClass.getRuleValue(strStudyId,"DLVLT_GRP");
        gmRptImplantInfoForm.setAlPartTypes(GmCommonClass.getCodeList(pGrpID));
        
        strOptLocal = gmRptImplantInfoForm.getStrOpt();
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        /*
        if(strOptLocal.equals("")) 
        {
            //gmStudyFilterForm.loadSessionParameters(request);
            HttpSession  httpSession = request.getSession(false);                
            strOptLocal =(String) httpSession.getAttribute("strSessOpt");
        }
		*/
        log.debug("strStudyId==============="+strStudyId);
        log.debug("strOptLocal==============="+strOptLocal.indexOf("Link"));
        
        if(strOptLocal.indexOf("Link") == -1) 
        {
        	String ptype = gmRptImplantInfoForm.getCbo_PartType();
        	strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
        	gmStudyFilterForm.setStrSiteIds(strSiteId);
            gmStudyFilterForm.setLdtResult(gmDemographicRptBean.reportLevelTreated(strStudyId,  strSiteId, strOptLocal, ptype));
            
            //log.debug("Im Hereeeeeee111");
        	HashMap hmParam = new HashMap();
    	    hmParam.put("TEMPLATE","GmClinicalDemoLevelTreated.vm");
    	    hmParam.put("TEMPLATEPATH","clinical/templates");
    	    hmParam.put("RLIST", gmStudyFilterForm.getLdtResult());
    	    hmParam.put("PARTYTYPE", gmRptImplantInfoForm.getCbo_PartType());
    	   String strXMLData = gmDemographicRptBean.getXmlGridData(hmParam);
    	//   log.debug("XML Data11  : "+ strXMLData);
    	   gmStudyFilterForm.setGridXmlData(strXMLData);
            log.debug("Exit");
            
        }
        else
        	gmStudyFilterForm.setGridXmlData("");
       
       
        return mapping.findForward(strOptLocal);
    }
    
    public ActionForward reportDiagnosis(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   
    	String strStudySiteId = "";
    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    	gmStudyFilterForm.loadSessionParameters(request);
    	setIncludeInfo(form);    	
    	GmDemographicRptBean gmDemographicRptBean = new GmDemographicRptBean();
    	
    	strStudyId = gmStudyFilterForm.getSessStudyId();
    	String strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
    	 if(!strSiteId.equals(""))
         {
         	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
         }
    	 strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
    	 gmStudyFilterForm.setStrSiteIds(strSiteId);
    	//if(strOpt.equals("Report"))
    	{
    		HashMap hmFinalResult = GmCommonClass.parseNullHashMap(gmDemographicRptBean.reportDiagnosis(strStudyId,strSiteId)) ;
    		gmStudyFilterForm.setHmCrossTabReport(hmFinalResult);

    	}
    	return mapping.findForward("reportDiagnosis");
    }
    
    public ActionForward reportImplantInfo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
                   
        log.debug("Enter");
        String strStudySiteId = "";
        GmDemographicRptBean gmDemographicRptBean = new GmDemographicRptBean();
        GmRptImplantInfoForm gmRptImplantInfoForm = (GmRptImplantInfoForm) form;
        gmRptImplantInfoForm.loadSessionParameters(request);
        setIncludeInfo(gmRptImplantInfoForm);   
        
        strStudyId = gmRptImplantInfoForm.getSessStudyId();
        String strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
        
        String pGrpID = GmCommonClass.getRuleValue(strStudyId,"DIMPL_INFO_GRP");
        gmRptImplantInfoForm.setAlPartTypes(GmCommonClass.getCodeList(pGrpID));
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        
        //if (gmRptImplantInfoForm.getStrOpt().equals("Report"))
        {
            //String strStudyId = gmRptImplantInfoForm.getStudyListId();   
            //String strSiteNames = GmCommonClass.createInputString(gmRptImplantInfoForm.getCheckSiteName());
            String strSiteNames = strSiteId.equals("")? GmCommonClass.createInputString(gmStudyFilterForm.getCheckSiteName()):strStudySiteId;
            gmRptImplantInfoForm.setStrSiteIds(strSiteNames);
            String strPartType = gmRptImplantInfoForm.getCbo_PartType();                
            gmRptImplantInfoForm.setLdtResult(gmDemographicRptBean.reportImplantInfo(strStudyId, strSiteNames, strPartType));  
            

        	HashMap hmParam = new HashMap();
    	    hmParam.put("TEMPLATE","GmClinicalDemoImplantsUsed.vm");
    	    hmParam.put("TEMPLATEPATH","clinical/templates");
    	    hmParam.put("PARTYTYPE",  strPartType);
    	    hmParam.put("RLIST", gmStudyFilterForm.getLdtResult());
    	   String strXMLData = gmDemographicRptBean.getXmlGridData(hmParam);
    	  // log.debug("XML Data  : "+ strXMLData);
    	   gmStudyFilterForm.setGridXmlData(strXMLData);
            log.debug("Exit");
        }       
    
        log.debug("Exit");
        return mapping.findForward("success");
    }
}
