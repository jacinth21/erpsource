package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmAdverseEventRptBean;
import com.globus.clinical.beans.GmClinicalAdverseEventInterface;
import com.globus.clinical.beans.GmClinicalStudyReflectBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmAdverseEventForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;

public class GmAdverseEventAction extends GmClinicalDispatchAction
{
	 String strSiteId="";
    public ActionForward reportAdverseEvent(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws AppError
    {   
        String strStudySiteId = "";
        String strOpt = "";
    	GmAdverseEventRptBean gmAdverseEventRptBean = new GmAdverseEventRptBean();
        GmAdverseEventForm gmAdverseEventForm = (GmAdverseEventForm) form;
        gmAdverseEventForm.loadSessionParameters(request);
        setIncludeInfo(gmAdverseEventForm);
        strOpt = gmAdverseEventForm.getStrOpt();
        strStudyId = gmAdverseEventForm.getSessStudyId();
        gmAdverseEventForm.setStudyListId(strStudyId);
        strSiteId = GmCommonClass.parseNull(gmAdverseEventForm.getSessSiteId());
        
        if(!strSiteId.equals(""))
        {
        	strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
        }
        strSiteId = strSiteId.equals("")? GmCommonClass.createInputString(gmAdverseEventForm.getCheckSiteName()):strStudySiteId;
        
        gmAdverseEventForm.setStrSiteIds(strSiteId);
        
        List alResult = new ArrayList();
        String strPatientId = GmCommonClass.parseNull(gmAdverseEventForm.getSessPatientId());
        strPatientId = strPatientId.equals("")? gmAdverseEventForm.getPatientId():strPatientId;
        String strTimePoint = request.getParameter("strTimePoint");
        if(strOpt.equals("Dashboard"))
        {
    	    strSiteId = gmAdverseEventRptBean.loadSiteId(strStudyId);
            request.setAttribute("HIDDENSITELIST", "DASHBOARDTOGGEL");
        }
        log.debug(" Patient Id is " +strPatientId+" strStudyId Id is " +strStudyId+" strStudyId Id is  " +strSiteId+strOpt);
        if(strOpt.equals("Report") ||strOpt.equals("Dashboard") )
        {
    	    //PC-1807 Reflect study - Adverse Event report
        	// Implemented an interface call for Adverse Event report
        	GmClinicalAdverseEventInterface gmClinicalAdverseEventInterface = (GmClinicalAdverseEventInterface) GmCommonClass
    				.getSpringBeanClass("xml/Clinical_AdverseEvent_Beans.xml", strStudyId);
        	alResult = gmClinicalAdverseEventInterface.loadAdverseEvent(strStudyId,strSiteId,strPatientId,strOpt,strTimePoint);
        	gmAdverseEventForm.setLdtResult(alResult);
        }	
        
        log.debug("Start" );
    	HashMap hmParam = new HashMap();
    	HashMap hmData = new HashMap();//Passing study id to hashmap 
    	hmData.put("STUDYID", strStudyId);
    	hmParam.put("hmData", hmData);
	    hmParam.put("TEMPLATE","GmClinicalRptAdverseEvent.vm");
	    hmParam.put("TEMPLATEPATH","clinical/templates");

	    hmParam.put("RLIST", gmAdverseEventForm.getLdtResult());
	   String strXMLData = gmAdverseEventRptBean.getXmlGridData(hmParam);
	   log.debug("XML Data  : "+ strXMLData);
	   gmStudyFilterForm.setGridXmlData(strXMLData);
        log.debug("Exit");
            
        return mapping.findForward("reportAdverseEvent");
    }
}
