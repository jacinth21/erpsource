package com.globus.clinical.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.clinical.forms.GmClinicalSurgeonSetupForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;

public class GmClinicalSurgeonSetupAction extends GmAction
{

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
        
        
            log.debug("Enter");
            GmClinicalSurgeonSetupForm gmClinicalSurgeonSetupForm = (GmClinicalSurgeonSetupForm) form;
            gmClinicalSurgeonSetupForm.loadSessionParameters(request);

            GmStudyBean gmStudyBean = new GmStudyBean();
            GmPartyBean gmPartyBean = new GmPartyBean();

            GmClinicalBean gmClinicalBean = new GmClinicalBean();
            GmPatientBean gmPatientBean = new GmPatientBean();

            HashMap hmParam = new HashMap();
            HashMap hmReturn = new HashMap();
            String strOpt = gmClinicalSurgeonSetupForm.getStrOpt();
            String strButtonDisabled = "";
            
            
            String strStudyId = (String)request.getSession().getAttribute("strSessStudyId");
            String strStudySiteId = (String)request.getSession().getAttribute("strSessStudySiteId");
            
            gmClinicalSurgeonSetupForm.setStudyList(strStudyId);
            gmClinicalSurgeonSetupForm.setSiteNameList(strStudySiteId);
            
            hmParam = GmCommonClass.getHashMapFromForm(gmClinicalSurgeonSetupForm);
            hmParam.put("SESSSTUDYID", strStudyId);
            strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParam);        
            if (strOpt.equals("edit"))
            {
                hmReturn = gmClinicalBean.loadSurgeonMapInfo(gmClinicalSurgeonSetupForm.getHsurgeonSetupID());
                
                log.debug("HMRETURN is "+hmReturn);
                GmCommonClass.getFormFromHashMap(gmClinicalSurgeonSetupForm, hmReturn);
            } 
            else if(strOpt.equals("save") || strOpt.equals("update"))
            {
                gmClinicalBean.saveClinicalSurgeonSiteMap(hmParam);

            }
    		
            // log.debug(gmClinicalSiteMapForm);

         //   if (!strStudySiteId.equals("0") && !strStudySiteId.equals(" "))
          //  {
                request.setAttribute("SURGEONSITES", gmClinicalBean.loadSurgeonMapDetails(strStudySiteId));
          //  }
           // gmClinicalSurgeonSetupForm.setAlStudyList(gmStudyBean.reportStudyNameList());
           // gmClinicalSurgeonSetupForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmClinicalSurgeonSetupForm
                          //  .getStudyList())); // To discuss about the static and access condition
            gmClinicalSurgeonSetupForm.setAlSurgeonNameList(gmPartyBean.reportPartyNameList("7000"));
            gmClinicalSurgeonSetupForm.setAlSurgeonType(GmCommonClass.getCodeList("SRTYP"));
            // if study is locked then set the button as disabled
            gmClinicalSurgeonSetupForm.setLockedStudyDisable(strButtonDisabled);
        return mapping.findForward("success");

    }
}
