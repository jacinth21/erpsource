package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalIRBApprovalBean;
import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmClinicalIRBApprovalForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalIRBApprovalAction extends GmAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception
	{
		
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		HttpSession session =request.getSession(false);
		String strStudyID=(String)session.getAttribute("strSessStudyId");
		
		GmClinicalIRBApprovalForm gmClinicalIRBApprovalForm = (GmClinicalIRBApprovalForm) form;
		gmClinicalIRBApprovalForm.loadSessionParameters(request);
		gmClinicalIRBApprovalForm.setStudyID(strStudyID);
		
		String strReturnTo="gmclinicalirbappr";	
		String strAction = GmCommonClass.parseNull(gmClinicalIRBApprovalForm.getStrOpt());
		String strButtonDisabled = "";
		
		HashMap hmParamVal = new HashMap();
		hmParamVal = GmCommonClass.getHashMapFromForm(gmClinicalIRBApprovalForm);
		hmParamVal.put("SESSSTUDYID", strStudyID);
		//disabling the submit button based on the study id
		strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParamVal);
		log.debug("strButtonDisabled::"+strButtonDisabled);
		if(strAction.equals("save_irb")){
			String strIrbid = saveIRBApproval(gmClinicalIRBApprovalForm,request);
			HashMap hmParam = new HashMap();
			hmParam.put("IRBID", gmClinicalIRBApprovalForm.getIrbid());			
			HashMap hmIRBApproval  = fetchIRBApproval(hmParam);
			
			ArrayList alIRBApproval = (ArrayList)hmIRBApproval.get("IRBAPPROVAL");
			gmClinicalIRBApprovalForm.setAlLogReasons((ArrayList)hmIRBApproval.get("IRBLOG")); 
			
			if(alIRBApproval!=null && alIRBApproval.size()>0){
				HashMap tempIRBApproval = (HashMap)alIRBApproval.get(0);
				gmClinicalIRBApprovalForm = (GmClinicalIRBApprovalForm)
												GmCommonClass.getFormFromHashMap(gmClinicalIRBApprovalForm, tempIRBApproval);
			}
			
//			HashMap hmParam = new HashMap();
//			hmParam.put("STUDYSITEID", gmClinicalIRBApprovalForm.getStudySiteID());
//			hmParam.put("IRBLOGID", gmClinicalIRBApprovalForm.getIrbid());
//			
//			HashMap hmIRBApproval = fetchIRBApproval(hmParam);
//			String gridData  = getGridData((ArrayList)hmIRBApproval.get("IRBAPPROVAL"));
//			
//			gmClinicalIRBApprovalForm.setGridData(gridData);
			strAction="list_irb";
			
		}
		if(strAction.equals("edit_irb")){
			
			HashMap hmParam = new HashMap();
			hmParam.put("IRBID", gmClinicalIRBApprovalForm.getIrbid());			
			HashMap hmIRBApproval  = fetchIRBApproval(hmParam);
			
			ArrayList alIRBApproval = (ArrayList)hmIRBApproval.get("IRBAPPROVAL");
			gmClinicalIRBApprovalForm.setAlLogReasons((ArrayList)hmIRBApproval.get("IRBLOG")); 
			
			if(alIRBApproval!=null && alIRBApproval.size()>0){
				HashMap tempIRBApproval = (HashMap)alIRBApproval.get(0);
				gmClinicalIRBApprovalForm = (GmClinicalIRBApprovalForm)
												GmCommonClass.getFormFromHashMap(gmClinicalIRBApprovalForm, tempIRBApproval);
			}
			strAction = "list_irb";
		}
		
		if(strAction.equals("list_irb")){
			HashMap hmParam = new HashMap();
			hmParam.put("STUDYSITEID", gmClinicalIRBApprovalForm.getStudySiteID());
			hmParam.put("IRBLOGID", gmClinicalIRBApprovalForm.getIrbid());
			HashMap hmIRBApproval = fetchIRBApproval(hmParam);
			
			gmClinicalIRBApprovalForm.setAlLogReasons((ArrayList)hmIRBApproval.get("IRBLOG"));
			String gridData = getGridData((ArrayList)hmIRBApproval.get("IRBAPPROVAL"));
			gmClinicalIRBApprovalForm.setGridData(gridData);
			
			gmClinicalIRBApprovalForm.setStrOpt(strAction);
			strAction="onLoad";
			
		}
		if(strAction.equals("onLoad")){
			loadIRBApproval(gmClinicalIRBApprovalForm,request);
		}
		gmClinicalIRBApprovalForm.setLockedStudyDisable(strButtonDisabled);
		return mapping.findForward(strReturnTo);
			
	}
	public String saveIRBApproval(GmClinicalIRBApprovalForm gmClinicalIRBApprovalForm, HttpServletRequest request) throws Exception{

		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalIRBApprovalForm);
		GmClinicalIRBApprovalBean gmClinicalIRBBean = new GmClinicalIRBApprovalBean();
		String strIrbid = gmClinicalIRBBean.saveIRBApproval(hmParam);
		gmClinicalIRBApprovalForm.setIrbid(strIrbid);
		log.debug(" # # # irbid"+strIrbid);
		return strIrbid;
	}
	
	public void loadIRBApproval(GmClinicalIRBApprovalForm gmClinicalIRBApprovalForm, HttpServletRequest request) throws Exception{
		
		GmPatientBean gmPatientBean = new GmPatientBean();
		String strStudyID = gmClinicalIRBApprovalForm.getStudyID();
		
		//Get SiteList from the Study ID.
		gmClinicalIRBApprovalForm.setAlSiteList(gmPatientBean.loadSiteFromStudy(strStudyID));
		
		//Get IRB Approval Type From CodeLookup.
		gmClinicalIRBApprovalForm.setAlApprovalTypeList(GmCommonClass.getCodeList("IRBATY"));
		
		//Get IRB Approval Reason From CodeLookup.
		gmClinicalIRBApprovalForm.setAlApprovalReasonList(GmCommonClass.getCodeList("IRBARS"));
		
		//Get IRB Approval Status From CodeLookup.
		//gmClinicalIRBApprovalForm.setAlApprovalStatusList(GmCommonClass.getCodeList("IRBSTA"));
		
		
	}
	
	public HashMap fetchIRBApproval(HashMap hmParam) throws Exception{
		GmClinicalIRBApprovalBean gmClinicalIRBBean = new GmClinicalIRBApprovalBean();
		
		HashMap hmIRBApproval = gmClinicalIRBBean.loadIRBApproval(hmParam);
		
		return hmIRBApproval;
	}
	
	private String getGridData(ArrayList alIRBApproval) throws Exception{
		
		GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
		gmTemplateUtil.setDataList("alResult", alIRBApproval);
		gmTemplateUtil.setTemplateName("GmClinicalIRBAppr.vm");
		gmTemplateUtil.setTemplateSubDir("clinical/templates");
		String gridData = gmTemplateUtil.generateOutput();
		return gridData;
	}

}
