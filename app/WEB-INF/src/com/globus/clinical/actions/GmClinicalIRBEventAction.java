package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalIRBApprovalBean;
import com.globus.clinical.beans.GmClinicalIRBEventBean;
import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.forms.GmClinicalIRBEventForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalIRBEventAction extends GmAction{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception
	{
	
		HttpSession session =request.getSession(false);
		String strStudyID=(String)session.getAttribute("strSessStudyId");
		
		GmClinicalIRBEventForm gmClinicalIRBEventForm = (GmClinicalIRBEventForm) form;
		gmClinicalIRBEventForm.loadSessionParameters(request);
		gmClinicalIRBEventForm.setStudyID(strStudyID);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		String strReturnTo="gmclinicalirbevent";
		String strHidden="";
		String strButtonDisabled = "";
		String strAction = GmCommonClass.parseNull(gmClinicalIRBEventForm.getStrOpt());
		HashMap hmParamVal = new HashMap();
		hmParamVal = GmCommonClass.getHashMapFromForm(gmClinicalIRBEventForm);
		hmParamVal.put("SESSSTUDYID", strStudyID);
		//disabling the submit button based on the study id
		strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParamVal);
		
		if(strAction.equals("save_irb_event")){
			saveIRBEvent(gmClinicalIRBEventForm,request);
			strHidden =strAction ; 
			strAction = "load_irb_event";
		}
		if(strAction.equals("edit_irb_event")){
			HashMap hmParam = new HashMap();
			hmParam.put("IRBEVENTID",gmClinicalIRBEventForm.getIrbEventID());
			ArrayList alIRBEvent = fetchIRBEvent(hmParam);
			
			if(alIRBEvent!=null&& alIRBEvent.size()>0){
				HashMap hmIRBEvent = (HashMap)alIRBEvent.get(0);
				GmCommonClass.getFormFromHashMap(gmClinicalIRBEventForm, hmIRBEvent);
			}
			
			strAction = "load_irb_event";
		}
		if(strAction.equals("load_irb_event")){
			
			loadIRBEvent(gmClinicalIRBEventForm,request);
			
			HashMap hmParam = new HashMap();
			hmParam.put("IRBID",gmClinicalIRBEventForm.getIrbid() );
			String strGirdData = getGridData(fetchIRBEvent(hmParam));
			gmClinicalIRBEventForm.setGridData(strGirdData);
			gmClinicalIRBEventForm.setStrOpt(strAction);
		}
		if(!strHidden.equals("") && strHidden.equals("save_irb_event")){
			gmClinicalIRBEventForm.setIrbEventID("");
			gmClinicalIRBEventForm.setActionTaken("");
			gmClinicalIRBEventForm.setEventDate("");
			gmClinicalIRBEventForm.setReviewPeriod("");
			gmClinicalIRBEventForm.setReviewType("");
			gmClinicalIRBEventForm.setNotes("");
		}
		gmClinicalIRBEventForm.setLockedStudyDisable(strButtonDisabled);
	return mapping.findForward(strReturnTo);
	}
	
	public void saveIRBEvent(GmClinicalIRBEventForm gmClinicalIRBEventForm, HttpServletRequest request) throws Exception{
		
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalIRBEventForm);
		GmClinicalIRBEventBean gmClinicalIRBEventBean = new GmClinicalIRBEventBean();
		String strIrEventbid = gmClinicalIRBEventBean.saveIRBEvent(hmParam);
		
		gmClinicalIRBEventForm.setIrbEventID(strIrEventbid);
		log.debug(" # # # irbeventid"+strIrEventbid);
	}

	private String getGridData(ArrayList alIRBEvent) throws Exception{
		
		GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
		gmTemplateUtil.setDataList("alResult", alIRBEvent);
		gmTemplateUtil.setTemplateName("GmClinicalIRBEvent.vm");
		gmTemplateUtil.setTemplateSubDir("clinical/templates");
		String gridData = gmTemplateUtil.generateOutput();
		log.debug(" >>>>> gridData "+gridData);
		return gridData;
	}

	public ArrayList fetchIRBEvent(HashMap hmParam) throws Exception{
		GmClinicalIRBEventBean gmClinicalIRBEventBean = new GmClinicalIRBEventBean();
		ArrayList alIRBEvent = gmClinicalIRBEventBean.loadIRBEvent(hmParam);
		return alIRBEvent;
	}

	public void loadIRBEvent(GmClinicalIRBEventForm gmClinicalIRBEventForm, HttpServletRequest request) throws Exception{
		

		HashMap hmParam = new HashMap();
		hmParam.put("IRBID", gmClinicalIRBEventForm.getIrbid());
		
		GmClinicalIRBApprovalBean gmClinicalIRBBean = new GmClinicalIRBApprovalBean();
		
		HashMap hmIRBApproval = gmClinicalIRBBean.loadIRBApproval(hmParam);
		
		
		
		if(hmIRBApproval!=null){
			ArrayList alIRBApproval = (ArrayList)hmIRBApproval.get("IRBAPPROVAL");
			
			if(alIRBApproval!=null && alIRBApproval.size()>0){
				hmIRBApproval = (HashMap)alIRBApproval.get(0);
			}
			//log.debug(" hmIRBApproval ????? 0"+hmIRBApproval);
			gmClinicalIRBEventForm = (GmClinicalIRBEventForm)GmCommonClass.getFormFromHashMap(gmClinicalIRBEventForm, hmIRBApproval);			
			
			HashMap temp = GmCommonClass.getHashMapFromForm(gmClinicalIRBEventForm);
			//log.debug(" # # # # temp "+temp);
			
			String approvalTypeName=GmCommonClass.parseNull((String)hmIRBApproval.get("APPROVALTYPENAME"));
			
			String codeNm="IRBATK"; // IRB ACTION TAKEN
			if(!approvalTypeName.equals("") && approvalTypeName.equals("Advertisement")){
				codeNm="IRBATA";// IRB ACTION TAKEN ADVERTISEMENT
			}
			gmClinicalIRBEventForm.setAlActionTakenList(GmCommonClass.getCodeList(codeNm));
			
			gmClinicalIRBEventForm.setReviewTypeList(GmCommonClass.getCodeList("IRBRTY"));
			
			gmClinicalIRBEventForm.setReviewPeriodList(GmCommonClass.getCodeList("IRBRPD"));
		}
	}
	
}
