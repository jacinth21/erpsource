package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalSiteMonitorBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.clinical.forms.GmClinicalSiteMonitorForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalSiteMonitorAction extends GmClinicalDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
    HashMap hmReturn = new HashMap();
   
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    { 
		
		
		log.debug("Inside GmClinicalSiteMonitorAction");	
		String strForward = "";
		String strSiteId = "";
		String strStudyId = "";
		String status="";
		String strUserId = "";
        String strComments="";
        String strSiteMtrId="";
        String strButtonDisabled = "";
        
        HashMap hmReturn = new HashMap();
        HashMap hmParam = new HashMap();
        GmCommonBean gmCommonBean = new GmCommonBean();
		String strCondition = getAccessFilter(request, response);
		GmClinicalSiteMonitorForm gmClinicalSiteMonitorForm = (GmClinicalSiteMonitorForm)form;
		gmClinicalSiteMonitorForm.loadSessionParameters(request);
		HashMap hmParamN = GmCommonClass.getHashMapFromForm(gmClinicalSiteMonitorForm);
		
		log.debug("hmParamN:"+hmParamN);
		
		
		
        GmStudyBean gmStudyBean = new GmStudyBean();
        GmPatientBean gmPatientBean = new GmPatientBean();

    	String strOpt = gmClinicalSiteMonitorForm.getStrOpt();
		String strMtrId = gmClinicalSiteMonitorForm.getSiteMtrId();
    	GmClinicalBean gmClinicalBean = new GmClinicalBean();
		log.debug("**************************************strMtrId*********:"+hmParamN);
		
		 strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParamN);
		 
		//gmClinicalSiteMonitorForm.setAlStudyList(gmStudyBean.reportStudyNameList());
		//gmClinicalSiteMonitorForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmClinicalSiteMonitorForm.getStudyList(),"")); // To discuss about the static and access condition
		gmClinicalSiteMonitorForm.setAlCraNames(gmCommonBean.fetchGroupMappingDetails("CLINICAL_CRA_LIST"));
		gmClinicalSiteMonitorForm.setAlPurposeOfVisit(GmCommonClass.getCodeList("SMPOV",strCondition));
        //strStudyId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getStudyListId());
		//strSiteId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudySiteId()); 
		//strStudyId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudyId());
		log.debug("studysiteid issssssssssssssssssss:"+gmClinicalSiteMonitorForm.getSessStudySiteId());
		String strHAction = gmClinicalSiteMonitorForm.getHaction();
		
		log.debug("strHAction   " + strHAction);	
	    log.debug("Inside GmClinicalSiteMonitorAction");
            
            GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean = new GmClinicalSiteMonitorBean();
            strCondition = getAccessFilter(request, response);
            //Get study and site from the session.
           strSiteId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudySiteId()); 
           strStudyId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudyId());
           strUserId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getUserId());
           //String strSiteNames = GmCommonClass.createInputString(gmClinicalSiteMonitorForm.getCheckSiteName());
           hmReturn.put("STUDYID", strStudyId);
           hmReturn.put("SITEID", strSiteId);
           hmReturn.put("strOpt", strOpt);
           
            log.debug("strOpt:"+strOpt+" strSiteId:"+strSiteId+" strCondition:"+strCondition+" strStudyId:"+strStudyId);
            gmClinicalSiteMonitorForm.setAlOwnerOfVisit(GmCommonClass.parseNullArrayList(gmCommonBean.fetchGroupMappingDetails("OWNER_VISIT_LIST")));
        	gmClinicalSiteMonitorForm.setAlOtherMonitor(GmCommonClass.parseNullArrayList(gmCommonBean.fetchGroupMappingDetails("SITE_MONITOR_LIST")));
        	
        	gmClinicalSiteMonitorForm.setAlSiteMonitorStatus(GmCommonClass.getCodeList("SITEMS",strCondition));
        	
            if(strOpt.equals("Load_Monitor")){
            	
            	gmClinicalSiteMonitorForm.setOwnerOfVisit(strUserId);
            	strForward= "gmClinicalSiteMonitor";
            	
            }else if (strOpt.equals("monitor_report")){
            	ArrayList alStatus = gmClinicalSiteMonitorForm.getAlSiteMonitorStatus();
            	HashMap hmCStatus = new HashMap();
            	hmCStatus.put("CODENMALT", "");
            	hmCStatus.put("CODEID", "60464");
            	hmCStatus.put("CONTROLTYP", "");
            	hmCStatus.put("CODENM", "Cancelled");
            	alStatus.add(hmCStatus);
            	gmClinicalSiteMonitorForm.setAlSiteMonitorStatus(alStatus);
            	log.debug("strCodeId:"+alStatus);
				if (strHAction.equalsIgnoreCase("loadReport")){
					strForward = fetchSiteMonitorReport (mapping, form, request, response);
				}else{
					strForward = "gmClinicalSiteMonitorReport";
				}
            } else if (strOpt.equals("search_observation")){
				gmClinicalSiteMonitorForm.setAlStatus(GmCommonClass.getCodeList("OBVSTS",strCondition));
				strForward = fetchSiteMtrObservation  (mapping, form, request, response);				
            }
		else if(strOpt.equals("Save")){
            	strSiteMtrId = saveSiteMonitor(mapping, form, request, response);
            	hmReturn.put("strSiteMtrId", strSiteMtrId);
            	hmReturn.put("STATUS", GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSiteMonitorStatus()));
            	strForward= "gmClinicalSiteMonitor";
            }
		else if(strOpt.equals("Edit_Monitor")){
			//GmClinicalSiteMonitorForm gmClinicalSiteMonitorForm = (GmClinicalSiteMonitorForm) form;
				hmParam = GmCommonClass.getHashMapFromForm(gmClinicalSiteMonitorForm);
            	hmParam = GmCommonClass.parseNullHashMap(gmClinicalSiteMonitorBean.fetchSiteMonitor(strMtrId));
            	//String strNotesForVisit = GmCommonClass.parseNull((String)hmParam.get("NOTESOFVISIT"));
            	gmClinicalSiteMonitorForm = (GmClinicalSiteMonitorForm) GmCommonClass.getFormFromHashMap(gmClinicalSiteMonitorForm, hmParam);
            	String strOtherMonitor[] = (String[])hmParam.get("otherMonitor");
            	gmClinicalSiteMonitorForm.setOtherMonitor(strOtherMonitor);
            	
            	status = gmClinicalSiteMonitorForm.getSiteMonitorStatus();
            	gmClinicalSiteMonitorForm.setSiteMtrId(strMtrId);
            	//hmReturn.put("COMMENTS", strNotesForVisit);
            	hmReturn.put("strSiteMtrId", strMtrId);
            	hmReturn.put("STATUS",status);
            	strForward= "gmClinicalSiteMonitor";
            }
            gmClinicalSiteMonitorForm.setLockedStudyDisable(strButtonDisabled);
		
            request.setAttribute("hmReturn", hmReturn);
        return mapping.findForward(strForward);
    }
   
    public String saveSiteMonitor(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {        
    	HashMap hmParam = new HashMap();
    	GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean = new GmClinicalSiteMonitorBean();
    	GmClinicalSiteMonitorForm gmClinicalSiteMonitorForm = (GmClinicalSiteMonitorForm) form;
    	
    	HashMap hmParamNew = GmCommonClass.getHashMapFromForm(gmClinicalSiteMonitorForm);
    	
    	log.debug("hmParamNew:"+hmParamNew);
    	
    	String strSiteId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudySiteId()); 
        strStudyId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudyId());
        String strUserId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getUserId());
        
        
        
        log.debug("1:"+GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getMonitorString()));
        log.debug("2:"+GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getDateVisited()));
        log.debug("3:"+GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getOwnerOfVisit()));
        log.debug("4:"+GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getPurposeOfVisit()));
        log.debug("5:"+GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSiteMonitorStatus()));
        log.debug("6:"+GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getNotesOfVisit()));
        log.debug("7:"+GmCommonClass.parseNull(request.getParameter("ID")));
        
    	String strOtherMonitor = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getMonitorString());
    	String strDateVisited = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getDateVisited());
    	String strOwnerOfVisit = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getOwnerOfVisit());
    	String strPurposeOfVisit = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getPurposeOfVisit());
    	String strSiteMonitorStatus = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSiteMonitorStatus());
    	String strNotesForVisit = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getNotesOfVisit());
    	String strIds = request.getParameter("ID");
    	
    	String strMtrId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSiteMtrId());
    	hmParam.put("STUDYID", strStudyId);
    	hmParam.put("SITEID", strSiteId);
    	hmParam.put("MONITORLIST", strOtherMonitor);
    	hmParam.put("VISITDT", strDateVisited);
    	hmParam.put("MONITOROWNER", strOwnerOfVisit);
    	hmParam.put("PURPOSEOFVISIT", strPurposeOfVisit);
    	hmParam.put("STATUS", (strSiteMonitorStatus.equals("0")?"60461":strSiteMonitorStatus));
    	hmParam.put("NOTESOFVISIT", strNotesForVisit);
    	hmParam.put("USERID", strUserId);
    	hmParam.put("STMTRID", strIds);
    	
    		
    	String strSiteMtrId=gmClinicalSiteMonitorBean.saveSiteMonitor(hmParam);
    	log.debug("strMtrId:"+strSiteMtrId);
       	
    	HashMap hmSmDtls = fetchSiteMonitor(strSiteMtrId,gmClinicalSiteMonitorBean,gmClinicalSiteMonitorForm);
    	ArrayList alSiteMtr = (ArrayList)hmSmDtls.get("alSiteMtr");
    	ArrayList alSiteMtrMap = (ArrayList)hmSmDtls.get("alSiteMtrMap");
    	request.setAttribute("alSiteMtr", alSiteMtr);
    	request.setAttribute("alSiteMtrMap", alSiteMtrMap);
    	
    	
    	gmClinicalSiteMonitorForm.setNotesOfVisit(strNotesForVisit);
    	//request.setAttribute("strSiteMtrId", strSiteMtrId);
            
        return strSiteMtrId;
    }
    
    public HashMap fetchSiteMonitor(String strSiteMtrId,GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean,GmClinicalSiteMonitorForm gmClinicalSiteMonitorForm) throws AppError{
    	HashMap hmParam = new HashMap();
    	//GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean = new GmClinicalSiteMonitorBean();
    	
    	hmParam = GmCommonClass.parseNullHashMap(gmClinicalSiteMonitorBean.fetchSiteMonitor(strSiteMtrId));
    	//gmClinicalSiteMonitorForm = (GmClinicalSiteMonitorForm) GmCommonClass.getFormFromHashMap(gmClinicalSiteMonitorForm, hmParam);
    	return hmParam;
    }

	public String fetchSiteMonitorReport (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
    	log.debug("Inside fetchSiteMonitorReport");	
		HashMap hmParam = new HashMap();  
 	    String strXmlGridData = "";
		String strCondition = getAccessFilter(request, response);
		
		GmClinicalSiteMonitorForm gmClinicalSiteMonitorForm = (GmClinicalSiteMonitorForm) form;
        gmClinicalSiteMonitorForm.loadSessionParameters(request);
        
		hmParam = GmCommonClass.getHashMapFromForm(gmClinicalSiteMonitorForm);
    	
		GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean = new GmClinicalSiteMonitorBean();
		HashMap hmReturn = gmClinicalSiteMonitorBean.loadSiteMonitorReport(hmParam, strCondition);
		ArrayList alResult = (ArrayList)hmReturn.get("alSiteMtr"); 
		
        HashMap hmParamV = new HashMap();
        hmParamV.put("TEMPLATE","GmClinicalMonitorReport.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);
        strXmlGridData = gmClinicalSiteMonitorBean.getXmlGridData(hmParamV);
        gmClinicalSiteMonitorForm.setGridXmlData(strXmlGridData);
        
		String strForward = "gmClinicalSiteMonitorReport";
        return strForward;
    }
    
    public String fetchSiteMtrObservation  (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
    	log.debug("Inside fetchSiteMtrObservation");	
		HashMap hmParam = new HashMap();
		String strSiteId = "";
		String strStudyId = "";
		String strUserId = "";
 	    String strXmlGridData = "";
 	    String strStudySiteId = "";
		String strCondition = getAccessFilter(request, response);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		GmClinicalSiteMonitorForm gmClinicalSiteMonitorForm = (GmClinicalSiteMonitorForm) form;
        strSiteId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudySiteId()); 
        strStudyId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudyId());
        strUserId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getUserId());
        gmClinicalSiteMonitorForm.setStudyList(strStudyId);
        gmClinicalSiteMonitorForm.setSiteList(strSiteId);
        gmClinicalSiteMonitorForm.setUserId(strUserId);
		hmParam = GmCommonClass.getHashMapFromForm(gmClinicalSiteMonitorForm);
		strStudySiteId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudySiteId()); 
		strSiteId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessSiteId());
		strStudyId = GmCommonClass.parseNull(gmClinicalSiteMonitorForm.getSessStudyId());
		hmParam.put("STUDY_ID", strStudyId);
		hmParam.put("SITE_ID", strSiteId);
		hmParam.put("STUDYSITEID", strStudySiteId);
		GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean = new GmClinicalSiteMonitorBean();
		gmClinicalSiteMonitorForm.setAlPatientList(GmCommonClass.parseNullArrayList(gmClinicalBean.loadPatientList(hmParam)));
		ArrayList alResult = gmClinicalSiteMonitorBean.loadSiteMtrObservation(hmParam, strCondition);
		
        HashMap hmParamV = new HashMap();
        hmParamV.put("TEMPLATE","GmClinicalObservationReport.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);
        strXmlGridData = gmClinicalSiteMonitorBean.getXmlGridData(hmParamV);
        gmClinicalSiteMonitorForm.setGridXmlData(strXmlGridData);
        
		String strForward = "gmClinicalObservationReport";
        return strForward;
    }

    
    
}
