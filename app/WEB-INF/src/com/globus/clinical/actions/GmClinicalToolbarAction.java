package com.globus.clinical.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalNavigationBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalToolbarAction extends GmAction {

  // Code to Initialize the Logger Class.
  Logger log = GmLogger.getInstance(this.getClass().getName());
  String strCondition = "";

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=UTF-8");
    response.setCharacterEncoding("UTF-8");

    String strXmlData = "";
    strCondition = getAccessFilter(request, response);
    GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm) form;
    gmStudyFilterForm.loadSessionParameters(request);
    GmClinicalNavigationBean gmClinicalNavigationBean = new GmClinicalNavigationBean();
    HashMap hmParamV = new HashMap();
    // HashMap hmSesnParam = new HashMap();
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
    hmParam.put("CONDITION", strCondition);
    hmParamV = gmClinicalNavigationBean.loadContextMenu(hmParam);
    hmParamV.put("TEMPLATE", "GmClinicalToolbar.vm");
    hmParamV.put("TEMPLATEPATH", "clinical/templates");
    strXmlData = gmClinicalNavigationBean.getXmlToolbarData(hmParamV);


    log.debug("strXmlData=============" + strXmlData);
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(strXmlData);
    pw.flush();
    return null;
  }
}
