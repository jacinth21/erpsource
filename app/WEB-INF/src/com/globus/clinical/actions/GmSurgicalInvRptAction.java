package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmSurgicalInvRptAction extends GmClinicalDispatchAction{
	 Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
	 
	    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
	    { 
			String strSiteId = "";
			String strStudyId = "";
			String strAccLvl="";
			HashMap hmParam =new HashMap();
			ArrayList alReturn = new ArrayList();
		    GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
		    gmStudyFilterForm.loadSessionParameters(request);
		    GmPatientBean gmPatientBean= new GmPatientBean();
		    //Get study and site from the session.
			strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
			strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
			strAccLvl = GmCommonClass.parseNull(gmStudyFilterForm.getSessAccLvl());
			hmParam.put("STUDYID", strStudyId);
			hmParam.put("SITEID", strSiteId);
			hmParam.put("VERIFYFL", "Y");
			alReturn = gmPatientBean.fetchSurgicalInvRpt(hmParam);
			hmParam.put("RLIST", alReturn);
            hmParam.put("TEMPLATE", "GmSurgicalInvRpt.vm");
            hmParam.put("TEMPLATEPATH", "clinical/templates");
            String strXmlData = gmPatientBean.getXmlGridData(hmParam);
            gmStudyFilterForm.setGridXmlData(strXmlData);

			return mapping.findForward("gmSurgicalInv");
	    } 
}
