package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalVoidReportAction extends GmClinicalDispatchAction
{
	 
        public ActionForward fetchVoidedForms(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
        {
    		String strStudyId = "";
    		String strSiteId = null;
    	    String strPatientId = "";
    	    String periodId = "";
     	    String strXmlGridData = "";
        	HashMap hmTemp = new HashMap();
        	HashMap hmParam = new HashMap();
        	
        	GmClinicalBean gmClinic = new GmClinicalBean();
        	GmPatientBean gmPatientBean = new GmPatientBean();
        	GmAccessControlBean gmAccessControlBean  = new GmAccessControlBean();
        	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm) form;
            gmStudyFilterForm.loadSessionParameters(request);	
    		String strCondition= getAccessFilter(request, response);
    		
    		strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
            strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
            strPatientId = GmCommonClass.parseNull(gmStudyFilterForm.getSessPatientPkey());;
            String strOpt = gmStudyFilterForm.getStrOpt();
            
            gmStudyFilterForm.setStudyListId(strStudyId);
            gmStudyFilterForm.setSiteList(strSiteId);
            
            if(!strPatientId.equals("")){
            	gmStudyFilterForm.setPatientId(strPatientId);
            }
            periodId = GmCommonClass.parseNull(gmStudyFilterForm.getPeriodId());
            strPatientId = GmCommonClass.parseNull(gmStudyFilterForm.getPatientId());

    		hmParam.put("STUDY_ID", strStudyId);
    		hmParam.put("SITE_ID", strSiteId);
    		hmParam.put("ACCESS_CONDITION", strCondition);

    		hmTemp = gmClinic.loadFormDataEntryLists(hmParam);
    		gmStudyFilterForm.setAlStudyList(gmClinic.loadStudyList(strCondition));
    		if(strStudyId!=null && !strStudyId.equals("")){
    			gmStudyFilterForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmStudyFilterForm.getStudyListId(),""));
    			gmStudyFilterForm.setAlPeriodList((ArrayList)hmTemp.get("PERIODLIST"));
    			gmStudyFilterForm.setAlPatientList((ArrayList)hmTemp.get("PATIENTLIST"));
    		}
    		if(periodId!=null && !periodId.equals("0") && !periodId.equals("")){
    			gmStudyFilterForm.setAlStudyFormList(gmClinic.loadFormListForPeriod(strStudyId, periodId));
    		}else{
    			gmStudyFilterForm.setAlStudyFormList(GmClinicalBean.loadStudyFormList(strStudyId));
    		}
    		gmStudyFilterForm.setAlReasonForClosing((ArrayList)GmCommonClass.getCodeList("VDFORM",strCondition));
    		hmParam.put("GROUPNAME","2007_VD_FRM_ACCESS");
    		
    		gmStudyFilterForm.setAlCraNames(gmAccessControlBean.fetchGroupMappingDetails(hmParam));
    		
    		if(strOpt.equalsIgnoreCase("loadReport")){
    			hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
    			ArrayList alResult = (ArrayList)gmClinic.fetchVoidedReportFroms(hmParam);
	    		HashMap hmParamV = new HashMap();
		        hmParam.put("TEMPLATE","GmClinicalVoidReport.vm");
		        hmParam.put("TEMPLATEPATH","clinical/templates");
		        hmParam.put("ALRESULT",alResult);
		        strXmlGridData = getXmlGridData(hmParam);
				gmStudyFilterForm.setGridXmlData(strXmlGridData);
    		}
            return mapping.findForward("gmClinicalVoidReport");
        }
        
        public String getXmlGridData(HashMap hmParamV) throws AppError {
    		GmTemplateUtil templateUtil = new GmTemplateUtil();
    		ArrayList alParam = (ArrayList)hmParamV.get("ALRESULT");
    		String strTemplate = (String)hmParamV.get("TEMPLATE");
    		String strTemplatePath = (String)hmParamV.get("TEMPLATEPATH");
    		templateUtil.setDataList("alResult", alParam);
    		templateUtil.setTemplateName(strTemplate);
    		templateUtil.setTemplateSubDir(strTemplatePath);
    		return templateUtil.generateOutput();
    	}
        
}
