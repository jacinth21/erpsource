package com.globus.clinical.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.clinical.forms.GmClinicalSiteMapForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalSiteMapAction extends GmAction
{

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
        log.debug("Enter");

        String strCondition = getAccessFilter(request, response);
    	String strStudyId = "";
        String strXmlGridData = "";
        String strDhtmlx = "";
        String strForward = "";
        String strButtonDisabled = "";
        GmClinicalSiteMapForm gmClinicalSiteMapForm = (GmClinicalSiteMapForm) form;
        gmClinicalSiteMapForm.loadSessionParameters(request);

        GmStudyBean gmStudyBean = new GmStudyBean();
        GmPatientBean gmPatientBean = new GmPatientBean();
        GmCommonBean gmCommonBean = new GmCommonBean();
        GmClinicalBean gmClinicalBean = new GmClinicalBean();

        HashMap hmParam = new HashMap();
        HashMap hmReturn = new HashMap();
        String strOpt = gmClinicalSiteMapForm.getStrOpt();
        hmParam = GmCommonClass.getHashMapFromForm(gmClinicalSiteMapForm);
        //strDhtmlx = GmCommonClass.parseNull((String)request.getParameter("strDhtmlx"));
        strDhtmlx = gmClinicalSiteMapForm.getStrDhtmlx();
        strButtonDisabled = gmClinicalBean.getFormEditAccess(hmParam);
          if (strOpt.equals("save"))
        {
            hmReturn = gmStudyBean.saveClinicalSiteMap(hmParam);
            gmClinicalSiteMapForm.setHsiteMapID((String) hmReturn.get("STUDYSITEID"));
            strOpt ="edit";
        } if (strOpt.equals("edit"))
        {
            hmReturn = gmPatientBean.loadSiteMapInfo(gmClinicalSiteMapForm.getHsiteMapID(), strCondition);
            GmCommonClass.getFormFromHashMap(gmClinicalSiteMapForm, hmReturn);
            log.debug("hmReturn " + hmReturn);
            log.debug("gmClinicalSiteMapForm " + gmClinicalSiteMapForm.getSiteNameList());
        }
        //request.setAttribute("SITEMAP", gmPatientBean.loadSiteMapDetails(gmClinicalSiteMapForm.getStudyList()));
        gmClinicalSiteMapForm.setAlStudyList(gmStudyBean.reportStudyNameList());
        
        String strCompanyIds = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue("CMPYID", "SITELIST"));
        gmClinicalSiteMapForm.setAlSiteNameList(gmClinicalBean.getSiteList(strCompanyIds)); // To get both US and OUS accounts
        gmClinicalSiteMapForm.setAlCraNames(gmCommonBean.fetchGroupMappingDetails("CLINICAL_CRA_LIST"));
        gmClinicalSiteMapForm.setAlIRBType(GmCommonClass.getCodeList("IRBTYP"));
        strStudyId = GmCommonClass.parseNull(gmClinicalSiteMapForm.getStudyList());
        
        //DHTMLX Changes
        strStudyId = strStudyId.equals("")?gmClinicalSiteMapForm.getSessStudyId():strStudyId;
        ArrayList alResult = gmPatientBean.loadSiteMapDetails(strStudyId, strCondition);
        
        HashMap hmParamV = new HashMap();
        hmParamV.put("STUDYLIST",strStudyId);
        hmParamV.put("TEMPLATE","GmClinicalSiteMap.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);
        hmParamV.put("DHTMLX",strDhtmlx);
        strXmlGridData = gmPatientBean.getXmlGridData(hmParamV);
        gmClinicalSiteMapForm.setGridXmlData(strXmlGridData);
        strForward =strDhtmlx.equals("")?"gmClinicalSiteMap":"gmClinicalSiteList";
        log.debug("Exit===SITEMAP======"+strForward);
        //END
		
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        String currentDate = dateFormat.format(cal.getTime());
			
        gmClinicalSiteMapForm.setCurrentDate(currentDate);
        gmClinicalSiteMapForm.setLockedStudyDisable(strButtonDisabled);
        return mapping.findForward(strForward);
    }
}
