package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalSnapshotBean;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalNavigationBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalNavigationAction extends GmAction
{

	// Code to Initialize the Logger Class.
    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strCondition = "";
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    		{   
		    	String strXmlSnapshotData ="";
		    	String strXmlReportData ="";
		    	String strXmlTransData ="";
		    	
		    	String strSessPgToLoad = "";
				String strSessStrOpt = "";
				String strSessLastClicked = "";
				String strSessOptLastClicked = "";
				
				String strStudyId = "";
		    	ArrayList alReportTree = new ArrayList();
		    	ArrayList alTransTree = new ArrayList();
		    	HashMap hmParam = new HashMap();
		    	//HashMap hmSnapshot = new HashMap();
		    	HashMap hmReport = new HashMap();
		    	HashMap hmTrans = new HashMap();
		    	HashMap hmTransRpt = new HashMap();
		    	
		    	HashMap hmActionParam = new HashMap();
		    	
		    	HttpSession session = request.getSession(false);                                             
	            request.setCharacterEncoding("UTF-8");
	    		response.setContentType("text/html; charset=UTF-8");
	    		response.setCharacterEncoding("UTF-8");
		    	strCondition = getAccessFilter(request, response);
		    	GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
		    	gmStudyFilterForm.loadSessionParameters(request);
		    	//GmClinicalBean gmClinicalBean = new GmClinicalBean();
		    	GmClinicalNavigationBean gmClinicalNavigationBean = new GmClinicalNavigationBean();
		    	GmClinicalSnapshotBean gmClinicalSnapshotBean = new GmClinicalSnapshotBean();
		    	hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
		    	hmParam.put("CONDITION", strCondition);
		    	
		    	//log.debug("hmParam navigation=================="+hmParam);
		    	//alSnapshot = gmClinicalSnapshotBean.loadTreeList_old(hmParam);
		    	
		    	hmTransRpt = gmClinicalNavigationBean.loadTranRptTreeList(hmParam);
		    	
		    	alReportTree = GmCommonClass.parseNullArrayList((ArrayList)hmTransRpt.get("ALREPORTTREE"));
		    	hmReport.put("TEMPLATE","GmClinicalReportTree.vm");
		    	hmReport.put("TEMPLATEPATH","clinical/templates");
		    	hmReport.put("RLIST",alReportTree);
		    	strXmlReportData = gmClinicalSnapshotBean.getXmlTreeData(hmReport);
		    	//log.debug("strXmlReportData======Navigation======="+strXmlReportData);
		    	gmStudyFilterForm.setTreeReportXmlData(strXmlReportData);
		    	
		    	alTransTree = GmCommonClass.parseNullArrayList((ArrayList)hmTransRpt.get("ALTRANSTREE"));
		    	hmTrans.put("TEMPLATE","GmClinicalTransTree.vm");
		    	hmTrans.put("TEMPLATEPATH","clinical/templates");
		    	hmTrans.put("RLIST",alTransTree);
		    	strXmlTransData = gmClinicalSnapshotBean.getXmlTreeData(hmTrans);
		    	//log.debug("strXmlTransData======Navigation======="+strXmlTransData);
		    	gmStudyFilterForm.setTreeTransXmlData(strXmlTransData);
		    	
		    	
		    	hmActionParam = gmClinicalNavigationBean.fetchLastClickedAction(hmTransRpt);
		    	strSessPgToLoad = GmCommonClass.parseNull((String)hmActionParam.get("SESSPGTOLOAD"));
				strSessStrOpt = GmCommonClass.parseNull((String)hmActionParam.get("SESSOPT"));
				strSessLastClicked = GmCommonClass.parseNull((String)hmActionParam.get("SESSLASTCLICKED"));
				strSessOptLastClicked = GmCommonClass.parseNull((String)hmActionParam.get("SESSOPTLASTCLICKED"));
				if(strSessLastClicked.equals(""))
				{
					session.setAttribute("strSessLastClicked", "");
					session.setAttribute("strSessOptLastClicked", "");
					session.setAttribute("strSessNodeId", "");	
					session.setAttribute("strSessAccrd", "");	
				}
				else
				{
					session.setAttribute("strSessPgToLoad", strSessLastClicked);
					session.setAttribute("strSessStrOpt", strSessOptLastClicked);	
					gmStudyFilterForm.setSessPgToLoad(strSessLastClicked);
					gmStudyFilterForm.setSessOpt(strSessOptLastClicked);
				}
				
				strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
				if(!strStudyId.equals("") )
				{
					strXmlSnapshotData = gmClinicalSnapshotBean.loadContextTreeXml(hmParam);
			    	/*
			    	hmSnapshot.put("TEMPLATE","GmClinicalSnapshotStudyTree.vm");
			    	hmSnapshot.put("TEMPLATEPATH","clinical/templates");
			    	hmSnapshot.put("RLIST",alSnapshot);
			    	strXmlSnapshotData = gmClinicalSnapshotBean.getXmlTreeData(hmSnapshot);
			    	*/
			    	//log.debug("strXmlSnapshotData======Navigation======="+strXmlSnapshotData);
			    	
			    	gmStudyFilterForm.setTreeSnapshotXmlData(strXmlSnapshotData);
				}
				
		    	return mapping.findForward("gmClinicalNavigation");
    		}  
}
