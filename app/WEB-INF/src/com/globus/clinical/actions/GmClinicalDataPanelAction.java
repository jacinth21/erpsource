package com.globus.clinical.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.servlets.GmServlet;

import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalDataPanelAction extends GmAction
{

	// Code to Initialize the Logger Class.
    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strCondition = "";
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    		{   
    			GmServlet gmServlet = new GmServlet();
    			GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
    			gmStudyFilterForm.loadSessionParameters(request);
    			HttpSession session = request.getSession(false);
    			HashMap hmParam = new HashMap();
    			hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
    			String strSessPgToLoad = "";
				String strSessStrOpt = "";
				String strMethod = "";
				String strDhtmlx = "";
				String strForward = "";
				//log.debug("hmParam=============data panel============"+hmParam);
				strSessPgToLoad = gmStudyFilterForm.getSessPgToLoad();
				strSessStrOpt = gmStudyFilterForm.getSessOpt();
				strDhtmlx = GmCommonClass.parseNull((String)request.getParameter("strDhtmlx")); 
				strMethod = GmCommonClass.parseNull((String)request.getParameter("method"));
				strForward = strSessPgToLoad+"&strDhtmlx="+strDhtmlx+"&method="+strMethod+"&strOpt="+strSessStrOpt;
				//log.debug("strForward=============data panel============"+strForward);
				gmServlet.gotoStrutsPage(strForward,request,response);
		    	return null;
    		}  
}
