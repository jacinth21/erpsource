/**
 * 
 */
package com.globus.clinical.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmCrtlEndPointRptBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author rshah
 *
 */
public class GmCrtlEndPointRptAction extends GmClinicalDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
    HashMap hmReturn = new HashMap();   
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    { 
		String strSiteId = "";
		String strStudySiteId = "";
		String strStudyId = "";
		String strAccLvl="";
	    HashMap hmReturn = new HashMap();
	    HashMap hmParam = new HashMap(); 
	    HashMap hmValues = new HashMap();
	    
	    Logger log = GmLogger.getInstance(this.getClass().getName());
	    String strCondition = getAccessFilter(request, response);
	    
	    GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm)form;
	    gmStudyFilterForm.loadSessionParameters(request);
	    
	    GmCrtlEndPointRptBean gmCrtlEndPointRptBean = new GmCrtlEndPointRptBean();
	    GmPatientBean gmPatientBean = new GmPatientBean();
	    
	    String strOpt = gmStudyFilterForm.getStrOpt();
	    log.debug("Inside gmCrtlEndPointRptForm "+strOpt);
		
	    //gmCrtlEndPointRptForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmCrtlEndPointRptForm.getSessStudyId(), "")); // To
	    
	    //Get study and site from the session.
		strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
		strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
		strAccLvl = GmCommonClass.parseNull(gmStudyFilterForm.getSessAccLvl());
		
		strStudySiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudySiteId());
		strStudySiteId = strStudySiteId.equals("") ? gmStudyFilterForm.getSiteList():strStudySiteId;
		gmStudyFilterForm.setSiteList(strStudySiteId);
		
		hmParam.put("STUDYID", strStudyId);
		hmParam.put("SITEID", strSiteId);
		hmParam.put("SITELIST", strStudySiteId);
		hmParam.put("CONDITION", strCondition);
		
		log.debug("strStudyId:"+strStudyId);
		log.debug("strSiteId:"+strSiteId);
		log.debug("strStudySiteId:"+strStudySiteId);
		log.debug("strOpt:"+strOpt);
		log.debug("strCondition:"+strCondition);
		
		hmReturn = gmCrtlEndPointRptBean.loadCrtlEndPointDetail(hmParam);		
		gmStudyFilterForm.setHmCrossTabReport(hmReturn);
		
		return mapping.findForward("gmCrtlEndPointRpt");
    } 
        
    
}