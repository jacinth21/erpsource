package com.globus.clinical.actions;


import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalSiteMtrObservationBean;
import com.globus.clinical.forms.GmClinicalObservationForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalObservationAction extends GmAction
{

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   	
        
        GmClinicalObservationForm gmClinicalObservationForm = (GmClinicalObservationForm) form;
        gmClinicalObservationForm.loadSessionParameters(request);
        GmClinicalSiteMtrObservationBean gmClinicalSiteMtrObservationBean = new GmClinicalSiteMtrObservationBean();       
        String strAction = GmCommonClass.parseNull(request.getParameter("haction"));
        log.debug("strAction :"+strAction);
        gmClinicalObservationForm.setMsg("");    	        
        
        if(strAction.equals("load_observation")){
        	log.debug("Load Observation");    
        	loadObservation( gmClinicalObservationForm, gmClinicalSiteMtrObservationBean);
        }
        
        else if(strAction.equals("save_observation")){
        	log.debug("Save Observation");
        	String strUserId = (String)request.getSession().getAttribute("strSessUserId");
        	saveObservation( gmClinicalObservationForm, gmClinicalSiteMtrObservationBean, strUserId);
        	loadObservation(gmClinicalObservationForm,gmClinicalSiteMtrObservationBean);
        }
        else if(strAction.equals("add_more")){
        	log.debug("Add More Observation");    
        	addMoreObservation( gmClinicalObservationForm);
        	loadObservation(gmClinicalObservationForm, gmClinicalSiteMtrObservationBean);
        }
        else if(strAction.equals("load_next_issue")){
        	log.debug("Next issue");    
        	loadNextIssue( gmClinicalObservationForm,gmClinicalSiteMtrObservationBean);
        	loadObservation(gmClinicalObservationForm, gmClinicalSiteMtrObservationBean);
        }
        else if(strAction.equals("edit_observation")){
        	log.debug("Edit issue");       	
        	loadObservation(gmClinicalObservationForm, gmClinicalSiteMtrObservationBean);
        	loadEditIssue( gmClinicalObservationForm,gmClinicalSiteMtrObservationBean);
        }       
       
        return mapping.findForward("load");
    }   
    public void loadEditIssue(GmClinicalObservationForm gmClinicalObservationForm, GmClinicalSiteMtrObservationBean gmClinicalSiteMtrObservationBean) throws AppError{
    	
    	  log.debug("Inside Edit Issue");
    	  String  strObservationId = gmClinicalObservationForm.getObservationId();
	      HashMap hmParam = new HashMap();	      
    	  hmParam.put("OBSERVATIONID", strObservationId);
	      ArrayList alSiteMtrObservationList = gmClinicalSiteMtrObservationBean.fetchSiteMtrObservation(hmParam);
	      log.debug("Inside Edit Issue alSiteMtrObservationList: "+alSiteMtrObservationList);
	      if(alSiteMtrObservationList.size()>0){
	    		 HashMap hmReturn = (HashMap)alSiteMtrObservationList.get(0);
	    		 String strPatientId = GmCommonClass.parseNull((String)hmReturn.get("PATIENT_ID"));
	        	 String strPeriodId = GmCommonClass.parseNull((String)hmReturn.get("STUDY_PERIOD"));
	        	 String strIssueDesc = GmCommonClass.parseNull((String)hmReturn.get("ISSUE_DESC"));
	        	 String strActionRequired = GmCommonClass.parseNull((String)hmReturn.get("ACTION"));
	        	 String strResponsibleBy = GmCommonClass.parseNull((String)hmReturn.get("RES_BY"));
	        	 String strTargetResDate = GmCommonClass.parseNull((String)hmReturn.get("TGT_RES_DT"));
	        	 String strResDesc = GmCommonClass.parseNull((String)hmReturn.get("RES_DESC"));
	        	 String strActualResDate = GmCommonClass.parseNull((String)hmReturn.get("ACTUAL_RES_DT"));
	        	 String strStatusId = GmCommonClass.parseNull((String)hmReturn.get("STATUS_ID"));
	        	 String strStatusName = GmCommonClass.parseNull((String)hmReturn.get("STATUS"));
	        	 
	        	 
	        	 gmClinicalObservationForm.setPatientId(strPatientId);
	        	 gmClinicalObservationForm.setPeriodId(strPeriodId);
	        	 gmClinicalObservationForm.setIssueDescription(strIssueDesc);
	        	 gmClinicalObservationForm.setActionRequired(strActionRequired);
	        	 gmClinicalObservationForm.setResponisbleBy(strResponsibleBy);
	        	 gmClinicalObservationForm.setTargetResDate(strTargetResDate);
	        	 gmClinicalObservationForm.setResolution(strResDesc);
	        	 gmClinicalObservationForm.setActualResDate(strActualResDate);
	        	 gmClinicalObservationForm.setStatus(strStatusId);
	        	 gmClinicalObservationForm.setStatusName(strStatusName);
	        	 gmClinicalObservationForm.setObservationId(strObservationId);
	      }
    }
    
    public void loadObservation(GmClinicalObservationForm gmClinicalObservationForm, GmClinicalSiteMtrObservationBean gmClinicalSiteMtrObservationBean) throws AppError{
    	
    	HashMap hmParam = new HashMap();
    	GmClinicalBean gmClinicalBean = new GmClinicalBean();    	 
         
         String strSiteMonitorId = gmClinicalObservationForm.getHsiteMonitorId();        
         String strStudySiteId = gmClinicalObservationForm.getSessStudySiteId();     	
     	 String strStudyId = gmClinicalObservationForm.getSessStudyId();
         String strPurpose = gmClinicalObservationForm.getHpurpose();
         String strStatus = gmClinicalObservationForm.getHstatus();
         String strFromDate = gmClinicalObservationForm.getHfromDate();
         String strToDate = gmClinicalObservationForm.getHtoDate();
         String strCraId = gmClinicalObservationForm.getHcraId();
         
		  hmParam.put("SITEMONITORID",strSiteMonitorId);
	      
		  hmParam.put("STUDYID",strStudyId);
	      hmParam.put("STUDYSITEID",strStudySiteId);
	      hmParam.put("PURPOSE",strPurpose);
	      hmParam.put("STATUS",strStatus);
	      hmParam.put("FROMDATE",strFromDate);
	      hmParam.put("TODATE",strToDate);
	      hmParam.put("CRAID",strCraId);   
    	
    	 ArrayList alSiteMtrList = gmClinicalSiteMtrObservationBean.fetchSiteMtr(hmParam);  
    	 
    	 if(alSiteMtrList.size()>0){
    		 HashMap hmReturn = (HashMap)alSiteMtrList.get(0);
    		 //GmCommonClass.getFormFromHashMap(gmClinicalObservationForm, hmReturn);
    	
    		 String strVisit = GmCommonClass.parseNull((String)hmReturn.get("VISIT_DT"));
    		 gmClinicalObservationForm.setVisitDate(GmCommonClass.parseNull(strVisit));    		 
    		 gmClinicalObservationForm.setPurpose(GmCommonClass.parseNull((String)hmReturn.get("PURPOSE")));
    		 gmClinicalObservationForm.setSurgeonName(GmCommonClass.parseNull((String)hmReturn.get("PARTY_NAME")));
    		 gmClinicalObservationForm.setSiteName(GmCommonClass.parseNull((String)hmReturn.get("ACC_NAME")));
    		 gmClinicalObservationForm.setStudyName(GmCommonClass.parseNull((String)hmReturn.get("STUDY_NAME")));
    	}
    	 
    	
    	String strSiteId="";
    	hmParam.put("STUDY_ID", strStudyId);
		hmParam.put("SITE_ID", strSiteId);
		hmParam.put("STUDYSITEID",strStudySiteId);
		
		ArrayList alPatientList = gmClinicalBean.loadPatientList(hmParam);
		ArrayList alPeriodList = gmClinicalBean.loadPeriodList(strStudyId);
		
	    ArrayList alActionReqList = GmCommonClass.getCodeList("ACTREQ");
	    ArrayList alResponisbleByList = GmCommonClass.getCodeList("RESBY");
	    ArrayList alStatusList =GmCommonClass.getCodeList("OBVSTS");	    
	    gmClinicalObservationForm.setAlPatientList(alPatientList);
	    gmClinicalObservationForm.setAlPeriodList(alPeriodList);
	    gmClinicalObservationForm.setAlActionReqList(alActionReqList);
	    gmClinicalObservationForm.setAlResponisbleByList(alResponisbleByList);
	    gmClinicalObservationForm.setAlStatusList(alStatusList);
    }   
    
    public void saveObservation(GmClinicalObservationForm gmClinicalObservationForm, GmClinicalSiteMtrObservationBean gmClinicalSiteMtrObservationBean, String strUserId) throws AppError{    	  
    	  
    	 HashMap hmParam = new HashMap();
    	 String strSiteMonitorId = gmClinicalObservationForm.getHsiteMonitorId();
    	 String strPatientId = gmClinicalObservationForm.getPatientId();
    	 String strPeriodId = gmClinicalObservationForm.getPeriodId();
    	 String strIssueDesc = gmClinicalObservationForm.getIssueDescription();
    	 String strActionRequired = gmClinicalObservationForm.getActionRequired();
    	 String strResponsibleBy = gmClinicalObservationForm.getResponisbleBy();
    	 String strTargetResDate = gmClinicalObservationForm.getTargetResDate();
    	 String strResDesc = gmClinicalObservationForm.getResolution();
    	 String strActualResDate = gmClinicalObservationForm.getActualResDate();
    	 String strStatus = gmClinicalObservationForm.getStatus();    	 
    	 String strObservationId=gmClinicalObservationForm.getObservationId();    
    	 
    	 if(strObservationId==null || strObservationId.equalsIgnoreCase("")){
    		 strObservationId="0";
    	 }  	     	 
		  hmParam.put("SITEMONITORID",strSiteMonitorId);
	      hmParam.put("PATIENTID",strPatientId);
	      hmParam.put("PERIODID",strPeriodId);
	      hmParam.put("ISSUEDESC",strIssueDesc);
	      hmParam.put("ACTIONREQ",strActionRequired);
	      hmParam.put("RESBY",strResponsibleBy);
	      hmParam.put("TARGETRESDATE",strTargetResDate);
	      hmParam.put("RESDESC",strResDesc);   
	      hmParam.put("ACTRESDATE",strActualResDate);
	      hmParam.put("STATUS",strStatus);
	      hmParam.put("USERID",strUserId);
	      hmParam.put("OBSERVATIONID",strObservationId);	      
	      strObservationId = gmClinicalSiteMtrObservationBean.saveSiteMtrObservation(hmParam);	      
	      gmClinicalObservationForm.setObservationId(strObservationId);
	      
	      hmParam= new HashMap();
	      hmParam.put("OBSERVATIONID", strObservationId);
	      ArrayList alSiteMtrObservationList = gmClinicalSiteMtrObservationBean.fetchSiteMtrObservation(hmParam); 
	      
	      if(alSiteMtrObservationList.size()>0){
	    		 HashMap hmReturn = (HashMap)alSiteMtrObservationList.get(0);
	    		 String strStatusName = GmCommonClass.parseNull((String)hmReturn.get("STATUS"));
	    		 gmClinicalObservationForm.setStatusName(strStatusName);
	      }
    }    
    
    public void addMoreObservation(GmClinicalObservationForm gmClinicalObservationForm) throws AppError{
    	 
    	 gmClinicalObservationForm.setPatientId("");
    	 gmClinicalObservationForm.setPeriodId("");
    	 gmClinicalObservationForm.setObservationId("");  
    	 gmClinicalObservationForm.setIssueDescription("");
    	 gmClinicalObservationForm.setActionRequired("");
    	 gmClinicalObservationForm.setResponisbleBy("");
    	 gmClinicalObservationForm.setTargetResDate("");
    	 gmClinicalObservationForm.setResolution("");
    	 gmClinicalObservationForm.setActualResDate("");
    	 gmClinicalObservationForm.setStatus("");
    } 
    
    public void loadNextIssue(GmClinicalObservationForm gmClinicalObservationForm, GmClinicalSiteMtrObservationBean gmClinicalSiteMtrObservationBean) throws AppError{
    	
	  	  String strObservationId = gmClinicalObservationForm.getObservationId();
	  	  String strStudyId = gmClinicalObservationForm.getHstudyId();
	      String strStudySiteId = gmClinicalObservationForm.getHstudySiteId();
	      String strVisitdate = gmClinicalObservationForm.getVisitDate();
	      String strStatus = gmClinicalObservationForm.getStatus();
        
	      gmClinicalObservationForm.setObservationId(strObservationId);	      
	      HashMap hmParam= new HashMap();
	      hmParam.put("OBSERVATIONID", strObservationId);
	      hmParam.put("STUDYID", strStudyId);
	      hmParam.put("STUDYSITEID", strStudySiteId);
	      hmParam.put("VISITDATE", strVisitdate);
	      hmParam.put("STATUS", strStatus);
	      ArrayList alSiteMtrObservationList = gmClinicalSiteMtrObservationBean.fetchNextSiteMtrObservation(hmParam);
	      
	      if(alSiteMtrObservationList.size()>0){
	    		 HashMap hmReturn = (HashMap)alSiteMtrObservationList.get(0);
	    		 String strStatusName = GmCommonClass.parseNull((String)hmReturn.get("STATUS_NAME"));	    		 
	        	 String strPatientId = GmCommonClass.parseNull((String)hmReturn.get("PATIENT_ID"));
	        	 String strPeriodId = GmCommonClass.parseNull((String)hmReturn.get("STUDY_PERIOD_ID"));
	        	 String strIssueDesc = GmCommonClass.parseNull((String)hmReturn.get("ISSUE_DESC"));
	        	 String strActionRequired = GmCommonClass.parseNull((String)hmReturn.get("ACTION"));
	        	 String strResponsibleBy = GmCommonClass.parseNull((String)hmReturn.get("RESPONSIBLE_BY"));
	        	 String strTargetResDate = GmCommonClass.parseNull((String)hmReturn.get("TGT_RES_DT"));
	        	 String strResDesc = GmCommonClass.parseNull((String)hmReturn.get("RES_DESC"));
	        	 String strActualResDate = GmCommonClass.parseNull((String)hmReturn.get("ACTUAL_RES_DT"));
	        	 strStatus = GmCommonClass.parseNull((String)hmReturn.get("STATUS_ID"));	        	 
	        	 strObservationId=GmCommonClass.parseNull((String)hmReturn.get("SIT_MTR_OBS_ID"));
	        	 
	    		 gmClinicalObservationForm.setStatusName(strStatusName);
	    		 
	    		 gmClinicalObservationForm.setPatientId(strPatientId);
	        	 gmClinicalObservationForm.setPeriodId(strPeriodId);
	        	 gmClinicalObservationForm.setIssueDescription(strIssueDesc);
	        	 gmClinicalObservationForm.setActionRequired(strActionRequired);
	        	 gmClinicalObservationForm.setResponisbleBy(strResponsibleBy);
	        	 gmClinicalObservationForm.setTargetResDate(strTargetResDate);
	        	 gmClinicalObservationForm.setResolution(strResDesc);
	        	 gmClinicalObservationForm.setActualResDate(strActualResDate);
	        	 gmClinicalObservationForm.setStatus(strStatus);
	        	 gmClinicalObservationForm.setObservationId(strObservationId);		        	 
	      }else{
	    	  gmClinicalObservationForm.setMsg("There is no next issue from same visit ");
	      }
  }
    
}
