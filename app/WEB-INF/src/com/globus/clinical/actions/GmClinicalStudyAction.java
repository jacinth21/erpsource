package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.forms.GmClinicalStudyForm;
import com.globus.common.beans.GmLogger;

public class GmClinicalStudyAction extends GmAction
{

	// Code to Initialize the Logger Class.
    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strCondition = "";
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    		HttpServletResponse response) throws Exception
    {   
    	strCondition = getAccessFilter(request, response);
    	GmClinicalStudyForm gmClinicalStudyFrom = (GmClinicalStudyForm)form;
    	GmClinicalBean gmClinicalBean = new GmClinicalBean();
		ArrayList alResult = gmClinicalBean.loadStudyList(strCondition);
		
        HashMap hmParamV = new HashMap();
        hmParamV.put("TEMPLATE","GmClinicalStudy.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);
        
        String strGridXmlData = gmClinicalBean.getXmlGridData(hmParamV);
        gmClinicalStudyFrom.setGridXmlData(strGridXmlData);
        
    	return mapping.findForward("gmClinicalStudy");
    }
  
}
