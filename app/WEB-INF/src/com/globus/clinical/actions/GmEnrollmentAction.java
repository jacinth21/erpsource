package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmEnrollmentRptBean;
import com.globus.clinical.forms.GmRptEnrollByMonthForm;
import com.globus.clinical.forms.GmRptEnrollByTreatementForm;
import com.globus.clinical.forms.GmRptSurgeryByDateForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmEnrollmentAction extends GmClinicalDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
    HashMap hmReturn = new HashMap();
   
    
    public ActionForward reportEnrollSiteByMonth(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
            GmEnrollmentRptBean gmEnrollmentRptBean = new GmEnrollmentRptBean();
            GmRptEnrollByMonthForm gmRptEnrollByMonthForm = (GmRptEnrollByMonthForm) form;
            setIncludeInfo(gmRptEnrollByMonthForm);
            setDefaultDate(gmRptEnrollByMonthForm,1);
            gmRptEnrollByMonthForm.loadSessionParameters(request);
            String strStudyId = "";
            String fromDate = "";
            String toDate = "";
            String strSiteId = "";
            String strDhtmlx = "";
            
            //Get study and site from the session.
            strSiteId = GmCommonClass.parseNull(gmRptEnrollByMonthForm.getSessSiteId()); 
            strStudyId = GmCommonClass.parseNull(gmRptEnrollByMonthForm.getSessStudyId());
            strOpt = GmCommonClass.parseNull(gmRptEnrollByMonthForm.getStrOpt());
            fromDate = gmRptEnrollByMonthForm.getFromDate();
            toDate = gmRptEnrollByMonthForm.getToDate();
            hmReturn = gmEnrollmentRptBean.reportEnrollSiteByMonth(strStudyId,fromDate,toDate);
            gmRptEnrollByMonthForm.setHmRptEnrollByMonth(hmReturn);                
            
        return mapping.findForward("success");
    }
    
    public ActionForward reportSurgeryByDate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
        String strCondition = "";
        String strOpt = "";
        String strForward = "";
        String strSiteId = "";
        String strStudyId = "";
        String strXmlGridData = "";
        strCondition = getAccessFilter(request, response);
        GmRptSurgeryByDateForm gmRptSurgeryByDateForm = (GmRptSurgeryByDateForm) form;
        gmRptSurgeryByDateForm.loadSessionParameters(request);
        setIncludeInfo(form, strCondition);
        
        //Get study and site from the session.
        strSiteId = GmCommonClass.parseNull(gmRptSurgeryByDateForm.getSessSiteId());
        strSiteId = strSiteId.equals("")?GmCommonClass.parseNull(gmRptSurgeryByDateForm.getSiteId()):strSiteId; 
        strStudyId = GmCommonClass.parseNull(gmRptSurgeryByDateForm.getSessStudyId());
        strOpt = GmCommonClass.parseNull(gmRptSurgeryByDateForm.getStrOpt());
        if (gmRptSurgeryByDateForm.getStrOpt().equals("Report"))
        {
            GmEnrollmentRptBean gmEnrollmentRptBean = new GmEnrollmentRptBean();
            HashMap hmParam = new HashMap();
            String strSiteNames = GmCommonClass.createInputString(gmRptSurgeryByDateForm.getCheckSiteName());
            String strStudySiteId = GmCommonClass.getStudySiteId(strSiteId, strStudyId);
            
            if (!strStudySiteId.equals(""))
            {
                gmRptSurgeryByDateForm.setCheckSiteName(new String[] {strStudySiteId});
            }
            
            strSiteNames = strSiteNames.equals("") ? strStudySiteId : strSiteNames;
            strSiteNames = strSiteNames.equals("") ? GmCommonClass.parseNull(gmRptSurgeryByDateForm.getSessSiteId()):strSiteNames; //DHTMLX Change
            String strTreatment = gmRptSurgeryByDateForm.getCbo_Treatment();            
            String strStatus = gmRptSurgeryByDateForm.getCbo_Status();
            
            strTreatment = strTreatment.equals("0") ? "" : strTreatment;
            strStatus = strStatus.equals("0") ? "" : strStatus;

            hmParam.put("STUDYID", strStudyId);
            hmParam.put("SITENAMES", strSiteNames);
            hmParam.put("TREATMENT", strTreatment);
            hmParam.put("STATUS", strStatus);
            gmRptSurgeryByDateForm.setStrSiteIds(strSiteNames);
            ArrayList alResult = (ArrayList)gmEnrollmentRptBean.reportSurgeryByDate(hmParam, strCondition);
            HashMap hmParamV = new HashMap();
            hmParamV.put("STUDYLIST",strStudyId);
            hmParamV.put("TEMPLATE","GmClinicalPlanSurgery.vm");
            hmParamV.put("TEMPLATEPATH","clinical/templates");
            hmParamV.put("RLIST",alResult);
            strXmlGridData = gmEnrollmentRptBean.getXmlGridData(hmParamV);
            gmRptSurgeryByDateForm.setGridXmlData(strXmlGridData);
        }
        
        gmRptSurgeryByDateForm.setAlTreatment(GmCommonClass.getCodeList(strStudyId));
        gmRptSurgeryByDateForm.setAlStatus(GmCommonClass.getCodeList("TRSTS"));
        strForward ="gmRptPlanSurgery";            
        return mapping.findForward(strForward);
    }
    
    public ActionForward reportEnrollByTreatment(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {         
            GmEnrollmentRptBean gmEnrollmentRptBean = new GmEnrollmentRptBean();
            GmRptEnrollByTreatementForm gmRptEnrollByTreatementForm = (GmRptEnrollByTreatementForm) form;
            setIncludeInfo(gmRptEnrollByTreatementForm);
            gmRptEnrollByTreatementForm.loadSessionParameters(request);
            String strStudyId = "";
            strStudyId = GmCommonClass.parseNull(gmRptEnrollByTreatementForm.getSessStudyId());
            //if (gmRptEnrollByTreatementForm.getStrOpt().equals("Report"))
            {
                //strStudyId = gmRptEnrollByTreatementForm.getStudyListId();
                gmRptEnrollByTreatementForm.setHmRptEnrollByTreatement(gmEnrollmentRptBean.reportEnrollByTreatment(strStudyId));                
            }
        return mapping.findForward("success");
    }
}
