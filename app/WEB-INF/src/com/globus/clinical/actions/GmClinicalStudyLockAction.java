package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.forms.GmClinicalStudyLockForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmClinicalStudyLockAction extends GmDispatchAction {

	// Code to Initialize the Logger Class.
	Logger log = GmLogger.getInstance(this.getClass().getName());
	String strCondition = "";
	String strOpt = "";
	String strUserId = "";
	String strApplnDateFmt = "";
	String strAccessFlag = "";

	/**
	 * loadStudyLockDtls() - load the study lock reports
	 * 
	 * @author mmuthusamy
	 * @param ActionMapping, form
	 * @exception AppError
	 */
	public ActionForward loadStudyLockDtls(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError {

		strCondition = getAccessFilter(request, response);
		GmClinicalStudyLockForm gmClinicalStudyLockFrom = (GmClinicalStudyLockForm) form;
		gmClinicalStudyLockFrom.loadSessionParameters(request);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		
		ArrayList alStatus = new ArrayList();
		ArrayList alStudyResult = new ArrayList();
		HashMap hmParamV = new HashMap();
		HashMap hmFormValues = new HashMap();
		HashMap hmAccess = new HashMap();
		
		

		hmFormValues = GmCommonClass.getHashMapFromForm(gmClinicalStudyLockFrom);
		
		strOpt = GmCommonClass.parseNull((String)hmFormValues.get("STROPT"));
		strUserId = GmCommonClass.parseNull((String)hmFormValues.get("USERID"));
		strApplnDateFmt = GmCommonClass.parseNull((String)hmFormValues.get("APPLNDATEFMT"));
		alStatus = GmCommonClass.parseNullArrayList((ArrayList)GmCommonClass.getCodeList("LOCSTY"));
		// fetch the study details
		alStudyResult = GmCommonClass.parseNullArrayList((ArrayList)gmClinicalBean.fetchStudyDtls());
		hmParamV.put("TEMPLATE", "GmClinicalStudyLock.vm");
		hmParamV.put("TEMPLATEPATH", "clinical/templates");
		hmParamV.put("RLIST", alStudyResult);
		HashMap hmApplnParam = new HashMap();
		hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
		hmParamV.put("hmApplnParam",hmApplnParam);

		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "LOCK_UNLOCK_STUDY"));
		strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		log.debug(" Access permissions flag Lock/Unlock " + hmAccess);
		gmClinicalStudyLockFrom.setSubmitAccess("");
		if(!strAccessFlag.equals("Y")){
			gmClinicalStudyLockFrom.setSubmitAccess("disabled");
		}

		String strGridXmlData = gmClinicalBean.getXmlGridData(hmParamV);
		gmClinicalStudyLockFrom.setGridXmlData(strGridXmlData);
		gmClinicalStudyLockFrom.setAlStudyStatus(alStatus);
		gmClinicalStudyLockFrom.setScreenName("Study Lock");

		return mapping.findForward("gmClinicalStudyLock");
	}
	
	/**
	 * loadStudyListRpt() - load the study List reports
	 * 
	 * @author mmuthusamy
	 * @param ActionMapping, form
	 * @exception AppError
	 */
	public ActionForward loadStudyListRpt(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError {

		strCondition = getAccessFilter(request, response);
		GmClinicalStudyLockForm gmClinicalStudyLockFrom = (GmClinicalStudyLockForm) form;
		gmClinicalStudyLockFrom.loadSessionParameters(request);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		ArrayList alResult = new ArrayList();
		HashMap hmParamV = new HashMap();
		HashMap hmFormValues = new HashMap();
		
		String strStudyId = "";
		String strScreenNm = "";

		hmFormValues = GmCommonClass.getHashMapFromForm(gmClinicalStudyLockFrom);
		
		String strTempletNm = "GmClinicalStudyListRpt.vm";
		strScreenNm = "Study List Report";
		strOpt = GmCommonClass.parseNull((String)hmFormValues.get("STROPT"));
		strUserId = GmCommonClass.parseNull((String)hmFormValues.get("USERID"));
		strStudyId = GmCommonClass.parseNull((String)hmFormValues.get("STUDYID"));
		strApplnDateFmt = GmCommonClass.parseNull((String)hmFormValues.get("APPLNDATEFMT"));
		alResult = GmCommonClass.parseNullArrayList((ArrayList)gmClinicalBean.fetchStudyDtls());
		
		if(strOpt.equals("FetchLog")){
			alResult = new ArrayList();
			alResult =  GmCommonClass.parseNullArrayList((ArrayList)gmClinicalBean.fetchStudyHistory(strStudyId));
			strTempletNm = "GmClinicalStudyHistory.vm";
			strScreenNm = "Study History Report";
		}
		
		hmParamV.put("TEMPLATE", strTempletNm);
		hmParamV.put("TEMPLATEPATH", "clinical/templates");
		hmParamV.put("RLIST", alResult);
		HashMap hmApplnParam = new HashMap();
		hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
		hmParamV.put("hmApplnParam",hmApplnParam);
		
		String strGridXmlData = gmClinicalBean.getXmlGridData(hmParamV);
		gmClinicalStudyLockFrom.setGridXmlData(strGridXmlData);
		gmClinicalStudyLockFrom.setScreenName(strScreenNm);
		return mapping.findForward("gmClinicalStudyList");
	}

}
