package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalTrainingBean;

import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.clinical.forms.GmStudyFilterForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;

public class GmClinicalTrainingAction extends GmClinicalDispatchAction {

	public ActionForward fchTrainingSummary(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError {
		
		String strForward = "gmClinicalTrainingSummary";
		String strSiteId = "";
		String strStudyId = "";
		String strXmlGridData = "";
		HashMap hmParam = new HashMap();
		log.debug("strForward  " + strForward);
		String strCondition = getAccessFilter(request, response);
		GmStudyFilterForm gmStudyFilterForm = (GmStudyFilterForm) form;
		GmStudyBean gmStudyBean = new GmStudyBean();
		GmPatientBean gmPatientBean = new GmPatientBean();
		gmStudyFilterForm.loadSessionParameters(request);

		String strOpt = gmStudyFilterForm.getStrOpt();
		log.debug("Inside GmClinicalTrainingAction "+strOpt);
		gmStudyFilterForm.setAlStudyList(gmStudyBean.reportStudyNameList());
		gmStudyFilterForm.setAlSiteNameList(gmPatientBean.loadSiteFromStudy(gmStudyFilterForm.getSessStudyId(), "")); // To
		// discuss
		// about
		// the
		// static
		// and
		// access
		// condition
		gmStudyFilterForm.setAlStatus((ArrayList) GmCommonClass.getCodeList("CRSTA", strCondition));
		gmStudyFilterForm.setAlTrainingFor((ArrayList) GmCommonClass.getCodeList("CRTPE", strCondition));
		//gmStudyFilterForm.setAlTrainingReason((ArrayList) GmCommonClass.getCodeList("CRRFT", strCondition));
		strStudyId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudyId());
		strSiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessSiteId());
		String strStudySiteId = "";		
		
		strStudySiteId = GmCommonClass.parseNull(gmStudyFilterForm.getSessStudySiteId());
		strStudySiteId = strStudySiteId.equals("") ? gmStudyFilterForm.getSiteList():strStudySiteId;
		gmStudyFilterForm.setSiteList(strStudySiteId);
		if (strOpt.equals("")) {
			strOpt = "loadReport";
		}		
		log.debug("strStudySiteId "+strStudySiteId);
		if(strOpt.equalsIgnoreCase("reloadReport")){
			gmStudyFilterForm.setTrainingReason("0");
		}
		hmParam = GmCommonClass.getHashMapFromForm(gmStudyFilterForm);
		String strTrainingFor = GmCommonClass.parseNull((String)hmParam.get("TRAININGFOR"));
		
		if (strTrainingFor.equals("2400")) {				
			 gmStudyFilterForm.setAlTrainingReason((ArrayList) GmCommonClass.getCodeList("TRRFSC", strCondition));
		}else if (strTrainingFor.equals("2401")) {
			gmStudyFilterForm.setAlTrainingReason((ArrayList) GmCommonClass.getCodeList("TRRFPI", strCondition));
		}else if (strTrainingFor.equals("2404")) {
			gmStudyFilterForm.setAlTrainingReason((ArrayList) GmCommonClass.getCodeList("TRRFSU", strCondition));
		}
		else if (strTrainingFor.equals("2402")){//CRA			
			gmStudyFilterForm.setAlTrainingReason((ArrayList) GmCommonClass.getCodeList("TRRFCD", strCondition));
		}
		else if (strTrainingFor.equals("2403")){//DM			
			gmStudyFilterForm.setAlTrainingReason((ArrayList) GmCommonClass.getCodeList("TRRFCD", strCondition));
		}
	
		
		hmParam.put("STUDYID", strStudyId);
		hmParam.put("SITELIST", strStudySiteId);
		

		if (strOpt.equalsIgnoreCase("reloadReport") || strOpt.equalsIgnoreCase("loadReport")) {

			GmClinicalTrainingBean gmClinicalTrainingBean = new GmClinicalTrainingBean();
			ArrayList alResult = (ArrayList) gmClinicalTrainingBean.loadTrainingReport(hmParam, strCondition);

			HashMap hmParamV = new HashMap();
			hmParamV.put("TEMPLATE", "GmClinicalTrainingReport.vm");
			hmParamV.put("TEMPLATEPATH", "clinical/templates");
			hmParamV.put("RLIST", alResult);
			strXmlGridData = gmClinicalTrainingBean.getXmlGridData(hmParamV);
			gmStudyFilterForm.setGridXmlData(strXmlGridData);
		}
		return mapping.findForward(strForward);
	}

}
