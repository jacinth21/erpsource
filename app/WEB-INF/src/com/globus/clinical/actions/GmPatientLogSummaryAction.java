package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmPatientLogBean;
import com.globus.clinical.forms.GmPatientLogSummaryForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonCurrConvForm;

public class GmPatientLogSummaryAction extends GmClinicalDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
    HashMap hmReturn = new HashMap();   
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    { 
		String strSiteId = "";
		String strStudySiteId = "";
		String strStudyId = "";
		String strAccLvl="";
	    String strXmlGridData = "";
	    HashMap hmParamN = new HashMap(); // to get patient id's
	    
	    GmPatientLogSummaryForm gmPatientLogForm = (GmPatientLogSummaryForm)form;
		gmPatientLogForm.loadSessionParameters(request);
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		GmPatientLogBean gmPatientLogBean = new GmPatientLogBean();
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmPatientLogForm);
		ArrayList alLogList = new ArrayList();
		
	    String strOpt = gmPatientLogForm.getStrOpt();		
		//Get study and site from the session.
		strStudySiteId = GmCommonClass.parseNull(gmPatientLogForm.getSessStudySiteId()); 
		strSiteId = GmCommonClass.parseNull(gmPatientLogForm.getSessSiteId());
		strStudyId = GmCommonClass.parseNull(gmPatientLogForm.getSessStudyId());
		hmParam.put("STUDY_ID", strStudyId);
		hmParam.put("SITE_ID", strSiteId);
		hmParam.put("STUDYSITEID", strStudySiteId);
		gmPatientLogForm.setAlStudyPeriod(GmCommonClass.parseNullArrayList(gmClinicalBean.loadPeriodList(strStudyId)));
		gmPatientLogForm.setAlPatientId(GmCommonClass.parseNullArrayList(gmClinicalBean.loadPatientList(hmParam)));
		gmPatientLogForm.setAlFormList(GmCommonClass.parseNullArrayList(gmClinicalBean.loadStudyFormList(strStudyId)));
		alLogList = gmPatientLogBean.fetchPatientLogSummary(hmParam);
		
		HashMap hmParamV = new HashMap();
        hmParamV.put("TEMPLATE","GmPatientLogSummary.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alLogList);
       // log.debug("List===="+alResult);
        strXmlGridData = gmClinicalBean.getXmlGridData(hmParamV);
        gmPatientLogForm.setGridXmlData(strXmlGridData);
		
		return mapping.findForward("gmPatientLogSummary");
    } 
        
    
}
