package com.globus.clinical.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalPaymentBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.forms.GmClinicalPaymentForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalPaymentAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    GmClinicalPaymentForm gmClinicalPaymentForm = (GmClinicalPaymentForm) form;
    instantiate(request, response);
    gmClinicalPaymentForm.loadSessionParameters(request);
    String strOpt = gmClinicalPaymentForm.getStrOpt();
    String strPartyId = "";
    String strMAPUpdFl = "";
    gmClinicalPaymentForm.setMsg("");

    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmClinicalPaymentForm);
    GmClinicalPaymentBean gmClinicalPaymentBean = new GmClinicalPaymentBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmPatientBean gmPatientBean = new GmPatientBean();
    HashMap hmAccess=new HashMap();
    HashMap hmDetails = new HashMap();
    ArrayList alPaymentDetailList = null;
    String strRequestType = gmClinicalPaymentForm.getRequestTypeId();
    String strStudySiteId = GmCommonClass.parseNull((String) hmParam.get("SESSSTUDYSITEID"));

  
    if (strOpt.equals("load_payment") || strOpt.equals("load_payable_to")
        || strOpt.equals("save_payment") || strOpt.equals("edit_payment")) {

      if (strOpt.equals("save_payment")) {
        String strPaymentRequestId = gmClinicalPaymentBean.saveClinicalPayment(hmParam);
        hmParam.put("PAYMENTREQUESTID", strPaymentRequestId);
      }
      if (!strOpt.equals("load_payment")) {
        HashMap hmReturn = gmClinicalPaymentBean.fetchPayment(hmParam);
        
        ArrayList alPaymentList = (ArrayList) hmReturn.get("PAYMENT");
        alPaymentDetailList = (ArrayList) hmReturn.get("PAYMENTDETAIL");
        hmParam.put("PAYMENTDETAIL", alPaymentDetailList);
        if (alPaymentList.size() > 0) {
          HashMap hmPayment = (HashMap) alPaymentList.get(0);
          GmCommonClass.getFormFromHashMap(gmClinicalPaymentForm, hmPayment);
          hmParam.put("REQUESTTYPEID", hmPayment.get("REQUESTTYPEID"));
        }
        if (strOpt.equals("load_payable_to")) {
          hmParam.put("REQUESTTYPEID", strRequestType);
        }
      }
      
      strPartyId =GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
      hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                  "TINNUMBER"));
      strMAPUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          
      gmClinicalPaymentForm.setAccessFlag(strMAPUpdFl); 
          
      HashMap hmReturn = loadClinicalPayment(hmParam);
     
      ArrayList alPatientList = (ArrayList) hmReturn.get("ALPATIENTLIST");
      ArrayList alPeriodList = (ArrayList) hmReturn.get("ALPERIODLIST");
      ArrayList alStatusList = (ArrayList) hmReturn.get("ALSTATUSLIST");
      ArrayList alPaymetReqList = (ArrayList) hmReturn.get("ALPAYMENTREQLIST");
      ArrayList alPayableList = (ArrayList) hmReturn.get("ALPAYABLELIST");

      gmClinicalPaymentForm.setAlPatientList(alPatientList);
      gmClinicalPaymentForm.setAlPeriodList(alPeriodList);
      gmClinicalPaymentForm.setAlStatusList(alStatusList);
      gmClinicalPaymentForm.setAlPaymetReqList(alPaymetReqList);
      gmClinicalPaymentForm.setAlPayableList(alPayableList);
     
      

      GmCommonClass.getFormFromHashMap(gmClinicalPaymentForm, hmReturn);
      
      String strPaymentReqId=gmClinicalPaymentForm.getPaymentRequestId();
   
      
      if(strPaymentReqId.equals("")){
      
      hmDetails = gmPatientBean.loadSiteMapInfo(strStudySiteId, "");
      gmClinicalPaymentForm.setTinNumber(GmCommonClass.parseNull((String)hmDetails.get("TINNUMBER")));
      
      }

      return mapping.findForward("loadpayment");
    } else if (strOpt.equals("print")) {
      HashMap hmHeader = new HashMap();
      ArrayList alDetails = new ArrayList();
      HashMap hmPrintPaymentDetailList =
      gmClinicalPaymentBean.fetchPaymentPrintDetails(gmClinicalPaymentForm.getPaymentRequestId());
      hmHeader = (HashMap) hmPrintPaymentDetailList.get("HEADER");
      alDetails = (ArrayList) hmPrintPaymentDetailList.get("DETAILS");
      gmClinicalPaymentForm.setHmPrintParam(hmHeader);
      gmClinicalPaymentForm.setAlPrintResult(alDetails);
      return mapping.findForward("print");
    }


    return mapping.findForward("loadpayment");
  }

  /**
   * loadClinicalPayment This method is used to load all drop down data.
   * 
   * @param HashMap hmParam
   * @param HashMap hmReturn
   * @exception AppError
   */
  public HashMap loadClinicalPayment(HashMap hmParam) throws AppError {
    
	  HashMap hmReturn = new HashMap();
   
    HashMap hmAccess = new HashMap();

    GmClinicalBean gmClinicalBean = new GmClinicalBean();
    GmPatientBean gmPatientBean = new GmPatientBean();
    GmClinicalPaymentBean gmClinicalPaymentBean = new GmClinicalPaymentBean();
    GmClinicalPaymentForm gmClinicalPaymentForm =new GmClinicalPaymentForm();
    
    HashMap hmStudySite = new HashMap();
    String strStudySiteId = (String) hmParam.get("SESSSTUDYSITEID");
    log.debug("Selected Study Site Id is :" + strStudySiteId);
    String strStudyId = (String) hmParam.get("SESSSTUDYID");
    String strSiteId = "";
   
    hmStudySite.put("STUDY_ID", strStudyId);
    hmStudySite.put("SITE_ID", strSiteId);
    hmStudySite.put("STUDYSITEID", strStudySiteId);

    ArrayList alPatientList = gmClinicalBean.loadPatientList(hmStudySite);
    ArrayList alPeriodList = gmClinicalBean.loadPeriodList(strStudyId);

    hmReturn.put("ALPATIENTLIST", gmClinicalBean.loadPatientList(hmStudySite));
    hmReturn.put("ALPERIODLIST", gmClinicalBean.loadPeriodList(strStudyId));
    hmReturn.put("ALSTATUSLIST", GmCommonClass.getCodeList("PTSTA"));
    hmReturn.put("ALPAYMENTREQLIST", GmCommonClass.getCodeList("PTREQ"));
    hmReturn.put("REQUESTTYPEID", hmParam.get("REQUESTTYPEID"));

    ArrayList alPaymentReasonList =
        gmClinicalPaymentBean.getCodeListByAlt((String) hmParam.get("REQUESTTYPEID"));

    hmReturn.put("ALPAYMENTREASONLIST",
        gmClinicalPaymentBean.getCodeListByAlt((String) hmParam.get("REQUESTTYPEID")));
    String strRequestType = (String) hmParam.get("REQUESTTYPEID");
    String strPayableCodeId = "ID";
    String strPayableCodeName = "NAME";
    ArrayList alPayableList = new ArrayList();
    HashMap hmTemp = new HashMap();
    hmTemp.put("ID", "1");
    hmTemp.put("NAME", "None");

    if (strRequestType.equalsIgnoreCase("60623")) {
      alPayableList = gmClinicalBean.loadPatientList(hmStudySite);
      strPayableCodeId = "PATID";
      strPayableCodeName = "PATIDE";
    } else if (strRequestType.equalsIgnoreCase("60621")) {
      alPayableList = gmClinicalPaymentBean.loadSiteCordinator(strStudySiteId);
    } else if (strRequestType.equalsIgnoreCase("60622")) {
      alPayableList = gmClinicalPaymentBean.loadSurgeonMapDetails(strStudySiteId);
    } else if (strRequestType.equalsIgnoreCase("60625")) {
      alPayableList = gmClinicalPaymentBean.loadSiteByStudySiteId(strStudySiteId);
    } else if (strRequestType.equalsIgnoreCase("60626") || strRequestType.equalsIgnoreCase("60627")
        || strRequestType.equalsIgnoreCase("60628") || strRequestType.equalsIgnoreCase("60624")
        || strRequestType.equalsIgnoreCase("60629")) {
      alPayableList.add(hmTemp);
    }

    hmReturn.put("PAYABLECODEID", strPayableCodeId);
    hmReturn.put("PAYABLECODENAME", strPayableCodeName);
    hmReturn.put("ALPAYABLELIST", alPayableList);

    ArrayList alPaymentDetailList = (ArrayList) hmParam.get("PAYMENTDETAIL");
    if (alPaymentDetailList == null) {
      alPaymentDetailList = new ArrayList();
    }
 
   
    
   
    HashMap hmData = new HashMap();
    hmData.put("RESULT", alPaymentDetailList);
    hmData.put("PATIENTLIST", alPatientList);
    hmData.put("PERIODLIST", alPeriodList);
    hmData.put("PAYMENTREASONLIST", alPaymentReasonList);
    hmData.put("REQUESTTYPE", hmReturn.get("REQUESTTYPEID"));
    hmData.put("ALGLACCOUNT", gmClinicalPaymentBean.loadAccountList());
    hmReturn.put("GRIDXMLDATA", generateOutPut(hmData));
    return hmReturn;

  }

  /**
   * generateOutPut This method is used to generate data which need to show in grid.
   * 
   * @param hmData HashMap String
   * @exception AppError
   */

  public String generateOutPut(HashMap hmData) throws AppError {

    ArrayList alResult = (ArrayList) hmData.get("RESULT");
    ArrayList alPatientList = (ArrayList) hmData.get("PATIENTLIST");
    ArrayList alPeriodList = (ArrayList) hmData.get("PERIODLIST");
    ArrayList alPaymentReasonList = (ArrayList) hmData.get("PAYMENTREASONLIST");
    ArrayList alGlAccountList = (ArrayList) hmData.get("ALGLACCOUNT");

    String strRequestTypeID = (String) hmData.get("REQUESTTYPE");
    log.debug(" Request Type is " + strRequestTypeID);

    HashMap hmParam = new HashMap();
    hmParam.put("RequestTypeID", strRequestTypeID);

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setTemplateName("GmClinicalPayment.vm");
    templateUtil.setTemplateSubDir("clinical/templates");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataList("alPatientList", alPatientList);
    templateUtil.setDataList("alPeriodList", alPeriodList);
    templateUtil.setDataList("alPaymentReasonList", alPaymentReasonList);
    templateUtil.setDataList("alGlAccountList", alGlAccountList);
    templateUtil.setDataMap("hmConditions", hmParam);

    String gridXmlData = templateUtil.generateOutput();
    log.debug(" gridXmlData :" + gridXmlData);
    return gridXmlData;
  }
}
