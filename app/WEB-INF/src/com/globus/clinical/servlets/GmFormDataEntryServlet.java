/*****************************************************************************
 * File			 : GmFormDataEntryServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.clinical.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.clinical.beans.*;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;

public class GmFormDataEntryServlet extends GmServlet {

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strClinicPath = GmCommonClass.getString("GMCLINIC");
		String strDispatchTo = strClinicPath.concat("/GmFormDataEntry.jsp");
		String strStudyId = "";
		String strFormId = "";
		String strPatientId = "";
		String strStudyPeriodId = "";
		String strStudyPeriodIdDB = "";
		String strFormDt = "";
		String strPatListId="";
		String strInitials = "";
		String strInputStr = "";
		String strPatientListId = "";
		String strVerifyType = "";
		String strCommentDS = "";
		String strCalcValue = "";
		String strMissingFollowup = "";
		String strEventNo = "";
		String strFlag = "";
		String strDisableQNo="";
		String strVoidFl = "";
		String strSessFormId = "";
		String strSessStudyPeriodId = "";
		String strSessClickedUrl = "";
		String strSessLastClickedUrl = "";
		String strIncVoidFrm = "";
		String strEventLbl = "";
		String strAdvEventFl  = "";
		String strButtonDisabled = "";
		String strRedirectUrl = "";
		String strSessStudyId ="";
		
		try {
			checkSession(response, session); // Checks if the current session
												// is valid, else redirecting to
												// SessionExpiry page
			instantiate(request,response);
			String strUserId = (String) session.getAttribute("strSessUserId");
			String strAction = request.getParameter("hAction");
			strAction = (strAction == null) ? request.getParameter("strAction"):strAction;
			strAction = (strAction == null) ? "Load" : strAction;
			
			strSessStudyId =GmCommonClass.parseNull((String)session.getAttribute("strSessStudyId"));
			
			strStudyId =GmCommonClass.parseNull(request.getParameter("hStudyId"));//To avoid study id duplication from session
			if(strStudyId.equals("")){
				strStudyId=strSessStudyId;
			}
			String strPatientInitials = GmCommonClass.getRuleValue(strStudyId,"INIT_LABEL");
			log.debug("strPatientInitials"+strPatientInitials);
			strAction=validateRequest(strAction,session,request);
			//strStudyId = request.getParameter("Cbo_Study") == null ? "" : request.getParameter("Cbo_Study");
			strFlag = request.getParameter("hFlag") == null ? "" : request.getParameter("hFlag");
			strEventNo = GmCommonClass.parseNull(request.getParameter("cbo_EventNo"));
			strVoidFl = GmCommonClass.parseNull(request.getParameter("hVoidFl"));
			GmClinicalBean gmClinic = new GmClinicalBean();
			GmCommonBean gmCommonBean = new GmCommonBean();
			HashMap hmReturn = new HashMap();
			HashMap hmTemp = new HashMap();
			HashMap hmParam = new HashMap();
			HashMap hmFormAccess = new HashMap();
			ArrayList alNoOfEvents = new ArrayList();
			HashMap hmValues = new HashMap();
			ArrayList alReturn = new ArrayList();
			String strCondition= getAccessFilter(request, response);
			
			String strVoidAccessFl =  gmCommonBean.checkUserExists(strUserId, "2007_VD_FRM_ACCESS");
			if(!strVoidAccessFl.equals("0")){
			    request.setAttribute("VoidAccessFl","Y");
			}else{
			    request.setAttribute("VoidAccessFl","N");
			}
			hmFormAccess.put("SESSSTUDYID", strStudyId);
			hmFormAccess.put("USERID", strUserId);
			strButtonDisabled = gmClinic.getFormEditAccess(hmFormAccess);
			log.debug(" strButtonDisabled --> "+strButtonDisabled);
			if (strAction.equals("Reload") || strAction.equals("ReloadForm") || strAction.equals("LoadQues") || strAction.equals("SaveAns")
					|| strAction.equals("EditAns") || strAction.equals("Unvoid")) {
				//alReturn = gmClinic.loadStudyList(strCondition);
				hmReturn.put("STUDYLIST", alReturn);
				hmParam.put("STUDY_ID", strStudyId);
				hmParam.put("ACCESS_CONDITION", strCondition);
				hmTemp = gmClinic.loadFormDataEntryLists(hmParam);
				hmReturn.put("PERIODLIST", hmTemp.get("PERIODLIST"));
				//hmReturn.put("PATIENTLIST", hmTemp.get("PATIENTLIST"));
				
				//based on the session params load the period and forms for that period.
				strPatientId = (String)session.getAttribute("strSessPatientPkey");
				//strPatientId = strPatientId.equals("")? request.getParameter("strPatientId"):strPatientId; 
				strStudyPeriodId = (String)session.getAttribute("strSessPeriodId");
				strSessFormId = GmCommonClass.parseNull((String)session.getAttribute("strSessFormId"));
				strSessStudyPeriodId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudyPeriodId"));
				strSessClickedUrl = GmCommonClass.parseNull((String)session.getAttribute("strSessClickedUrl"));
				
				strSessLastClickedUrl = GmCommonClass.parseNull((String)session.getAttribute("strSessLastClicked"));
				
				if(!strSessClickedUrl.equals(strSessLastClickedUrl))
				{
					strSessFormId = "";
					strSessStudyPeriodId = "";					
				}
				
				strStudyPeriodId = strStudyPeriodId.equals("") ? strSessStudyPeriodId :strStudyPeriodId;
				
				strPatientId = request.getParameter("hPatientId") == null ? strPatientId: request.getParameter("hPatientId");
				strFormId = request.getParameter("Cbo_Form") == null ? strSessFormId : request.getParameter("Cbo_Form");
				strStudyPeriodId = request.getParameter("Cbo_Period") == null ? strStudyPeriodId : request.getParameter("Cbo_Period");
				strStudyPeriodIdDB = request.getParameter("hStPerId") == null ? "" : request.getParameter("hStPerId");
				strIncVoidFrm = request.getParameter("hIncludeVoidFrm") == null ? "" : request.getParameter("hIncludeVoidFrm");
// get rulevalue for Event dropdown label & check if condition for default label.
				strEventLbl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strFormId,"EVENTNAME_LABEL"));
				strAdvEventFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strFormId,"ADVERSE_EVENT")); // Getting the Flag value for showing the UnVoid button for Adverse Event Forms Only as part of MNTTASK-3766
				if (strEventLbl.equals("")){
					strEventLbl = "Adverse Event:";
				}
				if(strVoidFl.equals("Y")){
					strIncVoidFrm = strVoidFl;
				}
				strFormDt = GmCommonClass.parseNull(request.getParameter("Txt_FormDate"));
				
				strPatListId = GmCommonClass.parseNull(request.getParameter("hPatListID"));
				
				strMissingFollowup = GmCommonClass.parseNull(request.getParameter("Chk_MissingFollowUp"));
				alReturn = gmClinic.loadAllFormsList(strStudyId);
				log.debug("alReturn frm"+alReturn);
				hmReturn.put("FORMLIST", alReturn);
				request.setAttribute("hmLists", hmReturn);
				// strFlag = (String) alReturn.get(1);
				hmTemp.put("RULEID", strStudyId+strStudyPeriodId+strFormId);
				hmTemp.put("RULEGROUP","CR_FRM_DISBL_QUES");			
				strDisableQNo = gmCommonBean.getRuleValue(hmTemp);
				
				request.setAttribute("hPatientId", strPatientId);
				request.setAttribute("hFormId", strFormId);
				request.setAttribute("hStudyPeriod", strStudyPeriodId);
				request.setAttribute("hEventNo", strEventNo);
				request.setAttribute("hDisableQNo", strDisableQNo);
// set the rulevalue for Event.				
				request.setAttribute("hEventLbl", strEventLbl);
				request.setAttribute("ADVEVEFL", strAdvEventFl);
				
				session.setAttribute("strSessFormId", strFormId);
				session.setAttribute("strSessStudyPeriodId", strStudyPeriodId);
				session.setAttribute("strSessClickedUrl", strSessLastClickedUrl);
				
				if (strAction.equals("LoadQues") || strAction.equals("Unvoid")) {
					/*
					log.debug("strPatientId====="+strPatientId);
					log.debug("strStudyPeriodIdDB====="+strStudyPeriodIdDB);
					log.debug("strStudyId====="+strStudyId);
					log.debug("strFormId====="+strFormId);
					log.debug("strEventNo====="+strEventNo);
					log.debug("strFormDt====="+strFormDt);
					*/
					//hmReturn = gmClinic.loadPatientFormDetails(strStudyId, strPatientId, strFormId, strStudyPeriodIdDB, strEventNo, strFormDt,strPatListId);
					//request.setAttribute("hmReturn", hmReturn);
					// This "Unvoid" if block is included for Un-Voiding the Voided Forms(Adverse Event Forms) as part of the MNTTASK-3766
					if (strAction.equals("Unvoid")){	
						strPatListId = request.getParameter("hPatListId") == null ? "" : request.getParameter("hPatListId");														
						gmClinic.unVoidReportForm(strPatListId, strPatientId, strFormId , strUserId);
						strVoidFl="N";	
						strAction="LoadQues";	
						strPatListId="";
						strIncVoidFrm="";
					}
					request.setAttribute("hStPerId", strStudyPeriodIdDB);
					request.setAttribute("hStudyPeriod", strStudyPeriodId);
					alNoOfEvents = gmClinic.loadNoOfEventsDesc(strPatientId, strFormId, strStudyPeriodIdDB,strVoidFl);
					request.setAttribute("alNoOfEvents", alNoOfEvents);
					
				} else if (strAction.equals("SaveAns") || strAction.equals("EditAns")) {
					strInitials = GmCommonClass.parseNull(request.getParameter("Txt_Initials"));
					strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
					strPatientListId = GmCommonClass.parseNull(request.getParameter("hPatListId"));
					strVerifyType = GmCommonClass.parseNull(request.getParameter("Cbo_VerifyType"));
					strCommentDS = GmCommonClass.parseNull(request.getParameter("txtaVerifyDesc"));
					strCalcValue = GmCommonClass.parseNull(request.getParameter("Txt_CalVal"));
					
					if(strFormDt.equals("")){
						strFormDt = (String)session.getAttribute("strSessTodaysDate");
					}

					hmValues.put("STUDYID", strStudyId);
					hmValues.put("STUDYPERIOD", strStudyPeriodIdDB);
					hmValues.put("PATIENTID", strPatientId);
					hmValues.put("FORMID", strFormId);
					hmValues.put("FORMDATE", strFormDt);
					hmValues.put("INITIALS", strInitials);
					hmValues.put("INPUTSTR", strInputStr);
					hmValues.put("USERID", strUserId);
					hmValues.put("PATLISTID", strPatientListId);
					hmValues.put("CALVAL", strCalcValue);
					hmValues.put("VERIFYTYPE", strVerifyType);
					hmValues.put("COMMENTDS", strCommentDS);
					hmValues.put("MISSINGFOLLOWUP", strMissingFollowup);

					if (strEventNo.equals("0")) {
						strAction = "SaveAns";
					}
					hmValues.put("EVENTNO", strEventNo);

					hmValues.put("FLAG", strFlag);
					hmReturn = gmClinic.saveFormAnswers(hmValues, strAction);
					//alNoOfEvents = gmClinic.loadNoOfEvents(strPatientId, strFormId, strStudyPeriodIdDB);
					alNoOfEvents = gmClinic.loadNoOfEventsDesc(strPatientId, strFormId, strStudyPeriodIdDB,strVoidFl);
					log.debug("FLAG" + strFlag);
					if (strEventNo.equals("0")) {
						int intNoOfEvents= alNoOfEvents.size();
						if(intNoOfEvents > 0){
						HashMap hmLastEvent = (HashMap)alNoOfEvents.get(intNoOfEvents-1);
						strEventNo = (String)hmLastEvent.get("EVENT_NO");
						}
					}

					//hmReturn = gmClinic.loadPatientFormDetails(strStudyId, strPatientId, strFormId, strStudyPeriodIdDB, strEventNo, strFormDt,strPatListId);
					//request.setAttribute("hmReturn", hmReturn);
					request.setAttribute("hStudyPeriod", strStudyPeriodId);
					// request.setAttribute("hEventNo",strEventNo);
					// request.setAttribute("hFlag",strFlag);

					request.setAttribute("alNoOfEvents", alNoOfEvents);
					// to form the redirect URL - same as Load
					strRedirectUrl = "/GmFormDataEntryServlet?hAction=LoadQues&Cbo_Form="
							+ strFormId
							+ "&Cbo_Period="
							+ strStudyPeriodId
							+ "&hPatientId="
							+ strPatientId
							+ "&hPeriodId="
							+ strStudyPeriodId
							+ "&hStudyId="
							+ strStudyId
							+ "&hStPerId="
							+ strStudyPeriodIdDB
							+ "&cbo_EventNo="
							+ strEventNo
							+ "&Txt_FormDate="
							+ strFormDt + "&hPatListID=" + strPatListId;
					// to call the send redirect method
					gotoPage(strRedirectUrl, request, response);
				}
			} else if (strAction.equals("ViewHis")) {
				strPatientListId = request.getParameter("hPatListId") == null ? "" : request.getParameter("hPatListId");
				alReturn = gmClinic.loadPatientAnsHistory(strPatientListId);
				request.setAttribute("HISTORYLIST", alReturn);
				strDispatchTo = strClinicPath.concat("/GmPatientAnswerHistory.jsp");
			}
			// to avoid the SQL Error ORA936 (when action is view history - at the time don't have the parameter values) 
			if(!strAction.equalsIgnoreCase("ViewHis")){
				hmReturn = gmClinic.loadPatientFormDetails(strStudyId, strPatientId, strFormId, strStudyPeriodIdDB, strEventNo, strFormDt,strPatListId,strIncVoidFrm);
			}			
			//PMT-243 get rule value to validate form answer group
			String strFormVld = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strFormId,strStudyId));
			alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(strFormVld));
			request.setAttribute("ALFRMVLD", alReturn);
			
			request.setAttribute("hmReturn", hmReturn);
			request.setAttribute("hStudyId", strStudyId);
			request.setAttribute("hAction", strAction);
			request.setAttribute("hEventNo", strEventNo);
			request.setAttribute("strPatientInitials", strPatientInitials);
			request.setAttribute("strButtonDisabled", strButtonDisabled);
			dispatch(strDispatchTo, request, response);

		}// End of try
		catch (Exception e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strDispatchTo = strComnPath + "/GmError.jsp";
			gotoPage(strDispatchTo, request, response);
		}// End of catch
	}// End of service method
	
	//This request will avoid the duplicate submit at fresh form submit
	public String validateRequest(String strAction,HttpSession session,HttpServletRequest request)
	{
		String token=(String) session.getAttribute("Token");
		if(strAction.equals("SaveAns") && token==null)
		{
			session.setAttribute("Token", request.getSession().getId());
		}else if(strAction.equals("SaveAns") && token!=null)
		{
			strAction = "Reload";
		}else
		{
			session.removeAttribute("Token");
		}
		return strAction;
	}
}// End of GmFormDataEntryServlet
