/*****************************************************************************
 * File			 : GmPatientMasterServlet 
 * Desc		 	 : This Servlet is used to handle the patient master information 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.clinical.servlets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;

public class GmPatientMasterServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
		String strClinicalPath = GmCommonClass.getString("GMCLINIC");

		String strDispatchTo = "/GmPatientMaster.jsp";
		String strId = "";
		String strRepId = "";
		
		String strStudyID = null;
		String strPatientID = "";
		String strStudySiteID = "";
        String strAccountID = "";
        String strRuleValue="";
        String strSiteName="";
        String strSurgTypId="";
        String strSurgLvlId="";
        String strSurgInvnId="";
        String strSurgDt="";
        String strComments="";
        String strSessSiteId ="";
        String strPatientListId="";
        String strSessAccLvl="";
        String strButtonDisabled = "";
		
		GmPatientBean gmPatient = new GmPatientBean();
		GmCommonClass gmCommon = new GmCommonClass();
		GmCustomerBean gmCust = new GmCustomerBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
        
		HashMap hmReturn = new HashMap();
		HashMap hmResult = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmFormAccess = new HashMap();

		ArrayList alDistList = new ArrayList();
		ArrayList alReturn = new ArrayList();

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			String strAction = request.getParameter("hAction");
			String strAppDtFmt =GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt")); 			
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("hAction");
			}
			strAction = (strAction == null)?"Load":strAction;
			
			strStudyID = GmCommonClass.parseNull((String) session.getAttribute("strSessStudyId"));
			//strStudyID = GmCommonClass.parseNull(request.getParameter("hStudyID"));
			strSessSiteId = GmCommonClass.parseNull((String) session.getAttribute("strSessSiteId"));
			strSessAccLvl = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
			strAccountID  = GmCommonClass.parseNull((String) session.getAttribute("strSessStudySiteId"));
			
			strPatientID = request.getParameter("hPatientID")== null ?"":request.getParameter("hPatientID");
            //strAccountID = GmCommonClass.parseNull(request.getParameter("Cbo_AccountID"));
            String strPatientMasterId     = GmCommonClass.parseNull(request.getParameter("hPKeyID"));
            String strUserId = 	(String)session.getAttribute("strSessUserId");
            
            hmFormAccess.put("SESSSTUDYID", strStudyID);
			hmFormAccess.put("USERID", strUserId);
			strButtonDisabled = gmClinicalBean.getFormEditAccess(hmFormAccess);
			
			if (strAction.equals("Edit"))
			{
				String strRPatientID 	= GmCommonClass.parseNull(request.getParameter("Txt_RPatientIDE")) ;
				String strSurgeryType 	= GmCommonClass.parseNull(request.getParameter("Cbo_SurgType"));
				String strSurgeryDate 	= GmCommonClass.parseNull(request.getParameter("Txt_SurgeryDate"));
				//String strAccountID 	= GmCommonClass.parseNull(request.getParameter("Cbo_AccountID"));
				String strSurgeonID 	= GmCommonClass.parseNull(request.getParameter("Cbo_SurgeonID"));
                String strSurgeryCompletedFl     = GmCommonClass.parseNull(request.getParameter("Chk_SurgeryComplete"));
                String strTxtLogReason     = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
                
                
				HashMap hmValues = new HashMap();
				hmValues.put("PID",strRPatientID) ;
				hmValues.put("SID",strStudyID) ;			
				hmValues.put("S_TYPE",strSurgeryType);
				hmValues.put("S_DATE",strSurgeryDate);
				hmValues.put("A_ID",strAccountID);
				hmValues.put("S_ID",strSurgeonID);                
                hmValues.put("S_FLAG",strSurgeryCompletedFl);
                hmValues.put("TXT_LOGREASON",strTxtLogReason);
                hmValues.put("PKEY_ID",strPatientMasterId);
                
                
                
                String strMessage = gmPatient.savePatientInfo(hmValues,strUserId);
                
				request.setAttribute("MSG", strMessage);
				// To load the saved value
				strAction = "EditLoad";
                if(strMessage.equals(""))
                {
                    strPatientID = strRPatientID;
                }

			}
			if(strAction.equals("LoadSurg")){
				GmAccessControlBean gmAccessControlBean=new GmAccessControlBean();
				strSiteName = GmCommonClass.parseNull(request.getParameter("hSiteName"));
				strPatientListId=GmCommonClass.parseNull(request.getParameter("hPatientLId"));
			    strSurgInvnId=GmCommonClass.parseNull(request.getParameter("hSurgInvnNo"));
            	strRuleValue=GmCommonClass.getRuleValue(strStudyID, "SURGICALINTERVENTION");
            	hmParam.put("GROUPNAME", " 2007_VD_FRM_ACCESS ");
            	hmParam.put("USERNM", strUserId);
            	alReturn = gmAccessControlBean.fetchGroupMappingDetails(hmParam);
            	if(!strSurgInvnId.equals("")){
            	hmReturn = gmPatient.fetchSurgicalDetails(strSurgInvnId,strPatientListId);
            	}
            	if(alReturn.size()>0)
            		hmReturn.put("VOIDFL", "Y");
            	
            	alReturn = GmCommonClass.getCodeList(strRuleValue);
            	hmReturn.put("SURGTYPE", alReturn);
            	strRuleValue=GmCommonClass.getRuleValue(strStudyID, "SURGICALINVNORGL");
            	alReturn = GmCommonClass.getCodeList(strRuleValue);
            	hmReturn.put("ORLVLINV", alReturn);
            	hmReturn.put("hSiteName", strSiteName);
            	hmReturn.put("strSessAccLvl", strSessAccLvl);
            	hmReturn.put("hAction", strAction);
            	strDispatchTo = "/GmSurgicalIntervention.jsp";
            	
            }else if(strAction.equals("SaveSurg") || strAction.equals("VerifySurg")){
            	strSurgInvnId = GmCommonClass.parseNull(request.getParameter("hSurgInvnId"));
            	strSurgTypId = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
            	strSurgLvlId = GmCommonClass.parseNull(request.getParameter("Cbo_Orglevel"));
            	strSurgDt = GmCommonClass.parseNull(request.getParameter("Txt_SurgeryDate"));
            	strComments = GmCommonClass.parseNull(request.getParameter("txtaVerifyDesc"));
            	
            	hmParam.put("SESSDATEFMT", strAppDtFmt);
            	hmParam.put("STUDYID",strStudyID);
            	hmParam.put("SURGINVID",strSurgInvnId);
            	hmParam.put("PATIENT_IDE_NO",strPatientID);
            	hmParam.put("SURGTYPE",strSurgTypId);
            	hmParam.put("SURGORGLVL",strSurgLvlId);
            	hmParam.put("SURGDT",strSurgDt);
            	hmParam.put("COMMENTS",strComments);
            	hmParam.put("USERID",strUserId);
            	if(strAction.equals("VerifySurg")){
            		hmParam.put("VRFLG","Y");	
            	}else{
            		hmParam.put("VRFLG","N");
            	}
            	gmPatient.saveSurgicalDetails(hmParam);
            	strAction = "EditLoad";
            }
			if (strAction.equals("EditLoad"))
			{
				hmResult = gmPatient.LoadPatientDetails(strPatientID,strStudyID);
				hmReturn.put("EDITLOAD",hmResult);				
				request.setAttribute("hAction","EditLoad");				
                strAccountID = (String) hmResult.get("A_ID"); 
                strPatientMasterId = (String)hmResult.get("PKEY_ID");
                hmResult = new HashMap();
                hmResult.put("STUDYID", strStudyID);
                hmResult.put("SITEID", strSessSiteId);
                hmResult.put("PATIENTIDENO", strPatientID);
                alReturn = gmPatient.fetchSurgicalInvRpt(hmResult);
                hmParam.put("RLIST", alReturn);
                hmParam.put("TEMPLATE", "GmSurgicalIntervention.vm");
                hmParam.put("TEMPLATEPATH", "clinical/templates");
                String strXmlData = gmPatient.getXmlGridData(hmParam);
                hmReturn.put("GRIDDATA", strXmlData);
			}
			
            if(strAction.equals("Reload") || (!strAccountID.equals("0") && !strAccountID.equals("")))
            {
                hmResult = gmPatient.loadAccountDetails(strAccountID);
                hmReturn.put("RELOAD",hmResult);                                                           
            }
            
            //	To load the default value and Combo Box value information
			hmReturn.put("COMBOVALUE",gmPatient.loadPatientComboValue(strStudyID, strAccountID));
			
			request.setAttribute("hSiteID",strAccountID);
			request.setAttribute("hmReturn",hmReturn);
			request.setAttribute("hStudyID",strStudyID);            
            request.setAttribute("hmLog",gmCommonBean.getLog(strPatientMasterId, "1230"));
            request.setAttribute("hPatientID",strPatientID);
            request.setAttribute("strButtonDisabled", strButtonDisabled);
			dispatch(strClinicalPath.concat(strDispatchTo),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmSalesRepServlet