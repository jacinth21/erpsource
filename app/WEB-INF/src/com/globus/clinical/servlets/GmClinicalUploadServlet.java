package com.globus.clinical.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadException;
import javazoom.upload.UploadFile;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmClinicalUploadBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmUploadServlet;
import com.globus.common.util.GmTemplateUtil;

public class GmClinicalUploadServlet extends GmUploadServlet {

	private static String strUploadHome = GmCommonClass.getString("UPLOADHOME");
	private static String strDefaultFileToDispatch = "";
	private static String strErrorFileToDispatch = "";
	private static String strFileName = "";
	private String strFileToDispatch = "";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	private String strPath = GmCommonClass.getString("GMCLINIC");
	private static String strComnPath = GmCommonClass.getString("GMCOMMON");
	private static String strFormName = "";

	public static void loadClinicalProperties() {
		strUploadHome = GmCommonClass.getString("UPLOADHOME");
		strDefaultFileToDispatch = "/clinical/GmClinicalUpload.jsp";
		strErrorFileToDispatch = strComnPath + "/GmError.jsp";
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		String strAction = "";
		String strPatientRecordId = "";
		String strButtonDisabled = "";
		
		ArrayList alReturn = new ArrayList();
		ArrayList alPatientRecord = new ArrayList();
		HashMap hmParams = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmFormAccess = new HashMap();
		GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
		String strStudyListId = "";
		String strFormList ="";
		String strDisplayname ="";
		MultipartFormDataRequest mrequest = null;
		strFileToDispatch = strDefaultFileToDispatch;
		String strRoot = "";
		checkSession(response, session); // Checks if the current session is
		// valid, else redirecting to
		// SessionExpiry page
		loadClinicalProperties();
		String xmlFileName = GmCommonClass.getString("XMLFILENAME");
		try {
			if (MultipartFormDataRequest.isMultipartFormData(request)) {
				// creating MultiPart request
				mrequest = new MultipartFormDataRequest(request);
				strAction = GmCommonClass.parseNull((String) mrequest.getParameter("hAction"));
				strDisplayname =GmCommonClass.parseNull((String) mrequest.getParameter("hDisplayNm"));
				log.debug("displayname::"+strDisplayname);
				strFormName = GmCommonClass.parseNull((String) mrequest.getParameter("hFormName"));

			} else {
				strAction = GmCommonClass.parseNull((String) request.getParameter("hAction"));
				strFormName = GmCommonClass.parseNull((String) request.getParameter("hFormName"));
			}

			request.setAttribute("mrequest", mrequest);
			log.debug("Action:" + strAction);
			request.setAttribute("hAction", strAction);
			GmClinicalBean gmClinicalBean = new GmClinicalBean();
			GmClinicalUploadBean gmClinicalUploadBean = new GmClinicalUploadBean();
			ArrayList alFormList = new ArrayList();
			ArrayList alUploadinfo = new ArrayList();
			
			String strStudyList = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessStudyId"));
			String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
			strFormList = GmCommonClass.parseNull((String) request.getParameter("strFormList"));
			strFormName = !strFormName.equals("") ? strFormName.toLowerCase() : strFormName;
			if (strFormName.equalsIgnoreCase("sf36")) {
				strUploadHome = GmCommonClass.getString("CLINICAL_UPLOADHOME");
			}
			log.debug("strUploadHome is " + strUploadHome);
			// setting alStudyList value to display on load
			ArrayList alStudyList = gmClinicalBean.loadStudyList();
			request.setAttribute("alStudyList", alStudyList);
			request.setAttribute("strStudyList", strStudyList);
			request.setAttribute("strFormList", strFormList);
			request.setAttribute("strFormName", strFormName);

			hmFormAccess.put("SESSSTUDYID", strStudyList);
			hmFormAccess.put("USERID", strUserId);
			strButtonDisabled = gmClinicalBean.getFormEditAccess(hmFormAccess);
			
			// setting alFormList value
			alFormList = gmClinicalBean.loadFormsList(strStudyList);
			request.setAttribute("alFormList", alFormList);
			request.setAttribute("strButtonDisabled", strButtonDisabled);

			if (strAction.equals("reload")) {
				// fetching unique strStudyListId value
				strStudyListId = gmClinicalUploadBean.getStudyListId(strStudyList, strFormList);
				// Fetching the file info
				alUploadinfo = gmCommonUploadBean.fetchUploadInfo("90900", strStudyListId);
				request.setAttribute("alUploadinfo", alUploadinfo);
			}

			if (strAction.equals("upload")) {

				// Upload the file on server
				uploadFile(mrequest, request);
				// fetching unique strStudyListId value
				strStudyListId = gmClinicalUploadBean.getStudyListId(strStudyList, strFormList);
				strRoot = GmCommonClass.getString("GMCLINICALXMLROOT" + strStudyListId);

				if (strFormName.equalsIgnoreCase("sf36")) {

					String strDownloadedFrm = GmCommonClass.getString(strStudyList + "_HLTH_STA_FRM");

					xmlFileName = GmCommonClass.getString("SF36XMLFILENAME");
					hmParams.put("UPLOADHOME", strUploadHome + strFormName + "\\");
					hmParams.put("UPLOADEDFILENAME", strFileName);
					hmParams.put("STUDYLISTID", strStudyListId);
					hmParams.put("TYPE", "90900");
					hmParams.put("STUDYLIST", strStudyList);
					hmParams.put("UPLOADEDFORMID", strFormList);
					hmParams.put("DOWNLOADEDFORMID", strDownloadedFrm);
					hmParams.put("XMLFILENAME", xmlFileName);
					hmParams.put("ROOT", strRoot);
					hmParams.put("USERID", (String) session.getAttribute("strSessUserId"));
					gmClinicalUploadBean.saveToSF36Form(hmParams);
					/** ********** */
					alFormList = gmClinicalBean.loadFormsList(strStudyList);
					request.setAttribute("alFormList", alFormList);
					/** ******************** */
				} else {
					// Saving the file info & values in database
					gmCommonUploadBean.validateFileName("90900", strStudyListId, strFileName);
					gmClinicalUploadBean.saveToForm(strUploadHome + strFormName + "\\", strFileName, strStudyListId, "90900", strStudyList, strFormList,
							xmlFileName, strRoot, (String) session.getAttribute("strSessUserId"));
				}
			}
			if (strAction.equals("load_radiograph")) {
				loadRadiographAttribute(request);
				dispatch(strPath.concat("/GmPatientRecord.jsp"), request, response);
			} else if (strAction.equals("search_radiograph")) {
				loadRadiographAttribute(request);
				alPatientRecord = loadPatientRecord(request);
				GmTemplateUtil templateUtil = new GmTemplateUtil();
				templateUtil.setTemplateName("GmClinicalSearchRadiograph.vm");
				templateUtil.setTemplateSubDir("clinical/templates");
				templateUtil.setDataList("alResult", alPatientRecord);
				request.setAttribute("gridData", templateUtil.generateOutput());
				dispatch(strPath.concat("/GmPatientRecord.jsp"), request, response);
			} else if (strAction.equals("save_new_upload")) {
				log.debug("save new upload called.......");
				uploadRadiograph(request, mrequest);
				alReturn = savePatientRecord(request, mrequest);
				request.setAttribute("alPatientRecord", alReturn);
				request.setAttribute("hAction", "overwrite_upload");
				loadRadiographAttribute(request);
				dispatch(strPath.concat("/GmRadiographUpload.jsp"), request, response);
			} else if (strAction.equals("save_overwrite_upload")) {
				uploadRadiograph(request, mrequest);
				log.debug("radiograph uploaded");
				strPatientRecordId = GmCommonClass.parseNull((String) mrequest.getParameter("hPatientRecordId"));
				hmParams.clear();
				hmParams.put("PAT_REC_ID", strPatientRecordId);
				hmParams.put("STUDYID", (String) request.getSession().getAttribute("strSessStudyId"));
				hmParams.put("SITEID", (String) request.getSession().getAttribute("strSessSiteId"));
				alPatientRecord = gmClinicalBean.loadPatientRecord(hmParams);
				request.setAttribute("alPatientRecord", alPatientRecord);
				loadRadiographAttribute(request);
				request.setAttribute("hAction", "overwrite_upload");
				dispatch(strPath.concat("/GmRadiographUpload.jsp"), request, response);
			} else if (strAction.equals("save_edit_attributes")) {
				String strOldFileName = GmCommonClass.parseNull((String) mrequest.getParameter("fileName"));
				String strTypeNm = GmCommonClass.parseNull((String) mrequest.getParameter("hTypeName"));
				String strFileXtn = strOldFileName.substring(strOldFileName.lastIndexOf("."));
				String strFilePath = GmCommonClass.getString("RADIOLOGYUPLOADIR") + strTypeNm + "\\";
				String strNewFileName = getFileName(mrequest);
				strNewFileName = strNewFileName + strFileXtn;

				File oldFile = new File(strFilePath + strOldFileName);
				File newFile = new File(strFilePath + strNewFileName);
				boolean success = oldFile.renameTo(newFile);

				request.setAttribute("fileName", strNewFileName);
				alReturn = savePatientRecord(request, mrequest);
				request.setAttribute("alPatientRecord", alReturn);
				loadRadiographAttribute(request);
				request.setAttribute("hAction", "edit_attributes");
				dispatch(strPath.concat("/GmRadiographUpload.jsp"), request, response);
			} else if (strAction.equals("new_upload")) {

				if(mrequest!=null){
					request.setAttribute("PatientID", GmCommonClass.parseNull((String) mrequest.getParameter("cbo_Patient")));
					request.setAttribute("PeriodID", GmCommonClass.parseNull((String) mrequest.getParameter("cbo_Period")));
					request.setAttribute("Type", GmCommonClass.parseNull((String) mrequest.getParameter("cbo_Type")));
					request.setAttribute("View", GmCommonClass.parseNull((String) mrequest.getParameter("cbo_View")));
					request.setAttribute("XRayDt", GmCommonClass.parseNull((String) mrequest.getParameter("Txt_XRayDt")));
				}else{
					request.setAttribute("PatientID", GmCommonClass.parseNull((String) request.getParameter("cbo_Patient")));
					request.setAttribute("PeriodID", GmCommonClass.parseNull((String) request.getParameter("cbo_Period")));
					request.setAttribute("Type", GmCommonClass.parseNull((String) request.getParameter("cbo_Type")));
					request.setAttribute("View", GmCommonClass.parseNull((String) request.getParameter("cbo_View")));					
				}				
				loadRadiographAttribute(request);
				dispatch(strPath.concat("/GmRadiographUpload.jsp"), request, response);
			} else if (strAction.equals("overwrite_upload")) {
				strPatientRecordId = GmCommonClass.parseNull((String) request.getParameter("hPatientRecordId"));
				if (strPatientRecordId.equals("")) {
					strPatientRecordId = GmCommonClass.parseNull((String) mrequest.getParameter("hPatientRecordId"));
				}
				hmParams.clear();
				hmParams.put("PAT_REC_ID", strPatientRecordId);
				hmParams.put("STUDYID", (String) request.getSession().getAttribute("strSessStudyId"));
				hmParams.put("SITEID", (String) request.getSession().getAttribute("strSessSiteId"));
				alPatientRecord = gmClinicalBean.loadPatientRecord(hmParams);
				request.setAttribute("alPatientRecord", alPatientRecord);
				loadRadiographAttribute(request);
				dispatch(strPath.concat("/GmRadiographUpload.jsp"), request, response);
			} else if (strAction.equals("edit_attributes")) {
				strPatientRecordId = GmCommonClass.parseNull((String) request.getParameter("hPatientRecordId"));
				log.debug("strPatientRecordId " + strPatientRecordId);
				hmParams.clear();
				hmParams.put("PAT_REC_ID", strPatientRecordId);
				hmParams.put("STUDYID", (String) request.getSession().getAttribute("strSessStudyId"));
				hmParams.put("SITEID", (String) request.getSession().getAttribute("strSessSiteId"));
				alPatientRecord = gmClinicalBean.loadPatientRecord(hmParams);
				request.setAttribute("alPatientRecord", alPatientRecord);
				loadRadiographAttribute(request);
				dispatch(strPath.concat("/GmRadiographUpload.jsp"), request, response);
				//checking action value if the value "view_upload_Images" getting all values from hmParams and passing it to GmRadiologyView.jsp								
			} else if(strAction.equals("view_upload_Images")){
				strPatientRecordId = GmCommonClass.parseNull((String) request.getParameter("hPatientRecordId"));
				log.debug("strPatientRecordId " + strPatientRecordId);
				hmParams.clear();
				hmParams.put("PAT_REC_ID", strPatientRecordId);
				hmParams.put("STUDYID", (String) request.getSession().getAttribute("strSessStudyId"));
				hmParams.put("SITEID", (String) request.getSession().getAttribute("strSessSiteId"));
				alPatientRecord = gmClinicalBean.loadPatientRecord(hmParams);
				int intRecordsize = alPatientRecord.size();
				if(intRecordsize == 1){
					hmReturn = GmCommonClass.parseNullHashMap((HashMap)alPatientRecord.get(0));
				}
				request.setAttribute("hmReturn", hmReturn);
				request.setAttribute("hAction", strAction);
				request.setAttribute("hView", "single");
				dispatch(strPath.concat("/GmRadiologyView.jsp"), request, response);
			} else {
				// upload page if load or reload
				log.debug("forwarding to " + strPath + "/GmClinicalUpload.jsp");
				dispatch(strPath.concat("/GmClinicalUpload.jsp"), request, response);

			}

		} catch (AppError e) {
			//File Validation time (same file uploaded again) file is deleting from server location
			if (e.getType().equals("E") && !e.getMessageId().equals("20104")) {
				File file = new File(strUploadHome + strFormName + "\\" + strFileName);
				if (file.exists())
					file.delete();
			 // after delete the record, need to update the delete flag.
					gmCommonUploadBean.deleteFile(strStudyListId, "90900", strFileName, strUserId);
			}else if(e.getType().equals("S")){
				String strURL = "/GmClinicalUploadServlet?hAction=reload&strFormList="+strFormList;
				request.setAttribute("hRedirectURL", strURL);
				request.setAttribute("hDisplayNm", strDisplayname);
			}
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			request.setAttribute("hType", e.getType());
			dispatch(strFileToDispatch, request, response);
		} catch (SAXException e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		} catch (ParserConfigurationException e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		} catch (UploadException e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		} catch (Exception uex) {
			File file = new File(strUploadHome + strFormName + "\\" + strFileName);
			if (file.exists())
				file.delete();
			// after delete the record, need to update the delete flag.
			gmCommonUploadBean.deleteFile(strStudyListId, "90900", strFileName, strUserId);
			session.setAttribute("hAppErrorMessage", uex.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		}
	}

	/**
	 * method to upload the radiograph(image)
	 * 
	 * @param request
	 * @param mrequest
	 * @throws Exception
	 */
	public void uploadRadiograph(HttpServletRequest request, MultipartFormDataRequest mrequest) throws Exception {
		String strFileNm = GmCommonClass.parseNull((String) mrequest.getParameter("fileName"));
		String strTypeNm = GmCommonClass.parseNull((String) mrequest.getParameter("hTypeName"));

		if (strFileNm.equals("")) {
			strFileNm = getFileName(mrequest);
		} else {
			strFileNm = strFileNm.substring(0, strFileNm.lastIndexOf("."));
		}

		String strWhiteList = GmCommonClass.getString("RADIOLOGYWHITELIST");
		String strUploadDir = GmCommonClass.getString("RADIOLOGYUPLOADIR");
		String strFileSize = GmCommonClass.getString("MAXFILESIZE");
		strUploadDir = strUploadDir + strTypeNm.replaceAll("/", "").toUpperCase() + "\\";
		log.debug("strUploadDir " + strUploadDir);
		log.debug("strFileNm " + strFileNm);
		setStrFileName(strFileNm);
		setStrWhitelist(strWhiteList);
		setStrUploadHome(strUploadDir);
		Hashtable hmFiles = mrequest.getFiles();
		if ((hmFiles != null) && (!hmFiles.isEmpty())) {

			UploadFile file = (UploadFile) hmFiles.get("uploadfile");
			long filesize = file.getFileSize();
			log.debug("imageize =>>>>>>>>>>>>>>>>>>>>>>>>" + filesize);
			if (filesize > Integer.parseInt(strFileSize)) {
				throw new AppError("The image size is too big, you can not upload!", "", 'E');
			}
		}
		try{
			super.uploadFile(mrequest, request);
		}catch(UploadException e){
			
			String strException = e.getMessage();
			if (strException.indexOf(GmCommonClass.getString("UPLOAD_DEFAULT_ERROR"))!=-1){
				strException = GmCommonClass.getString("UPLOAD_MODIFIED_ERROR")+getStrWhitelist();				
				throw new Exception(strException);
			}
			
		}
		request.setAttribute("fileName", getStrFileName());
	}

	public String getFileName(MultipartFormDataRequest mrequest) throws Exception {
		String strPeriod = GmCommonClass.parseNull((String) mrequest.getParameter("cbo_Period"));
		String strPatientide = GmCommonClass.parseNull((String) mrequest.getParameter("hPatientide"));
		String strXRayDt = GmCommonClass.parseNull((String) mrequest.getParameter("Txt_XRayDt"));
		String strFileNm = GmCommonClass.parseNull((String) mrequest.getParameter("fileName"));
		String strViewName = GmCommonClass.parseNull((String) mrequest.getParameter("hViewName"));
		String strPeriodNm = GmCommonClass.parseNull((String) mrequest.getParameter("hPeriodName"));

		String strTypeNm = GmCommonClass.parseNull((String) mrequest.getParameter("hTypeName"));
		String strUploadDir = GmCommonClass.getString("RADIOLOGYUPLOADIR");
		strUploadDir = strUploadDir + strTypeNm.replaceAll("/", "").toUpperCase() + "\\";
		String[] strDTSplit;
		if (strPeriod.equals("6309")) {// n/a
			strDTSplit = strXRayDt.split("/");
			strFileNm = strPatientide + "_" + strViewName + "_" + strDTSplit[2] + strDTSplit[0] + strDTSplit[1];
			strFileNm = strFileNm.replaceAll("/", "");
		} else {
			strFileNm = strPatientide + "_" + strViewName + "_" + strPeriodNm;
			strFileNm = strFileNm.replaceAll("/", "");
		}
		strFileNm = strFileNm.replaceAll(" ", "_");
		if(strTypeNm  != "60391"){
            strFileNm = checkFileName(strUploadDir, strFileNm);
        } 
		return strFileNm;
	}

	private static String checkFileName(String strUploadDir, String strFileNm)  throws Exception{
		String strPeriodNm = "";
		while(true){
			File fileExist = new File(strUploadDir+File.separator+strFileNm+".jpg");
			int fileNo = 0;
			int strPeriodNmLen = 0;
			int fileNoDate = 0;
			
			if(fileExist.exists()){
				strPeriodNm = strFileNm.substring(strFileNm.lastIndexOf("_")+1);
		        try{
		                   fileNo = Integer.parseInt(strPeriodNm);
		                   strPeriodNmLen = strPeriodNm.length();
		                   if(strPeriodNmLen == 8){
		                	   fileNoDate++;
		                	   strFileNm = strFileNm.substring(0,strFileNm.lastIndexOf("_")+1+strPeriodNmLen);
		                	   strFileNm +="_"+fileNoDate;
		                	}else{
		                		fileNo++;
		                		strFileNm = strFileNm.substring(0,strFileNm.lastIndexOf("_"));
		                	}
		        }catch(Exception e){	
		             fileNo++;
		        }
		        if(strPeriodNmLen != 8){
		          strFileNm +="_"+fileNo;
		        }
		     }else{
		    	 break;
		     }//end file condition code
		} //end while 
		return strFileNm;
	}
	/**
	 * method to save the patient record
	 * 
	 * @param request
	 * @param mrequest
	 * @return
	 * @throws Exception
	 */
	public ArrayList savePatientRecord(HttpServletRequest request, MultipartFormDataRequest mrequest) throws Exception {
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		String strPeriod = GmCommonClass.parseNull((String) mrequest.getParameter("cbo_Period"));
		String strPatientid = GmCommonClass.parseNull((String) mrequest.getParameter("cbo_Patient"));
		String strPatientRecordId = GmCommonClass.parseNull((String) mrequest.getParameter("hPatientRecordId"));
		String strXRayDt = GmCommonClass.parseNull((String) mrequest.getParameter("Txt_XRayDt"));
		String strView = GmCommonClass.parseNull((String) mrequest.getParameter("cbo_View"));
		String strType = GmCommonClass.parseNull((String) mrequest.getParameter("cbo_Type"));
		String strFileNm = "";
		String strStudyId = (String) request.getSession().getAttribute("strSessStudyId");
		strFileNm = GmCommonClass.parseNull((String) request.getAttribute("fileName"));
		if (strFileNm.equals("")) {
			strFileNm = GmCommonClass.parseNull((String) mrequest.getParameter("fileName"));
		}
		ArrayList alPatientRecord = new ArrayList();
		HashMap hmParams = new HashMap();
		hmParams.put("PATIENT_ID", strPatientid);
		hmParams.put("PAT_REC_ID", strPatientRecordId);
		hmParams.put("TIME_POINT_ID", strPeriod);
		hmParams.put("TYPE_ID", strType);
		hmParams.put("VIEW_ID", strView);
		hmParams.put("FILENAME", strFileNm);
		hmParams.put("USERID", (String) mrequest.getRequest().getSession().getAttribute("strSessUserId"));
		hmParams.put("STUDYID", strStudyId);
		hmParams.put("SITEID", (String) request.getSession().getAttribute("strSessSiteId"));
		hmParams.put("RADIOGRPHDT", strXRayDt);
		log.debug("hmParams " + hmParams);
		strPatientRecordId = "" + gmClinicalBean.savePatientRecord(hmParams);
		log.debug(" patient record returned " + strPatientRecordId);
		hmParams.clear();
		hmParams.put("STUDYID", strStudyId);
		hmParams.put("SITEID", (String) request.getSession().getAttribute("strSessSiteId"));
		hmParams.put("PAT_REC_ID", strPatientRecordId);
		alPatientRecord = gmClinicalBean.loadPatientRecord(hmParams);
		log.debug("alPatientRecord " + alPatientRecord);
		return alPatientRecord;
	}

	public boolean uploadFile(MultipartFormDataRequest mrequest, HttpServletRequest request) throws UploadException, IOException {
		// Upload the file on server
		// loadClinicalProperties();
		UploadBean uploadBean = new UploadBean();
		boolean flag = true;

		String strUploadPath = strUploadHome + strFormName + "\\";
		uploadBean.setFolderstore(strUploadPath);
		uploadBean.setOverwrite(true);
		uploadBean.setWhitelist("*.csv");
		Hashtable hmFiles = mrequest.getFiles();
		if ((hmFiles != null) && (!hmFiles.isEmpty())) {
			UploadFile file = (UploadFile) hmFiles.get("uploadfile");
			if (file != null) {
				uploadBean.store(mrequest, "uploadfile");
			}
			strFileName = file.getFileName();

		}

		return flag;
	}

	/**
	 * Method to load the dropdown values on the load radiograph screen
	 * 
	 * @param request
	 * @throws Exception
	 */
	public void loadRadiographAttribute(HttpServletRequest request) throws Exception {
		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		ArrayList alPeriodList = new ArrayList();
		ArrayList alPatientList = new ArrayList();
		ArrayList alTxnType = new ArrayList();
		HashMap hmTemp = new HashMap();
		String strType = "";
		String strStudyId = (String) request.getSession().getAttribute("strSessStudyId");
		String strSiteId = (String) request.getSession().getAttribute("strSessSiteId");

		hmParam.put("STUDY_ID", strStudyId);
		hmParam.put("SITE_ID", strSiteId);
		hmReturn = gmClinicalBean.loadFormDataEntryLists(hmParam);
		alPeriodList = GmCommonClass.parseNullArrayList((ArrayList) (hmReturn.get("PERIODLIST")));
		alPatientList = GmCommonClass.parseNullArrayList((ArrayList) (hmReturn.get("PATIENTLIST")));
		alTxnType = GmCommonClass.parseNullArrayList((ArrayList) (GmCommonClass.getCodeList("PTREC")));
		request.setAttribute("AlPeriod", alPeriodList);
		request.setAttribute("AlPatient", alPatientList);
		request.setAttribute("AlTyp", alTxnType);
		for (Iterator iter = alTxnType.iterator(); iter.hasNext();) {
			hmTemp = (HashMap) iter.next();
			strType = ((String) hmTemp.get("CODENM")).replaceAll("/", "").toUpperCase();
			request.setAttribute(strType + "_list", GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(strType, "")));
		}

	}

	/**
	 * loadPatientRecord - This method call bean method to retrieve all patient
	 * record for selected criteria on raidograph report page.
	 * 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList loadPatientRecord(HttpServletRequest request) throws AppError, Exception {

		GmClinicalBean gmClinicalBean = new GmClinicalBean();
		loadRadiographAttribute(request);

		HashMap hmParam = new HashMap();

		String strTimePointId = GmCommonClass.parseNull((String) request.getParameter("hTimePointId"));
		String strPatientId = GmCommonClass.parseNull((String) request.getParameter("hPatientId"));
		String strTypeId = GmCommonClass.parseNull((String) request.getParameter("hTypeId"));
		String strViewId = GmCommonClass.parseNull((String) request.getParameter("hViewId"));
		String strPatientRecordId = GmCommonClass.parseNull((String) request.getParameter("hPatientRecordId"));

		hmParam.put("TIME_POINT_ID", strTimePointId.equalsIgnoreCase("0") ? "" : strTimePointId);
		hmParam.put("PATIENT_ID", strPatientId.equalsIgnoreCase("0") ? "" : strPatientId);
		hmParam.put("TYPE_ID", strTypeId.equalsIgnoreCase("0") ? "" : strTypeId);
		hmParam.put("VIEW_ID", strViewId.equalsIgnoreCase("0") ? "" : strViewId);
		hmParam.put("PAT_REC_ID", strPatientRecordId.equalsIgnoreCase("0") ? "" : strPatientRecordId);
		hmParam.put("STUDYID",GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessStudyId")));
		hmParam.put("SITEID",GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSiteId")));
		log.debug("search parameters==========" + hmParam);

		ArrayList alPatientRecords = gmClinicalBean.loadPatientRecord(hmParam);

		request.setAttribute("strTimePointId", strTimePointId);
		request.setAttribute("strPatientId", strPatientId);
		request.setAttribute("strTypeId", strTypeId);
		request.setAttribute("strViewId", strViewId);
		request.setAttribute("strPatientRecordId", strPatientRecordId);

		return alPatientRecords;
	}

}
