/*****************************************************************************
 * File			 : GmClinicDashBoardServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.clinical.servlets;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.clinical.beans.GmClinicalBean;
import com.globus.clinical.beans.GmStudyBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.logon.beans.GmLogonBean;

public class GmClinicDashBoardServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
  		Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
		HttpSession session = request.getSession(false);
        HttpSession httpSession = request.getSession(false);
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmStudyBean gmStudy = new GmStudyBean();
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		//String strAcctPath = GmCommonClass.getString("GMCLINIC");
        String strCraId = GmCommonClass.parseNull((String)request.getParameter("studyListId"));        
        String strDispatchTo = "/GmDashBoardHome.jsp";
        String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
        String strAccessLevel = strCraId.equals("") ? GmCommonClass.parseNull((String)httpSession.getAttribute("strSessAccLvl")) : "";        
        strCraId = strAccessLevel.equals("3") ? strCraId : GmCommonClass.parseNull((String)httpSession.getAttribute("strSessUserId"));                
        strCraId = strCraId.equals("0") ? "" : strCraId;
        
        if(strAccessLevel.equals("1"))
        {
        	strDispatchTo = "/common/Gm_Blank.jsp";
        	strOperPath = GmCommonClass.getString("GMCOMMON");
        }
        request.setAttribute("studyListId",strCraId);
		

		//String strDeptId = 	(String)session.getAttribute("strSessDeptId");
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			String strAction = request.getParameter("hAction");
			strAction = (strAction == null)?"Load":strAction;
			//String strStudyID = GmCommonClass.parseNull(request.getParameter("Cbo_StudyID"));		
			
            request.setAttribute("CRAS",gmCommonBean.loadEmployeeList("R", "2007"));
            request.setAttribute("CRSTUDYLIST",gmStudy.reportStudyNameList());
            
            strAction= "CLINICALDASHBOARD";
            
			request.setAttribute("hAction", strAction);
			dispatch(strOperPath.concat(strDispatchTo),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of  GmClinicDashBoardServlet