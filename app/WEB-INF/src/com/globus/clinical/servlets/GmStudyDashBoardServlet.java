/*****************************************************************************
 * File			 : GmPatientMasterServlet 
 * Desc		 	 : This Servlet is used to handle the patient master information 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.clinical.servlets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.clinical.beans.GmPatientBean;
import com.globus.clinical.beans.GmStudyBean;

public class GmStudyDashBoardServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strClinicalPath = GmCommonClass.getString("GMCLINIC");

		String strDispatchTo = "";
		String strStudyID = null;
		String strPatientID = "";
		String strPKeyID = "";
		
		GmStudyBean	gmStudy = new GmStudyBean();
		ArrayList alReturn = new ArrayList();

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			String strAction = request.getParameter("hAction");
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("hAction");
			}
			strAction = (strAction == null)?"Load":strAction;
			
			
			strStudyID = request.getParameter("hStudyID"); 
			strPatientID = GmCommonClass.parseNull(request.getParameter("hPatientID"));
			strPKeyID	= GmCommonClass.parseNull(request.getParameter("hPKeyID")) ;

			//	To load the default value and Combo Box value information
			alReturn = gmStudy.reportStudyList(strPatientID, strStudyID);
			request.setAttribute("hStudyID",strStudyID);
			request.setAttribute("hPatientID",strPatientID);
			request.setAttribute("hPKeyID", GmCommonClass.parseNull(request.getParameter("hPKeyID")) );
			request.setAttribute("hResult",alReturn);
			
			dispatch(strClinicalPath.concat("/GmStudyDashBoard.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmSalesRepServlet