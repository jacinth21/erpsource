package com.globus.clinical.forms;

import java.util.ArrayList;
import com.globus.common.beans.GmCalenderOperations;

public class GmCommonClinicalRptForm extends GmStudyFilterForm
{
    private String fromMonth = "";
    private String toMonth = "";
    private String fromYear = "";
    private String toYear = "";    
    private ArrayList alMonthList = new ArrayList();
    private ArrayList alYearList = new ArrayList();

    public String getFromDate()
    {
        return GmCalenderOperations.formDateString(getFromMonth(),getFromYear(), GmCalenderOperations.FIRSTDAY);
    }
    
    public String getToDate()
    {
        return GmCalenderOperations.formDateString(getToMonth(),getToYear(), GmCalenderOperations.LASTDAY);
    }
    public ArrayList getAlMonthList()
    {
        return alMonthList;
    }
    public void setAlMonthList(ArrayList alMonthList)
    {
        this.alMonthList = alMonthList;
    }
    public ArrayList getAlYearList()
    {
        return alYearList;
    }
    public void setAlYearList(ArrayList alYearList)
    {
        this.alYearList = alYearList;
    }

    /**
     * @return Returns the fromMonth.
     */
    public String getFromMonth()
    {
        return fromMonth;
    }

    /**
     * @param fromMonth The fromMonth to set.
     */
    public void setFromMonth(String fromMonth)
    {
        this.fromMonth = fromMonth;
    }

    /**
     * @return Returns the fromYear.
     */
    public String getFromYear()
    {
        return fromYear;
    }

    /**
     * @param fromYear The fromYear to set.
     */
    public void setFromYear(String fromYear)
    {
        this.fromYear = fromYear;
    }

    /**
     * @return Returns the toMonth.
     */
    public String getToMonth()
    {
        return toMonth;
    }

    /**
     * @param toMonth The toMonth to set.
     */
    public void setToMonth(String toMonth)
    {
        this.toMonth = toMonth;
    }

    /**
     * @return Returns the toYear.
     */
    public String getToYear()
    {
        return toYear;
    }

    /**
     * @param toYear The toYear to set.
     */
    public void setToYear(String toYear)
    {
        this.toYear = toYear;
    }
    
  }
