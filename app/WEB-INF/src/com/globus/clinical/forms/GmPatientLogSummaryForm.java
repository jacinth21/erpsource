package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;

import com.globus.common.forms.GmCommonForm;

public class GmPatientLogSummaryForm extends GmStudyFilterForm
{
   
   private String patientLId="";
   private String patientId="";
   private String studyPeriod="";
   private String studyForm="";
   private String logDetail="";
   private String strOpt="";
   private String logFrom = "";
   private String logTo = "";
   
   private ArrayList alPatientId = new ArrayList();
   private ArrayList alStudyPeriod = new ArrayList();
   private ArrayList alFormList = new ArrayList();
   private ArrayList alLogList = new ArrayList();
/**
 * @return the alLogList
 */
public ArrayList getAlLogList() {
	return alLogList;
}
/**
 * @param alLogList the alLogList to set
 */
public void setAlLogList(ArrayList alLogList) {
	this.alLogList = alLogList;
}
/**
 * @return the patientLogId
 */
public String getPatientLId() {
	return patientLId;
}
/**
 * @param patientLogId the patientLogId to set
 */
public void setPatientLId(String patientLId) {
	this.patientLId = patientLId;
}
/**
 * @return the patiendId
 */
public String getPatientId() {
	return patientId;
}
/**
 * @param patiendId the patiendId to set
 */
public void setPatientId(String patientId) {
	this.patientId = patientId;
}
/**
 * @return the studyPeriod
 */
public String getStudyPeriod() {
	return studyPeriod;
}
/**
 * @param studyPeriod the studyPeriod to set
 */
public void setStudyPeriod(String studyPeriod) {
	this.studyPeriod = studyPeriod;
}
/**
 * @return the logDetail
 */
public String getLogDetail() {
	return logDetail;
}
/**
 * @param logDetail the logDetail to set
 */
public void setLogDetail(String logDetail) {
	this.logDetail = logDetail;
}
/**
 * @return the alPatiendId
 */
public ArrayList getAlPatientId() {
	return alPatientId;
}
/**
 * @param alPatiendId the alPatiendId to set
 */
public void setAlPatientId(ArrayList alPatientId) {
	this.alPatientId = alPatientId;
}
/**
 * @return the alStudyPeriod
 */
public ArrayList getAlStudyPeriod() {
	return alStudyPeriod;
}
/**
 * @param alStudyPeriod the alStudyPeriod to set
 */
public void setAlStudyPeriod(ArrayList alStudyPeriod) {
	this.alStudyPeriod = alStudyPeriod;
}
/**
 * @return the strOpt
 */
public String getStrOpt() {
	return strOpt;
}
/**
 * @param strOpt the strOpt to set
 */
public void setStrOpt(String strOpt) {
	this.strOpt = strOpt;
}
/**
 * @return the studyForm
 */
public String getStudyForm() {
	return studyForm;
}
/**
 * @param studyForm the studyForm to set
 */
public void setStudyForm(String studyForm) {
	this.studyForm = studyForm;
}
/**
 * @return the alFormList
 */
public ArrayList getAlFormList() {
	return alFormList;
}
/**
 * @param alFormList the alFormList to set
 */
public void setAlFormList(ArrayList alFormList) {
	this.alFormList = alFormList;
}
/**
 * @return the logFrom
 */
public String getLogFrom() {
	return logFrom;
}
/**
 * @param logFrom the logFrom to set
 */
public void setLogFrom(String logFrom) {
	this.logFrom = logFrom;
}
/**
 * @return the logTo
 */
public String getLogTo() {
	return logTo;
}
/**
 * @param logTo the logTo to set
 */
public void setLogTo(String logTo) {
	this.logTo = logTo;
}
}
