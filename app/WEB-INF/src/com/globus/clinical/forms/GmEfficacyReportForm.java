package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.List;

public class GmEfficacyReportForm extends GmStudyFilterForm
{

    private String treatmentId = "0";
    private String typeId = "";
    
    ArrayList alTreatment = new ArrayList();
    ArrayList alType = new ArrayList();
    
    /**
     * @return Returns the alTreatment.
     */
    public ArrayList getAlTreatment()
    {
        return alTreatment;
    }
    /**
     * @param alTreatment The alTreatment to set.
     */
    public void setAlTreatment(ArrayList alTreatment)
    {
        this.alTreatment = alTreatment;
    }
   
    /**
     * @return Returns the treatmentId.
     */
    public String getTreatmentId()
    {
        return treatmentId;
    }
    /**
     * @param treatmentId The treatmentId to set.
     */
    public void setTreatmentId(String treatmentId)
    {
        this.treatmentId = treatmentId;
    }
    /**
     * @return Returns the alType.
     */
    public ArrayList getAlType()
    {
        return alType;
    }
    /**
     * @param alType The alType to set.
     */
    public void setAlType(ArrayList alType)
    {
        this.alType = alType;
    }
    /**
     * @return Returns the typeId.
     */
    public String getTypeId()
    {
        return typeId;
    }
    /**
     * @param typeId The typeId to set.
     */
    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }
   
    
}
