/**
 * 
 */
package com.globus.clinical.forms;

import java.util.ArrayList;

/**
 * @author Brinal
 *
 */
public class GmDynamicDataLoadForm extends GmStudyFilterForm
{
    private String dynamicColumn= "";
    private String formList = "";
    
    private boolean surgeryDate;
    private boolean examDate;
    private boolean surgeon;
    private boolean treatment;
    
    private ArrayList alDynamicColumns = new ArrayList();    
    private ArrayList alFormList = new ArrayList();
    
    private ArrayList alTimePeriodList = new ArrayList();
    private ArrayList alColummList = new ArrayList();
    private ArrayList alQuestionList = new ArrayList();
    private ArrayList alAnswersList = new ArrayList();
    
    private String selectAllColumns = "";
    private String selectAllTimePeriods="";
    private String selectAllQuestions="";
    private String selectAllAnswers="";
    
    private String[] checkColumns = new String [1];
    private String[] checkTimePeriods = new String[1];
    private String[] checkQuestions = new String[1];
    private String[] checkAnswers = new String[1];
    
   
    public String[] getCheckColumns()
    {
        return checkColumns;
    }
    public void setCheckColumns(String[] checkColumns)
    {
        this.checkColumns = checkColumns;
    }
    public String[] getCheckTimePeriods()
    {
        return checkTimePeriods;
    }
    public void setCheckTimePeriods(String[] checkTimePeriods)
    {
        this.checkTimePeriods = checkTimePeriods;
    }
    public String getSelectAllColumns()
    {
        return selectAllColumns;
    }
    public void setSelectAllColumns(String selectAllColumns)
    {
        this.selectAllColumns = selectAllColumns;
    }
    public String getSelectAllTimePeriods()
    {
        return selectAllTimePeriods;
    }
    public void setSelectAllTimePeriods(String selectAllTimePeriods)
    {
        this.selectAllTimePeriods = selectAllTimePeriods;
    }
    public ArrayList getAlColummList()
    {
        return alColummList;
    }    
    public void setAlColummList(ArrayList alColummList)
    {
        this.alColummList = alColummList;
    }
    public ArrayList getAlTimePeriodList()
    {
        return alTimePeriodList;
    }
    public void setAlTimePeriodList(ArrayList alTimePeriodList)
    {
        this.alTimePeriodList = alTimePeriodList;
    }
    public ArrayList getAlDynamicColumns()
    {
        return alDynamicColumns;
    }
    public void setAlDynamicColumns(ArrayList alDynamicColumns)
    {
        this.alDynamicColumns = alDynamicColumns;
    }
    public ArrayList getAlFormList()
    {
        return alFormList;
    }
    public void setAlFormList(ArrayList alFormList)
    {
        this.alFormList = alFormList;
    }
    public String getDynamicColumn()
    {
        return dynamicColumn;
    }
    public void setDynamicColumn(String dynamicColumn)
    {
        this.dynamicColumn = dynamicColumn;
    }
    public String getFormList()
    {
        return formList;
    }
    public void setFormList(String formList)
    {
        this.formList = formList;
    }
	public boolean isSurgeryDate() {
		return surgeryDate;
	}
	public void setSurgeryDate(boolean surgeryDate) {
		this.surgeryDate = surgeryDate;
	}
	public boolean isSurgeon() {
		return surgeon;
	}
	public void setSurgeon(boolean surgeon) {
		this.surgeon = surgeon;
	}
	public boolean isTreatment() {
		return treatment;
	}
	public void setTreatment(boolean treatment) {
		this.treatment = treatment;
	}
	public ArrayList getAlQuestionList() {
		return alQuestionList;
	}
	public void setAlQuestionList(ArrayList alQuestionList) {
		this.alQuestionList = alQuestionList;
	}
	public ArrayList getAlAnswersList() {
		return alAnswersList;
	}
	public void setAlAnswersList(ArrayList alAnswersList) {
		this.alAnswersList = alAnswersList;
	}
	public String[] getCheckQuestions() {
		return checkQuestions;
	}
	public void setCheckQuestions(String[] checkQuestions) {
		this.checkQuestions = checkQuestions;
	}
	public String[] getCheckAnswers() {
		return checkAnswers;
	}
	public void setCheckAnswers(String[] checkAnswers) {
		this.checkAnswers = checkAnswers;
	}
	public String getSelectAllQuestions() {
		return selectAllQuestions;
	}
	public void setSelectAllQuestions(String selectAllQuestions) {
		this.selectAllQuestions = selectAllQuestions;
	}
	public String getSelectAllAnswers() {
		return selectAllAnswers;
	}
	public void setSelectAllAnswers(String selectAllAnswers) {
		this.selectAllAnswers = selectAllAnswers;
	}
	public boolean isExamDate() {
		return examDate;
	}
	public void setExamDate(boolean examDate) {
		this.examDate = examDate;
	}
}
