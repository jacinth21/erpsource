package com.globus.clinical.forms;

import com.globus.common.forms.GmCommonForm;

public class GmClinicalStudyForm extends GmCommonForm 
{
	private String EMPTY_STRING = "";
    private String gridXmlData = EMPTY_STRING;
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}    
}
