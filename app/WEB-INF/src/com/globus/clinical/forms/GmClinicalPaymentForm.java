package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author hparikh
 */
public class GmClinicalPaymentForm extends GmStudyFilterForm {

  private String msg = "";
  private ArrayList alPatientList = new ArrayList();
  private ArrayList alPeriodList = new ArrayList();
  private ArrayList alStatusList = new ArrayList();
  private ArrayList alPaymetReqList = new ArrayList();
  private ArrayList alPayableList = new ArrayList();
  private ArrayList alSurgonList = new ArrayList();
  private ArrayList alPaymentReasonList = new ArrayList();
  private HashMap hmPrintParam = new HashMap();
  private ArrayList alPrintResult = new ArrayList();


  String paymentRequestId = "";
  String requestTypeId = "";
  String paymenttoId = "";
  String comments = "";
  String statusId = "";
  String paymentDate = "";
  String checkNumber = "";
  String payableCodeId = "";
  String payableCodeName = "";
  String gridData = "";
  String paymentDetailData = "";
  String payable = "";
  String tinNumber = "";
  String tinReason = "";
  String accessFlag="";


  public String getAccessFlag() {
	return accessFlag;
}

public void setAccessFlag(String accessFlag) {
	this.accessFlag = accessFlag;
}

/**
   * @return the tinReason
   */
  public String getTinReason() {
    return tinReason;
  }

  /**
   * @param tinReason the tinReason to set
   */
  public void setTinReason(String tinReason) {
    this.tinReason = tinReason;
  }

  /**
   * @return the tinNumber
   */
  public String getTinNumber() {
    return tinNumber;
  }

  /**
   * @param tinNumber the tinNumber to set
   */
  public void setTinNumber(String tinNumber) {
    this.tinNumber = tinNumber;
  }

  /**
   * @return Returns the msg.
   */
  public String getMsg() {
    return msg;
  }

  /**
   * @param msg The msg to set.
   */
  public void setMsg(String msg) {
    this.msg = msg;
  }

  @Override
  public ArrayList getAlPatientList() {
    return alPatientList;
  }

  @Override
  public void setAlPatientList(ArrayList alPatientList) {
    this.alPatientList = alPatientList;
  }

  @Override
  public ArrayList getAlPeriodList() {
    return alPeriodList;
  }

  @Override
  public void setAlPeriodList(ArrayList alPeriodList) {
    this.alPeriodList = alPeriodList;
  }

  public ArrayList getAlStatusList() {
    return alStatusList;
  }

  public void setAlStatusList(ArrayList alStatusList) {
    this.alStatusList = alStatusList;
  }

  public ArrayList getAlPaymetReqList() {
    return alPaymetReqList;
  }

  public void setAlPaymetReqList(ArrayList alPaymetReqList) {
    this.alPaymetReqList = alPaymetReqList;
  }

  public String getPaymentRequestId() {
    return paymentRequestId;
  }

  public void setPaymentRequestId(String paymentRequestId) {
    this.paymentRequestId = paymentRequestId;
  }

  public ArrayList getAlPayableList() {
    return alPayableList;
  }

  public void setAlPayableList(ArrayList alPayableList) {
    this.alPayableList = alPayableList;
  }

  public String getRequestTypeId() {
    return requestTypeId;
  }

  public void setRequestTypeId(String requestTypeId) {
    this.requestTypeId = requestTypeId;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getStatusId() {
    return statusId;
  }

  public void setStatusId(String statusId) {
    this.statusId = statusId;
  }

  public String getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(String paymentDate) {
    this.paymentDate = paymentDate;
  }

  public String getCheckNumber() {
    return checkNumber;
  }

  public void setCheckNumber(String checkNumber) {
    this.checkNumber = checkNumber;
  }

  public ArrayList getAlSurgonList() {
    return alSurgonList;
  }

  public void setAlSurgonList(ArrayList alSurgonList) {
    this.alSurgonList = alSurgonList;
  }

  public String getPayableCodeId() {
    return payableCodeId;
  }

  public void setPayableCodeId(String payableCodeId) {
    this.payableCodeId = payableCodeId;
  }

  public String getPayableCodeName() {
    return payableCodeName;
  }

  public void setPayableCodeName(String payableCodeName) {
    this.payableCodeName = payableCodeName;
  }

  public String getGridData() {
    return gridData;
  }

  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  public ArrayList getAlPaymentReasonList() {
    return alPaymentReasonList;
  }

  public void setAlPaymentReasonList(ArrayList alPaymentReasonList) {
    this.alPaymentReasonList = alPaymentReasonList;
  }

  public String getPaymentDetailData() {
    return paymentDetailData;
  }

  public void setPaymentDetailData(String paymentDetailData) {
    this.paymentDetailData = paymentDetailData;
  }

  public String getPaymenttoId() {
    return paymenttoId;
  }

  public void setPaymenttoId(String paymenttoId) {
    this.paymenttoId = paymenttoId;
  }

  public String getPayable() {
    return payable;
  }

  public void setPayable(String payable) {
    this.payable = payable;
  }

  public HashMap getHmPrintParam() {
    return hmPrintParam;
  }

  public void setHmPrintParam(HashMap hmPrintParam) {
    this.hmPrintParam = hmPrintParam;
  }

  public ArrayList getAlPrintResult() {
    return alPrintResult;
  }

  public void setAlPrintResult(ArrayList alPrintResult) {
    this.alPrintResult = alPrintResult;
  }

}
