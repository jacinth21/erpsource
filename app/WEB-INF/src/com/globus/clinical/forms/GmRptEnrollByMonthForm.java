package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GmRptEnrollByMonthForm extends GmCommonClinicalRptForm
{
    private HashMap hmRptEnrollByMonth = new HashMap();
    private List rdRptEnrollByTreatment = new ArrayList();

    public List getRdRptEnrollByTreatment()
    {
        return rdRptEnrollByTreatment;
    }
    public void setRdRptEnrollByTreatment(List rdRptEnrollByTreatment)
    {
        this.rdRptEnrollByTreatment = rdRptEnrollByTreatment;
    }
    public HashMap getHmRptEnrollByMonth()
    {
        return hmRptEnrollByMonth;
    }
    public void setHmRptEnrollByMonth(HashMap hmRptEnrollByMonth)
    {
        this.hmRptEnrollByMonth = hmRptEnrollByMonth;
    }
 
}
