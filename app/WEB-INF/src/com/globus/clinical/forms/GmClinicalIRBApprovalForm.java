package com.globus.clinical.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;

public class GmClinicalIRBApprovalForm extends GmCancelForm {

	private String    studyID      = "";
	private String    studySiteID      = "";
	private ArrayList alSiteList    = new ArrayList();
	private String    approvalType      = "";
	private ArrayList alApprovalTypeList    = new ArrayList();
	private String    approvalReason      = "";
	private ArrayList alApprovalReasonList    = new ArrayList();
	private String    statusName      = "";
//	private String    status      = "";
//	private ArrayList alApprovalStatusList    = new ArrayList();
	private String approvalDate="";
	private String expiryDate="";
	private String irbid="";
	private String gridData=""; 
	private String lockedStudyDisable = "";
	
	/**
	 * @return the lockedStudyDisable
	 */
	public String getLockedStudyDisable() {
		return lockedStudyDisable;
	}
	/**
	 * @param lockedStudyDisable the lockedStudyDisable to set
	 */
	public void setLockedStudyDisable(String lockedStudyDisable) {
		this.lockedStudyDisable = lockedStudyDisable;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the irbid
	 */
	public String getIrbid() {
		return irbid;
	}
	/**
	 * @param irbid the irbid to set
	 */
	public void setIrbid(String irbid) {
		this.irbid = irbid;
	}
	/**
	 * @return the siteList
	 */
	public String getStudySiteID() {
		return studySiteID;
	}
	/**
	 * @param siteList the siteList to set
	 */
	public void setStudySiteID(String studySiteID) {
		this.studySiteID = studySiteID;
	}
	/**
	 * @return the alSiteList
	 */
	public ArrayList getAlSiteList() {
		return alSiteList;
	}
	/**
	 * @param alSiteList the alSiteList to set
	 */
	public void setAlSiteList(ArrayList alSiteList) {
		this.alSiteList = alSiteList;
	}
	/**
	 * @return the approvalType
	 */
	public String getApprovalType() {
		return approvalType;
	}
	/**
	 * @param approvalType the approvalType to set
	 */
	public void setApprovalType(String approvalType) {
		this.approvalType = approvalType;
	}
	/**
	 * @return the alApprovalTypeList
	 */
	public ArrayList getAlApprovalTypeList() {
		return alApprovalTypeList;
	}
	/**
	 * @param alApprovalTypeList the alApprovalTypeList to set
	 */
	public void setAlApprovalTypeList(ArrayList alApprovalTypeList) {
		this.alApprovalTypeList = alApprovalTypeList;
	}
	/**
	 * @return the approvalReason
	 */
	public String getApprovalReason() {
		return approvalReason;
	}
	/**
	 * @param approvalReason the approvalReason to set
	 */
	public void setApprovalReason(String approvalReason) {
		this.approvalReason = approvalReason;
	}
	/**
	 * @return the alApprovalReasonList
	 */
	public ArrayList getAlApprovalReasonList() {
		return alApprovalReasonList;
	}
	/**
	 * @param alApprovalReasonList the alApprovalReasonList to set
	 */
	public void setAlApprovalReasonList(ArrayList alApprovalReasonList) {
		this.alApprovalReasonList = alApprovalReasonList;
	}
	/**
	 * @return the status
	 */
//	public String getStatus() {
//		return status;
//	}
//	/**
//	 * @param status the status to set
//	 */
//	public void setStatus(String status) {
//		this.status = status;
//	}
//	/**
//	 * @return the alStatusList
//	 */
//	public ArrayList getAlApprovalStatusList() {
//		return alApprovalStatusList;
//	}
//	/**
//	 * @param alStatusList the alStatusList to set
//	 */
//	public void setAlApprovalStatusList(ArrayList alApprovalStatusList) {
//		this.alApprovalStatusList = alApprovalStatusList;
//	}
	/**
	 * @return the studyID
	 */
	public String getStudyID() {
		return studyID;
	}
	/**
	 * @param studyID the studyID to set
	 */
	public void setStudyID(String studyID) {
		this.studyID = studyID;
	}

	/**
	 * @return the approvalDate
	 */
	public String getApprovalDate() {
		return approvalDate;
	}
	/**
	 * @param approvalDate the approvalDate to set
	 */
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}
	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}
	/**
	 * @param statusName the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}	
}
