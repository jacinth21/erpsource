package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.List;

public class GmRptImplantInfoForm extends GmStudyFilterForm
{       
    
    private ArrayList alPartTypes = new ArrayList();
    private String cbo_PartType = "";
    
    public ArrayList getAlPartTypes()
    {
        return alPartTypes;
    }
    public void setAlPartTypes(ArrayList alPartTypes)
    {
        this.alPartTypes = alPartTypes;
    }
    public String getCbo_PartType()
    {
        return cbo_PartType;
    }
    public void setCbo_PartType(String cbo_PartType)
    {
        this.cbo_PartType = cbo_PartType;
    }

    

}
