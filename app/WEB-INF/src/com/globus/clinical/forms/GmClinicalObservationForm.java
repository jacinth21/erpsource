/**
 * 
 */
package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

/**
 * @author hparikh
 */
public class GmClinicalObservationForm extends GmStudyFilterForm
{
    
    private String    msg        = "";    
    private ArrayList alPatientList = new ArrayList();
    private ArrayList alPeriodList = new ArrayList();
    private ArrayList alActionReqList = new ArrayList();
    private ArrayList alResponisbleByList = new ArrayList();
    private ArrayList alStatusList = new ArrayList();
    private String patientId="";
    private String periodId="";
    private String issueDescription="";
    private String actionRequired="";
    private String responisbleBy="";
    private String targetResDate="";
    private String resolution="";
    private String actualResDate="";
    private String status="";
    
    private String visitDate="";
    private String purpose="";
    private String surgeonName="";
    private String siteName="";
    private String studyName="";
    
    private String hsiteMonitorId = "";
    private String hstudyId = "";
    private String hstudySiteId = "";
    private String hpurpose = "";
    private String hstatus = "";
    private String hfromDate = "";
    private String htoDate = "";
    private String hcraId = "";
    
    private String observationId="";
    private String statusName="";

    /**
     * @return Returns the msg.
     */
    public String getMsg()
    {
        return msg;
    }

    /**
     * @param msg
     *            The msg to set.
     */
    public void setMsg(String msg)
    {
        this.msg = msg;
    }  
    
    /**
     * @return Returns the alPatientList.
     */
    public ArrayList getAlPatientList()
    {
        return alPatientList;
    }

    /**
     * @param alPatientList
     *            The alPatientList to set.
     */
    public void setAlPatientList(ArrayList alPatientList)
    {
        this.alPatientList = alPatientList;
    }   
    
    
    /**
     * @return Returns the alPeriodList.
     */
    public ArrayList getAlPeriodList()
    {
        return alPeriodList;
    }

    /**
     * @param alPeriodList
     *            The alPeriodList to set.
     */
    public void setAlPeriodList(ArrayList alPeriodList)
    {
        this.alPeriodList = alPeriodList;
    }  
    
    /**
     * @return Returns the alActionReqList.
     */
    public ArrayList getAlActionReqList()
    {
        return alActionReqList;
    }
    /**
     * @param alActionReqList
     *            The alActionReqList to set.
     */
    public void setAlActionReqList(ArrayList alActionReqList)
    {
        this.alActionReqList = alActionReqList;
    }
    
    /**
     * @return Returns the alResponisbleByList.
     */
    public ArrayList getAlResponisbleByList()
    {
        return alResponisbleByList;
    }
    /**
     * @param alResponisbleByList
     *            The alResponisbleByList to set.
     */
    public void setAlResponisbleByList(ArrayList alResponisbleByList)
    {
        this.alResponisbleByList = alResponisbleByList;
    } 

    /**
     * @return Returns the alStatusList.
     */
    public ArrayList getAlStatusList()
    {
        return alStatusList;
    }
    /**
     * @param alStatusList
     *            The alStatusList to set.
     */
    public void setAlStatusList(ArrayList alStatusList)
    {
        this.alStatusList = alStatusList;
    }   
    
    /**
     * @return Returns the patientId.
     */
    public String getPatientId()
    {
        return patientId;
    }

    /**
     * @param patientId
     *            The patientId to set.
     */
    public void setPatientId(String patientId)
    {
        this.patientId = patientId;
    }   
    
    /**
     * @return Returns the periodId.
     */
    public String getPeriodId()
    {
        return periodId;
    }

    /**
     * @param periodId
     *            The periodId to set.
     */
    public void setPeriodId(String periodId)
    {
        this.periodId = periodId;
    }

    /**
     * @return Returns the issueDescription.
     */
	public String getIssueDescription() {
		return issueDescription;
	}
	/**
     * @param issueDescription
     *            The issueDescription to set.
     */
	public void setIssueDescription(String issueDescription) {
		this.issueDescription = issueDescription;
	}
	
	/**
     * @return Returns the actionRequired.
     */
	public String getActionRequired() {
		return actionRequired;
	}
	/**
     * @param actionRequired
     *            The actionRequired to set.
     */
	public void setActionRequired(String actionRequired) {
		this.actionRequired = actionRequired;
	}	
	/**
     * @return Returns the responisbleBy.
     */
	public String getResponisbleBy() {
		return responisbleBy;
	}
	/**
     * @param responisbleBy
     *            The responisbleBy to set.
     */
	public void setResponisbleBy(String responisbleBy) {
		this.responisbleBy = responisbleBy;
	}
	
	/**
     * @return Returns the targetResDate.
     */
	public String getTargetResDate() {
		return targetResDate;
	}
	/**
     * @param targetResDate
     *            The targetResDate to set.
     */
	public void setTargetResDate(String targetResDate) {
		this.targetResDate = targetResDate;
	}
	/**
     * @return Returns the resolution.
     */
	public String getResolution() {
		return resolution;
	}
	/**
     * @param resolution
     *            The resolution to set.
     */
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	/**
     * @return Returns the actualResDate.
     */
	public String getActualResDate() {
		return actualResDate;
	}
	/**
     * @param actualResDate
     *            The actualResDate to set.
     */
	public void setActualResDate(String actualResDate) {
		this.actualResDate = actualResDate;
	}
	/**
     * @return Returns the status.
     */
	public String getStatus() {
		return status;
	}
	/**
     * @param status
     *            The status to set.
     */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
     * @return Returns the visitDate.
     */
	public String getVisitDate() {
		return visitDate;
	}
	
	/**
     * @param visitDate
     *            The visitDate to set.
     */

	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}
	/**
     * @return Returns the purpose.
     */
	public String getPurpose() {
		return purpose;
	}
	
	/**
     * @param purpose
     *            The purpose to set.
     */

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	/**
     * @return Returns the surgeonName.
     */
	public String getSurgeonName() {
		return surgeonName;
	}
	/**
     * @param surgeonName
     *            The surgeonName to set.
     */
	public void setSurgeonName(String surgeonName) {
		this.surgeonName = surgeonName;
	}
	/**
     * @return Returns the siteName.
     */
	public String getSiteName() {
		return siteName;
	}
	/**
     * @param siteName
     *            The siteName to set.
     */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	/**
     * @return Returns the studyName.
     */
	public String getStudyName() {
		return studyName;
	}
	/**
     * @param studyName
     *            The studyName to set.
     */
	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}
	/**
     * @return Returns the hsiteMonitorId.
     */
	public String getHsiteMonitorId() {
		return hsiteMonitorId;
	}
	/**
     * @param hsiteMonitorId
     *            The hsiteMonitorId to set.
     */
	public void setHsiteMonitorId(String hsiteMonitorId) {
		this.hsiteMonitorId = hsiteMonitorId;
	}
	/**
     * @return Returns the hstudyId.
     */
	public String getHstudyId() {
		return hstudyId;
	}
	/**
     * @param hstudyId
     *            The hstudyId to set.
     */
	public void setHstudyId(String hstudyId) {
		this.hstudyId = hstudyId;
	}
	/**
     * @return Returns the hstudySiteId.
     */
	public String getHstudySiteId() {
		return hstudySiteId;
	}
	/**
     * @param hstudySiteId
     *            The hstudySiteId to set.
     */
	public void setHstudySiteId(String hstudySiteId) {
		this.hstudySiteId = hstudySiteId;
	}
	/**
     * @return Returns the hpurpose.
     */
	public String getHpurpose() {
		return hpurpose;
	}
	/**
     * @param hpurpose
     *            The hpurpose to set.
     */
	public void setHpurpose(String hpurpose) {
		this.hpurpose = hpurpose;
	}
	/**
     * @return Returns the hstatus.
     */
	public String getHstatus() {
		return hstatus;
	}
	/**
     * @param hstatus
     *            The hstatus to set.
     */
	public void setHstatus(String hstatus) {
		this.hstatus = hstatus;
	}
	/**
     * @return Returns the hfromDate.
     */
	public String getHfromDate() {
		return hfromDate;
	}
	/**
     * @param hfromDate
     *            The hfromDate to set.
     */
	public void setHfromDate(String hfromDate) {
		this.hfromDate = hfromDate;
	}
	/**
     * @return Returns the htoDate.
     */
	public String getHtoDate() {
		return htoDate;
	}
	/**
     * @param htoDate
     *            The htoDate to set.
     */
	public void setHtoDate(String htoDate) {
		this.htoDate = htoDate;
	}
	/**
     * @return Returns the hcraId.
     */
	public String getHcraId() {
		return hcraId;
	}
	/**
     * @param hcraId
     *            The hcraId to set.
     */
	public void setHcraId(String hcraId) {
		this.hcraId = hcraId;
	}
	/**
     * @return Returns the observationId.
     */
	public String getObservationId() {
		return observationId;
	}
	/**
     * @param observationId
     *            The observationId to set.
     */
	public void setObservationId(String observationId) {
		this.observationId = observationId;
	}
	/**
     * @return Returns the statusName.
     */
	public String getStatusName() {
		return statusName;
	}
	/**
     * @param statusName
     *            The statusName to set.
     */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}	    
}

