package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;

import com.globus.common.forms.GmCommonForm;

public class GmStudyFilterForm extends GmCommonForm 
{
    private String studyListId = "GPR002";
    private String studyListGrp = "ALLSDL";
    private String formId = "";
    private String craId = "";    
    private String[] checkSiteName = new String[50];
    private String selectAll = "";
    //Added for PED Score
    private String questionnairre = "";
    private String score = "";
    private String referenceId = "";
    private String patientIde = "";
    private String patientId = "";
    private String periodId = "";
    
    //Added from ImproveDetial
    private String treatmentId = "0";
    private String typeId = "";
    
    //For DHTMLX
    private String EMPTY_STRING = "";
    private String gridXmlData = EMPTY_STRING;
    private String treeSnapshotXmlData = EMPTY_STRING;
    private String treeReportXmlData = EMPTY_STRING;
    private String treeTransXmlData = EMPTY_STRING;
    private String toolbarXmlData = EMPTY_STRING;
    private String strDhtmlx = "";
    private String sessStudyId = "";
    private String sessSiteId = "";
    private String sessPatientId = "";
    private String sessPeriodId = "";
    private String sessAccLvl = "";
    private String sessStudySiteId = "";
    private String sessPatientPkey = "";
    private String sessNodeId = "";
    private String sessLastClicked = "";
    private String sessAccrd = "";
    private String sessOptLastClicked = "";
    private String sessPgToLoad = "";
    private String sessOpt = "";
    private String strTxnId = "";
    private String strFromDate = "";
    private String strToDate = "";
    private String reasonForClosing = "";
    private String ansGrpId = "";
    private String incidentStatus = "";
    private String formDataScr = "";
    private String lockedStudyDisable = "";
    
    
    
    private String trainingFor = "";    
    private String trainingReason = "";
    private String status = "";
    private String siteList = "";
    
    private String patientAnsListId = "";
    
    private HashMap hmCrossTabReport = new HashMap();
    
    private List ldtResult = new ArrayList();
    ArrayList alTreatment = new ArrayList();
    ArrayList alType = new ArrayList();
    private ArrayList alStudyList = new ArrayList();
    private ArrayList alSiteNameList = new ArrayList();
    private ArrayList alQuestionnairre = new ArrayList();
    private ArrayList alScore = new ArrayList();
    
    private ArrayList alTrainingFor = new ArrayList();
    private ArrayList alTrainingReason = new ArrayList();
    private ArrayList alStatus = new ArrayList();
    private ArrayList alCraNames = new ArrayList();
    private ArrayList alStudyFormList = new ArrayList();
    private ArrayList alPatientList = new ArrayList();
    private ArrayList alPeriodList = new ArrayList();
    private ArrayList alReasonForClosing = new ArrayList();
    private HashMap hmRules = new HashMap();
    
	public HashMap getHmRules() {
		return hmRules;
	}
	public void setHmRules(HashMap hmRules) {
		this.hmRules = hmRules;
	}
	private String strSiteIds = "";
	/**
	 * @return the strSiteIds
	 */
	public String getStrSiteIds() {
		return strSiteIds;
	}
	/**
	 * @param strSiteIds the strSiteIds to set
	 */
	public void setStrSiteIds(String strSiteIds) {
		this.strSiteIds = strSiteIds;
	}
	/**
     * @return Returns the hmCrossTabReport.
     */
    public HashMap getHmCrossTabReport()
    {
        return hmCrossTabReport;
    }
    /**
     * @param hmCrossTabReport The hmCrossTabReport to set.
     */
    public void setHmCrossTabReport(HashMap hmCrossTabReport)
    {
        this.hmCrossTabReport = hmCrossTabReport;
    }
    /**
     * @return Returns the selectAll.
     */
    public String getSelectAll()
    {
        return selectAll;
    }
    /**
     * @param selectAll The selectAll to set.
     */
    public void setSelectAll(String selectAll)
    {
        this.selectAll = selectAll;
    }
    /**
     * @return Returns the alSiteNameList.
     */
    public ArrayList getAlSiteNameList()
    {
        return alSiteNameList;
    }
    /**
     * @param alSiteNameList The alSiteNameList to set.
     */
    public void setAlSiteNameList(ArrayList alSiteNameList)
    {
        this.alSiteNameList = alSiteNameList;
    }
    /**
     * @return Returns the alStudyList.
     */
    public ArrayList getAlStudyList()
    {
        return alStudyList;
    }
    /**
     * @param alStudyList The alStudyList to set.
     */
    public void setAlStudyList(ArrayList alStudyList)
    {
       this.alStudyList = alStudyList;
    }
    /**
     * @return Returns the checkSiteName.
     */
    public String[] getCheckSiteName()
    {
        return checkSiteName;
    }
    /**
     * @param checkSiteName The checkSiteName to set.
     */
    public void setCheckSiteName(String[] checkSiteName)
    {
        this.checkSiteName = checkSiteName;
    }
    /**
     * @return Returns the studyListId.
     */
    public String getStudyListId()
    {
        return studyListId;
    }
    /**
     * @param studyListId The studyListId to set.
     */
    public void setStudyListId(String studyListId)
    {
        this.studyListId = studyListId;
    }
    /**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }
	/**
	 * @return the questionnaire
	 */
	public String getQuestionnairre() {
		return questionnairre;
	}
	/**
	 * @param questionnaire the questionnaire to set
	 */
	public void setQuestionnairre(String questionnairre) {
		this.questionnairre = questionnairre;
	}
	/**
	 * @return the score
	 */
	public String getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(String score) {
		this.score = score;
	}
	/**
	 * @return the alQuestionnaire
	 */
	public ArrayList getAlQuestionnairre() {
		return alQuestionnairre;
	}
	/**
	 * @param alQuestionnaire the alQuestionnaire to set
	 */
	public void setAlQuestionnairre(ArrayList alQuestionnairre) {
		this.alQuestionnairre = alQuestionnairre;
	}
	/**
	 * @return the alScore
	 */
	public ArrayList getAlScore() {
		return alScore;
	}
	/**
	 * @param alScore the alScore to set
	 */
	public void setAlScore(ArrayList alScore) {
		this.alScore = alScore;
	}
	/**
	 * @return the treatmentId
	 */
	public String getTreatmentId() {
		return treatmentId;
	}
	/**
	 * @param treatmentId the treatmentId to set
	 */
	public void setTreatmentId(String treatmentId) {
		this.treatmentId = treatmentId;
	}
	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the alTreatment
	 */
	public ArrayList getAlTreatment() {
		return alTreatment;
	}
	/**
	 * @param alTreatment the alTreatment to set
	 */
	public void setAlTreatment(ArrayList alTreatment) {
		this.alTreatment = alTreatment;
	}
	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}
	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}
	/**
	 * @return the studyListGrp
	 */
	public String getStudyListGrp() {
		return studyListGrp;
	}
	/**
	 * @param studyListGrp the studyListGrp to set
	 */
	public void setStudyListGrp(String studyListGrp) {
		this.studyListGrp = studyListGrp;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	
	/**
	 * @return the treeSnapshotXmlData
	 */
	public String getTreeSnapshotXmlData() {
		return treeSnapshotXmlData;
	}
	/**
	 * @param treeSnapshotXmlData the treeSnapshotXmlData to set
	 */
	public void setTreeSnapshotXmlData(String treeSnapshotXmlData) {
		this.treeSnapshotXmlData = treeSnapshotXmlData;
	}
	/**
	 * @return the treeReportXmlData
	 */
	public String getTreeReportXmlData() {
		return treeReportXmlData;
	}
	/**
	 * @param treeReportXmlData the treeReportXmlData to set
	 */
	public void setTreeReportXmlData(String treeReportXmlData) {
		this.treeReportXmlData = treeReportXmlData;
	}
	/**
	 * @return the treeTransXmlData
	 */
	public String getTreeTransXmlData() {
		return treeTransXmlData;
	}
	/**
	 * @param treeTransXmlData the treeTransXmlData to set
	 */
	public void setTreeTransXmlData(String treeTransXmlData) {
		this.treeTransXmlData = treeTransXmlData;
	}
	/**
	 * @return the toolbarXmlData
	 */
	public String getToolbarXmlData() {
		return toolbarXmlData;
	}
	/**
	 * @param toolbarXmlData the toolbarXmlData to set
	 */
	public void setToolbarXmlData(String toolbarXmlData) {
		this.toolbarXmlData = toolbarXmlData;
	}
	
	
	
	/**
	 * @return the sessStudyId
	 */
	public String getSessStudyId() {
		return sessStudyId;
	}
	/**
	 * @param sessStudyId the sessStudyId to set
	 */
	public void setSessStudyId(String sessStudyId) {
		this.sessStudyId = sessStudyId;
	}
	/**
	 * @return the sessSiteId
	 */
	public String getSessSiteId() {
		return sessSiteId;
	}
	/**
	 * @param sessSiteId the sessSiteId to set
	 */
	public void setSessSiteId(String sessSiteId) {
		this.sessSiteId = sessSiteId;
	}
	
	/**
	 * @return the strDhtmlx
	 */
	public String getStrDhtmlx() {
		return strDhtmlx;
	}
	/**
	 * @param strDhtmlx the strDhtmlx to set
	 */
	public void setStrDhtmlx(String strDhtmlx) {
		this.strDhtmlx = strDhtmlx;
	}
	
	/**
	 * @return the sessPatientId
	 */
	public String getSessPatientId() {
		return sessPatientId;
	}
	/**
	 * @param sessPatientId the sessPatientId to set
	 */
	public void setSessPatientId(String sessPatientId) {
		this.sessPatientId = sessPatientId;
	}	
	/**
	 * @return the sessPeriodId
	 */
	public String getSessPeriodId() {
		return sessPeriodId;
	}
	/**
	 * @param sessPeriodId the sessPeriodId to set
	 */
	public void setSessPeriodId(String sessPeriodId) {
		this.sessPeriodId = sessPeriodId;
	}
	/**
	 * @return the sessAccLvl
	 */
	public String getSessAccLvl() {
		return sessAccLvl;
	}
	/**
	 * @param sessAccLvl the sessAccLvl to set
	 */
	public void setSessAccLvl(String sessAccLvl) {
		this.sessAccLvl = sessAccLvl;
	}
	
	/**
	 * @return the strTxnId
	 */
	public String getStrTxnId() {
		return strTxnId;
	}
	/**
	 * @param strTxnId the strTxnId to set
	 */
	public void setStrTxnId(String strTxnId) {
		this.strTxnId = strTxnId;
	}
	
	/**
	 * @return the sessStudySiteId
	 */
	public String getSessStudySiteId() {
		return sessStudySiteId;
	}
	/**
	 * @param sessStudySiteId the sessStudySiteId to set
	 */
	public void setSessStudySiteId(String sessStudySiteId) {
		this.sessStudySiteId = sessStudySiteId;
	}
	/**
	 * @return the sessPatientPkey
	 */
	public String getSessPatientPkey() {
		return sessPatientPkey;
	}
	/**
	 * @param sessPatientPkey the sessPatientPkey to set
	 */
	public void setSessPatientPkey(String sessPatientPkey) {
		this.sessPatientPkey = sessPatientPkey;
	}
	/**
	 * @return the trainingFor
	 */
	public String getTrainingFor() {
		return trainingFor;
	}
	/**
	 * @param trainingFor the trainingFor to set
	 */
	public void setTrainingFor(String trainingFor) {
		this.trainingFor = trainingFor;
	}
	/**
	 * @return the trainingReason
	 */
	public String getTrainingReason() {
		return trainingReason;
	}
	/**
	 * @param trainingReason the trainingReason to set
	 */
	public void setTrainingReason(String trainingReason) {
		this.trainingReason = trainingReason;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the siteList
	 */
	public String getSiteList() {
		return siteList;
	}
	/**
	 * @param siteList the siteList to set
	 */
	public void setSiteList(String siteList) {
		this.siteList = siteList;
	}
	/**
	 * @return the alTrainingFor
	 */
	public ArrayList getAlTrainingFor() {
		return alTrainingFor;
	}
	/**
	 * @param alTrainingFor the alTrainingFor to set
	 */
	public void setAlTrainingFor(ArrayList alTrainingFor) {
		this.alTrainingFor = alTrainingFor;
	}
	/**
	 * @return the alTrainingReason
	 */
	public ArrayList getAlTrainingReason() {
		return alTrainingReason;
	}
	/**
	 * @param alTrainingReason the alTrainingReason to set
	 */
	public void setAlTrainingReason(ArrayList alTrainingReason) {
		this.alTrainingReason = alTrainingReason;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alCraNames
	 */
	public ArrayList getAlCraNames() {
		return alCraNames;
	}
	/**
	 * @param alCraNames the alCraNames to set
	 */
	public void setAlCraNames(ArrayList alCraNames) {
		this.alCraNames = alCraNames;
	}
	/**
	 * @return the alStudyFormList
	 */
	public ArrayList getAlStudyFormList() {
		return alStudyFormList;
	}
	/**
	 * @param alStudyFormList the alStudyFormList to set
	 */
	public void setAlStudyFormList(ArrayList alStudyFormList) {
		this.alStudyFormList = alStudyFormList;
	}
	/**
	 * @return the formId
	 */
	public String getFormId() {
		return formId;
	}
	/**
	 * @param formId the formId to set
	 */
	public void setFormId(String formId) {
		this.formId = formId;
	}
	/**
	 * @return the craId
	 */
	public String getCraId() {
		return craId;
	}
	/**
	 * @param craId the craId to set
	 */
	public void setCraId(String craId) {
		this.craId = craId;
	}
	/**
	 * @return the referenceId
	 */
	public String getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	/**
	 * @return the alPatientList
	 */
	public ArrayList getAlPatientList() {
		return alPatientList;
	}
	/**
	 * @param alPatientList the alPatientList to set
	 */
	public void setAlPatientList(ArrayList alPatientList) {
		this.alPatientList = alPatientList;
	}
	/**
	 * @return the alPeriodList
	 */
	public ArrayList getAlPeriodList() {
		return alPeriodList;
	}
	/**
	 * @param alPeriodList the alPeriodList to set
	 */
	public void setAlPeriodList(ArrayList alPeriodList) {
		this.alPeriodList = alPeriodList;
	}
	/**
	 * @return the patientIde
	 */
	public String getPatientIde() {
		return patientIde;
	}
	/**
	 * @param patientIde the patientIde to set
	 */
	public void setPatientIde(String patientIde) {
		this.patientIde = patientIde;
	}
	/**
	 * @return the periodId
	 */
	public String getPeriodId() {
		return periodId;
	}
	/**
	 * @param periodId the periodId to set
	 */
	public void setPeriodId(String periodId) {
		this.periodId = periodId;
	}
	/**
	 * @return the alReasonForClosing
	 */
	public ArrayList getAlReasonForClosing() {
		return alReasonForClosing;
	}
	/**
	 * @param alReasonForClosing the alReasonForClosing to set
	 */
	public void setAlReasonForClosing(ArrayList alReasonForClosing) {
		this.alReasonForClosing = alReasonForClosing;
	}
	/**
	 * @return the strDateTo
	 */
	public String getToDate() {
		return strToDate;
	}
	/**
	 * @param strDateTo the strDateTo to set
	 */
	public void setToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	/**
	 * @return the strDateFrom
	 */
	public String getFromDate() {
		return strFromDate;
	}
	/**
	 * @param strDateFrom the strDateFrom to set
	 */
	public void setFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}
	/**
	 * @return the reasonForClosing
	 */
	public String getReasonForClosing() {
		return reasonForClosing;
	}
	/**
	 * @param reasonForClosing the reasonForClosing to set
	 */
	public void setReasonForClosing(String reasonForClosing) {
		this.reasonForClosing = reasonForClosing;
	}
	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}
	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getAnsGrpId() {
		return ansGrpId;
	}
	public void setAnsGrpId(String ansGrpId) {
		this.ansGrpId = ansGrpId;
	}
	public String getIncidentStatus() {
		return incidentStatus;
	}
	public void setIncidentStatus(String incidentStatus) {
		this.incidentStatus = incidentStatus;
	}
	/**
	 * @return the sessNodeId
	 */
	public String getSessNodeId() {
		return sessNodeId;
	}
	/**
	 * @param sessNodeId the sessNodeId to set
	 */
	public void setSessNodeId(String sessNodeId) {
		this.sessNodeId = sessNodeId;
	}
	/**
	 * @return the sessLastClicked
	 */
	public String getSessLastClicked() {
		return sessLastClicked;
	}
	/**
	 * @param sessLastClicked the sessLastClicked to set
	 */
	public void setSessLastClicked(String sessLastClicked) {
		this.sessLastClicked = sessLastClicked;
	}
	/**
	 * @return the sessAccrd
	 */
	public String getSessAccrd() {
		return sessAccrd;
	}
	/**
	 * @param sessAccrd the sessAccrd to set
	 */
	public void setSessAccrd(String sessAccrd) {
		this.sessAccrd = sessAccrd;
	}
	/**
	 * @return the sessOptLastClicked
	 */
	public String getSessOptLastClicked() {
		return sessOptLastClicked;
	}
	/**
	 * @param sessOptLastClicked the sessOptLastClicked to set
	 */
	public void setSessOptLastClicked(String sessOptLastClicked) {
		this.sessOptLastClicked = sessOptLastClicked;
	}
	/**
	 * @return the sessPgToLoad
	 */
	public String getSessPgToLoad() {
		return sessPgToLoad;
	}
	/**
	 * @param sessPgToLoad the sessPgToLoad to set
	 */
	public void setSessPgToLoad(String sessPgToLoad) {
		this.sessPgToLoad = sessPgToLoad;
	}
	/**
	 * @return the sessOpt
	 */
	public String getSessOpt() {
		return sessOpt;
	}
	/**
	 * @param sessOpt the sessOpt to set
	 */
	public void setSessOpt(String sessOpt) {
		this.sessOpt = sessOpt;
	}
	
	/**
	 * @return the patientAnsListId
	 */
	public String getPatientAnsListId() {
		return patientAnsListId;
	}
	/**
	 * @param patientAnsListId the patientAnsListId to set
	 */
	public void setPatientAnsListId(String patientAnsListId) {
		this.patientAnsListId = patientAnsListId;
	}
	
	/**
	 * @return the lockedStudyDisable
	 */
	public String getLockedStudyDisable() {
		return lockedStudyDisable;
	}
	/**
	 * @param lockedStudyDisable the lockedStudyDisable to set
	 */
	public void setLockedStudyDisable(String lockedStudyDisable) {
		this.lockedStudyDisable = lockedStudyDisable;
	}
	public void loadSessionParameters(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        setUserId(GmCommonClass.parseNull((String) session.getAttribute("strSessUserId")));
        setDeptId(GmCommonClass.parseNull((String) session.getAttribute("strSessDeptSeq")));
        setSessPartyId(GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId")));
        setSessStudyId(GmCommonClass.parseNull((String) session.getAttribute("strSessStudyId")));
        setSessSiteId(GmCommonClass.parseNull((String) session.getAttribute("strSessSiteId")));
        setSessPatientId(GmCommonClass.parseNull((String) session.getAttribute("strSessPatientId")));
        setSessPeriodId(GmCommonClass.parseNull((String) session.getAttribute("strSessPeriodId")));
        setSessAccLvl(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
        setSessStudySiteId(GmCommonClass.parseNull((String) session.getAttribute("strSessStudySiteId")));
        setSessPatientPkey(GmCommonClass.parseNull((String) session.getAttribute("strSessPatientPkey")));
        setSessAccrd(GmCommonClass.parseNull((String) session.getAttribute("strSessAccrd")));
        setSessLastClicked(GmCommonClass.parseNull((String) session.getAttribute("strSessLastClicked")));
        setSessNodeId(GmCommonClass.parseNull((String) session.getAttribute("strSessNodeId")));
        setSessOpt(GmCommonClass.parseNull((String) session.getAttribute("strSessStrOpt")));
        setSessOptLastClicked(GmCommonClass.parseNull((String) session.getAttribute("strSessOptLastClicked")));
        setSessPgToLoad(GmCommonClass.parseNull((String) session.getAttribute("strSessPgToLoad")));
    }
	public String getFormDataScr() {
		return formDataScr;
	}
	public void setFormDataScr(String formDataScr) {
		this.formDataScr = formDataScr;
	}
    
}
