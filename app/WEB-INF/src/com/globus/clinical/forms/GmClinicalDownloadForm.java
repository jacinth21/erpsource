/**
 * 
 */
package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

/**
 * @author hparikh
 */
public class GmClinicalDownloadForm extends GmCommonForm {
	private String studyList = "";
	private ArrayList alStudyList = new ArrayList();
	private String formName = "";
	private ArrayList alFormList = new ArrayList();
	private ArrayList alDownloadHistoryList = new ArrayList();
	private String cancelFileId = "";
	private String cancelId = "";
	private String msg = "";
	private ArrayList alCancelReasonList = new ArrayList();
	private String limitedAccess = "";
	private String selFormName = "";
	private String sId = "";
	private String strUploadHome = "";
	private String strLockExist = "";
	private String sessStudyId = "";
	private String downloadPath = "";
	private String lockedStudyDisable = "";
	

	/**
	 * @return the sessStudyId
	 */
	public String getSessStudyId() {
		return sessStudyId;
	}

	/**
	 * @param sessStudyId the sessStudyId to set
	 */
	public void setSessStudyId(String sessStudyId) {
		this.sessStudyId = sessStudyId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[ studyList: " + studyList + " , formName: " + formName + "]";
	}

	/**
	 * @return Returns the msg.
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg
	 *            The msg to set.
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return Returns the alFormList.
	 */
	public ArrayList getAlFormList() {
		return alFormList;
	}

	/**
	 * @param alFormList
	 *            The alFormList to set.
	 */
	public void setAlFormList(ArrayList alFormList) {
		this.alFormList = alFormList;
	}

	/**
	 * @return Returns the alDownloadHistoryList.
	 */
	public ArrayList getAlDownloadHistoryList() {
		return alDownloadHistoryList;
	}

	/**
	 * @param alDownloadHistoryList
	 *            The alDownloadHistoryList to set.
	 */
	public void setAlDownloadHistoryList(ArrayList alDownloadHistoryList) {
		this.alDownloadHistoryList = alDownloadHistoryList;
	}

	/**
	 * @return Returns the alStudyList.
	 */
	public ArrayList getAlStudyList() {
		return alStudyList;
	}

	/**
	 * @param alStudyList
	 *            The alStudyList to set.
	 */
	public void setAlStudyList(ArrayList alStudyList) {
		this.alStudyList = alStudyList;
	}

	/**
	 * @return Returns the siteNameList.
	 */
	public String getFormName() {
		return formName;
	}

	/**
	 * @param siteNameList
	 *            The siteNameList to set.
	 */
	public void setFormName(String formName) {
		this.formName = formName;
	}

	/**
	 * @return Returns the studyList.
	 */
	public String getStudyList() {
		return studyList;
	}

	/**
	 * @param studyList
	 *            The studyList to set.
	 */
	public void setStudyList(String studyList) {
		this.studyList = studyList;
	}

	/**
	 * @return Returns the cancelFileId.
	 */
	public String getCancelFileId() {
		return cancelFileId;
	}

	/**
	 * @param cancelFileId
	 *            The cancelFileId to set.
	 */
	public void setCancelFileId(String cancelFileId) {
		this.cancelFileId = cancelFileId;
	}

	/**
	 * @return Returns the cancelId.
	 */
	public String getCancelId() {
		return cancelId;
	}

	/**
	 * @param cancelId
	 *            The cancelId to set.
	 */
	public void setCancelId(String cancelId) {
		this.cancelId = cancelId;
	}

	/**
	 * @return Returns the alCancelReasonList.
	 */
	public ArrayList getAlCancelReasonList() {
		return alCancelReasonList;
	}

	/**
	 * @param alCancelReasonList
	 *            The alCancelReasonList to set.
	 */
	public void setAlCancelReasonList(ArrayList alCancelReasonList) {
		this.alCancelReasonList = alCancelReasonList;
	}

	/**
	 * @param limitedAccess
	 *            The limitedAccess to set.
	 */
	public void setLimitedAccess(String limitedAccess) {
		this.limitedAccess = limitedAccess;
	}

	/**
	 * @return Returns the limitedAccess.
	 */
	public String getLimitedAccess() {
		return limitedAccess;
	}

	/**
	 * @param selFormName
	 *            The selFormName to set.
	 */
	public void setSelFormName(String selFormName) {
		this.selFormName = selFormName;
	}

	/**
	 * @return Returns the selFormName.
	 */
	public String getSelFormName() {
		return selFormName;
	}

	/**
	 * @param sId
	 *            The sId to set.
	 */
	public void setSId(String sId) {
		this.sId = sId;
	}

	/**
	 * @return Returns the sId.
	 */
	public String getSId() {
		return sId;
	}

	/**
	 * @param strUploadHome
	 *            The strUploadHome to set.
	 */
	public void setStrUploadHome(String strUploadHome) {
		this.strUploadHome = strUploadHome;
	}

	/**
	 * @return Returns the strUploadHome.
	 */
	public String getStrUploadHome() {
		return strUploadHome;
	}

	/**
	 * @param strLockExist
	 *            The strLockExist to set.
	 */
	public void setStrLockExist(String strLockExist) {
		this.strLockExist = strLockExist;
	}

	/**
	 * @return Returns the strLockExist.
	 */
	public String getStrLockExist() {
		return strLockExist;
	}

	/**
	 * @return the downloadPath
	 */
	public String getDownloadPath() {
		return downloadPath;
	}

	/**
	 * @param downloadPath the downloadPath to set
	 */
	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	/**
	 * @return the lockedStudyDisable
	 */
	public String getLockedStudyDisable() {
		return lockedStudyDisable;
	}

	/**
	 * @param lockedStudyDisable the lockedStudyDisable to set
	 */
	public void setLockedStudyDisable(String lockedStudyDisable) {
		this.lockedStudyDisable = lockedStudyDisable;
	}

}
