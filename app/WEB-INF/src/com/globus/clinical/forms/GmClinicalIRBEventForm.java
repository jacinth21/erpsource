package com.globus.clinical.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;

public class GmClinicalIRBEventForm extends GmCancelForm{
	
	private String    actionTaken      = "";
	private ArrayList alActionTakenList    = new ArrayList();
	private String eventDate = "";
	private String notes="";
	private String trackEventGridData ="";
	private String irbEventID="";
	private String studyName="";
	private String siteName="";
	private String approvalTypeName="";
	private String approvalReasonName="";
	private String reviewType="";
	private ArrayList reviewTypeList    = new ArrayList();
	private String reviewPeriod="";
	private ArrayList reviewPeriodList    = new ArrayList();
	private ArrayList iRBEventList    = new ArrayList();
	private String irbid="";
	private String    studyID      = "";
	private String gridData=""; 
	private String lockedStudyDisable = "";

	/**
	 * @return the studyID
	 */
	public String getStudyID() {
		return studyID;
	}
	/**
	 * @param studyID the studyID to set
	 */
	public void setStudyID(String studyID) {
		this.studyID = studyID;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the irbid
	 */
	public String getIrbid() {
		return irbid;
	}
	/**
	 * @param irbid the irbid to set
	 */
	public void setIrbid(String irbid) {
		this.irbid = irbid;
	}
	/**
	 * @return the eventDate
	 */
	public String getEventDate() {
		return eventDate;
	}
	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the trackEventGridData
	 */
	public String getTrackEventGridData() {
		return trackEventGridData;
	}
	/**
	 * @param trackEventGridData the trackEventGridData to set
	 */
	public void setTrackEventGridData(String trackEventGridData) {
		this.trackEventGridData = trackEventGridData;
	}
	/**
	 * @return the actionTaken
	 */
	public String getActionTaken() {
		return actionTaken;
	}
	/**
	 * @param actionTaken the actionTaken to set
	 */
	public void setActionTaken(String actionTaken) {
		this.actionTaken = actionTaken;
	}
	/**
	 * @return the alActionTakenList
	 */
	public ArrayList getAlActionTakenList() {
		return alActionTakenList;
	}
	/**
	 * @param alActionTakenList the alActionTakenList to set
	 */
	public void setAlActionTakenList(ArrayList alActionTakenList) {
		this.alActionTakenList = alActionTakenList;
	}
	/**
	 * @return the irbEventID
	 */
	public String getIrbEventID() {
		return irbEventID;
	}
	/**
	 * @param irbEventID the irbEventID to set
	 */
	public void setIrbEventID(String irbEventID) {
		this.irbEventID = irbEventID;
	}
	/**
	 * @return the studyName
	 */
	public String getStudyName() {
		return studyName;
	}
	/**
	 * @param studyName the studyName to set
	 */
	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}
	/**
	 * @return the siteName
	 */
	public String getSiteName() {
		return siteName;
	}
	/**
	 * @param siteName the siteName to set
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	/**
	 * @return the approvalTypeName
	 */
	public String getApprovalTypeName() {
		return approvalTypeName;
	}
	/**
	 * @param approvalTypeName the approvalTypeName to set
	 */
	public void setApprovalTypeName(String approvalTypeName) {
		this.approvalTypeName = approvalTypeName;
	}
	/**
	 * @return the approvalReasonName
	 */
	public String getApprovalReasonName() {
		return approvalReasonName;
	}
	/**
	 * @param approvalReasonName the approvalReasonName to set
	 */
	public void setApprovalReasonName(String approvalReasonName) {
		this.approvalReasonName = approvalReasonName;
	}
	/**
	 * @return the reviewType
	 */
	public String getReviewType() {
		return reviewType;
	}
	/**
	 * @param reviewType the reviewType to set
	 */
	public void setReviewType(String reviewType) {
		this.reviewType = reviewType;
	}
	/**
	 * @return the reviewTypeList
	 */
	public ArrayList getReviewTypeList() {
		return reviewTypeList;
	}
	/**
	 * @param reviewTypeList the reviewTypeList to set
	 */
	public void setReviewTypeList(ArrayList reviewTypeList) {
		this.reviewTypeList = reviewTypeList;
	}
	/**
	 * @return the reviewPeriod
	 */
	public String getReviewPeriod() {
		return reviewPeriod;
	}
	/**
	 * @param reviewPeriod the reviewPeriod to set
	 */
	public void setReviewPeriod(String reviewPeriod) {
		this.reviewPeriod = reviewPeriod;
	}
	/**
	 * @return the reviewPeriodList
	 */
	public ArrayList getReviewPeriodList() {
		return reviewPeriodList;
	}
	/**
	 * @param reviewPeriodList the reviewPeriodList to set
	 */
	public void setReviewPeriodList(ArrayList reviewPeriodList) {
		this.reviewPeriodList = reviewPeriodList;
	}
	/**
	 * @return the iRBEventList
	 */
	public ArrayList getIRBEventList() {
		return iRBEventList;
	}
	/**
	 * @param eventList the iRBEventList to set
	 */
	public void setIRBEventList(ArrayList iRBEventList) {
		iRBEventList = iRBEventList;
	}
	/**
	 * @return the lockedStudyDisable
	 */
	public String getLockedStudyDisable() {
		return lockedStudyDisable;
	}
	/**
	 * @param lockedStudyDisable the lockedStudyDisable to set
	 */
	public void setLockedStudyDisable(String lockedStudyDisable) {
		this.lockedStudyDisable = lockedStudyDisable;
	}
	
}
