package com.globus.clinical.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;

public class GmClinicalIRBMediaForm extends GmCancelForm{
	
	private String studyName="";
	private String siteName="";
	private String irbid="";
	private String approvalTypeName="";
	private String approvalReasonName="";
	private String mediaType="";
	private ArrayList mediaTypeList    = new ArrayList();
	private String irbMediaNotes ="";
	private String mediaTypeName="";
	private String gridData="";
	private String irbMediaID="";
	private String lockedStudyDisable = "";
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the studyName
	 */
	public String getStudyName() {
		return studyName;
	}
	/**
	 * @param studyName the studyName to set
	 */
	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}
	/**
	 * @return the siteName
	 */
	public String getSiteName() {
		return siteName;
	}
	/**
	 * @param siteName the siteName to set
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	/**
	 * @return the irbid
	 */
	public String getIrbid() {
		return irbid;
	}
	/**
	 * @param irbid the irbid to set
	 */
	public void setIrbid(String irbid) {
		this.irbid = irbid;
	}
	/**
	 * @return the approvalTypeName
	 */
	public String getApprovalTypeName() {
		return approvalTypeName;
	}
	/**
	 * @param approvalTypeName the approvalTypeName to set
	 */
	public void setApprovalTypeName(String approvalTypeName) {
		this.approvalTypeName = approvalTypeName;
	}
	/**
	 * @return the approvalReasonName
	 */
	public String getApprovalReasonName() {
		return approvalReasonName;
	}
	/**
	 * @param approvalReasonName the approvalReasonName to set
	 */
	public void setApprovalReasonName(String approvalReasonName) {
		this.approvalReasonName = approvalReasonName;
	}
	/**
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}
	/**
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	/**
	 * @return the mediaTypeList
	 */
	public ArrayList getMediaTypeList() {
		return mediaTypeList;
	}
	/**
	 * @param mediaTypeList the mediaTypeList to set
	 */
	public void setMediaTypeList(ArrayList mediaTypeList) {
		this.mediaTypeList = mediaTypeList;
	}


	/**
	 * @return the mediaTypeName
	 */
	public String getMediaTypeName() {
		return mediaTypeName;
	}
	/**
	 * @param mediaTypeName the mediaTypeName to set
	 */
	public void setMediaTypeName(String mediaTypeName) {
		this.mediaTypeName = mediaTypeName;
	}
	/**
	 * @return the irbMediaNotes
	 */
	public String getIrbMediaNotes() {
		return irbMediaNotes;
	}
	/**
	 * @param irbMediaNotes the irbMediaNotes to set
	 */
	public void setIrbMediaNotes(String irbMediaNotes) {
		this.irbMediaNotes = irbMediaNotes;
	}
	/**
	 * @return the irbMediaID
	 */
	public String getIrbMediaID() {
		return irbMediaID;
	}
	/**
	 * @param irbMediaID the irbMediaID to set
	 */
	public void setIrbMediaID(String irbMediaID) {
		this.irbMediaID = irbMediaID;
	}
	/**
	 * @return the lockedStudyDisable
	 */
	public String getLockedStudyDisable() {
		return lockedStudyDisable;
	}
	/**
	 * @param lockedStudyDisable the lockedStudyDisable to set
	 */
	public void setLockedStudyDisable(String lockedStudyDisable) {
		this.lockedStudyDisable = lockedStudyDisable;
	}
	

}
