/**
 * 
 */
package com.globus.clinical.forms;

import java.util.ArrayList;

/**
 * @author vprasath
 */
public class GmClinicalSiteMapForm extends GmStudyFilterForm {
  private String studyList = "";
  private ArrayList alStudyList = new ArrayList();
  private String siteNameList = "";
  private ArrayList alSiteNameList = new ArrayList();
  private String siteId = "";
  private String approvalDate = "";
  private String craName = "";
  private ArrayList alCraNames = new ArrayList();
  private String hsiteMapID = "";
  private String msg = "";
  private ArrayList alIRBType = new ArrayList();
  private String irbTypeName = "";
  private String studyPktSent = "";
  private String hApprovalDate = "";
  private String currentDate = "";
  private String tinNumber = "";



  /**
   * @return the tinNumber
   */
  public String getTinNumber() {
    return tinNumber;
  }

  /**
   * @param tinNumber the tinNumber to set
   */
  public void setTinNumber(String tinNumber) {
    this.tinNumber = tinNumber;
  }

  /**
   * @return the studyPktSent
   */
  public String getStudyPktSent() {
    return studyPktSent;
  }

  /**
   * @param studyPktSent the studyPktSent to set
   */
  public void setStudyPktSent(String studyPktSent) {
    this.studyPktSent = studyPktSent;
  }

  /**
   * @return the alIRBType
   */
  public ArrayList getAlIRBType() {
    return alIRBType;
  }

  /**
   * @param alIRBType the alIRBType to set
   */
  public void setAlIRBType(ArrayList alIRBType) {
    this.alIRBType = alIRBType;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {

    return "[ studyList: " + studyList + " , siteNameList: " + siteNameList + " , siteId: "
        + siteId + " ,approvalDate: " + approvalDate + " , craName: " + craName + " , hsiteMapID: "
        + hsiteMapID + "]";
  }

  /**
   * @return Returns the msg.
   */
  public String getMsg() {
    return msg;
  }

  /**
   * @param msg The msg to set.
   */
  public void setMsg(String msg) {
    this.msg = msg;
  }

  /**
   * @return Returns the alCraNames.
   */
  @Override
  public ArrayList getAlCraNames() {
    return alCraNames;
  }

  /**
   * @param alCraNames The alCraNames to set.
   */
  @Override
  public void setAlCraNames(ArrayList alCraNames) {
    this.alCraNames = alCraNames;
  }

  /**
   * @return Returns the alSiteNameList.
   */
  @Override
  public ArrayList getAlSiteNameList() {
    return alSiteNameList;
  }

  /**
   * @param alSiteNameList The alSiteNameList to set.
   */
  @Override
  public void setAlSiteNameList(ArrayList alSiteNameList) {
    this.alSiteNameList = alSiteNameList;
  }

  /**
   * @return Returns the alStudyList.
   */
  @Override
  public ArrayList getAlStudyList() {
    return alStudyList;
  }

  /**
   * @param alStudyList The alStudyList to set.
   */
  @Override
  public void setAlStudyList(ArrayList alStudyList) {
    this.alStudyList = alStudyList;
  }

  /**
   * @return Returns the approvalDate.
   */
  public String getApprovalDate() {
    return approvalDate;
  }

  /**
   * @param approvalDate The approvalDate to set.
   */
  public void setApprovalDate(String approvalDate) {
    this.approvalDate = approvalDate;
  }

  /**
   * @return Returns the craName.
   */
  public String getCraName() {
    return craName;
  }

  /**
   * @param craName The craName to set.
   */
  public void setCraName(String craName) {
    this.craName = craName;
  }

  /**
   * @return Returns the hsiteMapID.
   */
  public String getHsiteMapID() {
    return hsiteMapID;
  }

  /**
   * @param hsiteMapID The hsiteMapID to set.
   */
  public void setHsiteMapID(String hsiteMapID) {
    this.hsiteMapID = hsiteMapID;
  }

  /**
   * @return Returns the siteID.
   */
  public String getSiteId() {
    return siteId;
  }

  /**
   * @param siteID The siteID to set.
   */
  public void setSiteId(String siteId) {
    this.siteId = siteId;
  }

  /**
   * @return Returns the siteNameList.
   */
  public String getSiteNameList() {
    return siteNameList;
  }

  /**
   * @param siteNameList The siteNameList to set.
   */
  public void setSiteNameList(String siteNameList) {
    this.siteNameList = siteNameList;
  }

  /**
   * @return Returns the studyList.
   */
  public String getStudyList() {
    return studyList;
  }

  /**
   * @param studyList The studyList to set.
   */
  public void setStudyList(String studyList) {
    this.studyList = studyList;
  }

  /**
   * @return the irbTypeName
   */
  public String getIrbTypeName() {
    return irbTypeName;
  }

  /**
   * @param irbTypeName the irbTypeName to set
   */
  public void setIrbTypeName(String irbTypeName) {
    this.irbTypeName = irbTypeName;
  }

  /**
   * @return the hApprovalDate
   */
  public String getHApprovalDate() {
    return hApprovalDate;
  }

  /**
   * @param approvalDate the hApprovalDate to set
   */
  public void setHApprovalDate(String approvalDate) {
    hApprovalDate = approvalDate;
  }

  /**
   * @return the currentDate
   */
  public String getCurrentDate() {
    return currentDate;
  }

  /**
   * @param currentDate the currentDate to set
   */
  public void setCurrentDate(String currentDate) {
    this.currentDate = currentDate;
  }

}
