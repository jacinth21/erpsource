
package com.globus.clinical.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.forms.GmCommonForm;

/**
 * @author hparikh
 */
public class GmClinicalIRBReportForm extends GmStudyFilterForm
{
    
    private String    msg        = "";   
    private ArrayList alSiteList = new ArrayList();
    private ArrayList alIRBTypeList=new ArrayList();
    private ArrayList alIRBApprovalTypeList = new ArrayList();
    private ArrayList alDateTypeList = new ArrayList();
    private ArrayList alIRBApprovalReasonList = new ArrayList();
	private ArrayList alIRBApprovalStatusList = new ArrayList();
	private ArrayList alIRBActionTakenList = new ArrayList();
	private ArrayList alIRBMediaTypeList = new ArrayList();
    
    private String siteId="";
    private String irbTypeId="";
    private String irbAppTypeId="";
    private String dateTypeId="";
    private String startDate = "";
    private String endDate = "";
    private String irbReasonId = "";
    private String irbStatusId = "";
    private String irbActionTakenId = "";
    private String irbMediaTypeId = "";
    private String apprTypeName="";
    
    private String strDhtmlx="";
    private String gridXmlData = "";
    private String irbId="";
    
    /**
     * @return Returns the msg.
     */
    public String getMsg()
    {
        return msg;
    }

    /**
     * @param msg
     *            The msg to set.
     */
    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    /**
     * @return Returns the alSiteList.
     */
	public ArrayList getAlSiteList() {
		return alSiteList;
	}
	/**
     * @param alSiteList
     *            The alSiteList to set.
     */
	public void setAlSiteList(ArrayList alSiteList) {
		this.alSiteList = alSiteList;
	}

	/**
     * @return Returns the alIRBApprovalTypeList.
     */
	public ArrayList getAlIRBApprovalTypeList() {
		return alIRBApprovalTypeList;
	}
	/**
     * @param alIRBApprovalTypeList
     *            The alIRBApprovalTypeList to set.
     */
	public void setAlIRBApprovalTypeList(ArrayList alIRBApprovalTypeList) {
		this.alIRBApprovalTypeList = alIRBApprovalTypeList;
	}

	/**
     * @return Returns the alDateTypeList.
     */
	public ArrayList getAlDateTypeList() {
		return alDateTypeList;
	}
	/**
     * @param alDateTypeList
     *            The alDateTypeList to set.
     */
	public void setAlDateTypeList(ArrayList alDateTypeList) {
		this.alDateTypeList = alDateTypeList;
	}
	/**
     * @return Returns the alIRBTypeList.
     */
	public ArrayList getAlIRBTypeList() {
		return alIRBTypeList;
	}
	/**
     * @param alIRBTypeList
     *            The alIRBTypeList to set.
     */
	public void setAlIRBTypeList(ArrayList alIRBTypeList) {
		this.alIRBTypeList = alIRBTypeList;
	}

	/**
     * @return Returns the strDhtmlx.
     */
	public String getStrDhtmlx() {
		return strDhtmlx;
	}
	/**
     * @param strDhtmlx
     *            The strDhtmlx to set.
     */
	public void setStrDhtmlx(String strDhtmlx) {
		this.strDhtmlx = strDhtmlx;
	}
	/**
     * @return Returns the gridXmlData.
     */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
     * @param gridXmlData
     *            The gridXmlData to set.
     */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
     * @return Returns the alIRBApprovalReasonList.
     */
	public ArrayList getAlIRBApprovalReasonList() {
		return alIRBApprovalReasonList;
	}
	/**
     * @param alIRBApprovalReasonList
     *            The alIRBApprovalReasonList to set.
     */
	public void setAlIRBApprovalReasonList(ArrayList alIRBApprovalReasonList) {
		this.alIRBApprovalReasonList = alIRBApprovalReasonList;
	}
	/**
     * @return Returns the alIRBApprovalStatusList.
     */
	public ArrayList getAlIRBApprovalStatusList() {
		return alIRBApprovalStatusList;
	}
	/**
     * @param alIRBApprovalStatusList
     *            The alIRBApprovalStatusList to set.
     */
	public void setAlIRBApprovalStatusList(ArrayList alIRBApprovalStatusList) {
		this.alIRBApprovalStatusList = alIRBApprovalStatusList;
	}
	/**
     * @return Returns the siteId.
     */
	public String getSiteId() {
		return siteId;
	}
	/**
     * @param siteId
     *            The siteId to set.
     */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	/**
     * @return Returns the irbTypeId.
     */
	public String getIrbTypeId() {
		return irbTypeId;
	}
	/**
     * @param irbTypeId
     *            The irbTypeId to set.
     */
	public void setIrbTypeId(String irbTypeId) {
		this.irbTypeId = irbTypeId;
	}
	/**
     * @return Returns the irbAppTypeId.
     */
	public String getIrbAppTypeId() {
		return irbAppTypeId;
	}
	/**
     * @param irbAppTypeId
     *            The irbAppTypeId to set.
     */
	public void setIrbAppTypeId(String irbAppTypeId) {
		this.irbAppTypeId = irbAppTypeId;
	}
	/**
     * @return Returns the dateTypeId.
     */
	public String getDateTypeId() {
		return dateTypeId;
	}
	/**
     * @param dateTypeId
     *            The dateTypeId to set.
     */
	public void setDateTypeId(String dateTypeId) {
		this.dateTypeId = dateTypeId;
	}
	/**
     * @return Returns the startDate.
     */
	public String getStartDate() {
		return startDate;
	}
	/**
     * @param startDate
     *            The startDate to set.
     */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
     * @return Returns the endDate.
     */
	public String getEndDate() {
		return endDate;
	}
	/**
     * @param endDate
     *            The endDate to set.
     */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
     * @return Returns the irbReasonId.
     */
	public String getIrbReasonId() {
		return irbReasonId;
	}
	/**
     * @param irbReasonId
     *            The irbReasonId to set.
     */
	public void setIrbReasonId(String irbReasonId) {
		this.irbReasonId = irbReasonId;
	}
	/**
     * @return Returns the irbStatusId.
     */
	public String getIrbStatusId() {
		return irbStatusId;
	}
	/**
     * @param irbStatusId
     *            The irbStatusId to set.
     */
	public void setIrbStatusId(String irbStatusId) {
		this.irbStatusId = irbStatusId;
	}
	/**
     * @return Returns the alIRBActionTakenList.
     */
	public ArrayList getAlIRBActionTakenList() {
		return alIRBActionTakenList;
	}
	/**
     * @param alIRBActionTakenList
     *            The alIRBActionTakenList to set.
     */
	public void setAlIRBActionTakenList(ArrayList alIRBActionTakenList) {
		this.alIRBActionTakenList = alIRBActionTakenList;
	}
	/**
     * @return Returns the irbActionTakenId.
     */
	public String getIrbActionTakenId() {
		return irbActionTakenId;
	}
	/**
     * @param irbActionTakenId
     *            The irbActionTakenId to set.
     */
	public void setIrbActionTakenId(String irbActionTakenId) {
		this.irbActionTakenId = irbActionTakenId;
	}
	/**
     * @return Returns the alIRBMediaTypeList.
     */
	public ArrayList getAlIRBMediaTypeList() {
		return alIRBMediaTypeList;
	}
	/**
     * @param alIRBMediaTypeList
     *            The alIRBMediaTypeList to set.
     */
	public void setAlIRBMediaTypeList(ArrayList alIRBMediaTypeList) {
		this.alIRBMediaTypeList = alIRBMediaTypeList;
	}
	/**
     * @return Returns the irbMediaTypeId.
     */
	public String getIrbMediaTypeId() {
		return irbMediaTypeId;
	}
	/**
     * @param irbMediaTypeId
     *            The irbMediaTypeId to set.
     */
	public void setIrbMediaTypeId(String irbMediaTypeId) {
		this.irbMediaTypeId = irbMediaTypeId;
	}
	/**
     * @return Returns the apprTypeName.
     */
	public String getApprTypeName() {
		return apprTypeName;
	}
	/**
     * @param apprTypeName
     *            The apprTypeName to set.
     */
	public void setApprTypeName(String apprTypeName) {
		this.apprTypeName = apprTypeName;
	}
	/**
     * @return Returns the irbId.
     */
	public String getIrbId() {
		return irbId;
	}
	/**
     * @param irbId
     *            The irbId to set.
     */
	public void setIrbId(String irbId) {
		this.irbId = irbId;
	}
}

