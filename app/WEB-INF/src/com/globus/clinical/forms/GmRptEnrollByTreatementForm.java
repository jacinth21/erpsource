package com.globus.clinical.forms;

import java.util.HashMap;

public class GmRptEnrollByTreatementForm extends GmCommonClinicalRptForm {
	
	private HashMap hmRptEnrollByTreatement = new HashMap();

	/**
	 * @return the hmRptEnrollByTreatement
	 */
	public HashMap getHmRptEnrollByTreatement() {
		return hmRptEnrollByTreatement;
	}

	/**
	 * @param hmRptEnrollByTreatement the hmRptEnrollByTreatement to set
	 */
	public void setHmRptEnrollByTreatement(HashMap hmRptEnrollByTreatement) {
		this.hmRptEnrollByTreatement = hmRptEnrollByTreatement;
	}
	
}
