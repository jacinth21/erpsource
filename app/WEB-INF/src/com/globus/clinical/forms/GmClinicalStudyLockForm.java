package com.globus.clinical.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;
import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

public class GmClinicalStudyLockForm extends GmCancelForm {
	
	private String gridXmlData = "";
	private String statusId = "";
	private String inputStr = "";
	private String submitAccess = "";
	private String studyId = "";
	private String screenName = "";
	private ArrayList alStudyStatus = new ArrayList();
	
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the statusId
	 */
	public String getStatusId() {
		return statusId;
	}
	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	/**
	 * @return the submitAccess
	 */
	public String getSubmitAccess() {
		return submitAccess;
	}
	/**
	 * @param submitAccess the submitAccess to set
	 */
	public void setSubmitAccess(String submitAccess) {
		this.submitAccess = submitAccess;
	}
	/**
	 * @return the studyId
	 */
	public String getStudyId() {
		return studyId;
	}
	/**
	 * @param studyId the studyId to set
	 */
	public void setStudyId(String studyId) {
		this.studyId = studyId;
	}
	
	
	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}
	/**
	 * @param screenName the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	/**
	 * @return the alStudyStatus
	 */
	public ArrayList getAlStudyStatus() {
		return alStudyStatus;
	}
	/**
	 * @param alStudyStatus the alStudyStatus to set
	 */
	public void setAlStudyStatus(ArrayList alStudyStatus) {
		this.alStudyStatus = alStudyStatus;
	}
	
	
	
}
