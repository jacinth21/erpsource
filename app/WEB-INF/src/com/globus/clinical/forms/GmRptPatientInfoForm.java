package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.List;

public class GmRptPatientInfoForm extends GmCommonClinicalRptForm
{
    private List rdPatientInfo = new ArrayList();

    public List getRdPatientInfo()
    {
        return rdPatientInfo;
    }

    public void setRdPatientInfo(List rdPatientInfo)
    {
        this.rdPatientInfo = rdPatientInfo;
    }
   

}

