package com.globus.clinical.forms;

import com.globus.common.forms.GmCommonForm;

public class GmEditCheckForm extends GmCommonForm {
	private String refId;
	private String description;
	private String specification;
	private String activeFlag;
	private String strOpt;
	private String editID;
	private String lockedStudyDisable = "";
	/**
	 * @return the editID
	 */
	public String getEditID() {
		return editID;
	}
	/**
	 * @param editID the editID to set
	 */
	public void setEditID(String editID) {
		this.editID = editID;
	}
	/**
	 * @return the strOpt
	 */
	public String getStrOpt() {
		return strOpt;
	}
	/**
	 * @param strOpt the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}
	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}
	/**
	 * @param refId the refId to set
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the specification
	 */
	public String getSpecification() {
		return specification;
	}
	/**
	 * @param specification the specification to set
	 */
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	/**
	 * @return the activeFlag
	 */
	public String getActiveFlag() {
		return activeFlag;
	}
	/**
	 * @param activeFlag the activeFlag to set
	 */
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	/**
	 * @return the lockedStudyDisable
	 */
	public String getLockedStudyDisable() {
		return lockedStudyDisable;
	}
	/**
	 * @param lockedStudyDisable the lockedStudyDisable to set
	 */
	public void setLockedStudyDisable(String lockedStudyDisable) {
		this.lockedStudyDisable = lockedStudyDisable;
	}
	
}
