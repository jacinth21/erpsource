package com.globus.clinical.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmClinicalTrainingForm extends GmStudyFilterForm
{

    private String    strTrainingRecordId = "";
	private String    studyList         = "";
    private ArrayList alStudyList       = new ArrayList();
    private String    siteNameList      = "";
    private ArrayList alSiteNameList    = new ArrayList();
    private String    trainingForList   = "";
    private ArrayList alTrainingForList = new ArrayList();
    private String    traineeNameList   = "";
    private ArrayList alTraineeNameList = new ArrayList();
    private String    trainingReasonList  = "";
    private ArrayList alTrainingReasonList = new ArrayList();
    private String    trainingScope     = "";
    private String    completionDate    = "";
    private String    statusList   = "";
    private ArrayList alStatusList = new ArrayList();
    private String    msg               = "";
    private String currentDate ="";
	/**
	 * @return the currentDate
	 */
	public String getCurrentDate() {
		return currentDate;
	}
	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	/**
	 * @return the strTrainingRecordId
	 */
	public String getStrTrainingRecordId() {
		return strTrainingRecordId;
	}
	/**
	 * @param strTrainingRecordId the strTrainingRecordId to set
	 */
	public void setStrTrainingRecordId(String strTrainingRecordId) {
		this.strTrainingRecordId = strTrainingRecordId;
	}
	/**
	 * @return the studyList
	 */
	public String getStudyList() {
		return studyList;
	}
	/**
	 * @param studyList the studyList to set
	 */
	public void setStudyList(String studyList) {
		this.studyList = studyList;
	}
	/**
	 * @return the alStudyList
	 */
	public ArrayList getAlStudyList() {
		return alStudyList;
	}
	/**
	 * @param alStudyList the alStudyList to set
	 */
	public void setAlStudyList(ArrayList alStudyList) {
		this.alStudyList = alStudyList;
	}
	/**
	 * @return the siteNameList
	 */
	public String getSiteNameList() {
		return siteNameList;
	}
	/**
	 * @param siteNameList the siteNameList to set
	 */
	public void setSiteNameList(String siteNameList) {
		this.siteNameList = siteNameList;
	}
	/**
	 * @return the alSiteNameList
	 */
	public ArrayList getAlSiteNameList() {
		return alSiteNameList;
	}
	/**
	 * @param alSiteNameList the alSiteNameList to set
	 */
	public void setAlSiteNameList(ArrayList alSiteNameList) {
		this.alSiteNameList = alSiteNameList;
	}
	/**
	 * @return the trainingForList
	 */
	public String getTrainingForList() {
		return trainingForList;
	}
	/**
	 * @param trainingForList the trainingForList to set
	 */
	public void setTrainingForList(String trainingForList) {
		this.trainingForList = trainingForList;
	}
	/**
	 * @return the alTrainingForList
	 */
	public ArrayList getAlTrainingForList() {
		return alTrainingForList;
	}
	/**
	 * @param alTrainingForList the alTrainingForList to set
	 */
	public void setAlTrainingForList(ArrayList alTrainingForList) {
		this.alTrainingForList = alTrainingForList;
	}
	/**
	 * @return the traineeNameList
	 */
	public String getTraineeNameList() {
		return traineeNameList;
	}
	/**
	 * @param traineeNameList the traineeNameList to set
	 */
	public void setTraineeNameList(String traineeNameList) {
		this.traineeNameList = traineeNameList;
	}
	/**
	 * @return the alTraineeNameList
	 */
	public ArrayList getAlTraineeNameList() {
		return alTraineeNameList;
	}
	/**
	 * @param alTraineeNameList the alTraineeNameList to set
	 */
	public void setAlTraineeNameList(ArrayList alTraineeNameList) {
		this.alTraineeNameList = alTraineeNameList;
	}
	/**
	 * @return the trainingReasonList
	 */
	public String getTrainingReasonList() {
		return trainingReasonList;
	}
	/**
	 * @param trainingReasonList the trainingReasonList to set
	 */
	public void setTrainingReasonList(String trainingReasonList) {
		this.trainingReasonList = trainingReasonList;
	}
	/**
	 * @return the alTrainingReasonList
	 */
	public ArrayList getAlTrainingReasonList() {
		return alTrainingReasonList;
	}
	/**
	 * @param alTrainingReasonList the alTrainingReasonList to set
	 */
	public void setAlTrainingReasonList(ArrayList alTrainingReasonList) {
		this.alTrainingReasonList = alTrainingReasonList;
	}
	/**
	 * @return the trainingScope
	 */
	public String getTrainingScope() {
		return trainingScope;
	}
	/**
	 * @param trainingScope the trainingScope to set
	 */
	public void setTrainingScope(String trainingScope) {
		this.trainingScope = trainingScope;
	}
	/**
	 * @return the completionDate
	 */
	public String getCompletionDate() {
		return completionDate;
	}
	/**
	 * @param completionDate the completionDate to set
	 */
	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}
	/**
	 * @return the statusList
	 */
	public String getStatusList() {
		return statusList;
	}
	/**
	 * @param statusList the statusList to set
	 */
	public void setStatusList(String statusList) {
		this.statusList = statusList;
	}
	/**
	 * @return the alstatusList
	 */
	public ArrayList getAlStatusList() {
		return alStatusList;
	}
	/**
	 * @param alstatusList the alstatusList to set
	 */
	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
