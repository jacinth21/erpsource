package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;

import com.globus.common.forms.GmCommonForm;

public class GmClinicalSiteMonitorForm extends GmStudyFilterForm
{
   
    private String[] checkSiteName = new String[50];
    private String selectAll = "";
    //Added for PED Score
    
    
    //Added from ImproveDetial
    
    //For DHTMLX
    private String strStatus = "";
    
    private HashMap hmCrossTabReport = new HashMap();
    
    private List ldtResult = new ArrayList();
    ArrayList alTreatment = new ArrayList();
    ArrayList alType = new ArrayList();
    private ArrayList alCraNames = new ArrayList();
    
    private String strSiteIds = "";
    
    /*Himanshu code starts*/
    private String strStudyList="";
    private String strSiteList="";
    private String strDateVisited="";
    private String strOwnerOfVisit="";
    private String[] otherMonitor = new String[50];
    private String strPurposeOfVisit="";
    private String strNotesOfVisit="";
    private String strSiteMonitorStatus="";
    private String siteMonitorStatus="";
    private String strFromDate = "";
    private String strToDate = "";
    private String monitorString ="";
    private String siteMtrId="";
    //private 
    private ArrayList alOwnerOfVisit = new ArrayList();
    private ArrayList alPurposeOfVisit = new ArrayList();
    private ArrayList alSiteMonitorStatus = new ArrayList();
    private ArrayList alOtherMonitor = new ArrayList();
    private ArrayList alStatus = new ArrayList();
    
    
    /**
	 * @return the siteMtrId
	 */
	public String getSiteMtrId() {
		return siteMtrId;
	}
	/**
	 * @param siteMtrId the siteMtrId to set
	 */
	public void setSiteMtrId(String siteMtrId) {
		this.siteMtrId = siteMtrId;
	}
    
    /**
	 * @return the strStudyList
	 */
	public String getStudyList() {
		return strStudyList;
	}
	/**
	 * @param strStudyList the strStudyList to set
	 */
	public void setStudyList(String strStudyList) {
		this.strStudyList = strStudyList;
	}
	/**
	 * @return the strSiteList
	 */
	public String getSiteList() {
		return strSiteList;
	}
	/**
	 * @param strSiteList the strSiteList to set
	 */
	public void setSiteList(String strSiteList) {
		this.strSiteList = strSiteList;
	}
	/**
	 * @return the strDateVisited
	 */
	public String getDateVisited() {
		return strDateVisited;
	}
	/**
	 * @param strDateVisited the strDateVisited to set
	 */
	public void setDateVisited(String strDateVisited) {
		this.strDateVisited = strDateVisited;
	}
	/**
	 * @return the strOwnerOfVisit
	 */
	public String getOwnerOfVisit() {
		return strOwnerOfVisit;
	}
	/**
	 * @param strOwnerOfVisit the strOwnerOfVisit to set
	 */
	public void setOwnerOfVisit(String strOwnerOfVisit) {
		this.strOwnerOfVisit = strOwnerOfVisit;
	}
	
	/**
	 * @return the strPurposeOfVisit
	 */
	public String getPurposeOfVisit() {
		return strPurposeOfVisit;
	}
	/**
	 * @param strPurposeOfVisit the strPurposeOfVisit to set
	 */
	public void setPurposeOfVisit(String strPurposeOfVisit) {
		this.strPurposeOfVisit = strPurposeOfVisit;
	}
	/**
	 * @return the strNotesOfVisit
	 */
	public String getNotesOfVisit() {
		return strNotesOfVisit;
	}
	/**
	 * @param strNotesOfVisit the strNotesOfVisit to set
	 */
	public void setNotesOfVisit(String strNotesOfVisit) {
		this.strNotesOfVisit = strNotesOfVisit;
	}
	/**
	 * @return the strStatus
	 */
	public String getStrSiteMonitorStatus() {
		return strSiteMonitorStatus;
	}
	/**
	 * @param strStatus the strStatus to set
	 */
	public void setStrSiteMonitorStatus(String strSiteMonitorStatus) {
		this.strSiteMonitorStatus = strSiteMonitorStatus;
	}
	/**
	 * @return the strStatus
	 */
	public String getSiteMonitorStatus() {
		return siteMonitorStatus;
	}
	/**
	 * @param strStatus the strStatus to set
	 */
	public void setSiteMonitorStatus(String strSiteMonitorStatus) {
		this.siteMonitorStatus = strSiteMonitorStatus;
	}
	/**
	 * @return the alOwnerOfVisit
	 */
	public ArrayList getAlOwnerOfVisit() {
		return alOwnerOfVisit;
	}
	/**
	 * @param alOwnerOfVisit the alOwnerOfVisit to set
	 */
	public void setAlOwnerOfVisit(ArrayList alOwnerOfVisit) {
		this.alOwnerOfVisit = alOwnerOfVisit;
	}
	/**
	 * @return the alPurposeOfVisit
	 */
	public ArrayList getAlPurposeOfVisit() {
		return alPurposeOfVisit;
	}
	/**
	 * @param alPurposeOfVisit the alPurposeOfVisit to set
	 */
	public void setAlPurposeOfVisit(ArrayList alPurposeOfVisit) {
		this.alPurposeOfVisit = alPurposeOfVisit;
	}
	/**
	 * @return the alSiteMonitorStatus
	 */
	public ArrayList getAlSiteMonitorStatus() {
		return alSiteMonitorStatus;
	}
	/**
	 * @param alSiteMonitorStatus the alSiteMonitorStatus to set
	 */
	public void setAlSiteMonitorStatus(ArrayList alSiteMonitorStatus) {
		this.alSiteMonitorStatus = alSiteMonitorStatus;
	}
	/**
	 * @return the alOtherMonitor
	 */
	public ArrayList getAlOtherMonitor() {
		return alOtherMonitor;
	}
	/**
	 * @param alOtherMonitor the alOtherMonitor to set
	 */
	public void setAlOtherMonitor(ArrayList alOtherMonitor) {
		this.alOtherMonitor = alOtherMonitor;
	}
	/**
	 * @return the otherMonitor
	 */
	public String[] getOtherMonitor() {
		return otherMonitor;
	}
	/**
	 * @param otherMonitor the otherMonitor to set
	 */
	public void setOtherMonitor(String[] otherMonitor) {
		this.otherMonitor = otherMonitor;
	}
	
	/**
	 * @return the monitorString
	 */
	public String getMonitorString() {
		return monitorString;
	}
	/**
	 * @param monitorString the monitorString to set
	 */
	public void setMonitorString(String monitorString) {
		this.monitorString = monitorString;
	}
	/*****************************************************************************/
    /*Himanshu Code ends*/
	
	
	/**
	 * @return the strSiteIds
	 */
	public String getStrSiteIds() {
		return strSiteIds;
	}
	/**
	 * @param strSiteIds the strSiteIds to set
	 */
	public void setStrSiteIds(String strSiteIds) {
		this.strSiteIds = strSiteIds;
	}
	/**
     * @return Returns the hmCrossTabReport.
     */
    public HashMap getHmCrossTabReport()
    {
        return hmCrossTabReport;
    }
    /**
     * @param hmCrossTabReport The hmCrossTabReport to set.
     */
    public void setHmCrossTabReport(HashMap hmCrossTabReport)
    {
        this.hmCrossTabReport = hmCrossTabReport;
    }
    /**
     * @return Returns the selectAll.
     */
    public String getSelectAll()
    {
        return selectAll;
    }
    /**
     * @param selectAll The selectAll to set.
     */
    public void setSelectAll(String selectAll)
    {
        this.selectAll = selectAll;
    }
    
    /**
     * @return Returns the checkSiteName.
     */
    public String[] getCheckSiteName()
    {
        return checkSiteName;
    }
    /**
     * @param checkSiteName The checkSiteName to set.
     */
    public void setCheckSiteName(String[] checkSiteName)
    {
        this.checkSiteName = checkSiteName;
    }
    
    /**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }
	
	
	/**
	 * @return the alTreatment
	 */
	public ArrayList getAlTreatment() {
		return alTreatment;
	}
	/**
	 * @param alTreatment the alTreatment to set
	 */
	public void setAlTreatment(ArrayList alTreatment) {
		this.alTreatment = alTreatment;
	}
	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}
	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}
	
	
	/**
	 * @return the alCraNames
	 */
	public ArrayList getAlCraNames() {
		return alCraNames;
	}
	/**
	 * @param alCraNames the alCraNames to set
	 */
	public void setAlCraNames(ArrayList alCraNames) {
		this.alCraNames = alCraNames;
	}
	/**
	 * @return the strFromDate
	 */
	public String getFromDate() {
		return strFromDate;
	}
	/**
	 * @param strFromDate the strFromDate to set
	 */
	public void setFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}
	/**
	 * @return the strToDate
	 */
	public String getToDate() {
		return strToDate;
	}
	/**
	 * @param strToDate the strToDate to set
	 */
	public void setToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	/**
	 * @return the strStatus
	 */
	public String getStatus() {
		return strStatus;
	}
	/**
	 * @param strStatus the strStatus to set
	 */
	public void setStatus(String strStatus) {
		this.strStatus = strStatus;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	
	
    
}
