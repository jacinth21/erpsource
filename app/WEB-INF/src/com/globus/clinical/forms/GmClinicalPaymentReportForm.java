package com.globus.clinical.forms;

import java.util.ArrayList;

public class GmClinicalPaymentReportForm extends GmStudyFilterForm{
	
	private String requestType = "";
	private String reasonForPayment = "";
	private String showDetails = "";
	private String paymentId = "";
	private String paymentDetailId = "";
	
    private ArrayList alRequestType = new ArrayList();
    private ArrayList alReasonForPayment = new ArrayList();
	

	/**
	 * @return the alRequestType
	 */
	public ArrayList getAlRequestType() {
		return alRequestType;
	}
	/**
	 * @param alRequestType the alRequestType to set
	 */
	public void setAlRequestType(ArrayList alRequestType) {
		this.alRequestType = alRequestType;
	}
	/**
	 * @return the alReasonForPayment
	 */
	public ArrayList getAlReasonForPayment() {
		return alReasonForPayment;
	}
	/**
	 * @param alReasonForPayment the alReasonForPayment to set
	 */
	public void setAlReasonForPayment(ArrayList alReasonForPayment) {
		this.alReasonForPayment = alReasonForPayment;
	}
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}
	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	/**
	 * @return the reasonForPayment
	 */
	public String getReasonForPayment() {
		return reasonForPayment;
	}
	/**
	 * @param reasonForPayment the reasonForPayment to set
	 */
	public void setReasonForPayment(String reasonForPayment) {
		this.reasonForPayment = reasonForPayment;
	}
	/**
	 * @return the showDetails
	 */
	public String getShowDetails() {
		return showDetails;
	}
	/**
	 * @param showDetails the showDetails to set
	 */
	public void setShowDetails(String showDetails) {
		this.showDetails = showDetails;
	}
	/**
	 * @return the paymentId
	 */
	public String getPaymentId() {
		return paymentId;
	}
	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	/**
	 * @return the paymentDetailId
	 */
	public String getPaymentDetailId() {
		return paymentDetailId;
	}
	/**
	 * @param paymentDetailId the paymentDetailId to set
	 */
	public void setPaymentDetailId(String paymentDetailId) {
		this.paymentDetailId = paymentDetailId;
	}

}
