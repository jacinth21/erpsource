package com.globus.clinical.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmClinicalSurgeonSetupForm extends GmCommonForm
{

    private String    studyList         = "";
    private ArrayList alStudyList       = new ArrayList();
    private String    siteNameList      = "";
    private ArrayList alSiteNameList    = new ArrayList();
    private String    hsurgeonSetupID   = "";
    private String    msg               = "";
    private String    surgeonNameList   = "";
    private ArrayList alSurgeonNameList = new ArrayList();
    private String    firstName         = "";
    private String    lastName          = "";
    private String    surgeonType       = "";
    private String lockedStudyDisable = "";
    private ArrayList alSurgeonType     = new ArrayList();
    /**
     * @return Returns the alSiteNameList.
     */
    public ArrayList getAlSiteNameList()
    {
        return alSiteNameList;
    }
    /**
     * @param alSiteNameList The alSiteNameList to set.
     */
    public void setAlSiteNameList(ArrayList alSiteNameList)
    {
        this.alSiteNameList = alSiteNameList;
    }
    /**
     * @return Returns the alStudyList.
     */
    public ArrayList getAlStudyList()
    {
        return alStudyList;
    }
    /**
     * @param alStudyList The alStudyList to set.
     */
    public void setAlStudyList(ArrayList alStudyList)
    {
        this.alStudyList = alStudyList;
    }
    /**
     * @return Returns the alSurgeonNameList.
     */
    public ArrayList getAlSurgeonNameList()
    {
        return alSurgeonNameList;
    }
    /**
     * @param alSurgeonNameList The alSurgeonNameList to set.
     */
    public void setAlSurgeonNameList(ArrayList alSurgeonNameList)
    {
        this.alSurgeonNameList = alSurgeonNameList;
    }
    /**
     * @return Returns the alSurgeonType.
     */
    public ArrayList getAlSurgeonType()
    {
        return alSurgeonType;
    }
    /**
     * @param alSurgeonType The alSurgeonType to set.
     */
    public void setAlSurgeonType(ArrayList alSurgeonType)
    {
        this.alSurgeonType = alSurgeonType;
    }
    /**
     * @return Returns the firstName.
     */
    public String getFirstName()
    {
        return firstName;
    }
    /**
     * @param firstName The firstName to set.
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    /**
     * @return Returns the hsurgeonSetupID.
     */
    public String getHsurgeonSetupID()
    {
        return hsurgeonSetupID;
    }
    /**
     * @param hsurgeonSetupID The hsurgeonSetupID to set.
     */
    public void setHsurgeonSetupID(String hsurgeonSetupID)
    {
        this.hsurgeonSetupID = hsurgeonSetupID;
    }
    /**
     * @return Returns the lastName.
     */
    public String getLastName()
    {
        return lastName;
    }
    /**
     * @param lastName The lastName to set.
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    /**
     * @return Returns the msg.
     */
    public String getMsg()
    {
        return msg;
    }
    /**
     * @param msg The msg to set.
     */
    public void setMsg(String msg)
    {
        this.msg = msg;
    }
    /**
     * @return Returns the siteNameList.
     */
    public String getSiteNameList()
    {
        return siteNameList;
    }
    /**
     * @param siteNameList The siteNameList to set.
     */
    public void setSiteNameList(String siteNameList)
    {
        this.siteNameList = siteNameList;
    }
    /**
     * @return Returns the studyList.
     */
    public String getStudyList()
    {
        return studyList;
    }
    /**
     * @param studyList The studyList to set.
     */
    public void setStudyList(String studyList)
    {
        this.studyList = studyList;
    }
    /**
     * @return Returns the surgeonNameList.
     */
    public String getSurgeonNameList()
    {
        return surgeonNameList;
    }
    /**
     * @param surgeonNameList The surgeonNameList to set.
     */
    public void setSurgeonNameList(String surgeonNameList)
    {
        this.surgeonNameList = surgeonNameList;
    }
    /**
     * @return Returns the surgeonType.
     */
    public String getSurgeonType()
    {
        return surgeonType;
    }
    /**
     * @param surgeonType The surgeonType to set.
     */
    public void setSurgeonType(String surgeonType)
    {
        this.surgeonType = surgeonType;
    }
	/**
	 * @return the lockedStudyDisable
	 */
	public String getLockedStudyDisable() {
		return lockedStudyDisable;
	}
	/**
	 * @param lockedStudyDisable the lockedStudyDisable to set
	 */
	public void setLockedStudyDisable(String lockedStudyDisable) {
		this.lockedStudyDisable = lockedStudyDisable;
	}

}
