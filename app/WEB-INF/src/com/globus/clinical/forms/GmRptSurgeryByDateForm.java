package com.globus.clinical.forms;

import java.util.ArrayList;
import java.util.List;

public class GmRptSurgeryByDateForm extends GmCommonClinicalRptForm
{       
	private ArrayList alTreatment = new ArrayList();
    private ArrayList alStatus = new ArrayList();
    
    private String cbo_Treatment = "";
    private String cbo_Status = "60110";
    
    private List rdPlannedSurgerys  = new ArrayList();
    private String siteId = "";
    
    
    public String getSiteId()
    {
        return siteId;
    }
    public void setSiteId(String siteId)
    {
        this.siteId = siteId;
    }
    public List getRdPlannedSurgerys()
    {
        return rdPlannedSurgerys;
    }
    public void setRdPlannedSurgerys(List rdPlannedSurgerys)
    {
        this.rdPlannedSurgerys = rdPlannedSurgerys;
    }
    public ArrayList getAlStatus()
    {
        return alStatus;
    }
    public void setAlStatus(ArrayList alStatus)
    {
        
        this.alStatus = alStatus;
    }
    public ArrayList getAlTreatment()
    {
        return alTreatment;
    }
    public void setAlTreatment(ArrayList alTreatment)
    {
        this.alTreatment = alTreatment;
    }
    public String getCbo_Status()
    {
        return cbo_Status;
    }
    public void setCbo_Status(String cbo_Status)
    {
        this.cbo_Status = cbo_Status;
    }
    public String getCbo_Treatment()
    {
        return cbo_Treatment;
    }
    public void setCbo_Treatment(String cbo_Treatment)
    {
        this.cbo_Treatment = cbo_Treatment;
    }

}
