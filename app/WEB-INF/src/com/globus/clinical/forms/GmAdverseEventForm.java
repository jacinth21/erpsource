package com.globus.clinical.forms;

public class GmAdverseEventForm extends GmStudyFilterForm
{
    private String patientId = "";

    /**
     * @return Returns the patientId.
     */
    public String getPatientId()
    {
        return patientId;
    }

    /**
     * @param patientId The patientId to set.
     */
    public void setPatientId(String patientId)
    {
        this.patientId = patientId;
    }
    
}
