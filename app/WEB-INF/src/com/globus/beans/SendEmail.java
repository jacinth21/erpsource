package com.globus.beans;

import java.util.*;
import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.ResourceBundle;


public class SendEmail
{
	private String hostName;
	private String altHostName;
	private String frmAddress;
	private String toAddress;
	private String ccAddress;
	private String subject;
	private String message;
	private boolean debug=false;
	private int noOfTrials = 0;
	private int maxNoOfTrials = 0;
	private int noOfTrialsInPrimaryServer = 0;
	private String contentType;
	private int retCode = 0;
	ResourceBundle rb;
	Properties mailprops = new Properties();
	
	private static ResourceBundle rbApp = ResourceBundle.getBundle("com.globus.properties.mail");


	public SendEmail()
	{

		try
		{
//			FileInputStream is = new FileInputStream(System.getProperty("AppRoot")+"/com/ge/nbc/util/mail/mailserver.props");
			//InputStream is = getClass().getResourceAsStream("/globus/properties/mail.properties");

			//mailprops.load(is);
			//is.close();
		}
		catch(Exception e)
		{
			System.out.println("Properties not found"+e);
		}

	    /*hostName = mailprops.getProperty("HOST");
	    altHostName = mailprops.getProperty("ALTHOST");
	    maxNoOfTrials = new Integer(mailprops.getProperty("MaxNoOfTrials")).intValue();
		noOfTrialsInPrimaryServer = new Integer(mailprops.getProperty("NoOfTrialsInPrimaryServer")).intValue();
		*/

	    hostName = rbApp.getString("HOST");
		System.out.println("JAMES:hostName ///// "+hostName);
	    altHostName = rbApp.getString("ALTHOST");
	    maxNoOfTrials = new Integer(rbApp.getString("MaxNoOfTrials")).intValue();
		noOfTrialsInPrimaryServer = new Integer(rbApp.getString("NoOfTrialsInPrimaryServer")).intValue();
		message = rbApp.getString("ReackMailText");
		subject = rbApp.getString("SubText");
		frmAddress = rbApp.getString("FromId");
		toAddress = rbApp.getString("FromId");
	}

	public void setMailHost(String host)
	{
		hostName = host;
	}

	public void setAltMailHost(String host)
	{
		altHostName = host;
	}

	public void setFromAddress(String fAdd)
	{
		frmAddress=fAdd;
	}


	public void setToAddress(String fAdd)
	{
		toAddress=fAdd;
		//toAddress = "thillaisp@mascotsystems.com";
	}

	public void setCCAddress(String ccAdd)
	{
		ccAddress=ccAdd;
	}

	public void setMailSubject(String mlSubject)
	{
		subject=mlSubject;
	}

	public void setMailMessage(String mlMsg)
	{
		message=mlMsg;
	}
	public void setMailMessagebulk(StringBuffer mai)
	{
		message = mai.toString();
	}

	public int sendMail( String strContentType)
	{

		try
		{

				Session session = null;
				contentType = strContentType;

				Properties props = new Properties();
				if(noOfTrials < noOfTrialsInPrimaryServer)
				{
					session = Session.getDefaultInstance(props, null);
					props.put("mail.smtp.host", hostName);
					if (debug) props.put("mail.debug", "false");
				}
				else
				{
					props.remove("mail.smtp.host");
					props.put("mail.smtp.host", altHostName);
					if (debug) props.put("mail.debug", "false");
					session = Session.getInstance(props, null);
				}

System.out.println("/////////////////HERE///////////////");

				session.setDebug(debug);

				MimeMessage msg = new MimeMessage(session);
				MimeBodyPart mbp=new MimeBodyPart();
				mbp.setText(message);

				mbp.addHeader("Content-Type", strContentType);
				Multipart mp=new MimeMultipart();
				mp.addBodyPart(mbp);
    			if (debug) System.out.println("fromAddress :"+ frmAddress);
    			msg.setFrom(new InternetAddress(frmAddress));
				if (ccAddress != null)
				{
					StringTokenizer stcc=new StringTokenizer(ccAddress,";");
					InternetAddress[] ccadd = new InternetAddress[stcc.countTokens()];
					if (debug) System.out.println("Tokens="+stcc.countTokens());
					int j=0;
					while(stcc.hasMoreTokens())
					{
						ccadd[j] = new InternetAddress(stcc.nextToken());
						if (debug) System.out.println("address[j]="+ccadd[j]);
						j++;
					}
					msg.setRecipients(Message.RecipientType.CC, ccadd);
	    		}
				if (toAddress != null)
				{
					StringTokenizer stto=new StringTokenizer(toAddress,";");
					InternetAddress[] toadd = new InternetAddress[stto.countTokens()];
					if (debug) System.out.println("Tokens="+stto.countTokens());
					int i=0;
					while(stto.hasMoreTokens())
					{
						toadd[i] = new InternetAddress(stto.nextToken());
						if (debug) System.out.println("address[i]="+toadd[i]);
						i++;
					}
					if (debug) System.out.println(toadd[0]);
					msg.setRecipients(Message.RecipientType.TO, toadd);
				}
            	if (debug) System.out.println(subject);

				msg.setSubject(subject);
				msg.setContent(mp);
				msg.setSentDate(new Date());
				Transport.send(msg);
				noOfTrials = noOfTrials + 1;
				if (debug) System.out.println("no Of Trails ::::::"+noOfTrials);
				retCode = 0;

    	}
		catch (MessagingException mex)
		{
			if (debug) System.out.println("\n--Exception handling in msgsendsample.java");
			mex.printStackTrace();
			if (debug) System.out.println();
			Exception ex = mex;
			noOfTrials = noOfTrials + 1;
			if(noOfTrials < maxNoOfTrials)
			{
	    		do
	    		{
					if (ex instanceof SendFailedException)
					{
						sendMail(contentType);
					}
					if (debug) System.out.println();
	    		} while ((ex = ((MessagingException)ex).getNextException()) != null);
	    		retCode = 4;
			}
			else
			{
				// Error in Sending Mail. So return Error Code 4.
				retCode = 4;
			}
		}

		return retCode;
	}



	public void sendFile(String fileName,StringBuffer mess)
	{
		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.host", hostName);
			if (debug) props.put("mail.debug", "false");
		    Session session = Session.getDefaultInstance(props, null);
			session.setDebug(debug);
		    // create a message
		    MimeMessage msg = new MimeMessage(session);
    		msg.setFrom(new InternetAddress(frmAddress));

    		StringTokenizer stto=new StringTokenizer(toAddress,";");
			StringTokenizer stcc=new StringTokenizer(ccAddress,";");

			InternetAddress[] toadd = new InternetAddress[stto.countTokens()];
			InternetAddress[] ccadd = new InternetAddress[stcc.countTokens()];
			if (debug) System.out.println("Tokens="+stto.countTokens());
			if (debug) System.out.println("Tokens="+stcc.countTokens());
			int i=0;
			int j=0;
    		while(stto.hasMoreTokens())
    		{
	    		toadd[i] = new InternetAddress(stto.nextToken());
	    		if (debug) System.out.println("address[i]="+toadd[i]);
	    		i++;
	    	}
	    	while(stcc.hasMoreTokens())
			{
				ccadd[j] = new InternetAddress(stcc.nextToken());
				if (debug) System.out.println("address[j]="+ccadd[j]);
				j++;
		    }

    		msg.setRecipients(Message.RecipientType.TO, toadd);
    		msg.setRecipients(Message.RecipientType.CC, ccadd);


//    		InternetAddress[] address = {new InternetAddress(toAddress)};
// 		    msg.setRecipients(Message.RecipientType.TO, address);
		    msg.setSubject(subject);

		    // create and fill the first message part
		    MimeBodyPart mbp1 = new MimeBodyPart();
		    mbp1.setText(mess.toString());
		    // create the second message part
		    MimeBodyPart mbp2 = new MimeBodyPart();

	            // attach the file to the message
	   	    	FileDataSource fds = new FileDataSource(fileName);
			    mbp2.setDataHandler(new DataHandler(fds));
			    mbp2.setFileName(fds.getName());

            mbp1.addHeader("Content-Type", "text/html; charset=ISO_8859-1");
		    // create the Multipart and its parts to it
		    Multipart mp = new MimeMultipart();
		    mp.addBodyPart(mbp1);
		    mp.addBodyPart(mbp2);

		    // add the Multipart to the message
		    msg.setContent(mp);

		    // set the Date: header
		    msg.setSentDate(new Date());

		    // send the message
		    Transport.send(msg);

		}
		catch (MessagingException mex)
		{
			mex.printStackTrace();
			Exception ex = null;
			if ((ex = mex.getNextException()) != null)
			{
				ex.printStackTrace();
	    	}

		}
	}

}
