/*
 * Module:       GmTestBean.java
 * Author:       Dhinakaran James
 * Project:      Globus Medical App
 * Date-Written: 11 Mar 2004
 * Security:     Unclassified
 * Description:  Testing Bean
 *
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */

package com.globus.beans;
import java.util.*;
import java.sql.*;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.*;

public class GmTestBean
{

		/**
	  	  * getIdeaName - This method will retrieves ideaName given ideaId
	  	  * @param int IdeaId
	  	  * @return String
      	  * @exception ltw.util.LTCApplicationError
      	**/
		public String getFromBean() throws AppError
		{
			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();

			String strBeanMsg = "";
			StringBuffer sbQuery = null;
			HashMap hmResult = new HashMap();
			sbQuery = new StringBuffer();
			sbQuery.setLength(0);

			try
			{
				sbQuery.append(" SELECT sysdate FROM DUAL ");
				hmResult = dbCon.querySingleRecord(sbQuery.toString());
				strBeanMsg = (String)hmResult.get("SYSDATE");

			}catch(Exception e)
			{
				e.printStackTrace();
			}
		    return strBeanMsg;
		}

}// end of class GmTestBean