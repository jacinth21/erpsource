package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesDashLoanerBeanQuery extends GmSalesFilterConditionBean {


  public GmSalesDashLoanerBeanQuery() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmSalesDashLoanerBeanQuery(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }



  /**
   * 
   * Count and Drill down calls are separated into two methods. Removed the unwanted the rows and
   * where condition check while fetching the count. Database Object Creation - Changed the object
   * creation one per class instead of one per method. @ NEW Java File creation. Methods Added:
   * fetchReqLoanercount,fetchReqLoanerDrillDown,fetchDueTodayLoanerCount, fetchDue1DayLoanerCount,
   * fetchDue2DayLoanerCount , fetchOverDueLoanerCount, fetchShippedLoanerCount,
   * fetchDueLoanerDrillDown, fetchMissingPartLoanerCount, fetchMissingPartLoanerDrillDown,
   * fetchApprLoanerCount,fetchApprLoanerDrillDown
   * 
   * @author gpalani PMT-10095 (RFC2252)
   * @since 2017-01-03
   */



  // To Fetch the Requested Loaner count
  public StringBuffer fetchReqLoanercount(HashMap hmParam) {
    // TODO Auto-generated method stub
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT    'Requested'  name, count(A.REQSTATUS) total  FROM (SELECT T526.C526_Status_Fl REQSTATUS ");
    sbQuery.append("  FROM t526_product_request_detail t526 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append("  , t525_product_request t525 ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append("  , t525_product_request t525 , t504a_consignment_loaner t504acl ");
	/*
	 * sbQuery.
	 * append("  , (SELECT t504a.c504_consignment_id, c526_product_request_detail_id "
	 * ); sbQuery.append("  , t504a.c504a_expected_return_dt expretdate ");
	 * sbQuery.append("  FROM t504a_consignment_loaner t504a "); sbQuery.
	 * append("  , (SELECT   t504alt.c504_consignment_id, c526_product_request_detail_id "
	 * ); sbQuery.append("  FROM t504a_loaner_transaction t504alt "); sbQuery
	 * .append("  WHERE t504alt.c526_product_request_detail_id IS NOT NULL AND t504alt.c504a_void_fl IS NULL "
	 * ); sbQuery
	 * .append("  GROUP BY t504alt.c504_consignment_id, c526_product_request_detail_id) t504alt "
	 * ); sbQuery.
	 * append("  WHERE t504a.c504_consignment_id = t504alt.c504_consignment_id ");
	 * sbQuery.append("  AND t504a.c504a_status_fl != 60 ");
	 * sbQuery.append("  AND t504a.c504a_void_fl IS NULL) t504alt ");
	 */
    sbQuery.append("  WHERE c901_request_type = 4127 ");
    sbQuery.append("  AND t526.c525_product_request_id = t525.c525_product_request_id ");
    sbQuery.append("  AND t525.c525_void_fl IS NULL ");
    sbQuery.append("  AND t526.c526_void_fl IS NULL ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery
        //.append("  AND t526.c526_product_request_detail_id = t504alt.c526_product_request_detail_id(+) ");
    //sbQuery.append("  AND t504alt.c504_consignment_id = t504acl.c504_consignment_id(+) ");
    sbQuery.append("  And T526.C526_Status_Fl = 5 ");
    sbQuery.append("  And V700.REP_ID  = t525.c703_sales_rep_id ");
    sbQuery.append("  ORDER BY T525.C525_Created_Date ");
    sbQuery.append("  ) A ");
    return sbQuery;
  }

  // To Fetch the Requested Loaner drill down
  public StringBuffer fetchReqLoanerDrillDown(HashMap hmParam) {
    // TODO Auto-generated method stub
    initializeParameters(hmParam);
    ArrayList alReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T526.C526_Status_Fl REQSTATUS, T526.C525_Product_Request_Id reqid, t504acl.c504a_loaner_dt LDATE, ");
    sbQuery.append(" TO_CHAR(T525.C525_Surgery_Date, '" + getCompDateFmt() + "')  Surgdtfmt,");
    sbQuery.append(" TO_CHAR(t504acl.c504a_loaner_dt,'" + getCompDateFmt() + "')  LDATEFMT");
    sbQuery
        .append("  ,  Decode(C526_Status_Fl, '5', 'Requested', 'Approved') stsfl, T525.C525_Surgery_Date Surgdt  ");
    sbQuery.append("  , T526.C207_Set_Id Sid, Get_Set_Name (T526.C207_Set_Id) Sname ");
    sbQuery.append("  , 'Requested' Stsfl, Get_Account_Name(T525.C704_Account_Id) ACCTNAME ");
    sbQuery
        .append("  , GET_REP_NAME(t525.c703_sales_rep_id) repname , 0 daysoverdue , 0 daysellapsed , expretdate ");
    sbQuery
        .append(" , t526.c526_product_request_detail_id reqdtlid, get_lnreq_ext_status(t526.c526_product_request_detail_id)  pendingappr , NVL(t526.c526_extend_count,0) extcount ");
    sbQuery.append(", GET_REP_NAME(t525.c703_ass_rep_id) assocrepname,expretdatefmt ");
    sbQuery.append("  FROM t526_product_request_detail t526 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append("  , t525_product_request t525 , t504a_consignment_loaner t504acl ");
    sbQuery.append("  , (SELECT t504a.c504_consignment_id, c526_product_request_detail_id ");
    sbQuery.append("  , t504a.c504a_expected_return_dt expretdate ");
    sbQuery
        .append("  , NVL (t504a.c504a_expected_return_dt - TRUNC(CURRENT_DATE) , 0) daysoverdue ,NVL (t504a.c504a_loaner_dt - TRUNC(CURRENT_DATE) , 0) daysellapsed ,");
    sbQuery.append(" TO_CHAR (t504a.c504a_expected_return_dt, '" + getCompDateFmt()
        + "') expretdatefmt");
    sbQuery.append("  FROM t504a_consignment_loaner t504a ");
    sbQuery.append("  , (SELECT   t504alt.c504_consignment_id, c526_product_request_detail_id ");
    sbQuery.append("  FROM t504a_loaner_transaction t504alt ");
    sbQuery
        .append("  WHERE t504alt.c526_product_request_detail_id IS NOT NULL AND t504alt.c504a_void_fl IS NULL ");
    sbQuery
        .append("  GROUP BY t504alt.c504_consignment_id, c526_product_request_detail_id) t504alt ");
    sbQuery.append("  WHERE t504a.c504_consignment_id = t504alt.c504_consignment_id ");
    sbQuery.append("  AND t504a.c504a_status_fl != 60 ");
    sbQuery.append("  AND t504a.c504a_void_fl IS NULL) t504alt ");
    sbQuery.append("  WHERE c901_request_type = 4127 ");
    sbQuery.append("  AND t526.c525_product_request_id = t525.c525_product_request_id ");
    sbQuery.append("  AND t525.c525_void_fl IS NULL ");
    sbQuery.append("  AND t526.c526_void_fl IS NULL ");
    sbQuery
        .append("  AND t526.c526_product_request_detail_id = t504alt.c526_product_request_detail_id(+) ");
    sbQuery.append("  AND t504alt.c504_consignment_id = t504acl.c504_consignment_id(+) ");
    sbQuery.append("  And T526.C526_Status_Fl = '5' ");
    sbQuery.append("  And V700.REP_ID  = t525.c703_sales_rep_id ");
    sbQuery.append("  ORDER BY T525.C525_Created_Date ");
    return sbQuery;
  }

  // To Fetch the Due Today Loaner count
  public StringBuffer fetchDueTodayLoanerCount(HashMap hmParam)

  {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);
    String strReqDtlIDs = GmCommonClass.parseNull((String) hmParam.get("REQDTLIDS"));
    sbQuery.append(" SELECT	 'Due Today'  NAME, COUNT (a.returndt) total FROM ( ");
    sbQuery.append(" SELECT   t504a.c504a_expected_return_dt returndt ");
    sbQuery.append(" FROM t504_consignment t504 ");
    sbQuery.append(" , t504a_consignment_loaner t504a ");
    sbQuery.append(" , t504a_loaner_transaction t504b ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" , t907_shipping_info t907   ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    // sbQuery.append(" , t703_sales_rep t703 ");
    // sbQuery.append(" , t704_account t704 ");
    // sbQuery.append(" , t701_distributor t701 ");
    // sbQuery.append(" , t5010_tag t5010 ");
    sbQuery.append(" WHERE t504.c504_status_fl = 4 ");
    sbQuery.append(" AND t504.c504_verify_fl = 1 ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");

    if (!strReqDtlIDs.equals("")) {

      strReqDtlIDs = "'".concat(strReqDtlIDs);
      strReqDtlIDs = strReqDtlIDs.replaceAll(",", "','");
      strReqDtlIDs = strReqDtlIDs.concat("'");

      sbQuery.append(" AND t526.c526_product_request_detail_id in (");
      sbQuery.append(strReqDtlIDs);
      sbQuery.append(") ");

    }

    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    // sbQuery.append(" AND t504b.c704_account_id = t704.c704_account_id ");
    sbQuery.append(" AND t504.c701_distributor_id = v700.d_id  ");
    sbQuery.append(" AND t504b.c504a_return_dt IS NULL ");
    sbQuery.append(" AND t504b.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504a.c504a_status_fl = 20 AND t504a.c504a_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    // sbQuery.append(" AND t525.C525_VOID_FL IS NULL ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    // sbQuery.append(" AND t907.C907_REF_ID = t504.c504_consignment_id ");
    // sbQuery.append(" AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id ");
    // sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    // sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    // sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL ");

    sbQuery.append("  AND (t504a.c504a_expected_return_dt)  = TRUNC (CURRENT_DATE) ");

    // sbQuery.append(" ORDER BY t504a.c504a_expected_return_dt DESC  ");

    sbQuery.append(" )a WHERE   (a.returndt) = TRUNC(CURRENT_DATE) ");
    return sbQuery;
  }

  // To Fetch the Due 1 Day Loaner count
  public StringBuffer fetchDue1DayLoanerCount(HashMap hmParam)

  {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strReqDtlIDs = GmCommonClass.parseNull((String) hmParam.get("REQDTLIDS"));

    sbQuery.append(" SELECT	 'Due in 1 Day'  NAME, COUNT (a.returndt) total FROM ( ");
    sbQuery.append(" SELECT   t504a.c504a_expected_return_dt returndt ");
    sbQuery.append(" FROM t504_consignment t504 ");
    sbQuery.append(" , t504a_consignment_loaner t504a ");
    sbQuery.append(" , t504a_loaner_transaction t504b ");
    sbQuery.append(" , t526_product_request_detail t526 ");
     //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" , t907_shipping_info t907   ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    // sbQuery.append(" , t703_sales_rep t703 ");
    // sbQuery.append(" , t704_account t704 ");
    // sbQuery.append(" , t701_distributor t701 ");
    // sbQuery.append(" , t5010_tag t5010 ");
    sbQuery.append(" WHERE t504.c504_status_fl = 4 ");
    sbQuery.append(" AND t504.c504_verify_fl = 1 ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");

    if (!strReqDtlIDs.equals("")) {

      strReqDtlIDs = "'".concat(strReqDtlIDs);
      strReqDtlIDs = strReqDtlIDs.replaceAll(",", "','");
      strReqDtlIDs = strReqDtlIDs.concat("'");

      sbQuery.append(" AND t526.c526_product_request_detail_id in (");
      sbQuery.append(strReqDtlIDs);
      sbQuery.append(") ");

    }

    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    // sbQuery.append(" AND t504b.c704_account_id = t704.c704_account_id ");
    sbQuery.append(" AND t504.c701_distributor_id = v700.d_id  ");
    sbQuery.append(" AND t504b.c504a_return_dt IS NULL ");
    sbQuery.append(" AND t504b.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504a.c504a_status_fl = 20 AND t504a.c504a_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    // sbQuery.append(" AND t525.C525_VOID_FL IS NULL ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    // sbQuery.append(" AND t907.C907_REF_ID = t504.c504_consignment_id ");
    // sbQuery.append(" AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id ");
    // sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    // sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    //sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL ");
    sbQuery
        .append(" AND t504a.c504a_expected_return_dt  = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),5,TRUNC (CURRENT_DATE) + 3,TRUNC (CURRENT_DATE) + 1) ");
    sbQuery
        .append(" )a WHERE   a.returndt = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),5,TRUNC (CURRENT_DATE) + 3,TRUNC (CURRENT_DATE) + 1) ");
    return sbQuery;
  }

  // To Fetch the Due 2 Day Loaner count
  public StringBuffer fetchDue2DayLoanerCount(HashMap hmParam)

  {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strReqDtlIDs = GmCommonClass.parseNull((String) hmParam.get("REQDTLIDS"));

    sbQuery.append(" SELECT	 'Due in 2 Days'  NAME, COUNT (a.returndt) total FROM ( ");
    sbQuery.append(" SELECT   t504a.c504a_expected_return_dt returndt ");
    sbQuery.append(" FROM t504_consignment t504 ");
    sbQuery.append(" , t504a_consignment_loaner t504a ");
    sbQuery.append(" , t504a_loaner_transaction t504b ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" , t907_shipping_info t907   ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    // sbQuery.append(" , t703_sales_rep t703 ");
    // sbQuery.append(" , t704_account t704 ");
    // sbQuery.append(" , t701_distributor t701 ");
    // sbQuery.append(" , t5010_tag t5010 ");
    sbQuery.append(" WHERE t504.c504_status_fl = 4 ");
    sbQuery.append(" AND t504.c504_verify_fl = 1 ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");

    if (!strReqDtlIDs.equals("")) {

      strReqDtlIDs = "'".concat(strReqDtlIDs);
      strReqDtlIDs = strReqDtlIDs.replaceAll(",", "','");
      strReqDtlIDs = strReqDtlIDs.concat("'");

      sbQuery.append(" AND t526.c526_product_request_detail_id in (");
      sbQuery.append(strReqDtlIDs);
      sbQuery.append(") ");

    }

    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    // sbQuery.append(" AND t504b.c704_account_id = t704.c704_account_id ");
    sbQuery.append(" AND t504.c701_distributor_id = v700.d_id  ");
    sbQuery.append(" AND t504b.c504a_return_dt IS NULL ");
    sbQuery.append(" AND t504b.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504a.c504a_status_fl = 20 AND t504a.c504a_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    // sbQuery.append(" AND t525.C525_VOID_FL IS NULL ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" AND t907.C907_REF_ID = t504.c504_consignment_id ");
    //sbQuery.append(" AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id ");
    //sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    //sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    //sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL ");
    sbQuery
        .append("  AND   ( t504a.c504a_expected_return_dt )  = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),4, TRUNC (CURRENT_DATE) + 4, 5,TRUNC (CURRENT_DATE) + 4,TRUNC (CURRENT_DATE) + 2) ");
    sbQuery
        .append(" )a WHERE   (a.returndt) = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),4, TRUNC (CURRENT_DATE) + 4, 5,TRUNC (CURRENT_DATE) + 4,TRUNC (CURRENT_DATE) + 2) ");
    return sbQuery;
  }

  // To Fetch the OverDue Loaner count
  public StringBuffer fetchOverDueLoanerCount(HashMap hmParam)

  {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strReqDtlIDs = GmCommonClass.parseNull((String) hmParam.get("REQDTLIDS"));
    sbQuery.append(" SELECT	 'Overdue'  NAME, COUNT (a.returndt) total	FROM ( ");
    sbQuery.append(" SELECT   t504a.c504a_expected_return_dt returndt ");
    sbQuery.append(" FROM t504_consignment t504 ");
    sbQuery.append(" , t504a_consignment_loaner t504a ");
    sbQuery.append(" , t504a_loaner_transaction t504b ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" , t907_shipping_info t907   ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    // sbQuery.append(" , t703_sales_rep t703 ");
    // sbQuery.append(" , t704_account t704 ");
    // sbQuery.append(" , t701_distributor t701 ");
    // sbQuery.append(" , t5010_tag t5010 ");
    sbQuery.append(" WHERE t504.c504_status_fl = 4 ");
    sbQuery.append(" AND t504.c504_verify_fl = 1 ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");

    if (!strReqDtlIDs.equals("")) {

      strReqDtlIDs = "'".concat(strReqDtlIDs);
      strReqDtlIDs = strReqDtlIDs.replaceAll(",", "','");
      strReqDtlIDs = strReqDtlIDs.concat("'");

      sbQuery.append(" AND t526.c526_product_request_detail_id in (");
      sbQuery.append(strReqDtlIDs);
      sbQuery.append(") ");

    }

    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    // sbQuery.append(" AND t504b.c704_account_id = t704.c704_account_id ");
    sbQuery.append(" AND t504.c701_distributor_id = v700.d_id  ");
    sbQuery.append(" AND t504b.c504a_return_dt IS NULL ");
    sbQuery.append(" AND t504b.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504a.c504a_status_fl = 20 AND t504a.c504a_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    // sbQuery.append(" AND t525.C525_VOID_FL IS NULL ");
   //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" AND t907.C907_REF_ID = t504.c504_consignment_id ");
    //sbQuery.append(" AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id ");
    //sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    //sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    //sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL ");
    sbQuery.append(" AND  ( t504a.c504a_expected_return_dt )  < TRUNC (CURRENT_DATE) ");
    sbQuery.append(" )a WHERE   (a.returndt) < TRUNC (CURRENT_DATE) ");
    return sbQuery;
  }

  // To Fetch the Shipped (All Loaner) Loaner count
  public StringBuffer fetchShippedLoanerCount(HashMap hmParam)

  {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strReqDtlIDs = GmCommonClass.parseNull((String) hmParam.get("REQDTLIDS"));
    sbQuery.append(" SELECT	 'All Loaners'  NAME, COUNT (a.returndt) total	FROM ( ");
    sbQuery
        .append(" SELECT t504a.c504a_expected_return_dt returndt ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" SELECT t504a.c504a_expected_return_dt returndt, t907.C907_SHIPPED_DT shipdate "); 
    sbQuery.append(" FROM t504_consignment t504 ");
    sbQuery.append(" , t504a_consignment_loaner t504a ");
    sbQuery.append(" , t504a_loaner_transaction t504b ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    //sbQuery.append(" , t907_shipping_info t907   ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    // sbQuery.append(" , t703_sales_rep t703 ");
    // sbQuery.append(" , t704_account t704 ");
    // sbQuery.append(" , t701_distributor t701 ");
    // sbQuery.append(" , t5010_tag t5010 ");
    sbQuery.append(" WHERE t504.c504_status_fl = 4 ");
    sbQuery.append(" AND t504.c504_verify_fl = 1 ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");

    if (!strReqDtlIDs.equals("")) {

      strReqDtlIDs = "'".concat(strReqDtlIDs);
      strReqDtlIDs = strReqDtlIDs.replaceAll(",", "','");
      strReqDtlIDs = strReqDtlIDs.concat("'");

      sbQuery.append(" AND t526.c526_product_request_detail_id in (");
      sbQuery.append(strReqDtlIDs);
      sbQuery.append(") ");

    }

    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    // sbQuery.append(" AND t504b.c704_account_id = t704.c704_account_id ");
    sbQuery.append(" AND t504.c701_distributor_id = v700.d_id  ");
    sbQuery.append(" AND t504b.c504a_return_dt IS NULL ");
    sbQuery.append(" AND t504b.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504a.c504a_status_fl = 20 AND t504a.c504a_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    // sbQuery.append(" AND t525.C525_VOID_FL IS NULL ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    // sbQuery.append(" AND t907.C907_REF_ID = t504.c504_consignment_id ");
    // sbQuery.append(" AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id ");
    // sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    // sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    // sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL ");
    sbQuery.append("  AND t504b.c504a_expected_return_dt is NOT NULL ");
    //sbQuery.append("  AND t504b.c504a_expected_return_dt >= TRUNC(current_date) ");		
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append("  AND trunc(t907.C907_SHIPPED_DT)  <= TRUNC (CURRENT_DATE) ");
    //sbQuery.append(" )a WHERE   (a.shipdate) <=  (CURRENT_DATE) "); 
    sbQuery.append(" )a ");
    //sbQuery.append(" )a WHERE (a.returndt) >= TRUNC(CURRENT_DATE) ");
    return sbQuery;
  }

  // To Fetch the Loaner drill down ( Due Today, Due 1 Day, Due 2 day, Overdue, Shipped)
  public StringBuffer fetchDueLoanerDrillDown(HashMap hmParam, String strName)

  {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
    String strReqDtlIDs = GmCommonClass.parseNull((String) hmParam.get("REQDTLIDS"));
    sbQuery
        .append(" SELECT   t526.c525_product_request_id reqid, t504a.c504a_expected_return_dt returndt, t504.c207_set_id sid ");
    sbQuery.append(" , get_set_name (t504.c207_set_id) sname, v700.rep_name repname ");
    sbQuery.append(" , get_account_name (t504b.c704_account_id) acctname, 'Pending Return' stsfl ");
    sbQuery
        .append(" , DECODE (t504a.c504a_status_fl, 20, TRUNC (CURRENT_DATE) - t504a.c504a_loaner_dt) daysellapsed ");
    sbQuery
        .append(" , get_work_days_count(TO_CHAR(TRUNC(t504a.c504a_expected_return_dt),'MM/dd/yyyy') ,TO_CHAR(TRUNC(CURRENT_DATE),'MM/dd/yyyy')) daysoverdue ");
    sbQuery.append(" , t504a.c504a_expected_return_dt expretdate ");
    sbQuery.append(" , t504a.c504a_loaner_dt ldate ,");
    sbQuery.append(" TO_CHAR(t504a.c504a_loaner_dt,'" + getCompDateFmt() + "')  LDATEFMT,");
    sbQuery.append(" TO_CHAR (t504a.c504a_expected_return_dt, '" + getCompDateFmt()
        + "') expretdatefmt");
    sbQuery
        .append(" , DECODE (t504a.c504a_expected_return_dt, TRUNC (CURRENT_DATE) + 2, 'Y', 'N') isdue ");
    sbQuery.append(" , t907.C907_SHIPPED_DT shipdate");
    sbQuery.append(", GET_REP_NAME(t504b.c703_ass_rep_id ) assocrepname ");
    sbQuery.append(" ,DECODE (t504a.c504a_expected_return_dt - TRUNC (CURRENT_DATE )");
    sbQuery.append(" , 1, 'Due in 1 Day' ");
    sbQuery.append(" , 2, 'Due in 2 Days' ");
    sbQuery.append(" , 'Over Due' ");
    sbQuery.append(" ) NAME ");
    sbQuery
        .append(" , t907.c901_delivery_carrier CARRIERID,DECODE (t907.C907_Tracking_Number, Null, 'Y', t907.C907_Tracking_Number) TRACK ");
    sbQuery
        .append(" , get_rule_value(t907.c901_delivery_carrier,'SHIP_CARIER_TRCK_URL')  TRACKURL ");
    // The below function returns the Tag id with ',' separator if CN having
    // multiple tag ID's.
    sbQuery.append(" , t504a.c504a_etch_id etchid  ");
    // sbQuery.append(" ,gm_pkg_op_request_master.get_tag_id (t504.c504_consignment_id) tagid ");
    sbQuery.append(" , tag.tag_id tagid  ");
    sbQuery
        .append(" , t526.c526_product_request_detail_id reqdtlid, get_lnreq_ext_status(t526.c526_product_request_detail_id)  pendingappr , NVL(t526.c526_extend_count,0) extcount ");
    if (!strReqDtlIDs.equals("")) {
      sbQuery
          .append(" , t504.c504_consignment_id consignid, get_new_new_expdt_str(t504a.c504a_expected_return_dt) newextndate ");
      sbQuery
          .append(" , NVL(gm_pkg_op_loaner_extension.get_parent_lnrextn_cnt(t526.c525_product_request_id ),0) parentextncnt ");
    }

    sbQuery.append(" FROM t504_consignment t504 ");
    sbQuery.append(" , t504a_consignment_loaner t504a ");
    sbQuery.append(" , t504a_loaner_transaction t504b ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    sbQuery.append(" , t907_shipping_info t907   ");
    sbQuery
        .append(" ,(  SELECT trans_id, RTRIM (XMLAGG (xmlelement (E, tag_id || ', ')) .extract ('//text()'), ' , ') tag_id ");
    sbQuery.append("  FROM (  SELECT C5010_TAG_ID TAG_ID, C5010_LAST_UPDATED_TRANS_ID TRANS_ID ");
    sbQuery
        .append("   FROM T5010_TAG   WHERE C901_INVENTORY_TYPE = 10004 AND C5010_VOID_FL    IS NULL  ) ");
    sbQuery.append("   GROUP BY trans_id  )  tag ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    // sbQuery.append(" , t703_sales_rep t703 ");
    // sbQuery.append(" , t704_account t704 ");
    // sbQuery.append(" , t701_distributor t701 ");
    // sbQuery.append(" , t5010_tag t5010 ");
    sbQuery.append(" WHERE t504.c504_status_fl = '4' ");
    sbQuery.append(" AND t504.c504_verify_fl = '1' ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");

    if (!strReqDtlIDs.equals("")) {

      strReqDtlIDs = "'".concat(strReqDtlIDs);
      strReqDtlIDs = strReqDtlIDs.replaceAll(",", "','");
      strReqDtlIDs = strReqDtlIDs.concat("'");

      sbQuery.append(" AND t526.c526_product_request_detail_id in (");
      sbQuery.append(strReqDtlIDs);
      sbQuery.append(") ");

    }

    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    // sbQuery.append(" AND t504b.c704_account_id = t704.c704_account_id ");
    sbQuery.append(" AND t504.c701_distributor_id = v700.d_id  ");
    sbQuery.append(" AND t504b.c504a_return_dt IS NULL ");
    sbQuery.append(" AND t504b.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504a.c504a_status_fl = '20' AND t504a.c504a_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    // sbQuery.append(" AND t525.C525_VOID_FL IS NULL ");
    sbQuery.append(" AND t907.C907_REF_ID = t504.c504_consignment_id ");
    sbQuery.append(" AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id ");
    sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    sbQuery.append(" AND T504.C504_CONSIGNMENT_ID  = TAG.TRANS_ID(+) ");
    sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL ");
    if (strPassName.equals("Due1Day")) {

      sbQuery
          .append("  AND TRUNC ( t504a.c504a_expected_return_dt )  = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),5,TRUNC (CURRENT_DATE) + 3,TRUNC (CURRENT_DATE) + 1) ");

    }
    if (strPassName.equals("Due2Day")) {

      sbQuery
          .append("  AND TRUNC ( t504a.c504a_expected_return_dt )  = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),4, TRUNC (CURRENT_DATE) + 4, 5,TRUNC (CURRENT_DATE) + 4,TRUNC (CURRENT_DATE) + 2) ");

    }
    if (strPassName.equals("OverDue")) {

      sbQuery.append(" AND TRUNC ( t504a.c504a_expected_return_dt )  < TRUNC (CURRENT_DATE) ");
    }
    if (strPassName.equals("Shipped")) {

      sbQuery.append("  AND trunc(t907.C907_SHIPPED_DT)  <= TRUNC (CURRENT_DATE) ");
    }
    if (strPassName.equals("DueToday")) {

      sbQuery.append("  AND trunc(t504a.c504a_expected_return_dt)  = TRUNC (CURRENT_DATE) ");
    }

    // commented this code for PMT-2202.
    // The below lines are provide duplicate values if CN having multiple
    // Tag ID's.
    /*
     * sbQuery.append( " AND t504.c504_consignment_id = t5010.c5010_last_updated_trans_id " );
     * sbQuery.append(" AND t5010.c5010_void_fl IS NULL ");
     * sbQuery.append(" AND t5010.c901_INVENTORY_TYPE = 10004 "); sbQuery.append
     * (" AND t504.c701_distributor_id =  t5010.c5010_location_id ");
     */
    sbQuery.append(" ORDER BY t504a.c504a_expected_return_dt DESC  ");

    if (strName.equals("DueToday")) {
      sbQuery.append(" )a WHERE   TRUNC(a.returndt) = TRUNC(CURRENT_DATE) ");
    }
    if (strName.equals("Due1Day")) {
      sbQuery
          .append(" )a WHERE   TRUNC(a.returndt) = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),5,TRUNC (CURRENT_DATE) + 3,TRUNC (CURRENT_DATE) + 1) ");
    }

    if (strName.equals("Due2Day")) {
      sbQuery
          .append(" )a WHERE   TRUNC(a.returndt) = Decode(TO_CHAR (MOD (TO_CHAR (CURRENT_DATE, 'J'), 7) + 1),4, TRUNC (CURRENT_DATE) + 4, 5,TRUNC (CURRENT_DATE) + 4,TRUNC (CURRENT_DATE) + 2) ");
    }

    if (strName.equals("OverDue")) {
      sbQuery.append(" )a WHERE   TRUNC(a.returndt) < TRUNC (CURRENT_DATE) ");
    }
    if (strName.equals("Shipped")) {
      sbQuery.append(" )a WHERE   TRUNC(a.shipdate) <= TRUNC (CURRENT_DATE) ");
    }

    return sbQuery;
  }

  // To Fetch the Missing part loaner count
  public StringBuffer fetchMissingPartLoanerCount(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT 'Missing Parts' NAME, COUNT (1) total FROM (SELECT  NVL (qtymiss, 0) QTYMISS FROM ");
    sbQuery.append(" (SELECT t413.c205_part_number_id pnum ");
    sbQuery.append(" , SUM (DECODE (t413.c413_status_fl, 'Q', (t413.c413_item_qty), 0)) qtymiss ");
    sbQuery.append(" , SUM (DECODE (t413.c413_status_fl, 'Q', 0, t413.c413_item_qty)) qtyrecon ");
    sbQuery.append(" , SUM (DECODE (t413.c413_status_fl, 'S', t413.c413_item_qty, 0)) qtysold ");
    sbQuery.append(" , t504.c207_set_id setid ");
    sbQuery.append(" , v700.d_name fldsls, t504a.c703_sales_rep_id repid ");
    sbQuery.append(" , t504a.c704_account_id acctid ");
    sbQuery.append(" FROM t504a_loaner_transaction t504a ");
    sbQuery.append(" , t412_inhouse_transactions t412 ");
    sbQuery.append(" , t413_inhouse_trans_items t413 ");
    sbQuery.append(" , t504_consignment t504 ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    // sbQuery.append(" , t525_product_request t525 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id ");
    sbQuery.append(" AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504a.c504a_consigned_to_id = t504a.c504a_consigned_to_id ");
    sbQuery
        .append(" AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+) ");
    // sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id(+) ");
    sbQuery.append(" AND t504a.c504a_consigned_to_id = v700.d_id ");
    // sbQuery.append(" AND t504a.c901_consigned_to IN (50170, 50172) ");
    sbQuery.append(" AND t412.c412_type = 50159 ");
    // sbQuery.append(" AND t412.c412_inhouse_purpose IN (50127, 50107) ");
    sbQuery.append(" AND t504a.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t412.c412_void_fl IS NULL ");
    sbQuery.append(" AND t413.c413_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_void_fl IS NULL ");
    sbQuery.append(" AND t526.c526_void_fl IS NULL ");
    sbQuery.append(" AND t412.c412_status_fl <> 4 ");
    sbQuery.append(" GROUP BY t526.c525_product_request_id ");
    sbQuery.append(" , t413.c205_part_number_id ");
    sbQuery.append(" , t412.c412_inhouse_trans_id ");
    sbQuery.append(" , t504a.c504a_consigned_to_id ");
    sbQuery.append(" , t504.c207_set_id ");
    sbQuery.append(" , v700.d_name ");
    sbQuery.append(" , t504a.c703_sales_rep_id ");
    sbQuery.append(" , t504a.c704_account_id ");
    sbQuery.append(" , t504a.c703_ass_rep_id  ");
    sbQuery.append(" ) req_dtl ");
    sbQuery.append(" WHERE (qtysold <> 0 OR qtymiss <> 0) AND qtymiss - qtyrecon > 0 )");
    return sbQuery;
  }

  // To Fetch the Missing part loaner drill down
  public StringBuffer fetchMissingPartLoanerDrillDown(HashMap hmParam)

  {
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    initializeParameters(hmParam);


    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT REQID, PNUM, t205.C205_PART_NUM_DESC PDESC, NVL (qtymiss, 0) QTYMISS ");
    sbQuery.append(" , TRANSID, SETID, C207_SET_NM SNAME, FLDSLS, " + strRepNmColumn
        + " REPNAME,  " + strAccountNmColumn + " ACCTNAME, assocrepname ");
    sbQuery
        .append(" FROM (SELECT	 t526.c525_product_request_id reqid, t413.c205_part_number_id pnum ");
    sbQuery.append(" , t412.c412_inhouse_trans_id transid ");
    sbQuery.append(" , SUM (DECODE (t413.c413_status_fl, 'Q', (t413.c413_item_qty), 0)) qtymiss ");
    sbQuery.append(" , SUM (DECODE (t413.c413_status_fl, 'Q', 0, t413.c413_item_qty)) qtyrecon ");
    sbQuery.append(" , SUM (DECODE (t413.c413_status_fl, 'S', t413.c413_item_qty, 0)) qtysold ");
    sbQuery
        .append(" , t526.c526_product_request_detail_id reqdtlid, get_lnreq_ext_status(t526.c526_product_request_detail_id)  pendingappr , NVL(t526.c526_extend_count,0) extcount ");
    sbQuery.append(" , t504a.c504a_consigned_to_id conid ");
    sbQuery.append(" , t504.c207_set_id setid ");
    sbQuery.append(" , v700.d_name fldsls, t504a.c703_sales_rep_id repid ");
    sbQuery.append(", GET_REP_NAME(t504a.c703_ass_rep_id) assocrepname ");
    sbQuery.append(" , t504a.c704_account_id acctid ");
    sbQuery.append(" FROM t504a_loaner_transaction t504a ");
    sbQuery.append(" , t412_inhouse_transactions t412 ");
    sbQuery.append(" , t413_inhouse_trans_items t413 ");
    sbQuery.append(" , t504_consignment t504 ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    // sbQuery.append(" , t525_product_request t525 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id ");
    sbQuery.append(" AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504a.c504a_consigned_to_id = t504a.c504a_consigned_to_id ");
    sbQuery
        .append(" AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+) ");
    // sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id(+) ");
    sbQuery.append(" AND t504a.c504a_consigned_to_id = v700.d_id ");
    sbQuery.append(" AND t504a.c901_consigned_to IN (50170, 50172) ");
    sbQuery.append(" AND t412.c412_type = 50159 ");
    sbQuery.append(" AND t412.c412_inhouse_purpose IN (50127, 50107) ");
    sbQuery.append(" AND t504a.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t412.c412_void_fl IS NULL ");
    sbQuery.append(" AND t413.c413_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    // sbQuery.append(" AND t525.c525_void_fl IS NULL ");
    sbQuery.append(" AND t526.c526_void_fl IS NULL ");
    sbQuery.append(" AND t412.c412_status_fl <> 4 ");
    sbQuery.append(" GROUP BY t526.c525_product_request_id ");
    sbQuery.append(" , t413.c205_part_number_id ");
    sbQuery.append(" , t412.c412_inhouse_trans_id ");
    sbQuery.append(" , t504a.c504a_consigned_to_id ");
    sbQuery.append(" , t504.c207_set_id ");
    sbQuery.append(" , v700.d_name ");
    sbQuery.append(" , t504a.c703_sales_rep_id ");
    sbQuery.append(" , t504a.c704_account_id ");
    sbQuery
        .append(" , t504a.c703_ass_rep_id ,  t526.c526_product_request_detail_id, get_lnreq_ext_status(t526.c526_product_request_detail_id),  NVL (t526.c526_extend_count, 0)");
    sbQuery.append(" ) req_dtl, ");
    sbQuery.append(" T205_PART_NUMBER t205, ");
    sbQuery.append(" T207_SET_MASTER t207, ");
    sbQuery.append(" t704_account t704, ");
    sbQuery.append(" t703_sales_rep t703 ");
    sbQuery.append(" WHERE (qtysold <> 0 OR qtymiss <> 0) AND qtymiss - qtyrecon > 0 ");
    sbQuery.append(" and t205.c205_part_number_id = req_dtl.pnum ");
    sbQuery.append(" and t207.C207_SET_ID = req_dtl.setid ");
    sbQuery.append(" and req_dtl.acctid = t704.c704_account_id(+) ");
    sbQuery.append(" and req_dtl.repid = t703.c703_sales_rep_id(+) ");
    return sbQuery;
  }

  // To Fetch the Approved Loaner count
  public StringBuffer fetchApprLoanerCount(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" Select   'Approved'  name, count(A.REQSTATUS) total  from (Select T526.C526_Status_Fl REQSTATUS ");
    sbQuery.append(" FROM t526_product_request_detail t526 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery
        .append(" , t525_product_request t525 ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" , t525_product_request t525, t907_shipping_info t907, t504a_consignment_loaner t504acl ");
	/*
	 * sbQuery.
	 * append(" , (SELECT t504a.c504_consignment_id, c526_product_request_detail_id "
	 * ); sbQuery
	 * .append(" , NVL (t504a.c504a_expected_return_dt - TRUNC(CURRENT_DATE) , 0) daysoverdue ,NVL (t504a.c504a_loaner_dt - TRUNC(CURRENT_DATE) , 0) "
	 * ); sbQuery.append(" daysellapsed ");
	 * sbQuery.append(" FROM t504a_consignment_loaner t504a "); sbQuery.
	 * append(" , (SELECT   t504alt.c504_consignment_id, c526_product_request_detail_id "
	 * ); sbQuery.append(" FROM t504a_loaner_transaction t504alt "); sbQuery
	 * .append(" WHERE t504alt.c526_product_request_detail_id IS NOT NULL AND t504alt.c504a_void_fl IS NULL ) t504alt "
	 * ); // sbQuery.
	 * append(" GROUP BY t504alt.c504_consignment_id, c526_product_request_detail_id "
	 * ); sbQuery.
	 * append(" WHERE t504a.c504_consignment_id = t504alt.c504_consignment_id ");
	 * sbQuery.append(" AND t504a.c504a_status_fl != 60 ");
	 * sbQuery.append(" AND t504a.c504a_void_fl IS NULL) t504alt ");
	 */
    sbQuery.append(" WHERE c901_request_type = 4127 ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" AND t907.c901_source = 50185 ");
    sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery.append(" AND t525.c525_product_request_id = t907.c907_ref_id(+) ");
    sbQuery.append(" AND t525.c525_void_fl IS NULL ");
    sbQuery.append(" AND t526.c526_void_fl IS NULL ");
    //This Code is commented For Sales Dashboard  Loaner Widget Query Performance Improvement(PC-5706)
    //sbQuery
        //.append(" AND t526.c526_product_request_detail_id = t504alt.c526_product_request_detail_id(+) ");
    //sbQuery.append(" AND t504alt.c504_consignment_id = t504acl.c504_consignment_id(+) ");
    //sbQuery.append(" And T907.C907_Void_Fl  Is Null ");
    sbQuery.append(" And T526.C526_Status_Fl In (10,20 ) ");
    sbQuery.append("  And V700.REP_ID  = t525.c703_sales_rep_id ) A ");
    // sbQuery.append(" Order By T525.C525_Created_Date) A  ");
    return sbQuery;
  }

  // To Fetch the Approved Loaner drill down
  public StringBuffer fetchApprLoanerDrillDown(HashMap hmParam) {

    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" Select	   T526.C526_Status_Fl REQSTATUS, T526.C525_Product_Request_Id reqid, t504acl.c504a_loaner_dt LDATE,  Decode(C526_Status_Fl, '5', 'Requested', 'Approved') STSFL, ");
    sbQuery.append(" T525.C525_Surgery_Date  Surgdt, ");
    sbQuery.append(" TO_CHAR(T525.C525_Surgery_Date, '" + getCompDateFmt() + "')  Surgdtfmt,");
    sbQuery.append(" TO_CHAR(t504acl.c504a_loaner_dt,'" + getCompDateFmt() + "')  LDATEFMT");
    sbQuery
        .append(" , T526.C207_Set_Id Sid, Get_Set_Name (T526.C207_Set_Id) sname , T907.C907_Shipped_Dt shipdate");
    sbQuery.append(" , 'Approved' Stsfl, Get_Account_Name(T525.C704_Account_Id) Acctname, ");
    sbQuery
        .append(" GET_REP_NAME(t525.c703_sales_rep_id) repname , 0 daysoverdue , 0 daysellapsed , t504acl.c504a_expected_return_dt expretdate ,");
    sbQuery.append(" TO_CHAR (t504acl.c504a_expected_return_dt, '" + getCompDateFmt()
        + "') expretdatefmt");
    sbQuery
        .append(" , t526.c526_product_request_detail_id reqdtlid, get_lnreq_ext_status(t526.c526_product_request_detail_id)  pendingappr , NVL(t526.c526_extend_count,0) extcount ");
    sbQuery.append(", GET_REP_NAME(t525.c703_ass_rep_id) assocrepname ");
    sbQuery.append(" FROM t526_product_request_detail t526 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery
        .append(" , t525_product_request t525, t907_shipping_info t907, t504a_consignment_loaner t504acl ");
    sbQuery.append(" , (SELECT t504a.c504_consignment_id, c526_product_request_detail_id ");
    sbQuery
        .append(" , NVL (t504a.c504a_expected_return_dt - TRUNC(CURRENT_DATE) , 0) daysoverdue ,NVL (t504a.c504a_loaner_dt - TRUNC(CURRENT_DATE) , 0) ");
    sbQuery.append(" daysellapsed ");
    sbQuery.append(" FROM t504a_consignment_loaner t504a ");
    sbQuery.append(" , (SELECT   t504alt.c504_consignment_id, c526_product_request_detail_id ");
    sbQuery.append(" FROM t504a_loaner_transaction t504alt ");
    sbQuery
        .append(" WHERE t504alt.c526_product_request_detail_id IS NOT NULL AND t504alt.c504a_void_fl IS NULL ");
    sbQuery
        .append(" GROUP BY t504alt.c504_consignment_id, c526_product_request_detail_id) t504alt ");
    sbQuery.append(" WHERE t504a.c504_consignment_id = t504alt.c504_consignment_id ");
    sbQuery.append(" AND t504a.c504a_status_fl != 60 ");
    sbQuery.append(" AND t504a.c504a_void_fl IS NULL) t504alt ");
    sbQuery.append(" WHERE c901_request_type = 4127 ");
    //This code change for BUG-14874(Loaner Widget Approved details drill-down not match with count) in PC-5706.
    sbQuery.append(" AND t907.c901_source (+) = 50185 ");
    sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id ");
    sbQuery.append(" AND t525.c525_product_request_id = t907.c907_ref_id(+) ");
    sbQuery.append(" AND t525.c525_void_fl IS NULL ");
    sbQuery.append(" AND t526.c526_void_fl IS NULL ");
    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504alt.c526_product_request_detail_id(+) ");
    sbQuery.append(" AND t504alt.c504_consignment_id = t504acl.c504_consignment_id(+) ");
    sbQuery.append(" And T907.C907_Void_Fl (+) Is Null ");
    sbQuery.append(" And T526.C526_Status_Fl In (10,20 ) ");
    sbQuery.append("  And V700.REP_ID  = t525.c703_sales_rep_id ");
    sbQuery.append(" Order By T525.C525_Created_Date ");
    return sbQuery;

  }

/**
	 * fetchAllLoanerDueCnt- This method called from Portal and Device and
	 * it is used to Fetch loaner due counts in sales dashboard loaner widget
	 * 
	 * 
	 * @param hmParam
	 * @return StringBuffer
	 *            
	 */
  public StringBuffer fetchAllLoanerDueCnt(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strReqDtlIDs = GmCommonClass.parseNull((String) hmParam.get("REQDTLIDS"));
    sbQuery.append(" SELECT due_by NAME, count(1) TOTAL FROM  ( ");
    sbQuery
        .append(" SELECT  decode(to_char (mod (to_char (CURRENT_DATE, 'J'), 7) + 1),5,trunc (CURRENT_DATE) + 3,trunc (CURRENT_DATE) + 1) over_due_1, ");
    sbQuery
        .append(" decode(to_char (mod (to_char (CURRENT_DATE, 'J'), 7) + 1),4, trunc (CURRENT_DATE) + 4, 5,trunc (CURRENT_DATE) + 4,trunc (CURRENT_DATE) + 2) over_due_2,  ");
    sbQuery.append(" (A.returndt) dates, ");
    sbQuery.append(" CASE WHEN A.returndt < trunc (CURRENT_DATE) THEN 'Overdue' ELSE  ");
    sbQuery.append("  decode (A.returndt,trunc (CURRENT_DATE), 'Due Today', ");
    sbQuery
        .append("  decode(to_char (mod (to_char (CURRENT_DATE, 'J'), 7) + 1),4, trunc (CURRENT_DATE) + 4, 5,trunc (CURRENT_DATE) + 4,trunc (CURRENT_DATE) + 2),'Due in 2 Days',  ");
    sbQuery
        .append("  decode(to_char (mod (to_char (CURRENT_DATE, 'J'), 7) + 1),5,trunc (CURRENT_DATE) + 3,trunc (CURRENT_DATE) + 1),'Due in 1 Day') END due_by ");
    sbQuery.append(" FROM (SELECT t504a.c504a_expected_return_dt returndt ");
    sbQuery.append(" FROM t504_consignment t504 , ");
    sbQuery.append(" t504a_consignment_loaner t504a , ");
    sbQuery.append(" t504a_loaner_transaction t504b , ");
    sbQuery.append(" t526_product_request_detail t526 , ");
    sbQuery.append(" t907_shipping_info t907  ");

    sbQuery.append(getAccessFilterClauseWithRepID());

    sbQuery.append(" WHERE t504.c504_status_fl = '4' ");
    sbQuery.append(" AND t504.c504_verify_fl = '1' ");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");

    if (!strReqDtlIDs.equals("")) {

      strReqDtlIDs = "'".concat(strReqDtlIDs);
      strReqDtlIDs = strReqDtlIDs.replaceAll(",", "','");
      strReqDtlIDs = strReqDtlIDs.concat("'");

      sbQuery.append(" AND t526.c526_product_request_detail_id in (");
      sbQuery.append(strReqDtlIDs);
      sbQuery.append(") ");

    }

    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504b.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.C526_VOID_FL is null");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504.c701_distributor_id = v700.d_id  ");
    sbQuery.append(" AND t504b.c504a_return_dt IS NULL ");
    sbQuery.append(" AND t504b.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t504b.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t504a.c504a_status_fl = '20' AND t504a.c504a_void_fl IS NULL ");
    sbQuery.append(" AND t907.C907_REF_ID = t504.c504_consignment_id ");
    sbQuery.append(" AND T526.C525_Product_Request_Id = T907.C525_Product_Request_Id ");
    sbQuery.append(" AND T907.C907_STATUS_FL  = '40'");
    sbQuery.append(" AND T907.C907_VOID_FL IS NULL");
    sbQuery.append(" AND T907.C907_Shipped_Dt IS NOT NULL ");
    sbQuery.append(" )A )WHERE   due_by IS NOT NULL GROUP BY due_by  ");

    return sbQuery;
  }

}
