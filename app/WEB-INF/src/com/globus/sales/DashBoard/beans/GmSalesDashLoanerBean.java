package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.lang.Enum;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author gpalani
 *
 */
/**
 * @author gpalani
 *
 */
public class GmSalesDashLoanerBean extends GmSalesFilterConditionBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	GmSalesDashLoanerBeanQuery gmLoanerQuery = new GmSalesDashLoanerBeanQuery();

	public GmSalesDashLoanerBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	public GmSalesDashLoanerBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * Count and Drill down calls are separated into two methods.
	 * Removed the unwanted the rows and where condition check while fetching the count.
	 * Database Object Creation - Changed the object creation one per class instead of one per method.
	 * Methods Modified: fetchReqLoaner, fetchApprLoaner, fetchDuedayLoaner, fetchMissingPartLoaner
	 * @author gpalani PMT-10095 (RFC2252)
	 * @since 2017-01-03
	 */
	

	public ArrayList fetchReqLoaner(HashMap hmParam) throws AppError {
		ArrayList alReturn = null;
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));
		StringBuffer sbQuery = new StringBuffer();

		// String strApplDateFmt =
		// GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT",
		// "DATEFORMAT"));
		// If condition will be executed if the request is only for getting the
		// count
	
				
		if (!strPassName.equals("Requested")) {
			sbQuery = gmLoanerQuery.fetchReqLoanercount(hmParam);

		}
		// else condition will be executed if the request is to get the drill
		// down values.
		else if (strPassName.equals("Requested"))

		{
			sbQuery = gmLoanerQuery.fetchReqLoanerDrillDown(hmParam);
		}

		log.debug("sbQuery Requested loaners===>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	public ArrayList fetchApprLoaner(HashMap hmParam) throws AppError {

		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));
		// String strApplDateFmt =
		// GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT",
		// "DATEFORMAT"));

		/*
		 * Count and Drill down calls are separated as two methods. Removed the
		 * unwanted the rows and where condition check while fetching the count
		 * Moved the Object creation per method to Object per class. Author:
		 * gpalani PMT-10095
		 */

		if (!strPassName.equals("Approved")) {
			sbQuery = gmLoanerQuery.fetchApprLoanerCount(hmParam);
		}

		else if (strPassName.equals("Approved")) {
			sbQuery = gmLoanerQuery.fetchApprLoanerDrillDown(hmParam);

		}
		
		log.debug("sbQuery Fetch missing parts loaners===>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;

	}

	public ArrayList fetchDuedayLoaner(HashMap hmParam, String strName)
			throws AppError {

		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();

		// String strApplDateFmt =

		// GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT",
		// "DATEFORMAT"));
		// Adding below condition To Display Due Today Drill down in Sales
		// Dashboard Under Loaner
		// Section

		/*
		 * Count and Drill down calls are separated as two methods. Removed the
		 * unwanted the rows and where condition check while fetching the count
		 * Moved the Object creation per method to Object per class. Author:
		 * gpalani PMT-10095
		 

	// Below code was commented for PMT-27245, to fetch all loaner due counts in single query
		if (strName.equals("DueToday")) {
			sbQuery = gmLoanerQuery.fetchDueTodayLoanerCount(hmParam);
		}
		if (strName.equals("Due1Day")) {
			sbQuery = gmLoanerQuery.fetchDue1DayLoanerCount(hmParam);

		}

		if (strName.equals("Due2Day")) {
			sbQuery = gmLoanerQuery.fetchDue2DayLoanerCount(hmParam);

		}

		if (strName.equals("OverDue")) {
			sbQuery = gmLoanerQuery.fetchOverDueLoanerCount(hmParam);

		}*/
		
		// To fetch DueToday,Due1Day,Due2Day,OverDue loaner count
		  if (strName.equals("AllLoanerDue")) {
		      sbQuery = gmLoanerQuery.fetchAllLoanerDueCnt(hmParam);

		    }

		if (strName.equals("Shipped")) {
			sbQuery = gmLoanerQuery.fetchShippedLoanerCount(hmParam);

		}

		else if (strName.equals("")) {
			sbQuery = gmLoanerQuery.fetchDueLoanerDrillDown(hmParam, strName);

		}

		log.debug(strName+"sbQuery Loaners Count  details===" + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	public ArrayList fetchMissingPartLoaner(HashMap hmParam) throws AppError {
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));

		/*
		 * Count and Drill down calls are separated as two methods. Removed the
		 * unwanted the rows and where condition check while fetching the count
		 * Moved the Database Object creation per method to Object creation per
		 * class. Author: gpalani PMT-10095
		 */
		if (!strPassName.equals("MissingPart")) {
			sbQuery = gmLoanerQuery.fetchMissingPartLoanerCount(hmParam);
		}

		else if (strPassName.equals("MissingPart")) {
			sbQuery = gmLoanerQuery.fetchMissingPartLoanerDrillDown(hmParam);

		}
		log.debug("sbQuery for missing part===>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

		return alReturn;
	}

	/**
	 * fetchLoanerUsageDetail This method called from Portal and Device and it
	 * is used to Fetch the Detail Of the Loaner sets which is loaned for a
	 * Particular month and used to calculate the Late Fee and Business days for
	 * a period of time.
	 * 
	 * @param hmParam
	 *            thehmParam
	 * @return the array list
	 * @throws AppError
	 *             the app error
	 */
	public ArrayList fetchLoanerUsageDetail(HashMap hmParam) throws AppError {

		initializeParameters(hmParam);
		GmDBManager gmDBManager = new GmDBManager();
		StringBuffer sbQuery = new StringBuffer();

		ArrayList alLoanerUsage = new ArrayList();
		HashMap hmfilterDetails = GmCommonClass
				.parseNullHashMap((HashMap) hmParam.get("HMFILTERDETAILS"));

		String strLatefee = "";
		String strLateFeeDays = "";
		String strusagemin = "";
		String strusagemax = "";
		String strSetId = GmCommonClass.parseNull((String) hmfilterDetails
				.get("SETID"));
		String strLoanerFromDate = GmCommonClass
				.parseNull((String) hmfilterDetails.get("LOANFROMDT"));
		String strLoanerTODate = GmCommonClass
				.parseNull((String) hmfilterDetails.get("LOANTODT"));
		String strsetName = GmCommonClass.parseNull((String) hmfilterDetails
				.get("SETNM"));
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		String strDistID = GmCommonClass.parseNull((String) hmfilterDetails
				.get("DISTID"));

		strLatefee = GmCommonClass.getRuleValue("LATE_FEE", "LOANER_USAGE_RPT");
		strLateFeeDays = GmCommonClass.getRuleValue("LATE_FEE_DAYS",
				"LOANER_USAGE_RPT");
		strSetId = GmCommonClass.getStringWithQuotes(strSetId);
		strusagemin = GmCommonClass.getRuleValue("USAGE_MIN",
				"LOANER_USAGE_RPT");
		strusagemax = GmCommonClass.getRuleValue("USAGE_MAX",
				"LOANER_USAGE_RPT");

		sbQuery.append(" SELECT LOANER.REQ_ID REQID,LOANER.C207_SET_ID SETID, LOANER.C207_SET_NM SETNAME,LOANER.NAME distname ");
		sbQuery.append(" , loaner.loan_dt loandate, loaner.ret_dt returndate, bus_days ");
		sbQuery.append(" , DECODE (SIGN (bus_days -" + strusagemin
				+ " ), - 1, 1, 0, 1, 1, DECODE (SIGN (bus_days - "
				+ strusagemax + "), - 1, 2, 0, 2, 1, 2), bus_days) usage ");
		sbQuery.append(" ,loaner.ass_rep_nm assocrepnm,loaner.rep_nm repnm ");
		sbQuery.append(" FROM ( ");
		sbQuery.append(" SELECT T525.C525_PRODUCT_REQUEST_ID REQ_ID,T526.C207_SET_ID, T207.C207_SET_NM, T701.C701_DISTRIBUTOR_NAME name ");
		sbQuery.append(" , TO_CHAR(T504A.C504A_LOANER_DT, '" + strApplDateFmt
				+ "') LOAN_DT, NVL (to_char(T504A.C504A_RETURN_DT,'"
				+ strApplDateFmt + "'), to_char(T504A.C504A_TRANSF_DATE,'"
				+ strApplDateFmt + "')) RET_DT ");
		sbQuery.append(" , GET_WORK_DAYS_COUNT (TO_CHAR (T504A.C504A_LOANER_DT,'MM/dd/yyyy'), TO_CHAR (NVL ( T504A.C504A_RETURN_DT, NVL (T504A.C504A_TRANSF_DATE, CURRENT_DATE)), 'MM/dd/yyyy')) BUS_DAYS ");

		sbQuery.append(" ,T703SALES.C703_SALES_REP_NAME REP_NM ,t703assoc_sales.C703_SALES_REP_NAME ass_rep_nm");
		sbQuery.append(" FROM T504A_LOANER_TRANSACTION t504a, T526_PRODUCT_REQUEST_DETAIL t526, T525_PRODUCT_REQUEST T525, T703_SALES_REP T703SALES,T703_sales_rep t703assoc_sales ");
		sbQuery.append(getAccessFilterClauseWithRepID());
		sbQuery.append(" , T207_SET_MASTER T207, T701_DISTRIBUTOR T701, T504_CONSIGNMENT T504 ");
		sbQuery.append(" WHERE T504A.C526_PRODUCT_REQUEST_DETAIL_ID = t526.C526_PRODUCT_REQUEST_DETAIL_ID ");
		sbQuery.append(" AND T525.C525_PRODUCT_REQUEST_ID         = t526.C525_PRODUCT_REQUEST_ID ");
		sbQuery.append(" AND T504.C504_CONSIGNMENT_ID =  T504A.C504_CONSIGNMENT_ID ");
		sbQuery.append(" AND T504.C504_TYPE=4127 ");
		sbQuery.append(" AND T207.c207_set_id = t526.c207_set_id ");
		sbQuery.append(" AND T504A.C504A_VOID_FL IS NULL ");
		sbQuery.append(" AND   T504A.C703_SALES_REP_ID =  T703SALES.C703_SALES_REP_ID ");
		sbQuery.append(" AND   T504A.C703_ASS_REP_ID =t703assoc_sales.c703_sales_rep_id(+) ");
		sbQuery.append(" AND T525.C525_VOID_FL  IS NULL ");
		sbQuery.append(" AND T526.c526_status_fl   IN ('30') ");// Status is 30
																// to Pull
																// Record only
																// which are
																// shipped out
		sbQuery.append(" AND T526.C526_VOID_FL  IS NULL ");
		sbQuery.append(" AND T504A.C504A_CONSIGNED_TO_ID = T701.C701_DISTRIBUTOR_ID ");
		sbQuery.append(" AND V700.REP_ID = T525.C703_SALES_REP_ID ");

		if (!strLoanerFromDate.equals("") && !strLoanerTODate.equals("")) {// Filter
																			// based
																			// on
			// Loaner_From and
			// Loaner_To Date
			sbQuery.append(" AND t504a.C504A_LOANER_DT");
			sbQuery.append(" >= to_date('");
			sbQuery.append(strLoanerFromDate);
			sbQuery.append("','" + strApplDateFmt
					+ "') AND t504a.C504A_LOANER_DT <= to_date('");
			sbQuery.append(strLoanerTODate);
			sbQuery.append("','" + strApplDateFmt + "')");
		}

		if (!strDistID.equals("")) {// Filter based on Dist Id
			sbQuery.append("  and T701.C701_DISTRIBUTOR_ID = '");
			sbQuery.append(strDistID);
			sbQuery.append("'");
		}

		if (!strSetId.equals("")) {// Filter based on Set Id
			sbQuery.append("  and T504.c207_set_id in('");
			sbQuery.append(strSetId);
			sbQuery.append("')");
		}
		if (!strsetName.equals("")) {// Filter based on Set Name
			sbQuery.append("  and T504.c207_set_id ='");
			sbQuery.append(strsetName);
			sbQuery.append("'");
		}

		sbQuery.append(" order by bus_days ");
		sbQuery.append(" )loaner ");

		sbQuery.append(" ORDER BY LOANER.NAME,LOANER.REQ_ID,LOANER.C207_SET_NM,LOAN_DT ");
		log.debug("sbQuery loaner usage===>" + sbQuery);
		alLoanerUsage = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alLoanerUsage;
	}

	/**
	 * fetchLoanerUsageSummary- This method called from Portal and Device and it
	 * is used to Fetch the Summary Of the Loaner sets which is loaned for a
	 * Particular month
	 * 
	 * @param hmParam
	 *            the hmParam
	 * @return the array list
	 * @throws AppError
	 *             the app error
	 */
	public ArrayList fetchLoanerUsageSummary(HashMap hmParam) throws AppError {

		initializeParameters(hmParam);
		ArrayList alLoanerUsage = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();

		HashMap hmfilterDetails = GmCommonClass
				.parseNullHashMap((HashMap) hmParam.get("HMFILTERDETAILS"));

		String strSetId = GmCommonClass.parseNull((String) hmfilterDetails
				.get("SETID"));
		String strLoanerFromDate = GmCommonClass
				.parseNull((String) hmfilterDetails.get("LOANFROMDT"));
		String strLoanerTODate = GmCommonClass
				.parseNull((String) hmfilterDetails.get("LOANTODT"));
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		String strsetName = GmCommonClass.parseNull((String) hmfilterDetails
				.get("SETNM"));
		String strDistID = GmCommonClass.parseNull((String) hmfilterDetails
				.get("DISTID"));

		strSetId = GmCommonClass.getStringWithQuotes(strSetId);

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" select setid,setname,distname,distid,repnm , sum(usage)usagesum ,firstdate,lastdate,month from( ");
		sbQuery.append(" select loanersummary.setid,loanersummary.setname,loanersummary.distname,loanersummary.distid,loanersummary.repnm ,bus_days ,loanersummary.req_id reqid, ");
		sbQuery.append(" decode (sign (bus_days -5 ), - 1, 1, 0, 1, 1, decode (sign (bus_days - 10), - 1, 2, 0, 2, 1, 2), bus_days) usage,");

		sbQuery.append(" loanersummary.firstdate firstdate ,loanersummary.lastdate lastdate ,loanersummary.month month from (");
		sbQuery.append(" Select t525.c525_product_request_id req_id, t526.c526_product_request_detail_id child_req_id,T526.C207_SET_ID setid, T207.C207_SET_NM setname, T701.C701_DISTRIBUTOR_NAME distname ,T701.C701_DISTRIBUTOR_id distid ");
		sbQuery.append(" , T703SALES.C703_SALES_REP_NAME  repnm  ,TO_CHAR(T504A.C504A_LOANER_DT, 'Month') Month");
		sbQuery.append("  ,to_char( LAST_DAY (T504A.C504A_LOANER_DT) ,'"
				+ strApplDateFmt
				+ "') LASTDATE ,to_char(TRUNC (C504A_LOANER_DT, 'mm') ,'"
				+ strApplDateFmt + "' )FIRSTDATE ,");
		sbQuery.append(" GET_WORK_DAYS_COUNT (TO_CHAR (T504A.C504A_LOANER_DT,'MM/dd/yyyy'), NVL (TO_CHAR (NVL ( T504A.C504A_RETURN_DT, NVL (T504A.C504A_TRANSF_DATE, SYSDATE)), 'MM/dd/yyyy'), TRUNC (SYSDATE))) BUS_DAYS ");
		sbQuery.append(" FROM T504A_LOANER_TRANSACTION t504a, T526_PRODUCT_REQUEST_DETAIL t526, T525_PRODUCT_REQUEST T525 ");
		sbQuery.append(getAccessFilterClauseWithRepID());
		sbQuery.append(" , T207_SET_MASTER T207, T701_DISTRIBUTOR T701, T504_CONSIGNMENT T504 , T703_SALES_REP T703SALES ");
		sbQuery.append(" WHERE T504A.C526_PRODUCT_REQUEST_DETAIL_ID = t526.C526_PRODUCT_REQUEST_DETAIL_ID ");
		sbQuery.append(" AND T525.C525_PRODUCT_REQUEST_ID         = t526.C525_PRODUCT_REQUEST_ID ");
		sbQuery.append(" AND T504.C504_CONSIGNMENT_ID =  T504A.C504_CONSIGNMENT_ID ");
		sbQuery.append(" AND T504.C504_TYPE=4127 ");// Loaners
		sbQuery.append(" AND   T504A.C703_SALES_REP_ID =  T703SALES.C703_SALES_REP_ID ");
		sbQuery.append(" AND T207.c207_set_id = t526.c207_set_id ");
		sbQuery.append(" AND T504A.C504A_VOID_FL IS NULL ");
		sbQuery.append(" AND T526.c526_status_fl   IN ('30') "); // Status is 30
																	// to Pull
																	// Record
																	// only
																	// which are
																	// shipped
																	// out.
		sbQuery.append(" AND T525.C525_VOID_FL  IS NULL ");
		sbQuery.append(" AND T526.C526_VOID_FL  IS NULL ");
		sbQuery.append(" AND T504A.C504A_CONSIGNED_TO_ID = T701.C701_DISTRIBUTOR_ID ");
		sbQuery.append(" AND V700.REP_ID = T525.C703_SALES_REP_ID ");

		if (!strDistID.equals("")) {// Filter based on Dist Id
			sbQuery.append("  and T701.C701_DISTRIBUTOR_ID = '");
			sbQuery.append(strDistID);
			sbQuery.append("'");
		}
		if (!strLoanerFromDate.equals("") && !strLoanerTODate.equals("")) {// Filter
																			// based
																			// on
			// Loaned_From and
			// Loaned_To Date
			sbQuery.append(" AND t504a.C504A_LOANER_DT");
			sbQuery.append(" >= to_date('");
			sbQuery.append(strLoanerFromDate);
			sbQuery.append("','" + strApplDateFmt
					+ "') AND t504a.C504A_LOANER_DT <= to_date('");
			sbQuery.append(strLoanerTODate);
			sbQuery.append("','" + strApplDateFmt + "')");
		}

		if (!strSetId.equals("")) {// Filter based on Set Id
			sbQuery.append("  and T504.c207_set_id in('");
			sbQuery.append(strSetId);
			sbQuery.append("')");
		}
		if (!strsetName.equals("")) {// Filter based on Set Name
			sbQuery.append("  and T504.c207_set_id ='");
			sbQuery.append(strsetName);
			sbQuery.append("'");
		}
		sbQuery.append(" group by T526.C207_SET_ID ,T207.C207_SET_NM,T701.C701_DISTRIBUTOR_NAME,T504A.C703_SALES_REP_ID, T504A.C504A_LOANER_DT,T701.C701_DISTRIBUTOR_ID ,T703SALES.C703_SALES_REP_NAME");
		sbQuery.append("  ,t504a.c504a_return_dt ,t504a.c504a_transf_date , t525.c525_product_request_id, t526.c526_product_request_detail_id  ");
		sbQuery.append(" Order by TO_CHAR(T504A.C504A_LOANER_DT, 'MM/YYYY') desc )loanersummary)");
		sbQuery.append(" group by setid,setname,distname,distid,repnm,firstdate,lastdate,month ");

		log.debug("sbQuery loaner usage summary===>" + sbQuery);
		alLoanerUsage = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alLoanerUsage;
	}

	/**
	 * fetchAggregateLoanerUsage- This method called from Portal and Device and
	 * it is used to Fetch the Summary Of the Loaner sets which is loaned for a
	 * period of 31 Days
	 * 
	 * @param hmParam
	 *            the hmParam
	 * @return the array list
	 * @throws AppError
	 *             the app error
	 */
	public ArrayList fetchLoanerUsageAggregate(HashMap hmParam) throws AppError {

		initializeParameters(hmParam);
		ArrayList alLoanerUsage = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();

		HashMap hmfilterDetails = GmCommonClass
				.parseNullHashMap((HashMap) hmParam.get("HMFILTERDETAILS"));

		String strSetId = GmCommonClass.parseNull((String) hmfilterDetails
				.get("SETID"));
		String strLoanerFromDate = GmCommonClass
				.parseNull((String) hmfilterDetails.get("LOANFROMDT"));
		String strLoanerTODate = GmCommonClass
				.parseNull((String) hmfilterDetails.get("LOANTODT"));
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		String strsetName = GmCommonClass.parseNull((String) hmfilterDetails
				.get("SETNM"));

		strSetId = GmCommonClass.getStringWithQuotes(strSetId);

		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" select distname,distid,repnm,sum(usagesum) overallusage ,month from(");
		sbQuery.append(" select distname,distid,repnm , sum(usage)usagesum ,month from( ");
		sbQuery.append(" select loaneraggregate.distname,loaneraggregate.distid,loaneraggregate.repnm ,bus_days ,loaneraggregate.req_id reqid, ");
		sbQuery.append(" decode (sign (bus_days -5 ), - 1, 1, 0, 1, 1, decode (sign (bus_days - 10), - 1, 2, 0, 2, 1, 2), bus_days) usage,");

		sbQuery.append(" loaneraggregate.firstdate firstdate ,loaneraggregate.lastdate lastdate ,loaneraggregate.month month from (");
		sbQuery.append(" Select t525.c525_product_request_id req_id, t526.c526_product_request_detail_id child_req_id,T526.C207_SET_ID setid, T207.C207_SET_NM setname, T701.C701_DISTRIBUTOR_NAME distname ,T701.C701_DISTRIBUTOR_id distid ");
		sbQuery.append(" , T703SALES.C703_SALES_REP_NAME  repnm  ,TO_CHAR(T504A.C504A_LOANER_DT, 'Month') Month");
		sbQuery.append("  ,to_char( LAST_DAY (T504A.C504A_LOANER_DT) ,'"
				+ strApplDateFmt
				+ "') LASTDATE ,to_char(TRUNC (C504A_LOANER_DT, 'mm') ,'"
				+ strApplDateFmt + "' )FIRSTDATE ,");
		sbQuery.append(" GET_WORK_DAYS_COUNT (TO_CHAR (T504A.C504A_LOANER_DT,'MM/dd/yyyy'), NVL (TO_CHAR (NVL ( T504A.C504A_RETURN_DT, NVL (T504A.C504A_TRANSF_DATE, SYSDATE)), 'MM/dd/yyyy'), TRUNC (SYSDATE))) BUS_DAYS ");
		sbQuery.append(" FROM T504A_LOANER_TRANSACTION t504a, T526_PRODUCT_REQUEST_DETAIL t526, T525_PRODUCT_REQUEST T525 ");
		sbQuery.append(getAccessFilterClauseWithRepID());
		sbQuery.append(" , T207_SET_MASTER T207, T701_DISTRIBUTOR T701, T504_CONSIGNMENT T504 , T703_SALES_REP T703SALES ");
		sbQuery.append(" WHERE T504A.C526_PRODUCT_REQUEST_DETAIL_ID = t526.C526_PRODUCT_REQUEST_DETAIL_ID ");
		sbQuery.append(" AND T525.C525_PRODUCT_REQUEST_ID         = t526.C525_PRODUCT_REQUEST_ID ");
		sbQuery.append(" AND T504.C504_CONSIGNMENT_ID =  T504A.C504_CONSIGNMENT_ID ");
		sbQuery.append(" AND T504.C504_TYPE=4127 ");// Loaners
		sbQuery.append(" AND   T504A.C703_SALES_REP_ID =  T703SALES.C703_SALES_REP_ID ");
		sbQuery.append(" AND T207.c207_set_id = t526.c207_set_id ");
		sbQuery.append(" AND T504A.C504A_VOID_FL IS NULL ");
		sbQuery.append(" AND T526.c526_status_fl   IN ('30') ");// Status is 30
																// to Pull
																// Record only
																// which are
																// shipped out.
		sbQuery.append(" AND T525.C525_VOID_FL  IS NULL ");
		sbQuery.append(" AND T526.C526_VOID_FL  IS NULL ");
		sbQuery.append(" AND T504A.C504A_CONSIGNED_TO_ID = T701.C701_DISTRIBUTOR_ID ");
		sbQuery.append(" AND V700.REP_ID = T525.C703_SALES_REP_ID ");

		if (!strLoanerFromDate.equals("") && !strLoanerTODate.equals("")) {// Filter
																			// based
																			// on
			// Loaned_From and
			// Loaned_To Date
			sbQuery.append(" AND t504a.C504A_LOANER_DT");
			sbQuery.append(" >= to_date('");
			sbQuery.append(strLoanerFromDate);
			sbQuery.append("','" + strApplDateFmt
					+ "') AND t504a.C504A_LOANER_DT <= to_date('");
			sbQuery.append(strLoanerTODate);
			sbQuery.append("','" + strApplDateFmt + "')");
		}

		if (!strSetId.equals("")) {// Filter based on Set Id
			sbQuery.append("  and T504.c207_set_id in('");
			sbQuery.append(strSetId);
			sbQuery.append("')");
		}
		if (!strsetName.equals("")) {// Filter based on Set Name
			sbQuery.append("  and T504.c207_set_id ='");
			sbQuery.append(strsetName);
			sbQuery.append("'");
		}
		sbQuery.append(" group by T526.C207_SET_ID ,T207.C207_SET_NM,T701.C701_DISTRIBUTOR_NAME,T504A.C703_SALES_REP_ID, T504A.C504A_LOANER_DT,T701.C701_DISTRIBUTOR_ID ,T703SALES.C703_SALES_REP_NAME");
		sbQuery.append("  ,t504a.c504a_return_dt ,t504a.c504a_transf_date , t525.c525_product_request_id, t526.c526_product_request_detail_id  ");
		sbQuery.append(" Order by TO_CHAR(T504A.C504A_LOANER_DT, 'MM/YYYY') desc )loaneraggregate)");
		sbQuery.append(" group by distname,distid,repnm,firstdate,lastdate,month ) ");
		sbQuery.append(" GROUP BY distname , distid,repnm ,month");

		log.debug("sbQuery loaner usage ===>" + sbQuery);
		alLoanerUsage = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alLoanerUsage;
	}
}
