package com.globus.sales.DashBoard.beans;

import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.sales.beans.GmSalesFilterConditionBean;

public class GmSalesDashCongSetsBeanQuery extends GmSalesFilterConditionBean {

  /**
   * Count and Drill down calls are separated into two methods. Removed the unwanted the rows and
   * where condition check while fetching the count. Database Object Creation - Changed the object
   * creation one per class instead of one per method.
   * 
   * @ NEW Java File creation.
   * 
   * Methods Added: fetchRequestedCountCongSets, fetchRequestedDrillDownCongSets,
   * fetchApprovedCountCongSets, fetchApprovedDrillDownCongSets, fetchShippedCountCongSets.
   * fetchShippedDrillDownCongSets, fetchBackOrderCountCongSets, fetchBackOrderDrillDownCongSets,
   * fetchAllSetsCountCongSets, fetchAllSetsDrillDownCongSets,
   * fetchSetsMissingLocationCountCongSets, fetchSetsMissingLocationDrillDownCongSets
   * 
   * @author gpalani PMT-10095 (RFC2252)
   * @since 2017-01-03
   */
  // To fetch Requested consignment sets count
  public StringBuffer fetchRequestedCountCongSets(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT    'Requested'  name, count(A.C526_Status_Fl) total  FROM (");
    sbQuery.append(" SELECT T526.C526_STATUS_FL ");
    sbQuery.append(" FROM t526_product_request_detail t526 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(", T525_PRODUCT_REQUEST T525, T907_SHIPPING_INFO T907");
    sbQuery.append(" WHERE t526.c901_request_type = 400087");
    sbQuery.append(" AND t907.c901_source  = 50181");
    sbQuery.append(" AND t526.c525_product_request_id   = t525.c525_product_request_id");
    sbQuery.append(" AND to_char(t526.c526_product_request_detail_id) = t907.c907_ref_id(+)  ");
    sbQuery.append(" AND t525.c525_void_fl IS NULL");
    sbQuery.append(" AND t526.c526_void_fl IS NULL");
    sbQuery.append(" AND T907.C907_Void_Fl  IS NULL");
    sbQuery.append(" AND T526.C526_Status_Fl  = '5'");
    sbQuery.append(" AND v700.d_id  = t525.c525_request_for_id");
    sbQuery.append("  ) A ");
    return sbQuery;
  }

  // To fetch Requested consignment sets drill down
  public StringBuffer fetchRequestedDrillDownCongSets(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" select T526.C526_STATUS_FL, T526.C525_PRODUCT_REQUEST_ID REQID , t526.C526_PRODUCT_REQUEST_DETAIL_ID LNREQDTLID, TO_CHAR (T525.C525_SURGERY_DATE, '");
    sbQuery.append(getCompDateFmt());
    sbQuery.append("') SURGDTFMT,  DECODE (C526_STATUS_FL ,'5', 'Requested', 'Approved') STATUS");
    sbQuery
        .append(", T525.C525_Surgery_Date Surgdt , T526.C207_Set_Id SETID, Get_Set_Name (T526.C207_Set_Id) NAME, ");
    sbQuery.append(" TO_CHAR (T907.C907_Shipped_Dt, '");
    sbQuery.append(getCompDateFmt());
    sbQuery.append("') shpdt , Get_Account_Name (T525.C704_Account_Id) ACCTNAME,");
    sbQuery
        .append(" GET_REP_NAME (t525.c703_sales_rep_id)SLSREP, V700.D_NAME FLDSLS FROM t526_product_request_detail t526");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(", T525_PRODUCT_REQUEST T525, T907_SHIPPING_INFO T907");
    sbQuery.append(" WHERE t526.c901_request_type = 400087");
    sbQuery.append(" AND t907.c901_source  = 50181");
    sbQuery.append(" AND t526.c525_product_request_id   = t525.c525_product_request_id");
    sbQuery.append(" AND to_char(t526.c526_product_request_detail_id) = t907.c907_ref_id(+)  ");
    sbQuery.append(" AND t525.c525_void_fl IS NULL");
    sbQuery.append(" AND t526.c526_void_fl IS NULL");
    sbQuery.append(" AND T907.C907_Void_Fl  IS NULL");
    sbQuery.append(" AND T526.C526_Status_Fl  = '5'");
    sbQuery.append(" AND v700.d_id  = t525.c525_request_for_id");
    sbQuery.append(" ORDER BY T525.C525_Created_Date");
    return sbQuery;
  }

  // To fetch Approved consignment sets count
  public StringBuffer fetchApprovedCountCongSets(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT 'Approved' name, count(1) total FROM (");
    sbQuery.append(" SELECT t520.c520_request_id reqid");
    sbQuery.append(" FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t520.c520_void_fl IS NULL ");
    sbQuery.append(" AND t520.c520_request_id = t504.c520_request_id(+) ");
    sbQuery.append(" AND t520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
    sbQuery.append(" AND t520.c520_status_fl in (0,10,15,20,30) ");
    sbQuery.append(" AND t520.C520_Master_Request_Id Is Null ");
    sbQuery.append(" AND t520.C207_Set_Id Is Not Null ");
    sbQuery.append(" AND	t520.C520_Request_For = 40021");
    sbQuery.append(" AND t520.c520_request_to = v700.d_id ");
    sbQuery.append(" AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
    // Added company id for load records depending on CompanyId.
    sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(")");

    return sbQuery;
  }

  // To fetch Approved consignment sets drill down
  public StringBuffer fetchApprovedDrillDownCongSets(HashMap hmParam)

  {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    sbQuery.append("SELECT t520.c520_request_id reqid, T504.C504_Consignment_Id CNID, ");
    sbQuery.append("t520.c207_set_id setid, Get_Set_Name (T520.C207_Set_Id) Name, ");
    sbQuery.append("get_consignment_part_attribute (t504.c504_consignment_id, 92340) tag, ");
    sbQuery.append("Get_Code_Name (T520.C520_Request_For) Reqfor, ");
    sbQuery.append("Gm_Pkg_Op_Request_Summary.Get_Request_Status (T520.C520_Status_Fl) Status, ");
    sbQuery.append("Get_Distributor_Name (T520.C520_Request_To) FLDSLS, ");
    sbQuery.append("GET_TAG_ID(T504.C504_Consignment_Id) tagnum, ");
    sbQuery.append("DECODE (t520.c901_ship_to ,'4121',");
    sbQuery.append("get_rep_name(C520_ship_to_id),'') SLSREP,To_Char (t504.C504_SHIP_DATE, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') shpdt ");
    sbQuery.append("FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
    sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
    sbQuery.append("AND t520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
    sbQuery.append("AND t520.c520_status_fl in (0,10,15,20,30) ");
    sbQuery.append("AND t520.C520_Master_Request_Id Is Null ");
    sbQuery.append("AND t520.C207_Set_Id Is Not Null ");
    sbQuery.append("AND	t520.C520_Request_For = 40021");
    sbQuery.append(" AND t520.c520_request_to = v700.d_id ");
    sbQuery.append("AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
    // Added company id for load records depending on CompanyId.
    sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());
    return sbQuery;
  }

  // To fetch Shipped consignment sets count
  public StringBuffer fetchShippedCountCongSets(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT 'Shipped in last 7 days' name, count(1) total FROM (");
    sbQuery.append(" SELECT t520.c520_request_id reqid ");
    sbQuery.append(" FROM t520_request t520, t504_consignment t504, ");
    sbQuery.append(" t4020_demand_master t4020, t907_shipping_info t907 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t520.c520_void_fl IS NULL ");
    sbQuery.append(" AND t520.c520_request_id = t504.c520_request_id(+) ");
    sbQuery.append("And T520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
    // -- AND t520.c520_status_fl = get_code_name_alt ('50644')
    sbQuery.append(" AND t520.c520_status_fl in (40) ");
    sbQuery.append(" AND T520.C520_Master_Request_Id Is Null ");
    sbQuery.append(" AND T520.C207_Set_Id Is Not Null ");
    sbQuery.append(" AND T520.C520_Request_For = 40021 ");
    sbQuery.append(" AND t907.c901_source = 50181 ");
    sbQuery.append(" AND T907.C907_Void_Fl  Is Null ");
    sbQuery.append(" AND  T907.C907_Ref_Id = T504.C504_Consignment_Id ");
    sbQuery.append(" AND TRUNC(t907.c907_shipped_dt) >= TRUNC (CURRENT_DATE - 7) ");
    sbQuery.append(" AND t504.C701_DISTRIBUTOR_ID = v700.d_id");
    sbQuery.append(" AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
    // Added company id for load records depending on CompanyId.
    sbQuery.append(" AND T520.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(")");

    return sbQuery;
  }

  // To fetch Shipped consignment sets drill down
  public StringBuffer fetchShippedDrillDownCongSets(HashMap hmParam) {
    initializeParameters(hmParam);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append("SELECT t520.c520_request_id reqid, T504.C504_Consignment_Id CNID, ");
    sbQuery.append("t520.c207_set_id setid, To_Char (t907.c907_shipped_dt, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') shpdt, Get_Set_Name (T520.C207_Set_Id) Name, ");
    sbQuery.append("get_consignment_part_attribute (t504.c504_consignment_id, 92340) tag, ");
    sbQuery.append("t504.c504_consignment_id, ");
    sbQuery.append("Get_Code_Name (T520.C520_Request_For) Reqfor, ");
    sbQuery.append("Gm_Pkg_Op_Request_Summary.Get_Request_Status (T520.C520_Status_Fl) Status, ");
    sbQuery.append("Get_Distributor_Name (T520.C520_Request_To) FLDSLS, ");
    sbQuery.append("Get_Tag_Id(T504.C504_Consignment_Id) tagnum, ");
    // sbQuery.append("T504.C504_Consignment_Id Tagid, ");
    sbQuery.append("DECODE (t520.c901_ship_to ,'4121',get_rep_name(C520_ship_to_id),'') SLSREP ");
    sbQuery
        .append(" , t907.c901_delivery_carrier CARRIERID,DECODE (t907.C907_Tracking_Number, Null, 'Y', t907.C907_Tracking_Number) TRACK ");
    sbQuery.append("FROM t520_request t520, t504_consignment t504, ");
    sbQuery.append("t4020_demand_master t4020, t907_shipping_info t907 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
    sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
    sbQuery.append("And T520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
    // -- AND t520.c520_status_fl = get_code_name_alt ('50644')
    sbQuery.append("AND t520.c520_status_fl in (40) ");
    sbQuery.append("AND T520.C520_Master_Request_Id Is Null ");
    sbQuery.append("AND T520.C207_Set_Id Is Not Null ");
    sbQuery.append("AND T520.C520_Request_For = 40021 ");
    sbQuery.append("AND t907.c901_source = 50181 ");
    sbQuery.append("AND T907.C907_Void_Fl  Is Null ");
    sbQuery.append("AND  T907.C907_Ref_Id = T504.C504_Consignment_Id ");
    sbQuery.append("AND TRUNC(t907.c907_shipped_dt) >= TRUNC (CURRENT_DATE - 7) ");
    sbQuery.append("AND t504.C701_DISTRIBUTOR_ID = v700.d_id");
    sbQuery.append(" AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
    // Added company id for load records depending on CompanyId.
    sbQuery.append(" AND T520.C1900_COMPANY_ID = " + getCompId());
    return sbQuery;
  }

  // To fetch backorder consignment sets count
  public StringBuffer fetchBackOrderCountCongSets(HashMap hmParam) {
    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);
    sbQuery.append(" SELECT 'Back Order' name, count(1) total FROM (");
    sbQuery.append(" SELECT t520.c520_request_id reqid");
    sbQuery.append(" FROM t520_request t520, t504_consignment t504, ");
    sbQuery.append(" t4020_demand_master t4020 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t520.c520_void_fl IS NULL ");
    sbQuery.append(" AND t520.c520_request_id = t504.c520_request_id(+) ");
    sbQuery.append(" And t520.C520_Request_Txn_Id = t4020.C4020_Demand_Master_Id(+) ");
    sbQuery.append(" AND t520.c520_status_fl in (10) ");
    sbQuery.append(" AND t520.C520_Master_Request_Id Is Null ");
    sbQuery.append(" AND t520.C207_Set_Id Is Not Null ");
    sbQuery.append(" AND t520.C520_Request_For = 40021");
    sbQuery.append(" AND t520.c520_request_to = v700.d_id ");
    sbQuery.append(" AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
    // Added company id for load records depending on CompanyId.
    sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(")");

    return sbQuery;
  }

  // To fetch backorder consignment sets drill down
  public StringBuffer fetchBackOrderDrillDownCongSets(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    initializeParameters(hmParam);
    sbQuery.append("SELECT t520.c520_request_id reqid, T504.C504_Consignment_Id CNID, ");
    sbQuery.append("t520.c207_set_id setid, Get_Set_Name (t520.C207_Set_Id) Name, ");
    sbQuery.append("get_consignment_part_attribute (t504.c504_consignment_id, 92340) tag, ");
    sbQuery.append("Get_Code_Name (t520.C520_Request_For) Reqfor, ");
    sbQuery.append("Gm_Pkg_Op_Request_Summary.Get_Request_Status (t520.C520_Status_Fl) Status, ");
    sbQuery.append("Get_Distributor_Name (t520.C520_Request_To) FLDSLS, ");
    sbQuery.append("GET_TAG_ID(t504.C504_Consignment_Id) tagnum, ");
    sbQuery.append("DECODE (t520.c901_ship_to ,'4121',");
    sbQuery.append("get_rep_name(C520_ship_to_id),'') SLSREP, To_Char (t504.C504_SHIP_DATE, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') shpdt ");
    sbQuery.append("FROM t520_request t520, t504_consignment t504, ");
    sbQuery.append("t4020_demand_master t4020 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
    sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
    sbQuery.append("And t520.C520_Request_Txn_Id = t4020.C4020_Demand_Master_Id(+) ");
    sbQuery.append("AND t520.c520_status_fl in (10) ");
    sbQuery.append("AND t520.C520_Master_Request_Id Is Null ");
    sbQuery.append("AND t520.C207_Set_Id Is Not Null ");
    sbQuery.append("AND t520.C520_Request_For = 40021");
    sbQuery.append(" AND t520.c520_request_to = v700.d_id ");
    sbQuery.append("AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
    // Added company id for load records depending on CompanyId.
    sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());
    return sbQuery;

  }

  // To fetch All consignment sets count
  public StringBuffer fetchAllSetsCountCongSets(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT 'All Sets' name, count(1) total FROM (");
    sbQuery.append(" SELECT t5010.c5010_tag_id TAGID, t5010.c207_set_id SETID ");
    sbQuery.append(" FROM t5010_tag t5010,v5010a_tag_attribute v5010a ");
    sbQuery.append(" , (SELECT tagid, NAME ");
    sbQuery.append(" 	FROM v5010_curr_address) v5010 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" , T207_SET_MASTER t207, T703_SALES_REP T703, T704_ACCOUNT T704  ");
    sbQuery.append(" WHERE t5010.c5010_tag_id       = v5010a.c5010_tag_id (+) ");
    sbQuery.append(" AND t5010.c5010_tag_id = v5010.tagid(+) ");
    sbQuery.append(" AND t5010.c901_trans_type    = 51000 ");
    sbQuery.append(" AND t5010.c901_inventory_type not in (10004,10002) ");
    sbQuery.append(" AND t5010.c901_location_type = 4120 ");
    sbQuery.append(" AND t5010.c5010_void_fl     IS NULL ");
    // sbQuery.append(" AND t5010.c207_set_id IS NOT NULL ");
    sbQuery.append(" AND t5010.c5010_location_id  =  v700.d_id ");
    sbQuery.append(" AND T5010.c703_sales_rep_id = T703.c703_sales_rep_id(+) ");
    sbQuery.append(" AND T5010.c704_account_id = T704.c704_account_id(+)  ");
    sbQuery.append(" AND t5010.c207_set_id = T207.c207_set_id(+) ) ");

    return sbQuery;
  }

  // To fetch All consignment sets drill down
  public StringBuffer fetchAllSetsDrillDownCongSets(HashMap hmParam) {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");

    sbQuery.append(" SELECT t5010.c5010_tag_id TAGID, t5010.c207_set_id SETID ");
    // sbQuery.append(" , get_set_name (t5010.c207_set_id) NAME, v700.d_name FLDSLS ");
    sbQuery.append(" , T207.c207_set_nm NAME, v700.d_name FLDSLS ");
    // sbQuery.append(" , get_rep_name (t5010.c703_sales_rep_id) SLSREP, get_account_name(t5010.c704_account_id) ACCNAME ");
    sbQuery.append(" , " + strRepNmColumn + " SLSREP, " + strAccountNmColumn + "  ACCNAME ");
    sbQuery.append(" , DECODE (v5010a.c5010_lockset, 'Y','Locked','Unlock') LOCKINVFL ");
    sbQuery
        .append(" , DECODE (v5010a.c5010_decommissioned, 'Y','Decommissioned','Active') DECOMSINFL  ");
    sbQuery.append(" FROM t5010_tag t5010,v5010a_tag_attribute v5010a ");
    sbQuery.append(" , (SELECT tagid, NAME, address1, address2 ");
    sbQuery
        .append(" 		   , DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', zipcode, city) city ");
    sbQuery.append(" 	   , get_code_name_alt (state) state ");
    sbQuery
        .append("    , DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', city, zipcode) zipcode ");
    sbQuery.append("    , phonenumber, emailid ");
    sbQuery.append(" 	FROM v5010_curr_address) v5010 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" , T207_SET_MASTER t207, T703_SALES_REP T703, T704_ACCOUNT T704  ");
    sbQuery.append(" WHERE t5010.c5010_tag_id       = v5010a.c5010_tag_id (+) ");
    sbQuery.append(" AND t5010.c5010_tag_id = v5010.tagid(+) ");
    sbQuery.append(" AND t5010.c901_trans_type    = 51000 ");
    sbQuery.append(" AND t5010.c901_inventory_type not in (10004,10002) ");
    sbQuery.append(" AND t5010.c901_location_type = 4120 ");
    sbQuery.append(" AND t5010.c5010_void_fl     IS NULL ");
    // sbQuery.append(" AND t5010.c207_set_id IS NOT NULL ");
    sbQuery.append(" AND t5010.c5010_location_id  =  v700.d_id ");
    sbQuery.append(" AND T5010.c703_sales_rep_id = T703.c703_sales_rep_id(+) ");
    sbQuery.append(" AND T5010.c704_account_id = T704.c704_account_id(+)  ");
    sbQuery.append(" AND t5010.c207_set_id = T207.c207_set_id(+)  ");
    sbQuery.append(" ORDER BY SETID, NAME ");

    return sbQuery;
  }

  // To fetch Missing location sets count
  public StringBuffer fetchSetsMissingLocationCountCongSets(HashMap hmParam) {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append("SELECT 'Sets missing location' name, count(1) total FROM (");
    sbQuery
        .append(" SELECT t5010.c5010_tag_id TAGID, t5010.c207_set_id SETID, T207.c207_set_nm NAME ");
    sbQuery.append(" FROM t5010_tag t5010,v5010a_tag_attribute v5010a ");
    sbQuery.append(" , (SELECT tagid, NAME, ADDRESS1 ");
    sbQuery.append(" 	FROM v5010_curr_address) v5010 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" ,T207_SET_MASTER t207, T703_SALES_REP T703, T704_ACCOUNT T704  ");
    sbQuery.append(" WHERE t5010.c5010_tag_id       = v5010a.c5010_tag_id (+) ");
    sbQuery.append(" AND t5010.c5010_tag_id = v5010.tagid(+) ");
    sbQuery.append(" AND t5010.c901_trans_type    = 51000 ");
    sbQuery.append(" AND t5010.c901_location_type = 4120 ");
    sbQuery.append(" AND t5010.c901_inventory_type not in (10004,10002) ");
    sbQuery.append(" AND t5010.c5010_void_fl IS NULL ");
    sbQuery.append(" AND v5010.address1 is null ");
    // sbQuery.append(" AND t5010.c207_set_id IS NOT NULL ");
    sbQuery.append(" AND t5010.c5010_location_id  =  v700.d_id ");
    sbQuery.append(" AND T5010.c703_sales_rep_id = T703.c703_sales_rep_id(+) ");
    sbQuery.append(" AND T5010.c704_account_id = T704.c704_account_id(+)  ");
    sbQuery.append(" AND t5010.c207_set_id = T207.c207_set_id(+) )  ");

    return sbQuery;
  }

  // To fetch Missing location sets drill down
  public StringBuffer fetchSetsMissingLocationDrillDownCongSets(HashMap hmParam) {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);


    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");

    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");


    sbQuery.append(" SELECT t5010.c5010_tag_id TAGID, t5010.c207_set_id SETID ");
    // sbQuery.append(" , get_set_name (t5010.c207_set_id) NAME, v700.d_name FLDSLS ");
    sbQuery.append(" , T207.c207_set_nm NAME, v700.d_name FLDSLS ");
    // sbQuery.append(" , get_rep_name (t5010.c703_sales_rep_id) SLSREP, get_account_name(t5010.c704_account_id) ACCNAME ");
    sbQuery.append(" , " + strRepNmColumn + " SLSREP, " + strAccountNmColumn + "  ACCNAME ");
    sbQuery.append(" , DECODE (v5010a.c5010_lockset, 'Y','Locked','Unlock') LOCKINVFL ");
    sbQuery
        .append(" , DECODE (v5010a.c5010_decommissioned, 'Y','Decommissioned','Active') DECOMSINFL  ");
    sbQuery.append(" FROM t5010_tag t5010,v5010a_tag_attribute v5010a ");
    sbQuery.append(" , (SELECT tagid, NAME, address1, address2 ");
    sbQuery
        .append(" , DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', zipcode, city) city ");
    sbQuery.append(" 	   , get_code_name_alt (state) state ");
    sbQuery
        .append("    , DECODE (NVL (get_rule_value ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', city, zipcode) zipcode ");
    sbQuery.append("    , phonenumber, emailid ");
    sbQuery.append(" 	FROM v5010_curr_address) v5010 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" ,T207_SET_MASTER t207, T703_SALES_REP T703, T704_ACCOUNT T704  ");
    sbQuery.append(" WHERE t5010.c5010_tag_id       = v5010a.c5010_tag_id (+) ");
    sbQuery.append(" AND t5010.c5010_tag_id = v5010.tagid(+) ");
    sbQuery.append(" AND t5010.c901_trans_type    = 51000 ");
    sbQuery.append(" AND t5010.c901_location_type = 4120 ");
    sbQuery.append(" AND t5010.c901_inventory_type not in (10004,10002) ");
    sbQuery.append(" AND t5010.c5010_void_fl IS NULL ");
    sbQuery.append(" AND v5010.address1 is null ");
    // sbQuery.append(" AND t5010.c207_set_id IS NOT NULL ");
    sbQuery.append(" AND t5010.c5010_location_id  =  v700.d_id ");
    sbQuery.append(" AND T5010.c703_sales_rep_id = T703.c703_sales_rep_id(+) ");
    sbQuery.append(" AND T5010.c704_account_id = T704.c704_account_id(+)  ");
    sbQuery.append(" AND t5010.c207_set_id = T207.c207_set_id(+)  ");
    sbQuery.append(" ORDER BY SETID, NAME ");

    return sbQuery;
  }

}
