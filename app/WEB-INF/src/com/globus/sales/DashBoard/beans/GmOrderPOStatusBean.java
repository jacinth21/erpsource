package com.globus.sales.DashBoard.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * This method used to fetch data of Pending Order Dashboard and its details
 * @author tmuthusamy
 *
 */
public class GmOrderPOStatusBean extends GmSalesFilterConditionBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	 public GmOrderPOStatusBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		   super(gmDataStoreVO);
		   // TODO Auto-generated constructor stub
     }
	 /** This method used to fetch Pending Order Count and amount from t501 order table
	   *  for PO < 14, PO14 - 21, PO > 21, Pending approval and Pending Discrepancy status
	   * @param hmParam
	   * @return alReturn
	   * @throws AppError
	   * @Author tmuthusamy
	   */
	public ArrayList fetchPODashboard(HashMap hmParam) throws AppError {
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmSalesDashOrdersBeanQuery gmSalesDashOrderBeanQuery = new GmSalesDashOrdersBeanQuery();
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));

		if (strPassName.equalsIgnoreCase("PO21day")) {
			sbQuery = gmSalesDashOrderBeanQuery.fetchPOGreaterthan21daysCount(hmParam);
		}
		else if (strPassName.equalsIgnoreCase("PO14-21day")){
			sbQuery = gmSalesDashOrderBeanQuery.fetchPO14to21daysCount(hmParam);
		}
		else if (strPassName.equalsIgnoreCase("PO14day")) {
			sbQuery = gmSalesDashOrderBeanQuery.fetchPOLessthan14daysCount(hmParam);
		}
		else if (strPassName.equalsIgnoreCase("PendingApproval")) {
			sbQuery = fetchPOPendingApproval(hmParam);
		}
		else if (strPassName.equalsIgnoreCase("PendingDiscrepancy")) {
			sbQuery = fetchPOPendingDiscrepancy(hmParam);
		}

		log.debug("sbQuery==>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	/** This method used to fetch Pending Order Count and amount from t501 order table
	   *  to show on PO dashboard for Pending Approval status
	   * @param hmParam
	   * @return sbQuery
	   * @throws AppError
	   * @Author tmuthusamy
	   */
	public StringBuffer fetchPOPendingApproval(HashMap hmParam) throws AppError{
		String strCondition = GmCommonClass.parseNull((String) hmParam
				.get("Condition"));
		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		sbQuery.append(" SELECT 'Pending Approval' name, count(1) total, NVL(sum(amt),0) amt ");
		sbQuery.append(" FROM (SELECT t501.c501_order_id ORDID,get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') amt ");
		sbQuery.append(" FROM t501_order t501 , t703_sales_rep T703, t704_account T704, T5001_ORDER_PO_DTLS T5001 "); // Join the table t5001
		sbQuery.append(" WHERE t501.c501_void_fl IS NULL ");
		sbQuery.append(" AND c501_delete_fl IS NULL ");
		sbQuery.append(" AND t501.c501_status_fl IS NOT NULL ");
		sbQuery.append(" AND t501.c503_invoice_id IS NULL ");
		sbQuery.append(" AND NVL (t501.c901_order_type, -9999) <> 2535 ");
		// Filter Condition to fetch record based on access code
		if (!strCondition.toString().equals("")) {
			sbQuery.append(" AND ");
			sbQuery.append(strCondition);
		}
		sbQuery.append(getSalesFilterClause());
		sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
		sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id ");
		sbQuery.append(" AND t501.c501_order_id = t5001.c501_order_id ");
		// Pending Approval PO Status (109541)
		sbQuery.append(" AND t5001.c5001_void_fl IS NULL AND t5001.C901_CUST_PO_STATUS = 109541 GROUP by t501.c501_order_id) ");

		return sbQuery;
	}

	/** This method used to fetch Pending Order Count and amount from t501 order table
	   *  to show on PO dashboard for Pending Discrepancy status
	   * @param hmParam
	   * @return sbQuery
	   * @throws AppError
	   * @Author tmuthusamy
	   */
	public StringBuffer fetchPOPendingDiscrepancy(HashMap hmParam) throws AppError {
		String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		sbQuery.append(" SELECT 'Pending Discrepancy' name, count(1) total, NVL(sum(amt),0) amt ");
		sbQuery.append(" FROM (SELECT t501.c501_order_id ORDID,get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') amt ");
		sbQuery.append(" FROM t501_order t501 , t703_sales_rep T703, t704_account T704, T5001_ORDER_PO_DTLS T5001  "); // Join the table t5001
		sbQuery.append(" WHERE t501.c501_void_fl IS NULL ");
		sbQuery.append(" AND c501_delete_fl IS NULL ");
		sbQuery.append(" AND t501.c501_status_fl IS NOT NULL ");
		sbQuery.append(" AND t501.c503_invoice_id IS NULL ");
		sbQuery.append(" AND NVL (t501.c901_order_type, -9999) <> 2535 "); 
		// Filter Condition to fetch record based on access code
		if (!strCondition.toString().equals("")) {
			sbQuery.append(" AND ");
			sbQuery.append(strCondition);
		}
		sbQuery.append(getSalesFilterClause());
		sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
		sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id ");
		sbQuery.append(" AND t501.c501_order_id = t5001.c501_order_id ");
		// Pending Discrepancy PO Status (109544)
		sbQuery.append(" AND t5001.c5001_void_fl IS NULL AND t5001.C901_CUST_PO_STATUS = 109544  GROUP by t501.c501_order_id ) ");
		return sbQuery;

	}
	
	/** This method used to fetch Pending Order Report for PO < 14, PO14 - 21, PO > 21,
	 *  Pending approval and Pending Discrepancy status
	   * @param hmParam
	   * @return alReturn
	   * @throws AppError
	   * @Author tmuthusamy
	   */
	public ArrayList fetchPOReport (HashMap hmParam) throws AppError {
		ArrayList alReturn = null;
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmSalesDashOrdersBeanQuery gmSalesDashOrderBeanQuery = new GmSalesDashOrdersBeanQuery();
	    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
	    StringBuffer sbQuery = new StringBuffer();
		if (strPassName.equalsIgnoreCase("PendingApproval") || strPassName.equalsIgnoreCase("PendingDiscrepancy")) {
			sbQuery = fetchPOStatusDrillDown(hmParam);
		} else {
			sbQuery = gmSalesDashOrderBeanQuery.fetchPOsDrillDown(hmParam);
		}
	    log.debug("sbQuery==>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	  }
	
	/** This sbquery calling for  fetch Pending Order Report for 
	 *  Pending approval and Pending Discrepancy status
	   * @param hmParam
	   * @return sbQuery
	   * @throws AppError
	   * @Author tmuthusamy
	   */
	public StringBuffer fetchPOStatusDrillDown(HashMap hmParam)throws AppError {
		
		GmSalesFilterConditionBean gmSalesFilterConditionBean =
		        new GmSalesFilterConditionBean(getGmDataStoreVO());
		String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
	    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
	    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));

	    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
	    String strAccountNmColumn =
	        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");
	    String strRepNmColumn =
	        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");
		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		sbQuery.append("SELECT t501.c501_order_id ORDID,");
		sbQuery.append("TO_CHAR(t501.c501_order_date, '");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("') odt,");
		sbQuery.append("get_total_order_amt (t501.c501_order_id,'"
				+ strCurrType + "') COST,");
		// sbQuery.append("get_account_name (t501.c704_account_id) aname,get_rep_name(t501.c703_sales_rep_id) repname, t501.c501_PARENT_ORDER_ID DO, ");
		sbQuery.append(" " + strAccountNmColumn + " aname, " + strRepNmColumn
				+ " repname, t501.c501_PARENT_ORDER_ID DO, ");
		sbQuery.append("get_user_name (t501.c501_created_by) nm, ");
		sbQuery.append("t501.c501_customer_po po,");
		sbQuery.append("t501.c901_Order_Type Ordertype,");
		sbQuery.append("t501.c501_po_amount poamount,");
		sbQuery.append("get_code_name(t501.c901_order_type) ordertypedesc,");
	    sbQuery.append("t501.c501_ship_cost shipcost,");
	    sbQuery.append("get_code_name(t5001.c901_po_disc_category_type) category,");
	    sbQuery.append("t501.c501_do_doc_upload_fl uploadfl,");
	    sbQuery.append("to_char(T501.C501_CUSTOMER_PO_DATE, '"+strApplDateFmt+"') poenterdt, ");
	    sbQuery.append("t501.c501_receive_mode ordmode, "); //PC-4864 Add Online PDF option on Pending PO Screen
		sbQuery.append("to_char(t501.c501_Shipping_Date,'");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("') sdate, t501.c501_hold_fl holdfl ");// --c501_status_fl
																// sfl,
		sbQuery.append("FROM t501_order t501 , t703_sales_rep T703, t704_account T704, T5001_ORDER_PO_DTLS T5001  ");
		sbQuery.append("WHERE t501.c501_void_fl IS NULL ");
		sbQuery.append("AND c501_delete_fl IS NULL ");
		sbQuery.append("AND t501.c501_status_fl IS NOT NULL ");
		sbQuery.append("AND t501.c503_invoice_id IS NULL ");
		sbQuery.append("AND NVL (t501.c901_order_type, -9999) <> 2535 ");
		sbQuery.append(" AND t5001.c5001_void_fl IS NULL  ");
		if (strPassName.equals("PendingApproval")) {
			sbQuery.append(" AND t5001.C901_CUST_PO_STATUS = 109541  "); // --Pending Approval PO Status(109541)
		}
		if (strPassName.equals("PendingDiscrepancy")) {
			sbQuery.append(" AND t5001.C901_CUST_PO_STATUS = 109544  "); // --Pending Discrepancy PO Status(109544)
		}
		// Filter Condition to fetch record based on access code
		if (!strCondition.toString().equals("")) {
			sbQuery.append(" AND ");
			sbQuery.append(strCondition);
		}
		sbQuery.append(getSalesFilterClause());
		sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
		sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id ");
		sbQuery.append(" AND T501.c501_order_id = t5001.c501_order_id ");
		sbQuery.append("ORDER BY aname,t501.c501_Order_Date DESC");
		return sbQuery;

	}
	
	/**
	   * checkCustomerPoId - THIS METHOD IS USED TO CHECK THE AVAIABLITY OF CUSTOMER POID
	   * 
	   * @param String hmParam
	   * @return String
	   * @exception AppError
	   */
	  public String checkCustomerPO(HashMap hmParam) throws AppError {
		String strPONumber = GmCommonClass.parseNull((String) hmParam.get("PO"));
		String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
		String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("CMPID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT COUNT (1)    FROM t501_order WHERE c501_customer_po   = '"+strPONumber+"' ");
		sbQuery.append(" AND c501_delete_fl IS NULL " +" AND c501_void_fl IS NULL "+" AND( c501_order_id !='"+strOrderId+"' ");
		sbQuery.append(" AND NVL(c501_parent_order_id,'-9999') !='"+strOrderId+"' )"+" AND (c901_ext_country_id IS NULL OR ");
		sbQuery.append(" c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)) ");
		sbQuery.append(" AND c1900_company_id ='"+strCompanyId+"'");
		log.debug("sbQuery==>" + sbQuery);
		
		String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

	    return strReturn;
	  }
	  
	  /**
	   * This method used to fetch the order list of having PO Number which pass into service
	   * 
	   * @param String hmParam
	   * @return String
	   * @exception AppError
	   */
	  public String fetchOrderPoList(HashMap hmParam) throws AppError {
			String strPONumber = GmCommonClass.parseNull((String) hmParam.get("PO"));
			String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("CMPID"));
			String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("CMPDFMT"));
			
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT( 'orderid' value C501_ORDER_ID, ");
			sbQuery.append(" 'orddt' value TO_CHAR(C501_ORDER_DATE_TIME,'"+strDateFmt+"'), ");
			sbQuery.append(" 'shiptrack' value gm_pkg_cm_shipping_info.get_track_no(c501_order_id,50180), ");
			sbQuery.append(" 'total' value gm_pkg_ar_po_trans.get_order_total_amt_by_po(C501_ORDER_ID,C501_CUSTOMER_PO), ");
			sbQuery.append(" 'repacnt' value T704.C704_ACCOUNT_NM, ");
			sbQuery.append(" 'usernm' value GET_USER_NAME(C501_CREATED_BY), ");
			sbQuery.append(" 'ponum' value SUBSTR(C501_CUSTOMER_PO,0,20), ");
			sbQuery.append(" 'invoice' value T503.C503_INVOICE_ID, ");
			sbQuery.append(" 'ordertype' value get_code_name(C901_ORDER_TYPE), ");
			sbQuery.append(" 'parentorderid' value C501_PARENT_ORDER_ID, ");
			sbQuery.append(" 'shipdt' value TO_CHAR(C501_SHIPPING_DATE,'"+strDateFmt+"') ");
			sbQuery.append(" )RETURNING CLOB) ");
			sbQuery.append(" FROM T501_ORDER T501,T704_ACCOUNT T704,T503_INVOICE T503 ");
			sbQuery.append(" WHERE C501_VOID_FL IS NULL AND T501.C503_INVOICE_ID = T503.C503_INVOICE_ID(+) ");
			sbQuery.append(" AND T503.C503_VOID_FL (+) IS NULL AND T501.C704_ACCOUNT_id = T704.C704_ACCOUNT_id ");
			sbQuery.append(" AND (C501_DELETE_FL  IS NULL  OR (C901_ORDER_TYPE  = '102080' AND C501_DELETE_FL  ='Y' )) ");
			sbQuery.append(" AND ( T501.C901_ext_country_id IS NULL OR T501.C901_ext_country_id    IN (SELECT country_id FROM V901_COUNTRY_CODES))");
			sbQuery.append(" AND C501_CUSTOMER_PO='"+strPONumber+"' AND NVL (c901_order_type, - 9999) NOT IN (SELECT t906.c906_rule_value "); 
			sbQuery.append(" FROM t906_rules t906 WHERE t906.c906_rule_grp_id = 'ORDTYPE' AND c906_rule_id = 'EXCLUDE' ) ");
			sbQuery.append(" AND T501.C1900_COMPANY_ID ="+strCompanyId+" ORDER BY C501_ORDER_DATE_TIME DESC ");
			log.debug("sbQuery==>" + sbQuery);
			
			String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

		    return strReturn;
	  }
	  
	  /**
	   * This method used to fetch the Access flag information for the user have a permission access to uploaded file
	   * 
	   * @param String hmParam
	   * @return String
	   * @exception AppError
	   */
	  public String fetchAccessFlInfo(HashMap hmParam) throws AppError {
			String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
			String strFunctionId = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
			
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT( 'ID' value t1530.c1530_access_id, ");
			sbQuery.append(" 'updfl' value t1530.c1530_update_access_fl, ");
			sbQuery.append(" 'readfl' value t1530.c1530_read_access_fl, ");
			sbQuery.append(" 'voidfl' value t1530.c1530_void_access_fl, ");
			sbQuery.append(" 'groupId' value t1530.c1500_group_id ");
			sbQuery.append(" )RETURNING CLOB) ");
			sbQuery.append("  FROM t1530_access t1530     WHERE (    t1530.c1530_void_fl IS NULL ");
			sbQuery.append("  AND (   t1530.c101_party_id = '" + strPartyId + "' ");
			sbQuery.append("   OR t1530.c1500_group_id IN ( SELECT NVL (t1501.c1500_group_id, ");
			sbQuery.append("   t1501.c1500_mapped_group_id) FROM t1501_group_mapping t1501, t1530_access t1530 ");
			sbQuery.append("   WHERE t1501.c101_mapped_party_id = '" + strPartyId + "' ");
			sbQuery.append(" AND t1530.c1500_group_id =  NVL (t1501.c1500_group_id, t1501.c1500_mapped_group_id) ");
  			sbQuery.append(" and c1501_void_fl is null)) ");
  			sbQuery.append("  AND (   t1530.c1520_function_id = '" + strFunctionId + "' ");
			sbQuery.append("  OR t1530.c1520_function_id IN (SELECT t1520.c1520_function_id FROM t1520_function_list t1520");
			sbQuery.append("   WHERE t1520.c1520_inherited_from_id = '" + strFunctionId + "' ))) ");
			log.debug("sbQuery==>" + sbQuery);
			
			String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

		    return strReturn;
	  }
	
}
