package com.globus.sales.DashBoard.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;


public class GmSalesDashConfigDrilldownBean {
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance();
	/**
	 * fetchDrilldownGroup - This method will fetch the Column Name Which will be displayed in the drill down for Sales Dash Board.
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchDrilldownGroup(HashMap hmParam)throws AppError{
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		
		gmDBManager.setPrepareString("gm_pkg_sm_confg_drilldown.gm_fch_drilldown_grp", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1)));
		gmDBManager.close();
		return alReturn;
	}
	/**
	 * fetchDrilldownDetails - This method will fetch the Details Of The Column Name Which will be displayed in the drill down for Sales Dash Board.
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchDrilldownDetails(HashMap hmParam)throws AppError{
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		String strddgrpId		       = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		gmDBManager.setPrepareString("gm_pkg_sm_confg_drilldown.gm_fch_drilldown_dtl", 2);
		gmDBManager.setString(1,strddgrpId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		gmDBManager.close();
		return alReturn;
	}
	
}