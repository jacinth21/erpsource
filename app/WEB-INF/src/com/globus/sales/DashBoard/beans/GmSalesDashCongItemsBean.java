package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesDashCongItemsBean extends GmSalesFilterConditionBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmSalesDashCongItemsBeanQuery gmCongItemQuery= new GmSalesDashCongItemsBeanQuery();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


	  public GmSalesDashCongItemsBean() {
		  super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	  
	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmSalesDashCongItemsBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }	
	  
	  /**
		 * Count and Drill down calls are separated into two methods.
		 * Removed the unwanted the rows and where condition check while fetching the count.
		 * Database Object Creation - Changed the object creation one per class instead of one per method.
		 * Methods Modified: fetchCongItemsCount, fetchApprCongItems, fetchBackOrderCongItems, fetchShippedCongItems, fetchCongItemsAAS,
		 * fetchApprovedCongItems
		 * @author gpalani PMT-10095 (RFC2252)
		 * @since 2017-01-03
		 */
		
		
	public ArrayList fetchCongItemsCount(HashMap hmParam) throws AppError {

		//initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		String strTotals = GmCommonClass.parseNull((String) hmParam.get("TOTALS"));
		 //To show account item consignment reports based on the below rule(PMT-29390)
		String strAccItemConFl = GmCommonClass.parseNull((String) hmParam.get("ACCITEMCONRPTFL"));
		//to avoid calling the query without the Account Item Consign Flag check
		String strAccItemConRuleVal = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
      strAccItemConFl=strAccItemConFl.equals("")?strAccItemConRuleVal:strAccItemConFl;
	      if(strAccItemConFl.equals("Y"))
	        {
	        sbQuery = fetchAccCongItemsCount(hmParam);
	        }
	      else
	      {
    		// return count of consignment items
    		if(strTotals.equals("Y"))
    		{
    			if(strPassName.equals("Approved")){
    				sbQuery = gmCongItemQuery.fetchApprovedCountCongItems(hmParam);
    			}
    			if(strPassName.equals("Shipped")){
    				sbQuery = gmCongItemQuery.fetchShippedCountCongItems(hmParam);
    			}
    			if(strPassName.equals("BO")){
    				sbQuery = gmCongItemQuery.fetchBackOrderCountCongItems(hmParam);
    			}
    			if(strPassName.equals("AAS")){
    				sbQuery = gmCongItemQuery.fetchAASCountCongItems(hmParam);
    			}
    			
    			if(strPassName.equals("Requested"))
    			{
    				sbQuery = gmCongItemQuery.fetchRequestedCountCongItems(hmParam);
    
    			}
    		}
	      }
			
		log.debug("sbQuery***********==>"+sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}
	
	public ArrayList fetchApprCongItems(HashMap hmParam) throws AppError {

	//	initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		 //To show account item consignment reports based on the below rule(PMT-29390)
	    String strAccItemConFl = GmCommonClass.parseNull((String) hmParam.get("ACCITEMCONRPTFL"));

	          if(strAccItemConFl.equals("Y"))
	            {
	            sbQuery = gmCongItemQuery.fetchAccountCongDrillDownItems(hmParam);
	            }
	          else
	          {
		sbQuery = gmCongItemQuery.fetchApprovedDrillDownCongItems(hmParam);
	          }
		log.debug("sbQuery==>"+sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}
	
	public ArrayList fetchBackOrderCongItems(HashMap hmParam) throws AppError {

	//	initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		 //To show account item consignment reports based on the below rule(PMT-29390)
	    String strAccItemConFl = GmCommonClass.parseNull((String) hmParam.get("ACCITEMCONRPTFL"));
	     
	              if(strAccItemConFl.equals("Y"))
	                {
	                sbQuery = gmCongItemQuery.fetchAccountCongDrillDownItems(hmParam);
	                }
	              else
	              {
		sbQuery = gmCongItemQuery.fetchBackOrderDrillDownCongItems(hmParam);
	              }
		log.debug("sbQuery==>"+sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}
	
	public ArrayList fetchShippedCongItems(HashMap hmParam) throws AppError {
		//initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		 //To show account item consignment reports based on the below rule(PMT-29390)
	      String strAccItemConFl = GmCommonClass.parseNull((String) hmParam.get("ACCITEMCONRPTFL"));

	              if(strAccItemConFl.equals("Y"))
	                {
	                sbQuery = gmCongItemQuery.fetchAccountCongDrillDownItems(hmParam);
	                }
	              else
	              {
		sbQuery = gmCongItemQuery.fetchShippedDrillDownCongItems(hmParam);	
	              }
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}
	
	public ArrayList fetchCongItemsAAS(HashMap hmParam) throws AppError {

		initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		sbQuery = gmCongItemQuery.fetchAASDrillDownCongItems(hmParam);		
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}
	
	public ArrayList fetchApprovedCongItems(HashMap hmParam){
		
	//	initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		 //To show account item consignment reports based on the below rule(PMT-29390)
		  String strAccItemConFl = GmCommonClass.parseNull((String) hmParam.get("ACCITEMCONRPTFL"));
		  String strAccItemConRuleVal = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
	            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
		  strAccItemConFl=strAccItemConFl.equals("")?strAccItemConRuleVal:strAccItemConFl;
          if(strAccItemConFl.equals("Y"))
            {
             if(!strPassName.equals("Requested")){
	              sbQuery = gmCongItemQuery.fetchRequestedCountAccCongItems(hmParam);
	         }
	                
	         else if (strPassName.equals("Requested"))
	         {
	              sbQuery = gmCongItemQuery.fetchRequestedDrillDownAccCongItems(hmParam);
	         }
            }
          else
          {
	
        		if(!strPassName.equals("Requested")){
        			sbQuery = gmCongItemQuery.fetchRequestedCountCongItems(hmParam);
        		}
        		
        		else if (strPassName.equals("Requested") )
        		{
        			sbQuery = gmCongItemQuery.fetchRequestedDrillDownCongItems(hmParam);
        		}
          }	
		log.debug("sbQuery===>"+sbQuery);
		alReturn=gmDBManager.queryMultipleRecords(sbQuery.toString());
		
		return alReturn;
	}

    /**
     * fetchAccCongItemsCount - This method is used to fetch count of the account Item Consignment
     * 
     * @param HashMap - hmParam
     * @return String Buffer sbQuery
     * 
     */   
	   public StringBuffer fetchAccCongItemsCount(HashMap hmParam) throws AppError {

	      
	       
	        StringBuffer sbQuery = new StringBuffer();
	        String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
	        String strTotals = GmCommonClass.parseNull((String) hmParam.get("TOTALS"));
	      

	        // return count of consignment items
	        if(strTotals.equals("Y"))
	        {
	            if(strPassName.equals("Approved_BO")){
	                sbQuery = gmCongItemQuery.fetchCountAccCongItems(hmParam);
	            }
	            if(strPassName.equals("Shipped")){
	                sbQuery = gmCongItemQuery.fetchShippedCountAccCongItems(hmParam);
	            }

	        }

	        return sbQuery;
	    }
}
