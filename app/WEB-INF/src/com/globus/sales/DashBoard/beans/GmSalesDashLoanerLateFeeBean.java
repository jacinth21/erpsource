package com.globus.sales.DashBoard.beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.lang.Enum;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * @author sindhu
 *
 */
public class GmSalesDashLoanerLateFeeBean extends GmSalesFilterConditionBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	GmSalesDashLoanerBeanQuery gmLoanerQuery = new GmSalesDashLoanerBeanQuery();

	public GmSalesDashLoanerLateFeeBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	public GmSalesDashLoanerLateFeeBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * fetchLoanerAccrLateFeeTotal-to fetch loaner late fee calculated count
	 */
	public ArrayList fetchLoanerAccrLateFeeTotal(HashMap hmParam) throws AppError, Exception {
		initializeParameters(hmParam);
		ArrayList alReturn = new ArrayList();
		HashMap hmDateRange = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		ArrayList alReturnList = new ArrayList();
		
		hmDateRange = getLoanerLateFeeDateRange(); 
		log.debug("hmDateRange===> " + hmDateRange);
		
		hmParam.put("STARTDATE", GmCommonClass.parseNull((String) hmDateRange.get("PREMONTHSTARTDATE")));
		hmParam.put("ENDDATE", GmCommonClass.parseNull((String) hmDateRange.get("PREMONTHENDDATE")));
		hmParam.put("PASSNAME", "LateFeeAccrStmt");
		
		sbQuery = fetchLoanerAccrLateFeeTotalQuery(hmParam);
		alReturnList = gmDBManager.queryMultipleRecords(sbQuery.toString());
		alReturn.addAll(alReturnList);
		hmParam.put("STARTDATE", GmCommonClass.parseNull((String) hmDateRange.get("CURRMONTHSTARTDATE")));
		hmParam.put("ENDDATE", GmCommonClass.parseNull((String) hmDateRange.get("CURRMONTHENDDATE")));
		hmParam.put("PASSNAME", "RecentLateFeeAccr");
		sbQuery = fetchLoanerRecentLateFeeTotalQuery(hmParam);
		alReturnList = gmDBManager.queryMultipleRecords(sbQuery.toString());
		alReturn.addAll(alReturnList);
		
		log.debug("fetchLoanerAccrLateFeeTotal===> " + alReturn.size());
		return alReturn;
	}
	
	/**
	 * getLoanerLateFeeDateRange-to get the date range
	 */
	public HashMap getLoanerLateFeeDateRange() throws AppError {
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		TimeZone timeZone = TimeZone.getTimeZone(getGmDataStoreVO().getCmptzone());
        Calendar cal = Calendar.getInstance();
        Calendar curDate = Calendar.getInstance();

        SimpleDateFormat fmt = new SimpleDateFormat(getGmDataStoreVO().getCmpdfmt());

       if(curDate.get(Calendar.DAY_OF_MONTH)>15){
            cal.add(Calendar.MONTH, -1); 
       }else{
           cal.add(Calendar.MONTH, -2);
       }
       
       cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
       hmReturn.put("PREMONTHSTARTDATE",fmt.format(cal.getTime()));
       
       cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));  
       hmReturn.put("PREMONTHENDDATE",fmt.format(cal.getTime()));
       
       cal.add(Calendar.DATE, 1);
       hmReturn.put("CURRMONTHSTARTDATE",fmt.format(cal.getTime()));
        
       hmReturn.put("CURRMONTHENDDATE",fmt.format(curDate.getTime()));

		return hmReturn;
	}
	
	/**
	 * fetchLoanerAccrLateFeeTotalQuery-to get name and amount of loaner late fee
	 */
	public StringBuffer fetchLoanerAccrLateFeeTotalQuery(HashMap hmParam) throws AppError, Exception {
		 initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		
		String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strPassNm = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		
		sbQuery.append(" SELECT   GET_RULE_VALUE('"+strPassNm+"','LATEFEEDASHLABEL') NAME, NVL(sum(t9201.C9201_AMOUNT),0) total ");
		sbQuery.append(" FROM T9200_Incident T9200 , ");
		sbQuery.append(" T9201_Charge_Details T9201 ");
		sbQuery.append(getAccessFilterClauseWithRepID());
		sbQuery.append(" WHERE T9200.C9200_Incident_Id = T9201.C9200_Incident_Id ");
		sbQuery.append(" AND t9201.C703_Sales_Rep_Id = v700.rep_id ");
		sbQuery.append(" AND T9200.C9200_Void_Fl      IS NULL ");
		sbQuery.append(" AND T9200.C901_Incident_Type  = '92068' "); //Late Fee(By Set)
		sbQuery.append(" AND t9200.C1900_company_id  = " + getCompId() +" AND ");
		sbQuery.append(" t9201.c9201_status in (10,15,20,25) ");//Submitted,Accrued,Hold,credit
		sbQuery.append(" AND t9201.c9201_void_fl     IS NULL ");
    	sbQuery.append(" AND ((trunc(t9201.C9201_CHARGE_START_DATE) >=trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_START_DATE) <=  trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"'))) ");
    	sbQuery.append(" OR (trunc(t9201.C9201_CHARGE_END_DATE) >= trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_END_DATE) <= trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"')))) ");

    	log.debug("sbQuery.toString()---> "+sbQuery.toString());
		return sbQuery;
	}
	
	/**
	 * fetchLoanerRecentLateFeeTotalQuery-to get name and amount of Recent loaner late fee
	 */
	public StringBuffer fetchLoanerRecentLateFeeTotalQuery(HashMap hmParam) throws AppError, Exception {
		 initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strPassNm = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		
		sbQuery.append(" SELECT   GET_RULE_VALUE('"+strPassNm+"','LATEFEEDASHLABEL') NAME, NVL(sum(t9201.C9201_AMOUNT),0) total ");
		sbQuery.append(" FROM T9200_Incident T9200 , ");
		sbQuery.append(" T9201_Charge_Details T9201 ");
		sbQuery.append(getAccessFilterClauseWithRepID());
		sbQuery.append(" WHERE T9200.C9200_Incident_Id = T9201.C9200_Incident_Id ");
		sbQuery.append(" AND t9201.C703_Sales_Rep_Id = v700.rep_id ");
		sbQuery.append(" AND T9200.C9200_Void_Fl      IS NULL ");
		sbQuery.append(" AND T9200.C901_Incident_Type  = '92068' "); //Late Fee(By Set)
		sbQuery.append(" AND t9200.C1900_company_id  = " + getCompId() +" AND ");
		sbQuery.append(" t9201.c9201_status in (10,15,20,25) ");//Submitted,Accrued,Hold,credit
		sbQuery.append(" AND t9201.c9201_void_fl     IS NULL ");
    	sbQuery.append(" AND ((trunc(t9201.C9201_CHARGE_START_DATE) >=trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_START_DATE) <=  trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"'))) ");
    	sbQuery.append(" OR (trunc(t9201.C9201_CHARGE_END_DATE) >= trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_END_DATE) <= trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"')))) ");

    	log.debug("sbQuery.toString()-Recent--> "+sbQuery.toString());
		return sbQuery;
	}
	
	/**
	 * fetchLoanerAccrLateFeeDetail-to get the loaner late fee details for window opener
	 */
	public ArrayList fetchLoanerAccrLateFeeDetail(HashMap hmParam) throws AppError, Exception {
		initializeParameters(hmParam);
		ArrayList alReturn = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strPassNm = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
			
		sbQuery.append("  SELECT T9200.c9200_ref_id, v700.d_name distname, v700.rep_name repname, t703ass.c703_sales_rep_name assrepname,");
		sbQuery.append("   v700.d_name FLDSLS, "); //add duplicate column for ipad
		sbQuery.append(" t703ass.c703_sales_rep_name assocrepname, "); //add duplicate column for ipad
		sbQuery.append(" mv.C525_PRODUCT_REQUEST_ID reqid, ");
		sbQuery.append(" get_account_name(t525.c704_account_id) acctname, ");
		sbQuery.append(" mv.C207_SET_ID setid , ");
		sbQuery.append(" t207.C207_Set_nm SNAME, ");
		sbQuery.append("  mv.TAGID tagid,");
		sbQuery.append("  mv.C504a_Etch_Id etchid,");
		sbQuery.append("  mv.C504a_Loaner_Dt LOANDATE,");
		sbQuery.append("  mv.C504a_Loaner_Dt ldatefmt,"); //add duplicate column for ipad
		sbQuery.append("  mv.C504a_Expected_Return_Dt EXPRETDATEFMT,");
		sbQuery.append("  mv.C504A_RETURN_DT returneddt,"); 
		sbQuery.append("  mv.C504A_RETURN_DT actreturndt,"); //add duplicate column for ipad
		sbQuery.append("  gm_pkg_op_loaner_charges.Get_Work_Days_Count(mv.C504a_Expected_Return_Dt, NVL (mv.C504A_RETURN_DT, CURRENT_DATE)) overdue,");
		sbQuery.append("  gm_pkg_op_loaner_charges.Get_Work_Days_Count(mv.C504a_Expected_Return_Dt, NVL (mv.C504A_RETURN_DT, CURRENT_DATE)) DAYSOVERDUE,"); //add duplicate column for ipad
		sbQuery.append("  gm_pkg_op_loaner_charges_rpt.get_late_fee_status_name(t9201.c9201_status)CHARGESTS ,");
		sbQuery.append("  t9201.C9201_CHARGE_START_DATE chargestartdt,");
		sbQuery.append("  t9201.C9201_CHARGE_END_DATE chargeenddt,");
		sbQuery.append("  t9201.c9201_deduction_date DEDUCTIONDT,");
	    sbQuery.append("  T9200.C9200_INCIDENT_DATE INCIDENTDT,");
	    sbQuery.append("  t9201.C9201_AMOUNT CHARGEAMT");
	    sbQuery.append("  FROM T9200_Incident T9200 ,");
	    sbQuery.append("  T9201_Charge_Details T9201,");
	    sbQuery.append("  v_loaner_late_fee mv, "); 
	    sbQuery.append("  t703_sales_rep t703ass, ");
	    sbQuery.append("  t207_set_master t207, ");
	    sbQuery.append(" t525_product_request t525, ");
	    sbQuery.append(" t526_product_request_detail t526 ");
	    sbQuery.append(getAccessFilterClauseWithRepID());
	    sbQuery.append(" WHERE T9200.C9200_Incident_Id = T9201.C9200_Incident_Id ");
	    sbQuery.append(" AND T9200.C9200_Ref_Id = mv.c526_product_request_detail_id");
	    sbQuery.append(" AND T9201.C703_Sales_Rep_Id = v700.rep_id");
	    sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id");
	    sbQuery.append(" and mv.c526_product_request_detail_id = t526.c526_product_request_detail_id");
	    sbQuery.append(" AND t525.c525_void_fl IS NULL");
	    sbQuery.append(" AND t526.c526_void_fl IS NULL");
	    sbQuery.append(" AND mv.c207_set_id =t207.c207_set_id");
	    sbQuery.append(" AND t9201.c703_ass_rep_id = t703ass.c703_sales_rep_id(+)");
	    sbQuery.append(" AND T9200.C9200_Void_Fl IS NULL");
	    sbQuery.append(" AND T9200.C901_Incident_Type = '92068'");
	    sbQuery.append(" AND t9200.C1900_company_id = " + getCompId() +" And ");
	    sbQuery.append(" t9201.c9201_status in (10,15,20,25)");//Submitted,Accrued,Hold,credit
	    sbQuery.append(" AND t9201.c9201_void_fl IS NULL");
	    sbQuery.append(" AND ((trunc(t9201.C9201_CHARGE_START_DATE) >= trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_START_DATE) <=  trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"'))) ");
	    sbQuery.append(" OR (trunc(t9201.C9201_CHARGE_END_DATE) >= trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_END_DATE) <= trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"')))) ");
	    sbQuery.append(" ORDER BY t9201.C9201_CHARGE_START_DATE,t9201.C9201_CHARGE_END_DATE");
	    log.debug("fetchLoanerAccrLateFeeDetail "+sbQuery.toString());
	   alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
	   return alReturn;

	}
	
	/**
	 * fetchLoanerRecentLateFeeDetail-to get the loaner late fee details for window opener
	 */
	public ArrayList fetchLoanerRecentLateFeeDetail(HashMap hmParam) throws AppError, Exception {
		initializeParameters(hmParam);
		ArrayList alReturn = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
		String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
		String strPassNm = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		
		sbQuery.append("  SELECT T9200.c9200_ref_id, v700.d_name distname, v700.rep_name repname, t703ass.c703_sales_rep_name assrepname,");
		sbQuery.append("   v700.d_name FLDSLS, "); //add duplicate column for ipad
		sbQuery.append(" t703ass.c703_sales_rep_name assocrepname, "); //add duplicate column for ipad
		sbQuery.append(" mv.C525_PRODUCT_REQUEST_ID reqid, ");
		sbQuery.append(" get_account_name(t525.c704_account_id) acctname, ");
		sbQuery.append(" mv.C207_SET_ID setid , ");
		sbQuery.append(" t207.C207_Set_nm SNAME, ");
		sbQuery.append("  mv.TAGID tagid,");
		sbQuery.append("  mv.C504a_Etch_Id etchid,");
		sbQuery.append("  mv.C504a_Loaner_Dt LOANDATE,");
		sbQuery.append("  mv.C504a_Loaner_Dt ldatefmt,"); //add duplicate column for ipad
		sbQuery.append("  mv.C504a_Expected_Return_Dt EXPRETDATEFMT,");
		sbQuery.append("  mv.C504A_RETURN_DT returneddt,"); 
		sbQuery.append("  mv.C504A_RETURN_DT actreturndt,"); //add duplicate column for ipad
		sbQuery.append("  gm_pkg_op_loaner_charges.Get_Work_Days_Count(mv.C504a_Expected_Return_Dt, NVL (mv.C504A_RETURN_DT, CURRENT_DATE)) overdue,");
		sbQuery.append("  gm_pkg_op_loaner_charges.Get_Work_Days_Count(mv.C504a_Expected_Return_Dt, NVL (mv.C504A_RETURN_DT, CURRENT_DATE)) DAYSOVERDUE,"); //add duplicate column for ipad
		sbQuery.append("  gm_pkg_op_loaner_charges_rpt.get_late_fee_status_name(t9201.c9201_status)CHARGESTS ,");
		sbQuery.append("  t9201.C9201_CHARGE_START_DATE chargestartdt,");
		sbQuery.append("  t9201.C9201_CHARGE_END_DATE chargeenddt,");
		sbQuery.append("  t9201.c9201_deduction_date DEDUCTIONDT,");
	    sbQuery.append("  T9200.C9200_INCIDENT_DATE INCIDENTDT,");
	    sbQuery.append("  t9201.C9201_AMOUNT CHARGEAMT");
	    sbQuery.append("  FROM T9200_Incident T9200 ,");
	    sbQuery.append("  T9201_Charge_Details T9201,");
	    sbQuery.append("  v_loaner_late_fee mv, "); 
	    sbQuery.append("  t703_sales_rep t703ass, ");
	    sbQuery.append("  t207_set_master t207, ");
	    sbQuery.append(" t525_product_request t525, ");
	    sbQuery.append(" t526_product_request_detail t526 ");
	    sbQuery.append(getAccessFilterClauseWithRepID());
	    sbQuery.append(" WHERE T9200.C9200_Incident_Id = T9201.C9200_Incident_Id ");
	    sbQuery.append(" AND T9200.C9200_Ref_Id = mv.c526_product_request_detail_id");
	    sbQuery.append(" AND T9201.C703_Sales_Rep_Id = v700.rep_id");
	    sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id");
	    sbQuery.append(" and mv.c526_product_request_detail_id = t526.c526_product_request_detail_id");
	    sbQuery.append(" AND t525.c525_void_fl IS NULL");
	    sbQuery.append(" AND t526.c526_void_fl IS NULL");
	    sbQuery.append(" AND mv.c207_set_id =t207.c207_set_id");
	    sbQuery.append(" AND t9201.c703_ass_rep_id = t703ass.c703_sales_rep_id(+)");
	    sbQuery.append(" AND T9200.C9200_Void_Fl IS NULL");
	    sbQuery.append(" AND T9200.C901_Incident_Type = '92068'");
	    sbQuery.append(" AND t9200.C1900_company_id = " + getCompId() +" And ");
	    sbQuery.append(" t9201.c9201_status in (10,15,20,25) ");//Submitted,Accrued,Hold,credit
	    sbQuery.append(" AND t9201.c9201_void_fl IS NULL");
	    sbQuery.append(" AND ((trunc(t9201.C9201_CHARGE_START_DATE) >=trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_START_DATE) <=  trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"'))) ");
	    sbQuery.append(" OR (trunc(t9201.C9201_CHARGE_END_DATE) >= trunc(TO_DATE('" + strStartDt + "','"+ strDateFmt +"')) AND trunc(t9201.C9201_CHARGE_END_DATE) <= trunc(TO_DATE('" + strEndDt + "','"+ strDateFmt +"')))) ");
	    sbQuery.append(" ORDER BY t9201.C9201_CHARGE_START_DATE,t9201.C9201_CHARGE_END_DATE");
	    log.debug("fetchLoanerRecentLateFeeDetail "+sbQuery.toString());
	   alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
	   
	   return alReturn;

	}
	
}
