package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesDashOrdersBean extends GmSalesFilterConditionBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	GmSalesDashOrdersBeanQuery gmOrderQuery = new GmSalesDashOrdersBeanQuery();


	// this will be removed all place changed with Data Store VO constructor

	public GmSalesDashOrdersBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmSalesDashOrdersBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/*
	 * Returns Submitted Orders as a count or detail
	 */
	
	/**
	 * 
	 * Count and Drill down calls are separated into two methods.
	 * Removed the unwanted the rows and where condition check while fetching the count.
	 * Database Object Creation - Changed the object creation one per class instead of one per method.
	 * Methods Modified: fetchSubmittedOrders, fetchShippedOrders, fetchHeldOrders, fetchBackOrders, fetchPOs
	 * @author gpalani PMT-10095 (RFC2252)
	 * @since 2017-01-03
	 */
	

	public ArrayList fetchSubmittedOrders(HashMap hmParam) throws AppError

	{
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strTotals = GmCommonClass.parseNull((String) hmParam.get("TOTALS"));
		String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

		// Show Totals
		if (strTotals.equals("Y")) {
			sbQuery = gmOrderQuery.fetchSubmittedOrdersCount(hmParam);
		}

		else if (!strTotals.equals("Y")) {
			if(strOpt.equals("DOREPORT")){
				sbQuery = gmOrderQuery.fetchSubmittedOrdersForDOReport(hmParam);
			}
			else{
				sbQuery = gmOrderQuery.fetchSubmittedOrdersDrillDown(hmParam);
			}
		}
		
		

		log.debug("sbQuery==>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	public ArrayList fetchShippedOrders(HashMap hmParam) throws AppError {

		ArrayList alReturn = null;

		StringBuffer sbQuery = new StringBuffer();
		String strTotals = GmCommonClass.parseNull((String) hmParam.get("TOTALS"));

		// Show Shipped Order Totals
		if (strTotals.equals("Y")) {
			sbQuery = gmOrderQuery.fetchShippedOrdersCount(hmParam);
		}

		// Show Shipped Order Details
		else if (!strTotals.equals("Y"))

		{
			sbQuery = gmOrderQuery.fetchShippedOrdersDrillDown(hmParam);
		}

		log.debug("sbQuery==>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	public ArrayList fetchHeldOrders(HashMap hmParam) throws AppError {
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
        
		String strTotals = GmCommonClass.parseNull((String) hmParam.get("TOTALS"));

		// Show Held Order Totals
		if (strTotals.equals("Y")) {
			sbQuery = gmOrderQuery.fetchHeldOrdersCount(hmParam);
		}

		// Show Held Order Details
		else if (!strTotals.equals("Y"))

		{
			sbQuery = gmOrderQuery.fetchHeldOrdersDrillDown(hmParam);

		}

		log.debug("sbQuery==>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}
	
	public ArrayList fetchBackOrders(HashMap hmParam) throws AppError {
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strTotals = GmCommonClass.parseNull((String) hmParam.get("TOTALS"));

		// Show Totals
		if (strTotals.equals("Y")) {
			sbQuery = gmOrderQuery.fetchBackOrdersCount(hmParam);
		}

		else if (!strTotals.equals("Y"))

		{
			sbQuery = gmOrderQuery.fetchBackOrdersDrillDown(hmParam);

		}

		log.debug("sbQuery==>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	public ArrayList fetchPOs(HashMap hmParam) throws AppError {
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
		String strTotals = GmCommonClass.parseNull((String) hmParam.get("TOTALS"));
	
		// Show Totals
		if (strTotals.equals("Y")) 
		{

			if (strPassName.equals("PO21day")) 
			{
				sbQuery = gmOrderQuery.fetchPOGreaterthan21daysCount(hmParam);

			}

			if (strPassName.equals("PO14-21day"))

			{
				sbQuery = gmOrderQuery.fetchPO14to21daysCount(hmParam);
			}

			if (strPassName.equals("PO14day")) {
				sbQuery = gmOrderQuery.fetchPOLessthan14daysCount(hmParam);
			}

		}

		else if (!strTotals.equals("Y"))

		{
			sbQuery = gmOrderQuery.fetchPOsDrillDown(hmParam);
		}

		log.debug("sbQuery==>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}
}
