package com.globus.sales.DashBoard.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.beans.GmSalesFilterConditionBean;



public class GmSalesDashOrdersBeanQuery extends GmSalesFilterConditionBean

{
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Count and Drill down calls are separated into two methods. Removed the unwanted the rows and
   * where condition check while fetching the count. Database Object Creation - Changed the object
   * creation one per class instead of one per method. @ NEW Java File creation. Methods Added:
   * fetchSubmittedOrdersCount, fetchSubmittedOrdersDrillDown, fetchShippedOrdersCount,
   * fetchShippedOrdersDrillDown, fetchHeldOrdersCount, fetchHeldOrdersDrillDown,
   * fetchBackOrdersCount, fetchBackOrdersDrillDown, fetchPOLessthan14daysCount,
   * fetchPO14to21daysCount, fetchPOGreaterthan21daysCount, fetchPOsDrillDown
   * 
   * @author gpalani PMT-10095 RFC2252
   * @since 2017-01-03
   */



  // To fetch Submitted Orders Count
  public StringBuffer fetchSubmittedOrdersCount(HashMap hmParam) throws AppError

  {
    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);
    // The Below Code (strOrderStatusRuleValue is added for PMT-5988 and here we are getting Order
    // status flag value from
    // Rule Table.To Device we are displaying PIP and RFP Status
    String strOrderStatusRuleValue =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DOREPORT", "ORDSTATUS"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("DOID"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));

    sbQuery.append("SELECT 'Submitted' name, count(A.C501_Status_Fl) total FROM ( ");
    sbQuery.append("SELECT t501.C501_Status_Fl");
    sbQuery.append(" FROM t501_Order t501, t703_sales_rep T703, t704_account T704 ");
    sbQuery.append(" WHERE t501.c501_Delete_fl IS NULL ");
    if (!strPassName.equals("includevoidedorders")) {
      sbQuery.append("AND t501.c501_void_fl IS NULL ");
      if (strOpt.equals("DOREPORT")) {
        sbQuery
            .append(" AND (t501.c501_parent_order_id IS NULL  OR get_order_attribute_value(t501.c501_order_id,4000536) = 4000537) ");
      }
    }

    if (strOpt.equals("todaysales"))

    {// //this code only for today sales popup screen
      sbQuery.append(" AND TRUNC(t501.c501_order_date) = TRUNC(CURRENT_DATE) ");
    }

    else if (!strOpt.equals("DOREPORT"))

    // No need to check below condition for today sales and DO Report
    {
      sbQuery.append("AND (t501.c901_ext_country_id IS NULL ");
      sbQuery
          .append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
      sbQuery.append("AND t501.c501_Shipping_Date IS NULL ");
      sbQuery.append("AND t501.c501_status_Fl < 3 "); // 0 - BO, 1 - pending control, 2 - pending
                                                      // shipment
    }

    // below condition for only DO Report
    if (strOpt.equals("DOREPORT")) {
      sbQuery.append("AND (t501.c901_ext_country_id IS NULL ");
      sbQuery
          .append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
      // 2-Shipped, 3-Shipped, 8-Pending CS Confirmation
      sbQuery.append("AND t501.c501_status_Fl in (" + strOrderStatusRuleValue + ") ");
      sbQuery.append(" AND NVL (t501.c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'DOAPPEXCLUDE') ");
    }

    else {
      // this condition will be included other than DO Report
      sbQuery.append(getSalesFilterClause());

    }


    if (!strAccountID.equals(""))// this code only for today sales popup screen
    {
      sbQuery.append(" AND t501.c704_account_id = ");
      sbQuery.append(strAccountID);
    }
    if (!strOrderId.equals("")) {
      sbQuery.append(" AND (t501.c501_order_id   IN('");
      sbQuery.append(strOrderId);
      sbQuery.append("') or t501.c501_parent_order_id in ('");
      sbQuery.append(strOrderId);
      sbQuery.append("') AND t501.c501_void_fl IS not NULL)");
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.equals("")) {
      sbQuery.append(" AND ( ");
      sbQuery.append(strCondition);

      /*
       * This condition is to get the orders (for DO Report) that the logged in user has placed,this
       * will be handy when a sales rep is covering for another rep,so the DO Report will also show
       * the orders that logged in user has placed as well.
       */
      if (strOpt.equals("DOREPORT") && !strUserID.equals("")) {
        sbQuery.append(" or t501.c501_created_by='").append(strUserID).append("'");
      }
      sbQuery.append(")");

    }
    if (!strFromDate.equals("") && !strToDate.equals("")) {
      sbQuery.append(" AND C501_ORDER_DATE BETWEEN to_date('");
      sbQuery.append(strFromDate);
      sbQuery.append("','");
      sbQuery.append(strApplDateFmt);
      sbQuery.append("') AND to_date('");
      sbQuery.append(strToDate);
      sbQuery.append("','");
      sbQuery.append(strApplDateFmt);
      sbQuery.append("') ");
    }
    // if all values through do report is empty then it will default load current date orders
    if (strOpt.equals("DOREPORT") && strFromDate.equals("") && strToDate.equals("")
        && (strAccountID.equals("") || strAccountID.equals("0")) && strOrderId.equals("")) {
      sbQuery.append(" AND TRUNC(C501_ORDER_DATE) = TRUNC(CURRENT_DATE) ");
    }
    sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
    sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id ");
    sbQuery.append(") A ");


    return sbQuery;
  }

  // To fetch Submitted Orders Drill down
  public StringBuffer fetchSubmittedOrdersDrillDown(HashMap hmParam) throws AppError

  {
    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);
    String strOrderStatusRuleValue =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DOREPORT", "ORDSTATUS"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("DOID"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    //Used For DO Report based on LOT and Surgeon Name
    String strlotno = GmCommonClass.parseNull((String) hmParam.get("LOTNUMBER"));
    String strSurgeon = GmCommonClass.parseNull((String) hmParam.get("SURGEONNAME"));


    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strCompId = getCompId();
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");


    sbQuery.append("SELECT t501.C501_Status_Fl, t501.C501_Order_Id ORDID, ");
    sbQuery.append("to_char(t501.c501_order_date,'");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') odt, ");
    if (strOpt.equals("DOREPORT")) {
      sbQuery.append(" get_total_do_amt(t501.c501_order_id,'" + strCurrType + "') COST, ");
    }

    else

    {
      sbQuery.append(" get_total_order_amt(t501.c501_order_id,'" + strCurrType + "') COST, ");
    }

    sbQuery.append("t501.c704_account_id acctid, ");
    sbQuery.append("" + strAccountNmColumn + " aname," + strRepNmColumn
        + " repname,t501.c501_PARENT_ORDER_ID DO, ");
    sbQuery
        .append("t501.C501_Customer_Po po, get_code_name(t501.c901_Order_Type) ordertypedesc, t501.c501_void_fl vfl, ");
    sbQuery.append("to_char(t501.C501_Shipping_Date,'");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') sdate, t501.C501_hold_fl holdfl ");
    sbQuery
        .append(" ,DECODE(T501.C501_VOID_FL,'Y','Rejected',gm_pkg_op_do_process_report.get_order_status_desc(t501.c501_order_id,'Y')) NAME ");
    sbQuery.append(" FROM t501_Order t501, t703_sales_rep T703, t704_account T704 ");
    // Append table based on Lot number and Surgeon name
    sbQuery.append(" WHERE t501.c501_Delete_fl IS NULL ");
   
    // this condtion is to display voided reports also, when passname is "includevoidedorders"
    if (!strPassName.equals("includevoidedorders")) {
      sbQuery.append("AND t501.c501_void_fl IS NULL ");
      if (strOpt.equals("DOREPORT")) {
        sbQuery
            .append(" AND (t501.c501_parent_order_id IS NULL  OR get_order_attribute_value(t501.c501_order_id,4000536) = 4000537) ");
      }
    }

    if (strOpt.equals("todaysales"))

    {// //this code only for today sales popup screen
      sbQuery.append(" AND TRUNC(t501.c501_order_date) = TRUNC(CURRENT_DATE) ");

    } else if (!strOpt.equals("DOREPORT"))

    {// No need to check below condition for today sales and
     // DO Report
      sbQuery.append("AND (t501.c901_ext_country_id IS NULL ");
      sbQuery
          .append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
      sbQuery.append("AND t501.c501_Shipping_Date IS NULL ");
      sbQuery.append("AND t501.c501_status_Fl < 3 "); // 0 - BO, 1 - pending control, 2 - pending
                                                      // shipment
    }
    // below condition for only DO Report
    if (strOpt.equals("DOREPORT")) {
      sbQuery.append("AND (t501.c901_ext_country_id IS NULL ");
      sbQuery
          .append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
      // 2-Shipped, 3-Shipped, 8-Pending CS Confirmation
      sbQuery.append("AND t501.c501_status_Fl in (" + strOrderStatusRuleValue + ") ");
      sbQuery.append(" AND NVL (t501.c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'DOAPPEXCLUDE') ");
    }

    else

    {
      // this condition will be included other than DO Report
      sbQuery.append(getSalesFilterClause());
    }

    if (!strAccountID.equals(""))// this code only for today sales popup screen
    {
      sbQuery.append(" AND t501.c704_account_id = ");
      sbQuery.append(strAccountID);
    }

    if (!strOrderId.equals(""))

    {
      sbQuery.append(" AND (t501.c501_order_id   IN('");
      sbQuery.append(strOrderId);
      sbQuery.append("') or t501.c501_parent_order_id in ('");
      sbQuery.append(strOrderId);
      sbQuery.append("') AND t501.c501_void_fl IS not NULL)");
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.equals(""))

    {
      sbQuery.append(" AND ( ");
      sbQuery.append(strCondition);

      /*
       * This condition is to get the orders (for DO Report) that the logged in user has placed,this
       * will be handy when a sales rep is covering for another rep,so the DO Report will also show
       * the orders that logged in user has placed as well.
       */
      if (strOpt.equals("DOREPORT") && !strUserID.equals("")) {
        sbQuery.append(" or t501.c501_created_by='").append(strUserID).append("'");
      }
      sbQuery.append(")");

    }
    if (!strFromDate.equals("") && !strToDate.equals("")) {
      sbQuery.append(" AND C501_ORDER_DATE BETWEEN to_date('");
      sbQuery.append(strFromDate);
      sbQuery.append("','");
      sbQuery.append(strApplDateFmt);
      sbQuery.append("') AND to_date('");
      sbQuery.append(strToDate);
      sbQuery.append("','");
      sbQuery.append(strApplDateFmt);
      sbQuery.append("') ");
    }
    // if all values through do report is empty then it will default load current date orders
    if (strOpt.equals("DOREPORT") && strFromDate.equals("") && strToDate.equals("")
        && (strAccountID.equals("") || strAccountID.equals("0")) && strOrderId.equals("")) {
      sbQuery.append(" AND TRUNC(C501_ORDER_DATE) = TRUNC(CURRENT_DATE) ");
    }
    sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
    sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id ");
    sbQuery.append("ORDER BY t501.c501_Order_Date DESC");

    return sbQuery;

  }

  // To fetch Shipped Orders Count

  public StringBuffer fetchShippedOrdersCount(HashMap hmParam) throws AppError

  {
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    log.debug("In fetch shipped order 1");
    sbQuery.append(" SELECT 'Shipped in last 7 days' name, count(1) total FROM (");
    sbQuery.append(" SELECT t501.c501_order_id ORDID ");
    sbQuery.append(" FROM t501_order t501, t907_shipping_info t907 ");
    sbQuery.append(" WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append(" AND t501.c501_delete_fl IS NULL ");
    sbQuery.append(" AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND t501.c501_order_id = t907.c907_ref_id ");
    sbQuery.append(" AND t907.c907_shipped_dt  >= TRUNC (CURRENT_DATE) - 7 ");
    sbQuery.append(" AND t907.c901_source = 50180 and t907.c907_status_fl = '40' ");
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append(")");
    return sbQuery;
  }

  // To fetch Shipped Orders drill down
  public StringBuffer fetchShippedOrdersDrillDown(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    sbQuery.append("SELECT t501.c501_order_id ORDID,");
    sbQuery.append("TO_CHAR(t501.c501_order_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') odt,");
    sbQuery.append("get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') COST,");
    sbQuery
        .append("get_account_name (t501.c704_account_id) aname,get_rep_name(t501.c703_sales_rep_id) repname,t501.c501_PARENT_ORDER_ID DO, ");
    sbQuery.append("get_user_name (t501.c501_created_by) nm,");
    sbQuery.append("t501.c501_customer_po po, t501.c901_Order_Type Ordertype,");
    sbQuery.append("get_code_name(t501.c901_order_type) ordertypedesc, ");
    sbQuery.append("to_char(t907.c907_shipped_dt,'");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') sdate, t501.c501_hold_fl holdfl ");// --c501_status_fl sfl
    sbQuery
        .append(" , t907.c901_delivery_carrier CARRIERID,DECODE (t907.C907_Tracking_Number, Null, 'Y', t907.C907_Tracking_Number) TRACK ");
    sbQuery
        .append(" , get_rule_value(t907.c901_delivery_carrier,'SHIP_CARIER_TRCK_URL')  TRACKURL ");
    sbQuery.append("FROM t501_order t501, t907_shipping_info t907 ");
    sbQuery.append("WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append("AND t501.c501_delete_fl IS NULL ");
    sbQuery.append("AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append("AND t501.c501_order_id = t907.c907_ref_id ");
    sbQuery.append("AND t907.c907_shipped_dt  >= TRUNC (CURRENT_DATE) - 7 ");
    sbQuery.append("AND t907.c901_source = 50180 and t907.c907_status_fl = '40' ");
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append("ORDER BY t501.c501_Order_Date DESC");

    return sbQuery;
  }

  // To fetch held Orders count
  public StringBuffer fetchHeldOrdersCount(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    initializeParameters(hmParam);
    sbQuery.append("SELECT 'Held Orders' name, count(1) total FROM (");
    sbQuery.append("SELECT t501.c501_order_id ORDID ");
    sbQuery.append("FROM t501_order t501 ");
    sbQuery.append("WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append("AND t501.c501_delete_fl IS NULL ");
    sbQuery.append("AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append("AND t501.c501_hold_fl = 'Y' ");
    sbQuery.append("AND NVL(T501.C901_ORDER_TYPE,-9999) NOT IN ('2535') ");
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append(")");


    return sbQuery;
  }

  // To fetch held Orders drill down
  public StringBuffer fetchHeldOrdersDrillDown(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    sbQuery.append("SELECT t501.c501_order_id ORDID, ");
    sbQuery.append("TO_CHAR(t501.c501_order_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') odt,");
    sbQuery.append("get_total_order_amt(t501.c501_order_id,'" + strCurrType + "') COST, ");
    sbQuery
        .append("get_account_name(t501.c704_account_id) aname,get_rep_name(t501.c703_sales_rep_id) repname, t501.c501_PARENT_ORDER_ID DO, ");
    sbQuery.append("get_user_name (t501.c501_created_by) nm, ");
    sbQuery.append("t501.c501_customer_po po, t501.c901_Order_Type Ordertype, ");
    sbQuery.append("get_code_name(t501.c901_order_type) ordertypedesc, ");
    sbQuery.append("to_char(t501.c501_Shipping_Date,'");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') sdate, t501.c501_hold_fl holdfl, PRICEREQ ");
     //use subquery 
    sbQuery.append("FROM t501_order t501,");
    sbQuery.append("( SELECT DECODE(C7500_ACCOUNT_PRICE_REQ_ID,NULL,NULL,'Click here to view Price Request') PRICEREQ, c501_order_id ,COUNT(c501_order_id) ");
    sbQuery.append("FROM T7503_ACCOUNT_PRICE_REQ_ORDER ");
    sbQuery.append("WHERE C7503_VOID_FL IS NULL ");
    sbQuery.append("GROUP BY DECODE(C7500_ACCOUNT_PRICE_REQ_ID,NULL,NULL,'Click here to view Price Request'), c501_order_id ");
    sbQuery.append(")t7503 ");
    sbQuery.append("WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append("AND t501.c501_delete_fl IS NULL ");
    sbQuery.append("AND T501.c501_order_id = t7503.c501_order_id(+) ");
    sbQuery.append("AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append("AND t501.c501_hold_fl = 'Y' ");
    sbQuery.append("AND NVL(T501.C901_ORDER_TYPE,-9999) NOT IN ('2535') ");

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());

    sbQuery.append("Order By t501.c501_Order_Date DESC");
    log.debug("fetchHeldOrdersDrillDown:::::::" + sbQuery.toString());

    return sbQuery;
  }

  // To fetch back order count
  public StringBuffer fetchBackOrdersCount(HashMap hmParam) throws AppError

  {
    initializeParameters(hmParam);
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append("SELECT 'Back Orders' name, count(1) total FROM (");
    sbQuery.append("SELECT t501.c501_order_id ORDID ");
    sbQuery.append("FROM t501_order t501 ");
    sbQuery.append("WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append("AND t501.c501_delete_fl IS NULL ");
    sbQuery.append("AND (t501.c901_ext_country_id IS NULL ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append("AND t501.c901_Order_Type = 2525 ");
    sbQuery.append("AND t501.c501_Status_Fl = 0 ");// --0, BO, 1 pending control, 2 pending shipment
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append(")");

    return sbQuery;
  }


  // To fetch back order drill down
  public StringBuffer fetchBackOrdersDrillDown(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));

    sbQuery.append("SELECT t501.c501_order_id ORDID,");
    sbQuery.append("TO_CHAR (t501.c501_order_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') odt,");
    sbQuery.append("get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') COST,");
    sbQuery
        .append("get_account_name (t501.c704_account_id) aname,get_rep_name(t501.c703_sales_rep_id) repname, t501.c501_PARENT_ORDER_ID DO, ");
    sbQuery.append("get_user_name(t501.c501_created_by) nm, ");
    sbQuery.append("t501.c501_customer_po po, t501.c901_Order_Type Ordertype,");
    sbQuery.append("get_code_name(t501.c901_order_type) ordertypedesc,");
    sbQuery.append("to_char(t501.c501_Shipping_Date,'");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') sdate, t501.c501_hold_fl holdfl ");// --c501_status_fl sfl,
    sbQuery.append("FROM t501_order t501 ");
    sbQuery.append("WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append("AND t501.c501_delete_fl IS NULL ");
    sbQuery.append("AND (t501.c901_ext_country_id IS NULL ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append("AND t501.c901_Order_Type = 2525 ");
    sbQuery.append("AND t501.c501_Status_Fl = 0 ");// --0, BO, 1 pending control, 2 pending shipment
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append("Order By t501.c501_Order_Date DESC");
    return sbQuery;
  }

  // To fetch PO less than 14 days count
  public StringBuffer fetchPOLessthan14daysCount(HashMap hmParam) throws AppError

  {
    initializeParameters(hmParam);
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT 'Pending PO < 14 days' name, count(1) total,NVL(sum(amt),0) amt ");
	sbQuery.append(" FROM (SELECT t501.c501_order_id ORDID,get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') amt ");
    sbQuery.append(" FROM t501_order t501 , t703_sales_rep T703, t704_account T704 ");
    sbQuery.append(" WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append(" AND t501.c501_delete_fl IS NULL ");
    sbQuery.append(" AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND t501.c501_status_fl IS NOT NULL ");
    sbQuery.append(" AND t501.c501_Customer_Po Is Null ");
    sbQuery.append(" AND t501.c503_invoice_id IS NULL ");
    sbQuery.append(" AND NVL (t501.c901_order_type, -9999) <> 2535 ");
    sbQuery.append(" AND TRUNC (t501.c501_order_date) > TRUNC (CURRENT_DATE-14) ");
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
    sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id GROUP BY t501.c501_order_id) ");

    return sbQuery;

  }

  // To fetch PO between 14 to 21 days count
  public StringBuffer fetchPO14to21daysCount(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT 'Pending PO 14 - 21 days' name, count(1) total,NVL(sum(amt),0) amt ");
	sbQuery.append(" FROM (SELECT t501.c501_order_id ORDID,get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') amt ");
    sbQuery.append(" FROM t501_order t501 , t703_sales_rep T703, t704_account T704 ");
    sbQuery.append(" WHERE t501.c501_delete_fl IS NULL ");
    sbQuery.append(" AND t501.c501_void_fl IS NULL ");
    sbQuery.append(" AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND t501.c501_status_fl IS NOT NULL ");
    sbQuery.append(" AND t501.c501_Customer_Po Is Null ");
    sbQuery.append(" AND t501.c503_invoice_id IS NULL ");
    sbQuery.append(" AND NVL (t501.c901_order_type, -9999) <> 2535 ");
    sbQuery.append(" AND TRUNC (t501.c501_order_date) >= TRUNC (CURRENT_DATE-21) ");
    sbQuery.append(" AND TRUNC (t501.c501_order_date) <= TRUNC (CURRENT_DATE-14) ");
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
    sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id GROUP BY t501.c501_order_id) ");

    return sbQuery;
  }

  // To fetch PO Greater than 21 days count
  public StringBuffer fetchPOGreaterthan21daysCount(HashMap hmParam) throws AppError

  {
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);
    sbQuery.append(" SELECT 'Pending PO > 21 days' name, count(1) total ,NVL(sum(amt),0) amt ");
	sbQuery.append(" FROM (SELECT t501.c501_order_id ORDID,get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') amt ");
    sbQuery.append(" FROM t501_order t501 , t703_sales_rep T703, t704_account T704 ");
    sbQuery.append(" WHERE t501.c501_delete_fl IS NULL ");
    sbQuery.append(" AND t501.c501_void_fl IS NULL ");
    sbQuery.append(" AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND t501.c501_status_fl IS NOT NULL ");
    sbQuery.append(" AND t501.c501_Customer_Po Is Null ");
    sbQuery.append(" AND t501.c503_invoice_id IS NULL ");
    sbQuery.append(" AND NVL (t501.c901_order_type, -9999) <> 2535 ");
    sbQuery.append(" AND TRUNC (t501.c501_order_date) < TRUNC (CURRENT_DATE-21) ");
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
    sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id GROUP BY t501.c501_order_id) ");


    return sbQuery;

  }

  // To fetch PO Drill down
  public StringBuffer fetchPOsDrillDown(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    initializeParameters(hmParam);
    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append("SELECT t501.c501_order_id ORDID,");
    sbQuery.append("TO_CHAR(t501.c501_order_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') odt,");
    sbQuery.append("get_total_order_amt (t501.c501_order_id,'" + strCurrType + "') COST,");
    // sbQuery.append("get_account_name (t501.c704_account_id) aname,get_rep_name(t501.c703_sales_rep_id) repname, t501.c501_PARENT_ORDER_ID DO, ");
    sbQuery.append(" " + strAccountNmColumn + " aname, " + strRepNmColumn
        + " repname, t501.c501_PARENT_ORDER_ID DO, ");
    sbQuery.append("get_user_name (t501.c501_created_by) nm, ");
    sbQuery.append("t501.c501_customer_po po,");
    sbQuery.append("t501.c901_Order_Type Ordertype,");
	sbQuery.append("t501.c501_po_amount poamount,");
    sbQuery.append("get_code_name(t501.c901_order_type) ordertypedesc,");
    sbQuery.append("t501.c501_ship_cost shipcost,");
    sbQuery.append("to_char(t501.c501_Shipping_Date,'");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') sdate, t501.c501_hold_fl holdfl ");// --c501_status_fl sfl,
    sbQuery.append(" ,t5003.C901_PO_DTLS postatus, t5003.C901_DO_DTLS dostatus ");
    sbQuery.append(" , t501.c501_receive_mode ordmode "); //PC-4864 Add Online PDF option on Pending PO Screen
    sbQuery.append("FROM t501_order t501 , t703_sales_rep T703, t704_account T704 ");
    sbQuery.append(" ,t5003_order_revenue_sample_dtls t5003 "); // To Fetch Revenue PO, DO Details
    sbQuery.append("WHERE t501.c501_void_fl IS NULL ");
    sbQuery.append("AND c501_delete_fl IS NULL ");
    sbQuery.append("AND (t501.c901_ext_country_id Is Null ");
    sbQuery.append("OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append("AND t501.c501_status_fl IS NOT NULL ");
    sbQuery.append("AND t501.c501_Customer_Po Is Null ");
    sbQuery.append("AND t501.c503_invoice_id IS NULL ");
    sbQuery.append("AND NVL (t501.c901_order_type, -9999) <> 2535 ");

    if (strPassName.equals("PO21day")) {
      sbQuery.append("AND TRUNC (t501.c501_order_date) < TRUNC (CURRENT_DATE-21) ");
    }

    if (strPassName.equals("PO14-21day")) {
      sbQuery.append("AND TRUNC (t501.c501_order_date) >= TRUNC (CURRENT_DATE-21) ");
      sbQuery.append("AND TRUNC (t501.c501_order_date) <= TRUNC (CURRENT_DATE-14) ");
    }

    if (strPassName.equals("PO14day")) {
      sbQuery.append("AND TRUNC (t501.c501_order_date) > TRUNC (CURRENT_DATE-14) ");
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(getSalesFilterClause());
    sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
    sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id ");
    sbQuery.append(" AND T501.c501_order_id = t5003.c501_order_id(+) ");
    sbQuery.append(" AND t5003.C5003_VOID_FL(+) IS NULL ");
    sbQuery.append("ORDER BY t501.c501_Order_Date DESC");
    return sbQuery;
  }
  
  //To fetch Submitted Orders for DO Report
	public StringBuffer fetchSubmittedOrdersForDOReport(HashMap hmParam)
			throws AppError {

		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		String strOrderStatusRuleValue = GmCommonClass.parseNull(GmCommonClass
				.getRuleValue("DOREPORT", "ORDSTATUS"));
		String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));
		String strUserID = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));
		String strOrderId = GmCommonClass.parseNull((String) hmParam
				.get("DOID"));
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		String strFromDate = GmCommonClass.parseNull((String) hmParam
				.get("FROMDT"));
		String strToDate = GmCommonClass
				.parseNull((String) hmParam.get("TODT"));
		String strAccountID = GmCommonClass.parseNull((String) hmParam
				.get("ACCOUNTID"));
		String strCurrType = GmCommonClass.parseNull((String) hmParam
				.get("CURRTYPE"));
		// Used For DO Report based on LOT and Surgeon Name
		String strlotno = GmCommonClass.parseNull((String) hmParam
				.get("LOTNUMBER"));
		String strSurgeon = GmCommonClass.parseNull((String) hmParam
				.get("SURGEONNAME"));

		GmSalesFilterConditionBean gmSalesFilterConditionBean = new GmSalesFilterConditionBean(
				getGmDataStoreVO());

		String strCmpLangId = GmCommonClass.parseNull((String) hmParam
				.get("COMP_LANG_ID"));
		String strCompId = getCompId();
//		String npiRule = GmCommonClass.parseNull(GmCommonClass
//				.getRuleValueByCompany("DO_LOT_SURGEON_RPT", "DO_ORDER",
//						strCompId));
		String strAccountNmColumn = gmSalesFilterConditionBean
				.getAccountNameFilterClause(strCmpLangId, "T704");
		String strRepNmColumn = gmSalesFilterConditionBean
				.getSalesRepNameFilterClause(strCmpLangId, "T703");
		sbQuery.append("SELECT T501_ord.sfl,T501_ord.ORDID,T501_ord.odt,T501_ord.COST,T501_ord.acctid,T501_ord.aname,T501_ord.repname,T501_ord.DO,T501_ord.po,T501_ord.ordertypedesc,T501_ord.vfl,T501_ord.sdate,T501_ord.holdfl,T501_ord.NAME FROM(");
		sbQuery.append("SELECT t501.C501_Status_Fl sfl, t501.C501_Order_Id ORDID, ");
		sbQuery.append("to_char(t501.c501_order_date,'");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("') odt, ");
		sbQuery.append("get_total_do_amt(t501.c501_order_id,'" + strCurrType
				+ "') COST, ");
		sbQuery.append("t501.c704_account_id acctid, ");
		sbQuery.append("" + strAccountNmColumn + " aname," + strRepNmColumn
				+ " repname,t501.c501_PARENT_ORDER_ID DO, ");
		sbQuery.append("t501.C501_Customer_Po po, get_code_name(t501.c901_Order_Type) ordertypedesc, t501.c501_void_fl vfl, ");
		sbQuery.append("to_char(t501.C501_Shipping_Date,'");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("') sdate, t501.C501_hold_fl holdfl ");
		sbQuery.append(" ,DECODE(T501.C501_VOID_FL,'Y','Rejected',gm_pkg_op_do_process_report.get_order_status_desc(t501.c501_order_id,'Y')) NAME ");
		sbQuery.append(" FROM t501_Order t501, t703_sales_rep T703, t704_account T704 ");
		sbQuery.append(" WHERE t501.c501_Delete_fl IS NULL ");

		// this condtion is to display voided reports also, when passname is
		// "includevoidedorders"
		if (!strPassName.equals("includevoidedorders")) {
			sbQuery.append(" AND t501.c501_void_fl IS NULL ");
			sbQuery.append(" AND (t501.c501_parent_order_id IS NULL  OR get_order_attribute_value(t501.c501_order_id,4000536) = 4000537) ");
		}

		if (strOpt.equals("todaysales"))

		{// //this code only for today sales popup screen
			sbQuery.append(" AND TRUNC(t501.c501_order_date) = TRUNC(CURRENT_DATE) ");

		}
		// below condition for only DO Report
		sbQuery.append(" AND (t501.c901_ext_country_id IS NULL ");
		sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
		// 2-Shipped, 3-Shipped, 8-Pending CS Confirmation
		sbQuery.append(" AND t501.c501_status_Fl in ("
				+ strOrderStatusRuleValue + ") ");
		sbQuery.append(" AND NVL (t501.c901_order_type, -9999) NOT IN ( ");
		sbQuery.append(" SELECT t906.c906_rule_value ");
		sbQuery.append(" FROM t906_rules t906 ");
		sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
		sbQuery.append(" AND c906_rule_id = 'DOAPPEXCLUDE') ");

		if (!strAccountID.equals(""))// this code only for today sales popup
										// screen
		{
			sbQuery.append(" AND t501.c704_account_id = ");
			sbQuery.append(strAccountID);
		}

		if (!strOrderId.equals(""))

		{
			sbQuery.append(" AND (t501.c501_order_id   IN('");
			sbQuery.append(strOrderId);
			sbQuery.append("') or t501.c501_parent_order_id in ('");
			sbQuery.append(strOrderId);
			sbQuery.append("') AND t501.c501_void_fl IS not NULL)");
		}
		// Filter Condition to fetch record based on access code
		if (!strCondition.equals(""))

		{
			sbQuery.append(" AND ( ");
			sbQuery.append(strCondition);

			/*
			 * This condition is to get the orders (for DO Report) that the
			 * logged in user has placed,this will be handy when a sales rep is
			 * covering for another rep,so the DO Report will also show the
			 * orders that logged in user has placed as well.
			 */
			if (!strUserID.equals("")) {
				sbQuery.append(" or t501.c501_created_by='").append(strUserID)
						.append("'");
			}
			sbQuery.append(")");

		}
		if (!strFromDate.equals("") && !strToDate.equals("")) {
			sbQuery.append(" AND C501_ORDER_DATE BETWEEN to_date('");
			sbQuery.append(strFromDate);
			sbQuery.append("','");
			sbQuery.append(strApplDateFmt);
			sbQuery.append("') AND to_date('");
			sbQuery.append(strToDate);
			sbQuery.append("','");
			sbQuery.append(strApplDateFmt);
			sbQuery.append("') ");
		}
		// if all values through do report is empty then it will default load
		// current date orders
		if (strFromDate.equals("") && strToDate.equals("")
				&& (strAccountID.equals("") || strAccountID.equals("0"))
				&& strOrderId.equals("")) {
			sbQuery.append(" AND TRUNC(C501_ORDER_DATE) = TRUNC(CURRENT_DATE)");
		}
		sbQuery.append(" AND T501.c704_account_id = T704.c704_account_id ");
		sbQuery.append(" AND T501.c703_sales_rep_id = t703.c703_sales_rep_id");
		sbQuery.append(" ORDER BY t501.c501_Order_Date DESC) T501_ord ,");
		// Surgeon name Search
		sbQuery.append(" (SELECT t6640.c6640_ref_id refid,  t6640.c6600_surgeon_npi_id npiid, t6640.c6640_surgeon_name surgeonname");
		sbQuery.append(" FROM t6640_npi_transaction t6640 WHERE t6640.c6640_void_fl IS NULL  AND t6640.c6640_transaction_fl  = 'Y'  AND t6640.c6640_valid_fl = 'Y' AND t6640.c6600_surgeon_npi_id IS NOT NULL");

		sbQuery.append(" GROUP BY t6640.c6640_ref_id,t6640.c6600_surgeon_npi_id,t6640.c6640_surgeon_name) t6640_npi,");
		// Lot number Search
		sbQuery.append(" (SELECT t502b.c501_order_id npiorderid , t502b.c502b_usage_lot_num lotnum");
		sbQuery.append(" FROM t502b_item_order_usage t502b WHERE t502b.c502b_void_fl IS NULL AND t502b.c502b_usage_lot_num IS NOT NULL   GROUP BY t502b.c501_order_id,t502b.c502b_usage_lot_num) t502b_order");
		sbQuery.append(" WHERE T501_ord.ORDID = t6640_npi.refid(+) AND T501_ord.ORDID = t502b_order.npiorderid(+)");
		// Check strSurgeon is empty or not
		if (!strSurgeon.equals("")) {
			sbQuery.append(" AND UPPER(t6640_npi.surgeonname) LIKE UPPER('%"
					+ strSurgeon + "%') ");
		}
		// Check strlotno is empty or not
		if (!strlotno.equals("")) {
			sbQuery.append(" AND UPPER(t502b_order.lotnum) = UPPER('" + strlotno
					+ "') ");
		}
		sbQuery.append("GROUP BY T501_ord.sfl,T501_ord.ORDID,T501_ord.odt,T501_ord.COST,T501_ord.acctid,T501_ord.aname,T501_ord.repname,T501_ord.DO,T501_ord.po,T501_ord.ordertypedesc,T501_ord.vfl,T501_ord.sdate,T501_ord.holdfl,T501_ord.NAME");
		return sbQuery;

	}


}
