package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesDashCongItemsBeanQuery extends GmSalesFilterConditionBean {

	/**
	 * Count and Drill down calls are separated into two methods.
	 * Removed the unwanted the rows and where condition check while fetching the count.
	 * Database Object Creation - Changed the object creation one per class instead of one per method.
	 * 
	 * @ NEW Java File creation.
	 * 
	 * Methods Added: fetchRequestedCountCongItems, fetchRequestedDrillDownCongItems, fetchShippedCountCongItems, fetchShippedDrillDownCongItems, 
	 * fetchApprovedCountCongItems. fetchApprovedDrillDownCongItems, fetchBackOrderCountCongItems, fetchBackOrderDrillDownCongItems,
	 * fetchAASCountCongItems, fetchAASDrillDownCongItems
	 * @author gpalani PMT-10095 (RFC2252)
	 * @since 2017-01-03
	 */
	// To fetch Requested Consignment Item count
	public StringBuffer fetchRequestedCountCongItems(HashMap hmParam) {

		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT    'Requested'  name, count(A.C526_Status_Fl) total  FROM (");
		sbQuery.append(" SELECT T526.C526_STATUS_FL");
		sbQuery.append(" FROM t526_product_request_detail t526");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append(" ,T525_PRODUCT_REQUEST T525, T7104_CASE_SET_INFORMATION T7104");
		sbQuery.append(" WHERE t526.c901_request_type             = 400088");
		sbQuery.append(" AND T7104.C526_PRODUCT_REQUEST_DETAIL_ID = T526.C526_PRODUCT_REQUEST_DETAIL_ID");
		sbQuery.append(" AND T7104. C7104_VOID_FL                IS NULL");
		sbQuery.append(" AND t526.c525_product_request_id         = t525.c525_product_request_id");
		sbQuery.append(" AND t525.c525_void_fl                   IS NULL");
		sbQuery.append(" AND t526.c526_void_fl                   IS NULL");
		sbQuery.append(" AND T526.C205_PART_NUMBER_ID            IS NOT NULL");
		sbQuery.append(" AND T526.C526_Status_Fl                 IN ('2','5')");
		// Since we don't have sales rep id for consignment items, so there is
		// no salesrep record in t525 table
		// sbQuery.append(" AND V700.REP_ID                      = t525.c703_sales_rep_id");
		sbQuery.append(" AND V700.D_ID                          = t525.c525_request_for_id");
		sbQuery.append("  ) A ");

		return sbQuery;
	}
	// To fetch Requested Consignment Item drill down
	public StringBuffer fetchRequestedDrillDownCongItems(HashMap hmParam) {

		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		sbQuery.append(" SELECT T526.C526_STATUS_FL,");
		sbQuery.append(" T526.C525_PRODUCT_REQUEST_ID REQID , t526.C526_PRODUCT_REQUEST_DETAIL_ID LNREQDTLID, TO_CHAR (T525.C525_SURGERY_DATE, '");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("')SURGDTFMT, DECODE (C526_STATUS_FL ,'2','Pending AD Approval','5', 'Requested', 'Approved') STATUS ,");
		sbQuery.append(" T525.C525_SURGERY_DATE SURGDT,T526.C205_PART_NUMBER_ID pnum,GET_PARTNUM_DESC (T526.C205_PART_NUMBER_ID) pdesc,");
		sbQuery.append(" T7104.C7104_ITEM_QTY reqqty,");
		sbQuery.append(" DECODE(T526.C526_Status_Fl,2, T7104.C7104_ITEM_QTY,5, t526. C526_QTY_REQUESTED)qty ,");
		sbQuery.append(" GET_DISTRIBUTOR_NAME (t525.c525_request_for_id) REQTO ");
		sbQuery.append(" FROM t526_product_request_detail t526");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append(" ,T525_PRODUCT_REQUEST T525, T7104_CASE_SET_INFORMATION T7104");
		sbQuery.append(" WHERE t526.c901_request_type             = 400088");
		sbQuery.append(" AND T7104.C526_PRODUCT_REQUEST_DETAIL_ID = T526.C526_PRODUCT_REQUEST_DETAIL_ID");
		sbQuery.append(" AND T7104. C7104_VOID_FL                IS NULL");
		sbQuery.append(" AND t526.c525_product_request_id         = t525.c525_product_request_id");
		sbQuery.append(" AND t525.c525_void_fl                   IS NULL");
		sbQuery.append(" AND t526.c526_void_fl                   IS NULL");
		sbQuery.append(" AND T526.C205_PART_NUMBER_ID            IS NOT NULL");
		sbQuery.append(" AND T526.C526_Status_Fl                 IN ('2','5')");
		// Since we don't have sales rep id for consignment items, so there is
		// no salesrep record in t525 table
		// sbQuery.append(" AND V700.REP_ID                      = t525.c703_sales_rep_id");
		sbQuery.append(" AND V700.D_ID                          = t525.c525_request_for_id");
		sbQuery.append(" ORDER BY T525.C525_Created_Date");

		return sbQuery;
	}
	// To fetch Shipped Consignment Item count
	public StringBuffer fetchShippedCountCongItems(HashMap hmParam)
			throws AppError {

		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		sbQuery.append(" SELECT 'Shipped in last 7 days' name, count(1) total from (");
		sbQuery.append(" SELECT t520.c520_request_id reqid, ");
		sbQuery.append(" t520.c207_set_id setid ");
		sbQuery.append("FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020, ");
		sbQuery.append("t907_shipping_info t907 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
		sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
		sbQuery.append("And T520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
		// -- AND t520.c520_status_fl = get_code_name_alt ('50644')
		sbQuery.append("AND t520.c520_status_fl in (40) ");
		sbQuery.append("And T520.C520_Master_Request_Id Is Null ");
		sbQuery.append("And T520.C207_Set_Id Is Null ");
		sbQuery.append("And T520.C520_Request_For = 40021 ");
		sbQuery.append("AND t907.c901_source = 50181 ");
		sbQuery.append("And T907.C907_Void_Fl  Is Null ");
		sbQuery.append("And  T907.C907_Ref_Id = T504.C504_Consignment_Id ");
		sbQuery.append("and t907.C907_SHIPPED_DT >= trunc(CURRENT_DATE) -7 ");
		sbQuery.append("AND t504.c701_distributor_id = v700.D_ID ");
		sbQuery.append("AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
		sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());
		sbQuery.append(")");
		return sbQuery;
	}
	// To fetch Shipped Consignment Item drill down
	public StringBuffer fetchShippedDrillDownCongItems(HashMap hmParam)
			throws AppError {

		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		sbQuery.append("SELECT t520.c520_request_id reqid, T504.C504_Consignment_Id CNID, ");
		sbQuery.append("t520.c207_set_id setid, ");
		sbQuery.append("Get_Set_Name (T520.C207_Set_Id) Name, ");
		sbQuery.append("get_consignment_part_attribute (t504.c504_consignment_id, 92340) tag, ");
		sbQuery.append("TO_CHAR (t504.c504_ship_date, '");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("') sdate, ");
		sbQuery.append("t504.c504_consignment_id, Get_Code_Name (T520.C520_Request_For) Reqfor, ");
		sbQuery.append("Gm_Pkg_Op_Request_Summary.Get_Request_Status (T520.C520_Status_Fl) Status, ");
		sbQuery.append("Get_Distributor_Name (T520.C520_Request_To) Reqto, get_rep_name(t504.C703_LOCATION_SALES_REP_ID) repnm, ");
		sbQuery.append("Get_Tag_Id(T504.C504_Consignment_Id) Tagid, t907.C907_SHIPPED_DT ");
		sbQuery.append(" , t907.c901_delivery_carrier CARRIERID,DECODE (t907.C907_Tracking_Number, Null, 'Y', t907.C907_Tracking_Number) TRACK ");
		sbQuery.append("FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020, ");
		sbQuery.append("t907_shipping_info t907 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
		sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
		sbQuery.append("And T520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
		// -- AND t520.c520_status_fl = get_code_name_alt ('50644')
		sbQuery.append("AND t520.c520_status_fl in (40) ");
		sbQuery.append("And T520.C520_Master_Request_Id Is Null ");
		sbQuery.append("And T520.C207_Set_Id Is Null ");
		sbQuery.append("And T520.C520_Request_For = 40021 ");
		sbQuery.append("AND t907.c901_source = 50181 ");
		sbQuery.append("And T907.C907_Void_Fl  Is Null ");
		sbQuery.append("And  T907.C907_Ref_Id = T504.C504_Consignment_Id ");
		sbQuery.append("and t907.C907_SHIPPED_DT >= trunc(sysdate) -7 ");
		sbQuery.append("AND t504.c701_distributor_id = v700.D_ID ");
		return sbQuery;
	}
	// To fetch Approved Consignment Item count
	public StringBuffer fetchApprovedCountCongItems(HashMap hmParam) {

		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT 'Approved' name, count(1) total from (");
		sbQuery.append(" SELECT t520.c520_request_id reqid, t520.c207_set_id setid ");
		sbQuery.append("FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
		sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
		sbQuery.append("AND T520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
		// -- AND t520.c520_status_fl = get_code_name_alt ('50644')
		sbQuery.append("AND t520.c520_status_fl in (0,10,15,20,30) ");
		sbQuery.append("And T520.C520_Master_Request_Id Is Null ");
		sbQuery.append("And T520.C207_Set_Id Is  Null ");
		sbQuery.append("And T520.C520_Request_For = 40021 and t504.c504_status_fl < 4");
		sbQuery.append("AND t504.c701_distributor_id = v700.D_ID ");
		sbQuery.append("AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
		sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());
		sbQuery.append(")");
		return sbQuery;
	}
	// To fetch Approved Consignment Item drill down
	public StringBuffer fetchApprovedDrillDownCongItems(HashMap hmParam) {

		initializeParameters(hmParam);
		StringBuffer sbQuery = new StringBuffer();
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		sbQuery.append(" SELECT t520.c520_request_id reqid,  t520.c207_set_id setid, ");
		sbQuery.append(" Get_Set_Name(T520.C207_Set_Id) Name, ");
		sbQuery.append("get_consignment_part_attribute (t504.c504_consignment_id, 92340) tag, ");
		sbQuery.append("Get_Code_Name (T520.C520_Request_For) Reqfor, 'setnm' setnm, ");
		sbQuery.append("Gm_Pkg_Op_Request_Summary.Get_Request_Status (T520.C520_Status_Fl) Status, ");
		sbQuery.append("Get_Distributor_Name(T520.C520_Request_To) reqto, get_rep_name(t504.C703_LOCATION_SALES_REP_ID) repnm, ");
		sbQuery.append("GET_TAG_ID(T504.C504_Consignment_Id) tagid, ");
		sbQuery.append("TO_CHAR (t504.c504_ship_date, '");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("') sdate ");
		sbQuery.append("FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
		sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
		sbQuery.append("AND T520.C520_Request_Txn_Id = T4020.C4020_Demand_Master_Id(+) ");
		// -- AND t520.c520_status_fl = get_code_name_alt ('50644')
		sbQuery.append("AND t520.c520_status_fl in (0,10,15,20,30) ");
		sbQuery.append("And T520.C520_Master_Request_Id Is Null ");
		sbQuery.append("And T520.C207_Set_Id Is  Null ");
		sbQuery.append("And T520.C520_Request_For = 40021 and t504.c504_status_fl < 4");
		sbQuery.append("AND t504.c701_distributor_id = v700.D_ID ");
		sbQuery.append("AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
		sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());

		return sbQuery;
	}
	// To fetch Back Order Consignment Item count
	public StringBuffer fetchBackOrderCountCongItems(HashMap hmParam) {
		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		sbQuery.append(" SELECT 'Back Order' name, count(1) total from (");
		sbQuery.append(" SELECT t520.c520_request_id reqid, ");
		sbQuery.append(" t520.c207_set_id setid ");
		sbQuery.append(" FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append(" WHERE t520.c520_void_fl         IS NULL ");
		sbQuery.append(" AND t520.c520_request_id         = t504.c520_request_id(+) ");
		sbQuery.append(" AND T520.C520_Request_Txn_Id     = T4020.C4020_Demand_Master_Id(+) ");
		sbQuery.append(" AND t520.c520_status_fl         = 10 ");
		sbQuery.append(" AND t520.c207_Set_Id            IS NULL ");
		sbQuery.append(" AND t520.C520_Request_For        = 40021 ");
		sbQuery.append(" AND t520.c520_request_to         = v700.D_ID ");
		sbQuery.append(" AND T520.C520_REQUEST_TO          IS NOT NULL ");
		sbQuery.append(" AND ( T520.C520_MASTER_REQUEST_ID IS NULL ");
		sbQuery.append(" OR T520.C520_MASTER_REQUEST_ID    IN ");
		sbQuery.append(" ( SELECT C520_REQUEST_ID FROM T520_REQUEST WHERE C207_SET_ID IS NULL ");
		sbQuery.append(" ) ) ");
		sbQuery.append("AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
		sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());
		sbQuery.append(")");

		return sbQuery;
	}
	// To fetch Back Order Consignment Item drill down
	public StringBuffer fetchBackOrderDrillDownCongItems(HashMap hmParam) {
		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		sbQuery.append("SELECT t520.c520_request_id reqid, ");
		sbQuery.append("t520.c207_set_id setid, ");
		sbQuery.append("Get_Set_Name (T520.C207_Set_Id) Name, ");
		sbQuery.append("get_consignment_part_attribute (t504.c504_consignment_id, 92340) tag, ");
		sbQuery.append("Get_Code_Name (T520.C520_Request_For) Reqfor, '' setnm, ");
		sbQuery.append("Gm_Pkg_Op_Request_Summary.Get_Request_Status (T520.C520_Status_Fl) Status, ");
		sbQuery.append("Get_Distributor_Name (T520.C520_Request_To) Reqto, get_rep_name(t504.C703_LOCATION_SALES_REP_ID) repnm, ");
		sbQuery.append("GET_TAG_ID(T504.C504_Consignment_Id) tagid, ");
		sbQuery.append("TO_CHAR (t504.c504_ship_date, '");
		sbQuery.append(strApplDateFmt);
		sbQuery.append("') sdate ");
		sbQuery.append("FROM t520_request t520, t504_consignment t504, t4020_demand_master t4020 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append(" WHERE t520.c520_void_fl         IS NULL ");
		sbQuery.append(" AND t520.c520_request_id         = t504.c520_request_id(+) ");
		sbQuery.append(" AND T520.C520_Request_Txn_Id     = T4020.C4020_Demand_Master_Id(+) ");
		sbQuery.append(" AND t520.c520_status_fl         = 10 ");
		sbQuery.append(" AND t520.c207_Set_Id            IS NULL ");
		sbQuery.append(" AND t520.C520_Request_For        = 40021 ");
		sbQuery.append(" AND t520.c520_request_to         = v700.D_ID ");
		sbQuery.append(" AND T520.C520_REQUEST_TO          IS NOT NULL ");
		sbQuery.append(" AND ( T520.C520_MASTER_REQUEST_ID IS NULL ");
		sbQuery.append(" OR T520.C520_MASTER_REQUEST_ID    IN ");
		sbQuery.append(" ( SELECT C520_REQUEST_ID FROM T520_REQUEST WHERE C207_SET_ID IS NULL ");
		sbQuery.append(" ) ) ");
		sbQuery.append("AND t520.C1900_COMPANY_ID = t504.C1900_COMPANY_ID(+) ");
		sbQuery.append(" AND t520.C1900_COMPANY_ID = " + getCompId());

		return sbQuery;
	}
	// To fetch AAS Consignment Item count
	public StringBuffer fetchAASCountCongItems(HashMap hmParam) {
		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		sbQuery.append("SELECT 'Item Consignment - AAS' name, count(1) total from (");
		sbQuery.append("SELECT vconsigndetail.c207_set_id setid ");
		sbQuery.append("FROM t730_virtual_consignment t730, t731_virtual_consign_item t731 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append("WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id ");
		sbQuery.append("AND t730.c207_set_id IN (SELECT t207a.c207_link_set_id ");
		sbQuery.append("FROM t207a_set_link t207a ");
		sbQuery.append("WHERE t207a.c901_type = 20005) ");
		sbQuery.append("AND t730.c701_distributor_id = v700.d_id ");
		sbQuery.append("GROUP BY t730.c207_set_id, t731.c205_part_number_id, v700.d_name) vconsigndetail, ");
		sbQuery.append("t208_set_details t208, t205_part_number t205 ");
		sbQuery.append("WHERE vconsigndetail.c207_set_id = t208.c207_set_id(+) ");
		sbQuery.append("AND t208.c208_void_fl IS NULL ");
		sbQuery.append("AND vconsigndetail.c205_part_number_id = t208.c205_part_number_id(+) ");
		sbQuery.append("AND vconsigndetail.c205_part_number_id = t205.c205_part_number_id(+) ");
		sbQuery.append("AND t205.c205_product_family NOT IN ('4053', '4052') ");
		sbQuery.append("ORDER BY prodfamily, pid ");
		sbQuery.append(")");
		return sbQuery;
	}
	// To fetch AAS Consignment Item drill down
	public StringBuffer fetchAASDrillDownCongItems(HashMap hmParam) {
		StringBuffer sbQuery = new StringBuffer();
		initializeParameters(hmParam);
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		sbQuery.append("SELECT vconsigndetail.c207_set_id setid, ");
		sbQuery.append("vconsigndetail.c205_part_number_id pid, ");
		sbQuery.append("get_partnum_desc (vconsigndetail.c205_part_number_id) pdesc, ");
		sbQuery.append("NVL (t208.c208_set_qty, 0) sqty, get_code_name (t205.c205_product_family) prodfamily, ");
		sbQuery.append("vconsigndetail.qtyconsigned setusage, listvalue, vconsigndetail.d_name dname ");
		sbQuery.append("FROM (SELECT t730.c207_set_id, t731.c205_part_number_id, SUM (t731.c731_item_qty) qtyconsigned, ");
		sbQuery.append("SUM (t731.c731_item_qty * t731.c731_item_price) listvalue, v700.d_name ");
		sbQuery.append("FROM t730_virtual_consignment t730, t731_virtual_consign_item t731 ");
		sbQuery.append(getAccessFilterClause());
		sbQuery.append("WHERE t730.c730_virtual_consignment_id = t731.c730_virtual_consignment_id ");
		sbQuery.append("AND t730.c207_set_id IN (SELECT t207a.c207_link_set_id ");
		sbQuery.append("FROM t207a_set_link t207a ");
		sbQuery.append("WHERE t207a.c901_type = 20005) ");
		sbQuery.append("AND t730.c701_distributor_id = v700.d_id ");
		sbQuery.append("GROUP BY t730.c207_set_id, t731.c205_part_number_id, v700.d_name) vconsigndetail, ");
		sbQuery.append("t208_set_details t208, t205_part_number t205 ");
		sbQuery.append("WHERE vconsigndetail.c207_set_id = t208.c207_set_id(+) ");
		sbQuery.append("AND t208.c208_void_fl IS NULL ");
		sbQuery.append("AND vconsigndetail.c205_part_number_id = t208.c205_part_number_id(+) ");
		sbQuery.append("AND vconsigndetail.c205_part_number_id = t205.c205_part_number_id(+) ");
		sbQuery.append("AND t205.c205_product_family NOT IN ('4053', '4052') ");
		sbQuery.append("ORDER BY prodfamily, pid ");

		return sbQuery;
	}
	
    /**
     * fetchRequestedCountAccCongItems - This method is used to fetch count of requested account Item Consignment
     * 
     * @param HashMap - hmParam
     * @return String Buffer sbQuery
     * 
     */   
	
    public StringBuffer fetchRequestedCountAccCongItems(HashMap hmParam) {

        initializeParameters(hmParam);
        StringBuffer sbQuery = new StringBuffer();
        sbQuery.append(" SELECT    'Requested'  name, count(A.C526_Status_Fl) total  FROM (");
        sbQuery.append(" SELECT T526.C526_STATUS_FL");
        sbQuery.append(" FROM t526_product_request_detail t526");
        sbQuery.append(getAccessFilterForAcc());
        sbQuery.append(" ,T525_PRODUCT_REQUEST T525, T7104_CASE_SET_INFORMATION T7104");
        sbQuery.append(" WHERE t526.c901_request_type             = 40050");
        sbQuery.append(" AND T7104.C526_PRODUCT_REQUEST_DETAIL_ID = T526.C526_PRODUCT_REQUEST_DETAIL_ID");
        sbQuery.append(" AND T7104. C7104_VOID_FL                IS NULL");
        sbQuery.append(" AND t526.c525_product_request_id         = t525.c525_product_request_id");
        sbQuery.append(" AND t525.c525_void_fl                   IS NULL");
        sbQuery.append(" AND t526.c526_void_fl                   IS NULL");
        sbQuery.append(" AND T526.C205_PART_NUMBER_ID            IS NOT NULL");
        sbQuery.append(" AND T526.C526_Status_Fl                 IN ('2','5')");
        sbQuery.append(" AND V700.AC_ID                          = t525.c525_request_for_id");
        sbQuery.append("  ) A ");

        return sbQuery;
    }
     /**
     * fetchRequestedDrillDownAccCongItems - This method is used to  fetch Requested Account Consignment Item drill down
     * 
     * @param HashMap - hmParam
     * @return String Buffer sbQuery
     * 
     */  
   
    public StringBuffer fetchRequestedDrillDownAccCongItems(HashMap hmParam) {

        initializeParameters(hmParam);
        StringBuffer sbQuery = new StringBuffer();
        String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
                .get("APPLDATEFMT"));
        sbQuery.append(" SELECT T526.C526_STATUS_FL,");
        sbQuery.append(" T526.C525_PRODUCT_REQUEST_ID REQID , t526.C526_PRODUCT_REQUEST_DETAIL_ID LNREQDTLID, TO_CHAR (T525.C525_SURGERY_DATE, '");
        sbQuery.append(strApplDateFmt);
        sbQuery.append("')SURGDTFMT, DECODE (C526_STATUS_FL ,'2','Pending AD Approval','5', 'Requested', 'Approved') STATUS ,");
        sbQuery.append(" T525.C525_SURGERY_DATE SURGDT,T526.C205_PART_NUMBER_ID pnum,GET_PARTNUM_DESC (T526.C205_PART_NUMBER_ID) pdesc,");
        sbQuery.append(" T7104.C7104_ITEM_QTY reqqty,");
        sbQuery.append(" DECODE(T526.C526_Status_Fl,2, T7104.C7104_ITEM_QTY,5, t526. C526_QTY_REQUESTED)qty , T525.C525_REQUEST_FOR_ID ACC_ID,T525.C525_REQUEST_BY_ID REPID,");
        sbQuery.append(" GET_ACCOUNT_NAME(T525.C525_REQUEST_FOR_ID) ACCTNAME,GET_REP_NAME(T525.C525_REQUEST_BY_ID) repnm ");
        sbQuery.append(" FROM t526_product_request_detail t526");
        sbQuery.append(getAccessFilterForAcc());
        sbQuery.append(" ,T525_PRODUCT_REQUEST T525, T7104_CASE_SET_INFORMATION T7104");
        sbQuery.append(" WHERE t526.c901_request_type             = 40050");
        sbQuery.append(" AND T7104.C526_PRODUCT_REQUEST_DETAIL_ID = T526.C526_PRODUCT_REQUEST_DETAIL_ID");
        sbQuery.append(" AND T7104. C7104_VOID_FL                IS NULL");
        sbQuery.append(" AND t526.c525_product_request_id         = t525.c525_product_request_id");
        sbQuery.append(" AND t525.c525_void_fl                   IS NULL");
        sbQuery.append(" AND t526.c526_void_fl                   IS NULL");
        sbQuery.append(" AND T526.C205_PART_NUMBER_ID            IS NOT NULL");
        sbQuery.append(" AND T526.C526_Status_Fl                 IN ('2','5')");
        sbQuery.append(" AND V700.AC_ID                          = t525.c525_request_for_id");
        sbQuery.append(" ORDER BY T525.C525_Created_Date desc");

        return sbQuery;
    }
    
     /**
     * fetchShippedCountAccCongItems - This method is used to fetch Shipped Account Consignment Item count
     * 
     * @param HashMap - hmParam
     * @return String Buffer sbQuery
     * 
     */  

    public StringBuffer fetchShippedCountAccCongItems(HashMap hmParam)
            throws AppError {

        StringBuffer sbQuery = new StringBuffer();
        initializeParameters(hmParam);
        sbQuery.append(" SELECT 'Shipped in last 7 days' name, count(1) total from (");
        sbQuery.append(" SELECT t520.c520_request_id reqid, ");
        sbQuery.append(" t520.c207_set_id setid ");
        sbQuery.append("FROM t520_request t520, t504_consignment t504,  ");
        sbQuery.append("t907_shipping_info t907 ");
        sbQuery.append(getAccessFilterForAcc());
        sbQuery.append("WHERE t520.c520_void_fl IS NULL ");
        sbQuery.append("AND t520.c520_request_id = t504.c520_request_id(+) ");
        sbQuery.append("AND t520.c520_status_fl in (40) ");
        sbQuery.append("And T520.C520_Master_Request_Id Is Null ");
        sbQuery.append("And T520.C207_Set_Id Is Null ");
        sbQuery.append("And T520.C520_Request_For = 40025 ");
        sbQuery.append("AND t907.c901_source = 50181 ");
        sbQuery.append("And T907.C907_Void_Fl  Is Null ");
        sbQuery.append("And  T907.C907_Ref_Id = T504.C504_Consignment_Id ");
        sbQuery.append("and t907.C907_SHIPPED_DT >= trunc(CURRENT_DATE) -7 ");
        sbQuery.append("AND t504.c704_account_id = v700.AC_ID ");
        sbQuery.append(")");
        return sbQuery;
    }
    /**
    * fetchCountAccCongItems - This method is used to fetch  Account Consignment Item count currently for both approved and BO
    * the Output will be like its status name and its count e.g Approved 15 and Back Order 20 in arraylist
    * @param HashMap - hmParam
    * @return String Buffer sbQuery
    * 
    */  
  
    public StringBuffer fetchCountAccCongItems(HashMap hmParam) {
      StringBuffer sbQuery = new StringBuffer();
      initializeParameters(hmParam);
      sbQuery.append(" SELECT statusname name, count(1) total from (");
      sbQuery.append(" SELECT T520.C520_REQUEST_ID REQID,T520.C520_STATUS_FL STATUSFL, ");
      sbQuery.append("  CASE WHEN T520.C520_STATUS_FL  IN (0,10,15,20,30) AND  T504.C504_STATUS_FL < 4 "); // to fetch the count of Approved Request
      sbQuery.append("  THEN 'Approved' ");
      sbQuery.append("  WHEN T520.C520_STATUS_FL = 10 ");//--10--Back order to fetch the count of Back order Request
      sbQuery.append("  THEN 'Back Order'  ");
      sbQuery.append("   END statusname  ");
      sbQuery.append(" FROM t520_request t520, t504_consignment t504 ");
      sbQuery.append(getAccessFilterForAcc());
      sbQuery.append(" WHERE t520.c520_void_fl         IS NULL ");
      sbQuery.append(" AND t520.c520_request_id         = t504.c520_request_id(+) ");
      sbQuery.append(" AND T520.C520_STATUS_FL         IN (0,10,15,20,30) "); //fetching all the request 
                                                                              //based on the case it will display the status and its count
      sbQuery.append(" AND t520.c207_Set_Id            IS NULL ");
      sbQuery.append(" AND t520.C520_Request_For        = 40025 ");
      sbQuery.append(" AND t520.c520_request_to         = v700.AC_ID ");
      sbQuery.append(" AND T520.C520_REQUEST_TO          IS NOT NULL ");
      sbQuery.append(" AND ( T520.C520_MASTER_REQUEST_ID IS NULL ");
      sbQuery.append(" OR T520.C520_MASTER_REQUEST_ID    IN ");
      sbQuery.append(" ( SELECT C520_REQUEST_ID FROM T520_REQUEST WHERE C207_SET_ID IS NULL ");
      sbQuery.append("  AND C520_Request_For  = 40025 AND c520_void_fl  IS NULL AND c520_request_to = v700.AC_ID) ) ");
      sbQuery.append(" ) GROUP BY statusname");

      return sbQuery;
  }
    /**
    * fetchAccountCongDrillDownItems - This method is used to fetch Account Consignment Item 
    * based on the Pass Name (Shipped, BO)
    * @param HashMap - hmParam
    * @return String Buffer sbQuery
    * 
    */    
    public StringBuffer fetchAccountCongDrillDownItems(HashMap hmParam)
        throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    initializeParameters(hmParam);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
            .get("APPLDATEFMT"));
    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
    sbQuery.append(" SELECT t520.c520_request_id reqid,  ");
    sbQuery.append(" T504.C504_Consignment_Id CNID, ");
    sbQuery.append(" t520.c207_set_id setid, ");
    sbQuery.append(" '' Name, ");
    sbQuery.append(" '' tag, ");
    sbQuery.append(" TO_CHAR (t504.c504_ship_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') sdate, ");
    sbQuery.append(" t504.c504_consignment_id, Get_Code_Name (T520.C520_Request_For) Reqfor, ");
    sbQuery.append(" Gm_Pkg_Op_Request_Summary.Get_Request_Status (T520.C520_Status_Fl) Status, T520.C520_Request_To ACC_ID,T520.C520_REQUEST_BY REPID, ");
    sbQuery.append(" GET_ACCOUNT_NAME(T520.C520_Request_To) ACCTNAME,  GET_REP_NAME(T520.C520_REQUEST_BY) repnm, ");
    sbQuery.append(" '' Tagid ");
    if(strPassName.equals("Shipped")){
      sbQuery.append(" , t907.c901_delivery_carrier CARRIERID,DECODE (t907.C907_Tracking_Number, Null, 'Y', t907.C907_Tracking_Number) TRACK ");
    }
  
    sbQuery.append(" FROM t520_request t520, t504_consignment t504 ");
    if(strPassName.equals("Shipped")){
    sbQuery.append(" ,t907_shipping_info t907 ");
    }
    sbQuery.append(getAccessFilterForAcc());
    sbQuery.append(" WHERE t520.c520_void_fl IS NULL ");
    sbQuery.append(" AND t520.c520_request_id = t504.c520_request_id(+) ");
    sbQuery.append(" And T520.C207_Set_Id Is Null ");
    sbQuery.append(" And T520.C520_Request_For = 40025 ");
    if(strPassName.equals("Shipped") || strPassName.equals("BO")){
    sbQuery.append(" AND t520.c520_status_fl = DECODE('"+strPassName+"','Shipped','40','BO','10') ");
    }else if (strPassName.equals("Approved")){
      sbQuery.append(" AND t520.c520_status_fl   IN (0,10,15,20,30) ");
      sbQuery.append(" AND T504.C504_STATUS_FL < 4 ");
      sbQuery.append(" And T520.C520_Master_Request_Id Is Null ");
    }
    if(strPassName.equals("BO")){
      sbQuery.append(" AND T520.C520_REQUEST_TO          IS NOT NULL");
      sbQuery.append(" AND ( T520.C520_MASTER_REQUEST_ID IS NULL");
      sbQuery.append(" OR T520.C520_MASTER_REQUEST_ID    IN");
      sbQuery.append("  ( SELECT C520_REQUEST_ID FROM T520_REQUEST WHERE C207_SET_ID IS NULL");
      sbQuery.append("  AND C520_Request_For  = 40025 AND c520_void_fl  IS NULL AND c520_request_to = v700.AC_ID ) ) ");
    }
    if(strPassName.equals("Shipped")){
    sbQuery.append(" AND t907.c901_source = 50181 ");
    sbQuery.append(" And T907.C907_Void_Fl  Is Null ");
    sbQuery.append(" And T520.C520_Master_Request_Id Is Null ");
    sbQuery.append(" And  T907.C907_Ref_Id = T504.C504_Consignment_Id ");
    sbQuery.append(" and t907.C907_SHIPPED_DT >= trunc(CURRENT_DATE) -7 ");
    }
    if (strPassName.equals("BO")){
      sbQuery.append(" AND t520.c520_request_to = v700.AC_ID ");
    }else{
      sbQuery.append(" AND t504.c704_account_id = v700.AC_ID ");
    }
    if(strPassName.equals("Shipped")){
    sbQuery.append(" ORDER BY t907.C907_SHIPPED_DT desc");
    }else{
      sbQuery.append(" ORDER BY t520.c520_created_date desc ");
    }
   
    return sbQuery;
}

}
