package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author gpalani
 *
 */
public class GmSalesDashCongSetsBean extends GmSalesFilterConditionBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	GmSalesDashCongSetsBeanQuery gmCongSetQuery = new GmSalesDashCongSetsBeanQuery();

	public GmSalesDashCongSetsBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmSalesDashCongSetsBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * Count and Drill down calls are separated into two methods.
	 * Removed the unwanted the rows and where condition check while fetching the count.
	 * Database Object Creation - Changed the object creation one per class instead of one per method.
	 * Methods Modified: fetchCongSets, fetchShippedCongSets, fetchAllCurrCongSets, fetchAllMissLocCongSets, fetchApprovedCongSets
	 * @author gpalani PMT-10095 (RFC2252)
	 * @since 2017-01-03
	 */
	

	
	public ArrayList fetchCongSets(HashMap hmParam) throws AppError {

	//	initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strTotals = GmCommonClass.parseNull((String) hmParam
				.get("TOTALS"));
		String strCongSetType = GmCommonClass.parseNull((String) hmParam
				.get("CONGSETTYPE"));

		// Show Totals
		if (strTotals.equals("Y")) {
			if (strCongSetType.equals("Approved")) {
				sbQuery = gmCongSetQuery.fetchApprovedCountCongSets(hmParam);
			}
			if (strCongSetType.equals("BackOrder")) {
				sbQuery = gmCongSetQuery.fetchBackOrderCountCongSets(hmParam);
			}
		}
		// Show DrillDown
		else if (!strTotals.equals("Y")) {
			if (strCongSetType.equals("Approved")) {
				sbQuery = gmCongSetQuery
						.fetchApprovedDrillDownCongSets(hmParam);

			}
			if (strCongSetType.equals("BackOrder")) {
				sbQuery = gmCongSetQuery
						.fetchBackOrderDrillDownCongSets(hmParam);

			}
		}

		log.debug("sbQuery:" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

		return alReturn;
	}

	
	public ArrayList fetchShippedCongSets(HashMap hmParam) throws AppError {
	//	initializeParameters(hmParam);
		ArrayList alReturn = null;
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String strTotals = GmCommonClass.parseNull((String) hmParam
				.get("TOTALS"));

		if (strTotals.equals("Y")) {
			sbQuery = gmCongSetQuery.fetchShippedCountCongSets(hmParam);

		}

		else if (!strTotals.equals("Y"))

		{
			sbQuery = gmCongSetQuery.fetchShippedDrillDownCongSets(hmParam);

		}

		log.debug("sbQuery======================>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	
	public ArrayList fetchAllCurrCongSets(HashMap hmParam) throws AppError {
	//	initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strTotals = GmCommonClass.parseNull((String) hmParam
				.get("TOTALS"));
		// Show Totals
		if (strTotals.equals("Y")) {
			sbQuery = gmCongSetQuery.fetchAllSetsCountCongSets(hmParam);

		}

		else if (!strTotals.equals("Y")) {

			sbQuery = gmCongSetQuery.fetchAllSetsDrillDownCongSets(hmParam);

		}

		log.debug("sbQuery======================>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	
	public ArrayList fetchAllMissLocCongSets(HashMap hmParam) throws AppError {
		//initializeParameters(hmParam);
		ArrayList alReturn = null;
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String strTotals = GmCommonClass.parseNull((String) hmParam
				.get("TOTALS"));
		// Show Totals
		if (strTotals.equals("Y")) {
			sbQuery = gmCongSetQuery
					.fetchSetsMissingLocationCountCongSets(hmParam);
		}

		else if (!strTotals.equals("Y")) {
			sbQuery = gmCongSetQuery
					.fetchSetsMissingLocationDrillDownCongSets(hmParam);

		}

		log.debug("sbQuery======================>" + sbQuery);

		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

	
	public ArrayList fetchApprovedCongSets(HashMap hmParam) throws AppError {

		//initializeParameters(hmParam);
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));
		// String strApplDateFmt =
		// GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT",
		// "DATEFORMAT"));
		if (!strPassName.equals("Requested")) {
			sbQuery = gmCongSetQuery.fetchRequestedCountCongSets(hmParam);
		}

		else if (strPassName.equals("Requested")) {
			sbQuery = gmCongSetQuery.fetchRequestedDrillDownCongSets(hmParam);

		}

		log.debug("sbQuery===>Requested===>" + sbQuery);
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

		return alReturn;
	}
}
