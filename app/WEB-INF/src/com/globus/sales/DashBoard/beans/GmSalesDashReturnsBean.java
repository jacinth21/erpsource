package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesDashReturnsBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmDBManager gmDBManager = new GmDBManager();


  public GmSalesDashReturnsBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmSalesDashReturnsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Count and Drill down calls are separated into two methods. Removed the unwanted the rows and
   * where condition check while fetching the count. Database Object Creation - Changed the object
   * creation one per class instead of one per method. Methods Modified: fetchReturnCount,
   * 
   * @author gpalani PMT-10095 (RFC2252)
   * @since 2017-01-03
   */

  public ArrayList fetchReturnCount(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    ArrayList alReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));

    // Show Totals
    if (strPassName.equals("Initiated")) {
      sbQuery.append(" SELECT 'RA - Initiated in Last 60 days' name, count(1) total FROM ( ");
      sbQuery.append(" SELECT t506.c506_rma_id raid ");
      sbQuery.append(" FROM t506_Returns t506 ");
      sbQuery.append(getAccessFilterClause());
      sbQuery.append(" WHERE t506.c506_Void_Fl IS NULL ");
      sbQuery.append(" AND t506.c506_Status_Fl IN (0,1) ");
      sbQuery.append(" AND t506.c501_Reprocess_Date IS NULL ");
      sbQuery.append(" AND TRUNC(t506.c506_created_date) >= TRUNC (SYSDATE - 60) ");
      sbQuery.append(" And V700.D_ID  = t506.c701_distributor_id ) ");


    }

    else if (strPassName.equals("Closed")) {
      sbQuery.append(" SELECT 'RA - Closed in Last 60 days' name, count(1) total FROM (");
      sbQuery.append(" SELECT t506.c506_rma_id raid ");
      sbQuery.append(" FROM t506_Returns t506 ");
      sbQuery.append(getAccessFilterClause());
      sbQuery.append(" WHERE t506.c506_Void_Fl IS NULL ");
      sbQuery.append(" AND t506.c506_Status_Fl = 2 ");
      sbQuery.append(" AND V700.D_ID  = t506.c701_distributor_id ");
      sbQuery.append(" AND TRUNC(t506.c506_credit_date) >= TRUNC (SYSDATE - 60) ) ");

    }


    log.debug("sbQuery==>" + sbQuery);
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  public ArrayList fetchAllInitReturns(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));

    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");


    sbQuery.append("SELECT t506.c506_rma_id raid, TO_CHAR (t506.c506_created_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') cdate, get_code_name (t506.c506_type) TYPE, ");
    sbQuery.append("TO_CHAR (t506.c506_expected_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') edate, ");
    sbQuery
        .append("DECODE (t506.c701_distributor_id, NULL, get_account_name (t506.c704_account_id), ");
    sbQuery.append("" + strDistNmColumn + ") dname, ");
    sbQuery.append("Get_Rep_Name (t506.c703_Sales_Rep_Id) Rname, ");
    // -- , DECODE (c506_status_fl, 0, '-', 1, 'Y') status_fl
    sbQuery.append("'Open' status, TO_CHAR (t506.c506_return_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') rdate, t506.c501_order_id parentoid, t506.c506_parent_rma_id parentrmaid ");
    sbQuery.append("FROM t506_Returns t506 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append("WHERE t506.c506_Void_Fl IS NULL ");
    sbQuery.append("AND t506.c506_Status_Fl IN (0,1) ");
    sbQuery.append("AND t506.c501_Reprocess_Date IS NULL ");
    sbQuery.append("AND TRUNC(t506.c506_created_date) >= TRUNC (SYSDATE - 60) ");
    sbQuery.append(" And V700.D_ID  = t506.c701_distributor_id ");
    sbQuery.append("ORDER BY t506.c506_Created_Date DESC");

    log.debug("sbQuery==>" + sbQuery);
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  public ArrayList fetchAllClosedReturns(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));

    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");

    sbQuery.append("SELECT t506.c506_rma_id raid, TO_CHAR (t506.c506_created_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') cdate, get_code_name (t506.c506_type) TYPE, ");
    sbQuery.append("TO_CHAR (t506.c506_credit_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') crdate, DECODE (t506.c701_distributor_id, NULL, ");
    sbQuery.append("get_account_name (t506.c704_account_id), ");
    sbQuery.append("" + strDistNmColumn + ") dname, ");
    sbQuery.append("Get_Rep_Name (t506.c703_Sales_Rep_Id) Rname, ");
    // -- , DECODE (c506_status_fl, 0, '-', 1, 'Y') status_fl
    sbQuery.append("'Closed' status, TO_CHAR (t506.c506_return_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') rdate, t506.c501_order_id parentoid, t506.c506_parent_rma_id parentrmaid ");
    sbQuery.append("FROM t506_Returns t506 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append("WHERE t506.c506_Void_Fl IS NULL ");
    sbQuery.append("AND t506.c506_Status_Fl = 2 ");
    sbQuery.append("AND V700.D_ID  = t506.c701_distributor_id ");
    sbQuery.append("AND TRUNC(t506.c506_credit_date) >= TRUNC (SYSDATE - 60) ");
    sbQuery.append("ORDER BY t506.c506_Created_Date DESC");

    log.debug("sbQuery==>" + sbQuery);
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

     /**
     * fetchAccReturnCount - This method is used to fetch Return Account Consignment Item count
     * 
     * @param HashMap - hmParam
     * @return ArrayList alReturn
     * 
     */ 
  public ArrayList fetchAccReturnCount(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    ArrayList alReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    String strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));


      sbQuery.append(" SELECT statusname name, count(1) total FROM ( ");
      sbQuery.append(" SELECT t506.c506_rma_id raid,  ");
      sbQuery.append(" CASE WHEN T506.C506_STATUS_FL = 2  ");
      sbQuery.append("  THEN 'RA - Closed in Last 60 days'  ");
      sbQuery.append("  WHEN T506.C506_STATUS_FL  IN (0,1) AND t506.c501_Reprocess_Date IS NULL  ");
      sbQuery.append(" THEN 'RA - Initiated in Last 60 days'  ");
      sbQuery.append("  END statusname  ");
      sbQuery.append(" FROM t506_Returns t506 ");
      sbQuery.append(getAccessFilterForAcc());
      sbQuery.append(" WHERE t506.c506_Void_Fl IS NULL ");
      sbQuery.append(" AND T506.C506_Type = 3304");
      sbQuery.append(" AND TRUNC( DECODE(T506.C506_STATUS_FL,'2',T506.C506_CREDIT_DATE,T506.C506_CREATED_DATE)) >= TRUNC (CURRENT_DATE - 60) ");
      sbQuery.append(" And V700.AC_ID  = t506.c704_account_id  ");
      sbQuery.append(" ) Group by statusname ");
    log.debug("sbQuery==>" + sbQuery);
    alReturn = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    return alReturn;
  }

     /**
     * fetchAccCongItemReturns - This method is used to fetch all Initiated and Closed Returns 
     * of Account Consignment for Item drill down
     * @param HashMap - hmParam
     * @return ArrayList alReturn
     * 
     */ 
  public ArrayList fetchAccCongItemReturns(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strRetStatus = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));
    String strInitStatus = "0,1";


    sbQuery.append(" SELECT t506.c506_rma_id raid, TO_CHAR (t506.c506_created_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') cdate, get_code_name (t506.c506_type) TYPE, ");
    if(strRetStatus.equals("Initiated")){
      sbQuery.append(" TO_CHAR (t506.c506_expected_date, '");
      sbQuery.append(strApplDateFmt);
      sbQuery.append("') edate, ");
    }else if(strRetStatus.equals("Closed")){
      sbQuery.append(" TO_CHAR (t506.c506_credit_date, '");
      sbQuery.append(strApplDateFmt);
      sbQuery.append("') crdate,  ");
    }
    sbQuery.append(" get_account_name (t506.c704_account_id) ACCTNAME, ");
    sbQuery.append(" Get_Rep_Name (t506.c703_Sales_Rep_Id) Rname, ");
    sbQuery.append(" DECODE('"+strRetStatus+"','Initiated','Open','Closed','Closed') status, TO_CHAR (t506.c506_return_date, '");
    sbQuery.append(strApplDateFmt);
    sbQuery.append("') rdate,  t506.c506_parent_rma_id parentrmaid ");
    sbQuery.append(" FROM t506_Returns t506 ");
    sbQuery.append(getAccessFilterForAcc());
    sbQuery.append(" WHERE t506.c506_Void_Fl IS NULL ");
    if(strRetStatus.equals("Initiated")){
    sbQuery.append(" AND t506.c506_Status_Fl IN (0,1)");
      sbQuery.append(" AND t506.c501_Reprocess_Date IS NULL ");
    }else if(strRetStatus.equals("Closed")) {
      sbQuery.append(" AND t506.c506_Status_Fl = 2 ");
    }
    sbQuery.append(" AND T506.C506_Type = 3304");
    sbQuery.append(" AND V700.AC_ID  = t506.c704_account_id ");
    sbQuery.append(" AND TRUNC(DECODE('"+strRetStatus+"','Initiated',t506.c506_created_date,'Closed',t506.c506_credit_date)) >= TRUNC (CURRENT_DATE - 60) ");
    sbQuery.append(" ORDER BY t506.c506_Created_Date DESC");

    log.debug("sbQuery==>" + sbQuery);
    alReturn = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    return alReturn;
  }
}
