package com.globus.sales.DashBoard.beans;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesDashBoardBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  // this will be removed all place changed with Data Store VO constructor

  public GmSalesDashBoardBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesDashBoardBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public String fetchTypeQuota(HttpServletRequest req, HttpServletResponse res) {
    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strQuery = "";
    HashMap hmParam = new HashMap();

    HttpSession session = req.getSession(false);

    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    int intOverrideAccLevel =
        Integer.parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideAccLvl")));
    int intOverrideASSAccLevel =
        Integer
            .parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideASSAccLvl")));
    String strRptRepId = GmCommonClass.parseNull((String) session.getAttribute("strSessRptRepId"));


    hmParam.put("USER_ID", strEmpId);
    hmParam.put("ACCES_LVL", intAccLevel + "");
    hmParam.put("DEPT_ID", strDepartMentID);
    hmParam.put("OVERRIDE_ACCS_LVL", intOverrideAccLevel + "");
    hmParam.put("OVERRIDE_ASS_LVL", intOverrideASSAccLevel + "");
    hmParam.put("RPT_REPID", strRptRepId);

    return this.fetchTypeQuota(hmParam);
  }

  public String fetchTypeQuota(HashMap hmParam) {


    String strQuery = "";
    GmCommonClass gmcc = new GmCommonClass();

    String strEmpId = gmcc.parseNull((String) hmParam.get("USER_ID"));
    int intAccLevel = Integer.parseInt(gmcc.parseZero((String) hmParam.get("ACCES_LVL")));
    String strDepartMentID = gmcc.parseNull((String) hmParam.get("DEPT_ID"));
    int intOverrideAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ACCS_LVL")));
    int intOverrideASSAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ASS_LVL")));
    String strRptRepId = gmcc.parseNull((String) hmParam.get("RPT_REPID"));

    if (intOverrideAccLevel == 1) {
      intAccLevel = 2;
    }
    
    // PC-318: To remove the associate sales rep override access level 
    // To show the correct sales amount (Daily Sales/ Dashboard/ Quota reports)
    // So, Remove the below override access level if condition.
    
    /*if (intOverrideASSAccLevel == 7) {
      strEmpId = strRptRepId;
      intAccLevel = 1;
    }*/

    // *********************************************************
    // Will Execute the below Query if the Department is Sales
    // *********************************************************
    if (strDepartMentID.equals("S")) {
      switch (intAccLevel) {

      // Sales Rep Filter Condition
        case 1: {
          strQuery = "TERR";
          break;
        }

        // Distributor Filter Condition
        case 2: {

          strQuery = "DIST";
          break;
        }

        // AD Filter Condition
        case 3: {
          strQuery = "AD";
          break;
        }

        // ASS Filter condition
        case 7: {
          strQuery = "TERR";
          break;
        }

        default: {
          strQuery = "VP";
        }

      }

    } else {
      strQuery = "VP";
    }
    log.debug("strQuery==>" + strQuery);
    return strQuery;
  }

  public String fetchUserType(HttpServletRequest req, HttpServletResponse res) {
    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strQuery = "";
    HashMap hmParam = new HashMap();

    HttpSession session = req.getSession(false);
    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    int intOverrideAccLevel =
        Integer.parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideAccLvl")));
    int intOverrideASSAccLevel =
        Integer
            .parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideASSAccLvl")));
    String strRptRepId = GmCommonClass.parseNull((String) session.getAttribute("strSessRptRepId"));

    hmParam.put("USER_ID", strEmpId);
    hmParam.put("ACCES_LVL", intAccLevel + "");
    hmParam.put("DEPT_ID", strDepartMentID);
    hmParam.put("OVERRIDE_ACCS_LVL", intOverrideAccLevel + "");
    hmParam.put("OVERRIDE_ASS_LVL", intOverrideASSAccLevel + "");
    hmParam.put("RPT_REPID", strRptRepId);
    return this.fetchUserType(hmParam);


  }

  public String fetchUserType(HashMap hmParam) {
    String strQuery = "";
    GmCommonClass gmcc = new GmCommonClass();

    String strEmpId = gmcc.parseNull((String) hmParam.get("USER_ID"));
    int intAccLevel = Integer.parseInt(gmcc.parseZero((String) hmParam.get("ACCES_LVL")));
    String strDepartMentID = gmcc.parseNull((String) hmParam.get("DEPT_ID"));
    int intOverrideAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ACCS_LVL")));
    int intOverrideASSAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ASS_LVL")));
    String strRptRepId = gmcc.parseNull((String) hmParam.get("RPT_REPID"));

    if (intOverrideAccLevel == 1) {
      intAccLevel = 2;
    }
    
    // PC-318: To remove the associate sales rep override access level 
    // To show the correct sales amount (Daily Sales/ Dashboard/ Quota reports)
    // So, Remove the below if condition.
    
   /*if (intOverrideASSAccLevel == 7) {
      strEmpId = strRptRepId;
      intAccLevel = 1;
    }*/

    // *********************************************************
    // Will Execute the below Query if the Department is Sales
    // *********************************************************
    if (strDepartMentID.equals("S")) {
      switch (intAccLevel) {

      // Sales Rep Filter Condition
        case 1: {
          strQuery = "REP";
          break;
        }

        // Distributor Filter Condition
        case 2: {

          strQuery = "DIST";
          break;
        }

        // AD Filter Condition
        case 3: {
          strQuery = "AD";
          break;
        }

        // VP Filter Condition
        case 5: {
          strQuery = "MGT";
          break;
        }

        // ASS Filter condition
        case 7: {
          strQuery = "ASS";
          break;
        }

        default: {
          strQuery = "VP";
        }
      }

    } else {
      strQuery = "MGT";
    }
    log.debug("strQuery==>" + strQuery);
    return strQuery;
  }

  /**
   * todaysSalesReport - This method is used in sales dashboard today sales dataset tab and chart
   * tab. strOpt - detail for popup in sales dashboard today sales
   * 
   * @param String strDate
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList todaysSalesReport(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    initializeParameters(hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
    String strDate = GmCommonClass.parseNull((String) hmParam.get("TDATE"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
    String strFormat = GmCommonClass.parseNull((String) hmParam.get("FORMAT"));
    String strUserType = GmCommonClass.parseNull((String) hmParam.get("USERTYPE"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strDetailID = GmCommonClass.parseNull((String) hmParam.get("DETAILID"));

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "V700");
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");


    StringBuffer sbQuery = new StringBuffer();
    if (strOpt.equals("detail")) {// today sales detail popup
      sbQuery.append(" SELECT V700.*,  SUM (V700.SALES) OVER () ORDER_TOTAL   FROM   ( ");
      sbQuery.append(" SELECT V700.Vp_Name, V700.Ad_Name, V700.D_Name, v700.REP_NAME, v700.AC_NAME, v700.AC_ID ");
    } else {
      sbQuery.append(getFrameColumn(strUserType));
    }
    sbQuery.append(" ," + strItemPriceColumn + " SALES ");
    sbQuery.append(" FROM  T501_ORDER T501, T502_ITEM_ORDER T502,  T704_ACCOUNT T704, ");
    sbQuery.append(" (SELECT COMPID,DIVID,GP_ID,REGION_ID,VP_ID,VP_NAME,AD_ID,AD_NAME,D_ID,"
        + strDistNmColumn + " D_NAME,D_COMPID,REP_COMPID, REP_ID, " + strRepNmColumn
        + " REP_NAME,AC_ID, " + strAccountNmColumn + " AC_NAME ");
    sbQuery.append(" FROM v700_territory_mapping_detail  V700 ");
    sbQuery.append(" WHERE AC_ID IS NOT NULL ");
    if (strAccessFilter != null && !strAccessFilter.equals("")) {
      sbQuery.append(strAccessFilter);
    }
    if (strOpt.equals("detail")) {// In today sales - detail sales for specific VP/AD etc.
      sbQuery.append(" AND ");
      sbQuery.append(getFrameCondition(strUserType));
      sbQuery.append(" = " + strDetailID);
    }
    sbQuery.append(" ) V700 ");
    sbQuery.append(" WHERE T501.C501_ORDER_DATE = to_date('");
    sbQuery.append(strDate);
    sbQuery.append("','");
    sbQuery.append(strFormat);
    sbQuery.append("') ");
    sbQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbQuery.append(getSalesFilterClause());
    // Filter Condition to fetch record based on access code

    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    sbQuery.append(" AND t501.c704_account_id =v700.ac_id ");
    sbQuery.append(" AND t501.c703_sales_rep_id =v700.rep_id ");
    // Filter Condition to fetch record based on Sales Hierarchy
    // sbQuery.append(getHierarchyWhereCondition(hmParam,"T501."));

    sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
    sbQuery.append(" AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID");
    if (strOpt.equals("detail")) { // today sales detail popup
      sbQuery.append(" GROUP BY v700.VP_NAME, v700.AD_NAME, v700.D_NAME ");
      sbQuery.append(" , v700.REP_NAME, v700.AC_NAME, v700.AC_ID ");
      sbQuery.append(" ORDER BY v700.VP_NAME, v700.AD_NAME, v700.D_NAME ");
      sbQuery.append(" , v700.REP_NAME, v700.AC_NAME  ) V700");
    } else {
      sbQuery.append(getFrameGroup(strUserType));
      sbQuery.append(" ORDER BY SALES desc, NAME ");
    }
    log.debug("QUERY:" + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of todaysSalesReport

  public String getFrameCondition(String strAction) {

    StringBuffer sbFrameColumnQuery = new StringBuffer();
    sbFrameColumnQuery.setLength(0);
    if (strAction.equals("ASS") || strAction.equals("REP")) {
      sbFrameColumnQuery.append(" v700.AC_ID ");
    } else if (strAction.equals("DIST")) {
      sbFrameColumnQuery.append(" v700.REP_ID ");
    } else if (strAction.equals("AD")) {
      sbFrameColumnQuery.append(" v700.D_ID ");
    } else if (strAction.equals("VP")) {
      sbFrameColumnQuery.append(" v700.AD_ID ");
    } else if (strAction.equals("MGT") || strAction.equals("")) {
      sbFrameColumnQuery.append(" v700.VP_ID ");
    }

    log.debug("sbFrameColumnQuery :" + sbFrameColumnQuery.toString());
    return sbFrameColumnQuery.toString();
  }

  @Override
  public String getFrameColumn(String strAction) {
    StringBuffer sbFrameColumnQuery = new StringBuffer();
    sbFrameColumnQuery.setLength(0);

    if (strAction.equals("ASS") || strAction.equals("REP")) {
      sbFrameColumnQuery.append(" SELECT   v700.AC_ID ID ");
      sbFrameColumnQuery.append(" ,v700.AC_NAME NAME ");
    } else if (strAction.equals("DIST")) {
      sbFrameColumnQuery.append(" SELECT v700.REP_ID ID");
      sbFrameColumnQuery.append(" , v700.REP_NAME  NAME ");
    } else if (strAction.equals("AD")) {
      sbFrameColumnQuery.append(" SELECT v700.D_ID  ID ");
      sbFrameColumnQuery.append(" ,v700.D_NAME NAME ");
    } else if (strAction.equals("VP")) {
      sbFrameColumnQuery.append(" SELECT NVL(v700.AD_ID,0) ID ");
      sbFrameColumnQuery.append(" , NVL(v700.AD_NAME,'*No AD') NAME ");
    } else if (strAction.equals("MGT") || strAction.equals("")) {
      sbFrameColumnQuery.append(" SELECT v700.VP_ID ID ");
      sbFrameColumnQuery.append(" ,v700.VP_Name  NAME ");
    }
    log.debug("sbFrameColumnQuery :" + sbFrameColumnQuery.toString());
    return sbFrameColumnQuery.toString();
  }

  /**
   * getFrameGroup - This Method is used to frame the GROUPBY part of the main query depending on
   * Part or distributor
   * 
   * @param String strAction - Action is hardcoded from left menu
   * @return String sbFrameGroupQuery() - Query
   * @exception AppError
   */

  @Override
  public String getFrameGroup(String strAction) {

    StringBuffer sbFrameColumnQuery = new StringBuffer();
    sbFrameColumnQuery.setLength(0);

    if (strAction.equals("ASS") || strAction.equals("REP")) {
      sbFrameColumnQuery.append(" GROUP BY v700.AC_ID, v700.AC_NAME ");
    } else if (strAction.equals("DIST")) {
      sbFrameColumnQuery.append(" GROUP BY v700.REP_ID, v700.REP_NAME ");
    } else if (strAction.equals("AD")) {
      sbFrameColumnQuery.append(" GROUP BY v700.D_ID, v700.D_NAME ");
    } else if (strAction.equals("VP")) {
      sbFrameColumnQuery.append(" GROUP BY  NVL(v700.AD_ID,0), NVL(v700.AD_NAME,'*No AD') ");
    } else if (strAction.equals("MGT") || strAction.equals("")) {
      sbFrameColumnQuery.append(" GROUP BY v700.VP_ID, v700.VP_Name ");
    }
    log.debug("sbFrameColumnQuery :" + sbFrameColumnQuery.toString());
    return sbFrameColumnQuery.toString();
  }

  public HashMap getQuotaHmapDetails(ArrayList alReturn, HashMap hmParam) {
    int alsize = alReturn.size();
    double dbValMTD = 0.0;
    double dbValQTD = 0.0;
    double dbValYTD = 0.0;
    double dbValQMTD = 0.0;
    double dbValMQPER = 0.0;
    double dbValEMTD = 0.0;
    double dbValQQTD = 0.0;
    double dbValQQPER = 0.0;
    double dbValEQTD = 0.0;
    double dbValQYTD = 0.0;
    double dbValYQPER = 0.0;
    double dbValEYTD = 0.0;
    double dbValMTDGTHPER = 0.0;
    double dbValMTDGTHDLR = 0.0;
    double dbValYTDGTHDLR = 0.0;
    double dbValYTDGTHPER = 0.0;
    double dbValPYTD = 0.0;
    double dbValPMTD = 0.0;
    double dbValQTDGTHDLR = 0.0;
    double dblMtd = 0.0;
    double dbActualValEMTD = 0.0;
    double dblQtd = 0.0;
    double dblYtd = 0.0;
    double dblQMTD = 0.0;
    double dblMQPER = 0.0;
    double dblEMTD = 0.0;
    double dblQQTD = 0.0;
    double dblQQPER = 0.0;
    double dblQYTD = 0.0;
    double dblEQTD = 0.0;
    double dblYQPER = 0.0;
    double dblEYTD = 0.0;
    double dblMTDGTHPER = 0.0;
    double dblMTDGTHDLR = 0.0;
    double dblQTDGTHDLR = 0.0;
    double dblYTDGTHDLR = 0.0;
    double dblYTDGTHPER = 0.0;
    double dblPYTD = 0.0;
    double dblPMTD = 0.0;
    double dbTempMTD = 0.0;
    double dbPercentMTD = 0.0;
    double dbTempYTD = 0.0;
    double dbPercentYTD = 0.0;
    double dbTempGYTD = 0.0;
    double dbPercentGYTD = 0.0;
    double dbTempGMTD = 0.0;
    double dbPercentGMTD = 0.0;
    int intValMQPER = 0;
    int intValQQPER = 0;
    int intValYQPER = 0;
    int intValMTDGTHPER = 0;
    int intValYTDGTHPER = 0;
    HashMap hmLoop = new HashMap();
    HashMap hmReturn = new HashMap();
    double dbProjQuotaQMTD = 0.0;
    double dbProjQuotaQYTD = 0.0;

    int dbValue = 0;
    for (int i = 0; i < alsize; i++) {
      hmLoop = (HashMap) alReturn.get(i);
      dblMtd = GmCommonClass.parseDouble((Double) hmLoop.get("MTD")) / 1000;

      dbValMTD += dblMtd;

      dblQtd = GmCommonClass.parseDouble((Double) hmLoop.get("QTD")) / 1000;
      dbValQTD += dblQtd;

      dblYtd = GmCommonClass.parseDouble((Double) hmLoop.get("YTD")) / 1000;
      dbValYTD += dblYtd;

      dblQMTD = GmCommonClass.parseDouble((Double) hmLoop.get("QMTD")) / 1000;
      dbValQMTD += dblQMTD;

      dblMQPER = GmCommonClass.parseDouble((Double) hmLoop.get("MQPER"));
      dbValMQPER += dblMQPER;

      dblEMTD = GmCommonClass.parseDouble((Double) hmLoop.get("EMTD")) / 1000;
      dbValEMTD += dblEMTD;

      dblEMTD = GmCommonClass.parseDouble((Double) hmLoop.get("EMTD"));
      dbActualValEMTD += dblEMTD;
      dblQQTD = GmCommonClass.parseDouble((Double) hmLoop.get("QQTD")) / 1000;
      dbValQQTD += dblQQTD;

      dblQQPER = GmCommonClass.parseDouble((Double) hmLoop.get("QQPER"));
      dbValQQPER += dblQQPER;

      dblQYTD = GmCommonClass.parseDouble((Double) hmLoop.get("QYTD")) / 1000;
      dbValQYTD += dblQYTD;

      dblEQTD = GmCommonClass.parseDouble((Double) hmLoop.get("EQTD")) / 1000;
      dbValEQTD += dblEQTD;

      dblYQPER = GmCommonClass.parseDouble((Double) hmLoop.get("YQPER"));
      dbValYQPER += dblYQPER;

      dblEYTD = GmCommonClass.parseDouble((Double) hmLoop.get("EYTD") / 1000);
      dbValEYTD += dblEYTD;

      dblMTDGTHPER = GmCommonClass.parseDouble((Double) hmLoop.get("MTDGTHPER"));
      dbValMTDGTHPER += dblMTDGTHPER;

      dblMTDGTHDLR = GmCommonClass.parseDouble((Double) hmLoop.get("MTDGTHDLR")) / 1000;
      dbValMTDGTHDLR += dblMTDGTHDLR;

      dblQTDGTHDLR = GmCommonClass.parseDouble((Double) hmLoop.get("QTDGTHDLR")) / 1000;
      dbValQTDGTHDLR += dblQTDGTHDLR;

      dblYTDGTHDLR = GmCommonClass.parseDouble((Double) hmLoop.get("YTDGTHDLR") / 1000);
      dbValYTDGTHDLR += dblYTDGTHDLR;

      dblYTDGTHPER = GmCommonClass.parseDouble((Double) hmLoop.get("YTDGTHPER"));
      dbValYTDGTHPER += dblYTDGTHPER;

      dblPYTD = GmCommonClass.parseDouble((Double) hmLoop.get("PYTD")) / 1000;
      dbValPYTD += dblPYTD;

      dblPMTD = GmCommonClass.parseDouble((Double) hmLoop.get("PMTD")) / 1000;
      dbValPMTD += dblPMTD;

    }
    dbTempMTD = dbValMTD / dbValQMTD;
    if (Double.isNaN(dbTempMTD) || Double.isInfinite(dbTempMTD)) {
      dbTempMTD = 0;
    }
    dbPercentMTD = generateRoundUp(dbTempMTD * 100);

    dbTempYTD = dbValYTD / dbValQYTD;
    if (Double.isNaN(dbTempYTD) || Double.isInfinite(dbTempYTD)) {
      dbTempYTD = 0;
    }
    dbPercentYTD = generateRoundUp(dbTempYTD * 100);

    dbTempGYTD = ((dbValYTD - dbValPYTD) / dbValPYTD);
    if (Double.isNaN(dbTempGYTD) || Double.isInfinite(dbTempGYTD)) {
      dbTempGYTD = 0;
    }
    dbPercentGYTD = generateRoundUp(dbTempGYTD * 100);

    dbTempGMTD = ((dbValMTD - dbValPMTD) / dbValPMTD);
    if (Double.isNaN(dbTempGMTD) || Double.isInfinite(dbTempGMTD)) {
      dbTempGMTD = 0;
    }
    dbPercentGMTD = generateRoundUp(dbTempGMTD * 100);

    dbValMTD = generateRoundUp(dbValMTD);
    dbValYTD = generateRoundUp(dbValYTD);
    dbValQMTD = generateRoundUp(dbValQMTD);
    dbValQYTD = generateRoundUp(dbValQYTD);
    dbValPMTD = generateRoundUp(dbValPMTD);
    dbValPYTD = generateRoundUp(dbValPYTD);
    dbValYTDGTHDLR = generateRoundUp(dbValYTDGTHDLR);
    dbValMTDGTHDLR = generateRoundUp(dbValMTDGTHDLR);
    dbValEMTD = generateRoundUp(dbValEMTD);
    dbActualValEMTD = generateRoundUp(dbActualValEMTD);
    dbValEYTD = generateRoundUp(dbValEYTD);

    dbProjQuotaQMTD = dbValEMTD / dbValQMTD;
    if (Double.isNaN(dbProjQuotaQMTD) || Double.isInfinite(dbProjQuotaQMTD)) {
      dbProjQuotaQMTD = 0;
    }
    dbProjQuotaQMTD = generateRoundUp(dbProjQuotaQMTD * 100);

    dbProjQuotaQYTD = dbValEYTD / dbValQYTD;
    if (Double.isNaN(dbProjQuotaQYTD) || Double.isInfinite(dbProjQuotaQYTD)) {
      dbProjQuotaQYTD = 0;
    }
    dbProjQuotaQYTD = generateRoundUp(dbProjQuotaQYTD * 100);

    hmReturn.put("MTD", dbValMTD);
    hmReturn.put("YTD", dbValYTD);
    hmReturn.put("QMTD", dbValQMTD);
    hmReturn.put("QYTD", dbValQYTD);
    hmReturn.put("MQPER", dbPercentMTD);
    hmReturn.put("YQPER", dbPercentYTD);
    hmReturn.put("PMTD", dbValPMTD);
    hmReturn.put("PYTD", dbValPYTD);
    hmReturn.put("YTDGTHDLR", dbValYTDGTHDLR);
    hmReturn.put("MTDGTHDLR", dbValMTDGTHDLR);
    hmReturn.put("YTDGTHPER", dbPercentGYTD);
    hmReturn.put("MTDGTHPER", dbPercentGMTD);
    hmReturn.put("EMTD", dbValEMTD);
    hmReturn.put("EMTDACTUAL", dbActualValEMTD);
    hmReturn.put("EYTD", dbValEYTD);
    hmReturn.put("QTD", dbValQTD);
    hmReturn.put("QQTD", dbValQQTD);
    hmReturn.put("QQPER", dbValQQPER);
    hmReturn.put("EQTD", dbValEQTD);
    hmReturn.put("PRJQMTD", dbProjQuotaQMTD);
    hmReturn.put("PRJQYTD", dbProjQuotaQYTD);
    return hmReturn;
  }

  private Double generateRoundUp(Double dbValue) {
    double finalValue = Math.round(dbValue * 100.0) / 100.0;
    return finalValue;
  }
}
