package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashCongItemsBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmSalesDashCongItemsAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward congItemsCount(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmApprBoCount = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alApprovedCongItems = new ArrayList();
    ArrayList alBackOrderCongItems = new ArrayList();
    ArrayList alShippedCongItems = new ArrayList();
    ArrayList alAASCongItems = new ArrayList();
    ArrayList alRequestedCongItems = new ArrayList();
    ArrayList alApprBoCnt = new ArrayList();

    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strAccItemConRptFl = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    HttpSession session = request.getSession(false);

    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));


    GmSalesDashCongItemsBean gmSalesDashCongItemsBean =
        new GmSalesDashCongItemsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    GmAction gmAction = new GmAction();
    //To show account item consignment reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    gmSalesDashForm.setAccItemConRptFl(strAccItemConRptFl);



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    // end

    request.setAttribute("hmSalesFilters", hmSalesFilters);

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);
    hmParam.put("TOTALS", "Y");

      alRequestedCongItems =
          GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchApprovedCongItems(hmParam));
      
      if(strAccItemConRptFl.equals("Y")){
        hmParam.put("PASSNAME", "Approved_BO");  
         alApprBoCnt=
            GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));
        
        int alsize = alApprBoCnt.size();
        if (alsize <= 2) {
          for (int i = 0; i < alsize; i++) {
            hmTemp = GmCommonClass.parseNullHashMap((HashMap) alApprBoCnt.get(i));
            hmApprBoCount.put(hmTemp.get("NAME"), hmTemp.get("TOTAL"));
            if(hmTemp.get("NAME").equals("Back Order")){
              alBackOrderCongItems.add(hmTemp);
            }else if(hmTemp.get("NAME").equals("Approved")){
              alApprovedCongItems.add(hmTemp);
            }
          }
          //Adding Back Order value as 0
          if (hmApprBoCount.get("Back Order") == null) {
            hmTemp = new HashMap();
            hmTemp.put("NAME", "Back Order");
            hmTemp.put("TOTAL", 0);
            alBackOrderCongItems.add(hmTemp);
          }
          //Adding Approved value as 0
          if (hmApprBoCount.get("Approved") == null) {
            hmTemp = new HashMap();
            hmTemp.put("NAME", "Approved");
            hmTemp.put("TOTAL", 0);
            alApprovedCongItems.add(hmTemp);
          }
        }
       
      }else{
      hmParam.put("PASSNAME", "Approved");
      alApprovedCongItems =
          GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));
      hmParam.put("PASSNAME", "BO");
      alBackOrderCongItems =
          GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));
    }   
    hmParam.put("PASSNAME", "Shipped");
    alShippedCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));

    // Removed because giving more count for executive staff and taking time to load. if required
    // will add later.
    // hmParam.put("PASSNAME", "AAS");
    // alAASCongItems =
    // GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsCount(hmParam));

    alReturn.addAll(alRequestedCongItems);
    alReturn.addAll(alApprovedCongItems);
    alReturn.addAll(alShippedCongItems);
    alReturn.addAll(alBackOrderCongItems);

    // Removed because giving more count for executive staff and taking time to load. if required
    // will add later.
    // alReturn.addAll(alAASCongItems);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashCNItemCnt.vm");
    hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");

    // Use this format for Count
    strGridXmlData = generateOutPut(hmParam);
    if (!strGridXmlData.equals("")) {
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward requestedCongItems(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alApprovedCongItems = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));


    GmSalesDashCongItemsBean gmSalesDashCongItemsBean =
        new GmSalesDashCongItemsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    GmAction gmAction = new GmAction();
    //To show account item consignment reports based on the below rule(PMT-29390)
    String strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    gmSalesDashForm.setAccItemConRptFl(strAccItemConRptFl);


    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);

    hmParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);

    alApprovedCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchApprovedCongItems(hmParam));

    alReturn.addAll(alApprovedCongItems);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashReqCNItemDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");

  }

  public ActionForward apprCongItems(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alApprovedCongItems = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));


    GmSalesDashCongItemsBean gmSalesDashCongItemsBean =
        new GmSalesDashCongItemsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    GmAction gmAction = new GmAction();
    //To show account item consignment reports based on the below rule(PMT-29390)
    String strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    gmSalesDashForm.setAccItemConRptFl(strAccItemConRptFl);



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end
    hmParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmParam.put("APPLDATEFMT", strApplDateFmt);

    hmParam.put("AccessFilter", strFilterCondition);

    alApprovedCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchApprCongItems(hmParam));

    alReturn.addAll(alApprovedCongItems);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashCNItemDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward backOrderCongItems(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);

    HashMap hmParam = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alBackOrderCongItems = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));



    GmSalesDashCongItemsBean gmSalesDashCongItemsBean =
        new GmSalesDashCongItemsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    GmAction gmAction = new GmAction();
    //To show account item consignment reports based on the below rule(PMT-29390)
    String strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    gmSalesDashForm.setAccItemConRptFl(strAccItemConRptFl);


    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end


    hmParam.put("APPLDATEFMT", strApplDateFmt);

    hmParam.put("AccessFilter", strFilterCondition);

    alBackOrderCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchBackOrderCongItems(hmParam));

    alReturn.addAll(alBackOrderCongItems);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashCNItemDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward shippedCongItems(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alReturn = new ArrayList();
    ArrayList alShippedCongItems = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));



    GmSalesDashCongItemsBean gmSalesDashCongItemsBean =
        new GmSalesDashCongItemsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    GmAction gmAction = new GmAction();
    String strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    gmSalesDashForm.setAccItemConRptFl(strAccItemConRptFl);



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);

    alShippedCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchShippedCongItems(hmParam));

    alReturn.addAll(alShippedCongItems);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashCNItemDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward allCurrCongItemsASS(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alReturn = new ArrayList();
    ArrayList alAASCongItems = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));



    GmSalesDashCongItemsBean gmSalesDashCongItemsBean =
        new GmSalesDashCongItemsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    GmAction gmAction = new GmAction();



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);

    alAASCongItems =
        GmCommonClass.parseNullArrayList(gmSalesDashCongItemsBean.fetchCongItemsAAS(hmParam));
    alReturn.addAll(alAASCongItems);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashCNItemAASDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  private String generateOutPut(HashMap hmParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));

    ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alReturn"));
    String strFileName = GmCommonClass.parseNull((String) hmParam.get("fileName"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
