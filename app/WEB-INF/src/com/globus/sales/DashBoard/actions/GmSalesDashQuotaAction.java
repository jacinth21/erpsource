package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashBoardBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmTerritoryReportBean;

public class GmSalesDashQuotaAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward QuotaPerformance(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    String strCondition = "";
    String strGridXmlData = "";
    String strAccessFilter = "";
    String strSalesFilter = "";
    String strFilter = "";
    String strOpt = "";
    String strAccess = "";
    String strCompFilter = "";
    String strSessCompId = "";
    
    ArrayList alReturn = new ArrayList();
    ArrayList alReturnGrid = new ArrayList();


    HashMap hmSalesFilters = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmGridParam = new HashMap();
    HashMap hmReturn = new HashMap();
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
    GmTerritoryReportBean gmTerr = new GmTerritoryReportBean(getGmDataStoreVO());
    GmServlet gmServlet = new GmServlet();
    GmSalesDashBoardBean gmsalesDashBean = new GmSalesDashBoardBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();
    String strApplCurrFmt = gmSalesDashForm.getApplnCurrFmt();
    HttpSession session = request.getSession(false);
    String strAccessLvl = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
    request.setAttribute("strOverrideASSAccLvl", strAccessLvl);
    String strSessClientSysType = GmCommonClass.parseNull(gmSalesDashForm.getClientSysType());
    strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
    strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
    strCondition = gmSalesDispatchAction.getAccessCondition(request, response, true);
    strOpt = GmCommonClass.parseNull(gmSalesDashForm.getStrOpt());
    String strApplCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
    strCompFilter = GmCommonClass.parseNull(request.getParameter("hCompFilter"));
    strSessCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
    strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter;

    //PMT-33183 Quota Performance Historical
    String strReportType = GmCommonClass.parseNull(gmSalesDashForm.getReportType());
    gmFramework.getRequestParams(request, response, session, hmParam);

    if(strReportType.equals("Historical")){
    	// AccessCondition with t501 table
    	String strConditionHist = gmServlet.getAccessCondition(request, response, true);
    	hmParam.put("HistCondition", strConditionHist);
    	hmParam.put("ReportType", strReportType);
    	
    }
    //PMT-33183 Quota Performance Historical
    request.setAttribute("hmSalesFilters", hmSalesFilters);
    hmParam.put("Condition", strCondition);
    strSalesFilter = gmServlet.getSalesFilter(request, response);
    hmParam.put("Filter", strSalesFilter);
    String strType = gmsalesDashBean.fetchTypeQuota(request, response);
    hmParam.put("TYPE", strType);
    hmParam.put("Divider", "1");
    hmParam.put("strApplCurrSign", strApplCurrSign);
    hmParam.put("strApplCurrFmt", strApplCurrFmt);
    hmParam.put("COMPID", strCompFilter);
    hmReturn = gmTerr.fetchTerritoryPerformanceReport(hmParam);
    alReturn = (ArrayList) hmReturn.get("Details");
    hmReturn = gmsalesDashBean.getQuotaHmapDetails(alReturn, hmParam);
    hmReturn.put("saleType", "Sales (" + strApplCurrSign + ")");
    hmReturn.put("quotaType", "Quota (" + strApplCurrSign + ")");
    hmReturn.put("quotaTypePer", "(%) To Quota ");
    hmReturn.put("presale", "Previous Sale (" + strApplCurrSign + ")");
    hmReturn.put("growth", "Growth (" + strApplCurrSign + ")");
    hmReturn.put("perToGrowth", "(%) To Growth");
    hmReturn.put("proj", "Projected (" + strApplCurrSign + ") ");
    hmReturn.put("SESSCURRFMT", strApplCurrFmt);
    hmReturn.put("SESSCURRSIGN", strApplCurrSign);
    alReturnGrid.add(hmReturn);
    hmGridParam.put("alResult", alReturnGrid);
    hmGridParam.put("fileName", "GmSalesDashQuotaDetail.vm");
    strGridXmlData = generateOutPut(hmGridParam);

    if (!strGridXmlData.equals("")) {

      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }
    return mapping.findForward("gmSalesDashBoard");
  }

  private String generateOutPut(HashMap hmGridParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmGridParam.get("alResult"));
    String strFileName = GmCommonClass.parseNull((String) hmGridParam.get("fileName"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.Dashboard.GmSalesDashQuotaDetail", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }


}
