package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashCongSetsBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmSalesDashCongSetsAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward congSetsCount(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alAppCongSets = new ArrayList();
    ArrayList alShippedCongSets = new ArrayList();
    ArrayList alBackOrderCongSets = new ArrayList();
    ArrayList alAllCurrCongSets = new ArrayList();
    ArrayList alAllMissLocCongSets = new ArrayList();
    ArrayList alRequestedCongSets = new ArrayList();
    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);

    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();



    GmSalesDashCongSetsBean gmSalesDashCongSetsBean =
        new GmSalesDashCongSetsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();
    GmAction gmAction = new GmAction();



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);

    // end

    request.setAttribute("hmSalesFilters", hmSalesFilters);


    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);
    hmParam.put("TOTALS", "Y");

    hmParam.put("CONGSETTYPE", "Approved");

    gmFramework.getRequestParams(request, response, session, hmParam);

    alRequestedCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchApprovedCongSets(hmParam));
    alAppCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchCongSets(hmParam));
    alShippedCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchShippedCongSets(hmParam));
    hmParam.put("CONGSETTYPE", "BackOrder");

    alBackOrderCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchCongSets(hmParam));
    alAllCurrCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchAllCurrCongSets(hmParam));
    alAllMissLocCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchAllMissLocCongSets(hmParam));

    alReturn.addAll(alRequestedCongSets);
    alReturn.addAll(alAppCongSets);
    alReturn.addAll(alShippedCongSets);
    alReturn.addAll(alBackOrderCongSets);
    alReturn.addAll(alAllCurrCongSets);
    alReturn.addAll(alAllMissLocCongSets);



    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashCNSetCnt.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");

    strGridXmlData = generateOutPut(hmParam);
    if (!strGridXmlData.equals("")) {

      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }

    return mapping.findForward("gmSalesDashBoard");
  }

  /*
   * requestedCongSets method returns the list of Sets Info which are Requested Status.
   */
  public ActionForward requestedCongSets(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alCongSets = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmParamGrid = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAction gmAction = new GmAction();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    GmSalesDashCongSetsBean gmSalesDashCongSetsBean =
        new GmSalesDashCongSetsBean(getGmDataStoreVO());

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);

    hmParam.put("AccessFilter", strFilterCondition);
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("CONGSETTYPE", "Requested");

    alCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchApprovedCongSets(hmParam));

    hmParamGrid.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmParamGrid.put("alReturn", alCongSets);
    hmParamGrid.put("fileName", "GmSalesDashCNSetDetail.vm");
    hmParamGrid.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParamGrid.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNSetDetail");

    strGridXmlData = generateOutPut(hmParamGrid);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward approvedCongSets(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alCongSets = new ArrayList();
    ArrayList alReturn = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmParamGrid = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAction gmAction = new GmAction();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    GmSalesDashCongSetsBean gmSalesDashCongSetsBean =
        new GmSalesDashCongSetsBean(getGmDataStoreVO());


    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end


    hmParam.put("AccessFilter", strFilterCondition);
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("CONGSETTYPE", "Approved");

    alCongSets = GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchCongSets(hmParam));
    alReturn.addAll(alCongSets);

    hmParamGrid.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmParamGrid.put("alReturn", alReturn);
    hmParamGrid.put("fileName", "GmSalesDashCNSetDetail.vm");
    hmParamGrid.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParamGrid.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNSetDetail");

    strGridXmlData = generateOutPut(hmParamGrid);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward shippedCongSets(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alCongSets = new ArrayList();
    ArrayList alReturn = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmParamGrid = new HashMap();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAction gmAction = new GmAction();

    GmSalesDashCongSetsBean gmSalesDashCongSetsBean =
        new GmSalesDashCongSetsBean(getGmDataStoreVO());



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);

    hmParam.put("CONGSETTYPE", "Approved");

    alCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchShippedCongSets(hmParam));

    alReturn.addAll(alCongSets);

    hmParamGrid.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmParamGrid.put("alReturn", alReturn);
    hmParamGrid.put("fileName", "GmSalesDashCNSetDetail.vm");
    hmParamGrid.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParamGrid.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNSetDetail");

    strGridXmlData = generateOutPut(hmParamGrid);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward backOrderCongSets(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alCongSets = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmParamGrid = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAction gmAction = new GmAction();

    GmSalesDashCongSetsBean gmSalesDashCongSetsBean =
        new GmSalesDashCongSetsBean(getGmDataStoreVO());



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    strFilterCondition = getAccessFilter(request, response, true);

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);
    hmParam.put("CONGSETTYPE", "BackOrder");

    alCongSets = GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchCongSets(hmParam));
    alReturn.addAll(alCongSets);

    hmParamGrid.put("alReturn", alReturn);
    hmParamGrid.put("fileName", "GmSalesDashCNSetDetail.vm");
    hmParamGrid.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParamGrid.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNSetDetail");

    strGridXmlData = generateOutPut(hmParamGrid);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward getAllCongSets(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alCongSets = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HttpSession session = request.getSession(false);
    HashMap hmParam = new HashMap();
    HashMap hmParamGrid = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAction gmAction = new GmAction();
    GmFramework gmFramework = new GmFramework();

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strGridXmlData = "";
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    GmSalesDashCongSetsBean gmSalesDashCongSetsBean =
        new GmSalesDashCongSetsBean(getGmDataStoreVO());



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    String strFilterCondition = "";

    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);

    hmParam.put("AccessFilter", strFilterCondition);
    gmFramework.getRequestParams(request, response, session, hmParam);
    alCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchAllCurrCongSets(hmParam));
    alReturn.addAll(alCongSets);
    hmParamGrid.put("alReturn", alReturn);
    hmParamGrid.put("fileName", "GmSalesDashCNSetTagDetail.vm");
    hmParamGrid.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParamGrid.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNSetDetail");

    strGridXmlData = generateOutPut(hmParamGrid);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward getMissingLocation(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alCongSets = new ArrayList();
    ArrayList alReturn = new ArrayList();

    String strAccessCondition = "";
    String strGridXmlData = "";
    String strFilterCondition = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HashMap hmSalesFilters = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmParamGrid = new HashMap();

    HttpSession session = request.getSession(false);

    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAction gmAction = new GmAction();
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    GmSalesDashCongSetsBean gmSalesDashCongSetsBean =
        new GmSalesDashCongSetsBean(getGmDataStoreVO());



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    hmParam.put("AccessFilter", strFilterCondition);

    alCongSets =
        GmCommonClass.parseNullArrayList(gmSalesDashCongSetsBean.fetchAllMissLocCongSets(hmParam));
    alReturn.addAll(alCongSets);
    hmParamGrid.put("alReturn", alReturn);
    hmParamGrid.put("fileName", "GmSalesDashCNSetTagDetail.vm");
    hmParamGrid.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParamGrid.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNSetDetail");

    strGridXmlData = generateOutPut(hmParamGrid);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  private String generateOutPut(HashMap hmParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alReturn"));
    String strFileName = GmCommonClass.parseNull((String) hmParam.get("fileName"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(strFileName);
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
