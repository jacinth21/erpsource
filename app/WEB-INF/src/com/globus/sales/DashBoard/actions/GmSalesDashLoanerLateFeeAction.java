package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashBoardBean;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerBean;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerLateFeeBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmSalesDashLoanerLateFeeAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmCalenderOperations gmCal = new GmCalenderOperations();

  /**
	 * fetchLateFeeAccrStmt to get the loaner late fee details for previous month 
	 */
  public ActionForward fetchLateFeeAccrStmt(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmDateRange = new HashMap();
    HashMap hmGridParam = new HashMap();

    String strGridXmlData = "";
    String strFilterCondition = "";
    String strTransType = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    request.setAttribute("strOverrideAccLvl",
            GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    request.setAttribute("strOverrideASSAccLvl",
            GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    GmSalesDashLoanerLateFeeBean gmSalesDashLoanerLateFeeBean = new GmSalesDashLoanerLateFeeBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
   
    strFilterCondition = getAccessFilter(request, response, true);
    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    hmParam.put("AccessFilter", strFilterCondition);
    hmDateRange = gmSalesDashLoanerLateFeeBean.getLoanerLateFeeDateRange();
    hmParam.put("STARTDATE", GmCommonClass.parseNull((String) hmDateRange.get("PREMONTHSTARTDATE")));
	hmParam.put("ENDDATE", GmCommonClass.parseNull((String) hmDateRange.get("PREMONTHENDDATE")));
	
	alReturn = gmSalesDashLoanerLateFeeBean.fetchLoanerAccrLateFeeDetail(hmParam);

    hmGridParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmGridParam.put("SESSDATEFMT", strApplDateFmt);
    hmGridParam.put("APPLNCURRSIGN", GmCommonClass.parseNull((String) hmParam.get("APPLNCURRSIGN")));
    hmGridParam.put("APPLNCURRFMT", GmCommonClass.parseNull((String) hmParam.get("APPLNCURRFMT")));
    hmGridParam.put("alParam", alReturn);
    hmGridParam.put("fileName", "GmSalesDashLoanerLateFeeDtl.vm");
    hmGridParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmGridParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashLoanerDetail");

    strGridXmlData = generateOutPut(hmGridParam);
    strTransType = "Loaners";
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    gmSalesDashForm.setTransType(strTransType);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  /**
	 * fetchLateFeeAccrStmt to get the loaner late fee details for current month 
	 */
  public ActionForward fetchRecentLateFeeAccr(ActionMapping mapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {
	    instantiate(request, response);
	    ArrayList alReturn = new ArrayList();
	    HashMap hmParam = new HashMap();
	    HashMap hmDateRange = new HashMap();
	    HashMap hmGridParam = new HashMap();

	    String strGridXmlData = "";
	    String strFilterCondition = "";
	    String strTransType = "";
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
	    request.setAttribute("strOverrideAccLvl",
	            GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
	    request.setAttribute("strOverrideASSAccLvl",
	            GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

	    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
	    gmSalesDashForm.loadSessionParameters(request);
	    GmSalesDashLoanerLateFeeBean gmSalesDashLoanerLateFeeBean = new GmSalesDashLoanerLateFeeBean(getGmDataStoreVO());
	    GmCommonBean gmCommonBean = new GmCommonBean();

	    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

	    strFilterCondition = getAccessFilter(request, response, true);
	    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
	    hmParam.put("AccessFilter", strFilterCondition);
	    hmDateRange = gmSalesDashLoanerLateFeeBean.getLoanerLateFeeDateRange();
	    hmParam.put("STARTDATE", GmCommonClass.parseNull((String) hmDateRange.get("CURRMONTHSTARTDATE")));
		hmParam.put("ENDDATE", GmCommonClass.parseNull((String) hmDateRange.get("CURRMONTHENDDATE")));
		
		alReturn = gmSalesDashLoanerLateFeeBean.fetchLoanerRecentLateFeeDetail(hmParam);

	    hmGridParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
	    hmGridParam.put("APPLNCURRSIGN", GmCommonClass.parseNull((String) hmParam.get("APPLNCURRSIGN")));
	    hmGridParam.put("APPLNCURRFMT", GmCommonClass.parseNull((String) hmParam.get("APPLNCURRFMT")));
	    hmGridParam.put("SESSDATEFMT", strApplDateFmt);
	    hmGridParam.put("alParam", alReturn);
	    hmGridParam.put("fileName", "GmSalesDashLoanerLateFeeDtl.vm");
	    hmGridParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
	    hmGridParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashLoanerDetail");
	  
	    strGridXmlData = generateOutPut(hmGridParam);
	    
	    strTransType = "Loaners";
	    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
	    gmSalesDashForm.setTransType(strTransType);

	    return mapping.findForward("gmSalesDashBoardDetail");
	  }

  /**
	 * generateOutPut to generate the xml data 
	 */
   private String generateOutPut(HashMap hmGridParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmGridParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmGridParam.get("VMFILEPATH"));

    String strFileName = GmCommonClass.parseNull((String) hmGridParam.get("fileName"));
    templateUtil.setDataMap("hmParam", hmGridParam);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
