package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmSalesDashCaseSchdAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward casesScheduledCount(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alScheduledCases = new ArrayList();

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);

    HttpSession session = request.getSession(false);
    String strApplDateFmt =
        GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strPassName = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strPartyId = (String) request.getSession().getAttribute("strSessPartyId");
    String strCaseBookFl = "";
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                "110010")); //110010 - Case Book Setup link
        strCaseBookFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = getAccessCondition(request, response, true);
    // end

    request.setAttribute("hmSalesFilters", hmSalesFilters);
    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    strPassName = GmCommonClass.parseNull((String) hmParam.get("PASSNAME"));

    log.debug("casesScheduledCount ==> strPassName: " + strPassName);
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("PASSNAME", strPassName);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("CaseBookFl", strCaseBookFl);

    alScheduledCases =
        GmCommonClass.parseNullArrayList(gmCaseBookRptBean.fetchCasesScheduled(hmParam));
    log.debug("alScheduledCases  " + alScheduledCases);
    hmParam.put("alReturn", alScheduledCases);

    if (strPassName.equals("")) {
      hmParam.put("fileName", "GmSalesDashCaseCnt.vm");
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCNItemAASDetail");
      // Use this format for Count
      strGridXmlData = generateOutPut(hmParam);
      if (!strGridXmlData.equals("")) {
        response.setContentType("text/xml");
        PrintWriter pw = response.getWriter();
        pw.write(strGridXmlData);
        pw.flush();
        return null;
      }
    } else {
      hmParam.put("fileName", "GmSalesDashCaseDetail.vm");
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashCaseDetail");
      // Use this format for details
      strGridXmlData = generateOutPut(hmParam);
      gmSalesDashForm.setStrXMLGrid(strGridXmlData);
      return mapping.findForward("gmSalesDashBoardDetail");
    }

    return mapping.findForward("gmSalesDashBoard");
  }

  private String generateOutPut(HashMap hmParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));

    ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alReturn"));
    String strFileName = GmCommonClass.parseNull((String) hmParam.get("fileName"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
