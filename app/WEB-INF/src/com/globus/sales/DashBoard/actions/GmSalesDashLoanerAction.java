package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashBoardBean;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerBean;
import com.globus.sales.DashBoard.beans.GmSalesDashLoanerLateFeeBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmSalesDashLoanerAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmCalenderOperations gmCal = new GmCalenderOperations();

  public ActionForward loadSalesDashBoard(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);

    HashMap hmSales = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmMonthSales = new HashMap();
    ArrayList alUserTypeList = new ArrayList();

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean(getGmDataStoreVO());
    GmSalesDashBoardBean gmSalesDashBoardBean = new GmSalesDashBoardBean(getGmDataStoreVO());
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmAction gmAction = new GmAction();
    GmCommonBean gmCommBn = new GmCommonBean();
    GmCommonClass gmComm = new GmCommonClass();
    GmFramework gmFramework = new GmFramework();
    String strUserAccessType = "";
    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strTodaySalesType = GmCommonClass.parseNull(gmSalesDashForm.getTodaySalesType());
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strUserId = gmSalesDashForm.getUserId();
    String strDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strAccessLvl = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));

    String strApplCurrFmt = gmSalesDashForm.getApplnCurrFmt();
    String strDaySales = "";
    String strMonthSales = "";
    String strOpt = "";
    String daySale = "";
    String monthSale = "";

    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
   //To show account item consignment reports based on the below rule(PMT-29390)
    String strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    gmSalesDashForm.setAccItemConRptFl(strAccItemConRptFl);

    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = gmAction.getSalesAccessCondition(request, response, true);
    strUserAccessType = gmSalesDashBoardBean.fetchUserType(request, response);
    hmParam.put("USERID", strUserId);
    hmParam.put("TDATE", strDate != null ? strDate : "");
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("FORMAT", strApplDateFmt);

    String strApplCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
    gmFramework.getRequestParams(request, response, session, hmParam);

    hmSales = gmSalesReportBean.todaysSales(hmParam); // Todays and Month to Date sales figures
    strDaySales = (String) hmSales.get("DAYSALES");
    if (strDaySales.indexOf("-") != -1) {
      strDaySales = strDaySales.replace("-", "");
      daySale =
          "<font color=red>" + strApplCurrSign + " ("
              + gmComm.getStringWithCommas(strDaySales, strApplCurrFmt) + ")</font>";
    } else {
      daySale = strApplCurrSign + " " + gmComm.getStringWithCommas(strDaySales, strApplCurrFmt);
    }
    gmSalesDashForm.setTodaySales(daySale);
    strMonthSales = (String) hmSales.get("MONTHSALES");
    if (strMonthSales.indexOf("-") != -1) {
      strMonthSales = strMonthSales.replace("-", "");
      monthSale =
          "<font color=red>" + strApplCurrSign + " ("
              + gmComm.getStringWithCommas(strMonthSales, strApplCurrFmt) + ")</font>";
    } else {
      monthSale = strApplCurrSign + " " + gmComm.getStringWithCommas(strMonthSales, strApplCurrFmt);
    }
    gmSalesDashForm.setMonthlySales(monthSale);

    strOpt = gmSalesDashForm.getStrOpt();

    if (strOpt.equals("AjaxDaySales")) {
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(daySale);
      pw.flush();
      return null;
    } else if (strOpt.equals("AjaxMonthSales")) {
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(monthSale);
      pw.flush();
      return null;
    }

    ArrayList alTypeList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TDSTYP"));
    alUserTypeList =
        GmCommonClass.parseNullArrayList(populateUserTypeList(strUserAccessType, alTypeList));

    gmSalesDashForm.setAlUserTyp(alUserTypeList);
    gmSalesDashForm.setTodaySalesType(strTodaySalesType);
    request.setAttribute("hmSalesFilters", hmSalesFilters);
    gmSalesDashForm.loadSessionParameters(request);
    return mapping.findForward("gmSalesDashBoard");
  }

  /*
   * populateUserTypeList -- Used to populate the Today sales filetr by Drop down List.
   * 
   * @param strUserAccessType
   * 
   * @param alUserTypeList
   * 
   * @return ArrayList
   * 
   * @throws AppError
   */
  public ArrayList populateUserTypeList(String strUserAccessType, ArrayList alUserTypeList)
      throws AppError {

    ArrayList alReturnList = new ArrayList();
    boolean blFlag = false;
    for (int i = 0; i < alUserTypeList.size(); i++) {
      HashMap hmval = new HashMap();
      hmval = (HashMap) alUserTypeList.get(i);
      if (strUserAccessType.equals(hmval.get("CODENMALT"))) {
        blFlag = true;
      }
      if (blFlag) {
        alReturnList.add(hmval);
      }
    }
    return alReturnList;
  }

  public ActionForward loanersCount(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alReturn = new ArrayList();
    ArrayList alReqLoaners = new ArrayList();
    ArrayList alApprLoaners = new ArrayList();
    ArrayList alShippedLoaners = new ArrayList();
    ArrayList alDue1dayLoaners = new ArrayList();
    ArrayList alDue2dayLoaners = new ArrayList();
    ArrayList alOverDueLoaners = new ArrayList();
    ArrayList alMissPartLoaners = new ArrayList();
    ArrayList alDuetodayLoaners = new ArrayList();
    ArrayList alLoanerAllDue = new ArrayList();
	ArrayList alLoanerLateFees = new ArrayList();
    HashMap hmSalesFilters = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();
    HashMap hmParam = new HashMap();
    HashMap hmGridParam = new HashMap();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean(getGmDataStoreVO());
    GmSalesDashLoanerLateFeeBean gmSalesDashLoanerLateFeeBean = new GmSalesDashLoanerLateFeeBean(getGmDataStoreVO());
    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    request.setAttribute("strOverrideASSAccLvl",
            GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    
    String strShowDashLateFee  = GmCommonClass.parseNull(GmCommonClass.getString("SHOW_DASH_LATE_FEE"));

    request.setAttribute("hmSalesFilters", hmSalesFilters);

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

   
    alReqLoaners = GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchReqLoaner(hmParam));
    alApprLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchApprLoaner(hmParam));
    alShippedLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "Shipped"));
    alDue1dayLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "Due1Day"));
    alDue2dayLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "Due2Day"));
    alOverDueLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
            .fetchDuedayLoaner(hmParam, "OverDue"));


    /**
     * Commented the duplicate call to fetch the Overdue count to save the SQL execution time
     * 
     * @author gpalani PMT-10095 (RFC2252)
     * @since 2017-01-03
     */


    /*
     * alOverDueLoaners = GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean
     * .fetchDuedayLoaner(hmParam, "OverDue"));
     */

    // Adding new ArrayList To Display Due Today Drill down in Sales Dashboard Under Loaner Section
    alDuetodayLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchDuedayLoaner(hmParam,
            "DueToday"));


    alMissPartLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchMissingPartLoaner(hmParam));


    alReturn.addAll(alReqLoaners);
    alReturn.addAll(alApprLoaners);
    alReturn.addAll(alShippedLoaners);
    alReturn.addAll(alDuetodayLoaners);
    alReturn.addAll(alDue1dayLoaners);
    alReturn.addAll(alDue2dayLoaners);
    alReturn.addAll(alOverDueLoaners);
    alReturn.addAll(alMissPartLoaners);
    if(strShowDashLateFee.equalsIgnoreCase("YES")){
    	alLoanerLateFees = gmSalesDashLoanerLateFeeBean.fetchLoanerAccrLateFeeTotal(hmParam);
        alReturn.addAll(alLoanerLateFees);
    }

    hmGridParam.put("alParam", alReturn);
    hmGridParam.put("fileName", "GmSalesDashLoanerCnt.vm");
    hmGridParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmGridParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashLoanerDetail");

    strGridXmlData = generateOutPut(hmGridParam);
    if (!strGridXmlData.equals("")) {

      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }
    return mapping.findForward("gmSalesDashBoard");
  }

  public ActionForward requestedLoaners(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCommonBean gmCommonBean = new GmCommonBean();
    ArrayList alReturn = new ArrayList();
    ArrayList alReqLoaners = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strTransType = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HashMap hmParam = new HashMap();
    HashMap hmGridParam = new HashMap();

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean(getGmDataStoreVO());
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    strFilterCondition = getAccessFilter(request, response, true);

    // strAccessCondition = getAccessFilter(request, response);

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    hmParam.put("AccessFilter", strFilterCondition);
    alReqLoaners = GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchReqLoaner(hmParam));

    alReturn.addAll(alReqLoaners);

    hmGridParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmGridParam.put("SESSDATEFMT", strApplDateFmt);
    hmGridParam.put("alParam", alReturn);
    hmGridParam.put("fileName", "GmSalesDashLoanerDetail.vm");
    hmGridParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmGridParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashLoanerDetail");

    strGridXmlData = generateOutPut(hmGridParam);
    strTransType = "Loaners";
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    gmSalesDashForm.setTransType(strTransType);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward approvedLoaners(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alReturn = new ArrayList();
    ArrayList alReqLoaners = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmGridParam = new HashMap();

    String strGridXmlData = "";
    String strFilterCondition = "";
    String strTransType = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    strFilterCondition = getAccessFilter(request, response, true);
    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    hmParam.put("AccessFilter", strFilterCondition);
    alReqLoaners = GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchApprLoaner(hmParam));

    alReturn.addAll(alReqLoaners);

    hmGridParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmGridParam.put("SESSDATEFMT", strApplDateFmt);
    hmGridParam.put("alParam", alReturn);
    hmGridParam.put("fileName", "GmSalesDashLoanerDetail.vm");
    hmGridParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmGridParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashLoanerDetail");

    strGridXmlData = generateOutPut(hmGridParam);
    strTransType = "Loaners";
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    gmSalesDashForm.setTransType(strTransType);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward dueDayLoaners(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alReturn = new ArrayList();
    ArrayList alReqLoaners = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmGridParam = new HashMap();
    HashMap hmAccess = new HashMap();
    
    String strGridXmlData = "";
    String strFilterCondition = "";
    String strTransType = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean(getGmDataStoreVO());
    HashMap hmSalesFilters = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    
    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    strFilterCondition = getAccessFilter(request, response, true);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strPartyId = (String) request.getSession().getAttribute("strSessPartyId");
    
    hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                "SALEDASH_LOANER_EXTN")); 
    gmSalesDashForm.setAccessFlag(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));
    
    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    hmParam.put("AccessFilter", strFilterCondition);
    alReqLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchDuedayLoaner(hmParam, ""));
    alReturn.addAll(alReqLoaners);

    hmGridParam.put("LOANERACCESSFLAG", GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));
    hmGridParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmGridParam.put("SESSDATEFMT", strApplDateFmt);
    hmGridParam.put("alParam", alReturn);
    hmGridParam.put("fileName", "GmSalesDashLoanerDetail.vm");
    hmGridParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmGridParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashLoanerDetail");

    strGridXmlData = generateOutPut(hmGridParam);
    strTransType = "Loaners";
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    gmSalesDashForm.setTransType(strTransType);

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward missingPartLoaners(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    ArrayList alReturn = new ArrayList();
    ArrayList alReqLoaners = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmGridParam = new HashMap();
    GmSalesDashLoanerBean gmSalesDashLoanerBean = new GmSalesDashLoanerBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();
    String strGridXmlData = "";
    String strFilterCondition = "";
    String strTransType = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    HashMap hmSalesFilters = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean();
    log.debug(" In missingPartLoaners Action");
    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    strFilterCondition = getAccessFilter(request, response, true);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

    alReqLoaners =
        GmCommonClass.parseNullArrayList(gmSalesDashLoanerBean.fetchMissingPartLoaner(hmParam));

    alReturn.addAll(alReqLoaners);

    hmGridParam.put("PASSNAME", GmCommonClass.parseNull((String) hmParam.get("PASSNAME")));
    hmGridParam.put("SESSDATEFMT", strApplDateFmt);
    hmGridParam.put("alParam", alReturn);
    hmGridParam.put("fileName", "GmSalesDashLoanerMissPartDtl.vm");
    hmGridParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmGridParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashLoanerDetail");

    strTransType = "Loaners";
    strGridXmlData = generateOutPut(hmGridParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    gmSalesDashForm.setTransType(strTransType);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  private String generateOutPut(HashMap hmGridParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmGridParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmGridParam.get("VMFILEPATH"));

    // ArrayList alParam = GmCommonClass.parseNullArrayList((ArrayList)hmGridParam.get("alParam"));
    String strFileName = GmCommonClass.parseNull((String) hmGridParam.get("fileName"));
    templateUtil.setDataMap("hmParam", hmGridParam);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
