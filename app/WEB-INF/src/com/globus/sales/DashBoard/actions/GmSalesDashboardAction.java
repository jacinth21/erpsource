package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashBoardBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmSalesDashboardAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmCalenderOperations gmCal = new GmCalenderOperations();

  public ActionForward todaySales(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmSales = new HashMap();

    ArrayList alReturn = new ArrayList();

    String strAccessFilter = "";
    String strGridXmlData = "";
    String strAccessCondition = "";
    String strUserAccessType = "";
    String strTodaySalesType = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmSalesDashBoardBean gmSalesDashBoardBean = new GmSalesDashBoardBean(getGmDataStoreVO());



    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmAction gmAction = new GmAction();
    GmCommonBean gmCommBn = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    HttpSession session = request.getSession(false);

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    String strOpt = gmSalesDashForm.getStrOpt();
    String strApplDateFmt = gmSalesDashForm.getApplnDateFmt();
    String strUserId = gmSalesDashForm.getUserId();
    String strApplCurrFmt = gmSalesDashForm.getApplnCurrFmt();
    String strDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strAccessLvl = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
    request.setAttribute("strOverrideASSAccLvl", strAccessLvl);
    // Access Code for more filter and condition.
    strAccessFilter = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommBn.getSalesFilterLists(strAccessFilter);
    strAccessFilter = getAccessFilter(request, response, true);
    strAccessCondition = gmAction.getSalesAccessCondition(request, response, true);
    strUserAccessType = gmSalesDashBoardBean.fetchUserType(request, response);
    strTodaySalesType = GmCommonClass.parseNull(gmSalesDashForm.getTodaySalesType());
    strUserAccessType =
        (strTodaySalesType.equals("") || strTodaySalesType.equals("0")) ? strUserAccessType
            : strTodaySalesType;
    String strApplCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));

    hmParam.put("USERTYPE", strUserAccessType);
    hmParam.put("USERID", strUserId);
    // The below line format the date if date coming as 02/07/2014 to 07/02/2014.So commented below
    // code.
    // hmParam.put("TDATE", strDate!=null?GmCommonClass.getStringFromDate(new Date(strDate),
    // strApplDateFmt):"");
    hmParam.put("TDATE", strDate);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("FORMAT", strApplDateFmt);

    gmFramework.getRequestParams(request, response, session, hmParam);

    hmSales = gmSalesReportBean.todaysSales(hmParam); // Todays and Month to Date sales figures
    hmReturn.put("SALES", hmSales);

    alReturn = GmCommonClass.parseNullArrayList(gmSalesDashBoardBean.todaysSalesReport(hmParam));
    hmReturn.put("TODAY", alReturn);

    if (strOpt.equals("dataset")) {
      hmReturn.put("fileName", "GmSalesDashDaySalesRpt.vm");
      hmReturn.put("USERTYPE", strUserAccessType);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmReturn.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashDaySalesDtl");
    } else if (strOpt.equals("detail")) {// today sales detail popup
      hmReturn.put("fileName", "GmSalesDashDaySalesDtl.vm");
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmReturn.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashDaySalesDtl");
    } else {
      hmReturn.put("fileName", "GmSalesDashPie.vm");
    }
    hmReturn.put("AccessLvl", strAccessLvl);
    hmReturn.put("SESSCURRFMT", strApplCurrFmt);
    hmReturn.put("SESSCURRSIGN", strApplCurrSign);

    strGridXmlData = generateOutPut(hmReturn);

    if (strOpt.equals("detail")) { // today sales detail popup
      gmSalesDashForm.setStrXMLGrid(strGridXmlData);

      return mapping.findForward("gmSalesDashBoardDetail");
    }

    if (!strGridXmlData.equals("")) {

      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }
    return mapping.findForward("gmSalesDashBoard");
  }

  public ActionForward monthlySales(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    HashMap hmReturn = new HashMap();
    HashMap hmSales = new HashMap();

    ArrayList alReturn = new ArrayList();

    String strAccessFilter = "";
    String strGridXmlData = "";
    String strAccessCondition = "";
    String strMonthAvgSales = "";
    double dbMonthSales = 0;
    double dbAvgSales = 0;
    int intWorkDays = 0;
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmAction gmAction = new GmAction();
    GmCommonBean gmCommBn = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    HttpSession session = request.getSession(false);

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    String strOpt = gmSalesDashForm.getStrOpt();
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strApplCurrFmt = gmSalesDashForm.getApplnCurrFmt();

    String strUserId = gmSalesDashForm.getUserId();
    String strDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strAccessLvl = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
    request.setAttribute("strOverrideASSAccLvl", strAccessLvl);
    // Access Code for more filter and condition.
    strAccessFilter = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommBn.getSalesFilterLists(strAccessFilter);
    strAccessFilter = getAccessFilter(request, response, true);
    strAccessCondition = gmAction.getSalesAccessCondition(request, response, true);
    String strApplCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));

    gmFramework.getRequestParams(request, response, session, hmParam);

    hmParam.put("USERID", strUserId);
    hmParam.put("TDATE", strDate != null ? strDate : "");
    hmParam.put("COND", strAccessCondition);
    hmParam.put("FORMAT", strApplDateFmt);
    hmParam.put("Condition", strAccessCondition);
    hmSales = gmSalesReportBean.todaysSales(hmParam); // Todays and Month to Date sales figures
    hmReturn.put("SALES", hmSales);

    alReturn = GmCommonClass.parseNullArrayList(gmSalesReportBean.dailySalesReport(hmParam));
    hmReturn.put("MONTH", alReturn);

    /* below code for get average sales for current month working days */
    dbMonthSales = Double.parseDouble(GmCommonClass.parseZero((String) hmSales.get("MONTHSALES")));
    intWorkDays = gmSalesReportBean.fetchWorkingDays("");
    dbAvgSales = (intWorkDays == 0) ? 0 : (dbMonthSales / intWorkDays);
    if (dbAvgSales < 0) {
      strMonthAvgSales += (dbAvgSales * -1);
      strMonthAvgSales =
          "<font color=red>" + strApplCurrSign + " ("
              + GmCommonClass.getStringWithCommas(strMonthAvgSales, strApplCurrFmt) + ")</font>";
    } else {
      strMonthAvgSales += dbAvgSales;
      strMonthAvgSales =
          strApplCurrSign + " "
              + GmCommonClass.getStringWithCommas(strMonthAvgSales, strApplCurrFmt);
    }
    if (strOpt.equals("dataset")) {
      hmReturn.put("fileName", "GmSalesDashMonthSalesRpt.vm");
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmReturn.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashDaySalesDtl");
    } else {
      hmReturn.put("fileName", "GmSalesDashBar.vm");
    }

    hmReturn.put("AccessLvl", strAccessLvl);
    hmReturn.put("SESSDATEFMT", strApplDateFmt);
    hmReturn.put("SESSCURRFMT", strApplCurrFmt);
    hmReturn.put("SESSCURRSIGN", strApplCurrSign);

    strGridXmlData = generateOutPut(hmReturn);

    if (!strGridXmlData.equals("")) {
      strGridXmlData = strGridXmlData + "|" + strMonthAvgSales + "|" + strApplCurrSign; // Average
                                                                                        // sales
                                                                                        // appended
                                                                                        // here it
      // will be splited in js
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }
    return mapping.findForward("gmSalesDashBoard");
  }

  private String generateOutPut(HashMap hmParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    String strFileName = GmCommonClass.parseNull((String) hmParam.get("fileName"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
