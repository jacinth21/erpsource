package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashReturnsBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmSalesDashReturnsAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward returnCount(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmClosedInitCount = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alReturnsInitiated = new ArrayList();
    ArrayList alReturnsClosed = new ArrayList();
    ArrayList alReturnsCount = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strAccItemConRptFl = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();



    GmSalesDashReturnsBean gmSalesDashReturnsBean = new GmSalesDashReturnsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    GmAction gmAction = new GmAction();



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    // end

    request.setAttribute("hmSalesFilters", hmSalesFilters);

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);
    hmParam.put("PASSNAME", "Initiated");
    //To show account item consignment RA reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    if(strAccItemConRptFl.equals("Y")){
    //to fetch the count of both initiated and closed returns in single method call(query) 
      alReturnsCount =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAccReturnCount(hmParam));
      int alsize = alReturnsCount.size();
      if (alsize <= 2) {
        for (int i = 0; i < alsize; i++) {
          hmTemp = GmCommonClass.parseNullHashMap((HashMap) alReturnsCount.get(i));
          hmClosedInitCount.put(hmTemp.get("NAME"), hmTemp.get("TOTAL"));
          //if it is matched , then setting into the corresponding arraylist
          if(hmTemp.get("NAME").equals("RA - Initiated in Last 60 days")){
            alReturnsInitiated.add(hmTemp);
          }else if(hmTemp.get("NAME").equals("RA - Closed in Last 60 days")){
            alReturnsClosed.add(hmTemp);
          }
        }
        //Adding RA - Initiated  value as 0
        if (hmClosedInitCount.get("RA - Initiated in Last 60 days") == null ){
          hmTemp = new HashMap();
          hmTemp.put("NAME", "RA - Initiated in Last 60 days");
          hmTemp.put("TOTAL", 0);
          alReturnsInitiated.add(hmTemp);
        }
        //Adding RA - Closed  value as 0
        if (hmClosedInitCount.get("RA - Closed in Last 60 days") == null ){
          hmTemp = new HashMap();
          hmTemp.put("NAME", "RA - Closed in Last 60 days");
          hmTemp.put("TOTAL", 0);
          alReturnsClosed.add(hmTemp);
        }
      }
     
    }else{
      alReturnsInitiated =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchReturnCount(hmParam));     
      hmParam.put("PASSNAME", "Closed");
      alReturnsClosed =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchReturnCount(hmParam));
    }
   


    alReturn.addAll(alReturnsInitiated);
    alReturn.addAll(alReturnsClosed);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashRetnCnt.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashRetnCnt");

    // Use this format for Count
    strGridXmlData = generateOutPut(hmParam);
    if (!strGridXmlData.equals("")) {

      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }

    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward allInitReturns(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HashMap hmParam = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alReturnsInitiated = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strAccItemConRptFl = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();


    GmSalesDashReturnsBean gmSalesDashReturnsBean = new GmSalesDashReturnsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmFramework gmFramework = new GmFramework();
    GmAction gmAction = new GmAction();

    gmSalesDashForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end
    hmParam.put("PASSNAME", "Initiated");
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);
    //To show account item consignment RA reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    hmParam.put("ACCITEMCONRPTFL", strAccItemConRptFl);
    if(strAccItemConRptFl.equals("Y")){
      alReturnsInitiated =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAccCongItemReturns(hmParam));
    }else{
      alReturnsInitiated =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAllInitReturns(hmParam));
    }
    alReturn.addAll(alReturnsInitiated);

    hmParam.put("alReturn", alReturn);
    hmParam.put("alParam", alReturn);
    hmParam.put("fileName", "GmSalesDashRetnDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashRetnCnt");

    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward allClosedReturns(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HashMap hmParam = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alReturnsClosed = new ArrayList();

    String strFilterCondition = "";
    String strGridXmlData = "";
    String strAccItemConRptFl = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();


    GmSalesDashReturnsBean gmSalesDashReturnsBean = new GmSalesDashReturnsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmFramework gmFramework = new GmFramework();
    GmAction gmAction = new GmAction();

    gmSalesDashForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, true);
    // end
    hmParam.put("PASSNAME", "Closed");
    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);
    //To show account item consignment RA reports based on the below rule(PMT-29390)
    strAccItemConRptFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SAL_DASH_ACC_ITM",
            "SALEDASHTAB", getGmDataStoreVO().getCmpid()));
    hmParam.put("ACCITEMCONRPTFL", strAccItemConRptFl);
    if(strAccItemConRptFl.equals("Y")){
      alReturnsClosed =
          GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAccCongItemReturns(hmParam));
    }else{
    alReturnsClosed =
        GmCommonClass.parseNullArrayList(gmSalesDashReturnsBean.fetchAllClosedReturns(hmParam));
    }
    alReturn.addAll(alReturnsClosed);

    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashRetnDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashRetnCnt");

    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  private String generateOutPut(HashMap hmParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alReturn"));
    String strFileName = GmCommonClass.parseNull((String) hmParam.get("fileName"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateName(strFileName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
