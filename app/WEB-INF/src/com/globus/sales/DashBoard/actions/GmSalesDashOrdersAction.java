package com.globus.sales.DashBoard.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashOrdersBean;
import com.globus.sales.DashBoard.forms.GmSalesDashForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmSalesDashOrdersAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward ordersCount(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alSubmittedOrders = new ArrayList();
    ArrayList alShippedOrders = new ArrayList();
    ArrayList alHeldOrders = new ArrayList();
    ArrayList alBackOrders = new ArrayList();
    ArrayList alPO7Days = new ArrayList();
    ArrayList alPO15Days = new ArrayList();
    ArrayList alPO30Days = new ArrayList();

    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";

    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();



    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    GmServlet gmServlet = new GmServlet();



    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = gmServlet.getAccessCondition(request, response, true);// getAccessCondition(request,response,
                                                                               // true);
    // end

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("AccessFilter", strFilterCondition);
    hmParam.put("TOTALS", "Y");

    gmFramework.getRequestParams(request, response, session, hmParam);

    alSubmittedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchSubmittedOrders(hmParam));
    alShippedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchShippedOrders(hmParam));
    alHeldOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchHeldOrders(hmParam));
    alBackOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchBackOrders(hmParam));
    hmParam.put("PASSNAME", "PO14day");
    alPO7Days = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));
    hmParam.put("PASSNAME", "PO14-21day");
    alPO15Days = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));
    hmParam.put("PASSNAME", "PO21day");
    alPO30Days = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));

    alReturn.addAll(alSubmittedOrders);
    alReturn.addAll(alShippedOrders);
    alReturn.addAll(alHeldOrders);
    alReturn.addAll(alBackOrders);
    alReturn.addAll(alPO7Days);
    alReturn.addAll(alPO15Days);
    alReturn.addAll(alPO30Days);


    hmParam.put("alReturn", alReturn);
    hmParam.put("fileName", "GmSalesDashOrderCnt.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashOrderDetail");

    // Use this format for Count
    strGridXmlData = generateOutPut(hmParam);
    if (!strGridXmlData.equals("")) {

      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strGridXmlData);
      pw.flush();
      return null;
    }

    return mapping.findForward("gmSalesDashBoardDetail");
  }


  public ActionForward submittedOrders(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alSubmittedOrders = new ArrayList();

    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strOpt = "";

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    GmServlet gmServlet = new GmServlet();

    gmSalesDashForm.loadSessionParameters(request);
    strOpt = gmSalesDashForm.getStrOpt();

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = gmServlet.getAccessCondition(request, response, true);// getAccessCondition(request,response,
                                                                               // // true);
    // end
    String strCurrSymbol = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

    alSubmittedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchSubmittedOrders(hmParam));

    alReturn.addAll(alSubmittedOrders);

    hmParam.put("alReturn", alReturn);
    hmParam.put("alParam", alReturn);
    hmParam.put("CURRSYMB", strCurrSymbol);
    hmParam.put("fileName", "GmSalesDashOrderDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashOrderDetail");

    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward shippedOrders(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alShippedOrders = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    GmServlet gmServlet = new GmServlet();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = gmServlet.getAccessCondition(request, response, true);// getAccessCondition(request,response,
                                                                               // true);
    // end

    String strCurrSymbol = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

    alShippedOrders =
        GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchShippedOrders(hmParam));
    alReturn.addAll(alShippedOrders);

    hmParam.put("alReturn", alReturn);
    hmParam.put("CURRSYMB", strCurrSymbol);
    hmParam.put("fileName", "GmSalesDashOrderDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashOrderDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward heldOrders(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    ArrayList alHeldOrders = new ArrayList();
    ArrayList alReturn = new ArrayList();

    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmServlet gmServlet = new GmServlet();

    gmSalesDashForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);

    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = gmServlet.getAccessCondition(request, response, true);
    // end

    String strCurrSymbol = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

    alHeldOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchHeldOrders(hmParam));

    alReturn.addAll(alHeldOrders);

    hmParam.put("alReturn", alReturn);
    hmParam.put("alParam", alReturn);
    hmParam.put("CURRSYMB", strCurrSymbol);
    hmParam.put("fileName", "GmSalesDashOrderDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashOrderDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward backOrders(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alBackOrders = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();


    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    GmServlet gmServlet = new GmServlet();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = gmServlet.getAccessCondition(request, response, true);// getAccessCondition(request,response,
                                                                               // true);
    // end
    String strCurrSymbol = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

    alBackOrders = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchBackOrders(hmParam));
    alReturn.addAll(alBackOrders);

    hmParam.put("alReturn", alReturn);
    hmParam.put("CURRSYMB", strCurrSymbol);
    hmParam.put("fileName", "GmSalesDashOrderDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashOrderDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  public ActionForward pendingOrders(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    ArrayList alPOs = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    String strFilterCondition = "";
    String strAccessCondition = "";
    String strGridXmlData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmSalesDashForm gmSalesDashForm = (GmSalesDashForm) form;
    gmSalesDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    GmSalesDashOrdersBean gmSalesDashOrdersBean = new GmSalesDashOrdersBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashForm);
    GmServlet gmServlet = new GmServlet();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    request.setAttribute("strOverrideASSAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    // To get the Access Level information
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    strAccessCondition = gmServlet.getAccessCondition(request, response, true);// getAccessCondition(request,response,
                                                                               // true);
    // end

    String strCurrSymbol = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));

    hmParam.put("APPLDATEFMT", strApplDateFmt);
    hmParam.put("Condition", strAccessCondition);
    hmParam.put("AccessFilter", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

    alPOs = GmCommonClass.parseNullArrayList(gmSalesDashOrdersBean.fetchPOs(hmParam));
    alReturn.addAll(alPOs);

    hmParam.put("alReturn", alReturn);
    hmParam.put("CURRSYMB", strCurrSymbol);
    hmParam.put("fileName", "GmSalesDashOrderDetail.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("VMFILEPATH", "properties.labels.sales.Dashboard.GmSalesDashOrderDetail");
    // Use this format for details
    strGridXmlData = generateOutPut(hmParam);
    gmSalesDashForm.setStrXMLGrid(strGridXmlData);
    return mapping.findForward("gmSalesDashBoardDetail");
  }

  private String generateOutPut(HashMap hmParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alReturn"));
    String strFileName = GmCommonClass.parseNull((String) hmParam.get("fileName"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(strFileName);
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    return templateUtil.generateOutput();
  }
}
