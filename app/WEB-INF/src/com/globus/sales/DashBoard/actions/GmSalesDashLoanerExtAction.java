package com.globus.sales.DashBoard.actions;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.forms.GmSalesDashLoanerExtForm;
import com.globus.sales.Loaners.beans.GmLoanerExtBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmSalesDashLoanerExtAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  // Method to fetch the Loaner Extension Details based on the Product Request Detail id.
  public ActionForward fetchLoanerExt(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmSalesDashLoanerExtForm gmSalesDashLoanerExtForm = (GmSalesDashLoanerExtForm) form;
    instantiate(request, response);
    String strOpt = "";
    String strUserId = "";
    ArrayList alRequestDtls = new ArrayList();
    ArrayList alNewExtnDate = new ArrayList();
    ArrayList alDayExtn = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmResultData = new HashMap();
    strOpt = gmSalesDashLoanerExtForm.getStrOpt();
    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
    String strApplDateFmt = GmCommonClass.parseNull(gmSalesDashLoanerExtForm.getApplnDateFmt());
    // Gettign company JS date format
    String strApplJSDateFmt =
        GmCommonClass.getCodeAltName(GmCommonClass.getCodeID(strApplDateFmt, "DATFMT"));
    String strSessTodaysDate =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessTodaysDate"));
    String strSessCompanyLocale =
            GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashLoanerExtForm);
    hmParam.put("DATEFMT", strApplDateFmt);
    hmParam.put("TODAYSDATE", strSessTodaysDate);
    hmResultData = GmCommonClass.parseNullHashMap(gmLoanerExtBean.fetchLoanerExt(hmParam));
    alRequestDtls = (ArrayList) hmResultData.get("EXTNDETAILS");
    alNewExtnDate = (ArrayList) hmResultData.get("NEWEXTNDETIALS");
    alDayExtn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("EXTDAY"));
    gmSalesDashLoanerExtForm.setAlNewExtnDateValues(alNewExtnDate);
    gmSalesDashLoanerExtForm.setAlLoaneExtDetails(alRequestDtls);
    hmReturn.put("PENDLNRDATA", alRequestDtls);
    hmReturn.put("SESSDATEFMT", strApplDateFmt);
    hmReturn.put("SESSTODAYDATE", strSessTodaysDate);
    hmReturn.put("DROPDOWNLIST", alDayExtn);
    hmReturn.put("strSessCompanyLocale", strSessCompanyLocale);
    hmReturn.put("SESSJSDATEFMT", strApplJSDateFmt);
    String strxmlGridData = generateOutPut(hmReturn, "GmSalesDashLoanerExtDetail.vm");
    gmSalesDashLoanerExtForm.setXmlGridData(strxmlGridData);
    return mapping.findForward("GmSalesDashLoanerExt");
  }

  // Method to save the Loaner Extension
  public ActionForward saveLoanerExt(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ParseException,
      Exception {
    String strMessage = "Loaner extension requests are submitted successfully";
    GmSalesDashLoanerExtForm gmSalesDashLoanerExtForm = (GmSalesDashLoanerExtForm) form;
    instantiate(request, response);
    String strOpt = "";
    String strUserId = "";
    HashMap hmParam = new HashMap();
    strOpt = gmSalesDashLoanerExtForm.getStrOpt();
    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
    String strApplDateFmt = GmCommonClass.parseNull(gmSalesDashLoanerExtForm.getApplnDateFmt());
    String strSessTodaysDate =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessTodaysDate"));
    hmParam = GmCommonClass.getHashMapFromForm(gmSalesDashLoanerExtForm);
    strUserId =  GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    hmParam.put("USERID",strUserId);
    try {
      gmLoanerExtBean.saveLoanerExt(hmParam);
      if (!strMessage.equals("")) {
        throw new AppError(strMessage, "", 'S');
      }

    } catch (AppError appError) {
      request.setAttribute("hType", appError.getType());
      throw appError;
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("GmSalesDashLoanerExt");
  }

  // Method to report the Data in Grid Format
  private String generateOutPut(HashMap hmReturn, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale=GmCommonClass.parseNull((String) hmReturn.get("strSessCompanyLocale"));
    ArrayList alDayList = new ArrayList();
    alDayList = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("DROPDOWNLIST"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setTemplateSubDir("sales/DashBoard/templates");
    templateUtil.setTemplateName(vmFile);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.sales.Dashboard.GmSalesDashLoanerExtDetail", strSessCompanyLocale));
    templateUtil.setDropDownMaster("alDayCount", alDayList);
    return templateUtil.generateOutput();
  }
}
