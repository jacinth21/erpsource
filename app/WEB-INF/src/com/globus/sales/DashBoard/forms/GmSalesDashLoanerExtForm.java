package com.globus.sales.DashBoard.forms;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmLogForm;

public class GmSalesDashLoanerExtForm extends GmLogForm{
	private String xmlGridData = "";
	private String inputString = "";
	private String consignId = "";
	private String strComments = "";
	private String extendString = "";
	private String reqiestDtlID = "";
	
	private ArrayList alLoaneExtDetails = new ArrayList();
	private ArrayList alNewExtnDateValues = new ArrayList();

	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}

	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}

	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	/**
	 * @return the consignId
	 */
	public String getConsignId() {
		return consignId;
	}

	/**
	 * @param consignId the consignId to set
	 */
	public void setConsignId(String consignId) {
		this.consignId = consignId;
	}
		
	/**
	 * @return the strComments
	 */
	public String getStrComments() {
		return strComments;
	}

	/**
	 * @param strComments the strComments to set
	 */
	public void setStrComments(String strComments) {
		this.strComments = strComments;
	}	
		
	/**
	 * @return the extendString
	 */
	public String getExtendString() {
		return extendString;
	}

	/**
	 * @param extendString the extendString to set
	 */
	public void setExtendString(String extendString) {
		this.extendString = extendString;
	}

	/**
	 * @return the reqiestDtlID
	 */
	public String getReqiestDtlID() {
		return reqiestDtlID;
	}

	/**
	 * @param reqiestDtlID the reqiestDtlID to set
	 */
	public void setReqiestDtlID(String reqiestDtlID) {
		this.reqiestDtlID = reqiestDtlID;
	}

	/**
	 * @return the alLoaneExtDetails
	 */
	public ArrayList getAlLoaneExtDetails() {
		return alLoaneExtDetails;
	}

	/**
	 * @param alLoaneExtDetails the alLoaneExtDetails to set
	 */
	public void setAlLoaneExtDetails(ArrayList alLoaneExtDetails) {
		this.alLoaneExtDetails = alLoaneExtDetails;
	}

	/**
	 * @return the alNewExtnDateValues
	 */
	public ArrayList getAlNewExtnDateValues() {
		return alNewExtnDateValues;
	}

	/**
	 * @param alNewExtnDateValues the alNewExtnDateValues to set
	 */
	public void setAlNewExtnDateValues(ArrayList alNewExtnDateValues) {
		this.alNewExtnDateValues = alNewExtnDateValues;
	}
}
