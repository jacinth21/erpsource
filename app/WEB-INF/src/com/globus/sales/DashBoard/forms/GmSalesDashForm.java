package com.globus.sales.DashBoard.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmSalesDashForm extends GmCommonForm{
	private String passName = "";
	private String totals = "";
	private String strXMLGrid = "";
	private String todaySales="";
	private String monthlySales="";
	private String todaySalesType="";
	private String detailID = "";
	private String accountID = "";
	private String todaySalesTab = "";
	private String monthSalesTab = "";
	private String transType = "";
	private String accessFlag = "";
	private String accItemConRptFl = "";
	private ArrayList alUserTyp =new ArrayList();
	private String reportType ="";//33183 Quota Performance
	
		
	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}
	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}//33183 Quota Performance
	public String getAccessFlag() {
		return accessFlag;
	}
	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}
	public String getTodaySalesTab() {
		return todaySalesTab;
	}
	public void setTodaySalesTab(String todaySalesTab) {
		this.todaySalesTab = todaySalesTab;
	}
	public String getMonthSalesTab() {
		return monthSalesTab;
	}
	public void setMonthSalesTab(String monthSalesTab) {
		this.monthSalesTab = monthSalesTab;
	}
	public String getAccountID() {
		return accountID;
	}
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	public String getDetailID() {
		return detailID;
	}
	public void setDetailID(String detailID) {
		this.detailID = detailID;
	}
	public String getStrXMLGrid() {
		return strXMLGrid;
	}
	public void setStrXMLGrid(String strXMLGrid) {
		this.strXMLGrid = strXMLGrid;
	}
	public String getPassName() {
		return passName;
	}
	public void setPassName(String passName) {
		this.passName = passName;
	}
	
	/**
	 * @return the totals
	 */
	public String getTotals() {
		return totals;
	}
	/**
	 * @param totals the totals to set
	 */
	public void setTotals(String totals) {
		this.totals = totals;
	}
	
	public String getTodaySales() {
		return todaySales;
	}
	
	public void setTodaySales(String todaySales) {
		this.todaySales = todaySales;
	}	
	
	public String getMonthlySales() {
		return monthlySales;
	}
	
	public void setMonthlySales(String monthlySales) {
		this.monthlySales = monthlySales;
	}	
	
	public String getTodaySalesType() {
		return todaySalesType;
	}
	
	public void setTodaySalesType(String todaySalesType) {
		this.todaySalesType = todaySalesType;
	}	
	
	public ArrayList getAlUserTyp() {
		return alUserTyp;
	}
	
	public void setAlUserTyp(ArrayList alUserTyp) {
		this.alUserTyp = alUserTyp;
	}
	/**
	 * @return the transType
	 */
	public String getTransType() {
		return transType;
	}
	/**
	 * @param transType the transType to set
	 */
	public void setTransType(String transType) {
		this.transType = transType;
	}
  /**
   * @return the accItemConRptFl
   */
  public String getAccItemConRptFl() {
    return accItemConRptFl;
  }
  /**
   * @param accItemConRptFl the accItemConRptFl to set
   */
  public void setAccItemConRptFl(String accItemConRptFl) {
    this.accItemConRptFl = accItemConRptFl;
  }

	
}
