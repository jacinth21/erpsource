package com.globus.sales.InvManagement.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmTagSetInvMgtForm extends GmCommonForm{
	
	private String zone = "";
	private String region = "";
	private String distributorId = "";
	private String salesRepId = "";
	private String accountId = "";
	private String setId = "";
	private String tagIdFrom = "";   
	private String tagIdTo = "";
	private String currentLoc = "";
	private String lockInv = "";
	private String decommission = "";
	private String addressId = "";
	private String strTagIds = "";
	private String system = "";
	private String tagAttrType = "";
	private String tagAttrValue = "";
	private String tagAttrReason = "";
	private String tagAttrCmt = "";
	private String tagAttrStatus = "";
	private String cancelType = "";
	private String lockAccessFl = "";
	private String decomsAccessFl = "";
	private String distId = "";
	private String subLocId = "";
	private String lockAccId = "";
	private String curLocId = "";
	private String parentFrmName = "";
	private String divison = "";	
	private String typeid = "";
	
	
	public String getDivison() {
		return divison;
	}
	public void setDivison(String divison) {
		this.divison = divison;
	}
	private ArrayList alTagAttrReason = null;
	private ArrayList alZone = null;
	private ArrayList alRegion = null;
	private ArrayList alDistributor = null;
	private ArrayList alSalesRep = null;
	private ArrayList alAccount = null;
	private ArrayList alResult = null;
	private ArrayList alCurrentLoc = null;
	private ArrayList alLockInv = null;
	private ArrayList alDecommission = null;
	private ArrayList alAddressId = null;
	private ArrayList alSystem = null;
	private ArrayList alCommentsLog = null;
	private ArrayList alDivison = null;  
	private ArrayList alSet = null;
	private ArrayList alType = null;
	
	public ArrayList getAlDivison() {
		return alDivison;
	}
	public void setAlDivison(ArrayList alDivison) {
		this.alDivison = alDivison;
	}
	private String pageType = null;
//This variable is having a space, as the bean util will throw exception when its null
	private String gridData = " ";
	
	public String getGridData() {
		return gridData;
	}
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	public String getPageType() {
		return pageType;
	}
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getDistributorId() {
		return distributorId;
	}
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}
	public String getSalesRepId() {
		return salesRepId;
	}
	public void setSalesRepId(String salesRepId) {
		this.salesRepId = salesRepId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public String getTagIdFrom() {
		return tagIdFrom;
	}
	public void setTagIdFrom(String tagIdFrom) {
		this.tagIdFrom = tagIdFrom;
	}
	public String getTagIdTo() {
		return tagIdTo;
	}
	public void setTagIdTo(String tagIdTo) {
		this.tagIdTo = tagIdTo;
	}
	public String getCurrentLoc() {
		return currentLoc;
	}
	public void setCurrentLoc(String currentLoc) {
		this.currentLoc = currentLoc;
	}
	public String getLockInv() {
		return lockInv;
	}
	public void setLockInv(String lockInv) {
		this.lockInv = lockInv;
	}
	public String getDecommission() {
		return decommission;
	}
	public void setDecommission(String decommission) {
		this.decommission = decommission;
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getStrTagIds() {
		return strTagIds;
	}
	public void setStrTagIds(String strTagIds) {
		this.strTagIds = strTagIds;
	}
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getTagAttrType() {
		return tagAttrType;
	}
	public void setTagAttrType(String tagAttrType) {
		this.tagAttrType = tagAttrType;
	}
	public String getTagAttrValue() {
		return tagAttrValue;
	}
	public void setTagAttrValue(String tagAttrValue) {
		this.tagAttrValue = tagAttrValue;
	}
	public String getTagAttrReason() {
		return tagAttrReason;
	}
	public void setTagAttrReason(String tagAttrReason) {
		this.tagAttrReason = tagAttrReason;
	}
	public String getTagAttrCmt() {
		return tagAttrCmt;
	}
	public void setTagAttrCmt(String tagAttrCmt) {
		this.tagAttrCmt = tagAttrCmt;
	}
	public String getTagAttrStatus() {
		return tagAttrStatus;
	}
	public void setTagAttrStatus(String tagAttrStatus) {
		this.tagAttrStatus = tagAttrStatus;
	}
	public String getCancelType() {
		return cancelType;
	}
	public void setCancelType(String cancelType) {
		this.cancelType = cancelType;
	}
	
	
	public String getLockAccessFl() {
		return lockAccessFl;
	}
	public void setLockAccessFl(String lockAccessFl) {
		this.lockAccessFl = lockAccessFl;
	}
	public String getDecomsAccessFl() {
		return decomsAccessFl;
	}
	public void setDecomsAccessFl(String decomsAccessFl) {
		this.decomsAccessFl = decomsAccessFl;
	}
	public String getDistId() {
		return distId;
	}
	public void setDistId(String distId) {
		this.distId = distId;
	}

	public String getSubLocId() {
		return subLocId;
	}
	public void setSubLocId(String subLocId) {
		this.subLocId = subLocId;
	}
	public String getLockAccId() {
		return lockAccId;
	}
	public void setLockAccId(String lockAccId) {
		this.lockAccId = lockAccId;
	}
	public String getCurLocId() {
		return curLocId;
	}
	public void setCurLocId(String curLocId) {
		this.curLocId = curLocId;
	}
	/**
	 * @return the parentFrmName
	 */
	public String getParentFrmName() {
		return parentFrmName;
	}
	/**
	 * @param parentFrmName the parentFrmName to set
	 */
	public void setParentFrmName(String parentFrmName) {
		this.parentFrmName = parentFrmName;
	}
	public ArrayList getAlTagAttrReason() {
		return alTagAttrReason;
	}
	public void setAlTagAttrReason(ArrayList alTagAttrReason) {
		this.alTagAttrReason = alTagAttrReason;
	}
	public ArrayList getAlZone() {
		return alZone;
	}
	public void setAlZone(ArrayList alZone) {
		this.alZone = alZone;
	}
	public ArrayList getAlRegion() {
		return alRegion;
	}
	public void setAlRegion(ArrayList alRegion) {
		this.alRegion = alRegion;
	}
	public ArrayList getAlDistributor() {
		return alDistributor;
	}
	public void setAlDistributor(ArrayList alDistributor) {
		this.alDistributor = alDistributor;
	}
	public ArrayList getAlSalesRep() {
		return alSalesRep;
	}
	public void setAlSalesRep(ArrayList alSalesRep) {
		this.alSalesRep = alSalesRep;
	}
	public ArrayList getAlAccount() {
		return alAccount;
	}
	public void setAlAccount(ArrayList alAccount) {
		this.alAccount = alAccount;
	}
	public ArrayList getAlResult() {
		return alResult;
	}
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	public ArrayList getAlCurrentLoc() {
		return alCurrentLoc;
	}
	public void setAlCurrentLoc(ArrayList alCurrentLoc) {
		this.alCurrentLoc = alCurrentLoc;
	}
	public ArrayList getAlLockInv() {
		return alLockInv;
	}
	public void setAlLockInv(ArrayList alLockInv) {
		this.alLockInv = alLockInv;
	}
	public ArrayList getAlDecommission() {
		return alDecommission;
	}
	public void setAlDecommission(ArrayList alDecommission) {
		this.alDecommission = alDecommission;
	}
	public ArrayList getAlAddressId() {
		return alAddressId;
	}
	public void setAlAddressId(ArrayList alAddressId) {
		this.alAddressId = alAddressId;
	}
	public ArrayList getAlSystem() {
		return alSystem;
	}
	public void setAlSystem(ArrayList alSystem) {
		this.alSystem = alSystem;
	}
	public ArrayList getAlCommentsLog() {
		return alCommentsLog;
	}
	public void setAlCommentsLog(ArrayList alCommentsLog) {
		this.alCommentsLog = alCommentsLog;
	}	
	public String getTypeid() {
		return typeid;
	}
	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}	
	public ArrayList getAlSet() {
		return alSet;
	}
	public void setAlSet(ArrayList alSet) {
		this.alSet = alSet;
	}
	public ArrayList getAlType() {
		return alType;
	}
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}
}
