package com.globus.sales.InvManagement.displaytag;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTTagSetInvMgtRtpWrapper extends TableDecorator{
	 private HashMap hmRow ;
	 private int i = 0;
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		
		public String getTAGID() {
			hmRow = (HashMap) this.getCurrentRowObject();
			String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAG_ID"));
			String strSubLoc = GmCommonClass.parseNull((String) hmRow.get("SUB_LOCATION_ID"));
			String strImgCnt = GmCommonClass.parseZero((String) hmRow.get("IMGCNT"));
			StringBuffer sbHtml = new StringBuffer();
			sbHtml.append("	  <input type=\"checkbox\" value=\"" + strTAGID + "\"  name=\"Chk_tagid" + i + "\" onClick=\"fnTagID(this);\">");
			if (strImgCnt.equals("0")){
				sbHtml.append(" <img id='imgEdit' style='cursor:hand' src='/images/page.gif' title='Upload Image' width='20' height='20' onClick=\"fnTagImage('");	
			}else{
				sbHtml.append(" <img id='imgEdit' style='cursor:hand' src='/images/jpg_icon.jpg' title='View/Upload Image' width='20' height='22' onClick=\"fnTagImage('");
			}
			sbHtml.append(strTAGID);
			sbHtml.append("');\">" );
			sbHtml.append("  " + strTAGID);
			i++;
			return sbHtml.toString();
		}	
		
	public String getDECOMSIN() {
		hmRow =  	(HashMap) this.getCurrentRowObject();  
		String strDecommission = GmCommonClass.parseNull(String.valueOf(hmRow.get("DECOMSIN_FL"))); 
		String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAG_ID"));
		StringBuffer sbHtml = new StringBuffer();
		if (strDecommission.equals("Y")){
			sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/hold.png' title='Decommissioned' width=35' height='38'/>");				
		}else{
			sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/success.gif' title='Active' width='30' height='35'/>");;
		}
		sbHtml.append("<input type=\"hidden\" value=\"" +strDecommission+ "\"  name=\"Decomsin_" +strTAGID+ "\" >");
		return sbHtml.toString();	
	}
	
	public String getEXLDECOMSIN() {
		hmRow =  	(HashMap) this.getCurrentRowObject();  
		String strDecommission = GmCommonClass.parseNull(String.valueOf(hmRow.get("DECOMSIN_FL"))); 
		String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAG_ID"));
		StringBuffer sbHtml = new StringBuffer();
		if (strDecommission.equals("Y")){
			sbHtml.append("Decommissioned");				
		}else{
			sbHtml.append("Active");;
		}
		return sbHtml.toString();	
	}
	
	
	public String getLOCKINV() {
		hmRow =  	(HashMap) this.getCurrentRowObject();  
		String strLockInv = GmCommonClass.parseNull(String.valueOf(hmRow.get("LOCK_INV_FL"))); 
		String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAG_ID"));
		StringBuffer sbHtml = new StringBuffer();
		if (strLockInv.equals("Y")){
			sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/lock.jpg' title='Locked' width='32' height='37'/>");				
		}else{
			sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/unlock.gif' title='Unlock' width='28' height='32'/>");;
		}	
		sbHtml.append("<input type=\"hidden\" value=\"" +strLockInv+ "\"  name=\"Lock_" +strTAGID+ "\" >");
		return sbHtml.toString();	
	}
	
	public String getEXLOCKINV() {
		hmRow =  	(HashMap) this.getCurrentRowObject();  
		String strLockInv = GmCommonClass.parseNull(String.valueOf(hmRow.get("LOCK_INV_FL"))); 
		String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAG_ID"));
		StringBuffer sbHtml = new StringBuffer();
		if (strLockInv.equals("Y")){
			sbHtml.append("Locked");				
		}else{
			sbHtml.append("Unlock");;
		}	
		return sbHtml.toString();	
	}
	
	public String getCURADDR() {
		hmRow =  	(HashMap) this.getCurrentRowObject();  
		String strAddrNM = GmCommonClass.parseNull((String) hmRow.get("ADDR_NM"));
		String strAddr1 = GmCommonClass.parseNull((String) hmRow.get("ADDRESS1"));
		String strAddr2 = GmCommonClass.parseNull((String) hmRow.get("ADDRESS2"));
		String strCity = GmCommonClass.parseNull((String) hmRow.get("CITY"));
		String strState = GmCommonClass.parseNull((String) hmRow.get("STATE"));
		String strZipCode = GmCommonClass.parseNull((String) hmRow.get("ZIPCODE"));
		String strPhoneNumber = GmCommonClass.parseNull((String) hmRow.get("PHONENUMBER"));
		String strEmailId = GmCommonClass.parseNull((String) hmRow.get("EMAILID"));
		StringBuffer sbHtml = new StringBuffer();
		
		if(!strAddrNM.equals("")){
			sbHtml.append(strAddrNM);
			sbHtml.append("<BR>");
		}
		if(!strAddr1.equals("")){
			sbHtml.append(strAddr1);
			sbHtml.append("<BR>");
		}
		if(!strAddr2.equals("")){
			sbHtml.append(strAddr2);
			sbHtml.append("<BR>");
		}
		if(!strCity.equals("")){
			sbHtml.append(strCity);
			sbHtml.append(" ");
		}
		if(!strState.equals("")){
			sbHtml.append(strState);
			sbHtml.append(" ");
		}
		if(!strZipCode.equals("")){
			sbHtml.append(strZipCode);
			sbHtml.append("<BR>");
		}
		if(!strAddrNM.equals("")){
			sbHtml.append("Ph:-"+strPhoneNumber);
			sbHtml.append("<BR>");
		}
		if(!strAddrNM.equals("")){
			sbHtml.append("Email:-"+strEmailId);
			sbHtml.append("<BR>");
		}
		sbHtml.append("<BR>");

		return sbHtml.toString();	
	}
	
	public String getEXLCURADDR() {
		hmRow =  	(HashMap) this.getCurrentRowObject();  
		String strAddrNM = GmCommonClass.parseNull((String) hmRow.get("ADDR_NM"));
		String strAddr1 = GmCommonClass.parseNull((String) hmRow.get("ADDRESS1"));
		String strAddr2 = GmCommonClass.parseNull((String) hmRow.get("ADDRESS2"));
		String strCity = GmCommonClass.parseNull((String) hmRow.get("CITY"));
		String strState = GmCommonClass.parseNull((String) hmRow.get("STATE"));
		String strZipCode = GmCommonClass.parseNull((String) hmRow.get("ZIPCODE"));
		String strPhoneNumber = GmCommonClass.parseNull((String) hmRow.get("PHONENUMBER"));
		String strEmailId = GmCommonClass.parseNull((String) hmRow.get("EMAILID"));
		StringBuffer sbHtml = new StringBuffer();
		
		if(!strAddrNM.equals("")){
			sbHtml.append(strAddrNM);
			sbHtml.append(",");
		}
		if(!strAddr1.equals("")){
			sbHtml.append(strAddr1);
			sbHtml.append(",");
		}
		if(!strAddr2.equals("")){
			sbHtml.append(strAddr2);
			sbHtml.append(",");
		}
		if(!strCity.equals("")){
			sbHtml.append(strCity);
			sbHtml.append(" ");
		}
		if(!strState.equals("")){
			sbHtml.append(strState);
			sbHtml.append(" ");
		}
		if(!strZipCode.equals("")){
			sbHtml.append(strZipCode);
			sbHtml.append(",");
		}
		if(!strAddrNM.equals("")){
			sbHtml.append("Ph:-"+strPhoneNumber);
			sbHtml.append(",");
		}
		if(!strAddrNM.equals("")){
			sbHtml.append("Email:-"+strEmailId);
			sbHtml.append(",");
		}

		return sbHtml.toString();	
	}
}
