package com.globus.sales.InvManagement.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmTagSetAttributeBean extends GmBean{
	
	  public GmTagSetAttributeBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmTagSetAttributeBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public void saveTagSetInvLocation(HashMap hmParams) throws AppError{
		log.debug("========hmParams========="+hmParams);
		String strTagIds = GmCommonClass.parseNull((String) hmParams.get("STRTAGIDS"));
		String strCurrentLoc = GmCommonClass.parseNull((String) hmParams.get("CURLOCID"));
		String strSubLocId = GmCommonClass.parseNull((String) hmParams.get("SUBLOCID"));
		String strAddressID = GmCommonClass.parseNull((String) hmParams.get("ADDRESSID"));
		String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_tagset_txn.gm_sav_tagset_inv_location",6);
		gmDBManager.setString(1, strTagIds);
		gmDBManager.setString(2, strCurrentLoc);
		gmDBManager.setString(3, strSubLocId);
		gmDBManager.setString(4, strAddressID);
		gmDBManager.setString(5, "");
		gmDBManager.setString(6, strUserID);
		gmDBManager.execute();
		gmDBManager.commit();
		
	}
	public ArrayList fetchTagSetInvLocation(HashMap hmParams) throws AppError{
		log.debug("========hmParams========="+hmParams);
		ArrayList alResult = new ArrayList();
		String strTagIds = GmCommonClass.parseNull((String) hmParams.get("STRTAGIDS"));
		String strTagAttrType = GmCommonClass.parseNull((String) hmParams.get("TAGATTRTYPE"));
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_tagset_rpt.gm_fch_tagset_inv_location",3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTagIds);
		gmDBManager.setString(2, strTagAttrType);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		log.debug("========alResult========="+alResult);

		return alResult;
	}
	
	public void saveTagSetAttribute(HashMap hmParams) throws AppError{
		log.debug("========hmParams========="+hmParams);
		String strTagIds = GmCommonClass.parseNull((String) hmParams.get("STRTAGIDS"));
		String strAccID = GmCommonClass.parseNull((String) hmParams.get("LOCKACCID"));
		String strTagAttrType = GmCommonClass.parseNull((String) hmParams.get("TAGATTRTYPE"));
		String strTagAttrValue = GmCommonClass.parseNull((String) hmParams.get("TAGATTRVALUE"));
		String strTagAttrReason = GmCommonClass.parseNull((String) hmParams.get("TAGATTRREASON"));
		String strTagAttrCmt = GmCommonClass.parseNull((String) hmParams.get("TAGATTRCMT"));
		String strCancelType= GmCommonClass.parseNull((String) hmParams.get("CANCELTYPE"));
		String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		
		strAccID = strAccID.equals("0")?"":strAccID;
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_tagset_txn.gm_sav_tagset_attribute",8);
		gmDBManager.setString(1, strTagIds);
		gmDBManager.setString(2, strTagAttrType);
		gmDBManager.setString(3, strTagAttrValue);
		gmDBManager.setString(4, strAccID);
		gmDBManager.setString(5, strCancelType);
		gmDBManager.setString(6, strTagAttrReason);
		gmDBManager.setString(7, strTagAttrCmt);
		gmDBManager.setString(8, strUserID);
		gmDBManager.execute();
		gmDBManager.commit();
		
	}
	
	public ArrayList fetchCommentLogInfo(HashMap hmParams) throws AppError{
		log.debug("========hmParams========="+hmParams);
		ArrayList alResult = new ArrayList();
		String strTagIds = GmCommonClass.parseNull((String) hmParams.get("STRTAGIDS"));
		String strcancelType = GmCommonClass.parseNull((String)hmParams.get("CANCELTYPE"));
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_tagset_rpt.gm_fch_comment_loginfo",3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTagIds);
		gmDBManager.setString(2, strcancelType);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		log.debug("========alResult========="+alResult);

		return alResult;
	}
	
	 public HashMap sendEmail(HashMap hmParams,ArrayList alReturn)
		{
		    log.debug("hmParams  ="+hmParams);
		    log.debug("alReturn  ="+alReturn);
			HashMap hmReturn = new HashMap();
			GmEmailProperties emailProps = getEmailProperties(GmCommonClass.parseNull((String)hmParams.get("TEMPLATENAME")));
			GmJasperMail jasperMail = new GmJasperMail();
			String strDistMail = GmCommonClass.parseNull((String)hmParams.get("DISTEMAILID"));
			String strRepMail = GmCommonClass.parseNull((String)hmParams.get("REPEMAILID"));
			String strSubject = GmCommonClass.parseNull((String)hmParams.get("REASON"));
			jasperMail.setJasperReportName("/GmTagSetStatusChangeEmail.jasper");
			emailProps.setRecipients(strDistMail+","+strRepMail);
			jasperMail.setAdditionalParams(hmParams);
			jasperMail.setReportData(alReturn);
			jasperMail.setEmailProperties(emailProps);
			hmReturn = jasperMail.sendMail();
			return hmReturn;
		}
	 
	public GmEmailProperties getEmailProperties(String strTemplate)
		{
			GmEmailProperties emailProps = new GmEmailProperties();
			emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
			emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
			emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
			emailProps.setCc(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.CC));
			return emailProps;
		}
	public ArrayList fetchDistAcctAddress(String strRefId,String strEntity) throws AppError {
		String strPrcName = "";
		ArrayList alist = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		strPrcName =  strEntity.equals("50221")?"gm_pkg_sm_tagset_rpt.gm_fch_dist_address":"gm_pkg_sm_tagset_rpt.gm_fch_acct_address";
		gmDBManager.setPrepareString(strPrcName, 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strRefId);
		gmDBManager.execute();
		alist = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();

		return alist;

	}
}
