package com.globus.sales.InvManagement.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadException;
import javazoom.upload.UploadFile;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.servlets.GmUploadServlet;

public class GmSetImgMgtServlet extends GmUploadServlet {
  private static String strFileName = "";
  private static String strComnPath = GmCommonClass.getString("GMCOMMON");
  private static String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
  String strWhiteList = GmCommonClass.getString("SETIMAGEWHITELIST");
  String strUploadDir = GmCommonClass.getString("SETIMGDIR");
  String strFileSize = GmCommonClass.getString("MAXFILESIZE");

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    String strAction = "";
    String strFwdAction = "";
    String strName = "";
    String strID = "";
    String strJSPName = "/InvManagement/GmUploadTagSetImage.jsp";
    String strInputStr = "";
    String strRefType = "";
    String strTagID = "";

    try {
      checkSession(response, session);
      instantiate(request, response);
      GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
      MultipartFormDataRequest mrequest = null;
      ArrayList alUploadinfo = new ArrayList();

      if (MultipartFormDataRequest.isMultipartFormData(request)) { // creating MultiPart request
        mrequest = new MultipartFormDataRequest(request);
        strAction = GmCommonClass.parseNull(mrequest.getParameter("hAction"));
        strFwdAction = GmCommonClass.parseNull(mrequest.getParameter("hFwdAction"));
        strTagID = GmCommonClass.parseNull(mrequest.getParameter("TAGID"));
        strID = GmCommonClass.parseNull(mrequest.getParameter("hImgId"));
        strInputStr = GmCommonClass.parseNull(mrequest.getParameter("hInputStr"));
        strRefType = GmCommonClass.parseNull(mrequest.getParameter("hRefType"));
      } else {
        strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
        strFwdAction = GmCommonClass.parseNull(request.getParameter("hFwdAction"));
        strTagID = GmCommonClass.parseNull(request.getParameter("TAGID"));
        strID = GmCommonClass.parseNull(request.getParameter("hImgId"));
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        strRefType = GmCommonClass.parseNull(request.getParameter("hRefType"));
      }
      request.setAttribute("mrequest", mrequest);
      strAction = strAction.equals("") ? "Load" : strAction;

      if (strAction.equals("Save")) {
        uploadTagImage(request, mrequest, strTagID);
        strFileName = GmCommonClass.parseNull((String) request.getAttribute("fileName"));
        if (strFileName.equals("")) {
          strFileName = GmCommonClass.parseNull(mrequest.getParameter("fileName"));
        }
        if (!strFileName.equals("")) {
          gmCommonUploadBean.saveUploadUniqueInfo(strTagID, strRefType, strFileName, strUserId);
        }
        strAction = "Load";
      } else if (strAction.equals("Large")) {
        strName = gmCommonUploadBean.fetchFileName(strID);
      } else if (strAction.equals("Void")) {
        gmCommonUploadBean.deleteUploadInfo(strInputStr, strUserId);
        strAction = "Load";
      }

      alUploadinfo = gmCommonUploadBean.fetchUploadInfo(strRefType, strTagID);
      request.setAttribute("TAGID", strTagID);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hFwdAction", strFwdAction);
      request.setAttribute("alUploadinfo", alUploadinfo);
      request.setAttribute("NAME", strName);
      request.setAttribute("hImgId", strID);
      strDispatchTo = strSalesPath.concat(strJSPName);
      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (AppError e) {
      if (e.getType().equals("E")) {
        File file = new File(strUploadDir + "\\" + strFileName);
        if (file.exists())
          file.delete();
      }
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      dispatch(strErrorFileToDispatch, request, response);
    } catch (Exception uex) {
      session.setAttribute("hAppErrorMessage", uex.getMessage());
      gotoPage(strErrorFileToDispatch, request, response);
    }
  } // End of service method

  private void uploadTagImage(HttpServletRequest request, MultipartFormDataRequest mrequest,
      String strTagID) throws Exception {
    strFileName = GmCommonClass.parseNull(mrequest.getParameter("fileName"));
    String strTypeNm = GmCommonClass.parseNull(mrequest.getParameter("hTypeName"));
    if (strFileName.equals("")) {
      strFileName = getFileName(mrequest, strTagID);
    } else {
      strFileName = strFileName.substring(0, strFileName.lastIndexOf("."));
    }

    strUploadDir = strUploadDir + strTypeNm.replaceAll("/", "").toUpperCase() + "\\";
    setStrFileName(strFileName);
    setStrWhitelist(strWhiteList);
    setStrUploadHome(strUploadDir);
    Hashtable hmFiles = mrequest.getFiles();

    try {
      super.uploadFile(mrequest, request);

      if ((hmFiles != null) && (!hmFiles.isEmpty())) {
        UploadFile file = (UploadFile) hmFiles.get("uploadfile");
        long filesize = file.getFileSize();
        if (filesize > Integer.parseInt(strFileSize)) {
          String strURL = "/GmSetImgMgtServlet?hAction=Load&TAGID=" + strTagID;
          request.setAttribute("hRedirectURL", strURL);
          throw new AppError("", "20441");
        }
      }
    } catch (UploadException e) {
      request.setAttribute("hRedirectURL", "/GmSetImgMgtServlet?hAction=Load&TAGID=" + strTagID);
      String strException = e.getMessage();
      if (strException.indexOf(GmCommonClass.getString("UPLOAD_DEFAULT_ERROR")) != -1) {
        strException = GmCommonClass.getString("UPLOAD_MODIFIED_ERROR") + getStrWhitelist();
        throw new AppError(strException, "", 'E');
      }
    }
    request.setAttribute("fileName", getStrFileName());
  } // End of uploadTagImage method

  public String getFileName(MultipartFormDataRequest mrequest, String strTagID) throws Exception {
    strFileName = GmCommonClass.parseNull(mrequest.getParameter("fileName"));
    String strTypeNm = GmCommonClass.parseNull(mrequest.getParameter("hTypeName"));
    String strTodaysDate =
        GmCommonClass.parseNull((String) session.getAttribute("strSessTodaysDate"));
    String strUploadDir = GmCommonClass.getString("SETIMGDIR");
    String strDate = strTodaysDate.replaceAll("/", "");
    Calendar cal = new GregorianCalendar();
    int hour = cal.get(Calendar.HOUR);
    int min = cal.get(Calendar.MINUTE);
    int sec = cal.get(Calendar.SECOND);
    strUploadDir = strUploadDir + strTypeNm.replaceAll("/", "").toUpperCase() + "\\";
    strFileName = strTagID + "_" + strDate + hour + min + sec;

    return strFileName;
  } // End of getFileName mehtod

} // End of Servlet

