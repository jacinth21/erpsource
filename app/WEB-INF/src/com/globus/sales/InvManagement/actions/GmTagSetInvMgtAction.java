package com.globus.sales.InvManagement.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.party.beans.GmAddressBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.InvManagement.beans.GmTagSetAttributeBean;
import com.globus.sales.InvManagement.forms.GmTagSetInvMgtForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmTagSetInvMgtAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward tagSetInvReport(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    String strCondition = "";
    String strOpt = "";
    String strUserId = "";
    String strAccessFl = "";

    ArrayList alResult = new ArrayList();
    ArrayList alDist = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmTemp = new HashMap();

    String strPageType = "";
    String strDeptID = "";
    GmTagSetInvMgtForm gmTagSetInvMgtForm = (GmTagSetInvMgtForm) actionForm;
    gmTagSetInvMgtForm.loadSessionParameters(request);


    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmTagBean gmTagBean = new GmTagBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();



    strUserId = gmTagSetInvMgtForm.getUserId();
    strOpt = GmCommonClass.parseNull(gmTagSetInvMgtForm.getStrOpt());
    strPageType = GmCommonClass.parseNull(gmTagSetInvMgtForm.getPageType());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "LOCK_SET_INV"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmTagSetInvMgtForm.setLockAccessFl(strAccessFl);

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "DECMS_SET_INV"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmTagSetInvMgtForm.setDecomsAccessFl(strAccessFl);

    HttpSession session = request.getSession(false);
    strDeptID = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    strCondition = getAccessFilter(request, response);
    hmParam = GmCommonClass.getHashMapFromForm(gmTagSetInvMgtForm);

    // When the Request is from Set Inventory Report,then the Sales Rep can see all the filter.
    // In this condition , we will be excluding the Sales Filter completely.
    if (strOpt.equals("set_inv_rpt") || strPageType.equals("report")) {
      strCondition = "";
      // This denotes, the jsp will be for a report, hence Edit Button will be hidden or disabled.
      gmTagSetInvMgtForm.setPageType("report");
    }
    HashMap hmFilter = new HashMap();
    HashMap hmExclusion = new HashMap();
    hmFilter.put("CONDITION", strCondition);

    // From the Set Inventory Report the Algea Company and OUS distributor information is not
    // required. Hence setting it to the exclusion map.
    hmExclusion.put("COMPANY", " AND v700.compid NOT IN (100801) ");
    hmExclusion.put("DIST_TYPE", " AND v700.DISTTYPEID NOT IN (70105,70107) ");

    // For Sales Team the OUS Divison should not be displayed.
    // For Sales Team, they cannot look at OUS Tag details.
    if (strDeptID.equals("S")) {
      hmExclusion.put("DIVISION", " AND v700.divid NOT IN (100824) ");
      hmParam.put("DIVISON", "100823");
    }

    hmExclusion.put("COMPANY", " AND v700.COMPID NOT IN (100801) ");

    hmFilter.put("EXCLUSION_MAP", hmExclusion);
    hmFilter.put("FLTR_DIST_BY_COMP", "YES");// Filter Distributor Based on company
    hmSalesFilters = GmCommonClass.parseNullHashMap(gmCommonBean.getSalesFilterLists(hmFilter));
    gmTagSetInvMgtForm.setAlZone(GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters
        .get("ZONE")));
    gmTagSetInvMgtForm.setAlRegion(GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters
        .get("REGN")));
    gmTagSetInvMgtForm.setAlDivison(GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters
        .get("DIV")));
    alDist = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
    gmTagSetInvMgtForm.setAlDistributor(alDist);

    // As the Account and Sales Rep filter is not required in the report, we are not populating the
    // data .
    gmTagSetInvMgtForm.setAlAccount(new ArrayList());
    gmTagSetInvMgtForm.setAlSalesRep(new ArrayList());

    gmTagSetInvMgtForm.setAlCurrentLoc(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("MVTGL")));
    gmTagSetInvMgtForm.setAlLockInv(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("LCKINV")));
    gmTagSetInvMgtForm.setAlDecommission(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("DECINV")));
    gmTagSetInvMgtForm.setAlSystem(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("SYSTYP")));

    if (alDist.size() == 1) {
      hmTemp = (HashMap) alDist.get(0);
      gmTagSetInvMgtForm.setDistributorId(GmCommonClass.parseNull((String) hmTemp.get("ID")));
    }



    hmParam.put("ACCESSFILTER", strCondition);
    if (strOpt.equals("Reload")) {
      alResult = GmCommonClass.parseNullArrayList(gmTagBean.loadTagCurrentLocationDetails(hmParam));
      gmTagSetInvMgtForm.setAlResult(alResult);

      // If the Request is coming from Set Inventory Report, data has to be displayed in Grid.
      if (GmCommonClass.parseNull(gmTagSetInvMgtForm.getPageType()).equals("report")) {

        GmTemplateUtil templateUtil = new GmTemplateUtil();
        templateUtil.setDataList("alResult", alResult);
        templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.sales.InvManagement.GmTagSetInvMgtRpt", strSessCompanyLocale));
        templateUtil.setTemplateName("GmTagSetInvMgtReport.vm");
        templateUtil.setTemplateSubDir("sales/templates");
        gmTagSetInvMgtForm.setGridData(templateUtil.generateOutput());
      }

    }

    return actionMapping.findForward("GmTagSetInvMgtRpt");
  }



  public ActionForward tagSetLocation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    log.debug("GmTagSetInvMgtAction ");
    String strOpt = "";
    instantiate(request, response);
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    GmTagSetInvMgtForm gmTagSetInvMgtForm = (GmTagSetInvMgtForm) actionForm;
    gmTagSetInvMgtForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmTagSetAttributeBean gmTagSetAttributeBean = new GmTagSetAttributeBean(getGmDataStoreVO());



    hmParam = GmCommonClass.getHashMapFromForm(gmTagSetInvMgtForm);

    strOpt = gmTagSetInvMgtForm.getStrOpt();

    gmTagSetInvMgtForm.setAlDistributor(GmCommonClass.parseNullArrayList(GmCommonClass
        .parseNullArrayList(gmCustomerBean.getDistributorList("Active"))));
    gmTagSetInvMgtForm.setAlSalesRep(GmCommonClass.parseNullArrayList(GmCommonClass
        .parseNullArrayList(gmCustomerBean.getRepList("Active"))));
    gmTagSetInvMgtForm.setAlAccount(GmCommonClass.parseNullArrayList(GmCommonClass
        .parseNullArrayList(gmCommonBean.loadAccountList(""))));
    gmTagSetInvMgtForm.setAlCurrentLoc(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("MVTGL")));

    if (strOpt.equals("save")) {
      gmTagSetAttributeBean.saveTagSetInvLocation(hmParam);
    }

    if (strOpt.equals("load") || strOpt.equals("save")) {
      hmReturn =
          GmCommonClass.parseNullHashMap(tagSetInvLocation(hmParam, GmCommonClass
              .parseNullArrayList(gmTagSetAttributeBean.fetchTagSetInvLocation(hmParam))));
      gmTagSetInvMgtForm =
          (GmTagSetInvMgtForm) GmCommonClass.getFormFromHashMap(gmTagSetInvMgtForm, hmReturn);
    }
    return actionMapping.findForward("GmTagSetLocation");
  }

  public ActionForward tagSetAttribute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    String strCondition = "";
    String strOpt = "";
    String strReasonGrp = "";
    String strTagAttrType = "";
    String strReason = "";
    String strEmailTemplate = "";
    String strTagAttValue = "";
    String strTagAttrStatus = "";
    String strUpdateAccess = "";
    String strSecurityEvent = "";
    String strUserId = "";
    String strMailTo = "";
    boolean blMailFl = false;

    ArrayList alCommentsLog = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alMailList = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmTemp = new HashMap();
    GmTagSetInvMgtForm gmTagSetInvMgtForm = (GmTagSetInvMgtForm) actionForm;
    gmTagSetInvMgtForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmTagSetAttributeBean gmTagSetAttributeBean = new GmTagSetAttributeBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();



    strUserId = gmTagSetInvMgtForm.getUserId();
    strCondition = getAccessFilter(request, response);

    strOpt = gmTagSetInvMgtForm.getStrOpt();
    strTagAttrType = GmCommonClass.parseNull(gmTagSetInvMgtForm.getTagAttrType());
    strTagAttValue = GmCommonClass.parseNull(gmTagSetInvMgtForm.getTagAttrValue());
    strTagAttrStatus = GmCommonClass.parseNull(gmTagSetInvMgtForm.getTagAttrStatus());

    if (strTagAttrType.equals("11305")) {
      strReasonGrp = strTagAttrStatus.equals("Y") ? "ATULKR" : "ATLCKR";
      strSecurityEvent = "LOCK_SET_INV";
    } else if (strTagAttrType.equals("11310")) {
      strReasonGrp = strTagAttrStatus.equals("Y") ? "ATUDSR" : "ATDCSR";
      strSecurityEvent = "DECMS_SET_INV";
    }
    log.debug("strSecurityEvent" + strSecurityEvent);
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            strSecurityEvent));
    log.debug("hmAccess" + hmAccess);
    strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug("strUpdateAccess" + strUpdateAccess);
    if (!strUpdateAccess.equals("Y")) {
      throw new AppError("", "20592");
    }

    gmTagSetInvMgtForm.setAlAccount(GmCommonClass.parseNullArrayList(gmCommonBean
        .loadAccountList(strCondition)));
    gmTagSetInvMgtForm.setAlTagAttrReason(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList(strReasonGrp)));

    hmParam = GmCommonClass.getHashMapFromForm(gmTagSetInvMgtForm);
    if (strOpt.equals("save")) {
      gmTagSetAttributeBean.saveTagSetAttribute(hmParam);

      if (strTagAttValue.equals("Y")) {
        if (strTagAttrType.equals("11305")) {
          strEmailTemplate = "GmTagLockEmail";
          strReason = "Locked";
          blMailFl = true;
        } else if (strTagAttrType.equals("11310")) {
          strEmailTemplate = "GmTagDecommissionEmail";
          strReason = "Decommission";
          blMailFl = true;
        }
      }
      if (blMailFl) {
        alResult =
            GmCommonClass.parseNullArrayList(gmTagSetAttributeBean.fetchTagSetInvLocation(hmParam));

        for (int i = 0; i < alResult.size(); i++) {
          hmTemp = (HashMap) alResult.get(i);
          String strRepID = (String) hmTemp.get("REPID");
          if (i == 0) {
            strMailTo = strRepID;
          }

          if (strMailTo.equals(strRepID)) {
            hmTemp.put("TEMPLATENAME", strEmailTemplate);
            hmTemp.put("REASON", strReason);
            alMailList.add(hmTemp);
          } else {
            hmTemp.put("TEMPLATENAME", strEmailTemplate);
            hmTemp.put("REASON", strReason);
            alMailList.add(hmTemp);
            gmTagSetAttributeBean.sendEmail(hmTemp, alMailList);
            // alMailList = new ArrayList();
            strMailTo = strRepID;
          }

          if ((i == (alResult.size() - 1)) && alMailList.size() > 0) {
            gmTagSetAttributeBean.sendEmail(hmTemp, alMailList);
          }

        }
      }
    }

    if (strOpt.equals("load") || strOpt.equals("save")) {
      hmReturn =
          GmCommonClass.parseNullHashMap(tagSetInvLocation(hmParam, GmCommonClass
              .parseNullArrayList(gmTagSetAttributeBean.fetchTagSetInvLocation(hmParam))));
      gmTagSetInvMgtForm =
          (GmTagSetInvMgtForm) GmCommonClass.getFormFromHashMap(gmTagSetInvMgtForm, hmReturn);
    }
    alCommentsLog =
        GmCommonClass.parseNullArrayList(gmTagSetAttributeBean.fetchCommentLogInfo(hmParam));
    gmTagSetInvMgtForm.setAlCommentsLog(alCommentsLog);
    return actionMapping.findForward("GmTagSetAttribute");
  }

  public ActionForward repAddressList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTagSetInvMgtForm gmTagSetInvMgtForm = (GmTagSetInvMgtForm) actionForm;
    gmTagSetInvMgtForm.loadSessionParameters(request);
    GmAddressBean gmAddressBean = new GmAddressBean(getGmDataStoreVO());
    GmTagSetAttributeBean gmTagSetAttributeBean = new GmTagSetAttributeBean(getGmDataStoreVO());

    ArrayList alAddress = new ArrayList();

    String strRefId = "";
    String strEntity = "";
    String strXMLString = "";
    String strInactiveFl = "";
    strRefId = GmCommonClass.parseNull(request.getParameter("RefID"));
    strEntity = GmCommonClass.parseNull(request.getParameter("Entity"));
    if (strEntity.equals("50222")) {
      alAddress =
          GmCommonClass
              .parseNullArrayList(gmAddressBean.fetchPartyAddress(strRefId, strInactiveFl));
    } else if (strEntity.equals("50221") || strEntity.equals("11301")) {
      alAddress =
          GmCommonClass.parseNullArrayList(gmTagSetAttributeBean.fetchDistAcctAddress(strRefId,
              strEntity));
    }
    log.debug("alAddress= " + alAddress);
    strXMLString = createAjaxString(alAddress, "ID", "NAME");
    PrintWriter writer = response.getWriter();
    writer.println(strXMLString);
    writer.close();
    return null;
  }

  private String createAjaxString(ArrayList alList, String strId, String strName) {
    HashMap hmTemp = new HashMap();
    StringBuffer sbVal = new StringBuffer();
    String strID = "";
    String strNAME = "";

    for (int i = 0, j = 1; i < alList.size(); i++, j++) {
      hmTemp = (HashMap) alList.get(i);
      strID = GmCommonClass.parseNull((String) hmTemp.get(strId));
      strNAME = GmCommonClass.parseNull((String) hmTemp.get(strName));
      sbVal.append("obj.options[" + j + "] = new Option('");
      sbVal.append(strNAME.replaceAll("'", ""));
      sbVal.append("','");
      sbVal.append(strID);
      sbVal.append("');\n");
    }
    log.debug("ajax string " + sbVal.toString());
    return sbVal.toString();
  }

  private HashMap tagSetInvLocation(HashMap hmParams, ArrayList alResult) throws AppError {

    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();

    String strReturnTagIds = "";
    for (int i = 0; i < alResult.size(); i++) {
      hmTemp = (HashMap) alResult.get(i);
      String strID = (String) hmTemp.get("TAGID");
      strReturnTagIds = strReturnTagIds + "," + strID;
    }
    log.debug("========strReturnTagIds=========" + strReturnTagIds);
    if (alResult.size() > 0) {
      strReturnTagIds = strReturnTagIds.substring(1, strReturnTagIds.length());
      hmReturn = GmCommonClass.parseNullHashMap((HashMap) alResult.get(0));
      String strLoc = GmCommonClass.parseNull((String) hmReturn.get("CURRENTLOC"));
      hmReturn.put("STRTAGIDS", strReturnTagIds);
    }
    return hmReturn;
  }

  /**
   * fetchSetInvReport will be used to load the Sets Inventory Report based on Sales Hierarchy.
   * 
   * @param ActionMapping, ActionForm, HttpServletRequest and HttpServletResponse
   * @return ActionForward
   * @exception Exception
   */
  public ActionForward fetchSetInvReport(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    GmTagSetInvMgtForm gmTagSetInvMgtForm = (GmTagSetInvMgtForm) actionForm;
    gmTagSetInvMgtForm.loadSessionParameters(request);
    GmTagBean gmTagBean = new GmTagBean(getGmDataStoreVO());
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    ArrayList alSystems = new ArrayList();
    ArrayList alSets = new ArrayList();
    ArrayList alTypes = new ArrayList();
    ArrayList alDist = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmSetParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    String strCondition = "";
    String strOpt = "";
    String strTempDept = "";



    // Below lines of code is added to open the wide access to pull all the sets. For this report we
    // do not need access filter.
    hmSalesFilters = gmCommonBean.getSalesFilterLists("");
    HttpSession session = request.getSession(false);
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    strTempDept = strDepartMentID;
    session.setAttribute("strSessDeptId", "");
    strCondition = getAccessFilter(request, response, true);
    session.setAttribute("strSessDeptId", strTempDept);
    strTempDept = "";
    strDepartMentID = "";
    // //

    strOpt = GmCommonClass.parseNull(gmTagSetInvMgtForm.getStrOpt());
    hmParam = GmCommonClass.getHashMapFromForm(gmTagSetInvMgtForm);
    hmParam.put("CONDITION", strCondition);

    if (strOpt.equals("reload")) {
      alResult = gmTagBean.fetchSetInvReport(hmParam);
      GmTemplateUtil templateUtil = new GmTemplateUtil();
      templateUtil.setDataList("alResult", alResult);
      templateUtil.setTemplateName("GmSetInvReport.vm");
      templateUtil.setTemplateSubDir("sales/templates");
      gmTagSetInvMgtForm.setGridData(templateUtil.generateOutput());
    }

    alDist = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
    gmTagSetInvMgtForm.setAlDistributor(alDist);

    alSystems = GmCommonClass.parseNullArrayList(gmSalesReportBean.loadProjectGroup());
    hmSetParam.put("TYPE", "1601");
    alSets = GmCommonClass.parseNullArrayList(gmProjectBean.loadSetMap(hmSetParam));
    alTypes = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SETTYP"));
    log.debug("alSets in Action :: " + alSets);
    gmTagSetInvMgtForm.setAlSystem(alSystems);
    gmTagSetInvMgtForm.setAlType(alTypes);
    gmTagSetInvMgtForm.setAlSet(alSets);
    request.setAttribute("hmSalesFilters", hmSalesFilters);
    return actionMapping.findForward("GmSetInvReport");
  }
}
