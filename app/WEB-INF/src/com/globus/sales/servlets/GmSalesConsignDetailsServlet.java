/*****************************************************************************
 * File : GmSalesConsignDetailsServlet Desc : This Servlet will be used for all
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesConsignDetailBean;


public class GmSalesConsignDetailsServlet extends GmSalesServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMSALES");

    String strDispatchTo = "";
    String strJSPName = "";
    String strHeader = "";

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      GmCommonClass gmCommon = new GmCommonClass();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmSalesConsignDetailBean gmSalesConsignDetailBean =
          new GmSalesConsignDetailBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();

      HashMap hmReturn = new HashMap();
      HashMap hmFilterDetails = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmSalesFilters = new HashMap();
      GmCommonBean gmCommBn = new GmCommonBean();
      ArrayList alSets = new ArrayList(); // Value used to fill the combo box value
      ArrayList alDistributor = new ArrayList();
      ArrayList alStates = new ArrayList();
      ArrayList alRegions = new ArrayList();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();


      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      String strFilterCondition = "";
      // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
      String strSalesFilterFullAcc = GmCommonClass.parseNull((String) session.getAttribute("strSessSalesFullAccess"));
      request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      request.setAttribute("hmSalesFilters", hmSalesFilters);

      gmFramework.getRequestParams(request, response, session, hmParam);
      // Receive all the INput params through the GmSalesServlet and set them inside hmParam
      hmParam = receiveCrossTabRequest(request, response);

      // override AccessFilter
      hmParam.put("AccessFilter", strFilterCondition);
      // To retrieve the Distributor and Set List values
      alDistributor = gmCommonBean.getDistributorList();
      alSets = gmProject.loadSetMap("1600");
      alStates = GmCommonClass.getCodeList("STATE", getGmDataStoreVO());
      // log.debug("States :"+alStates);

      alRegions = gmCommBn.getRegions((String) hmParam.get("AccessFilter"));

      // Setting the distributor and set list to get displayed in the screen
      hmFilterDetails.put("DISTRIBUTORLIST", alDistributor);
      hmFilterDetails.put("SETLIST", alSets);
      hmFilterDetails.put("STATES", alStates);
      hmFilterDetails.put("REGIONS", alRegions);

      request.setAttribute("FILTERDETAILS", hmFilterDetails);


      log.debug(" hmParam input values " + hmParam);
      strJSPName = "/GmSalesConsignDetailsByPart.jsp";

      hmParam = addDates(hmParam, 3); // During load condition, if the dates are null, then set the
                                      // difference to 3 months
      // Get the report values
      // if (strOpt.equals("Report")){
      hmReturn = gmSalesConsignDetailBean.reportConsignSaleByDetail(hmParam);
      // }
      log.debug(" hmReturn input values " + hmReturn);
      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

      hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
      hmReturn.put("ALYEARDROPDOWN", alYearDropDown);


      request.setAttribute("hmReturn", hmReturn);

      // Additioan Filter details
      request.setAttribute("hFilterDetails", hmFilterDetails);
      request.setAttribute("strHeader", strHeader);

      // Set all the other parameters into request object through the sendRequestAttributes in
      // GmSalesServlet
      sendRequestAttributes(request, hmParam);

      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmTerritoryServlet
