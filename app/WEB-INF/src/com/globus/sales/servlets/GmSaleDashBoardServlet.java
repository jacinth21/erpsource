/*****************************************************************************
 * File : GmAcctDashBoardServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.DashBoard.beans.GmSalesDashBoardBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.beans.GmTerritoryReportBean;
import com.globus.webservice.sales.resources.GmSalesDashSalesResource;

public class GmSaleDashBoardServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    GmCalenderOperations gmCal = new GmCalenderOperations();
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    String strPageToLoad = "";
    String strSubMenu = "";
    String strFrom = "";
    String strId = "";
    String strPO = "";
    String strUserId = (String) session.getAttribute("strSessUserId");

    String strFilterCondition = null;
    String strAccessCondition = null;
    String strRegnFilter = "";
    String strCompFilter = "";
    String strDivFilter = "";
    String strZoneFilter = "";
    String strDistFilter = "";
    String strTerrFilter = "";
    String strPgrpFilter = "";
   
    

    HashMap hmParam = new HashMap();
    ArrayList alColorCodes = new ArrayList();
    
    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
     
      strAction = (strAction == null) ? "Load" : strAction;
      String strDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      GmCommonClass gmCommon = new GmCommonClass();
      GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      GmSalesDashBoardBean gmSalesDashBoardBean = new GmSalesDashBoardBean(getGmDataStoreVO());
      GmTerritoryReportBean gmTerritoryReportBean = new GmTerritoryReportBean(getGmDataStoreVO());
      GmSalesDashSalesResource gmSalesDashSalesResource = new GmSalesDashSalesResource();
      GmFramework gmFramework = new GmFramework();
      //PMT-33147 Quota Performance Historical
      String strReportType = GmCommonClass.parseNull(request.getParameter("hReportType"));
    
     
      HashMap hmReturn = new HashMap();
      HashMap hmTempParam = new HashMap();
      HashMap hmTempReturn = new HashMap();
      HashMap hmSalesFilters = new HashMap();

      String strAccessLvl = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
      request.setAttribute("strOverrideASSAccLvl", strAccessLvl);
      // To get the Access Level information
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      strAccessCondition = getAccessCondition(request, response, true);
      // getting color codes and legend count from rules
      String strColorCodes =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("ALLCOLORS", "DSALES"));
      String strOthersCode =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("OTHERSCOLOR", "DSALES"));
      String strLegendsCnt =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("LEGENDCOUNT", "DSALES"));
      // getting values from string array and adding to ArrayList
      String strCodes[] = strColorCodes.split(",");
      alColorCodes = new ArrayList(Arrays.asList(strCodes));

      gmFramework.getRequestParams(request, response, session, hmParam);
      if(strReportType.equalsIgnoreCase("Historical_HIDECOMPANY")){
    	  strReportType = "Historical";
    	  request.setAttribute("hideCompany", "Y");
      }

      if (strAction.equals("Load") || strAction.equals("Reload")) {
        hmParam.put("USERID", strUserId);
        hmParam.put("TDATE", GmCommonClass.parseNull(strDate));
        hmParam.put("COND", strAccessCondition);
        hmParam.put("FORMAT", strApplDateFmt);
        // This is to display the daily sales grid values by sorting of distributor by sales desc.
        hmParam.put("DISTSALEAMTGRP", "Y");
        hmReturn = gmSales.reportDashboard(hmParam);
        hmReturn.put("ALCOLORSLIST", alColorCodes);
        hmReturn.put("STROTHERSCOLORCODE", strOthersCode);
        hmReturn.put("STRLEGENDSCNT", strLegendsCnt);
        hmReturn.put("FORMAT", strApplDateFmt);
        request.setAttribute("hmReturn", hmReturn);
        // Form the fusion chart JSON data Array
        hmReturn.put("ALMONTHLISTSIZE", ((ArrayList) hmReturn.get("MONTH")).size());
        String strCurrFmt =
            GmCommonClass.parseNull((String) session.getAttribute("strSessApplCurrFmt"));
        hmReturn.put("SESSCURRFMT", strCurrFmt);
        String strCurrSign = GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb"));
        hmReturn.put("APPLNCURRSIGN", strCurrSign);
        String strSalesBarData = generateOutPut(hmReturn, "GmDailySalesBar.vm");
        hmTempParam.putAll(hmParam);

        hmTempReturn.put("TODAY", hmReturn.get("TODAY"));
        hmTempReturn.put("ALLCOLORS", alColorCodes);
        hmTempReturn.put("OTHERSCOLOR", strOthersCode);
        hmTempReturn.put("LEGENDCOUNT", Integer.parseInt(strLegendsCnt));
        String strDoughnutData = generateOutPut(hmTempReturn, "GmDailySalesDoughnut.vm");

        request.setAttribute("DOUGHNUTJSON", strDoughnutData);
        request.setAttribute("BARJSON", strSalesBarData);

        // Calculate TargetADS - Refer GmSalesDashQuotaAction/gmSalesDashSalesResource
        // Form the HashMap Data
        hmParam = new HashMap();
        String strSessCompId = "";
        strCompFilter = GmCommonClass.parseNull(request.getParameter("hCompFilter"));
        strSessCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
        strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter;
        hmParam.put("Condition", strFilterCondition);
        String strSalesFilter = getSalesFilter(request, response);
        hmParam.put("Filter", strSalesFilter);
        String strType = gmSalesDashBoardBean.fetchTypeQuota(request, response);
        hmParam.put("TYPE", strType);
        hmParam.put("Divider", "1");
        hmParam.put("strApplCurrSign", strCurrSign);
        hmParam.put("strApplCurrFmt", strCurrFmt);

        hmParam.put("COMPID", strCompFilter);

        gmFramework.getRequestParams(request, response, session, hmParam);
        // Get the Quota information

        /*
         * As we do not need the Previous Year sales for Calculating the MTD Quota in the Daily
         * Sales Chart, we are passing this parameter which will ignore the Previous year in the
         * query, which will result in faster page load time.
         */

        hmParam.put("IGNORE_LAST_YEAR_SALES", "YES");
        //33183 Quota Performance Month to Date Graph
        
        if(strReportType.equals("Historical")){
        	hmParam.put("HistCondition", strAccessCondition); 
            hmParam.put("ReportType", strReportType);
        } //33183 Quota Performance Month to Date Graph

        HashMap hmQuota = gmTerritoryReportBean.fetchTerritoryPerformanceReport(hmParam);
        
        ArrayList alDetails = GmCommonClass.parseNullArrayList((ArrayList) hmQuota.get("Details"));
        hmQuota = gmSalesDashBoardBean.getQuotaHmapDetails(alDetails, hmParam);

        // Pass Quota info & Calculate TargetADS
        hmQuota.put("DAILYQUOTA", "Y");
        String strTgtAvgSales =
            GmCommonClass.parseNull(gmSalesDashSalesResource.calculateTargetADS(hmQuota));
        request.setAttribute("TOACHIEVEQUOTA", strTgtAvgSales);

        double dbValEMTD = 0.0;
        double dbValMQPER = 0.0;
        double dbValPrjQuotaMQPER = 0.0;
        dbValEMTD = GmCommonClass.parseDouble((Double) hmQuota.get("EMTDACTUAL"));
        dbValMQPER = GmCommonClass.parseDouble((Double) hmQuota.get("MQPER"));
        dbValPrjQuotaMQPER = GmCommonClass.parseDouble((Double) hmQuota.get("PRJQMTD"));


        request.setAttribute("PROJECTEDSALES", dbValEMTD + "");
        request.setAttribute("QUOTATYPEPER", dbValMQPER + "");
        request.setAttribute("PRJQUOTAPERTYPE", dbValPrjQuotaMQPER + "");
      }

      request.setAttribute("hmSalesFilters", hmSalesFilters);
      dispatch(strAcctPath.concat("/GmSalesHome.jsp"), request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private String generateOutPut(HashMap hmTemplate, String strName) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmTemplate", hmTemplate);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setTemplateName(strName);
    return templateUtil.generateOutput();
  }

}// End of GmSaleDashBoardServlet
