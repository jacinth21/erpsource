package com.globus.sales.servlets;

/*****************************************************************************
 * File			 : GmSalesConsignSearchServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;



public class GmSalesAcctPriceReportServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strSalesPath = GmCommonClass.getString("GMSALES");
        String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
        
        String strBaseDirToUpload = GmCommonClass.parseNull(GmCommonClass.getString("PRICEINCREASEUPLOADDIR"));
        
		String strDispatchTo = strSalesPath.concat("/GmAccountPriceReport.jsp");
        
		RowSetDynaClass resultSet = null ;
		HashMap hmparameter;
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			instantiate(request,response);
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
			GmCommonBean gmComBean 		= new GmCommonBean(getGmDataStoreVO());
            GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
            GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
            GmCommonClass gmCommonClass = new GmCommonClass();
            GmPricingRequestBean gmPricingRequestBean= new GmPricingRequestBean(getGmDataStoreVO());
            GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());
            GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
            HashMap hmReturn = new HashMap();
            HashMap hmParam = new HashMap();
            ArrayList alReturn = new ArrayList();
            ArrayList alAccReturn = new ArrayList();
            ArrayList alSystemList = new ArrayList();
            ArrayList alLikeOptions = new ArrayList();
            
            Map parameters = new HashMap();
            
            HashMap hmResult = new HashMap();
            HashMap hmTemp = new HashMap();
            HashMap hmAccDtl = new HashMap();

            String strUserId =  (String)session.getAttribute("strSessUserId");
            String strProjId = "";
            String strhProjId = "";
            String strAccId = "";
            String strAccNm = "";
            String strFrmDt = "";
            String strToDt = "";
            String strDownloadType = "";
            String strIncreasePrice = "6";
            String strMode = "";
            String strQueryBy = "";
            String strPartNum = "";
            String strPartNumber = "";
            String strLikeOption = "";
            String strChkSelectAll = GmCommonClass.parseNull((String)request.getParameter("Chk_selectAll"));
            String strAccCurrSymb ="";
           
			String strAction = request.getParameter("hAction");
			strDownloadType = GmCommonClass.parseNull(request.getParameter("hDTYPE")) ;
			strMode = GmCommonClass.parseNull(request.getParameter("hMode")) ;
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("hAction");
			}
			strAction = (strAction == null)?"Load":strAction;

	
            hmTemp.put("COLUMN","APRICE");
            hmTemp.put("STATUSID","20302"); // Filter all launched projects
			
			
			String strOpt = request.getParameter("hOpt");
			
			if (strOpt == null)
			{
				strOpt = (String)session.getAttribute("strSessOpt");
			}
			
				//To get the Access Level information from Materialized view 
				String strAccessFilter =  getAccessFilter(request,response);		
				
				//To get the Access Level information 
				String strCondition =  getAccessCondition(request,response);
				
				// To get the Account information for the logged in user
				alAccReturn  = gmComBean.getAccountList(strCondition);
				hmReturn.put("ACCOUNTLIST",alAccReturn);
				ArrayList alProjList = gmProj.reportProject(hmTemp);
		        hmReturn.put("PROJECTLIST",alProjList);
				alSystemList = gmPricingRequestBean.loadSystemsList();
				hmReturn.put("SYSTEMLIST", alSystemList);
				 alLikeOptions = gmAutoCompleteReportBean.getCodeList("LIKES", getGmDataStoreVO()); 
				hmReturn.put("LIKEOPTIONS", alLikeOptions);
				
				if (strAction.equals("Load"))
				{
					request.setAttribute("hmReturn",hmReturn);
				}
				
				else if (strAction.equals("Reload") || strAction.equals("PRINT") )
				{
					//if (strAction.equals("PRINT") ) 
						
					HashMap hmValues = new HashMap();
					
					strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
					strAccNm = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));
					strhProjId = GmCommonClass.parseNull(request.getParameter("hProjIds"));
					strQueryBy = GmCommonClass.parseNull(request.getParameter("Cbo_QueryBy"));
					strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
					strLikeOption = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
					strProjId = strhProjId.replaceAll(",","','");
					
					if (strOpt.equals("PriceIncrease") ) {
						strIncreasePrice = GmCommonClass.parseNull(request.getParameter("Txt_PerIncrease"));
					
						if( strIncreasePrice.charAt(strIncreasePrice.length()-1) == '%')
							strIncreasePrice = strIncreasePrice.substring(0,strIncreasePrice.length()-1);
					}
					
					if (strLikeOption.equals("90181")) {
						strPartNumber = ("^").concat(strPartNum);
						strPartNumber = strPartNumber.replaceAll(",", "|^");
					 } else if (strLikeOption.equals("90182")) {
						 strPartNumber = strPartNum.replaceAll(",", "\\$|");
						 strPartNumber = strPartNumber.concat("$");
					 } else {
						 strPartNumber = strPartNum.replaceAll(",", "|");
					 }
					
					
					
					hmValues.put("ACCID",strAccId);
					hmValues.put("PROJID",strProjId);
					hmValues.put("ACCESSCONDITION",strCondition);
					hmValues.put("INCREASEPRICE",strIncreasePrice);
					hmValues.put("MODE",strMode);
					hmValues.put("QUERYBY",strQueryBy);
					hmValues.put("PARTNUM",strPartNumber);
					hmValues.put("ACCNAME", strAccNm);
					
					if( !strAccId.equals("") && !strAccId.equals("0") )
					{
						hmResult = gmSales.reportAccPrice(hmValues);
					}
					
					resultSet = (RowSetDynaClass)hmResult.get("PRICINGLIST");
					hmReturn.put("ACCDETAIL",hmResult.get("ACCDETAIL"));
					hmReturn.put("PRICINGLISTREPORT",hmResult.get("PRICINGLISTREPORT"));
					hmParam.put("DISTID",strAccId);
					hmParam.put("SETID",strProjId);
					
					// resultSet = gmSales.reportSeachSalesConsign(hmParam);
					request.setAttribute("ACCPRICEREPORT",resultSet);
					hmParam.put("SETID",strhProjId);
					
					request.setAttribute("hmParam",hmParam);
					
                    //********************************************************************
                    // Jasper Report setting
                    // Calling the japser and send the report on the user requested format)
					//********************************************************************
                    if (strAction.equals("PRINT"))
                    {
                        JasperPrint jp      =null;
                        JRExporter exporter = null;
                        InputStream reportStream ;
                        hmparameter = (HashMap)hmResult.get("ACCDETAIL");
                        ServletOutputStream servletOutputStream = response.getOutputStream();
                       
                        
                        String fileExtEx = ".xls";
                        String fileName ="";
                        
                        String strRegionFilenm=(String)hmparameter.get("REGION");//
           				String strAccountFilenm=(String)hmparameter.get("NAME");//
           				String strAccountIdFilenm=(String)hmparameter.get("ID");//
        				
           				
           				String strDirs =strBaseDirToUpload + "\\" + strRegionFilenm+"\\"+ strAccountIdFilenm;
           			
           				//String  strFileNameToPopulate = strDirs +"\\" + strAccountIdFilenm+"_"+ strAccountFilenm+"_"+ GmCalenderOperations.getCurrentDate("yyyyMMdd");
           				//String excelFileName =strFileNameToPopulate+fileExtEx;
        				
           				File dir = new File(strDirs);
           				if (dir.exists()) {
           					File chkFile = new File(strDirs);
           					String strFileList[] = chkFile.list();
           					//log.debug("strFileList.length"+strFileList.length);
           						for(int j=0; j<strFileList.length; j++)
           						{
           							
           							fileName = strFileList[j];
           							if(fileName.endsWith(fileExtEx)){
           							fileName = strFileList[j];	
           							}
           						}

           				} 
           				String excelFileName = strDirs+"\\"+fileName ;
           				
           				
        				//String strAdId=(String)hmparameter.get("ID");
           				//String strAdName=(String)hmparameter.get("ADNAME");
        				//String strAdEmail=(String)hmparameter.get("EMAILID");
        				//String strAdPhoneNo=(String)hmparameter.get("PHONENO");
        				//String strAdtitle=(String)hmparameter.get("TITLE");
           				//String strAcAdd=(String)hmparameter.get("ACADD");
           				
           				// Here we have to create object for reportStream.
           				reportStream = new FileInputStream(excelFileName);
                        if (strOpt.equals("PriceIncrease"))
                        {
                            reportStream = getServletConfig().getServletContext().getResourceAsStream(strJasperPath + "/GmPrintAcctPriceIncrease.jasper");    
                        }else if (strOpt.equals("DTL"))
                        {
                            reportStream = getServletConfig().getServletContext().getResourceAsStream(strJasperPath + "/GmPrintAcctPriceDiscount.jasper");    
                        }
                        else if (strOpt.equals("ACC_PRICE_XLS"))
                        {	
                        	reportStream = new FileInputStream(excelFileName);

                            //reportStream = getServletConfig().getServletContext().getResourceAsStream(strJasperPath + "/GmPrintAcctPrice.jasper");
                        }
                        

                        //jp = JasperFillManager.fillReport(reportStream, hmparameter , new JRBeanCollectionDataSource(resultSet.getRows()));   

    					if (strDownloadType.equals("HTML") )	
                        {
                            jp.setPageHeight(970);
                            
                            exporter = new JRHtmlExporter();
                            exporter.setParameter( JRExporterParameter.JASPER_PRINT, jp );
                            exporter.setParameter( JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
                            
                            request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp );
                            exporter.setParameter( JRHtmlExporterParameter.IMAGES_MAP, new HashMap() );
                            
                            exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,request.getContextPath() + "/GmImageServlet?image=" );
                        }
                    
                        if (strDownloadType.equals("PDF") ) 
                        {
                            response.setHeader("Content-Disposition", "inline; filename=\"file.pdf\"");
                            response.setContentType("application/pdf");
                            
                            exporter = new JRPdfExporter();
                            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
                            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
                        }

                    
                        if (strDownloadType.equals("RTF") ) 
                        {
                            response.setHeader("Content-Disposition", "inline; filename=\"file.rtf\"");
                            response.setContentType("application/rtf");
                            
                            exporter = new JRRtfExporter();
                            
                            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
                            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
                        }
                    
                        if (strDownloadType.equals("XLS") ) 
                        {
                            response.setContentType("application/xls");
                            response.setHeader("Content-Disposition", "inline; filename="+fileName+"");
                            
                            int intCnt=0;
                            while((intCnt=reportStream.read())!=-1){
                            servletOutputStream.write(intCnt);
                            }
                            reportStream.close();
                            servletOutputStream.flush();
                            

                            
                            
                            //exporter = new JRXlsExporter();
                            
                            //exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
                            //exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
                        }
                    
                        //exporter.exportReport();
                        servletOutputStream.close();
                        return;
                        /* Below code are for email and currently not user 
                         * will be used in future email process coding
                        if (strDownloadType.equals("EMAIL") ) 
                        {
                            StringBuffer sbuffer = new StringBuffer();
                            
                            hmparameter.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
                           
                            jp = JasperFillManager.fillReport(reportStream, hmparameter
                                        , new JRBeanCollectionDataSource(resultSet.getRows()));   
                            
                            
                            exporter = new JRHtmlExporter();
                            exporter.setParameter( JRExporterParameter.JASPER_PRINT, jp );
                            exporter.setParameter( JRExporterParameter.OUTPUT_STRING_BUFFER, sbuffer);
    
                            request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp );
                            exporter.setParameter( JRHtmlExporterParameter.IMAGES_MAP, new HashMap() );
    
                            log.debug( " ContextPath URL "  + request.getContextPath() );
                            exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI,request.getContextPath() + "http://localhost/GmImageServlet?image=" );
                            
                            log.debug(" After Session Setting " );
                            exporter.exportReport();
                            
                            gmCommonClass.sendMail("GM System<GMSystem@globusmedical.com>",GmCommonClass.StringtoArray("Richard<richardk@globusmedical.com>",";")
                                    ,GmCommonClass.StringtoArray("",";"),"Account Pricing",sbuffer.toString());
                            return;
                            
                        }
                        if (strDownloadType.equals("EMAILPDF") ) 
                        {
    
                            log.debug(" Before Calling Email PDF " );
                            byte[] pdfByte ;
                            
                            jp = JasperFillManager.fillReport(reportStream, hmparameter, new JRBeanCollectionDataSource(resultSet.getRows()));
                            
                            
                            pdfByte = JasperExportManager.exportReportToPdf( jp);
                            
                            gmCommonClass.sendMail("GM System<GMSystem@globusmedical.com>",GmCommonClass.StringtoArray("Richard<richardk@globusmedical.com>",";")
                                    ,GmCommonClass.StringtoArray("",";"),"Account Pricing","See Attached document for Pricing info ",pdfByte );
                            
                            return;
                        }*/
                 } 

                 // When click Download to Excel button , it will be used following block.
                 if(strMode.equals("ACC_PRICE")){
                	 
                	 response.setContentType("application/vnd.ms-excel");
                	 response.setHeader("Content-disposition","attachment;filename=AccountPriceReport.xls");
                	 strDispatchTo = strSalesPath.concat("/GmAccountPriceReportExcel.jsp");
                 }
			}
				
			      hmAccDtl = gmCustSalesSetupBean.fetchRepAccountInfo(strAccId);
			      strAccCurrSymb = GmCommonClass.parseNull((String) hmAccDtl.get("ACC_CURR_SYMB"));
			    
			    request.setAttribute("ACC_CURR_SYMB", strAccCurrSymb);
				request.setAttribute("hAction",strAction);
				request.setAttribute("hAccId",strAccId);
				request.setAttribute("hProjectIds",strProjId);
				request.setAttribute("hmReturn",hmReturn);
				request.setAttribute("hOpt",strOpt);
				request.setAttribute("hQueryBy",strQueryBy);
				request.setAttribute("hPerIncrease",strIncreasePrice);
				request.setAttribute("CHKSELECTALL", strChkSelectAll);
				request.setAttribute("hPartNum", strPartNum);
				request.setAttribute("hLikeOption",strLikeOption);
				request.setAttribute("hAccNm",strAccNm);
				
				dispatch(strDispatchTo,request,response); 
		}// End of try
		catch (Exception e)
		{
			e.printStackTrace();
            log.error(e.getMessage());
            session.setAttribute("hAppErrorMessage",e.getMessage());
			strDispatchTo = strComnPath + "/GmError.jsp";
			gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmSalesConsignSearchServlet