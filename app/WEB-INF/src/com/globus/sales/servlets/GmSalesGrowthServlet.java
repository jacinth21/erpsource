/*****************************************************************************
 * File : GmSalesGrowthServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmTerritoryReportBean;

public class GmSalesGrowthServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = strSalesPath.concat("/GmSalesGrowthReport.jsp");

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      // checkSession(response, session);
      instantiate(request, response);
      String strFrom = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strOpt = "";

      String strCondition = null;
      String strAccessFilter = null;
      String strSalesFilter = null;
      String strFilter = "";
      String strConditionHist = "";

      HashMap hmFilter = new HashMap();
      HashMap hmSalesFilters = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmFromDate = new HashMap();
      HashMap hmFilterDetails = new HashMap();
      HashMap hmparam = new HashMap();
      ArrayList alReturn = new ArrayList();

      String strHeader = "";

      // Additional drilldown value
      String strSetNumber = "";
      String strDistributorID = "";
      String strAccountID = "";
      String strRepID = "";
      String strADID = "";
      String strTerritoryID = "";
      String strToMonth = "";
      String strToYear = "";
      String strVPID = "";
      String strRegnID = "";
      String strHPreYear = "";
      String strMonth = "";
      String strQuarter = "";
      String strYear = "";
      String strReportType = "";
      String strCompFilter = "";
      String strSessCompId = "";
      // Checks if the current session is valid, else redirecting to SessionExpiry page

      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;
      strOpt = request.getParameter("strSessOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
        // session.removeAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmCommonClass gmCommon = new GmCommonClass();
      GmSalesCompBean gmSales = new GmSalesCompBean(getGmDataStoreVO());

      GmTerritoryReportBean gmTerritoryBean = new GmTerritoryReportBean(getGmDataStoreVO());
      GmCommonBean gmCommBn = new GmCommonBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();



      String strId = request.getParameter("hId") == null ? "" : request.getParameter("hId");

      // Additional Filter
      strSetNumber = request.getParameter("hSetNumber");
      strDistributorID = request.getParameter("hDistributorID");
      strAccountID = request.getParameter("hAccountID");
      strRepID = request.getParameter("hRepID");
      strADID = request.getParameter("hAreaDirector");
      strTerritoryID = request.getParameter("hTerritory");
      strVPID = request.getParameter("hVPID");
      strRegnID = request.getParameter("hRegnID");
      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");
      strHPreYear = GmCommonClass.parseNull(request.getParameter("hPreYear"));
      strMonth = GmCommonClass.parseNull(request.getParameter("hMonth"));
      strQuarter = GmCommonClass.parseNull(request.getParameter("hQuarter"));
      strYear = GmCommonClass.parseNull(request.getParameter("hYear"));
      strReportType = GmCommonClass.parseNull(request.getParameter("hReportType"));
      strCompFilter = GmCommonClass.parseNull(request.getParameter("hCompFilter"));
      strSessCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
      strCompFilter =
          strCompFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "chCompFilter")) : strCompFilter;
      strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter;
     //PMT-33183 QUOTA PERFORMANCE
      
      if(strReportType.equalsIgnoreCase("Historical_HIDECOMPANY")){
    	 
    	  strReportType = "Historical";
    	  request.setAttribute("hideCompany", "Y");
      }

     
      
      // Get cookie only on onload for the first time and not on the Go button
      if (strToMonth == null) {
        strToMonth =
            strToMonth == null ? gmCookieManager.getCookieValue(request, "cQuotaToMonth")
                : strToMonth;
        strToYear =
            strToYear == null ? gmCookieManager.getCookieValue(request, "cQuotaToYear") : strToYear;
        strMonth =
            strMonth.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,
                "cMonthFl")) : strMonth;
        strQuarter =
            strQuarter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,
                "cQuarterFl")) : strQuarter;
        strYear =
            strYear.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,
                "cYearFl")) : strYear;
        strHPreYear =
            strHPreYear.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
                request, "cHPreYearFl")) : strHPreYear;
        strMonth = strMonth.equals("off") ? "" : "on";
        strQuarter = strQuarter.equals("off") ? "" : "on";
        strYear = strYear.equals("off") ? "" : "on";
        strHPreYear = strHPreYear.equals("") ? "off" : "on";
      }

      HashMap hmParam = new HashMap();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();


      // Additional filter / drilldown
      hmFilter.put("SetNumber", strSetNumber);
      hmFilter.put("DistributorID", strDistributorID);
      hmFilter.put("AccountID", strAccountID);
      hmFilter.put("RepID", strRepID);
      hmFilter.put("ADID", strADID);
      hmFilter.put("TerritoryID", strTerritoryID);
      hmFilter.put("VPID", strVPID);
      hmFilter.put("RegnID", strRegnID);
      hmFilter.put("ReportType", strReportType);
      
      
      hmFilter.put("ToMonth", strToMonth);
      hmFilter.put("ToYear", strToYear);
      hmFilter.put("COMPID", strCompFilter);
      log.debug("strToYear" + strToYear);

      // Currently divided by thousand is hardcorder and soon will be from the screen
      hmFilter.put("Divider", "1");

      request.setAttribute("strOverrideASSAccLvl",
          GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommBn.getSalesFilterLists(strAccessFilter);
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
      strCondition = gmSalesDispatchAction.getAccessCondition(request, response, true);

      // AccessCondition with t501 table
      strConditionHist = getAccessCondition(request, response, true);
      hmFilter.put("HistCondition", strConditionHist);

      hmFilter.put("Condition", strCondition);

      strSalesFilter = getSalesFilter(request, response);
      hmFilter.put("Filter", strSalesFilter);

      hmFilter.put("TYPE", strOpt);
      
      gmFramework.getRequestParams(request, response, session, hmFilter);

      if (strOpt.equals("TERR")) {
        hmReturn = gmTerritoryBean.fetchTerritoryPerformanceReport(hmFilter);
        strHeader = "- By Rep";
      }
      if (strOpt.equals("DIST")) {
        // hmReturn = gmSales.loadDistributorCompSales(hmFilter);
        hmReturn = gmTerritoryBean.fetchTerritoryPerformanceReport(hmFilter);
      
        strHeader = "- By Field Sales";
      } else if (strOpt.equals("ACCT")) {
        hmReturn = gmSales.loadAccountSalesGrowth(hmFilter);

        strHeader = "- By Account";
      } else if (strOpt.equals("SREP")) {
        hmReturn = gmSales.loadRepSalesGrowth(hmFilter);
        strHeader = "- By Spine Specialist";
      } else if (strOpt.equals("PGRP")) {
        hmReturn = gmSales.loadGroupSalesGrowth(hmFilter);
        strHeader = "- By Product Groups";
      } else if (strOpt.equals("PART")) {
        hmReturn = gmSales.loadPartSalesGrowth(hmFilter);
        strHeader = "- By Part";
      } else if (strOpt.equals("AD")) {
        // hmReturn = gmSales.loadADCompSales(hmFilter);
        hmReturn = gmTerritoryBean.fetchTerritoryPerformanceReport(hmFilter);
        strHeader = "- By AD";
      } else if (strOpt.equals("VP")) {
        // hmReturn = gmSales.loadVPCompSales(hmFilter);
        hmReturn = gmTerritoryBean.fetchTerritoryPerformanceReport(hmFilter);
        strHeader = "- By VP";
      }

      // To fetch the filter details to be displayed
      hmFilterDetails = gmSales.GetFilterDetails(hmFilter);

      // To get the detail query information
      alReturn = (ArrayList) hmReturn.get("Details");
      if (strOpt.equals("VP") || strOpt.equals("AD") || strOpt.equals("TERR")
          || strOpt.equals("DIST")) {
        hmParam.put("INCLUDEPREYEAR", strHPreYear);
        hmParam.put("MONTH", strMonth);
        hmParam.put("QUARTER", strQuarter);
        hmParam.put("YEAR", strYear);
        hmParam.put("CURRTYPE", GmCommonClass.parseNull((String) hmparam.get("CURRTYPE")));
        gmTerritoryBean.fliterQuotaPerformance(alReturn, hmParam);
      }
      
      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FDate");

      if (hmFromDate.get("FirstTime") != null) {
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");
      }

      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommBn.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommBn.getYearList());

      request.setAttribute("ALMONTHDROPDOWN", alMonthDropDown);
      request.setAttribute("ALYEARDROPDOWN", alYearDropDown);
     
      request.setAttribute("alReturn", alReturn);
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);

      request.setAttribute("hFilterDetails", hmFilterDetails);
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hHeader", strHeader);

      request.setAttribute("hPreYear", strHPreYear);
      request.setAttribute("hMonth", strMonth);
      request.setAttribute("hQuarter", strQuarter);
      request.setAttribute("hYear", strYear);
      request.setAttribute("hReportType", strReportType);
      
      
      // Set Cookies

      log.debug("Save COOKIE CHECK " + strMonth + "," + strQuarter + "," + strYear + ","
          + strHPreYear);
      strMonth = strMonth.equals("") ? "off" : strMonth;
      strQuarter = strQuarter.equals("") ? "off" : strQuarter;
      strYear = strYear.equals("") ? "off" : strYear;
      strHPreYear = strHPreYear.equals("off") ? "" : strHPreYear;

      gmCookieManager.setCookieValue(response, "cQuotaToMonth", strToMonth,
          GmCookieManager.MAX_AGE_8);
      gmCookieManager
          .setCookieValue(response, "cQuotaToYear", strToYear, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cMonthFl", strMonth);
      gmCookieManager.setCookieValue(response, "cQuarterFl", strQuarter);
      gmCookieManager.setCookieValue(response, "cYearFl", strYear);
      gmCookieManager.setCookieValue(response, "cHPreYearFl", strHPreYear);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSalesGrowthServlet
