/*****************************************************************************
 * File : GmSalesByProjectServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmSalesByProjectServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    String strJSPName = "";
    String strDistributorID = "";
    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";

    String strShowArrowFl = "";
    String strShowPercentFl = "";
    String strHideAmount = "";
    String strHeader = "";
    String strFilterCondition = "";

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));

      GmCommonClass gmCommon = new GmCommonClass();
      GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();
      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmFromDate = new HashMap();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();

      String strCondition = null;

      strFromMonth = request.getParameter("Cbo_FromMonth");
      strFromYear = request.getParameter("Cbo_FromYear");
      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");

      // Get cookie only on onload for the first time and not on the Go button
      if (strFromMonth == null) {
        strFromMonth =
            strFromMonth == null ? gmCookieManager.getCookieValue(request, "cFromMonth")
                : strFromMonth;
        strFromYear =
            strFromYear == null ? gmCookieManager.getCookieValue(request, "cFromYear")
                : strFromYear;
        strToMonth =
            strToMonth == null ? gmCookieManager.getCookieValue(request, "cToMonth") : strToMonth;
        strToYear =
            strToYear == null ? gmCookieManager.getCookieValue(request, "cToYear") : strToYear;
      }

      // System.out.println("**** strShowArrowFl ****"+ strShowArrowFl);
      strAction = (strAction.equals("")) ? "LoadProjectSales" : strAction;
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      HashMap hmSalesFilters = new HashMap();
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      // To get the Access Level information
      strCondition = getAccessCondition(request, response, true);

      gmFramework.getRequestParams(request, response, session, hmParam);
      String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));

      if (strAction.equals("LoadProjectSales")) {
        strHeader = "Sales By Project";
        hmReturn =
            gmSales.loadSalesByProject(strFromMonth, strFromYear, strToMonth, strToYear,
                strCondition, strCurrType);
        alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
        alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

        hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
        hmReturn.put("ALYEARDROPDOWN", alYearDropDown);
        request.setAttribute("hmReturn", hmReturn);
      }

      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FromDate");
      if (hmFromDate.get("FirstTime") != null) {
        // System.out.println("To get the date for the onload condition...");
        strFromMonth = (String) hmFromDate.get("FROMMONTH");
        strFromYear = (String) hmFromDate.get("FROMYEAR");
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");

        // System.out.println("hmFromDate" + hmFromDate);
      }

      request.setAttribute("Cbo_FromMonth", strFromMonth);
      request.setAttribute("Cbo_FromYear", strFromYear);
      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);

      strJSPName = "/GmProjectSalesReport.jsp";

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cFromMonth", strFromMonth,
          GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cFromYear", strFromYear, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToMonth", strToMonth, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToYear", strToYear, GmCookieManager.MAX_AGE_8);

      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmTerritoryServlet
