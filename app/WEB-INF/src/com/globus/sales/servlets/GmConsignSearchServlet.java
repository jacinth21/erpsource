/*****************************************************************************
 * File : GmConsignSearchServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesConsignReportBean;


public class GmConsignSearchServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(true);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptSeq"));
    String strDispatchTo = "";
    RowSetDynaClass resultSet = null;
    HashMap hmFilterDetails = new HashMap();
    GmSalesCompBean gmSalesComp = new GmSalesCompBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    strDispatchTo = strSalesPath.concat("/GmSalesConsignSearch.jsp");
    String strAction = request.getParameter("hAction");
    if (strAction == null) {
      strAction = (String) session.getAttribute("hAction");
    }
    strAction = (strAction == null) ? "Load" : strAction;

    String strOpt = request.getParameter("hOpt");
    if (strOpt == null) {
      strOpt = (String) session.getAttribute("strSessOpt");
    }
    strOpt = (strOpt == null) ? "" : strOpt;

    GmSalesConsignReportBean gmSales = new GmSalesConsignReportBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();

    ArrayList alReturn = new ArrayList();
    String strUserId = (String) session.getAttribute("strSessUserId");
    String strSetId = "";
    String strhSetId = "";
    String strPartNum = "";
    String strDistId = "";
    String strFrmDt = "";
    String strToDt = "";
    String strType = "";
    String strCategory = "";
    String strStatus = "";
    String strTransId = "";
    String strDummyTran = "";
    String strInHouseFl = "";
    String strAccessFilter = "";

    // To get the Access Level information from Materialized view
    // The filter condition should be applied for ICS Dept users alone.
    if (strDeptId.equals("ICS"))
      strAccessFilter = getAccessFilter(request, response);

    // To load values for dropdowns, multiselects
    hmReturn = gmSales.loadConsignSeachLists(strAccessFilter);

    if (strAction.equals("Load")) {
      request.setAttribute("hmReturn", hmReturn);
    } else if (strAction.equals("Reload")) {
      strFrmDt = GmCommonClass.parseNull(request.getParameter("Txt_FrmDt"));
      strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDt"));
      strhSetId = GmCommonClass.parseNull(request.getParameter("hSetIds"));
      strDistId = GmCommonClass.parseNull(request.getParameter("hDistIds"));
      strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));
      strCategory = GmCommonClass.parseZero(request.getParameter("Cbo_Category"));
      strStatus = GmCommonClass.parseZero(request.getParameter("Cbo_Status"));
      strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
      strTransId = GmCommonClass.parseNull(request.getParameter("Txt_TransId"));
      strDummyTran = GmCommonClass.parseNull(request.getParameter("hOpt"));
      strInHouseFl = GmCommonClass.parseNull(request.getParameter("Chk_InHouse"));

      if (strInHouseFl.equals("on")) {
        strDummyTran = "4112";
      } else {
        strDummyTran = "4110";
      }

      hmParam.put("DISTID", strDistId);
      hmParam.put("SETID", strhSetId);
      hmParam.put("FRMDT", strFrmDt);
      hmParam.put("TODT", strToDt);
      hmParam.put("TYPE", strType);
      hmParam.put("CATEGORY", strCategory);
      hmParam.put("STATUS", strStatus);
      hmParam.put("PNUM", strPartNum);
      hmParam.put("TRANSID", strTransId);
      hmParam.put("DUMMYTRAN", strDummyTran);

      resultSet = gmSales.reportSeachSalesConsign(hmParam);
      request.setAttribute("results", resultSet);
      hmParam.put("SETID", strhSetId);
      request.setAttribute("hmParam", hmParam);
    }

    gmFramework.getRequestParams(request, response, session, hmParam);
    // To fetch the filter details to be displayed (Additioan Filter details)
    hmFilterDetails = gmSalesComp.GetFilterDetails(hmParam);
    request.setAttribute("hFilterDetails", hmFilterDetails);
    request.setAttribute("hAction", strAction);
    request.setAttribute("hOpt", strOpt);
    request.setAttribute("hmReturn", hmReturn);
    dispatch(strDispatchTo, request, response);


  }// End of service method
}// End of GmConsignSearchServlet
