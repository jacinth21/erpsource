/*****************************************************************************
 * File : GmSalesGrowthServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmTerritoryReportBean;

public class GmQuotaTrackingServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = strSalesPath.concat("/GmTerrGrowthQuotaReport.jsp");
    String strFrom = "";
    String strUserId = (String) session.getAttribute("strSessUserId");
    String strOpt = "";

    String strAccessFilter = null;
    String strSalesFilter = null;
    String strFilter = "";

    HashMap hmFilter = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmFromDate = new HashMap();
    HashMap hmFilterDetails = new HashMap();
    HashMap hmparam = new HashMap();


    ArrayList alReturn = new ArrayList();
    ArrayList alMonthDropDown = new ArrayList();
    ArrayList alYearDropDown = new ArrayList();

    String strHeader = "";

    // Additional drilldown value
    String strSetNumber = "";
    String strDistributorID = "";
    String strAccountID = "";
    String strRepID = "";
    String strADID = "";
    String strTerritoryID = "";
    String strToMonth = "";
    String strToYear = "";
    String strRegnFilter = "";
    String strCondition = "";
    instantiate(request, response);
    GmCommonClass gmCommon = new GmCommonClass();
    GmCommonBean gmCommBn = new GmCommonBean(getGmDataStoreVO());
    GmSalesCompBean gmSales = new GmSalesCompBean(getGmDataStoreVO());
    GmTerritoryReportBean gmTerr = new GmTerritoryReportBean(getGmDataStoreVO());
    GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
    GmFramework gmFramework = new GmFramework();
    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page

      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;
      strOpt = request.getParameter("strSessOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
        // session.removeAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      String strId = request.getParameter("hId") == null ? "" : request.getParameter("hId");
      String strCompFilter = "";
      String strSessCompId = "";
      // Additional Filter
      strSetNumber = request.getParameter("hSetNumber");
      strDistributorID = request.getParameter("hDistributorID");
      strAccountID = request.getParameter("hAccountID");
      strRepID = request.getParameter("hRepID");
      strADID = request.getParameter("hAreaDirector");
      strTerritoryID = request.getParameter("hTerritory");

      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");
      // Get cookie only on onload for the first time and not on the Go button
      strToMonth =
          strToMonth == null ? gmCookieManager.getCookieValue(request, "cQuotaToMonth")
              : strToMonth;
      strToYear =
          strToYear == null ? gmCookieManager.getCookieValue(request, "cQuotaToYear") : strToYear;

      strRegnFilter = GmCommonClass.parseNull(request.getParameter("Cbo_RegnFilter"));

      strCompFilter = GmCommonClass.parseNull(request.getParameter("hCompFilter"));
      strSessCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
      strCompFilter =
          strCompFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "chCompFilter")) : strCompFilter;
      strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter;

      gmFramework.getRequestParams(request, response, session, hmparam);

      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommBn.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommBn.getYearList());

      // Additional filter / drilldown
      hmFilter.put("SetNumber", strSetNumber);
      hmFilter.put("DistributorID", strDistributorID);
      hmFilter.put("AccountID", strAccountID);
      hmFilter.put("RepID", strRepID);
      hmFilter.put("ADID", strADID);
      hmFilter.put("TerritoryID", strTerritoryID);
      hmFilter.put("ToMonth", strToMonth);
      hmFilter.put("ToYear", strToYear);
      hmFilter.put("CURRTYPE", GmCommonClass.parseNull((String) hmparam.get("CURRTYPE")));
      hmFilter.put("COMP_LANG_ID", GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID")));
      // Currently divided by thousand is hardcorder and soon will be from the screen
      hmFilter.put("Divider", "1");

      // To get the Access Level information
      request.setAttribute("strOverrideASSAccLvl",
          GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommBn.getSalesFilterLists(strAccessFilter);
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
      strCondition = gmSalesDispatchAction.getAccessCondition(request, response, true);

      hmFilter.put("Condition", strCondition);
      hmFilter.put("RCOND", strRegnFilter);

      strSalesFilter = getSalesFilter(request, response);
      hmFilter.put("Filter", strSalesFilter);


      // hmReturn = gmTerr.reportTerritorySalesToQuota(hmFilter);
      hmFilter.put("TYPE", "Summary");
      hmFilter.put("COMPID", strCompFilter);
      hmReturn = gmTerr.fetchTerritoryPerformanceReport(hmFilter);
      // hmReturn.put("REPORT",alReturn);


      // To get the detail query information
      alReturn = (ArrayList) hmReturn.get("Details");

      // To Filter the results
      HashMap hmParam = new HashMap();
      hmParam.put("SUMMARY", "on");
      gmTerr.fliterQuotaPerformance(alReturn, hmParam);
      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FDate");

      if (hmFromDate.get("FirstTime") != null) {
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");
      }

      hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
      hmReturn.put("ALYEARDROPDOWN", alYearDropDown);

      request.setAttribute("hmSalesFilters", hmSalesFilters);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);
      request.setAttribute("Cbo_RegnFilter", strRegnFilter);
      strDispatchTo = strSalesPath.concat("/GmTerrGrowthQuotaReport.jsp");

      /*
       * request.setAttribute("hmSalesFilters",hmSalesFilters);
       * request.setAttribute("Cbo_ToMonth",strToMonth );
       * request.setAttribute("Cbo_ToYear",strToYear );
       * 
       * request.setAttribute("hFilterDetails",hmFilterDetails);
       * request.setAttribute("hOpt",strOpt); request.setAttribute("hHeader",strHeader);
       */

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cQuotaToMonth", strToMonth,
          GmCookieManager.MAX_AGE_8);
      gmCookieManager
          .setCookieValue(response, "cQuotaToYear", strToYear, GmCookieManager.MAX_AGE_8);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSalesGrowthServlet
