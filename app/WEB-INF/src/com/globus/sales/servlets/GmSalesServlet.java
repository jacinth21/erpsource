package com.globus.sales.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;

public class GmSalesServlet extends GmServlet
{

    Logger           log         = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                                    // Class.
    protected String strComnPath = GmCommonClass.getString("GMCOMMON");
    protected String strOperPath = GmCommonClass.getString("GMSALES");

    protected String strSessType ="";
    protected String strSessTurn ="";
    protected String strSessFromMonth ="";
    protected String strSessFromYear ="";
    protected String strSessToMonth ="";
    protected String strSessToYear ="";
    protected String strSessSalesType ="";
    
    protected HttpSession session;    

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
    }

    public HashMap receiveCrossTabRequest(HttpServletRequest request, HttpServletResponse response)
    {
        session = request.getSession(true);
        HashMap hmParam = new HashMap();
        GmCookieManager gmCookieManager = new GmCookieManager();
        
        String strDistributorID = "";
        String strSetNumber = "";
        String strFromMonth = "";
        String strFromYear = "";
        String strToMonth = "";
        String strToYear = "";

        String strShowArrowFl = "";
        String strShowPercentFl = "";
        String strHideAmount = "";
        String strAccountID = "";
        String strRepID = "";
        String strPartNumber = "";
        String strADID = "";
        String strVPRegionID = "";
        String strTerritoryID = "";
        String strAccessFilter = "";
        String strCondition = "";
        String strExcelFileName = "";
        String strSetNumberIn = "";
        String strType = "";
        String strExcel = "";
        String strSetId = "";
        String strSetIdIn = "";
        String strPartNumberIn = "";
        String strAction = "";
        String strSortColumn = "";
        String strSortOrder = "";
        String strConType = "";
        String strPeriod = "";
        String strSetType = "";
        String strStatus = "";
        String strState = "";      
        String strRegion ="";
        
        // Consignment Overview Filter Variables

        String strTypeCon = "";
        String strSalesType = "";
        String strTurns = "";
        String strLoanerType = "";
        String strLoanerTypeDisable = "";
        String strShowAI = "";
        String strShowAIDisabled = "";
        String strHeaderPrefix = "Sales Consignment";
        String strBaselineName = "Baseline Sets";
        String strTurnsLabel =  "#Cases<BR> per M/Set";
        String strSystemOnlyCondition = "";

        strSessSalesType = GmCommonClass.parseNull((String)session.getAttribute("SAL_TYPE"));
        strSessFromMonth = GmCommonClass.parseNull((String)session.getAttribute("Cbo_FromMonth"));
        strSessFromYear = GmCommonClass.parseNull((String)session.getAttribute("Cbo_FromYear"));
        strSessToMonth = GmCommonClass.parseNull((String)session.getAttribute("Cbo_ToMonth"));
        strSessToYear = GmCommonClass.parseNull((String)session.getAttribute("Cbo_ToYear"));
        strSessType = GmCommonClass.parseNull((String)session.getAttribute("Cbo_TypeCon"));
        strSessTurn = GmCommonClass.parseNull((String)session.getAttribute("Cbo_Turns"));

        strFromMonth = request.getParameter("Cbo_FromMonth");
        strFromYear = request.getParameter("Cbo_FromYear");
        strToMonth = request.getParameter("Cbo_ToMonth");
        strToYear = request.getParameter("Cbo_ToYear");
        strShowArrowFl = GmCommonClass.parseNull(request.getParameter("Chk_ShowArrowFl"));
        strShowPercentFl = GmCommonClass.parseNull(request.getParameter("Chk_ShowPercentFl"));
        strHideAmount = GmCommonClass.parseNull(request.getParameter("Chk_HideAmountFl"));
        strAction = request.getParameter("hAction") == null ? (String) session.getAttribute("strSessOpt") : request
                        .getParameter("hAction");

        // Additional Filter
        strSetNumber = GmCommonClass.parseNull(request.getParameter("SetNumber"));
        strDistributorID = GmCommonClass.parseNull(request.getParameter("DistributorID"));
        strAccountID = GmCommonClass.parseNull(request.getParameter("hAccountID"));
        strRepID = GmCommonClass.parseNull(request.getParameter("hRepID"));
        strPartNumber = GmCommonClass.parseNull(request.getParameter("hPartNumber"));
        strADID = GmCommonClass.parseNull(request.getParameter("hAreaDirector"));
        strVPRegionID = GmCommonClass.parseNull(request.getParameter("hVpRegionID"));
        strTerritoryID = GmCommonClass.parseNull(request.getParameter("hTerritory"));
        strExcelFileName = GmCommonClass.parseNull(request.getParameter("hcboName"));
        
        strConType = GmCommonClass.parseNull((String) request.getParameter("Cbo_Con_Type"));
        strPeriod = GmCommonClass.parseNull((String) request.getParameter("Cbo_Period"));
        strSetType = GmCommonClass.parseNull((String) request.getParameter("Cbo_SetType"));
        strStatus = GmCommonClass.parseNull((String) request.getParameter("Cbo_Status"));
        
        
        
        strSetType = strSetType.equals("") ? "20160" : strSetType ;
        strPeriod = strPeriod.equals("") ? "50461" : strPeriod;
        strStatus = strStatus.equals("") ? "0" : strStatus;
        
        strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));

        // Default selected value
        strType = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
        
        strExcel = GmCommonClass.parseNull(request.getParameter("hExcel"));
        strType = (strType.equals("")) ? "1" : strType; // Setting combo box value

        // Consignment Overview Filter Default Values
       
        strTypeCon = GmCommonClass.parseNull((String)request.getParameter("Cbo_TypeCon"));
        strSalesType = (request.getParameter("Cbo_SalesType") == null) ? "50300" : ((String) request
                        .getParameter("Cbo_SalesType"));
        strTurns = GmCommonClass.parseNull((String)request.getParameter("Cbo_Turns"));
        
        strLoanerType = GmCommonClass.parseNull((String)request.getParameter("Cbo_LoanerType"));
        // If the request is coming from left menu (ie haction is not in request, then check the Show AI by default)
        
        strShowAI = GmCommonClass.getCheckBoxValue((String)request.getParameter("Chk_ShowAI"));

        
        if (strFromMonth == null)
        {
        	strFromMonth        = strFromMonth == null ? gmCookieManager.getCookieValue(request, "cFromMonth") : strFromMonth;
			strFromYear         = strFromYear == null ? gmCookieManager.getCookieValue(request, "cFromYear") : strFromYear;
			strToMonth          = strToMonth == null ? gmCookieManager.getCookieValue(request, "cToMonth") : strToMonth;
			strToYear           =strToYear == null ? gmCookieManager.getCookieValue(request, "cToYear") : strToYear;
			strTypeCon			= strTypeCon.equals("")? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cConsignType")) : strTypeCon;
			strTypeCon			= strTypeCon == null ? "50400":strTypeCon;			
			strTurns			= strTurns.equals("")? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cTurns")) : strTurns;
			
			strTurns			= strTurns.equals("")? "3":strTurns;				
			strShowAI			= strShowAI.equals("")? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cShowAI")) : strShowAI;
			if (strShowAI.equals("N"))
			{
				strShowAI = "";
			}
			else
			{
				strShowAI = "Y";
			}		
			
        }
        
        
        if (strSalesType.equals("50300")){
        	strLoanerTypeDisable = "disabled";
            strHeaderPrefix = "Sales Consignment";
        }
        else{
        	strShowAIDisabled = "disabled";
        	strShowAI = "";
        	strHeaderPrefix = "Loaner";
        }
        
        //if (strLoanerType.equals("70131")){
        if (strSalesType.equals("50301")){
        	if(strAction.equalsIgnoreCase("LOADGCONS")){
        		strBaselineName = "Loaner Usage";
        	}
        	else{
        		strBaselineName = "Loaner Usage";
        	//strTurnsLabel = "Utilization %";
        	}
        }
        else if (strSalesType.equals("50302")){
        	strBaselineName = "Total Sets";
        	//strTurnsLabel = "Utilization %";
        }
        /*if (strSalesType.equals("50302")){
        	strBaselineName = "Baseline Sets + (0.25 * <BR>LoanerUsage)";
        }*/
        
        log.debug (" strShowAI " + strShowAI + " plain value " +(String)request.getParameter("Chk_ShowAI") + " strOpt " +strOpt);
        strState = GmCommonClass.parseNull((String)request.getParameter("cbo_State"));
        strRegion = GmCommonClass.parseNull((String)request.getParameter("cbo_Region"));

        strType = strTypeCon.equals("50401") || strConType.equals("50441") ? "0" : strType;
        
        strSetNumberIn = GmCommonClass.getStringWithQuotes(strSetNumber); // replacing the , to ',' so that IN
                                                                            // operator works good for multiple Set Id
                                                                            // ingets
        log.debug("strSetNumberIn " + strSetNumberIn);

        strSetIdIn = GmCommonClass.getStringWithQuotes(strSetId); // replacing the , to ',' so that IN operator works
                                                                    // good for multiple Set Id ingets
        log.debug("strSetIdIn " + strSetIdIn);

        strPartNumberIn = GmCommonClass.getStringWithQuotes(strPartNumber); // replacing the , to ',' so that IN
                                                                            // operator works good for multiple Set Id
                                                                            // ingets

        if (!strSetIdIn.equals("") && !strSetNumberIn.equals(""))
        {
            strSetNumberIn = strSetNumberIn.concat("','").concat(strSetIdIn); // concatenate the setId and setNumber
                                                                                // strings
        }

        if (strSetNumberIn.equals(""))
        {
            strSetNumberIn = strSetIdIn;
        }

        log.debug("strSetNumberIn " + strSetNumberIn);

        // To get the Access Level information
        strCondition = getAccessCondition(request, response,true);

        // To get the Access Level information from Materialized view
        strAccessFilter = getAccessFilter(request, response);

        // To get sort values
        strSortColumn = request.getParameter("hSortColumn");
        strSortOrder = request.getParameter("hSortOrder");
        
        // If user is seeking System information and is not a drill down then drillcondn = "ONLYSYSTEM". 
        // This will be used to drive the totalsets and #cases/M/Set calculation
        if("LOADGCONS".equalsIgnoreCase(strAction) && strADID.equals("") && strVPRegionID.equals("") && strDistributorID.equals("") ){
        	strSystemOnlyCondition = "ONLYSYSTEM";
        }
        String strLoanerApproval = GmCommonClass.parseNull((String) request.getParameter("Loaner_app"));
        log.debug("strLoanerApproval==>"+strLoanerApproval);
        // Check if the tab is changed, if so then set the variables in session
       
        session.setAttribute("SAL_TYPE", strSalesType);
        log.debug(" strSalesType " +strSalesType + " strSessSalesType " + strSessSalesType + " strTypeCon " + strTypeCon + " strType " +strType);
        
        if(strSalesType.equals(strSessSalesType)){
        	session.setAttribute("Cbo_FromMonth", strFromMonth);
            session.setAttribute("Cbo_FromYear", strFromYear);
            session.setAttribute("Cbo_ToMonth", strToMonth);
            session.setAttribute("Cbo_ToYear", strToYear);
            session.setAttribute("Cbo_TypeCon",strTypeCon);
            session.setAttribute("Cbo_Turns",strTurns);
         }
        
        // These values wont be used by Bean. These would be just set in the request back to JSP
        hmParam.put("Chk_ShowArrowFl", strShowArrowFl);
        hmParam.put("Chk_ShowPercentFl", strShowPercentFl);
        hmParam.put("Chk_HideAmountFl", strHideAmount);
        hmParam.put("hExcel", strExcel);
        hmParam.put("hcboName", strExcelFileName);
        hmParam.put("hAction", strAction);

        // Adding the below values so that they can be set back in the request and read from the JSP
        hmParam.put("hSetNumber", strSetNumber);
        hmParam.put("hPartNumber", strPartNumber);
        hmParam.put("SetId", strSetId);

        // Add the filter to hashmap paramter
        hmParam.put("FromMonth", strFromMonth);
        hmParam.put("FromYear", strFromYear);
        hmParam.put("ToMonth", strToMonth);
        hmParam.put("ToYear", strToYear);
        hmParam.put("Condition", strCondition);
        hmParam.put("AccessFilter", strAccessFilter);
        hmParam.put("Type", strType);

        // Additional filter / drilldown
        hmParam.put("DistributorID", strDistributorID);
        hmParam.put("AccountID", strAccountID);
        hmParam.put("RepID", strRepID);
        hmParam.put("SetNumber", strSetNumberIn);
        hmParam.put("PartNumber", strPartNumberIn);
        hmParam.put("ADID", strADID);
        hmParam.put("VPID", strVPRegionID);
        hmParam.put("TerritoryID", strTerritoryID);
        // Consignment Filter
        hmParam.put("ConsignmentType", strTypeCon);
        hmParam.put("SalesType", strSalesType);
        hmParam.put("ConsignmentSummaryType",strConType);
        hmParam.put("ConsignmentPeriod",strPeriod);
        hmParam.put("ConsignmentSetType",strSetType);
        hmParam.put("Status",strStatus);
        hmParam.put("State",strState);
        hmParam.put("Region",strRegion);
        hmParam.put("TURNS",strTurns);
        hmParam.put("LOANERTYPE",strLoanerType);
        hmParam.put("LOANERTYPEDISABLED",strLoanerTypeDisable);
        hmParam.put("SHOWAI",strShowAI);
        hmParam.put("SHOWAIDISABLED",strShowAIDisabled);
        hmParam.put("HEADERPREFIX",strHeaderPrefix);
        hmParam.put("BASELINENAME",strBaselineName);
        hmParam.put("TURNSLABEL",strTurnsLabel);
        hmParam.put("SYSTEMONLYCONDITION",strSystemOnlyCondition);
        
        
        gmCookieManager.setCookieValue(response, "cFromMonth", strFromMonth, GmCookieManager.MAX_AGE_8);
		gmCookieManager.setCookieValue(response, "cFromYear", strFromYear, GmCookieManager.MAX_AGE_8);
		gmCookieManager.setCookieValue(response, "cToMonth", strToMonth, GmCookieManager.MAX_AGE_8);
		gmCookieManager.setCookieValue(response, "cToYear", strToYear, GmCookieManager.MAX_AGE_8);
		gmCookieManager.setCookieValue(response, "cConsignType", strTypeCon);
		gmCookieManager.setCookieValue(response, "cTurns", strTurns);
		if(strSalesType.equals("50300"))
		{
			strShowAI			= strShowAI.equals("")? "N":strShowAI;
			gmCookieManager.setCookieValue(response, "cShowAI", strShowAI);
		}
		
        return hmParam;
    }

    public void sendRequestAttributes(HttpServletRequest request, HashMap hmParam)
    {
        String strDistributorID = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
        String strSetNumber = GmCommonClass.parseNull((String) hmParam.get("hSetNumber"));
        String strFromMonth = GmCommonClass.parseNull((String) hmParam.get("FromMonth"));
        String strFromYear = GmCommonClass.parseNull((String) hmParam.get("FromYear"));
        String strToMonth = GmCommonClass.parseNull((String) hmParam.get("ToMonth"));
        String strToYear = GmCommonClass.parseNull((String) hmParam.get("ToYear"));

        String strAccountID = GmCommonClass.parseNull((String) hmParam.get("AccountID"));
        String strRepID = GmCommonClass.parseNull((String) hmParam.get("RepID"));
        String strADID = GmCommonClass.parseNull((String) hmParam.get("ADID"));
        String strVPRegionID = GmCommonClass.parseNull((String) hmParam.get("VPID"));
        String strTerritoryID = GmCommonClass.parseNull((String) hmParam.get("TerritoryID"));
        String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("AccessFilter"));
        String strCondition = GmCommonClass.parseNull((String) hmParam.get("Condition"));
        String strType = GmCommonClass.parseNull((String) hmParam.get("Type"));
        String strSetId = GmCommonClass.parseNull((String) hmParam.get("SetId"));
        String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("hPartNumber"));

        String strShowArrowFl = GmCommonClass.parseNull((String) hmParam.get("Chk_ShowArrowFl"));
        String strShowPercentFl = GmCommonClass.parseNull((String) hmParam.get("Chk_ShowPercentFl"));
        String strHideAmount = GmCommonClass.parseNull((String) hmParam.get("Chk_HideAmountFl"));
        String strExcelFileName = GmCommonClass.parseNull((String) hmParam.get("hcboName"));
        String strExcel = GmCommonClass.parseNull((String) hmParam.get("hExcel"));
        String strAction = GmCommonClass.parseNull((String) hmParam.get("hAction"));

        String strConsignmentType = (String) hmParam.get("ConsignmentType");
        String strSalesType = (String) hmParam.get("SalesType");
        String strConType = (String) hmParam.get("ConsignmentSummaryType");
        String strPeriod = (String) hmParam.get("ConsignmentPeriod");
        String strSetType = (String) hmParam.get("ConsignmentSetType");
        String strStatus = (String)hmParam.get("Status");
        String strState = (String)hmParam.get("State");
        String strRegion = (String)hmParam.get("Region");
        String strTurns = (String)hmParam.get("TURNS");
        String strLoanerType = (String)hmParam.get("LOANERTYPE");
        String strLoanerTypeDisable = (String)hmParam.get("LOANERTYPEDISABLED");
        String strShowAI = (String)hmParam.get("SHOWAI");
        String strShowAIDisabled = (String)hmParam.get("SHOWAIDISABLED");
        String strBaselineName = (String)hmParam.get("BASELINENAME");
        String strTurnsLabel = (String)hmParam.get("TURNSLABEL");
        String strSystemOnlyCondition = GmCommonClass.parseNull((String) hmParam.get("SYSTEMONLYCONDITION"));

        request.setAttribute("Cbo_FromMonth", strFromMonth);
        request.setAttribute("Cbo_FromYear", strFromYear);
        request.setAttribute("Cbo_ToMonth", strToMonth);
        request.setAttribute("Cbo_ToYear", strToYear);
        request.setAttribute("Chk_ShowArrowFl", strShowArrowFl);
        request.setAttribute("Chk_ShowPercentFl", strShowPercentFl);
        request.setAttribute("Chk_HideAmountFl", strHideAmount);
        request.setAttribute("Cbo_Type", strType);
        request.setAttribute("hAction", strAction);

        // Additional filter
        request.setAttribute("hAccountID", strAccountID);
        request.setAttribute("hDistributorID", strDistributorID);
        request.setAttribute("hSetNumber", strSetNumber);
        request.setAttribute("hRepID", strRepID);
        request.setAttribute("hPartNumber", strPartNumber);
        request.setAttribute("hTerritory", strTerritoryID);
        request.setAttribute("hAreaDirector", strADID);
        request.setAttribute("hVpRegionID", strVPRegionID);
        request.setAttribute("SetId", strSetId);

        request.setAttribute("strSalesConsign", strConsignmentType);
        request.setAttribute("strSaleType", strSalesType);
        request.setAttribute("strConType",strConType);
        request.setAttribute("strPeriod",strPeriod);
        request.setAttribute("strSetType",strSetType);
        request.setAttribute("strStatus",strStatus);
        request.setAttribute("strState",strState);
        request.setAttribute("strRegion",strRegion);
        request.setAttribute("strTurns",strTurns);
        request.setAttribute("Cbo_LoanerType",strLoanerType);
        request.setAttribute("LOANERTYPEDISABLED",strLoanerTypeDisable);
        request.setAttribute("SHOWAI",strShowAI);
        request.setAttribute("SHOWAIDISABLED",strShowAIDisabled);
        request.setAttribute("HBASELINENAME",strBaselineName);
        request.setAttribute("HTURNSLABEL",strTurnsLabel);
        request.setAttribute("PARAM", hmParam);
        request.setAttribute("CALCULATIONNOTES", getCalculationNotes(strSalesType, strSystemOnlyCondition));
    }

    public HashMap addDates(HashMap hmParam, int intDiff)
    {
        String strFromMonth = GmCommonClass.parseNull((String) hmParam.get("FromMonth"));
        HashMap hmDateVal = new HashMap();

        if (strFromMonth.equals(""))
        {
            hmDateVal = GmCommonClass.getMonthDiff(intDiff,false);
            hmParam.putAll(hmDateVal);
        }

        return hmParam;
    }
    
    public String getCalculationNotes(String strSalesType, String strSystemOnlyCondition){
    	
    	StringBuffer sbCalculationNotes = new StringBuffer();
    	
    	sbCalculationNotes.append("All sales $ in '000s <br>");
    	if("50300".equals(strSalesType)){
    		sbCalculationNotes.append("<b>AI : </b>Additional Inventory <br> ");
    		sbCalculationNotes.append("<b>AAS : </b>Additonal Available Sets <br> ");
    		sbCalculationNotes.append("<b>STD : </b>Additional Inventory from STanDard sets <br> ");
    		sbCalculationNotes.append("<b>AAI : </b>Additional Available Inventory <br> ");
    		sbCalculationNotes.append("<b> *  : </b>Turns mapping with associated system <br> ");
    		
        	return sbCalculationNotes.toString();
    	}
    	else if("50301".equals(strSalesType)){
    		sbCalculationNotes.append("<b>#cases per M / Set </B>= ");
    		if("ONLYSYSTEM".equalsIgnoreCase(strSystemOnlyCondition)){
    			sbCalculationNotes.append("(#cases per month) / Loaner Sets <br> ");
    		}
    		else{
    			sbCalculationNotes.append("(#cases per month) / (Usage / ( 2 * (Period/30)) ) <br> ");
    		}
   		}
    	else if("50302".equals(strSalesType)){
    		sbCalculationNotes.append("<b>Total Sets </B>= ");
    		if("ONLYSYSTEM".equalsIgnoreCase(strSystemOnlyCondition)){
    			sbCalculationNotes.append("Consign Sets + Loaner Sets <br> ");
    		}
    		else{
    			sbCalculationNotes.append("Consign Sets + (Loaner Usage / ( 2 * (Period/30)) ) <br> ");
    		}
    			sbCalculationNotes.append("<b>#cases per M / Set </B>= (#cases per month) / Total Sets <br> ");
   		}
		sbCalculationNotes.append("<b>#Loaner cases in period </B>= (#Loaner case per month) * (Period/30) <br> ");
		sbCalculationNotes.append("<b>Utilization % </B>= Percentage( #Loaner cases in period / Loaner Usage ) <br> ");
    	
    	return sbCalculationNotes.toString();
    }

}
