/*****************************************************************************
 * File			 : GmGroupSetMaintServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;
import com.globus.common.beans.*;
import com.globus.prodmgmnt.beans.*;
import com.globus.common.servlets.*;
import com.globus.sales.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmGroupSetMaintServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strSalesPath = GmCommonClass.getString("GMSALES");
		String strDispatchTo = strSalesPath.concat("/GmGroupSetMap.jsp");
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;

				String strOpt = request.getParameter("hOpt");
				if (strOpt == null)
				{
					strOpt = (String)session.getAttribute("strSessOpt");
				}
				strOpt = (strOpt == null)?"":strOpt;

				GmProjectBean gmProj = new GmProjectBean();
				GmSalesConsignReportBean gmSales = new GmSalesConsignReportBean();
				GmSalesConsignTransBean gmSalesTrans = new GmSalesConsignTransBean();
				
				HashMap hmReturn = new HashMap();
				HashMap hmResult = new HashMap();
				HashMap hmSave = new HashMap();
				ArrayList alReturn = new ArrayList();
				String strUserId = 	(String)session.getAttribute("strSessUserId");
				String strInputString = "";
				String strSetId = "";
				String strBeanMsg = "";
				
				if (strOpt.equals("LINK"))
				{
					alReturn = gmProj.loadSetMap("1600");
				}
				hmReturn.put("SETMASTERLIST",alReturn);
				
				if (strAction.equals("Load"))
				{
					request.setAttribute("hmReturn",hmReturn);
				}
				else if (strAction.equals("Go") || strAction.equals("Save"))
				{
					strSetId = request.getParameter("Cbo_Set")==null?"":request.getParameter("Cbo_Set");
					if (strAction.equals("Save"))
					{
						strSetId = request.getParameter("hSetId")==null?"":request.getParameter("hSetId");
						strInputString = request.getParameter("inputString")== null ?"":request.getParameter("inputString");
						strBeanMsg = gmSalesTrans.saveGroupSetMap(strSetId,strInputString,strUserId);
					}
					hmResult = gmSales.viewGroupSetMap(strSetId);
					hmReturn.put("MAPPEDLIST",hmResult.get("MAPPED"));
					hmReturn.put("UNMAPPEDLIST",hmResult.get("UNMAPPED"));
					request.setAttribute("hmReturn",hmReturn);
					request.setAttribute("hSetId",strSetId);
					request.setAttribute("hAction","Go");
				}		

				request.setAttribute("hOpt",strOpt);				
				dispatch(strDispatchTo,request,response);
		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmGroupSetMaintServlet