/*****************************************************************************
 * File : GmSalesVirtualActualServlet Desc : Servlet to serve reports for Sales Virtual Part / Set
 * Author : Richard Version /Date : 1.0 / Jan 3 2007 Copyright : Globus Medical Inc
 * 
 * Change History Version Date Description 1.1 Jan 31 ' 07 Changes for adding One Part Search
 * 
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesConsignReportBean;


public class GmSalesVirtualActualServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // Common variable
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";

    // variable used by the servlet
    RowSetDynaClass resultSet = null;
    HashMap hmFilterDetails = new HashMap();
    GmSalesCompBean gmSalesComp = new GmSalesCompBean(getGmDataStoreVO());
    HashMap hmDropDownValues = new HashMap();



    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    strDispatchTo = strSalesPath.concat("/GmSalesVirtualActual.jsp");

    String strAction = request.getParameter("hAction");
    if (strAction == null) {
      strAction = (String) session.getAttribute("hAction");
    }
    strAction = (strAction == null) ? "Load" : strAction;

    String strOpt = request.getParameter("hOpt");
    if (strOpt == null) {
      strOpt = (String) session.getAttribute("strSessOpt");
    }
    strOpt = (strOpt == null) ? "" : strOpt;

    log.debug(" Action is " + strAction + " Opt is " + strOpt);

    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    GmSalesConsignReportBean gmSales = new GmSalesConsignReportBean(getGmDataStoreVO());
    GmCommonBean gmCommon = new GmCommonBean(getGmDataStoreVO());
    GmFramework gmFramework = new GmFramework();

    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alLikeOptions = new ArrayList();
    String strUserId = (String) session.getAttribute("strSessUserId");
    String strSetId = "";
    String strhSetId = "";
    String strhPartId = "";
    // String strType = "";
    String strPartNum = "";
    String strSearch = "";
    HashMap hmValues = null;
    // HashMap hmParam =
    gmFramework.getRequestParams(request, response, session, hmParam);
    // To get the Access Level information from Materialized view
    String strAccessFilter = getAccessFilter(request, response);
    String strId = GmCommonClass.parseNull(request.getParameter("Cbo_Id"));
    if (strOpt.equals("BYCDISTSALES") || strOpt.equals("BYCDIST")) {
      if (strAction.equals("Reload")) {
        hmValues = new HashMap();

        String strSearchSet = request.getParameter("Txt_Setname");
        String strSystem = request.getParameter("Cbo_System");
        String strHierarchy = request.getParameter("Cbo_Hierarchy");
        String strCategory = request.getParameter("Cbo_Category");
        String strType = request.getParameter("Cbo_Type");

        hmParam.put("SYSTEM", strSystem);
        hmParam.put("HIERARCHY", strHierarchy);
        hmParam.put("CATEGORY", strCategory);
        hmParam.put("TYPE", strType);
        request.setAttribute("hmParam", hmParam);
        // hmValues.put("SETTYPE",strType);
        // hmValues.put("SETSTATUS",strStatus);
      }
    }


    if (strOpt.equals("BYDIST") || strOpt.equals("BYCDIST") || strOpt.equals("BYCDISTSALES")) {

      HashMap hmFilter = new HashMap();

      hmFilter.put("CONDITION", strAccessFilter);
      hmFilter.put("FLTR_DIST_BY_COMP", "YES");// Filter Distributor Based on company

      alReturn = gmCommon.getSalesFilterDist(hmFilter);
      strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
      alLikeOptions = GmCommonClass.getCodeList("LIKES");
      strSetId = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
      log.debug(" search value is " + strSearch);

      hmParam.put("DistributorID", strId);
      hmParam.put("Cbo_Search", strSearch);
      hmParam.put("SetId", strSetId);
      hmParam.put("AccessFilter", strAccessFilter);
      resultSet = gmSales.reportVirtualvsActualByDist(hmParam);
      hmReturn.put("LIKEOPTIONS", alLikeOptions);
      // log.debug(" resultSet values are  "+resultSet.getRows().toString()) ;

      hmDropDownValues = gmProj.fetchSetMappingList();

      request.setAttribute("HMDROPDOWN", hmDropDownValues);

    } else if (strOpt.equals("BYSET") || strOpt.equals("BYCSET") || strOpt.equals("BYCSETSALES")) {
      alReturn = gmProj.loadSetMap("1601");

      hmParam.put("SetNumber", strId);
      hmParam.put("AccessFilter", strAccessFilter);
      resultSet = gmSales.reportVirtualvsActualBySet(hmParam);
    }

    else if (strOpt.equals("BYPART")) {
      alReturn = gmProj.loadSetMap("1601");
      hmParam.put("SetNumber", strId);

      strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
      if (!strPartNum.equals("")) {
        resultSet = gmSales.reportVirtualvsActualByOnePart(strId, strPartNum);
      }
      request.setAttribute("hPartNum", strPartNum);
    }

    hmReturn.put("LIST", alReturn);

    request.setAttribute("results", resultSet);
    request.setAttribute("hmReturn", hmReturn);
    request.setAttribute("hOpt", strOpt);

    dispatch(strDispatchTo, request, response);

  }// End of service method
}// End of GmSalesConsignSearchServlet
