/*****************************************************************************
 * File : GmSalesYTDReportServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmMonthlyDashboardServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    String strJSPName = "";
    String strDistributorID = "";
    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";

    String strShowArrowFl = "";
    String strShowPercentFl = "";
    String strHideAmount = "";
    String strHeader = "";
    String strFilterCondition = "";

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));

      GmCommonClass gmCommon = new GmCommonClass();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();
      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmFromDate = new HashMap();
      HashMap hmTempParam = new HashMap();
      HashMap hmCharts = new HashMap();

      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();
      ArrayList alColorCodes = new ArrayList();
      String strCondition = null;


      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");

      strAction = (strAction.equals("")) ? "LoadStateSales" : strAction;
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      HashMap hmSalesFilters = new HashMap();
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      // To get the Access Level information
      strCondition = getAccessCondition(request, response, true);

      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

      gmFramework.getRequestParams(request, response, session, hmParam);
      String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
      if (strAction.equals("LoadStateSales")) {
        strHeader = "Sales By Sate";
        hmReturn = gmSales.loadMonthlySales(strToMonth, strToYear, strCondition, strCurrType);
        hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
        hmReturn.put("ALYEARDROPDOWN", alYearDropDown);
        request.setAttribute("hmReturn", hmReturn);

        // getting color codes and legend count from rules
        String strColorCodes =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("ALLCOLORS", "DSALES"));
        String strOthersCode =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("OTHERSCOLOR", "DSALES"));
        String strLegendsCnt =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("LEGENDCOUNT", "DSALES"));
        // getting values from string array and adding to ArrayList
        String strCodes[] = strColorCodes.split(",");
        alColorCodes = new ArrayList(Arrays.asList(strCodes));


        hmTempParam.put("TOPAGENT", hmReturn.get("TOPAGENT"));
        hmTempParam.put("TOPACCOUNT", hmReturn.get("TOPACCOUNT"));
        hmTempParam.put("MONTHLYSALES", hmReturn.get("MONTHLYSALES"));
        hmTempParam.put("PROJECTSALES", hmReturn.get("PROJECTSALES"));
        hmTempParam.put("WORKING_DAYS", hmReturn.get("WORKING_DAYS"));
        hmTempParam.put("FORMAT", getGmDataStoreVO().getCmpdfmt());
        hmTempParam.put("APPLNCURRSIGN",
            GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb")));
        hmTempParam.put("SESSCURRFMT",
            GmCommonClass.parseNull((String) session.getAttribute("strSessApplCurrFmt")));



        String strTopDistJSON = gmSales.generateOutPut(hmTempParam, "GmMonthlyDashTopDistChart.vm");
        String strTopAcctJSON = gmSales.generateOutPut(hmTempParam, "GmMonthlyDashTopAcctChart.vm");
        String strDailySalesJSON =
            gmSales.generateOutPut(hmTempParam, "GmMonthlyDashDailyChart.vm");

        hmTempParam.put("ALLCOLORS", alColorCodes);
        hmTempParam.put("OTHERSCOLOR", strOthersCode);
        hmTempParam.put("LEGENDCOUNT", Integer.parseInt(strLegendsCnt));

        String strProductJSON = gmSales.generateOutPut(hmTempParam, "GmMonthlyDashProductChart.vm");
        log.debug(strTopDistJSON);
        log.debug(strProductJSON);
        hmCharts.put("TOPDISTJSON", strTopDistJSON);
        hmCharts.put("TOPACCTJSON", strTopAcctJSON);
        hmCharts.put("DAILYSALESJSON", strDailySalesJSON);
        hmCharts.put("PRODUCTJSON", strProductJSON);

        request.setAttribute("HMCHARTS", hmCharts);
      }



      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FromDate");
      if (hmFromDate.get("FirstTime") != null) {
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");
      }

      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);

      strJSPName = "/GmMonthlyDashBoard.jsp";
      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmTerritoryServlet
