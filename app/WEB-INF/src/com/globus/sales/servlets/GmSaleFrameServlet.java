/*****************************************************************************
 * File			 : GmSaleFrameServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;
import com.globus.common.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

public class GmSaleFrameServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);

		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
		String strRootPath = GmCommonClass.getString("GMROOT");
		String strDispatchTo = "";
		String strPageToLoad = "";
		String strSubMenu = "";
		String strFrom = "";
		String strId = "";
		String strPO = "";
		String strType = "";

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				strFrom = request.getParameter("hFrom");

				if (strFrom != null && strFrom.equals("MonthDashboard"))
				{
					strId = request.getParameter("hId");
					strType = request.getParameter("hType");
					session.setAttribute("strSessDailyDate",strId);
					session.setAttribute("hType",strType);
				}

				strPageToLoad = request.getParameter("hPgToLoad");
				strSubMenu = request.getParameter("hSubMenu");

				strPageToLoad = (strPageToLoad == null)?"Blank":strPageToLoad;
				strSubMenu = (strSubMenu == null)?"Home":strSubMenu;
				
				session.setAttribute("strSessPgToLoad",strPageToLoad);
				session.setAttribute("strSessSubMenu",strSubMenu);

				gotoPage(strRootPath.concat("/GmSales.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of  GmSaleFrameServlet