package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProdReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmBaselineValueBean;

public class GmSetBaselineValueServlet extends GmServlet {

   /* STRBASELINECASETYPE = "20175";
      STRORGLISTTURNSTYPE = "20176";
      STRORGBASELINECASETYPE = "20177";
      STRORGCOGSBASELINECASETYPE = "20178"; */
    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {        
        HttpSession session = request.getSession(false);
        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strProdPath = GmCommonClass.getString("GMSALES");
        String strDispatchTo = "/GmSetBaselineValue.jsp";
        String strBaselineType="";
        String strTitle="";

        try
        {
            checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
            
                log.debug("Entering Service Method");
                String strUserId =  (String)session.getAttribute("strSessUserId");
                String strAction = request.getParameter("hAction");
                ArrayList alBaselineParameters = new ArrayList();      
                GmBaselineValueBean gmBaselineValueBean = new GmBaselineValueBean();
                
                if (strAction == null)
                {
                    strAction = (String)session.getAttribute("hAction");
                }
                strAction = (strAction == null)?"Load":strAction;
                
                String strOpt = request.getParameter("hOpt");
                if (strOpt == null)
                {
                    strOpt = (String)session.getAttribute("strSessOpt");
                }
                strOpt = (strOpt == null)?"":strOpt;
                
                log.debug(" opt is " + strOpt);
                // strOpt got from LeftMenu is set as the baseline type
                strBaselineType = strOpt;
                // Title of the page is set in the C901_CODE_NM
                strTitle = GmCommonClass.getCodeName(strBaselineType);

                if(strAction.equals("Save")) {
                     String strBaselineParameters = GmCommonClass.parseNull(request.getParameter("hBaselineParameter"));
                     gmBaselineValueBean.saveBaselineValue(strBaselineParameters,strBaselineType,strUserId);
                }
                alBaselineParameters = gmBaselineValueBean.loadBaselineValue(strBaselineType);
                request.setAttribute("alBaselineParameters",alBaselineParameters);
                request.setAttribute("strOpt",strOpt);
                request.setAttribute("strTitle",strTitle);
                
                log.debug("Exiting Service Method");
                dispatch(strProdPath.concat(strDispatchTo),request,response);

        } //End of try
        catch (Exception e)
        {
                session.setAttribute("hAppErrorMessage",e.getMessage());
                strDispatchTo = strComnPath + "/GmError.jsp";
                gotoPage(strDispatchTo,request,response);
        }// End of catch

}
}
