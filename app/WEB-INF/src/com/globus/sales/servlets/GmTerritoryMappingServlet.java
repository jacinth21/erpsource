/*****************************************************************************
 * File			 : GmSetYTDReportServlet
 * Desc		 	 : This Servlet will be used for all Sales YTD Report
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import com.globus.common.beans.*;
import com.globus.common.util.GmCookieManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;

import java.io.*;
import java.util.*;

public class GmTerritoryMappingServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strOperPath = GmCommonClass.getString("GMSALES");
		String strDispatchTo = "";
		String strJSPName = "";
		String strDistributorID = "";
		String strFromMonth = "";
		String strFromYear = "";
		String strToMonth = "";
		String strToYear = "";
		
		String strShowArrowFl = "";
		String strShowPercentFl = "";
		String strHideAmount = "";
		String strHeader = "";

		// Get Settings from cookie
		GmCookieManager gmCookieManager = new GmCookieManager();
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			String strAction = request.getParameter("hAction");
			
			GmCommonClass gmCommon = new GmCommonClass();
			GmCommonBean gmComBean = new GmCommonBean();
			GmTerritoryReportBean gmTerritory = new GmTerritoryReportBean();
			GmCustomerBean gmCustomer = new GmCustomerBean();
			GmSalesBean gmSalesCombo = new GmSalesBean();
			GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
			GmCommonBean gmCommonBean = new GmCommonBean();
			GmFramework gmFramework = new GmFramework();
			
			HashMap hmReturn = new HashMap();
			HashMap hmMap = new HashMap();
			HashMap hmFromDate = new HashMap();
			HashMap hmComonValue = new HashMap();
			
			String strCondition = "";
			String strFilterCondition = "";
			String strCompFilter    ="";
			String strSessCompId = "";
			
			String strId = "";
			String strExcelFileName = "";
			ArrayList alReturn = new ArrayList();	// Value used to fill the combo box value
			ArrayList alDistList = new ArrayList();
			ArrayList alMonthDropDown = new ArrayList();
			ArrayList alYearDropDown = new ArrayList();
			
			HashMap hmSalesFilters 	= new HashMap();
			
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("strSessOpt");
			}
			
			strFromMonth = request.getParameter("Cbo_FromMonth");
			strFromYear = request.getParameter("Cbo_FromYear");
			strToMonth = request.getParameter("Cbo_ToMonth");
			strToYear = request.getParameter("Cbo_ToYear");
			strShowArrowFl =  request.getParameter("Chk_ShowArrowFl");
			strShowPercentFl = request.getParameter("Chk_ShowPercentFl"); 
			strHideAmount = request.getParameter("Chk_HideAmountFl");

			// Default selected value
			String strID = gmCommon.parseZero(request.getParameter("Cbo_Name")) ;
			String strType = request.getParameter("Cbo_Type");
			String strExcel = request.getParameter("hExcel");
			
			//The below code is added to make the alge quota to 0 for Globus rep and vice versa.
			strCompFilter = GmCommonClass.parseNull((String)request.getParameter("hCompFilter"));
			strSessCompId =	GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
			strCompFilter = strCompFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "chCompFilter")) : strCompFilter;
			strCompFilter = strCompFilter.equals("") ? strSessCompId :strCompFilter;
			
			// Get cookie only on onload for the first time and not on the Go button 
			if (strFromMonth == null) 
			{ 
				strFromMonth        = strFromMonth == null ? gmCookieManager.getCookieValue(request, "cFromMonth") : strFromMonth;
				strFromYear         = strFromYear == null ? gmCookieManager.getCookieValue(request, "cFromYear") : strFromYear;
				strToMonth          = strToMonth == null ? gmCookieManager.getCookieValue(request, "cToMonth") : strToMonth;
				strToYear           = strToYear == null ? gmCookieManager.getCookieValue(request, "cToYear") : strToYear;
				strShowPercentFl    = strShowPercentFl == null ? gmCookieManager.getCookieValue(request, "cShowPercentFl") : strShowPercentFl;
				strShowArrowFl      = strShowArrowFl == null ? gmCookieManager.getCookieValue(request, "cShowArrowFl") : strShowArrowFl;
				strHideAmount       = strHideAmount == null ? gmCookieManager.getCookieValue(request, "cHideAmount") : strHideAmount;
			}
			
			strType = (strType == null)?"1":strType; // Setting combo box value
			strAction = (strAction == null)?"LoadSet":strAction;
			
			request.setAttribute("strOverrideASSAccLvl",GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
			//To get the Access Level information 
			strFilterCondition =  gmSalesDispatchAction.getAccessFilter(request,response,false);
			hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
			strFilterCondition =  gmSalesDispatchAction.getAccessFilter(request,response, true);
			strCondition =  gmSalesDispatchAction.getAccessCondition(request, response, true);
			
			strExcelFileName = request.getParameter("hcboName");
			
			// Add the filter to  hashmap paramter 
			hmMap.put("FromMonth",strFromMonth);
			hmMap.put("FromYear",strFromYear);
			hmMap.put("ToMonth",strToMonth);
			hmMap.put("ToYear", strToYear) ;
			hmMap.put("Condition",strCondition) ;
			hmMap.put("Action",strAction) ;
			hmMap.put("COMPID", strCompFilter) ;
			if (strAction.equals("LoadQuota") || strAction.equals("LoadQuotaVP"))
			{
				strHeader = "Territory Mapping ";
				gmFramework.getRequestParams(request, response, session, hmMap);
				hmReturn = gmTerritory.reportTerritoryMapping(hmMap);
			}
			
			hmComonValue.put("PROJECTS",alReturn);
			request.setAttribute("hmComonValue",hmComonValue);
			
			// To fetch the value for the unload condition 
			hmFromDate = (HashMap)hmReturn.get("FromDate");
			if (hmFromDate.get("FirstTime") != null)
			{
				//System.out.println("To get the date for the onload condition...");
				strFromMonth = (String)hmFromDate.get("FROMMONTH");
				strFromYear  = (String)hmFromDate.get("FROMYEAR");
				strToMonth 	 = (String)hmFromDate.get("TOMONTH");
				strToYear 	 = (String)hmFromDate.get("TOYEAR");
			
				//System.out.println("hmFromDate" + hmFromDate);
			}

			alMonthDropDown = GmCommonClass.parseNullArrayList(gmComBean.getMonthList());
			alYearDropDown = GmCommonClass.parseNullArrayList(gmComBean.getYearList());

			hmReturn.put("ALMONTHDROPDOWN",alMonthDropDown );
			hmReturn.put("ALYEARDROPDOWN",alYearDropDown );
			
				
			request.setAttribute("hmSalesFilters",hmSalesFilters);

			request.setAttribute("hAction",strAction);
			request.setAttribute("strHeader" ,strHeader);
			request.setAttribute("Cbo_FromMonth",strFromMonth);
			request.setAttribute("Cbo_FromYear",strFromYear );
			request.setAttribute("Cbo_ToMonth",strToMonth );
			request.setAttribute("Cbo_ToYear",strToYear );
			request.setAttribute("hmReturn",hmReturn);
			request.setAttribute("Chk_ShowArrowFl", strShowArrowFl);
			request.setAttribute("Chk_ShowPercentFl",strShowPercentFl);
			request.setAttribute("Chk_HideAmountFl",strHideAmount);
			request.setAttribute("Cbo_Name",strID);
			request.setAttribute("Cbo_Type",strType);

			if (strExcel == null){
				strJSPName= "/GmTerritoryMapping.jsp";
			} else {
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-disposition","attachment;filename=" + strExcelFileName +".xls");						
				strJSPName = "/GmReportYTDExcel.jsp";
			}

			// Set Cookies
			gmCookieManager.setCookieValue(response, "cFromMonth", strFromMonth, GmCookieManager.MAX_AGE_8);
			gmCookieManager.setCookieValue(response, "cFromYear", strFromYear, GmCookieManager.MAX_AGE_8);
			gmCookieManager.setCookieValue(response, "cToMonth", strToMonth, GmCookieManager.MAX_AGE_8);
			gmCookieManager.setCookieValue(response, "cToYear", strToYear, GmCookieManager.MAX_AGE_8);
			gmCookieManager.setCookieValue(response, "cShowPercentFl", strShowPercentFl);
			gmCookieManager.setCookieValue(response, "cShowArrowFl", strShowArrowFl);
			gmCookieManager.setCookieValue(response, "cHideAmount", strHideAmount);
			
			dispatch(strOperPath.concat(strJSPName),request,response);
		}// End of try
		catch (Exception e)
		{
			 e.printStackTrace();
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmTerritoryServlet