/*****************************************************************************
 * File : GmSalesConsignSearchServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesConsignReportBean;


public class GmSalesConsignSearchServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strSalesPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    RowSetDynaClass resultSet = null;
    HashMap hmFilterDetails = new HashMap();
    GmSalesCompBean gmSalesComp = new GmSalesCompBean(getGmDataStoreVO());

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      strDispatchTo = strSalesPath.concat("/GmSalesConsignSearch.jsp");

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmSalesConsignReportBean gmSales = new GmSalesConsignReportBean(getGmDataStoreVO());
      GmCommonBean gmCommon = new GmCommonBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strSetId = "";
      String strhSetId = "";
      String strhPartId = "";
      String strDistId = "";
      String strFrmDt = "";
      String strToDt = "";
      String strType = "";

      // To get the Access Level information from Materialized view
      String strAccessFilter = getAccessFilter(request, response);

      alReturn = gmProj.loadSetMap("1601");
      hmReturn.put("SETMASTERLIST", alReturn);
      alReturn = gmCommon.getSalesFilterDist(strAccessFilter);
      hmReturn.put("DISTRIBUTORLIST", alReturn);

      hmReturn = gmSales.loadConsignSeachLists(strAccessFilter); // copied from
                                                                 // GmConsignSearchServlet

      strDistId = GmCommonClass.parseNull(request.getParameter("Cbo_DistId"));
      strhSetId = GmCommonClass.parseNull(request.getParameter("hSetIds"));
      strhPartId = GmCommonClass.parseNull(request.getParameter("hPartID"));
      strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));

      log.debug("******* strOpt *******" + strOpt);

      if (strAction.equals("Load")) {
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("Reload") && strOpt.equals("BYSET")) {

        strSetId = strhSetId.replaceAll(",", "','");
        strFrmDt =
            request.getParameter("Txt_FrmDt") == null ? "" : request.getParameter("Txt_FrmDt");
        strToDt = request.getParameter("Txt_ToDt") == null ? "" : request.getParameter("Txt_ToDt");

        hmParam.put("DISTID", strDistId);
        hmParam.put("SETID", strSetId);
        hmParam.put("FRMDT", strFrmDt);
        hmParam.put("TODT", strToDt);
        hmParam.put("CATEGORY", "20210"); // hardcoding so that reportSeachSalesConsign works fine
        hmParam.put("DUMMYTRAN", "4110"); // hardcoding so that reportSeachSalesConsign works fine

        resultSet = gmSales.reportSeachSalesConsign(hmParam);
        request.setAttribute("results", resultSet);
        hmParam.put("SETID", strhSetId);
        request.setAttribute("hmParam", hmParam);
      }
      // Fetch set information
      if (strOpt.equals("BYGSET")) {
        hmParam.put("DISTID", strDistId);
        hmParam.put("SETID", strSetId);
        hmParam.put("FRMDT", strFrmDt);
        hmParam.put("TODT", strToDt);

      }

      // If by Part search is clicked
      if (strOpt.equals("BYPART")) {
        hmParam.put("SetNumber", strhSetId);
        hmParam.put("DistributorID", strDistId);
        hmParam.put("Type", "BYPART");
        resultSet = gmSales.reportSeachPartConsign(hmParam);
        request.setAttribute("results", resultSet);
        strDispatchTo = strSalesPath.concat("/GmSalesConsignPartSearch.jsp");
      }

      // If by Part search is clicked for selected sets
      if (strOpt.equals("BYGPART")) {
        hmParam.put("SetNumber", strhSetId);
        hmParam.put("DistributorID", strDistId);
        hmParam.put("Type", "BYGPART");
        strOpt = "BYPART";
        resultSet = gmSales.reportSeachPartConsign(hmParam);
        request.setAttribute("results", resultSet);
        strDispatchTo = strSalesPath.concat("/GmSalesConsignPartSearch.jsp");
      }



      // If Part search is clicked by Set
      if (strOpt.equals("BYSETPART")) {
        hmParam.put("SetNumber", strhSetId);
        hmParam.put("DistributorID", strDistId);
        hmParam.put("Type", strType);
        resultSet = gmSales.reportSearchhPartSetDetails(hmParam);
        request.setAttribute("results", resultSet);
        strDispatchTo = strSalesPath.concat("/GmSalesConsignPartSearch.jsp");
      }

      // If Part search is clicked by Set
      if (strOpt.equals("BYPARTDETAIL")) {
        hmParam.put("SetNumber", strhSetId);
        hmParam.put("DistributorID", strDistId);
        hmParam.put("PartNumber", strhPartId);


        resultSet = gmSales.reportSearchPartConsign(hmParam);
        request.setAttribute("results", resultSet);
        strDispatchTo = strSalesPath.concat("/GmSalesConsignPartSearch.jsp");
      }

      // If Part search is clicked by Set
      if (strOpt.equals("BYPARTCROSSOVER")) {
        // hmParam.put("SetNumber" , strhSetId );
        hmParam.put("DistributorID", strDistId);
        hmParam.put("PartNumber", strhPartId);

        resultSet = gmSales.reportSearchCrossOverPart(hmParam);
        request.setAttribute("results", resultSet);
        strDispatchTo = strSalesPath.concat("/GmSalesConsignPartSearch.jsp");
      }

      gmFramework.getRequestParams(request, response, session, hmParam);
      // To fetch the filter details to be displayed (Additioan Filter details)
      hmFilterDetails = gmSalesComp.GetFilterDetails(hmParam);
      request.setAttribute("hFilterDetails", hmFilterDetails);

      request.setAttribute("hAction", strAction);
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("hmReturn", hmReturn);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSalesConsignSearchServlet
