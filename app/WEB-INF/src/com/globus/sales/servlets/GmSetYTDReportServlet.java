/*****************************************************************************
 * File : GmSetYTDReportServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesYTDBean;

public class GmSetYTDReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    String strJSPName = "";

    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";

    String strShowArrowFl = "";
    String strShowPercentFl = "";
    String strHideAmount = "";
    String strHeader = "";

    // Additional Filter condition
    String strDistributorID = "";
    String strSetNumber = "";
    String strAccountID = "";
    String strRepID = "";
    String strADID = "";
    String strTerritoryID = "";
    String strGroupAccId = "";
    String strGrpPricingName = "";
    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = request.getParameter("hAction");

      GmCommonClass gmCommon = new GmCommonClass();
      GmCommonBean gmComBean = new GmCommonBean(getGmDataStoreVO());
      GmCustomerBean gmCustomer = new GmCustomerBean(getGmDataStoreVO());
      GmSalesCompBean gmSalesComp = new GmSalesCompBean(getGmDataStoreVO());
      GmSalesYTDBean gmSalesYTD = new GmSalesYTDBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();

      HashMap hmReturn = new HashMap();
      HashMap hmFromDate = new HashMap();
      HashMap hmComonValue = new HashMap();
      HashMap hmparam = new HashMap();
      HashMap hmFilterDetails = new HashMap();
      HashMap hmSalesFilters = new HashMap();

      String strCondition = null;
      String strAccessFilter = null;

      String strId = "";
      String strExcelFileName = "";
      ArrayList alReturn = new ArrayList(); // Value used to fill the combo box value
      ArrayList alDistList = new ArrayList();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();



      if (strAction == null) {
        strAction = (String) session.getAttribute("strSessOpt");
      }

      strFromMonth = request.getParameter("Cbo_FromMonth");
      strFromYear = request.getParameter("Cbo_FromYear");
      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");
      strShowArrowFl = request.getParameter("Chk_ShowArrowFl");
      strShowPercentFl = request.getParameter("Chk_ShowPercentFl");
      strHideAmount = request.getParameter("Chk_HideAmountFl");

      // Additional Filter
      strSetNumber = request.getParameter("SetNumber");
      strDistributorID = request.getParameter("DistributorID");
      strAccountID = request.getParameter("hAccountID");
      strRepID = request.getParameter("hRepID");
      strADID = request.getParameter("hAreaDirector");
      strTerritoryID = request.getParameter("hTerritory");

      // MNTTTASK-6864 - While select Group Account in drop down in Pricing Request Screen, will
      // show Historical Sales and System Icon.
      strGroupAccId = GmCommonClass.parseNull(request.getParameter("hGroupAccId"));
      strGrpPricingName = GmCommonClass.parseNull(request.getParameter("hGroupName"));

      // Default selected value
      String strProjectID = gmCommon.parseZero(request.getParameter("Cbo_Proj"));
      String strID = gmCommon.parseZero(request.getParameter("Cbo_Name"));
      String strType = request.getParameter("Cbo_Type");
      String strExcel = request.getParameter("hExcel");
      // Get cookie only on onload for the first time and not on the Go button
      if (strFromMonth == null) {
        strFromMonth =
            strFromMonth == null ? gmCookieManager.getCookieValue(request, "cFromMonth")
                : strFromMonth;
        strFromYear =
            strFromYear == null ? gmCookieManager.getCookieValue(request, "cFromYear")
                : strFromYear;
        strToMonth =
            strToMonth == null ? gmCookieManager.getCookieValue(request, "cToMonth") : strToMonth;
        strToYear =
            strToYear == null ? gmCookieManager.getCookieValue(request, "cToYear") : strToYear;
        strShowPercentFl =
            strShowPercentFl == null ? gmCookieManager.getCookieValue(request, "cShowPercentFl")
                : strShowPercentFl;
        strShowArrowFl =
            strShowArrowFl == null ? gmCookieManager.getCookieValue(request, "cShowArrowFl")
                : strShowArrowFl;
        strHideAmount =
            strHideAmount == null ? gmCookieManager.getCookieValue(request, "cHideAmount")
                : strHideAmount;

        // Check for account name in cookie
        strID = gmCookieManager.getCookieValue(request, "cAccountName");
        // ReCheck if not found set account name to default
        strID = strID == null ? "0" : strID;

        strType = strType == null ? gmCookieManager.getCookieValue(request, "cType") : strType;
        // Recheck Type if not found in cookie
        strType = (strType == null) ? "1" : strType; // Setting combo box value
      }

      strAction = (strAction == null) ? "LoadSet" : strAction;

      strExcelFileName = request.getParameter("hcboName");
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
			GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
      strCondition = getAccessCondition(request, response, true);
      String strConditionFalse = getAccessCondition(request, response, false);
      request.setAttribute("hmSalesFilters", hmSalesFilters);

      // Add the filter to hashmap paramter
      hmparam.put("FromMonth", strFromMonth);
      hmparam.put("FromYear", strFromYear);
      hmparam.put("ToMonth", strToMonth);
      hmparam.put("ToYear", strToYear);
      hmparam.put("Condition", strCondition);
      hmparam.put("AccessFilter", strAccessFilter);
      hmparam.put("ProjectID", strProjectID);
      hmparam.put("Type", strType);

      // Additional filter / drilldown
      hmparam.put("SetNumber", strSetNumber);
      hmparam.put("DistributorID", strDistributorID);
      hmparam.put("AccountID", strAccountID);
      hmparam.put("RepID", strRepID);
      hmparam.put("ADID", strADID);
      hmparam.put("TerritoryID", strTerritoryID);

      gmFramework.getRequestParams(request, response, session, hmparam);

      if (strAction.equals("LoadSet")) {
        // To retrieve the combo box value
        alReturn = gmCustomer.getDistributorList();
        strHeader = "Sales By Set By Month";
        hmReturn =
            gmSalesYTD.reportYTDBySet(strFromMonth, strFromYear, strToMonth, strToYear,
                strCondition, strID, strType, hmparam);
      } else if (strAction.equals("LoadGroup")) {
        // To retrieve the combo box value
        alReturn = gmComBean.getSalesFilterAcct(strAccessFilter);
        strHeader = "Territory Sales By Group By Month";
        // BUG-7835- We are using the getHierarchyWhereConditionV700 to get the filter condition,
        // here we are not having the condition for Group.
        // So getting the system filter from the request and setting it to set number drilldown.
        String strPgrpFilter = GmCommonClass.parseNull(request.getParameter("hPgrpFilter"));
        strPgrpFilter = strPgrpFilter.replaceAll(",", "','");
        hmparam.put("SetNumber", strPgrpFilter);
        hmReturn = gmSalesYTD.reportYTDByGroup(hmparam);
        // Resetting the SetNumber filter
        hmparam.put("SetNumber", "");
      } else if (strAction.equals("LoadGroupA")) {
        // To retrieve the combo box value
        alReturn = gmComBean.getAccountList(strConditionFalse);
        strHeader = "Historical Sales By Group By Month";
        hmparam.put("GroupAccId", strGroupAccId);
        hmReturn = gmSalesYTD.reportYTDByGroupActual(hmparam);
      }
      // To fetch the filter details to be displayed
      hmFilterDetails = gmSalesComp.GetFilterDetails(hmparam);
      hmFilterDetails.put("GRPNAME", strGrpPricingName);

      hmComonValue.put("PROJECTS", alReturn);
      request.setAttribute("hmComonValue", hmComonValue);

      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FromDate");
      if (hmFromDate.get("FirstTime") != null) {
        // System.out.println("To get the date for the onload condition...");
        strFromMonth = (String) hmFromDate.get("FROMMONTH");
        strFromYear = (String) hmFromDate.get("FROMYEAR");
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");

        // System.out.println("hmFromDate" + hmFromDate);
      }

      alMonthDropDown = GmCommonClass.parseNullArrayList(gmComBean.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmComBean.getYearList());
      hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
      hmReturn.put("ALYEARDROPDOWN", alYearDropDown);

      request.setAttribute("hAction", strAction);
      request.setAttribute("strHeader", strHeader);
      request.setAttribute("Cbo_FromMonth", strFromMonth);
      request.setAttribute("Cbo_FromYear", strFromYear);
      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("Chk_ShowArrowFl", strShowArrowFl);
      request.setAttribute("Chk_ShowPercentFl", strShowPercentFl);
      request.setAttribute("Chk_HideAmountFl", strHideAmount);
      request.setAttribute("Cbo_Name", strID);
      request.setAttribute("Cbo_Type", strType);
      request.setAttribute("GroupAccId", strGroupAccId);
      request.setAttribute("GrpPricingName", strGrpPricingName);


      // Additional filter
      request.setAttribute("hAccountID", strAccountID);
      request.setAttribute("hDistributorID", strDistributorID);
      request.setAttribute("hSetNumber", strSetNumber);
      request.setAttribute("hRepID", strRepID);
      request.setAttribute("hTerritory", strTerritoryID);
      request.setAttribute("hAreaDirector", strADID);

      // Additioan Filter details
      request.setAttribute("hFilterDetails", hmFilterDetails);

      if (strExcel == null) {
        strJSPName = "/GmSetReportYTDBySet.jsp";
      } else {

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + strExcelFileName
            + ".xls");
        strJSPName = "/GmReportYTDExcel.jsp";

      }

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cFromMonth", strFromMonth,
          GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cFromYear", strFromYear, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToMonth", strToMonth, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToYear", strToYear, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cShowPercentFl", strShowPercentFl);
      gmCookieManager.setCookieValue(response, "cShowArrowFl", strShowArrowFl);
      gmCookieManager.setCookieValue(response, "cHideAmount", strHideAmount);
      gmCookieManager.setCookieValue(response, "cAccountName", strID);
      gmCookieManager.setCookieValue(response, "cType", strType);

      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmTerritoryServlet
