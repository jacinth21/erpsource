/*****************************************************************************
 * File : GmSalesYTDReportServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSecurityBean;
// import com.globus.sales.beans.GmSalesConsignReportSummaryBean;
import com.globus.common.util.GmCookieManager;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesConsignReportBean;
import com.globus.sales.beans.GmSalesConsignReportSummaryBean;

public class GmSalesConsignmentServlet extends GmSalesServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strDispatchTo = "";
    String strJSPName = "";
    String strHeader = "";
    String strHeaderPrefix = "";
    String strType = "";
    String strDeptId = "";
    boolean blCOGSAccess = false;

    try {

      Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

      log.debug("Entering GmSalesConsignmentServlet -- " + request.getParameter("hAction"));

      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to
      // SessionExpiry page
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      String strAccessLevel =
          GmCommonClass.parseZero((String) session.getAttribute("strSessAccLvl"));
      GmCookieManager gmCookieManager = new GmCookieManager();
      GmSalesConsignReportBean gmConsignment = new GmSalesConsignReportBean(getGmDataStoreVO());
      GmSalesCompBean gmSalesComp = new GmSalesCompBean(getGmDataStoreVO());
      GmSecurityBean gmSecurityBean = new GmSecurityBean();
      GmSalesConsignReportSummaryBean gmSummaryBean =
          new GmSalesConsignReportSummaryBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();

      HashMap hmFinal = new HashMap();
      HashMap hmReturn = new HashMap();
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      HashMap hmSalesFilters = new HashMap();
      String strFilterCondition = "";
      String strAccessLvl = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
      request.setAttribute("strOverrideASSAccLvl", strAccessLvl);
      // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
      String strSalesFilterFullAcc = GmCommonClass.parseNull((String) session.getAttribute("strSessSalesFullAccess"));
      request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);      
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      HashMap hmparam = receiveCrossTabRequest(request, response);
      String strCompFilter = "";
      String strSessCompId = "";
      // Get Settings from cookie
      strCompFilter = GmCommonClass.parseNull(request.getParameter("hCompFilter"));
      strSessCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
      strCompFilter =
          strCompFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "chCompFilter")) : strCompFilter;
      strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter;
      hmparam.put("COMPID", strCompFilter);
      // override Access filter
      log.debug("<<< AccessFilter1 >>>" + hmparam.get("AccessFilter"));
      hmparam.put("AccessFilter", strFilterCondition);
      log.debug("<<< AccessFilte2 >>>" + hmparam.get("AccessFilter"));
      hmparam = addDates(hmparam, 4);
      HashMap hmFilterDetails = new HashMap();
      // GmSalesFilterConditionBean conditionBean = new GmSalesFilterConditionBean();

      ArrayList alConsignmentTypes = new ArrayList();
      ArrayList alSaleTypes = new ArrayList();
      ArrayList alConTypes = new ArrayList();
      ArrayList alPeriods = new ArrayList();
      ArrayList alSetTypes = new ArrayList();
      ArrayList alStatus = new ArrayList();
      ArrayList alTurns = new ArrayList();
      ArrayList alLoanerType = new ArrayList();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();

      strJSPName = "/GmVSetReport.jsp";

      log.debug(" Parameters -- > " + hmparam);
      if (strAction == null) {
        strAction = (String) session.getAttribute("strSessOpt");
        strOpt = strAction;
        strJSPName = "/GmVSetContainer.jsp";
        // dispatch(strOperPath.concat(strJSPName), request, response);
        log.debug(" action inside " + strAction + " strOpt " + strOpt + " strJSPName " + strJSPName);
      }
      strAction = (strAction == null) ? "LoadDistributor" : strAction;

      alConsignmentTypes = GmCommonClass.getCodeList("CSGTP");
      alSaleTypes = GmCommonClass.getCodeList("ORDTP");
      alStatus = GmCommonClass.getCodeList("STATS");
      alTurns = GmCommonClass.getCodeList("TURN");
      alLoanerType = GmCommonClass.getCodeList("LTRNT");

      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());


      // removing "Loaner Set" option for anything other than BY SYSTEM
      if (!strAction.equals("LoadGCONS") || !strAccessLevel.equals("4")) {
        alLoanerType.remove(0);
      }

      // Use this type to check COGS access and populate the consignment arraylist with the COGS
      // value
      strType = GmCommonClass.parseNull((String) hmparam.get("Type"));
      strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
      strHeaderPrefix = GmCommonClass.parseNull((String) hmparam.get("HEADERPREFIX"));

      log.debug(" type is " + strType + " dept Id is " + strDeptId);
      log.debug(" Action is " + strAction);

      blCOGSAccess = gmSecurityBean.checkCOGSAccess(strType, strDeptId);

      if (blCOGSAccess) {
        alConsignmentTypes = GmCommonClass.getCodeList("CSGTP,CSGCO");
      }

      log.debug(" alConsignmentTypes " + alConsignmentTypes);

      gmFramework.getRequestParams(request, response, session, hmparam);

      if (strAction.equals("LoadSummary") || strAction.equals("LoadDistSummary")) {
        strHeader = strHeaderPrefix + " Summary Report";

        // To remove from the filter condition
        hmparam.put("ConsignmentType", "");
        hmparam.put("SalesType", "");
        // ******************************//

        hmReturn = gmSummaryBean.reportConsignmentSummary(hmparam);

        alConTypes = GmCommonClass.getCodeList("CSTPS");
        alPeriods = GmCommonClass.getCodeList("CSTPP");
        alSetTypes = GmCommonClass.getCodeList("VGUAD");

        request.setAttribute("Cbo_Type", "0");

        request.setAttribute("alConTypes", alConTypes);
        request.setAttribute("alPeriods", alPeriods);
        request.setAttribute("alSetTypes", alSetTypes);
        strJSPName = "/GmSalesConsignmentReport.jsp";
      } else if (strAction.equals("LoadGCONS")) {
        strHeader = strHeaderPrefix + " Summary Report By System ";
        // gmConsignment.setAction("LoadGCONS");
        log.debug("Action :" + hmparam.get("hAction"));
        log.debug("Accesss Filter in Servlet :" + hmparam.get("AccessFilter"));
        hmFinal = gmConsignment.reportVSetSummaryByGroup(hmparam);
        log.debug(" hashmap valies " + hmFinal);
        hmReturn = (HashMap) hmFinal.get("CROSSVALE");
      } else if (strAction.equals("LoadDCONS")) {
        strHeader = strHeaderPrefix + " Summary Report By Field Sales ";
        // gmConsignment.setAction("LoadDistributor");
        hmFinal = gmConsignment.reportVSetSummaryByDist(hmparam);
        hmReturn = (HashMap) hmFinal.get("CROSSVALE");
      } else if (strAction.equals("LoadAD")) {
        strHeader = strHeaderPrefix + " Summary Report By AD ";
        // gmConsignment.setAction("LoadDistributor");
        hmFinal = gmConsignment.reportVSetSummaryByAD(hmparam);
        hmReturn = (HashMap) hmFinal.get("CROSSVALE");
      }

      else if (strAction.equals("LoadVP")) {
        strHeader = strHeaderPrefix + " Summary Report By VP ";
        // gmConsignment.setAction("LoadDistributor");
        hmFinal = gmConsignment.reportVSetSummaryByVP(hmparam);
        hmReturn = (HashMap) hmFinal.get("CROSSVALE");
      }

      log.debug("Before Filter");
      // To fetch the filter details to be displayed
      hmFilterDetails = gmSalesComp.GetFilterDetails(hmparam);
      log.debug("After Filter");
      // Additional filter
      hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
      hmReturn.put("ALYEARDROPDOWN", alYearDropDown);

      sendRequestAttributes(request, hmparam);
      request.setAttribute("hAction", strAction);
      request.setAttribute("strHeader", strHeader);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("alSaleTypes", alSaleTypes);
      request.setAttribute("alStatus", alStatus);
      request.setAttribute("alTurns", alTurns);
      request.setAttribute("alLoanerType", alLoanerType);
      request.setAttribute("alConsignmentTypes", alConsignmentTypes);

      log.debug("After Setting the Attribute");

      // Additioan Filter details
      request.setAttribute("hFilterDetails", hmFilterDetails);

      if (!hmparam.get("hExcel").equals("")) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + hmparam.get("hcboName")
            + ".xls");
        strJSPName = "/GmReportYTDExcel.jsp";
      }
      dispatch(strOperPath.concat(strJSPName), request, response);
      log.debug("Exiting GmSalesConsignmentServlet");

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSalesConsignmentServlet
