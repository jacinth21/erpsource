/*****************************************************************************
 * File			 : GmSetMapServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmSalesConsignReportBean;

import com.globus.common.beans.GmLogger;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;



public class GmVirtualSetMapServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath 	= GmCommonClass.getString("GMCOMMON");
		String strPDPath 	= GmCommonClass.getString("GMPRODMGMNT");
		String strSAPath 	= GmCommonClass.getString("GMSALES");
		String strDispatchTo = strPDPath.concat("/GmSetMapReport.jsp");
		RowSetDynaClass resultSet = null ;
		GmSalesConsignReportBean GmConsignment = new GmSalesConsignReportBean();
		
		GmCommonBean gmCommonBean = new GmCommonBean();
		int i = 0;
		i = i +1;
		HashMap hmReturn = new HashMap();
		ArrayList alMapReturn = new ArrayList();

		String strInputString = "";
		String strSetId = "";
		String strSetName = "";
		
		// Parameter used for additional filter 
		String strADID			= "";
		String strDistributorID	= "";
		HashMap hmparam 		= new HashMap();
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

			
			String strAction = request.getParameter("hAction");
			if (strAction == null)
			{
				strAction = (String)session.getAttribute("hAction");
			}
			GmProjectBean gmProj = new GmProjectBean();

			
			String strUserId = 	(String)session.getAttribute("strSessUserId");

			String strOpt = request.getParameter("hOpt");
			if (strOpt == null)
			{
				strOpt = (String)session.getAttribute("strSessOpt");
			}
			strOpt = "SETMAP";//(strOpt == null)?"SET":strOpt;
			
			java.util.Locale locale = new Locale("en","US",strOpt);
			GmResourceBundleBean resourceBundleBean = new GmResourceBundleBean("GmBrand",locale);
			request.setAttribute("brandBean", resourceBundleBean);
			// Code ends for Resource Bundle Configuration

			strSetId = request.getParameter("hSetNumber")==null?"":request.getParameter("hSetNumber");
			if (strAction.equals("Drilldown"))
			{

				strSetName = request.getParameter("hSetName")==null?"":request.getParameter("hSetName");
				alMapReturn = gmProj.fetchSetMaping(strSetId,"VSET");
				request.setAttribute("SETMAPPEDLIST",alMapReturn);
				request.setAttribute("hSetId",strSetId);
				request.setAttribute("hSetNm",strSetName);
				request.setAttribute("hAction",strAction);
			}
			else if (strAction.equals("LoadGCons"))
			{
				
			    //strSetId 		= request.getParameter("hSetNumber");
				strDistributorID 	= request.getParameter("hDistributorID");
				strADID				= request.getParameter("hAreaDirector");
				
				hmparam.put("SetNumber" , strSetId );
				hmparam.put("DistributorID", strDistributorID);
				hmparam.put("ADID" , strADID );
				hmparam.put("Type" , "Main" );
				strAction = "LoadUnACons";
			    hmReturn = GmConsignment.reportConsignmentDetails(hmparam);
			    
			    request.setAttribute("results",(RowSetDynaClass)hmReturn.get("PDETAIL"));
			    strDispatchTo= strSAPath.concat("/GmSalesVirtualDetails.jsp") ;
			    
			}			
			

			else if (strAction.equals("LoadUnACons"))
			{
			    //strSetId 		= request.getParameter("hSetNumber");
				strDistributorID 	= request.getParameter("hDistributorID");
				strADID				= request.getParameter("hAreaDirector");
				
				hmparam.put("SetNumber" , strSetId );
				hmparam.put("DistributorID", strDistributorID);
				hmparam.put("ADID" , strADID );
				hmparam.put("Type" , "Child" );
			    hmReturn = GmConsignment.reportConsignmentDetails(hmparam);
			    
			    request.setAttribute("results",(RowSetDynaClass)hmReturn.get("PDETAIL"));
			    strDispatchTo= strSAPath.concat("/GmSalesVirtualDetails.jsp") ;
			    
			}
			dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmSetMapServlet