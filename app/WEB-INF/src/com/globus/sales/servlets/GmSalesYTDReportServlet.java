/*****************************************************************************
 * File : GmSalesYTDReportServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.beans.GmSalesYTDBean;

public class GmSalesYTDReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMSALES");

    String strDispatchTo = "";
    String strJSPName = "";
    String strDistributorID = "";
    String strSetNumber = "";
    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";
    String strShowArrowFl = "";
    String strShowPercentFl = "";
    String strHideAmount = "";
    String strHeader = "";
    String strAccountID = "";
    String strGroupID = "";
    String strRepID = "";
    String strPartNumber = "";
    String strADID = "";
    String strTerritoryID = "";
    String strAccessFilter = "";
    String strPageLoad = "";
    String strGroupAccId = "";
    String strhSalesType = "";

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = request.getParameter("hAction");

      GmCommonClass gmCommon = new GmCommonClass();
      GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
      GmSalesYTDBean gmSalesYTD = new GmSalesYTDBean(getGmDataStoreVO());
      GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
      GmSalesCompBean gmSalesComp = new GmSalesCompBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();

      HashMap hmReturn = new HashMap();
      HashMap hmFromDate = new HashMap();
      HashMap hmComonValue = new HashMap();
      HashMap hmparam = new HashMap();
      HashMap hmFilterDetails = new HashMap();
      HashMap hmSalesFilters = new HashMap();

      String strCondition = null;

      String strId = "";
      String strExcelFileName = "";
      ArrayList alReturn = new ArrayList(); // Value used to fill the combo box value
      ArrayList alDistList = new ArrayList();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();

      if (strAction == null) {
        strAction = (String) session.getAttribute("strSessOpt");
      }

      // Get from request or from cookie if not found
      strFromMonth = request.getParameter("Cbo_FromMonth");
      strFromYear = request.getParameter("Cbo_FromYear");
      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");
      strShowArrowFl = request.getParameter("Chk_ShowArrowFl");
      strShowPercentFl = request.getParameter("Chk_ShowPercentFl");
      strHideAmount = request.getParameter("Chk_HideAmountFl");

      // Additional Filter
      strSetNumber = request.getParameter("SetNumber");
      strDistributorID = request.getParameter("DistributorID");
      strAccountID = request.getParameter("hAccountID");
      strRepID = request.getParameter("hRepID");
      strPartNumber = request.getParameter("hPartNumber");
      strADID = request.getParameter("hAreaDirector");
      strTerritoryID = request.getParameter("hTerritory");
      strPageLoad = request.getParameter("pageLoad");
      strhSalesType = request.getParameter("hSalesType");
      log.debug("strhSalesType ===" + strhSalesType);
      // MNTTTASK-6864 - While select Group Account in drop down in Pricing Request Screen, will
      // show Historical Sales and System Icon.
      strGroupAccId = GmCommonClass.parseNull(request.getParameter("hGroupAccId"));

      // Default selected value
      String strType = request.getParameter("Cbo_Type");
      String strSalesType = request.getParameter("Cbo_SalesType");
      // Get cookie only on onload for the first time and not on the Go button
      if (strFromMonth == null) {
        strFromMonth =
            strFromMonth == null ? gmCookieManager.getCookieValue(request, "cFromMonth")
                : strFromMonth;
        strFromYear =
            strFromYear == null ? gmCookieManager.getCookieValue(request, "cFromYear")
                : strFromYear;
        strToMonth =
            strToMonth == null ? gmCookieManager.getCookieValue(request, "cToMonth") : strToMonth;
        strToYear =
            strToYear == null ? gmCookieManager.getCookieValue(request, "cToYear") : strToYear;
        strShowPercentFl =
            strShowPercentFl == null ? gmCookieManager.getCookieValue(request, "cShowPercentFl")
                : strShowPercentFl;
        strShowArrowFl =
            strShowArrowFl == null ? gmCookieManager.getCookieValue(request, "cShowArrowFl")
                : strShowArrowFl;// GmCommonClass.parseZero(gmCookieManager.getCookieValue(request,
                                 // "cShowArrowFl")) ? null : "1";
        strHideAmount =
            strHideAmount == null ? gmCookieManager.getCookieValue(request, "cHideAmount")
                : strHideAmount;
        strSetNumber =
            strSetNumber == null ? gmCookieManager.getCookieValue(request, "cSetNumber")
                : strSetNumber;
        strType = strType == null ? gmCookieManager.getCookieValue(request, "cType") : strType;
        strSalesType =
            strSalesType == null ? gmCookieManager.getCookieValue(request, "cSalesType")
                : strSalesType;
        // Recheck SalesType if not found in cookie
        strSalesType = strSalesType == null ? "0" : strSalesType;
      }
      String strFromPage = GmCommonClass.parseNull(request.getParameter("hFromPage"));
      // Recheck Type if not found in cookie
      strType = (strType == null) ? "1" : strType; // Setting combo box value
      // Below lines of code is added because, when we change from Dollar to Unit or vice versa, it
      // was giving incorrect data.
      if (strFromPage.equals("FIELDINVENTORYREPORT")) {
        strSalesType = strSalesType == null ? "0" : strSalesType;
        strhSalesType = strhSalesType == null ? "0" : strhSalesType;
        strSalesType = strSalesType.equals("0") ? strhSalesType : strSalesType;
      }
      String strExcel = request.getParameter("hExcel");

      ArrayList alSalesType = GmCommonClass.getCodeList("ORDTP");
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      strAction = (strAction == null) ? "LoadDistributor" : strAction;


      // To retrieve the combo box value
      alReturn = gmSales.loadProjectGroup();
      hmComonValue.put("PROJECTS", alReturn);
      request.setAttribute("hmComonValue", hmComonValue);

      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
      strCondition = getAccessCondition(request, response, true);
      log.debug("  strCondition " + strCondition);

      request.setAttribute("hmSalesFilters", hmSalesFilters);
      // Add the filter to hashmap paramter
      hmparam.put("FromMonth", strFromMonth);
      hmparam.put("FromYear", strFromYear);
      hmparam.put("ToMonth", strToMonth);
      hmparam.put("ToYear", strToYear);
      hmparam.put("Condition", strCondition);
      hmparam.put("AccessFilter", strAccessFilter);
      hmparam.put("Type", strType);
      hmparam.put("SalesType", strSalesType);

      // Additional filter / drilldown
      hmparam.put("DistributorID", strDistributorID);
      hmparam.put("AccountID", strAccountID);
      hmparam.put("RepID", strRepID);
      hmparam.put("SetNumber", strSetNumber);
      hmparam.put("PartNumber", strPartNumber);
      hmparam.put("ADID", strADID);
      hmparam.put("TerritoryID", strTerritoryID);
      hmparam.put("GroupAccId", strGroupAccId);

      log.debug(" params for uinput " + hmparam);
      strExcelFileName = request.getParameter("hcboName");
      strJSPName = "/GmSalesReportYTD.jsp";

      gmFramework.getRequestParams(request, response, session, hmparam);

      if (strAction.equals("LoadSummary")) {
        strHeader = "Territory Summary Report";
        hmReturn = gmSalesYTD.reportYTDBySummary(hmparam);
        strJSPName = "/GmSalesReportMultiYTD.jsp";
      } else if (strAction.equals("LoadTerritory")) {
        // strExcelFileName = "DistributorYTD";
        strHeader = "Territory Sales By Month";
        hmReturn = gmSalesYTD.reportYTDByTerritory(hmparam);
      } else if (strAction.equals("LoadDistributor") || strAction.equals("LoadAD")
          || strAction.equals("LoadVP")) {
        // strExcelFileName = "DistributorYTD";
        if (strAction.equals("LoadDistributor")) {
          strHeader = "Territory Sales By Field Sales By Month";
          hmparam.put("RPTTYPE", "DIST");
        } else if (strAction.equals("LoadAD")) {
          strHeader = "Territory Sales By AD By Month";
          hmparam.put("RPTTYPE", "AD");
        } else if (strAction.equals("LoadVP")) {
          strHeader = "Territory Sales By VP By Month";
          hmparam.put("RPTTYPE", "VP");
        }
        hmReturn = gmSalesYTD.reportYTDByDist(hmparam);
      } else if (strAction.equals("LoadDistributorA")) {
        // strExcelFileName = "DistributorYTD";
        strHeader = "Historical Sales By Field Sales By Month";
        hmReturn = gmSalesYTD.reportYTDByDistActual(hmparam);
      } else if (strAction.equals("LoadAccount")) {
        // strExcelFileName = "AccountYTD";
        strHeader = "Territory Sales By Account By Month";
        hmReturn = gmSalesYTD.reportYTDByAccount(hmparam);
        request.setAttribute("strDistributorID", strDistributorID);
      } else if (strAction.equals("LoadAccountA")) {
        // strExcelFileName = "AccountYTD";
        strHeader = "Historical Sales By Account By Month";
        hmReturn = gmSalesYTD.reportYTDByAccountActual(hmparam);
        request.setAttribute("strDistributorID", strDistributorID);
      } else if (strAction.equals("LoadRep")) {
        // strExcelFileName = "AccountYTD";
        strHeader = "Territory Sales By Spine Specialist By Month";
        hmReturn = gmSalesYTD.reportYTDBySalesRep(hmparam);
      } else if (strAction.equals("LoadRepA")) {
        // strExcelFileName = "AccountYTD";
        strHeader = "Historical Sales By Spine Specialist By Month";
        hmReturn = gmSalesYTD.reportYTDBySalesRepActual(hmparam);
      }

      else if (strAction.equals("LoadPart")) {
        // strExcelFileName = "AccountYTD";
        strHeader = "Territory Sales By Part By Month";
        hmReturn = gmSalesYTD.reportYTDByPart(hmparam);
      } else if (strAction.equals("LoadPartA")) {
        // strExcelFileName = "AccountYTD";
        strHeader = "Historical Sales By Part By Month";
        hmReturn = gmSalesYTD.reportYTDByPartActual(hmparam);

      }


      // To fetch the filter details to be displayed
      hmFilterDetails = gmSalesComp.GetFilterDetails(hmparam);

      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FromDate");
      if (hmFromDate.get("FirstTime") != null) {
        // System.out.println("To get the date for the onload condition...");
        strFromMonth = (String) hmFromDate.get("FROMMONTH");
        strFromYear = (String) hmFromDate.get("FROMYEAR");
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");

        // System.out.println("hmFromDate" + hmFromDate);
      }
      alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
      alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

      hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
      hmReturn.put("ALYEARDROPDOWN", alYearDropDown);

      request.setAttribute("hAction", strAction);
      request.setAttribute("strHeader", strHeader);
      request.setAttribute("Cbo_FromMonth", strFromMonth);
      request.setAttribute("Cbo_FromYear", strFromYear);
      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("Chk_ShowPercentFl", strShowPercentFl);
      request.setAttribute("Chk_ShowArrowFl", strShowArrowFl);
      request.setAttribute("Chk_HideAmountFl", strHideAmount);
      request.setAttribute("Cbo_Type", strType);
      request.setAttribute("Cbo_SalesType", strSalesType);
      request.setAttribute("SALESTYPE", alSalesType);
      request.setAttribute("HFROMPAGE", strFromPage);
      request.setAttribute("pageLoad", strPageLoad);
      request.setAttribute("GroupAccId", strGroupAccId);

      // Additional filter
      request.setAttribute("hAccountID", strAccountID);
      request.setAttribute("hDistributorID", strDistributorID);
      request.setAttribute("hSetNumber", strSetNumber);
      request.setAttribute("hRepID", strRepID);
      request.setAttribute("hPartNumber", strPartNumber);
      request.setAttribute("hTerritory", strTerritoryID);
      request.setAttribute("hAreaDirector", strADID);

      // Additioan Filter details
      request.setAttribute("hFilterDetails", hmFilterDetails);

      if (strExcel != null) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + strExcelFileName
            + ".xls");
        strJSPName = "/GmReportYTDExcel.jsp";
      }

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cFromMonth", strFromMonth,
          GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cFromYear", strFromYear, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToMonth", strToMonth, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToYear", strToYear, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cShowPercentFl", strShowPercentFl);
      gmCookieManager.setCookieValue(response, "cShowArrowFl", strShowArrowFl);
      gmCookieManager.setCookieValue(response, "cHideAmount", strHideAmount);
      gmCookieManager.setCookieValue(response, "cSetNumber", strSetNumber);
      gmCookieManager.setCookieValue(response, "cType", strType);
      gmCookieManager.setCookieValue(response, "cSalesType", strSalesType);

      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmTerritoryServlet
