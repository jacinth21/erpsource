/*****************************************************************************
 * File			 : GmPartReportServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;

public class GmGenIcfServlet extends HttpServlet
{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
  		try{
  		GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
  		GmLogonBean gmLogonBean= new GmLogonBean();
  		HashMap hmParam=new HashMap();
  		HashMap hmReturn=new HashMap();
  		DateFormat formatter ; 
  		formatter = new SimpleDateFormat("dd/mm/yyyy");
  		Date startDate=formatter.parse("01/01/"+((new Date().getYear())+1899)); //previous year
  		Date endDate=formatter.parse("30/12/"+((new Date().getYear())+1901)); // next year
  		String dtFormat="dd/mm/yyyy";
  		
  		String strUserName = GmCommonClass.parseNull(request.getParameter("username"));
  		String strPassword = GmCommonClass.parseNull(request.getParameter("password"));
  		log.debug("ICAL generating for "+strUserName);
  		hmReturn=gmLogonBean.getUserInfo(strUserName);
  		String strEmpId = (String) hmReturn.get("USERID");
		String strDepartMentID = (String) hmReturn.get("DEPTID");
		String strPartyId = (String) hmReturn.get("PARTYID");
		int intAccLevel = Integer.parseInt(GmCommonClass.parseZero((String) hmReturn.get("ACID")));
		
  		hmParam.put("DATEFORMAT",dtFormat);
		hmParam.put("FROMDT",startDate);
		hmParam.put("TODT",endDate);
		hmParam.put("AccessFilter",getAccessFilter(strEmpId,intAccLevel,strDepartMentID));
  		ArrayList alResult = gmCaseBookRptBean.fetchCaseCalendar(hmParam);
		
		gmCaseBookRptBean.sendCaseCalendarICal(alResult,strUserName);
		
  		response.sendRedirect("/ical/"+strUserName+".ics");
  		}catch (Exception e)
        {
            e.printStackTrace();
        }// End of catch

	}//End of service method		
	
  	
  	private final String getAccessFilter(String strEmpId,int intAccLevel,String strDepartMentID) 
	{
				
		// Holds the where condition information 
		StringBuffer strQuery   = new StringBuffer();
				
		//*********************************************************
		// Will Execute the below Query if the Department is Sales
		//*********************************************************
		if (strDepartMentID.equals("S"))
		{
			switch( intAccLevel){
			
				// Sales Rep Filter Condition  
				case 1 :
				{
					strQuery.append(" V700.REP_ID = " + strEmpId );
					break;
				}
				
				//Distributor Filter Condition 
				case 2 :
				{
					strQuery.append("  V700.D_ID IN  " );
					strQuery.append("  ( SELECT C701_DISTRIBUTOR_ID");
					strQuery.append("  FROM T101_USER WHERE C101_USER_ID = " + strEmpId + " )  ");

					break;
				}

				//AD Filter Condition 
				case 3 :
				{
					strQuery.append("  V700.AD_ID = " +  strEmpId );
					break;
				}

				//VP Filter Condition 
				case 4 :
				{
					strQuery.append("  V700.REGION_ID IN (SELECT  C901_REGION_ID ");
					strQuery.append("  FROM T708_REGION_ASD_MAPPING ");
					strQuery.append("  WHERE C101_USER_ID = " +  strEmpId + "");
					strQuery.append("  AND   C708_DELETE_FL IS NULL ");
					strQuery.append("  AND 	C901_USER_ROLE_TYPE = 8001 )");
					break;
				}	
				
				// other AD's like Kirk Tovey want to see other region 
				case 6 :
				{
					strQuery.append("  V700.REGION_ID IN (SELECT  C901_REGION_ID ");
					strQuery.append("  FROM T708_REGION_ASD_MAPPING ");
					strQuery.append("  WHERE C101_USER_ID = " +  strEmpId + "");
					strQuery.append("  AND  C708_DELETE_FL IS NULL ");
					strQuery.append("  AND 	C901_USER_ROLE_TYPE = 8002 )");
					break;
				}	
				
				// ASS Filter condition
				case 7 :
				{
					strQuery.append("  V700.AC_ID IN  " );
					strQuery.append("  (  SELECT C704_ACCOUNT_ID FROM T704A_ACCOUNT_REP_MAPPING ");
					strQuery.append("  WHERE C704A_VOID_FL is NULL AND C703_SALES_REP_ID = " + strEmpId + " )  ");
					break;
				}
				
			}
		}
		return strQuery.toString();
	}
	
  	
}// End of GmGenIcfServlet