/*****************************************************************************
 * File : GmDetailedSalesServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesReportBean;

public class GmDetailedSalesServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strSalesPath = GmCommonClass.getString("GMSALES");

    String strDispatchTo = strSalesPath.concat("/GmDetailedSalesReport.jsp");
    String strUserId = (String) session.getAttribute("strSessUserId");
    String strType = "";
    String strAccessFilter = null;

    // Additional drilldown value

    String strFDate = "";
    String strTDate = "";

    HashMap hmParam = new HashMap();
    HashMap hmFilter = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    ArrayList alColorCodes = new ArrayList();

    String strCondition = null;

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page

      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;
      strType = request.getParameter("hType");
      String strGridXmlData = "";

      if (strType == null) {
        strType = (String) session.getAttribute("hType");
        session.removeAttribute("hType");
      }

      strType = (strType == null) ? "" : strType;

      GmCommonClass gmCommon = new GmCommonClass();
      GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
      GmCommonBean gmCommBn = new GmCommonBean(getGmDataStoreVO());
      GmSalesCompBean gmSalesComp = new GmSalesCompBean(getGmDataStoreVO());
      GmCalenderOperations gmCalOper = new GmCalenderOperations();
      GmFramework gmFramework = new GmFramework();

      Calendar cal = new GregorianCalendar();
      int year = cal.get(Calendar.YEAR);
      int month = cal.get(Calendar.MONTH);
      int day = 0;// We assigned day as 0.
      String strMonth = "" + month;
      String strDay = "" + day;
      if (strMonth.length() == 1) {
        strMonth = "0".concat(strMonth);
      }
      if (strDay.length() == 1) {
        strDay = "0".concat(strDay);
      }
      String strFromDate = "";
      String strToDate = "";
      // Date dtFromDate=(Date)GmCalenderOperations.getDate(strFromDate);
      // Date dtToDate=(Date)GmCommonClass.getCurrentDate(strApplDateFmt);

      strFromDate = gmCalOper.getAnyDateOfMonth(month, day, strApplDateFmt);// To get month of begin
                                                                            // date we are calling
                                                                            // this method.
      strToDate = gmCalOper.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      Date dtFromDate = GmCommonClass.getStringToDate(strFromDate, strApplDateFmt);
      Date dtToDate = GmCommonClass.getCurrentDate(strApplDateFmt);

      String strReportType = GmCommonClass.parseNull(request.getParameter("Cbo_Report"));
      if (strReportType.equals("")) {
        strReportType =
            GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cDMSReportType"));
      }
      strReportType = strReportType.equals("") ? "S" : strReportType;

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();
      HashMap hmTempParam = new HashMap();
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommBn.getSalesFilterLists(strAccessFilter);
      strAccessFilter = gmSalesDispatchAction.getAccessFilter(request, response, true);
      strCondition = getAccessCondition(request, response, true);
      // getting color codes and legend count from rules
      String strColorCodes =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("ALLCOLORS", "DSALES"));
      String strOthersCode =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("OTHERSCOLOR", "DSALES"));
      String strLegendsCnt =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("LEGENDCOUNT", "DSALES"));
      // getting values from string array and adding to ArrayList
      String strCodes[] = strColorCodes.split(",");
      alColorCodes = new ArrayList(Arrays.asList(strCodes));

      // To get the Access Level information

      hmFilter.put("Condition", strCondition);

      hmFilter.put("Filter", strAccessFilter);
      
      gmFramework.getRequestParams(request, response, session, hmParam);

      if (strType.equals("DAILY") || strType.equals("SalesDashDetail"))// SalesDashDetail - It opens
                                                                       // as popup from sales
                                                                       // dashboard month sales
      {
        strFromDate = request.getParameter("hFromDate");

        if (strFromDate == null) {
          strFromDate = (String) session.getAttribute("strSessDailyDate");
          session.removeAttribute("strSessDailyDate");
        }
        strFromDate = (strFromDate == null) ? "" : strFromDate;

        hmParam.put("USERID", strUserId);
        hmParam.put("TDATE", strFromDate);
        hmParam.put("COND", strCondition);
        hmParam.put("FORMAT", strApplDateFmt);
        // This is to display the daily sales grid values by sorting of distributor by sales desc.
        hmParam.put("DISTSALEAMTGRP", "Y");
        alReturn = gmSales.todaysSalesReport(hmParam);
        hmReturn.put("DAILYSALES", alReturn);
        hmReturn.put("ALCOLORSLIST", alColorCodes);
        hmReturn.put("STROTHERSCOLORCODE", strOthersCode);
        hmReturn.put("STRLEGENDSCNT", strLegendsCnt);

        hmTempParam.putAll(hmParam);

        hmTempParam.put("TODAY", alReturn);
        hmTempParam.put("ALLCOLORS", alColorCodes);
        hmTempParam.put("OTHERSCOLOR", strOthersCode);
        hmTempParam.put("LEGENDCOUNT", Integer.parseInt(strLegendsCnt));
        String strDoughnutData = generateOutPut(hmTempParam, "GmDailySalesDoughnut.vm");
        request.setAttribute("DOUGHNUTJSON", strDoughnutData);

        dtFromDate = getDateFromStr(strFromDate);

        request.setAttribute("hType", strType);
        strDispatchTo = strSalesPath.concat("/GmDailySalesReport.jsp");
      } else if (strAction.equals("Load") || strAction.equals("Reload")) {
        log.debug("hmFilter:" + hmFilter);
        // if (strAction.equals("Reload"))
        // {
        strFDate =
            request.getParameter("Txt_FromDate") == null ? strFromDate : request
                .getParameter("Txt_FromDate");
        strTDate =
            request.getParameter("Txt_ToDate") == null ? strToDate : request
                .getParameter("Txt_ToDate");
        dtFromDate = getDateFromStr(strFDate);
        dtToDate = getDateFromStr(strTDate);
        // }

        hmParam.put("FRMDT", strFDate);
        hmParam.put("TODT", strTDate);
        hmParam.put("FORMAT", strApplDateFmt);
        hmParam.put("ACCESS", strCondition);
        hmParam.put("FILTER", hmFilter.get("Filter"));
        hmParam.put("REPORT", strReportType);
        log.debug("hmParam:" + hmParam);
        alReturn = gmSales.monthSalesReport(hmParam);
        log.debug("alReturn:" + alReturn);
        hmReturn.put("SALES", alReturn);
      }

      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hFrom", dtFromDate);
      request.setAttribute("hTo", dtToDate);
      request.setAttribute("hRptType", strReportType);
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      request.setAttribute("alReturn", alReturn);// for displayTag - Detail Report

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cDMSReportType", strReportType);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private String generateOutPut(HashMap hmTemplate, String strName) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmTemplate", hmTemplate);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setTemplateName(strName);
    return templateUtil.generateOutput();
  }
}// End of GmDetailedSalesServlet
