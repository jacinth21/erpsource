/*****************************************************************************
 * File : GmSalesYTDReportServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.sales.beans.GmSalesConsignReportBean;

public class GmSalesVirtualCDetailServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMSALES");

    String strDispatchTo = "";
    String strJSPName = "";
    String strDistributorID = "";
    String strSetNumber = "";

    String strHeader = "";
    String strAccountID = "";
    String strGroupID = "";
    String strRepID = "";
    String strPartNumber = "";
    String strADID = "";
    String strTerritoryID = "";
    String strAccessFilter = "";

    RowSetDynaClass resultSet = null;
    GmSalesConsignReportBean GmConsignment = new GmSalesConsignReportBean();
    GmCommonClass gmCommon = new GmCommonClass();
    GmSalesCompBean gmSalesComp = new GmSalesCompBean();
    GmFramework gmFramework = new GmFramework();

    HashMap hmReturn = new HashMap();
    HashMap hmFromDate = new HashMap();
    HashMap hmComonValue = new HashMap();
    HashMap hmparam = new HashMap();
    HashMap hmFilterDetails = new HashMap();

    String strCondition = null;

    String strId = "";
    String strExcelFileName = "";
    ArrayList alReturn = new ArrayList(); // Value used to fill the combo box value
    ArrayList alDistList = new ArrayList();
    ArrayList alSharedSet = new ArrayList();

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("strSessOpt");
      }

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      // Additional Filter
      strSetNumber = request.getParameter("SetNumber");
      strDistributorID = request.getParameter("DistributorID");
      strAccountID = request.getParameter("hAccountID");
      strRepID = request.getParameter("hRepID");
      strPartNumber = request.getParameter("hPartNumber");
      strADID = request.getParameter("hAreaDirector");
      strTerritoryID = request.getParameter("hTerritory");

      // Default selected value
      String strType = request.getParameter("Cbo_Type");
      String strSalesType = request.getParameter("Cbo_SalesType");
      String strTurns = request.getParameter("Cbo_Turns");
      String strExcel = request.getParameter("hExcel");
      strType = (strType == null) ? "1" : strType; // Setting combo box value
      String strTypeVal = "";
      String strHeaderPrefix = "Consignment";

      strAction = (strAction == null) ? "LoadDistributor" : strAction;
      String strFromPage = GmCommonClass.parseNull(request.getParameter("FromPage"));

      // To get the Access Level information
      strCondition = getAccessCondition(request, response);

      // To get the Access Level information from Materialized view
      strAccessFilter = getAccessFilter(request, response);

      if (strSalesType.equals("50301")) {
        strHeaderPrefix = "Loaner";
      }

      hmparam.put("SetNumber", strSetNumber);
      hmparam.put("DistributorID", strDistributorID);
      hmparam.put("AccountID", strAccountID);
      hmparam.put("RepID", strRepID);
      hmparam.put("ADID", strADID);
      hmparam.put("TerritoryID", strTerritoryID);
      hmparam.put("Condition", strCondition);
      hmparam.put("AccessFilter", strAccessFilter);
      hmparam.put("SALESTYPE", strSalesType);
      hmparam.put("TURNS", strTurns);

      // hmparam.put("Type", strType) ;
      log.debug(" strFromPage" + strFromPage + " Action is " + strAction + " strOpt " + strOpt
          + " HashMap valies " + hmparam);
      if (strAction.equals("CheckSharedSet")) {
        alSharedSet = GmConsignment.fetchSharedSystem(strSetNumber);
        strJSPName = "/GmSharedSetDetailsContainer.jsp";
        log.debug(" alSharedSet " + alSharedSet);

        if (alSharedSet.size() == 0 && !strFromPage.equals("CONTAINER")) {
          HashMap hmTemp = new HashMap();
          hmTemp.put("MAINID", strSetNumber);
          hmTemp.put("TITLE", strHeaderPrefix + " Set Details - Level 1");
          alSharedSet.add(hmTemp);
        }

        request.setAttribute("ALSHAREDSET", alSharedSet);
        request.setAttribute("DISTRIBUTORID", strDistributorID);
      }

      if (strAction.equals("LoadCons")) {
        strTypeVal = strOpt.equals("BYSETPART") ? "BYSETPART" : "LoadCons";
        strHeader =
            strOpt.equals("BYSETPART") ? strHeaderPrefix + " Set Details - Level 2" : GmCommonClass
                .parseNull(request.getParameter("HEADER"));
        hmparam.put("Type", strTypeVal);
        log.debug(" values in hashmap " + hmparam);
        resultSet = GmConsignment.reportSalesBySummary(hmparam);
        strJSPName = "/GmSalesVirtualSummary.jsp";
        // strJSPName= "/GmSharedSetDetailsContainer.jsp";
      }
      gmFramework.getRequestParams(request, response, session, hmparam);
      // To fetch the filter details to be displayed
      hmFilterDetails = gmSalesComp.GetFilterDetails(hmparam);

      request.setAttribute("hAction", strAction);
      request.setAttribute("strHeader", strHeader);
      request.setAttribute("results", resultSet);
      request.setAttribute("Cbo_Type", "0");
      request.setAttribute("Cbo_SalesType", strSalesType);
      request.setAttribute("Cbo_Turns", strTurns);

      // Additioan Filter details
      request.setAttribute("hFilterDetails", hmFilterDetails);

      if (strExcel != null) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=" + strExcelFileName
            + ".xls");
        strJSPName = "/GmReportYTDExcel.jsp";
      }
      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmSalesConsignmentServlet
