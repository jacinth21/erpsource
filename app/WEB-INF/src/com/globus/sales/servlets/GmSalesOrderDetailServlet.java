/*
 * Created on May 2, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.globus.sales.servlets;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.*;
import com.globus.common.servlets.*;
import com.globus.sales.beans.*;

/**
 * @author jkumar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GmSalesOrderDetailServlet  extends GmServlet{
	
	
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strSalesPath = GmCommonClass.getString("GMSALES");
		String strDispatchTo = strSalesPath.concat("/GmDetailedSalesReport.jsp");
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			RowSetDynaClass resultSet = null ;
			HashMap hmAccData = new HashMap();
			GmCommonClass gmCommon = new GmCommonClass();
			GmSalesReportBean gmSales = new GmSalesReportBean();
		
			String strFromDate 	= GmCommonClass.parseNull(request.getParameter("hFromDate"));
			String strToDate 		= GmCommonClass.parseNull(request.getParameter("hToDate"));
			String strAccId 			= GmCommonClass.parseNull(request.getParameter("hAccId"));
			String strAccName 	= GmCommonClass.parseNull(request.getParameter("hAccname"));
			String strDistId = GmCommonClass.parseNull(request.getParameter("hDistId")); // This data will be only available if the User clicks on the right arrow image next to Dist Name
			String strCondition =  getAccessCondition(request,response);
			String strAction = request.getParameter("hAction");
			strAction = (strAction == null)?"Load":strAction;

			hmAccData.put("strCondition",strCondition);
			hmAccData.put("strFromDate",strFromDate);
			hmAccData.put("strToDate",strToDate);
			hmAccData.put("strAccId",strAccId);
			hmAccData.put("strDistId",strDistId);
	
			
			if (strAction.equals("ViewSale") || strAction.equals("ViewDistDetails"))
			{
				resultSet = gmSales.reportOrderDetail(hmAccData);
				request.setAttribute("strAccId",strAccId);
				request.setAttribute("results",resultSet);
				dispatch(strSalesPath.concat("/GmSalesOrderDetails.jsp"),request,response);
			}
				
		}

catch (Exception e)	
{
	session.setAttribute("hAppErrorMessage",e.getMessage());
	strDispatchTo = strComnPath + "/GmError.jsp";
	gotoPage(strDispatchTo,request,response);
}

}
  	
}
