/*****************************************************************************
 * File : GmSalesYTDReportServlet Desc : This Servlet will be used for all Sales YTD Report
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmDetailStateSalesServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strOperPath = GmCommonClass.getString("GMSALES");
    String strDispatchTo = "";
    String strJSPName = "";
    String strDistributorID = "";
    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";

    String strShowArrowFl = "";
    String strShowPercentFl = "";
    String strHideAmount = "";
    String strHeader = "";
    String strFilterCondition = "";

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();


    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));

      GmCommonClass gmCommon = new GmCommonClass();
      GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();

      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmFromDate = new HashMap();
      HashMap hmTempParam = new HashMap();
      ArrayList alMonthDropDown = new ArrayList();
      ArrayList alYearDropDown = new ArrayList();

      String strCondition = null;

      strFromMonth = request.getParameter("Cbo_FromMonth");
      strFromYear = request.getParameter("Cbo_FromYear");
      strToMonth = request.getParameter("Cbo_ToMonth");
      strToYear = request.getParameter("Cbo_ToYear");

      // Get cookie only on onload for the first time and not on the Go button
      if (strFromMonth == null) {
        strFromMonth =
            strFromMonth == null ? gmCookieManager.getCookieValue(request, "cFromMonth")
                : strFromMonth;
        strFromYear =
            strFromYear == null ? gmCookieManager.getCookieValue(request, "cFromYear")
                : strFromYear;
        strToMonth =
            strToMonth == null ? gmCookieManager.getCookieValue(request, "cToMonth") : strToMonth;
        strToYear =
            strToYear == null ? gmCookieManager.getCookieValue(request, "cToYear") : strToYear;
      }

      strAction = (strAction.equals("")) ? "LoadStateSales" : strAction;

      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      HashMap hmSalesFilters = new HashMap();
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      request.setAttribute("hmSalesFilters", hmSalesFilters);

      // To get the Access Level information
      strCondition = getAccessCondition(request, response, true);

      gmFramework.getRequestParams(request, response, session, hmParam);
      String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
      if (strAction.equals("LoadStateSales")) {
        strHeader = "Sales By Sate";
        hmReturn =
            gmSales.loadSalesByState(strFromMonth, strFromYear, strToMonth, strToYear,
                strCondition, strCurrType);
        alMonthDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
        alYearDropDown = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());

        hmReturn.put("ALMONTHDROPDOWN", alMonthDropDown);
        hmReturn.put("ALYEARDROPDOWN", alYearDropDown);
        request.setAttribute("hmReturn", hmReturn);

        ArrayList alStateSalesList =
            GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("STATESALE"));
        ArrayList alColorList = generateColorRangeList(alStateSalesList);

        hmTempParam.put("COLORRANGE", alColorList);
        hmTempParam.put("COLORRANGESIZE", alColorList.size());
        hmTempParam.put("STATESALE", alStateSalesList);
        hmTempParam.put("APPLNCURRSIGN",
            GmCommonClass.parseNull((String) request.getAttribute("hCurrSymb")));
        hmTempParam.put("SESSCURRFMT",
            GmCommonClass.parseNull((String) session.getAttribute("strSessApplCurrFmt")));

        String strStateSalesData = gmSales.generateOutPut(hmTempParam, "GmDailySalesbyState.vm");
        request.setAttribute("STATESALESJSON", strStateSalesData);

        log.debug("outside:::" + strStateSalesData);

      }

      // To fetch the value for the unload condition
      hmFromDate = (HashMap) hmReturn.get("FromDate");
      if (hmFromDate.get("FirstTime") != null) {
        strFromMonth = (String) hmFromDate.get("FROMMONTH");
        strFromYear = (String) hmFromDate.get("FROMYEAR");
        strToMonth = (String) hmFromDate.get("TOMONTH");
        strToYear = (String) hmFromDate.get("TOYEAR");
      }

      request.setAttribute("Cbo_FromMonth", strFromMonth);
      request.setAttribute("Cbo_FromYear", strFromYear);
      request.setAttribute("Cbo_ToMonth", strToMonth);
      request.setAttribute("Cbo_ToYear", strToYear);

      strJSPName = "/GmStateSalesReport.jsp";

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cFromMonth", strFromMonth,
          GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cFromYear", strFromYear, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToMonth", strToMonth, GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cToYear", strToYear, GmCookieManager.MAX_AGE_8);

      dispatch(strOperPath.concat(strJSPName), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  /**
   * generateColorRangeList - This method is used to generate the USA State map color range
   * arraylist
   * 
   * @param alStateSalesList
   * @return ArrayList
   * @throws Exception
   * 
   */
  private ArrayList generateColorRangeList(ArrayList alStateSalesList) throws AppError {

    String[] arrColorRange = new String[4];
    String strColorFrmRules = GmCommonClass.getRuleValue("STATEMAPCOLOR", "STATESALES");
    arrColorRange = strColorFrmRules.split(",");

    int intStateSalesListSize = alStateSalesList.size();

    // calculate the base index values
    int intColorRange = intStateSalesListSize / 4;
    int intColorQuo = intStateSalesListSize % 4;
    int arrSize = (intColorQuo < 4 && intColorRange == 0) ? intColorQuo : 4;
    int arrColor[] = new int[arrSize];
    int minValInd = 0;
    int maxValInd = 0;
    double minVal = 0.00;
    double maxVal = 0.00;
    HashMap hmTemp = new HashMap();
    ArrayList alColorList = new ArrayList();
    HashMap hmColorMap = new HashMap();

    // Assigning the base index values in all 4 legends array.
    for (int i = 0; i < arrColor.length; i++) {
      arrColor[i] = intColorRange;
    }
    // Finding the difference betweeen the actual size values.
    int rangeDiff = intStateSalesListSize - (intColorRange * 4);

    // If there is any difference values, then loop through the array
    // and add respective index values with one.
    if (rangeDiff > 0) {
      for (int j = 0; j < rangeDiff; j++) {
        arrColor[j] = arrColor[j] + 1;
      }
    }
    // Now the array arrColor, is having details of all four legends (alStateSalesList index valuse)
    // Loop through this array & get the respective color code values with MIN, MAX values.
    // The color value is driven from rules.
    for (int i = 0; i < arrColor.length; i++) {
      hmColorMap = new HashMap();

      hmTemp = (HashMap) alStateSalesList.get(minValInd);
      hmColorMap.put("MINVAL", hmTemp.get("AMT"));

      maxValInd = arrColor[i] + minValInd;

      hmTemp = (HashMap) alStateSalesList.get(maxValInd - 1);
      hmColorMap.put("MAXVAL", hmTemp.get("AMT"));

      if (i == 0) {
        minValInd = minValInd + maxValInd;
      } else {
        minValInd = maxValInd;
      }

      hmColorMap.put("COLORCODE", arrColorRange[i]);
      alColorList.add(hmColorMap);
    }

    log.debug("::alColorList:::" + alColorList);
    // Fianl array is having the values in the below format.
    // {"minvalue":"21140","maxvalue":"117873","code": "#08088A","displayvalue":"21140 - 117873"}
    return alColorList;

  }

}// End of GmTerritoryServlet
