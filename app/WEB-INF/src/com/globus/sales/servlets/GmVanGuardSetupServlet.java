package com.globus.sales.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.prodmgmnt.beans.GmProdReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmBaselineValueBean;

public class GmVanGuardSetupServlet extends GmServlet {

    
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {        
        HttpSession session = request.getSession(false);
        String strComnPath = GmCommonClass.getString("GMCOMMON");
        String strProdPath = GmCommonClass.getString("GMSALES");
        String strDispatchTo = "/GmVanGuardList.jsp";
      
        try
        {
            checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
            GmProjectBean gmProjectBean = new GmProjectBean();
            ArrayList alVanguardParameters = new ArrayList();     
            ArrayList alVGuardType = new ArrayList();
            
            String strUserId =  (String)session.getAttribute("strSessUserId");
            String strAction = request.getParameter("hAction");
            alVGuardType = GmCommonClass.getCodeList("VGUAD");
            
            log.debug(" vguard values "+alVGuardType + " size " +alVGuardType.size() );
                
                if (strAction == null)
                {
                    strAction = (String)session.getAttribute("hAction");
                }
                strAction = (strAction == null)?"Load":strAction;
                
                if(strAction.equals("Save")) {
                     String strVGuardParameters = GmCommonClass.parseNull(request.getParameter("hVguardParameter"));
                     log.debug(" strVGuardParameters " + strVGuardParameters);
                     gmProjectBean.saveVanGuardList (strVGuardParameters,strUserId);
                }
                
                alVanguardParameters = gmProjectBean.loadVanGuardList();
                
                request.setAttribute("alVGuardType",alVGuardType);
                request.setAttribute("alVanguardParameters",alVanguardParameters);
                
                dispatch(strProdPath.concat(strDispatchTo),request,response);

        } //End of try
        catch (Exception e)
        {
                session.setAttribute("hAppErrorMessage",e.getMessage());
                e.printStackTrace();
                strDispatchTo = strComnPath + "/GmError.jsp";
                gotoPage(strDispatchTo,request,response);
        }// End of catch

}
}
