/*---------------------------------
 * Bean : GmCaseSchedulerCalBean.java 
 * Author : N Raja
 * Project : Globus Medical App 
 * Security: Unclassified 
 * Description: This Bean will be used to case scheduler calendar
 ----------------------------------------*/

package com.globus.sales.CaseManagement.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
public class GmCaseSchedulerCalBean extends GmBean{
	
    public GmCaseSchedulerCalBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * @description fetchCaseCalendar used for fetch calendar event
	 * @param hmParam
	 * @return alReturn
	 * @throws AppError
	 */
	
	public ArrayList fetchCaseSchedulerCal(HashMap hmParam) throws AppError {
				GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
			    if (gmDataStoreVO == null) {
			      gmDataStoreVO = getGmDataStoreVO();
			    }
			    ArrayList alReturn = new ArrayList();
			    StringBuffer sbQuery = new StringBuffer();
			    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
			    String strTerm = GmCommonClass.parseNull((String) hmParam.get("TERMDATA"));
			    Date dtFromDT = (Date) hmParam.get("FROMDT");
			    Date dtToDT = (Date) hmParam.get("TODT");
			    String strFromDate = GmCommonClass.getStringFromDate(dtFromDT, strDateFmt);
			    String strToDate = GmCommonClass.getStringFromDate(dtToDT, strDateFmt);
				sbQuery.append(" SELECT T7100.C7100_CASE_INFO_ID INFOID , T7100.C7100_CASE_ID CASE_ID,T7100.C901_CASE_STATUS STATUS, ");
				sbQuery.append(" TO_CHAR(T7100.C7100_SHIP_DATE, '"+strDateFmt+"' || ' HH:MM') START_DATE, ");
				sbQuery.append(" TO_CHAR(T7100.C7100_SURGERY_DATE,'"+strDateFmt+"' || ' HH:MM') SURGERY_DATE, ");
				sbQuery.append(" TO_CHAR(T7100.C7100_RETURN_DATE,'"+strDateFmt+"' || ' HH:MM') END_DATE, ");
				sbQuery.append(" GET_DISTRIBUTOR_NAME (T7100.C701_DISTRIBUTOR_ID) DISTNM, ");
				sbQuery.append(" GET_ACCOUNT_NAME (T7100.C704_ACCOUNT_ID) ACCOUNTNM , ");
				sbQuery.append(" GET_REP_NAME (T7100.C703_SALES_REP_ID) REPNM ");
				sbQuery.append(" FROM T7100_CASE_INFORMATION T7100 ");
				sbQuery.append(" WHERE T7100.C7100_SURGERY_DATE BETWEEN TO_DATE('"+strFromDate+"','"+strDateFmt+"')  AND TO_DATE('"+strToDate+"','"+strDateFmt+"') ");
				sbQuery.append(" AND T7100.C901_TYPE = '107285' ");
				sbQuery.append(" AND T7100.C901_CASE_STATUS != '107163' ");
				sbQuery.append(" AND T7100.C1900_COMPANY_ID = '" + gmDataStoreVO.getCmpid() + "' ");  
				if(strTerm.equals("shortTerm"))  
				sbQuery.append(" AND T7100.C7100_LONG_TERM_FL ='N' ");
				if(strTerm.equals("longTerm"))
				sbQuery.append(" AND T7100.C7100_LONG_TERM_FL='Y' ");
				if(strTerm.equals("viewAll"))  
				sbQuery.append(" ");	
				sbQuery.append(" AND C7100_VOID_FL IS NULL ORDER BY T7100.C7100_CASE_INFO_ID ");
				log.debug("case_Schedule_Calendar_query==" + sbQuery.toString());
				alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
				log.debug("case_Schedule_Calendar_query alReturn --> " +alReturn.size());
       return alReturn;
    }
}
