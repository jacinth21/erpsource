package com.globus.sales.CaseManagement.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseBuilderBean extends GmSalesFilterConditionBean{
	
	public GmCaseBuilderBean() {
		  super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	  
	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmCaseBuilderBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public ArrayList fetchFavoriteCase(HashMap hmParam) throws AppError {
		
		
		ArrayList alReturn = new ArrayList();
		String strOpt=GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		if(!strOpt.equals("EVENT"))
		{
		initializeParameters(hmParam);
		}
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		String strRep = GmCommonClass.parseNull((String) hmParam.get("REPID"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID")); 
		
		strRep = strRep.equals("")?strUserID:strRep; //while profile building the repid will be empty
	 
		sbQuery.append(" SELECT T7120.C7120_FAVOURITE_CASE_ID CODEID, T7120.C7120_FAVOURITE_CASE_NM CODENM ");
		sbQuery.append(" FROM T7120_FAVOURITE_CASE T7120 ");
		if(!strOpt.equals("EVENT"))
		{
		sbQuery.append(getAccessFilterClauseWithRepID());
		}
		sbQuery.append(" WHERE T7120.C7120_VOID_FL IS NULL ");
		sbQuery.append(" AND C7120_FAVOURITE_CASE_NM IS NOT NULL ");
		if(!strOpt.equals("EVENT"))
		{
		sbQuery.append(" AND T7120.c703_sales_rep_id   = V700.REP_ID "); 
		sbQuery.append(" AND t7120.c703_sales_rep_id ="+strRep);
		}else
		{
			sbQuery.append(" AND t7120.C7120_CREATED_BY ="+strUserID);
		}
    sbQuery.append(" AND t7120.c1900_COMPANY_ID = " +getCompId());
		sbQuery.append(" ORDER BY T7120.C7120_FAVOURITE_CASE_NM "); 

		log.debug(" Query to fetchFavoriteCase is " + sbQuery.toString());

		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString()); 

		return alReturn;
	}

	public void saveCaseBuilder(HashMap hmParam) throws AppError {
		String strCaseInfoIDfromDB = ""; 
	 
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 
		String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
		String strSelectedCategories = GmCommonClass.parseNull((String) hmParam.get("SELECTEDCATGR"));
		String strUserId = (String) hmParam.get("USERID"); 
		gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_case_builder_info", 3);
		 
		 	 
		gmDBManager.setString(1, strCaseInfoID);
		gmDBManager.setString(2, strSelectedCategories); 
		gmDBManager.setString(3, strUserId); 
		gmDBManager.execute();

	//	strCaseInfoIDfromDB = gmDBManager.getString(10);
		  
		gmDBManager.commit();
	//	return strCaseInfoIDfromDB;
	}
	
	
	public String saveCaseProfileBuilder(HashMap hmParam) throws AppError {
		String strFavCaseIDfromDB = ""; 
	 
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strFavCaseID = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASEID"));
		String strFavCaseNM = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASENM"));
		String strSelectedCategories = GmCommonClass.parseNull((String) hmParam.get("SELECTEDCATGR"));
		String strUserId = (String) hmParam.get("USERID"); 
		log.debug("hmParam>>>>>>>>>>>>>>>>>>>> is " + hmParam);
		
		gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_profile_builder", 5);
		gmDBManager.registerOutParameter(5,  java.sql.Types.CHAR); 	  
		 	 
		gmDBManager.setString(1, strFavCaseID);
		gmDBManager.setString(2, strFavCaseNM);
		gmDBManager.setString(3, strSelectedCategories);
		gmDBManager.setString(4, strUserId); 
		gmDBManager.execute();

		strFavCaseIDfromDB = gmDBManager.getString(5);
		  
		gmDBManager.commit();
	 	return strFavCaseIDfromDB;
	}
	 
	public HashMap fetchBookedCase(HashMap hmParam) throws AppError {
		HashMap hmReturn=new HashMap();
		HashMap hmResult=new HashMap();
		ArrayList alReturnAppr = new ArrayList();
		ArrayList alReturnReg = new ArrayList();
		
		initializeParameters(hmParam);
		
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer(); 
		String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
		
		sbQuery.append(" SELECT T7100.C7100_CASE_INFO_ID CASEINFOID, T7100.c901_case_status CASESTATUS, ");
		sbQuery.append(" T7100.c703_sales_rep_id REPID, get_case_attr_type('CASCAT', "+strCaseInfoID +") SELECTEDCATEGORIES "); 
		sbQuery.append(" FROM T7100_CASE_INFORMATION T7100 ");  
		sbQuery.append(getAccessFilterClauseWithRepID());
		sbQuery.append(" WHERE  T7100.c7100_case_info_id = "+strCaseInfoID);
		sbQuery.append(" AND T7100.C7100_VOID_FL IS NULL ");
		sbQuery.append(" AND t7100.C901_type = 1006503 ");
		sbQuery.append(" AND T7100.c703_sales_rep_id   = V700.REP_ID ");  
		
		log.debug(" Query to fetchBookedCase is " + sbQuery.toString());
		hmResult = gmDBManager.querySingleRecord(sbQuery.toString());  
		 
		return hmResult;
	}
	
	public HashMap fetchFavoriteDtls(HashMap hmParam) throws AppError { 
		
		HashMap hmReturn=new HashMap();
		HashMap hmResult=new HashMap();
		ArrayList alReturnAppr = new ArrayList();
		ArrayList alReturnReg = new ArrayList();
		String strGroup = "CASCAT";
		String strOpt=GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID")); 
		
		if(!strOpt.equals("EVENT"))
		{
		initializeParameters(hmParam);
		}else
		{
			strGroup = "EVEMET";
		}
				  
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer(); 
		 
		String strFavCaseID = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASEID"));
		strFavCaseID = strFavCaseID.equals("")? "-9999" : strFavCaseID;
		sbQuery.append(" SELECT T7120.c7120_favourite_case_id favouriteCaseID,T7120.c7120_favourite_case_nm favouriteCaseNM, ");
		sbQuery.append(" get_favcase_attr_type('"+strGroup+"', "+strFavCaseID +") SELECTEDCATEGORIES "); 
		sbQuery.append(" FROM T7120_FAVOURITE_CASE T7120 ");  
		if(!strOpt.equals("EVENT"))
		{
		sbQuery.append(getAccessFilterClauseWithRepID());
		}
		sbQuery.append(" WHERE  T7120.c7120_favourite_case_id = "+strFavCaseID);
		sbQuery.append(" AND T7120.C7120_VOID_FL IS NULL ");
		if(!strOpt.equals("EVENT"))
		{
		sbQuery.append(" AND T7120.c703_sales_rep_id   = V700.REP_ID ");  
		}
		else if (!strUserID.equals(""))
		{
			sbQuery.append(" AND T7120.c7120_created_by   =  " + strUserID );  
		}
		log.debug(" Query to fetchFavoriteDtls is " + sbQuery.toString());
		hmResult = gmDBManager.querySingleRecord(sbQuery.toString());  
		 
		return hmResult;
	}
	
	public void saveFavSets(HashMap hmParam) throws AppError {
		String strCaseInfoIDfromDB = ""; 
	 
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 
		String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
		String strFavCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASEID"));
	 
		String strUserId = (String) hmParam.get("USERID"); 
		gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_fav_over_casesets", 3);
		 
		 	 
		gmDBManager.setString(1, strFavCaseInfoID);
		gmDBManager.setString(2, strCaseInfoID);
		gmDBManager.setString(3, strUserId);   
		gmDBManager.execute();

	//	strCaseInfoIDfromDB = gmDBManager.getString(10);
		  
		gmDBManager.commit();
	//	return strCaseInfoIDfromDB;
	}

}
