package com.globus.sales.CaseManagement.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseSchedularBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public GmCaseSchedularBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	public GmCaseSchedularBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
	}

/**
 * saveCaseDetails This method used to save Case Schedular details tab 
 * 
 * @param HashMap hmParam
 * @return String
 * @exception  Exception
 */
	public String saveCaseDetails(HashMap hmParam) throws Exception{
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strDateFormat = "";
		String strCompanyId ="";
		String strSurDt = GmCommonClass.parseNull((String) hmParam.get("SURGERYDT"));
		String strShipDt = GmCommonClass.parseNull((String) hmParam.get("SHIPDT"));
		String strReturnDt = GmCommonClass.parseNull((String) hmParam.get("RETDT"));
		String strDivision = GmCommonClass.parseNull((String) hmParam.get("DIVISION"));
		String strFieldSales = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTER"));
		String strSalesRep = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
		String strAcc = GmCommonClass.parseNull((String) hmParam.get("ACCOUNT"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strLongTerm = GmCommonClass.parseNull((String) hmParam.get("LONGTERM"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		strDateFormat = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());		
		strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		if(strDateFormat.equals("") || strCompanyId.equals("")){
			strDateFormat = GmCommonClass.parseNull((String) hmParam.get("COMPANYDTFMT"));
			strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));				
		}	
		String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
		Date strSurDate = GmCommonClass.getStringToDate(strSurDt,strDateFormat);
		Date strShipDate = GmCommonClass.getStringToDate(strShipDt,strDateFormat);
		Date strReturnDate = GmCommonClass.getStringToDate(strReturnDt,strDateFormat);

		gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_save_case_details", 12);
		gmDBManager.setDate(1, strSurDate);	 
		gmDBManager.setDate(2, strShipDate);
		gmDBManager.setDate(3, strReturnDate);
		gmDBManager.setString(4, strDivision);
		gmDBManager.setString(5, strFieldSales);
		gmDBManager.setString(6, strSalesRep);
		gmDBManager.setString(7, strAcc);
		gmDBManager.setString(8, strUserId);
		gmDBManager.setString(9, strStatus);
		gmDBManager.setString(10, strCaseId);
		gmDBManager.setString(11, strCompanyId);
		gmDBManager.setString(12, strLongTerm);
		gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
	 
		gmDBManager.execute();
		String strCaseID = GmCommonClass.parseNull(gmDBManager.getString(10));
		if (!strLogReason.equals("")) {
			GmCommonBean gmCommonBean = new GmCommonBean();
			log.debug("strCaseID==>>>>>>>"+strCaseID);
			gmCommonBean.saveLog(gmDBManager, strCaseID, strLogReason, strUserId, "26240507");  // 26240507 - Log Code Number for Case Scheduler Details comments.
		}
		gmDBManager.commit();
		return strCaseID;	
	}
/**
 * fetchKitDetails This method used to fetch the kit details for inventory selection screen 
 * 
 * @param String strKitName
 * @return HashMap
 * @exception  AppError, Exception
 */
	public HashMap fetchKitDetails(String strKitName,String strCaseId) throws AppError {
  		ArrayList alList = new ArrayList();
  		HashMap hmParam = new HashMap();
  		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
  		gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_fetch_kit_details", 4);
  		gmDBManager.setString(1, strKitName);
  		gmDBManager.setString(4, strCaseId);
  		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
  		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
  		gmDBManager.execute();
  		alList  = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
  		String strKitStatus = gmDBManager.getString(3);
  		hmParam.put("ALLIST", alList);
  		hmParam.put("STRKITSTATUS", strKitStatus);
  		log.debug("alList==>"+alList.size());
  		gmDBManager.close();
  		return hmParam;
	} 
/**
 * saveInvSelection This method used to save inventory selection details
 * 
 * @param HashMap hmParam
 * @return 
 * @exception  AppError, Exception
 */
	public String saveInvSelection(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String caseid = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
		String kitid = GmCommonClass.parseNull((String) hmParam.get("KITID"));
		String userid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_save_inv_selection",5);
		gmDBManager.setString(1, caseid);
		gmDBManager.setString(2, kitid);
		gmDBManager.setString(3, userid);
		gmDBManager.setString(4, strCompanyId);
		gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
		gmDBManager.execute();
		String strStatus = gmDBManager.getString(5);
		if (!strLogReason.equals("")) {
			GmCommonBean gmCommonBean = new GmCommonBean();
			gmCommonBean.saveLog(gmDBManager, caseid, strLogReason, userid, "26240508");  // 26240508 - Log Code Number for Case Scheduler Inv Comments.
		}
		gmDBManager.commit();
		return strStatus;
	} 
/**
 * saveCaseNotes This method used to save case notes
 * 
 * @param HashMap hmParam
 * @return 
 * @exception  AppError, Exception
 */
	public void saveCaseNotes(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String caseid = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_save_case_notes",4);
		gmDBManager.setString(1, caseid);
		gmDBManager.setString(2, strUserId);
		gmDBManager.setString(3, strLogReason);
		gmDBManager.setString(4, strCompanyId);
		gmDBManager.execute();
			if (!strLogReason.equals("")) {
				GmCommonBean gmCommonBean = new GmCommonBean();
				gmCommonBean.saveLog(gmDBManager, caseid, strLogReason, strUserId, "107320");  // 107320 - Log Code Number for Case Scheduler Notes.
			}
		gmDBManager.commit();
	} 
/**
 * fetchCaseDetails This method used to fetch case details tab based on case id
 * 
 * @param HashMap hmParam
 * @return HashMap
 * @exception  AppError, Exception
 */
	public HashMap fetchCaseDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompanyId = "";
		String caseid = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
		strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());  
		if(strCompanyId.equals("")){
			  strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		 }
		gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_fetch_case_details",3);			
		gmDBManager.setString(1, caseid);
		gmDBManager.setString(2, strCompanyId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmParam  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
	  	gmDBManager.close();
		return hmParam;
	} 	
	
/**
 * fetchInvSelection This method used to fetch the inventory selection details
 * 
 * @param String strCaseId
 * @return ArrayList
 * @exception  AppError, Exception
 */
	public ArrayList fetchInvSelection(String strCaseId) throws AppError {
		 ArrayList alList = new ArrayList();
		 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_fetch_inv_selection", 2);
		 gmDBManager.setString(1, strCaseId);
		 gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		 gmDBManager.execute();
		 alList  = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		 log.debug("alList==>"+alList.size());
		 gmDBManager.close();
		 return alList;
	} 
	
/**
 * removeKit This method used remove the kit details from case inventory selection tab
 * 
 * @param String strKitId, String strCaseId, HashMap hmParam
 * @return HashMap
 * @exception  AppError, Exception
 */
	public HashMap removeKit(String strKitId, String strCaseId,HashMap hmParam) throws AppError {
		 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 String strCompanyId =  "";
		 HashMap hmValues = new HashMap();
		 strCompanyId =  GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		 if(strCompanyId.equals("")) {
			 strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));	 
		 }	
		 String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		 String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		 gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_remove_kit", 6);
		 gmDBManager.setString(1, strKitId); 
		 gmDBManager.setString(2, strCaseId);
		 gmDBManager.setString(3, strCompanyId);
		 gmDBManager.setString(4, strUserId);
		 gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
		 gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
		 gmDBManager.execute();
		 String strCount = GmCommonClass.parseNull(gmDBManager.getString(5)); 
		 String strCheckDate = GmCommonClass.parseNull(gmDBManager.getString(6));
		 hmValues.put("strCount", strCount);
		 hmValues.put("strCheckDate", strCheckDate);
		 if(!(strCheckDate.equals("Y") || strCount.equals("1"))){
			 if (!strLogReason.equals("")) {
					GmCommonBean gmCommonBean = new GmCommonBean();
					gmCommonBean.saveLog(gmDBManager, strCaseId, strLogReason, strUserId, "26240508");  // 26240508 - Log Code Number for Case Scheduler Inv Comments.
				}
		 }
		 gmDBManager.commit();
		 return hmValues;
	} 	
/**
 * checkCaseStatus This method used to check case status
 * 
 * @param String strCaseID
 * @return HashMap
 * @exception  Exception
 */
	public HashMap checkCaseStatus(String strCaseID, String strReturnDate,String strCmpDtFmt) throws Exception{
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmVal = new HashMap();
		String strCompanyId =  GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());	
		log.debug("strReturnDate==>"+strReturnDate+" strCmpDtFmt==>"+strCmpDtFmt);
		Date strRetDate = GmCommonClass.getStringToDate(strReturnDate,strCmpDtFmt);
		gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_check_case_status", 6);
		gmDBManager.setString(1, strCaseID);
		gmDBManager.setString(3, strCompanyId);
		gmDBManager.setDate(5, strRetDate);
		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
		gmDBManager.execute();
		String strCaseStatus = GmCommonClass.parseNull(gmDBManager.getString(2));
		String strCheckInv = GmCommonClass.parseNull(gmDBManager.getString(4));
		String strReturnValues = GmCommonClass.parseNull(gmDBManager.getString(6));
		hmVal.put("STRCASESTATUS", strCaseStatus);
		hmVal.put("STRCHECKINV", strCheckInv);
		hmVal.put("STRRETURNVALUES", strReturnValues);
		gmDBManager.close();
		return hmVal;	
	}	 
/**
 * updateCaseSchedulerJMS - This method used to update tag details
 * for the Orders
 * 
 * @param - HashMap:hmParam
 * @return - Void
 * @exception - AppError
 */
	public void updateCaseSchedulerJMS(HashMap hmParam) throws AppError {
	    
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();	      
	    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_TAG_QUEUE"));
	    hmParam.put("QUEUE_NAME", strQueueName);
	    
	      // ORDER_CONSUMER_CLASS is mentioned in JMSConsumerConfig. properties file.This key contain the
	      // path for : com.globus.jms.consumers.processors.GmCaseSchedulerTagConsumerJob
	    String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("CASE_SCHEDULER_TAG_CONSUMER_CLASS"));
	    hmParam.put("HMHOLDVALUES", hmParam);
	    hmParam.put("CONSUMERCLASS", strConsumerClass);
	    log.debug("strQueueName=>"+strQueueName+" strConsumerClass=>"+strConsumerClass+" hmParam=>>>"+hmParam);
	    gmConsumerUtil.sendMessage(hmParam);      
	}
	      
/**
 * updateSetDeatails This method used to update tag details
 * 
 * @param strCaseId,strKitId, strUserId
 * @return 
 * @exception  AppError, Exception
 */
	public void updateSetDeatails(String strCaseId,String strKitId,String strStatus,String strUserId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompanyId =  GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		gmDBManager.setPrepareString("gm_pkg_case_schedular.gm_save_set_details",5);
		gmDBManager.setString(1, strCaseId);
		gmDBManager.setString(2, strKitId);
		gmDBManager.setString(3, strStatus);
		gmDBManager.setString(4, strUserId);
		gmDBManager.setString(5, strCompanyId);
		gmDBManager.execute();
		gmDBManager.commit();
	} 		


/**
 * loadFieldSalesList - This method to fetch the field sales information based on user search
 * 
 * @param strKey
 * @param searchPrefix
 * @param iMaxCount
 * @return
 * @throws AppError
 */
public String loadFieldSalesList(String strKey, String searchPrefix, int iMaxCount,HashMap hmParam)
	      throws AppError {
	    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	    log.debug(" Key values is " + strKey);
	    log.debug(" searchPrefix values is " + searchPrefix);
	    gmCacheManager.setKey(strKey);
	    gmCacheManager.setSearchPrefix(searchPrefix);
	    gmCacheManager.setMaxFetchCount(iMaxCount);
	    String strReturn = gmCacheManager.returnJsonString(getDistributorQueryStr(hmParam));

	    return strReturn;
	  }

public String getDistributorQueryStr(HashMap hmParam) throws AppError {
  String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
  String strDistTyp = GmCommonClass.parseNull((String) hmParam.get("DISTTYP"));
  String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
  String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
  String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));

  StringBuffer sbQuery = new StringBuffer();
  sbQuery
      .append(" SELECT  C701_DISTRIBUTOR_ID ID,DECODE('"
          + getComplangid()
          + "', '103097',NVL(C701_DISTRIBUTOR_NAME_EN, C701_DISTRIBUTOR_NAME), C701_DISTRIBUTOR_NAME) NAME, ");
  sbQuery
      .append(" LOWER (DECODE('"
          + getComplangid()
          + "', '103097',NVL(C701_DISTRIBUTOR_NAME_EN, C701_DISTRIBUTOR_NAME), C701_DISTRIBUTOR_NAME)) distname ");
  sbQuery.append(" FROM T701_DISTRIBUTOR ");
  // Instead of function we are getting the division id from master table
  sbQuery.append(" ,(SELECT DISTINCT t710_tmp.c901_area_id, t710_tmp.c901_division_id ");
  sbQuery.append(" FROM t710_sales_hierarchy t710_tmp ");
  sbQuery.append(" WHERE t710_tmp.c710_active_fl = 'Y' ");
  sbQuery.append(" AND t710_tmp.c710_void_fl IS NULL) t710 ");
  sbQuery.append(" WHERE C701_VOID_FL IS NULL AND (c901_ext_country_id is NULL ");
  sbQuery.append(" OR c901_ext_country_id  in (select country_id from v901_country_codes))");
  if (strFilter.equals("Active")) {
    sbQuery.append(" AND ");
    sbQuery.append(" C701_ACTIVE_FL = 'Y'");
  }
  if (!strDistTyp.equals("")) {
    sbQuery.append(" AND c901_distributor_type = ");
    sbQuery.append(strDistTyp);
  }
  if (!strShipTyp.equals("")) {
    sbQuery.append(" AND C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120," + strRepID
        + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='" + strRepID
        + "'))) ");

  }
  sbQuery.append(" AND c701_region = t710.c901_area_id (+) ");
  // Filtering Distributor based on all the companies associated with the plant.
  if (strDistFilter.equals("COMPANY-PLANT")) {
    sbQuery
        .append(" AND ( C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5040_PLANT_ID = '"
            + getCompPlantId() + "'  AND C5041_VOID_FL IS NULL )");
    sbQuery.append(" OR C1900_COMPANY_ID IS NULL )  ");
    sbQuery
        .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
    sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
        + getCompId() + "') ");

  } else {
    sbQuery.append("  AND ( C1900_COMPANY_ID IS NULL OR C1900_COMPANY_ID = '" + getCompId()
        + "') ");
    sbQuery
        .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
    sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
        + getCompId() + "') ");
  }
  sbQuery.append(" ORDER BY distname ");
  log.debug(" Working changes " + sbQuery.toString());
  return sbQuery.toString();
}

/**
 * This method returns an ArrayList containing Comments and other information Entered by the user
 * 
 * @param strID - Code id to be added to the filter
 * @param strType - to pass the type of transaction
 * @exception AppError
 * @return ArrayList object
 */
public ArrayList getLog(String strID, String strType,String gmCmpDtFmt) throws AppError {
  return getLog(strID, strType, gmCmpDtFmt,"");
}

public ArrayList getLog(String strID, String strType, String gmCmpDtFmt, String strJNDIConnection) throws AppError {
  ArrayList arList = new ArrayList();
  GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
  String strQuery = "";

  strQuery = this.getLogQuery(strID, strType,gmCmpDtFmt);

  arList = gmDBManager.queryMultipleRecords(strQuery);

  return arList;
}
private String getLogQuery(String strID, String strType,String gmCmpDtFmt) {
    StringBuffer sbQuery = new StringBuffer();
    String strIdParsed = GmCommonClass.getStringWithQuotes(strID);
    sbQuery.append("SELECT  GET_USER_NAME(C902_CREATED_BY) UNAME, C902_COMMENTS COMMENTS, ");
    sbQuery.append(" TO_CHAR(C902_CREATED_DATE,'" + gmCmpDtFmt + "'|| ':HH:MI:SS') DT ");
    sbQuery
        .append(" ,C902_REF_ID  ID, GET_CODE_NAME(C902_TYPE) COMMENTTYPE, C902_LOG_ID LOGID, GET_AUDIT_LOG_FLAG(c902_log_id,'902') AUDIT_LOG ");
    sbQuery.append(" FROM  T902_LOG ");
    if (strType != null && strType.equals("1200")) { // 1200 Orders LOG
      sbQuery
          .append(" WHERE  C902_REF_ID IN (SELECT c501_order_id FROM t501_order  WHERE (c501_ref_id  IN ('"
              + strIdParsed
              + "') OR c501_order_id  IN ('"
              + strIdParsed
              + "' )) AND c501_void_fl IS NULL ");
      sbQuery.append(" UNION ALL SELECT c501_ref_id FROM t501_order WHERE (c501_ref_id  IN ('"
          + strIdParsed + "') OR c501_order_id  IN ('" + strIdParsed
          + "' )) AND c501_void_fl IS NULL ) ");
    } else {
      sbQuery.append(" WHERE  C902_REF_ID IN ('" + strIdParsed + "')");
    }

    if (strType != null) {
      sbQuery.append(" AND C902_TYPE = " + strType);
    }
    sbQuery.append(" AND C902_VOID_FL IS NULL  ORDER BY C902_LOG_ID desc");
    log.debug("sbQuery.toString()"+sbQuery.toString());
    return sbQuery.toString();
  }
/**
 * method fecth Account List Names
 * 
 * @return ArrayList
 * @Param String
 * @throws AppError
 */	
public ArrayList loadAccountList(String strCondition) throws AppError {

	    ArrayList alReturn = new ArrayList();

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();

	    sbQuery
	        .append(" SELECT    DISTINCT AC_ID ID,AC_NAME NAME, AC_ID||'-'|| AC_NAME NM, AD_ID ADID ");
	    sbQuery.append(" , VP_ID VPID, D_ID DID, REP_ID repid, REP_NAME rname ");
	    sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700, T704_ACCOUNT T704  ");

	    // Filter Condition to fetch record based on access code
	    if (!strCondition.toString().equals("")) {
	      sbQuery.append(" WHERE ");
	      sbQuery.append(strCondition);
	      sbQuery.append(" and ac_id is not null  ");
	    } else {
	      sbQuery.append("where ac_id is not null  ");
	    }
	    sbQuery.append(" AND v700.ac_id = c704_account_id ");
	    sbQuery.append(" AND c704_active_fl = 'Y' ");
	    sbQuery.append(" AND c1900_company_id = '"+getCompId()+"' AND (T704.c901_ext_country_id is NULL ");
	    sbQuery.append(" OR T704.c901_ext_country_id  in (select country_id from v901_country_codes))");
	    sbQuery.append(" ORDER BY AC_NAME ");
	    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
	    
	    return alReturn;
	  }

}   // End of GmCaseSchedularBean