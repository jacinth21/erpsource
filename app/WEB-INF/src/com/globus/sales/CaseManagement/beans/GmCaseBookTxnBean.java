package com.globus.sales.CaseManagement.beans;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.sales.InvAllocation.actions.GmLoanerRequestApprlAction;
import com.globus.sales.InvAllocation.beans.GmLoanerRequestApprovalBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.sales.event.beans.GmEventReportBean;
import com.globus.sales.event.beans.GmEventSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.request.GmCaseAttributeVO;
import com.globus.valueobject.operations.request.GmCaseInfoVO;
import com.globus.valueobject.operations.request.GmCaseSetInfoVO;
import com.globus.valueobject.operations.request.GmPartDetailVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

public class GmCaseBookTxnBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmCaseBookTxnBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmCaseBookTxnBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public HashMap saveCaseInfo(HashMap hmParam) throws AppError {
    String strCaseInfoIDfromDB = "";
    String strCaseIDfromDB = "";
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    Date dtSurgeryDate = (Date) hmParam.get("SURGERYDATE");
    String strSurgeryTimeHour =
        GmCommonClass.getCodeNameFromCodeId((String) hmParam.get("SURGERYTIMEHOUR"));
    String strSurgeryTimeMin =
        GmCommonClass.getCodeNameFromCodeId((String) hmParam.get("SURGERYTIMEMIN"));
    String strSurgeryAMPM =
        GmCommonClass.getCodeNameFromCodeId((String) hmParam.get("SURGERYAMPM"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strDistributorId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
    String strPersonalNote = GmCommonClass.parseNull((String) hmParam.get("PERSONALNOTE"));
    String strUserId = (String) hmParam.get("USERID");
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_case_info", 12);
    log.debug("strCaseInfoID-----------------" + strCaseInfoID);
    gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);

    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setDate(2, dtSurgeryDate);
    gmDBManager.setString(3, strSurgeryTimeHour);
    gmDBManager.setString(4, strSurgeryTimeMin);
    gmDBManager.setString(5, strSurgeryAMPM);
    gmDBManager.setString(6, strRepId);
    gmDBManager.setString(7, strAccountId);
    gmDBManager.setString(8, strDistributorId);
    gmDBManager.setString(9, strPersonalNote);
    gmDBManager.setString(10, strUserId);
    gmDBManager.execute();

    strCaseInfoIDfromDB = gmDBManager.getString(11);
    strCaseIDfromDB = gmDBManager.getString(12);

    gmDBManager.commit();

    hmReturn.put("CASEID", strCaseIDfromDB);
    hmReturn.put("CASEINFOID", strCaseInfoIDfromDB);

    return hmReturn;
  }


  public void saveCaseSetsInfo(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveCaseSetsInfo(hmParam, gmDBManager);
    gmDBManager.commit();
  }

  public void saveCaseSetsInfo(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String screen = GmCommonClass.parseNull((String) hmParam.get("FROMSCREEN"));
    String strProdType = GmCommonClass.parseNull((String) hmParam.get("PRODUCTIONTYPE"));
    String strCaseDtlStr = GmCommonClass.parseNull((String) hmParam.get("CASEDETINPUTSTR"));
    String strCaseShipDtlStr = GmCommonClass.parseNull((String) hmParam.get("CASESHIPINPUTSTR"));
    log.debug(" screen is............. " + screen);

    String strUserId = (String) hmParam.get("USERID");
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_case_sets_info", 6);

    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strProdType);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strCaseDtlStr);
    gmDBManager.setString(6, strCaseShipDtlStr);
    gmDBManager.execute();
  }

  public void saveCaseProfileSets(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    String strFavCaseID = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASEID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String screen = GmCommonClass.parseNull((String) hmParam.get("FROMSCREEN"));
    String strProdType = GmCommonClass.parseNull((String) hmParam.get("PRODUCTIONTYPE"));


    String strUserId = (String) hmParam.get("USERID");
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_profile_sets", 4);

    gmDBManager.setString(1, strFavCaseID);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strProdType);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }


  public void saveCasePostInfo(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));

    String strUserId = (String) hmParam.get("USERID");
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_case_post_info", 2);

    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public String saveRescheduleCase(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCaseInfoIDfromDB = "";
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    Date dtSurgeryDate = (Date) hmParam.get("SURGERYDATE");
    String strSurgeryTimeHour =
        GmCommonClass.getCodeNameFromCodeId((String) hmParam.get("SURGERYTIMEHOUR"));
    String strSurgeryTimeMin =
        GmCommonClass.getCodeNameFromCodeId((String) hmParam.get("SURGERYTIMEMIN"));
    String strSurgeryAMPM =
        GmCommonClass.getCodeNameFromCodeId((String) hmParam.get("SURGERYAMPM"));
    String strUserId = (String) hmParam.get("USERID");
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_save_resch_case", 7);

    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setDate(2, dtSurgeryDate);
    gmDBManager.setString(3, strSurgeryTimeHour);
    gmDBManager.setString(4, strSurgeryTimeMin);
    gmDBManager.setString(5, strSurgeryAMPM);
    gmDBManager.setString(6, strUserId);
    gmDBManager.execute();
    strCaseInfoIDfromDB = gmDBManager.getString(7);
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strCaseInfoID, strLogReason, strUserId, "1266"); // 1266 -
                                                                                         // Log Code
                                                                                         // Number
                                                                                         // for case
                                                                                         // book
                                                                                         // reschedule
    }
    gmDBManager.commit();
    return strCaseInfoIDfromDB;
  }

  public void saveFavouriteCase(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strCaseID = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
    String strFavouriteNm = GmCommonClass.parseNull((String) hmParam.get("FAVOURITENAME"));
    String strUserId = (String) hmParam.get("USERID");
    log.debug("strUserId" + strUserId);
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_favourite_case", 4);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setString(2, strCaseID);
    gmDBManager.setString(3, strFavouriteNm);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveRequestDetail This is main method calling from MIM resource & it will Save the Case Request
   * Details from device.
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError, ParseException, Exception
   */
  public HashMap saveRequestDetail(HashMap hmParam) throws AppError, ParseException, Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean(getGmDataStoreVO());
    GmEventReportBean gmEventReportBean = new GmEventReportBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());

    // Save case info's.
    HashMap hmDetails = saveRequestCaseInfo(gmDBManager, hmParam);

    String strRqtType = GmCommonClass.parseNull((String) hmDetails.get("TYPE"));
    String strCaseId = GmCommonClass.parseNull((String) hmDetails.get("CASEIDFROMDB"));
    String strCaseInfoId = GmCommonClass.parseNull((String) hmDetails.get("CASEINFOIDFROMDB"));
    hmParam.put("CASEIDFROMDB", strCaseId);
    hmParam.put("CASEINFOIDFROMDB", strCaseInfoId);
    hmParam.put("CASEINFOID", strCaseInfoId);
    hmParam.put("TYPE", strRqtType);

    // Save case attribute info's.
    saveRequestCaseAttribute(gmDBManager, hmParam);

    // Save case request item info
    saveRequestSetItemInfo(gmDBManager, hmParam);

    // Save event request details info
    hmParam.put("SAVESHIPDETAILS", "NO");
    gmEventSetupBean.saveEventRequestDetails(hmParam, gmDBManager);

    // Update ship info from temp table
    saveRequestShipDetails(gmDBManager, hmParam);

    gmDBManager.commit();

    // saveApproveSetItem(gmDBManager, hmParam);

    return hmParam;
  }

  /**
   * saveRequestCaseInfo This overloaded method will Save the Case info Details.
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError, ParseException, Exception
   */
  public HashMap saveRequestCaseInfo(GmDBManager gmDBManager, HashMap hmParam) throws AppError,
      ParseException {
    HashMap hmReturn = new HashMap();
    GmCaseInfoVO gmCaseInfoVO = (GmCaseInfoVO) hmParam.get("GMCASEINFOVO");

    GmWSUtil gmWSUtil = new GmWSUtil();
    hmParam = gmWSUtil.getHashMapFromVO(gmCaseInfoVO);

    hmReturn = saveRequestInfo(gmDBManager, hmParam);
    return hmReturn;
  }

  public void saveRequestInfo(HashMap hmParam) throws AppError, ParseException {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveRequestInfo(gmDBManager, hmParam);
    gmDBManager.commit();
  }

  public HashMap saveRequestInfo(GmDBManager gmDBManager, HashMap hmParam) throws AppError,
      ParseException {

    HashMap hmReturn = new HashMap();

    String strCaseID = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
    String strParentCaseID = GmCommonClass.parseNull((String) hmParam.get("PRTCAID"));
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CAINFOID"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACID"));
    String strSurgeryDate = GmCommonClass.parseNull((String) hmParam.get("SURDT"));
    String strSurgeryTime = GmCommonClass.parseNull((String) hmParam.get("SURTIME"));
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DID"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strCaseStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strCaseType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strCaseNotes = GmCommonClass.parseNull((String) hmParam.get("PRNOTES"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSkipFl = GmCommonClass.parseNull((String) hmParam.get("SKIPVALIDATIONFL"));
    String strDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());

    if (strDateFmt.equals(""))
      strDateFmt = GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT");

    Date dtSurgeryDate = null;
    if (!strSurgeryDate.equals("")) {
      dtSurgeryDate = GmCommonClass.getStringToDate(strSurgeryDate, strDateFmt);
    }

    gmDBManager.setPrepareString("gm_pkg_op_inv_mgt_request_txn.gm_sav_request_info", 13);
    gmDBManager.setString(1, strCaseID);
    gmDBManager.setString(2, strCaseInfoID);
    if (dtSurgeryDate == null) {
      gmDBManager.setDate(3, null);
    } else {
      gmDBManager.setDate(3, new java.sql.Date(dtSurgeryDate.getTime()));
    }
    gmDBManager.setString(4, strSurgeryTime);
    gmDBManager.setString(5, strDistID);
    gmDBManager.setString(6, strRepID);
    gmDBManager.setString(7, strAccountID);
    gmDBManager.setString(8, strCaseStatus);
    gmDBManager.setString(9, strCaseType);
    gmDBManager.setString(10, strCaseNotes);
    gmDBManager.setString(11, strUserID);
    gmDBManager.registerOutParameter(12, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(13, OracleTypes.VARCHAR);

    gmDBManager.execute();

    String strCaseId = GmCommonClass.parseNull(gmDBManager.getString(12));
    String strCaseInfoId = GmCommonClass.parseNull(gmDBManager.getString(13));

    hmParam.put("CASEIDFROMDB", strCaseId);
    hmParam.put("TYPE", strCaseType);
    hmParam.put("CASEINFOIDFROMDB", strCaseInfoId);

    return hmParam;

  } // End of saveRequestInfo

  /**
   * saveRequestCaseAttribute This method will Save the Case attribute Details.
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   */
  public void saveRequestCaseAttribute(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCaseInfoVO gmCaseInfoVO = (GmCaseInfoVO) hmParam.get("GMCASEINFOVO");

    // Convert VOs to DB input String
    GmCaseAttributeVO[] arrGmCaseAttributeVO = gmCaseInfoVO.getArrGmCaseAttributeVO();

    String strCaseAttributeInputstr =
        gmWSUtil.getDbInputStringFromVO(arrGmCaseAttributeVO, "^", "|",
            GmCaseAttributeVO.getSaveCaseAttributeProperties(), "getSaveCaseAttributeProperties");

    hmParam.put("CASEATTRINPUTSTR", strCaseAttributeInputstr);
    saveCaseAttribute(gmDBManager, hmParam);
  }

  /**
   * saveRequestCaseAttribute This method will Save the Case attribute Details.
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   */
  public void saveRequestCaseAttribute(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveRequestCaseAttribute(gmDBManager, hmParam);
    gmDBManager.commit();
  }

  /**
   * saveRequestCaseAttribute This overloaded method will Save the attribute Details.
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   */
  public void saveCaseAttribute(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveCaseAttribute(gmDBManager, hmParam);
    gmDBManager.commit();
  }

  public void saveCaseAttribute(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_case_attr", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("CASEINFOIDFROMDB")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("CASEATTRINPUTSTR")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.execute();
  } // End of saveCaseAttribute

  /**
   * saveRequestSetItemInfo This method will Save the set / Item Details.
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   */
  public void saveRequestSetItemInfo(GmDBManager gmDBManager, HashMap hmParam) throws AppError {

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmEventReportBean gmEventReportBean = new GmEventReportBean(getGmDataStoreVO());
    GmCaseInfoVO gmCaseInfoVO = (GmCaseInfoVO) hmParam.get("GMCASEINFOVO");
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCaseType = GmCommonClass.parseNull(gmCaseInfoVO.getType());
    String strProdType = "";
    String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEIDFROMDB"));
    String strComments = GmCommonClass.parseNull(gmCaseInfoVO.getPrnotes());
    String strAssRepId = GmCommonClass.parseNull(gmCaseInfoVO.getAsrepid());
    String strUserId = GmCommonClass.parseNull(gmCaseInfoVO.getUserid());
    String strLogType = "4000760";// 4000760 - Code Id; Item Consignment
    // Convert VOs to DB input String
    GmCaseSetInfoVO[] arrGmCaseSetInfoVO = gmCaseInfoVO.getArrGmCaseSetInfoVO();
    int setlength = 0;
    if (arrGmCaseSetInfoVO != null) {
      setlength = arrGmCaseSetInfoVO.length;
    }

    if (setlength > 0) {
      for (int i = 0; i < setlength; i++) {

        String strReqDt = GmCommonClass.parseNull(arrGmCaseSetInfoVO[i].getReqdt());
        String strPlanShipDt = GmCommonClass.parseNull(arrGmCaseSetInfoVO[i].getPlanshipdt());
        String strCaseDtlStr = strAssRepId + "," + strReqDt + "," + strPlanShipDt;

        GmTxnShipInfoVO gmTxnShipInfoVO = arrGmCaseSetInfoVO[i].getGmTxnShipInfoVO();
        String strShipSrc = GmCommonClass.parseNull(gmTxnShipInfoVO.getSource());
        String strShipto = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipto());
        String strShiptoid = GmCommonClass.parseNull(gmTxnShipInfoVO.getShip_to_id());
        String strShipcarrier = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipcarrier());
        String strShipMode = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipmode());
        String strShipAddress = GmCommonClass.parseNull(gmTxnShipInfoVO.getAddressid());
        String strShipAttnto = GmCommonClass.parseNull(gmTxnShipInfoVO.getAttnto());
        String strShipInstruction = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipinstruction());
        String strCaseShipDtlStr =
            strShipSrc + "^" + strShipto + "^" + strShiptoid + "^" + strShipcarrier + "^"
                + strShipMode + "^" + strShipAddress + "^" + strShipAttnto + "^"
                + strShipInstruction;
        String strCasePartInputStr, strSetId = "";
        hmParam.put("CASEDETINPUTSTR", strCaseDtlStr);
        hmParam.put("CASESHIPINPUTSTR", strCaseShipDtlStr);

        if (strCaseType.equals("1006505") || strCaseType.equals("1006506")) { // Loaner/Consignment
                                                                              // set
          strProdType = strCaseType.equals("1006505") ? "103642" : "103643"; // For Loaner set t7104
                                                                             // Type as 103642. else
                                                                             // 103643 for
                                                                             // consignment.
          // 1006506: Set Consignment; 26240267: Set consignment
          strLogType = strCaseType.equals("1006506") ? "26240283" : strLogType;
          hmParam.put("PRODUCTIONTYPE", strProdType);

          strSetId = GmCommonClass.parseNull(arrGmCaseSetInfoVO[i].getSetid());
          hmParam.put("HINPUTSTR", strSetId);
          if (strSetId.equals("")) {
            throw new AppError("", "20640", 'E');
          } else {
            saveCaseSetsInfo(hmParam, gmDBManager);
          }
        } else if (strCaseType.equals("1006507") || strCaseType.equals("107286")) { // Item consignment, 107286 -Account Item Consignment 
         
         if(strCaseType.equals("107286")){ //107286 -Account Item Consignment 
           strProdType = "107860"; // 107860 - Account Item Consignment
         }else{
           strProdType = "103644";
         }
        
          hmParam.put("PRODUCTIONTYPE", strProdType);

          GmPartDetailVO[] arrGmPartDetailVO = arrGmCaseSetInfoVO[i].getArrGmPartDetailVO();
          if (arrGmPartDetailVO.length == 0) {
            throw new AppError("", "20639", 'E');
          } else {
            strCasePartInputStr =
                gmWSUtil.getDbInputStringFromVO(arrGmPartDetailVO, ",", "|",
                    GmPartDetailVO.getPartDetailProperties(), "getPartDetailProperties");
            log.debug("strCasePartInputStr:::::::" + strCasePartInputStr);
          }
          hmParam.put("INPUTSTRING", strCasePartInputStr);
          gmEventReportBean.saveEventItemInfo(hmParam, gmDBManager);
        }

      }

      gmCommonBean.saveLog(strCaseId, strComments, strUserId, strLogType);
      // Item Initiate.This
      // entry is to make an
      // entry into Comments
      // Log.
    }
  } // End of saveRequestSetItemInfo

  /**
   * saveRequestShipDetails This overloaded method will update the shipping details for the existing
   * shipping records.
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   */
  public void saveRequestShipDetails(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveRequestShipDetails(gmDBManager, hmParam);
    gmDBManager.commit();
  }

  public void saveRequestShipDetails(GmDBManager gmDBManager, HashMap hmParam) throws AppError {

    gmDBManager.setPrepareString("gm_pkg_op_inv_mgt_request_txn.gm_update_request_ship", 2);
    gmDBManager.setString(1, ((String) hmParam.get("CASEINFOIDFROMDB")));
    gmDBManager.setString(2, ((String) hmParam.get("TYPE")));
    gmDBManager.execute();

  }

  /**
   * saveApproveSetItem This overloaded method will calculate the set turn and update the status.
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   */
  public void saveApproveSetItem(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveApproveSetItem(gmDBManager, hmParam);
    gmDBManager.commit();
  }

  public void saveApproveSetItem(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_sav_req_set_item.gm_sav_approve_set_item", 3);
    gmDBManager.setString(1, ((String) hmParam.get("CASEIDFROMDB")));
    gmDBManager.setString(2, ((String) hmParam.get("CASEATTRINPUTSTR")));
    gmDBManager.setString(3, ((String) hmParam.get("USERID")));
    gmDBManager.execute();
  }

  /**
   * saveRequestInfo will get the required information that is used for sending out the email, when
   * the Item Request is approved by PD.
   * 
   * @param String strInputString
   * @return
   * @exception AppError
   */
  public String saveRequestInfo(String strInputString,String strCompanyInfo) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33511
    String strUserId = "";
    HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33511
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_request_frm_case", 3);
    gmDBManager.setString(1, strInputString);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strRequestId = GmCommonClass.parseNull(gmDBManager.getString(2));
    String strConsignmentId = GmCommonClass.parseNull(gmDBManager.getString(3));
    /* checkAndSplitConsignamnet() - check wheather the viacell parts are availabe in the request
     * if it's available then split the request based on viacell and non-viacell parts
     * */
    if (!strRequestId.equals("")){
     gmTxnSplitBean.checkAndSplitConsignamnet(gmDBManager, strRequestId, strUserId);
    }
    log.debug("strConsignmentId in saveRequestInfo():::::::" + strConsignmentId);
     gmDBManager.commit();
     // To Save the Insert part number details for Consignment from GlobusAPP Using JMS CALL /106761-consignment
     HashMap hmJMSParam = new HashMap();
     hmJMSParam.put("TXNIDSTR", strConsignmentId);
     hmJMSParam.put("TXNTYPE", "40021");
     hmJMSParam.put("STATUS", "93342");
     hmJMSParam.put("USERID", strUserId);
     hmJMSParam.put("COMPANYINFO", strCompanyInfo);
     gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);
     
   //JMS call for Storing Building Info PMT-33511  
     log.debug("Storing building information saveRequestInfo ");
     hmStorageBuildInfo.put("TXNID", strConsignmentId);
     hmStorageBuildInfo.put("TXNTYPE", "CONSIGNMENT");
     gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);
     
   
    return strRequestId;

  }

  /**
   * saveLoanerAutoApprReq - will save the Loaner Auto Appr Req
   * 
   * @param HashMap
   * @return
   * @exception AppError
   */
  public void saveLoanerAutoApprReq(HashMap hmParam) throws AppError, Exception {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmLoanerRequestApprlAction gmLoanerRequestApprlAction = new GmLoanerRequestApprlAction();
    String strCaseinfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("CASESTATUS"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));

    gmDBManager.setPrepareString("gm_pkg_op_inv_mgt_request_txn.gm_sav_auto_appr_loaner_req", 4);
    gmDBManager.setString(1, strCaseinfoID);
    gmDBManager.setString(2, strStatus);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strApprRqstIds = GmCommonClass.parseNull(gmDBManager.getString(3));
    String strRejRqstIds = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();

    // If any sets approved, send email notification.
    if (!strApprRqstIds.equals("")) {
      gmLoanerRequestApprlAction.sendRequestNotification(strApprRqstIds, "Approved", "1006505",
          strCompanyId, "");
    }

  } // End of saveLoanerAutoApprReq

  /**
   * saveSetAutoApprReq - will save the Loaner Auto Appr Req
   * 
   * @param HashMap
   * @return
   * @exception AppError
   */
  public void saveSetAutoApprReq(HashMap hmParam) throws AppError, Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmLoanerRequestApprlAction gmLoanerRequestApprlAction = new GmLoanerRequestApprlAction();
    String strCaseinfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("CASESTATUS"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));

    gmDBManager.setPrepareString("gm_pkg_op_inv_mgt_request_txn.gm_sav_auto_appr_set_req", 4);
    gmDBManager.setString(1, strCaseinfoID);
    gmDBManager.setString(2, strStatus);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strApprRqstIds = GmCommonClass.parseNull(gmDBManager.getString(3));
    String strRejRqstIds = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();

    // If any sets approved, send email notification.
    if (!strApprRqstIds.equals("")) {
      gmLoanerRequestApprlAction.sendRequestNotification(strApprRqstIds, "Approved", "1006506",
          strCompanyId, "");
    }

  } // End of saveSetAutoApprReq

  /**
   * saveItemAutoApprReq - will save the Loaner Auto Appr Req
   * 
   * @param HashMap
   * @return
   * @exception AppError
   */
  public void saveItemAutoApprReq(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmLoanerRequestApprovalBean gmLoanerRequestApprovalBean =
        new GmLoanerRequestApprovalBean(getGmDataStoreVO());
    GmLoanerRequestApprlAction gmLoanerRequestApprlAction = new GmLoanerRequestApprlAction();
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    String strCaseinfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("CASESTATUS"));

    HashMap hmdata = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_op_inv_mgt_request_txn.gm_sav_auto_appr_item_req", 4);
    gmDBManager.setString(1, strCaseinfoID);
    gmDBManager.setString(2, strStatus);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strApprRqstIds = GmCommonClass.parseNull(gmDBManager.getString(3));
    String strRejRqstIds = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();

    // If any sets approved, send email notification.
    if (!strApprRqstIds.equals("")) {
      hmdata.put("STRREQIDS", strApprRqstIds);
      gmLoanerRequestApprovalBean.fetchPDApprovalDtl(hmdata);
    }

    // below code part added to send item consignment email notification to sales rep without Ad
    // approval
    if (!strRejRqstIds.equals("")) {
      hmdata.put("STRREQIDS", strRejRqstIds);
      gmLoanerRequestApprovalBean.fetchADApprovalDtl(hmdata);
    }
    // below code part is commented for remove AD request Notification Email.
    // The below part is added to send the AD request Notification Email.
    // if (!strRejRqstIds.equals("")) {
    // gmCaseBookRptBean.sendADRequestNotificationEmail(strRejRqstIds);
    // }
  } // End of saveItemAutoApprReq

  /**
   * saveInvRequestApprove - To Approve Item/Set/Loaner Initiated From Device The Beleow method
   * needs to be called based on type Approval method will be called.
   * 
   * @param HashMap
   * @return
   * @exception AppError
   */
  public void saveInvRequestApprove(HashMap hmParam) throws Exception {

    String strCaseStatus = GmCommonClass.parseNull((String) hmParam.get("CASESTATUS"));
    String strCaseType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strCaseid = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    hmParam.put("COMPANYID", strCompanyId);

    if (strCaseType.equals("1006505")) {// Loaner Approval
      saveLoanerAutoApprReq(hmParam);
    } else if (strCaseType.equals("1006506")) {// Set Approval
      saveSetAutoApprReq(hmParam);
    } else if (strCaseType.equals("1006507") || strCaseType.equals("107286")) {// Item Consignment Approval, 107286 -Account Item Consignment 
      saveItemAutoApprReq(hmParam);
    }


  }

  /**
   * saveVoidInvRequest - To Void the Item/Set/Loaner Initiated from Device, which are not approved
   * the below method is called.
   * 
   * @param HashMap
   * @return
   * @exception AppError
   */
  public void saveVoidInvRequest(HashMap hmParam) throws Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCaseinfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_op_inv_mgt_request_txn.gm_sav_request_void", 2);
    gmDBManager.setString(1, strCaseinfoID);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }
}
