package com.globus.sales.CaseManagement.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseSchDashboardBean extends GmSalesFilterConditionBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	//GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	public GmCaseSchDashboardBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	public GmCaseSchDashboardBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}
/**
 * fetchKitShipDashboard This method used to fetch kit overdue, today and tomorrow values for shipping - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchKitShipDashboard() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	HashMap hmoverdue = new HashMap();
	HashMap hmtoday = new HashMap();
	HashMap hmtomorrow = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_kit_ship_dashboard", 3);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmoverdue  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
	hmtoday  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
	hmtomorrow  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
	hmParam.put("SHIPOVERDUE", hmoverdue);
	hmParam.put("SHIPTODAY", hmtoday);
	hmParam.put("SHIPTMW", hmtomorrow);
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
/**
 * fetchKitReturnsDashboard This method used to fetch kit overdue, today and tomorrow values for Returns - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchKitReturnsDashboard() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	HashMap hmoverdue = new HashMap();
	HashMap hmtoday = new HashMap();
	HashMap hmtomorrow = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_kit_Returns_dashboard", 3);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmoverdue  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
	hmtoday  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
	hmtomorrow  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
	hmParam.put("RETOVERDUE", hmoverdue);
	hmParam.put("RETTODAY", hmtoday);
	hmParam.put("RETTMW", hmtomorrow);
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
/**
 * fetchShippingCaseDashboard This method used to fetch case overdue, today and tomorrow values for shipping - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchShippingCaseDashboard() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	HashMap hmoverdue = new HashMap();
	HashMap hmtoday = new HashMap();
	HashMap hmtomorrow = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_shipping_case_dashboard", 3);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmoverdue  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
	hmtoday  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
	hmtomorrow  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
	hmParam.put("SHIPOVERDUE", hmoverdue);
	hmParam.put("SHIPTODAY", hmtoday);
	hmParam.put("SHIPTMW", hmtomorrow);
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
/**
 * fetchReturnsCaseDashboard This method used to fetch case overdue, today and tomorrow values for returns - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchReturnsCaseDashboard() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	HashMap hmoverdue = new HashMap();
	HashMap hmtoday = new HashMap();
	HashMap hmtomorrow = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_Returns_case_dashboard", 3);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmoverdue  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
	hmtoday  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
	hmtomorrow  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
	hmParam.put("RETOVERDUE", hmoverdue);
	hmParam.put("RETTODAY", hmtoday);
	hmParam.put("RETTMW", hmtomorrow);
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
/**
 * fetchKitShipUpComing This method used to fetch kit upcoming week, this week and this month values for shipping - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchKitShipUpComing() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_kit_ship_upcoming", 1);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmParam  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
/**
 * fetchKitRetUpComing This method used to fetch kit upcoming week, this week and this month values for return - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchKitRetUpComing() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_kit_Ret_upcoming", 1);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmParam  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
/**
 * fetchCaseShipUpComing This method used to fetch case upcoming week, this week and this month values for shipping - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchCaseShipUpComing() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_case_ship_upcoming", 1);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmParam  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
/**
 * fetchCaseRetUpComing This method used to fetch case upcoming week, this week and this month values for retuns - case scheduler dsh screen 
 * 
 * @param 
 * @return HashMap
 * @exception  Exception
 */
public HashMap fetchCaseRetUpComing() throws Exception{
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	HashMap hmParam = new HashMap();
	gmDBManager.setPrepareString("gm_pkg_case_sch_dashboard.gm_fetch_case_Ret_upcoming", 1);
	gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	gmDBManager.execute();
	hmParam  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(1));
  	gmDBManager.close();
  	log.debug("hmParam==>"+hmParam);
	return hmParam;	
}
}//End of Bean
