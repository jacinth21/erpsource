package com.globus.sales.CaseManagement.beans;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.CaseManagement.forms.GmICalForm;
import com.globus.sales.beans.GmICalBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseBookRptBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmCaseBookRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmCaseBookRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  /**
	 * Count and Drill down calls are separated into two methods.
	 * Removed the unwanted the rows and where condition check while fetching the count.
	 * Database Object Creation - Changed the object creation one per class instead of one per method.
	 * Methods Modified: fetchCasesScheduled
	 * New Method Added: fetchCasesScheduledCount
	 * @author gpalani PMT-10095 (RFC2252)
	 * @since 2017-01-03
	 */

	public ArrayList fetchCasesScheduled(HashMap hmParam) throws AppError {
		initializeParameters(hmParam);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = null;
		StringBuffer sbQuery = new StringBuffer();
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));
		String strAccessCondition = GmCommonClass.parseNull((String) hmParam
				.get("AccessFilter"));
		String strApplDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLDATEFMT"));
		String strCondition = GmCommonClass.parseNull((String) hmParam
				.get("Condition")); // To fetch
									// access
									// condition
		String strCaseDays = GmCommonClass.parseNull(GmCommonClass
				.getRuleValue("CASEDAYS", "SALESDASHBOARD"));
		strCaseDays = strCaseDays.equals("") ? "4" : strCaseDays;

		// return count of cases scheduled
		if (strPassName.equals("")) {

			sbQuery = fetchCasesScheduledCount(hmParam);

		}

		else if (!strPassName.equals(""))

		{
			sbQuery.append("SELECT  t7100.c7100_case_info_id infoid, t7100.C7100_CASE_ID caseid,");
			sbQuery.append("TO_CHAR (t7100.c7100_surgery_date,'");
			sbQuery.append(strApplDateFmt);// mm/dd/yyyy
			sbQuery.append("') surgery_date,");
			sbQuery.append("decode(trunc(CURRENT_DATE),t7100.c7100_surgery_date,1,null) today_cnt,");
			sbQuery.append("decode(trunc(CURRENT_DATE+1),t7100.c7100_surgery_date,1,null) tomorrow_cnt,");
			sbQuery.append("1 nextXDay_cnt,");
			sbQuery.append("GET_DISTRIBUTOR_NAME (t7100.C701_DISTRIBUTOR_ID) distnm,");
			sbQuery.append("GET_REP_NAME (t7100.C703_SALES_REP_ID) repnm,");
			sbQuery.append("get_account_name (t7100.c704_account_id) accountnm,");
			sbQuery.append("GET_CODE_NAME (t7100.c901_case_status) status,");
			sbQuery.append("t7100.c901_case_status casests,t7100.c7100_personal_notes personal_notes ");
			sbQuery.append("FROM T7100_CASE_INFORMATION t7100 ");
			sbQuery.append(" , (SELECT DISTINCT compid, divid, ter_id ");
			sbQuery.append(" , ter_name, rep_name, rep_id ,rep_compid,d_compid, d_id ,d_name, vp_id, vp_name ");
			sbQuery.append(" , region_id, region_name, ad_id, ad_name, gp_id, gp_name  ");
			sbQuery.append(" FROM v700_territory_mapping_detail) V700 ");
			sbQuery.append(" WHERE t7100.c7100_void_fl IS NULL ");
			sbQuery.append("AND t7100.c7100_surgery_date IS NOT NULL ");
			sbQuery.append("AND t7100.c901_case_status IN (11090,11091) "); // Draft,
																			// Active
			sbQuery.append("AND t7100.c901_type = 1006503 ");
			sbQuery.append("AND V700.REP_ID  = t7100.C703_SALES_REP_ID ");
			if (!strCondition.equals("")) {
				sbQuery.append(" AND ");
				sbQuery.append(strCondition);
			}

			if (strPassName.equals("Today")) {
				sbQuery.append("AND t7100.c7100_surgery_date = trunc(CURRENT_DATE) ");
			}

			else if (strPassName.equals("Tomorrow"))

			{
				sbQuery.append("AND t7100.c7100_surgery_date = trunc(CURRENT_DATE+1) ");
			}

			else {
				sbQuery.append("AND t7100.c7100_surgery_date between trunc(CURRENT_DATE) and trunc(CURRENT_DATE+");
				sbQuery.append(strCaseDays);
				sbQuery.append(") ");
			}

			sbQuery.append("ORDER BY t7100.c7100_surgery_date ");
		}

		log.debug("fetchCasesScheduled query: " + sbQuery.toString());

		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	}

  //Added the new method to fetch only the count - PMT-10095
	public StringBuffer fetchCasesScheduledCount(HashMap hmParam)
			throws AppError

	{
		initializeParameters(hmParam);
		String strPassName = GmCommonClass.parseNull((String) hmParam
				.get("PASSNAME"));
		StringBuffer sbQuery = new StringBuffer();
		String strCondition = GmCommonClass.parseNull((String) hmParam
				.get("Condition")); // To fetch access condition
		String strCaseDays = GmCommonClass.parseNull(GmCommonClass
				.getRuleValue("CASEDAYS", "SALESDASHBOARD"));
		strCaseDays = strCaseDays.equals("") ? "4" : strCaseDays;

		sbQuery.append(" SELECT NVL(sum(today_cnt),0) TODAY_CNT, NVL(sum(tomorrow_cnt),0) TOMORROW_CNT, NVL(sum(nextXDay_cnt),0) NEXTXDAY_CNT FROM (");
		sbQuery.append(" SELECT TO_CHAR (t7100.c7100_surgery_date,'MM/dd/yyyy') surgery_date, ");
		sbQuery.append(" decode(trunc(CURRENT_DATE),t7100.c7100_surgery_date,1,null) today_cnt,");
		sbQuery.append(" decode(trunc(CURRENT_DATE+1),t7100.c7100_surgery_date,1,null) tomorrow_cnt,");
		sbQuery.append(" 1 nextXDay_cnt ");
		sbQuery.append(" FROM T7100_CASE_INFORMATION t7100 ");
		sbQuery.append(" , (SELECT DISTINCT compid, divid, ter_id ");
		sbQuery.append(" , ter_name, rep_name, rep_id ,rep_compid,d_compid, d_id ,d_name, vp_id, vp_name ");
		sbQuery.append(" , region_id, region_name, ad_id, ad_name, gp_id, gp_name  ");
		sbQuery.append(" FROM v700_territory_mapping_detail) V700 ");
		sbQuery.append(" WHERE t7100.c7100_void_fl IS NULL ");
		sbQuery.append(" AND t7100.c7100_surgery_date IS NOT NULL ");
		sbQuery.append(" AND t7100.c901_case_status IN (11090,11091) "); // Draft,
																			// Active
		sbQuery.append(" AND t7100.c901_type = 1006503 ");
		sbQuery.append(" AND V700.REP_ID  = t7100.C703_SALES_REP_ID ");
		if (!strCondition.equals("")) {
			sbQuery.append(" AND ");
			sbQuery.append(strCondition);
		}
		if (strPassName.equals("Today")) {
			sbQuery.append(" AND t7100.c7100_surgery_date = trunc(CURRENT_DATE) ");
		} else if (strPassName.equals("Tomorrow")) {
			sbQuery.append(" AND t7100.c7100_surgery_date = trunc(CURRENT_DATE+1) ");
		} else {
			sbQuery.append(" AND t7100.c7100_surgery_date between trunc(CURRENT_DATE) and trunc(CURRENT_DATE+");
			sbQuery.append(strCaseDays);
			sbQuery.append(") )");
		}

		
		return sbQuery;
	}


  public ArrayList fetchCaseCalendar(HashMap hmParam) throws AppError {

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
    Date dtFromDT = (Date) hmParam.get("FROMDT");
    Date dtToDT = (Date) hmParam.get("TODT");
    String strFromDate = GmCommonClass.getStringFromDate(dtFromDT, strDateFmt);
    String strToDate = GmCommonClass.getStringFromDate(dtToDT, strDateFmt);

    sbQuery
        .append(" SELECT t7100.c7100_case_info_id infoid, to_char(t7100.C7100_SURGERY_TIME,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')||' :HH24:MI:SS') surgery_time ");
    sbQuery
        .append(",to_char(t7100.c7100_surgery_date,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) surgery_date, get_account_name (t7100.c704_account_id) accountnm ");
    sbQuery
        .append(" ,GET_DISTRIBUTOR_NAME (t7100.C701_DISTRIBUTOR_ID) distnm, GET_REP_NAME (t7100.C703_SALES_REP_ID) repnm, t7100.c7100_personal_notes personalNote  ");
    sbQuery
        .append(" ,GET_CODE_NAME(t7100.c901_case_status) status,TO_CHAR(t7100.C7100_SURGERY_TIME+2/24,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')||' :HH24:MI:SS') surgery_end_time");
    sbQuery
        .append(" ,get_case_attr_name ('CASRGN', t7100.c7100_case_info_id) region, get_case_attr_name ('APPROH', t7100.c7100_case_info_id) approach");
    sbQuery
        .append(" ,get_pending_po(t7100.c7100_case_info_id) pending_po, get_set_pending_return(t7100.c7100_case_info_id) pending_set_return,TO_CHAR(t7100.c7100_surgery_date,'dd') surgery_day,");
    sbQuery
        .append(" TO_CHAR(t7100.c7100_surgery_date,'mm')surgery_month, TO_CHAR(t7100.c7100_surgery_date,'yyyy') surgery_year,TO_CHAR(t7100.c7100_surgery_time,'hh24') surgery_hr,  TO_CHAR(t7100.c7100_surgery_time,'mi') ");
    sbQuery.append(" surgery_min,t703.c703_email_id repemail ");
    sbQuery.append(" FROM t7100_case_information t7100,t703_sales_rep t703 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(" WHERE t7100.c7100_surgery_date BETWEEN TO_DATE ('" + strFromDate);
    sbQuery.append("','" + strDateFmt + "') AND TO_DATE ('" + strToDate + "', '" + strDateFmt
        + "')");
    sbQuery.append(" AND t7100.c703_sales_rep_id   = V700.REP_ID ");
    sbQuery.append(" AND t7100.c7100_void_fl IS NULL");
    sbQuery.append(" AND t7100.c901_type = 1006503 ");
    sbQuery
        .append(" AND t7100.c704_account_id IS NOT NULL and t7100.c703_sales_rep_id = t703.c703_sales_rep_id and t703.c703_void_fl is null");
    log.debug("case Calendar query" + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  public void sendCaseCalendarICal(ArrayList alParam, String strUserName) throws AppError,
      IOException {
    HashMap hmValues = new HashMap();
    ArrayList alEvent = new ArrayList();
    GmICalForm gmICalForm = new GmICalForm();

    if (alParam.size() > 0) {

      for (Iterator i = alParam.iterator(); i.hasNext();) {
        hmValues = (HashMap) i.next();
        gmICalForm = new GmICalForm();
        gmICalForm.setStartday(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_DAY"))));
        gmICalForm.setStartmonth(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_MONTH"))));
        gmICalForm.setStartyear(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_YEAR"))));
        gmICalForm.setStartHR(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_HR"))));
        gmICalForm.setStartMIN(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_MIN"))));
        gmICalForm.setEndday(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_DAY"))));
        gmICalForm.setEndmonth(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_MONTH"))));
        gmICalForm.setEndyear(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_YEAR"))));
        gmICalForm.setEndHR(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_HR"))));
        gmICalForm.setEndMIN(Integer.parseInt(GmCommonClass.parseZero((String) hmValues
            .get("SURGERY_MIN"))));
        gmICalForm.setEventHeader("CSMSurgeryInfo");
        gmICalForm.setSubject((String) hmValues.get("PERSONALNOTE"));
        gmICalForm.setMessageDesc("");
        gmICalForm.setCategories((String) hmValues.get("STATUS"));
        gmICalForm.setDesc("Account - " + (String) hmValues.get("ACCOUNTNM") + " \nSales Rep - "
            + (String) hmValues.get("REPNM") + " \nDistributor - "
            + (String) hmValues.get("DISTNM") + " \nStatus - " + (String) hmValues.get("STATUS"));
        alEvent.add(gmICalForm);
      }
      GmICalBean.createICalEvent(strUserName, alEvent);
    }

  }

  public HashMap fetchCalendarCaseInfo(String strCaseInfoId) throws AppError {

    HashMap hmReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT t7100.C7100_CASE_ID caseid, to_char(t7100.C7100_SURGERY_TIME,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')||' :HH24:MI') surgery_time, to_char(t7100.c7100_surgery_date,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) surgery_date ,");
    sbQuery
        .append(" GET_DISTRIBUTOR_NAME (t7100.C701_DISTRIBUTOR_ID) distnm, GET_REP_NAME (t7100.C703_SALES_REP_ID) repnm,");
    sbQuery
        .append(" get_account_name (t7100.c704_account_id) accountnm , GET_CODE_NAME(t7100.c901_case_status) status,reqset,alocset ");
    sbQuery.append(" FROM T7100_CASE_INFORMATION t7100");
    // temp
    sbQuery.append(",( SELECT count(*) reqset FROM t7104_case_set_information  t7104 ");
    sbQuery.append("WHERE  t7104.c7100_case_info_id = " + strCaseInfoId);
    sbQuery.append("),( SELECT count(*) alocset FROM t7104_case_set_information  t7104 ");
    sbQuery.append(" WHERE  t7104.c7100_case_info_id = " + strCaseInfoId);
    sbQuery.append("AND t7104.C5010_TAG_ID IS NOT NULL  )");
    // end
    sbQuery.append(" WHERE t7100.c7100_case_info_id =" + strCaseInfoId);
    sbQuery.append(" AND t7100.c7100_void_fl IS NULL");
    sbQuery.append(" AND t7100.c901_type = 1006503 ");
    log.debug("case Calendar Info query" + sbQuery.toString());

    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
    return hmReturn;
  }

  public ArrayList fetchCaseListRpt(HashMap hmParam) throws AppError {

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
    String strDist = GmCommonClass.parseNull((String) hmParam.get("DIST"));
    String strRep = GmCommonClass.parseNull((String) hmParam.get("REP"));
    String strAccount = GmCommonClass.parseNull((String) hmParam.get("ACCOUNT"));
    String strCatg = GmCommonClass.parseNull((String) hmParam.get("CATEGORY"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
    Date dtFromDT = (Date) hmParam.get("FROMDT");
    Date dtToDT = (Date) hmParam.get("TODT");
    String strFromDate = GmCommonClass.getStringFromDate(dtFromDT, strDateFmt);
    String strToDate = GmCommonClass.getStringFromDate(dtToDT, strDateFmt);
    String strCompDateFormat = getCompDateFmt();

    sbQuery
        .append(" SELECT  t7100.c7100_case_info_id caseinfoid,t7100.C7100_CASE_ID caseid, to_char(t7100.C7100_SURGERY_TIME,'"
            + strCompDateFormat
            + "'||' :HH24:MI') surgery_time, t7100.c7100_surgery_date surgery_date ,");
    sbQuery
        .append(" GET_DISTRIBUTOR_NAME (t7100.C701_DISTRIBUTOR_ID) distnm, GET_REP_NAME (t7100.C703_SALES_REP_ID) repnm,");
    sbQuery
        .append(" get_account_name (t7100.c704_account_id) accountnm , GET_CODE_NAME(t7100.c901_case_status) status, t7100.c901_case_status casests,");
    sbQuery
        .append(" decode(t7100.c901_case_status,'11093','N/A',(select count(*) from t7104_case_set_information t7104 where t7104.c7100_case_info_id = t7100.c7100_case_info_id and t7104.c7104_void_fl is null)) reqset,");
    sbQuery
        .append(" decode(t7100.c901_case_status,'11093','N/A',(select count(*) from t7104_case_set_information t7104 where t7104.c7100_case_info_id = t7100.c7100_case_info_id and t7104.c7104_void_fl is null ");
    sbQuery
        .append(" and (t7104.C5010_TAG_ID is not null or (t7104.C526_PRODUCT_REQUEST_DETAIL_ID is not null AND (select c526_status_fl from t526_product_request_detail  where c526_product_request_detail_id=t7104.C526_PRODUCT_REQUEST_DETAIL_ID) IN (20,30))) )) alocset,  ");

    if (!strCatg.equals("") && !strCatg.equals("0")) {
      sbQuery.append(" get_code_name(caseatt.c901_attribute_type) category");
    } else {
      sbQuery.append(" get_case_attr_name ('CASCAT', t7100.c7100_case_info_id) category");
    }
    sbQuery.append(" FROM T7100_CASE_INFORMATION t7100");
    // for Region
    if (!strCatg.equals("") && !strCatg.equals("0")) {
      sbQuery.append(" ,(select c7100_case_info_id infoid ,c901_attribute_type");
      sbQuery.append(" FROM t7102_case_attribute t7102");
      sbQuery.append(" WHERE t7102.c901_attribute_type IN (" + strCatg + ")");// =(decode("+strRegion+",0,t7102.c901_attribute_type,"+strRegion+"))");
      sbQuery
          .append(" AND t7102.c7102_attribute_value = 'Y' AND t7102.c7102_void_fl IS NULL ) caseatt");
    }

    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(" WHERE ");
    if (!strFromDate.equals("") && !strToDate.equals("") && strCaseId.equals("")) {
      sbQuery.append(" t7100.c7100_surgery_date BETWEEN TO_DATE ('" + strFromDate);
      sbQuery.append("','" + strDateFmt + "') AND TO_DATE ('" + strToDate + "', '" + strDateFmt
          + "') AND ");
    }
    sbQuery.append("  t7100.c703_sales_rep_id   = V700.REP_ID ");

    if (!strCatg.equals("") && !strCatg.equals("0")) {
      sbQuery.append(" AND t7100.C7100_CASE_INFO_ID = caseatt.infoid ");
    }
    //
    if (!strCaseId.equals("") && !strCaseId.equals("0")) {

      sbQuery.append(" AND t7100.C7100_CASE_ID ='" + strCaseId + "'");
    }
    if (!strDist.equals("") && !strDist.equals("0")) {

      sbQuery.append(" AND t7100.C701_DISTRIBUTOR_ID =" + strDist);
    }
    if (!strRep.equals("") && !strRep.equals("0")) {

      sbQuery.append(" AND t7100.C703_SALES_REP_ID =" + strRep);
    }
    if (!strAccount.equals("") && !strAccount.equals("0")) {

      sbQuery.append(" AND t7100.c704_account_id =" + strAccount);
    }
    if (!strStatus.equals("") && !strStatus.equals("0")) {

      sbQuery.append(" AND t7100.c901_case_status =" + strStatus);
    }
    if (!strStatus.equals("11093")) {

      sbQuery.append(" AND t7100.c7100_void_fl IS NULL");
    }
    sbQuery.append(" AND t7100.c901_type = 1006503 ");
    sbQuery.append(" order by t7100.c7100_surgery_date");

    log.debug("Case List Report" + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  /**
   * Fetch Cases based on sales hierachy and Cases mapped to user. This method will call from web
   * service
   * 
   * @param HashMap
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCaseScheduleRpt(HashMap hmParam) throws AppError {

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strAccount = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));

    String strDateFmt = GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT");
    String strDefltTimeZone = GmCommonClass.getRuleValue("DEFAULT", "TIMEZONE");

    sbQuery.append(" SELECT t7100.c7100_case_info_id caseinfoid, t7100.C7100_CASE_ID caseid ");
    sbQuery.append(" , TO_CHAR (t7100.C7100_SURGERY_TIME , '" + strDateFmt
        + " HH24:MI:SS TZH:TZM') surgerytime ");
    sbQuery.append(" , NVL(T7102.C7102_ATTRIBUTE_VALUE,'" + strDefltTimeZone + "') TIMEZONE ");
    sbQuery.append(" , TO_CHAR (T7100.C7100_SURGERY_DATE, '" + strDateFmt + "' ) SURGERYDT ");
    sbQuery.append(" , TO_CHAR (C7100_SURGERY_TIME AT TIME ZONE '" + strDefltTimeZone + "', '"
        + strDateFmt + " hh24:mi:ss') SURGERYTIME_EST ");
    sbQuery.append(" , T7100.C701_DISTRIBUTOR_ID DISTID, T701.C701_DISTRIBUTOR_NAME DISTNM ");
    sbQuery.append(" , T7100.C703_SALES_REP_ID REPID, T703.C703_SALES_REP_NAME REPNM ");
    sbQuery.append(" , T7100.C704_ACCOUNT_ID ACCOUNTID , t704.c704_account_nm accountnm ");
    sbQuery
        .append(" , GET_CODE_NAME (T7100.C901_CASE_STATUS) STATUS, T7100.C901_CASE_STATUS CASESTS ");
    sbQuery.append(" , T7100.C7100_PERSONAL_NOTES PNOTES ");
    sbQuery
        .append(" FROM T7100_CASE_INFORMATION t7100, T701_DISTRIBUTOR T701, T703_SALES_REP T703 ");
    sbQuery.append(" , T704_ACCOUNT T704, T7102_CASE_ATTRIBUTE T7102, ( ");
    sbQuery.append(" SELECT t7100.c7100_case_info_id FROM T7100_CASE_INFORMATION t7100 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(" WHERE t7100.c703_sales_rep_id = V700.REP_ID  ");
    sbQuery.append(" AND t7100.c7100_void_fl    IS NULL  ");
    sbQuery.append(" AND t7100.c901_type         = 1006503  ");
    sbQuery.append(" UNION ");
    sbQuery.append(" SELECT t7100.c7100_case_info_id ");
    sbQuery.append(" FROM T7100_CASE_INFORMATION t7100, ( ");
    sbQuery.append(" SELECT t7102.C7100_CASE_INFO_ID ");
    sbQuery.append(" FROM T7102_CASE_ATTRIBUTE T7102 ");
    sbQuery.append(" WHERE t7102.C7102_VOID_FL        IS NULL ");
    sbQuery.append(" AND T7102.C901_ATTRIBUTE_TYPE  IN (103646, 103647) "); // Assignee,Share with
    sbQuery.append(" AND T7102.C7102_ATTRIBUTE_VALUE = '" + strUserId + "' ");
    sbQuery.append(" )  t7102 ");
    sbQuery.append("  WHERE T7100.C7100_CASE_INFO_ID = T7102.C7100_CASE_INFO_ID ");
    sbQuery.append(" AND T7100.C7100_VOID_FL     IS NULL  ");
    sbQuery.append(" AND T7100.C901_TYPE          = 1006503 ");
    sbQuery.append(" UNION ");
    sbQuery.append(" SELECT T7100.C7100_CASE_INFO_ID ");
    sbQuery.append(" FROM T7100_CASE_INFORMATION T7100 ");
    sbQuery.append(" WHERE T7100.C7100_CREATED_BY = '" + strUserId + "' ");
    sbQuery.append(" AND T7100.C901_TYPE          = 1006503) CSCHD  ");
    sbQuery.append(" WHERE T7100.C7100_CASE_INFO_ID = CSCHD.C7100_CASE_INFO_ID  ");
    sbQuery.append(" AND T7100.C7100_CASE_INFO_ID  = T7102.C7100_CASE_INFO_ID(+) ");
    sbQuery.append(" AND T7102.C7102_VOID_FL IS NULL ");
    sbQuery.append(" AND T7102.C901_ATTRIBUTE_TYPE(+) = 4000534 "); // Time zone
    sbQuery.append(" AND T7100.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID   ");
    sbQuery.append(" AND T7100.C703_SALES_REP_ID   = T703.C703_SALES_REP_ID ");
    sbQuery.append(" AND t7100.c704_account_id = t704.c704_account_id  ");

    if (!strFromDate.equals("") && !strToDate.equals("")) {
      sbQuery.append(" AND T7100.C7100_SURGERY_DATE BETWEEN TO_DATE ('" + strFromDate);
      sbQuery.append("','" + strDateFmt + "') AND TO_DATE ('" + strToDate + "', '" + strDateFmt
          + "') ");
    }

    if (!strAccount.equals("") && !strAccount.equals("0")) {

      sbQuery.append(" AND T7100.C704_ACCOUNT_ID  =" + strAccount);
    }
    if (!strStatus.equals("") && !strStatus.equals("0")) {

      sbQuery.append(" AND T7100.C901_CASE_STATUS =" + strStatus);
    }

    sbQuery.append(" ORDER BY surgerydt");

    log.debug("Case Schedule Report" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    return alReturn;
  }

  public ArrayList fetchCaseFavoritesListRpt(HashMap hmParam) throws AppError {

    initializeParameters(hmParam);
    ArrayList alReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strRep = GmCommonClass.parseNull((String) hmParam.get("REP"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strFavoriteName = GmCommonClass.parseNull((String) hmParam.get("FAVORITENM"));

    sbQuery
        .append(" SELECT t7120.c7120_favourite_case_id fcaseid, t7120.c7120_favourite_case_nm  casenm, ");
    sbQuery
        .append(" get_favouritecase_attr_name('CASCAT', t7120.c7120_favourite_case_id) category, GET_DIST_REP_NAME (t7120.c703_sales_rep_id) distnm,");
    sbQuery.append(" get_rep_name (t7120.c703_sales_rep_id) repnm");
    sbQuery.append(" FROM t7120_favourite_case t7120");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(" WHERE t7120.c703_sales_rep_id = V700.REP_ID");
    sbQuery.append(" AND C7120_FAVOURITE_CASE_NM IS NOT NULL ");
    if (!strFavoriteName.equals("") && !strFavoriteName.equals("0")) {

      sbQuery.append(" AND LOWER(t7120.c7120_favourite_case_nm) LIKE LOWER('%" + strFavoriteName
          + "%')");
    }
    // if (!strRep.equals("") && !strRep.equals("0")){

    sbQuery.append(" AND t7120.c703_sales_rep_id =" + strUserID);
    // }
    sbQuery.append(" AND t7120.c7120_void_fl IS NULL ");
    sbQuery.append(" ORDER BY UPPER(t7120.c7120_favourite_case_nm)");

    log.debug("Case Favorites Query :: " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }



  public HashMap fetchCaseInfo(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    HashMap hmReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));

    sbQuery
        .append(" SELECT t7100.c7100_case_info_id caseinfoid, t7100.c7100_case_id caseid, t7100.c701_distributor_id distributorId, get_code_name(c901_case_status) caseStatus, ");
    sbQuery
        .append(" t7100.c703_sales_rep_id repid, t7100.c704_account_id accountId, t7100.c7100_parent_case_info_id pcaseinfoid");
    sbQuery
        .append(" , t7100.c7100_surgery_date  surgeryDate, t7100.c7100_personal_notes personalNote ");
    sbQuery
        .append(" , GET_CODE_ID(TO_CHAR (t7100.c7100_surgery_time, 'hh12'), 'HOUR') surgeryTimeHour,  GET_CODE_ID(TO_CHAR (t7100.c7100_surgery_time, 'mi'), 'MINUTE') surgeryTimeMin, ");
    sbQuery
        .append(" (CASE WHEN (TO_NUMBER (TO_CHAR (t7100.c7100_surgery_time, 'hh24'))) >= 12  THEN 11082 ELSE 11081 END) surgeryAMPM");
    sbQuery.append(" FROM t7100_case_information t7100");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery
        .append(" WHERE t7100.c703_sales_rep_id   = V700.REP_ID  AND t7100.c7100_void_fl is null");
    sbQuery.append(" AND t7100.c7100_case_info_id =" + strCaseInfoID);
    sbQuery.append(" AND t7100.c901_type = 1006503 ");
    log.debug("case fetchCaseInfo query" + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
    return hmReturn;
  }

  public HashMap fetchAllBookedSets(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    HashMap hmReturn = new HashMap();

    HashMap hmGenerlInfo = new HashMap();
    ArrayList alCaseAttrDtls = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    StringBuffer sbQueryGenInfo = new StringBuffer();

    ArrayList alReturnSets = new ArrayList();
    sbQueryGenInfo
        .append("  SELECT t7100.C7100_CASE_ID caseid, TO_CHAR (t7100.C7100_SURGERY_TIME, GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')||' hh12:mi') surgery_time,  ");
    sbQueryGenInfo
        .append("  (CASE WHEN (TO_NUMBER (TO_CHAR (t7100.c7100_surgery_time, 'hh24'))) >= 12  THEN 'PM' ELSE 'AM' END) surgeryAMPM,  ");
    sbQueryGenInfo
        .append(" t7100.c7100_surgery_date surgery_date, GET_DISTRIBUTOR_NAME (t7100.C701_DISTRIBUTOR_ID) distnm,");
    sbQueryGenInfo
        .append(" GET_REP_NAME (t7100.C703_SALES_REP_ID) repnm, get_account_name (t7100.c704_account_id) accountnm, ");
    sbQueryGenInfo
        .append(" GET_CODE_NAME (t7100.c901_case_status) status, t7100.c901_case_status statusid, get_case_id(t7100.c7100_parent_case_info_id) parentcaseid ");
    sbQueryGenInfo
        .append(" ,t7100.c701_distributor_id distid, t7100.C703_SALES_REP_ID repid, GET_REP_ID(t7100.c704_account_id) accrepid, t7100.c704_account_id accid , t7100.c7100_case_info_id caseinfoid, get_do_id(t7100.c7100_case_info_id) doid");
    sbQueryGenInfo.append(" ,SFL dofl ");
    sbQueryGenInfo
        .append(" FROM T7100_CASE_INFORMATION t7100, (select COUNT(1) SFL from t501_order where c7100_case_info_id = "
            + strCaseInfoID + "  and C501_SHIPPING_DATE is null and c501_void_fl IS NULL) T501");
    sbQueryGenInfo.append(getAccessFilterClauseWithRepID());
    sbQueryGenInfo.append(" WHERE t7100.C7100_CASE_INFO_ID = " + strCaseInfoID);
    sbQueryGenInfo.append(" AND t7100.c7100_void_fl  IS NULL ");
    sbQueryGenInfo.append(" AND t7100.c901_type = 1006503 ");
    sbQueryGenInfo
        .append(" AND t7100.c703_sales_rep_id   = V700.REP_ID AND t7100.c7100_void_fl is null");

    log.debug(" Query to hmGenerlInfo is " + sbQueryGenInfo.toString());

    hmGenerlInfo = gmDBManager.querySingleRecord(sbQueryGenInfo.toString());

    StringBuffer sbQueryAttr = new StringBuffer();

    sbQueryAttr.append(" select get_code_name (t7102.c901_attribute_type) category  ");
    sbQueryAttr.append(" from t7102_case_attribute t7102,T7100_CASE_INFORMATION t7100 ");
    sbQueryAttr.append(getAccessFilterClauseWithRepID());
    sbQueryAttr.append(" where t7100.c7100_case_info_id = " + strCaseInfoID);
    sbQueryAttr.append(" AND t7100.c7100_case_info_id   = t7102.c7100_case_info_id ");
    sbQueryAttr
        .append(" AND t7100.c703_sales_rep_id   = V700.REP_ID AND t7100.c7100_void_fl is null ");
    sbQueryAttr.append(" AND t7100.c901_type = 1006503 ");
    sbQueryAttr
        .append(" AND t7102.c7102_void_fl IS NULL AND t7102.c7102_attribute_value = 'Y' order by t7102.c901_attribute_type ");

    log.debug(" Query to sbQueryAttr is " + sbQueryAttr.toString());

    alCaseAttrDtls = gmDBManager.queryMultipleRecords(sbQueryAttr.toString());
    StringBuffer sbQuerySETS = new StringBuffer();

    if (strOpt.equals("nextConfirm")) {

      sbQuerySETS
          .append(" SELECT  t7104.c207_set_id setid, get_set_name (t7104.c207_set_id) setnm,get_code_name (t207.c207_category) family, ");
      sbQuerySETS
          .append(" DECODE (t207.c901_hierarchy, 20700, 'Standard', 20702, 'Additional') stype,count(t7104.c207_set_id)  qty ");
      sbQuerySETS
          .append(" FROM t7104_case_set_information t7104, t207_set_master t207, T7100_CASE_INFORMATION t7100");
      sbQuerySETS.append(getAccessFilterClauseWithRepID());
      sbQuerySETS.append(" WHERE t7104.c7100_case_info_id = " + strCaseInfoID);
      sbQuerySETS.append(" and t7104.c7100_case_info_id = t7100.c7100_case_info_id");
      sbQuerySETS
          .append(" and t7104.c207_set_id  = t207.c207_set_id and t7104.c7104_void_fl IS NULL");
      sbQuerySETS
          .append(" AND t7100.c703_sales_rep_id   = V700.REP_ID AND t7100.c7100_void_fl is null  ");
      sbQuerySETS.append(" AND t7104.c7104_shipped_del_fl IS NULL ");
      sbQuerySETS.append(" AND t7100.c901_type = 1006503 ");
      sbQuerySETS.append(" group by t7104.c207_set_id ,t207.c207_category ,t207.c901_hierarchy  ");
      sbQuerySETS.append(" order by SUBSTR(setnm, 1, 3), setid, t207.c901_hierarchy  ");
      log.debug(" Query to all sets sbQuerySETS is " + sbQuerySETS.toString());

      alReturnSets = gmDBManager.queryMultipleRecords(sbQuerySETS.toString());
      log.debug(" Query to all sets confirm is " + sbQuerySETS.toString());
      hmReturn.put("ALLSETS", alReturnSets);
    } else {
      sbQuerySETS
          .append(" SELECT t7104.c7100_case_info_id caseinfoid, t7104.c7104_case_set_id casesetid, t7100.c901_case_status statusid, t7100.c701_distributor_id casedistid, t7104.c207_set_id setid, get_set_name (t7104.c207_set_id) setnm ");
      sbQuerySETS
          .append(" , gm_pkg_sm_loaner_allocation.chk_loaner_set_avail(t7104.c207_set_id) loanerflag ");
      // Comment for MNTTASK-7882 Auto Allocation
      // sbQuerySETS.append(" , decode(t7100.c901_case_status, 11092, '', 11093, DECODE(t7104.c7104_shipped_del_fl,'Y',TO_CHAR (t7104.c7104_tag_lock_from_date, 'mm/dd/yyyy'),''), TO_CHAR (t7104.c7104_tag_lock_from_date, 'mm/dd/yyyy')) lockfromdt ");
      sbQuerySETS
          .append(" , trunc((((86400*(t7100.c7100_surgery_date-(CURRENT_DATE)))/60)/60)/24) surgerydays, t7104.c7104_shipped_del_fl SHIPPEDDELFL  ");
      // sbQuerySETS.append(" , decode(t7100.c901_case_status, 11092, '', 11093, DECODE(t7104.c7104_shipped_del_fl,'Y',TO_CHAR (t7104.c7104_tag_lock_to_date, 'mm/dd/yyyy'),''), TO_CHAR (t7104.c7104_tag_lock_to_date, 'mm/dd/yyyy')) locktodt ");
      sbQuerySETS
          .append(" , DECODE (t7104.c5010_tag_id, NULL, DECODE(t7104.c526_product_request_detail_id, NULL, decode(t7100.c901_case_status, 11092, '', 11093, DECODE(t7104.c7104_shipped_del_fl,'Y',t7104.c5010_tag_id,''),'TBD') ");
      sbQuerySETS
          .append(" , ( select t504a.C504A_ETCH_ID from t504a_consignment_loaner t504a, t504a_loaner_transaction t504a_txn ");
      sbQuerySETS
          .append("   where t504a.c504_consignment_id = t504a_txn.c504_consignment_id  and t504a_txn.c526_product_request_detail_id =t7104.c526_product_request_detail_id AND t504a.c504a_void_fl IS NULL AND t504a_txn.c504a_void_fl IS NULL AND ROWNUM=1)),t7104.c5010_tag_id ) tagid ");
      sbQuerySETS.append(" , DECODE (t7104.c901_set_location_type, 11381, 'Loaner','') setalloc ");
      sbQuerySETS
          .append(" , DECODE (t526.c526_status_fl, 5, 'Pending Approval', 10, 'Approved', 20, 'Allocated', 30, 'Closed', 40, 'Rejected') status ");
      sbQuerySETS
          .append(", (SELECT  name || '<br>' || address1 || '<br>' ||DECODE(address2,'','',address2||'<br>') ");
      sbQuerySETS
          .append(" || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',zipcode,city) ");
      sbQuerySETS.append(" || ' ' || get_code_name_alt (state) || ' '  ");
      sbQuerySETS
          .append(" || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',city,zipcode) ");
      sbQuerySETS.append(" || '<br>'||'Ph:-'||phonenumber|| '<br>'||'Email:-'||emailid ");
      sbQuerySETS.append(" FROM v5010_curr_address WHERE tagid=t7104.C5010_TAG_ID) currentloc ");

      sbQuerySETS
          .append(", (SELECT  decode(shiplcntoid, 4122, get_rep_name(t7100.c703_sales_rep_id),'')||'<br>' || name || '<br>' || address1 || '<br>' ||DECODE(address2,'','',address2||'<br>') ");
      sbQuerySETS
          .append(" || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',zipcode,city) ");
      sbQuerySETS.append(" || ' ' || get_code_name_alt (state) || ' '  ");
      sbQuerySETS
          .append(" || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',city,zipcode) ");
      sbQuerySETS.append(" || '<br>'||'Ph:-'||phonenumber|| '<br>'||'Email:-'||emailid ");
      sbQuerySETS
          .append(" FROM v7100_ship_address WHERE casesetid=t7104.c7104_case_set_id and source = DECODE(t7104.c901_set_location_type,11381,50182 "); // 11381-
                                                                                                                                                     // Loaner
                                                                                                                                                     // type,
                                                                                                                                                     // 50182
                                                                                                                                                     // -
                                                                                                                                                     // Loaner
                                                                                                                                                     // source
      /*
       * Comment for MNTTASK-7882 Auto Allocation sbQuerySETS.append(" ,11385 "); 11385 - Requestor
       */
      sbQuerySETS.append(" )) shipin ");
      sbQuerySETS
          .append("  ,(select refid||'~'||shiptoid||'~'||shiplcntoid||'~'||addressid||'~'||dmode from v7100_ship_address WHERE casesetid       = t7104.c7104_case_set_id");
      sbQuerySETS.append("    AND source = DECODE(t7104.c901_set_location_type,11381,50182 "); // 11381-
                                                                                               // Loaner
                                                                                               // type,
                                                                                               // 50182
                                                                                               // -
                                                                                               // Loaner
                                                                                               // source
      /*
       * Comment for MNTTASK-7882 Auto Allocation sbQuerySETS.append(" ,11385 "); 11385 - Requestor
       */
      sbQuerySETS.append(" )) shipindetail,t7104.c901_set_location_type locationtype");
      /*
       * Comment for MNTTASK-7882 Auto Allocation sbQuerySETS.append(
       * ", (SELECT  name || '<br>' || address1 || '<br>' ||DECODE(address2,'','',address2||'<br>') "
       * ); sbQuerySETS.append(
       * " || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',zipcode,city) ");
       * sbQuerySETS.append(" || ' ' || get_code_name_alt (state) || ' '  "); sbQuerySETS.append(
       * " || DECODE(NVL(GET_RULE_VALUE('SWAP_ZIP_CODE','SWAP_ZIP_CODE'),'N'),'Y',city,zipcode) ");
       * sbQuerySETS.append(" || '<br>'||'Ph:-'||phonenumber|| '<br>'||'Email:-'||emailid ");
       * sbQuerySETS.append(
       * " FROM v7100_ship_address WHERE refid=t7104.c7104_case_set_id and source = 11388) shipout "
       * );
       */
      sbQuerySETS
          .append(" ,(SELECT count(*) FROM t903_upload_file_list t903 WHERE  c903_ref_id = t7104.C5010_TAG_ID AND c903_delete_fl IS NULL) tagimagecnt  ");
      sbQuerySETS
          .append(" FROM t7104_case_set_information t7104, t526_product_request_detail t526, T7100_CASE_INFORMATION t7100");
      sbQuerySETS.append(getAccessFilterClauseWithRepID());
      sbQuerySETS.append(" WHERE t7104.c7100_case_info_id = " + strCaseInfoID);
      sbQuerySETS.append(" AND t7104.c7100_case_info_id = t7100.c7100_case_info_id");
      sbQuerySETS
          .append(" AND  t7104.c526_product_request_detail_id  =  t526.c526_product_request_detail_id(+) ");
      sbQuerySETS
          .append(" AND t7100.c703_sales_rep_id   = V700.REP_ID AND t7100.c7100_void_fl is null AND t7104.c7104_void_fl is null ");
      sbQuerySETS.append(" AND t7100.c901_type = 1006503 ");
      sbQuerySETS.append(" AND t526.c526_void_fl IS NULL  ");
      sbQuerySETS.append(" ORDER BY SETNM ");
      log.debug(" Query to all sets summary is " + sbQuerySETS.toString());

      alReturnSets = gmDBManager.queryMultipleRecords(sbQuerySETS.toString());
      hmReturn.put("ALLSETS", alReturnSets);
    }

    hmReturn.put("GENERALINFO", hmGenerlInfo);
    hmReturn.put("CASEATTR", alCaseAttrDtls);

    log.debug(" hmReturn is............. " + hmReturn);
    return hmReturn;
  }


  public HashMap fetchAllProfileSets(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    HashMap hmReturn = new HashMap();

    HashMap hmGenerlInfo = new HashMap();
    ArrayList alCaseAttrDtls = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strFavCaseID = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASEID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    StringBuffer sbQueryGenInfo = new StringBuffer();

    ArrayList alReturnSets = new ArrayList();

    StringBuffer sbQueryAttr = new StringBuffer();

    sbQueryAttr.append(" select get_code_name (t7122.c901_attribute_type) category  ");
    sbQueryAttr.append(" from t7122_favourite_case_attribute t7122,T7120_FAVOURITE_CASE t7120 ");
    sbQueryAttr.append(getAccessFilterClauseWithRepID());
    sbQueryAttr.append(" where t7122.c7120_favourite_case_id = " + strFavCaseID);
    sbQueryAttr.append(" AND t7120.c7120_favourite_case_id   = t7122.c7120_favourite_case_id ");
    sbQueryAttr
        .append(" AND t7120.c703_sales_rep_id   = V700.REP_ID AND t7120.c7120_void_fl is null ");
    sbQueryAttr
        .append(" AND t7122.c7122_void_fl IS NULL AND t7122.c7122_attribute_value = 'Y' order by t7122.c901_attribute_type");

    log.debug(" Query to profile sbQueryAttr is " + sbQueryAttr.toString());

    alCaseAttrDtls = gmDBManager.queryMultipleRecords(sbQueryAttr.toString());
    StringBuffer sbQuerySETS = new StringBuffer();

    sbQuerySETS
        .append(" SELECT  t7124.c207_set_id setid, get_set_name (t7124.c207_set_id) setnm,get_code_name (t207.c207_category) family, ");
    sbQuerySETS
        .append(" DECODE (t207.c901_hierarchy, 20700, 'Standard', 20702, 'Additional') stype,count(t7124.c207_set_id)  qty ");
    sbQuerySETS
        .append(" FROM T7124_FAVOURITE_CASE_SET_INFO t7124, t207_set_master t207, T7120_FAVOURITE_CASE t7120");
    sbQuerySETS.append(getAccessFilterClauseWithRepID());
    sbQuerySETS.append(" WHERE t7124.c7120_favourite_case_id = " + strFavCaseID);
    sbQuerySETS.append(" and t7124.c7120_favourite_case_id = t7120.c7120_favourite_case_id");
    sbQuerySETS
        .append(" and t7124.c207_set_id  = t207.c207_set_id and t7124.c7124_void_fl IS NULL");
    sbQuerySETS
        .append(" AND t7120.c703_sales_rep_id   = V700.REP_ID AND t7120.c7120_void_fl is null  ");
    sbQuerySETS.append(" group by t7124.c207_set_id ,t207.c207_category ,t207.c901_hierarchy  ");
    sbQuerySETS.append(" order by SUBSTR(setnm, 1, 3), setid, t207.c901_hierarchy  ");
    log.debug(" Query to all sets sbQuerySETS is " + sbQuerySETS.toString());

    alReturnSets = gmDBManager.queryMultipleRecords(sbQuerySETS.toString());
    log.debug(" Query to profile all sets confirm is " + sbQuerySETS.toString());
    hmReturn.put("ALLSETS", alReturnSets);

    hmReturn.put("CASEATTR", alCaseAttrDtls);
    log.debug(" hmReturn is............. " + hmReturn);
    return hmReturn;
  }

  /**
   * Fetch all mapped system and sets.
   * 
   * @param String
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCaseSetsMap(HashMap hmParam) throws AppError {
    ArrayList alReturnSysSets = new ArrayList();
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strFwdProductType = GmCommonClass.parseNull((String) hmParam.get("FWDPRODUCTTYPE"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_fch_mapped_sets", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull(strFwdProductType));
    gmDBManager.setString(2, GmCommonClass.parseNull(strCaseInfoID));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturnSysSets = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    // log.debug("Exit");
    return alReturnSysSets;
  }

  /**
   * Fetch all mapped system and sets for favorite case.
   * 
   * @param String
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchFavCaseSetsMap(HashMap hmParam) throws AppError {
    ArrayList alReturnSysSets = new ArrayList();
    String strFavCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASEID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strFwdProductType = GmCommonClass.parseNull((String) hmParam.get("FWDPRODUCTTYPE"));
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_fch_fav_mapped_sets", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull(strFwdProductType));
    gmDBManager.setString(2, GmCommonClass.parseNull(strFavCaseInfoID));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturnSysSets = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    // log.debug("Exit");
    return alReturnSysSets;
  }

  public ArrayList fetchCaseSetsInfo(HashMap hmParam) throws AppError {


    ArrayList alReturn = new ArrayList();
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    if (!strOpt.equals("editEvent")) {
      initializeParameters(hmParam);
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String interBodyFL = GmCommonClass.parseNull((String) hmParam.get("INTERBODYFL"));
    String biologicFL = GmCommonClass.parseNull((String) hmParam.get("BIOLOGICFL"));
    String fixationFL = GmCommonClass.parseNull((String) hmParam.get("FIXATIONFL"));
    String screen = GmCommonClass.parseNull((String) hmParam.get("FROMSCREEN"));
    String strFwdProductType = GmCommonClass.parseNull((String) hmParam.get("FWDPRODUCTTYPE"));

    sbQuery.append(" SELECT t7104.c207_set_id setid,  count(t7104.c207_set_id)  qty ");
    sbQuery.append("  FROM t7104_case_set_information t7104, t7100_case_information t7100 ");
    if (!strOpt.equals("editEvent")) {
      sbQuery.append(getAccessFilterClauseWithRepID());
    }
    sbQuery.append(" WHERE t7104.c7104_void_fl IS NULL ");
    sbQuery.append(" AND  T7100.c7100_case_info_id = " + strCaseInfoID);
    if (!strOpt.equals("editEvent")) {
      sbQuery.append(" AND t7100.c703_sales_rep_id   = V700.REP_ID ");
    }
    sbQuery
        .append(" and t7104.c7100_case_info_id = t7100.c7100_case_info_id AND t7100.c7100_void_fl is null ");
    sbQuery.append(" and t7104.c901_system_type = " + strFwdProductType);
    sbQuery.append(" AND t7104.c7104_shipped_del_fl IS NULL ");
    sbQuery.append(" group by t7104.c207_set_id order by t7104.c207_set_id ");

    log.debug(" Query to fetchFavoriteCase is " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }


  public ArrayList fetchFavCaseSets(HashMap hmParam) throws AppError {


    ArrayList alReturn = new ArrayList();

    initializeParameters(hmParam);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strFavCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("FAVOURITECASEID"));
    String strFwdProductType = GmCommonClass.parseNull((String) hmParam.get("FWDPRODUCTTYPE"));

    sbQuery.append(" SELECT t7124.c207_set_id setid,  count(t7124.c207_set_id)  qty ");
    sbQuery.append("  FROM T7124_FAVOURITE_CASE_SET_INFO t7124, T7120_FAVOURITE_CASE t7120 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery.append(" WHERE t7124.c7124_void_fl IS NULL ");
    sbQuery.append(" AND  t7124.c7120_favourite_case_id = " + strFavCaseInfoID);
    sbQuery.append(" AND t7120.c703_sales_rep_id   = V700.REP_ID ");

    sbQuery
        .append(" and t7124.c7120_favourite_case_id = t7120.c7120_favourite_case_id AND t7120.c7120_void_fl is null ");
    sbQuery.append(" and t7124.c901_system_type = " + strFwdProductType);

    sbQuery.append(" group by t7124.c207_set_id order by t7124.c207_set_id ");

    log.debug(" Query to fetchFavoriteCase is " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }

  /**
   * Fetch updated case count for the user. This method will call from web service
   * 
   * @param HashMap
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCaseSynUpdate(HashMap hmParams) throws AppError {

    String strReftypes = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_rpt.gm_fch_case_sync_upd", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("UUID"));
    gmDBManager.setString(4, (String) hmParams.get("USERID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    log.debug("alResult=>" + alResult);
    gmDBManager.close();

    return alResult;
  }

  /**
   * Fetch Cases and Case attribute mapped to the user. This method will call from web service
   * 
   * @param HashMap
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCaseMaster(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_sm_casebook_rpt.gm_fch_case_sync", 6);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.setString(5, (String) hmParams.get("USERID"));

    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();

    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    log.debug("fetchCaseMaster=>" + alResult.size());

    gmDBManager.close();

    return alResult;
  }

  /**
   * Fetch Cases and Case attribute mapped to the user. This method will call from web service
   * 
   * @param HashMap
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCaseAttribute(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_sm_casebook_rpt.gm_fch_case_attr_sync", 6);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.setString(5, (String) hmParams.get("USERID"));

    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();

    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    log.debug("fetchCaseMaster=>" + alResult.size());

    gmDBManager.close();

    return alResult;
  }

  /**
   * Fetch Cases Info which are used to send email notification on Request Approved.
   * 
   * @param String, String
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchSetReqNotificationInfo(String strProdReqDtlId, String strCaseType)
      throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strDateFmt = getCompDateFmt();
    sbQuery.append(" SELECT T7100.C7100_CASE_ID REQID,  T526.C525_PRODUCT_REQUEST_ID LOANREQID ,");
    sbQuery.append(" T526.C207_SET_ID setid,  GET_SET_NAME(T526.C207_SET_ID) setnm, ");
    sbQuery
        .append("  GET_ACCOUNT_NAME(T7100.C704_ACCOUNT_ID)acctnm,  TO_CHAR(T526.C526_REQUIRED_DATE, '"
            + strDateFmt + "' ) SHIPDT,");
    sbQuery.append("  TO_CHAR(T7100.C7100_SURGERY_DATE, '" + strDateFmt + "' ) SURGDT,");
    sbQuery
        .append(" DECODE(T526.C526_STATUS_FL,'5','Pending Approval',10,'Open',20,'Allocated')STATUS , ");
    sbQuery
        .append(" T7100.C703_SALES_REP_ID REPID,   get_user_name(t526.C526_APPROVE_REJECT_BY) uname,  ");
    sbQuery
        .append("  DECODE( T7100.C901_TYPE,'1006505', get_party_primary_contact( GET_REP_PARTY_ID(T7100.C703_SALES_REP_ID) ,90452)  ");
    sbQuery
        .append(" ,'1006506', DECODE( get_rep_id_from_user_id (T7100.C7100_CREATED_BY) , NULL, get_user_emailid(T7100.C7100_CREATED_BY)  ");
    sbQuery
        .append(" , get_party_primary_contact( GET_REP_PARTY_ID( get_rep_id_from_user_id (T7100.C7100_CREATED_BY)  ),90452)) , get_rep_emailid (T7100.C703_SALES_REP_ID))  emailid, ");
    sbQuery
        .append(" GET_REP_NAME(T7100.C703_SALES_REP_ID) repnm, T526.C526_PRODUCT_REQUEST_DETAIL_ID reqdetailid, ");
    sbQuery
        .append(" TO_CHAR(t526.c526_approve_reject_date, '"
            + strDateFmt
            + "'||' hh:mi:ss am') rejdate, GET_DISTRIBUTOR_NAME(T7100.C701_DISTRIBUTOR_ID) fieldSales ");
    if(strCaseType.equals("1006506")){	 // Comment for 1006506 - consignment Set type for this PMT-53079
    	sbQuery.append(" ,NVL(get_user_name(t7100.c7100_created_by),'') REQUESTORNM ");
    }
    sbQuery
        .append(" FROM T7100_CASE_INFORMATION T7100,  T7104_CASE_SET_INFORMATION T7104,  T526_PRODUCT_REQUEST_DETAIL T526 , t907_shipping_info t907");
    sbQuery.append(" WHERE T7100.C7100_VOID_FL                IS NULL");
    sbQuery.append(" AND T7104.C7104_VOID_FL                  IS NULL");
    sbQuery.append(" AND T526.C526_VOID_FL                    IS NULL");
    sbQuery
        .append(" AND T526.C526_PRODUCT_REQUEST_DETAIL_ID   = T7104.C526_PRODUCT_REQUEST_DETAIL_ID");
    sbQuery.append(" AND T7100.C7100_CASE_INFO_ID              = T7104.C7100_CASE_INFO_ID");
    sbQuery.append(" AND to_char(T526.C526_PRODUCT_REQUEST_DETAIL_ID) = t907.c907_ref_id ");
    sbQuery.append(" AND T7104.C526_PRODUCT_REQUEST_DETAIL_ID IN (");
    if (!strProdReqDtlId.equals("") && !strProdReqDtlId.equals("0")) {
      sbQuery.append(strProdReqDtlId);
    }
    sbQuery.append(") AND T7100.C901_TYPE                      = '");
    if (!strCaseType.equals("") && !strCaseType.equals("0")) {
      sbQuery.append(strCaseType);
    }
    sbQuery.append("' AND T526.C526_STATUS_FL NOT              IN (40)");
    sbQuery.append(" ORDER BY T7100.C703_SALES_REP_ID,");
    sbQuery
        .append("  T7100.C7100_SURGERY_DATE, t907.c901_ship_to, t907.c907_ship_to_id, t907.c106_address_id, T526.C207_SET_ID");

    log.debug(" Query to fetchSetReqNotificationInfo :: " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  /**
   * Fetch Cases Info which are used to send email notification on Item Requests Approved by AD.
   * This method is called from 2 places, 1. To send Notification Emails to AD, 2. At the time of AD
   * approving the requests.
   * 
   * @param String inputString
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchADApprovalNotification(String strInputStr, String strStatus)
      throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_casebook_rpt.gm_fch_ad_aprvl_request_info", 3);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.setString(2, strStatus);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * Fetch Cases Info which are used to send email notification on Item Requests Approved by PD.
   * 
   * @param String inputString
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchPDApprovalNotification(String strInputStr) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_sm_casebook_rpt.gm_fch_pd_aprvl_request_info", 2);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * sendADRequestNotificationEmail will send the Email notification once item request is Requested
   * from Device.
   * 
   * @param String strReqIds
   * @return void
   * @exception AppError
   */
  public void sendADRequestNotificationEmail(String strReqIds) throws AppError {
    GmCaseBookRptBean gmcaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    ArrayList alList = new ArrayList();
    GmEmailProperties emailProps = null;
    String strJasperNm = "/GmItemRequestADNotificationEmail.jasper";
    int intArLength = 0;
    String strAdId = "";
    String strAdEmailId = null;
    String strCCRepEmailId = "";
    String strCaseID = "";
    String strReqId = "";
    String strParentReqId = "";
    String strPartNum = "";
    String strPartDesc = "";
    String strReqQty = "";
    String strApprQty = "";
    String strSubject = "";
    String strDefaultEmailID = "";
    String strRepID = "";
    String strRepName = "";
    alResult =
        GmCommonClass.parseNullArrayList(gmcaseBookRptBean.fetchADApprovalNotification(strReqIds,
            "2"));
    // To fetch email id from rules. In this way, incase if anybody need explicit access we can add
    // them.
    strDefaultEmailID =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ITEM_REQ_AD_NOTIFY", "DEFAULT_CC"));
    String strStepsToApprove =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("STEPS", "ITEM_REQ"));

    intArLength = alResult.size();

    if (intArLength > 0) {
      HashMap hmLoop = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmEmailData = new HashMap();
      String strPrevAdId = "";
      int intCount = 0;

      boolean blFlag = false;

      for (int i = 0; i < intArLength; i++) {
        hmLoop = (HashMap) alResult.get(i);
        if (i == 0) {
          strPrevAdId = GmCommonClass.parseNull((String) hmLoop.get("AD_ID"));
        }
        strAdId = GmCommonClass.parseNull((String) hmLoop.get("AD_ID"));
        strCCRepEmailId = GmCommonClass.parseNull((String) hmLoop.get("REP_EMAIL"));
        strCaseID = GmCommonClass.parseNull((String) hmLoop.get("ID"));
        strReqId = GmCommonClass.parseNull((String) hmLoop.get("REQ_ID"));
        strParentReqId = GmCommonClass.parseNull((String) hmLoop.get("PAR_REQID"));
        strPartNum = GmCommonClass.parseNull((String) hmLoop.get("PNUM"));
        strPartDesc = GmCommonClass.parseNull((String) hmLoop.get("PDESC"));
        strReqQty = GmCommonClass.parseNull((String) hmLoop.get("REQ_QTY"));
        strRepID = GmCommonClass.parseNull((String) hmLoop.get("REP_ID"));
        strRepName = GmCommonClass.parseNull((String) hmLoop.get("REP_NAME"));

        if (strAdId.equals(strPrevAdId)) {
          hmParam = new HashMap();
          hmParam.put("ID", strCaseID);
          hmParam.put("REQID", strReqId);
          hmParam.put("PARREQID", strParentReqId);
          hmParam.put("PNUM", strPartNum);
          hmParam.put("PDESC", strPartDesc);
          hmParam.put("REQQTY", strReqQty);
          hmParam.put("REPID", strRepID);
          hmParam.put("REPNAME", strRepName);
          alList.add(hmParam);
          intCount++;

        } else {
          // start
          emailProps = new GmEmailProperties();
          String TEMPLATE_NAME = "GmInvMgmItemReqNotifyEmail";
          emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.FROM));
          emailProps.setRecipients(strAdEmailId);
          emailProps.setCc(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.CC));
          emailProps.setCc(strCCRepEmailId);
          emailProps.setCc(strDefaultEmailID);
          emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.MIME_TYPE));
          strSubject =
              GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
          strSubject = GmCommonClass.replaceAll(strSubject, "<#STATUS>", "Requested");
          emailProps.setSubject(strSubject);
          hmEmailData.put("EMAILPROPS", emailProps);

          hmEmailData.put("STEPSTOAPPROVE", strStepsToApprove);
          GmJasperMail jasperMail = new GmJasperMail();
          jasperMail.setJasperReportName(strJasperNm);
          jasperMail.setAdditionalParams(hmEmailData);
          jasperMail.setReportData(alList);
          jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));
          HashMap hmjasperReturn = jasperMail.sendMail();
          strSubject = "";// Subject was showing multiple times, so need to remove previous value
          blFlag = false;
          alList.clear();
          // end

          hmParam = new HashMap();
          hmParam.put("ID", strCaseID);
          hmParam.put("REQID", strReqId);
          hmParam.put("PARREQID", strParentReqId);
          hmParam.put("PNUM", strPartNum);
          hmParam.put("PDESC", strPartDesc);
          hmParam.put("REQQTY", strReqQty);
          hmParam.put("REPID", strRepID);
          hmParam.put("REPNAME", strRepName);
          alList.add(hmParam);
          intCount = 1;
        }
        strAdEmailId = GmCommonClass.parseNull((String) hmLoop.get("AD_EMAIL_ID"));
        strPrevAdId = strAdId;
      }
      if (intCount > 0) {
        emailProps = new GmEmailProperties();
        String TEMPLATE_NAME = "GmInvMgmItemReqNotifyEmail";
        emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
            + GmEmailProperties.FROM));
        emailProps.setRecipients(strAdEmailId);
        emailProps
            .setCc(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.CC));
        emailProps.setCc(strCCRepEmailId);
        emailProps.setCc(strDefaultEmailID);
        emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
            + GmEmailProperties.MIME_TYPE));
        strSubject =
            GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
        strSubject = GmCommonClass.replaceAll(strSubject, "<#STATUS>", "Requested");
        emailProps.setSubject(strSubject);
        hmEmailData.put("EMAILPROPS", emailProps);

        hmEmailData.put("STEPSTOAPPROVE", strStepsToApprove);
        GmJasperMail jasperMail = new GmJasperMail();
        jasperMail.setJasperReportName(strJasperNm);
        jasperMail.setAdditionalParams(hmEmailData);
        jasperMail.setReportData(alList);
        jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));
        HashMap hmjasperReturn = jasperMail.sendMail();
      }
    }
  }
}
