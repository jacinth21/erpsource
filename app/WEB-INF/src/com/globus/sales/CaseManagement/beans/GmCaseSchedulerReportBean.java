/**
 * FileName : GmCaseSchedulerReportBean.java, Description : case scheduler Report bean, Author : N Raja
 */
package com.globus.sales.CaseManagement.beans;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseSchedulerReportBean extends GmBean {
	        Logger log = GmLogger.getInstance(this.getClass().getName());

	    public GmCaseSchedulerReportBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		    super(gmDataStoreVO);
	      }
	/**
	 * @description fetchReport used for case scheduler report page grid data 
	 * @param hmParam
	 * @return alReturn 
	 * @throws AppError
	 */
		public ArrayList fetchReport(HashMap hmParam) throws AppError {
		    ArrayList alReturn = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    StringBuffer sbQuery = new StringBuffer();
		    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
		    String strDateSelectName = GmCommonClass.parseZero((String) hmParam.get("DATESELECTION"));
		    log.debug("strDateSelectName=== "+strDateSelectName);
		    String strDivision = GmCommonClass.parseNull((String) hmParam.get("DIVISION"));
		    String strFieldSales = GmCommonClass.parseZero((String) hmParam.get("FIELDSALES"));
		    String strSalesRep = GmCommonClass.parseZero((String) hmParam.get("SALESREP"));
		    String strAccount = GmCommonClass.parseZero((String) hmParam.get("ACCOUNT"));
		    String strKitMapId = GmCommonClass.parseNull((String) hmParam.get("KITMAPID"));
		    String strKitDash = GmCommonClass.parseNull((String) hmParam.get("KITDASH"));
		    String strLongTerm = GmCommonClass.parseNull((String) hmParam.get("TERM"));
		    String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
		    String strCompanyId = getGmDataStoreVO().getCmpid();
		    log.debug("strCompanyId=>"+strCompanyId);
		    Date dtFromDT = (Date) hmParam.get("FROMDT");
		    Date dtToDT = (Date) hmParam.get("TODT");
		    String strFromDate = GmCommonClass.getStringFromDate(dtFromDT, strDateFmt);
		    String strToDate = GmCommonClass.getStringFromDate(dtToDT, strDateFmt);

		    sbQuery.append("SELECT distinct C7100_CASE_ID CASEID,GET_CODE_NAME(T7100.C901_CASE_STATUS) STATUS,T7100.C901_CASE_STATUS STATUSID, C7100_SURGERY_DATE SURGERYDATE,"
		    		+ " GET_DISTRIBUTOR_NAME (T7100.C701_DISTRIBUTOR_ID) FIELDSALES,GET_REP_NAME (T7100.C703_SALES_REP_ID) SALESREP,"
		    		+ " GET_ACCOUNT_NAME (T7100.C704_ACCOUNT_ID) ACCOUNTNM ");
		    sbQuery.append(" FROM T7100_CASE_INFORMATION T7100 ");
		    
		    if ((!strKitMapId.equals("")) && (strKitMapId != null)) {
		    	 sbQuery.append(" ,T7105_CASE_SCHEDULAR_DTL T7105 ");   
			}if(strKitDash.equals("KITDASH")){
				 sbQuery.append(" ,T7105_CASE_SCHEDULAR_DTL T7105 ");   
			}
		    sbQuery.append("WHERE T7100.C901_TYPE = '107285' AND ");
		    
		    if ((strKitMapId != null) && (!strKitMapId.equals(""))) {
		    sbQuery.append(" T7100.C7100_CASE_INFO_ID = T7105.C7100_CASE_INFO_ID(+) AND");
		    }
		    if(strKitDash.equals("KITDASH")){
		    	sbQuery.append(" T7100.C7100_CASE_INFO_ID = T7105.C7100_CASE_INFO_ID AND ");
		    }if(strLongTerm.equals("107681")){
		    	sbQuery.append(" T7100.C7100_LONG_TERM_FL = 'Y' AND ");
		    }else if(strLongTerm.equals("107680")){
		    	sbQuery.append(" T7100.C7100_LONG_TERM_FL = 'N' AND ");
		    }
		    if (strDateSelectName.equals("107300")) {
			      sbQuery.append(" T7100.C7100_SURGERY_DATE ");
			}
		    if (strDateSelectName.equals("107301")) {
			      sbQuery.append(" T7100.C7100_SHIP_DATE ");
			}
		    if (strDateSelectName.equals("107302")) {
			      sbQuery.append(" T7100.C7100_RETURN_DATE ");
			 }
		    sbQuery.append(" BETWEEN TO_DATE('"+strFromDate+"','"+strDateFmt+"') AND TO_DATE('"+strToDate+"','"+strDateFmt+"')");
		  
		    if(!(strDivision.equals("") || strDivision.equals("0"))) {
		    sbQuery.append(" AND T7100.C1910_DIVISION_ID = '" + strDivision + "'");
		    }
		    if ((!strFieldSales.equals("0"))) {
			      sbQuery.append(" AND t7100.C701_DISTRIBUTOR_ID = '" + strFieldSales + "'");
			}
		    
		    if ((!strSalesRep.equals("0"))) {
			      sbQuery.append(" AND t7100.C703_SALES_REP_ID = '" + strSalesRep + "'");
			}
		    if ((!strAccount.equals("0"))) {
			      sbQuery.append(" AND t7100.C704_ACCOUNT_ID = '" + strAccount + "'");
			}
		    if ((strKitMapId != null) && (!strKitMapId.equals(""))) {
		    	  sbQuery.append(" AND T7105.C2078_KIT_MAP_ID =  '" + strKitMapId + "'");
		    	  sbQuery.append(" AND T7105.C7105_VOID_FL IS NULL ");
			}
		    if((strCompanyId != null) && (!strCompanyId.equals(""))){
		    	 sbQuery.append(" AND T7100.C1900_COMPANY_ID = '" + strCompanyId + "'");
		    }
		    if (!(strStatus.equals("0")) || strStatus.equals("")) {
		        strStatus = strStatus.replaceAll(",", "','");
		        sbQuery.append(" AND ");
		        sbQuery.append(" T7100.C901_CASE_STATUS IN ('");
		        sbQuery.append(strStatus);
		        sbQuery.append("')");
		    }
		    sbQuery.append(" AND T7100.C7100_VOID_FL IS NULL ORDER BY T7100.C7100_SURGERY_DATE ");
		    log.debug("fetchReport==> " + sbQuery.toString());
		    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		    gmDBManager.close();
		    return alReturn;
		}
		
		/**
		 * @description loadInventoryInfo used for case id clicked view grid data
		 * @param hmParam
		 * @return alReturn
		 * @throws AppError
		 */
		public ArrayList loadInventoryInfo(HashMap hmParam) throws AppError {
			    StringBuffer sbQuery = new StringBuffer();
			    ArrayList alReturn = new ArrayList();
			    String strCaseId = GmCommonClass.parseZero((String) hmParam.get("paramCaseId"));
			    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			    
			    sbQuery.append(" SELECT T2078.C2078_KIT_NM KITNAME, T2079.C207_SET_ID SETID, ");
			    sbQuery.append(" GET_SET_NAME(T2079.C207_SET_ID) SETNAME, T2079.C5010_TAG_ID TAGID ");
			    sbQuery.append(" FROM T7100_CASE_INFORMATION T7100, T7105_CASE_SCHEDULAR_DTL t7105, ");
			    sbQuery.append(" T2078_KIT_MAPPING T2078, T2079_KIT_MAP_DETAILS T2079 ");
			    sbQuery.append(" WHERE T7100.C7100_CASE_INFO_ID =  T7105.C7100_CASE_INFO_ID ");
			    sbQuery.append(" AND T2078.C2078_KIT_MAP_ID = T2079.C2078_KIT_MAP_ID ");
			    sbQuery.append(" AND T2078.C2078_KIT_MAP_ID = T7105.C2078_KIT_MAP_ID ");
			    sbQuery.append(" AND T7100.C7100_CASE_ID  = '" + strCaseId + "' AND ");			    
			    sbQuery.append(" T7100.C7100_VOID_FL IS NULL ");
			    sbQuery.append(" AND T7105.C7105_VOID_FL IS NULL ");
			    sbQuery.append(" AND T2078.C2078_VOID_FL IS NULL ");
			    sbQuery.append(" AND T2079.C2079_VOID_FL IS NULL ");
			    
			    log.debug("loadInventoryInfo==> " + sbQuery.toString());
			    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
			    gmDBManager.close();
			    return alReturn;
		   }
 }
