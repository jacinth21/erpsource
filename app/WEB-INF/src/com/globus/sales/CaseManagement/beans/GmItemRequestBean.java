package com.globus.sales.CaseManagement.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;
/**********************************************************************************
 * File		 		: GmItemRequestBean
 * Desc		 		: Bean For Item Request Approval(GmItemRequestApprlAction).
 * author			: SaravananM
 ************************************************************************************/
public class GmItemRequestBean extends GmSalesFilterConditionBean{
	 Logger log = GmLogger.getInstance(this.getClass().getName());

	  public GmItemRequestBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmItemRequestBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
//PC-3849 - back order for item request
	public String saveRequestInfo( String strApprvQtyString,
			String strBoQtyString,String strCompanyInfo) {
		String strRequestId="";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
	    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
	    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33511
	    String strUserId = "";
	    HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33511
	    gmDBManager.setPrepareString("gm_pkg_item_apprv_txn.gm_sav_request_frm_item", 5);
	    gmDBManager.setString(1, strApprvQtyString);
	    gmDBManager.setString(2, strBoQtyString);
	    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
	    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
	    gmDBManager.execute();
	    String strReqIds = GmCommonClass.parseNull(gmDBManager.getString(3));
	    String strConsignmentId = GmCommonClass.parseNull(gmDBManager.getString(4));
	    String strBoId = GmCommonClass.parseNull(gmDBManager.getString(5));
	    strReqIds=strReqIds.concat(",").concat(strBoId);
	    /* checkAndSplitConsignamnet() - check wheather the viacell parts are availabe in the request
	     * if it's available then split the request based on viacell and non-viacell parts
	     * */
	    if (!strReqIds.equals("")){
	     gmTxnSplitBean.checkAndSplitConsignamnet(gmDBManager, strReqIds, strUserId);
	    }
	    log.debug("strBoId - "+strBoId);
	    log.debug("strReqIds - "+strReqIds);
	    log.debug("strConsignmentId - "+strConsignmentId);
	    log.debug("strConsignmentId in saveRequestInfo():::::::" + strConsignmentId);
	     gmDBManager.commit();
	     // To Save the Insert part number details for Consignment from GlobusAPP Using JMS CALL /106761-consignment
	     HashMap hmJMSParam = new HashMap();
	     hmJMSParam.put("TXNIDSTR", strConsignmentId);
	     hmJMSParam.put("TXNTYPE", "40021");
	     hmJMSParam.put("STATUS", "93342");
	     hmJMSParam.put("USERID", strUserId);
	     hmJMSParam.put("COMPANYINFO", strCompanyInfo);
	     gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);
	     
	   //JMS call for Storing Building Info PMT-33511  
	     log.debug("Storing building information saveRequestInfo ");
	     hmStorageBuildInfo.put("TXNID", strConsignmentId);
	     hmStorageBuildInfo.put("TXNTYPE", "CONSIGNMENT");
	     gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);
	     
	   
	    return strReqIds;
	}
}
