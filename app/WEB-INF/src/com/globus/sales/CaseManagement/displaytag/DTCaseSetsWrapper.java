package com.globus.sales.CaseManagement.displaytag;

import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.forms.GmGroupPartMapForm;
import com.globus.sales.CaseManagement.forms.GmCasePostForm;

public class DTCaseSetsWrapper extends TableDecorator{
	

	private HashMap db;
    private String strSetId; 
    private String strSetNM; 
    private String strTAGID = "";
    private boolean bnSetDelFl = false;
    private boolean blLoanerFlag = false;
    public StringBuffer strValue = new StringBuffer(); 
    private int i = 0;
    private GmCasePostForm gmCasePostForm;

    public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmCasePostForm = (GmCasePostForm)getPageContext().getAttribute("frmCasePost", PageContext.REQUEST_SCOPE);	
	}
    
public String getSETID(){
	String strParentForm = gmCasePostForm.getParentForm();

	db = 	(HashMap) this.getCurrentRowObject();
	strValue.setLength(0);
	strSetId=GmCommonClass.parseNull((String)db.get("SETID")); 
	strSetNM=GmCommonClass.parseNull((String)db.get("SETNM"));
	strSetNM =  strSetNM.replaceAll(" ", "-"); 
	String strShippedDelFl = GmCommonClass.parseNull((String) db.get("SHIPPEDDELFL"));

	if (strShippedDelFl.equals("Y")){
		if(!bnSetDelFl){
			strValue.append("<input type=\"hidden\" value=\"Y\"  name=\"hTagDelFl\" >");
			bnSetDelFl = true;
		}
		if (!strParentForm.equals("SALESDASHBOARD")) {
			strValue.append("<a href =\"#\" onClick=\"return doOnLoad('"+strSetId+"','" +strSetNM+"'"+")\">");
		}
		strValue.append("<font style=\"color:red;\">");
		strValue.append(strSetId);
		strValue.append("</font></a>");
	}else{
		if (!strParentForm.equals("SALESDASHBOARD")) {
			strValue.append("<a href =\"#\" onClick=\"return doOnLoad('"+strSetId+"','" +strSetNM+"'"+")\">"+strSetId+"</a>");
		} else {
			strValue.append(strSetId);
		}
	}
	return strValue.toString();
}

public String getSORTSETID(){
	db = 	(HashMap) this.getCurrentRowObject();
	strValue.setLength(0);
	strSetId=GmCommonClass.parseNull((String)db.get("SETID")); 
	strValue.append(strSetId);
	return strValue.toString();
}

public String getSETNM(){
	db = 	(HashMap) this.getCurrentRowObject();
	strValue.setLength(0);
	strSetNM=GmCommonClass.parseNull((String)db.get("SETNM"));
	String strShippedDelFl = GmCommonClass.parseNull((String) db.get("SHIPPEDDELFL"));
	if (strShippedDelFl.equals("Y")){
		strValue.append("<font style=\"color:red;\">");
		strValue.append(strSetNM);
		strValue.append("</font></a>");
	}else{
		strValue.append(strSetNM);
	}
	return strValue.toString();

}

public String getTAGID() {
	db = (HashMap) this.getCurrentRowObject();
	strTAGID = GmCommonClass.parseNull((String) db.get("TAGID")); 
	String strSetID = GmCommonClass.parseNull((String) db.get("SETID"));
	String strSetName = GmCommonClass.parseNull((String) db.get("SETNM"));
	String strCaseDistId = GmCommonClass.parseNull((String) db.get("CASEDISTID"));
	String strCaseSetId = GmCommonClass.parseNull((String) db.get("CASESETID"));
	String strCaseInfoId = GmCommonClass.parseNull((String) db.get("CASEINFOID"));
	String strStatusID = GmCommonClass.parseNull((String) db.get("STATUSID"));
	String strShippedDelFl = GmCommonClass.parseNull((String) db.get("SHIPPEDDELFL"));
	String strLockFromDt = GmCommonClass.parseNull((String) db.get("LOCKFROMDT"));
	String strLockToDt = GmCommonClass.parseNull((String) db.get("LOCKTODT"));
	String strParentForm = gmCasePostForm.getParentForm();
	int intSurgeryDays = Integer.parseInt(GmCommonClass.parseZero((String) db.get("SURGERYDAYS")));
	int TagImgCnt = Integer.parseInt(GmCommonClass.parseZero((String) db.get("TAGIMAGECNT")));
	strValue.setLength(0);
	if (!strParentForm.equals("SALESDASHBOARD")) {
		if(!strTAGID.equals("TBD")&&!strTAGID.equals("")&& TagImgCnt>0)
		{
			strValue.append("<img src='/images/jpg_icon.jpg' alt='Tag' width='16' height='16' id='imgEdit' style='cursor:hand' title='View Image' onclick=\"return viewImg('"+strTAGID+"')\" /> ");
		} 
		if(strStatusID.equals("11090")|| ((strStatusID.equals("11092") || strStatusID.equals("11093"))&& strShippedDelFl.equals("Y")))//Draft
		{
			strValue.append("  <a href='#none'  style='color: #000; text-decoration: none; cursor:default'>" + strTAGID +"</a>");
		}
		else if(!strStatusID.equals("11092")&&!strStatusID.equals("11093"))
		{
			//Comment for MNTTASK-7882 Auto Allocation
			//if (intSurgeryDays >= 0 && !strShippedDelFl.equals("Y")){
			if (!strShippedDelFl.equals("Y") && !strStatusID.equals("11094")){
				strValue.append(" <img id='imgOverride' style='cursor:hand' src='/images/detail_icon.gif' title='Set Override' width='16' height='16' onClick=\"fnSetOverride('");
				strValue.append(strSetID);
				strValue.append("' , '" );
				strValue.append(strSetName);
				strValue.append("' , '" );
				strValue.append(strCaseDistId);
				strValue.append("' , '" );
				strValue.append(strCaseSetId);
				strValue.append("' , '" );
				strValue.append(strCaseInfoId);
				strValue.append("' , '" );
				strValue.append(strLockFromDt);
				strValue.append("' , '" );
				strValue.append(strLockToDt);
				strValue.append("');\">" );
			}
			strValue.append("  <a href='#none'  style='color: #000; text-decoration: none; cursor:default'>" + strTAGID +"</a>");
		}
	} else {
		strValue.append("  <a href='#none'  style='color: #000; text-decoration: none; cursor:default'>" + strTAGID +"</a>");
	}
	return strValue.toString();
}

public String getSORTTAGID() {
	db = (HashMap) this.getCurrentRowObject();
	strTAGID = GmCommonClass.parseNull((String) db.get("TAGID")); 
	strValue.setLength(0);
	strValue.append(strTAGID);
	return strValue.toString();
}

public String getSHIPIN() {
	db = (HashMap) this.getCurrentRowObject();
	
	String strShipIn = GmCommonClass.parseNull((String) db.get("SHIPIN"));
	String strShipInDetail = GmCommonClass.parseNull((String) db.get("SHIPINDETAIL"));
	String strLocationType = GmCommonClass.parseNull((String) db.get("LOCATIONTYPE"));
	String strStatus = GmCommonClass.parseNull((String) db.get("STATUS"));
	String strParentForm = gmCasePostForm.getParentForm();
	strValue.setLength(0);
	if(!strStatus.equals("Rejected")){//For rejected request no need to show shipping address
		strValue.append(strShipIn);
		if((strLocationType.equals("11381") || strLocationType.equals("11378"))&& !strShipIn.equals("") && !strStatus.equals("Closed"))
		{
			if (!strParentForm.equals("SALESDASHBOARD")) {
			    strValue.append("<br> <img id='editadd' style='cursor:hand' src='/images/edit.jpg' title='Edit Address' width='23' height='16'  onclick=\"return fnAddress('"+strShipInDetail+"','"+strLocationType+"');\"/>");
			}
		} 
	}
	return strValue.toString();
}

public String getINITLN() {
	db = (HashMap) this.getCurrentRowObject();
	String strTAGID = GmCommonClass.parseNull((String) db.get("TAGID")); 
	String strSetID = GmCommonClass.parseNull((String) db.get("SETID"));
	String strCaseSetId = GmCommonClass.parseNull((String) db.get("CASESETID"));
	String strLocationType = GmCommonClass.parseNull((String) db.get("LOCATIONTYPE"));
	String strLoanerFlag = GmCommonClass.parseNull((String) db.get("LOANERFLAG"));
	String strStatusID = GmCommonClass.parseNull((String) db.get("STATUSID"));
	
	int intSurgeryDays = Integer.parseInt(GmCommonClass.parseZero((String) db.get("SURGERYDAYS")));
	strValue.setLength(0);
	if(!strLocationType.equals("11381") && strLoanerFlag.equals("Y") && intSurgeryDays >= 0 && strStatusID.equals("11091"))
	/* 11381 - Loaner already allocated
	 * LoanerFlag - Set is participating in Loaner program
	 * Surgery date should be greater than equal to current date
	 */
	{
		strValue.append("<input type=\"checkbox\" value=\"" + strCaseSetId + "\"  name=\"chkLoan"+i+"\" onClick=\"fnChkLoaner(this);\">" );
		strValue.append("<input type=\"hidden\" value="+strTAGID+"  name=\"hTag" + strCaseSetId + "\" id=\"hTag" + strCaseSetId + "\" >");
		strValue.append("<input type=\"hidden\" value="+strSetID+"  name=\"hSet" + strCaseSetId + "\" id=\"hSet" + strCaseSetId + "\" >");
		if (!blLoanerFlag)
		{
			strValue.append("<input type=\"hidden\" value=\"Y\"  name=\"hLoanerFlag\" id=\"hLoanerFlag\" >");
			blLoanerFlag = true;
		}
		i++;
	} 

	return strValue.toString();
}
 
}
