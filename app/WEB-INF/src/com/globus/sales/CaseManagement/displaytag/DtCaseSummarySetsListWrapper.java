package com.globus.sales.CaseManagement.displaytag;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DtCaseSummarySetsListWrapper extends TableDecorator{
	 private HashMap hmRow ;
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
	public String getTAGID() {
		hmRow = (HashMap) this.getCurrentRowObject();
		String strTagID = GmCommonClass.parseNull((String) hmRow.get("TAGID"));
		String strSetID = GmCommonClass.parseNull((String) hmRow.get("SETID"));
		String strSetName = GmCommonClass.parseNull((String) hmRow.get("SETNM"));
		String strCaseDistId = GmCommonClass.parseNull((String) hmRow.get("CASEDISTID"));
		String strCaseSetId = GmCommonClass.parseNull((String) hmRow.get("CASESETID"));
		StringBuffer sbHtml = new StringBuffer();
		sbHtml.append(strTagID);
		sbHtml.append(" <img id='imgOverride' style='cursor:hand' src='/images/detail_icon.gif' title='Set Override' width='20' height='22' onClick=\"fnSetOverride('");
		sbHtml.append(strSetID);
		sbHtml.append("' , '" );
		sbHtml.append(strSetName);
		sbHtml.append("' , '" );
		sbHtml.append(strCaseDistId);
		sbHtml.append("' , '" );
		sbHtml.append(strCaseSetId);
		sbHtml.append("');\">" );
		return sbHtml.toString();
	}	
}
