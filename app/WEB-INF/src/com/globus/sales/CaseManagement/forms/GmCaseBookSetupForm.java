package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCancelForm;

public class GmCaseBookSetupForm extends GmCancelForm{
	
	private Date surgeryDate = null ;
	private String surgeryTimeHour="";
	private String surgeryTimeMin="";
	private String surgeryAMPM="";
	private String distributorId = "";
	private String repId = "";
	private String accountId = "";
	private String caseStatus = "";
	private String personalNote = "";
	private String caseInfoID = "";
	private String caseID = "";
	
	private ArrayList alSurgeryTimeHour = new ArrayList();
	private ArrayList alSurgeryTimeMin = new ArrayList();
	private ArrayList alSurgeryAMPM = new ArrayList();
	
	private ArrayList alDistList = new ArrayList();
	private ArrayList alRepList = new ArrayList();
	private ArrayList alAcctList = new ArrayList();
	
	
	
	
	/**
	 * @return the caseID
	 */
	public String getCaseID() {
		return caseID;
	}
	/**
	 * @param caseID the caseID to set
	 */
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the personalNote
	 */
	public String getPersonalNote() {
		return personalNote;
	}
	/**
	 * @param personalNote the personalNote to set
	 */
	public void setPersonalNote(String personalNote) {
		this.personalNote = personalNote;
	}
	/**
	 * @return the caseStatus
	 */
	public String getCaseStatus() {
		return caseStatus;
	}
	/**
	 * @param caseStatus the caseStatus to set
	 */
	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}
	/**
	 * @return the alSurgeryTimeHour
	 */
	public ArrayList getAlSurgeryTimeHour() {
		return alSurgeryTimeHour;
	}
	/**
	 * @param alSurgeryTimeHour the alSurgeryTimeHour to set
	 */
	public void setAlSurgeryTimeHour(ArrayList alSurgeryTimeHour) {
		this.alSurgeryTimeHour = alSurgeryTimeHour;
	}
	/**
	 * @return the alSurgeryTimeMin
	 */
	public ArrayList getAlSurgeryTimeMin() {
		return alSurgeryTimeMin;
	}
	/**
	 * @param alSurgeryTimeMin the alSurgeryTimeMin to set
	 */
	public void setAlSurgeryTimeMin(ArrayList alSurgeryTimeMin) {
		this.alSurgeryTimeMin = alSurgeryTimeMin;
	}
	/**
	 * @return the alSurgeryAMPM
	 */
	public ArrayList getAlSurgeryAMPM() {
		return alSurgeryAMPM;
	}
	/**
	 * @param alSurgeryAMPM the alSurgeryAMPM to set
	 */
	public void setAlSurgeryAMPM(ArrayList alSurgeryAMPM) {
		this.alSurgeryAMPM = alSurgeryAMPM;
	}

	/**
	 * @return the distributorId
	 */
	public String getDistributorId() {
		return distributorId;
	}
	/**
	 * @param distributorId the distributorId to set
	 */
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}
	/**
	 * @return the repId
	 */
	public String getRepId() {
		return repId;
	}
	/**
	 * @param repId the repId to set
	 */
	public void setRepId(String repId) {
		this.repId = repId;
	}
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the alDistList
	 */
	public ArrayList getAlDistList() {
		return alDistList;
	}
	/**
	 * @param alDistList the alDistList to set
	 */
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the alAcctList
	 */
	public ArrayList getAlAcctList() {
		return alAcctList;
	}
	/**
	 * @param alAcctList the alAcctList to set
	 */
	public void setAlAcctList(ArrayList alAcctList) {
		this.alAcctList = alAcctList;
	}
	 
	/**
	 * @return the surgeryDate
	 */
	public Date getSurgeryDate() {
		return surgeryDate;
	}
	/**
	 * @param surgeryDate the surgeryDate to set
	 */
	public void setSurgeryDate(Date surgeryDate) {
		this.surgeryDate = surgeryDate;
	}
	/**
	 * @return the surgeryTimeHour
	 */
	public String getSurgeryTimeHour() {
		return surgeryTimeHour;
	}
	/**
	 * @param surgeryTimeHour the surgeryTimeHour to set
	 */
	public void setSurgeryTimeHour(String surgeryTimeHour) {
		this.surgeryTimeHour = surgeryTimeHour;
	}
	/**
	 * @return the surgeryTimeMin
	 */
	public String getSurgeryTimeMin() {
		return surgeryTimeMin;
	}
	/**
	 * @param surgeryTimeMin the surgeryTimeMin to set
	 */
	public void setSurgeryTimeMin(String surgeryTimeMin) {
		this.surgeryTimeMin = surgeryTimeMin;
	}
	/**
	 * @return the surgeryAMPM
	 */
	public String getSurgeryAMPM() {
		return surgeryAMPM;
	}
	/**
	 * @param surgeryAMPM the surgeryAMPM to set
	 */
	public void setSurgeryAMPM(String surgeryAMPM) {
		this.surgeryAMPM = surgeryAMPM;
	}
	
	 
	
}
