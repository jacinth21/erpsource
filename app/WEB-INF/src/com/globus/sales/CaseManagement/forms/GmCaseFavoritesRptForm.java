package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmCaseFavoritesRptForm extends GmCommonForm{
	
	private String rep="";
	private String favoriteNm="";
	private String gridData="";
	
	ArrayList alRep = new ArrayList();
	ArrayList alResult = new ArrayList();

	/**
	 * @return the rep
	 */
	public String getRep() {
		return rep;
	}

	/**
	 * @param rep the rep to set
	 */
	public void setRep(String rep) {
		this.rep = rep;
	}

	/**
	 * @return the favoriteNm
	 */
	public String getFavoriteNm() {
		return favoriteNm;
	}

	/**
	 * @param favoriteNm the favoriteNm to set
	 */
	public void setFavoriteNm(String favoriteNm) {
		this.favoriteNm = favoriteNm;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}

	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}

	/**
	 * @return the alRep
	 */
	public ArrayList getAlRep() {
		return alRep;
	}

	/**
	 * @param alRep the alRep to set
	 */
	public void setAlRep(ArrayList alRep) {
		this.alRep = alRep;
	}

	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}

	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	

}
