package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCancelForm;

public class GmCaseBookSetDtlsForm extends GmCancelForm{
	
	private String favouriteCaseID=""; 
	 
	private String caseInfoID = "";
	private String caseID = "";
	private String hinputStr = "";
	private String accessFL = "";
	private String dbaccessFL = "";
	private String categoryList = "";
	private String screenTitle = "";
	private String fromscreen = ""; 
	private String productionType = "";
	private String fwdProductType = "";
	private String eventName  = "";
	private String eventEndDate = "";
	private String eventStartDate  = "";
	private String eventPurpose = "";
	private String purpose = "";
	private String plannedShipDate 	 = "";
	private String expReturnDate 	 = "";
	private String screenNote  = "";
	private String eventEditSts  = "";
	private String backScreen    = "";
	private String companyInfo    = "";
	
	
	public String getEventEditSts() {
		return eventEditSts;
	}
	public void setEventEditSts(String eventEditSts) {
		this.eventEditSts = eventEditSts;
	}
	public String getExpReturnDate() {
		return expReturnDate;
	}
	public void setExpReturnDate(String expReturnDate) {
		this.expReturnDate = expReturnDate;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventEndDate() {
		return eventEndDate;
	}
	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}
	public String getEventStartDate() {
		return eventStartDate;
	}
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	public String getEventPurpose() {
		return eventPurpose;
	}
	public void setEventPurpose(String eventPurpose) {
		this.eventPurpose = eventPurpose;
	}
	public String getPlannedShipDate() {
		return plannedShipDate;
	}
	public void setPlannedShipDate(String plannedShipDate) {
		this.plannedShipDate = plannedShipDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList getAlEventInfoList() {
		return alEventInfoList;
	}
	public void setAlEventInfoList(ArrayList alEventInfoList) {
		this.alEventInfoList = alEventInfoList;
	}

	private String status = "";	
	private ArrayList alSystemSets = new ArrayList();
	private ArrayList alBookedSets = new ArrayList();
	private ArrayList alEventInfoList = new ArrayList();
	HashMap hmGenerlInfo = new HashMap(); 
	ArrayList alCaseAttrDtls = new ArrayList();
	
	


	public String getFwdProductType() {
		return fwdProductType;
	}
	public void setFwdProductType(String fwdProductType) {
		this.fwdProductType = fwdProductType;
	}
	public String getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(String categoryList) {
		this.categoryList = categoryList;
	}
	public String getScreenTitle() {
		return screenTitle;
	}
	public void setScreenTitle(String screenTitle) {
		this.screenTitle = screenTitle;
	}

	private ArrayList alAllBookedSets = new ArrayList();
	
	
	
	/**
	 * @return the accessFL
	 */
	public String getAccessFL() {
		return accessFL;
	}
	/**
	 * @param accessFL the accessFL to set
	 */
	public void setAccessFL(String accessFL) {
		this.accessFL = accessFL;
	}
	/**
	 * @return the dbaccessFL
	 */
	public String getDbaccessFL() {
		return dbaccessFL;
	}
	/**
	 * @param dbaccessFL the dbaccessFL to set
	 */
	public void setDbaccessFL(String dbaccessFL) {
		this.dbaccessFL = dbaccessFL;
	}
	/**
	 * @return the alAllBookedSets
	 */
	public ArrayList getAlAllBookedSets() {
		return alAllBookedSets;
	}
	/**
	 * @param alAllBookedSets the alAllBookedSets to set
	 */
	public void setAlAllBookedSets(ArrayList alAllBookedSets) {
		this.alAllBookedSets = alAllBookedSets;
	}
	/**
	 * @return the hmGenerlInfo
	 */
	public HashMap getHmGenerlInfo() {
		return hmGenerlInfo;
	}
	/**
	 * @param hmGenerlInfo the hmGenerlInfo to set
	 */
	public void setHmGenerlInfo(HashMap hmGenerlInfo) {
		this.hmGenerlInfo = hmGenerlInfo;
	}
	
	public ArrayList getAlCaseAttrDtls() {
		return alCaseAttrDtls;
	}
	public void setAlCaseAttrDtls(ArrayList alCaseAttrDtls) {
		this.alCaseAttrDtls = alCaseAttrDtls;
	}
	/**
	 * @return the fromscreen
	 */
	public String getFromscreen() {
		return fromscreen;
	}
	/**
	 * @param fromscreen the fromscreen to set
	 */
	public void setFromscreen(String fromscreen) {
		this.fromscreen = fromscreen;
	}
	/**
	 * @return the productionType
	 */
	public String getProductionType() {
		return productionType;
	}
	/**
	 * @param productionType the productionType to set
	 */
	public void setProductionType(String productionType) {
		this.productionType = productionType;
	}
	/**
	 * @return the hinputStr
	 */
	public String getHinputStr() {
		return hinputStr;
	}
	/**
	 * @param hinputStr the hinputStr to set
	 */
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}
	/**
	 * @return the favouriteCaseID
	 */
	public String getFavouriteCaseID() {
		return favouriteCaseID;
	}
	/**
	 * @param favouriteCaseID the favouriteCaseID to set
	 */
	public void setFavouriteCaseID(String favouriteCaseID) {
		this.favouriteCaseID = favouriteCaseID;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the caseID
	 */
	public String getCaseID() {
		return caseID;
	}
	/**
	 * @param caseID the caseID to set
	 */
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}
	/**
	 * @return the alSystemSets
	 */
	public ArrayList getAlSystemSets() {
		return alSystemSets;
	}
	/**
	 * @param alSystemSets the alSystemSets to set
	 */
	public void setAlSystemSets(ArrayList alSystemSets) {
		this.alSystemSets = alSystemSets;
	}
	/**
	 * @return the alBookedSets
	 */
	public ArrayList getAlBookedSets() {
		return alBookedSets;
	}
	/**
	 * @param alBookedSets the alBookedSets to set
	 */
	public void setAlBookedSets(ArrayList alBookedSets) {
		this.alBookedSets = alBookedSets;
	}
	/**
	 * @return the screenNote
	 */
	public String getScreenNote() {
		return screenNote;
	}
	/**
	 * @param screenNote the screenNote to set
	 */
	public void setScreenNote(String screenNote) {
		this.screenNote = screenNote;
	}
	/**
	 * @return the backScreen
	 */
	public String getBackScreen() {
		return backScreen;
	}
	/**
	 * @param backScreen the backScreen to set
	 */
	public void setBackScreen(String backScreen) {
		this.backScreen = backScreen;
	}
	/**
	 * @return the companyInfo
	 */
	public String getCompanyInfo() {
		return companyInfo;
	}
	/**
	 * @param companyInfo the companyInfo to set
	 */
	public void setCompanyInfo(String companyInfo) {
		this.companyInfo = companyInfo;
	}
	
	 
	
}
