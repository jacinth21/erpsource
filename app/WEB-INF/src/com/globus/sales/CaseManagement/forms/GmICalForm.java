package com.globus.sales.CaseManagement.forms;


import com.globus.common.forms.GmCommonForm;

public class GmICalForm extends GmCommonForm{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int startday;
	private int startmonth;
	private int startyear;
	private int startHR;
	private int startMIN;
	private int endday;
	private int endmonth;
	private int endyear;
	private int endHR;
	private int endMIN;
	private String eventHeader="";
	private String subject="";
	private String messageDesc="";
	private String desc="";
	private String categories="";
	
	
	
	public String getCategories() {
		return categories;
	}
	public void setCategories(String categories) {
		this.categories = categories;
	}
	public int getStartday() {
		return startday;
	}
	public void setStartday(int startday) {
		this.startday = startday;
	}
	public int getStartmonth() {
		return startmonth;
	}
	public void setStartmonth(int startmonth) {
		this.startmonth = startmonth;
	}
	public int getStartyear() {
		return startyear;
	}
	public void setStartyear(int startyear) {
		this.startyear = startyear;
	}
	public int getStartHR() {
		return startHR;
	}
	public void setStartHR(int startHR) {
		this.startHR = startHR;
	}
	public int getStartMIN() {
		return startMIN;
	}
	public void setStartMIN(int startMIN) {
		this.startMIN = startMIN;
	}
	public int getEndday() {
		return endday;
	}
	public void setEndday(int endday) {
		this.endday = endday;
	}
	public int getEndmonth() {
		return endmonth;
	}
	public void setEndmonth(int endmonth) {
		this.endmonth = endmonth;
	}
	public int getEndyear() {
		return endyear;
	}
	public void setEndyear(int endyear) {
		this.endyear = endyear;
	}
	public int getEndHR() {
		return endHR;
	}
	public void setEndHR(int endHR) {
		this.endHR = endHR;
	}
	public int getEndMIN() {
		return endMIN;
	}
	public void setEndMIN(int endMIN) {
		this.endMIN = endMIN;
	}
	public String getEventHeader() {
		return eventHeader;
	}
	public void setEventHeader(String eventHeader) {
		this.eventHeader = eventHeader;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessageDesc() {
		return messageDesc;
	}
	public void setMessageDesc(String messageDesc) {
		this.messageDesc = messageDesc;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	 
	
}
