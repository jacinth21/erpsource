/**
 * 
 */
package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

/**
 * @author smanimaran
 *
 */
public class GmItemRequestForm extends GmLogForm {
	  
	private ArrayList alResult = new ArrayList();
	  private String strReqIds = "";
	  private String appvlAccessFl = "";
	  private String rejectAccessFl = "";
	  private String xmlGridData = "";
	  private String strTxnType = "";
	  private String requestId = "";
	  private String eventName = "";
	  private String startDt = "";
	  private String endDt = "";
	  private String requestType = "";
	  private String salesRep = "";
	  private String fieldSales = "";
	  private String nextApprStatus = "";
	  private String userType = "";
	  private String hApproveInputString = "";
	  private String prReqDetLitePnum = "";
	  private String systemid = "";
	  private String partString = "";
	  private String raqtyString = "";
	  private String sheetOwner = "";
	  private String chk_ShowArrowFl = "";
	  private String email_to = "";
	  private String email_cc = "";
	  private String email_subject = "";
	  private String email_comments = "";
	  private String requestor_name = "";
	  private String message = "";
	  private String raId = "";
	  private String chkbox_display = "";
	  private String request_id = "";

	  private ArrayList alEventName = new ArrayList();
	  private ArrayList alSalesReps = new ArrayList();
	  private ArrayList alFieldSales = new ArrayList();
	  private ArrayList alSystemList = new ArrayList();
	  private ArrayList alDSOwner = new ArrayList();
	  
	  private String apprvString = "";
	  private String boString = "";
	  private String strResLoad = "";
	  
	  
	
	/**
	 * @return the strResLoad
	 */
	public String getStrResLoad() {
		return strResLoad;
	}
	/**
	 * @param strResLoad the strResLoad to set
	 */
	public void setStrResLoad(String strResLoad) {
		this.strResLoad = strResLoad;
	}
	/**
	 * @return the apprvString
	 */
	public String getApprvString() {
		return apprvString;
	}
	/**
	 * @param apprvString the apprvString to set
	 */
	public void setApprvString(String apprvString) {
		this.apprvString = apprvString;
	}
	/**
	 * @return the boString
	 */
	public String getBoString() {
		return boString;
	}
	/**
	 * @param boString the boString to set
	 */
	public void setBoString(String boString) {
		this.boString = boString;
	}
	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}
	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	/**
	 * @return the strReqIds
	 */
	public String getStrReqIds() {
		return strReqIds;
	}
	/**
	 * @param strReqIds the strReqIds to set
	 */
	public void setStrReqIds(String strReqIds) {
		this.strReqIds = strReqIds;
	}
	/**
	 * @return the appvlAccessFl
	 */
	public String getAppvlAccessFl() {
		return appvlAccessFl;
	}
	/**
	 * @param appvlAccessFl the appvlAccessFl to set
	 */
	public void setAppvlAccessFl(String appvlAccessFl) {
		this.appvlAccessFl = appvlAccessFl;
	}
	/**
	 * @return the rejectAccessFl
	 */
	public String getRejectAccessFl() {
		return rejectAccessFl;
	}
	/**
	 * @param rejectAccessFl the rejectAccessFl to set
	 */
	public void setRejectAccessFl(String rejectAccessFl) {
		this.rejectAccessFl = rejectAccessFl;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the strTxnType
	 */
	public String getStrTxnType() {
		return strTxnType;
	}
	/**
	 * @param strTxnType the strTxnType to set
	 */
	public void setStrTxnType(String strTxnType) {
		this.strTxnType = strTxnType;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * @return the startDt
	 */
	public String getStartDt() {
		return startDt;
	}
	/**
	 * @param startDt the startDt to set
	 */
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	/**
	 * @return the endDt
	 */
	public String getEndDt() {
		return endDt;
	}
	/**
	 * @param endDt the endDt to set
	 */
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}
	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	/**
	 * @return the salesRep
	 */
	public String getSalesRep() {
		return salesRep;
	}
	/**
	 * @param salesRep the salesRep to set
	 */
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}
	/**
	 * @return the fieldSales
	 */
	public String getFieldSales() {
		return fieldSales;
	}
	/**
	 * @param fieldSales the fieldSales to set
	 */
	public void setFieldSales(String fieldSales) {
		this.fieldSales = fieldSales;
	}
	/**
	 * @return the nextApprStatus
	 */
	public String getNextApprStatus() {
		return nextApprStatus;
	}
	/**
	 * @param nextApprStatus the nextApprStatus to set
	 */
	public void setNextApprStatus(String nextApprStatus) {
		this.nextApprStatus = nextApprStatus;
	}
	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}
	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}
	/**
	 * @return the hApproveInputString
	 */
	public String gethApproveInputString() {
		return hApproveInputString;
	}
	/**
	 * @param hApproveInputString the hApproveInputString to set
	 */
	public void sethApproveInputString(String hApproveInputString) {
		this.hApproveInputString = hApproveInputString;
	}
	/**
	 * @return the prReqDetLitePnum
	 */
	public String getPrReqDetLitePnum() {
		return prReqDetLitePnum;
	}
	/**
	 * @param prReqDetLitePnum the prReqDetLitePnum to set
	 */
	public void setPrReqDetLitePnum(String prReqDetLitePnum) {
		this.prReqDetLitePnum = prReqDetLitePnum;
	}
	/**
	 * @return the systemid
	 */
	public String getSystemid() {
		return systemid;
	}
	/**
	 * @param systemid the systemid to set
	 */
	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}
	/**
	 * @return the partString
	 */
	public String getPartString() {
		return partString;
	}
	/**
	 * @param partString the partString to set
	 */
	public void setPartString(String partString) {
		this.partString = partString;
	}
	/**
	 * @return the raqtyString
	 */
	public String getRaqtyString() {
		return raqtyString;
	}
	/**
	 * @param raqtyString the raqtyString to set
	 */
	public void setRaqtyString(String raqtyString) {
		this.raqtyString = raqtyString;
	}
	/**
	 * @return the sheetOwner
	 */
	public String getSheetOwner() {
		return sheetOwner;
	}
	/**
	 * @param sheetOwner the sheetOwner to set
	 */
	public void setSheetOwner(String sheetOwner) {
		this.sheetOwner = sheetOwner;
	}
	/**
	 * @return the chk_ShowArrowFl
	 */
	public String getChk_ShowArrowFl() {
		return chk_ShowArrowFl;
	}
	/**
	 * @param chk_ShowArrowFl the chk_ShowArrowFl to set
	 */
	public void setChk_ShowArrowFl(String chk_ShowArrowFl) {
		this.chk_ShowArrowFl = chk_ShowArrowFl;
	}
	/**
	 * @return the email_to
	 */
	public String getEmail_to() {
		return email_to;
	}
	/**
	 * @param email_to the email_to to set
	 */
	public void setEmail_to(String email_to) {
		this.email_to = email_to;
	}
	/**
	 * @return the email_cc
	 */
	public String getEmail_cc() {
		return email_cc;
	}
	/**
	 * @param email_cc the email_cc to set
	 */
	public void setEmail_cc(String email_cc) {
		this.email_cc = email_cc;
	}
	/**
	 * @return the email_subject
	 */
	public String getEmail_subject() {
		return email_subject;
	}
	/**
	 * @param email_subject the email_subject to set
	 */
	public void setEmail_subject(String email_subject) {
		this.email_subject = email_subject;
	}
	/**
	 * @return the email_comments
	 */
	public String getEmail_comments() {
		return email_comments;
	}
	/**
	 * @param email_comments the email_comments to set
	 */
	public void setEmail_comments(String email_comments) {
		this.email_comments = email_comments;
	}
	/**
	 * @return the requestor_name
	 */
	public String getRequestor_name() {
		return requestor_name;
	}
	/**
	 * @param requestor_name the requestor_name to set
	 */
	public void setRequestor_name(String requestor_name) {
		this.requestor_name = requestor_name;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the raId
	 */
	public String getRaId() {
		return raId;
	}
	/**
	 * @param raId the raId to set
	 */
	public void setRaId(String raId) {
		this.raId = raId;
	}
	/**
	 * @return the chkbox_display
	 */
	public String getChkbox_display() {
		return chkbox_display;
	}
	/**
	 * @param chkbox_display the chkbox_display to set
	 */
	public void setChkbox_display(String chkbox_display) {
		this.chkbox_display = chkbox_display;
	}
	/**
	 * @return the request_id
	 */
	public String getRequest_id() {
		return request_id;
	}
	/**
	 * @param request_id the request_id to set
	 */
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}
	/**
	 * @return the alEventName
	 */
	public ArrayList getAlEventName() {
		return alEventName;
	}
	/**
	 * @param alEventName the alEventName to set
	 */
	public void setAlEventName(ArrayList alEventName) {
		this.alEventName = alEventName;
	}
	/**
	 * @return the alSalesReps
	 */
	public ArrayList getAlSalesReps() {
		return alSalesReps;
	}
	/**
	 * @param alSalesReps the alSalesReps to set
	 */
	public void setAlSalesReps(ArrayList alSalesReps) {
		this.alSalesReps = alSalesReps;
	}
	/**
	 * @return the alFieldSales
	 */
	public ArrayList getAlFieldSales() {
		return alFieldSales;
	}
	/**
	 * @param alFieldSales the alFieldSales to set
	 */
	public void setAlFieldSales(ArrayList alFieldSales) {
		this.alFieldSales = alFieldSales;
	}
	/**
	 * @return the alSystemList
	 */
	public ArrayList getAlSystemList() {
		return alSystemList;
	}
	/**
	 * @param alSystemList the alSystemList to set
	 */
	public void setAlSystemList(ArrayList alSystemList) {
		this.alSystemList = alSystemList;
	}
	/**
	 * @return the alDSOwner
	 */
	public ArrayList getAlDSOwner() {
		return alDSOwner;
	}
	/**
	 * @param alDSOwner the alDSOwner to set
	 */
	public void setAlDSOwner(ArrayList alDSOwner) {
		this.alDSOwner = alDSOwner;
	}
	  
	  
}
