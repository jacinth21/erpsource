package com.globus.sales.CaseManagement.forms;

import com.globus.common.forms.GmCommonForm;

public class GmFavouriteCaseForm extends GmCommonForm{

	private String caseID="";
	private String caseInfoID="";
	private String favouriteName="";
	/**
	 * @return the caseID
	 */
	public String getCaseID() {
		return caseID;
	}
	/**
	 * @param caseID the caseID to set
	 */
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the favouriteName
	 */
	public String getFavouriteName() {
		return favouriteName;
	}
	/**
	 * @param favouriteName the favouriteName to set
	 */
	public void setFavouriteName(String favouriteName) {
		this.favouriteName = favouriteName;
	}
	
}