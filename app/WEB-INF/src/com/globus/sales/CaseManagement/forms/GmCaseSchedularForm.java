package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmLogForm;

public class GmCaseSchedularForm extends GmLogForm{
	private String caseId = "";
	private String status = "";
	private Date surgeryDt = null;
	private Date shipDt =  null;
	private Date retDt = null; 
	private String division = "";
	private String distributor = "";
	private String salesrep = "";
	private String account = "";
	private String invSelection = "";
	private String notes = "";
	private String kitID = "";
	private String setID= "";
	private String searchkitId = "";
	private String searchdistributor = "";
	private String searchsalesrep = "";
	private String searchaccount = "";
	private String strSurgeryDt ="";
    private String strShipDt = "";
	private String strRetDt = "";
	private String longTerm = "";
	private ArrayList alSetStatus = new ArrayList ();
	private ArrayList alDivList = new ArrayList ();
	private ArrayList alList = new ArrayList ();
	private ArrayList	alListAcc = new ArrayList ();
	private ArrayList alDisList = new ArrayList ();
	private ArrayList alSalesRepList = new ArrayList ();
	
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getSurgeryDt() {
		return surgeryDt;
	}
	public void setSurgeryDt(Date surgeryDt) {
		this.surgeryDt = surgeryDt;
	}
	public Date getShipDt() {
		return shipDt;
	}
	public void setShipDt(Date shipDt) {
		this.shipDt = shipDt;
	}
	public Date getRetDt() {
		return retDt;
	}
	public void setRetDt(Date retDt) {
		this.retDt = retDt;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDistributor() {
		return distributor;
	}
	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}
	public String getSalesrep() {
		return salesrep;
	}
	public void setSalesrep(String salesrep) {
		this.salesrep = salesrep;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getInvSelection() {
		return invSelection;
	}
	public void setInvSelection(String invSelection) {
		this.invSelection = invSelection;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getKitID() {
		return kitID;
	}
	public void setKitID(String kitID) {
		this.kitID = kitID;
	}
	public String getSetID() {
		return setID;
	}
	public void setSetID(String setID) {
		this.setID = setID;
	}
	
	public String getSearchkitId() {
		return searchkitId;
	}
	public void setSearchkitId(String searchkitId) {
		this.searchkitId = searchkitId;
	}
	public ArrayList getAlSetStatus() {
		return alSetStatus;
	}
	public void setAlSetStatus(ArrayList alSetStatus) {
		this.alSetStatus = alSetStatus;
	}
	public ArrayList getAlDivList() {
		return alDivList;
	}
	public void setAlDivList(ArrayList alDivList) {
		this.alDivList = alDivList;
	}
	public ArrayList getAlList() {
		return alList;
	}
	public void setAlList(ArrayList alList) {
		this.alList = alList;
	}
	public String getSearchdistributor() {
		return searchdistributor;
	}
	public void setSearchdistributor(String searchdistributor) {
		this.searchdistributor = searchdistributor;
	}
	public String getSearchsalesrep() {
		return searchsalesrep;
	}
	public void setSearchsalesrep(String searchsalesrep) {
		this.searchsalesrep = searchsalesrep;
	}
	public String getSearchaccount() {
		return searchaccount;
	}
	public void setSearchaccount(String searchaccount) {
		this.searchaccount = searchaccount;
	}
	public ArrayList getAlListAcc() {
		return alListAcc;
	}
	public void setAlListAcc(ArrayList alListAcc) {
		this.alListAcc = alListAcc;
	}
	public String getStrSurgeryDt() {
		return strSurgeryDt;
	}
	public void setStrSurgeryDt(String strSurgeryDt) {
		this.strSurgeryDt = strSurgeryDt;
	}
	public String getStrShipDt() {
		return strShipDt;
	}
	public void setStrShipDt(String strShipDt) {
		this.strShipDt = strShipDt;
	}
	public String getStrRetDt() {
		return strRetDt;
	}
	public void setStrRetDt(String strRetDt) {
		this.strRetDt = strRetDt;
	}
	public ArrayList getAlDisList() {
		return alDisList;
	}
	public void setAlDisList(ArrayList alDisList) {
		this.alDisList = alDisList;
	}
	public ArrayList getAlSalesRepList() {
		return alSalesRepList;
	}
	public void setAlSalesRepList(ArrayList alSalesRepList) {
		this.alSalesRepList = alSalesRepList;
	}
	public String getLongTerm() {
		return longTerm;
	}
	public void setLongTerm(String longTerm) {
		this.longTerm = longTerm;
	}
	
}