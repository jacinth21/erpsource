/**
 * FileName    : GmCaseScheduleCalendarForm.java 
 * Description :
 */
package com.globus.sales.CaseManagement.forms;
/**
 * @author N Raja
 *
 */
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

public class GmCaseScheduleCalendarForm extends GmCommonForm{
	
	private static final long serialVersionUID = 1L;
	private Date fromDt=null;
	private Date toDt=null;
	private String gridData="";
	private String caseInfoId="";
	private int ccYear;
	private int ccMonth;
	//
	
	/**
	 * @return the ccMonth
	 */
	public int getCcMonth() {
		return ccMonth;
	}
	/**
	 * @param ccMonth the ccMonth to set
	 */
	public void setCcMonth(int ccMonth) {
		this.ccMonth = ccMonth;
	}
	
	/**
	 * @return the ccYear
	 */
	public int getCcYear() {
		return ccYear;
	}
	/**
	 * @param ccYear the ccYear to set
	 */
	public void setCcYear(int ccYear) {
		this.ccYear = ccYear;
	}
	/**
	 * @return the caseInfoId
	 */
	public String getCaseInfoId() {
		return caseInfoId;
	}
	/**
	 * @param caseInfoId the caseInfoId to set
	 */
	public void setCaseInfoId(String caseInfoId) {
		this.caseInfoId = caseInfoId;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the fromDt
	 */
	public Date getFromDt() {
		return fromDt;
	}
	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(Date fromDt) {
		this.fromDt = fromDt;
	}
	/**
	 * @return the toDt
	 */
	public Date getToDt() {
		return toDt;
	}
	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(Date toDt) {
		this.toDt = toDt;
	}
	
	
}
