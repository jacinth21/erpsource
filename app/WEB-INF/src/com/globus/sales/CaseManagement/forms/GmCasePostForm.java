package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCancelForm;

public class GmCasePostForm extends GmCancelForm{
	 
	private String caseInfoID = "";
	private String caseID = "";   
	private String parentForm = "";
	private String inputString = "";  
	private String phoneOrderAccess = "";  
	
	HashMap hmGenerlInfo = new HashMap(); 
	ArrayList alCaseAttrDtls = new ArrayList();
	private ArrayList alAllBookedSets = new ArrayList();
	
	
	
	public String getPhoneOrderAccess() {
		return phoneOrderAccess;
	}
	public void setPhoneOrderAccess(String phoneOrderAccess) {
		this.phoneOrderAccess = phoneOrderAccess;
	}
	public String getInputString() {
		return inputString;
	}
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @return the caseID
	 */
	public String getCaseID() {
		return caseID;
	}
	/**
	 * @param caseID the caseID to set
	 */
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}
	/**
	 * @return the alAllBookedSets
	 */
	public ArrayList getAlAllBookedSets() {
		return alAllBookedSets;
	}
	/**
	 * @param alAllBookedSets the alAllBookedSets to set
	 */
	public void setAlAllBookedSets(ArrayList alAllBookedSets) {
		this.alAllBookedSets = alAllBookedSets;
	}
	/**
	 * @return the hmGenerlInfo
	 */
	public HashMap getHmGenerlInfo() {
		return hmGenerlInfo;
	}
	/**
	 * @param hmGenerlInfo the hmGenerlInfo to set
	 */
	public void setHmGenerlInfo(HashMap hmGenerlInfo) {
		this.hmGenerlInfo = hmGenerlInfo;
	}
	
	 
	public ArrayList getAlCaseAttrDtls() {
		return alCaseAttrDtls;
	}
	public void setAlCaseAttrDtls(ArrayList alCaseAttrDtls) {
		this.alCaseAttrDtls = alCaseAttrDtls;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	public String getParentForm() {
		return parentForm;
	}
	public void setParentForm(String parentForm) {
		this.parentForm = parentForm;
	}
	 
	 
	
}
