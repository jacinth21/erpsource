package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;

public class GmCaseBuilderForm extends GmCancelForm{
	
	private String favouriteCaseID=""; 
	private String favouriteCaseNM=""; 
	private String[] checkCategories = new String [10];
	private String caseInfoID = "";
	private String caseID = "";
	private String selectedCategories = "";
	private String accessFL = "";
	private String caseStatus = "";
	private String repId = "";
	
	private ArrayList alFavouriteCases = new ArrayList();
	private ArrayList alCategories = new ArrayList();
	
	
	public String getRepId() {
		return repId;
	}
	public void setRepId(String repId) {
		this.repId = repId;
	}
	public String getFavouriteCaseID() {
		return favouriteCaseID;
	}
	public void setFavouriteCaseID(String favouriteCaseID) {
		this.favouriteCaseID = favouriteCaseID;
	}
	public String getFavouriteCaseNM() {
		return favouriteCaseNM;
	}
	public void setFavouriteCaseNM(String favouriteCaseNM) {
		this.favouriteCaseNM = favouriteCaseNM;
	}
	public String[] getCheckCategories() {
		return checkCategories;
	}
	public void setCheckCategories(String[] checkCategories) {
		this.checkCategories = checkCategories;
	}
	public String getCaseInfoID() {
		return caseInfoID;
	}
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	public String getCaseID() {
		return caseID;
	}
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}
	public String getSelectedCategories() {
		return selectedCategories;
	}
	public void setSelectedCategories(String selectedCategories) {
		this.selectedCategories = selectedCategories;
	}
	public String getAccessFL() {
		return accessFL;
	}
	public void setAccessFL(String accessFL) {
		this.accessFL = accessFL;
	}
	public String getCaseStatus() {
		return caseStatus;
	}
	public void setCaseStatus(String caseStatus) {
		this.caseStatus = caseStatus;
	}
	public ArrayList getAlFavouriteCases() {
		return alFavouriteCases;
	}
	public void setAlFavouriteCases(ArrayList alFavouriteCases) {
		this.alFavouriteCases = alFavouriteCases;
	}
	public ArrayList getAlCategories() {
		return alCategories;
	}
	public void setAlCategories(ArrayList alCategories) {
		this.alCategories = alCategories;
	}
	
		
}
