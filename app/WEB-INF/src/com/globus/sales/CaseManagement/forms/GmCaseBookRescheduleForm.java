package com.globus.sales.CaseManagement.forms;

import java.util.Date;
import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmCaseBookRescheduleForm extends GmLogForm{
	
	private Date surgeryDate=null;
	private String surgeryTimeHour="";
	private String surgeryTimeMin="";
	private String surgeryAMPM="";
	private String reasons ="";
	private String caseInfoID="";
	private ArrayList alSurgeryTimeHour = new ArrayList();
	private ArrayList alSurgeryTimeMin = new ArrayList();
	private ArrayList alSurgeryAMPM = new ArrayList();
	private ArrayList alReasons = new ArrayList();

	/**
	 * @return the surgeryAMPM
	 */
	public String getSurgeryAMPM() {
		return surgeryAMPM;
	}
	/**
	 * @param surgeryAMPM the surgeryAMPM to set
	 */
	public void setSurgeryAMPM(String surgeryAMPM) {
		this.surgeryAMPM = surgeryAMPM;
	}
	/**
	 * @return the surgeryDate
	 */
	public Date getSurgeryDate() {
		return surgeryDate;
	}
	/**
	 * @param surgeryDate the surgeryDate to set
	 */
	public void setSurgeryDate(Date surgeryDate) {
		this.surgeryDate = surgeryDate;
	}
	/**
	 * @return the surgeryTimeHour
	 */
	public String getSurgeryTimeHour() {
		return surgeryTimeHour;
	}
	/**
	 * @param surgeryTimeHour the surgeryTimeHour to set
	 */
	public void setSurgeryTimeHour(String surgeryTimeHour) {
		this.surgeryTimeHour = surgeryTimeHour;
	}
	/**
	 * @return the surgeryTimeMin
	 */
	public String getSurgeryTimeMin() {
		return surgeryTimeMin;
	}
	/**
	 * @param surgeryTimeMin the surgeryTimeMin to set
	 */
	public void setSurgeryTimeMin(String surgeryTimeMin) {
		this.surgeryTimeMin = surgeryTimeMin;
	}
	/**
	 * @return the reasons
	 */
	public String getReasons() {
		return reasons;
	}
	/**
	 * @param reasons the reasons to set
	 */
	public void setReasons(String reasons) {
		this.reasons = reasons;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the alSurgeryTimeHour
	 */
	public ArrayList getAlSurgeryTimeHour() {
		return alSurgeryTimeHour;
	}
	/**
	 * @param alSurgeryTimeHour the alSurgeryTimeHour to set
	 */
	public void setAlSurgeryTimeHour(ArrayList alSurgeryTimeHour) {
		this.alSurgeryTimeHour = alSurgeryTimeHour;
	}
	/**
	 * @return the alSurgeryTimeMin
	 */
	public ArrayList getAlSurgeryTimeMin() {
		return alSurgeryTimeMin;
	}
	/**
	 * @param alSurgeryTimeMin the alSurgeryTimeMin to set
	 */
	public void setAlSurgeryTimeMin(ArrayList alSurgeryTimeMin) {
		this.alSurgeryTimeMin = alSurgeryTimeMin;
	}
	/**
	 * @return the alSurgeryAMPM
	 */
	public ArrayList getAlSurgeryAMPM() {
		return alSurgeryAMPM;
	}
	/**
	 * @param alSurgeryAMPM the alSurgeryAMPM to set
	 */
	public void setAlSurgeryAMPM(ArrayList alSurgeryAMPM) {
		this.alSurgeryAMPM = alSurgeryAMPM;
	}
	/**
	 * @return the alReasons
	 */
	public ArrayList getAlReasons() {
		return alReasons;
	}
	/**
	 * @param alReasons the alReasons to set
	 */
	public void setAlReasons(ArrayList alReasons) {
		this.alReasons = alReasons;
	}
	
}
