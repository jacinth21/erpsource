package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

public class GmCaseListRptForm extends GmCommonForm{
	
	private Date fromDt=null;
	private Date toDt=null;
	private String gridData="";
	private String caseId="";
	private String dist ="";
	private String rep ="";
	private String category ="";
	private String account ="";
	private String status ="";
	
    private ArrayList alDist = new ArrayList(); 
    private ArrayList alAccount = new ArrayList();
    private ArrayList alRep = new ArrayList();
    private ArrayList alStatus = new ArrayList();
    private ArrayList alResult = new ArrayList();
    private ArrayList alCategory = new ArrayList();
	/**
	 * @return the fromDt
	 */
	public Date getFromDt() {
		return fromDt;
	}
	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(Date fromDt) {
		this.fromDt = fromDt;
	}
	/**
	 * @return the toDt
	 */
	public Date getToDt() {
		return toDt;
	}
	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(Date toDt) {
		this.toDt = toDt;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the caseId
	 */
	public String getCaseId() {
		return caseId;
	}
	/**
	 * @param caseId the caseId to set
	 */
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	/**
	 * @return the dist
	 */
	public String getDist() {
		return dist;
	}
	/**
	 * @param dist the dist to set
	 */
	public void setDist(String dist) {
		this.dist = dist;
	}
	/**
	 * @return the rep
	 */
	public String getRep() {
		return rep;
	}
	/**
	 * @param rep the rep to set
	 */
	public void setRep(String rep) {
		this.rep = rep;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the alDist
	 */
	public ArrayList getAlDist() {
		return alDist;
	}
	/**
	 * @param alDist the alDist to set
	 */
	public void setAlDist(ArrayList alDist) {
		this.alDist = alDist;
	}
	/**
	 * @return the alAccount
	 */
	public ArrayList getAlAccount() {
		return alAccount;
	}
	/**
	 * @param alAccount the alAccount to set
	 */
	public void setAlAccount(ArrayList alAccount) {
		this.alAccount = alAccount;
	}
	/**
	 * @return the alRep
	 */
	public ArrayList getAlRep() {
		return alRep;
	}
	/**
	 * @param alRep the alRep to set
	 */
	public void setAlRep(ArrayList alRep) {
		this.alRep = alRep;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}
	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	
	public ArrayList getAlCategory() {
		return alCategory;
	}
	public void setAlCategory(ArrayList alCategory) {
		this.alCategory = alCategory;
	}

}
