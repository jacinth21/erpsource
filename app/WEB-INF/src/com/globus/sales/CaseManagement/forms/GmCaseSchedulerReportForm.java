/**
 * FileName : GmCaseSchedulerReportForm.java, Description : case scheduler Report Form, Author : N Raja
 */
package com.globus.sales.CaseManagement.forms;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.forms.GmCommonForm;

public class GmCaseSchedulerReportForm extends GmCommonForm{
	private static final long serialVersionUID = 1L;
	private Date fromDt = null;
	private Date toDt = null;
	private String division = "";
	private String fieldSales = "";
	private String salesRep = "";
	private String account = "";
	private String caseInventory = "";
	private String status = "";
	private String gridXmlData = "";
	private String gridXmlData1 = "";
	private String searchaccount = "";
	private String searchsalesRep = "";
	private String searchfieldSales = "";
	private ArrayList alList =new ArrayList();
	private ArrayList aldateSelection =new ArrayList();
	private String surgerydate = "";
	private String dateSelection = "";
	private String kitMapId ="";
	private String kitMap ="";
	private String searchkitMapId ="";
	private String spineDivision = "";
	private ArrayList alStatus =new ArrayList();
	private ArrayList alTerm =new ArrayList();
	private ArrayList alDivision =new ArrayList();
	private String longTerm = "";
	private String term = "";
	public String getFieldSales() {
		return fieldSales;
	}
	public void setFieldSales(String fieldSales) {
		this.fieldSales = fieldSales;
	}
	public String getSalesRep() {
		return salesRep;
	}
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getCaseInventory() {
		return caseInventory;
	}
	public void setCaseInventory(String caseInventory) {
		this.caseInventory = caseInventory;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the division
	 */
	public String getDivision() {
		return division;
	}
	/**
	 * @param division the division to set
	 */
	public void setDivision(String division) {
		this.division = division;
	}
	public Date getFromDt() {
		return fromDt;
	}
	public void setFromDt(Date fromDt) {
		this.fromDt = fromDt;
	}
	public Date getToDt() {
		return toDt;
	}
	public void setToDt(Date toDt) {
		this.toDt = toDt;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getGridXmlData1() {
		return gridXmlData1;
	}
	public void setGridXmlData1(String gridXmlData1) {
		this.gridXmlData1 = gridXmlData1;
	}
	/**
	 * @return the alList
	 */
	public ArrayList getAlList() {
		return alList;
	}
	/**
	 * @param alList the alList to set
	 */
	public void setAlList(ArrayList alList) {
		this.alList = alList;
	}
	public String getSearchaccount() {
		return searchaccount;
	}
	public void setSearchaccount(String searchaccount) {
		this.searchaccount = searchaccount;
	}
	public String getSearchsalesRep() {
		return searchsalesRep;
	}
	public void setSearchsalesRep(String searchsalesRep) {
		this.searchsalesRep = searchsalesRep;
	}
	public String getSearchfieldSales() {
		return searchfieldSales;
	}
	public void setSearchfieldSales(String searchfieldSales) {
		this.searchfieldSales = searchfieldSales;
	}
	public ArrayList getAldateSelection() {
		return aldateSelection;
	}
	public void setAldateSelection(ArrayList aldateSelection) {
		this.aldateSelection = aldateSelection;
	}
	public String getDateSelection() {
		return dateSelection;
	}
	public void setDateSelection(String dateSelection) {
		this.dateSelection = dateSelection;
	}
	public String getSurgerydate() {
		return surgerydate;
	}
	public void setSurgerydate(String alSurgerydate) {
		this.surgerydate = alSurgerydate;
	}
	public ArrayList getAlStatus() {
		return alStatus;
	}
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	public String getKitMapId() {
		return kitMapId;
	}
	public void setKitMapId(String kitMapId) {
		this.kitMapId = kitMapId;
	}
	public String getKitMap() {
		return kitMap;
	}
	public void setKitMap(String kitMap) {
		this.kitMap = kitMap;
	}
	public String getSearchkitMapId() {
		return searchkitMapId;
	}
	public void setSearchkitMapId(String searchkitMapId) {
		this.searchkitMapId = searchkitMapId;
	}
	public String getSpineDivision() {
		return spineDivision;
	}
	public void setSpineDivision(String spineDivision) {
		this.spineDivision = spineDivision;
	}
	public ArrayList getAlDivision() {
		return alDivision;
	}
	public void setAlDivision(ArrayList alDivision) {
		this.alDivision = alDivision;
	}
	public String getLongTerm() {
		return longTerm;
	}
	public void setLongTerm(String longTerm) {
		this.longTerm = longTerm;
	}
	public ArrayList getAlTerm() {
		return alTerm;
	}
	public void setAlTerm(ArrayList alTerm) {
		this.alTerm = alTerm;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	
}