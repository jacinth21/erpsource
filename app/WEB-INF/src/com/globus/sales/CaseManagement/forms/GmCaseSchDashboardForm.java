package com.globus.sales.CaseManagement.forms;

import java.util.HashMap;
import com.globus.common.forms.GmLogForm;

public class GmCaseSchDashboardForm extends GmLogForm{

	private static final long serialVersionUID = 1L;
private HashMap hmKitShipDsh = new HashMap();
private HashMap hmKitReturnDsh = new HashMap();
private HashMap hmKitShipDshup = new HashMap();
private HashMap hmKitRetDshup = new HashMap();

private HashMap hmCaseShipDsh = new HashMap();
private HashMap hmCaseReturnDsh = new HashMap();
private HashMap hmCaseShipDshup = new HashMap();
private HashMap hmCaseRetDshup = new HashMap();
private String strName = "";

public HashMap getHmKitShipDsh() {
	return hmKitShipDsh;
}
public void setHmKitShipDsh(HashMap hmKitShipDsh) {
	this.hmKitShipDsh = hmKitShipDsh;
}
public HashMap getHmKitReturnDsh() {
	return hmKitReturnDsh;
}
public void setHmKitReturnDsh(HashMap hmKitReturnDsh) {
	this.hmKitReturnDsh = hmKitReturnDsh;
}
public HashMap getHmKitShipDshup() {
	return hmKitShipDshup;
}
public void setHmKitShipDshup(HashMap hmKitShipDshup) {
	this.hmKitShipDshup = hmKitShipDshup;
}
public HashMap getHmKitRetDshup() {
	return hmKitRetDshup;
}
public void setHmKitRetDshup(HashMap hmKitRetDshup) {
	this.hmKitRetDshup = hmKitRetDshup;
}
public HashMap getHmCaseShipDsh() {
	return hmCaseShipDsh;
}
public void setHmCaseShipDsh(HashMap hmCaseShipDsh) {
	this.hmCaseShipDsh = hmCaseShipDsh;
}
public HashMap getHmCaseReturnDsh() {
	return hmCaseReturnDsh;
}
public void setHmCaseReturnDsh(HashMap hmCaseReturnDsh) {
	this.hmCaseReturnDsh = hmCaseReturnDsh;
}
public HashMap getHmCaseShipDshup() {
	return hmCaseShipDshup;
}
public void setHmCaseShipDshup(HashMap hmCaseShipDshup) {
	this.hmCaseShipDshup = hmCaseShipDshup;
}
public HashMap getHmCaseRetDshup() {
	return hmCaseRetDshup;
}
public void setHmCaseRetDshup(HashMap hmCaseRetDshup) {
	this.hmCaseRetDshup = hmCaseRetDshup;
}
public String getStrName() {
	return strName;
}
public void setStrName(String strName) {
	this.strName = strName;
}
}
