package com.globus.sales.CaseManagement.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.forms.GmCaseListRptForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.pricing.beans.GmPricingRequestBean;


public class GmCaseListRptAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  public ActionForward caseListRpt(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCaseListRptForm gmCaseListRptForm = (GmCaseListRptForm) form;
    gmCaseListRptForm.loadSessionParameters(request);

    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());

    HttpSession session = request.getSession(false);
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();


    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    String strMonthFirstDay = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
    // Need to pass date format to get date in the session format
    String strMonthLastDay = GmCalenderOperations.addDays(30, strApplnDateFmt);

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    String strAccessCondition = getAccessFilter(request, response);
    HashMap hmFilter = new HashMap();
    hmFilter.put("CONDITION",strAccessCondition);
    hmFilter.put("FLTR_DIST_BY_COMP","YES");//Filter Distributor Based on company
    hmSalesFilters = gmCommonBean.getSalesFilterLists(hmFilter);

    String strOpt = gmCaseListRptForm.getStrOpt();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
    gmCaseListRptForm.setAlDist(alResult);
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
    gmCaseListRptForm.setAlRep(alResult);

    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
    gmCaseListRptForm.setAlAccount(GmCommonClass.parseNullArrayList(gmCommonBean
        .loadAccountList(strAccessCondition)));

    alResult = GmCommonClass.getCodeList("CASEST");
    gmCaseListRptForm.setAlStatus(alResult);
    alResult = GmCommonClass.getCodeList("CASCAT");
    gmCaseListRptForm.setAlCategory(alResult);
    if (!strOpt.equals("load")) {
      //Coomented below line because dd.mm.yyyy format showing as unparsable date format
      //Date strdate = GmCalenderOperations.getDate(strMonthFirstDay);
      if (gmCaseListRptForm.getToDt() == null) {
        gmCaseListRptForm.setToDt(GmCommonClass.getStringToDate(strMonthLastDay, strApplnDateFmt));
      }
      if (gmCaseListRptForm.getFromDt() == null) {
        gmCaseListRptForm.setFromDt(GmCommonClass
            .getStringToDate(strMonthFirstDay, strApplnDateFmt));
      }
    } else {
      hmParam = GmCommonClass.getHashMapFromForm(gmCaseListRptForm);
      hmParam.put("AccessFilter", strAccessCondition);
      hmParam.put("DATEFORMAT", strApplnDateFmt);
      alResult = gmCaseBookRptBean.fetchCaseListRpt(hmParam);
      log.debug("alResult" + alResult.size());
      gmCaseListRptForm.setAlResult(alResult);
      // gmCaseListRptForm.setGridData(generateOutPut(alResult));
    }
    return mapping.findForward("caseListRpt");
  }

  /*
   * private String generateOutPut(ArrayList alResult) throws Exception { GmTemplateUtil
   * templateUtil = new GmTemplateUtil(); templateUtil.setDataList("alResult", alResult);
   * templateUtil.setTemplateSubDir("sales/CaseManagement/templates");
   * templateUtil.setTemplateName("GmCaseListRpt.vm"); return templateUtil.generateOutput(); }
   */
}
