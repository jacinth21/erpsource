package com.globus.sales.CaseManagement.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.sales.CaseManagement.beans.GmCaseSchedularBean;
import com.globus.sales.CaseManagement.forms.GmCaseSchedularForm;


public class GmCaseSchedularAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	public ActionForward execute(ActionMapping actionMapping,ActionForm actionForm,HttpServletRequest request,HttpServletResponse response)throws Exception {  
		
		instantiate(request, response);
        GmCaseSchedularForm gmCaseSchedularForm = (GmCaseSchedularForm) actionForm;
        gmCaseSchedularForm.loadSessionParameters(request);
        GmCaseSchedularBean gmCaseSchedularBean = new GmCaseSchedularBean(getGmDataStoreVO());
        GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
        GmCustSalesSetupBean gmCustSetup = new GmCustSalesSetupBean(getGmDataStoreVO());
        GmWSUtil gmWSUtil = new GmWSUtil();
        String strOpt = GmCommonClass.parseNull(gmCaseSchedularForm.getStrOpt());
        HashMap hmParam = new HashMap();
        hmParam = GmCommonClass.getHashMapFromForm(gmCaseSchedularForm);
        HttpSession session = request.getSession(false);

	    if(strOpt.equals("caseDetails")){
	    	String caseId = GmCommonClass.parseNull(request.getParameter("caseId"));
	    	fetchCaseDetails(gmCaseSchedularForm,hmParam,request);
	    }if(strOpt.equals("saveCaseDetails")){
	    	String strCaseId = GmCommonClass.parseNull(request.getParameter("case_id")); 
		    String strSurDt = GmCommonClass.parseNull(request.getParameter("surgery_date"));   
	    	String strShipDt = GmCommonClass.parseNull(request.getParameter("ship_date"));
	    	String strRetDt = GmCommonClass.parseNull(request.getParameter("return_date"));
	    	String strFieldsales = GmCommonClass.parseNull(request.getParameter("field_sales"));
	    	String strSalesRep = GmCommonClass.parseNull(request.getParameter("sales_rep"));
	    	String strAccount = GmCommonClass.parseNull(request.getParameter("account"));
	    	String strDivision = GmCommonClass.parseNull(request.getParameter("division"));	
	    	String strStatus = GmCommonClass.parseNull(request.getParameter("status"));	
	    	String strlongterm = GmCommonClass.parseNull(request.getParameter("longterm"));	
	    	String strCompanyId = GmCommonClass.parseNull(request.getParameter("companyId"));	
	    	String strCompanyDtFmt = GmCommonClass.parseNull(request.getParameter("gCmpDateFmt"));	
	    	String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
	    	String strcomments = GmCommonClass.parseNull(request.getParameter("comments"));
	    	String strCmpDtFmt = GmCommonClass.parseNull(gmCaseSchedularForm.getApplnDateFmt());
	    	hmParam.put("CASEID",strCaseId);
	    	hmParam.put("SURGERYDT",strSurDt);
	    	hmParam.put("SHIPDT",strShipDt);
	    	hmParam.put("RETDT",strRetDt);
	    	hmParam.put("DIVISION",strDivision);
	    	hmParam.put("DISTRIBUTER",strFieldsales);
	    	hmParam.put("SALESREP",strSalesRep);
	    	hmParam.put("ACCOUNT",strAccount);
	    	hmParam.put("STATUS",strStatus);
	    	hmParam.put("LONGTERM",strlongterm);
	    	hmParam.put("COMPANYID",strCompanyId);
	    	hmParam.put("COMPANYDTFMT",strCompanyDtFmt);
	    	hmParam.put("COMPANYINFO",strCompanyInfo);
	    	hmParam.put("TXT_LOGREASON",strcomments);
		    String castID = gmCaseSchedularBean.saveCaseDetails(hmParam);
		    ArrayList alLogList = gmCaseSchedularBean.getLog(castID, "26240507",strCmpDtFmt);
		    alLogList.add(alLogList.size(),castID);
		    if(strStatus.equals("107161") || strStatus.equals("107162")){   
		    	log.debug("JMS CALL-------------");
		    	gmCaseSchedularBean.updateCaseSchedulerJMS(hmParam);              //JMS call to update Tag report
		    }
		    hmParam.put("CASEID",castID);
		    gmCaseSchedularForm = (GmCaseSchedularForm) GmCommonClass.getFormFromHashMap(gmCaseSchedularForm,hmParam);
		    gmCaseSchedularForm.setCaseId(castID);
		       String strJSON = gmWSUtil.parseArrayListToJson(alLogList);
		        if (!strJSON.equals("")) {
		          response.setContentType("text/plain");
		          PrintWriter pw = response.getWriter();
		          pw.write(strJSON);
		          pw.flush();
		          return null;
		        }
	    }if(strOpt.equals("fetchKitDetails")){
	    	String strKitId = GmCommonClass.parseNull(request.getParameter("kitId")); 
	    	String strKitName = GmCommonClass.parseNull(request.getParameter("KitName")); 
	    	String strCaseId = GmCommonClass.parseNull(request.getParameter("caseid")); 
	    	String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
	    	String strKitStatus= "";
	    	String strCaseVal = "";
	    	ArrayList alList = new ArrayList();
	    	hmParam = gmCaseSchedularBean.fetchKitDetails(strKitId,strCaseId);
	    	String strStatuskit = GmCommonClass.parseNull((String)hmParam.get("STRKITSTATUS"));	   
	    	if(strStatuskit != null && strStatuskit.indexOf("^") != -1) {
	    	String[] Msg =new String[2];
	    	Msg = strStatuskit.split("\\^");
	    	strKitStatus =  GmCommonClass.parseNull(Msg[0]);
	    	strCaseVal  =  GmCommonClass.parseNull(Msg[1]);
	    	}
	    	alList = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("ALLIST"));
	    	hmParam.put("ALLIST",alList);
	    	hmParam.put("KITID",strKitId);
	    	hmParam.put("CASEID",strCaseId);
	    	hmParam.put("COMPANYINFO",strCompanyInfo);
	    	hmParam.put("USERID",GmCommonClass.parseNull(gmCaseSchedularForm.getUserId()));
	    	ArrayList alLogList = new ArrayList();
	    	if(!strKitStatus.equals("N")){
	    		String strComments = "Kit <b>"+strKitName+"</b> added";
	    		hmParam.put("TXT_LOGREASON",strComments);
	    		String strStatus = gmCaseSchedularBean.saveInvSelection(hmParam);
	    		alList.add(alList.size(),strStatus);
		    	//gmCaseSchedularBean.updateCaseSchedulerJMS(hmParam);     //JMS call to update Tag report
		    	gmCaseSchedularForm.setAlList(alList);
		    	gmCaseSchedularForm.setKitID(strKitId);
		    	strOpt = "invSelection";
		    	gmCaseSchedularForm.setStatus(strStatus);
		    	alLogList = gmCaseSchedularBean.getLog(strCaseId, "26240508",getGmDataStoreVO().getCmpdfmt());
		    	alList.add(alList.size(),alLogList);
	    	}
	        String strJSON = gmWSUtil.parseArrayListToJson(alList);
	        if (!strJSON.equals("")) {
	          response.setContentType("text/plain");
	          PrintWriter pw = response.getWriter();
	          pw.write(strJSON);
	          pw.flush();
	          return null;
	        }
	    }if(strOpt.equals("RemoveKit")){
	    	String strkitId = GmCommonClass.parseNull(request.getParameter("kitId"));
	    	String strkitName = GmCommonClass.parseNull(request.getParameter("KitName"));
	    	String strCaseId = GmCommonClass.parseNull(request.getParameter("caseId"));
	    	String strCompanyId = GmCommonClass.parseNull(request.getParameter("companyId"));
	    	String strCmpDtFmt = GmCommonClass.parseNull(request.getParameter("gmCmpDtFmt"));
	    	String strComments = "Kit <b>"+strkitName+"</b> removed";
	    	hmParam.put("COMPANYID",strCompanyId);
	    	hmParam.put("TXT_LOGREASON",strComments);
	    	HashMap hmVal = gmCaseSchedularBean.removeKit(strkitId,strCaseId,hmParam);
	    	String strCount = GmCommonClass.parseNull((String)hmVal.get("strCount"));
	    	String strCheckShipDt = GmCommonClass.parseNull((String)hmVal.get("strCheckDate"));
	    	ArrayList alLogList = gmCaseSchedularBean.getLog(strCaseId, "26240508",strCmpDtFmt);
	    	alLogList.add(alLogList.size(),strCount);
	    	alLogList.add(alLogList.size(),strCheckShipDt);
	    	String strInputString = strCount + "^" + strCheckShipDt;
	    	String strJSON = gmWSUtil.parseArrayListToJson(alLogList);
		        if (!strJSON.equals("")) {
		          response.setContentType("text/plain");
		          PrintWriter pw = response.getWriter();
		          pw.write(strJSON);
		          pw.flush();
		          return null;
		        }

	    }if (strOpt.equals("saveCaseNotes")){
	    	String strCaseId = GmCommonClass.parseNull(request.getParameter("caseId"));  
	    	String strCmpDtFmt = GmCommonClass.parseNull(request.getParameter("gmCmpDtFmt"));
	    	hmParam.put("CASEID",strCaseId);
	    	gmCaseSchedularForm.setApplnDateFmt(strCmpDtFmt);
	    	gmCaseSchedularBean.saveCaseNotes(hmParam);
	    	gmCaseSchedularForm.setAlLogReasons(gmCaseSchedularBean.getLog(strCaseId, "107320",strCmpDtFmt));
	    	fetchCaseNotes(gmCaseSchedularForm,hmParam,strCaseId,strCmpDtFmt);
	    	strOpt = "notes";
	    }if (strOpt.equals("")) {
	    	String strStatus = GmCommonClass.parseNull((String)request.getParameter("status"));
	    	loadCaseSchedularStatus(gmCaseSchedularForm,hmParam);
	    	gmCaseSchedularForm.setStatus(strStatus);
	    	if(strStatus.equals(""))
			loadCaseSchedular(gmCaseSchedularForm,hmParam);
		    strOpt = "GmCaseSchedule";
		}if(strOpt.equals("notes")){
			String strCaseId = GmCommonClass.parseNull(gmCaseSchedularForm.getCaseId());
			String strCmpDtFmt = GmCommonClass.parseNull(gmCaseSchedularForm.getApplnDateFmt());
			gmCaseSchedularForm.setAlLogReasons(gmCaseSchedularBean.getLog(strCaseId, "107320",strCmpDtFmt));
			fetchCaseNotes(gmCaseSchedularForm,hmParam,strCaseId,strCmpDtFmt);
			gmCaseSchedularForm.setCaseId(strCaseId);
		}if(strOpt.equals("invSelection")){
			String caseId = GmCommonClass.parseNull(request.getParameter("caseId"));
			ArrayList alList = new ArrayList();
			alList= fetchInvSelection(gmCaseSchedularForm,caseId,request);
			hmParam.put("ALLIST",alList);
			gmCaseSchedularForm.setAlList(alList);
		}if(strOpt.equals("checkStatus")){
			HashMap hmValue = new HashMap();
			String strCaseId = GmCommonClass.parseNull(request.getParameter("case_id"));
			String strReturnDate = GmCommonClass.parseNull(request.getParameter("return_date"));
			String strCompanyDtFmt = GmCommonClass.parseNull(request.getParameter("gCmpDateFmt"));	
			hmValue = gmCaseSchedularBean.checkCaseStatus(strCaseId,strReturnDate,strCompanyDtFmt);
			log.debug("hmValuehmValue==>"+hmValue);
			String strCaseStatus = GmCommonClass.parseNull((String) hmValue.get("STRCASESTATUS"));
			String strCheckInv = GmCommonClass.parseNull((String) hmValue.get("STRCHECKINV"));
			String strReturnValues = GmCommonClass.parseNull((String) hmValue.get("STRRETURNVALUES"));
			String inputString = strCaseStatus + "^" + strCheckInv + "^" + strReturnValues;
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
	          pw.write(inputString);
	          pw.flush();
	          return null;
		}if(strOpt.equals("checkSalesRep")){
			HashMap hmResult = new HashMap();
			String strfieldsales = GmCommonClass.parseNull(request.getParameter("Repid"));
			 hmResult = gmCustSetup.loadEditSalesRep(strfieldsales);
			 strfieldsales = GmCommonClass.parseNull((String)hmResult.get("DNAME"));
			 String strfieldid = GmCommonClass.parseNull((String)hmResult.get("DID"));
			 response.setContentType("text/xml");
			 PrintWriter pw = response.getWriter();
	          pw.write(strfieldsales+"^"+strfieldid);
	          pw.flush();
			  return null;
		}
		if(strOpt.equals("kitMapCaseInfo")) {
			HashMap hmResult = new HashMap();
			String strCaseId = GmCommonClass.parseNull(request.getParameter("paramCaseId"));
			String strPrimaryCmpyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
			hmParam.put("COMPANYID", strPrimaryCmpyId);
			hmParam.put("CASEID",strCaseId);
			hmResult=fetchCaseDetails(gmCaseSchedularForm,hmParam,request);
			fetchCaseInfo(gmCaseSchedularForm,hmResult);
		}
		
		return actionMapping.findForward(strOpt);
	}
	
/**
 * 
 * This method is used to Load case scheduler screen
 * 
 * @param gmCaseSchedularForm
 * @param hmParam
 */	
	private void loadCaseSchedular(GmCaseSchedularForm gmCaseSchedularForm, HashMap hmParam) throws AppError {		
	    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
		ArrayList alStatusList = new ArrayList();
		alStatusList = (GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CASESC")));		
		gmCaseSchedularForm.setStatus("107160");
		gmCaseSchedularForm.setAlSetStatus(alStatusList);

	}
/**
 * 
 * This method is used to Load case scheduler status from calendar screen
 * 
 * @param gmCaseSchedularForm
 * @param hmParam
 */	
	private void loadCaseSchedularStatus(GmCaseSchedularForm gmCaseSchedularForm, HashMap hmParam) throws AppError {		
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
		ArrayList alStatusList = new ArrayList();
		alStatusList = (GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CASESC")));		
		gmCaseSchedularForm.setAlSetStatus(alStatusList);
	}
/**
 * 
 * This method is used to Load case scheduler Details tab 
 * 
 * @param gmCaseSchedularForm
 * @param hmParam
 */	
	private void loadCaseSchedularDeatils(GmCaseSchedularForm gmCaseSchedularForm, HashMap hmParam)throws AppError {
	    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
		ArrayList alDivisionList = new ArrayList();			
		alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));			
		gmCaseSchedularForm.setDivision("2000");
		gmCaseSchedularForm.setAlDivList(alDivisionList);
	}
 /**
  * 
  * This method is used to Load case scheduler notes tab 
  * 
  * @param gmCaseSchedularForm
  * @param hmParam
  * @param strCaseId
  */	
	private void fetchCaseNotes(GmCaseSchedularForm gmCaseSchedularForm, HashMap hmParam,String strCaseId,String strCmpDtFmt)throws AppError {
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmCaseSchedularBean gmCaseSchedularBean = new GmCaseSchedularBean(getGmDataStoreVO());
    	gmCaseSchedularForm.setAlLogReasons(gmCaseSchedularBean.getLog(strCaseId, "107320",strCmpDtFmt));

	}
	
 /**
  * 
  * This method is used to fetch case scheduler details tab 
  * 
  * @param gmCaseSchedularForm
  * @param hmParam
  */	
	private HashMap fetchCaseDetails(GmCaseSchedularForm gmCaseSchedularForm, HashMap hmParam,HttpServletRequest request)throws AppError {

     	GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
	    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
	    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
	    GmCaseSchedularBean gmCaseSchedularBean = new GmCaseSchedularBean(getGmDataStoreVO());
		ArrayList alDivisionList = new ArrayList();
		ArrayList alRepList = new ArrayList();
		ArrayList alAccList = new ArrayList();
		HashMap hmValues = new HashMap();		
		alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));		
		gmCaseSchedularForm.setDivision("2000");
		ArrayList alDistributorList = new ArrayList();
		alDistributorList = gmCustomerBean.getDistributorList("Active");
		alRepList = gmCustomerBean.getRepList("Active");
		alAccList = gmCaseSchedularBean.loadAccountList("");
		gmCaseSchedularForm.setAlSalesRepList(alRepList);
		gmCaseSchedularForm.setAlDisList(alDistributorList);
		gmCaseSchedularForm.setAlDivList(alDivisionList);
		gmCaseSchedularForm.setAlListAcc(alAccList);
		hmValues = gmCaseSchedularBean.fetchCaseDetails(hmParam);
		String strLongTerm = GmCommonClass.parseNull((String)hmValues.get("LONGTREM"));
		String strCaseId = GmCommonClass.parseNull((String)hmValues.get("CASEID"));
		String strCmpDtFmt = GmCommonClass.parseNull(gmCaseSchedularForm.getApplnDateFmt());
		request.setAttribute("STRLONGTERM",strLongTerm);	
		gmCaseSchedularForm.setAlLogReasons(gmCaseSchedularBean.getLog(strCaseId, "26240507",strCmpDtFmt));
		ArrayList alLogList = gmCaseSchedularBean.getLog(strCaseId, "26240507",strCmpDtFmt);
		request.setAttribute("hmLog",alLogList);	
		gmCaseSchedularForm = (GmCaseSchedularForm) GmCommonClass.getFormFromHashMap(gmCaseSchedularForm,hmValues);
		return hmValues;
	}
	
 /**
  * 
  * This method is used to fetch inventory selection tab details
  * 
  * @param gmCaseSchedularForm
  * @param hmParam
  */	
	private ArrayList fetchInvSelection(GmCaseSchedularForm gmCaseSchedularForm,String strCaseId, HttpServletRequest request)throws AppError {
	
		GmCaseSchedularBean gmCaseSchedularBean = new GmCaseSchedularBean(getGmDataStoreVO());
		ArrayList alList = new ArrayList();
		alList = gmCaseSchedularBean.fetchInvSelection(strCaseId);
		ArrayList alLogList = gmCaseSchedularBean.getLog(strCaseId, "26240508",getGmDataStoreVO().getCmpdfmt());
		request.setAttribute("hmLog",alLogList);	
		return alList;
	}
	/**
	 * This method is used to fetch Case Info details for kit map report
	 * @param gmCaseSchedularForm
	 * @param hmResult
	 * @throws AppError
	 */
	private void fetchCaseInfo(GmCaseSchedularForm gmCaseSchedularForm, HashMap hmResult)throws AppError {
	 	gmCaseSchedularForm = (GmCaseSchedularForm) GmCommonClass.getFormFromHashMap(gmCaseSchedularForm,hmResult);
	 	gmCaseSchedularForm.setCaseId(GmCommonClass.parseNull((String)hmResult.get("CASEID")));
		gmCaseSchedularForm.setStrSurgeryDt(GmCommonClass.parseNull((String)hmResult.get("SURGERYDTFMT")));
		gmCaseSchedularForm.setStrShipDt(GmCommonClass.parseNull((String)hmResult.get("SHIPDTFMT")));
		gmCaseSchedularForm.setStrRetDt(GmCommonClass.parseNull((String)hmResult.get("RETDTFMT")));
		String strDivisionName = GmCommonClass.parseNull((String)hmResult.get("DIVNAME"));
		String strFieldSales = GmCommonClass.parseNull((String)hmResult.get("SEARCHDISTRIBUTOR"));
		String strSalesrep = GmCommonClass.parseNull((String)hmResult.get("SEARCHSALESREP"));
		String strAccountName = GmCommonClass.parseNull((String)hmResult.get("SEARCHACCOUNT"));
		String strAccountId = GmCommonClass.parseNull((String)hmResult.get("ACCOUNT"));
		gmCaseSchedularForm.setDivision(strDivisionName);
		gmCaseSchedularForm.setSearchsalesrep(strSalesrep);
		gmCaseSchedularForm.setSearchdistributor(strFieldSales);
		gmCaseSchedularForm.setSearchaccount(strAccountName);
		gmCaseSchedularForm.setAccount(strAccountId);
	}
	
}
