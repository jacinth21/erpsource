package com.globus.sales.CaseManagement.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.beans.GmCaseBuilderBean;
import com.globus.sales.CaseManagement.forms.GmCaseBuilderForm;

public class GmCaseBuilderAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCaseBuilderForm gmCaseBuilderForm = (GmCaseBuilderForm) form;
    gmCaseBuilderForm.loadSessionParameters(request);

    GmCaseBuilderBean gmCaseBuilderBean = new GmCaseBuilderBean(getGmDataStoreVO());
    ArrayList alFavouriteList = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmResult1 = new HashMap();
    String strForward = "success";

    String strAccessCondition = getAccessFilter(request, response);
    String strOpt = gmCaseBuilderForm.getStrOpt();
    String strCaseInfoID = gmCaseBuilderForm.getCaseInfoID();
    String strCaseID = gmCaseBuilderForm.getCaseID();
    String strFavCaseID = gmCaseBuilderForm.getFavouriteCaseID();
    String strSelectedCategories = "";
    String strFwdOpt = "edit";
    hmParam.put("AccessFilter", strAccessCondition);

    hmParam = GmCommonClass.getHashMapFromForm(gmCaseBuilderForm);

    if (strOpt.equals("save") || strOpt.equals("next") || strOpt.equals("ProfileNext")) {

      strSelectedCategories =
          GmCommonClass.createInputString(gmCaseBuilderForm.getCheckCategories());
      log.debug("======strSelectedCategories========" + strSelectedCategories);
      if (strSelectedCategories.equals("")) {
        hmParam.put("SELECTEDCATGR", ",");
        strFwdOpt = "nextConfirm";
      } else {
        hmParam.put("SELECTEDCATGR", strSelectedCategories);
      }

      if (!strOpt.equals("ProfileNext")) {
        gmCaseBuilderBean.saveCaseBuilder(hmParam);
      } else {
        strFavCaseID = gmCaseBuilderBean.saveCaseProfileBuilder(hmParam);
      }

      if (strOpt.equals("save")) {
        strOpt = "edit";
      }
      if (!strFavCaseID.equals("0") && !strOpt.equals("ProfileNext")) {
        gmCaseBuilderBean.saveFavSets(hmParam);
      }

    }

    if (strOpt.equals("edit")) {

      hmResult = gmCaseBuilderBean.fetchBookedCase(hmParam);
      GmCommonClass.getFormFromHashMap(gmCaseBuilderForm, hmResult);
      hmParam.putAll(hmResult); // get repid from the case

    }
    if (strOpt.equals("fetchFavorite")) {

      hmResult1 = gmCaseBuilderBean.fetchFavoriteDtls(hmParam);

      GmCommonClass.getFormFromHashMap(gmCaseBuilderForm, hmResult1);
      gmCaseBuilderForm.setCaseInfoID(strCaseInfoID);
    }
    if (strOpt.equals("next") || strOpt.equals("ProfileNext")) {
      ActionForward actionForward = mapping.findForward("savenext");
      //ActionForward nextForward = new ActionForward(actionForward);
      String strFwdProductType = "";
      String strPath = "";
      String[] arryCategories = strSelectedCategories.split(",");
      Arrays.sort(arryCategories);
      strFwdProductType = arryCategories[0];

      if (strOpt.equals("ProfileNext")) {
        strPath =
            "/gmCaseBookSetDtls.do?strOpt=ProfileEdit&fromscreen=casebuilder&favouriteCaseID="
                + strFavCaseID + "&fwdProductType=" + strFwdProductType + "&categoryList="
                + strSelectedCategories;// +"&interBodyFL="+interBodyFL+"&biologicFL="+biologicFL+"&fixationFL="+fixationFL+"&accessFL="+accessFL
                                        // ;
      } else {
        strPath =
            "/gmCaseBookSetDtls.do?strOpt=" + strFwdOpt + "&fromscreen=casebuilder&caseInfoID="
                + strCaseInfoID + "&caseID=" + strCaseID + "&fwdProductType=" + strFwdProductType
                + "&categoryList=" + strSelectedCategories;// +"&interBodyFL="+interBodyFL+"&biologicFL="+biologicFL+"&fixationFL="+fixationFL
                                                           // +"&accessFL="+accessFL+"&favouriteCaseID="+
                                                           // strFavCaseID;

      }
      log.debug("=====strPath==========" + strPath);
      //nextForward.setPath(strPath);
      //nextForward.setRedirect(true);
      //return nextForward;
      return actionRedirect(strPath, request);
    }
    gmCaseBuilderForm.setAlFavouriteCases(GmCommonClass.parseNullArrayList(gmCaseBuilderBean
        .fetchFavoriteCase(hmParam)));
    gmCaseBuilderForm.setAlCategories(GmCommonClass.getCodeList("CASCAT"));
    return mapping.findForward(strForward);

  }

}
