package com.globus.sales.CaseManagement.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.forms.GmCaseCalendarForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmCaseCalendarAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward caseCalendar(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmCaseCalendarForm gmCaseCalendarForm = (GmCaseCalendarForm) form;
    gmCaseCalendarForm.loadSessionParameters(request);
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
    HttpSession session = request.getSession(false);
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    log.debug("inside of GmCaseCalendarAction");
    String strApplnDateFmt =
        GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth - 1);
    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth + 1);
    String strAccessCondition = "";
    String strOpt = gmCaseCalendarForm.getStrOpt();
    String strAction = GmCommonClass.parseNull(gmCaseCalendarForm.getHaction());

    Date currDate = GmCommonClass.getCurrentDate(strApplnDateFmt);

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    strAccessCondition = getAccessFilter(request, response);
    log.debug("strAccessCondition >>> " + strAccessCondition);

    if (strOpt.equals("load")) {
      int intMonth = gmCaseCalendarForm.getCcMonth();
      int intYear = gmCaseCalendarForm.getCcYear();
      log.debug("intMonth-" + intMonth + "intYear" + intYear);
      strMonthFirstDay =
          getFirstDayOfMonth(((intMonth - 1 <= 0) ? 11 : (intMonth - 1)),
              ((intMonth - 1 <= 0) ? (intYear - 1) : intYear));
      gmCaseCalendarForm.setFromDt(GmCalenderOperations.getDate(strMonthFirstDay));
      strMonthLastDay =
          getLastDayOfMonth(((intMonth + 1 > 11) ? 0 : (intMonth + 1)),
              ((intMonth + 1 > 11) ? (intYear + 1) : intYear));
      gmCaseCalendarForm.setToDt(GmCalenderOperations.getDate(strMonthLastDay));
      log.debug("strMonthFirstDay-" + strMonthFirstDay + "strMonthLastDay" + strMonthLastDay);
      currDate = GmCalenderOperations.getDate(getFirstDayOfMonth(intMonth, intYear));
    }

    if (strAction.equals("day") || strAction.equals("week")) {
      currDate = gmCaseCalendarForm.getFromDt();
    }
    gmCaseCalendarForm.setToDt(GmCalenderOperations.getDate(strMonthLastDay));
    gmCaseCalendarForm.setFromDt(GmCalenderOperations.getDate(strMonthFirstDay));

    // if(strOpt.equals("load")){
    hmParam = GmCommonClass.getHashMapFromForm(gmCaseCalendarForm);
    hmParam.put("AccessFilter", strAccessCondition);
    hmParam.put("DATEFORMAT", strApplnDateFmt);
    alResult = gmCaseBookRptBean.fetchCaseCalendar(hmParam);
    gmCaseCalendarForm.setFromDt(currDate);
    log.debug("alResult==>" + alResult);
    gmCaseCalendarForm.setGridData(generateOutPut(alResult));
    // }
    return mapping.findForward("caseCalendarView");
  }

  public ActionForward calendarCaseInfo(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmCaseCalendarForm gmCaseCalendarForm = (GmCaseCalendarForm) form;
    gmCaseCalendarForm.loadSessionParameters(request);
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    HashMap hmResult = new HashMap();
    String strCaseInfoId = gmCaseCalendarForm.getCaseInfoId();
    log.debug("strCaseInfoId==>" + strCaseInfoId);
    hmResult = gmCaseBookRptBean.fetchCalendarCaseInfo(strCaseInfoId);
    log.debug("hmResult==>" + hmResult);
    hmResult.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    gmCaseCalendarForm.setGridData(generateOutPut(hmResult));
    return mapping.findForward("calendarCaseInfo");
  }

  private String generateOutPut(ArrayList alResult) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateSubDir("sales/CaseManagement/templates");
    templateUtil.setTemplateName("GmCaseCalendar.vm");
    return templateUtil.generateOutput();
  }

  private String generateOutPut(HashMap hmResult) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmResult.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmResult", hmResult);
    templateUtil.setTemplateSubDir("sales/CaseManagement/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.CaseManagement.GmCaseCalendarInfo", strSessCompanyLocale));
    templateUtil.setTemplateName("GmCaseCalendarInfo.vm");

    return templateUtil.generateOutput();
  }

  public String getFirstDayOfMonth(int intMonth, int intYear) {
    SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.MONTH, intMonth);
    calendar.set(Calendar.YEAR, intYear);
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    return fmt.format(calendar.getTime());
  }

  public static String getLastDayOfMonth(int intMonth, int intYear) {
    SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.MONTH, intMonth);
    calendar.set(Calendar.YEAR, intYear);
    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    return fmt.format(calendar.getTime());
  }
}
