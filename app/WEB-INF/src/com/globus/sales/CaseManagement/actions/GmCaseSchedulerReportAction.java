/**
 * FileName : GmCaseSchedulerReportAction.java, Description : case scheduler Report, Author : N Raja
 */
package com.globus.sales.CaseManagement.actions;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.text.SimpleDateFormat;
import java.io.PrintWriter;

import java.text.DateFormat;  
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.actions.GmDispatchAction;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.sales.CaseManagement.beans.GmCaseSchedulerReportBean;
import com.globus.sales.CaseManagement.forms.GmCaseSchedulerReportForm;

public class GmCaseSchedulerReportAction extends GmSalesDispatchAction  {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
/**
 * @description caseSchedulerListRpt used for Case scheduler report page load
 * @param mapping, form, request, response
 * @return mapping findForward
 * @throws Exception
 */
    public ActionForward caseSchedulerListRpt(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			 HttpServletResponse response) throws Exception {
	            instantiate(request, response);
				GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
				GmCaseSchedulerReportForm gmCaseSchedulerReportForm = (GmCaseSchedulerReportForm) form;
				gmCaseSchedulerReportForm.loadSessionParameters(request);
				GmCaseSchedulerReportBean gmCaseSchedulerReportBean=new GmCaseSchedulerReportBean(getGmDataStoreVO());
				GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
				HttpSession session = request.getSession(false);
				HashMap hmParam = new HashMap();
				ArrayList alReturn = new ArrayList();
				ArrayList alDivisionList = new ArrayList();
				String strOpt = gmCaseSchedulerReportForm.getStrOpt();
				String strGridXmlData="";
				String strSessCompanyLocale = "";
				String strApplnDateFmt  = getGmDataStoreVO().getCmpdfmt();
				int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
		 	    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplnDateFmt);
		 	    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth,strApplnDateFmt);
		 	    gmCaseSchedulerReportForm.setToDt((Date)gmCaseSchedulerReportForm.getToDt());
		 	    gmCaseSchedulerReportForm.setFromDt((Date)gmCaseSchedulerReportForm.getFromDt());
			    gmCaseSchedulerReportForm.setAldateSelection(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CSHRPT", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASESC", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlTerm(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASETM", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setStatus("107160,107161,107162");
			    gmCaseSchedulerReportForm.setLongTerm("107680");
			    gmCaseSchedulerReportForm.setSurgerydate("107300");//Surgery date for date selection
			    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
			    alDivisionList.remove("0");
			        log.debug("alDivisionList== "+alDivisionList );
			    gmCaseSchedulerReportForm.setAlDivision(alDivisionList);
			    gmCaseSchedulerReportForm.setSpineDivision("2000");
			     if(strOpt.equals("fetch")) { 
			    	String strStatus = GmCommonClass.parseNull(request.getParameter("status"));
			    	gmCaseSchedulerReportForm.setStatus(strStatus);
			    	alReturn = fetchCaseSchedulerReport(gmCaseSchedulerReportForm,request,strApplnDateFmt,"");
	                DateFormat dateFormat = new SimpleDateFormat(strApplnDateFmt);  
	                strMonthFirstDay = dateFormat.format(gmCaseSchedulerReportForm.getFromDt());  
	                strMonthLastDay = dateFormat.format(gmCaseSchedulerReportForm.getToDt());
	                gmCaseSchedulerReportForm.setSpineDivision(gmCaseSchedulerReportForm.getDivision());
			    	hmParam.put("SESSDATEFMT", strApplnDateFmt);
		            hmParam.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
		            hmParam.put("TEMPLATE_NM", "GmCaseSchedulerRpt.vm");
			        strGridXmlData = generateOutPut(hmParam,alReturn);
			     }
			    gmCaseSchedulerReportForm.setGridXmlData(strGridXmlData);
			    request.setAttribute("XMLGRIDDATA",strGridXmlData);
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
		 return mapping.findForward("caseSchedulerRpt");
	}
/**
 * @description KitShipDshRpt used to load case scheduler report based on kit shipping details on case scheduler dashboard screen
 * @param mapping, form, request, response
 * @return mapping findForward
 * @throws Exception
 */  
    public ActionForward KitShipDshRpt(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			 HttpServletResponse response) throws Exception {

	            instantiate(request, response);
				GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
				GmCaseSchedulerReportForm gmCaseSchedulerReportForm = (GmCaseSchedulerReportForm) form;
				gmCaseSchedulerReportForm.loadSessionParameters(request);
				GmCaseSchedulerReportBean gmCaseSchedulerReportBean=new GmCaseSchedulerReportBean(getGmDataStoreVO());
				GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
				HttpSession session = request.getSession(false);
				HashMap hmParam = new HashMap();
				ArrayList alReturn = new ArrayList();
				ArrayList alDivisionList = new ArrayList();
				String strOpt = gmCaseSchedulerReportForm.getStrOpt();
				String strGridXmlData="";
				String strSessCompanyLocale = "";
				String strApplnDateFmt  = getGmDataStoreVO().getCmpdfmt();
				int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
		 	    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplnDateFmt);
		 	    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth,strApplnDateFmt);

			    gmCaseSchedulerReportForm.setAldateSelection(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CSHRPT", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASESC", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlTerm(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASETM", getGmDataStoreVO())));
			    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
			    alDivisionList.remove("0");
			    gmCaseSchedulerReportForm.setAlDivision(alDivisionList);
			    gmCaseSchedulerReportForm.setSpineDivision("0");
			    String strAction = GmCommonClass.parseNull((String)request.getParameter("strAction"));
		 	    if(strAction.equals("KITOVERDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitOverstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitOverendDt"));
		 	    }if(strAction.equals("KITTODAYDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitTodaystDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitTodayendDt"));
		 	    }if(strAction.equals("KITTMWDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitTmwstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitTmwendDt"));
		 	    }if(strAction.equals("KITSHIPTW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitupendDt"));		 	    	
		 	    }if(strAction.equals("KITSHIPNW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitupendDt"));		 	    	
		 	    }if(strAction.equals("KITSHIPTM")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitupendDt"));		 	    	
		 	    }
	 	    	gmCaseSchedulerReportForm.setDateSelection("107301");
	 	    	gmCaseSchedulerReportForm.setStatus("107160");
	 	    	gmCaseSchedulerReportForm.setLongTerm("0");
	 	    	gmCaseSchedulerReportForm.setToDt(GmCommonClass.getStringToDate(strMonthLastDay,strApplnDateFmt));
	 	    	gmCaseSchedulerReportForm.setFromDt(GmCommonClass.getStringToDate(strMonthFirstDay,strApplnDateFmt));
	 	    	String strKitDash = "KITDASH";
			    alReturn = fetchCaseSchedulerReport(gmCaseSchedulerReportForm,request,strApplnDateFmt,strKitDash);
	            DateFormat dateFormat = new SimpleDateFormat(strApplnDateFmt);  
	            strMonthFirstDay = dateFormat.format(gmCaseSchedulerReportForm.getFromDt());  
	            strMonthLastDay = dateFormat.format(gmCaseSchedulerReportForm.getToDt());
	            gmCaseSchedulerReportForm.setSpineDivision(gmCaseSchedulerReportForm.getDivision());
			    hmParam.put("SESSDATEFMT", strApplnDateFmt);
		        hmParam.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
		        hmParam.put("TEMPLATE_NM", "GmCaseSchedulerRpt.vm");
			    strGridXmlData = generateOutPut(hmParam,alReturn);
				gmCaseSchedulerReportForm.setGridXmlData(strGridXmlData);
				request.setAttribute("XMLGRIDDATA",strGridXmlData);
		 	    gmCaseSchedulerReportForm.setSurgerydate("107301");
		 	    gmCaseSchedulerReportForm.setStatus("107160");
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
		 return mapping.findForward("caseSchedulerRpt");
	}
/**
 * @description KitRetDshRpt used to load case scheduler report based on kit returns details on case scheduler dashboard screen
 * @param mapping, form, request, response
 * @return mapping findForward
 * @throws Exception
 */ 
    public ActionForward KitRetDshRpt(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			 HttpServletResponse response) throws Exception {

	            instantiate(request, response);
				GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
				GmCaseSchedulerReportForm gmCaseSchedulerReportForm = (GmCaseSchedulerReportForm) form;
				gmCaseSchedulerReportForm.loadSessionParameters(request);
				GmCaseSchedulerReportBean gmCaseSchedulerReportBean=new GmCaseSchedulerReportBean(getGmDataStoreVO());
				GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
				HttpSession session = request.getSession(false);
				HashMap hmParam = new HashMap();
				ArrayList alReturn = new ArrayList();
				ArrayList alDivisionList = new ArrayList();
				String strOpt = gmCaseSchedulerReportForm.getStrOpt();
				String strGridXmlData="";
				String strSessCompanyLocale = "";
				String strApplnDateFmt  = getGmDataStoreVO().getCmpdfmt();
				int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
		 	    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplnDateFmt);
		 	    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth,strApplnDateFmt);

			    gmCaseSchedulerReportForm.setAldateSelection(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CSHRPT", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASESC", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlTerm(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASETM", getGmDataStoreVO())));
			    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
			    alDivisionList.remove("0");
			    gmCaseSchedulerReportForm.setAlDivision(alDivisionList);
			    gmCaseSchedulerReportForm.setSpineDivision("0");
			    
			    String strAction = GmCommonClass.parseNull((String)request.getParameter("strAction"));
		 	    if(strAction.equals("KITOVERDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitOverstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitOverendDt"));
		 	    }if(strAction.equals("KITTODAYDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitTodaystDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitTodayendDt"));
		 	    }if(strAction.equals("KITTMWDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitTmwstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitTmwendDt"));
		 	    }if(strAction.equals("KITRETTW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitupendDt"));
		 	    }if(strAction.equals("KITRETNW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitupendDt"));
		 	    }if(strAction.equals("KITRETTM")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("kitupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("kitupendDt"));
		 	    }  
		 	    gmCaseSchedulerReportForm.setDateSelection("107302");
		 	    gmCaseSchedulerReportForm.setSurgerydate("107302");
		 	    gmCaseSchedulerReportForm.setStatus("107161");
		 	    gmCaseSchedulerReportForm.setLongTerm("0");
		 	    gmCaseSchedulerReportForm.setToDt(GmCommonClass.getStringToDate(strMonthLastDay,strApplnDateFmt));
	 	    	gmCaseSchedulerReportForm.setFromDt(GmCommonClass.getStringToDate(strMonthFirstDay,strApplnDateFmt));
	 	    	String strKitDash = "KITDASH";
			    alReturn = fetchCaseSchedulerReport(gmCaseSchedulerReportForm,request,strApplnDateFmt,strKitDash);
	            DateFormat dateFormat = new SimpleDateFormat(strApplnDateFmt);  
	            strMonthFirstDay = dateFormat.format(gmCaseSchedulerReportForm.getFromDt());  
	            strMonthLastDay = dateFormat.format(gmCaseSchedulerReportForm.getToDt());
	            gmCaseSchedulerReportForm.setSpineDivision(gmCaseSchedulerReportForm.getDivision());
			    hmParam.put("SESSDATEFMT", strApplnDateFmt);
		        hmParam.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
		        hmParam.put("TEMPLATE_NM", "GmCaseSchedulerRpt.vm");
			    strGridXmlData = generateOutPut(hmParam,alReturn);
				gmCaseSchedulerReportForm.setGridXmlData(strGridXmlData);
				request.setAttribute("XMLGRIDDATA",strGridXmlData);
		 	    gmCaseSchedulerReportForm.setSurgerydate("107302");
		 	    gmCaseSchedulerReportForm.setStatus("107161");
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
		 return mapping.findForward("caseSchedulerRpt");
	}
/**
 * @description CaseShipDshRpt used to load case scheduler report based on case shipping details on case scheduler dashboard screen
 * @param mapping, form, request, response
 * @return mapping findForward
 * @throws Exception
 */     
    public ActionForward CaseShipDshRpt(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			 HttpServletResponse response) throws Exception {

	            instantiate(request, response);
				GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
				GmCaseSchedulerReportForm gmCaseSchedulerReportForm = (GmCaseSchedulerReportForm) form;
				gmCaseSchedulerReportForm.loadSessionParameters(request);
				GmCaseSchedulerReportBean gmCaseSchedulerReportBean=new GmCaseSchedulerReportBean(getGmDataStoreVO());
				GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
				HttpSession session = request.getSession(false);
				HashMap hmParam = new HashMap();
				ArrayList alReturn = new ArrayList();
				ArrayList alDivisionList = new ArrayList();
				String strOpt = gmCaseSchedulerReportForm.getStrOpt();
				String strGridXmlData="";
				String strSessCompanyLocale = "";
				String strApplnDateFmt  = getGmDataStoreVO().getCmpdfmt();
				int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
		 	    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplnDateFmt);
		 	    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth,strApplnDateFmt);

			    gmCaseSchedulerReportForm.setAldateSelection(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CSHRPT", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASESC", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlTerm(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASETM", getGmDataStoreVO())));
			    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
			    alDivisionList.remove("0");
			    gmCaseSchedulerReportForm.setAlDivision(alDivisionList);
			    gmCaseSchedulerReportForm.setSpineDivision("0");
			    String strAction = GmCommonClass.parseNull((String)request.getParameter("strAction"));
		 	    if(strAction.equals("CASEOVERDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseOverstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseOverendDt"));
		 	    }if(strAction.equals("CASETODAYDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseTodaystDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseTodayendDt"));
		 	    }if(strAction.equals("CASETMWDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseTmwstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseTmwendDt"));
		 	    }if(strAction.equals("CASESHIPTW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseupendDt"));
		 	    }if(strAction.equals("CASESHIPNW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseupendDt"));
		 	    }if(strAction.equals("CASESHIPTM")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseupendDt"));
		 	    }
		 	    gmCaseSchedulerReportForm.setDateSelection("107301");
		 	    gmCaseSchedulerReportForm.setSurgerydate("107301");
		 	    gmCaseSchedulerReportForm.setStatus("107160");
		 	    gmCaseSchedulerReportForm.setLongTerm("0");
		 	    gmCaseSchedulerReportForm.setToDt(GmCommonClass.getStringToDate(strMonthLastDay,strApplnDateFmt));
	 	    	gmCaseSchedulerReportForm.setFromDt(GmCommonClass.getStringToDate(strMonthFirstDay,strApplnDateFmt));
			    alReturn = fetchCaseSchedulerReport(gmCaseSchedulerReportForm,request,strApplnDateFmt,"");
	            DateFormat dateFormat = new SimpleDateFormat(strApplnDateFmt);  
	            strMonthFirstDay = dateFormat.format(gmCaseSchedulerReportForm.getFromDt());  
	            strMonthLastDay = dateFormat.format(gmCaseSchedulerReportForm.getToDt());
	            gmCaseSchedulerReportForm.setSpineDivision(gmCaseSchedulerReportForm.getDivision());
			    hmParam.put("SESSDATEFMT", strApplnDateFmt);
		        hmParam.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
		        hmParam.put("TEMPLATE_NM", "GmCaseSchedulerRpt.vm");
			    strGridXmlData = generateOutPut(hmParam,alReturn);
				gmCaseSchedulerReportForm.setGridXmlData(strGridXmlData);
				request.setAttribute("XMLGRIDDATA",strGridXmlData);
		 	    gmCaseSchedulerReportForm.setSurgerydate("107301");
		 	    gmCaseSchedulerReportForm.setStatus("107160");
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
		 return mapping.findForward("caseSchedulerRpt");
	}
/**
 * @description CaseRetDshRpt used to load case scheduler report based on case returns details on case scheduler dashboard screen
 * @param mapping, form, request, response
 * @return mapping findForward
 * @throws Exception
 */ 
    public ActionForward CaseRetDshRpt(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			 HttpServletResponse response) throws Exception {

	            instantiate(request, response);
				GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
				GmCaseSchedulerReportForm gmCaseSchedulerReportForm = (GmCaseSchedulerReportForm) form;
				gmCaseSchedulerReportForm.loadSessionParameters(request);
				GmCaseSchedulerReportBean gmCaseSchedulerReportBean=new GmCaseSchedulerReportBean(getGmDataStoreVO());
				GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
				HttpSession session = request.getSession(false);
				HashMap hmParam = new HashMap();
				ArrayList alReturn = new ArrayList();
				ArrayList alDivisionList = new ArrayList();
				String strOpt = gmCaseSchedulerReportForm.getStrOpt();
				String strGridXmlData="";
				String strSessCompanyLocale = "";
				String strApplnDateFmt  = getGmDataStoreVO().getCmpdfmt();
				int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
		 	    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplnDateFmt);
		 	    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth,strApplnDateFmt);

			    gmCaseSchedulerReportForm.setAldateSelection(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CSHRPT", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASESC", getGmDataStoreVO())));
			    gmCaseSchedulerReportForm.setAlTerm(GmCommonClass.parseNullArrayList(GmCommonClass
			            .getCodeList("CASETM", getGmDataStoreVO())));
			    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
			    alDivisionList.remove("0");
			    gmCaseSchedulerReportForm.setAlDivision(alDivisionList);
			    gmCaseSchedulerReportForm.setSpineDivision("0");
			    String strAction = GmCommonClass.parseNull((String)request.getParameter("strAction"));
		 	    if(strAction.equals("CASEOVERDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseOverstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseOverendDt"));
		 	    }if(strAction.equals("CASETODAYDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseTodaystDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseTodayendDt"));
		 	    }if(strAction.equals("CASETMWDSH")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseTmwstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseTmwendDt"));
		 	    }if(strAction.equals("CASERETTW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseupendDt"));
		 	    }if(strAction.equals("CASERETNW")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseupendDt"));
		 	    }if(strAction.equals("CASERETTM")){
		 	    	strMonthFirstDay = GmCommonClass.parseNull((String)request.getParameter("caseupstDt"));
		 	    	strMonthLastDay = GmCommonClass.parseNull((String)request.getParameter("caseupendDt"));
		 	    } 
		 	    gmCaseSchedulerReportForm.setDateSelection("107302");
		 	    gmCaseSchedulerReportForm.setSurgerydate("107302");
		 	    gmCaseSchedulerReportForm.setStatus("107161");
		 	    gmCaseSchedulerReportForm.setLongTerm("0");
		 	    gmCaseSchedulerReportForm.setToDt(GmCommonClass.getStringToDate(strMonthLastDay,strApplnDateFmt));
	 	    	gmCaseSchedulerReportForm.setFromDt(GmCommonClass.getStringToDate(strMonthFirstDay,strApplnDateFmt));
			    alReturn = fetchCaseSchedulerReport(gmCaseSchedulerReportForm,request,strApplnDateFmt,"");
	            DateFormat dateFormat = new SimpleDateFormat(strApplnDateFmt);  
	            strMonthFirstDay = dateFormat.format(gmCaseSchedulerReportForm.getFromDt());  
	            strMonthLastDay = dateFormat.format(gmCaseSchedulerReportForm.getToDt());
	            gmCaseSchedulerReportForm.setSpineDivision(gmCaseSchedulerReportForm.getDivision());
			    hmParam.put("SESSDATEFMT", strApplnDateFmt);
		        hmParam.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
		        hmParam.put("TEMPLATE_NM", "GmCaseSchedulerRpt.vm");
			    strGridXmlData = generateOutPut(hmParam,alReturn);
				gmCaseSchedulerReportForm.setGridXmlData(strGridXmlData);
				request.setAttribute("XMLGRIDDATA",strGridXmlData);
		 	    gmCaseSchedulerReportForm.setSurgerydate("107302");
		 	    gmCaseSchedulerReportForm.setStatus("107161");
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
			    request.setAttribute("hFrmDt",strMonthFirstDay);
	            request.setAttribute("hToDt",strMonthLastDay);
		 return mapping.findForward("caseSchedulerRpt");
	}
    
 /**
  * @description generateOutPut used for case scheduler report grid data view
  * @param hmParam, alReturn, 
  * @return templateUtil xml data
  * @throws Exception
  */
    private String generateOutPut(HashMap hmParam , ArrayList alReturn) throws Exception {
			 	GmTemplateUtil templateUtil = new GmTemplateUtil();
			 	String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
			 	String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE_NM"));
			 	templateUtil.setDataList("alResult", alReturn);
			 	templateUtil.setDataMap("hmParam", hmParam);
				templateUtil.setTemplateName(strTemplateName);
				templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.sales.CaseManagement.GmCaseSchedulerRpt", strSessCompanyLocale));
				templateUtil.setTemplateSubDir("sales/CaseManagement/templates");
		 return templateUtil.generateOutput();
      }
  
   
/**
 * @description fetchCaseInventoryInfo used case id clicked open window view
 * @param mapping, form, request, response
 * @return mapping findForward
 * @throws Exception
 */
	public ActionForward fetchCaseInventoryInfo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
			    GmCodeGroupBean gmCodeGroupBean = new GmCodeGroupBean();
				GmCaseSchedulerReportForm gmCaseSchedulerReportForm = (GmCaseSchedulerReportForm) form;
				gmCaseSchedulerReportForm.loadSessionParameters(request);
				GmCaseSchedulerReportBean gmCaseSchedulerReportBean=new GmCaseSchedulerReportBean(getGmDataStoreVO());
				HttpSession session = request.getSession(false);
				ArrayList aldata = new ArrayList();
				HashMap hmData = new HashMap();
				HashMap hmParam = new HashMap();
				String strSessCompanyLocale = "";
				String strCaseId = GmCommonClass.parseNull((String) request.getParameter("paramCaseId"));
				hmParam.put("paramCaseId",strCaseId);
	            aldata = GmCommonClass.parseNullArrayList(gmCaseSchedulerReportBean.loadInventoryInfo(hmParam));
	            log.debug("aldata size --> "+aldata.size());
	            hmData.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
	            hmData.put("TEMPLATE_NM", "GmCaseSchedulerInventoryInfo.vm");
		        String strGridXml = generateGridOutPut(hmData,aldata);
		       	request.setAttribute("XMLGRIDINVDATA",strGridXml);
		       	gmCaseSchedulerReportForm.setGridXmlData1(strGridXml);
			return mapping.findForward("caseSchedulerInfo");  
	  }
	  
/**
 * @description generateGridOutPut used for case id details grid data view
 * @param hmParam, alReturn
 * @return  templateUtil xml data
 * @throws Exception
 */
   private String generateGridOutPut(HashMap hmParam , ArrayList alReturn) throws Exception {
			 	GmTemplateUtil templateUtil = new GmTemplateUtil();
			 	String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
			 	String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE_NM"));
			 	templateUtil.setDataList("alResult", alReturn);
			 	templateUtil.setDataMap("hmParam", hmParam);
				templateUtil.setTemplateName(strTemplateName);
				templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.sales.CaseManagement.GmCaseInventoryInfo", strSessCompanyLocale));
				templateUtil.setTemplateSubDir("sales/CaseManagement/templates");
		  return templateUtil.generateOutput();
	  }
	  
/**
  * @description fetchCaseSchedulerReport used for case scheduler Report page filtering grid data
  * @param gmCaseSchedulerReportForm,gmCaseSchedulerReportBean,request,hmParam,alReturn,strApplnDateFmt
  * @return alReturn
  * @throws AppError
  */
  private ArrayList fetchCaseSchedulerReport(GmCaseSchedulerReportForm gmCaseSchedulerReportForm,HttpServletRequest request, String strApplnDateFmt,String strKitDash)throws AppError {
	  			GmCaseSchedulerReportBean gmCaseSchedulerReportBean=new GmCaseSchedulerReportBean(getGmDataStoreVO());
	  			HashMap hmParam = new HashMap();
	  			ArrayList alReturn = new ArrayList(); 
			    hmParam = GmCommonClass.getHashMapFromForm(gmCaseSchedulerReportForm);
			    log.debug("hmParam--> " +hmParam);
			    hmParam.put("DATEFORMAT", strApplnDateFmt);
			    hmParam.put("KITDASH", strKitDash);
			    alReturn = GmCommonClass.parseNullArrayList(gmCaseSchedulerReportBean.fetchReport(hmParam));
			    String strStatus = GmCommonClass.parseNull(request.getParameter("status"));
			    String strAccount = GmCommonClass.parseNull(request.getParameter("account"));
			    String strSalesRep = GmCommonClass.parseNull(request.getParameter("salesRep"));
			    String strFieldSales = GmCommonClass.parseNull(request.getParameter("fieldSales"));
			    String dateSelection = GmCommonClass.parseNull(request.getParameter("dateSelection"));
			    String kitMapId = GmCommonClass.parseNull(request.getParameter("kitMapId"));
			    String strLongTerm = GmCommonClass.parseNull(request.getParameter("term"));	
			    log.debug("strLongTerm=>"+strLongTerm+" dateSelection=>"+dateSelection);
			    gmCaseSchedulerReportForm.setStatus(strStatus);
				gmCaseSchedulerReportForm.setAccount(strAccount);
				gmCaseSchedulerReportForm.setSalesRep(strSalesRep);
				gmCaseSchedulerReportForm.setFieldSales(strFieldSales);
				gmCaseSchedulerReportForm.setSurgerydate(dateSelection);
				gmCaseSchedulerReportForm.setKitMapId(kitMapId);
				gmCaseSchedulerReportForm.setLongTerm(strLongTerm);
          return alReturn;
	  }
} 
