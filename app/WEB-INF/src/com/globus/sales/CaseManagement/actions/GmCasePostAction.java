package com.globus.sales.CaseManagement.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.CaseManagement.forms.GmCasePostForm;
import com.globus.sales.InvAllocation.beans.GmSetOverrideRptBean;

public class GmCasePostAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCasePostForm gmCasePostForm = (GmCasePostForm) form;
    gmCasePostForm.loadSessionParameters(request);

    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean(getGmDataStoreVO());
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmSetOverrideRptBean gmSetOverrideRptBean = new GmSetOverrideRptBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alBookedSets = new ArrayList();
    String strForward = "success";
    String strPartyId = "";
    String strAccessFl = "";

    String strAccessCondition = getAccessFilter(request, response);
    String strOpt = gmCasePostForm.getStrOpt();
    String strCaseInfoID = gmCasePostForm.getCaseInfoID();

    hmParam.put("AccessFilter", strAccessCondition);

    hmParam = GmCommonClass.getHashMapFromForm(gmCasePostForm);
    log.debug("hmParam>>>> " + hmParam);


    if (strOpt.equals("post")) {
      gmCaseBookTxnBean.saveCasePostInfo(hmParam);
    } else if (strOpt.equals("initLoaner")) {
      gmSetOverrideRptBean.saveLoanerTagRequest(hmParam);
    }

    hmResult = gmCaseBookRptBean.fetchAllBookedSets(hmParam);
    gmCasePostForm.setHmGenerlInfo((HashMap) hmResult.get("GENERALINFO"));
    gmCasePostForm.setAlCaseAttrDtls((ArrayList) hmResult.get("CASEATTR"));
    gmCasePostForm.setAlAllBookedSets((ArrayList) hmResult.get("ALLSETS"));

    strPartyId = gmCasePostForm.getSessPartyId();
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "CSM_NEW_ORDER"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmCasePostForm.setPhoneOrderAccess(strAccessFl);
    return mapping.findForward(strForward);

  }
}
