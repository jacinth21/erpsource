package com.globus.sales.CaseManagement.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.forms.GmCaseFavoritesRptForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmCaseFavoritesRptAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  public ActionForward caseFavoritesRpt(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCaseFavoritesRptForm gmCaseFavoritesRptForm = (GmCaseFavoritesRptForm) form;
    gmCaseFavoritesRptForm.loadSessionParameters(request);

    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    String strAccessCondition = getAccessFilter(request, response);
    log.debug("strAccessCondition ::" + strAccessCondition);
    HashMap hmFilter = new HashMap();
    hmFilter.put("CONDITION",strAccessCondition);
    hmFilter.put("FLTR_DIST_BY_COMP","YES");//Filter Distributor Based on company
    hmSalesFilters = gmCommonBean.getSalesFilterLists(hmFilter);

    String strOpt = gmCaseFavoritesRptForm.getStrOpt();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
    gmCaseFavoritesRptForm.setAlRep(alResult);
    hmParam = GmCommonClass.getHashMapFromForm(gmCaseFavoritesRptForm);
    hmParam.put("AccessFilter", strAccessCondition);
    if (strOpt.equals("load")) {
      alResult = gmCaseBookRptBean.fetchCaseFavoritesListRpt(hmParam);
      log.debug("alResult" + alResult.size());
      gmCaseFavoritesRptForm.setAlResult(alResult);
      // gmCaseFavoritesRptForm.setGridData(generateOutPut(alResult));
    }
    return mapping.findForward("caseFavoritesRpt");
  }
  /*
   * private String generateOutPut(ArrayList alResult) throws Exception { GmTemplateUtil
   * templateUtil = new GmTemplateUtil(); templateUtil.setDataList("alResult", alResult);
   * templateUtil.setTemplateSubDir("sales/CaseManagement/templates");
   * templateUtil.setTemplateName("GmCaseFavoritesRpt.vm"); return templateUtil.generateOutput(); }
   */
}
