package com.globus.sales.CaseManagement.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.CaseManagement.forms.GmCaseBookSetupForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.pricing.beans.GmPricingRequestBean;

public class GmCaseBookSetupAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCaseBookSetupForm gmCaseBookSetupForm = (GmCaseBookSetupForm) form;
    gmCaseBookSetupForm.loadSessionParameters(request);

    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    ArrayList alDist = new ArrayList();
    String strDistId = "";
    String strForward = "success";

    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    String strAccessCondition = getAccessFilter(request, response);
    String strOpt = gmCaseBookSetupForm.getStrOpt();
    String strCaseInfoID = gmCaseBookSetupForm.getCaseInfoID();
    String strCaseID = gmCaseBookSetupForm.getCaseID();
    log.debug("strCaseInfoID>>>>" + strCaseInfoID);
    hmParam = GmCommonClass.getHashMapFromForm(gmCaseBookSetupForm);
    hmParam.put("AccessFilter", strAccessCondition);
    log.debug("hmParam>>>>" + hmParam);

    if (strOpt.equals("save") || strOpt.equals("next")) {

      hmResult = gmCaseBookTxnBean.saveCaseInfo(hmParam);
      strCaseInfoID = GmCommonClass.parseNull((String) hmResult.get("CASEINFOID"));
      strCaseID = GmCommonClass.parseNull((String) hmResult.get("CASEID"));
      log.debug("hmResult>>>>" + hmResult);
      gmCaseBookSetupForm.setCaseInfoID(strCaseInfoID);
      gmCaseBookSetupForm.setCaseID(strCaseID);
      if (strOpt.equals("save")) {
        strOpt = "edit";
      }

    }
    if (strOpt.equals("next")) {
      ActionForward actionForward = mapping.findForward("savenext");
      //ActionForward nextForward = new ActionForward(actionForward);
      String strPath =
          "/gmCaseBuilder.do?strOpt=edit&caseInfoID=" + strCaseInfoID + "&caseID=" + strCaseID;

      //nextForward.setPath(strPath);
      //nextForward.setRedirect(true);
      //return nextForward;
      return actionRedirect(strPath, request);
    }
    if (strOpt.equals("edit")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmCaseBookSetupForm);
      hmResult = gmCaseBookRptBean.fetchCaseInfo(hmParam);
      GmCommonClass.getFormFromHashMap(gmCaseBookSetupForm, hmResult);

    }

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
    // HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessCondition);
    HashMap hmFilter = new HashMap();
    
    hmFilter.put("CONDITION",strAccessCondition);
    hmFilter.put("STATUS","Active");
    hmFilter.put("FLTR_DIST_BY_COMP","YES");//Filter Distributor Based on company
    
    alDist =
        GmCommonClass.parseNullArrayList(gmCommonBean.getSalesFilterDist(hmFilter));
    strDistId = GmCommonClass.parseNull(gmCaseBookSetupForm.getDistributorId());
    if (strDistId.equals("") && alDist.size() == 1) {
      hmTemp = (HashMap) alDist.get(0);
      gmCaseBookSetupForm.setDistributorId(GmCommonClass.parseNull((String) hmTemp.get("ID")));
      gmCaseBookSetupForm.setRepId(gmCaseBookSetupForm.getUserId());
    }
    gmCaseBookSetupForm.setAlAcctList(GmCommonClass.parseNullArrayList(gmCommonBean
        .loadAccountList(strAccessCondition)));
    gmCaseBookSetupForm.setAlRepList(GmCommonClass.parseNullArrayList(gmCommonBean
        .getSalesFilterRep(strAccessCondition, "Active")));
    gmCaseBookSetupForm.setAlDistList(alDist);

    gmCaseBookSetupForm.setAlSurgeryTimeHour(GmCommonClass.getCodeList("HOUR"));
    gmCaseBookSetupForm.setAlSurgeryTimeMin(GmCommonClass.getCodeList("MINUTE"));
    gmCaseBookSetupForm.setAlSurgeryAMPM(GmCommonClass.getCodeList("AMPM"));
    return mapping.findForward(strForward);

  }

}
