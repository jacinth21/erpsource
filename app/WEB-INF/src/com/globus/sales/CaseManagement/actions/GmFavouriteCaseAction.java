package com.globus.sales.CaseManagement.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.CaseManagement.forms.GmFavouriteCaseForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmFavouriteCaseAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    GmFavouriteCaseForm gmFavouriteCaseForm = (GmFavouriteCaseForm) form;
    gmFavouriteCaseForm.loadSessionParameters(request);
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean();
    HashMap hmParam = new HashMap();
    String strOpt = gmFavouriteCaseForm.getStrOpt();
    hmParam = GmCommonClass.getHashMapFromForm(gmFavouriteCaseForm);
    if (strOpt.equals("save")) {
      gmCaseBookTxnBean.saveFavouriteCase(hmParam);
      request.setAttribute("hType", "S");
      throw new AppError("", "20440");
    }
    return mapping.findForward("favouriteCase");
  }
}
