package com.globus.sales.CaseManagement.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.actions.GmShippingAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.CaseManagement.forms.GmCaseBookSetDtlsForm;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmCaseBookSetDtlsAction extends GmDispatchAction{

		Logger log = GmLogger.getInstance(this.getClass().getName());
		
		public ActionForward execute (ActionMapping mapping , ActionForm form , HttpServletRequest request, HttpServletResponse response)throws Exception{
			
    instantiate(request, response);
			GmCaseBookSetDtlsForm gmCaseBookSetDtlsForm = (GmCaseBookSetDtlsForm)form;
			gmCaseBookSetDtlsForm.loadSessionParameters(request);
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean(getGmDataStoreVO());
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
			GmShippingAction gmShippingAction = new GmShippingAction();
			HashMap hmParam = new HashMap(); 
			HashMap hmResult = new HashMap();  
			ArrayList alBookedSets = new ArrayList();
			String strForward = "success"; 
			String strBackScreen = "";
			String strShipId = "";
			String strPlannedShipDate = "";
			Boolean forwardFl = true; 
			String strAccessCondition = getAccessFilter(request, response);
			String strOpt=gmCaseBookSetDtlsForm.getStrOpt(); 
	        String strCaseInfoID = gmCaseBookSetDtlsForm.getCaseInfoID();
	        String strCaseID = gmCaseBookSetDtlsForm.getCaseID();
	        String strAction = GmCommonClass.parseNull((String)gmCaseBookSetDtlsForm.getHaction()); 
	      //  String strEventInfoID = GmCommonClass.parseNull((String)gmCaseBookSetDtlsForm.getEventInfoID());
	        
	        hmParam.put("AccessFilter",strAccessCondition); 
	        
	        hmParam = GmCommonClass.getHashMapFromForm(gmCaseBookSetDtlsForm);
	        log.debug("hmParam>>>>"+hmParam);
	    	String strProdType = GmCommonClass.parseNull((String) hmParam.get("PRODUCTIONTYPE"));
		 	String strFwdProdType = GmCommonClass.parseNull((String) hmParam.get("FWDPRODUCTTYPE"));
		 	String screen = GmCommonClass.parseNull((String) hmParam.get("FROMSCREEN"));
		 	gmCaseBookSetDtlsForm.setScreenTitle(GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strFwdProdType)));
		 	String strCategoryList = gmCaseBookSetDtlsForm.getCategoryList();
		 	if(!strFwdProdType.equals(""))
		 	{
			 	StringTokenizer st = new StringTokenizer("strCategoryList",",");
			 	int count = st.countTokens();
			 	log.debug("x= "+count);
			 	
			 	for(int i=0 ; i<count ; i++){
			 		
			 		String strcodeID = String.valueOf((Integer.parseInt(strFwdProdType) + 1));
			 	
				 	if(strCategoryList.indexOf(strcodeID)!=-1){
				 		gmCaseBookSetDtlsForm.setScreenNote(GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strcodeID)));
				 		//gmCaseBookSetDtlsForm.setScreenNote(GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strFwdProdType)));
				 	}		 	
			 	}
		 	}
		   	if(!strOpt.equals("nextConfirm")&&!strOpt.equals("ProfileConfirm"))
		 	{
		  		 if(!strOpt.equals("ProfileNext")&&!strOpt.equals("ProfileEdit"))
		  	 	{
		  			gmCaseBookSetDtlsForm.setAlSystemSets((ArrayList) gmCaseBookRptBean.fetchCaseSetsMap(hmParam));
		  		}
		  		else 
		  		{
		  			gmCaseBookSetDtlsForm.setAlSystemSets((ArrayList) gmCaseBookRptBean.fetchFavCaseSetsMap(hmParam));
		  		}
		  		 
		 	}
			if(strOpt.equals("save")||strOpt.equals("next")||strOpt.equals("eventNext")||strOpt.equals("ProfileNext")||strOpt.equals("nextConfirm")||strOpt.equals("eventNextConfirm")||strOpt.equals("ProfileConfirm")){ 
		 
				if(!strAction.equals("load")){
					 if(strOpt.equals("ProfileConfirm")||strOpt.equals("ProfileNext"))
					 {
						 gmCaseBookTxnBean.saveCaseProfileSets(hmParam); 
					 }
					 else 
					 {
						 gmCaseBookTxnBean.saveCaseSetsInfo(hmParam); 
						 //creating request for event related sets
						 if(strOpt.equals("eventNext")||strOpt.equals("eventNextConfirm"))
						 {
							 gmEventSetupBean.saveEventRequestDetails (hmParam);
						 }
					 }
				}
			 	 if(strOpt.equals("save")||strOpt.equals("next"))
					{
						strOpt = "edit";
					}
				 	if(strOpt.equals("eventNext"))
					{
				 		strOpt = strFwdProdType.equals("19533")?"editItem":"editEvent";
						hmParam.put("STROPT",strOpt);
					}		 	 
			 	 
				 	if(strOpt.equals("ProfileNext"))
					{
						strOpt = "ProfileEdit";
					}
		 
				}
			
			if(strOpt.equals("edit")||strOpt.equals("editEvent")){
				 
			 	alBookedSets=  gmCaseBookRptBean.fetchCaseSetsInfo(hmParam) ;
			 	gmCaseBookSetDtlsForm.setAlBookedSets(alBookedSets); 
			 
				}
			strBackScreen = gmCaseBookSetDtlsForm.getBackScreen();
			if(strOpt.equals("ProfileEdit")){
				 
			 	alBookedSets=  gmCaseBookRptBean.fetchFavCaseSets(hmParam) ;
			 	gmCaseBookSetDtlsForm.setAlBookedSets(alBookedSets);
			 
			 
				}
			if (strOpt.equals("nextConfirm")||strOpt.equals("ProfileConfirm")||strOpt.equals("eventNextConfirm")) {
				
				if(strOpt.equals("nextConfirm"))
				{
					hmResult = gmCaseBookRptBean.fetchAllBookedSets(hmParam); 
					gmCaseBookSetDtlsForm.setHmGenerlInfo((HashMap)hmResult.get("GENERALINFO"));
      } else if (strOpt.equals("eventNextConfirm")) {
        strCategoryList = gmCaseBookSetDtlsForm.getCategoryList();
        //ActionForward actionForward = mapping.findForward("savenext");
        //ActionForward nextForward = new ActionForward(actionForward);
        // to fetch the shipping id for event edit
        strShipId = GmCommonClass.parseNull(gmEventSetupBean.fecthEventShipId(strCaseInfoID));
        strPlannedShipDate =
            GmCommonClass.parseNull(gmEventSetupBean.fecthEventPlannedShipDate(strCaseInfoID));
        String strPath =
            "/gmIncShipDetails.do?screenType=Loaner&RE_FORWARD=gmEventShipAction&backScreen="
                + strBackScreen + "&strOpt=eventshipping&haction=Load&eventInfoID=" + strCaseInfoID
                + "&caseInfoID=" + strCaseInfoID + "&categoryList=" + strCategoryList + "&shipId="
                + strShipId + "&plannedShipDate=" + strPlannedShipDate;
       // nextForward.setPath(strPath);
        //nextForward.setRedirect(true);
        return actionRedirect(strPath,request);


      } else {
					hmResult = gmCaseBookRptBean.fetchAllProfileSets(hmParam); 
				}
				gmCaseBookSetDtlsForm.setAlCaseAttrDtls((ArrayList)hmResult.get("CASEATTR"));
				gmCaseBookSetDtlsForm.setAlAllBookedSets((ArrayList)hmResult.get("ALLSETS"));
				gmCaseBookSetDtlsForm.setCaseID(strCaseID);
				 
	            return mapping.findForward("savenext");
    } else if (strOpt.equals("editItem")) {
    	String strRedirectPath ="/gmEventItemDtl.do?strOpt=load&backScreen=" + strBackScreen
              + "&fromscreen=" + screen + "&productionType=" + strFwdProdType + "&caseInfoID="
              + strCaseInfoID + "&categoryList=" + strCategoryList;
      return actionRedirect(strRedirectPath,request);
    }
			//event shipping - load & save
			 if ( (strOpt.equals("eventshipping") ) && (strAction.equals("Load")||strAction.equals("Save"))) {				
					
	        	  gmCaseBookSetDtlsForm.setCaseInfoID(strCaseInfoID);
	        	  String strrUserID =  GmCommonClass.parseNull((String)gmCaseBookSetDtlsForm.getUserId());
	        	   if (strAction.equals("Load"))
					{
						GmCommonClass.getFormFromHashMap(gmCaseBookSetDtlsForm, GmCommonClass.parseNullHashMap((HashMap)gmEventSetupBean.fetchEventBookingInfo(strCaseInfoID,strrUserID)));
						String strEditSts = GmCommonClass.parseNull((String)gmEventSetupBean.fetchEventEditStatus(strCaseInfoID));
						gmCaseBookSetDtlsForm.setEventEditSts(strEditSts);
						strForward = "loadEventShip";
						String strMatAddSts = GmCommonClass.parseNull((String)gmEventSetupBean.fetchEventMaterialAddSts(strCaseInfoID));
						if(strMatAddSts.equals("Y"))
						{
							forwardFl = false;
						}
					}
					else if (strAction.equals("Save"))
					{
						hmParam.putAll(GmCommonClass.parseNullHashMap(gmShippingAction.loadShipReqParams(request)));
						log.debug("hmParam:::"+hmParam);
						gmEventSetupBean.saveEventRequestDetails (hmParam);
						
						
						String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTO"));
						String strShipMode = GmCommonClass.parseZero((String) hmParam.get("SHIPMODE"));
						log.debug(strShipMode+"===strShipMode====strShipTo===="+strShipTo);
						
					      gmEventSetupBean.sendRequestRecievedMail(hmParam);
						
						gmCaseBookSetDtlsForm.setCaseInfoID(strCaseInfoID);
						
					}
      if (forwardFl) {
        // redirect to event summary page
        //ActionForward actionForward = mapping.findForward("eventnext");
        //ActionForward nextForward = new ActionForward(actionForward);
        String strPath =
            "/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=HTML&caseInfoID=" + strCaseInfoID;
        //nextForward.setPath(strPath);
        //nextForward.setRedirect(true);
        return actionRedirect(strPath,request);
      }
				
	    		} 
		  
			return mapping.findForward(strForward);
			
		}
	 
	}

