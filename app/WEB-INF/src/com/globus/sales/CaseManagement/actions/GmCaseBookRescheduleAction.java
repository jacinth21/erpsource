package com.globus.sales.CaseManagement.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.CaseManagement.forms.GmCaseBookRescheduleForm;

public class GmCaseBookRescheduleAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute (ActionMapping mapping , ActionForm form , HttpServletRequest request, HttpServletResponse response)throws Exception{
		
		log.debug("enter GmCaseBookRescheduleAction");
		ActionForward actionForward = null;
		ActionForward nextForward = null;
		String   strPath = "";
		GmCaseBookRescheduleForm gmCaseBookRescheduleForm = (GmCaseBookRescheduleForm)form;
		gmCaseBookRescheduleForm.loadSessionParameters(request);
		GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean();
		GmCommonBean gmCommonBean= new GmCommonBean();
		GmCaseBookRptBean gmCaseBookRptBean =new GmCaseBookRptBean(); 
		ArrayList alLogReasons = new ArrayList();
		HashMap hmParam = new HashMap();
		String strCaseInfoId ="";
		String strForward = "success"; 
		String strAccessCondition = getAccessFilter(request, response);
		gmCaseBookRescheduleForm.setAlSurgeryTimeHour((ArrayList) GmCommonClass.getCodeList("HOUR"));
		gmCaseBookRescheduleForm.setAlSurgeryTimeMin((ArrayList) GmCommonClass.getCodeList("MINUTE"));
		gmCaseBookRescheduleForm.setAlSurgeryAMPM((ArrayList) GmCommonClass.getCodeList("AMPM"));
		gmCaseBookRescheduleForm.setAlReasons((ArrayList) GmCommonClass.getCodeList("RESCHR"));
		String strOpt=gmCaseBookRescheduleForm.getStrOpt();
		hmParam = GmCommonClass.getHashMapFromForm(gmCaseBookRescheduleForm);
		String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
		if(strOpt.equals("editresch")){
			hmParam = gmCaseBookRptBean.fetchCaseInfo(hmParam);
			gmCaseBookRescheduleForm.setSurgeryDate((Date)hmParam.get("SURGERYDATE"));
			gmCaseBookRescheduleForm.setSurgeryTimeHour(GmCommonClass.parseNull((String)hmParam.get("SURGERYTIMEHOUR")));
			gmCaseBookRescheduleForm.setSurgeryTimeMin(GmCommonClass.parseNull((String)hmParam.get("SURGERYTIMEMIN")));
			gmCaseBookRescheduleForm.setSurgeryAMPM(GmCommonClass.parseNull((String)hmParam.get("SURGERYAMPM")));
			log.debug("hmParam :::"+hmParam);
		}
		if(strOpt.equals("reschedule"))
		{
			strCaseInfoId = gmCaseBookTxnBean.saveRescheduleCase(hmParam);
			log.debug("strCaseInfoId ::"+strCaseInfoId);
			/* after reschedule load the case book summary with message.*/
			actionForward = mapping.findForward("success"); 
	        nextForward = new ActionForward(actionForward);
	        strPath = "/gmCasePost.do?strOpt="+strOpt+"&caseInfoID="+strCaseInfoId;
	        nextForward.setPath(strPath);
	        nextForward.setRedirect(true);
	        return nextForward;
		}
		alLogReasons = gmCommonBean.getLog(strCaseInfoID,"1266");
		gmCaseBookRescheduleForm.setAlLogReasons(alLogReasons);
		log.debug("exit==>");
		return mapping.findForward(strForward);
		
	}
 

}
