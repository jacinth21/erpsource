/*----------------------------------------------
 * FileName    : GmCaseScheduleCalAction.java 
 * Description : Load Case Schedule Calendar page
 * Author      : N Raja
 *----------------------------------------------
 */

package com.globus.sales.CaseManagement.actions;

import java.text.SimpleDateFormat;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hornetq.utils.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.codehaus.jackson.type.TypeReference;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.CaseManagement.beans.GmCaseSchedulerCalBean;
import com.globus.sales.CaseManagement.beans.GmCaseSchedulerReportBean;
import com.globus.sales.CaseManagement.forms.GmCaseScheduleCalendarForm;
import com.globus.common.util.GmWSUtil;

public class GmCaseScheduleCalendarAction extends GmSalesDispatchAction {

    Logger log = GmLogger.getInstance(this.getClass().getName());

    /**
     * @description fatchCaseSchedularCalendar used for load page
     * @param mapping,form,request,response
     * @return mapping
     * @throws Exception
     */
    public ActionForward fatchCaseSchedularCalendar(ActionMapping mapping, ActionForm form, HttpServletRequest request, 
    		HttpServletResponse response) throws Exception {
    	        log.debug("inside of fatchCaseSchedularCalendar");
    	        instantiate(request, response);
			    GmCaseScheduleCalendarForm gmCaseScheduleCalendarForm = (GmCaseScheduleCalendarForm) form;
			    gmCaseScheduleCalendarForm.loadSessionParameters(request);
			    GmCaseSchedulerCalBean gmCaseSchedulerCalBean = new GmCaseSchedulerCalBean(getGmDataStoreVO());
			    HttpSession session = request.getSession(false);
		        String strApplnDateFmt  = getGmDataStoreVO().getCmpdfmt();
			    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
			    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth - 1,strApplnDateFmt);
			    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth + 1,strApplnDateFmt);
			    Date currDate = GmCommonClass.getCurrentDate(strApplnDateFmt);
			    gmCaseScheduleCalendarForm.setToDt(GmCalenderOperations.getDate(strMonthLastDay));
			    gmCaseScheduleCalendarForm.setFromDt(GmCalenderOperations.getDate(strMonthFirstDay));
			    gmCaseScheduleCalendarForm.setApplnDateFmt(GmCommonClass.parseNull(strApplnDateFmt));
			    gmCaseScheduleCalendarForm.setFromDt(currDate);
		   return mapping.findForward("caseScheduleCalendarView");
  }
   
    /**
     * @description fetchCaseSchedulerCalendarInfo used for case scheduler calendar page data 
     * @param mapping,form,request,response 
     * @return
     * @throws Exception
     */
    public ActionForward fetchCaseSchedulerCalendarInfo(ActionMapping mapping, ActionForm form,
    	      HttpServletRequest request, HttpServletResponse response) throws Exception {
    	        log.debug("inside of fetchCaseSchedulerCalendarInfo");
    	        instantiate(request, response);
	    	    String strJSON = "";
	    	    String strOpt = "";
	    	    ArrayList alReturn = new ArrayList();
	    	    GmWSUtil gmWSUtil = new GmWSUtil();
	    	    HashMap hmParam = new HashMap();
	    	    GmCaseScheduleCalendarForm gmCaseScheduleCalendarForm = (GmCaseScheduleCalendarForm) form;
	    	    gmCaseScheduleCalendarForm.loadSessionParameters(request);
	    	    String strApplnDate  = getGmDataStoreVO().getCmpdfmt();
	    	    log.debug("strApplnDate== "+strApplnDate);
	    	    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	    	    GmCaseSchedulerCalBean gmCaseSchedulerCalBean = new GmCaseSchedulerCalBean(getGmDataStoreVO());
	    	    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
	    	    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplnDate);
	    	    String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth + 2,strApplnDate);
	    	    gmCaseScheduleCalendarForm.setToDt(GmCalenderOperations.getDate(strMonthLastDay));
	    	    gmCaseScheduleCalendarForm.setFromDt(GmCalenderOperations.getDate(strMonthFirstDay));
	    	    hmParam = GmCommonClass.getHashMapFromForm(gmCaseScheduleCalendarForm);
	    	    hmParam.put("DATEFORMAT", strApplnDate);
	    	    String strTermData = GmCommonClass.parseNull(request.getParameter("strOpt"));
	    	    log.debug("strTermData== "+strTermData);
	    	    	 hmParam.put("TERMDATA", strTermData);
	    	    	 strJSON = gmWSUtil.parseArrayListToJson(gmCaseSchedulerCalBean.fetchCaseSchedulerCal(hmParam));
			    	    if (!strJSON.equals("")) {
			    	      response.setContentType("text/plain");
			    	      PrintWriter pw = response.getWriter();
			    	      pw.write(strJSON);
			    	      pw.flush();
			    	      return null;
			    	    }
        return mapping.findForward("caseScheduleCalendarView");
			    	  
    	  }
}
