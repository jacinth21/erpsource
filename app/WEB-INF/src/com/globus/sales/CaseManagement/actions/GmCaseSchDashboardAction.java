/**
 * FileName : GmCaseSchDashboardAction.java, Description : case scheduler dashboard, Author : Agilan
 */
package com.globus.sales.CaseManagement.actions;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.CaseManagement.forms.GmCaseSchDashboardForm;
import com.globus.sales.CaseManagement.beans.GmCaseSchDashboardBean;
import com.globus.sales.DashBoard.beans.GmSalesDashOrdersBean;

public class GmCaseSchDashboardAction extends GmDispatchAction {
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	 
/**
 * 
 * This method is used to fetch kit overdue, today, tomorrow and upcoming week, month details for case scheduler dashboard screen
 * 
 * @param mapping
 * @param request, respone
 * @param form
 */	
	public ActionForward fetchKitData(ActionMapping mapping, ActionForm form,
		    HttpServletRequest request, HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmCaseSchDashboardBean  gmCaseSchDashboardBean = new  GmCaseSchDashboardBean(getGmDataStoreVO());
		    GmCaseSchDashboardForm gmCaseSchDashboardForm = (GmCaseSchDashboardForm) form;
		    gmCaseSchDashboardForm.loadSessionParameters(request);
		    HttpSession session = request.getSession(false);	    
		    HashMap hmParam = new HashMap();
	        hmParam = GmCommonClass.getHashMapFromForm(gmCaseSchDashboardForm);  
	        HashMap hmKitShipDsh = gmCaseSchDashboardBean.fetchKitShipDashboard();
			HashMap hmKitReturnDsh = gmCaseSchDashboardBean.fetchKitReturnsDashboard();
			HashMap hmKitShipDshup  = gmCaseSchDashboardBean.fetchKitShipUpComing();
			HashMap hmKitRetDshup = gmCaseSchDashboardBean.fetchKitRetUpComing();			
			gmCaseSchDashboardForm.setHmKitShipDsh(hmKitShipDsh);
			gmCaseSchDashboardForm.setHmKitReturnDsh(hmKitReturnDsh);
			gmCaseSchDashboardForm.setHmKitShipDshup(hmKitShipDshup);
			gmCaseSchDashboardForm.setHmKitRetDshup(hmKitRetDshup);

		    return mapping.findForward("GmCaseSchDashboard");
		  }
/**
 * 
 * This method is used to fetch case overdue, today, tomorrow and upcoming week, month details for case scheduler dashboard screen
 * 
 * @param mapping
 * @param request, respone
 * @param form
 */		
	public ActionForward fetchCaseData(ActionMapping mapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmCaseSchDashboardBean  gmCaseSchDashboardBean = new  GmCaseSchDashboardBean(getGmDataStoreVO());
		    GmCaseSchDashboardForm gmCaseSchDashboardForm = (GmCaseSchDashboardForm) form;
		    gmCaseSchDashboardForm.loadSessionParameters(request);
		    HttpSession session = request.getSession(false);		    
		    HashMap hmParam = new HashMap();
	        hmParam = GmCommonClass.getHashMapFromForm(gmCaseSchDashboardForm);  
	        HashMap hmCaseShipDsh = gmCaseSchDashboardBean.fetchShippingCaseDashboard();
			HashMap hmCaseReturnDsh = gmCaseSchDashboardBean.fetchReturnsCaseDashboard();
			HashMap hmCaseShipDshup  = gmCaseSchDashboardBean.fetchCaseShipUpComing();
			HashMap hmCaseRetDshup = gmCaseSchDashboardBean.fetchCaseRetUpComing();
			gmCaseSchDashboardForm.setHmCaseShipDsh(hmCaseShipDsh);
			gmCaseSchDashboardForm.setHmCaseReturnDsh(hmCaseReturnDsh);
			gmCaseSchDashboardForm.setHmCaseShipDshup(hmCaseShipDshup);
			gmCaseSchDashboardForm.setHmCaseRetDshup(hmCaseRetDshup);
			gmCaseSchDashboardForm.setStrName("CASE");

		    return mapping.findForward("GmCaseSchDashboard");
		  }
}
