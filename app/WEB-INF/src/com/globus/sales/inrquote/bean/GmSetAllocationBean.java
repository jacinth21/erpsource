/**
 * 
 */
package com.globus.sales.inrquote.bean;

import java.util.HashMap;

import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.OracleTypes;

/**
 * @author asingaravel
 * This class is used to update confirm info and send notification mail
 *
 */
public class GmSetAllocationBean extends GmBean {
	  Logger log = GmLogger.getInstance(this.getClass().getName());


	  public GmSetAllocationBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmSetAllocationBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }

   /*
    * checkRFSValidation method is used to check RFS validation.
    */
	public String checkRFSValidation(HashMap hmParam) throws AppError {
		
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));

		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		gmDBManager.setPrepareString("gm_pkg_sm_robotics_confim.gm_validate_rfs_quote_main", 6);
		gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strQuoteId);
		gmDBManager.setString(2, "");
		gmDBManager.setString(3, "Y");
		gmDBManager.setString(4, "");
		gmDBManager.setString(5, strUserId);
		gmDBManager.execute();

		String strMsg = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.commit();
		return strMsg;

	}

	/*
	 * updateSetConfirmedbyDtls method is used to update the notification flag in
	 * T5506 table
	 */
	public void updateSetConfirmedbyDtls(HashMap hmParam) throws AppError {

		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));

		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		gmDBManager.setPrepareString("gm_pkg_sm_robotics_set_allocation.gm_upd_set_confirmed_by_dtls", 2);
		gmDBManager.setString(1, strQuoteId);
		gmDBManager.setString(2, strUserId);
		
		gmDBManager.execute();
		gmDBManager.commit();

	}
	/*
	 * updateQuoteCategoryStatus method is used to update the category status as ready in
	 * T5507 table
	 */
	public void updateQuoteCategoryStatus(HashMap hmParam) throws AppError {

		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));

		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		gmDBManager.setPrepareString("gm_pkg_sm_robotics_set_allocation.gm_upd_quote_category_status_main", 2);
		gmDBManager.setString(1, strQuoteId);
		gmDBManager.setString(2, strUserId);
		
		gmDBManager.execute();
		gmDBManager.commit();
	}
	/*
	 * updateCNConfirmFlag method is used to update cn confirm flag in
	 * T5513 table
	 */
	public void updateCNConfirmFlag(HashMap hmParam) throws AppError {

		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));
		String strCnFl = GmCommonClass.parseNull((String) hmParam.get("CNFL"));
		
		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		gmDBManager.setPrepareString("gm_pkg_sm_robotics_set_allocation.gm_upd_cn_confirm_flag", 3);
		gmDBManager.setString(1, strQuoteId);
		gmDBManager.setString(2, strCnFl);
		gmDBManager.setString(3, strUserId);
		
		gmDBManager.execute();
		gmDBManager.commit();
	}
	/*
	 * fetchHeaderDtls method is used to fetch the Quote header details
	 */	
	public HashMap fetchHeaderDtls(HashMap hmParam) {
		
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));
		String strDtFormat = GmCommonClass.parseNull((String) hmParam.get("DTFMT"));
		HashMap hmReturn = new HashMap();
		
		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		gmDBManager.setPrepareString("gm_pkg_sm_robotics_quote_rpt.gm_fetch_header_details", 3);
		gmDBManager.setString(1, strQuoteId);
		gmDBManager.setString(2, strDtFormat);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
	    
		hmReturn =
		        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
		            .getObject(3)));
		gmDBManager.close();
		log.debug("fetchHeaderDtls in bean **** " + hmReturn.size());
		
		return hmReturn;
		
	}
	/*
	* fetchRFSPartsDtls method is used to fetch RFS Error details
	*/
	public HashMap fetchRFSPartsDtls(HashMap hmParam) {
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));
		HashMap hmReturn = new HashMap();
		ArrayList alRFSDtls = new ArrayList();
		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		gmDBManager.setPrepareString("gm_pkg_sm_robotics_notification.gm_fch_rfs_parts_dtls", 2);
		gmDBManager.setString(1, strQuoteId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alRFSDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		hmReturn.put("RFSdetails", alRFSDtls);
		log.debug("fetchRFSPartsDtls in bean **** " + hmReturn.size());
		return hmReturn;
	}
	
	/*
	 * saveOrderJms method is used to create order
	 */
	public void saveOrderJms (HashMap hmParam) throws AppError {
		
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));
		String strCatSeqIds = GmCommonClass.parseNull((String) hmParam.get("CATSEQIDS"));
		String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
		
		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		//GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, gmDataStoreVO);
	    
		gmDBManager.setPrepareString("gm_pkg_sm_robotics_order_txn.gm_order_process_main", 4);
		
		gmDBManager.setString(1, strQuoteId);
		gmDBManager.setString(2, strCatSeqIds);
		gmDBManager.setString(3, strComments);
		gmDBManager.setString(4, strUserId);
		
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
