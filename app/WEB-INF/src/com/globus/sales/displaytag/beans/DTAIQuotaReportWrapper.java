package com.globus.sales.displaytag.beans;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TotalTableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.forms.GmDemandSheetSetupForm;
import com.globus.sales.forms.GmAIQuotaReportForm;

public class DTAIQuotaReportWrapper extends TotalTableDecorator{
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        /**
         * DecimalFormat used to format money in getMoney().
         */
        private DynaBean db ;        
        private String strFieldSalesId ;        
        private String strFieldSalesName;
        private double dblTot;
        private String strTxnType;
        private String strTxnId;
        private String strTsfId;
        private GmAIQuotaReportForm gmAIQuotaReportForm;
        private String strFromPage;
        
        /**
         * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
         */
        public DTAIQuotaReportWrapper()
        {
            super();
        }
        
        public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
    		super.init(pageContext, decorated, tableModel);
    		gmAIQuotaReportForm = (GmAIQuotaReportForm) getPageContext().getAttribute("frmAIQuotaReportForm", PageContext.REQUEST_SCOPE);
    		strFromPage =  gmAIQuotaReportForm.getHfromPage();
        }
        public String addRowClass()
	    {
		 db = 	(DynaBean) this.getCurrentRowObject();
		 dblTot = Double.parseDouble((db.get("TXNQTY")).toString());
		 return dblTot > 0 ? "RightText" : "RightTextRed";
	    }

        public String getFIELDSALES()
        {
            db =    (DynaBean) this.getCurrentRowObject();
            strFieldSalesId   = db.get("DISTID").toString();
            strFieldSalesName   =  GmCommonClass.encodeForJS(db.get("FIELDSALES").toString());
            
            if (strFromPage.equals("REPORT")){
            	return strFieldSalesName;
            }
            else{
            	return "&nbsp;<a class='RightText'  href=\"javascript:fnViewFieldSales("+strFieldSalesId+",'"+strFieldSalesName+"'"+")\" >"  + strFieldSalesName + "</a>" ;
            }
        }
        
        public String getTXNID(){
        	strTxnType  	= GmCommonClass.parseNull(db.get("TXNTYPE").toString());
        	strTxnId 		= GmCommonClass.parseNull((String)db.get("TXNID"));
        	strTsfId = GmCommonClass.parseNull((String)db.get("TSFID"));
        	
        	if(!strTsfId.equals("")){
        		return strTsfId;
        	}
		 	
	        if (strTxnType.equals("20211") )
	        {
	        	return "&nbsp;<a href=javascript:fnPrintConsignVer('" + 
		         strTxnId + "')>"  + strTxnId + "</a> " ; 
	        }
	        else
	        {
	        	return "&nbsp;<a class='RightTextRed' href=javascript:fnPrintRA('" + 
		         strTxnId + "')>"  + strTxnId + "</a>" ; 
			}
        //	return "1";
        }
    }
