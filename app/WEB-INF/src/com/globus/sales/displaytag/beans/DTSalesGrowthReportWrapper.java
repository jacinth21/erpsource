/**
 * FileName : DTSalesGrowthReportWrapper.java Description : Author : sthadeshwar Date : May 21, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.sales.displaytag.beans;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author sthadeshwar
 * 
 */
public class DTSalesGrowthReportWrapper extends TableDecorator {

  GmLogger log = GmLogger.getInstance();
  private DecimalFormat moneyFormat;
  private HashMap hmValues;
  private String strValue;
  DecimalFormat df = new DecimalFormat("#");
  private String strForeColor = "<span style=\"color:red;\">";
  String strSessCurrSymbol = "";
  String strDObj = "0";

  public DTSalesGrowthReportWrapper() {
    super();
    // Formats for displaying dates and money.
    this.moneyFormat = new DecimalFormat("#,###,##0.00"); //$NON-NLS-1$
  }

  public String getID() {
    String strId = "";
    String strFieldTIcon = "";
    String strFieldFSIcon = "";
    String strFieldSalesIcon = "";
    
    String strReportType =
        GmCommonClass.parseNull((String) getPageContext().getRequest().getAttribute("hReportType"));
    hmValues = (HashMap) this.getCurrentRowObject();
    String strType = GmCommonClass.parseNull((String) hmValues.get("RTYPE"));
    Object obj = hmValues.get("ID");
    if (obj != null)
      strId = obj.toString();
    if (strType.equals("VP")) {
      // Image Icon information
      // String strHospIcon =
      // "<img src='images/HospitalIcon.gif' style='cursor:hand' onClick=\"javascript:fnCallHosp(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      // String strGrpIcon =
      // "<img src='images/group._green.gif' style='cursor:hand' onClick=\"javascript:fnCallGrp(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      String strFieldADIcon = "";
      if (strReportType.equals("Historical")) {
        strFieldADIcon =
            "<img src='images/a.gif' title='BY AD' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
                + strType + "','AD','Historical_HIDECOMPANY' )\" id='" + strId + "'>&nbsp;";
        //PMT-33147 Quota Performance Historical
         strFieldFSIcon =
        		"<img src='images/icon-letter.png' title='BY Field Sales' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
                        + strType + "','DIST','Historical_HIDECOMPANY' )\" id='" + strId + "'>&nbsp;";
         strFieldTIcon =
            		 "<img src='images/icon_sales.gif' title='BY Territory' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
            	                + strType + "','TERR','Historical_HIDECOMPANY' )\" id='" + strId + "'>&nbsp;";
       
        strValue = strFieldADIcon +strFieldFSIcon +  strFieldTIcon; //PMT-33147 Quota Performance Historical
      } else {
        strFieldADIcon =
            "<img src='images/a.gif' title='BY AD' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
                + strType + "','AD' )\" id='" + strId + "'>&nbsp;";
         strFieldFSIcon =
            "<img src='/images/icon-letter.png' title='BY Field Sales' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
                + strType + "','DIST' )\" id='" + strId + "'>&nbsp;";
         strFieldTIcon =
            "<img src='images/icon_sales.gif' title='BY Territory'  style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
                + strType + "','TERR' )\" id='" + strId + "'>&nbsp;";
        // String strSpineSIcon =
        // "<img src='images/icon_sales.gif'  style='cursor:hand' onClick=\"javascript:fnCallSpineS(this, '"
        // + strType + "' )\" id='"+strId+"'>&nbsp;";
        // String strPartIcon =
        // "<img src='images/product_icon.jpg'  style='cursor:hand' onClick=\"javascript:fnCallPart(this, '"
        // + strType + "' )\" id='"+strId+"'>&nbsp;";
        // /images/d.gif
        // strValue = strGrpIcon + strHospIcon + strSpineSIcon + strPartIcon;
        strValue = strFieldADIcon + strFieldFSIcon + strFieldTIcon;
      }
    }

    else if (strType.equals("AD") ) {
      // Image Icon information
      // String strHospIcon =
      // "<img src='images/HospitalIcon.gif' style='cursor:hand' onClick=\"javascript:fnCallHosp(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      // String strGrpIcon =
      // "<img src='images/group._green.gif' style='cursor:hand' onClick=\"javascript:fnCallGrp(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
    	
      // String strSpineSIcon =
      // "<img src='images/icon_sales.gif'  style='cursor:hand' onClick=\"javascript:fnCallSpineS(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      // String strFieldFSIcon =
      // "<img src='images/DD.gif'  style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
      // + strType + "','DIST' )\" id='"+strId+"'>&nbsp;";
    //33147 Sales Report
  	if(strReportType.equals("Historical")){
  		strFieldSalesIcon =
    	          "<img src='/images/icon-letter.png' title='BY Field Sales' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
    	              + strType + "','DIST','Historical_HIDECOMPANY' )\" id='" + strId + "'>&nbsp;";
  		strFieldTIcon =
  		  		"<img src='images/icon_sales.gif' title='BY Territory' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
  		  		+ strType + "','TERR' ,'Historical_HIDECOMPANY')\" id='" + strId + "'>&nbsp;";
  		 
  		
  	}else{

  		//33147Sales Report
         strFieldSalesIcon =
   	          "<img src='/images/icon-letter.png' title='BY Field Sales' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
   	              + strType + "','DIST' )\" id='" + strId + "'>&nbsp;";
         strFieldTIcon =
                 "<img src='images/icon_sales.gif' title='BY Territory' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
                     + strType + "','TERR' )\" id='" + strId + "'>&nbsp;";
       
  	}
      // String strPartIcon =
      // "<img src='images/product_icon.jpg'  style='cursor:hand' onClick=\"javascript:fnCallPart(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      // /images/d.gif
      // strValue = strGrpIcon + strHospIcon + strSpineSIcon + strPartIcon;
       
       
      strValue = strFieldSalesIcon + strFieldTIcon;
      } else if (strType.equals("DIST")) {
      // Image Icon information
      // String strHospIcon =
      // "<img src='images/HospitalIcon.gif' style='cursor:hand' onClick=\"javascript:fnCallHosp(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      // String strGrpIcon =
      // "<img src='images/group._green.gif' style='cursor:hand' onClick=\"javascript:fnCallGrp(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
    	 //33147 Sales Report
    	if(strReportType.equals("Historical")){
    		strFieldTIcon =
    		"<img src='images/icon_sales.gif' title='BY Territory' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
    		+ strType + "','TERR' ,'Historical_HIDECOMPANY')\" id='" + strId + "'>&nbsp;";
    	}else{
 
    		//33147Sales Report
    		strFieldTIcon =
          "<img src='images/icon_sales.gif' title='BY Territory' style='cursor:hand' onClick=\"javascript:fnCallFieldSalesBy(this,'"
              + strType + "','TERR' )\" id='" + strId + "'>&nbsp;";
    	}
      // String strSpineSIcon =
      // "<img src='images/icon_sales.gif'  style='cursor:hand' onClick=\"javascript:fnCallSpineS(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      // String strPartIcon =
      // "<img src='images/product_icon.jpg'  style='cursor:hand' onClick=\"javascript:fnCallPart(this, '"
      // + strType + "' )\" id='"+strId+"'>&nbsp;";
      // /images/d.gif
      // strValue = strGrpIcon + strHospIcon + strSpineSIcon + strPartIcon;
      strValue = strFieldTIcon;
    }

    else {
      strValue = "";
    }
    return strValue;
  }

  public Double getSMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTD");
    strSessCurrSymbol =
        GmCommonClass.parseNull((String) getPageContext().getRequest().getAttribute("hCurrSymb"))
            + "&nbsp";
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTD");
    strSessCurrSymbol =
        GmCommonClass.parseNull((String) getPageContext().getRequest().getAttribute("hCurrSymb"))
            + " ";
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }


    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSPMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getPMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

   
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLPMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSMTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getMTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLMTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSMTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }
    return dObj;
  }

  public String getMTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2))
            + "&nbsp;%";

    if (dObj < 0.0) {
      return strForeColor + "(" + strDObj + ")</span>";
    } else {
      return strDObj;
    }
  }

  public String getEXLMTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2)) + " "
            + "%";
  
    if (dObj < 0.0) {
      return "(" + strDObj + ")";
    } else {
      return strDObj;
    }
  }

  public Double getSQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSPQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getPQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLPQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }


    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSQTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getQTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

   
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLQTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSQTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }
    return dObj;
  }

  public String getQTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2))
            + "&nbsp;%";
  
    if (dObj < 0.0) {
      return strForeColor + "(" + strDObj + ")</span>";

    } else {
      return strDObj;
    }
  }

  public String getEXLQTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2)) + " "
            + "%";
   
    if (dObj < 0.0) {
      return "(" + strDObj + ")";
    } else {
      return strDObj;
    }
  }

  public Double getSYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

  
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    
    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSPYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getPYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLPYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("PYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSYTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getYTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLYTDGTHDLR() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTDGTHDLR");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSYTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }
    return dObj;
  }

  public String getYTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2))
            + "&nbsp;%";
 
    if (dObj < 0.0) {
      return strForeColor + "(" + strDObj + ")</span>";

    } else {
      return strDObj;
    }
  }

  public String getEXLYTDGTHPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YTDGTHPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2)) + " "
            + "%";
  
    if (dObj < 0.0) {
      return "(" + strDObj + ")";
    } else {
      return strDObj;
    }
  }

  public Double getSQMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getQMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLQMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSQYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getQYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }


    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLQYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSQQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getQQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }


    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLQQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

  
    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSMQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }
    return dObj;
  }

  public String getMQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2))
            + "&nbsp;%";
 
    if (dObj < 0.0) {
      return strForeColor + "(" + strDObj + ")</span>";
    } else {
      return strDObj;
    }
  }

  public String getEXLMQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("MQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2)) + " "
            + "%";
    
    if (dObj < 0.0) {
      return "(" + strDObj + ")";
    } else {
      return strDObj;
    }
  }

  public Double getSQQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }
    return dObj;
  }

  public String getQQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2))
            + "&nbsp;%";
 
    if (dObj < 0.0) {
      return strForeColor + "(" + strDObj + ")</span>";
    } else {
      return strDObj;
    }
  }

  public String getEXLQQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("QQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2)) + " "
            + "%";
 
    if (dObj < 0.0) {
      return "(" + strDObj + ")";
    } else {
      return strDObj;
    }
  }

  public Double getSYQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }
    return dObj;
  }

  public String getYQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2))
            + "&nbsp;%";
 
    if (dObj < 0.0) {
      return strForeColor + "(" + strDObj + ")</span>";
    } else {
      return strDObj;
    }
  }

  public String getEXLYQPER() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("YQPER");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue());
    }

    dObj = dObj * 100;
    strDObj =
        "" + Math.abs(round(Double.parseDouble(BigDecimal.valueOf(dObj).toPlainString()), 2)) + " "
            + "%";

    if (dObj < 0.0) {
      return "(" + strDObj + ")";
    } else {
      return strDObj;
    }
  }

  public Double getSEMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getEMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
 
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLEMTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EMTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSEQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getEQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
 
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLEQTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EQTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

 
    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public Double getSEYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }
    return dObj;
  }

  public String getEYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

   
    if (dObj < 0) {
      return strForeColor + strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1)
          + ")</span>";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public String getEXLEYTD() {
    Double dObj = new Double(0);
    hmValues = (HashMap) this.getCurrentRowObject();
    Object o = hmValues.get("EYTD");
    if (o != null && o instanceof Double) {
      Double obj = (Double) o;
      dObj = new Double(obj.doubleValue() / 1000);
    }

  
    if (dObj < 0) {
      return strSessCurrSymbol + "(" + this.moneyFormat.format(dObj * -1) + ")";
    } else {
      return strSessCurrSymbol + this.moneyFormat.format(dObj);
    }
  }

  public double round(double value, int places) {
    if (places < 0)
      throw new IllegalArgumentException();

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
  }
}
