package com.globus.sales.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesFilterConditionBean extends GmBean {
  protected String strType;
  protected String strFromMonth;
  protected String strFromYear;
  protected String strToMonth;
  protected String strToYear;
  protected String strTypeConignment = "";
  protected String strTypeCon;
  protected HashMap hmapFromToDate = new HashMap(); // Final value report
                                                    // details
  protected GmCrossTabReport gmCrossReport = new GmCrossTabReport();
  protected String strAction;
  protected String strCondition;
  protected String strSetNumber;
  protected String strDistributorID;
  protected String strAccountID;
  protected String strRepID;
  protected String strPartNumber;
  protected String strADID;
  protected String strVPRegionID;
  protected String strTerritoryID;
  protected String strAccessFilter;
  protected String strCompFilter;
  protected boolean bFilterFlag = false;
  protected String strConType;
  protected String strPeriod;
  protected String strSetType;
  protected String strStatus;
  protected String strRegion;
  protected String strState;
  protected String strBaselineName;
  protected String strTurnsLabel;
  protected String strSalesType;
  protected String str501bFlag;
  protected String strCurrType;
  protected String strCompLangId;
  //PC-3905 Field Inventory Report performance improvement
  private final String strFilterSelect =
      " SELECT DISTINCT AD_ID, AD_NAME, REGION_ID, REGION_NAME, D_ID, D_NAME, D_NAME_EN,VP_ID, VP_NAME , V700.D_ACTIVE_FL, V700.D_COMPID  ";
  private final String strv703 = " FROM V703_REP_MAPPING_DETAIL V700 ";
  // PC-5926 CRM not loading surgeon (in account filter to use V700 view)
  private final String strv700 = " FROM V700_TERRITORY_MAPPING_DETAIL V700 ";
  private final String strFilterDiv = " ,DIVID ";
  private final String strAccFilterSelect =
      " SELECT AC_ID, AD_ID, AD_NAME, REGION_ID, REGION_NAME, D_ID, D_NAME, D_NAME_EN,VP_ID, VP_NAME , V700.D_ACTIVE_FL, V700.D_COMPID  ";



  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmSalesFilterConditionBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmSalesFilterConditionBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public String getSalesFilterClause() {
    StringBuffer sbFilter = new StringBuffer();
    if ("50401".equals(strTypeConignment)) {
      sbFilter.append(" AND  NVL(T501.C901_ORDER_TYPE,-9999) NOT IN ('2524'");
      sbFilter.append(") ");
    }
    /*
     * Fetching the order types from the materialized view instead of 3 different Rule value fetch
     * PMT-20656: Author :gpalani Jun 2018
     */
    sbFilter
        .append(" AND NVL (c901_order_type, -9999) NOT IN (SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP) ");

    return sbFilter.toString();
  }


  /**
   * getAccessFilterClause - This method used to get the V700 information and apply the sales more
   * filter condition
   * 
   * @return String
   */

  public String getAccessFilterClause() {
    log.debug("Enter");
    StringBuffer sbDetailQuery = new StringBuffer();
    sbDetailQuery.append(" , ( " + strFilterSelect.concat(strv703));
    // sbDetailQuery.append(" WHERE V700.D_ACTIVE_FL = 'Y' ");
    if (strAccessFilter != null && !strAccessFilter.equals("")) {
      sbDetailQuery.append(" WHERE " + strAccessFilter);
    }
    sbDetailQuery.append(" ) V700 ");
    log.debug("Exit");
    return sbDetailQuery.toString();
  }

  /**
   * getAccessFilterClauseWithDiv - This method used to get the V700 information with Division ID
   * and apply the sales more filter condition
   * 
   * @return String
   */

  public String getAccessFilterClauseWithDiv() {
    log.debug("Enter");
    StringBuffer sbDetailQuery = new StringBuffer();
    sbDetailQuery.append(" , ( " + strFilterSelect.concat(strFilterDiv).concat(strv703));
    // sbDetailQuery.append(" WHERE V700.D_ACTIVE_FL = 'Y' ");
    if (strAccessFilter != null && !strAccessFilter.equals("")) {
      sbDetailQuery.append(" WHERE " + strAccessFilter);
    }
    sbDetailQuery.append(" ) V700 ");
    log.debug("Exit");
    return sbDetailQuery.toString();
  }

  /**
   * getAccessFilterClauseWithRepID - This method used to get the V700 information with Sales Rep
   * and apply the sales more filter condition
   * 
   * @return String
   */

  public String getAccessFilterClauseWithRepID() {
    log.debug("Enter");
    StringBuffer sbDetailQuery = new StringBuffer();
    sbDetailQuery
        .append(" , (SELECT DISTINCT AD_ID, AD_NAME, REGION_ID, REGION_NAME, D_ID, D_NAME, REP_ID, REP_NAME, D_ACTIVE_FL, TER_NAME  FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
    // sbDetailQuery.append(" WHERE V700.D_ACTIVE_FL = 'Y' ");
    if (strAccessFilter != null && !strAccessFilter.equals("")) {
      sbDetailQuery.append(" WHERE " + strAccessFilter);
    }
    sbDetailQuery.append(" ) V700 ");
    log.debug("Exit");
    return sbDetailQuery.toString();
  }
    /**
   * getAccessFilterForAcc - This method used to get the V700 information with Account
   * and apply the sales more filter condition
   * 
   * @return String
   */
public String getAccessFilterForAcc() {
    
    StringBuffer sbDetailQuery = new StringBuffer();
    sbDetailQuery.append(" , ( " + strAccFilterSelect.concat(strv700));
    if (strAccessFilter != null && !strAccessFilter.equals("")) {
      sbDetailQuery.append(" WHERE " + strAccessFilter);
    }
    sbDetailQuery.append(" ) V700 ");
    
    return sbDetailQuery.toString();
  }

  public void initializeParameters(HashMap hmparam) {
    strAccessFilter = (String) hmparam.get("AccessFilter");
    strCondition = (String) hmparam.get("Condition");
    strSetNumber = (String) hmparam.get("SetNumber");
    strAccountID = (String) hmparam.get("AccountID");
    strRepID = (String) hmparam.get("RepID");
    strPartNumber = (String) hmparam.get("PartNumber");
    strTerritoryID = (String) hmparam.get("TerritoryID");
    strAction = GmCommonClass.parseNull((String) hmparam.get("hAction"));
    strConType = GmCommonClass.parseNull((String) hmparam.get("ConsignmentSummaryType"));
    strPeriod = GmCommonClass.parseNull((String) hmparam.get("ConsignmentPeriod"));
    strSetType = GmCommonClass.parseNull((String) hmparam.get("ConsignmentSetType"));
    strCompFilter = GmCommonClass.parseNull((String) hmparam.get("COMPID"));
    // Filter from consignment overview
    strTypeCon = GmCommonClass.parseNull((String) hmparam.get("ConsignmentType"));

    // Class Level variable
    strType = GmCommonClass.parseNull((String) hmparam.get("Type"));
    strTypeConignment = GmCommonClass.parseNull((String) hmparam.get("ConsignmentType"));

    // From and To Date
    strFromMonth = (String) hmparam.get("FromMonth");
    strFromYear = (String) hmparam.get("FromYear");
    strToMonth = (String) hmparam.get("ToMonth");
    strToYear = (String) hmparam.get("ToYear");

    strDistributorID = (String) hmparam.get("DistributorID");
    strADID = (String) hmparam.get("ADID");
    strVPRegionID = (String) hmparam.get("VPID");
    strStatus = GmCommonClass.parseNull((String) hmparam.get("Status"));

    strBaselineName = GmCommonClass.parseNull((String) hmparam.get("BASELINENAME"));
    strTurnsLabel = GmCommonClass.parseNull((String) hmparam.get("TURNSLABEL"));

    strRegion = GmCommonClass.parseNull((String) hmparam.get("Region"));
    strState = GmCommonClass.parseNull((String) hmparam.get("State"));
    strSalesType = GmCommonClass.parseNull((String) hmparam.get("SalesType"));
    str501bFlag = GmCommonClass.parseNull((String) hmparam.get("T501B_FLAG"));

    log.debug(" strFromMonth " + strFromMonth);
    log.debug(" strFromYear " + strFromYear);

    // Currency Type Base or Original
    strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    strCurrType = strCurrType.equals("") ? "105460" : strCurrType;
    hmparam.put("CURRTYPE", strCurrType);

    // Getting company language id from session.(For multilingual support)
    // Based on the language id, we will show the Japan Master data(Account,Dist,Rep) in English .
    strCompLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    hmparam.put("COMP_LANG_ID", strCompLangId);
  }

  /**
   * getCrossTabFilterCondition - form the filter condition string according to the input selected
   * 
   * @param hmparam
   * @return
   * @throws AppError
   */
  public String getCrossTabFilterCondition(HashMap hmparam) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();
    initializeParameters(hmparam);

    log.debug("sbDetailQuery in get cross tab**" + sbDetailQuery);


    log.debug(" strSalesType is**** ----------- " + strSalesType);
    if (!strSalesType.equals("") && !strSalesType.equals("50302") && !strSalesType.equals("null")
        && !strTypeConignment.equals("50420") && !strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = " + strSalesType);
      log.debug("sbDetailQuery in get cross tab 1**" + sbDetailQuery);

    }


    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("") && !strSetNumber.equals("null")
        && str501bFlag.equals("") && !strSetNumber.equals("0")) {
      sbDetailQuery.append(" AND   T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery
          .append(" ( SELECT C205_PART_NUMBER_ID FROM T207_SET_MASTER T207,  T208_SET_DETAILS T208 ");
      sbDetailQuery.append(" WHERE T207.C207_SET_ID = T208.C207_SET_ID ");
      sbDetailQuery.append(" AND T208.C207_SET_ID IN ('");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("')) ");
      log.debug("sbDetailQuery in get cross tab 2**" + sbDetailQuery);

      bFilterFlag = true;
    } else if (strSetNumber != null && !strSetNumber.equals("") && !strSetNumber.equals("null")
        && str501bFlag.equals("Y") && !strSetNumber.equals("0")) {
      sbDetailQuery.append(" AND   T502.C207_SYSTEM_ID IN ('");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("') ");
      bFilterFlag = true;
      log.debug("sbDetailQuery in get cross tab 3**" + sbDetailQuery);

    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("") && !strAccountID.equals("null")) {
      sbDetailQuery.append(" AND   T501.C704_ACCOUNT_ID = " + strAccountID);
      bFilterFlag = true;
      log.debug("sbDetailQuery in get cross tab 4**" + sbDetailQuery);

    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("") && !strRepID.equals("null")) {
      sbDetailQuery.append(" AND   T501.C703_SALES_REP_ID = " + strRepID);
      bFilterFlag = true;
      log.debug("sbDetailQuery in get cross tab 5**" + sbDetailQuery);

    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("") && !strPartNumber.equals("null")) {
      sbDetailQuery.append(" AND  T502.C205_PART_NUMBER_ID IN ('" + strPartNumber + "') ");
      bFilterFlag = true;
      log.debug("sbDetailQuery in get cross tab 6**" + sbDetailQuery);

    }

    // Territory Filter
    if (strTerritoryID != null && !strTerritoryID.equals("") && !strTerritoryID.equals("null")) {
      sbDetailQuery.append(" AND   V700.TER_ID = ");
      sbDetailQuery.append(strTerritoryID);
      bFilterFlag = true;
      log.debug("sbDetailQuery in get cross tab 7**" + sbDetailQuery);

    }


    // Filter Condition to fetch record based on access code
    if (strAccessFilter != null && !strAccessFilter.toString().equals("")
        && !strAccessFilter.equals("null")) {
      sbDetailQuery.append(" AND ");
      log.debug("sbDetailQuery 241" + sbDetailQuery);
      sbDetailQuery.append(strAccessFilter);
      log.debug("sbDetailQuery in get cross tab 8**" + sbDetailQuery);

    }

    log.debug("strADID is**" + strADID);

    // Area Director filter
    if (strADID != null && !strADID.equals("") && !strADID.equals("null") && !strADID.equals("0")) {

      log.debug("IN strADID is**" + strADID);
      log.debug("sbDetailQuery in get cross tab AD filter**" + sbDetailQuery);

      // PMT-30402 to remove the Region id and placed only AD ID
      if (!(strADID.indexOf("-") == -1)) {
        strADID = strADID.substring(strADID.indexOf("-") + 1, strADID.length());
      }

      sbDetailQuery.append(" AND   T501.C704_ACCOUNT_ID IN ");
      sbDetailQuery.append(" (SELECT  AC_ID FROM V700_TERRITORY_MAPPING_DETAIL WHERE AD_ID = "
          + strADID + ")");
      bFilterFlag = true;
    }

    // Filter Condition to fetch record based on access code
    if (strCondition != null && !strCondition.toString().equals("") && !strCondition.equals("null")) {
      sbDetailQuery.append(" AND ");
      log.debug("sbDetailQuery in 265" + sbDetailQuery);

      sbDetailQuery.append(strCondition);
      log.debug("sbDetailQuery in 268" + sbDetailQuery);
      log.debug("sbDetailQuery in get cross tab 2**" + sbDetailQuery);

    }

    sbDetailQuery.append(getFieldInventoryCrossTabFilterCondition(hmparam).toString());
    log.debug("sbDetailQuery in get cross tab AD filter**" + sbDetailQuery.toString());

    return sbDetailQuery.toString();
  }

  /**
   * getFieldInventoryCrossTabFilterCondition - form the filter condition string according to the
   * input selected
   * 
   * @param hmparam
   * @return
   * @throws AppError
   */
  public StringBuffer getFieldInventoryCrossTabFilterCondition(HashMap hmparam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    String strBaselineMethod = GmCommonClass.parseNull((String) hmparam.get("BASELINEMETHOD"));
    if (strBaselineMethod.equals("BASELINE")) {
      initializeParameters(hmparam);
    }



    // only if status is inactive this filter will be added
    if (strStatus != null && !strStatus.equals("") && !strStatus.equals("0")
        && strStatus.equals("50481")) {
      sbQuery.append(" AND   V700.D_ACTIVE_FL = 'N'");
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("")
        && !strDistributorID.equals("null") && strDistributorID.indexOf(",") == -1) {
      sbQuery.append(" AND   V700.D_ID IN ('" + strDistributorID + "')");
      bFilterFlag = true;
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("")
        && !strDistributorID.equals("null") && strDistributorID.indexOf(",") != -1) {
      sbQuery.append(" AND   V700.D_ID IN (" + strDistributorID + ")");
      bFilterFlag = true;
    }

    if (strBaselineMethod.equals("BASELINE") && strADID != null && !strADID.equals("")
        && !strADID.equals("null") && !strADID.equals("0")) {

      // PMT-27002 to remove the Region id and placed only AD ID
      if (!(strADID.indexOf("-") == -1)) {
        strADID = strADID.substring(strADID.indexOf("-") + 1, strADID.length());
      }
      sbQuery.append(" AND V700.AD_ID = " + strADID);
    }

    if (strBaselineMethod.equals("BASELINE") && strSetNumber != null && !strSetNumber.equals("")
        && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
      sbQuery.append(" AND  V207.C207_SET_ID = '" + strSetNumber + "'");
    }

    if (strVPRegionID != null && strVPRegionID != ("") && !strVPRegionID.equals("null")
        && strVPRegionID != ("0")) {
      sbQuery.append(" AND  VP_ID = '" + strVPRegionID + "'");
    }

    return sbQuery;
  }

  /**
   * getFromToDate - forms the where condition for the selected 'from' and 'to' date
   * 
   * @param intMonth
   * @return
   * @throws AppError
   */

  public String getFromToDate(int intMonth) throws AppError {
    StringBuffer sbWhereCondtion = new StringBuffer();
    // Code to frame the Where Condition

    if (strFromMonth != null && strFromYear != null && !strFromMonth.equals("")
        && !strFromYear.equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
      sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
      sbWhereCondtion.append("','MM/YYYY') ");
    }

    if (strToMonth != null && strToYear != null && !strToMonth.equals("") && !strToYear.equals("")) {
      if (sbWhereCondtion != null) {
        sbWhereCondtion.append(" AND");
      }
      sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
      sbWhereCondtion.append(strToMonth + "/" + strToYear);
      sbWhereCondtion.append("','MM/YYYY')) ");
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') ,-" + intMonth + ")");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    return sbWhereCondtion.toString();
  }

  /**
   * getFrameColumn - This Method is used to frame the SELECT part of the main query depending on
   * Part or distributor
   * 
   * @param String strAction - Action is hardcoded from left menu
   * @return String sbFrameColumnQuery.toString() - Query
   * @exception AppError
   */
  public String getFrameColumn(String strAction) {
    StringBuffer sbFrameColumnQuery = new StringBuffer();
    sbFrameColumnQuery.setLength(0);

    if (strAction.equals("LoadPart")) {
      sbFrameColumnQuery.append(" SELECT   T205.C205_PART_NUMBER_ID ID ");
      sbFrameColumnQuery
          .append(" ,T205.C205_PART_NUMBER_ID  || ' - ' || T205.C205_PART_NUM_DESC NAME ");
    } else if (strAction.equals("LoadDistributor")) {
      sbFrameColumnQuery.append(" SELECT T205.C205_PART_NUMBER_ID || ' - ' || D_ID ID");
      sbFrameColumnQuery.append(" , T205.C205_PART_NUMBER_ID || ' - ' || D_NAME  NAME ");
    }

    else if (strAction.equals("LoadDCONS") || strAction.equals("LoadDistSummary")) {
      sbFrameColumnQuery.append(" SELECT D_ID  ID ");
      sbFrameColumnQuery.append(" ,D_NAME NAME");
    }

    else if (strAction.equals("LoadGCONS")) {
      sbFrameColumnQuery.append("SELECT V207.C207_SET_ID ID ");
      sbFrameColumnQuery.append(",V207.C207_SET_NM NAME, V207.C207_SEQ_NO ");
    } else if (strAction.equals("LoadSummary") || strAction.equals("LoadAD")) {
      sbFrameColumnQuery.append(" SELECT REGION_ID || '-' || NVL(v700.AD_ID,0) ID ");
      sbFrameColumnQuery
          .append(" ,v700.REGION_NAME || ' - (' || NVL(v700.AD_NAME,'*No AD') || ')'  NAME ");
    } else if (strAction.equals("LoadVP")) {
      sbFrameColumnQuery.append(" SELECT v700.VP_ID ID ");
      sbFrameColumnQuery.append(" ,v700.VP_Name  NAME ");
    }
    log.debug("sbFrameColumnQuery :" + sbFrameColumnQuery.toString());
    return sbFrameColumnQuery.toString();
  }


  /**
   * getFrameGroup - This Method is used to frame the GROUPBY part of the main query depending on
   * Part or distributor
   * 
   * @param String strAction - Action is hardcoded from left menu
   * @return String sbFrameGroupQuery() - Query
   * @exception AppError
   */

  public String getFrameGroup(String strAction) {
    StringBuffer sbFrameGroupQuery = new StringBuffer();
    sbFrameGroupQuery.setLength(0);

    if (strAction.equals("LoadPart")) {
      sbFrameGroupQuery.append(" GROUP BY T205.C205_PART_NUMBER_ID ");
      sbFrameGroupQuery.append(" ,T205.C205_PART_NUM_DESC ");
    } else if (strAction.equals("LoadDistributor")) {
      sbFrameGroupQuery.append(" GROUP BY D_ID, D_NAME , T205.C205_PART_NUMBER_ID ");
    } else if (strAction.equals("LoadDCONS") || strAction.equals("LoadDistSummary")) {
      sbFrameGroupQuery.append(" GROUP BY D_ID,D_NAME  ");
    } else if (strAction.equals("LoadGCONS")) {
      sbFrameGroupQuery.append(" GROUP BY V207.C207_SET_ID,");
      sbFrameGroupQuery.append(" V207.C207_SET_NM,V207.C207_SEQ_NO ");
    } else if (strAction.equals("LoadSummary") || strAction.equals("LoadAD")) {
      sbFrameGroupQuery.append(" GROUP BY AD_ID, AD_NAME, REGION_NAME, REGION_ID  ");
    } else if (strAction.equals("LoadVP")) {
      sbFrameGroupQuery.append(" GROUP BY v700.VP_ID, v700.VP_Name  ");
    }
    return sbFrameGroupQuery.toString();
  }

  /**
   * getFrameOrder - This Method is used to frame the ORDERBY part of the main query depending on
   * Part or distributor
   * 
   * @param String strAction - Action is hardcoded from left menu
   * @return String sbFrameGroupQuery() - Query
   * @exception AppError
   */

  public String getFrameOrder(String strAction) {
    StringBuffer sbFrameOrderQuery = new StringBuffer();
    sbFrameOrderQuery.setLength(0);

    if (strAction.equals("LoadPart")) {
      sbFrameOrderQuery.append(" ORDER BY ID ");
      sbFrameOrderQuery.append(" , TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY') ");
    } else if (strAction.equals("LoadDistributor")) {
      sbFrameOrderQuery.append(" ORDER BY NAME ");
      sbFrameOrderQuery.append(" , TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY') ");
    } else if (strAction.equals("LoadDCONS")) {
      sbFrameOrderQuery.append(" ORDER BY NAME ");
      sbFrameOrderQuery.append(" , TO_DATE(R_DATE,'Mon YY') ");
    } else if (strAction.equals("LoadGCONS")) {
      sbFrameOrderQuery.append(" ORDER BY C207_SEQ_NO, NAME ");
      sbFrameOrderQuery.append(" , TO_DATE(R_DATE,'Mon YY') ");
    } else if (strAction.equals("LoadSummary") || strAction.equals("LoadAD")) {
      sbFrameOrderQuery.append(" ORDER BY ID, NAME ");
    } else if (strAction.equals("LoadVP")) {
      sbFrameOrderQuery.append(" ORDER BY v700.VP_ID, v700.VP_Name  ");
    }
    return sbFrameOrderQuery.toString();
  }

  /**
   * calculateAverageMonths - calculate the months between the given from and to date excluding the
   * current month
   * 
   * @param months
   * @return
   * @throws AppError
   */
  public String calculateAverageMonths(int monthRange) throws AppError {
    String strReturn = "";
    HashMap fromToDate =
        GmCalenderOperations.calculateRange(monthRange, strToMonth, strToYear, true);

    strFromMonth = (String) fromToDate.get("FromMonth");
    strFromYear = (String) fromToDate.get("FromYear");
    strToMonth = (String) fromToDate.get("ToMonth");
    strToYear = (String) fromToDate.get("ToYear");

    strReturn = getFromToDate(monthRange);

    return strReturn;
  }

  /**
   * Gets the company filter clause.
   * 
   * @param strCompFilter
   * @return the company filter clause
   * @throws AppError This Method is used to return the Company Value based on value choosen in drop
   *         down
   */
  public String getCompanyFilterClause(String strCompFilter) throws AppError {

    if (strCompFilter.contains(",")) {
      return " COMPID IN (" + strCompFilter + ")";
    } else {
      return " (COMPID = DECODE(" + strCompFilter + ",100803, NULL," + strCompFilter
          + ") OR DECODE(" + strCompFilter + ",100803,1,2) = 1)";
    }
  }

  public String getCompanyFilterClause() throws AppError {
    return (getCompanyFilterClause(strCompFilter));
  }

  /**
   * Gets the order item filter clause.
   * 
   * @param strCurrType - Base or Original
   * @return the order item filter clause
   * @throws AppError
   */
  public String getOrderItemPriceFilterClause(String strCurrType) throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.setLength(0);

    // BASE - 105460, ORIGINAL - 105461
    if (strCurrType.equals("105461")) {
      sbQuery.append("  NVL(SUM( T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY),0 )");
    } else {
      sbQuery.append(" NVL( SUM( T502.C502_CORP_ITEM_PRICE * T502.C502_ITEM_QTY),0)");
    }

    log.debug("OrderItemPrice Query ==" + sbQuery.toString());
    return sbQuery.toString();
  }

  /**
   * Gets the Account Name Filter clause
   * 
   * @param strCompLangId - Company language ID , default English(103097)
   * @return the Account Name filter clause
   * @throws AppError
   */
  public String getAccountNameFilterClause(String strCompLangId, String strColType) throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.setLength(0);

    if (strColType.equals("T704")) {
      if (strCompLangId.equals("103097")) {
        sbQuery.append("  NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM) ");
      } else {
        sbQuery.append(" T704.C704_ACCOUNT_NM ");
      }
    } else if (strColType.equals("V700")) {

      if (strCompLangId.equals("103097")) {
        sbQuery.append("  NVL(V700.ac_name_en,V700.ac_name) ");
      } else {
        sbQuery.append(" V700.ac_name ");
      }

    }
    log.debug("Account Name filter Query ==" + sbQuery.toString());
    return sbQuery.toString();
  }

  /**
   * Gets the Dist name filter clause.
   * 
   * @param strCompLangId - Company language ID , default English(103097)
   * @return the Dist name filter clause
   * @throws AppError
   */
  public String getDistNameFilterClause(String strCompLangId, String strColType) throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.setLength(0);

    if (strColType.equals("T701")) {
      if (strCompLangId.equals("103097")) {
        sbQuery.append("  NVL(T701.C701_DISTRIBUTOR_NAME_EN,T701.C701_DISTRIBUTOR_NAME) ");
      } else {
        sbQuery.append(" T701.C701_DISTRIBUTOR_NAME ");
      }
    } else if (strColType.equals("V700")) {

      if (strCompLangId.equals("103097")) {
        sbQuery.append("  NVL(V700.d_name_en,V700.d_name) ");
      } else {
        sbQuery.append(" V700.d_name ");
      }
    }
    log.debug("Dist Name filter Query ==" + sbQuery.toString());
    return sbQuery.toString();
  }

  /**
   * Gets the SalesRep Name filter clause.
   * 
   * @param strCompLangId - Company language ID , default English(103097)
   * @return the SalesRep Nam filter clause
   * @throws AppError
   */
  public String getSalesRepNameFilterClause(String strCompLangId, String strColType)
      throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.setLength(0);

    if (strColType.equals("T703")) {
      if (strCompLangId.equals("103097")) {
        sbQuery.append("  NVL(T703.C703_SALES_REP_NAME_EN,T703.C703_SALES_REP_NAME) ");
      } else {
        sbQuery.append(" T703.C703_SALES_REP_NAME ");
      }
    } else if (strColType.equals("V700")) {

      if (strCompLangId.equals("103097")) {
        sbQuery.append("  NVL(V700.rep_name_en,V700.rep_name) ");
      } else {
        sbQuery.append(" V700.rep_name ");
      }
    }

    log.debug("Sales Rep Name Filter Query ==" + sbQuery.toString());
    return sbQuery.toString();
  }



}
