package com.globus.sales.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmSalesSystemBean extends GmSalesFilterConditionBean{
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	 
	public RowSetDynaClass fetchQuarterlyQuotaDrillDownByAD(HashMap hmParam) throws AppError{ 
	GmDBManager gmDBManager = new GmDBManager();
	RowSetDynaClass rdQuarterlyQuotaDrillDownByAD = null;
	String strADID = GmCommonClass.parseNull((String)hmParam.get("ADID"));
	String strVPID = GmCommonClass.parseNull((String)hmParam.get("VPID"));
	String strShowDetailsFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("SHOWDETAILSFLAG"));
	String strFieldSalesCheckArray = GmCommonClass.parseNull((String)hmParam.get("CHECKFIELDSALES"));
	String strFromPage = GmCommonClass.parseNull((String)hmParam.get("HFROMPAGE"));
	String strFromQuotaDate = GmCommonClass.parseNull((String)hmParam.get("QUOTAFROMDATE"));
	String strToQuotaDate = GmCommonClass.parseNull((String)hmParam.get("QUOTATODATE"));
	String strPartNumberId = GmCommonClass.parseNull((String)hmParam.get("PARTNUMBERID"));
	String strPartNumberDescription = GmCommonClass.parseNull((String)hmParam.get("PARTNUMBERDESCRIPTION"));
	boolean blShowDetails = false; 
	String strDateFilter = getDateFilter(strFromQuotaDate,strToQuotaDate);
	strAccessFilter = GmCommonClass.parseNull((String)hmParam.get("ACCESSFILTER"));
	
	//strFieldSalesId = strFieldSalesId.concat(strFieldSalesCheckArray);
	
	log.debug(" strShowDetailsFlag " +strShowDetailsFlag + "strFieldSalesCheckArray " +strFieldSalesCheckArray);
	// need to show details when the screen is a report or when the showdetails flag is checked
	if ( strShowDetailsFlag.equals("Y")){
		blShowDetails = true;
	}
	
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT V700.AD_NAME ADNAME, T733.C701_DISTRIBUTOR_ID DISTID, get_distributor_name(T733.C701_DISTRIBUTOR_ID) FIELDSALES ");
    sbQuery.append(" ,  NVL(SUM(T733.C733_TRANSACTION_QTY),0) TXNQTY, NVL(SUM(t733.C733_PRICE)/COUNT(t733.C733_PRICE),0) PRICE ");
    sbQuery.append(" ,  NVL(SUM((T733.C733_TRANSACTION_QTY * T733.C733_PRICE )),0) TOTALPRICE ");

    
    if (blShowDetails ){
    sbQuery.append(" , T205.C205_PART_NUMBER_ID PNUM , T205.C205_PART_NUM_DESC PNUMDESC ");
    sbQuery.append(" , t733.C733_TRANSACTION_ID TXNID, t733.C733_TRANSACTION_DATE TXNDATE ");
    sbQuery.append(" , t733.C901_TRANSACTION_TYPE TXNTYPE , T733.C920_TRANSFER_ID TSFID ");
    }
    
    sbQuery.append(" FROM T733_AI_QUOTA_LOG T733 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" , t205_part_number T205 ");
    sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID =  T733.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T733.C701_DISTRIBUTOR_ID = v700.d_id ");
    sbQuery.append(strDateFilter);
    
    if(!strVPID.equals("")){
	  sbQuery.append(" AND V700.vp_id = '");
	  sbQuery.append(strVPID);
	  sbQuery.append("' ");
    }
    
    if (!strADID.equals("")){
    sbQuery.append(" AND V700.ad_id = '");
    sbQuery.append(strADID);
    sbQuery.append("' ");
    }
    
    if (!strFieldSalesCheckArray.equals("")){
        sbQuery.append(" AND V700.d_id IN('");
        sbQuery.append(strFieldSalesCheckArray);
        sbQuery.append("') ");
        }
    
    if (!strPartNumberId.equals("")){
    	sbQuery.append(" AND T205.C205_PART_NUMBER_ID LIKE '%");
        sbQuery.append(strPartNumberId);
        sbQuery.append("%' ");
    }
    
    if (!strPartNumberDescription.equals("")){
    	sbQuery.append(" AND UPPER(T205.C205_PART_NUM_DESC) LIKE '%");
        sbQuery.append(strPartNumberDescription.toUpperCase());
        sbQuery.append("%' ");
    }
    
    sbQuery.append(" GROUP BY V700.AD_NAME, T733.C701_DISTRIBUTOR_ID ");
    if (blShowDetails){
    sbQuery.append(" , T205.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC, C733_TRANSACTION_ID,  T733.C920_TRANSFER_ID, C733_TRANSACTION_DATE, t733.C901_TRANSACTION_TYPE ");
    }
    
    sbQuery.append(" ORDER BY ADNAME, FIELDSALES ");
	
    log.debug(" Query for getting fetchQuarterlyQuotaDrillDownByAD " +sbQuery.toString());
    
    rdQuarterlyQuotaDrillDownByAD = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return rdQuarterlyQuotaDrillDownByAD;
	}
	
	public String getDateFilter(String strFromDate, String strToDate){
		
		StringBuffer sbQuery = new StringBuffer();
		
		if (strFromDate.equals("") && (strToDate.equals(""))){
			 sbQuery.append(" AND T733.C733_LOAD_DATE >= TRUNC (SYSDATE, 'Q') ");
			 sbQuery.append(" AND T733.C733_LOAD_DATE <=  ADD_MONTHS (TRUNC (SYSDATE, 'Q'), 3) - 1 ");
		}
		
		if (!strFromDate.equals("")){
			sbQuery.append(" AND T733.C733_LOAD_DATE >= TO_DATE('");
			sbQuery.append(strFromDate);
			sbQuery.append("','MM/DD/YYYY')");
		}
		
		if (!strToDate.equals("")){
			sbQuery.append(" AND T733.C733_LOAD_DATE <= TO_DATE('");
			sbQuery.append(strToDate);
			sbQuery.append("','MM/DD/YYYY')");
		}
		
		return sbQuery.toString();
		
	}
	
	/*public RowSetDynaClass fetchQuarterlyQuotaDrillDownByFS(HashMap hmParam) throws AppError{ 
		GmDBManager gmDBManager = new GmDBManager();
		RowSetDynaClass rdQuarterlyQuotaDrillDownByFS = null;
		String strFieldSalesId = GmCommonClass.parseNull((String)hmParam.get("FIELDSALES"));
		
	    StringBuffer sbQuery = new StringBuffer();

	    sbQuery.append(" SELECT T733.C205_PART_NUMBER_ID PNUM , get_partnum_desc(T733.C205_PART_NUMBER_ID) PNUMDESC ");
	    sbQuery.append(" ,  NVL(T733.C733_TRANSACTION_QTY,0) TXNQTY, NVL(t733.C733_PRICE,0) price ");
	    sbQuery.append(" ,  NVL((T733.C733_TRANSACTION_QTY * T733.C733_PRICE ),0) totalprice ");
	    sbQuery.append(" FROM T733_AI_QUOTA_LOG T733, ");
	    sbQuery.append(" (SELECT DISTINCT ad_id, ad_name, region_id, region_name, d_id, d_name "); 
	    sbQuery.append(" 	FROM v700_territory_mapping_detail) v700 ");
	    sbQuery.append(" WHERE T733.C701_DISTRIBUTOR_ID = v700.d_id ");
	    sbQuery.append(" AND T733.C733_LOAD_DATE >= TRUNC (SYSDATE, 'Q') ");
	    sbQuery.append(" AND T733.C733_LOAD_DATE <=  ADD_MONTHS (TRUNC (SYSDATE, 'Q'), 3) - 1 ");
	    sbQuery.append(" AND V700.d_id = '");
	    sbQuery.append(strFieldSalesId);
	    sbQuery.append("' ");
	    sbQuery.append("GROUP BY v700.d_id ");
		
	    log.debug(" Query for getting fetchQuarterlyQuotaDrillDownByAD " +sbQuery.toString());
	    
	    rdQuarterlyQuotaDrillDownByFS = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
	    return rdQuarterlyQuotaDrillDownByFS;
		}
		*/
	
	public ArrayList getDistributorList(String strAccessFilter) throws AppError{
		
		GmDBManager gmDBManager = new GmDBManager();
		ArrayList alDistributorList = new ArrayList();
	    StringBuffer sbDetailQuery = new StringBuffer();
	    
        sbDetailQuery.append(" SELECT DISTINCT  D_ID, D_NAME, V700.D_ACTIVE_FL FROM V700_TERRITORY_MAPPING_DETAIL V700 ");        
        //sbDetailQuery.append(" WHERE V700.D_ACTIVE_FL = 'Y' ");
        if(strAccessFilter != null && !strAccessFilter.equals(""))
        {             
            sbDetailQuery.append(" WHERE "+strAccessFilter);
         }                
        sbDetailQuery.append(" ORDER BY D_NAME ");
	    log.debug(" Query for getting getDistributorList " +sbDetailQuery.toString());
	    
	    alDistributorList = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());
	    return alDistributorList;
	}
}
