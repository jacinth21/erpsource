package com.globus.sales.beans;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;



public class GmInvTagShareOptionRptBean extends GmBean {

	  Logger log = GmLogger.getInstance(this.getClass().getName());
	 
	  public GmInvTagShareOptionRptBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	  
	  public GmInvTagShareOptionRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		   super(gmDataStoreVO);
		   // TODO Auto-generated constructor stub
	   }

	  /**
	   * loadTagShareReportDetails - Fetch the Tag details 
	   * 
	   * @param hmParam
	   * @return String
	   * @exception AppError
	   **/	  
	public String loadTagShareReportDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
	    String strSystem = GmCommonClass.parseNull((String) hmParam.get("SYSTEM"));
	    String strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" Select Json_Arrayagg( Json_Object( 'tagid' Value Tagid,  ");
		sbQuery.append("  'distid' Value Distid, 'distnm' Value Distnm, 'setid' Value Setid, 'setnm' Value Setnm , 'invshare' Value invshare,'invpromoteshare' Value Invpromoteshare ,'invdontshare' Value invdontshare ");
		sbQuery.append(" )ORDER BY distnm,setnm ");
		sbQuery.append(" RETURNING CLOB) FROM ");
		sbQuery.append(" ( SELECT T5010g.C5010_Tag_Id Tagid ,V700.D_Id Distid,V700.D_Name Distnm , T207.C207_Set_Id Setid , T207.C207_Set_Nm Setnm, ");
		sbQuery.append(" (Case When C901_Tag_Share_Options = 111141 Then 1 Else 0 End) invshare, ");
		sbQuery.append(" (Case When C901_Tag_Share_Options = 111140 Then 1 Else 0 End) invpromoteshare, ");
		sbQuery.append(" (case when C901_Tag_Share_Options = 111142 then 1 ELSE 0 end) invdontshare ");
		sbQuery.append(" FROM ");
		sbQuery.append(" T5010g_Tag_System_Locator T5010g, T207_Set_Master t207, ");
			sbQuery.append(" (");
			 if (!strCondition.equals("")) {
			      sbQuery.append(" SELECT DISTINCT AD_NAME Adname,REGION_NAME Regionname, D_Name, D_Id ");
			      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 WHERE ");
			      sbQuery.append(strCondition);
			    }
		sbQuery.append(" )V700");
		sbQuery.append(" WHERE T5010g.C701_Distributor_Id     = V700.D_Id ");
		sbQuery.append(" AND T5010g.C207_Set_Id  = T207.C207_Set_Id(+) ");
		 if (!strSetId.equals("") && !strSetId.equals("0")) {
			  sbQuery.append(" AND T5010g.C207_Set_Id IN (");
			  sbQuery.append(strSetId);
			  sbQuery.append(")");
		      }
		 if (!strSystem.equals("") && !strSystem.equals("0")) {
		      sbQuery.append(" AND T5010g.C207_Set_Sales_System_Id IN (");
		      sbQuery.append(strSystem);
		      sbQuery.append(")");
		}
	    sbQuery.append(" GROUP BY  T5010g.C5010_Tag_Id,V700.D_Id,V700.D_Name,T207.C207_Set_Id,T207.C207_Set_Nm,T5010g.C901_Tag_Share_Options ");
	    sbQuery.append(")");
	    
		log.debug("sbQuery in loadTagShareReportDetails >>>>>" + sbQuery);
		String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

		return strReturn;
	}
	/**
	 *  updateTagShareOption is used to update the Tag Sharing Options
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 */
	public String updateTagShareOptions(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strTagId = "";
	    String strShareOptionId = "";
	    String strMessage = "";
	    String strUserId = "";
	    
	    strTagId = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
	    strShareOptionId = GmCommonClass.parseNull((String) hmParam.get("SHAREOPTIONID"));
	    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    
        gmDBManager.setPrepareString("gm_pkg_inv_tag_share_option_txn.gm_upd_tag_share_options", 4);
	    gmDBManager.registerOutParameter(4,OracleTypes.VARCHAR);
		gmDBManager.setString(1, strTagId);
		gmDBManager.setString(2, strShareOptionId);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		strMessage = GmCommonClass.parseNull(gmDBManager.getString(4));
		log.debug("strMessage  in updateTagShareOptions ==" + strMessage);
		gmDBManager.commit();
		return strMessage;
	}
	
}
