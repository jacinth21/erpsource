
package com.globus.sales.beans;

import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * GmSalesEfficiencyScoreCardRptBean - Contains the methods used for fetch the Efficiency Score By System, Field Sales and Area Director (AD) score card Report screen
 * @author rajan
 *
 */
public class GmSalesEfficiencyScoreCardRptBean extends GmSalesFilterConditionBean { 

/**
 * Code to Initialize the Logger Class.
 */
  Logger log = GmLogger.getInstance(this.getClass().getName());
  
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesEfficiencyScoreCardRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * fetchSystemScoreCardDtls - used to fetch the System score card details
   * @return
   * @throws AppError
   */
  public String fetchSystemScoreCardDtls(HashMap hmParam) throws AppError {
	    initializeParameters(hmParam); 
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	    String queryJSONRecord = "";
	    String strDistId = "";
	    String strADId = "";
	    
	    // Added allowed tag field according to PC-5101: Scorecard DO classification changes
	    String strAllowedTag = "";
	    
	    // to get the Dist and AD id details
	    strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
	    strADId = GmCommonClass.parseNull((String) hmParam.get("ADID"));
	    
	    // Added allowed tag field according to PC-5101: Scorecard DO classification changes
	    strAllowedTag = GmCommonClass.parseZero((String) hmParam.get("ALLOWEDTAG"));
	    
	    // to set the my context values (Allowed Tag) - based on values view returns the data
	    setAllowedTagValuesToContext (strAllowedTag, gmDBManager);
	    
	    // Added allowed tag field to the query according to PC-5101: Scorecard DO classification changes
	    
	    sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT ('id' value CRITICAL_SYSTEM_ID, 'name' value critical_system_name, ");
	    sbQuery.append(" 'system_count' value sys_count, 'missing_count' value sys_count, ");
	    
	    //PBUG -Project score changes added (CN)
	    sbQuery.append(" 'cn_completed' value cn_completed, 'cn_last_2_month' value cn_last2_month, 'cn_tot_proj' value cn_tot_proj, ");
	    // LN
	    sbQuery.append(" 'ln_completed' value ln_completed, 'ln_last_2_month' value ln_last2_month, 'ln_mtd_proj' value ln_mtd_proj, ");
	    sbQuery.append(" 'ln_proj' value ln_projected, 'ln_tot_proj' value ln_total_projected, ");
	    // Exp. usage
	    sbQuery.append(" 'exp_usage_completed' value exp_usage_completed, 'exp_usage_proj' value exp_usage_projected, ");
	    // Procedure
	    sbQuery.append(" 'proc_completed' value proc_completed, 'proc_last_2_month' value proc_last2_month, 'proc_mtd_proj' value proc_mtd_projected, ");
	    sbQuery.append(" 'proc_proj' value proc_projected, 'proc_tot_proj' value proc_total_projected, ");
	    // Efficiency
	    sbQuery.append(" 'effi_completed' value efficiency_completed, 'effi_proj' value efficiency_projected, ");
	    // Efficiency score
	    sbQuery.append(" 'effi_score_completed' value efficiency_score_completed, 'effi_score_proj' value efficiency_score_projected) ");

	    sbQuery.append("  ORDER BY critical_system_name RETURNING CLOB ) ");	     

	    sbQuery.append(" FROM ( ");
	    sbQuery.append(" SELECT  critical_system_id, critical_system_name ,");
	    sbQuery.append(" SUM (sys_count) sys_count, SUM (missing_count) missing_count, SUM (cn_completed) cn_completed ");
	    sbQuery.append(" , SUM (cn_last2_month) cn_last2_month, SUM (cn_tot_proj) cn_tot_proj, SUM (ln_completed) ln_completed ");
	    sbQuery.append(" , SUM (ln_last2_month) ln_last2_month, SUM (ln_mtd_proj) ln_mtd_proj, SUM (ln_projected) ln_projected ");
	    sbQuery.append(" , SUM (ln_total_projected) ln_total_projected, SUM (exp_usage_completed) exp_usage_completed, ");
	    sbQuery.append(" SUM (exp_usage_projected) exp_usage_projected, SUM (proc_completed) proc_completed, SUM (proc_last2_month) ");
	    sbQuery.append(" proc_last2_month, SUM (proc_mtd_projected) proc_mtd_projected, SUM (proc_projected) proc_projected ");
	    sbQuery.append(" , SUM (proc_total_projected) proc_total_projected, SUM (efficiency_completed) efficiency_completed,  ");
	    sbQuery.append(" SUM (efficiency_projected) efficiency_projected, ");
	    sbQuery.append(" CASE WHEN SUM (exp_usage_completed) = 0 THEN 0 ELSE ROUND( SUM (proc_completed)/  SUM (exp_usage_completed) * 100, 1) END efficiency_score_completed ");
	    sbQuery.append(" , CASE WHEN SUM (exp_usage_projected) = 0 THEN 0 ELSE ROUND(SUM (proc_total_projected)/ SUM (exp_usage_projected) * 100, 1) END efficiency_score_projected ");
	    sbQuery.append("  FROM v_score_card_efficiency ");	    
	    sbQuery.append( getAccessFilterClause() );  
	    sbQuery.append("  where DISTRIBUTOR_ID = v700.d_id ");
	    // to remove the empty records
	    //sbQuery.append(" AND sys_count IS NOT NULL ");
	    
		//PC-3626: to hide the inactive data
		sbQuery.append(" AND (sys_count IS NOT NULL OR efficiency_completed <> 0 OR exp_usage_completed <> 0 OR  efficiency_projected <> 0 ) ");

	    // to exclude the On board distributors
	    sbQuery.append(" AND VP_ID <> 4271942 ");
	    
	    // to apply drill down filter
	    if(!strDistId.equals(""))
	    	sbQuery.append(" AND DISTRIBUTOR_ID = "+strDistId);
	    
	    if(!strADId.equals(""))
	    	sbQuery.append(" AND AD_ID = "+strADId);
	    
	    sbQuery.append("  GROUP BY critical_system_id, critical_system_name ) ");   	                    
	    
	    log.debug("fetchSystemScoreCardDtls  Query  " + sbQuery.toString());
	    // to fix the NullPointerException
	    queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecordWithExistingConnection(sbQuery.toString()));
	    gmDBManager.close();
	    return queryJSONRecord;
  }

  /**
   * fetchFieldSalesScoreCardDtls - used to fetch the Field Sales score card details
   * @return
   * @throws AppError
   */
  public String fetchFieldSalesScoreCardDtls(HashMap hmParam) throws AppError {
	    initializeParameters(hmParam);
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	    String queryJSONRecord = "";
	    
	    // to get the System and AD id details
	    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
	    String strADId = GmCommonClass.parseNull((String) hmParam.get("ADID"));
	    
	    // Added allowed tag field according to PC-5101: Scorecard DO classification changes
	    String strAllowedTag = GmCommonClass.parseZero((String) hmParam.get("ALLOWEDTAG"));
	    
	    // to set the my context values (Allowed Tag) - based on values view returns the data
	    setAllowedTagValuesToContext (strAllowedTag, gmDBManager);
	    
	    // Added allowed tag field to the query according to PC-5101: Scorecard DO classification changes
	    
	    sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT ('id' value distributor_id, 'name' value distributor_name, ");
	    sbQuery.append(" 'system_count' value sys_count, 'missing_count' value sys_count, ");
	    
	    //PBUG -Project score changes added (CN)
	    sbQuery.append(" 'cn_completed' value cn_completed, 'cn_last_2_month' value cn_last2_month, 'cn_tot_proj' value cn_tot_proj, ");
	    // LN
	    sbQuery.append(" 'ln_completed' value ln_completed, 'ln_last_2_month' value ln_last2_month, 'ln_mtd_proj' value ln_mtd_proj, ");
	    sbQuery.append(" 'ln_proj' value ln_projected, 'ln_tot_proj' value ln_total_projected, ");
	    // Exp. usage
	    sbQuery.append(" 'exp_usage_completed' value exp_usage_completed, 'exp_usage_proj' value exp_usage_projected, ");
	    // Procedure
	    sbQuery.append(" 'proc_completed' value proc_completed, 'proc_last_2_month' value proc_last2_month, 'proc_mtd_proj' value proc_mtd_projected, ");
	    sbQuery.append(" 'proc_proj' value proc_projected, 'proc_tot_proj' value proc_total_projected, ");
	    // Efficiency
	    sbQuery.append(" 'effi_completed' value efficiency_completed, 'effi_proj' value efficiency_projected, ");
	    // Efficiency score
	    sbQuery.append(" 'effi_score_completed' value efficiency_score_completed, 'effi_score_proj' value efficiency_score_projected) ");

	    sbQuery.append(" ORDER BY distributor_name RETURNING CLOB) ");
	    
	    sbQuery.append(" FROM ( ");
	    sbQuery.append(" SELECT  distributor_id, distributor_name ,");
	    sbQuery.append(" SUM (sys_count) sys_count, SUM (missing_count) missing_count, SUM (cn_completed) cn_completed ");
	    sbQuery.append(" , SUM (cn_last2_month) cn_last2_month, SUM (cn_tot_proj) cn_tot_proj, SUM (ln_completed) ln_completed ");
	    sbQuery.append(" , SUM (ln_last2_month) ln_last2_month, SUM (ln_mtd_proj) ln_mtd_proj, SUM (ln_projected) ln_projected ");
	    sbQuery.append(" , SUM (ln_total_projected) ln_total_projected, SUM (exp_usage_completed) exp_usage_completed, ");
	    sbQuery.append(" SUM (exp_usage_projected) exp_usage_projected, SUM (proc_completed) proc_completed, SUM (proc_last2_month) ");
	    sbQuery.append(" proc_last2_month, SUM (proc_mtd_projected) proc_mtd_projected, SUM (proc_projected) proc_projected ");
	    sbQuery.append(" , SUM (proc_total_projected) proc_total_projected, SUM (efficiency_completed) efficiency_completed,  ");
	    sbQuery.append(" SUM (efficiency_projected) efficiency_projected, ");
	    // to calculate percentage values
	    sbQuery.append(" CASE WHEN SUM (exp_usage_completed) = 0 THEN 0 ELSE ROUND( SUM (proc_completed)/  SUM (exp_usage_completed) * 100, 1) END efficiency_score_completed ");
	    sbQuery.append(" , CASE WHEN SUM (exp_usage_projected) = 0 THEN 0 ELSE ROUND(SUM (proc_total_projected)/ SUM (exp_usage_projected) * 100, 1) END efficiency_score_projected ");
	    sbQuery.append("  FROM v_score_card_efficiency ");	    
	    sbQuery.append( getAccessFilterClause() );     
	    sbQuery.append(" where distributor_id = v700.d_id ");
	    // to remove the empty records
	    //sbQuery.append(" AND sys_count IS NOT NULL ");
		
	  //PC-3626: to hide the inactive data
	    sbQuery.append(" AND (sys_count IS NOT NULL OR efficiency_completed <> 0 OR exp_usage_completed <> 0 OR  efficiency_projected <> 0 ) ");

	    // to exclude the On board distributors
	    sbQuery.append(" AND v700.VP_ID <> 4271942 ");
	    
	 // to apply drill down filter
	    if(!strSystemId.equals(""))
	    	sbQuery.append(" AND critical_system_id = '"+strSystemId+"' ");
	    
	    if(!strADId.equals(""))
	    	sbQuery.append(" AND ad_id = "+strADId);
	    
	    sbQuery.append(" GROUP BY distributor_id, distributor_name ) ");
	    
	    log.debug("fetxchFieldSalesScoreCardDtls  Query  " + sbQuery.toString());
	    queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecordWithExistingConnection(sbQuery.toString()));
	    gmDBManager.close();
	    return queryJSONRecord;
     }
  
  /**
   * fetchADScoreCardDtls - used to fetch the Area Director (AD) score card details
   * @return
   * @throws AppError
   */
  public String fetchADScoreCardDtls(HashMap hmParam) throws AppError {
	    initializeParameters(hmParam);
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	    String queryJSONRecord = "";
	    
	    // to get the System and AD id details
	    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
	    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
	    
	    // Added allowed tag field according to PC-5101: Scorecard DO classification changes
	    String strAllowedTag = GmCommonClass.parseZero((String) hmParam.get("ALLOWEDTAG"));
	    
	    // to set the my context values (Allowed Tag) - based on values view returns the data
	    setAllowedTagValuesToContext (strAllowedTag, gmDBManager);
	    
	    // Added allowed tag field to the query according to PC-5101: Scorecard DO classification changes
	    sbQuery.append(" SELECT JSON_ARRAYAGG ( JSON_OBJECT ('id' value ad_id, 'name' value ad_name, ");
	    sbQuery.append(" 'system_count' value sys_count, 'missing_count' value sys_count, ");
	    
	    //PBUG -Project score changes added (CN)
	    sbQuery.append(" 'cn_completed' value cn_completed, 'cn_last_2_month' value cn_last2_month, 'cn_tot_proj' value cn_tot_proj, ");
	    // LN
	    sbQuery.append(" 'ln_completed' value ln_completed, 'ln_last_2_month' value ln_last2_month, 'ln_mtd_proj' value ln_mtd_proj, ");
	    sbQuery.append(" 'ln_proj' value ln_projected, 'ln_tot_proj' value ln_total_projected, ");
	    // Exp. usage
	    sbQuery.append(" 'exp_usage_completed' value exp_usage_completed, 'exp_usage_proj' value exp_usage_projected, ");
	    // Procedure
	    sbQuery.append(" 'proc_completed' value proc_completed, 'proc_last_2_month' value proc_last2_month, 'proc_mtd_proj' value proc_mtd_projected, ");
	    sbQuery.append(" 'proc_proj' value proc_projected, 'proc_tot_proj' value proc_total_projected, ");
	    // Efficiency
	    sbQuery.append(" 'effi_completed' value efficiency_completed, 'effi_proj' value efficiency_projected, ");
	    // Efficiency score
	    sbQuery.append(" 'effi_score_completed' value efficiency_score_completed, 'effi_score_proj' value efficiency_score_projected) ");

	    
	    sbQuery.append("  ORDER BY ad_name RETURNING CLOB) ");
	  
	    sbQuery.append(" FROM ( ");
	    sbQuery.append(" SELECT  v700.ad_id, v700.ad_name ,");
	    sbQuery.append(" SUM (sys_count) sys_count, SUM (missing_count) missing_count, SUM (cn_completed) cn_completed ");
	    sbQuery.append(" , SUM (cn_last2_month) cn_last2_month, SUM (cn_tot_proj) cn_tot_proj, SUM (ln_completed) ln_completed ");
	    sbQuery.append(" , SUM (ln_last2_month) ln_last2_month, SUM (ln_mtd_proj) ln_mtd_proj, SUM (ln_projected) ln_projected ");
	    sbQuery.append(" , SUM (ln_total_projected) ln_total_projected, SUM (exp_usage_completed) exp_usage_completed, ");
	    sbQuery.append(" SUM (exp_usage_projected) exp_usage_projected, SUM (proc_completed) proc_completed, SUM (proc_last2_month) ");
	    sbQuery.append(" proc_last2_month, SUM (proc_mtd_projected) proc_mtd_projected, SUM (proc_projected) proc_projected ");
	    sbQuery.append(" , SUM (proc_total_projected) proc_total_projected, SUM (efficiency_completed) efficiency_completed,  ");
	    sbQuery.append(" SUM (efficiency_projected) efficiency_projected, ");
	    // to calculate percentage values
	    sbQuery.append(" CASE WHEN SUM (exp_usage_completed) = 0 THEN 0 ELSE ROUND( SUM (proc_completed)/  SUM (exp_usage_completed) * 100, 1) END efficiency_score_completed ");
	    sbQuery.append(" , CASE WHEN SUM (exp_usage_projected) = 0 THEN 0 ELSE ROUND(SUM (proc_total_projected)/ SUM (exp_usage_projected) * 100, 1) END efficiency_score_projected ");
	    
	    sbQuery.append("  FROM v_score_card_efficiency ");	    
	    sbQuery.append( getAccessFilterClause() );     
	    sbQuery.append(" where distributor_id = v700.d_id ");
	    // to remove the empty records
	    //sbQuery.append(" AND sys_count IS NOT NULL ");
	    
	    //PC-3626: to hide the inactive data
	    sbQuery.append(" AND (sys_count IS NOT NULL OR efficiency_completed <> 0 OR exp_usage_completed <> 0 OR  efficiency_projected <> 0 ) ");

	    // to exclude the On board distributors
	    sbQuery.append(" AND v700.VP_ID <> 4271942 ");
	    
	    // to apply drill down filter
	    if(!strSystemId.equals(""))
	    	sbQuery.append(" AND critical_system_id = '"+strSystemId +"' ");
	    
	    if(!strDistId.equals(""))
	    	sbQuery.append(" AND distributor_id = "+strDistId);
	    
	    sbQuery.append( "GROUP BY v700.ad_id, v700.ad_name ) " );
	    
	    log.debug("fetchADScoreCardDtls  Query  " + sbQuery.toString());
	    queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecordWithExistingConnection(sbQuery.toString()));
	    gmDBManager.close();
	    return queryJSONRecord;
   }
  
  /**
   * fetchOverallSummaryDtls - used to fetch the System score card details
   * @return
   * @throws AppError
   */
  public String fetchOverallSummaryDtls (HashMap hmParam) throws AppError {
	    initializeParameters(hmParam); 
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	    String queryJSONRecord = "";
	    String strDistId = "";
	    String strADId = "";
	    
	    // to get the Dist and AD id details
	    strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
	    strADId = GmCommonClass.parseNull((String) hmParam.get("ADID"));
	    
	    // Added allowed tag field according to PC-5101: Scorecard DO classification changes
	    String strAllowedTag = GmCommonClass.parseZero((String) hmParam.get("ALLOWEDTAG"));
	    
	    // to set the my context values (Allowed Tag) - based on values view returns the data
	    setAllowedTagValuesToContext (strAllowedTag, gmDBManager);
	    
	    sbQuery.append(" SELECT JSON_OBJECT (");
	    sbQuery.append(" 'system_count' value sys_count, 'missing_count' value sys_count, ");
	    
	    //PBUG -Project score changes added (CN)
	    sbQuery.append(" 'cn_completed' value cn_completed, 'cn_last_2_month' value cn_last2_month, 'cn_tot_proj' value cn_tot_proj, ");
	    // LN
	    sbQuery.append(" 'ln_completed' value ln_completed, 'ln_last_2_month' value ln_last2_month, 'ln_mtd_proj' value ln_mtd_proj, ");
	    sbQuery.append(" 'ln_proj' value ln_projected, 'ln_tot_proj' value ln_total_projected, ");
	    // Exp. usage
	    sbQuery.append(" 'exp_usage_completed' value exp_usage_completed, 'exp_usage_proj' value exp_usage_projected, ");
	    // Procedure
	    sbQuery.append(" 'proc_completed' value proc_completed, 'proc_last_2_month' value proc_last2_month, 'proc_mtd_proj' value proc_mtd_projected, ");
	    sbQuery.append(" 'proc_proj' value proc_projected, 'proc_tot_proj' value proc_total_projected, ");
	    // Efficiency
	    sbQuery.append(" 'effi_completed' value efficiency_completed, 'effi_proj' value efficiency_projected, ");
	    // Efficiency score
	    sbQuery.append(" 'effi_score_completed' value efficiency_score_completed, 'effi_score_proj' value efficiency_score_projected RETURNING CLOB) ");

	    sbQuery.append(" FROM ( ");
	    sbQuery.append(" SELECT ");
	    sbQuery.append(" SUM (sys_count) sys_count, SUM (missing_count) missing_count, SUM (cn_completed) cn_completed ");
	    sbQuery.append(" , SUM (cn_last2_month) cn_last2_month, SUM (cn_tot_proj) cn_tot_proj, SUM (ln_completed) ln_completed ");
	    sbQuery.append(" , SUM (ln_last2_month) ln_last2_month, SUM (ln_mtd_proj) ln_mtd_proj, SUM (ln_projected) ln_projected ");
	    sbQuery.append(" , SUM (ln_total_projected) ln_total_projected, SUM (exp_usage_completed) exp_usage_completed, ");
	    sbQuery.append(" SUM (exp_usage_projected) exp_usage_projected, SUM (proc_completed) proc_completed, SUM (proc_last2_month) ");
	    sbQuery.append(" proc_last2_month, SUM (proc_mtd_projected) proc_mtd_projected, SUM (proc_projected) proc_projected ");
	    sbQuery.append(" , SUM (proc_total_projected) proc_total_projected, SUM (efficiency_completed) efficiency_completed,  ");
	    sbQuery.append(" SUM (efficiency_projected) efficiency_projected, ");
	    sbQuery.append(" CASE WHEN SUM (exp_usage_completed) = 0 THEN 0 ELSE ROUND( SUM (proc_completed)/  SUM (exp_usage_completed) * 100, 1) END efficiency_score_completed ");
	    sbQuery.append(" , CASE WHEN SUM (exp_usage_projected) = 0 THEN 0 ELSE ROUND(SUM (proc_total_projected)/ SUM (exp_usage_projected) * 100, 1) END efficiency_score_projected ");
	    sbQuery.append("  FROM v_score_card_efficiency ");	    
	    sbQuery.append( getAccessFilterClause() );  
	    sbQuery.append("  where DISTRIBUTOR_ID = v700.d_id ");
	    // to remove the empty records
	    //sbQuery.append(" AND sys_count IS NOT NULL ");
	    // to exclude the On board distributors
	    sbQuery.append(" AND VP_ID <> 4271942 ");
	    
	    // to apply drill down filter
	    if(!strDistId.equals(""))
	    	sbQuery.append(" AND DISTRIBUTOR_ID = "+strDistId);
	    
	    if(!strADId.equals(""))
	    	sbQuery.append(" AND AD_ID = "+strADId);
	    
	    sbQuery.append(" ) ");   	                    
	    
	    log.debug("fetchOverallSummaryDtls  Query  " + sbQuery.toString());
	    // to fix the NullPointerException
	    queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecordWithExistingConnection(sbQuery.toString()));
	    gmDBManager.close();
	    return queryJSONRecord;
}
  
	/**
	 * setAllowedTagValuesToContext - This method used to set the Allowed Tag values
	 * to context
	 * 
	 * @param strAllowedTagVal
	 * @param gmDBManager
	 * @throws AppError
	 * 
	 */
	
	public void setAllowedTagValuesToContext(String strAllowedTagVal, GmDBManager gmDBManager) throws AppError {
		
		String strApplyAllowedTag = strAllowedTagVal;
	
		// if user select all - then to show all the records
		if (strAllowedTagVal.equals("0")) {
			strApplyAllowedTag = "Y,N";
		}
		// my_context.set_my_inlist_ctx ('Y,N');
		// my_context.set_my_inlist_ctx ('Y');
		// my_context.set_my_inlist_ctx ('N');
	
		gmDBManager.setPrepareString("my_context.set_my_inlist_ctx", 1);
		gmDBManager.setString(1, strApplyAllowedTag);
		gmDBManager.execute();
		// return gmDBManager;
	}

  
  }