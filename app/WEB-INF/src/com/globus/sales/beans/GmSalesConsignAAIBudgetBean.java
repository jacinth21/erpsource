package com.globus.sales.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesConsignAAIBudgetBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  public GmSalesConsignAAIBudgetBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesConsignAAIBudgetBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * reportConsignmentSummary - This Method is used to fetch consignment summary report Will have
   * additional Filter
   * 
   * @return HashMap
   * @exception AppError
   */

  public ArrayList reportConsignAAIBudget(String strtype, HashMap hmParam) throws AppError {
    String strSalesType = GmCommonClass.parseNull((String) hmParam.get("SalesType"));
    ArrayList alReturn = new ArrayList();
    initializeParameters(hmParam);
    log.debug(" Type is " + strSalesType);
    // Only for Consignment
    if (strSalesType.equals("50300")) {
      alReturn = reportConsignmentAAIBudget(strtype, hmParam);
    }
    return alReturn;

  }


  public ArrayList reportConsignmentAAIBudget(String strtype, HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alAAISummary = new ArrayList();

    StringBuffer sbHeaderQuery = getConsignmentAAIBudget(strtype, hmParam);
    // log.debug("AD ******" + strADID );
    log.debug("By Dist ******" + sbHeaderQuery.toString());
    alAAISummary = gmDBManager.queryMultipleRecords(sbHeaderQuery.toString());

    return alAAISummary;
  }



  public StringBuffer getConsignmentAAIBudget(String strtype, HashMap hmParam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer strFilterCondition = getFieldInventoryCrossTabFilterCondition(hmParam);
    String strStdPercent =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("FIELD_INV_STD", "FIELD_INV_FORMULA"));
    String strAAIBudget =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("FIELD_INV_AAI_BUDGET",
            "FIELD_INV_FORMULA"));
    log.debug("strStdPercent" + strStdPercent);
    log.debug("strAAIBudget" + strAAIBudget);

    if (strtype.equals("AD")) {


      sbHeaderQuery
          .append(" SELECT ID,  NAME, TOTAL_STD, TOTAL_AAI_BUDJET,  AAI, (AAI/TOTAL_AAI_BUDJET *100 ) AS AAI_PERCENT ");

      sbHeaderQuery.append("   FROM  (SELECT ID, NAME, TOTAL_STD, TOTAL_AAI_BUDJET, ");

      sbHeaderQuery
          .append(" SUM(AAI) AAI FROM (SELECT  ID, NAME, TOTAL_STD,TOTAL_AAI_BUDJET, AAI FROM ( ");

      sbHeaderQuery.append(" SELECT  ID, NAME, TOTAL_STD,TOTAL_AAI_BUDJET, ");
      sbHeaderQuery.append(" AAI_PERCENT , AAI FROM (SELECT  ID, NAME,STD,  ");
      sbHeaderQuery.append(" SUM((AAI / AAI_BUDGET)*100)  OVER ");
      sbHeaderQuery.append(" (PARTITION BY ID ORDER BY 1)AAI_PERCENT , ");
      sbHeaderQuery.append(" SUM (STD) OVER (PARTITION BY ID ORDER BY 1)");
      sbHeaderQuery.append(" TOTAL_STD ,SUM (AAI_BUDGET) OVER ");
      sbHeaderQuery.append(" (PARTITION BY ID ORDER BY 1) ");
      sbHeaderQuery
          .append(" TOTAL_AAI_BUDJET ,AAI,ROW_NUMBER () OVER (PARTITION BY ID ORDER BY 1) AD_RN FROM ");
      sbHeaderQuery.append(" (SELECT  ID, NAME,STD,TEMP,(CASE WHEN TEMP <");
      sbHeaderQuery.append(strAAIBudget + " THEN " + strAAIBudget);
      sbHeaderQuery
          .append(" ELSE TEMP END) AAI_BUDGET, TOTAL_VAL, AAI FROM(SELECT  ID, NAME,STD,(std * ");
      sbHeaderQuery.append(strStdPercent + " ) temp, TOTAL_VAL, AAI ");
      sbHeaderQuery
          .append(" FROM(SELECT  ID, NAME,(TOTAL_VAL - STD) std,TOTAL_VAL, AAI FROM(SELECT  ID, NAME , ");

      sbHeaderQuery
          .append(" SUM (  CASE  WHEN TYP IN ('AAI', 'AAS')  THEN VTOTAL  END) OVER (PARTITION BY DIST_ID, DIST_NAME ORDER BY DIST_ID) STD, SUM (  CASE  WHEN TYP = 'AAI' ");
      sbHeaderQuery
          .append(" THEN VTOTAL END) OVER (PARTITION BY DIST_ID, DIST_NAME ORDER BY DIST_ID) AAI, SUM (VTOTAL_VALUE) OVER    (PARTITION BY DIST_ID, DIST_NAME) TOTAL_VAL, ROW_NUMBER () OVER (PARTITION BY DIST_ID, ");
      sbHeaderQuery.append(" DIST_NAME ORDER BY 1) RN  FROM  ( ");


      sbHeaderQuery
          .append(" SELECT D_ID DIST_ID,  REGION_ID || '-' || V700.AD_ID ID, v700.VP_ID VP_ID, V700.VP_NAME VP_NM ");
      sbHeaderQuery.append(", V700.ad_NAME NAME , D_NAME DIST_NAME "); // ad_NM AD


    }


    else if (strtype.equals("VP"))

    {

      sbHeaderQuery
          .append(" SELECT ID,  NAME, TOTAL_STD, TOTAL_AAI_BUDJET,  AAI, (AAI/TOTAL_AAI_BUDJET *100 ) AS AAI_PERCENT ");

      sbHeaderQuery.append("   FROM  (SELECT ID, NAME, TOTAL_STD, TOTAL_AAI_BUDJET, ");

      sbHeaderQuery
          .append(" SUM(AAI) AAI FROM (SELECT  ID, NAME, TOTAL_STD,TOTAL_AAI_BUDJET, AAI FROM ( ");

      sbHeaderQuery.append(" SELECT  ID,NAME ,TOTAL_STD  ,TOTAL_AAI_BUDJET ");
      sbHeaderQuery.append(" , AAI_PERCENT, AAI FROM ");
      sbHeaderQuery.append(" ( SELECT  ID, NAME, STD, AAI  ,  AAI_BUDGET,");
      sbHeaderQuery.append("  SUM((AAI / AAI_BUDGET)*100)  OVER  ");
      sbHeaderQuery.append(" (PARTITION BY ID ORDER BY 1)AAI_PERCENT,");
      sbHeaderQuery.append(" SUM (STD) OVER (PARTITION BY ID ORDER BY 1)");
      sbHeaderQuery.append(" TOTAL_STD , SUM (AAI_BUDGET) OVER (PARTITION BY ID ORDER BY 1) ");
      sbHeaderQuery
          .append(" TOTAL_AAI_BUDJET  , ROW_NUMBER () OVER (PARTITION BY ID ORDER BY 1) VP_RN FROM  ");
      sbHeaderQuery.append(" (SELECT  ID, NAME,STD,TEMP,(CASE WHEN TEMP <");

      sbHeaderQuery.append(strAAIBudget + " THEN " + strAAIBudget);

      sbHeaderQuery
          .append(" ELSE TEMP END) AAI_BUDGET, TOTAL_VAL, AAI FROM(SELECT  ID, NAME,STD,(std * ");
      sbHeaderQuery.append(strStdPercent + " ) temp, TOTAL_VAL, AAI ");

      sbHeaderQuery
          .append(" FROM  (SELECT ID,NAME,  (TOTAL_VAL - STD) std , TOTAL_VAL, AAI FROM  (SELECT  ID,NAME , ");

      sbHeaderQuery
          .append(" SUM (  CASE  WHEN TYP IN ('AAI', 'AAS')  THEN VTOTAL  END) OVER (PARTITION BY DIST_ID, DIST_NAME ORDER BY DIST_ID) STD, SUM (  CASE  WHEN TYP = 'AAI' ");
      sbHeaderQuery
          .append(" THEN VTOTAL END) OVER (PARTITION BY DIST_ID, DIST_NAME ORDER BY DIST_ID) AAI, SUM (VTOTAL_VALUE) OVER    (PARTITION BY DIST_ID, DIST_NAME) TOTAL_VAL, ROW_NUMBER () OVER (PARTITION BY DIST_ID, ");
      sbHeaderQuery.append(" DIST_NAME ORDER BY 1) RN  FROM  ( ");
      sbHeaderQuery
          .append(" SELECT D_ID DIST_ID, V700.AD_ID AD, v700.VP_ID ID, V700.VP_NAME NAME ");
      sbHeaderQuery.append(", V700.ad_NAME ad_NM, D_NAME DIST_NAME ");

    }

    else if (strtype.equals("FS"))

    {
      sbHeaderQuery.append(" SELECT  ID, NAME , TOTAL_STD , ");
      sbHeaderQuery.append(" TOTAL_AAI_BUDJET,AAI_PERCENT, AAI   FROM ");
      sbHeaderQuery
          .append(" ( SELECT  ID, NAME  , STD , AAI_BUDGET, ((AAI / AAI_BUDGET)*100) AAI_PERCENT ");
      sbHeaderQuery.append(", SUM (STD) OVER ");
      sbHeaderQuery.append(" (PARTITION BY ID ORDER BY 1) TOTAL_STD ,");
      sbHeaderQuery.append(" SUM (AAI_BUDGET) OVER ");
      sbHeaderQuery
          .append("(PARTITION BY ID,NAME ORDER BY 1)TOTAL_AAI_BUDJET , AAI , ROW_NUMBER ()");
      sbHeaderQuery.append(" OVER (PARTITION BY ID,NAME  ORDER BY 1) ");
      sbHeaderQuery.append(" FIELD_RN    FROM  (SELECT  ID, NAME, STD , ");
      sbHeaderQuery.append(" TEMP, (CASE  WHEN TEMP < ");
      sbHeaderQuery.append(strAAIBudget + " THEN " + strAAIBudget);
      sbHeaderQuery.append(" ELSE TEMP END) AAI_BUDGET, TOTAL_VAL, AAI  FROM ");
      sbHeaderQuery.append(" (SELECT  ID, NAME , STD , (std * ");
      sbHeaderQuery.append(strStdPercent + ") temp, TOTAL_VAL, ");
      sbHeaderQuery
          .append(" AAI  FROM (SELECT  ID, NAME , (TOTAL_VAL - STD) std , TOTAL_VAL, AAI  FROM (SELECT  ID, NAME  , ");
      sbHeaderQuery
          .append(" SUM (  CASE  WHEN TYP IN ('AAI', 'AAS')  THEN VTOTAL  END) OVER (PARTITION BY ID, NAME ORDER BY ID) STD, SUM (  CASE  WHEN TYP = 'AAI' ");
      sbHeaderQuery
          .append(" THEN VTOTAL END) OVER (PARTITION BY ID, NAME ORDER BY ID) AAI, SUM (VTOTAL_VALUE) OVER    (PARTITION BY ID,NAME) TOTAL_VAL, ROW_NUMBER () OVER (PARTITION BY ID, ");
      sbHeaderQuery.append(" NAME ORDER BY 1) RN  FROM  ( ");


      sbHeaderQuery.append(" SELECT D_ID ID, V700.AD_ID AD, v700.VP_ID VP_ID, V700.VP_NAME VP_NM ");
      sbHeaderQuery.append(", V700.ad_NAME ad_NM, D_NAME NAME ");

    }



    sbHeaderQuery
        .append(" , DECODE(C901_CUSTOM_HIERARCHY,20700,'PERCENT',GET_CODE_NAME_ALT(v207.C901_CUSTOM_HIERARCHY)) TYP ");
    sbHeaderQuery.append(" , DECODE(C901_CUSTOM_HIERARCHY ,20700, ");
    sbHeaderQuery.append(" 100 - trunc( ratio_to_report(SUM ( T730.C730_TOTAL_COST)) ");
    sbHeaderQuery.append(" over(PARTITION BY D_ID ");
    sbHeaderQuery.append(" ) * 100, 2) ");
    sbHeaderQuery.append(" , NVL (SUM ( t730.c730_total_cost ");
    sbHeaderQuery.append("), 0)) vtotal ");
    sbHeaderQuery.append(" , SUM (DECODE (V207.C901_LINK_TYPE, 20100, 1, 0)) VCOUNT, NVL (SUM ( ");
    sbHeaderQuery.append(" T730.C730_TOTAL_COST), 0) VTOTAL_VALUE");
    sbHeaderQuery.append("  FROM t730_virtual_consignment t730 ");
    sbHeaderQuery.append(" , v207a_set_consign_link v207 ");
    log.debug("strFilterCondition in AAI" + strFilterCondition);
    sbHeaderQuery.append(getAccessFilterClause());
    log.debug("getAccessFilterClause in Field Sales" + getAccessFilterClause());
    sbHeaderQuery.append("  WHERE T730.C207_SET_ID =  V207.C207_ACTUAL_SET_ID ");
    sbHeaderQuery.append(" AND  V700.D_ID = T730.C701_DISTRIBUTOR_ID ");
    sbHeaderQuery.append(" AND  V700.D_ACTIVE_FL='Y' ");
    sbHeaderQuery.append(strFilterCondition);
    sbHeaderQuery
        .append(" GROUP BY D_ID, ad_id, V700.ad_NAME,  V700.AD_ID , D_NAME, v207.C901_CUSTOM_HIERARCHY, v700.VP_ID, V700.VP_NAME ");
    if (strtype.equals("AD")) {
      sbHeaderQuery.append(", v700.region_id ");
    }

    sbHeaderQuery.append(") )");
    sbHeaderQuery.append(" WHERE RN = 1 ) ) ) )");

    /*
     * Row num is used since we are applying the formula by pulling all the field sales data mapped
     * for the AD and VP. So without using Rownum will give exact fetch error.All the rows will have
     * the same output, so by using the Row num will not change the data
     */


    if (strtype.equals("AD")) {

      sbHeaderQuery.append("  ) ) GROUP BY ID,NAME,TOTAL_STD,TOTAL_AAI_BUDJET) ");

    }

    else if (strtype.equals("VP")) {

      sbHeaderQuery.append("  ) ) GROUP BY ID,NAME,TOTAL_STD,TOTAL_AAI_BUDJET) ");

    }

    if (strtype.equals("FS")) {

      sbHeaderQuery.append(" WHERE FIELD_RN = 1 ");

    }
    log.debug("sbHeaderQuery is" + sbHeaderQuery);
    return sbHeaderQuery;
  }


}
