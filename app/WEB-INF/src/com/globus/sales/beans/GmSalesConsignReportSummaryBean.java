/**
 * 
 */
package com.globus.sales.beans;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonSort;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 */
public class GmSalesConsignReportSummaryBean extends GmSalesConsignReportBean
{
    /**
     * reportConsignmentSummary - This Method is used to fetch consignment summary report Will have additional Filter
     * 
     * @return HashMap
     * @exception AppError
     */
    private boolean LOADSETSUMMARY   = false;
    private boolean LOADSALESSUMMARY = false;

	  public GmSalesConsignReportSummaryBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmSalesConsignReportSummaryBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
    public HashMap reportConsignmentSummary(HashMap hmParam) throws AppError
    {
        StringBuffer sbHeaderQuery = new StringBuffer();
        String strFilterCondition = getCrossTabFilterCondition(hmParam);
        // Main Header SQL (Fetch header value)
        sbHeaderQuery.append(" SELECT DISTINCT T207.C207_SET_ID SID, C207_SEQ_NO  ");
        sbHeaderQuery.append(" FROM T207_SET_MASTER  T207  ");
        sbHeaderQuery.append(" WHERE T207.C901_SET_GRP_TYPE  = 1600 ");
        sbHeaderQuery.append(" AND T207.C207_VOID_FL IS NULL  ");
        sbHeaderQuery.append(" AND T207.C207_SKIP_RPT_FL IS NULL ");
        sbHeaderQuery.append(" AND T207.C207_OBSOLETE_FL IS NULL ");
        if (strSetType != null && !strSetType.equals("") && !strSetType.equals("0"))
        {// Set Type is Primary or Secondary
            sbHeaderQuery.append(" AND T207.C901_VANGUARD_TYPE = " + strSetType);
        }
        sbHeaderQuery.append(" ORDER BY C207_SEQ_NO ");

        HashMap hmReturn = new HashMap();

        if (strConType.equals("50442") || strConType.equals("50443"))
        {
            hmReturn = loadSummaryBySets(hmParam, sbHeaderQuery, strFilterCondition);
        } else if (strConType.equals("50445") || strConType.equals("50446") || strConType.equals("50447"))
        {
            hmReturn = loadCalculateValue(hmParam, sbHeaderQuery, strFilterCondition) == null ? new HashMap()
                            : loadCalculateValue(hmParam, sbHeaderQuery, strFilterCondition);
        } else
        // if (strConType.equals("50440") || strConType.equals("50441")
        // || strConType.equals("50444") || strConType.equals("50449") || strConType.equals("50448"))
        {
            log.debug("Header Query :" + sbHeaderQuery + ", \n Header Query :" + strFilterCondition);
            hmReturn = loadSummaryBySales(hmParam, sbHeaderQuery, strFilterCondition);
        }
        return hmReturn;
    }

    private HashMap loadCalculateValue(HashMap hmParam, StringBuffer sbHeaderQuery, String strFilterCondition)
                    throws AppError
    {
        HashMap hmSalesReturn = new HashMap();
        HashMap hmSetReturn = new HashMap();
        HashMap hmReturn = new HashMap();

        LOADSETSUMMARY = false;
        LOADSALESSUMMARY = true;
        hmSalesReturn = loadSummaryBySales(hmParam, sbHeaderQuery, strFilterCondition);

        if (strConType.equals("50445") || strConType.equals("50446")) // List Turns || Baseline Case Per Set
        {
            LOADSETSUMMARY = true;
            LOADSALESSUMMARY = false;
            hmSetReturn = loadSummaryBySets(hmParam, sbHeaderQuery, strFilterCondition);
            hmReturn = mergeHashMaps(hmSalesReturn, hmSetReturn);
        } else if (strConType.equals("50447")) 
        {// Tracking Implant Effeciency
            hmReturn = loadTrackingImplantEfficiency(hmParam, hmSalesReturn, sbHeaderQuery, strFilterCondition);
        }
        return hmReturn;
    }

    private HashMap mergeHashMaps(HashMap hmSalesReturn, HashMap hmSetReturn)
    {
        ArrayList alSalesDetails = new ArrayList();
        ArrayList alSetDetails = new ArrayList();
        
        HashMap hmSalesFooter = new HashMap();
        HashMap hmSetFooter = new HashMap();
        HashMap hmFooter = new HashMap();
        
        Set sSalesFooter = hmSetFooter.keySet();
        
        ArrayList alReturnDetails = new ArrayList();
        
        ArrayList alHeader = new ArrayList();
        
        HashMap hmSales = new HashMap();
        HashMap hmSet = new HashMap();
        
        alSalesDetails = (ArrayList) hmSalesReturn.get("Details");
        alSetDetails = (ArrayList) hmSetReturn.get("Details");
        
        hmSalesFooter = (HashMap)hmSalesReturn.get("Footer");
        hmSetFooter = (HashMap)hmSetReturn.get("Footer");
        
        alHeader = (ArrayList) hmSalesReturn.get("Header");
        
        String title = "";
        double dSales =0;
        double dSet =0;
        double dSetFooter =0;
        double dSalesFooter =0;
        
        Iterator itrAlSetDetails;
        Iterator itrAlSalesDetails;
        Iterator itrSSalesFooter;
        Iterator itrHeader;
        String setTitleValue="";
        String salesTitleValue="";
        String setTitleFooterValue = "";
        String salesTitleFooterValue = "";
        String salesID="";
        String setID="";
        
        // Details Calculation
        itrAlSalesDetails = alSalesDetails.iterator();
        while (itrAlSalesDetails.hasNext())
        {
            hmSales = (HashMap) itrAlSalesDetails.next();            
            itrAlSetDetails = alSetDetails.iterator();
            while (itrAlSetDetails.hasNext())
            {
                hmSet = (HashMap) itrAlSetDetails.next();
                salesID = (hmSales.get("ID") == null)?"":(String)hmSales.get("ID");
                setID = (hmSet.get("ID") == null) ? "" :(String)hmSet.get("ID");
                if (salesID.equals(setID))
                { 
                    itrHeader = alHeader.iterator();
                    while (itrHeader.hasNext())
                    {
                        title = (String) itrHeader.next();
                        if(!title.equals("Name") && !title.equals("Total"))
                            {
                                setTitleValue =  ( hmSet.get(title) == null || ((String) hmSet.get(title)).equals("") ) ? "0" : (String) hmSet.get(title);
                                salesTitleValue = (hmSales.get(title) == null || ((String) hmSales.get(title)).equals("")) ? "0" :  (String) hmSales.get(title);
                                                    
                                dSet = Double.parseDouble(setTitleValue);
                                dSales = Double.parseDouble(salesTitleValue);
                                
                                    if (dSet > 0)
                                    {
                                        dSales = dSales / dSet;
                                    }   
                                    else
                                    {
                                        dSales = 0;
                                    }
                               hmSales.put(title, String.valueOf(dSales));
                            } 
                      }    
                    alReturnDetails.add(hmSales);
                    itrAlSetDetails.remove();
                    itrAlSalesDetails.remove();
                    break;
                }
            }                       
        }  
        
        //Make the Left Over Set Values to zero
        
        itrAlSetDetails = alSetDetails.iterator();
        while(itrAlSetDetails.hasNext())
        {
           hmSet = (HashMap) itrAlSetDetails.next();
           if(hmSet.get("ID") != null && hmSet.get("Name") != null)
           {
           itrHeader = alHeader.iterator();           
           while (itrHeader.hasNext())
           {
               title = (String) itrHeader.next();
               if(!title.equals("Name") && !title.equals("Total"))
                   {                                                                                                   
                      hmSet.put(title, "0");                      
                   } 
             } 
           //  Add to the return list           
           alReturnDetails.add(hmSet);
        }
        }   
        //Make the Left Over Sales Values to zero
        
        itrAlSalesDetails = alSalesDetails.iterator();
        while(itrAlSalesDetails.hasNext())
        {
           hmSales = (HashMap) itrAlSalesDetails.next();
           if(hmSales.get("ID") != null && hmSales.get("Name") != null)
           {    
           itrHeader = alHeader.iterator();           
           while (itrHeader.hasNext())
           {
               title = (String) itrHeader.next();
               if(!title.equals("Name") && !title.equals("Total"))
                   {                                                                                                   
                     hmSales.put(title, "0");                      
                   } 
             }
           //  Add to the return list
           alReturnDetails.add(hmSales);
        }
        }
        alReturnDetails = (ArrayList) GmCommonSort.sortArrayList(alReturnDetails, "Name", "String", 1);     
        
        // Footer Calculation
        sSalesFooter = hmSalesFooter.keySet();
        itrSSalesFooter = sSalesFooter.iterator();
        while(itrSSalesFooter.hasNext())
        {
            title = (String)itrSSalesFooter.next();
            
            if(!title.equals("Name") && !title.equals("Total"))
            {
            setTitleFooterValue =  ( hmSetFooter.get(title) == null || ((String) hmSetFooter.get(title)).equals("") ) ? "0" : (String) hmSetFooter.get(title);
            salesTitleFooterValue = ( hmSalesFooter.get(title) == null || ((String) hmSalesFooter.get(title)).equals("") ) ? "0" : (String) hmSalesFooter.get(title);
            
            dSetFooter = Double.parseDouble(setTitleFooterValue);
            dSalesFooter = Double.parseDouble(salesTitleFooterValue);

                if(dSetFooter > 0)
                {
                    dSalesFooter = dSalesFooter / dSetFooter;                                
                }
                else
                {
                    dSalesFooter = 0;
                }
                hmFooter.put(title, String.valueOf(dSalesFooter));
                
                if(hmSetFooter.get(title) != null)
                {
                    hmSetFooter.remove(title);
                }
                
                if(hmSalesFooter.get(title) != null)
                {
                    itrSSalesFooter.remove();
                }
            }     
        }
        
        // Make the Left Over Set Values to zero
        Set sSetFooter = hmSetFooter.keySet();
        Iterator itrSSetFooter = sSetFooter.iterator();
        while(itrSSetFooter.hasNext())
        {
           title = (String)itrSSetFooter.next();
           if(!title.equals("Name") && !title.equals("Total"))
           {
               hmSetFooter.put(title,"0");
           }
        }
        
        // Make the Left Over Sales Values to zero
        sSalesFooter = hmSalesFooter.keySet();
        itrSSalesFooter = sSalesFooter.iterator();
        while(itrSSalesFooter.hasNext())
        {
           title = (String)itrSSalesFooter.next();
           if(!title.equals("Name") && !title.equals("Total"))
           {
               hmSalesFooter.put(title,"0");
           }
        }

        
        // Add the left over footer values from the set to the sales
        hmFooter.putAll(hmSetFooter);
        hmFooter.putAll(hmSalesFooter);
        // Add the Details and the Footer to the return HashMap        

        hmSalesReturn.put("Details", alReturnDetails);
        hmSalesReturn.put("Footer", hmFooter);
        
        return hmSalesReturn;
    }

    private HashMap loadTrackingImplantEfficiency(HashMap hmParam, HashMap hmSalesReturn, StringBuffer sbHeaderQuery,
                    String strFilterCondition) throws AppError
    {
        getCrossTabFilterCondition(hmParam);
        // *****************************************************************
        // Query to Fetch the Consignment information for # Sets and $Value
        // *****************************************************************

        log.debug(" Type is " + strType);
        StringBuffer sbDetailQuery = new StringBuffer();
        HashMap hmapFinalValue; // Final value report details
        HashMap hmTrackingImplantHeldReturn = new HashMap();
        // String strWhereCondition = calcualtePeriod(hmParam);

        String strFrameColumQuery = getFrameColumn(strAction);
        log.debug(" strFrameColumQuery is " + strFrameColumQuery);

        String strFrameGroupQuery = getFrameGroup(strAction);
        log.debug(" strFrameGroupQuery is " + strFrameGroupQuery);
        
        sbDetailQuery.setLength(0);
        sbDetailQuery.append(strFrameColumQuery);
        sbDetailQuery.append("  ,V207.C207_SET_ID SID  ");
        sbDetailQuery.append(" ,SUM(T731.C731_ITEM_QTY) AMOUNT ");
        sbDetailQuery.append(" FROM ");
        sbDetailQuery.append(" T731_VIRTUAL_CONSIGN_ITEM T731 ");
        sbDetailQuery.append(" ,T730_VIRTUAL_CONSIGNMENT T730 ");
        sbDetailQuery.append(" ,V207A_SET_CONSIGN_LINK V207 ");
        sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700 ");
        sbDetailQuery.append(" ,V207B_SET_PART_DETAILS V207B ");
        sbDetailQuery.append(" WHERE ");
        sbDetailQuery.append(" T730.C701_DISTRIBUTOR_ID = V700.D_ID ");
        sbDetailQuery.append(" AND T730.C207_SET_ID =  V207.C207_ACTUAL_SET_ID ");
        sbDetailQuery.append(" AND T730.C730_VIRTUAL_CONSIGNMENT_ID = T731.C730_VIRTUAL_CONSIGNMENT_ID ");
        sbDetailQuery.append(" AND V207.C901_LINK_TYPE = 20100 ");
        sbDetailQuery.append(" AND T731.C205_PART_NUMBER_ID = V207B.C205_PART_NUMBER_ID ");   
        sbDetailQuery.append(" AND V207B.C205_TRACKING_IMPLANT_FL = 'Y' ");

        // to fetch based on distributor rep filter
        if (strDistributorID != null && !strDistributorID.equals("") && !strDistributorID.equals("null"))
        {
            sbDetailQuery.append("  AND T730.C701_DISTRIBUTOR_ID = ");
            sbDetailQuery.append(strDistributorID);
        }        
        if (strSetType != null && !strSetType.equals("") && !strSetType.equals("0"))
        {// Set Type is Primary or Secondary
            sbDetailQuery.append(" AND V207.C901_VANGUARD_TYPE = " + strSetType);
        }
        if (strADID != null && !strADID.equals("") && !strADID.equals("null") && !strADID.equals("0"))
        {
            sbDetailQuery.append(" AND V700.AD_ID = " + strADID);
        }
        // sbQuery.append(gmSalesFilterConditionBean.getCrossTabFilterCondition(hmParam));
        sbDetailQuery.append(strFrameGroupQuery);
        sbDetailQuery.append(", V700.REGION_NAME, V207.C207_SET_ID, V207.C207_SEQ_NO ");
        sbDetailQuery.append(" ORDER BY NAME, V207.C207_SEQ_NO ");
        log.debug("by reportSetSummary ++++ " + sbHeaderQuery.toString());
        log.debug("by reportSetSummary ++++ " + sbDetailQuery.toString());
        hmTrackingImplantHeldReturn = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery
                        .toString());
        //log.debug(" hmTrackingImplantHeldReturn :" +hmTrackingImplantHeldReturn);
       //log.debug(" hmSalesReturn : "+ hmSalesReturn);
        hmapFinalValue = mergeHashMaps(hmSalesReturn, hmTrackingImplantHeldReturn);
        log.debug("Exiting loadSummaryBySales");
        return hmapFinalValue;
    }

    private HashMap loadSummaryBySets(HashMap hmParam, StringBuffer sbHeaderQuery, String strFilterCondition)
                    throws AppError
    {
        getCrossTabFilterCondition(hmParam);
        // *****************************************************************
        // Query to Fetch the Consignment information for # Sets and $Value
        // *****************************************************************

        log.debug(" Type is " + strType);
        StringBuffer sbDetailQuery = new StringBuffer();
        HashMap hmapFinalValue; // Final value report details

        // String strWhereCondition = calcualtePeriod(hmParam);

        String strFrameColumQuery = getFrameColumn(strAction);
        log.debug(" strFrameColumQuery is " + strFrameColumQuery);

        String strFrameGroupQuery = getFrameGroup(strAction);
        log.debug(" strFrameGroupQuery is " + strFrameGroupQuery);

        sbDetailQuery.setLength(0);
        sbDetailQuery.append(strFrameColumQuery);
        sbDetailQuery.append(" ,V207.C207_SET_ID SID ");
        sbDetailQuery.append(getSelectClause(strConType, strType, strPeriod));
        sbDetailQuery.append(" FROM T730_VIRTUAL_CONSIGNMENT T730 ");
        sbDetailQuery.append(", V207A_SET_CONSIGN_LINK V207 ");
        sbDetailQuery.append(getAccessFilterClause());
        sbDetailQuery.append(" WHERE T730.C207_SET_ID =  V207.C207_ACTUAL_SET_ID ");
        sbDetailQuery.append(" AND   V700.D_ID = T730.C701_DISTRIBUTOR_ID ");
        // to fetch based on distributor rep filter
        if (strDistributorID != null && !strDistributorID.equals("") && !strDistributorID.equals("null"))
        {
            sbDetailQuery.append("  AND T730.C701_DISTRIBUTOR_ID = ");
            sbDetailQuery.append(strDistributorID);
        }
        if (strSetType != null && !strSetType.equals("") && !strSetType.equals("0"))
        {// Set Type is Primary or Secondary
            sbDetailQuery.append(" AND V207.C901_VANGUARD_TYPE = " + strSetType);
        }
        if (strADID != null && !strADID.equals("") && !strADID.equals("null") && !strADID.equals("0"))
        {
            sbDetailQuery.append(" AND V700.AD_ID = " + strADID);
        }
        // sbQuery.append(gmSalesFilterConditionBean.getCrossTabFilterCondition(hmParam));
        sbDetailQuery.append(strFrameGroupQuery);
        sbDetailQuery.append(", V207.C207_SET_ID, V207.C207_SEQ_NO ");
        sbDetailQuery.append(" ORDER BY NAME, V207.C207_SEQ_NO ");
        log.debug("by reportSetSummary ++++ " + sbHeaderQuery.toString());
        log.debug("by reportSetSummary ++++ " + sbDetailQuery.toString());
        hmapFinalValue = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
        log.debug("Exiting loadSummaryBySets");
        return hmapFinalValue;
    }

    private HashMap loadSummaryBySales(HashMap hmParam, StringBuffer sbHeaderQuery, String strFilterCondition)
                    throws AppError
    {
        log.debug("Entering loadSummaryBySales :" + strAction);

        StringBuffer sbDetailQuery = new StringBuffer();
        HashMap hmapFinalValue; // Final value report details
        GmCrossTabReport gmCrossReport = new GmCrossTabReport();

        String strWhereCondition = calcualtePeriod(hmParam);

        String strFrameColumQuery = getFrameColumn(strAction);
        log.debug(" strFrameColumQuery is " + strFrameColumQuery);

        String strFrameGroupQuery = getFrameGroup(strAction);
        log.debug(" strFrameGroupQuery is " + strFrameGroupQuery);

        String strFrameOrderQuery = getFrameOrder(strAction);
        log.debug(" strFrameGroupQuery is " + strFrameGroupQuery);

        //

        // ***********************************************************
        // Query to Fetch the Consignment information by area director
        // and distributor virtual consignment count
        // ***********************************************************
        sbDetailQuery.append(strFrameColumQuery);
        sbDetailQuery.append(" ,V207.C207_SET_ID SID  ");
        sbDetailQuery.append(getSelectClause(strConType, strType, strPeriod));
        sbDetailQuery.append("FROM ");
        sbDetailQuery.append("T501_ORDER T501 ");
        sbDetailQuery.append(",T502_ITEM_ORDER T502 ");
        sbDetailQuery.append(",V207B_SET_PART_DETAILS V207 ");
        sbDetailQuery.append(",V700_TERRITORY_MAPPING_DETAIL V700 ");
        sbDetailQuery.append("WHERE  ");
        sbDetailQuery.append("V207.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
        sbDetailQuery.append("AND V700.AC_ID  = T501.C704_ACCOUNT_ID ");
        sbDetailQuery.append("AND T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
        sbDetailQuery.append("AND T502.C502_VOID_FL IS NULL ");
        sbDetailQuery.append("AND T501.C501_VOID_FL IS NULL ");
        sbDetailQuery.append("AND T501.C501_DELETE_FL IS NULL ");
        sbDetailQuery.append(getWhereClause(strSetType, strConType));
        sbDetailQuery.append(strFilterCondition);
        sbDetailQuery.append(getSalesFilterClause());
        sbDetailQuery.append(" AND " + strWhereCondition);
        sbDetailQuery.append(strFrameGroupQuery);
        sbDetailQuery.append(",REGION_NAME ");
        sbDetailQuery.append(",V207.C207_SET_ID ");
        if (strConType.equals("50441") || strConType.equals("50444") || strConType.equals("50446")
                        || strConType.equals("50447"))
        {
            sbDetailQuery.append(" , V207.C204_VALUE ");
        }
        sbDetailQuery.append("ORDER BY ");
        sbDetailQuery.append("NAME ");
        sbDetailQuery.append(",SID ");

        log.debug("******* Error " + sbHeaderQuery.toString());
        log.debug(" DetailsQuery is " + sbDetailQuery.toString());
        hmapFinalValue = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
        log.debug("Exiting loadSummaryBySales");
        return hmapFinalValue;
    }

    private String getWhereClause(String strSetType, String strConType)
    {
        StringBuffer strWhereClause = new StringBuffer();

        strConType = strConType == null ? "" : strConType;
        strSetType = strSetType == null ? "" : strSetType;

        if (strConType.equals("50441") || strConType.equals("50444"))
        { // Baseline Case
            strWhereClause.append(" AND V207.C205_TRACKING_IMPLANT_FL = 'Y' ");
        }
        if (!strSetType.equals("") && !strSetType.equals("0"))
        {// Set Type is Primary or Secondary
            strWhereClause.append(" AND V207.C901_VANGUARD_TYPE = " + strSetType);
        }

        return strWhereClause.toString();
    }

    private String getSelectClause(String strConType, String strType, String strPeriod) throws AppError
    {
        StringBuffer strSelectClause = new StringBuffer();
        strConType = strConType == null ? "" : strConType;
        strType = strType == null ? "" : strType;

        int codeAlt = 1;
        String codeAltName = GmCommonClass.getCodeAltName(strPeriod);
        if (codeAltName != null && !codeAltName.equals("") && !codeAltName.equals("0"))
        {
            codeAlt = Integer.parseInt(codeAltName);
        }
        if ((strConType.equals("50446") && !LOADSETSUMMARY && LOADSALESSUMMARY))
        { //Baseline Case Per Set
            strSelectClause.append(",DECODE(V207.C204_VALUE , 0, 0,  (SUM(DECODE(C205_TRACKING_IMPLANT_FL, 'Y', T502.C502_ITEM_QTY,0) ) / V207.C204_VALUE)) /"
                            + codeAlt + " AMOUNT ");
        }
        else if (strConType.equals("50441"))
        // Baseline Case 
        {            
            strSelectClause.append(",DECODE(V207.C204_VALUE, 0, 0, (SUM(T502.C502_ITEM_QTY)/V207.C204_VALUE) /"
                            + codeAlt + ") AMOUNT ");
        } else if (strConType.equals("50444") || strConType.equals("50447"))
        // Tracking Implants || Tracking Implant Efficiency
        {
            strSelectClause.append(",SUM(T502.C502_ITEM_QTY) AMOUNT ");
        } else if (strConType.equals("50449") || (strConType.equals("50445") && !LOADSETSUMMARY && LOADSALESSUMMARY))
        // Annualized Revenue || List Turns
        {
            strSelectClause.append(",(( SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY)");
            strSelectClause.append(" / " + codeAlt + " ) * 12 ) AMOUNT ");
        } else if (strConType.equals("50448")) // Avg. Price of Tracking Implants
        {
            strSelectClause.append(",( AVG(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY)");
            strSelectClause.append("  ) AMOUNT ");
        } else if (strConType.equals("50442") || (strConType.equals("50446") && LOADSETSUMMARY && !LOADSALESSUMMARY))
        {// # Sets || Base Line Case per Set
            strSelectClause.append(",SUM(DECODE(V207.C901_LINK_TYPE  ,20100 , 1 , 0) )  AMOUNT ");
        } else if (strConType.equals("50443")  || (strConType.equals("50445") && LOADSETSUMMARY && !LOADSALESSUMMARY)) 
        {// $ Value of Inventory || List Turns
            strSelectClause.append(",NVL(SUM(T730.C730_TOTAL_COST),0) AMOUNT ");
        } else if (strType.equals("1"))
        {
            strSelectClause.append(",SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY)  AMOUNT ");
        } else
        {
            // 2524 Maps to adjustment
            strSelectClause.append(",SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY )) AMOUNT ");
        }
        return strSelectClause.toString();
    }

    private String calcualtePeriod(HashMap hmParam) throws AppError
    {
        String strWhereCondtion = "";
        // String strCodeID = (String)hmParam.get("strPeriod");
        log.debug("strPeriod : " + strPeriod);
        int codeAlt = 0;
        String codeAltName = GmCommonClass.getCodeAltName(strPeriod);
        log.debug("codeAltName : " + codeAltName);
        if (codeAltName != null && !codeAltName.equals(""))
        {
            codeAlt = Integer.parseInt(codeAltName);
        }
        log.debug("codeAlt : " + codeAlt);
        boolean excludeCurrentMonth = codeAlt == 0 ? false : true;
        HashMap hmFromToDate = GmCommonClass.getMonthDiff(codeAlt, excludeCurrentMonth);
        log.debug("hmFromToDate : " + hmFromToDate);
        strFromMonth = (String) hmFromToDate.get("FromMonth");
        strFromYear = (String) hmFromToDate.get("FromYear");
        strToMonth = (String) hmFromToDate.get("ToMonth");
        strToYear = (String) hmFromToDate.get("ToYear");
        strWhereCondtion = getFromToDate(codeAlt);
        log.debug("strWhereCondtion : " + strWhereCondtion);
        log.debug("Date for periods" + hmFromToDate);

        return strWhereCondtion;
    }

}
