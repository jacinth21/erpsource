package com.globus.sales.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesConsignReportBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  // this will be removed all place changed with Data Store VO constructor

  public GmSalesConsignReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesConsignReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public static final String strTurnsLabel = "#Cases<BR> per M/Set";

  // log4j

  /**
   * viewGroupSetMap -
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   */
  public HashMap viewGroupSetMap(String strSetId) throws AppError {


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    CallableStatement csmt = null;
    Connection conn = null;

    String strPrepareString = null;
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;



    gmDBManager.setPrepareString("GM_SM_FCH_SET_LINK_DETAILS", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strSetId);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    hmReturn.put("MAPPED", alReturn);
    rs = (ResultSet) gmDBManager.getObject(3);
    alReturn = gmDBManager.returnArrayList(rs);
    hmReturn.put("UNMAPPED", alReturn);
    gmDBManager.close();

    return hmReturn;
  } // End of viewGroupSetMap

  /**
   * reportConsignmentSummary - This Method is used to fetch consignment summary report Will have
   * additional Filter
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportConsignmentSummary(HashMap hmparam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value k details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    // Below variable is used for cross tab format
    ArrayList alRowDetail = new ArrayList();
    ArrayList alColDetail = new ArrayList();
    ArrayList alDetailField = new ArrayList();

    HashMap hmapGroupInfo = new HashMap();
    HashMap hmapValue = new HashMap();

    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strAccessFilter = (String) hmparam.get("AccessFilter");

    // Main Header SQL (Fetch header value)
    sbHeaderQuery.append(" SELECT DISTINCT T207.C207_SET_ID S_NAME, C207_SEQ_NO  ");
    sbHeaderQuery.append(" FROM T207_SET_MASTER  T207  ");
    sbHeaderQuery.append(" WHERE T207.C901_SET_GRP_TYPE  = 1600 ");
    sbHeaderQuery.append(" AND 	 T207.C207_VOID_FL IS NULL  ");
    sbHeaderQuery.append(" ORDER BY C207_SEQ_NO ");

    // ***********************************************************
    // Query to Fetch the Consignment information by area director
    // and distributor virtual consignment count
    // ***********************************************************
    sbDetailQuery.append(" SELECT NVL(v700.AD_ID,0) AD_ID ");
    sbDetailQuery
        .append(" ,v700.REGION_NAME || ' - (' || NVL(v700.AD_NAME,'*No AD') || ')'  AD_NAME ");
    sbDetailQuery.append(" ,NVL(v700.D_ID,0) D_ID  ");
    sbDetailQuery.append(" ,NVL(v700.D_NAME,' *No Distributor') D_NAME ");
    sbDetailQuery.append(" ,T207ASUB.C207_MAIN_SET_ID S_NAME ");

    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" , SUM(DECODE(T207LINK.C901_CONS_RPT_ID  ,20100 , 1 , 0) )  S_VALUE ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(C730_TOTAL_COST)  S_VALUE ");
    }

    sbDetailQuery.append(" FROM T730_VIRTUAL_CONSIGNMENT T730, ");
    sbDetailQuery.append(" T207A_SET_LINK T207A ");
    sbDetailQuery.append(" , T207_SET_MASTER T207LINK, T207A_SET_LINK T207ASUB");
    sbDetailQuery.append(" , (    SELECT DISTINCT AD_ID,  AD_NAME,  REGION_ID,  ");
    sbDetailQuery.append(" REGION_NAME,  D_ID , D_NAME ");
    sbDetailQuery.append(" FROM   V700_TERRITORY_MAPPING_DETAIL )  V700 ");
    sbDetailQuery.append(" WHERE 	  T730.	C701_DISTRIBUTOR_ID  =  V700.D_ID ");
    sbDetailQuery.append(" AND 	  T730.C207_SET_ID = T207A.C207_LINK_SET_ID ");
    sbDetailQuery.append(" AND   T207A.C901_TYPE = 20003 ");
    sbDetailQuery.append(" AND T207A.C207_MAIN_SET_ID = T207ASUB.C207_LINK_SET_ID ");
    // sbDetailQuery.append(" AND T207ASUB.C207_MAIN_SET_ID =
    // T207.C207_SET_ID ");
    sbDetailQuery.append(" AND T207LINK.C207_SET_ID = T207A.C207_LINK_SET_ID ");

    // sbDetailQuery.append(" AND T207.C207_SET_ID = T207A.C207_MAIN_SET_ID
    // ");
    // bDetailQuery.append(" AND T207LINK.C207_SET_ID =
    // T207A.C207_LINK_SET_ID ");
    sbDetailQuery.append(" GROUP BY REGION_ID,REGION_NAME, AD_ID,  AD_NAME,      ");
    sbDetailQuery.append(" D_ID , D_NAME, T207ASUB.C207_MAIN_SET_ID  ");
    sbDetailQuery.append(" ORDER BY REGION_NAME ,  AD_NAME, D_NAME, S_NAME");

    /***********************************************************************
     * Report setting details to specify row and column information alRowDetail Holds Row
     * Information alColDetail Holds Column Information alDetailField Holds detail field value
     * (Month Data)
     **********************************************************************/

    // Distributor Information Level I
    hmapValue = null;
    hmapValue = new HashMap();
    hmapValue.put("KEY", "D_NAME");
    hmapValue.put("ID", "D_ID");
    alRowDetail.add(hmapValue);

    // Area Director Information Level II
    hmapValue = null;
    hmapValue = new HashMap();
    hmapValue.put("KEY", "AD_NAME");
    hmapValue.put("ID", "AD_ID");
    alRowDetail.add(hmapValue);

    alColDetail.add("S_NAME");

    alDetailField.add("S_VALUE");

    hmapGroupInfo.put("ROW", alRowDetail);
    hmapGroupInfo.put("COLUMN", alColDetail);
    hmapGroupInfo.put("DETAILFIELD", alDetailField);

    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString(),
            hmapGroupInfo);

    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * reportLoanerBySummary - This Method is used to fetch Loaner details Will have additional Filter
   * (like distributor, rep, etc)
   * 
   * @return HashMap
   * @exception AppError
   */
  public RowSetDynaClass reportLoanerBySummary(HashMap hmparam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    RowSetDynaClass resultSet = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strTurns = GmCommonClass.parseZero((String) hmparam.get("TURNS"));
    int turns = Integer.parseInt(strTurns);
    HashMap hmFromToMonth = GmCommonClass.getMonthDiff(turns, true);
    log.debug(" hmFromToMonth " + hmFromToMonth);
    String strFromDate = hmFromToMonth.get("FromMonth") + "/" + hmFromToMonth.get("FromYear");
    String strToDate = hmFromToMonth.get("ToMonth") + "/" + hmFromToMonth.get("ToYear");

    String strDistributorID = (String) hmparam.get("DistributorID");
    String strADID = (String) hmparam.get("ADID");

    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strAccessFilter = (String) hmparam.get("AccessFilter");

    String strSetNumber = (String) hmparam.get("SetNumber");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strSetColumn = " v207.c207_set_id";
    String strGroupColumn = "";
    String strVSetId = "v207.C207_LINK_SET_ID";

    StringBuffer sbDrillQuery = new StringBuffer();
    sbDrillQuery
        .append(" , gm_pkg_sm_part_consign_info.get_current_set_count (v207.C207_LINK_SET_ID) vsetidcount , ");
    sbDrillQuery.append(strVSetId);
    sbDrillQuery.append("  vsetid, get_set_name(v207.C207_LINK_SET_ID) vsetnm ");

    if (strType.equals("BYSETPART")) {
      strSetColumn = " v207.C207_LINK_SET_ID ";
      strGroupColumn = " , v207.C207_ACTUAL_SET_ID ";
      strVSetId = "v207.C207_ACTUAL_SET_ID";

      sbDrillQuery.setLength(0);
      sbDrillQuery
          .append(" , gm_pkg_sm_part_consign_info.get_current_set_count (v207.C207_ACTUAL_SET_ID) vsetidcount , ");
      sbDrillQuery.append(strVSetId);
      sbDrillQuery.append("  vsetid, get_set_name(v207.C207_ACTUAL_SET_ID) vsetnm ");
    }


    // Main Header SQL (Fetch header value)
    sbQuery.append(" SELECT NVL (d_id, 0) d_id , NVL (d_name, ' *No Distributor') dname ");
    sbQuery.append(" , v207.C207_SET_ID groupid, get_set_name (v207.C207_SET_ID) groupnm  ");
    sbQuery.append(" , get_code_name_alt ( GET_SET_HIERARCHY(");
    sbQuery.append(strVSetId);
    sbQuery.append(" )) hch, NVL(v207.C901_LINK_TYPE,0) VCONSRPTID ");
    sbQuery.append(" , count(1) VSETCOUNT , '' PLINE, '' SGDIMAGE, '' DIMAGE, '-' VSETCOST ");
    sbQuery.append(" , NVL(v207.C207A_SHARED_STATUS,0) SHAREDSTATUS ");
    sbQuery.append(sbDrillQuery);
    sbQuery.append(" FROM t504a_loaner_transaction t504a ");
    sbQuery.append(" , t504_consignment t504 ");
    sbQuery.append(" , v207a_set_consign_link v207 ");
    sbQuery
        .append(" ,  (SELECT DISTINCT AD_ID,  AD_NAME,  REGION_ID,   REGION_NAME,   D_ID , D_NAME  FROM  V700_TERRITORY_MAPPING_DETAIL) v700 ");
    sbQuery.append(" WHERE t504.c504_consignment_id = t504a.c504_consignment_id ");
    sbQuery.append(" AND v700.D_ID = c504a_consigned_to_id ");
    sbQuery.append(" AND t504.c207_set_id = v207.c207_actual_set_id ");
    sbQuery.append(" AND TRUNC (t504a.c504a_loaner_dt) >= TO_DATE ('");
    sbQuery.append(strFromDate);
    sbQuery.append("', 'MM/YYYY') ");
    sbQuery.append(" AND TRUNC (t504a.c504a_loaner_dt) <= LAST_DAY (TO_DATE ('");
    sbQuery.append(strToDate);
    sbQuery.append("','MM/YYYY')) ");
    sbQuery.append(" AND t504.c504_type = 4127 ");
    sbQuery.append(" AND t504.c207_set_id IS NOT NULL ");
    sbQuery.append(" AND t504a.c504a_void_fl IS NULL ");

    // To filter by set information
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
      sbQuery.append(" AND ");
      sbQuery.append(strSetColumn);
      sbQuery.append(" =   '");
      sbQuery.append(strSetNumber);
      sbQuery.append("' ");
    }

    // Area Director filter
    if (strADID != null && !strADID.equals("null") && !strADID.equals("")) {
      sbQuery.append(" AND AD_ID  = ");
      sbQuery.append(strADID);
    }

    // Distributor Filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbQuery.append(" AND D_ID = " + strDistributorID);
    }

    sbQuery.append(" GROUP BY ad_id, region_name, ad_name ");
    sbQuery.append(" , d_id, d_name ,  v207.C207_SET_ID ");
    sbQuery.append(" , v207.C901_CUSTOM_HIERARCHY, v207.C901_LINK_TYPE ");
    sbQuery.append(" , v207.C207_SEQ_NO , v207.C207A_SHARED_STATUS  ");
    sbQuery.append(" , v207.C207_LINK_SET_ID ");
    sbQuery.append(strGroupColumn);

    sbQuery.append(" ORDER BY VCONSRPTID, v207.C207_SEQ_NO ");

    log.debug("####### - report Loaner by summary " + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return resultSet;

  }


  /**
   * reportSalesBySummary - This Method is used to fetch consignment / loaner details depending on
   * the sales type that have been chosen
   * 
   * @return HashMap
   * @exception AppError
   */
  public RowSetDynaClass reportSalesBySummary(HashMap hmparam) throws AppError {
    String strSalesType = GmCommonClass.parseNull((String) hmparam.get("SALESTYPE"));
    RowSetDynaClass resultSet = null;

    if (strSalesType.equals("50300")) {
      resultSet = reportConsignmentBySummary(hmparam);
    } else {
      resultSet = reportLoanerBySummary(hmparam);
    }

    return resultSet;
  }

  /**
   * reportConsignmentBySummary - This Method is used to fetch consignment details Will have
   * additional Filter (like distributor, rep, etc)
   * 
   * @return HashMap
   * @exception AppError
   */
  public RowSetDynaClass reportConsignmentBySummary(HashMap hmparam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    RowSetDynaClass resultSet = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strDistributorID = (String) hmparam.get("DistributorID");
    String strADID = (String) hmparam.get("ADID");

    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strAccessFilter = (String) hmparam.get("AccessFilter");

    String strSetNumber = (String) hmparam.get("SetNumber");
    String strPartNumber = (String) hmparam.get("PartNumber");

    // Main Header SQL (Fetch header value)
    sbHeaderQuery.append(" SELECT NVL(AD_ID,0) AD_ID ");
    sbHeaderQuery.append(" , REGION_NAME || ' - (' || NVL(AD_NAME,'*No AD') || ')'  ADNAME  ");
    sbHeaderQuery.append(" , NVL(D_ID,0) D_ID   ,NVL(D_NAME,' *No Distributor') DNAME   ");
    sbHeaderQuery.append(" , T207A.M_SET_ID GROUPID");
    sbHeaderQuery.append(" , get_set_name(t207a.m_set_id)  GROUPNM ");
    if (strType.equals("BYSETPART")) {
      sbHeaderQuery.append(" , T207A.SETID VSETID "); // need to verify
      // this...
      sbHeaderQuery.append(" , 1 VSETIDCOUNT ");

    } else {
      sbHeaderQuery
          .append(" , gm_pkg_sm_part_consign_info.get_current_set_id(T207A.SETID) VSETID ");
      sbHeaderQuery
          .append(" , gm_pkg_sm_part_consign_info.get_current_set_count(T207A.SETID) VSETIDCOUNT ");
    }
    sbHeaderQuery.append(" , T207A.SETID LSETID ");
    sbHeaderQuery.append(" , t207.C207_SET_NM  VSETNM ");
    sbHeaderQuery.append(" , NVL (t207.c901_cons_rpt_id, 0) VCONSRPTID");
    sbHeaderQuery
        .append(" , SUM(DECODE(T730.C207_SET_ID,NULL,0,  DECODE (T207.C901_SET_GRP_TYPE, NULL, 0, 1604,0,1603,0,1605,0,1)) ) VSETCOUNT ");
    sbHeaderQuery
        .append(" , SUM(DECODE(T730.C207_SET_ID,NULL,0,  DECODE (c901_cons_rpt_id, NULL, 0, 20100,1,0)) ) VASETCOUNT ");
    sbHeaderQuery.append(" , NVL(SUM(T730.C730_TOTAL_COST),0) VSETCOST ");
    sbHeaderQuery.append(" , T207.C207_SEQ_NO ");
    sbHeaderQuery.append(" ,'' PLINE ");
    sbHeaderQuery.append(" ,'' SIMAGE "); // To display set mapping
    sbHeaderQuery.append(" ,'' DIMAGE "); // used for Detail value
    sbHeaderQuery.append(" ,'' AIMAGE "); // Used for Actual Value
    sbHeaderQuery.append(" ,'' SGDIMAGE"); // Used for Set Part Group
    // details
    sbHeaderQuery.append(" ,'' SDIMAGE ,'"); // Used for Set Part details
    sbHeaderQuery.append(strType + "' TYPE"); // Used for Set Part details
    sbHeaderQuery.append(" , get_code_name_alt(NVL(t207.C901_HIERARCHY,20701)) hch ");
    sbHeaderQuery.append(" , NVL(T207A.sharedstatus,0) SHAREDSTATUS  ");
    sbHeaderQuery.append(" FROM T730_VIRTUAL_CONSIGNMENT T730, ");
    sbHeaderQuery.append(" T207_SET_MASTER T207 , ");
    // Below query to fetch main set information
    sbHeaderQuery.append(" ( SELECT * FROM ");

    sbHeaderQuery
        .append(" ( SELECT   t207a.c207_main_set_id m_set_id,t207a.c207_link_set_id setid,CONNECT_BY_ROOT t207a.c207_link_set_id l_set_id");
    sbHeaderQuery
        .append(" , CONNECT_BY_ROOT t207a.C901_TYPE typ , CONNECT_BY_ROOT t207a.C901_SHARED_STATUS sharedstatus ");
    sbHeaderQuery.append(" FROM t207a_set_link t207a    ");
    sbHeaderQuery.append(" WHERE t207a.c207_main_set_id IN (SELECT t207.c207_set_id ");
    sbHeaderQuery.append("  FROM t207_set_master t207 ");

    // To filter by set information
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
      sbHeaderQuery.append(" WHERE t207.c207_set_id =   '");
      sbHeaderQuery.append(strSetNumber);
      sbHeaderQuery.append("') ");
    }
    sbHeaderQuery.append(" CONNECT BY PRIOR c207_main_set_id = t207a.c207_link_set_id ");
    // sbHeaderQuery.append(" AND CONNECT_BY_ROOT t207a.C901_TYPE  <> 20004  "); This doesnt work in
    // Oracle 11g
    sbHeaderQuery.append(" ) T207A ");
    sbHeaderQuery.append(" , (    SELECT DISTINCT AD_ID,  AD_NAME,  REGION_ID,   REGION_NAME,  ");
    sbHeaderQuery.append(" D_ID , D_NAME  FROM  V700_TERRITORY_MAPPING_DETAIL ");
    sbHeaderQuery.append(" WHERE D_ID = D_ID");

    // Area Director filter
    if (strADID != null && !strADID.equals("null") && !strADID.equals("")) {
      sbHeaderQuery.append(" AND AD_ID  = ");
      sbHeaderQuery.append(strADID);
    }

    // Distributor Filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbHeaderQuery.append(" AND D_ID = " + strDistributorID);
    }
    sbHeaderQuery.append("  ) V700 ");
    sbHeaderQuery.append(" )  T207A ");

    sbHeaderQuery.append(" WHERE t730.c207_set_id = t207a.l_set_id  ");
    sbHeaderQuery.append(" AND  T207A.D_ID  = T730.C701_DISTRIBUTOR_ID (+) ");
    sbHeaderQuery.append(" AND  t207.c207_set_id = t207a.setid   ");
    sbHeaderQuery.append(" AND  t207a.typ  <> 20004 ");

    /*
     * sbHeaderQuery.append(" ( SELECT B.C207_MAIN_SET_ID , B.C207_LINK_SET_ID , A.C207_SET_NM
     * LINK_SET_NM "); sbHeaderQuery.append(" , A.C901_SET_GRP_TYPE ");
     * sbHeaderQuery.append(" FROM T207_SET_MASTER A, T207A_SET_LINK B "); sbHeaderQuery.append("
     * WHERE A.C207_SET_ID = B.C207_LINK_SET_ID AND B.C901_TYPE = 20000 "); sbHeaderQuery.append(" )
     * T207A, "); sbHeaderQuery.append(" T207_SET_MASTER T207 "); sbHeaderQuery.append(" , ( SELECT
     * DISTINCT AD_ID, AD_NAME, REGION_ID, REGION_NAME, D_ID , D_NAME FROM
     * "); sbHeaderQuery.append(" V700_TERRITORY_MAPPING_DETAIL ) V700 "); sbHeaderQuery.append("
     * WHERE T730.C701_DISTRIBUTOR_ID = V700.D_ID "); sbHeaderQuery.append(" AND T730.C207_SET_ID =
     * T207A.C207_LINK_SET_ID "); sbHeaderQuery.append(" AND T207.C207_SET_ID =
     * T207A.C207_MAIN_SET_ID "); //To filter the information based on the set number if
     * (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
     * sbHeaderQuery.append(" AND T207.C207_SET_ID = '"); sbHeaderQuery.append(strSetNumber);
     * sbHeaderQuery.append("' "); }
     */

    sbHeaderQuery.append(" GROUP BY REGION_ID,REGION_NAME, AD_ID,  AD_NAME,       D_ID , D_NAME  ");
    sbHeaderQuery.append(" ,t207a.setid  ");
    sbHeaderQuery.append(" , t207.c207_set_nm   ");
    sbHeaderQuery.append(" ,t207a.m_set_id ");
    sbHeaderQuery.append(" , c901_cons_rpt_id ");
    sbHeaderQuery.append(" , T207.C207_SEQ_NO , t207.C901_HIERARCHY , T207A.sharedstatus");
    sbHeaderQuery.append(" ORDER BY c901_cons_rpt_id, T207.C207_SEQ_NO ");

    log.debug("####### - reportConsignmentBySummary " + sbHeaderQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbHeaderQuery.toString());

    return resultSet;

  }

  /**
   * reportConsignmentDetails - This Method is used to fetch consignment details Will have
   * additional Filter (like distributor, rep, etc)
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportConsignmentDetails(HashMap hmparam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    HashMap hmreturn = new HashMap(); // Final value report details
    RowSetDynaClass resultSet = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strDistributorID = (String) hmparam.get("DistributorID");

    String strADID = (String) hmparam.get("ADID");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strAccessFilter = (String) hmparam.get("AccessFilter");
    String strSetID = (String) hmparam.get("SetNumber");

    // Main Header SQL (Fetch header value)
    sbHeaderQuery
        .append(" SELECT T731.C205_PART_NUMBER_ID PNUM, C205_PART_NUM_DESC PDESC, GET_CODE_NAME(C205_PRODUCT_FAMILY) PRODFAMILY ");

    // sbHeaderQuery
    // .append("
    // SUM(gm_pkg_sm_part_consign_info.get_part_consigned_count(C205_PART_NUMBER_ID,
    // T730.C701_DISTRIBUTOR_ID)) QTY ");
    sbHeaderQuery.append(" , SUM(C731_ITEM_QTY) QTY ");

    sbHeaderQuery.append(" , C731_ITEM_PRICE PRICE ");
    // sbHeaderQuery
    // .append(" ,
    // SUM(gm_pkg_sm_part_consign_info.get_part_consigned_count(C205_PART_NUMBER_ID,
    // T730.C701_DISTRIBUTOR_ID) * C731_ITEM_PRICE ) TOTAL ");
    sbHeaderQuery.append(" , (SUM(C731_ITEM_QTY)  *  C731_ITEM_PRICE ) TOTAL  ");

    sbHeaderQuery.append(" , '' PLINE ");
    sbHeaderQuery
        .append(" FROM T730_VIRTUAL_CONSIGNMENT T730,  T731_VIRTUAL_CONSIGN_ITEM T731, T205_PART_NUMBER T205  ");
    sbHeaderQuery
        .append(" WHERE  T730.C730_VIRTUAL_CONSIGNMENT_ID =  T731.C730_VIRTUAL_CONSIGNMENT_ID  ");
    sbHeaderQuery.append(" AND  T731.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID  ");

    if (strType.equals("Main")) {
      // To filter the information based on the set number
      if (strSetID != null && !strSetID.equals("null") && !strSetID.equals("0")) {
        sbHeaderQuery.append(" AND T730.C207_SET_ID IN ( ");
        sbHeaderQuery.append(" SELECT T207A.C207_LINK_SET_ID  FROM T207A_SET_LINK  T207A ");
        sbHeaderQuery.append(" WHERE  T207A.C901_TYPE = 20003  ");
        sbHeaderQuery.append(" AND T207A.C207_MAIN_SET_ID  = '");
        sbHeaderQuery.append(strSetID);
        sbHeaderQuery.append("' ) ");
      }

    } else {
      // To filter the information based on the set number
      if (strSetID != null && !strSetID.equals("null") && !strSetID.equals("0")) {
        sbHeaderQuery.append(" AND T730.C207_SET_ID = '");
        sbHeaderQuery.append(strSetID);
        sbHeaderQuery.append("' ");
      }
    }

    // Distributor Filter
    if (strDistributorID != null && !strDistributorID.equals("null")) {
      sbHeaderQuery.append(" AND	 T730.C701_DISTRIBUTOR_ID = " + strDistributorID);
    }

    sbHeaderQuery
        .append(" GROUP BY T731.C205_PART_NUMBER_ID , C205_PART_NUM_DESC, C205_PRODUCT_FAMILY, C731_ITEM_PRICE ");
    sbHeaderQuery.append(" ORDER BY PNUM, PDESC ");

    // Area Director filter
    /*
     * if (strADID != null && !strADID.equals("null")) { sbHeaderQuery.append(" AND V700.AD_ID = ");
     * sbHeaderQuery.append( strADID ); } log.debug("strDistributorID " + strDistributorID); //
     * Distributor Filter if (strDistributorID != null && !strDistributorID.equals("null")) {
     * sbHeaderQuery.append(" AND V700.D_ID = " + strDistributorID ); } sbHeaderQuery.append(" GROUP
     * BY REGION_ID,REGION_NAME, AD_ID, AD_NAME, D_ID , D_NAME ");
     * sbHeaderQuery.append(" ,T207A.C207_MAIN_SET_ID ");
     * sbHeaderQuery.append(" , T207.C207_SET_NM "); sbHeaderQuery.append(" ,T207A.C207_LINK_SET_ID
     * "); sbHeaderQuery.append(" , T207A.LINK_SET_NM ");
     */

    log.debug("############## ---- reportConsignmentDetails " + sbHeaderQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbHeaderQuery.toString());

    hmreturn.put("PDETAIL", resultSet);

    return hmreturn;
  }

  /**
   * loadConsignSeachLists - to search parts consigned for the selected set
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public HashMap loadConsignSeachLists(String strAccessFilter) throws AppError {
    GmCommonClass gmCommon = new GmCommonClass();
    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();

    HashMap hmFilter = new HashMap();
    hmFilter.put("CONDITION", strAccessFilter);
    hmFilter.put("FLTR_DIST_BY_COMP", "YES");// Filter Distributor Based on company

    alResult = gmCommon.getCodeList("CNSRC");
    hmReturn.put("SEARCHTYPES", alResult);
    alResult = gmCommon.getCodeList("CNSRT");
    hmReturn.put("SEARCHKIND", alResult);
    alResult = gmCommon.getCodeList("CNSRP");
    hmReturn.put("SEARCHSTATUS", alResult);
    alResult = gmProj.loadSetMap("1601");
    hmReturn.put("SETMASTERLIST", alResult);
    alResult = gmCommonBean.getSalesFilterDist(hmFilter);
    hmReturn.put("DISTRIBUTORLIST", alResult);

    return hmReturn;
  }

  // End of loadConsignSeachLists

  /**
   * reportSeachSalesConsign - to search parts consigned for the selected set
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */

  public RowSetDynaClass reportSeachSalesConsign(HashMap hmParam) throws AppError

  {
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();

    boolean blCheck = false;

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strFrmDt = GmCommonClass.parseNull((String) hmParam.get("FRMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strCategory = GmCommonClass.parseNull((String) hmParam.get("CATEGORY"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strTransId = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    String strDummyFilter = GmCommonClass.parseNull((String) hmParam.get("DUMMYTRAN"));
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    strPartNum = strPartNum.replaceAll(",", "','");
    strSetId = strSetId.replaceAll(",", "','");

    log.debug(" hmParam values are " + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    RowSetDynaClass resultSet = null;

    if (strCategory.equals("20210") || strCategory.equals("20211")) {
      // Code to fetch consignment information
      sbQuery.append(" SELECT A.C504_CONSIGNMENT_ID ID, NVL(A.C504_TRACKING_NUMBER,'-') TRACK  ");
      sbQuery.append(" ,  GET_DISTRIBUTOR_NAME(A.C701_DISTRIBUTOR_ID) DISTID ");
      sbQuery.append(" ,A.C207_SET_ID SETID, A.C504_SHIP_DATE DDATE ");
      sbQuery.append(" ,TO_NUMBER(TO_CHAR(A.C504_SHIP_DATE,'yyyymmdd') ) SDATE ");
      sbQuery.append(" , 'C' CTYPE,  '' PLINE , '' DIMAGE, A.C504_DELIVERY_CARRIER CARRIER  ");
      sbQuery.append(" FROM T504_CONSIGNMENT A ");
      sbQuery.append(" WHERE ");
      sbQuery.append(" A.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND A.C504_TYPE = ");
      sbQuery.append(strDummyFilter);
      // Added company id for load records depending on CompanyId.
      sbQuery.append(" AND A.C1900_COMPANY_ID = " + getCompId());

      if (strType.equals("20202")) {
        sbQuery.append(" AND A.C207_SET_ID IS NULL ");
      } else if (strType.equals("20201")) {
        sbQuery.append(" AND A.C207_SET_ID IS NOT NULL ");
      }

      if (!strPartNum.equals("")) {
        sbQuery.append(" AND A.C504_CONSIGNMENT_ID IN (SELECT  B.C504_CONSIGNMENT_ID ");
        sbQuery.append(" FROM T505_ITEM_CONSIGNMENT B WHERE  B.C205_PART_NUMBER_ID IN ('");
        sbQuery.append(strPartNum);
        sbQuery.append("') AND B.C505_ITEM_QTY > 0 ) ");
      }

      if (!strSetId.equals("") && !strType.equals("20202")) {
        sbQuery.append(" AND A.C207_SET_ID IN ('");
        sbQuery.append(strSetId);
        sbQuery.append("') ");
      }

      if (!strDistId.equals("")) {
        sbQuery.append(" AND A.C701_DISTRIBUTOR_ID IN (");
        sbQuery.append(strDistId);
        sbQuery.append(") ");
      }

      if (!strFrmDt.equals("") && !strToDt.equals("")) {
        sbQuery.append(" AND A.C504_SHIP_DATE BETWEEN ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFrmDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");
      }

      if (!strTransId.equals("")) {
        sbQuery.append(" AND A.C504_CONSIGNMENT_ID = '");
        sbQuery.append(strTransId);
        sbQuery.append("'");
      }

      if (strStatus.equals("20231")) {
        sbQuery.append(" AND A.C504_STATUS_FL IN (2,3) ");
      } else {
        sbQuery.append(" AND A.C504_STATUS_FL = 4 "); // only if the
        // product is
        // shipped
      }
    }

    if (strCategory.equals("20210") && strDummyFilter.equals("4110")) {
      sbQuery.append(" UNION ");
    }

    if ((strCategory.equals("20210") || strCategory.equals("20212"))
        && strDummyFilter.equals("4110")) {
      sbQuery.append(" SELECT A.C506_RMA_ID ID , '-' TRACK ");
      sbQuery.append(" , GET_DISTRIBUTOR_NAME(A.C701_DISTRIBUTOR_ID)  DISTID ");
      sbQuery.append(" ,A.C207_SET_ID SETID ,A.C506_CREDIT_DATE DDATE   ");
      sbQuery.append(" ,TO_NUMBER(TO_CHAR(A.C506_CREDIT_DATE,'yyyymmdd') ) SDATE ");
      sbQuery.append(" , 'R' CTYPE ,  '' PLINE , '' DIMAGE, 0 CARRIER   ");
      sbQuery.append(" FROM T506_RETURNS A ");
      sbQuery.append(" WHERE ");
      sbQuery.append(" A.C506_VOID_FL IS NULL ");
      // Added company id for load records depending on CompanyId.
      sbQuery.append(" AND A.C1900_COMPANY_ID = " + getCompId());
      if (strType.equals("20202")) {
        sbQuery.append(" A.C207_SET_ID IS NULL ");
      } else if (strType.equals("20201")) {
        sbQuery.append(" AND A.C207_SET_ID IS NOT NULL ");
      }

      if (!strPartNum.equals("")) {
        sbQuery.append(" AND A.C506_RMA_ID IN (SELECT  B.C506_RMA_ID  FROM T507_RETURNS_ITEM B ");
        sbQuery.append(" WHERE B.C205_PART_NUMBER_ID IN ('");
        sbQuery.append(strPartNum);
        sbQuery.append("') AND B.C507_ITEM_QTY > 0 ) ");
      }

      if (!strSetId.equals("") && !strType.equals("20202")) {
        sbQuery.append(" AND A.C207_SET_ID IN ('");
        sbQuery.append(strSetId);
        sbQuery.append("')  ");
      }

      if (!strDistId.equals("")) {
        sbQuery.append(" AND A.C701_DISTRIBUTOR_ID IN (");
        sbQuery.append(strDistId);
        sbQuery.append(")");
      }

      if (!strFrmDt.equals("") && !strToDt.equals("")) {
        sbQuery.append(" AND A.C506_CREDIT_DATE BETWEEN ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFrmDt);
        sbQuery.append("','" + strDateFormat + "') AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strDateFormat + "')");
      }

      if (!strTransId.equals("")) {
        sbQuery.append(" AND A.C506_RMA_ID = '");
        sbQuery.append(strTransId);
        sbQuery.append("'");
      }
      sbQuery.append(" AND A.C506_STATUS_FL = 2 ");
      sbQuery.append(" ORDER BY SDATE ");
    }
    log.debug(" reportSeachSalesConsign query is " + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return resultSet;
  } // End of reportSeachSalesConsign

  /**
   * reportSeachSalesConsign - to search parts consigned for the selected set
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */

  public RowSetDynaClass reportSearchPartConsign(HashMap hmParam) throws AppError

  {
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();

    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PartNumber"));
    String strFrmDt = GmCommonClass.parseNull((String) hmParam.get("FRMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    RowSetDynaClass resultSet = null;

    sbQuery.append(" SELECT T504.C504_CONSIGNMENT_ID ID ");
    sbQuery.append(" ,T505.C205_PART_NUMBER_ID PID");
    sbQuery.append(" , T504.C701_DISTRIBUTOR_ID DID");
    sbQuery.append(" ,T505.C505_ITEM_QTY QTY");
    sbQuery.append(" , NVL(T504.C504_TRACKING_NUMBER,'-') TRACK  ");
    sbQuery.append(" ,T504.C207_SET_ID || ' - ' || GET_SET_NAME(T504.C207_SET_ID) SETID  ");
    sbQuery.append(" , DECODE(t504.c207_set_id,NULL,'ITEM','SET') SETORITEM ");
    sbQuery
        .append(" , TO_CHAR(T504.C504_SHIP_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) DDATE  ");
    sbQuery.append(" ,TO_NUMBER(TO_CHAR(T504.C504_SHIP_DATE,'yyyymmdd') ) SDATE  ");
    sbQuery.append(" ,GET_TOTAL_CONSIGNMENT_AMT( T504.C504_CONSIGNMENT_ID) TOTAL_COST  ");
    sbQuery.append(" ,T505.C505_ITEM_CONSIGNMENT_ID DUMMID ");
    sbQuery.append(" , 'C' CTYPE  ,  '' PLINE  , '' DIMAGE "); // Adding
    // setusage
    // as it is
    // in addrow
    // class for
    sbQuery.append(" FROM T504_CONSIGNMENT T504, T505_ITEM_CONSIGNMENT T505  ");
    sbQuery.append(" WHERE  T504.C701_DISTRIBUTOR_ID = ");
    sbQuery.append(strDistId);
    sbQuery.append(" AND    T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID  ");
    sbQuery.append(" AND T504.C504_STATUS_FL = '4'  ");
    sbQuery.append(" AND T504.C504_VOID_FL IS NULL ");
    sbQuery.append(" AND T505.C205_PART_NUMBER_ID = '");
    sbQuery.append(strPartID);
    sbQuery.append("' AND T504.C504_TYPE in (4110, 4129)");
    sbQuery.append("AND TRIM (T505.C505_CONTROL_NUMBER) IS NOT NULL   ");

    // Second query to fetch the returns
    sbQuery.append(" UNION   ");
    sbQuery.append(" SELECT T506.C506_RMA_ID ID  ");
    sbQuery.append(" ,T507.C205_PART_NUMBER_ID  PID ");
    sbQuery.append(" , T506.C701_DISTRIBUTOR_ID DID  ");
    sbQuery.append(" ,T507.C507_ITEM_QTY * -1  QTY");
    sbQuery.append(" , '-' TRACK   ");
    sbQuery.append(" ,T506.C207_SET_ID  || ' - ' || GET_SET_NAME(T506.C207_SET_ID) SETID  ");
    sbQuery.append(" , DECODE(t506.c207_set_id,NULL,'ITEM','SET') SETORITEM ");
    sbQuery
        .append(" ,TO_CHAR(T506.C506_RETURN_DATE,GET_RULE_VALUE('DATEFMT', 'DATEFORMAT')) DDATE  ");
    sbQuery.append(" ,TO_NUMBER(TO_CHAR(T506.C506_RETURN_DATE,'yyyymmdd') ) SDATE  ");
    sbQuery.append(" ,GET_TOTAL_RETURN_AMT (T506.C506_RMA_ID) TOTAL_COST  ");
    sbQuery.append(" ,T507.C507_RETURNS_ITEM_ID DUMMID ");
    sbQuery.append(" , 'R' CTYPE ,  '' PLINE , '' DIMAGE  ");
    sbQuery.append(" FROM T506_RETURNS T506, T507_RETURNS_ITEM T507  ");
    sbQuery.append(" WHERE T506.C506_RMA_ID = T507.C506_RMA_ID  ");
    sbQuery.append(" AND T506.C506_VOID_FL IS NULL ");
    sbQuery.append(" AND T506.C701_DISTRIBUTOR_ID =   ");
    sbQuery.append(strDistId);
    sbQuery.append(" AND T507.C205_PART_NUMBER_ID = '");
    sbQuery.append(strPartID);
    sbQuery.append("' AND T506.C506_STATUS_FL = '2'  ");
    sbQuery.append(" AND T507.C507_STATUS_FL in ('C','W') ");

    sbQuery.append(" ORDER BY SDATE  ");

    log.debug("reportSearchPartConsign " + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return resultSet;
  } // End of reportSeachSalesConsign

  /**
   * reportSearchhPartDetails -This methos fetchs part information for the selected SET ID and the
   * Distributor
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */

  public RowSetDynaClass reportSearchhPartSetDetailsOld(HashMap hmParam) throws AppError

  {
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SetNumber"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("Type"));
    // String strFrmDt =
    // GmCommonClass.parseNull((String)hmParam.get("FRMDT"));
    // String strToDt =
    // GmCommonClass.parseNull((String)hmParam.get("TODT"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    RowSetDynaClass resultSet = null;

    // Code to fetch consignment part information
    sbQuery.append(" SELECT T208.C205_PART_NUMBER_ID PID");
    sbQuery.append(" ,T205.C205_PART_NUM_DESC  PDESC");
    sbQuery.append(" , T208.C208_SET_QTY SQTY  ");
    sbQuery.append(" , NVL(T208.C208_CRITICAL_FL,'N') CRTFL");
    sbQuery
        .append(" , NVL(C208_CRITICAL_QTY,C208_SET_QTY)  CQTY , NVL(C208_CRITICAL_TAG,'-') CTAG ");
    sbQuery.append(" , " + strDistId + " DID");
    sbQuery
        .append(" ,GM_PKG_SM_PART_CONSIGN_INFO.GET_PART_CONSIGNED_COUNT(T208.C205_PART_NUMBER_ID, ");
    sbQuery.append(strDistId);
    sbQuery.append(" ) TCOUNT  ");
    sbQuery
        .append(" ,GM_PKG_SM_PART_CONSIGN_INFO.GET_PART_CROSSOVER_COUNT(T208.C205_PART_NUMBER_ID, T208.C207_SET_ID, ");
    sbQuery.append(strDistId);
    sbQuery.append(" )  CCOUNT  ");
    sbQuery
        .append(" , GM_PKG_SM_PART_CONSIGN_INFO.GET_CROSSOVER_PART_FL(T208.C205_PART_NUMBER_ID) CFL  ");
    sbQuery
        .append(" ,GM_PKG_SM_PART_CONSIGN_INFO.GET_PART_SET_USED_COUNT(T208.C205_PART_NUMBER_ID, T208.C207_SET_ID, ");
    sbQuery.append(strDistId);
    sbQuery.append(" ) SETUSAGE  ");
    sbQuery
        .append(" ,GM_PKG_SM_PART_CONSIGN_INFO.GET_PART_UNUSED_COUNT (T208.C205_PART_NUMBER_ID,  ");
    sbQuery.append(strDistId);
    sbQuery.append(") PUNUSED	  ");
    sbQuery.append(" , '' PLINE ");
    sbQuery.append(" , '' PCONS "); // Possiable consignment value
    sbQuery.append(" FROM T208_SET_DETAILS T208   ");
    sbQuery.append(" , T205_PART_NUMBER T205 	   ");
    sbQuery.append(" WHERE T208.C207_SET_ID =  ('");
    sbQuery.append(strSetId);
    sbQuery.append("')  AND  T208.C205_PART_NUMBER_ID =  T205.C205_PART_NUMBER_ID   ");
    // sbQuery.append(" AND T208.C208_INSET_FL = 'Y' ");
    // sbQuery.append(" AND T208.C208_SET_QTY <> 0 ");
    if (strType.equals("0")) {
      sbQuery.append("  AND T208.C208_CRITICAL_FL = 'Y' ");
    } else if (strType.equals("2")) {
      sbQuery.append("  AND NVL(T208.C208_CRITICAL_FL,'N')  = 'N' ");
    }
    sbQuery.append(" ORDER BY   T208.C205_PART_NUMBER_ID  ");

    log.debug("****** reportSearchhPartSetDetails ******" + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return resultSet;
  } // End of reportSeachSalesConsign

  public RowSetDynaClass reportSearchhPartSetDetails(HashMap hmParam) throws AppError

  {
    StringBuffer sbQuery = new StringBuffer();

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SetNumber"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;

    sbQuery.append(" SELECT VCONSIGNDETAIL.C207_SET_ID, VCONSIGNDETAIL.C205_PART_NUMBER_ID PID");
    sbQuery.append(" ,GET_PARTNUM_DESC (VCONSIGNDETAIL.C205_PART_NUMBER_ID) PDESC");
    sbQuery.append(" , NVL(T208.C208_SET_QTY,0) SQTY  ");
    sbQuery
        .append(" , get_code_name( GET_PARTNUM_PRODUCT(VCONSIGNDETAIL.C205_PART_NUMBER_ID)) prodfamily  ");
    sbQuery.append(" , NVL(T208.C208_CRITICAL_FL,'N') CRTFL");
    sbQuery.append(" , VCONSIGNDETAIL.QTYCONSIGNED SETUSAGE, LISTVALUE ");
    sbQuery.append(" , " + strDistId + " DID");
    sbQuery.append(" , '' PLINE ");
    sbQuery
        .append(" FROM (SELECT T730.C207_SET_ID, T731.C205_PART_NUMBER_ID, SUM (T731.C731_ITEM_QTY) QTYCONSIGNED");
    sbQuery.append(" , SUM (T731.C731_ITEM_QTY * T731.C731_ITEM_PRICE) LISTVALUE ");
    sbQuery.append(" FROM T730_VIRTUAL_CONSIGNMENT T730, T731_VIRTUAL_CONSIGN_ITEM T731");
    sbQuery.append(" WHERE T730.C730_VIRTUAL_CONSIGNMENT_ID = T731.C730_VIRTUAL_CONSIGNMENT_ID ");
    sbQuery.append(" AND T730.C207_SET_ID IN  (");
    sbQuery.append(" SELECT t207a.c207_link_set_id  ");
    sbQuery.append(" 		FROM t207a_set_link t207a ");
    sbQuery.append(" 		WHERE t207a.C207_MAIN_SET_ID = '");
    sbQuery.append(strSetId);
    sbQuery.append("' AND t207a.C901_TYPE <> 20004 ");
    sbQuery.append(" UNION ALL ");
    sbQuery.append(" SELECT '");
    sbQuery.append(strSetId);
    sbQuery.append("' FROM DUAL ");
    sbQuery.append(")  AND  T730.C701_DISTRIBUTOR_ID = ('");
    sbQuery.append(strDistId);
    sbQuery.append("') GROUP BY  T730.C207_SET_ID, T731.C205_PART_NUMBER_ID) VCONSIGNDETAIL ");
    sbQuery.append(" , T208_SET_DETAILS T208 ");
    sbQuery
        .append(" WHERE VCONSIGNDETAIL.C207_SET_ID = T208.C207_SET_ID (+) AND T208.C208_VOID_FL IS NULL ");
    sbQuery.append(" AND VCONSIGNDETAIL.C205_PART_NUMBER_ID = T208.C205_PART_NUMBER_ID (+) ");
    sbQuery.append(" ORDER BY crtfl DESC, prodfamily,PID  ");

    log.debug("****** reportSearchhPartSetDetails ******" + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return resultSet;
  } // End of reportSeachSalesConsign

  /**
   * reportSearchCrossOverPart
   * 
   * @return HashMap
   */
  public RowSetDynaClass reportSearchCrossOverPart(HashMap hmParam) throws AppError

  {

    StringBuffer sbQuery = new StringBuffer();
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PartNumber"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    RowSetDynaClass resultSet = null;

    // Code to fetch critical part information
    sbQuery.append(" SELECT T207.C207_SET_ID SID, T207.C207_SET_NM SNM ");
    sbQuery.append(" , T208.C208_SET_QTY SQTY ");

    sbQuery.append(" , GM_PKG_SM_PART_CONSIGN_INFO.GET_SET_USED_COUNT(T207.C207_SET_ID, ");
    sbQuery.append(strDistId);
    sbQuery.append(" ) SETCOUNT");

    sbQuery
        .append(" , GM_PKG_SM_PART_CONSIGN_INFO.GET_PART_SET_USED_COUNT(T208.C205_PART_NUMBER_ID, T207.C207_SET_ID, ");
    sbQuery.append(strDistId);
    sbQuery.append(" ) SETPARTCOUNT ");

    sbQuery.append(" , '' PLINE ");
    sbQuery.append(" FROM T207_SET_MASTER T207, T208_SET_DETAILS T208 ");
    sbQuery.append(" WHERE T207.C207_SET_ID = T208.C207_SET_ID ");
    sbQuery.append(" AND T207.C901_SET_GRP_TYPE = 1601 ");
    sbQuery.append(" AND T208.C208_CRITICAL_FL = 'Y' ");
    sbQuery.append(" AND T208.C205_PART_NUMBER_ID = '");
    sbQuery.append(strPartID);
    sbQuery.append("' ");
    sbQuery.append(" ORDER BY T207.C207_SEQ_NO, T207.C207_SET_ID ");

    log.debug("****** reportSearchCrossOverPart ******" + sbQuery.toString());

    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return resultSet;

  }

  /**
   * reportVirtualVSActual - this method will be used to find virtual / actual consignment for the
   * selected distributor
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public RowSetDynaClass reportVirtualvsActualByDist(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;

    getCrossTabFilterCondition(hmParam);

    String strSearch = GmCommonClass.parseNull((String) hmParam.get(("Cbo_Search")));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
    String strSetNumFormat = GmCommonClass.parseNull((String) hmParam.get("SetId"));

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SetId"));
    String strSystem = GmCommonClass.parseNull((String) hmParam.get("SYSTEM"));
    String strHierarchy = GmCommonClass.parseNull((String) hmParam.get("HIERARCHY"));
    String strCategory = GmCommonClass.parseNull((String) hmParam.get("CATEGORY"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());

    if (strDistId.equals("")) {
      return null;
    }

    if (strSearch.equals("90181")) {
      strSetNumFormat = ("^").concat(strSetNumFormat);
      strSetNumFormat = strSetNumFormat.replaceAll(",", "|^");
    } else if (strSearch.equals("90182")) {
      strSetNumFormat = strSetNumFormat.replaceAll(",", "\\$|");
      strSetNumFormat = strSetNumFormat.concat("$");
    } else {
      strSetNumFormat = strSetNumFormat.replaceAll(",", "|");
    }
    log.debug(" strSetNumFormat value is " + strSetNumFormat);

    // Code to fetch consignment information
    sbQuery.append(" SELECT T207.C207_SET_ID SID,  T207.C207_SET_NM SNAME,  ");
    sbQuery.append(" NVL(VSET.VCOUNT , 0) VCOUNT , ");
    sbQuery.append(strDistId + " DID ");
    sbQuery.append(" , GET_LOG_FLAG (" + strDistId
        + " || 'R' ||  T207.C207_SET_ID   , 1207  )  LOG ");
    sbQuery.append(" , 'D' DIFFFLAG ");
    sbQuery.append(" , NVL(CSET.CCOUNT, 0) COUNT , NVL( RSET.RCOUNT, 0) RCOUNT ");
    sbQuery.append(" , ( NVL(CSET.CCOUNT, 0) - NVL( RSET.RCOUNT, 0) ) FCOUNT ");
    sbQuery.append(" , '' PLINE ");
    sbQuery.append(" ,GET_SET_NAME (C207_SET_SALES_SYSTEM_ID) SYSTEMNAME");
    sbQuery.append(" ,GET_CODE_NAME_ALT(C901_HIERARCHY) HIERARCHY");
    sbQuery.append(" ,GET_CODE_NAME (C207_CATEGORY) CATEG ");
    sbQuery.append(" ,get_code_name (C207_TYPE) TYPE ");

    sbQuery.append(" FROM T207_SET_MASTER  T207  ");
    /*
     * if(!strDistId.equals("0")){ sbQuery.append(getAccessFilterClause()); }
     */
    // Below code to fetch the virtual set count
    sbQuery.append(", ( SELECT C207_SET_ID, COUNT(1)VCOUNT  FROM T730_VIRTUAL_CONSIGNMENT T730 ");
    if (!strDistId.equals("0")) {
      sbQuery.append(" WHERE T730.C701_DISTRIBUTOR_ID =  ");
      sbQuery.append(strDistId);
    }
    sbQuery.append(" GROUP BY C207_SET_ID ) VSET, ");

    // Below code to fetch actual set consignment count
    sbQuery.append(" (SELECT C207_SET_ID, COUNT(1)CCOUNT FROM T504_CONSIGNMENT T504 ");
    sbQuery.append(" WHERE ");
    if (!strDistId.equals("0")) {
      sbQuery.append(" T504.C701_DISTRIBUTOR_ID =  ");
      sbQuery.append(strDistId);
      sbQuery.append(" AND ");
    }
    sbQuery.append(" T504.C504_STATUS_FL = 4 ");
    sbQuery.append(" AND T504.C504_VOID_FL IS NULL ");
    sbQuery.append(" AND T504.C504_TYPE IN(4110,4129) ");
    sbQuery.append(" AND T504.C207_SET_ID IS NOT NULL ");
    sbQuery.append(" GROUP BY C207_SET_ID) CSET,  ");

    // Below code to fetch actual return consignment count
    sbQuery.append(" ( SELECT C207_SET_ID, COUNT(1) RCOUNT FROM T506_RETURNS T506 ");
    sbQuery.append(" WHERE T506.C701_DISTRIBUTOR_ID =   ");
    sbQuery.append(strDistId);
    sbQuery.append(" AND T506.C207_SET_ID IS NOT NULL ");
    sbQuery.append(" AND T506.C506_STATUS_FL = 2  ");
    sbQuery.append(" AND T506.C506_VOID_FL IS NULL ");
    sbQuery.append(" GROUP BY C207_SET_ID ) RSET ,T2080_SET_COMPANY_MAPPING T2080 ");

    // final where condition
    sbQuery.append(" WHERE T207.C207_SET_ID  = VSET.C207_SET_ID (+) ");
    sbQuery.append(" AND T207.C207_SET_ID = CSET.C207_SET_ID  (+)  ");
    sbQuery.append(" AND T207.C207_SET_ID = RSET.C207_SET_ID  (+)  ");
    sbQuery.append(" AND T207.C207_SET_ID       = T2080.C207_SET_ID   ");
    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
    sbQuery.append(" AND T2080.C1900_COMPANY_ID = " + strCompanyId);
    sbQuery.append(" AND T207.C901_SET_GRP_TYPE = 1601 ");
    sbQuery.append(" AND T207.C207_SET_ID NOT LIKE 'MER%' ");

    /*
     * if(!strDistId.equals("0")){ sbQuery.append(" AND V700.D_ID = "); sbQuery.append(strDistId); }
     */
    // filter for System
    if (!strSystem.equals("0")) {
      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN ('");
      sbQuery.append(strSystem);
      sbQuery.append("')");
    }
    // filter for Hierarchy
    if (!strHierarchy.equals("0")) {
      sbQuery.append(" AND C901_HIERARCHY IN ('");
      sbQuery.append(strHierarchy);
      sbQuery.append("')");
    }
    // filter for Category
    if (!strCategory.equals("0")) {
      sbQuery.append(" AND C207_CATEGORY IN ('");
      sbQuery.append(strCategory);
      sbQuery.append("')");
    }
    // filter for Type
    if (!strType.equals("0")) {
      sbQuery.append(" AND C207_TYPE IN ('");
      sbQuery.append(strType);
      sbQuery.append("')");
    }
    // filter for Set Name
    if (!strSetId.equals("")) {
      sbQuery.append(" AND UPPER(T207.C207_SET_NM) like ('");
      sbQuery.append(strSetNumFormat.toUpperCase());
      sbQuery.append("%')");
    }
    sbQuery.append(" ORDER BY SNAME ");

    log.debug("reportSeachPartConsign " + sbQuery.toString());

    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return resultSet;
  } // End of reportVirtualVSActual

  /**
   * reportVirtualVSActual - this method will be used to find virtual / actual consignment for the
   * selected distributor
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public RowSetDynaClass reportVirtualvsActualBySet(HashMap hmParam) throws AppError

  {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;
    getCrossTabFilterCondition(hmParam);

    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SetNumber"));
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    if (strSetID.equals("")) {
      return null;
    }
    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "T701");

    // Code to fetch consignment information
    sbQuery.append(" SELECT T701.C701_DISTRIBUTOR_ID DID,  " + strDistNmColumn + " SNAME,  ");
    sbQuery.append(" NVL(VSET.VCOUNT , 0) VCOUNT , '");
    sbQuery.append(strSetID + "' SID ");

    sbQuery.append(" , GET_LOG_FLAG (T701.C701_DISTRIBUTOR_ID || 'R" + strSetID
        + "' , 1207  )  LOG ");
    sbQuery.append(" , 'D' DIFFFLAG ");

    sbQuery.append(" , NVL(CSET.CCOUNT, 0) COUNT , NVL( RSET.RCOUNT, 0) RCOUNT ");
    sbQuery.append(" , ( NVL(CSET.CCOUNT, 0) - NVL( RSET.RCOUNT, 0) ) FCOUNT ");
    sbQuery.append(" , '' PLINE ");
    sbQuery.append(" FROM T701_DISTRIBUTOR T701  ");
    sbQuery.append(getAccessFilterClause());
    // Below code to fetch the virtual set count
    sbQuery
        .append(" ,( SELECT C701_DISTRIBUTOR_ID, COUNT(1)VCOUNT  FROM T730_VIRTUAL_CONSIGNMENT T730 ");
    sbQuery.append(" WHERE C207_SET_ID =  '");
    sbQuery.append(strSetID + "'");
    sbQuery.append(" GROUP BY C701_DISTRIBUTOR_ID ) VSET, ");

    // Below code to fetch actual set consignment count
    sbQuery.append(" (SELECT C701_DISTRIBUTOR_ID, COUNT(1)CCOUNT FROM T504_CONSIGNMENT T504 ");
    sbQuery.append(" WHERE T504.C207_SET_ID =  '");
    sbQuery.append(strSetID);
    sbQuery.append("' AND T504.C504_STATUS_FL = 4 ");
    sbQuery.append(" AND T504.C504_VOID_FL IS NULL ");
    sbQuery.append(" AND T504.C504_TYPE in (4110, 4129) ");
    sbQuery.append(" AND T504.C207_SET_ID IS NOT NULL ");
    sbQuery.append(" GROUP BY C701_DISTRIBUTOR_ID) CSET,  ");

    // Below code to fetch actual return consignment count
    sbQuery.append(" ( SELECT C701_DISTRIBUTOR_ID, COUNT(1) RCOUNT FROM T506_RETURNS T506 ");
    sbQuery.append(" WHERE T506.C207_SET_ID =   '");
    sbQuery.append(strSetID);
    sbQuery.append("' AND T506.C207_SET_ID IS NOT NULL ");
    sbQuery.append(" AND T506.C506_STATUS_FL = 2  ");
    sbQuery.append(" AND T506.C506_VOID_FL IS NULL ");
    sbQuery.append(" GROUP BY C701_DISTRIBUTOR_ID ) RSET  ");

    // final where condition
    sbQuery.append(" WHERE T701.C701_DISTRIBUTOR_ID  = VSET.C701_DISTRIBUTOR_ID (+) ");
    sbQuery.append(" AND T701.C701_DISTRIBUTOR_ID = CSET.C701_DISTRIBUTOR_ID  (+)  ");
    sbQuery.append(" AND T701.C701_DISTRIBUTOR_ID = RSET.C701_DISTRIBUTOR_ID  (+)  ");
    sbQuery.append(" AND   V700.D_ID = T701.C701_DISTRIBUTOR_ID ");
    sbQuery.append(" AND  T701.C1900_COMPANY_ID = " + strCompanyId);
    // sbQuery.append(" AND T207.C901_SET_GRP_TYPE = 1601 ");
    // sbQuery.append(" AND T207.C207_SET_ID NOT LIKE 'MER%' ");
    sbQuery.append(" ORDER BY SNAME ");

    log.debug("reportSeachPartConsign " + sbQuery.toString());

    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return resultSet;
  } // End of reportVirtualVSActual

  /**
   * reportSeachSalesConsign -
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public RowSetDynaClass reportSeachPartConsign(HashMap hmParam) throws AppError

  {
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SetNumber"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DistributorID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("Type"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;

    // Code to fetch consignment information

    sbQuery.append(" SELECT  T205.C205_PART_NUMBER_ID PID, T205.C205_PART_NUM_DESC PDESC");
    sbQuery.append(" ,T730.C701_DISTRIBUTOR_ID DID ");
    sbQuery
        .append(" , gm_pkg_sm_part_consign_info.get_part_consigned_count(T205.C205_PART_NUMBER_ID, T730.C701_DISTRIBUTOR_ID) TCOUNT");
    sbQuery.append(" , T731.C731_ITEM_PRICE  PRICE  ");
    sbQuery
        .append(" , SUM(gm_pkg_sm_part_consign_info.get_part_consigned_count(T205.C205_PART_NUMBER_ID, T730.C701_DISTRIBUTOR_ID))  * T731.C731_ITEM_PRICE TPRICE ");
    sbQuery.append(" ,'' PLINE ");
    sbQuery.append(" FROM T730_VIRTUAL_CONSIGNMENT T730,  ");
    sbQuery.append(" T731_VIRTUAL_CONSIGN_ITEM T731,  ");
    sbQuery.append(" T205_PART_NUMBER T205  ");
    sbQuery
        .append(" WHERE 	T730.C730_VIRTUAL_CONSIGNMENT_ID  = T731.C730_VIRTUAL_CONSIGNMENT_ID  ");
    // to fetch based on distributor rep filter
    if (strDistId != null && !strDistId.equals("null")) {
      sbQuery.append(" AND T730.C701_DISTRIBUTOR_ID = ");
      sbQuery.append(strDistId);
    }

    // Query to fetch the main set other set information
    sbQuery.append(" AND T730.C207_SET_ID IN ");

    // If main group is clicked the execute the below query
    if (strType.equals("BYGPART")) {
      sbQuery.append(" (SELECT CONNECT_BY_ROOT t207a.c207_link_set_id l_set_id ");
      sbQuery.append(" FROM t207a_set_link t207a  ");
      sbQuery.append(" WHERE t207a.c207_main_set_id IN (SELECT t207.c207_set_id  ");
      sbQuery.append(" FROM t207_set_master t207  ");
      sbQuery.append(" WHERE t207.c207_set_id = '");
      sbQuery.append(strSetId);
      sbQuery.append("' ) CONNECT BY PRIOR c207_main_set_id = t207a.c207_link_set_id ");
      sbQuery.append(" AND CONNECT_BY_ROOT t207a.C901_TYPE  <> 20004) ");

    } else {
      sbQuery.append(" (SELECT T207A.C207_LINK_SET_ID FROM T207A_SET_LINK T207A ");
      sbQuery.append(" WHERE   ");
      sbQuery.append(" T207A.C207_MAIN_SET_ID = '");
      sbQuery.append(strSetId);
      sbQuery.append("' )");
    }

    sbQuery.append("  AND T205.C205_PART_NUMBER_ID  = T731.C205_PART_NUMBER_ID   ");
    sbQuery.append(" GROUP BY T205.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC, ");
    sbQuery.append(" T730.C701_DISTRIBUTOR_ID, T731.C731_ITEM_PRICE ");

    log.debug("reportSeachPartConsign " + sbQuery.toString());

    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return resultSet;
  } // End of reportSeachSalesConsign

  /**
   * reportVSetSummaryByGroup - This Method is used to fetch VSet Summary Along with sales
   * information by Group
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportVSetSummaryByGroup(HashMap hmparam) throws AppError {

    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    HashMap hmreturn = new HashMap(); // Final value report details
    HashMap hmLinkDetails = new HashMap();
    HashMap hmapFinal = new HashMap();

    // result data stores
    ArrayList alSetSummary = new ArrayList();
    ArrayList alConsignAvgUse = new ArrayList();
    ArrayList alConsignMetrics = new ArrayList();
    ArrayList alReportConsignTarget = new ArrayList();
    ArrayList alReportPercentage = new ArrayList();
    ArrayList alAdditionalInventory = new ArrayList();
    ArrayList alSystemMapping = new ArrayList();
    ArrayList alAssociatedSystems = new ArrayList();
    ArrayList alLoanerBaselineSummary = new ArrayList();

    GmSalesYTDBean gmSalesYTD = new GmSalesYTDBean();

    // ****************************************************************
    // To fetch actual sales value information (last 6 month) from the
    // Group list (active for last 6 month)
    // ****************************************************************
    String strType = GmCommonClass.parseNull((String) hmparam.get("ConsignmentType"));
    String strSalesType = GmCommonClass.parseNull((String) hmparam.get("SalesType"));
    log.debug(" hmparam value " + hmparam);

    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportYTDByCOGS
      hmreturn = gmSalesYTD.reportYTDByCOGS(hmparam);
    } else {
      hmreturn = gmSalesYTD.reportYTDByDetailSummary(hmparam);
    }
    log.debug(" hmreturn after reportYTDByDetailSummary metrics " + hmreturn);
    log.debug(" Loaner bvaseline set summary " + hmreturn);
    alSetSummary = reportBaselineSummary(hmparam);

    hmLinkDetails = getLinkInfo("SET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alSetSummary, hmLinkDetails);
    log.debug(" hmreturn after alSetSummary metrics " + hmreturn);

    log.debug(" strSalesType ----------------- " + strSalesType);
    // For Loaner Baseline sets
    if (!strSalesType.equals("50300")) {
      alLoanerBaselineSummary = reportLoanerBaselineSummary(hmparam);
      hmLinkDetails = getLinkInfo("LOANERBASELINESET");
      hmreturn = gmCrossReport.linkCrossTab(hmreturn, alLoanerBaselineSummary, hmLinkDetails);
    }

    // to find the Average cases and Annualized Revenue
    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportConsignAvguseByCOGS
      alConsignAvgUse = reportConsignAvguseByCOGS(hmparam);
    } else {
      alConsignAvgUse = reportConsignAvguseByDetailSummary(hmparam);
    }
    log.debug(" alConsignAvgUse " + alConsignAvgUse);
    hmLinkDetails = getLinkInfo("AVG");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignAvgUse, hmLinkDetails);
    log.debug(" hmreturn after alConsignAvgUse metrics " + hmreturn);

    // to find the metrics
    alConsignMetrics = calculateConsignMetrics(alSetSummary, alConsignAvgUse, hmparam);
    hmLinkDetails = getLinkInfo("MET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignMetrics, hmLinkDetails);
    log.debug(" hmreturn after lijking metrics " + hmreturn);

    // Associated Mapping code for system.
    alSystemMapping = fetchSystemMapping();
    alAssociatedSystems =
        fetchAssociatedSystemCases(alSystemMapping, (ArrayList) hmreturn.get("Details"));
    hmLinkDetails = getLinkInfo("SYSMAP");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alAssociatedSystems, hmLinkDetails);
    log.debug(" hmreturn after getting associated systems metrics " + hmreturn);

    // to show Additional Inventory details
    alAdditionalInventory = reportAdditionalInventory(hmparam);

    // log.debug(" Additional Inveotyr " + alAdditionalInventory);
    hmLinkDetails = getLinkInfo("ADDITIONALINVENTORY");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alAdditionalInventory, hmLinkDetails);
    // log.debug(" hmreturn with Add invne " + hmreturn);
    hmreturn.put("HMPARAM", hmparam);
    hmreturn = modifyFooterColumn(hmreturn);

    hmapFinal.put("CROSSVALE", hmreturn);

    return hmapFinal;
  }

  /**
   * fetchAssociatedSystemCases - This method used to add the System associate set information
   * 
   * For example - 300.121 COALITION associate to 300.117 COLONIAL system
   * 
   * @param alSysMap
   * @param alData
   * @return ArrayList
   * @throws AppError
   */

  public ArrayList fetchAssociatedSystemCases(ArrayList alSysMap, ArrayList alData) throws AppError {
    DecimalFormat dfTwoDForm = new DecimalFormat("#.#");
    Iterator itDataDetails = alData.iterator();
    ArrayList alAssociatedSystem = new ArrayList();
    HashMap hmTempData = new HashMap();
    HashMap hmTempDetails = new HashMap();
    HashMap hmAllData = new HashMap();
    HashMap hmMainRowObj = new HashMap();
    HashMap hmAsstRowObj = new HashMap();
    Iterator itDetails = alSysMap.iterator();
    String strMainSys = "";
    String strAsstSys = "";
    // String strID = "";
    String strMainCaseCnt = "";
    String strAsstCaseCnt = "";
    String strMainNm = "";
    String strAsstNm = "";
    String strMain = "";
    String strAsst = "";
    double dbTotalAsstCnt = 0.0;
    while (itDataDetails.hasNext()) {
      hmAllData = (HashMap) itDataDetails.next();
      hmTempDetails.put(hmAllData.get("ID"), hmAllData);
    }
    while (itDetails.hasNext()) {
      hmTempData = (HashMap) itDetails.next();
      strMainSys = (String) hmTempData.get("MAIN");
      strAsstSys = (String) hmTempData.get("ASST");
      hmMainRowObj = GmCommonClass.parseNullHashMap((HashMap) hmTempDetails.get(strMainSys));
      hmAsstRowObj = GmCommonClass.parseNullHashMap((HashMap) hmTempDetails.get(strAsstSys));

      // PMT-27002 : Currently, Set available or not - Just added the associate information (All the
      // values are blank).

      // So, to avoid the blank set in the Loaner tab - checking the hashmap not null condition.

      if (!hmMainRowObj.isEmpty()) {
        strMainCaseCnt = GmCommonClass.parseNull((String) hmMainRowObj.get(strTurnsLabel));
        strMainCaseCnt = strMainCaseCnt.equals("") ? "0" : strMainCaseCnt;
        strMainNm = GmCommonClass.parseNull((String) hmMainRowObj.get("Name"));
        strMain = strMainNm + " " + dfTwoDForm.format(Double.parseDouble(strMainCaseCnt));
        strAsstCaseCnt = GmCommonClass.parseNull((String) hmAsstRowObj.get(strTurnsLabel));
        strAsstCaseCnt = strAsstCaseCnt.equals("") ? "0" : strAsstCaseCnt;
        strAsstNm = GmCommonClass.parseNull((String) hmAsstRowObj.get("Name"));
        strAsst = strAsstNm + " " + dfTwoDForm.format(Double.parseDouble(strAsstCaseCnt));
        dbTotalAsstCnt = Double.parseDouble(strAsstCaseCnt) + Double.parseDouble(strMainCaseCnt);

        hmAsstRowObj.put(strTurnsLabel, String.valueOf(dbTotalAsstCnt));

        HashMap hmTemp = new HashMap();

        hmTemp.put("ID", strAsstSys);
        hmTemp.put("ASSTCASES", strMain + " + " + strAsst);
        alAssociatedSystem.add(hmTemp);

      } // end if hmMainRowObj

    }
    return alAssociatedSystem;
  }


  public String getFramePartion(String strAction) {

    String strPartition = "";

    if (strAction.equals("LoadPart")) {
      strPartition = "T205.C205_PART_NUMBER_ID";
    } else if (strAction.equals("LoadDistributor")) {
      strPartition = " D_ID ";
    }

    else if (strAction.equals("LoadDCONS") || strAction.equals("LoadDistSummary")) {
      strPartition = " D_ID ";
    }

    else if (strAction.equals("LoadGCONS")) {
      strPartition = " V207.C207_SET_ID ";
    } else if (strAction.equals("LoadSummary") || strAction.equals("LoadAD")) {
      strPartition = " v700.AD_ID ";
    } else if (strAction.equals("LoadVP")) {
      strPartition = " v700.VP_ID ";
    }

    return strPartition;
  }

  /**
   * reportAdditionalInventory - fetches Additional Inventory details
   */
  public ArrayList reportAdditionalInventory(HashMap hmParam) throws AppError {
    ArrayList alAdditionalInventory = new ArrayList();
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmCrossTabReport = new HashMap();
    GmCrossTabReport gmCrossTabReport = new GmCrossTabReport();
    String strDistributorID = (String) hmParam.get("DistributorID");
    String strType = GmCommonClass.parseNull((String) hmParam.get("ConsignmentType"));
    String strTotalColumn = "t730.c730_total_cost";
    String strShowAI = GmCommonClass.parseNull((String) hmParam.get("SHOWAI"));

    if (!strShowAI.equals("Y")) {
      return alAdditionalInventory;
    }

    String strFrameColumQuery = getFrameColumn(strAction);
    log.debug(" strADID is " + strADID);

    String strFrameGroupQuery = getFrameGroup(strAction);
    // if(strAction.equals("LoadGCONS")){
    strFrameGroupQuery = strFrameGroupQuery + ", v207.C901_CUSTOM_HIERARCHY ";
    // }
    log.debug(" strFrameGroupQuery is " + strFrameGroupQuery);

    String strPartition = getFramePartion(strAction);
    log.debug(" strPartition is " + strPartition);

    log.debug(" Type is " + strType);
    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportConsignAvguseByCOGS
      strTotalColumn = "C730_TOTAL_COGS_COST";
    }

    sbHeaderQuery
        .append(" SELECT DECODE(T901.C901_CODE_ID,20700,'PERCENT',T901.C902_CODE_NM_ALT) CODENMALT ");
    sbHeaderQuery.append(" FROM T901_CODE_LOOKUP T901 ");
    sbHeaderQuery.append(" WHERE T901.C901_CODE_ID IN (20700, 20702, 20704, 20705) ");

    sbQuery.append(" SELECT ID  , NAME,TYP , vtotal ");
    sbQuery.append(" FROM ( ");
    sbQuery.append(strFrameColumQuery);
    sbQuery
        .append(" , DECODE(C901_CUSTOM_HIERARCHY,20700,'PERCENT',GET_CODE_NAME_ALT(v207.C901_CUSTOM_HIERARCHY)) TYP ");
    sbQuery.append(" , DECODE(C901_CUSTOM_HIERARCHY ,20700, ");
    sbQuery.append(" 100 - trunc( ratio_to_report(SUM (");
    sbQuery.append(strTotalColumn);
    sbQuery.append(")) over(PARTITION BY  ");
    sbQuery.append(strPartition);
    sbQuery.append(" ) * 100, 2) ");
    sbQuery.append(" , NVL (SUM (");
    sbQuery.append(strTotalColumn);
    sbQuery.append("), 0)) vtotal ");
    sbQuery.append("  FROM t730_virtual_consignment t730 ");
    sbQuery.append(" , v207a_set_consign_link v207 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t730.c207_set_id = v207.c207_actual_set_id ");
    sbQuery.append(" AND v700.d_id = t730.c701_distributor_id ");
    sbQuery.append(" AND v700.d_active_fl = 'Y'");


    if (strDistributorID != null && !strDistributorID.equals("")) {
      sbQuery.append("  AND T730.C701_DISTRIBUTOR_ID = ");
      sbQuery.append(strDistributorID);
    }
    if (strADID != null && !strADID.equals("") && !strADID.equals("null") && !strADID.equals("0")) {
      sbQuery.append(" AND V700.AD_ID = " + strADID);
    }
    if (strSetNumber != null && !strSetNumber.equals("") && !strSetNumber.equals("null")
        && !strSetNumber.equals("0")) {
      sbQuery.append(" AND  V207.C207_SET_ID = '" + strSetNumber + "'");
    }
    if (strVPRegionID != null && strVPRegionID != ("") && !strVPRegionID.equals("null")
        && strVPRegionID != ("0")) {
      sbQuery.append(" AND  V700.VP_ID = '" + strVPRegionID + "'");
    }
    sbQuery.append(strFrameGroupQuery);
    sbQuery.append(" )");
    log.debug(" alAdditionalInventory query " + sbQuery);


    hmCrossTabReport =
        gmCrossTabReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbQuery.toString());
    alAdditionalInventory = (ArrayList) hmCrossTabReport.get("Details");

    log.debug(" alAdditionalInventory " + alAdditionalInventory);

    return alAdditionalInventory;
  }

  /**
   * reportAdditionalInventory - fetches Additional Inventory details
   */
  public ArrayList reportAIQuartetlyQuota(HashMap hmParam) throws AppError {

    ArrayList alAIQuota = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strFrameColumQuery = getFrameColumn(strAction);
    String strFrameGroupQuery = getFrameGroup(strAction);

    sbQuery.append(strFrameColumQuery);
    sbQuery
        .append(" , COUNT(DISTINCT v700.ad_id) * SUM(T709.C709_QUOTA_BREAKUP_AMT)/ COUNT(T709.C709_QUOTA_BREAKUP_AMT) quota ");
    sbQuery.append(" , SUM(T733.C733_TRANSACTION_QTY * T733.C733_PRICE ) quota_to_date ");
    sbQuery.append(" , SUM(0) AIQUOTAPERCENTAGE ");
    // will be enabled once budget is set
    // sbQuery.append(" , round(SUM(T733.C733_TRANSACTION_QTY *
    // T733.C733_PRICE )*100/ ( COUNT(DISTINCT v700.ad_id) * SUM
    // (t709.c709_quota_breakup_amt) / COUNT (t709.c709_quota_breakup_amt))
    // ,2) AIQUOTAPERCENTAGE ");
    sbQuery.append(" FROM T733_AI_QUOTA_LOG T733, T709_QUOTA_BREAKUP T709 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE T733.C701_DISTRIBUTOR_ID = v700.d_id ");
    sbQuery.append(" AND T709.C709_REFERENCE_ID = V700.AD_ID ");
    sbQuery.append(" AND T733.C733_LOAD_DATE >= TRUNC (CURRENT_DATE, 'Q') ");
    sbQuery.append(" AND T733.C733_LOAD_DATE <=  ADD_MONTHS (TRUNC (CURRENT_DATE, 'Q'), 3) - 1 ");
    sbQuery.append(" AND T709.C901_TYPE = 20751 ");
    sbQuery.append(" AND T709.C709_START_DATE = TRUNC (CURRENT_DATE, 'Q') ");

    if (strDistributorID != null && !strDistributorID.equals("")) {
      sbQuery.append("  AND  V700.D_ID = ");
      sbQuery.append(strDistributorID);
    }
    if (strADID != null && !strADID.equals("") && !strADID.equals("null") && !strADID.equals("0")) {
      sbQuery.append(" AND V700.AD_ID = " + strADID);
    }
    if (strSetNumber != null && !strSetNumber.equals("") && !strSetNumber.equals("null")
        && !strSetNumber.equals("0")) {
      sbQuery.append(" AND  T733.C207_SET_ID = '" + strSetNumber + "'");
    }
    if (strVPRegionID != null && strVPRegionID != ("") && !strVPRegionID.equals("null")
        && strVPRegionID != ("0")) {
      sbQuery.append(" AND  V700.VP_ID = '" + strVPRegionID + "'");
    }
    sbQuery.append(strFrameGroupQuery);

    log.debug(" Query for AI Quota data " + sbQuery.toString());
    alAIQuota = gmDBManager.queryMultipleRecords(sbQuery.toString());

    log.debug(" AI Quota is " + alAIQuota);

    return alAIQuota;
  }

  /**
   * calculateConsignPercentage - This Method is used to fetch the percentage of cases for Set Group
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList calculateConsignPercentage(HashMap hmReturn) {
    ArrayList alConsignPercentage = new ArrayList();
    ArrayList alDetails = (ArrayList) hmReturn.get("Details");
    Iterator itrDetails = alDetails.iterator();
    HashMap hmTemp = new HashMap();
    HashMap hmResultData;
    String strMetricCases = "";
    String strVtotal = "";
    String strTargetCases = "";
    double dMetricCases;
    double dVTotal;
    double dTargetCases;
    double dPercentageCases;
    double dSetsBySales;
    double dValue;

    while (itrDetails.hasNext()) {
      hmTemp = (HashMap) itrDetails.next();
      // log.debug("hmTemp value"+hmTemp);
      strMetricCases = (String) hmTemp.get("#Cases<BR> per Month");
      strVtotal = (String) hmTemp.get(strBaselineName);
      strTargetCases = (String) hmTemp.get("#C/M/S<br> Target");

      dMetricCases =
          (strMetricCases != null && !strMetricCases.equals("")) ? Double
              .parseDouble(strMetricCases) : 0;
      dVTotal = (strVtotal != null && !strVtotal.equals("")) ? Double.parseDouble(strVtotal) : 0;
      dTargetCases =
          (strTargetCases != null && !strTargetCases.equals("")) ? Double
              .parseDouble(strTargetCases) : 0;
      /*
       * if(hmTemp.get("ID") != null && !((String)hmTemp.get("ID")).equals("")) {
       */
      // Add ID
      hmResultData = new HashMap();
      hmResultData.put("ID", hmTemp.get("ID"));
      // Calculating Percentage for no. of cases for set group
      dPercentageCases =
          (dVTotal > 0 && dTargetCases > 0) ? (dMetricCases / (dVTotal * dTargetCases)) * 100 : 0;
      hmResultData.put("PERCENTAGE", String.valueOf(dPercentageCases));
      // Calculating No.of sets by Sales
      dSetsBySales = (dTargetCases > 0) ? dMetricCases / dTargetCases : 0;
      // if No. of sets is 0 there cant be any sets by sales.
      dSetsBySales = (dVTotal == 0) ? 0 : dSetsBySales;
      hmResultData.put("SETSBYSALES", String.valueOf(dSetsBySales));
      // Calculating SETSTORETURN + / -
      dValue = (dSetsBySales - dVTotal < 0) ? -(dSetsBySales - dVTotal) : 0;
      // if the no. of sets is the same as sets to return, means that the
      // sets are instrument sets.
      dValue = (dValue == dVTotal) ? 0 : dValue;
      hmResultData.put("SETSTORETURN", String.valueOf(dValue));

      alConsignPercentage.add(hmResultData);
      // }
    }

    return alConsignPercentage;
  }

  /**
   * reportConsignTarget - This Method is used to fetch the target for Set Group
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportConsignTarget(HashMap hmParam) throws AppError {
    ArrayList alReportConsignTarget = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    getCrossTabFilterCondition(hmParam);
    String strType = GmCommonClass.parseNull((String) hmParam.get("ConsignmentType"));
    String strTargetListQuery = ", V240A.C240_TARGET_LIST TARGETLIST ";
    log.debug(" Consignment Type is " + strType);

    if (strType.equals("50420"))// COGS
    {
      strTargetListQuery = ", V240A.C240_TARGET_COGS_LIST TARGETLIST ";
    }

    sbQuery.append("SELECT V240A.C207_SET_ID ID, V240A.C207_SET_NAME NAME ");
    sbQuery.append(", V240A.C240_TARGET_CASES  TARGETCASES ");
    sbQuery.append(strTargetListQuery);
    sbQuery.append(", V240A.C207_SEQ_NO SEQNO ");
    sbQuery.append(" FROM V240A_SALES_TARGET_LIST V240A");

    sbQuery.append(" ORDER BY ID ");
    log.debug("by reportConsignTarget ++++ " + sbQuery.toString());
    alReportConsignTarget = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReportConsignTarget;
  }

  /**
   * calculateConsignMetrics - This Method is used to calculate the metrics for Set Group or
   * Distributor
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList calculateConsignMetrics(ArrayList alSetSummary, ArrayList alConsignAvgUse,
      HashMap hmParam) throws AppError {
    ArrayList alConsignMetrics = new ArrayList();
    ArrayList alAdditionalSetSummary = new ArrayList();
    // String strLoanerType = GmCommonClass.parseNull((String) hmParam.get("LOANERTYPE"));
    String strSalesType = GmCommonClass.parseNull((String) hmParam.get("SalesType"));
    String strHAction = GmCommonClass.parseNull((String) hmParam.get("hAction"));
    String strTurns = GmCommonClass.parseZero((String) hmParam.get("TURNS"));
    String strSystemOnlyCondition =
        GmCommonClass.parseNull((String) hmParam.get("SYSTEMONLYCONDITION"));
    Iterator iteratorSetSummary = alSetSummary.iterator();

    double dSetCount;
    double dAvgCases;
    double dListValue;
    double dAvgRevenue;
    double dLoanerCasesInPeriod;
    double dLoanerUsage;
    double dLoanerBaselineSets;
    // double dConsignedSets;
    int turns = Integer.parseInt(strTurns);

    String strMetricCasesPerMonth = "";
    String strMetricListTurns = "";
    String strMetricUtilization = "";

    while (iteratorSetSummary.hasNext()) {
      HashMap hmTempSetSummary = (HashMap) iteratorSetSummary.next();
      HashMap hmTempReturn = new HashMap();

      Iterator iteratorConsignAvgUse = alConsignAvgUse.iterator();
      while (iteratorConsignAvgUse.hasNext()) {
        HashMap hmTempConsignAvgUse = (HashMap) iteratorConsignAvgUse.next();

        if (((String) hmTempConsignAvgUse.get("ID")).equals(hmTempSetSummary.get("ID"))) {
          dSetCount = Double.parseDouble((String) hmTempSetSummary.get("VCOUNT"));
          dAvgCases = Double.parseDouble((String) hmTempConsignAvgUse.get("AVGCASES"));
          dListValue = Double.parseDouble((String) hmTempSetSummary.get("VTOTAL"));
          dAvgRevenue = Double.parseDouble((String) hmTempConsignAvgUse.get("AVGREVENUE"));
          dLoanerCasesInPeriod =
              Double.parseDouble((String) hmTempConsignAvgUse.get("ALLLOANERCASESINPERIOD"));
          dLoanerUsage =
              strSalesType.equals("50302") ? Double.parseDouble((String) hmTempSetSummary
                  .get("LOANER_USAGE_COUNT")) : Double.parseDouble((String) hmTempSetSummary
                  .get("VCOUNT"));

          strMetricCasesPerMonth = dSetCount == 0 ? "" : "" + (dAvgCases / dSetCount);
          strMetricListTurns =
              dListValue <= 0 ? "0" : ""
                  + (((dAvgRevenue / (dListValue)) <= 0) ? 0 : (dAvgRevenue / (dListValue)));

          // New formula for turns of Field Loaner Usage
          // if (!strHAction.equals("LoadGCONS") && strSalesType.equals("50301")){
          if (!("ONLYSYSTEM").equalsIgnoreCase(strSystemOnlyCondition)
              && strSalesType.equals("50301")) {
            strMetricCasesPerMonth =
                dLoanerUsage == 0 ? "" : "" + (dAvgCases / (dLoanerUsage / (2 * turns)));

            // " calculating Cases Per Month "+strMetricCasesPerMonth+
            // " =  in Average cases "+dAvgCases + " Loaner Usage " + dLoanerUsage + " and turns " +
            // turns);
          } else if (("ONLYSYSTEM").equalsIgnoreCase(strSystemOnlyCondition)
              && strSalesType.equals("50301")) { // (strHAction.equals("LoadGCONS") &&
                                                 // strSalesType.equals("50301")){
            dLoanerBaselineSets =
                Double.parseDouble(GmCommonClass.parseZero((String) hmTempSetSummary
                    .get("LOANERBASELINESETS")));

            // " in Average cases "+dAvgCases);
            strMetricCasesPerMonth =
                dLoanerBaselineSets == 0 ? "" : "" + (dAvgCases / dLoanerBaselineSets);
          }

          // If Loaner or ALL, then calculate Utilization %
          if (!strSalesType.equals("50300")) {

            // " in dLoanerUsage "+dLoanerUsage);
            strMetricUtilization =
                dLoanerUsage == 0 ? "" : "" + (dLoanerCasesInPeriod * 100 / dLoanerUsage);
          }

          hmTempReturn.put("ID", hmTempSetSummary.get("ID"));
          hmTempReturn.put("METRICCASES", strMetricCasesPerMonth);
          hmTempReturn.put("METRICLISTTURNS", strMetricListTurns);
          hmTempReturn.put("METRICUTILIZATION", strMetricUtilization);
          alConsignMetrics.add(hmTempReturn);
          break;
        }
      }
      if (!((String) hmTempSetSummary.get("ID")).equals(hmTempReturn.get("ID"))) {

        hmTempReturn.put("ID", hmTempSetSummary.get("ID"));
        hmTempReturn.put("METRICCASES", "0");
        hmTempReturn.put("METRICLISTTURNS", "0");
        alAdditionalSetSummary.add(hmTempReturn);
      }
    }
    alConsignMetrics.addAll(alAdditionalSetSummary);
    return alConsignMetrics;
  }

  /**
   * reportConsignAvguseBySet - This Method is used to fetch the Anuualized Value and the Average #
   * of cases by Set Group or Distributor
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportConsignAvguseByDetailSummary(HashMap hmParam) throws AppError {

    ArrayList alConsignAvgUse = new ArrayList();
    // GmSalesFilterConditionBean gmSalesFilterConditionBean = new
    // GmSalesFilterConditionBean() ;
    String strTurns = GmCommonClass.parseZero((String) hmParam.get("TURNS"));
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery
        .append(" SELECT AVERAGEUSEDETAIL.ID ID  , AVERAGEUSEDETAIL.NAME NAME ,NVL ( TRUNC (SUM(AVERAGEUSEDETAIL.AVGCASES),1),0) AVGCASES , SUM(AVERAGEUSEDETAIL.AVGREVENUE) AVGREVENUE ");
    sbQuery.append(", NVL ( ROUND (");
    if ("50302".equals(strSalesType)) {
      sbQuery.append("  SUM(AVERAGEALLLOANERUSEDETAIL.AVGCASES) *  ");
    } else {
      sbQuery.append("  SUM(AVERAGEUSEDETAIL.AVGCASES) *  ");
    }
    sbQuery.append(strTurns);
    sbQuery.append(" ,0) , 0) ");
    sbQuery.append(" ALLLOANERCASESINPERIOD ");
    sbQuery.append(" FROM  (");
    sbQuery.append(getConsignAvguseByDetailSummary(hmParam));
    sbQuery.append(" ) AVERAGEUSEDETAIL ");
    if ("50302".equals(strSalesType)) {
      hmParam.put("SalesType", "50301");
      sbQuery.append(" , ( ");
      sbQuery.append(getConsignAvguseByDetailSummary(hmParam));
      this.strSalesType = "50302";
      hmParam.put("SalesType", "50302");
      sbQuery.append(" ) AVERAGEALLLOANERUSEDETAIL ");
      sbQuery.append(" WHERE AVERAGEUSEDETAIL.ID = AVERAGEALLLOANERUSEDETAIL.ID(+) ");
    }
    sbQuery.append("  GROUP BY AVERAGEUSEDETAIL.ID  , AVERAGEUSEDETAIL.NAME ");
    sbQuery.append(" ORDER BY AVERAGEUSEDETAIL.ID, AVERAGEUSEDETAIL.NAME ");

    log.debug("by reportConsignAvguseByDetailSummary ++++ " + sbQuery.toString());
    alConsignAvgUse = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alConsignAvgUse;
  }

  public StringBuffer getConsignAvguseByDetailSummary(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    String strCompFilter = GmCommonClass.parseNull((String) hmParam.get("COMPID"));
    hmParam.put("T501B_FLAG", "Y");
    String strFilterCondition = getCrossTabFilterCondition(hmParam);
    String strTurns = GmCommonClass.parseZero((String) hmParam.get("TURNS"));

    // If no value passed unload to fetch 7 month of information
    String strWhereCondtion = calculateAverageMonths(Integer.parseInt(strTurns));
    log.debug(" where condn is " + strWhereCondtion + " strAction " + strAction);

    String strFrameColumQuery = getFrameColumn(strAction);
    log.debug(" strFrameColumQuery is " + strFrameColumQuery);

    String strFrameGroupQuery = getFrameGroup(strAction);
    log.debug(" strFrameGroupQuery is " + strFrameGroupQuery);
    // SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) /
    // DECODE(V207.C204_VALUE,0,1,V207.C204_VALUE) )

    sbQuery
        .append(" SELECT ID, NAME, TRUNC(SUM(AVGCASES),1) AVGCASES, SUM(AVGREVENUE) AVGREVENUE ");
    sbQuery.append(" FROM ( ");

    // To fetch case and revenue information
    sbQuery
        .append(" SELECT PART_DETAIL.ID, PART_DETAIL.NAME, PART_DETAIL.avgrevenue,CASE_DETAIL.avgcases  FROM (");
    // Below code to fetch case information
    sbQuery.append(strFrameColumQuery);
    sbQuery.append(", SUM(t502.c501b_qty) / ");
    sbQuery.append(strTurns);


    sbQuery.append(" AVGCASES ");
    sbQuery.append(" FROM  T501_ORDER  T501,");
    sbQuery.append(" t501b_order_by_system t502, ");
    sbQuery.append(" t207_set_master v207, ");
    log.debug("strCompFilter" + strCompFilter);
    sbQuery.append("  (SELECT DISTINCT DECODE(" + strCompFilter
        + ",100803, 100803, compid) compid, divid, ter_id ");
    sbQuery
        .append(" , ter_name, rep_name, rep_id ,rep_compid,d_compid, d_id ,d_name, vp_id, vp_name ");
    sbQuery.append(" , region_id, region_name, ad_id, ad_name, gp_id, gp_name  ");
    //PC-3905 Field Inventory Report performance improvement
    sbQuery.append(" FROM V703_REP_MAPPING_DETAIL) V700 ");

    sbQuery.append(" WHERE V700.REP_ID  = T501.C703_SALES_REP_ID ");
    sbQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    sbQuery.append(" AND t502.c501b_void_fl IS NULL ");
    sbQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbQuery.append(" AND t502.c501_order_id = t501.c501_order_id ");
    sbQuery.append(" AND t502.c207_system_id = v207.c207_set_id ");
    sbQuery.append(strFilterCondition);
    log.debug("strFilterCondition" + strFilterCondition);
    sbQuery.append(getSalesFilterClause());
    log.debug("getSalesFilterClause" + getSalesFilterClause());

    sbQuery.append(" AND ");
    sbQuery.append(strWhereCondtion);
    log.debug("strWhereCondtion" + strWhereCondtion);

    sbQuery.append(strFrameGroupQuery);
    log.debug("strFrameGroupQuery" + strFrameGroupQuery);

    sbQuery.append(" ) CASE_DETAIL ,  ( ");

    // Below code to fetch part number information
    hmParam.put("T501B_FLAG", "");
    strFilterCondition = getCrossTabFilterCondition(hmParam);
    log.debug("getCrossTabFilterCondition" + strFilterCondition);

    log.debug("strFrameColumQuery" + strFrameColumQuery);

    sbQuery.append(strFrameColumQuery);
    sbQuery.append(", (( SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY)");
    sbQuery.append(" / ");
    sbQuery.append(strTurns);
    sbQuery.append(") * 12 ) AVGREVENUE");
    sbQuery.append(" FROM  T501_ORDER  T501,");
    sbQuery.append(" T502_ITEM_ORDER T502, ");
    sbQuery.append(" V207B_SET_PART_DETAILS V207, ");

    sbQuery.append("  (SELECT DISTINCT DECODE(" + strCompFilter
        + ",100803, 100803, compid) compid, divid, ter_id ");
    sbQuery
        .append(" , ter_name, rep_name, rep_id ,rep_compid,d_compid, d_id ,d_name, vp_id, vp_name ");
    sbQuery.append(" , region_id, region_name, ad_id, ad_name, gp_id, gp_name  ");
    sbQuery.append(" FROM v700_territory_mapping_detail) V700 ");

    sbQuery.append(" WHERE V207.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND V700.REP_ID  = T501.C703_SALES_REP_ID ");
    sbQuery.append(" AND T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
    sbQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    sbQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbQuery.append(strFilterCondition);
    log.debug("strFilterCondition" + strFilterCondition);
    sbQuery.append(getSalesFilterClause());
    log.debug("getSalesFilterClause" + getSalesFilterClause());

    sbQuery.append(" AND ");
    sbQuery.append(strWhereCondtion);
    log.debug("strWhereCondtion" + strWhereCondtion);

    sbQuery.append(strFrameGroupQuery);
    log.debug("strFrameGroupQuery" + strFrameGroupQuery);

    sbQuery.append(" ) PART_DETAIL ");
    sbQuery.append(" WHERE PART_DETAIL.ID = CASE_DETAIL.ID(+) )");

    sbQuery.append(" GROUP BY ID, NAME ");


    log.debug(" Query for getConsignAvguseByDetailSummary in  is ------- " + sbQuery.toString());

    return sbQuery;
  }

  /**
   * reportConsignAvguseByCOGS - This Method is used to fetch the Anuualized COGS Value and the
   * Average # of cases by Set Group or Distributor
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportConsignAvguseByCOGS(HashMap hmParam) throws AppError {
    // Method commented as discussed with Richard
    ArrayList alConsignAvgUse = new ArrayList();
    /*
     * // GmSalesFilterConditionBean gmSalesFilterConditionBean = new //
     * GmSalesFilterConditionBean(); StringBuffer sbQuery = new StringBuffer(); GmDBManager
     * gmDBManager = new GmDBManager(getGmDataStoreVO()); // String strFilterCondition =
     * getCrossTabFilterCondition(hmParam); // // Call the super class method to get the // filter
     * condition
     * 
     * // If no value passed unload to fetch 7 month of information String strTurns =
     * GmCommonClass.parseZero((String) hmParam.get("TURNS"));
     * 
     * // If no value passed unload to fetch 7 month of information String strWhereCondtion =
     * calculateAverageMonths(Integer.parseInt(strTurns)); String strFrameColumQuery =
     * getFrameColumn(strAction); log.debug(" strFrameColumQuery is " + strFrameColumQuery);
     * 
     * String strFrameGroupQuery = getFrameGroup(strAction); log.debug(" strFrameGroupQuery is " +
     * strFrameGroupQuery);
     * 
     * String strFrameOrderQuery = getFrameOrder(strAction);
     * 
     * sbQuery.append(" SELECT ID  , NAME , SUM(AVGCASES) AVGCASES , SUM(AVGREVENUE) AVGREVENUE ");
     * sbQuery.append(" FROM ( "); sbQuery.append(strFrameColumQuery);
     * sbQuery.append(", DECODE(V207.C204_VALUE , 0, 0, "); sbQuery .append(
     * " (SUM(DECODE(C205_TRACKING_IMPLANT_FL, 'Y', ((T502.C810_QTY * DECODE (NVL(T502.C810_DR_AMT,0),0,0,1)) - (T502.C810_QTY * DECODE (NVL(T502.C810_CR_AMT,0),0,0,1))) ,0) ) "
     * ); sbQuery.append(" / V207.C204_VALUE)) / "); sbQuery.append(strTurns);
     * sbQuery.append(" AVGCASES "); sbQuery
     * .append(", (((SUM(T502.C810_QTY * T502.C810_DR_AMT) - SUM(T502.C810_QTY * T502.C810_CR_AMT)) "
     * ); sbQuery.append(" / "); sbQuery.append(strTurns); sbQuery.append(" ) * 12 ) AVGREVENUE");
     * // sbQuery.append(" , V207.C207_SEQ_NO SNO ");
     * sbQuery.append(" FROM T810_INVENTORY_TXN T502 ");
     * sbQuery.append(" ,V207B_SET_PART_DETAILS V207 ");
     * sbQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700  ");
     * sbQuery.append(" WHERE  V207.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID  ");
     * sbQuery.append(" AND V700.AC_ID  = T502.C810_PARTY_ID  "); //
     * sbQuery.append(" AND V207.C205_TRACKING_IMPLANT_FL = 'Y' ");
     * sbQuery.append(" AND T502.C801_ACCOUNT_ELEMENT_ID = 5 "); // Element // Id 5 // refers to //
     * sales. sbQuery.append(" AND T502.C810_TXN_DATE >= TO_DATE('"); sbQuery.append(strFromMonth +
     * "/" + strFromYear); sbQuery.append("','MM/YYYY') "); sbQuery.append(" AND ");
     * sbQuery.append(" T502.C810_TXN_DATE <= LAST_DAY(TO_DATE('"); sbQuery.append(strToMonth + "/"
     * + strToYear); sbQuery.append("','MM/YYYY')) ");
     * 
     * if (strDistributorID != null && !strDistributorID.equals("")) {
     * sbQuery.append("  AND V700.D_ID = "); sbQuery.append(strDistributorID); } if (strADID != null
     * && !strADID.equals("") && !strADID.equals("null") && !strADID.equals("0")) {
     * sbQuery.append(" AND V700.AD_ID = " + strADID); } if (strSetNumber != null &&
     * !strSetNumber.equals("") && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
     * sbQuery.append(" AND V207.C207_SET_ID = " + strSetNumber); } // To get other filter condition
     * // sbQuery.append(strFilterCondition);
     * 
     * sbQuery.append(strFrameGroupQuery); sbQuery.append(" , V207.C204_VALUE ");
     * sbQuery.append(" ) GROUP BY ID  , NAME "); // sbQuery.append(strFrameOrderQuery);
     * 
     * log.debug(" reportConsignAvguseByCOGS Query is " + sbQuery.toString()); alConsignAvgUse =
     * gmDBManager.queryMultipleRecords(sbQuery.toString());
     */

    return alConsignAvgUse;
  }

  /**
   * reportAllBaselineSummary - This Method is used to fetch baseline values for both consignment
   * and loaner Group
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportAllBaselineSummary(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alValue = new ArrayList();
    String strTurns = GmCommonClass.parseZero((String) hmParam.get("TURNS"));
    String strSystemOnlyCondition =
        GmCommonClass.parseNull((String) hmParam.get("SYSTEMONLYCONDITION"));
    int turns = Integer.parseInt(strTurns);

    String strSelectQuery = "";
    String strJoinTable = "";
    String strWhereQuery = "";
    String strActions = GmCommonClass.parseNull((String) hmParam.get("hAction"));
    String strSalesType = GmCommonClass.parseNull((String) hmParam.get("SalesType"));

    // String strSplitVal = " ,consignment.vcount||' + ('||nvl(LOANERUSAGE.vcount,0)||'/"+ turns *
    // 4+")' VSPLIT ";
    sbQuery.setLength(0);

    // PMT-27002: to fix the incorrect Consignment set issue (AD) - 50302 (All tab)

    if (strSalesType.equals("50302") && strActions.equals("LoadAD")) {
      strSelectQuery =
          " SELECT NVL(CONSIGNMENT.ID,LOANERUSAGE.ID) ID , NVL(CONSIGNMENT.NAME,LOANERUSAGE.NAME) NAME ";
      strJoinTable = " FULL OUTER JOIN (  ";
      strWhereQuery = " ON CONSIGNMENT.ID = LOANERUSAGE.ID ";
    } else {
      strSelectQuery = " SELECT CONSIGNMENT.ID ID , CONSIGNMENT.NAME NAME ";
      strJoinTable = " , ( ";
      strWhereQuery = " WHERE CONSIGNMENT.ID = LOANERUSAGE.ID (+) ";
    }

    sbQuery.append(strSelectQuery);
    /*
     * sbQuery.append(" , ROUND(CONSIGNMENT.vcount + (0.25/ "); sbQuery.append(turns);
     * sbQuery.append("* NVL(LOANERUSAGE.vcount,0))) VCOUNT, 0 VTOTAL ");
     * sbQuery.append(strSplitVal);
     */
    sbQuery.append(", NVL(ROUND( consignment.vcount),0) consignment_set_count ");
    sbQuery.append(", NVL(loanerusage.vcount,0) loaner_usage_count ");
    sbQuery.append(", 0 VTOTAL ");
    if (("ONLYSYSTEM").equalsIgnoreCase(strSystemOnlyCondition)) {
      sbQuery.append(" , NVL(loanerbaseline.vcount,0) loaner_baseline_count ");
      sbQuery
          .append(" , NVL(ROUND( NVL(consignment.vcount,0) + NVL(loanerbaseline.vcount,0) ),0) VCOUNT ");
    } else {
      sbQuery.append(" , ROUND(NVL(CONSIGNMENT.vcount,0) + (NVL(LOANERUSAGE.vcount,0)/ (2 * ");
      sbQuery.append(turns);
      sbQuery.append("))) VCOUNT ");
    }
    sbQuery.append(" FROM ( ");
    sbQuery.append(getConsignmentBaselineSummary(hmParam));
    sbQuery.append(" ) CONSIGNMENT " + strJoinTable);
    sbQuery.append(getLoanerUsageSetSummary(hmParam));
    sbQuery.append(" ) LOANERUSAGE ");
    if ("ONLYSYSTEM".equalsIgnoreCase(strSystemOnlyCondition)) {
      sbQuery.append(" , ( ");
      sbQuery.append(getLoanerBaselineSummary(hmParam));
      sbQuery.append(" ) LOANERBASELINE ");
    }

    sbQuery.append(strWhereQuery);


    if ("ONLYSYSTEM".equalsIgnoreCase(strSystemOnlyCondition)) {
      sbQuery.append(" AND CONSIGNMENT.ID = LOANERBASELINE.ID (+) ");
      sbQuery.append(" ORDER BY CONSIGNMENT.NAME ");
    } else {
      sbQuery.append(" ORDER BY CONSIGNMENT.NAME ");
    }

    log.debug("Baseline Query for All Set summary ++++ " + sbQuery.toString());
    alValue = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug(" Baseline valuies " + alValue);
    return alValue;
  }


  /*
   * Forms the Query for Loaner USage.
   */
  public StringBuffer getLoanerUsageSetSummary(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();

    String strWhereCondition = getFieldInventoryCrossTabFilterCondition(hmParam).toString();
    String strFrameColumQuery = getFrameColumn(strAction);
    String strFrameGroupQuery = getFrameGroup(strAction);

    String strTurns = GmCommonClass.parseZero((String) hmParam.get("TURNS"));
    int turns = Integer.parseInt(strTurns);
    HashMap hmFromToMonth = GmCommonClass.getMonthDiff(turns, true);
    log.debug(" hmFromToMonth " + hmFromToMonth);
    String strFromDate = hmFromToMonth.get("FromMonth") + "/" + hmFromToMonth.get("FromYear");
    String strToDate = hmFromToMonth.get("ToMonth") + "/" + hmFromToMonth.get("ToYear");

    sbQuery.setLength(0);
    sbQuery.append(strFrameColumQuery);
    sbQuery.append(" , COUNT (1)  VCOUNT ");
    sbQuery.append(" , 0 VTOTAL ");
    //PC-3905 Field Inventory Report performance improvement
    sbQuery.append(" FROM v504a_sales_loaner_transaction v504a "); 
    sbQuery.append(" , V207A_SET_CONSIGN_LINK V207 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE v504a.C207_SET_ID =  V207.C207_ACTUAL_SET_ID  ");
    sbQuery.append(" AND v504a.C504A_CONSIGNED_TO_ID = V700.D_ID ");
    sbQuery.append(" AND v504a.c504a_loaner_dt >= TO_DATE ('");
    sbQuery.append(strFromDate);
    sbQuery.append("', 'MM/YYYY') ");
    sbQuery.append(" AND v504a.c504a_loaner_dt <= LAST_DAY (TO_DATE ('");
    sbQuery.append(strToDate);
    sbQuery.append("','MM/YYYY')) ");
    sbQuery.append(" AND v207.c901_link_type = 20100 ");
    sbQuery.append(strWhereCondition);
    sbQuery.append(strFrameGroupQuery);
    sbQuery.append(" ORDER BY NAME ");

    return sbQuery;
  }

  /**
   * reportVSetSummaryByDist - This Method is used to fetch VSet Summary Along with sales
   * information by Distributor
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportVSetSummaryByDist(HashMap hmparam) throws AppError {

    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    HashMap hmapFinal = new HashMap();
    HashMap hmLinkDetails = new HashMap();
    GmSalesYTDBean gmSalesYTD = new GmSalesYTDBean(getGmDataStoreVO());
    GmSalesConsignAAIBudgetBean gmSalesConsignAAIBudgetBean =
        new GmSalesConsignAAIBudgetBean(getGmDataStoreVO());

    // result data stores
    ArrayList alSetDistSummary = new ArrayList();
    ArrayList alConsignAvgUseDist = new ArrayList();
    ArrayList alConsignMetrics = new ArrayList();
    ArrayList alReportConsignTargetDist = new ArrayList();
    ArrayList alReportPercentage = new ArrayList();
    ArrayList alAdditionalInventory = new ArrayList();
    ArrayList alConsignAAIBudget = new ArrayList();
    HashMap hmreturn = new HashMap();

    // ****************************************************************
    // To fetch actual sales value information (last 6 month) from the
    // Group list (active for last 6 month)
    // ****************************************************************
    String strType = GmCommonClass.parseNull((String) hmparam.get("ConsignmentType"));
    // ("!@#$%^&* TYPE : !@#$%^&*() : "+ strType);

    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportYTDByCOGS
      hmreturn = gmSalesYTD.reportYTDByCOGS(hmparam);
    } else {
      hmreturn = gmSalesYTD.reportYTDByDetailSummary(hmparam);
    }

    alSetDistSummary = reportBaselineSummary(hmparam);

    alConsignAAIBudget = gmSalesConsignAAIBudgetBean.reportConsignAAIBudget("FS", hmparam);

    hmLinkDetails = getLinkInfo("SET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alSetDistSummary, hmLinkDetails);

    // to find the Average cases and Annualized Revenue
    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportConsignAvguseByCOGS
      alConsignAvgUseDist = reportConsignAvguseByCOGS(hmparam);
    } else {
      alConsignAvgUseDist = reportConsignAvguseByDetailSummary(hmparam);
    }

    hmLinkDetails = getLinkInfo("AVG");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignAvgUseDist, hmLinkDetails);

    alConsignMetrics = calculateConsignMetrics(alSetDistSummary, alConsignAvgUseDist, hmparam);
    hmLinkDetails = getLinkInfo("MET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignMetrics, hmLinkDetails);

    // to show Additional Inventory details
    alAdditionalInventory = reportAdditionalInventory(hmparam);

    hmLinkDetails = getLinkInfo("ADDITIONALINVENTORY");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alAdditionalInventory, hmLinkDetails);
    hmreturn.put("HMPARAM", hmparam);
    // to show Consignment AAI BUDGET details
    hmLinkDetails = getLinkInfo("CONSIGNAAIBUDGET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignAAIBudget, hmLinkDetails);
    hmreturn = modifyFooterColumn(hmreturn);

    hmapFinal.put("CROSSVALE", hmreturn);

    return hmapFinal;
  }

  /**
   * reportVSetSummaryByAD - This Method is used to fetch VSet Summary Along with sales information
   * by AD
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportVSetSummaryByAD(HashMap hmparam) throws AppError {

    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    GmSalesConsignAAIBudgetBean gmSalesConsignAAIBudgetBean =
        new GmSalesConsignAAIBudgetBean(getGmDataStoreVO());

    HashMap hmapFinal = new HashMap();
    HashMap hmLinkDetails = new HashMap();
    GmSalesYTDBean gmSalesYTD = new GmSalesYTDBean(getGmDataStoreVO());

    // result data stores
    ArrayList alSetDistSummary = new ArrayList();
    ArrayList alConsignAvgUseDist = new ArrayList();
    ArrayList alConsignMetrics = new ArrayList();
    ArrayList alAdditionalInventory = new ArrayList();
    ArrayList alAIQuota = new ArrayList();
    ArrayList alConsignAAIBudget = new ArrayList();


    HashMap hmreturn = new HashMap();

    // ****************************************************************
    // To fetch actual sales value information (last 6 month) from the
    // Group list (active for last 6 month)
    // ****************************************************************
    String strType = GmCommonClass.parseNull((String) hmparam.get("ConsignmentType"));
    // ("!@#$%^&* TYPE : !@#$%^&*() : "+ strType);

    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportYTDByCOGS
      hmreturn = gmSalesYTD.reportYTDByCOGS(hmparam);
    } else {
      hmreturn = gmSalesYTD.reportYTDByDetailSummary(hmparam);
    }
    alSetDistSummary = reportBaselineSummary(hmparam);
    hmLinkDetails = getLinkInfo("SET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alSetDistSummary, hmLinkDetails);

    // to find the Average cases and Annualized Revenue

    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportConsignAvguseByCOGS
      alConsignAvgUseDist = reportConsignAvguseByCOGS(hmparam);
    } else {
      alConsignAvgUseDist = reportConsignAvguseByDetailSummary(hmparam);
    }

    hmLinkDetails = getLinkInfo("AVG");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignAvgUseDist, hmLinkDetails);
    alConsignMetrics = calculateConsignMetrics(alSetDistSummary, alConsignAvgUseDist, hmparam);
    hmLinkDetails = getLinkInfo("MET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignMetrics, hmLinkDetails);

    // to show Additional Inventory details

    alAdditionalInventory = reportAdditionalInventory(hmparam);

    hmLinkDetails = getLinkInfo("ADDITIONALINVENTORY");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alAdditionalInventory, hmLinkDetails);
    // to show Consignment AAI BUDGET details
    alConsignAAIBudget = gmSalesConsignAAIBudgetBean.reportConsignAAIBudget("AD", hmparam);
    hmLinkDetails = getLinkInfo("CONSIGNAAIBUDGET");

    // log.debug("AAI_PERCENT in AD Class"+alConsignAAIBudget.get("AAI_PERCENT"));
    // log.debug("TOTAL_STD in AD Class"+alConsignAAIBudget.get("TOTAL_STD"));
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignAAIBudget, hmLinkDetails);

    alAIQuota = reportAIQuartetlyQuota(hmparam);
    hmLinkDetails = getLinkInfo("AIQUOTA");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alAIQuota, hmLinkDetails);

    hmreturn.put("HMPARAM", hmparam);
    hmreturn = modifyFooterColumn(hmreturn);


    hmapFinal.put("CROSSVALE", hmreturn);

    return hmapFinal;
  }

  /**
   * reportVSetSummaryByAD - This Method is used to fetch VSet Summary Along with sales information
   * by AD
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportVSetSummaryByVP(HashMap hmparam) throws AppError {

    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    HashMap hmapFinal = new HashMap();
    HashMap hmLinkDetails = new HashMap();
    GmSalesYTDBean gmSalesYTD = new GmSalesYTDBean(getGmDataStoreVO());
    GmSalesConsignAAIBudgetBean gmSalesConsignAAIBudgetBean =
        new GmSalesConsignAAIBudgetBean(getGmDataStoreVO());


    // result data stores
    ArrayList alSetDistSummary = new ArrayList();
    ArrayList alConsignAvgUseDist = new ArrayList();
    ArrayList alConsignMetrics = new ArrayList();
    ArrayList alAdditionalInventory = new ArrayList();
    ArrayList alAIQuota = new ArrayList();
    ArrayList alConsignAAIBudget = new ArrayList();


    HashMap hmreturn = new HashMap();

    // ****************************************************************
    // To fetch actual sales value information (last 6 month) from the
    // Group list (active for last 6 month)
    // ****************************************************************
    String strType = GmCommonClass.parseNull((String) hmparam.get("ConsignmentType"));
    // ("!@#$%^&* TYPE : !@#$%^&*() : "+ strType);

    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportYTDByCOGS
      hmreturn = gmSalesYTD.reportYTDByCOGS(hmparam);
    } else {
      hmreturn = gmSalesYTD.reportYTDByDetailSummary(hmparam);
    }

    alSetDistSummary = reportBaselineSummary(hmparam);
    // log.debug(" alSetDistSummary values " + alSetDistSummary);


    hmLinkDetails = getLinkInfo("SET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alSetDistSummary, hmLinkDetails);

    // to find the Average cases and Annualized Revenue
    if (strType.equals("50420")) { // if the Type is COGS then call
      // reportConsignAvguseByCOGS
      alConsignAvgUseDist = reportConsignAvguseByCOGS(hmparam);
    } else {
      alConsignAvgUseDist = reportConsignAvguseByDetailSummary(hmparam);
    }

    hmLinkDetails = getLinkInfo("AVG");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignAvgUseDist, hmLinkDetails);

    alConsignMetrics = calculateConsignMetrics(alSetDistSummary, alConsignAvgUseDist, hmparam);
    log.debug("alConsignMetrics##1=" + alConsignMetrics);

    hmLinkDetails = getLinkInfo("MET");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignMetrics, hmLinkDetails);

    // to show Additional Inventory details
    alAdditionalInventory = reportAdditionalInventory(hmparam);

    hmLinkDetails = getLinkInfo("ADDITIONALINVENTORY");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alAdditionalInventory, hmLinkDetails);
    // to show Consignment AAI BUDGET details
    alConsignAAIBudget = gmSalesConsignAAIBudgetBean.reportConsignAAIBudget("VP", hmparam);

    hmLinkDetails = getLinkInfo("CONSIGNAAIBUDGET");

    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alConsignAAIBudget, hmLinkDetails);


    alAIQuota = reportAIQuartetlyQuota(hmparam);
    hmLinkDetails = getLinkInfo("AIQUOTA");
    hmreturn = gmCrossReport.linkCrossTab(hmreturn, alAIQuota, hmLinkDetails);
    hmreturn.put("HMPARAM", hmparam);
    hmreturn = modifyFooterColumn(hmreturn);
    hmapFinal.put("CROSSVALE", hmreturn);

    return hmapFinal;
  }

  /**
   * reportConsignTargetByDist - This Method is used to fetch Target # of Cases and List Turns Value
   * by Distributor
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportConsignTargetByDist(HashMap hmParam) throws AppError {

    ArrayList alReportConsignTargetDist = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strSetID = hmParam.get("SetNumber") == null ? "" : (String) hmParam.get("SetNumber");

    String strFrameColumQuery = getFrameColumn(strAction);
    String strFrameGroupQuery = getFrameGroup(strAction);

    String strTargetListQuery = ", V240A.C240_TARGET_LIST TARGETLIST ";
    String strGroupTarget = " V240A.C240_TARGET_LIST ";

    if (strTypeCon.equals("50420"))// COGS
    {
      strTargetListQuery = ", V240A.C240_TARGET_COGS_LIST TARGETLIST ";
      strGroupTarget = " V240A.C240_TARGET_COGS_LIST ";
    }

    sbQuery.setLength(0);
    sbQuery.append(strFrameColumQuery);
    sbQuery.append(", V240A.C240_TARGET_CASES  TARGETCASES ");
    sbQuery.append(strTargetListQuery);
    sbQuery.append(", V240A.C207_SEQ_NO SEQNO ");
    sbQuery.append(" FROM V240A_SALES_TARGET_LIST V240A");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append("WHERE  V240A.C207_SET_ID IN ( '");
    sbQuery.append(strSetID);
    sbQuery.append("' ) ");

    /*
     * if (strStatus != null && !strStatus.equals("") && !strStatus.equals("0")) {
     * sbQuery.append(" AND V700.D_ACTIVE_FL = ");
     * sbQuery.append(strStatus.equals("50480")?" 'Y' ":" 'N' "); }
     */
    /*
     * If the Distributor is InActive and he/she doesnt have any Sales and doesnt have any sets then
     * no need to show the distributor
     */
    sbQuery.append(" AND V700.D_ACTIVE_FL = 'Y' ");
    if (strADID != null && !strADID.equals("") && !strADID.equals("null") && !strADID.equals("0")) {
      sbQuery.append(" AND V700.AD_ID = " + strADID);
    }
    sbQuery.append(strFrameGroupQuery);
    sbQuery.append(", V240A.C240_TARGET_CASES, " + strGroupTarget + ", V240A.C207_SEQ_NO ");

    alReportConsignTargetDist = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReportConsignTargetDist;
  }

  /**
   * reportDistributorSummary - This Method is used to fetch distributor summary for consignment and
   * loaner transactions
   * 
   * @return ArrayList
   * @e Exception AppError
   */
  public ArrayList reportBaselineSummary(HashMap hmParam) throws AppError {
    String strSalesType = GmCommonClass.parseNull((String) hmParam.get("SalesType"));
    String strLoanerType = GmCommonClass.parseNull((String) hmParam.get("LOANERTYPE"));
    ArrayList alReturn = new ArrayList();
    initializeParameters(hmParam);

    hmParam.put("BASELINEMETHOD", "BASELINE");

    if (strSalesType.equals("50300")) {
      alReturn = reportConsignmentBaselineSummary(hmParam);
    } else if (strSalesType.equals("50302")) {
      alReturn = reportAllBaselineSummary(hmParam);
    } else {
      alReturn = reportLoanerUsageSummary(hmParam);
    }

    return alReturn;

  }

  /**
   * reportLoanerDistributorSummary - This Method is used to fetch distributor summary for Loaner
   * 
   * @return ArrayList
   * @e Exception AppError
   */
  public ArrayList reportLoanerBaselineSummary(HashMap hmParam) throws AppError // include
  {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alValue = new ArrayList();
    initializeParameters(hmParam);
    String strLoanerBaselineQuery = new String();
    strLoanerBaselineQuery = getLoanerBaselineSummary(hmParam).toString();
    alValue = gmDBManager.queryMultipleRecords(strLoanerBaselineQuery);

    return alValue;
  }

  /**
   * getLoanerBaselineSummary - This method used to fetch the Loaner set count - based on filter
   * condition
   * 
   * 
   * Loaner Set Count - To get the count from Consignment (t504), V700, Loaner (t504a), Plant
   * company mapping (t5041) and Region company map (t707) tables
   * 
   * @param hmParam
   * @return StringBuffer
   * @throws AppError
   */

  public StringBuffer getLoanerBaselineSummary(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.setLength(0);
    sbQuery.append(" SELECT V207.C207_SET_ID ID, V207.C207_SET_NM NAME ");
    sbQuery.append(" ,COUNT(DISTINCT t504.C504_CONSIGNMENT_ID )  VCOUNT ");
    if (strType.equals("50420")) { // COGS{
      sbQuery.append(" ,SUM(");
      sbQuery.append(" GET_SALESCSG_INV_COGS_VALUE(T505.C205_PART_NUMBER_ID) ");
      sbQuery.append(" * NVL(T505.C505_ITEM_QTY,0)) VTOTAL ");
    } else {
      sbQuery.append(" ,SUM(NVL(T505.C505_ITEM_PRICE,0) * NVL(T505.C505_ITEM_QTY,0)) VTOTAL ");
    }
    sbQuery.append(" FROM t504_consignment t504  ");
    sbQuery.append(" , t505_item_consignment t505 ");
    sbQuery.append(" , V207A_SET_CONSIGN_LINK V207 ");
    sbQuery.append(" , T504A_CONSIGNMENT_LOANER t504a ");
    sbQuery.append(" WHERE T504.C207_SET_ID =  V207.C207_ACTUAL_SET_ID ");
    sbQuery.append(" AND t505.C504_CONSIGNMENT_ID = t504.C504_CONSIGNMENT_ID ");
    sbQuery.append(" AND t504a.C504_CONSIGNMENT_ID = t504.C504_CONSIGNMENT_ID ");
    sbQuery.append(" AND exists (SELECT DISTINCT C5040_PLANT_ID ");

    // PMT-27002: Region not mapped to Company - So, Loaner Set count showing incorrectly
    // Mapping the Region company mapping

    sbQuery.append(" FROM t5041_plant_company_mapping t5041, t707_region_company_mapping t707 ");
    sbQuery.append(getAccessFilterClauseWithDiv());
    sbQuery
        .append(" WHERE t5041.C5041_VOID_FL   IS NULL  AND t5041.C1900_COMPANY_ID = v700.d_compid ");
    sbQuery.append(" and t707.c707_void_fl is null ");
    sbQuery.append(" and ( v700.region_id = t707.c901_region_id ");
    // the below OR condition is added to handle the US Division and due to this the region mapping
    // for US can be
    // avoided in the t707_region_company_mapping.
    sbQuery.append(" OR v700.divid   = 100823 ) ");
    // PMT-37471: Field Inventory report showing incorrect Qty - Loaner tab.
    // Added the primary flag - So, in GMNA company not pull the NEDC set
    sbQuery.append(" and t5041.c5041_primary_fl = 'Y' ");
	sbQuery.append("  AND t5041.C5040_PLANT_ID = t504a.c5040_plant_id ");
    sbQuery.append(" and t5041.c1900_company_id =  t707.c1900_company_id )");
    sbQuery.append(" AND t504.c504_void_fl IS NULL ");
    sbQuery.append(" AND t504.c207_set_id IS NOT NULL ");
    sbQuery.append(" AND t504.c504_type = 4127  ");
    sbQuery.append(" AND v207.c901_link_type = 20100 ");
    // PC-3905: status fl is char so checking the char
    sbQuery.append(" AND t504a.C504A_STATUS_FL <> '60' "); // filter out inactive loaners
    sbQuery.append(" AND t504a.C504A_VOID_FL IS NULL ");
    sbQuery.append(" GROUP  BY V207.C207_SET_ID , V207.C207_SET_NM, V207.C207_SEQ_NO ");
    sbQuery.append(" ORDER BY V207.C207_SET_NM ");
    log.debug("by Loaner Baseline summary " + sbQuery.toString());

    return sbQuery;
  }

  /**
   * reportLoanerDistributorSummary - This Method is used to fetch distributor summary for Loaner
   * 
   * @return ArrayList
   * @e Exception AppError
   */
  public ArrayList reportLoanerUsageSummary(HashMap hmParam) throws AppError // include
  {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alValue = new ArrayList();
    initializeParameters(hmParam);

    String strTurns = GmCommonClass.parseZero((String) hmParam.get("TURNS")); // .equals("0") ? "1";
    String strSystemOnlyCondition =
        GmCommonClass.parseNull((String) hmParam.get("SYSTEMONLYCONDITION"));

    // PMT-27002: In Loaner tab currently showing the blank set, because loaner usage not available
    // always. But Loaner base available.
    // So, added the NVL condition - ID, Name (Loaner Base/ Loaner Usage)

    // Added the NVL for VCOUNT and VTOTAL - to avoid the parseDouble error.
    if (("ONLYSYSTEM").equalsIgnoreCase(strSystemOnlyCondition)) {
      sbQuery
          .append(" SELECT  NVL(LOANERBASELINE.ID, LOANERUSAGE.ID) ID, NVL(LOANERBASELINE.NAME, LOANERUSAGE.NAME) NAME ");
    } else {
      sbQuery.append(" SELECT  LOANERUSAGE.ID ID, LOANERUSAGE.NAME NAME ");
    }
    sbQuery.append(" , NVL(LOANERUSAGE.vcount, 0) VCOUNT, NVL(LOANERUSAGE.vtotal, 0) VTOTAL ");
    sbQuery.append(" , DECODE(LOANERUSAGE.vcount,NULL, 0, 0, 0,  LOANERUSAGE.vcount / (2 *  ");
    sbQuery.append(strTurns);
    sbQuery.append(" ) ) LOANER_USAGE_IN_PERIOD ");
    if (("ONLYSYSTEM").equalsIgnoreCase(strSystemOnlyCondition)) {
      sbQuery.append(" , LOANERBASELINE.VCOUNT LOANERBASELINESETS ");
    }
    sbQuery.append(" FROM ( ");
    sbQuery.append(getLoanerUsageSetSummary(hmParam));
    sbQuery.append(" )  LOANERUSAGE ");
    if (("ONLYSYSTEM").equalsIgnoreCase(strSystemOnlyCondition)) {
      sbQuery.append(", (");
      sbQuery.append(getLoanerBaselineSummary(hmParam));
      sbQuery.append(" )  LOANERBASELINE ");
      sbQuery.append(" WHERE LOANERBASELINE.ID = LOANERUSAGE.ID (+) ");
    }
    sbQuery.append(" ORDER BY NAME ");
    log.debug("by Loaner Distributor Usage summary " + sbQuery.toString());

    // sbQuery.append(getLoanerUsageSetSummary(hmParam));
    alValue = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug(" Baseline valuies " + alValue);
    return alValue;
  }

  /**
   * reportConsignmentDistributorSummary - This Method is used to fetch distributor summary for
   * consignment
   * 
   * @return ArrayList
   * @e Exception AppError
   */
  public ArrayList reportConsignmentBaselineSummary(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alSetDistSummary = new ArrayList();

    StringBuffer sbHeaderQuery = getConsignmentBaselineSummary(hmParam);

    log.debug("By Dist ******" + sbHeaderQuery.toString());
    LocalDateTime startTime = LocalDateTime.now();
    alSetDistSummary = gmDBManager.queryMultipleRecords(sbHeaderQuery.toString());
    LocalDateTime endTime = LocalDateTime.now();
    log.debug("Total time taken ==> "+ Duration.between(startTime, endTime).getSeconds());

    return alSetDistSummary;
  }

  /*
   * Query to fetch the baseline details for Consignment
   */
  public StringBuffer getConsignmentBaselineSummary(HashMap hmParam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();

    String strWhereCondition = getFieldInventoryCrossTabFilterCondition(hmParam).toString();
    String strVTotalQuery = ",NVL(SUM(T730.C730_TOTAL_COST),0) VTOTAL ";

    if (strTypeConignment.equals("50420")) // COGS
    {
      strVTotalQuery = ",NVL(SUM(T730.C730_TOTAL_COGS_COST),0) VTOTAL ";
    }

    String strDistributorID = (String) hmParam.get("DistributorID");
    String strSetID = (String) hmParam.get("SetNumber");

    String strFrameColumQuery = getFrameColumn(strAction);
    String strFrameGroupQuery = getFrameGroup(strAction);

    sbHeaderQuery.setLength(0);
    sbHeaderQuery.append(strFrameColumQuery);
    sbHeaderQuery.append(",SUM(DECODE(V207.C901_LINK_TYPE  ,20100 , 1 , 0) )  VCOUNT ");
    sbHeaderQuery.append(strVTotalQuery);
    sbHeaderQuery.append(" FROM T730_VIRTUAL_CONSIGNMENT T730 ");
    sbHeaderQuery.append(" , V207A_SET_CONSIGN_LINK V207 ");
    sbHeaderQuery.append(getAccessFilterClause());
    sbHeaderQuery.append(" WHERE T730.C207_SET_ID =  V207.C207_ACTUAL_SET_ID ");
    sbHeaderQuery.append(" AND  V700.D_ID = T730.C701_DISTRIBUTOR_ID ");
    sbHeaderQuery.append(strWhereCondition);
    sbHeaderQuery.append(strFrameGroupQuery);

    if (strAction.equalsIgnoreCase("LOADGCONS")) {
      sbHeaderQuery.append(" ORDER BY C207_SET_NM ");
    } else {
      sbHeaderQuery.append(" ORDER BY NAME ");
    }

    return sbHeaderQuery;
  }

  /**
   * getFROMTODate - This Method is used to FROM and To Date for the selected period of time
   * 
   * @return HashMap
   * @exception AppError
   */
  private HashMap getFROMTODate() throws AppError {
    HashMap hmlocal = new HashMap();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // Main Header SQL to fetch from and to date information
    sbQuery.append(" SELECT TO_CHAR(CURRENT_DATE,'MM') TOMONTH  ");
    sbQuery.append(" , TO_CHAR(CURRENT_DATE,'YYYY') TOYEAR ");
    sbQuery.append(" , TO_CHAR(ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -5),'MM')  FROMMONTH ");
    sbQuery.append(" , TO_CHAR(ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -5),'YYYY')  FROMYEAR ");
    sbQuery.append(" FROM DUAL ");

    hmlocal = gmDBManager.querySingleRecord(sbQuery.toString());

    return hmlocal;
  }

  /**
   * getLinkInfo - This Method is used to to get the link object information
   * 
   * @return HashMap
   * @exception AppError
   */
  private HashMap getLinkInfo(String strType) {
    HashMap hmLinkDetails = new HashMap();
    HashMap hmapValue = new HashMap();
    ArrayList alLinkList = new ArrayList();

    // First parameter holds the link object name
    hmLinkDetails.put("LINKID", "ID");
    hmLinkDetails.put("POSITION", "AFTER");

    if (strType.equals("SET")) {

      hmLinkDetails.put("POSITION", "BEFORE");
      // Parameter to be added to link list
      if (strSalesType.equals("50302")) {
        hmapValue = new HashMap();
        hmapValue.put("KEY", "CONSIGNMENT_SET_COUNT");
        hmapValue.put("VALUE", "Consign Sets");
        alLinkList.add(hmapValue);

        hmapValue = new HashMap();
        hmapValue.put("KEY", "LOANER_USAGE_COUNT");
        hmapValue.put("VALUE", "Loaner Usage");
        alLinkList.add(hmapValue);

        hmapValue = new HashMap();
        hmapValue.put("KEY", "VCOUNT");
        hmapValue.put("VALUE", "Total Sets");
        alLinkList.add(hmapValue);
      } else {

        hmapValue = new HashMap();
        hmapValue.put("KEY", "VCOUNT");
        hmapValue.put("VALUE", strBaselineName);
        alLinkList.add(hmapValue);

        if (strSalesType.equals("50300")) {
          hmapValue = null;
          hmapValue = new HashMap();
          hmapValue.put("KEY", "VTOTAL");
          hmapValue.put("VALUE", "Total <BR>$Value");
          alLinkList.add(hmapValue);
        }
      }

    } else if (strType.equals("LOANERBASELINESET")) {
      hmLinkDetails.put("POSITION", "BEFORE");
      hmapValue.put("KEY", "VCOUNT");
      hmapValue.put("VALUE", "Loaner Sets");
      alLinkList.add(hmapValue);
    } else if (strType.equals("SYSMAP")) {
      hmapValue.put("KEY", "ASSTCASES");
      hmapValue.put("VALUE", "Case<BR>BreakUp");
      alLinkList.add(hmapValue);
    } else if (strType.equals("AVG")) {

      // Average No. of Cases per Month
      hmapValue.put("KEY", "AVGCASES");
      hmapValue.put("VALUE", "#Cases<BR> per Month");
      log.debug("hmapValue>>>>>>>>>" + hmapValue);
      alLinkList.add(hmapValue);


      if (!strSalesType.equals("50300")) {
        hmapValue = new HashMap();
        hmapValue.put("KEY", "ALLLOANERCASESINPERIOD");
        hmapValue.put("VALUE", "Loaner case in period");
        alLinkList.add(hmapValue);
      }
      // Annualized REVENUE per year
      /*
       * hmapValue = new HashMap(); hmapValue.put("KEY", "AVGREVENUE"); hmapValue.put("VALUE",
       * "Annualized Value"); alLinkList.add(hmapValue);
       */
    } else if (strType.equals("MET")) {

      // Metrics for No. of Cases per set per Month
      hmapValue = new HashMap();
      hmapValue.put("KEY", "METRICCASES");
      hmapValue.put("VALUE", strTurnsLabel);
      alLinkList.add(hmapValue);

      // Loaner Utilization
      if (!strSalesType.equals("50300")) {
        hmapValue = new HashMap();
        hmapValue.put("KEY", "METRICUTILIZATION");
        hmapValue.put("VALUE", "Loaner <BR>Utilization %");
        alLinkList.add(hmapValue);
      }

      // Metrics for List turns
      /*
       * hmapValue = new HashMap(); hmapValue.put("KEY", "METRICLISTTURNS"); hmapValue.put("VALUE",
       * "$ Turns"); alLinkList.add(hmapValue);
       */
    } else if (strType.equals("AIQUOTA")) {

      hmapValue = new HashMap();
      hmapValue.put("KEY", "QUOTA");
      hmapValue.put("VALUE", "Budget");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "QUOTA_TO_DATE");
      hmapValue.put("VALUE", "AI Usage<BR>(Quarter)");
      alLinkList.add(hmapValue);
      hmapValue = new HashMap();
      hmapValue.put("KEY", "AIQUOTAPERCENTAGE");
      hmapValue.put("VALUE", "% to <BR>AI Budget");
      alLinkList.add(hmapValue);

    } else if (strType.equals("ADDITIONALINVENTORY")) {

      hmapValue = new HashMap();
      hmapValue.put("KEY", "AAS");
      hmapValue.put("VALUE", "AAS");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "AAI");
      hmapValue.put("VALUE", "AAI");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PERCENT");
      hmapValue.put("VALUE", "% Additional");
      alLinkList.add(hmapValue);

    } else if (strType.equals("CONSIGNAAIBUDGET")) {
      hmapValue = new HashMap();
      hmapValue.put("KEY", "TOTAL_STD");
      hmapValue.put("VALUE", "STD");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "TOTAL_AAI_BUDJET");
      hmapValue.put("VALUE", "AAI Budget");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "AAI_PERCENT");
      hmapValue.put("VALUE", "% AAI Budget");
      alLinkList.add(hmapValue);


    }
    hmLinkDetails.put("LINKVALUE", alLinkList);

    return hmLinkDetails;
  }

  /**
   * reportVirtualvsActualByOnePart - This Method will Fetch information for Virtual report vs
   * Actual Report in terms of Part Number
   * 
   * @return RowSetDynaClass
   * @exception AppError
   */
  public RowSetDynaClass reportVirtualvsActualByOnePart(String strSetId, String strPartNum)
      throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;
    ResultSet rsOnePartSearch = null;
    CallableStatement csmt = null;
    Connection conn = null;

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;
    gmDBManager.setPrepareString("GM_PKG_SM_PART_CONSIGN_INFO.GM_PD_FCH_VIRVSACT_PART_SCH", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setString(1, strSetId);
    gmDBManager.setString(2, strPartNum);

    gmDBManager.execute();
    rsOnePartSearch = (ResultSet) gmDBManager.getObject(3);
    resultSet = gmDBManager.returnRowSetDyna(rsOnePartSearch);
    gmDBManager.close();

    return resultSet;

  }

  private HashMap modifyFooterColumn(HashMap hmReturn) {
    GmFooterCalcuationBean calcuationBean = null;
    ArrayList alDataStore = new ArrayList();
    ArrayList alNumerator = new ArrayList();
    ArrayList alDenominator = new ArrayList();
    ArrayList alOperands = new ArrayList();
    HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HMPARAM"));
    String strLoanerType = GmCommonClass.parseNull((String) hmParam.get("LOANERTYPE"));
    String strTurns = GmCommonClass.parseZero((String) hmParam.get("TURNS"));
    String strSystemOnlyCondition =
        GmCommonClass.parseNull((String) hmParam.get("SYSTEMONLYCONDITION"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("hAction"));

    // String strSalesType = GmCommonClass.parseNull((String) hmParam.get("SalesType"));

    log.debug("strAction in salesconsignment bean" + strAction);
    calcuationBean = new GmFooterCalcuationBean();
    // calcuationBean.setColumnNameToModify(strTurnsLabel);
    calcuationBean.setColumnNameToModify("#Cases<BR> per M/Set");

    if (strSalesType.equals("50301")) {
      calcuationBean.setOperator("LOANERCASESPERMONTH");
      alOperands.add("#Cases<BR> per Month");
      alOperands.add(strTurns);

      // if(!strAction.equalsIgnoreCase("LOADGCONS")){
      if (!("ONLYSYSTEM").equalsIgnoreCase(strSystemOnlyCondition)) {
        alOperands.add(strBaselineName);
      } else {
        alOperands.add("Loaner Sets");
      }
      calcuationBean.setOperands(alOperands);

      /*
       * calcuationBean.setOperator("LTURNS"); alOperands.add("#Cases<BR> per Month");
       * alOperands.add(strTurns); alOperands.add(strBaselineName);
       * calcuationBean.setOperands(alOperands);
       */
    } else {
      calcuationBean.setOperator("DIV");
      calcuationBean.setOperand("#Cases<BR> per Month");
      calcuationBean.setOperand(strBaselineName);
    }
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean();
    calcuationBean.setColumnNameToModify("Loaner <BR>Utilization %");
    calcuationBean.setOperator("PERCENTAGE");
    alNumerator = new ArrayList();
    alDenominator = new ArrayList();
    alNumerator.add("Loaner case in period");
    alDenominator.add("Loaner Usage");
    calcuationBean.setNumerator(alNumerator);
    calcuationBean.setDenominator(alDenominator);
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean();
    calcuationBean.setColumnNameToModify("$ Turns");
    calcuationBean.setOperator("DIV");
    calcuationBean.setOperand("Annualized Value");
    calcuationBean.setOperand("$ Value");
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean();
    calcuationBean.setColumnNameToModify("#C/M/S<br> Target");
    calcuationBean.setOperator("MAKENULL");
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean();
    calcuationBean.setColumnNameToModify("Turns Target");
    calcuationBean.setOperator("MAKENULL");
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean();
    calcuationBean.setColumnNameToModify("%Exp Cases");
    calcuationBean.setOperator("MAKENULL");
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean();
    alNumerator = new ArrayList();
    alDenominator = new ArrayList();
    calcuationBean.setColumnNameToModify("% Additional");
    calcuationBean.setOperator("PERCENTAGE");
    alNumerator.add("AAS");
    alNumerator.add("STD");
    alNumerator.add("AAI");
    alDenominator.add("Total <BR>$Value");
    calcuationBean.setNumerator(alNumerator);
    calcuationBean.setDenominator(alDenominator);
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean();
    alNumerator = new ArrayList();
    alDenominator = new ArrayList();
    calcuationBean.setColumnNameToModify("% to <BR>AI Quota");
    calcuationBean.setOperator("PERCENTAGE");
    alNumerator.add("AI Usage<BR>(Quarter)");
    alDenominator.add("Quota");
    calcuationBean.setNumerator(alNumerator);
    calcuationBean.setDenominator(alDenominator);
    alDataStore.add(calcuationBean);

    /*
     * calcuationBean = new GmFooterCalcuationBean();
     * calcuationBean.setColumnNameToModify("Excess<br>#Sets"); calcuationBean.setOperator("SUB");
     * calcuationBean.setOperand(strBaselineName); calcuationBean.setOperand("@Target<br>#Sets");
     * alDataStore.add(calcuationBean);
     */

    hmReturn = gmCrossReport.reCalculateFooter(hmReturn, alDataStore, strAction);

    return hmReturn;
  }

  public ArrayList fetchSharedSystem(String strSystemId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alSharedSet = new ArrayList();
    StringBuffer sbHeaderQuery = new StringBuffer();

    sbHeaderQuery.append(" SELECT DISTINCT T207A.C207_MAIN_SET_ID MAINID ");
    sbHeaderQuery.append(" , DECODE ('");
    sbHeaderQuery.append(strSystemId);
    sbHeaderQuery
        .append("',T207A.C207_MAIN_SET_ID,'Consignment Set Details','Related Inventory') TITLE ");
    sbHeaderQuery.append(" , DECODE ('");
    sbHeaderQuery.append(strSystemId);
    sbHeaderQuery.append("',T207A.C207_MAIN_SET_ID,1,2) SEQ ");
    sbHeaderQuery.append(" FROM T207A_SET_LINK T207A ");
    sbHeaderQuery.append(" WHERE T207A.C207_LINK_SET_ID IN ");
    sbHeaderQuery.append(" (SELECT T207AI.C207_LINK_SET_ID ");
    sbHeaderQuery.append(" FROM T207A_SET_LINK T207AI ");
    sbHeaderQuery.append(" 	WHERE T207AI.C901_SHARED_STATUS IS NOT NULL ");
    sbHeaderQuery.append(" AND T207AI.C207_MAIN_SET_ID = '");
    sbHeaderQuery.append(strSystemId);
    sbHeaderQuery.append("') AND T207A.C901_TYPE <> 20004 ");
    sbHeaderQuery.append(" ORDER BY SEQ ");

    log.debug(" Query for getting shared system " + sbHeaderQuery.toString());

    alSharedSet = gmDBManager.queryMultipleRecords(sbHeaderQuery.toString());
    return alSharedSet;
  }

  public ArrayList fetchSystemMapping() throws AppError {
    ArrayList alSystemMapping = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_part_consign_info.gm_fch_systemmapping", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alSystemMapping = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));

    gmDBManager.close();
    return alSystemMapping;
  }

}// End of GmSalesConsignReportBean
