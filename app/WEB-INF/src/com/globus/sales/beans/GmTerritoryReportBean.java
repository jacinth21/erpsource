/*
 * Module: GmSalesReportBean.java Author: RichardK Project: Globus Medical App Date-Written: Mar
 * 2005 Security: Unclassified Description: This beans will be used to generate all kind of sames
 * report
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.sales.beans;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabFormat;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmTerritoryReportBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmTerritoryReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmTerritoryReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /*
   * public static void main(String[] args) throws Exception { GmTerritoryReportBean t = new
   * GmTerritoryReportBean (); }
   * 
   * public GmTerritoryReportBean () throws Exception { HashMap hmapFilter = new HashMap();
   * log.debug("Begin..."); //reportTerritoryMapping(hmapFilter); reportTerritorySalesToQuota();
   * GmSalesCompBean gmSales = new GmSalesCompBean(); log.debug("Completed FROM THIS BEAN");
   * log.debug("Begin...FROM GmSalesCompBean"); HashMap hmReturn =
   * gmSales.loadTerrCompSales(hmapFilter); log.debug("hmReturn"+hmReturn);
   * log.debug("Completed.. GmSalesCompBean"); } //
   */
  /**
   * reportYTDByDist - This Method is used to fetch Territory YTD for the selected period Will have
   * additional Filter, Yet to Decide on the same
   * 
   * @param Hashmap contains filter condition
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap reportTerritoryMapping(HashMap hmparam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    StringBuffer sbCompQuery = new StringBuffer();
    String strCompFilter = "";
    String strCurrType = "";

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details

    // Below variable is used for cross tab format
    ArrayList alRowDetail = new ArrayList();
    ArrayList alColDetail = new ArrayList();
    ArrayList alDetailField = new ArrayList();

    HashMap hmapGroupInfo = new HashMap();
    HashMap hmapValue = new HashMap();

    GmCrossTabReport gmCrossReport = new GmCrossTabReport();
    GmCrossTabFormat gmFormat = new GmCrossTabFormat();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strReportType = (String) hmparam.get("Action");
    strCompFilter = GmCommonClass.parseNull((String) hmparam.get("COMPID"));

    initializeParameters(hmparam);
    strCurrType = (String) hmparam.get("CURRTYPE");
    sbWhereCondtion.append("");

    // The below code is added to make the alge quota to 0 for Globus rep and vice versa.
    sbCompQuery.append(" ,(SELECT t710.c901_company_id def_comp,t703.c703_sales_rep_id rep_id ");
    sbCompQuery
        .append(" FROM t710_sales_hierarchy t710, t701_distributor t701, t703_sales_rep t703 ");
    sbCompQuery.append(" WHERE t710.c901_area_id = t701.c701_region ");
    sbCompQuery.append(" AND t701.c701_distributor_id = t703.c701_distributor_id ");
    sbCompQuery.append(" AND t701.c701_void_fl is null ");
    sbCompQuery.append(" AND t703.c703_void_fl IS NULL ");
    sbCompQuery.append(" AND t710.c710_active_fl = 'Y') comp ");

    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" AND T709.C709_START_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        sbWhereCondtion.append(" AND");
        sbWhereCondtion.append(" T709.C709_END_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" AND T709.C709_START_DATE >= ");
      sbWhereCondtion.append("TO_DATE('01/01/' || TO_CHAR(SYSDATE,'YYYY'),'MM/DD/YYYY') ");
      sbWhereCondtion.append("AND T709.C709_END_DATE <= ");
      sbWhereCondtion.append(" TO_DATE('12/31/' || TO_CHAR(SYSDATE,'YYYY'),'MM/DD/YYYY') ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getCurrentYearRange();
    }

    // Query to Fetch the date
    sbHeaderQuery.append(" SELECT DISTINCT TO_CHAR(T709.C709_START_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append(" FROM T709_QUOTA_BREAKUP T709,  T702_TERRITORY T702  ");
    sbHeaderQuery.append(" WHERE T709.C709_REFERENCE_ID = T702.C702_TERRITORY_ID  ");
    sbHeaderQuery.append(" AND T709.C901_TYPE = 20750 ");
    sbHeaderQuery.append(" AND	NVL(T702.C702_ACTIVE_FL,'Y') = 'Y'  ");
    sbHeaderQuery.append(" AND	NVL(T702.C702_DELETE_FL,'N') = 'N' ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(" ORDER BY TO_DATE(R_DATE,'Mon YY') ");

    // ***********************************************************
    // Query to Fetch the Territory Information
    // Distributor Query updated as on Apr 06 (reason query was very slow)
    // ***********************************************************

    if (strReportType.equals("LoadQuota")) {
      sbDetailQuery.append(" SELECT		NVL(V700.AD_ID ,0) AD_ID ");
      sbDetailQuery
          .append(" ,NVL(V700.AD_NAME,'*No AD') || ' - (' || V700.REGION_NAME || ')'  AD_NAME  ");
      sbDetailQuery.append(" ,NVL(V700.D_ID,0) D_ID    ");
      sbDetailQuery.append(" ,NVL(V700.D_NAME,' *No Distributor') D_NAME  ");
      sbDetailQuery.append(" ,NVL(V700.REP_ID,0) REP_ID  ");
      sbDetailQuery.append(" ,NVL(V700.REP_NAME, '*No Rep Name')REP_NAME  ");
      sbDetailQuery.append(" ,TER_ID   ");
      sbDetailQuery
          .append(" ,TER_NAME  || ' - (' || NVL(REP_NAME, '*No Rep Name')  || ')'  TER_NAME  ");
      sbDetailQuery.append(" ,TO_CHAR(T709.C709_START_DATE, 'Mon YY') R_DATE  ");
      sbDetailQuery.append(" ,T709.C709_START_DATE  ");
      sbDetailQuery.append(" ,NVL(DECODE(" + strCurrType
          + ",105460,T709.C709_QUOTA_BREAKUP_AMT_BASE,T709.C709_QUOTA_BREAKUP_AMT),0)  AMOUNT  ");
      // sbDetailQuery.append(" ,DECODE(v700.def_comp,"+strCompFilter+",NVL(T709.C709_QUOTA_BREAKUP_AMT,0),0) AMOUNT");
      sbDetailQuery.append(" ,T709.C709_REFERENCE_ID  ");
      sbDetailQuery.append(" FROM  			T709_QUOTA_BREAKUP T709  ");
      sbDetailQuery
          .append(" ,( SELECT DISTINCT V700.AD_ID, V700.AD_NAME, V700.REGION_ID, V700.REGION_NAME,  ");
      sbDetailQuery
          .append(" 	V700.D_ID, V700.D_NAME, V700.TER_ID, V700.TER_NAME, V700.REP_ID, V700.REP_NAME, comp.def_comp, V700.REP_COMPID, v700.D_COMPID ");
      sbDetailQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700");

      sbDetailQuery.append(sbCompQuery);

      sbDetailQuery.append(" WHERE v700.rep_id = comp.rep_id     ");
      sbDetailQuery.append(" ) V700 ");
    } else if (strReportType.equals("LoadQuotaVP")) {
      sbDetailQuery
          .append(" SELECT NVL(V700.VP_ID ,0) AD_ID  ,NVL(V700.VP_NAME,'*No VP') AD_NAME ");
      sbDetailQuery
          .append(" ,NVL(V700.AD_ID,0) D_ID ,NVL(V700.REGION_NAME,'*No AD') || ' - (' || V700.AD_NAME || ')' D_NAME   ,NVL(V700.D_ID,0) TER_ID ");
      sbDetailQuery.append(" ,NVL(V700.D_NAME, '*No Distributor Name') TER_NAME ");
      sbDetailQuery
          .append(" ,TO_CHAR(T709.C709_START_DATE, 'Mon YY') R_DATE   ,T709.C709_START_DATE ");
      sbDetailQuery.append(" ,SUM(NVL(DECODE(" + strCurrType
          + ",105460,T709.C709_QUOTA_BREAKUP_AMT_BASE,T709.C709_QUOTA_BREAKUP_AMT),0))  AMOUNT  ");
      // sbDetailQuery.append(", SUM (DECODE(v700.def_comp,"+strCompFilter+",NVL(T709.C709_QUOTA_BREAKUP_AMT,0),0)) AMOUNT");
      sbDetailQuery.append(" FROM  			T709_QUOTA_BREAKUP T709  ");
      sbDetailQuery
          .append(" ,( SELECT DISTINCT v700.AD_ID, v700.AD_NAME, v700.REGION_ID, v700.REGION_NAME, comp.def_comp,  ");
      sbDetailQuery
          .append(" 	v700.D_ID, v700.D_NAME, v700.TER_ID, v700.REP_ID, v700.REP_NAME, V700.REP_COMPID, v700.D_COMPID, T901.C901_CODE_ID VP_ID, GET_CODE_NAME(get_code_name_alt(T901.C901_CODE_ID)) VP_NAME ");
      sbDetailQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700,  T901_CODE_LOOKUP T901 ");

      sbDetailQuery.append(sbCompQuery);

      sbDetailQuery.append(" WHERE V700.REGION_ID = T901.C901_CODE_ID     ");
      sbDetailQuery.append("   AND v700.rep_id = comp.rep_id     ");
      sbDetailQuery.append(" ) V700 ");
    }
    sbDetailQuery.append(" WHERE 		 V700.REP_ID = T709.C703_SALES_REP_ID  ");
    sbDetailQuery.append(" AND T709.C901_TYPE = 20750 ");
    sbDetailQuery.append(sbWhereCondtion.toString());
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND V700.REP_ID IN ( ");
      sbDetailQuery
          .append(" SELECT DISTINCT REP_ID FROM V700_TERRITORY_MAPPING_DETAIL T501 WHERE ");
      sbDetailQuery.append(strCondition);
      sbDetailQuery.append(" ) ");
    }
    if (strReportType.equals("LoadQuotaVP")) {
      sbDetailQuery
          .append(" GROUP BY V700.VP_ID, V700.AD_ID,V700.REGION_NAME, V700.AD_NAME, V700.D_ID, V700.VP_NAME, V700.D_NAME,T709.C709_START_DATE");
      sbDetailQuery.append(" ORDER BY AD_NAME, D_NAME, TER_NAME, C709_START_DATE  ");
    } else {
      sbDetailQuery.append(" ORDER BY AD_NAME, D_NAME, REP_NAME, C709_START_DATE  ");
    }

    /************************************************************
     * Report setting details to specify row and column information alRowDetail Holds Row
     * Information alColDetail Holds Column Information alDetailField Holds detail field value
     * (Month Data)
     *********************************************************/

    // Territory Information Level I
    hmapValue.put("KEY", "TER_NAME");
    hmapValue.put("ID", "TER_ID");
    alRowDetail.add(hmapValue);

    // Distributor Information Level II
    hmapValue = null;
    hmapValue = new HashMap();
    hmapValue.put("KEY", "D_NAME");
    hmapValue.put("ID", "D_ID");
    alRowDetail.add(hmapValue);

    // Area Director Information Level III
    hmapValue = null;
    hmapValue = new HashMap();
    hmapValue.put("KEY", "AD_NAME");
    hmapValue.put("ID", "AD_ID");
    alRowDetail.add(hmapValue);

    alColDetail.add("R_DATE");

    alDetailField.add("AMOUNT");

    hmapGroupInfo.put("ROW", alRowDetail);
    hmapGroupInfo.put("COLUMN", alColDetail);
    hmapGroupInfo.put("DETAILFIELD", alDetailField);

    log.debug(" reportTerritoryMapping " + sbDetailQuery.toString());
    log.debug(" header reportTerritoryMapping " + sbHeaderQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString(),
            hmapGroupInfo);

    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportTerritorySalesToQuota - This Method is used to fetch Territory
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap reportTerritorySalesToQuota(HashMap hmFilter) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();

    GmSalesCompBean gmSales = new GmSalesCompBean();

    // Database connection information
    DBConnectionWrapper dbCon = null;
    Connection conn = null;
    ResultSet rs = null;
    Statement stmt = null;

    HashMap hmResult = new HashMap();
    HashMap hmapFinalValue = new HashMap();

    ArrayList alReturn = new ArrayList();
    ArrayList alReturnFromThis = new ArrayList();
    ArrayList alReturnFromOther = new ArrayList();

    String strCondition = "";
    strCondition = (String) hmFilter.get("Condition");

    try {
      dbCon = new DBConnectionWrapper();
      conn = dbCon.getConnection();

      alReturnFromThis = gmSales.fetchTerritorySalesQuota(hmFilter);
      int intSizeOne = alReturnFromThis.size();

      log.debug("alReturnFromThis = " + alReturnFromThis);

      HashMap hmReturn = null;
      hmFilter.put("TYPE", "QUOTA");

      // ***************************************
      // below code to fetch sales information
      // ***************************************
      // hmReturn = gmSales.loadTerrCompSales(hmFilter);

      alReturnFromOther = (ArrayList) hmReturn.get("Details");

      int intSizeTwo = alReturnFromOther.size();
      // log.debug("alReturnFromOther:"+alReturnFromOther);
      // log.debug("alReturnFromOther:"+intSizeTwo);

      // ****************************************************
      // Below code to merge the quota and sales info
      // ****************************************************
      sbDetailQuery.setLength(0);
      sbDetailQuery.append("SELECT AD_ID, AD_NAME, REGION_ID RGID, ");
      sbDetailQuery
          .append(" NVL(V700.REGION_NAME,'*NO Ao') || ' - (' || V700.AD_NAME|| ')' RGNAME ");
      sbDetailQuery.append(" ,D_ID DID, D_NAME DNAME, ");
      sbDetailQuery.append(" TER_ID ID, TER_NAME NAME, REP_ID, REP_NAME RNAME");
      sbDetailQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700");
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbDetailQuery.append(" WHERE ");
        sbDetailQuery.append(strCondition);
      }
      sbDetailQuery.append(" GROUP BY AD_ID, AD_NAME, REGION_ID, REGION_NAME, ");
      sbDetailQuery.append(" D_ID, D_NAME, TER_ID, TER_NAME, REP_ID, REP_NAME ");
      sbDetailQuery.append("  ORDER BY REGION_NAME, D_NAME, REP_NAME, TER_NAME ");

      log.debug("sbDetailQuery.toString() = " + sbDetailQuery.toString());

      ArrayList alMap = dbCon.queryMultipleRecords(sbDetailQuery.toString());
      int intMapSize = alMap.size();

      HashMap hmMap = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmFinal = new HashMap();
      HashMap hmTempTwo = new HashMap();

      String strTerrrId = "";
      String strMapId = "";
      int k = 0;
      String strTerrrIdInner = "";
      boolean blFlag = false;

      for (int m = 0; m < intMapSize; m++) {
        hmFinal = new HashMap();
        hmMap = (HashMap) alMap.get(m);
        strMapId = (String) hmMap.get("ID");
        // log.debug("strMapId:"+strMapId);
        for (int i = 0; i < intSizeOne; i++) {
          hmTemp = (HashMap) alReturnFromThis.get(i);
          strTerrrId = (String) hmTemp.get("ID");
          if (strTerrrId.equals(strMapId)) {
            hmFinal.putAll(hmTemp);
            blFlag = true;
            break;
          }
        }

        for (int j = 0; j < intSizeTwo; j++) {
          hmTempTwo = (HashMap) alReturnFromOther.get(j);
          strTerrrIdInner = (String) hmTempTwo.get("ID");
          if (strTerrrIdInner.equals(strMapId)) {
            hmFinal.putAll(hmTempTwo);
            blFlag = true;
            break;
          }
        }
        if (blFlag) {
          hmFinal.putAll(hmMap);
          alReturn.add(hmFinal);
          blFlag = false;
        }
      }

      // Final Value information
      hmapFinalValue.put("REPORT", alReturn);
      hmapFinalValue.put("FDate", gmSales.GetDate(hmFilter));
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    } finally {
      try {
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (Exception e) {
        throw new AppError(e);
      }
    }
    return hmapFinalValue;
  }// End of reportTerritorySalesToQuota


  public HashMap fetchTerritoryPerformanceReport(HashMap hmFilter) throws AppError {
    HashMap hmapFinalValue = new HashMap();

    try {
      GmSalesCompBean gmSales = new GmSalesCompBean(getGmDataStoreVO());
      GmSalesCompHistBean gmSalesCompHistBean = new GmSalesCompHistBean(getGmDataStoreVO());
      GmCommonBean gmcom = new GmCommonBean(getGmDataStoreVO());
      int workingMon = 0;
      int workingMonCompl = 0;
      int workingQu = 0;
      int workingQuCompl = 0;
      int workingYe = 0;
      int workingYeCompl = 0;
      String strReportType = "";


      ArrayList alReturn = new ArrayList();
      ArrayList alSalesQuota = new ArrayList();
      ArrayList alSalesData = new ArrayList();
      ArrayList alMergeData = new ArrayList();


      alSalesQuota = gmSales.fetchTerritorySalesQuota(hmFilter);

      strReportType = GmCommonClass.parseNull((String) hmFilter.get("ReportType"));
      if (strReportType.equals("Historical")) {
        alSalesData = gmSalesCompHistBean.fetchTerritorySalesHist(hmFilter);
      } else {
        alSalesData = gmSales.fetchTerritorySales(hmFilter);
      }

      alMergeData = gmSales.fetchSalesMergeData(hmFilter);

      log.debug(" alSalesQuota...= " + alSalesQuota);

      // log.debug("alSalesQuota = " + alSalesQuota.size());
      // log.debug("alSalesData = " + alSalesData.size());
      // log.debug("alMergeData = " + alMergeData.size());

      int intSizeQuota = alSalesQuota.size();
      int intSizeSales = alSalesData.size();
      int intMergeDataSize = alMergeData.size();

      HashMap hmMap = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmFinal = new HashMap();
      HashMap hmTempTwo = new HashMap();
      HashMap hmDate = new HashMap();
      hmDate = gmSales.GetDate(hmFilter);
      log.debug("hmDate" + hmDate);
      String year = (String) hmFilter.get("ToYear");
      if (year == null) {
        year = (String) hmDate.get("TOYEAR");
      }
      DateFormat dateFormat = new SimpleDateFormat("yyyy");
      Date date = new Date();
      String curYear = dateFormat.format(date).toString();

      String month = (String) hmFilter.get("ToMonth");
      if (month == null) {
        month = (String) hmDate.get("TOMONTH");
      }
      dateFormat = new SimpleDateFormat("MM");
      String curMonth = dateFormat.format(date).toString();
      int curMonthInt = Integer.parseInt(curMonth);
      int temp = Integer.parseInt(curMonth) / 3;
      log.debug("1st temp" + temp);
      if (curMonthInt % 3 != 0) {
        temp = temp + 1;
      }
      int qtyEndMonth = temp * 3;
      int qtyBegMonth = qtyEndMonth - 2;
      // to find selected month with in the Quarter
      boolean quarterFlag = false;
      int monthInt = Integer.parseInt(month);
      log.debug("monthInt........." + monthInt);
      if (monthInt >= qtyBegMonth && monthInt <= qtyEndMonth) {
        quarterFlag = true;
      }

      workingMon = gmcom.getNoOfWorkingdays("Month");
      workingMonCompl = gmcom.getNoOfWorkingdays("MWC");
      if (workingMonCompl == 0) {
        workingMonCompl = 1; // if it is 1st day of the month
      }

      workingQu = gmcom.getNoOfWorkingdays("Quarter");
      workingQuCompl = gmcom.getNoOfWorkingdays("QWC");
      if (workingQuCompl == 0) {
        workingQuCompl = 1; // if it is 1st day of Quarter
      }
      workingYe = gmcom.getNoOfWorkingdays("Year");
      workingYeCompl = gmcom.getNoOfWorkingdays("YWC");
      if (workingYeCompl == 0) {
        workingYeCompl = 1; // if it is 1st day of the Year
      }
      String strTerrrId = "";
      String strMapId = "";
      int k = 0;
      String strTerrrIdInner = "";
      boolean blFlag = false;



      for (int m = 0; m < intMergeDataSize; m++) {

        hmFinal = new HashMap();
        hmMap = (HashMap) alMergeData.get(m);
        strMapId = (String) hmMap.get("ID");
        // log.debug("strMapId:"+strMapId);


        for (int i = 0; i < intSizeQuota; i++) {
          hmTemp = (HashMap) alSalesQuota.get(i);
          strTerrrId = (String) hmTemp.get("ID");
          if (strTerrrId.equals(strMapId)) {
            hmFinal.putAll(hmTemp);
            blFlag = true;
            break;
          }
        }



        for (int j = 0; j < intSizeSales; j++) {
          hmTempTwo = (HashMap) alSalesData.get(j);
          strTerrrIdInner = (String) hmTempTwo.get("ID");
          if (strTerrrIdInner.equals(strMapId)) {
            hmFinal.putAll(hmTempTwo);
            blFlag = true;
            break;
          }
        }

        if (blFlag) {
          // *** Calculating the %Quota information
          double dbSalesMTD = 0.0;
          double dbQuotaMTD = 0.0;

          double dbSalesQTD = 0.0;
          double dbQuotaQTD = 0.0;

          double dbSalesYTD = 0.0;
          double dbQuotaYTD = 0.0;

          DecimalFormat df = new DecimalFormat("0.####");
          double dbTemp = 0.0;
          String strTemp = "";

          // YTDGTHDLR YTDGTHPER

          // *******************************************************************
          // Putting the month sales, quota and %quota information
          // *******************************************************************
          Double salesMTD = (Double) hmFinal.get("MTD");
          Double quotaMTD = (Double) hmFinal.get("QMTD");
          Double prevSalesMTD = (Double) hmFinal.get("PMTD");



          Double salesGrowthMTD = (Double) hmFinal.get("MTDGTHDLR");
          Double salesGrowthMTDPer = (Double) hmFinal.get("MTDGTHPER");
          if (quotaMTD != null) {
            dbQuotaMTD = quotaMTD.doubleValue();
          }

          if (salesMTD != null) {
            dbSalesMTD = salesMTD.doubleValue();
            // dbQuotaMTD = quotaMTD.doubleValue();
            if (dbQuotaMTD != 0) {
              dbTemp = dbSalesMTD / dbQuotaMTD;
            } else {
              dbTemp = 0.0;
            }
          }
          strTemp = df.format(dbTemp);
          double salesGrowthExpMTD = 0.0;
          if (dbSalesMTD != 0 && month.equals(curMonth) && year.equals(curYear)) {
            salesGrowthExpMTD = ((dbSalesMTD / workingMonCompl) * workingMon) + dbSalesMTD;
          }

          hmFinal.put("MTD", GmCommonClass.parseDouble(salesMTD));
          hmFinal.put("PMTD", GmCommonClass.parseDouble(prevSalesMTD));
          hmFinal.put("QMTD", GmCommonClass.parseDouble(quotaMTD));

          hmFinal.put("MTDGTHDLR", GmCommonClass.parseDouble(salesGrowthMTD));
          hmFinal.put("MTDGTHPER", GmCommonClass.parseDouble(salesGrowthMTDPer));
          hmFinal.put("EMTD", new Double(salesGrowthExpMTD));

          hmFinal.put("MQPER", new Double(strTemp));

          // *******************************************************************
          // Putting the quarter sales, quota and %quota information
          // *******************************************************************
          Double salesQTD = (Double) hmFinal.get("QTD");
          Double quotaQTD = (Double) hmFinal.get("QQTD");
          Double prevSalesQTD = (Double) hmFinal.get("PQTD");

          Double salesGrowthQTD = (Double) hmFinal.get("QTDGTHDLR");
          Double salesGrowthQTDPer = (Double) hmFinal.get("QTDGTHPER");
          if (quotaQTD != null) {
            dbQuotaQTD = quotaQTD.doubleValue();
          }
          if (salesQTD != null) {
            dbSalesQTD = salesQTD.doubleValue();
            // dbQuotaQTD = quotaQTD.doubleValue();
            if (dbQuotaQTD != 0)
              dbTemp = dbSalesQTD / dbQuotaQTD;
            else
              dbTemp = 0.0;
          }
          strTemp = df.format(dbTemp);
          double salesGrowthExpQTD = 0.0;
          if (dbSalesQTD != 0 && year.equals(curYear) && quarterFlag) {
            salesGrowthExpQTD = ((dbSalesQTD / workingQuCompl) * workingQu) + dbSalesQTD;
          }


          hmFinal.put("QTD", GmCommonClass.parseDouble(salesQTD));
          hmFinal.put("PQTD", GmCommonClass.parseDouble(prevSalesQTD));
          hmFinal.put("QQTD", GmCommonClass.parseDouble(quotaQTD));

          hmFinal.put("QTDGTHDLR", GmCommonClass.parseDouble(salesGrowthQTD));
          hmFinal.put("QTDGTHPER", GmCommonClass.parseDouble(salesGrowthQTDPer));
          hmFinal.put("QQPER", new Double(strTemp));
          hmFinal.put("EQTD", new Double(salesGrowthExpQTD));

          // *******************************************************************
          // Putting the year sales, quota and %quota information
          // *******************************************************************
          Double salesYTD = (Double) hmFinal.get("YTD");
          Double quotaYTD = (Double) hmFinal.get("QYTD");
          Double prevSalesYTD = (Double) hmFinal.get("PYTD");

          Double salesGrowthYTD = (Double) hmFinal.get("YTDGTHDLR");
          Double salesGrowthYTDPer = (Double) hmFinal.get("YTDGTHPER");
          if (quotaYTD != null) {
            dbQuotaYTD = quotaYTD.doubleValue();
          }

          if (salesYTD != null) {
            dbSalesYTD = salesYTD.doubleValue();
            // dbQuotaYTD = quotaYTD.doubleValue();
            if (dbQuotaYTD != 0)
              dbTemp = dbSalesYTD / dbQuotaYTD;
            else
              dbTemp = 0.0;
          }
          double salesGrowthExpYTD = 0.0;
          // Projected
          if (dbSalesYTD != 0 && year.equals(curYear)) {
            salesGrowthExpYTD = ((dbSalesYTD / workingYeCompl) * workingYe) + dbSalesYTD;
          }


          strTemp = df.format(dbTemp);

          hmFinal.put("YQPER", new Double(strTemp));
          hmFinal.put("YTD", GmCommonClass.parseDouble(salesYTD));
          hmFinal.put("PYTD", GmCommonClass.parseDouble(prevSalesYTD));
          hmFinal.put("QYTD", GmCommonClass.parseDouble(quotaYTD));
          hmFinal.put("YTDGTHDLR", GmCommonClass.parseDouble(salesGrowthYTD));
          hmFinal.put("YTDGTHPER", GmCommonClass.parseDouble(salesGrowthYTDPer));
          hmFinal.put("EYTD", new Double(salesGrowthExpYTD));

          hmFinal.putAll(hmMap);
          // *******************************************************************

          // log.debug("hmFinal = " + hmFinal);
          alReturn.add(hmFinal);
          blFlag = false;
        }
      }

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("REPORT", alReturn);
      hmapFinalValue.put("FDate", gmSales.GetDate(hmFilter));
      // hmapFinalValue.put("FDate",gmSales.GetDate(hmDate));
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }
    return hmapFinalValue;
  }

  /**
   * reportRegnDistTerrMapping - This method will
   * 
   * @return ArrayList
   * @exception AppError
   */
  public RowSetDynaClass reportRegnDistTerrMapping(String strCmpLangId) throws AppError {
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;

    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "V700");
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");
    String strLangId = GmCommonClass.parseNull(getGmDataStoreVO().getCmplangid());
    String strComId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    try {

      DBConnectionWrapper dbCon = null;
      dbCon = new DBConnectionWrapper();

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT REGION_NAME RGNAME," + strDistNmColumn + " DNAME, "
          + strAccountNmColumn + " ANAME, AC_ID ACCID ,TER_NAME TNAME, ");
      sbQuery.append(" " + strRepNmColumn + " RNAME,V700.REP_ID REPID,ACC.C704_BILL_CITY CITY, ");
      sbQuery
          .append(" NVL(GET_CODE_NAME_ALT(ACC.C704_BILL_STATE),'-') STATE, ACC.C704_BILL_ZIP_CODE ZIP,DEALER_ID DEALERID,DECODE('"
              + strLangId + "','103097',DEALER_NM_EN,DEALER_NM) DEALERNAME ");
      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700,T704_ACCOUNT ACC ");
      sbQuery.append(" WHERE V700.AC_ID = ACC.C704_ACCOUNT_ID ");
      sbQuery.append(" AND acc.c1900_company_id='" + strComId + "'");
      sbQuery.append(" order by REGION_NAME ,DNAME, ANAME ");
      log.debug(sbQuery.toString());
      resultSet = dbCon.QueryDisplayTagRecordset(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmTerritoryReportBean:reportRegnDistTerrMapping",
          "Exception is:" + e);
    }
    return resultSet;
  } // End of reportRegnDistTerrMapping

  /*
   * Method to filter the Quota Performance, if the value is zero for the whole row that is
   * displayed, then that row needs to be removed
   */
  public ArrayList fliterQuotaPerformance(ArrayList alReturn, HashMap hmParam) {

    Double d = new Double(0);
    String strKey = "";
    String strValue = "";
    Map hmMap;
    Iterator sKeys;
    Iterator itr = alReturn.iterator();
    Iterator iKeySet = hmParam.keySet().iterator();
    boolean removeHashMap;
    ArrayList alColumns = new ArrayList();


    while (iKeySet.hasNext()) {
      strKey = (String) iKeySet.next();
      strValue = GmCommonClass.parseNull((String) hmParam.get(strKey));

      if ((strKey.equals("MONTH") || strKey.equals("SUMMARY"))
          && (strValue.equals("null") || strValue.equals("on"))) {
        alColumns.add("MTD");
        alColumns.add("QMTD");
        alColumns.add("MQPER");
        alColumns.add("EMTD");
      }

      if ((strKey.equals("QUARTER") || strKey.equals("SUMMARY"))
          && (strValue.equals("null") || strValue.equals("on"))) {
        alColumns.add("QTD");
        alColumns.add("QQTD");
        alColumns.add("QQPER");
        alColumns.add("EQTD");
      }

      if ((strKey.equals("YEAR") || strKey.equals("SUMMARY"))
          && (strValue.equals("null") || strValue.equals("on"))) {
        alColumns.add("YTD");
        alColumns.add("QYTD");
        alColumns.add("YQPER");
        alColumns.add("EYTD");
      }

      if ((strKey.equals("INCLUDEPREYEAR") || strKey.equals("SUMMARY"))
          && (strValue.equals("null") || strValue.equals("on"))) {
        alColumns.add("MTDGTHPER");
        alColumns.add("QTDGTHDLR");
        alColumns.add("QTDGTHPER");
        alColumns.add("MTDGTHDLR");
        alColumns.add("YTDGTHDLR");
        alColumns.add("YTDGTHPER");
        alColumns.add("PYTD");
        alColumns.add("PMTD");
        alColumns.add("PQTD");
      }
    }
    strKey = "";

    while (itr.hasNext()) {
      hmMap = (Map) itr.next();
      sKeys = hmMap.keySet().iterator();
      removeHashMap = true;

      while (sKeys.hasNext()) {
        strKey = (String) sKeys.next();

        if (alColumns.contains(strKey)) {

          d = (Double) hmMap.get(strKey);
        }

        if (d.doubleValue() > 0 || d.doubleValue() < 0) {
          removeHashMap = false;
          break;
        }
      }
      if (removeHashMap) {
        itr.remove();
      }

      d = new Double(0);
    }

    return alReturn;
  }

} // ENd of GmTerritoryReportBean
