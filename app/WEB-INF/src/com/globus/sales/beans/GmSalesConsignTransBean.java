package com.globus.sales.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmLogError;
import org.apache.log4j.Logger; 
import com.globus.common.beans.GmLogger;

public class GmSalesConsignTransBean {

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
		
	/**
	  	  * saveGroupSetMap -
	  	  * @param String,String,String
	  	  * @return String
   	  * @exception AppError
   	**/
		public String saveGroupSetMap(String strSetId,String strInputString,String strUserId)throws AppError
		{
			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();

			CallableStatement csmt = null;
			Connection conn = null;

			String strMsg = "";
			String strPrepareString = null;

			try
			{
				conn = dbCon.getConnection();

				strPrepareString = dbCon.getStrPrepareString("GM_SM_UPD_SET_LINK_DETAILS",4);
				csmt = conn.prepareCall(strPrepareString);
				/*
				 *register out parameter and set input parameters
				 */
				csmt.registerOutParameter(4,java.sql.Types.CHAR);
				csmt.setString(1,strSetId);
				csmt.setString(2,strInputString);
				csmt.setString(3,strUserId);
				
				csmt.execute();
				strMsg = csmt.getString(4);
				conn.commit();

			}catch(Exception e)
			{
				GmLogError.log("Exception in GmSalesConsignTransBean:saveGroupSetMap","Exception is:"+e);
				throw new AppError(e);
			}
			finally
			{
				try
				{
					if (csmt != null)
					{
						csmt.close();
					}//Enf of if  (csmt != null)
					if(conn!=null)
					{
						conn.close();		/* closing the Connections */
					}
				}
				catch(Exception e)
				{
					throw new AppError(e);
				}//End of catch
				finally
				{
					 csmt = null;
					 conn = null;
					 dbCon = null;
				}
			}
			return strMsg;
		} // End of saveGroupSetMap

}// End of GmSalesConsignTransBean
