
package com.globus.sales.beans;

import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * GmSalesEfficiencyScoreCardByLoanerReportBean - Contains the methods used for fetch the Scorecard - Exp. Loaner usage report screen
 * @author rajan
 *
 */
public class GmSalesEfficiencyScoreCardByLoanerReportBean extends GmSalesFilterConditionBean { 

/**
 * Code to Initialize the Logger Class.
 */
  Logger log = GmLogger.getInstance(this.getClass().getName());
  
  
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesEfficiencyScoreCardByLoanerReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * fetchLoanerUsageScoreCardDtls - used to fetch the  Scorecard - Exp. Loaner usage details 
   * @return
   * @throws AppError
   */
  public String fetchLoanerUsageScoreCardDtls(HashMap hmParam) throws AppError {
	    initializeParameters(hmParam); 
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
	    String strDateFmt = GmCommonClass.parseNull(getCompDateFmt());
	    
	    // to get the drilldown values
	    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
	    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
	    String strADId = GmCommonClass.parseNull((String) hmParam.get("ADID"));
	    
	    StringBuffer sbQuery = new StringBuffer();
	    String queryJSONRecord = "";
	    
	    sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT ('ad_name' value ad_name, 'dist_name' value D_NAME, 'system_name' value ");
	    sbQuery.append(" CRITICAL_SYSTEM_NAME, 'system_type_name' value CRITICAL_SYSTEM_TYPE_NAME, 'set_id' value SET_ID, 'set_name' value ");
	    sbQuery.append(" SET_NAME, 'placement_date' value PLACEMENT_DATE, 'return_date' value RETURN_DATE, 'adj_placement_date' value ");
	    sbQuery.append(" ADJUSTED_PLACEMENT_DATE, 'adj_return_date' value ADJUSTED_RETURN_DATE, 'loaner_id' value LOANER_ID, 'cn_id' value ");
	    sbQuery.append(" CONSIGNMENT_ID, 'etch_id' value ETCH_ID, 'status' value STATUS,");
	    // Add the rep_name and  ass_rep_name PC-3130 Scorecard - Loaner Usage screen to add Sales rep details
	    sbQuery.append(" 'rep_name' value REP_NAME, 'ass_rep_name' value  ASS_REP_NAME, ");
	    
	    sbQuery.append(" 'ln_cmpl' value exp_usage, 'ln_last_2_mon' value last2_mon_exp_usage, ");
	    sbQuery.append(" 'ln_proj' value loaner_proj_month_usage, 'ln_tot_proj' value proj_exp_usage) order by ad_name RETURNING CLOB) ");

	    sbQuery.append("  FROM ( SELECT v700.ad_name, v700.D_NAME, pbi_ln_usage.CRITICAL_SYSTEM_NAME ");
	    sbQuery.append(", pbi_ln_usage.CRITICAL_SYSTEM_TYPE_NAME, pbi_ln_usage.SET_ID, pbi_ln_usage.SET_NAME ");
	    sbQuery.append(", TO_CHAR (pbi_ln_usage.PLACEMENT_DATE,'"+strDateFmt+"') PLACEMENT_DATE, TO_CHAR (pbi_ln_usage.RETURN_DATE,'"+strDateFmt+"') RETURN_DATE, ");
	    sbQuery.append(" TO_CHAR (pbi_ln_usage.ADJUSTED_PLACEMENT_DATE,'"+strDateFmt+"') ");
	    sbQuery.append("ADJUSTED_PLACEMENT_DATE, TO_CHAR (pbi_ln_usage.ADJUSTED_RETURN_DATE,'"+strDateFmt+"') ADJUSTED_RETURN_DATE, ");
	    sbQuery.append("pbi_ln_usage.LOANER_ID, pbi_ln_usage.CONSIGNMENT_ID, pbi_ln_usage.ETCH_ID ");
	    sbQuery.append(", pbi_ln_usage.STATUS, pbi_ln_usage.EXP_USAGE  EXP_USAGE ");
	    sbQuery.append(" , pbi_ln_usage.LAST2_MON_EXP_USAGE LAST2_MON_EXP_USAGE ");
	    // Add the rep_name and  ass_rep_name PC-3130 Scorecard - Loaner Usage screen to add Sales rep details
	    sbQuery.append(" , pbi_ln_usage.REP_NAME REP_NAME,  pbi_ln_usage.ASS_REP_NAME ASS_REP_NAME ");
	    
	    //sbQuery.append(" , pbi_ln_usage.LOANER_PROJ_MONTH_USAGE LOANER_PROJ_MONTH_USAGE ");
	    //sbQuery.append(" , pbi_ln_usage.PROJ_EXP_USAGE PROJ_EXP_USAGE ");
	    sbQuery.append(" , round(((pbi_ln_usage.PROJ_EXP_USAGE)/(pbi_ln_usage.PROJ_WEEK_NUMBER))*(pbi_ln_usage.LOANER_PROJ_MONTH_USAGE),1) LOANER_PROJ_MONTH_USAGE ");
	    sbQuery.append(" , ((pbi_ln_usage.LAST2_MON_EXP_USAGE) + round(((pbi_ln_usage.PROJ_EXP_USAGE)/(pbi_ln_usage.PROJ_WEEK_NUMBER))*(pbi_ln_usage.LOANER_PROJ_MONTH_USAGE),1)) PROJ_EXP_USAGE ");

	    sbQuery.append(" FROM PBI_FAM_LOANER_USAGE_FACT pbi_ln_usage, pbi_fam_baseline_set_dim base_set ");
	    sbQuery.append( getAccessFilterClause() );  
	    sbQuery.append(" WHERE pbi_ln_usage.distributor_id = v700.d_id " );
	    
	    // to include only critical system
	    sbQuery.append(" AND base_set.set_id = pbi_ln_usage.set_id ");
	    sbQuery.append(" AND base_set.critical_system_fl = 'Y' ");
		
		// to exclude the On board distributors
	    sbQuery.append(" AND v700.VP_ID <> 4271942 ");
		
	    // to apply the drill down filter condition
	    if(!strSystemId.equals("")){
	    	sbQuery.append(" AND pbi_ln_usage.critical_system_id = '"+ strSystemId +"' ");
	    }
	    
	    if(!strDistId.equals("")){
	    	sbQuery.append(" AND pbi_ln_usage.distributor_id = "+ strDistId);
	    }
	    
	    if(!strADId.equals("")){
	    	sbQuery.append(" AND v700.ad_id = "+ strADId);
	    }
	    
	    sbQuery.append(" GROUP BY v700.ad_name, v700.D_NAME, pbi_ln_usage.CRITICAL_SYSTEM_NAME ");
	    sbQuery.append(" , pbi_ln_usage.CRITICAL_SYSTEM_TYPE_NAME, pbi_ln_usage.SET_ID, pbi_ln_usage.SET_NAME ");
	    sbQuery.append(" , pbi_ln_usage.PLACEMENT_DATE, pbi_ln_usage.RETURN_DATE, pbi_ln_usage.ADJUSTED_PLACEMENT_DATE ");
	    sbQuery.append(" , pbi_ln_usage.ADJUSTED_RETURN_DATE, pbi_ln_usage.LOANER_ID, pbi_ln_usage.CONSIGNMENT_ID ");
	    sbQuery.append(" , pbi_ln_usage.ETCH_ID, pbi_ln_usage.STATUS ");
	    //
	    sbQuery.append(" , pbi_ln_usage.EXP_USAGE, pbi_ln_usage.LAST2_MON_EXP_USAGE ");
	    // Add the rep_name and  ass_rep_name PC-3130 Scorecard - Loaner Usage screen to add Sales rep details
	    sbQuery.append(" ,  pbi_ln_usage.LOANER_PROJ_MONTH_USAGE, pbi_ln_usage.PROJ_EXP_USAGE"
	    		+ ", pbi_ln_usage.PROJ_WEEK_NUMBER, pbi_ln_usage.REP_NAME,  pbi_ln_usage.ASS_REP_NAME ) ");
	    
	    log.debug("fetchLoanerUsageScoreCardDtls  Query  " + sbQuery.toString());
	  
	    queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
	    gmDBManager.close();
	    return queryJSONRecord;
   }
  }
