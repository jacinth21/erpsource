package com.globus.sales.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmSalesManageBean extends GmBean {

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to

	public GmSalesManageBean() {
		// TODO Auto-generated constructor stub
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	public ArrayList getZone() throws AppError {
		ArrayList arList = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_sales.gm_get_zone", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		arList = GmCommonClass.parseNullArrayList(gmDBManager
				.returnArrayList((ResultSet) gmDBManager.getObject(1)));
		gmDBManager.close();
		return arList;
	}

	public ArrayList getRegionAD(String strZoneId, String strAction)
			throws AppError {
		ArrayList arList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_sales.gm_fetch_region", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strZoneId);
		gmDBManager.execute();
		arList = gmDBManager.returnArrayList((ResultSet) gmDBManager
				.getObject(2));
		gmDBManager.close();
		return arList;
	}

	public ArrayList getUserList(String strAction) throws AppError {
		ArrayList alReturn = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_sm_sales.gm_fetch_user_list", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager
				.returnArrayList((ResultSet) gmDBManager.getObject(1)));
		gmDBManager.close();
		return alReturn;
	}

	public void adreplacement(HashMap hmParam) throws AppError {

		String strADId = GmCommonClass.parseNull((String) hmParam
				.get("NEWADID"));
		String strOldADId = GmCommonClass.parseNull((String) hmParam
				.get("REGIONID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));
		log.debug("strUserId" + strUserId
				+ "===========strOldADId in ad replacement" + strOldADId
				+ "<<<<<<<<<<strADId in ad replacement" + strADId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_sm_sales.gm_sav_replace_ad", 3);
		gmDBManager.setString(1, strADId);
		gmDBManager.setString(2, strOldADId);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}

	public String createAD(HashMap hmParam) throws AppError {

		HashMap hmUserParam = new HashMap();
		ArrayList alCount = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strRegionName = GmCommonClass.parseNull((String) hmParam
				.get("REGIONNAME"));
		String strUserId = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));

		String strADID = null;
		int strpartyId = '0';

		gmDBManager.setPrepareString("gm_pkg_sm_sales.gm_create_open_ad", 16);
		gmDBManager.setString(1, strADID);
		gmDBManager.setString(2, strRegionName);
		gmDBManager.setString(3, strRegionName);
		gmDBManager.setString(4, "-TBH");
		gmDBManager.setString(5, "djames@globusmedical.com");
		gmDBManager.setString(6, "2005");
		gmDBManager.setString(7, "3");
		gmDBManager.setString(8, strUserId);
		gmDBManager.setString(9, "311");
		gmDBManager.setString(10, "");
		gmDBManager.setString(11, "");
		gmDBManager.setString(12, "");
		gmDBManager.setString(13, "");
		gmDBManager.setString(14, "");
		gmDBManager.registerOutParameter(15, java.sql.Types.INTEGER);
		gmDBManager.registerOutParameter(16, java.sql.Types.INTEGER);
		gmDBManager.execute();
		strADID = gmDBManager.getString(16); // GM-LN

		gmDBManager.commit();

		return strADID;
	}

}
