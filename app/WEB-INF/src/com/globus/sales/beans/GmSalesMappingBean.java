package com.globus.sales.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesMappingBean extends GmBean  {

  Logger log = GmLogger.getInstance(this.getClass().getName());
	
  // this will be removed all place changed with GmDataStoreVO constructor

  public GmSalesMappingBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesMappingBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public ArrayList fetchVPList(String strRegId) throws AppError {
	  log.debug("strRegId >>>>>>>>>>>>" + strRegId);
	    ArrayList alReturn = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager();
	    gmDBManager.setPrepareString("GM_PKG_SM_MAPPING.gm_fch_asd_users", 5);
	    gmDBManager.setString(1, strRegId);
	    gmDBManager.setString(2, "2005");
	    gmDBManager.setInt(3, 4);
	    gmDBManager.setString(4, "8001");
	    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
	    gmDBManager.close();
	    return alReturn;
  }

  public ArrayList fetchADPlusList(String strRegId) throws AppError {
	  	log.debug("strRegId >>>>>>>>>>>>" + strRegId);
	    ArrayList alReturn = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager();
	    gmDBManager.setPrepareString("GM_PKG_SM_MAPPING.gm_fch_adplus_users", 4);
	    gmDBManager.setString(1, strRegId);
	    gmDBManager.setString(2, "2005");
	    gmDBManager.setInt(3, 6);
	    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
	    gmDBManager.close();
	    return alReturn;
	  }
  public ArrayList fetchADList(String strRegId) throws AppError {
	  log.debug("strRegId >>>>>>>>>>>>" + strRegId);
	    ArrayList alReturn = new ArrayList();
	    GmDBManager db = new GmDBManager();
	    db.setPrepareString("GM_PKG_SM_MAPPING.gm_fch_asd_users", 5);
	    db.setString(1, strRegId);
	    db.setString(2, "2005");
	    db.setInt(3, 3);
	    db.setString(4, "8000");
	    db.registerOutParameter(5, OracleTypes.CURSOR);
	    db.execute();
	    alReturn = db.returnArrayList((ResultSet) db.getObject(5));
	    db.close();
	    return alReturn;
	  }

  public String saveZone(HashMap hmParam) throws AppError {
    String strzoneID = GmCommonClass.parseNull((String) hmParam.get("ZONEID"));
    String strzoneNM = GmCommonClass.parseNull((String) hmParam.get("ZONENM"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_SM_MAPPING.gm_sav_asd_region_mapping", 10);
    gmDBManager.registerOutParameter(1, java.sql.Types.VARCHAR);
    gmDBManager.setString(1, strzoneID);
    gmDBManager.setString(2, strzoneNM);
    gmDBManager.setString(3, "ZONE");
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, "");
    gmDBManager.setString(6, "");
    gmDBManager.setString(7, strUserId);
    gmDBManager.setString(8, "8001");
    gmDBManager.setString(9, "");
    gmDBManager.setString(10, "");
    gmDBManager.execute();
    strzoneID = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.commit();
    return strzoneID;
  }

  public ArrayList fetchZoneList() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_fch_zone", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, "");
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  public HashMap fetchZone(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    String strZoneID = GmCommonClass.parseNull((String) hmParam.get("ZONEID"));
    log.debug("strZoneID : " + strZoneID);
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_fch_zone", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strZoneID);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    log.debug("hmReturn : " + hmReturn);
    gmDBManager.close();
    return hmReturn;
  }


  public ArrayList fetchRegionList() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager db = new GmDBManager();
    db.setPrepareString("GM_PKG_SM_MAPPING.gm_fch_region", 2);
    db.registerOutParameter(2, OracleTypes.CURSOR);
    db.setString(1, "");
    db.execute();
    alReturn = db.returnArrayList((ResultSet) db.getObject(2));
    db.close();
    return alReturn;
  }

  public String saveRegion(HashMap hmParams) throws AppError {
    String strADType = GmCommonClass.parseNull((String) hmParams.get("ADTYPE"));
    strADType = strADType.equals("0") ? "" : strADType;
    String strVPID = GmCommonClass.parseZero((String) hmParams.get("VPID"));
    strVPID = strVPID.equals("0") ? "" : strVPID;
    String strADPlusID = GmCommonClass.parseZero((String) hmParams.get("ADPLUS"));
    strADPlusID = strADPlusID.equals("0") ? "" : strADPlusID;

    String strRegName = GmCommonClass.parseNull((String) hmParams.get("REGNAME"));
    String strRegionName = GmCommonClass.parseNull((String) hmParams.get("REGHIDDENNAME"));
    strRegionName = strRegionName.equals("") ? strRegName : strRegionName;
    
    log.debug("saveRegion hmParams>>>>>>>>>>>>" + hmParams);
    
    GmDBManager db = new GmDBManager();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_sav_asd_region_mapping", 14);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("REG_ID")));
    gmDBManager.setString(2, strRegionName);
    gmDBManager.setString(3, "REGN");
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("ZONETYPE")));
    gmDBManager.setString(5, strADType);
    gmDBManager.setString(6, strVPID);
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.setString(8, "8000");
    gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("DIVISIONTYPE")));
    gmDBManager.setString(10, GmCommonClass.parseNull((String) hmParams.get("COMPANYTYPE")));
    gmDBManager.setString(11, strADPlusID);
    gmDBManager.setString(12, GmCommonClass.parseNull((String) hmParams.get("NEWADFL")));
    gmDBManager.setString(13, GmCommonClass.parseNull((String) hmParams.get("NEWVPFL")));
    gmDBManager.setString(14, GmCommonClass.parseNull((String) hmParams.get("COUNTRYTYPE")));
    
    gmDBManager.execute();
    String strRegId = GmCommonClass.parseNull(gmDBManager.getString(1));
    
    gmDBManager.commit();
    return strRegId;
  }

  public HashMap fetchRegion(String strRegId) throws AppError {
    GmDBManager db = new GmDBManager();
    log.debug("fetchRegion strRegId>>>>>>>>>>>>" + strRegId);
    db.setPrepareString("GM_PKG_SM_MAPPING.gm_fch_region", 2);
    db.registerOutParameter(2, OracleTypes.CURSOR);
    db.setString(1, strRegId);
    db.execute();
    HashMap hmMap = db.returnHashMap((ResultSet) db.getObject(2));
    log.debug("hmMap : " + hmMap);
    db.close();
    return hmMap;
  }

  /*
   * public void saveSuperAD(HashMap hmParams) throws AppError{ StringTokenizer stInputRegions=new
   * StringTokenizer((String)hmParams.get("STRINPUTREGIONS"),"|");
   * while(stInputRegions.hasMoreTokens()){ GmDBManager gmDBManager = new GmDBManager(); String
   * strRegion=stInputRegions.nextToken();
   * gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_save_super_ad", 3); gmDBManager.setString(1,
   * GmCommonClass.parseNull(strRegion)); gmDBManager.setString(2,
   * GmCommonClass.parseNull((String)hmParams.get("ADTYPE"))); gmDBManager.setString(3,
   * GmCommonClass.parseNull((String)hmParams.get("USERID"))); gmDBManager.execute();
   * gmDBManager.commit(); gmDBManager.close(); } }
   */

  /**
   * This method return the Rep Email Id value using Rep id for sales rep.
   * 
   * @param strRepid
   * @return String
   * @throws AppError
   */
  public String getCSShipEmail(HashMap hmParam) throws AppError {
    String strRepEmailIdValue = "";
    String strShipid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strShiptype = GmCommonClass.parseNull((String) hmParam.get("USERTYPE"));

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("get_cs_ship_email", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strShiptype);// 4121- Type is for Sales Rep.
    gmDBManager.setString(3, strShipid);

    gmDBManager.execute();
    strRepEmailIdValue = gmDBManager.getString(1);
    gmDBManager.close();

    return strRepEmailIdValue;
  }

  /**
   * getAssocRepList - This method will fetch the associate spine specialist based on rep id or all
   * who are mapped to accounts.
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getAssocRepList(String strAccountId) throws AppError {
    ArrayList alReturn = new ArrayList();
    log.debug("the value inside strAccountId");
    // String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_fch_AssocRepList_info", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.setString(2, strAccountId);
    gmDBManager.execute();

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(1)));
    gmDBManager.close();
    return alReturn;

  } // End of getRepList

  /**
   * SaveAssocRepList - This method will fetch the associate spine specialist based on rep id or all
   * who are mapped to accounts. This procedure will be called to enable an Associate rep map,here
   * there are two input string INPUTSTRING - data will be saved into t704a_account_rep_mapping
   * VOIDINPUTSTRING - void flag will be updated based on the account id and rep id.
   * 
   * @exception AppError
   */
  public void SaveAssocRepList(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCTID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strvoidInputString = GmCommonClass.parseNull((String) hmParam.get("VOIDINPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_sav_assocrep_mapping", 4);

    gmDBManager.setString(1, strAccountId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strvoidInputString);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();

    gmDBManager.commit();


  }

  /**
   * fetchSalesHierarchyEmails - This method will fetch the sales hierarchy email based on account
   * id/rep id.
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchSalesHierarchyEmails(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_mapping.gm_fch_sales_hierarchy_emails", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("REPID")));
    gmDBManager.execute();
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(3)));
    log.debug("hmReturn : " + hmReturn);
    gmDBManager.close();
    return hmReturn;
  }

}
