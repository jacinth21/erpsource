package com.globus.sales.beans;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmReceiverEmailVO;
import com.globus.valueobject.sales.GmInvSetReqBorrowEmailVO;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import  com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.sales.GmSetDetlVO;
import com.globus.common.util.GmTemplateUtil;



public class GmInvSetReqBorrowRptBean extends GmBean {

	  Logger log = GmLogger.getInstance(this.getClass().getName());
	 
	  public GmInvSetReqBorrowRptBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	  
	  public GmInvSetReqBorrowRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		   super(gmDataStoreVO);
		   // TODO Auto-generated constructor stub
	   }

	  /**
	   * loadSetRequestBorrowDetails - Fetch the Borrow Set details when click load button from Inv Module-->Borrow Report from Globus App
	   * 
	   * @param hmParam
	   * @return String
	   * @exception AppError
	   **/	  
	public String loadSetRequestBorrowDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
	    String strSystem = GmCommonClass.parseNull((String) hmParam.get("SYSTEM"));
	    String strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" SELECT JSON_ARRAYAGG( JSON_OBJECT( 'setid' Value Setid,  ");
		sbQuery.append(" 'distid' Value Distid, 'setnm' Value Setnm , ");
		sbQuery.append(" 'distnm' Value Distnm,  ");
		sbQuery.append(" 'repphone' Value Repphone, 'emailid' Value Repemailid,'distemailid' Value distemailid, ");
		sbQuery.append(" 'adnm' Value Adname, 'regionnm' Value Regionname,'tagshareoptionid' Value tagshareoptionid ");
		sbQuery.append(" )ORDER BY tagshareoptionid,Distnm ASC ");
		sbQuery.append(" RETURNING CLOB) FROM ");
		sbQuery.append(" ( SELECT SETID,DISTID,SETNM ,DISTNM ,TO_CHAR(REPPHONE)REPPHONE,TO_CHAR(REPEMAILID)REPEMAILID,distemailid,Adname,Regionname ");
		sbQuery.append(" ,mv_sys_loc.TAGSHAREOPTIONID tagshareoptionid ");
		sbQuery.append(" FROM ");
		sbQuery.append(" MV5010g_tag_system_locator mv_sys_loc, ");
			sbQuery.append(" (");
			 if (!strCondition.equals("")) {
			      sbQuery.append(" SELECT DISTINCT AD_NAME Adname,REGION_NAME Regionname, D_Name, D_Id ");
			      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 WHERE ");
			      sbQuery.append(strCondition);
			    }else{
		    	  sbQuery.append(" SELECT DISTINCT AD_NAME Adname,REGION_NAME Regionname, D_Name, D_Id ");
				  sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL v700 ");
				  sbQuery.append("  WHERE v700.Divid         = 100823 ");
	              // PC-3905: to fix the sales report performance issue
	              // Currently ADDNL_COMP_IN_SALES having only BBA company id - this causing query slowness.
	              // So, we are removing the rules and hard code the values
				  //sbQuery.append(" (SELECT C906_RULE_ID  FROM T906_RULES  WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES' AND C906_Void_Fl IS NULL) ");
				  
				  sbQuery.append(" AND v700.REP_COMPID NOT IN ");
				  sbQuery.append(" (1001) ");
			    }
		sbQuery.append(" )V700");
		sbQuery.append("   WHERE mv_sys_loc.DISTID       = V700.D_ID");
		 if (!strSetId.equals("") && !strSetId.equals("0")) {
		        sbQuery.append(" AND mv_sys_loc.SETID IN (");
		        sbQuery.append(strSetId);
		        sbQuery.append(")");
		      }
		    if (!strSystem.equals("") && !strSystem.equals("0")) {
			      sbQuery.append(" AND mv_sys_loc.SYSTEMID IN (");
			      sbQuery.append(strSystem);
			      sbQuery.append(")");
			}
	      sbQuery.append(" AND REPEMAILID IS NOT NULL ");
	      sbQuery.append(" AND  mv_sys_loc.TAGSHAREOPTIONID <> 111142 "); //Exclude do not share Tag id's
	    sbQuery.append(" GROUP BY SETID, DISTID, SETNM ,DISTNM ,  TO_CHAR(Repphone), TO_CHAR(Repemailid), distemailid,Adname, Regionname, tagshareoptionid");
	    sbQuery.append(")");
		log.debug("sbQuery>>>>>" + sbQuery);
		String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

		return strReturn;
	}
	/**
	 *  sendRequestToBorrowEmail is used to send email to recipients. It contains links to resource in egnyte
	 * @author
	 * @param HashMap
	 * @return String
	 * @throws Exception 
	 */
	public String progressSetRequestBorrowDtls(HashMap hmParam) throws Exception {
		String strReturnMessage = "";
		String strErrorMsg = "";
		try{
		HashMap hmRepDetails = new HashMap();
		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("INV_REQUEST_BORROW_CONSUMER_CLASS"));
	    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_INV_SET_REQUEST_BORROW_QUEUE"));
		log.debug("sendRequestToBorrowEmail hmParam-->"+hmParam);

	    // This is JMS Code which will process the notification, jms/gminvsetreqborrowqueue is the JMS
	    // queue using for notification
	    GmQueueProducer qprod = new GmQueueProducer();
	    GmMessageTransferObject tf = new GmMessageTransferObject();
	    tf.setMessageObject(hmParam);
	    tf.setConsumerClass(strConsumerClass);
	    qprod.sendMessage(tf,strQueueName);
		}
		catch(Exception ex){
			  strErrorMsg = ex.getMessage();
		      log.error("progressSetRequestBorrowDtls exception " + GmCommonClass.getExceptionStackTrace(ex));
		}
		
		if(strErrorMsg != ""){
			strReturnMessage = strErrorMsg;
		}
	    return strReturnMessage;
	}
	
	
	/**
	   * fetchSenderRepDetails - Fetch the Seles rep details for requestor Information
	   * 
	   * @param hmParam
	   * @return String
	   * @exception AppError
	   **/	
	public HashMap fetchSenderRepDetails(String strRepId)	throws Exception{
		StringBuffer sbQuery = new StringBuffer();
		HashMap hmParam = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		sbQuery.append("SELECT T703.C703_Sales_Rep_Name Repname,T703.C703_Email_Id Repemail,T703.C703_Phone_Number Phonenumber,V700.Region_Name RegionName");
		sbQuery.append(" FROM T703_Sales_Rep T703 ,( SELECT DISTINCT V700.AD_ID,V700.AD_NAME,V700.REGION_ID,V700.Region_Name,V700.D_ID,V700.D_NAME FROM V700_TERRITORY_MAPPING_DETAIL V700");
		sbQuery.append(" WHERE ((V700.COMPID = DECODE(100803,100803, NULL,100803) OR DECODE(100803,100803,1,2) = 1)) AND V700.DIVID IN (100823) ");
		
        // PC-3905: to fix the sales report performance issue
        // Currently ADDNL_COMP_IN_SALES having only BBA company id - this causing query slowness.
        // So, we are removing the rules and hard code the values
		//sbQuery.append(" AND V700.REP_COMPID NOT IN(SELECT C906_RULE_ID FROM T906_RULES WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES' AND C906_VOID_FL IS NULL )) V700");
		
		sbQuery.append(" AND V700.REP_COMPID NOT IN(1001)) V700");
		sbQuery.append(" WHERE T703.C701_Distributor_Id = V700.D_Id AND T703.C703_Sales_Rep_Id = '"+strRepId+"' AND T703.C703_Void_Fl IS NULL");
		log.debug("Query ---> "+sbQuery);
		hmParam = gmDBManager. querySingleRecord(sbQuery.toString());  
		return hmParam;
		
		}

	/**
	   * sendRequestToBorrowEmails - Method used to form the Email tempalte and send request to borrow email
	   * 
	   * @param hmParam
	   * @return String
	   * @exception AppError
	   **/	
	public String sendRequestToBorrowEmails(HashMap hmParam) throws Exception {
		//Get all list from input param
		log.debug("Inside sendRequestToBorrowEmails ==" + hmParam);
		ArrayList alReceiverSetDtls = new ArrayList();
        ArrayList alSetdetails = new ArrayList();
        String strReceiverEmailId = "";
        String strDistEmailId = "";
        String strReceiverEmailBody = "";
        String strRequestorAddress = "";
        String strReturnMessage = "";
		String strErrorMsg = "";
		HashMap hmRepDetails = new HashMap();
		String strRepId = GmCommonClass.parseNull((String)hmParam.get("REQUESTORREPID"));
	    hmRepDetails = fetchSenderRepDetails(strRepId);
	    	    
	    hmParam.put("REPMAIL",GmCommonClass.parseNull((String)hmRepDetails.get("REPEMAIL")));
	    hmParam.put("REPNM",GmCommonClass.parseNull((String) hmRepDetails.get("REPNAME")));
	    hmParam.put("REPPHONE",GmCommonClass.parseNull((String) hmRepDetails.get("PHONENUMBER")));
	    hmParam.put("REPREGION",GmCommonClass.parseNull((String) hmRepDetails.get("REGIONNAME")));
			
			 GmReceiverEmailVO[] gmReceiverEmailVO =(GmReceiverEmailVO[]) hmParam.get("GMRECEIVEREMAILVO");
			 
			 for(int i=0;i<gmReceiverEmailVO.length;i++){
				 try { 
					 
				 strReceiverEmailId = gmReceiverEmailVO[i].getReceiveremail();
				 log.debug("strReceiverEmailId === " + strReceiverEmailId);
				 
				 strDistEmailId = gmReceiverEmailVO[i].getDistemailid();
				 log.debug("strDistEmailId === " + strDistEmailId);
				 
				 strReceiverEmailBody = gmReceiverEmailVO[i].getEmailbody();
				 log.debug("strReceiverEmailBody === " + strReceiverEmailBody);
				 
				 strRequestorAddress = gmReceiverEmailVO[i].getRequestorrepdefltaddress();
				 log.debug("strRequestorAddress === " + strRequestorAddress);
				 
				 hmParam.put("EMAILBODY",strReceiverEmailBody);
				 hmParam.put("REQUESTORREPDEFLTADDRESS",strRequestorAddress);
				 log.error("strReceiverEmailId === " + strReceiverEmailId);
				 GmSetDetlVO[] alSetDtls = (GmSetDetlVO[]) gmReceiverEmailVO[i].getArrSetDtl();
				 
	 	    	 List<HashMap<String, String>> alSetDetail = new ArrayList<>();
	 	    	 for(int j=0;j<alSetDtls.length;j++){
	 	    		HashMap<String, String> map = new HashMap<String, String>();
	 	    		map.put("SETID", alSetDtls[j].getSetid());
	 	    		map.put("SETNM", alSetDtls[j].getSetnm());
	 	    		if(j == 0){
	 	    			hmParam.put("SETNAME",alSetDtls[j].getSetnm());
	 	    		}
	 	    		alSetDetail.add(0, map);
	 	    	 }
				  log.debug("alSetDetail === " + alSetDetail);

				 
				    GmTemplateUtil templateUtil = new GmTemplateUtil();
					templateUtil.setTemplateSubDir("sales/templates");
					templateUtil.setTemplateName("GmInvSetReqBorrowEmail.vm");
					templateUtil.setDataList("alSetDetails", alSetDetail);
				    templateUtil.setDataMap("hmParam",  hmParam);
				    String strEmailContent = templateUtil.generateOutput();
				    log.debug(" : strOut >>> " + strEmailContent);
				    strDistEmailId = GmCommonClass.parseNull(strDistEmailId);
				    if (!strReceiverEmailId.equals("")) {
				        GmEmailProperties emailProps = new GmEmailProperties();
				        emailProps.setSubject(GmCommonClass.getEmailProperty("GmRequestToBorrowSet."+ GmEmailProperties.SUBJECT) + 
				        		 GmCommonClass.parseNull((String)hmParam.get("SETNAME")) + " " + 
				        		 GmCommonClass.getEmailProperty("GmRequestToBorrowSet.SubjectDtl") +  
				        		 GmCommonClass.parseNull((String)hmParam.get("REPNM")));
				        emailProps.setSender(GmCommonClass.getEmailProperty("GmRequestToBorrowSet."+ GmEmailProperties.FROM));
				        emailProps.setRecipients(GmCommonClass.parseNull((String)strReceiverEmailId));
				        emailProps.setCc(GmCommonClass.parseNull((String)hmParam.get("REPMAIL"))+","+strDistEmailId);
				        emailProps.setMimeType(GmCommonClass.getEmailProperty("GmRequestToBorrowSet."+ GmEmailProperties.MIME_TYPE));
				        emailProps.setMessage(strEmailContent);
				        GmCommonClass.sendMail(emailProps);
				      }
					
					}catch(Exception ex){
					      log.error("sendRequestToBorrowEmails exception " + GmCommonClass.getExceptionStackTrace(ex));
					      log.error("Exception in sendRequestToBorrowEmails body: " + ex.getMessage() + " and TO EMail----"
					              + strReceiverEmailId);
					      strErrorMsg = strErrorMsg +strReceiverEmailId+',';
						  HashMap hmExcpVal = new HashMap();
						  hmExcpVal.put("EXCEPTION", GmCommonClass.getExceptionStackTrace(ex, "<br>"));
						  hmExcpVal.put("EXCEPTIONEMAILID", strReceiverEmailId);
						  hmExcpVal.put("REQUESTOREMAILID", GmCommonClass.parseNull((String)hmParam.get("REPMAIL")));
						  hmExcpVal.put("SETNAME", GmCommonClass.parseNull((String)hmParam.get("SETNAME")));
						  
						  log.debug("hmExcepVal >>>> " + hmExcpVal);
					  	  generateExcpReport(hmExcpVal);
					}
		    }
		
			if(strErrorMsg != ""){
				strReturnMessage = strErrorMsg;
			}
		    return strReturnMessage;
 }
	
	
	/**
	   * generateExcpReport - If we have any exception while sending email then we need to capture the exception 
	   * and mail to the requestor.
	   * 
	   * @param hmParam
	   * @return String
	   * @exception AppError
	   **/	
	public void generateExcpReport(HashMap hmExcp) throws Exception {
		HashMap hmMailValues = new HashMap();
	    StringBuffer strExcpEmailData = new StringBuffer();
	    String strExcepEmailID = (String) hmExcp.get("EXCEPTIONEMAILID");
	    String strRequestorEMail = (String) hmExcp.get("REQUESTOREMAILID");
	    String strSetName = (String) hmExcp.get("SETNAME");
	    String strErrorMsg = "";
	    String strReturnMessage = "";

	    log.error("hmExcp in generateExcpReport sendRequestToBorrowEmails  "   + hmExcp);
	    strExcpEmailData.append("<tr> <td height=15 ></td> </tr>");
	    strExcpEmailData.append("<tr> <td height=15 ><B>Requesting Sets to Borrow Email not delivered </B></td> </tr><BR>");
	    strExcpEmailData.append("<tr> <td height=15 >Email not delivered to <B>" + strExcepEmailID + "</B>,Please try again or contact system Administrator. </td></tr>");
	    strExcpEmailData.append("<tr> <td height=15 ><br><b>Exception Message :</b>" + (String) hmExcp.get("EXCEPTION") + "</td></tr>");
	    strExcpEmailData.append("<tr> <td height=15 ></td> </tr>");

	    try{
	    	
	    	if (!strRequestorEMail.equals("")) {
	    		log.error("sendRequestToBorrowEmails exception " + strRequestorEMail);
		        GmEmailProperties emailProps = new GmEmailProperties();
		        emailProps.setSubject(GmCommonClass.getEmailProperty("GmRequestToBorrowSet."+ GmEmailProperties.SUBJECT) + strSetName + " " +
			    		GmCommonClass.getEmailProperty("GmRequestToBorrowSet.SubjectDtl") +" "+
			    	    GmCommonClass.getEmailProperty("GmRequestToBorrowSetExceptionMail.Subject"));
		        emailProps.setSender(GmCommonClass.getEmailProperty("GmRequestToBorrowSet."+ GmEmailProperties.FROM));
		        emailProps.setRecipients(GmCommonClass.parseNull((String)strRequestorEMail));
		        emailProps.setCc(GmCommonClass.getEmailProperty("GmRequestToBorrowSetExceptionMail.Cc"));
		        emailProps.setMimeType(GmCommonClass.getEmailProperty("GmRequestToBorrowSetExceptionMail.MimeType"));
		        emailProps.setMessage(strExcpEmailData.toString());
		        GmCommonClass.sendMail(emailProps);
		      }
	    }
	    catch(Exception ex){
		      log.error("sendRequestToBorrowEmails exception " + GmCommonClass.getExceptionStackTrace(ex));
		      log.error("Exception in sendRequestToBorrowEmails body: " + ex.getMessage() + " and TO EMail----"
		              + strRequestorEMail);
	    }
	}
}
