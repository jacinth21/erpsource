package com.globus.sales.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.validator.routines.DateValidator;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesPerformanceReportBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
  String strMonthRR;
  String strSalesFromMonth;
  String strSalesFromYear;
  String strSalesToMonth;
  String strSalesToYear;
  String strMonthRRColumn;
  int intStartXMonthRRDiff; // If the input is 18, then this value is 18 - 3 = 15

  public GmSalesPerformanceReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmSalesPerformanceReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /*
   * Currently the sales total is fetched from C501_TOTAL_COST. If we are to give drill down etc,
   * then use T502_ITEM_ORDER in all the methods referenced in reportSalesRunRate
   * 
   * sbDetailsQuery.append(" , SUM (C501_TOTAL_COST) sales  ");
   * sbDetailsQuery.append(" FROM t703_sales_rep t703, t501_order t501  ");
   * sbDetailsQuery.append(getAccessFilterClause());
   * sbDetailsQuery.append(" WHERE t502.c501_order_id = t501.c501_order_id ");
   * 
   * 
   * getFilterCondition will have
   * sbFilterCondition.append(" AND t502.c205_part_number_id IN (SELECT c205_part_number_id ");
   * sbFilterCondition.append(" FROM t208_set_details t208, t207_set_master t207 ");
   * sbFilterCondition
   * .append(" WHERE t208.c207_set_id = t207.c207_set_id AND t207.c901_set_grp_type = 1600) ");
   */

  public HashMap reportSalesRunRate(HashMap hmParam) throws AppError {
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    HashMap hmSalesRunRate = new HashMap(); // Final value report details
    HashMap hmLinkDetails = new HashMap();

    // result data stores
    ArrayList alSalesPersonInformation = new ArrayList();
    ArrayList alFirstTwelveMonthSales = new ArrayList();
    ArrayList alLastMonthSalesRR = new ArrayList();
    ArrayList alTwelveMonthSalesRR = new ArrayList();
    ArrayList alXMonthSalesRR = new ArrayList();
    ArrayList alLastThreeMonthAnnualized = new ArrayList();

    strSalesFromMonth = GmCommonClass.parseNull((String) hmParam.get("SALESFROMMONTH"));
    strSalesFromYear = GmCommonClass.parseNull((String) hmParam.get("SALESFROMYEAR"));
    strSalesToMonth = GmCommonClass.parseNull((String) hmParam.get("SALESTOMONTH"));
    strSalesToYear = GmCommonClass.parseNull((String) hmParam.get("SALESTOYEAR"));

    if (!strSalesFromMonth.equals("") && !strSalesFromYear.equals("")
        && !strSalesFromMonth.equals("0") && !strSalesFromYear.equals("0")
        && !strSalesToMonth.equals("") && !strSalesToYear.equals("")
        && !strSalesToMonth.equals("0") && !strSalesToYear.equals("0")) {
      hmSalesRunRate = fetchMonthRangeSales(hmParam);
    } else {
      hmSalesRunRate = fetchFirstTwelveMonthSales(hmParam);
    }
    strMonthRR = GmCommonClass.parseZero((String) hmParam.get("MONTHRR"));


    alSalesPersonInformation = fetchSalesPersonInformation(hmParam);
    log.debug(" values alSalesPersonInformation " + alSalesPersonInformation);
    hmLinkDetails = getLinkInfo("SALESPERSONINFO");
    hmSalesRunRate =
        gmCrossReport.linkCrossTab(hmSalesRunRate, alSalesPersonInformation, hmLinkDetails);

    /*
     * 
     * alLastMonthSalesRR = fetchLastMonthSalesRR(hmParam);
     * log.debug(" values alSalesPersonInformation " +alLastMonthSalesRR ); hmLinkDetails =
     * getLinkInfo("LASTMONTHRR"); hmSalesRunRate = gmCrossReport.linkCrossTab(hmSalesRunRate,
     * alLastMonthSalesRR, hmLinkDetails);
     */

    hmParam.put("MONTHRRPARAM", "12");
    alTwelveMonthSalesRR = fetchSalesRR(hmParam);
    log.debug(" values alSalesPersonInformation " + alTwelveMonthSalesRR);
    hmLinkDetails = getLinkInfo("SALESRR");
    hmSalesRunRate =
        gmCrossReport.linkCrossTab(hmSalesRunRate, alTwelveMonthSalesRR, hmLinkDetails);
    log.debug(" After 12 " + hmSalesRunRate);

    hmParam.put("MONTHRRPARAM", strMonthRR);
    alTwelveMonthSalesRR = fetchSalesRR(hmParam);
    log.debug(" values alSalesPersonInformation " + alTwelveMonthSalesRR);
    hmLinkDetails = getLinkInfo("SALESXRR");
    hmSalesRunRate =
        gmCrossReport.linkCrossTab(hmSalesRunRate, alTwelveMonthSalesRR, hmLinkDetails);
    log.debug(" After " + strMonthRR + " " + hmSalesRunRate);

    alLastThreeMonthAnnualized = fetchLastThreeMonthsAnnualized(hmParam);
    log.debug(" values alSalesPersonInformation " + alLastThreeMonthAnnualized);
    hmLinkDetails = getLinkInfo("LASTTHREEMONA");
    hmSalesRunRate =
        gmCrossReport.linkCrossTab(hmSalesRunRate, alLastThreeMonthAnnualized, hmLinkDetails);

    hmSalesRunRate = modifyFooterColumn(hmSalesRunRate);
    hmSalesRunRate.put("XMONTHRRCOLUMN", strMonthRRColumn);
    log.debug("Exit Report Summary " + hmSalesRunRate);

    return hmSalesRunRate;
  }

  public ArrayList fetchSalesPersonInformation(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alSalesPersonInformation = new ArrayList();
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");

    StringBuffer sbQuery = new StringBuffer();

    hmParam.put("METHOD", "fetchSalesPersonInformation");
    sbQuery.append(" SELECT t703.c703_sales_rep_id ID, " + strRepNmColumn
        + " NAME, GET_TERRITORY_NAME(T703.C702_TERRITORY_ID) territory ");
    sbQuery
        .append(" , DECODE (t703.c703_rep_category, 4020, 'D.R.', NVL (get_code_name_alt (t703.c901_designation), 'Distributor Rep')) desg ");
    sbQuery.append(" , TO_CHAR(c703_start_date,get_rule_value('DATEFMT','DATEFORMAT')) hire_date ");
    sbQuery.append(" FROM t703_sales_rep t703 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE v700.D_ID = t703.C701_DISTRIBUTOR_ID (+) ");
    sbQuery.append(getFilterConditions(hmParam));
    sbQuery.append(" GROUP BY t703.c703_sales_rep_id, " + strRepNmColumn
        + ",  t703.c901_designation ");
    sbQuery.append(" , c703_start_date, T703.C702_TERRITORY_ID,  t703.c703_rep_category ");
    sbQuery.append(" ORDER BY NAME ");

    log.info(" Query for alSalesPersonInformation - " + sbQuery.toString());
    alSalesPersonInformation = gmDBManager.queryMultipleRecords(sbQuery.toString());

    hmParam.put("METHOD", ""); // resetting the METHOD param
    return alSalesPersonInformation;
  }



  public HashMap fetchFirstTwelveMonthSales(HashMap hmParam) throws AppError {
    HashMap hmFirstTwelveMonthSales = new HashMap();
    strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");

    StringBuffer sbHeaderQuery = new StringBuffer();
    sbHeaderQuery.append(" SELECT 'M'||ROWNUM MON ");
    sbHeaderQuery.append(" FROM t703_sales_rep t703 ");
    sbHeaderQuery.append(" WHERE ROWNUM <=12 ORDER BY ROWNUM ");

    StringBuffer sbDetailsQuery = new StringBuffer();
    sbDetailsQuery.append(" select t703.c703_sales_rep_id ID, " + strRepNmColumn + " NAME ");
    sbDetailsQuery
        .append(" , 'M' || (ROUND((to_date(to_char(C501_ORDER_DATE,'YYYYMM'),'YYYYMM') - to_date(to_char(C703_START_DATE,'YYYYMM'),'YYYYMM') )/30) + 1) MON ");
    sbDetailsQuery.append(" , " + strItemPriceColumn + " sales  ");
    sbDetailsQuery.append(" FROM t703_sales_rep t703, t501_order t501 , T502_ITEM_ORDER T502  ");
    sbDetailsQuery.append(getAccessFilterClause());
    sbDetailsQuery.append(" WHERE t703.c703_sales_rep_id = t501.c703_sales_rep_id ");
    sbDetailsQuery.append(" AND v700.D_ID = t703.C701_DISTRIBUTOR_ID (+) ");
    sbDetailsQuery.append(" AND c501_order_date > c703_start_date ");
    sbDetailsQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbDetailsQuery.append("  AND T502.C502_VOID_FL IS NULL ");
    sbDetailsQuery.append(" AND C501_ORDER_DATE  <=  LAST_DAY(ADD_MONTHS (c703_start_date, 11)) ");
    sbDetailsQuery.append(getFilterConditions(hmParam));
    sbDetailsQuery.append(" GROUP BY t703.c703_sales_rep_id, " + strRepNmColumn + " ");
    sbDetailsQuery.append(" , TO_CHAR (c501_order_date, 'YYYYMM') ");
    sbDetailsQuery.append(" , TO_CHAR (c703_start_date, 'YYYYMM') ");
    sbDetailsQuery.append(" ORDER BY NAME,MON ");

    log.info(" Query for First 12 Mon Sales - " + sbDetailsQuery.toString());
    hmFirstTwelveMonthSales =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailsQuery.toString());
    return hmFirstTwelveMonthSales;
  }

  public HashMap fetchMonthRangeSales(HashMap hmParam) throws AppError {
    HashMap hmFirstTwelveMonthSales = new HashMap();
    strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");

    StringBuffer sbHeaderQuery = new StringBuffer();
    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append(" FROM t703_sales_rep t703, t501_order t501  ");
    sbHeaderQuery.append(getAccessFilterClause());
    sbHeaderQuery.append(" WHERE t703.c703_sales_rep_id = t501.c703_sales_rep_id ");
    sbHeaderQuery.append(" AND v700.D_ID = t703.C701_DISTRIBUTOR_ID (+) ");
    sbHeaderQuery.append(" AND t703.c703_sales_rep_id = t501.c703_sales_rep_id ");
    sbHeaderQuery.append(getFilterConditions(hmParam));
    sbHeaderQuery.append(getSalesMonthFilter());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    StringBuffer sbDetailsQuery = new StringBuffer();
    sbDetailsQuery.append(" select t703.c703_sales_rep_id ID, " + strRepNmColumn + " NAME ");
    sbDetailsQuery.append(" , TO_CHAR(C501_ORDER_DATE,'Mon YY') MON ");
    sbDetailsQuery.append(" , " + strItemPriceColumn + " sales  ");
    sbDetailsQuery.append(" FROM t703_sales_rep t703, t501_order t501, T502_ITEM_ORDER T502   ");
    sbDetailsQuery.append(getAccessFilterClause());
    sbDetailsQuery.append(" WHERE t703.c703_sales_rep_id = t501.c703_sales_rep_id ");
    sbDetailsQuery.append(" AND v700.D_ID = t703.C701_DISTRIBUTOR_ID (+) ");
    sbDetailsQuery.append(" AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbDetailsQuery.append("  AND T502.C502_VOID_FL IS NULL ");
    sbDetailsQuery.append(getFilterConditions(hmParam));
    sbDetailsQuery.append(getSalesMonthFilter());
    sbDetailsQuery.append(" GROUP BY t703.c703_sales_rep_id, " + strRepNmColumn + " ");
    sbDetailsQuery.append(" , TO_CHAR (c501_order_date, 'Mon YY') ");
    sbDetailsQuery.append(" ORDER BY NAME,MON ");

    log.info(" Query for sales date range - " + sbDetailsQuery.toString());
    hmFirstTwelveMonthSales =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailsQuery.toString());
    return hmFirstTwelveMonthSales;
  }

  public StringBuffer getSalesMonthFilter() {
    StringBuffer sbDetailsQuery = new StringBuffer();

    if (!strSalesFromMonth.equals("") && !strSalesFromYear.equals("")
        && !strSalesFromMonth.equals("0") && !strSalesFromYear.equals("0")) {
      sbDetailsQuery.append(" AND t501.c501_order_date >= ");
      sbDetailsQuery.append(" TO_DATE('");
      sbDetailsQuery.append(strSalesFromMonth);
      sbDetailsQuery.append("/");
      sbDetailsQuery.append(strSalesFromYear);
      sbDetailsQuery.append("','MM/YYYY') ");
    }

    if (!strSalesToMonth.equals("") && !strSalesToYear.equals("") && !strSalesToMonth.equals("0")
        && !strSalesToYear.equals("0")) {
      sbDetailsQuery.append(" AND t501.c501_order_date <= ");
      sbDetailsQuery.append(" LAST_DAY(TO_DATE('");
      sbDetailsQuery.append(strSalesToMonth);
      sbDetailsQuery.append("/");
      sbDetailsQuery.append(strSalesToYear);
      sbDetailsQuery.append("','MM/YYYY')) ");
    }
    return sbDetailsQuery;
  }

  public ArrayList fetchLastMonthSalesRR(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alLastMonthSalesRR = new ArrayList();

    strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" select t703.c703_sales_rep_id ID ");
    sbQuery.append(" , SUM(C501_TOTAL_COST) * 12 LastMon ");
    sbQuery.append(" FROM t703_sales_rep t703, t501_order t501  ");
    sbQuery
        .append(" , (SELECT t501.C703_SALES_REP_ID, DECODE(trunc(MAX (t501.c501_order_date),'MONTH'),TRUNC (CURRENT_DATE, 'MONTH'),ADD_MONTHS (TRUNC (CURRENT_DATE, 'MONTH'), -1) ");
    sbQuery.append(",trunc(MAX (t501.c501_order_date),'MONTH')) lastsale");
    sbQuery.append(" FROM t501_order t501 ");
    sbQuery.append(" GROUP BY t501.C703_SALES_REP_ID) replastsales ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t703.c703_sales_rep_id = t501.c703_sales_rep_id ");
    sbQuery.append(" AND v700.D_ID = t703.C701_DISTRIBUTOR_ID (+) ");
    sbQuery.append(" AND t501.C703_SALES_REP_ID = replastsales.C703_SALES_REP_ID ");
    sbQuery
        .append(" AND t501.c501_order_date >= TO_DATE (TO_CHAR (lastsale, 'YYYYMM'), 'YYYYMM') ");
    sbQuery
        .append(" AND t501.c501_order_date <= LAST_DAY(TO_DATE (TO_CHAR (lastsale, 'YYYYMM'), 'YYYYMM')) ");
    sbQuery.append(getFilterConditions(hmParam));
    sbQuery.append(" GROUP BY t703.c703_sales_rep_id, c703_sales_rep_name ");
    sbQuery.append(" ORDER BY c703_sales_rep_name ");

    log.info(" Query for Last Month Sales RR - " + sbQuery.toString());
    alLastMonthSalesRR = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alLastMonthSalesRR;
  }

  public ArrayList fetchSalesRR(HashMap hmParam) throws AppError {
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alSalesRR = new ArrayList();

    strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
    int intMonthRR =
        Integer.parseInt(GmCommonClass.parseZero((String) hmParam.get("MONTHRRPARAM")));
    int intMonthRRDiff = 3;
    intStartXMonthRRDiff = intMonthRR - (intMonthRRDiff - 1); // -1 because it is for 16,17,18 of
                                                              // intMonthRR = 18. But for oracle, we
                                                              // give 15 to 18 (it'll take 17)
    strMonthRRColumn = "MM " + intStartXMonthRRDiff + "-" + intMonthRR + " RR";
    log.debug(" strMonthRRColumn " + strMonthRRColumn);

    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" select t703.c703_sales_rep_id ID ");
    sbQuery.append(" , " + strItemPriceColumn + " * ");
    sbQuery.append(12 / intMonthRRDiff);
    sbQuery.append(" SALESRR ");
    sbQuery.append(" FROM t703_sales_rep t703, t501_order t501 , T502_ITEM_ORDER T502  ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t703.c703_sales_rep_id = t501.c703_sales_rep_id ");
    sbQuery
        .append(" AND C501_ORDER_DATE   >= ADD_MONTHS (TO_DATE (TO_CHAR (C703_START_DATE, 'YYYYMM'), 'YYYYMM'),");
    sbQuery.append(intMonthRR - intMonthRRDiff);
    sbQuery
        .append(") AND C501_ORDER_DATE   < ADD_MONTHS (TO_DATE (TO_CHAR (C703_START_DATE, 'YYYYMM'), 'YYYYMM'),");
    sbQuery.append(intMonthRR);
    sbQuery.append(") AND v700.D_ID = t703.C701_DISTRIBUTOR_ID (+) ");
    sbQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbQuery.append("  AND T502.C502_VOID_FL IS NULL ");
    sbQuery.append(getFilterConditions(hmParam));
    sbQuery.append(" GROUP BY t703.c703_sales_rep_id, t703.c703_sales_rep_name ");
    sbQuery.append(" ORDER BY c703_sales_rep_name ");

    log.info(" Query for Sales RR for " + intMonthRR + " is " + sbQuery.toString());
    alSalesRR = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alSalesRR;
  }

  public ArrayList fetchLastThreeMonthsAnnualized(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alLastMonthSalesRR = new ArrayList();

    strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" select t703.c703_sales_rep_id ID ");
    sbQuery.append(" , " + strItemPriceColumn + " * 12/3 LAST3MONANNUALIZED "); // Average of last 3
                                                                                // month
    // sales run rate
    sbQuery.append(" FROM t703_sales_rep t703, t501_order t501 , T502_ITEM_ORDER T502  ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t703.c703_sales_rep_id = t501.c703_sales_rep_id ");
    sbQuery.append(" AND t501.c501_order_date >= ADD_MONTHS (TRUNC (CURRENT_DATE, 'MONTH'), -3) ");
    sbQuery
        .append(" AND t501.c501_order_date <= ADD_MONTHS (TRUNC (CURRENT_DATE, 'MONTH'), 0) -1 ");
    sbQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbQuery.append("   AND T502.C502_VOID_FL IS NULL ");
    sbQuery.append(" AND v700.D_ID = t703.C701_DISTRIBUTOR_ID (+) ");
    sbQuery.append(getFilterConditions(hmParam));
    sbQuery.append(" GROUP BY t703.c703_sales_rep_id , t703.c703_sales_rep_name ");
    sbQuery.append(" ORDER BY c703_sales_rep_name ");

    log.info(" Query for Last 3 Months Annualized is " + sbQuery.toString());
    alLastMonthSalesRR = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alLastMonthSalesRR;
  }

  public HashMap modifyFooterColumn(HashMap hmReturn) {
    GmFooterCalcuationBean calcuationBean = null;
    ArrayList alDataStore = new ArrayList();
    ArrayList alOperands = new ArrayList();

    calcuationBean = new GmFooterCalcuationBean(getGmDataStoreVO());
    calcuationBean.setColumnNameToModify("12MONTHS");
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean(getGmDataStoreVO());
    calcuationBean.setColumnNameToModify(" MM 10-12 RR");
    calcuationBean.setOperator("MONTH");
    alOperands.add("1");
    alOperands.add("10");
    calcuationBean.setOperands(alOperands);
    alDataStore.add(calcuationBean);

    calcuationBean = new GmFooterCalcuationBean(getGmDataStoreVO());
    alOperands = new ArrayList();
    calcuationBean.setColumnNameToModify(strMonthRRColumn);
    calcuationBean.setOperator("MONTH");
    alOperands.add("1");
    alOperands.add(String.valueOf(intStartXMonthRRDiff));
    calcuationBean.setOperands(alOperands);
    alDataStore.add(calcuationBean);

    /*
     * calcuationBean = new GmFooterCalcuationBean();
     * calcuationBean.setColumnNameToModify("Last Month<BR> Sales RR");
     * calcuationBean.setOperator("RELATIVE"); calcuationBean.setOperand("1");
     * alDataStore.add(calcuationBean);
     */

    calcuationBean = new GmFooterCalcuationBean(getGmDataStoreVO());
    alOperands = new ArrayList();
    calcuationBean.setColumnNameToModify("Last 3 Mon RR");
    calcuationBean.setOperator("RELATIVE");
    alOperands.add(0, "-3");
    alOperands.add(1, "1");
    calcuationBean.setOperands(alOperands);
    alDataStore.add(calcuationBean);

    return reCalculateFooter(hmReturn, alDataStore);
  }

  public HashMap reCalculateFooter(HashMap hmParam, ArrayList alDataStore) {
    String strColumnName = "";
    HashMap hmFooter;
    ArrayList alDetails;
    hmFooter = hmParam.get("Footer") == null ? new HashMap() : (HashMap) hmParam.get("Footer");
    alDetails =
        hmParam.get("Details") == null ? new ArrayList() : (ArrayList) hmParam.get("Details");
    ArrayList alHeader =
        hmParam.get("Header") == null ? new ArrayList() : (ArrayList) hmParam.get("Header");
    double detailsSize = alDetails.size();
    String strColumnValue = "";
    HashMap hmTemp = new HashMap();
    GmFooterCalcuationBean calcuationBean = null;
    Iterator iDataStore = alDataStore.iterator();
    String strOperator = "";
    String strOperand = "";
    String strMonthCntOperand = "";
    String strRelativeMonthOperand = "";
    log.debug(" Header " + alHeader);
    log.debug(" Footer is " + hmFooter);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmResult = new HashMap();
    String strAppliDateFmt = "";
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT	GET_RULE_VALUE('DATEFMT','DATEFORMAT') APPLDATEFMT FROM DUAL ");
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    strAppliDateFmt = (String) hmResult.get("APPLDATEFMT");
    gmDBManager.close();

    while (iDataStore.hasNext()) {
      calcuationBean = (GmFooterCalcuationBean) iDataStore.next();
      strColumnName = calcuationBean.getColumnNameToModify();
      strColumnValue = (String) hmFooter.get(strColumnName);
      strOperator = calcuationBean.getOperator();

      if (strColumnName.equals("12MONTHS")) {
        int endIndex = 0;

        for (int k = 0; k < alHeader.size(); k++) {
          if (alHeader.get(k).equals("Total")) {
            endIndex = k;
            break;
          }
        }
        // log.debug(" End INdex " + endIndex);
        for (int j = 4; j < endIndex; j++) {
          int cnt = 0;
          double value = 0;
          strColumnName = (String) alHeader.get(j);
          for (int i = 0; i < detailsSize; i++) {
            hmTemp = (HashMap) alDetails.get(i);
            // log.debug(" row of info "+hmTemp);
            String strHireDate = GmCommonClass.parseNull((String) hmTemp.get("Date of Hire"));
            String strName = GmCommonClass.parseNull((String) hmTemp.get("Name"));
            value = Double.parseDouble(GmCommonClass.parseZero((String) hmTemp.get(strColumnName)));

            // we are negating by 5 because for M1 ie first month, need to add 0 months (ie -1 for
            // Java months) and on.
            // we exclude anyone who doesnt have a hire date
            if (value != 0
                || (value == 0 && !strHireDate.equals("") && isLessThanCurrentDate(strHireDate,
                    j - 3, 1, strAppliDateFmt))) {
              cnt++;
            }
          }
          log.debug(" strColumnName " + strColumnName + " value "
              + (String) hmFooter.get(strColumnName) + " count " + cnt);
          strColumnValue = GmCommonClass.parseZero((String) hmFooter.get(strColumnName));
          double newAvg = Double.parseDouble(strColumnValue) / cnt;
          hmFooter.put(strColumnName, String.valueOf(newAvg));
        }
        hmParam.put("Footer", hmFooter);
      }

      // If the operation is MONTH, then get the corresponding column and check for Month
      else if (strOperator.equals("MONTH") || strOperator.equals("RELATIVE")) {
        strRelativeMonthOperand = (String) calcuationBean.getOperands().get(0);
        strMonthCntOperand = (String) calcuationBean.getOperands().get(1);
        double value = 0;
        int cnt = 0;
        int intTodayCount = Integer.parseInt(strRelativeMonthOperand);
        int montcnt = Integer.parseInt(strMonthCntOperand);

        for (int i = 0; i < detailsSize; i++) {
          hmTemp = (HashMap) alDetails.get(i);
          // log.debug(" row of info "+hmTemp);
          String strHireDate = GmCommonClass.parseNull((String) hmTemp.get("Date of Hire"));
          value = Double.parseDouble(GmCommonClass.parseZero((String) hmTemp.get(strColumnName)));

          if (value != 0
              || (value == 0 && isLessThanCurrentDate(strHireDate, montcnt, intTodayCount,
                  strAppliDateFmt))) {
            cnt++;
          }
        }
        log.debug(" strColumnName " + strColumnName + " value "
            + (String) hmFooter.get(strColumnName) + " count " + cnt);
        strColumnValue = GmCommonClass.parseZero((String) hmFooter.get(strColumnName));
        double newAvg = Double.parseDouble(strColumnValue) / cnt;
        hmFooter.put(strColumnName, String.valueOf(newAvg));
        hmParam.put("Footer", hmFooter);
      }

      // If the operation is RELATIVE, then get the corresponding column and check for Month
      // We might need the End date of the person to make sure that this works .... so dont do
      // anything
      else if (strOperator.equals("RELATIVE")) {
        if (detailsSize > 0 && strColumnValue != null && !strColumnValue.equals("")) {
          strColumnValue = String.valueOf(Double.parseDouble(strColumnValue) / detailsSize);
        }
        hmFooter.put(strColumnName, strColumnValue);
      }
      hmParam.put("Footer", hmFooter);
    }
    hmFooter.put("Name", "Average");
    hmParam.put("Footer", hmFooter);
    return hmParam;
  }

  public boolean isLessThanCurrentDate(String strHireDate, int Mcount, int intTodayCount,
      String strAppliDateFmt) {
    String strINitialHireDate = strHireDate;

    if (strHireDate.equals("")) {
      return true;
    }

    DateValidator dateVal = DateValidator.getInstance();

    DateFormat dateFormat = new SimpleDateFormat(strAppliDateFmt);
    Date date = new Date();
    String strToday = dateFormat.format(date);
    strToday = GmCalenderOperations.addMonthsFromDate(strToday, intTodayCount, true);
    Date today = dateVal.validate(strToday);
    // log.debug(" today is " + strToday);

    strHireDate = GmCalenderOperations.addMonthsFromDate(strHireDate, Mcount, true);
    // log.debug(" strINitialHireDate is " + strINitialHireDate + " Hire date after " + Mcount +
    // " is " + strHireDate);
    Date dtFirstOccurence = dateVal.validate(strHireDate);

    // Zero if the months are equal, -1 if first parameter's month is less than the seconds and +1
    // if the first parameter's month is greater than.
    int dateCompare = dateVal.compareMonths(dtFirstOccurence, today, null);
    // log.debug("dateCompare is " +dateCompare);

    if (dateCompare < 0) {
      // log.debug(" true strINitialHireDate is " + strINitialHireDate + " Hire date after " +
      // Mcount + " is " + strHireDate + " todayz cnt " +intTodayCount);
      return true;
    } else {
      // log.debug(" false strINitialHireDate is " + strINitialHireDate + " Hire date after " +
      // Mcount + " is " + strHireDate + " todayz cnt " +intTodayCount);
      return false;
    }
  }

  public String getFilterConditions(HashMap hmParam) {

    String strShowInactiveReps =
        GmCommonClass.getCheckBoxValue((String) hmParam.get("SHOWINACTIVEREPSFLAG"));
    String strSelectAllFieldSalesType =
        GmCommonClass.getCheckBoxValue((String) hmParam.get("SELECTALLFIELDSALESTYPE"));
    String strZone = GmCommonClass.getStringWithQuotes((String) hmParam.get("ZONE"));
    String strArea = GmCommonClass.getStringWithQuotes((String) hmParam.get("AREA"));
    String strFieldSalesType =
        GmCommonClass.getStringWithQuotes((String) hmParam.get("FIELDSALESTYPE"));
    String strRepDesignation =
        GmCommonClass.getStringWithQuotes((String) hmParam.get("REPDESIGNATION"));
    String strAreaSize = GmCommonClass.parseZero((String) hmParam.get("AREASIZE"));
    String strAreaArray[] = strArea.split(",");
    log.debug(" Area Array " + strAreaArray.length + " Srea Size " + strAreaSize);

    String strHireFromMonth = GmCommonClass.parseNull((String) hmParam.get("HIREFROMMONTH"));
    String strHireFromYear = GmCommonClass.parseNull((String) hmParam.get("HIREFROMYEAR"));
    String strHireToMonth = GmCommonClass.parseNull((String) hmParam.get("HIRETOMONTH"));
    String strHireToYear = GmCommonClass.parseNull((String) hmParam.get("HIRETOYEAR"));

    String strMethod = GmCommonClass.parseNull((String) hmParam.get("METHOD"));
    String strRuleDesgExcl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("70119", "EPIC_RR_REPORT"));

    StringBuffer sbFilterCondition = new StringBuffer();

    if (!strMethod.equals("fetchSalesPersonInformation")) {
      sbFilterCondition.append(" AND T501.C501_VOID_FL IS NULL ");
      sbFilterCondition.append(" AND T501.C501_DELETE_FL IS NULL ");
      sbFilterCondition.append(" AND NVL(T501.C901_ORDER_TYPE,-9999) <> '2533' ");
      sbFilterCondition.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbFilterCondition.append(" SELECT t906.c906_rule_value ");
      sbFilterCondition.append(" FROM t906_rules t906 ");
      sbFilterCondition.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbFilterCondition.append(" AND c906_rule_id = 'EXCLUDE') ");
      sbFilterCondition.append(" AND V700.VP_ID <> '-9999' ");
      /*
       * sbFilterCondition.append(" AND t502.c205_part_number_id IN (SELECT c205_part_number_id ");
       * sbFilterCondition.append(" FROM t208_set_details t208, t207_set_master t207 ");
       * sbFilterCondition
       * .append(" WHERE t208.c207_set_id = t207.c207_set_id AND t207.c901_set_grp_type = 1600) ");
       */
    }
    sbFilterCondition.append(" AND t703.c703_void_fl IS NULL ");
    sbFilterCondition.append(" AND t703.C703_SALES_FL <> 'N' ");

    if (!strShowInactiveReps.equals("Y")) {
      sbFilterCondition.append(" AND T703.C703_ACTIVE_FL = 'Y' ");
    }

    if (!strZone.equals("")) {
      sbFilterCondition.append(" AND V700.VP_ID IN ('");
      sbFilterCondition.append(strZone);
      sbFilterCondition.append("')");
    }

    if (!strArea.equals("") && strAreaArray.length != Integer.parseInt(strAreaSize)) {
      sbFilterCondition.append(" AND V700.region_id IN ('");
      sbFilterCondition.append(strArea);
      sbFilterCondition.append("')");
    }

    if (!strFieldSalesType.equals("")) {
      sbFilterCondition.append(" AND T703.c703_rep_category IN ('");
      sbFilterCondition.append(strFieldSalesType);
      sbFilterCondition.append("')");
    }

    if (!strRepDesignation.equals("")) {
      sbFilterCondition.append(" AND T703.c901_designation IN ('");
      sbFilterCondition.append(strRepDesignation);
      sbFilterCondition.append("')");
    }
    if (!strRuleDesgExcl.equals("")) {
      sbFilterCondition.append(" AND NVL(T703.c901_designation,-999) NOT IN (");
      sbFilterCondition
          .append("select c906_rule_value from t906_rules where c906_rule_grp_id = 'EPIC_RR_REPORT' AND C906_VOID_FL IS NULL ");
      sbFilterCondition.append(")");
    }

    if (!strHireFromMonth.equals("") && !strHireFromYear.equals("")
        && !strHireFromMonth.equals("0") && !strHireFromYear.equals("0")) {
      sbFilterCondition.append(" AND T703.c703_start_date >= ");
      sbFilterCondition.append(" TO_DATE('");
      sbFilterCondition.append(strHireFromMonth);
      sbFilterCondition.append("/");
      sbFilterCondition.append(strHireFromYear);
      sbFilterCondition.append("','MM/YYYY') ");
    }

    if (!strHireToMonth.equals("") && !strHireToYear.equals("") && !strHireToMonth.equals("0")
        && !strHireToYear.equals("0")) {
      sbFilterCondition.append(" AND T703.c703_start_date <= ");
      sbFilterCondition.append(" TO_DATE('");
      sbFilterCondition.append(strHireToMonth);
      sbFilterCondition.append("/");
      sbFilterCondition.append(strHireToYear);
      sbFilterCondition.append("','MM/YYYY') ");
    }

    return sbFilterCondition.toString();
  }

  /**
   * getLinkInfo - This Method is used to to get the link object information
   * 
   * @return HashMap
   * @exception AppError
   */
  private HashMap getLinkInfo(String strType) {
    HashMap hmLinkDetails = new HashMap();
    HashMap hmapValue = new HashMap();
    ArrayList alLinkList = new ArrayList();

    // First parameter holds the link object name
    hmLinkDetails.put("LINKID", "ID");
    hmLinkDetails.put("POSITION", "AFTER");

    if (strType.equals("SALESPERSONINFO")) {

      hmLinkDetails.put("POSITION", "BEFORE");

      // Distributor Information Level II
      hmapValue.put("KEY", "TERRITORY");
      hmapValue.put("VALUE", "Territory");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "DESG");
      hmapValue.put("VALUE", "Type");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "HIRE_DATE");
      hmapValue.put("VALUE", "Date of Hire");
      alLinkList.add(hmapValue);

      /*
       * hmapValue = new HashMap(); hmapValue.put("KEY", "SALES"); hmapValue.put("VALUE",
       * "First 12<BR>Month Sales"); alLinkList.add(hmapValue);
       */

    } else if (strType.equals("LASTMONTHRR")) {

      // Average No. of Cases per Month
      hmapValue.put("KEY", "LASTMON");
      hmapValue.put("VALUE", "Last Month<BR> Sales RR");
      alLinkList.add(hmapValue);

    } else if (strType.equals("SALESRR")) {

      // Metrics for No. of Cases per set per Month
      hmapValue = new HashMap();
      hmapValue.put("KEY", "SALESRR");
      hmapValue.put("VALUE", " MM 10-12 RR");
      alLinkList.add(hmapValue);

    } else if (strType.equals("SALESXRR")) {

      // Metrics for No. of Cases per set per Month
      hmapValue = new HashMap();
      hmapValue.put("KEY", "SALESRR");
      hmapValue.put("VALUE", strMonthRRColumn);
      alLinkList.add(hmapValue);

    } else if (strType.equals("LASTTHREEMONA")) {

      hmapValue = new HashMap();
      hmapValue.put("KEY", "LAST3MONANNUALIZED");
      hmapValue.put("VALUE", "Last 3 Mon RR");
      alLinkList.add(hmapValue);

    }
    hmLinkDetails.put("LINKVALUE", alLinkList);

    return hmLinkDetails;
  }
}
