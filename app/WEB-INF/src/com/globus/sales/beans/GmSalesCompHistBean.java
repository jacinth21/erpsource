package com.globus.sales.beans;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesCompHistBean extends GmSalesFilterConditionBean {


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesCompHistBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  public GmSalesCompHistBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

  public ArrayList fetchTerritorySalesHist(HashMap hmFilter) throws AppError {
    ArrayList alReturn = new ArrayList();
    StringBuffer sbSalesQuery = new StringBuffer();
    String strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));
    GmSalesCompBean gmSalesCompBean = new GmSalesCompBean(getGmDataStoreVO());
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    // For getting the price based on Item price filter
    String strCurrType = GmCommonClass.parseNull((String) hmFilter.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
   
    //PMT-33147 Qouta performance by historical
    String strCondition = GmCommonClass.parseNull((String) hmFilter.get("Condition"));
    
   

    try {
      if (strType.equals("AD")) {
        sbSalesQuery.append(" SELECT t901.c901_code_id ID ");
        sbSalesQuery.append(" ," + strItemPriceColumn + " TOTAL ");
        sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
        // sbSalesQuery.append(" ,t901.c901_code_nm || '(' || t101.c101_user_f_name ||' '|| t101.c101_user_l_name || ')' NAME ");
        sbSalesQuery.append(" ,REG_NAME NAME ");
      }

      else if (strType.equals("VP")) {
        sbSalesQuery.append(" SELECT T501.c501_VP_ID ID ");
        sbSalesQuery.append(" ," + strItemPriceColumn + " TOTAL ");
        sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
        sbSalesQuery.append(" ,t101.c101_user_f_name ||' '|| t101.c101_user_l_name NAME ");
      }
    //PMT-33147 Quota Performance Historical
      else if (strType.equals("DIST")) {
    	  sbSalesQuery.append(" SELECT ID, SUM(TOTAL) TOTAL ,DT,NAME FROM ( ");
    	  sbSalesQuery.append(" SELECT T501.C501_DISTRIBUTOR_ID ID ");
    	  sbSalesQuery.append(" , NVL(DECODE("+strCurrType+",105461,T502.C502_ITEM_PRICE,T502.C502_CORP_ITEM_PRICE) * T502.C502_ITEM_QTY,0) TOTAL ");
    	  sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
    	  sbSalesQuery.append(" ,DECODE('"+ getComplangid()+"', '103097',NVL(T701.C701_DISTRIBUTOR_NAME_EN, T701.C701_DISTRIBUTOR_NAME), T701.C701_DISTRIBUTOR_NAME) NAME ");
      }  
     else if (strType.equals("TERR")) {
    	  sbSalesQuery.append(" SELECT ID, SUM(TOTAL) TOTAL ,DT,NAME FROM ( ");
    	  sbSalesQuery.append(" SELECT v700s.ter_id ID ");
    	    	  sbSalesQuery.append(" , NVL(DECODE("+strCurrType+",105461,T502.C502_ITEM_PRICE,T502.C502_CORP_ITEM_PRICE) * T502.C502_ITEM_QTY,0) TOTAL ");
    	  sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
    	  sbSalesQuery.append(" , V700S.TER_NAME NAME,  NVL(V700S.REP_NAME,'^No Rep') SNAME ");
    	  }

      //PMT-33147 Quota Performance Historical

      sbSalesQuery.append(" FROM ");
      sbSalesQuery.append(" T501_ORDER T501 ");
      sbSalesQuery.append(" , T502_ITEM_ORDER T502 ");
      //PMT-33147 Quota Performance Historical
      if(strType.equals("DIST")){
    	  sbSalesQuery.append(" , T701_DISTRIBUTOR T701 ");
      }
      else if(strType.equals("TERR")){
    	      	  sbSalesQuery.append(" , (SELECT DISTINCT  TER_ID , TER_NAME, REP_ID ,REP_NAME ");
    	  sbSalesQuery.append("  FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
    	  sbSalesQuery.append("  WHERE "+strCondition+" )V700S ");  
    	  
    	  }else{
    	  sbSalesQuery.append(" , T101_user T101 ");
      }

    //PMT-33147 Quota Performance Historical
      
      if (strType.equals("AD")) {
        sbSalesQuery.append(" , t901_code_lookup t901 ");
        sbSalesQuery.append(" , t708_region_asd_mapping t708 ");
        // Below line of code is added to bring the sale captured for voided AD like Peter Buffo
        // under NorthWest.
        sbSalesQuery
            .append(" , ( select distinct v700.region_id, v700.region_name|| '(' || v700.AD_NAME|| ')'  REG_NAME from v700_territory_mapping_detail v700 ) region ");
      }
      sbSalesQuery.append(" WHERE ");
      if (strType.equals("VP")) {
        sbSalesQuery.append(" t501.c501_vp_id = T101.c101_user_id ");
        // sbSalesQuery.append(" AND t501.c501_vp_id = t708.c101_user_id " );
        // sbSalesQuery.append(" t501.c703_sales_rep_id = v700.rep_id " );
      } else if (strType.equals("AD")) {
        sbSalesQuery.append(" t501.c501_ad_id = T101.c101_user_id ");
        sbSalesQuery.append(" AND t501.c501_ad_id = t708.c101_user_id ");
        sbSalesQuery.append(" AND  t708.c901_region_id = t901.c901_code_id ");
        sbSalesQuery.append(" AND t101.c101_user_id = t708.c101_user_id  ");
        sbSalesQuery.append(" AND region.region_id = t708.c901_region_id  ");
        sbSalesQuery.append(" AND T708.C901_User_Role_Type = 8000 ");
        sbSalesQuery.append(" AND T708.c708_inactive_fl IS NULL ");
        sbSalesQuery.append(" AND T708.C708_DELETE_FL IS NULL ");// Added delete flag check for PMT-51013 
      }
      //PMT-33147 Quota Performance Historical
       else if (strType.equals("DIST")) {
      	
      	sbSalesQuery.append(" t501.C501_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID ");

      } else if (strType.equals("TERR")) {
      		
      	    sbSalesQuery.append(" T501.C703_SALES_REP_ID = V700S.REP_ID  ");
      } 
      //PMT-33147 Quota Performance Historical

      sbSalesQuery.append(" AND  T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbSalesQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbSalesQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbSalesQuery.append(" AND ");
      sbSalesQuery.append(" T502.C205_PART_NUMBER_ID IN ");
      sbSalesQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbSalesQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbSalesQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
      sbSalesQuery.append(getSalesFilterClause());
      sbSalesQuery.append(GenerateFilterHist(hmFilter));

      sbSalesQuery.append(GenerateAdditionalFilterHist(hmFilter));
      // To get additional drilldown information

      if (strType.equals("AD")) {
        sbSalesQuery
            .append(" GROUP BY t901.c901_code_id, REG_NAME, TO_CHAR (C501_ORDER_DATE, 'YYQMM')");
        sbSalesQuery.append(" ORDER BY NAME,  DT ");
      }

      else if (strType.equals("VP")) {
        sbSalesQuery
            .append(" GROUP BY T501.c501_VP_ID, t101.c101_user_f_name ||' '|| t101.c101_user_l_name, TO_CHAR (C501_ORDER_DATE, 'YYQMM')");
        sbSalesQuery.append(" ORDER BY NAME,  DT ");
      }//PMT-33147 Quota Performance Historical
      else if (strType.equals("DIST")) {
          sbSalesQuery
              .append(") GROUP BY ID,DT,NAME ");
          sbSalesQuery.append(" ORDER BY NAME,  DT ");
      }
      else if (strType.equals("TERR") ) {
          sbSalesQuery
              .append(") GROUP BY ID,DT,NAME,SNAME ");
          sbSalesQuery.append(" ORDER BY SNAME, NAME, DT ");
      }//33147 Quota Performance Historical


      log.debug("sbSalesQuery = " + sbSalesQuery.toString());

      alReturn = gmSalesCompBean.parseGrowthData(sbSalesQuery.toString(), "T", hmFilter);
    
    } catch (Exception e) {
      throw new AppError(e);

    }
    return alReturn;

  }


	public String GenerateFilterHist(HashMap hmFilter) 
	{

		String strToMonth		= (String) hmFilter.get("ToMonth"); 
		String strToYear		= (String) hmFilter.get("ToYear") ;	
		String strCondition		= (String)hmFilter.get("HistCondition");	
		String strFilter		= (String)hmFilter.get("Filter");

		StringBuffer sbDetailFilter = new StringBuffer();

		// Date filter condition
		if (strToMonth != null && strToYear != null)
		{
			sbDetailFilter.append(" AND (T501.C501_ORDER_DATE BETWEEN " );
			sbDetailFilter.append(" ADD_MONTHS(TO_DATE(");
			sbDetailFilter.append(" '01/01/" + strToYear + "', 'MM/DD/YYYY') " );
			sbDetailFilter.append(", -12 )" );
			sbDetailFilter.append(" AND ADD_MONTHS( LAST_DAY(TO_DATE('");
			sbDetailFilter.append(strToMonth + "/01/"  + strToYear );
			sbDetailFilter.append("','mm/dd/yyyy')), -12)  " );
			sbDetailFilter.append(" OR  T501.C501_ORDER_DATE BETWEEN TO_DATE(");
			sbDetailFilter.append(" '01/01/" + strToYear + "', 'MM/DD/YYYY') " );
			sbDetailFilter.append(" AND LAST_DAY(TO_DATE('");
			sbDetailFilter.append(strToMonth + "/01/"  + strToYear );
			sbDetailFilter.append("','mm/dd/yyyy')) )" );

		}
		else
		{
			sbDetailFilter.append(" AND (T501.C501_ORDER_DATE BETWEEN TO_DATE('");
			sbDetailFilter.append("01/01'||TO_CHAR(CURRENT_DATE - 365,'YYYY'), 'MM/DD/YYYY') " );
			sbDetailFilter.append(" AND TRUNC(ADD_MONTHS(TRUNC( LAST_DAY(CURRENT_DATE)) , -12))");
			sbDetailFilter.append(" OR  T501.C501_ORDER_DATE BETWEEN TO_DATE(");
			sbDetailFilter.append(" '01/01'||TO_CHAR(CURRENT_DATE,'YYYY'), 'MM/DD/YYYY') " );
			sbDetailFilter.append(" AND TRUNC(CURRENT_DATE)");
			sbDetailFilter.append(" )" );
		}	

		//Filter Condition to fetch record based on access code
		if (!strCondition.toString().equals(""))
		{
			sbDetailFilter.append(" AND ");
			sbDetailFilter.append(strCondition);
		}
		return sbDetailFilter.toString();
	}

  private String GenerateAdditionalFilterHist(HashMap hmFilter) {

    String strSetNumber = (String) hmFilter.get("SetNumber");
    // String strDistributorID = (String)hmFilter.get("DistributorID");
    // String strAccountID = (String)hmFilter.get("AccountID");
    String strRepID = (String) hmFilter.get("RepID");
    String strPartNumber = (String) hmFilter.get("PartNumber");
    String strADID = (String) hmFilter.get("ADID");
    // String strTerritoryID = (String)hmFilter.get("TerritoryID");
    String strVPID = (String) hmFilter.get("VPID");
    // String strRegnID = (String)hmFilter.get("RegnID");

    StringBuffer sbDetailFilter = new StringBuffer();

    // Area Director Filter
    if (strADID != null && !strADID.equals("null")) {
      sbDetailFilter.append(" AND T501.C501_AD_ID = ");
      sbDetailFilter.append(strADID);
    }
    /*
     * // Distributor Filter if (strDistributorID != null && !strDistributorID.equals("null")) {
     * sbDetailFilter.append(" AND V700.D_ID = "); sbDetailFilter.append(strDistributorID); }
     */
    // Filter by Rep
    if (strRepID != null && !strRepID.equals("null")) {
      sbDetailFilter.append(" AND  T501.C703_SALES_REP_ID = ");
      sbDetailFilter.append(strRepID);
    }
    // Filter by VP
    if (strVPID != null && !strVPID.equals("null")) {
      sbDetailFilter.append(" AND  T501.C501_VP_ID = ");
      sbDetailFilter.append(strVPID);
    }
    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
      sbDetailFilter.append(" AND ");
      sbDetailFilter.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailFilter.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailFilter.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailFilter.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");
      sbDetailFilter.append(" AND T207.C207_SET_ID IN ('");
      sbDetailFilter.append(strSetNumber);
      sbDetailFilter.append("') )");

    }

    return sbDetailFilter.toString();
  }

}
