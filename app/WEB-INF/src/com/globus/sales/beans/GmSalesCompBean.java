/*
 * Module: GmSalesCompBean.java Author: DJames Project: Globus Medical App Date-Written: Apr 2006
 * Security: Unclassified Description: This bean contains methods that are used in the Sales Growth
 * Reports
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.sales.beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesCompBean extends GmSalesFilterConditionBean {
  public GmSalesCompBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesCompBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * loadTerrCompSales - This Method is used to fetch data for Territories based on the filter
   * condition
   * 
   * @param HashMap hmFilter Contains filter condition
   * @return ArrayList
   * @exception AppError
   **/

  public ArrayList fetchSalesMergeData(HashMap hmFilter) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    StringBuffer sbMergeQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ResultSet rs = null;
    Statement stmt = null;
    String strCondition = "";
    String strType = "";
    String strFilter = "";
    String strGenFilter = "";
    String strCmpLangId = GmCommonClass.parseNull((String) hmFilter.get("COMP_LANG_ID"));

    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");

    strCondition = (String) hmFilter.get("Condition");
    strFilter = (String) hmFilter.get("Filter");

    strType = (String) hmFilter.get("TYPE");
    strGenFilter = GenerateAdditionalFilter(hmFilter);


    if (strType.equals("DIST")) {
      sbMergeQuery.append("SELECT DISTINCT D_ID ID, " + strDistNmColumn + " NAME, '");
      sbMergeQuery.append(strType);
      sbMergeQuery.append("' RTYPE ");
    }

    if (strType.equals("AD")) {
      sbMergeQuery.append("SELECT DISTINCT REGION_ID ID, AD_NAME  NAME, REGION_NAME RGNAME, '");
      sbMergeQuery.append(strType);
      sbMergeQuery.append("' RTYPE ");
    }

    if (strType.equals("VP")) {
      sbMergeQuery.append("SELECT DISTINCT VP_ID ID, VP_NAME NAME, GP_NAME GNAME, '");
      sbMergeQuery.append(strType);
      sbMergeQuery.append("' RTYPE ");
    }

    if (strType.equals("Summary") || strType.equals("TERR")) {
      sbMergeQuery.append("SELECT AD_ID, AD_NAME, REGION_ID RGID, ");
      sbMergeQuery
          .append(" NVL(V700.REGION_NAME,'*NO Ao') || ' - (' || V700.AD_NAME|| ')' RGNAME ");
      sbMergeQuery.append(" ,D_ID DID, " + strDistNmColumn + " DNAME, ");
      sbMergeQuery.append(" TER_ID ID, TER_NAME NAME, REP_ID, " + strRepNmColumn + " RNAME, '");
      sbMergeQuery.append(strType);
      sbMergeQuery.append("' RTYPE ");
    }

    sbMergeQuery.append("FROM V700_TERRITORY_MAPPING_DETAIL V700");

    // *** This mess is here because we need to handle AND clause in the strFilter and
    // strCondition
    if (!strCondition.toString().equals("") || !strFilter.toString().equals("")
        || !strGenFilter.toString().equals("")) {
      sbMergeQuery.append(" WHERE ");
    }
    if (!strCondition.equals("")) {
      sbMergeQuery.append(strCondition);
      if (!strFilter.equals("")) {
        sbMergeQuery.append(" AND ");
      }
    }

    if (!strFilter.equals("")) {
      strFilter = strFilter.substring(4);
      sbMergeQuery.append(strFilter);
    }
    if (!strGenFilter.equals("")) {
      if (!strFilter.equals("") || !strCondition.equals("")) {
        sbMergeQuery.append(strGenFilter);
      } else {
        sbMergeQuery.append(strGenFilter.substring(4));
      }
    }
    // ***
    if (strType.equals("TERR")) {
      sbMergeQuery.append(" GROUP BY AD_ID, AD_NAME, REGION_ID, REGION_NAME, ");
      sbMergeQuery.append(" D_ID, " + strDistNmColumn + ", TER_ID, TER_NAME, REP_ID, "
          + strRepNmColumn + " ");
      sbMergeQuery.append("  ORDER BY RNAME, TER_NAME ");
    }

    if (strType.equals("Summary")) {
      sbMergeQuery.append(" GROUP BY AD_ID, AD_NAME, REGION_ID, REGION_NAME, ");
      sbMergeQuery.append(" D_ID, " + strDistNmColumn + ", TER_ID, TER_NAME, REP_ID, "
          + strRepNmColumn + " ");
      sbMergeQuery.append("  ORDER BY REGION_NAME, DNAME, RNAME, TER_NAME ");
    }

    if (strType.equals("DIST") || strType.equals("AD") || strType.equals("VP")) {
      sbMergeQuery.append(" ORDER BY NAME ");
    }

    log.debug("fetchSalesMergeData= " + sbMergeQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbMergeQuery.toString());

    return alReturn;
  }

  public ArrayList fetchTerritorySalesQuota(HashMap hmFilter) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    GmSalesCompBean gmSales = new GmSalesCompBean(getGmDataStoreVO());
    DBConnectionWrapper dbCon = null;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    dbCon = new DBConnectionWrapper();
    HashMap hmResult = new HashMap();
    HashMap hmapFinalValue = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alReturnFromThis = new ArrayList();

    String strBreakId = "";
    String strId = "";
    String strName = "";
    String strTotal = "";
    String strDate = "";
    String strYear = "";
    String strQtr = "";
    String strMon = "";
    String strRepId = "";
    String strQuota = "";
    String strQuotaDate = "";
    // String strRepName = "";
    String strRegnId = "";
    String strRegnNm = "";
    String strDistId = "";
    String strDistNm = "";

    String strCondition = "";
    String strFilter = "";
    String strType = "";
    String strCompFilter = "";

    // Below variable used for quota formula calculation
    Double dbTemp = new Double(0);
    double dbCurrYTD = 0;
    double dbCurrMTD = 0;
    double dbCurrQTD = 0;
    double dbCurrYTDQuota = 0;
    double dbCurrMTDQuota = 0;
    double dbCurrQTDQuota = 0;
    double dbMTDToQuota = 0;
    double dbQTDToQuota = 0;
    double dbYTDToQuota = 0;
    int roundvalue = 1;
    String strCurrType = "";

    initializeParameters(hmFilter);
    strCurrType = (String) hmFilter.get("CURRTYPE");
    String strCmpLangId = GmCommonClass.parseNull((String) hmFilter.get("COMP_LANG_ID"));

    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");

    DecimalFormat df = new DecimalFormat("#0.00");
    try {
      strCondition = GmCommonClass.parseNull((String) hmFilter.get("Condition")); // To fetch access
                                                                                  // condition
      strFilter = GmCommonClass.parseNull((String) hmFilter.get("Filter"));
      strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));
      strCompFilter = GmCommonClass.parseNull((String) hmFilter.get("COMPID"));

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbDetailQuery = new StringBuffer();
      if (strType.equals("TERR") || strType.equals("Summary")) {
        sbDetailQuery.append("SELECT V700.TER_ID ID, TO_CHAR(T709.C709_START_DATE,'YYQMM') QDT");
        sbDetailQuery.append(", SUM (NVL(DECODE(" + strCurrType
            + ",105460,T709.C709_QUOTA_BREAKUP_AMT_BASE,T709.C709_QUOTA_BREAKUP_AMT),0)) QUOTA");
        // sbDetailQuery.append(", SUM (DECODE(v700.def_comp,"+strCompFilter+",T709.C709_QUOTA_BREAKUP_AMT,0)) QUOTA");
        sbDetailQuery.append(", V700.TER_NAME NAME, V700.REP_NAME RNAME ");
      }

      if (strType.equals("DIST")) {
        sbDetailQuery.append("SELECT V700.D_ID ID, TO_CHAR(T709.C709_START_DATE,'YYQMM') QDT");
        sbDetailQuery.append(", SUM (NVL(DECODE(" + strCurrType
            + ",105460,T709.C709_QUOTA_BREAKUP_AMT_BASE,T709.C709_QUOTA_BREAKUP_AMT),0)) QUOTA");
        // sbDetailQuery.append(", SUM (DECODE(v700.def_comp,"+strCompFilter+",T709.C709_QUOTA_BREAKUP_AMT,0)) QUOTA");
        sbDetailQuery.append(", V700.D_NAME NAME ");
      }

      if (strType.equals("AD")) {
        sbDetailQuery.append("SELECT V700.REGION_ID ID, TO_CHAR(T709.C709_START_DATE,'YYQMM') QDT");
        sbDetailQuery.append(", SUM (NVL(DECODE(" + strCurrType
            + ",105460,T709.C709_QUOTA_BREAKUP_AMT_BASE,T709.C709_QUOTA_BREAKUP_AMT),0)) QUOTA");
        // sbDetailQuery.append(", SUM (DECODE(v700.def_comp,"+strCompFilter+",T709.C709_QUOTA_BREAKUP_AMT,0)) QUOTA");
        sbDetailQuery.append(", V700.REGION_NAME NAME ");
      }

      if (strType.equals("VP")) {
        sbDetailQuery.append("SELECT V700.VP_ID ID, TO_CHAR(T709.C709_START_DATE,'YYQMM') QDT");
        sbDetailQuery.append(", SUM (NVL(DECODE(" + strCurrType
            + ",105460,T709.C709_QUOTA_BREAKUP_AMT_BASE,T709.C709_QUOTA_BREAKUP_AMT),0)) QUOTA");
        // sbDetailQuery.append(", SUM (DECODE(v700.def_comp,"+strCompFilter+",T709.C709_QUOTA_BREAKUP_AMT,0)) QUOTA");
        sbDetailQuery.append(", V700.VP_NAME NAME ");
      }

      sbDetailQuery.append(" FROM ");
      sbDetailQuery
          .append("(SELECT DISTINCT V700.REP_ID, V700.TER_ID, V700.TER_NAME, V700.REGION_ID, V700.REGION_NAME || '(' || V700.AD_NAME || ')' REGION_NAME ");
      sbDetailQuery.append(",NVL(" + strRepNmColumn
          + ", '*NO REP NAME') REP_NAME, NVL (v700.AD_ID, 0) AD_ID ");
      sbDetailQuery.append(",V700.D_ID, " + strDistNmColumn
          + " D_NAME, V700.AD_NAME, V700.VP_ID, V700.VP_NAME, comp.def_comp ");
      sbDetailQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 ");

      // The below code is added to make the alge quota to 0 for Globus rep and vice versa.
      sbDetailQuery
          .append(" ,(SELECT t710.c901_company_id def_comp,t703.c703_sales_rep_id rep_id ");
      sbDetailQuery
          .append(" FROM t710_sales_hierarchy t710, t701_distributor t701, t703_sales_rep t703 ");
      sbDetailQuery.append(" WHERE t710.c901_area_id = t701.c701_region ");
      sbDetailQuery.append(" AND t701.c701_distributor_id = t703.c701_distributor_id ");
      sbDetailQuery.append(" AND t701.c701_void_fl is null ");
      sbDetailQuery.append(" AND t703.c703_void_fl IS NULL ");
      sbDetailQuery.append(" AND t710.c710_active_fl = 'Y') comp ");

      if (!strCondition.toString().equals("") || !strFilter.toString().equals("")) {
        sbDetailQuery.append(" WHERE ");
      }
      // sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));
      log.debug("GenerateAdditionalFilter" + GenerateAdditionalFilter(hmFilter));
      if (!strCondition.equals("")) {
        sbDetailQuery.append(strCondition);
        if (!strFilter.equals("")) {
          sbDetailQuery.append(" AND ");
        }
      }

      if (!strFilter.equals("")) {
        if (strFilter.startsWith("AND")) {
          strFilter = strFilter.substring(3);
          log.debug("strFilter = " + strFilter);
        }
        sbDetailQuery.append(strFilter);
      }
      sbDetailQuery.append(" AND v700.rep_id = comp.rep_id ");

      sbDetailQuery.append(") V700, T709_QUOTA_BREAKUP T709");
      sbDetailQuery.append(" WHERE v700.rep_id = t709.c703_sales_rep_id");
      sbDetailQuery.append("  AND C901_TYPE = 20750  ");
      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));
      // sbDetailQuery.append(" AND v700.rgid = 4019 AND v700.d_id = 376 ");

      sbDetailQuery.append(GenerateQuotaFilter(hmFilter));

      if (strType.equals("TERR")) {
        sbDetailQuery
            .append(" GROUP BY V700.TER_ID , V700.TER_NAME , V700.REP_NAME, TO_CHAR (T709.C709_START_DATE, 'YYQMM')");
        sbDetailQuery.append(" ORDER BY  V700.REP_NAME");
      }

      if (strType.equals("Summary")) {
        sbDetailQuery
            .append(" GROUP BY V700.TER_ID , V700.TER_NAME , V700.REP_NAME, TO_CHAR (T709.C709_START_DATE, 'YYQMM')");
        sbDetailQuery.append(" ORDER BY  V700.TER_NAME");
      }

      if (strType.equals("DIST")) {
        // sbDetailQuery.append(" AND D_ID = 8 ");
        sbDetailQuery
            .append(" GROUP BY V700.D_ID , V700.D_NAME , TO_CHAR (T709.C709_START_DATE, 'YYQMM')");
        sbDetailQuery.append(" ORDER BY V700.D_NAME ");
      }

      if (strType.equals("AD")) {
        // sbDetailQuery.append(" AND D_ID = 8 ");
        sbDetailQuery
            .append(" GROUP BY V700.REGION_ID , V700.REGION_NAME , TO_CHAR (T709.C709_START_DATE, 'YYQMM')");
        sbDetailQuery.append(" ORDER BY V700.REGION_NAME ");
      }

      if (strType.equals("VP")) {
        // sbDetailQuery.append(" AND D_ID = 8 ");
        sbDetailQuery
            .append(" GROUP BY V700.VP_ID , V700.VP_NAME , TO_CHAR (T709.C709_START_DATE, 'YYQMM')");
        sbDetailQuery.append(" ORDER BY V700.VP_NAME ");
      }

      // ********************************************************
      // Date filter condition
      // used to fecth the quater, month and year information
      // ********************************************************
      StringBuffer sbQuery = new StringBuffer();
      // Below Query to fetch date value for the selected value
      String strToMonth = (String) hmFilter.get("ToMonth");
      String strToYear = (String) hmFilter.get("ToYear");

      if (strToMonth == null && strToYear == null) {
        sbQuery.append(" SELECT to_char(CURRENT_DATE-365,'YY') YEAR ");
        sbQuery.append(" , to_char(CURRENT_DATE,'MM') MONTH ,");
        sbQuery.append(" to_char(CURRENT_DATE,'Q') QUARTER FROM DUAL ");
      } else {
        sbQuery.append(" SELECT to_char(DT-365,'YY') YEAR ");
        sbQuery.append(" , to_char(DT,'MM') MONTH ,");
        sbQuery.append(" to_char(DT,'Q') QUARTER FROM  ");
        sbQuery.append(" ( SELECT TO_DATE('");
        sbQuery.append(strToMonth + "/" + strToYear);
        sbQuery.append("','MM/YYYY') DT FROM DUAL ) ");
      }
      log.debug("All Sales Quota 1 = " + sbQuery.toString());
      log.debug("All Sales Quota 2= " + sbDetailQuery.toString());

      HashMap hmDate = gmDBManager.querySingleRecord(sbQuery.toString());
      String strQuart = (String) hmDate.get("QUARTER"); // "2";
      String strMonth = (String) hmDate.get("MONTH"); // "06";
      String strTemp = "";

      roundvalue = Integer.parseInt(((String) hmFilter.get("Divider")));

      conn = dbCon.getConnection();
      stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
      rs = stmt.executeQuery(sbDetailQuery.toString());
      while (rs.next()) {
        String temp = rs.getString(1);
        log.debug("temp = " + temp + " strBreakId = " + strBreakId);
        if (!strBreakId.equals(temp)) {
          if (!strBreakId.equals("")) {
            hmResult = new HashMap();

            hmResult.put("ID", strId);
            hmResult.put("NAME", strName);
            // hmResult.put("RNAME",strRepName);

            dbTemp = new Double(dbCurrMTDQuota / roundvalue);
            hmResult.put("QMTD", dbTemp); // Month To Date Quota

            dbTemp = new Double(dbCurrQTDQuota / roundvalue);
            hmResult.put("QQTD", dbTemp); // Quarter To Date Quota

            dbTemp = new Double(dbCurrYTDQuota / roundvalue);
            hmResult.put("QYTD", dbTemp); // Year To Date Quota

            if (dbCurrMTDQuota != 0) {
              dbMTDToQuota = (dbCurrMTD / dbCurrMTDQuota) * 100;
              strTemp = df.format(dbMTDToQuota);
              dbTemp = new Double(dbMTDToQuota);
              hmResult.put("MQPER", dbTemp); // Percent To Quota - Month
            } else {
              dbTemp = new Double(0.0);
              hmResult.put("MQPER", dbTemp); // Percent To Quota - Month
            }
            if (dbCurrQTDQuota != 0) {
              dbQTDToQuota = (dbCurrQTD / dbCurrQTDQuota) * 100;
              strTemp = df.format(dbQTDToQuota);
              dbTemp = new Double(strTemp);
              hmResult.put("QQPER", dbTemp); // Percent To Quota - Quarter
            } else {
              dbTemp = new Double(0.0);
              hmResult.put("QQPER", dbTemp); // Percent To Quota - Quarter
            }

            if (dbCurrYTDQuota != 0) {
              dbYTDToQuota = (dbCurrYTD / dbCurrYTDQuota) * 100;
              strTemp = df.format(dbYTDToQuota);
              dbTemp = new Double(strTemp);
              hmResult.put("YQPER", dbTemp); // Percent To Quota - Year
            } else {
              dbTemp = new Double(0.0);
              hmResult.put("YQPER", dbTemp); // Percent To Quota - Year
            }
            // log.debug(hmResult);
            alReturnFromThis.add(hmResult);
          }

          dbCurrMTD = 0;
          dbCurrQTD = 0;
          dbCurrYTD = 0;
          dbCurrYTDQuota = 0;
          dbCurrMTDQuota = 0;
          dbCurrQTDQuota = 0;
          dbMTDToQuota = 0;
          dbQTDToQuota = 0;
          dbYTDToQuota = 0;
        }

        strId = rs.getString(1);
        strQuotaDate = rs.getString(2);
        strQuota = rs.getString(3);
        strName = rs.getString(4);
        // strRepName = rs.getString(5);

        strBreakId = strId;

        if (strQuotaDate != null) {
          strYear = strQuotaDate.substring(0, 2);
          strQtr = strQuotaDate.substring(2, 3);
          strMon = strQuotaDate.substring(3, 5);

          strQuota = GmCommonClass.parseZero(strQuota);

          dbCurrYTDQuota = dbCurrYTDQuota + Double.parseDouble(strQuota);
          if (strMon.equals(strMonth)) {
            dbCurrMTDQuota = Double.parseDouble(strQuota);
          }

          if (strQtr.equals(strQuart)) {
            dbCurrQTDQuota = dbCurrQTDQuota + Double.parseDouble(strQuota);
          }
        }// End of strQuotaDate
      }

      hmResult = new HashMap();

      hmResult.put("ID", strId);
      hmResult.put("NAME", strName);
      // hmResult.put("RNAME",strRepName);

      dbTemp = new Double(dbCurrMTDQuota / roundvalue);
      hmResult.put("QMTD", dbTemp); // Month To Date Quota

      dbTemp = new Double(dbCurrQTDQuota / roundvalue);
      hmResult.put("QQTD", dbTemp); // Quarter To Date Quota

      dbTemp = new Double(dbCurrYTDQuota / roundvalue);
      hmResult.put("QYTD", dbTemp); // Year To Date Quota

      if (dbCurrMTDQuota != 0) {
        dbMTDToQuota = (dbCurrMTD / dbCurrMTDQuota) * 100;
        // strTemp = df.format(dbMTDToQuota);
        dbTemp = new Double(dbMTDToQuota);
        hmResult.put("MQPER", dbTemp); // Percent To Quota - Month
      } else {
        dbTemp = new Double(0.0);
        hmResult.put("MQPER", dbTemp); // Percent To Quota - Month
      }

      if (dbCurrQTDQuota != 0) {
        dbQTDToQuota = (dbCurrQTD / dbCurrQTDQuota) * 100;
        strTemp = df.format(dbQTDToQuota);
        dbTemp = new Double(strTemp);
        hmResult.put("QQPER", dbTemp); // Percent To Quota - Quarter
      } else {
        dbTemp = new Double(0.0);
        hmResult.put("QQPER", dbTemp); // Percent To Quota - Quarter
      }

      if (dbCurrYTDQuota != 0) {
        dbYTDToQuota = (dbCurrYTD / dbCurrYTDQuota) * 100;
        strTemp = df.format(dbYTDToQuota);
        dbTemp = new Double(strTemp);
        hmResult.put("YQPER", dbTemp); // Percent To Quota - Year
      } else {
        dbTemp = new Double(0.0);
        hmResult.put("YQPER", dbTemp); // Percent To Quota - Year
      }

      alReturnFromThis.add(hmResult);
      int intSizeOne = alReturnFromThis.size();

      // log.debug("alReturnFromThis = " + alReturnFromThis);
      // log.debug("intSizeOne = " + intSizeOne);
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    } finally {
      try {
        if (rs != null)
          rs.close();
        if (stmt != null)
          stmt.close();
        if (conn != null)
          conn.close();
      } catch (Exception e) {
        throw new AppError(e);
      }
    }
    return alReturnFromThis;
  }// End of fetchTerritorySalesQuota

  public ArrayList fetchTerrCompSales(HashMap hmFilter) throws AppError {
    ArrayList alReturn = new ArrayList();
    StringBuffer sbDetailQuery = new StringBuffer();
    String strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));

    try {
      sbDetailQuery.append(" SELECT V700.TER_ID ID ");
      sbDetailQuery.append(" ,SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ,V700.TER_NAME NAME ");
      sbDetailQuery.append(" ,nvl(v700.REP_NAME, '^ No Rep') SNAME "); // Holds rep name to be
                                                                       // displayed as additional
                                                                       // column
      sbDetailQuery.append(" FROM ");
      sbDetailQuery.append(" T501_ORDER T501 ");
      sbDetailQuery.append(" , T502_ITEM_ORDER T502 ");
      sbDetailQuery.append(" , V700_TERRITORY_MAPPING_DETAIL V700");
      sbDetailQuery.append(" WHERE ");
      sbDetailQuery.append(" T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbDetailQuery.append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      /*
       * sbDetailQuery.append(" AND "); sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
       * sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
       * sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
       * sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
       */
      sbDetailQuery.append(getSalesFilterClause());
      sbDetailQuery.append(GenerateFilter(hmFilter));

      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));
      // To get additional drilldown information

      sbDetailQuery
          .append(" GROUP BY V700.TER_ID,  V700.TER_NAME , v700.REP_NAME, TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
      if (strType.equals("QUOTA")) {
        sbDetailQuery.append(" ORDER BY NAME, SNAME, DT ");
      } else {
        sbDetailQuery.append(" ORDER BY NAME, SNAME, DT ");
      }


      log.debug("Territory Query Validation " + sbDetailQuery.toString());

      alReturn = parseGrowthData(sbDetailQuery.toString(), "T", hmFilter);

      // // Final Value information
      // hmapFinalValue.put("Details",alReturn);
      // hmapFinalValue.put("FDate",GetDate(hmFilter));

    } catch (Exception e) {
      throw new AppError(e);
    }
    return alReturn;

  }

  /*
   * fetchTerritorySales - This method is used to fetch the sales data on Quota Performance report.
   * Also where clause change to join with AC_ID instead of REP-ID as part of PMT-5662.
   * 
   * Returns ArrayList, Params HashMap, Throws AppError.
   */

  /**
   * To improve the execution time for the Quota/Growth SQL query, we have modified this method to
   * comment the Part number from the where clause when fetching the TerritorySales. Methods
   * Modified: fetchTerritorySales
   * 
   * @author gpalani PMT-10095 (RFC2252)
   * @since 2017-01-03
   */
  public ArrayList fetchTerritorySales(HashMap hmFilter) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    StringBuffer sbSalesQuery = new StringBuffer();
    String strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));
    String strCondition = (String) hmFilter.get("Condition");
    String strCompFilter = GmCommonClass.parseNull((String) hmFilter.get("COMPID"));

    // For getting the price based on Item price filter
    String strCurrType = GmCommonClass.parseNull((String) hmFilter.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmFilter.get("COMP_LANG_ID"));

    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");

    try {
      if (strType.equals("TERR") || strType.equals("Summary")) {
        sbSalesQuery.append(" SELECT V700.TER_ID ID ");
        sbSalesQuery.append(" ," + strItemPriceColumn + " TOTAL ");
        sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
        sbSalesQuery.append(" ,V700.TER_NAME NAME ");
        sbSalesQuery.append(" ,nvl(v700.REP_NAME,'^No Rep') SNAME "); // Holds rep name to be
                                                                      // displayed as additional
                                                                      // column
      }

      if (strType.equals("DIST")) {
        sbSalesQuery.append(" SELECT V700.D_ID ID ");
        sbSalesQuery.append(" ," + strItemPriceColumn + " TOTAL ");
        sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
        sbSalesQuery.append(" ,V700.D_NAME NAME ");
      }

      if (strType.equals("AD")) {
        sbSalesQuery.append(" SELECT V700.REGION_ID ID ");
        sbSalesQuery.append(" ," + strItemPriceColumn + " TOTAL ");
        sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
        sbSalesQuery.append(" ,V700.REGION_NAME || '(' || V700.AD_NAME || ')' NAME ");
      }

      if (strType.equals("VP")) {
        sbSalesQuery.append(" SELECT V700.VP_ID ID ");
        sbSalesQuery.append(" ," + strItemPriceColumn + " TOTAL ");
        sbSalesQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
        sbSalesQuery.append(" ,V700.VP_NAME NAME ");
      }

      sbSalesQuery.append(" FROM ");
      sbSalesQuery.append(" T501_ORDER T501 ");
      sbSalesQuery.append(" , T502_ITEM_ORDER T502 ");
      sbSalesQuery.append(" , (SELECT DISTINCT DECODE(" + strCompFilter
          + ",100803, 100803, compid) compid, divid, ter_id ");
      sbSalesQuery.append(" , ter_name, " + strRepNmColumn
          + " REP_NAME, rep_id ,rep_compid, d_id ," + strDistNmColumn
          + " D_NAME, d_compid, vp_id, vp_name ");
      sbSalesQuery.append(" , region_id, region_name, ad_id, ad_name, gp_id, gp_name, ac_id ");
      sbSalesQuery.append(" FROM v700_territory_mapping_detail V700");
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbSalesQuery.append(" WHERE ");
        sbSalesQuery.append(strCondition);
      }
      sbSalesQuery.append(" ) V700 ");
      sbSalesQuery.append(" WHERE ");
      // sbSalesQuery.append(" t501.c703_sales_rep_id = v700.rep_id " );
      sbSalesQuery.append(" t501.c704_account_id = v700.ac_id ");
      // The following code has been added because the quota was going down.
      // The Following Code has been changed for PMT-5662 This is to handle when Company value is
      // choosed as All in dropdown.
      if (!strCompFilter.equals("")) {
        sbSalesQuery
            .append(" AND  t501.c704_account_id IN ( select distinct ac_id from v700_territory_mapping_detail ");
        sbSalesQuery.append(" WHERE ");
        sbSalesQuery.append(getCompanyFilterClause(strCompFilter));
        sbSalesQuery.append(" ) ");
      }

      sbSalesQuery.append(" AND  T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbSalesQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbSalesQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbSalesQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      // Following Part Number where condition is commented since it is not mandatory to fetch the
      // territory sales.
      // PMT-10095. Author :gpalani
      /*
       * sbSalesQuery.append(" AND "); sbSalesQuery.append(" T502.C205_PART_NUMBER_ID IN ");
       * sbSalesQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
       * sbSalesQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
       * sbSalesQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
       */
      // sbSalesQuery.append(" AND V700.D_ID = 8 ");
      sbSalesQuery.append(getSalesFilterClause());
      sbSalesQuery.append(GenerateFilter(hmFilter));

      sbSalesQuery.append(GenerateAdditionalFilter(hmFilter));
      // To get additional drilldown information

      if (strType.equals("TERR")) {
        sbSalesQuery
            .append(" GROUP BY V700.TER_ID,  V700.TER_NAME , v700.REP_NAME, TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
        sbSalesQuery.append(" ORDER BY SNAME, NAME, DT ");
      }

      if (strType.equals("Summary")) {
        sbSalesQuery
            .append(" GROUP BY V700.TER_ID,  V700.TER_NAME , v700.REP_NAME, TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
        sbSalesQuery.append(" ORDER BY NAME, SNAME, DT ");
      }

      if (strType.equals("DIST")) {
        sbSalesQuery
            .append(" GROUP BY V700.D_ID,  V700.D_NAME , TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
        sbSalesQuery.append(" ORDER BY NAME,  DT ");
      }

      if (strType.equals("AD")) {
        sbSalesQuery
            .append(" GROUP BY V700.REGION_ID, V700.REGION_NAME || '(' || V700.AD_NAME || ')', TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
        sbSalesQuery.append(" ORDER BY NAME,  DT ");
      }

      if (strType.equals("VP")) {
        sbSalesQuery
            .append(" GROUP BY V700.VP_ID,  V700.VP_NAME , TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
        sbSalesQuery.append(" ORDER BY NAME,  DT ");
      }

      log.debug("sbSalesQuery = " + sbSalesQuery.toString());

      alReturn = parseGrowthData(sbSalesQuery.toString(), "T", hmFilter);

    } catch (Exception e) {
      throw new AppError(e);

    }
    return alReturn;

  }

  /**
   * loadTerrCompSales - This Method is used to fetch data for Distributor based on the filter
   * condition
   * 
   * @param HashMap hmFilter Contains filter condition
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap loadDistributorCompSales(HashMap hmFilter) throws AppError {
    ArrayList alReturn = new ArrayList();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    String strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));

    try {

      sbDetailQuery.append(" SELECT V700.D_ID ID ");
      sbDetailQuery.append(" ,SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ,V700.D_NAME NAME ");
      sbDetailQuery.append("  FROM T501_ORDER  T501 ");
      sbDetailQuery.append("  ,T502_ITEM_ORDER T502 ");
      sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
      sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbDetailQuery.append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
      sbDetailQuery.append(getSalesFilterClause());
      sbDetailQuery.append(GenerateFilter(hmFilter));

      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));
      // To get additional drilldown information


      sbDetailQuery.append(" GROUP BY V700.D_ID,  V700.D_NAME , TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
      sbDetailQuery.append(" ORDER BY NAME,  DT ");


      log.debug("Distributor Query Validation " + sbDetailQuery.toString());

      alReturn = parseGrowthData(sbDetailQuery.toString(), "D", hmFilter);

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("FDate", GetDate(hmFilter));

    } catch (Exception e) {
      throw new AppError(e);
    }
    return hmapFinalValue;

  } // End of loadTerrCompSales

  /**
   * loadAccountSalesGrowth - This Method is used to fetch data for all Accounts for a Growth Type
   * report
   * 
   * @param String strDistributorID (Contains the Selected Distributor ID) To be added
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap loadAccountSalesGrowth(HashMap hmFilter) throws AppError {
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    ArrayList alReturn = new ArrayList();
    // For getting the price based on the currency type
    String strCurrType = GmCommonClass.parseNull((String) hmFilter.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
    String strCmpLangId = GmCommonClass.parseNull((String) hmFilter.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "V700");

    try {
      sbDetailQuery.append(" SELECT V700.AC_ID ID ");
      sbDetailQuery.append(" ," + strItemPriceColumn + " TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ," + strAccountNmColumn + " NAME ");
      sbDetailQuery.append("  FROM T501_ORDER T501 ");
      sbDetailQuery.append("  ,T502_ITEM_ORDER T502 ");
      sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
      sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbDetailQuery.append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
      sbDetailQuery.append(getSalesFilterClause());
      // Date filter condition
      sbDetailQuery.append(GenerateFilter(hmFilter));


      // To get additional drilldown information
      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));

      sbDetailQuery.append(" GROUP BY V700.AC_ID,  " + strAccountNmColumn
          + " , TO_CHAR(C501_ORDER_DATE,'YYQMM')   ");
      sbDetailQuery.append(" ORDER BY NAME, DT ");

      log.debug("Account Query " + sbDetailQuery.toString());
      alReturn = parseGrowthData(sbDetailQuery.toString(), "A", hmFilter);

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("FDate", GetDate(hmFilter));

    } catch (Exception e) {
      throw new AppError(e);
    }
    return hmapFinalValue;

  } // End of loadAccountSalesGrowth


  /**
   * loadAccountSalesGrowth - This Method is used to fetch data for all Reps for a Growth Type
   * report
   * 
   * @param String strDistributorID (Contains the Selected Distributor ID) To be added
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap loadRepSalesGrowth(HashMap hmFilter) throws AppError {
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    ArrayList alReturn = new ArrayList();

    // For getting the price based on Currency Type
    String strCurrType = GmCommonClass.parseNull((String) hmFilter.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    try {
      sbDetailQuery.append(" SELECT T501.C703_SALES_REP_ID ID ");
      sbDetailQuery.append(" ," + strItemPriceColumn + " TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ,GET_REP_NAME(T501.C703_SALES_REP_ID)  NAME ");
      sbDetailQuery.append("  FROM T501_ORDER  T501 ");
      sbDetailQuery.append("  ,T502_ITEM_ORDER T502 ");
      sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = V700.AC_ID ");
      // sbDetailQuery.append(" WHERE   T501.C703_SALES_REP_ID = V700.REP_ID");
      sbDetailQuery.append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
      sbDetailQuery.append(getSalesFilterClause());
      // Date filter condition
      sbDetailQuery.append(GenerateFilter(hmFilter));

      // To get additional drilldown information
      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));

      sbDetailQuery
          .append(" GROUP BY T501.C703_SALES_REP_ID , TO_CHAR(C501_ORDER_DATE,'YYQMM')   ");
      sbDetailQuery.append(" ORDER BY NAME, DT ");

      log.debug("Sales Growth Mapping ********** " + sbDetailQuery.toString());

      alReturn = parseGrowthData(sbDetailQuery.toString(), "R", hmFilter);

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("FDate", GetDate(hmFilter));
    } catch (Exception e) {
      throw new AppError(e);
    }
    return hmapFinalValue;

  } // End of loadRepSalesGrowth



  /**
   * loadGroupSalesGrowth - This Method is used to fetch data for all Groups for a Growth Type
   * report
   * 
   * @param String strDistributorID (Contains the Selected Distributor ID) To be added
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap loadGroupSalesGrowth(HashMap hmFilter) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    ArrayList alReturn = new ArrayList();

    // For getting the price based on currency type
    String strCurrType = GmCommonClass.parseNull((String) hmFilter.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    try {
      sbDetailQuery.append(" SELECT T207.C207_SET_ID ID ");
      sbDetailQuery.append(" ," + strItemPriceColumn + " TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ,T207.C207_SET_NM  NAME ");
      sbDetailQuery.append(" ,T207.C207_SEQ_NO SNO ");
      sbDetailQuery.append("  FROM T501_ORDER T501 ");
      sbDetailQuery.append("  ,T502_ITEM_ORDER T502 ");
      sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
      sbDetailQuery.append("  ,T208_SET_DETAILS T208 ");
      sbDetailQuery.append("  ,T207_SET_MASTER T207 ");
      sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbDetailQuery.append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND  T502.C205_PART_NUMBER_ID = T208.C205_PART_NUMBER_ID ");
      sbDetailQuery.append(" AND T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailQuery.append(" AND T207.C901_SET_GRP_TYPE = 1600 ");
      sbDetailQuery.append(getSalesFilterClause());
      // Date filter condition
      sbDetailQuery.append(GenerateFilter(hmFilter));

      // To get additional drilldown information
      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));

      sbDetailQuery
          .append(" GROUP BY T207.C207_SET_ID , T207.C207_SEQ_NO,  T207.C207_SET_NM , TO_CHAR(C501_ORDER_DATE,'YYQMM')   ");
      sbDetailQuery.append(" ORDER BY SNO, DT ");
      log.debug(" Query for Territory perf group " + sbDetailQuery.toString());
      alReturn = parseGrowthData(sbDetailQuery.toString(), "G", hmFilter);

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("FDate", GetDate(hmFilter));
    } catch (Exception e) {
      throw new AppError(e);
    }
    return hmapFinalValue;

  } // End of loadGroupSalesGrowth

  /**
   * loadTerrCompSales - This Method is used to fetch data for Part based on the filter condition
   * 
   * @param HashMap hmFilter Contains filter condition
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap loadPartSalesGrowth(HashMap hmFilter) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    String strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));


    // For getting the price based on Currency Type
    String strCurrType = GmCommonClass.parseNull((String) hmFilter.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    try {

      sbDetailQuery.append(" SELECT T502.C205_PART_NUMBER_ID ID ");
      sbDetailQuery.append(" ," + strItemPriceColumn + " TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ,T205.C205_PART_NUM_DESC NAME ");
      sbDetailQuery.append("  FROM T501_ORDER T501 ");
      sbDetailQuery.append("  ,T502_ITEM_ORDER T502 ");
      sbDetailQuery.append("  ,T205_PART_NUMBER T205 ");
      sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
      sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbDetailQuery.append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND	T205.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
      sbDetailQuery.append(getSalesFilterClause());
      sbDetailQuery.append(GenerateFilter(hmFilter));

      // To get additional drilldown information
      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));


      sbDetailQuery
          .append(" GROUP BY T502.C205_PART_NUMBER_ID,  T205.C205_PART_NUM_DESC, TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
      sbDetailQuery.append(" ORDER BY ID, DT ");


      log.debug("Part Number Query ******************" + sbDetailQuery.toString());
      alReturn = parseGrowthData(sbDetailQuery.toString(), "P", hmFilter);

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("FDate", GetDate(hmFilter));

    } catch (Exception e) {
      throw new AppError(e);
    }
    return hmapFinalValue;

  } // End of loadTerrCompSales

  // This method will generate additional filter for selected value
  // and will be used by all the Growth query
  private String GenerateAdditionalFilter(HashMap hmFilter) {

    String strSetNumber = (String) hmFilter.get("SetNumber");
    String strDistributorID = (String) hmFilter.get("DistributorID");
    String strAccountID = (String) hmFilter.get("AccountID");
    String strRepID = (String) hmFilter.get("RepID");
    String strPartNumber = (String) hmFilter.get("PartNumber");
    String strADID = (String) hmFilter.get("ADID");
    String strTerritoryID = (String) hmFilter.get("TerritoryID");
    String strVPID = (String) hmFilter.get("VPID");
    String strRegnID = (String) hmFilter.get("RegnID");

    StringBuffer sbDetailFilter = new StringBuffer();

    // Area Director Filter
    if (strADID != null && !strADID.equals("null")) {
      sbDetailFilter.append(" AND V700.AD_ID = ");
      sbDetailFilter.append(strADID);
    }

    // Territory Filter
    if (strTerritoryID != null && !strTerritoryID.equals("null")) {
      sbDetailFilter.append(" AND V700.TER_ID = ");
      sbDetailFilter.append(strTerritoryID);
    }

    // Distributor Filter
    if (strDistributorID != null && !strDistributorID.equals("null")) {
      sbDetailFilter.append(" AND V700.D_ID = ");
      sbDetailFilter.append(strDistributorID);
    }

    if (strAccountID != null && !strAccountID.equals("null")) {
      sbDetailFilter.append(" AND  T501.C704_ACCOUNT_ID = ");
      sbDetailFilter.append(strAccountID);
    }

    // Filter by Rep
    if (strRepID != null && !strRepID.equals("null")) {
      sbDetailFilter.append(" AND  T501.C703_SALES_REP_ID = ");
      sbDetailFilter.append(strRepID);
    }
    // Filter by VP
    if (strVPID != null && !strVPID.equals("null")) {
      sbDetailFilter.append(" AND  V700.VP_ID = ");
      sbDetailFilter.append(strVPID);
    }
    // Filter by VP
    if (strRegnID != null && !strRegnID.equals("null")) {
      sbDetailFilter.append(" AND  V700.REGION_ID = ");
      sbDetailFilter.append(strRegnID);
    }

    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
      sbDetailFilter.append(" AND ");
      sbDetailFilter.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailFilter.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailFilter.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailFilter.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");
      sbDetailFilter.append(" AND T207.C207_SET_ID IN ('");
      sbDetailFilter.append(strSetNumber);
      sbDetailFilter.append("') )");

    }

    return sbDetailFilter.toString();
  }

  // This method fetchs filter information for the selected filter
  // passed my the user
  public HashMap GetFilterDetails(HashMap hmFilter) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    String strSetNumber = (String) hmFilter.get("SetNumber");
    String strDistributorID = (String) hmFilter.get("DistributorID");
    String strAccountID = (String) hmFilter.get("AccountID");
    String strRepID = (String) hmFilter.get("RepID");
    String strPartNumber = (String) hmFilter.get("PartNumber");
    String strADID = (String) hmFilter.get("ADID");
    String strTerritoryID = (String) hmFilter.get("TerritoryID");
    String strVPID = (String) hmFilter.get("VPID");
    String strRegnID = (String) hmFilter.get("RegnID");
    // String strADID = (String)hmFilter.get("hAreaDirector");
    log.debug("strADID" + strADID);

    String strCmpLangId = GmCommonClass.parseNull((String) hmFilter.get("COMP_LANG_ID"));

    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");

    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "T701");

    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");
    
    String strViewRepNmColumn =
            gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");

    StringBuffer sbQueryDetails = new StringBuffer();

    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      // VP Filter
      if (strVPID != null && !strVPID.equals("") && !strVPID.equals("null")) {
        sbQueryDetails.append("SELECT VP_ID ID");
        sbQueryDetails.append(" ,VP_Name  NAME");
        sbQueryDetails.append(" FROM V700_TERRITORY_MAPPING_DETAIL ");
        sbQueryDetails.append(" WHERE VP_ID = ");
        sbQueryDetails.append(strVPID);
        sbQueryDetails.append(" AND ROWNUM = 1 ");
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("VPNAME", hmTemp.get("NAME"));
      }

      // Territory Filter
      if (strTerritoryID != null && !strTerritoryID.equals("") && !strTerritoryID.equals("null")) {
        sbQueryDetails.append("SELECT V700.TER_ID ID");
        sbQueryDetails.append(" ,V700.TER_NAME || ' (' || "+strViewRepNmColumn+" ||  ') '  NAME");
        sbQueryDetails.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700");
        sbQueryDetails.append(" WHERE V700.TER_ID = ");
        sbQueryDetails.append(strTerritoryID);
        sbQueryDetails.append(" AND ROWNUM = 1 ");
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("TERRITORY", hmTemp.get("NAME"));
      }


      // Area Director
      if (strRegnID != null && !strRegnID.equals("") && !strRegnID.equals("null")) {
        sbQueryDetails.setLength(0);
        sbQueryDetails.append("SELECT REGION_ID ID");
        sbQueryDetails.append(" ,REGION_NAME || ' (' || AD_NAME ||  ') '  NAME");
        sbQueryDetails.append(" FROM V700_TERRITORY_MAPPING_DETAIL ");
        sbQueryDetails.append(" WHERE REGION_ID = ");
        sbQueryDetails.append(strRegnID);
        sbQueryDetails.append(" AND ROWNUM = 1 ");
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("ADNAME", hmTemp.get("NAME"));
      }

      // Area Director
      if (strADID != null && !strADID.equals("") && !strADID.equals("null")) {
        sbQueryDetails.setLength(0);
        sbQueryDetails.append("SELECT AD_ID ID");
        sbQueryDetails.append(" ,REGION_NAME || ' (' || AD_NAME ||  ') '  NAME");
        sbQueryDetails.append(" FROM V700_TERRITORY_MAPPING_DETAIL ");
        sbQueryDetails.append(" WHERE AD_ID = ");
        sbQueryDetails.append(strADID);
        sbQueryDetails.append(" AND ROWNUM = 1 ");
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("ADNAME", hmTemp.get("NAME"));
      }

      if (strDistributorID != null && !strDistributorID.equals("")
          && !strDistributorID.equals("null")) {
        sbQueryDetails.setLength(0);
        sbQueryDetails.append(" SELECT T701.C701_DISTRIBUTOR_ID ID ");
        sbQueryDetails.append(" ," + strDistNmColumn + "  NAME ");
        sbQueryDetails.append(" FROM T701_DISTRIBUTOR T701");
        sbQueryDetails.append(" WHERE  T701.C701_DISTRIBUTOR_ID  = ");
        sbQueryDetails.append(strDistributorID);
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("FIELDSALESNAME", hmTemp.get("NAME"));

      }

      if (strAccountID != null && !strAccountID.equals("") && !strAccountID.equals("null")) {
        sbQueryDetails.setLength(0);
        sbQueryDetails.append(" SELECT T704.C704_ACCOUNT_ID ID ");
        sbQueryDetails.append(" ," + strAccountNmColumn + "  NAME ");
        sbQueryDetails.append(" FROM T704_ACCOUNT T704");
        sbQueryDetails.append(" WHERE  T704.C704_ACCOUNT_ID  = ");
        sbQueryDetails.append(strAccountID);
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("ACCOUNT", hmTemp.get("NAME"));

      }

      // Filter by Rep
      if (strRepID != null && !strRepID.equals("") && !strRepID.equals("null")) {
        sbQueryDetails.setLength(0);
        sbQueryDetails.append(" SELECT T703.C703_SALES_REP_ID  ID");
        sbQueryDetails.append(" ," + strRepNmColumn + "  NAME ");
        sbQueryDetails.append(" FROM T703_SALES_REP T703");
        sbQueryDetails.append(" WHERE  T703.C703_SALES_REP_ID  = ");
        sbQueryDetails.append(strRepID);
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("REP", hmTemp.get("NAME"));

      }

      // To filter the information based on the set number
      if (strSetNumber != null && !strSetNumber.equals("") && !strSetNumber.equals("0")
          && !strSetNumber.equals("null")) {
        sbQueryDetails.setLength(0);

        sbQueryDetails.append(" SELECT C207_SET_ID  ID");
        sbQueryDetails.append(" ,C207_SET_NM NAME ");
        sbQueryDetails.append(" FROM T207_SET_MASTER ");
        sbQueryDetails.append(" WHERE  C207_SET_ID   = '");
        sbQueryDetails.append(strSetNumber);
        sbQueryDetails.append("'");
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("SET", hmTemp.get("NAME"));

      }

      // To filter the information based on the set number
      if (strPartNumber != null && !strPartNumber.equals("") && !strPartNumber.equals("0")
          && !strPartNumber.equals("null")) {
        sbQueryDetails.setLength(0);

        sbQueryDetails.append(" SELECT C205_PART_NUMBER_ID ID");
        sbQueryDetails.append(" , C205_PART_NUM_DESC   NAME");
        sbQueryDetails.append(" FROM T205_PART_NUMBER ");
        sbQueryDetails.append(" WHERE  C205_PART_NUMBER_ID   = '");
        sbQueryDetails.append(strPartNumber);
        sbQueryDetails.append("'");
        hmTemp = gmDBManager.querySingleRecord(sbQueryDetails.toString());
        hmResult.put("PART", hmTemp.get("NAME"));

      }

    } catch (Exception e) {
      // e.printStackTrace();
      GmLogError.log("Exception in GmSalesReportBean:getFromToDate", "Exception is:" + e);
      throw new AppError(e);
    }

    return hmResult;

  }

  // This method will generate date filter based on the parameter bassed
  // and will be used by all the Growth query
  public HashMap GetDate(HashMap hmFilter) throws AppError {

    String strToMonth = (String) hmFilter.get("ToMonth");
    String strToYear = (String) hmFilter.get("ToYear");


    HashMap hmResult = new HashMap();
    StringBuffer sbQuery = new StringBuffer();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      // Date filter condition
      if (strToMonth == null && strToYear == null) {
        sbQuery.append("SELECT  TO_CHAR(CURRENT_DATE,'MM') TOMONTH");
        sbQuery.append(" ,TO_CHAR(CURRENT_DATE,'YYYY') TOYEAR");
        sbQuery.append(" FROM DUAL");

        hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
        hmResult.put("FirstTime", "'YES");
      }
    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:getFromToDate", "Exception is:" + e);
      throw new AppError(e);
    }
    return hmResult;
  }

  // This method will generate date filter based on the parameter bassed
  // and will be used by all the Growth query
  private HashMap GetMMQYYDate(HashMap hmFilter) throws AppError {

    String strToMonth = (String) hmFilter.get("ToMonth");
    String strToYear = (String) hmFilter.get("ToYear");


    HashMap hmResult = new HashMap();
    StringBuffer sbQuery = new StringBuffer();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      // Date filter condition
      if (strToMonth == null && strToYear == null) {
        sbQuery.append("SELECT  TO_CHAR(CURRENT_DATE,'MM') TOMONTH");
        sbQuery.append(" ,TO_CHAR(CURRENT_DATE,'YYYY') TOYEAR");
        sbQuery.append(" FROM DUAL");

        hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
        hmResult.put("FirstTime", "'YES");
      }
    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:getFromToDate", "Exception is:" + e);
      throw new AppError(e);
    }
    return hmResult;
  }

  // This method will generate filter based on the parameter bassed
  // and will be used by all the Growth query
  public String GenerateFilter(HashMap hmFilter) {

    String strToMonth = (String) hmFilter.get("ToMonth");
    String strToYear = (String) hmFilter.get("ToYear");
    String strCondition = (String) hmFilter.get("Condition");
    String strFilter = (String) hmFilter.get("Filter");

    // /This Parameter will determine if the Previous Year sales should be included or not.
    String strIgnoreLastYrSales =
        GmCommonClass.parseNull((String) hmFilter.get("IGNORE_LAST_YEAR_SALES"));

    StringBuffer sbDetailFilter = new StringBuffer();

    // Date filter condition
    if (strToMonth != null && strToYear != null) {
      sbDetailFilter.append(" AND ( ");
      if (strIgnoreLastYrSales.equals("") || strIgnoreLastYrSales.equals("NO")) {

        sbDetailFilter.append(" T501.C501_ORDER_DATE BETWEEN ");
        sbDetailFilter.append(" ADD_MONTHS(TO_DATE(");
        sbDetailFilter.append(" '01/01/" + strToYear + "', 'MM/DD/YYYY') ");
        sbDetailFilter.append(", -12 )");
        sbDetailFilter.append(" AND ADD_MONTHS( LAST_DAY(TO_DATE('");
        sbDetailFilter.append(strToMonth + "/01/" + strToYear);
        sbDetailFilter.append("','mm/dd/yyyy')), -12) OR ");
      }
      sbDetailFilter.append("   T501.C501_ORDER_DATE BETWEEN TO_DATE(");
      sbDetailFilter.append(" '01/01/" + strToYear + "', 'MM/DD/YYYY') ");
      sbDetailFilter.append(" AND LAST_DAY(TO_DATE('");
      sbDetailFilter.append(strToMonth + "/01/" + strToYear);
      sbDetailFilter.append("','mm/dd/yyyy')) )");

    } else {
      sbDetailFilter.append(" AND ( ");
      if (strIgnoreLastYrSales.equals("") || strIgnoreLastYrSales.equals("NO")) {
        sbDetailFilter.append(" T501.C501_ORDER_DATE BETWEEN TO_DATE('");
        sbDetailFilter.append("01/01'||TO_CHAR(CURRENT_DATE - 365,'YYYY'), 'MM/DD/YYYY') ");
        sbDetailFilter.append(" AND TRUNC(ADD_MONTHS(TRUNC( LAST_DAY(CURRENT_DATE)) , -12))");
        sbDetailFilter.append(" OR  ");
      }
      sbDetailFilter.append(" T501.C501_ORDER_DATE BETWEEN TO_DATE(");
      sbDetailFilter.append(" '01/01'||TO_CHAR(CURRENT_DATE,'YYYY'), 'MM/DD/YYYY') ");
      sbDetailFilter.append(" AND TRUNC(CURRENT_DATE)");
      sbDetailFilter.append(" )");
    }

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailFilter.append(" AND ");
      sbDetailFilter.append(strCondition);
    }

    // Filter Condition based on user selection
    if (!strFilter.toString().equals("")) {
      // sbDetailFilter.append(" AND ");
      sbDetailFilter.append(strFilter);
    }

    return sbDetailFilter.toString();
  }

  /**
   * parseGrowthData - Parses the Data that results from the query for a Growth Type Report. The
   * ORder of the Select statment should be as follows: ID - COntains the ID of the entity TOTAL -
   * COntains the sum DT - Date format NAME - NAme of Entity
   * 
   * @param String strQuery
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList parseGrowthData(String strQuery, String strType, HashMap hmFilter)
      throws AppError {

    ArrayList alReturn = new ArrayList();
    HashMap hmResult = new HashMap();
    HashMap hmDate = new HashMap();

    DBConnectionWrapper dbCon = null;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    dbCon = new DBConnectionWrapper();
    int intColCnt = 0;

    int roundvalue = 1;

    String strId = "";
    String strName = "";
    String strTotal = "";
    String strDate = "";
    String strYear = "";
    String strQtr = "";
    String strMon = "";
    String strDistId = "";
    String strRepId = "";
    String strRepName = "";

    String strPrevYear = "";
    String strPrevQuart = "";
    String strPrevMonth = "";
    String strPrevId = "";

    String strHospIcon = "";
    String strGrpIcon = "";
    String strSpineSIcon = "";
    String strPartIcon = "";

    if (!strType.equals("AD") && !strType.equals("VP")) {
      // Image Icon information
      strHospIcon =
          "<img src='images/HospitalIcon.gif' style='cursor:hand' onClick=\"javascript:fnCallHosp(this, '"
              + strType + "' )\" ";
      strGrpIcon =
          "<img src='images/group._green.gif' style='cursor:hand' onClick=\"javascript:fnCallGrp(this, '"
              + strType + "' )\" ";
      strSpineSIcon =
          "<img src='images/icon_sales.gif'  style='cursor:hand' onClick=\"javascript:fnCallSpineS(this, '"
              + strType + "' )\" ";
      strPartIcon =
          "<img src='images/product_icon.jpg'  style='cursor:hand' onClick=\"javascript:fnCallPart(this, '"
              + strType + "' )\" ";
    }

    double dbPrevYTD = 0;
    double dbCurrYTD = 0;
    double dbYTDGrowth = 0;

    Double dbTemp = new Double(0);
    double dTemp = 0;
    String strTemp = "";

    double dbPrevMTD = 0;
    double dbCurrMTD = 0;
    double dbMTDGrowth = 0;

    double dbPrevQTD = 0;
    double dbCurrQTD = 0;
    double dbQTDGrowth = 0;

    DecimalFormat df = new DecimalFormat("#0.####");

    StringBuffer sbQuery = new StringBuffer();

    // Below Query to fetch date value for the selected value
    String strToMonth = (String) hmFilter.get("ToMonth");
    String strToYear = (String) hmFilter.get("ToYear");

    // ********************************************************
    // Date filter condition
    // used to fetch the quarter, month and year information
    // ********************************************************
    if (strToMonth == null && strToYear == null) {
      sbQuery.append(" SELECT to_char(CURRENT_DATE-365,'YY') YEAR ");
      sbQuery.append(" , to_char(CURRENT_DATE,'MM') MONTH ,");
      sbQuery.append(" to_char(CURRENT_DATE,'Q') QUARTER FROM DUAL ");

    } else {
      sbQuery.append(" SELECT to_char(DT-365,'YY') YEAR ");
      sbQuery.append(" , to_char(DT,'MM') MONTH ,");
      sbQuery.append(" to_char(DT,'Q') QUARTER FROM  ");
      sbQuery.append(" ( SELECT TO_DATE('");
      sbQuery.append(strToMonth + "/" + strToYear);
      sbQuery.append("','MM/YYYY') DT FROM DUAL ) ");
    }

    try {
      roundvalue = Integer.parseInt(((String) hmFilter.get("Divider")));

      dbCon = new DBConnectionWrapper();
      hmDate = dbCon.querySingleRecord(sbQuery.toString());

      strPrevYear = (String) hmDate.get("YEAR");
      strPrevQuart = (String) hmDate.get("QUARTER");
      strPrevMonth = (String) hmDate.get("MONTH");

      conn = dbCon.getConnection();
      stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

      rs = stmt.executeQuery(strQuery);
      log.debug("After executing  the requested query " + strQuery);
      intColCnt = rs.getMetaData().getColumnCount();

      // ----------------------------------------------------------------------------------------------
      while (rs.next()) {
        if (!strId.equals(rs.getString(1))) {
          if (!strId.equals("")) {
            hmResult = new HashMap();
            dbYTDGrowth = dbCurrYTD - dbPrevYTD;

            // Below code to generate Icon setting for multiple drilldown
            strTemp = "";
            if (!strType.equals("AD") && !strType.equals("VP")) {

              if (!strType.equals("G") && !strType.equals("P")) {
                strTemp = strTemp + strGrpIcon + " id=" + strId + ">&nbsp;";
              }

              if (!strType.equals("R") && !strType.equals("P")) {
                strTemp = strTemp + strSpineSIcon + " id=" + strId + ">";
              }

              if (!strType.equals("A") && !strType.equals("P")) {
                strTemp = strTemp + strHospIcon + " id=" + strId + ">&nbsp;";
              }
              if (!strType.equals("P") && !strType.equals("TERR")) {
                strTemp = strTemp + strPartIcon + " id=" + strId + ">&nbsp;";
              }
            }

            hmResult.put("ICON", strTemp);
            hmResult.put("ID", strId);
            hmResult.put("NAME", GmCommonClass.getStringWithTM(strName));
            // To display second name if required (Holds rep and other name)
            hmResult.put("SNAME", strRepName);

            // To calculate YTD Results
            dbTemp = new Double(dbCurrYTD / roundvalue);
            hmResult.put("YTD", dbTemp); // Year To Date

            dbTemp = new Double(dbPrevYTD / roundvalue);
            hmResult.put("PYTD", dbTemp); // Previous Year's To Date
            dbTemp = new Double(dbYTDGrowth / roundvalue);
            hmResult.put("YTDGTHDLR", dbTemp); // YTD Growth in Dollar
            if (dbPrevYTD != 0) {
              dTemp = dbYTDGrowth / dbPrevYTD;
              strTemp = df.format(dTemp);
              dbTemp = new Double(strTemp);
              hmResult.put("YTDGTHPER", dbTemp); // YTD Growth in Percentage
            } else {
              dbTemp = new Double(0.0);
              hmResult.put("YTDGTHPER", dbTemp); // YTD Growth in Percentage
            }

            // To calculate MTD Results
            dbTemp = new Double(dbPrevMTD / roundvalue);
            hmResult.put("PMTD", dbTemp); // Previous Year's Month

            dbTemp = new Double(dbCurrMTD / roundvalue);
            hmResult.put("MTD", dbTemp); // Month To Date

            dbMTDGrowth = dbCurrMTD - dbPrevMTD;
            dbTemp = new Double(dbMTDGrowth / roundvalue);
            hmResult.put("MTDGTHDLR", dbTemp); // MTD Growth in Dollar

            if (dbPrevMTD != 0) {
              dTemp = dbMTDGrowth / dbPrevMTD;
              strTemp = df.format(dTemp);
              dbTemp = new Double(strTemp);
              hmResult.put("MTDGTHPER", dbTemp); // MTD Growth in Percentage
            } else {
              dbTemp = new Double(0.0);
              hmResult.put("MTDGTHPER", dbTemp); // MTD Growth in Percentage
            }

            // To calculate QTD Results
            dbTemp = new Double(dbPrevQTD / roundvalue);
            hmResult.put("PQTD", dbTemp);

            dbTemp = new Double(dbCurrQTD / roundvalue);
            hmResult.put("QTD", dbTemp);

            dbQTDGrowth = dbCurrQTD - dbPrevQTD;
            dbTemp = new Double(dbQTDGrowth / roundvalue);
            hmResult.put("QTDGTHDLR", dbTemp); // QTD Growth in Dollar

            if (dbPrevQTD != 0) {
              dTemp = dbQTDGrowth / dbPrevQTD;
              strTemp = df.format(dTemp);
              dbTemp = new Double(strTemp);
              hmResult.put("QTDGTHPER", dbTemp); // QTD Growth in Percentage
            } else {
              dbTemp = new Double(0.0);
              hmResult.put("QTDGTHPER", dbTemp); // QTD Growth in Percentage
            }
            hmResult.put("PLINE", "");
            alReturn.add(hmResult);
          }

          dbPrevYTD = 0;
          dbCurrYTD = 0;
          dbCurrMTD = 0;
          dbPrevMTD = 0;
          dbPrevQTD = 0;
          dbCurrQTD = 0;
        }

        // To fetch the default value information
        strId = rs.getString(1);
        strTotal = rs.getString(2);
        strDate = rs.getString(3);
        strName = rs.getString(4);

        // To fetch rep name
        if (intColCnt > 4) {
          strRepName = rs.getString(5);
        }

        if (strDate != null) {
          strYear = strDate.substring(0, 2);
          strQtr = strDate.substring(2, 3);
          strMon = strDate.substring(3, 5);

          if (strYear.equals(strPrevYear)) {
            dbPrevYTD = dbPrevYTD + Double.parseDouble(strTotal);
            if (strMon.equals(strPrevMonth)) {
              dbPrevMTD = Double.parseDouble(strTotal);
            }
            if (strQtr.equals(strPrevQuart)) {
              dbPrevQTD = dbPrevQTD + Double.parseDouble(strTotal);
            }
          } else {
            dbCurrYTD = dbCurrYTD + Double.parseDouble(strTotal);
            if (strMon.equals(strPrevMonth)) {
              dbCurrMTD = Double.parseDouble(strTotal);
            }

            if (strQtr.equals(strPrevQuart)) {
              dbCurrQTD = dbCurrQTD + Double.parseDouble(strTotal);
            }
          }
        }
      } // End of While Loop
      // ----------------------------------------------------------------------------------------------


      // To add the final set of value from the loop
      hmResult = new HashMap();
      dbYTDGrowth = dbCurrYTD - dbPrevYTD;

      // Below code to generate Icon setting for multiple drilldown
      strTemp = "";
      if (!strType.equals("AD") && !strType.equals("VP")) {
        if (!strType.equals("G") && !strType.equals("P")) {
          strTemp = strTemp + strGrpIcon + " id=" + strId + ">&nbsp;";
        }

        if (!strType.equals("R") && !strType.equals("P")) {
          strTemp = strTemp + strSpineSIcon + " id=" + strId + ">";
        }

        if (!strType.equals("A") && !strType.equals("P")) {
          strTemp = strTemp + strHospIcon + " id=" + strId + ">&nbsp;";
        }
        if (!strType.equals("P") && !strType.equals("TERR")) {
          strTemp = strTemp + strPartIcon + " id=" + strId + ">&nbsp;";
        }
      }

      hmResult.put("ICON", strTemp);
      hmResult.put("ID", strId);
      hmResult.put("NAME", strName);
      // To display second name if required (Holds rep and other name)
      log.debug("strRepName...." + strRepName);
      hmResult.put("SNAME", strRepName);

      // To calculate TYD Results
      dbTemp = new Double(dbCurrYTD / roundvalue);
      hmResult.put("YTD", dbTemp); // Year To Date

      dbTemp = new Double(dbPrevYTD / roundvalue);
      hmResult.put("PYTD", dbTemp); // Previous Year's To Date

      dbTemp = new Double(dbYTDGrowth / roundvalue);
      hmResult.put("YTDGTHDLR", dbTemp); // YTD Growth in Dollar

      if (dbPrevYTD != 0) {
        dTemp = dbYTDGrowth / dbPrevYTD;
        strTemp = df.format(dTemp);
        dbTemp = new Double(strTemp);
        hmResult.put("YTDGTHPER", dbTemp); // YTD Growth in Percentage
      } else {
        dbTemp = new Double(0.0);
        hmResult.put("YTDGTHPER", dbTemp); // YTD Growth in Percentage
      }

      // To calculate MTD Results
      dbTemp = new Double(dbPrevMTD / roundvalue);
      hmResult.put("PMTD", dbTemp); // Previous Year's Month
      dbTemp = new Double(dbCurrMTD / roundvalue);
      hmResult.put("MTD", dbTemp); // Month To Date

      dbMTDGrowth = dbCurrMTD - dbPrevMTD;
      dbTemp = new Double(dbMTDGrowth / roundvalue);
      hmResult.put("MTDGTHDLR", dbTemp); // MTD Growth in Dollar

      if (dbPrevMTD != 0) {
        dTemp = dbMTDGrowth / dbPrevMTD;
        strTemp = df.format(dTemp);
        dbTemp = new Double(strTemp);
        hmResult.put("MTDGTHPER", dbTemp); // MTD Growth in Percentage
      } else {
        dbTemp = new Double(0.0);
        hmResult.put("MTDGTHPER", dbTemp); // MTD Growth in Percentage
      }

      // To calculate QTD Results
      dbTemp = new Double(dbPrevQTD / roundvalue);
      hmResult.put("PQTD", dbTemp);
      dbTemp = new Double(dbCurrQTD / roundvalue);
      hmResult.put("QTD", dbTemp);

      dbQTDGrowth = dbCurrQTD - dbPrevQTD;
      dbTemp = new Double(dbQTDGrowth / roundvalue);
      hmResult.put("QTDGTHDLR", dbTemp); // QTD Growth in Dollar

      if (dbPrevQTD != 0) {
        dTemp = dbQTDGrowth / dbPrevQTD;
        strTemp = df.format(dTemp);
        dbTemp = new Double(strTemp);
        hmResult.put("QTDGTHPER", dbTemp); // QTD Growth in Percentage
      } else {
        dbTemp = new Double(0.0);
        hmResult.put("QTDGTHPER", dbTemp); // QTD Growth in Percentage
      }
      hmResult.put("PLINE", "");
      alReturn.add(hmResult);
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    } finally {
      try {
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (Exception e) {
        throw new AppError(e);
      }
    }

    // log.debug("1111" + alReturn);
    return alReturn;

  } // End of parseGrowthData

  /**
   * loadADCompSales - This Method is used to fetch data for AD based on the filter condition
   * 
   * @param HashMap hmFilter Contains filter condition
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap loadADCompSales(HashMap hmFilter) throws AppError {
    ArrayList alReturn = new ArrayList();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    String strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));

    try {

      sbDetailQuery.append(" SELECT V700.REGION_NAME ID ");
      sbDetailQuery.append(" ,SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ,V700.AD_NAME NAME  ,V700.REGION_NAME SNAME ");
      sbDetailQuery.append("  FROM T501_ORDER  T501 ");
      sbDetailQuery.append("  ,T502_ITEM_ORDER T502 ");
      sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
      sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbDetailQuery.append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
      sbDetailQuery.append(getSalesFilterClause());
      sbDetailQuery.append(GenerateFilter(hmFilter));

      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));
      // To get additional drilldown information

      sbDetailQuery
          .append(" GROUP BY V700.AD_ID,  V700.AD_NAME ,V700.REGION_NAME, TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
      sbDetailQuery.append(" ORDER BY NAME,  DT ");

      log.debug("AD Query Validation " + sbDetailQuery.toString());

      alReturn = parseGrowthData(sbDetailQuery.toString(), "AD", hmFilter);

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("FDate", GetDate(hmFilter));

    } catch (Exception e) {
      throw new AppError(e);
    }
    return hmapFinalValue;

  } // End of loadADCompSales


  /**
   * loadVPCompSales - This Method is used to fetch data for AD based on the filter condition
   * 
   * @param HashMap hmFilter Contains filter condition
   * @return ArrayList
   * @exception AppError
   **/
  public HashMap loadVPCompSales(HashMap hmFilter) throws AppError {
    ArrayList alReturn = new ArrayList();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap(); // Final value report details
    String strType = GmCommonClass.parseNull((String) hmFilter.get("TYPE"));

    try {

      sbDetailQuery.append(" SELECT C902_CODE_NM_ALT ID ");
      sbDetailQuery.append(" ,SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) TOTAL ");
      sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'YYQMM') DT ");
      sbDetailQuery.append(" ,C902_CODE_NM_ALT NAME ,'' SNAME ");
      sbDetailQuery.append("  FROM T501_ORDER  T501 ");
      sbDetailQuery.append("  ,T502_ITEM_ORDER T502 ");
      sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700 ,  T901_CODE_LOOKUP T901");
      sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbDetailQuery
          .append(" AND	T502.C501_ORDER_ID = T501.C501_ORDER_ID AND V700.REGION_ID = T901.C901_CODE_ID ");
      sbDetailQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  ");
      sbDetailQuery.append(" AND  T501.C501_DELETE_FL IS NULL    ");
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 )");
      sbDetailQuery.append(getSalesFilterClause());
      sbDetailQuery.append(GenerateFilter(hmFilter));

      sbDetailQuery.append(GenerateAdditionalFilter(hmFilter));
      // To get additional drilldown information


      sbDetailQuery.append(" GROUP BY C902_CODE_NM_ALT, TO_CHAR(C501_ORDER_DATE,'YYQMM') ");
      sbDetailQuery.append(" ORDER BY ID,  DT ");


      log.debug("VP Query Validation " + sbDetailQuery.toString());

      alReturn = parseGrowthData(sbDetailQuery.toString(), "VP", hmFilter);

      // Final Value information
      hmapFinalValue.put("Details", alReturn);
      hmapFinalValue.put("FDate", GetDate(hmFilter));

    } catch (Exception e) {
      throw new AppError(e);
    }
    return hmapFinalValue;

  } // End of loadVPCompSales

  // This method will generate filter based on the parameter bassed
  // and will be used by all the Growth query for Quota filter
  private String GenerateQuotaFilter(HashMap hmFilter) {

    String strToMonth = (String) hmFilter.get("ToMonth");
    String strToYear = (String) hmFilter.get("ToYear");
    String strCondition = (String) hmFilter.get("Condition");
    String strFilter = (String) hmFilter.get("Filter");

    StringBuffer sbDetailFilter = new StringBuffer();

    // Date filter condition
    if (strToMonth != null && strToYear != null) {
      // sbDetailFilter.append(" AND (T709.C709_START_DATE BETWEEN " );
      // sbDetailFilter.append(" ADD_MONTHS(TO_DATE(");
      // sbDetailFilter.append(" '01/01/" + strToYear + "', 'MM/DD/YYYY') " );
      // sbDetailFilter.append(", -12 )" );
      // sbDetailFilter.append(" AND ADD_MONTHS( LAST_DAY(TO_DATE('");
      // sbDetailFilter.append(strToMonth + "/01/" + strToYear );
      // sbDetailFilter.append("','mm/dd/yyyy')), -12)  " );
      sbDetailFilter.append(" AND  T709.C709_START_DATE BETWEEN TO_DATE(");
      sbDetailFilter.append(" '01/01/" + strToYear + "', 'MM/DD/YYYY') ");
      sbDetailFilter.append(" AND LAST_DAY(TO_DATE('");
      sbDetailFilter.append(strToMonth + "/01/" + strToYear);
      sbDetailFilter.append("','mm/dd/yyyy')) ");

    } else {
      // sbDetailFilter.append(" AND (T709.C709_START_DATE BETWEEN TO_DATE('");
      // sbDetailFilter.append("01/01'||TO_CHAR(CURRENT_DATE - 365,'YYYY'), 'MM/DD/YYYY') " );
      // sbDetailFilter.append(" AND TRUNC(ADD_MONTHS(TRUNC(CURRENT_DATE) , -12))");
      sbDetailFilter.append(" AND  T709.C709_START_DATE BETWEEN TO_DATE(");
      sbDetailFilter.append(" '01/01'||TO_CHAR(CURRENT_DATE,'YYYY'), 'MM/DD/YYYY') ");
      sbDetailFilter.append(" AND TRUNC(CURRENT_DATE)");
      // sbDetailFilter.append(" )" );
    }


    return sbDetailFilter.toString();
  }

  /**
   * getRegionByUserId - This Method is used to fetch region list which are mapped to the user
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList getRegionByUserId(HashMap hmParam) {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    ArrayList alRegionList = new ArrayList();
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    sbQuery.append(" SELECT C901_REGION_ID FROM T708_REGION_ASD_MAPPING ");
    sbQuery.append(" WHERE C101_USER_ID = '" + strUserId + "' ");
    sbQuery.append("   AND C708_DELETE_FL     IS NULL ");
    sbQuery.append("   AND C708_INACTIVE_FL     IS NULL ");

    log.debug("Region by User Query:==" + sbQuery.toString());

    alRegionList =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    return alRegionList;
  }

} // End of GmSalesCompBean
