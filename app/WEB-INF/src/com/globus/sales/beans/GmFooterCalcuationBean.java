/**
 * 
 */
package com.globus.sales.beans;

import java.util.ArrayList;

import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 */
public class GmFooterCalcuationBean extends GmBean {
	
  public GmFooterCalcuationBean() {
	super(GmCommonClass.getDefaultGmDataStoreVO());
   }
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmFooterCalcuationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  private String columnNameToModify = "";
  private String operator = "";
  private ArrayList operands = new ArrayList();
  private ArrayList numerator = new ArrayList();
  private ArrayList denominator = new ArrayList();

  /**
   * @return Returns the columnNameToModify.
   */
  public String getColumnNameToModify() {
    return columnNameToModify;
  }

  /**
   * @param columnNameToModify The columnNameToModify to set.
   */
  public void setColumnNameToModify(String columnNameToModify) {
    this.columnNameToModify = columnNameToModify;
  }

  /**
   * @return Returns the operands.
   */
  public ArrayList getOperands() {
    return operands;
  }

  /**
   * @param operands The operands to set.
   */
  public void setOperand(String operand) {
    operands.add(operand);
  }

  /**
   * @return Returns the operator.
   */
  public String getOperator() {
    return operator;
  }

  /**
   * @param operator The operator to set.
   */
  public void setOperator(String operator) {
    this.operator = operator;
  }

  /**
   * @return the numerator
   */
  public ArrayList getNumerator() {
    return numerator;
  }

  /**
   * @param numerator the numerator to set
   */
  public void setNumerator(ArrayList numerator) {
    this.numerator = numerator;
  }

  /**
   * @return the denominator
   */
  public ArrayList getDenominator() {
    return denominator;
  }

  /**
   * @param denominator the denominator to set
   */
  public void setDenominator(ArrayList denominator) {
    this.denominator = denominator;
  }

  /**
   * @param operands the operands to set
   */
  public void setOperands(ArrayList operands) {
    this.operands = operands;
  }

}
