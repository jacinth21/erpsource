package com.globus.sales.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

  public class GmSalesConsignDetailBean extends GmSalesFilterConditionBean  {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
  
  public GmSalesConsignDetailBean(GmDataStoreVO gmDataStore) {
	  super(gmDataStore);
  }
	  
  public GmSalesConsignDetailBean() {
	  super(GmCommonClass.getDefaultGmDataStoreVO());
  } 
  /**
      * reportConsignSaleByDetail - This Method is used to fetch the consignment done on part basis for various filter conditions
      * Will have additional Filter   
      * @param String strFromMonth
      * @param String strFromYear
      * @param String strToMonth
      * @param String strToYear   
      * Algorithm :
      *     1. call GmSalesYTDBean.reportYTDByDetail(hmParam) and get the return HashMap as  hmapFinalValue
      *     2. call reportSales_ByDetail (hmParam) and get the return ArrayList as alReportSalesByDetail
      *     3. call getLinkInfo(String strType) - The type would be strSalesByDetail and so on. Get the return HashMap as hmLinkInfoSalesByDetail
      *     4. Link hmapFinalValue and alReportSalesByDetail as hmapFinalValue = gmCrossReport.linkCrossTab(hmapFinalValue, alReportSalesByDetail, hmLinkInfoSalesByDetail);
      *     5. call reportConsignByPart (hmParam) or reportConsignByDistributor(hmParam) depending on the Action and get the return ArrayList as alReportConsignByDetail
      *     6. call getLinkInfo(String strType) - The type would be strSalesByDetail and so on. Get the return HashMap as hmLinkInfoConsignByDetail
      *     7. Link hmapFinalValue and alReportConsignByDetail as hmapFinalValue = gmCrossReport.linkCrossTab(hmapFinalValue, alReportConsignByDetail, hmLinkInfoConsignByDetail);
      *  
      * @return HashMap
      * @exception AppError
    **/
      public HashMap reportConsignSaleByDetail(HashMap hmParam) throws AppError
      {
        HashMap hmapFinalValue = new HashMap();           // Final value report details
        HashMap hmLinkInfoSalesByDetail = new HashMap();
        HashMap hmLinkInfoConsignByDetail = new HashMap();
        String strAction = (String)hmParam.get("hAction");
        
        ArrayList alReportSalesByDetail = new ArrayList();
        ArrayList alReportConsignByDetail = new ArrayList();
        
    GmSalesYTDBean gmSalesYTDBean = new GmSalesYTDBean(getGmDataStoreVO());
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    hmapFinalValue = gmSalesYTDBean.reportYTDByDetail(hmParam);
    log.debug("size of hmapFinalValue " + hmapFinalValue.size());

    alReportSalesByDetail = reportSalesByDetail(hmParam);
    log.debug("size of alReportSalesByDetail " + alReportSalesByDetail.size());
    hmLinkInfoSalesByDetail = getLinkInfo("Sales");
    // log.debug("hmLinkInfoSalesByDetail " + hmLinkInfoSalesByDetail);
    hmapFinalValue =
        gmCrossReport.linkCrossTab(hmapFinalValue, alReportSalesByDetail, hmLinkInfoSalesByDetail);
    // log.debug("After hmLinkInfoSalesByDetail " + hmapFinalValue);

    if (strAction.equals("LoadPart")) {
      alReportConsignByDetail = reportConsignByPart(hmParam);
    }

    if (strAction.equals("LoadDistributor")) {
      alReportConsignByDetail = reportConsignByDistributor(hmParam);
    }

    hmLinkInfoConsignByDetail = getLinkInfo("Consign");
    // log.debug(" hmLinkInfoConsignByDetail is " +hmapFinalValue);

    hmapFinalValue =
        gmCrossReport.linkCrossTab(hmapFinalValue, alReportConsignByDetail,
            hmLinkInfoConsignByDetail);
    // log.debug(" hmapFinalValue is " +hmapFinalValue);
    return hmapFinalValue;
  }

  /**
   * getLinkInfo - This Method is used to to get the link object information
   * 
   * @return HashMap
   * @exception AppError
   **/
  private HashMap getLinkInfo(String strType) {
    HashMap hmLinkDetails = new HashMap();
    HashMap hmapValue = new HashMap();
    ArrayList alLinkList = new ArrayList();

    // First parameter holds the link object name
    hmLinkDetails.put("LINKID", "ID");

    if (strType.equals("Sales")) {
      // Parameter to be added to link list
      hmapValue.put("KEY", "AMOUNT");
      hmapValue.put("VALUE", "Sales");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "UNIT");
      hmapValue.put("VALUE", "Unit");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "AVGSALES");
      hmapValue.put("VALUE", "Avg. Sales");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "AVGUSAGE");
      hmapValue.put("VALUE", "Avg. Usage");
      alLinkList.add(hmapValue);
    }

    else if (strType.equals("Consign")) {
      // Parameter to be added to link list
      hmapValue.put("KEY", "QTY");
      hmapValue.put("VALUE", "Cons # ");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "AMOUNT");
      hmapValue.put("VALUE", "List Value");
      alLinkList.add(hmapValue);
    }

    hmLinkDetails.put("LINKVALUE", alLinkList);
    return hmLinkDetails;
  }

  /**
   * reportSalesByDetail - This Method is used to fetch the sales by Part Will have additional
   * Filter
   * 
   * @param HashMap hmParam
   * 
   * @return ArrayList alReportSalesByDetail
   * @exception AppError
   **/
  public ArrayList reportSalesByDetail(HashMap hmparam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    ArrayList alReportSalesByDetail = new ArrayList();; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
    GmSalesYTDBean gmSalesYTDBean = new GmSalesYTDBean(getGmDataStoreVO());



    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strFilterCondition = getCrossTabFilterCondition(hmparam); // Call the super class method
                                                                     // to get the filter condition

    // For getting the price based on Currency Type
    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
    String strMonthDiff =
        GmCommonClass.getMonthDiff(strFromMonth, strFromYear, strToMonth, strToYear);

    // get the SELECT attributes depending on the strAction value. strAction is set from
    // getCrossTabFilterCondition(hmparam)
    // The SELECT would be either for part or distributor
    String strFrameColumQuery = getFrameColumn(strAction);
    String strFrameGroupQuery = getFrameGroup(strAction);
    String strFrameOrderQuery = getFrameOrder(strAction);
    String strDateConditionQuery = getFromToDate(2);

    log.debug(" strDateConditionQuery " + strDateConditionQuery);
    log.debug(" sbWhereCondtion.toString() " + sbWhereCondtion.toString());
    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery.append(strFrameColumQuery);
    sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    // 2524 to skip the price adjustment count
    sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) UNIT ");
    sbDetailQuery.append(" ," + strItemPriceColumn + " / ");
    sbDetailQuery.append(strMonthDiff);
    sbDetailQuery.append(" AVGSALES ");
    sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY )) / ");
    sbDetailQuery.append(strMonthDiff);
    sbDetailQuery.append(" AVGUSAGE ");
    sbDetailQuery.append(" FROM     T501_ORDER  T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 ");
    sbDetailQuery.append(" ,T205_PART_NUMBER T205 ");
    sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
    sbDetailQuery.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbDetailQuery.append(" AND   V700.AC_ID = T501.C704_ACCOUNT_ID");
    sbDetailQuery.append(" AND   T502.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID");
    sbDetailQuery.append(" AND   T501.C501_VOID_FL IS NULL");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
    sbDetailQuery.append(" AND   T501.C1900_COMPANY_ID =" + getCompId());
    sbDetailQuery.append(" AND   ");
    sbDetailQuery.append(strDateConditionQuery);
    sbDetailQuery.append(strFilterCondition);
    sbDetailQuery.append(getSalesFilterClause());
    if (strRegion != null && !strRegion.equals("") && !strRegion.equals("null")
        && !strRegion.equals("0")) {
      bFilterFlag = true;
      sbDetailQuery.append(" AND V700.REGION_ID = " + strRegion);
    }
    if (strState != null && !strState.equals("") && !strState.equals("null")
        && !strState.equals("0")) {
      bFilterFlag = true;
      sbDetailQuery
          .append(" AND V700.D_ID  IN ( SELECT  T701.C701_DISTRIBUTOR_ID FROM T701_DISTRIBUTOR  T701 WHERE T701.C701_BILL_STATE = '"
              + strState + "' AND T701.C701_VOID_FL IS NULL AND T701.C701_ACTIVE_FL = 'Y') ");
    }
    sbDetailQuery.append(strFrameGroupQuery);

    log.debug("Inside the reportSales_ByDetail sql " + sbDetailQuery.toString());

    if (bFilterFlag == true) {
      alReportSalesByDetail = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());
    }


    return alReportSalesByDetail;
  }

  /**
   * reportConsignByPart - This Method is used to fetch the consignments by Part Will have
   * additional Filter
   * 
   * @param HashMap hmParam
   * 
   * @return ArrayList alReportConsignByPart
   * @exception AppError
   **/
  public ArrayList reportConsignByPart(HashMap hmparam) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();
    StringBuffer sbSetIdQuery = new StringBuffer();
    StringBuffer sbPartQuery = new StringBuffer();
    StringBuffer sbDistQuery = new StringBuffer();
    StringBuffer sbRegionQuery = new StringBuffer();
    StringBuffer sbStateQuery = new StringBuffer();

    getCrossTabFilterCondition(hmparam);
    ArrayList alReportConsignByPart = new ArrayList();; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
    GmSalesYTDBean gmSalesYTDBean = new GmSalesYTDBean(getGmDataStoreVO());
    boolean bFilterFlag = false;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    String strMonthDiff =
        GmCommonClass.getMonthDiff(strFromMonth, strFromYear, strToMonth, strToYear);
    // concatenate the setId and setNumber strings
    log.debug(" strSetNumber " + strSetNumber);

    // get the SELECT attributes depending on the strAction value. strAction is set from
    // getCrossTabFilterCondition(hmparam)
    // The SELECT would be either for part or distributor

    if (!strPartNumber.equals(null) && !strPartNumber.equals("")) {
      sbPartQuery.setLength(0);
      // since the same would be used for CSG and RTN, using a common alias name tcom instead of
      // t505 and t507
      sbPartQuery.append(" AND  T731.C205_PART_NUMBER_ID IN ('");
      sbPartQuery.append(strPartNumber);
      sbPartQuery.append("') ");
      bFilterFlag = true;
    }
    log.debug(" sbPartQuery " + sbPartQuery);


    if (!strSetNumber.equals(null) && !strSetNumber.equals("")) {
      sbSetIdQuery.setLength(0);
      // since the same would be used for CSG and RTN, using a common alias name tcom instead of
      // t505 and t507
      sbSetIdQuery.append(" AND T731.C205_PART_NUMBER_ID IN ");
      sbSetIdQuery.append(" (SELECT C205_PART_NUMBER_ID ");
      sbSetIdQuery.append("  FROM T208_SET_DETAILS ");
      sbSetIdQuery.append("  WHERE C207_SET_ID IN ('");
      sbSetIdQuery.append(strSetNumber);
      sbSetIdQuery.append("') ) ");
      bFilterFlag = true;
    }
    log.debug(" sbSetIdQuery " + sbSetIdQuery);

    if (!strDistributorID.equals(null) && !strDistributorID.equals("")) {
      sbDistQuery.setLength(0);
      // since the same would be used for CSG and RTN, using a common alias name tCOMP instead of
      // t504 and t506
      sbDistQuery.append(" AND  T730.C701_DISTRIBUTOR_ID IN (");
      sbDistQuery.append(strDistributorID);
      sbDistQuery.append(") ");
      bFilterFlag = true;
    }
    // Region Sub Query
    if (strRegion != null && !strRegion.equals("") && !strRegion.equals("null")
        && !strRegion.equals("0")) {
      sbRegionQuery.append(" AND V700.REGION_ID = " + strRegion);
      bFilterFlag = true;
    }
    // State Sub Query
    if (strState != null && !strState.equals("") && !strState.equals("null")
        && !strState.equals("0")) {
      sbStateQuery
          .append(" AND V700.D_ID  IN ( SELECT  T701.C701_DISTRIBUTOR_ID FROM T701_DISTRIBUTOR  T701 WHERE T701.C701_BILL_STATE = '"
              + strState + "' AND T701.C701_VOID_FL IS NULL AND T701.C701_ACTIVE_FL = 'Y') ");
      bFilterFlag = true;
    }

    log.debug(" sbDistQuery " + sbDistQuery);


    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery
        .append(" SELECT T205.C205_PART_NUMBER_ID ID , T205.C205_PART_NUMBER_ID  || ' - ' || T205.C205_PART_NUM_DESC NAME ");
    sbDetailQuery.append(" , SUM(T731.C731_ITEM_QTY) QTY ");
    sbDetailQuery.append(" ,  SUM(T731.C731_ITEM_QTY * T731.C731_ITEM_PRICE ) AMOUNT ");
    sbDetailQuery.append(" FROM t730_virtual_consignment t730 ");
    sbDetailQuery.append(" , T731_VIRTUAL_CONSIGN_ITEM t731 ");
    sbDetailQuery.append(" , v207a_set_consign_link v207 ");
    sbDetailQuery.append(" , T205_PART_NUMBER T205 ");
    sbDetailQuery.append(getAccessFilterClause());
    sbDetailQuery.append(" WHERE  v700.d_id = t730.c701_distributor_id ");
    sbDetailQuery.append(" AND t730.c207_set_id = v207.c207_actual_set_id ");
    sbDetailQuery
        .append(" AND t730.C730_VIRTUAL_CONSIGNMENT_ID = t731.C730_VIRTUAL_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND T731.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(sbPartQuery);
    sbDetailQuery.append(sbSetIdQuery);
    sbDetailQuery.append(sbDistQuery);
    sbDetailQuery.append(sbRegionQuery);
    sbDetailQuery.append(sbStateQuery);
    sbDetailQuery.append(" GROUP BY T205.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC ");
    sbDetailQuery.append(" ORDER BY ID ");
    log.debug("Inside the alReportConsignByPart sql " + sbDetailQuery.toString());

    if (bFilterFlag) {
      alReportConsignByPart = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());
    }

    return alReportConsignByPart;
  }


  /**
   * reportConsignByDistributor - This Method is used to fetch the consignments by distributor Will
   * have additional Filter
   * 
   * @param HashMap hmParam
   * 
   * @return ArrayList alReportConsignByDistributor
   * @exception AppError
   **/
  public ArrayList reportConsignByDistributor(HashMap hmparam) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();
    StringBuffer sbSetIdQuery = new StringBuffer();
    StringBuffer sbPartQuery = new StringBuffer();
    StringBuffer sbDistQuery = new StringBuffer();
    StringBuffer sbRegionQuery = new StringBuffer();
    StringBuffer sbStateQuery = new StringBuffer();
    getCrossTabFilterCondition(hmparam);
    ArrayList alReportConsignByDistributor = new ArrayList();; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
    GmSalesYTDBean gmSalesYTDBean = new GmSalesYTDBean(getGmDataStoreVO());
    boolean bFilterFlag = false;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    String strMonthDiff =
        GmCommonClass.getMonthDiff(strFromMonth, strFromYear, strToMonth, strToYear);
    // concatenate the setId and setNumber strings
    log.debug(" strSetNumber " + strSetNumber);

    // get the SELECT attributes depending on the strAction value. strAction is set from
    // getCrossTabFilterCondition(hmparam)
    // The SELECT would be either for part or distributor
    String strFrameColumQuery = gmSalesYTDBean.getFrameColumn(strAction);
    String strFrameGroupQuery = gmSalesYTDBean.getFrameGroup(strAction);

    if (strPartNumber != null && !strPartNumber.equals("")) {
      sbPartQuery.setLength(0);
      // since the same would be used for CSG and RTN, using a common alias name tcom instead of
      // t505 and t507
      sbPartQuery.append(" AND  COMC.C205_PART_NUMBER_ID IN ('");
      sbPartQuery.append(strPartNumber);
      sbPartQuery.append("') ");
      bFilterFlag = true;
    }
    log.debug(" sbPartQuery " + sbPartQuery);


    if (strSetNumber != null && !strSetNumber.equals("")) {
      sbSetIdQuery.setLength(0);
      // since the same would be used for CSG and RTN, using a common alias name tcom instead of
      // t505 and t507
      sbSetIdQuery.append(" AND COMC.C205_PART_NUMBER_ID IN ");
      sbSetIdQuery.append(" (SELECT C205_PART_NUMBER_ID ");
      sbSetIdQuery.append("  FROM T208_SET_DETAILS ");
      sbSetIdQuery.append("  WHERE C207_SET_ID IN ('");
      sbSetIdQuery.append(strSetNumber);
      sbSetIdQuery.append("') ) ");
      bFilterFlag = true;
    }
    log.debug(" sbSetIdQuery " + sbSetIdQuery);

    if (strDistributorID != null && !strDistributorID.equals("")) {
      sbDistQuery.setLength(0);
      // since the same would be used for CSG and RTN, using a common alias name tCOMP instead of
      // t504 and t506
      sbDistQuery.append(" AND  COMP.C701_DISTRIBUTOR_ID IN (");
      sbDistQuery.append(strDistributorID);
      sbDistQuery.append(") ");
      bFilterFlag = true;
    }
    log.debug(" sbDistQuery " + sbDistQuery);
    // Region Sub Query
    if (strRegion != null && !strRegion.equals("") && !strRegion.equals("null")
        && !strRegion.equals("0")) {
      sbRegionQuery.append(" AND V700.REGION_ID = " + strRegion);
      bFilterFlag = true;
    }
    // State Sub Query
    if (strState != null && !strState.equals("") && !strState.equals("null")
        && !strState.equals("0")) {
      sbStateQuery
          .append(" AND V700.D_ID  IN ( SELECT  T701.C701_DISTRIBUTOR_ID FROM T701_DISTRIBUTOR  T701 WHERE T701.C701_BILL_STATE = '");
      sbStateQuery.append(strState);

      sbStateQuery.append("' AND T701.C701_VOID_FL IS NULL AND T701.C701_ACTIVE_FL = 'Y') ");
      bFilterFlag = true;
    }

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery
        .append(" SELECT CONS.PNUM || ' - ' ||V700.D_ID ID , CONS.PNUM || ' - ' ||V700.D_NAME NAME");
    sbDetailQuery.append(" , NVL((CONS.QTY - NVL(RETN.QTY,0)),0) QTY ");
    sbDetailQuery.append(" , NVL((CONS.PRICE - NVL(RETN.PRICE,0)),0) AMOUNT ");
    sbDetailQuery.append(" FROM ( ");
    sbDetailQuery
        .append(" SELECT COMP.C701_DISTRIBUTOR_ID DISTID, SUM(COMC.C505_ITEM_QTY) QTY, SUM(COMC.C505_ITEM_PRICE * COMC.C505_ITEM_QTY ) PRICE , COMC.C205_PART_NUMBER_ID PNUM  ");
    sbDetailQuery.append(" FROM T504_CONSIGNMENT COMP, T505_ITEM_CONSIGNMENT COMC ");
    sbDetailQuery.append(" WHERE COMP.C504_CONSIGNMENT_ID = COMC.C504_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND COMP.C504_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND COMP.C504_TYPE = 4110 ");
    // Added company id for load records depending on CompanyId.
    sbDetailQuery.append(" AND COMP.C1900_COMPANY_ID = " + getCompId());
    sbDetailQuery.append(" AND TRIM (COMC.C505_CONTROL_NUMBER) IS NOT NULL  ");
    sbDetailQuery.append(sbPartQuery);
    sbDetailQuery.append(sbSetIdQuery);
    sbDetailQuery.append(sbDistQuery);
    sbDetailQuery.append(" GROUP BY COMP.C701_DISTRIBUTOR_ID , COMC.C205_PART_NUMBER_ID  ) CONS ");
    sbDetailQuery
        .append(" ,(SELECT COMP.C701_DISTRIBUTOR_ID DISTID , SUM(COMC.C507_ITEM_QTY) QTY, SUM(COMC.C507_ITEM_PRICE * COMC.C507_ITEM_QTY) PRICE , COMC.C205_PART_NUMBER_ID PNUM ");
    sbDetailQuery.append(" FROM T506_RETURNS COMP, T507_RETURNS_ITEM COMC ");
    sbDetailQuery.append(" WHERE COMP.C506_RMA_ID = COMC.C506_RMA_ID ");
    sbDetailQuery.append(" AND TRIM (COMC.C507_CONTROL_NUMBER) IS NOT NULL ");
    sbDetailQuery.append(" AND COMC.C507_STATUS_FL IN ('C','W')   ");
    sbDetailQuery.append(" AND COMP.C506_VOID_FL IS NULL ");
    sbDetailQuery.append(sbPartQuery);
    sbDetailQuery.append(sbSetIdQuery);
    sbDetailQuery.append(sbDistQuery);
    sbDetailQuery.append(" GROUP BY COMP.C701_DISTRIBUTOR_ID , COMC.C205_PART_NUMBER_ID ) RETN ");

    // Access Filter Query and Part Outer Join to include part return but not consigned
    // Below code frames a Temp table with v700 and Part Number
    sbDetailQuery.append(",( SELECT * FROM ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T205_PART_NUMBER COMC WHERE 1 = 1");
    sbDetailQuery.append(sbPartQuery);
    sbDetailQuery.append(sbSetIdQuery);
    sbDetailQuery.append(" ) ");
    sbDetailQuery.append(getAccessFilterClause());
    sbDetailQuery.append(" ) V700 ");
    // Temp table ends here

    sbDetailQuery.append(" WHERE V700.D_ID = CONS.DISTID  ");
    sbDetailQuery.append(" AND V700.D_ID = RETN.DISTID (+) ");
    sbDetailQuery.append(sbRegionQuery);
    sbDetailQuery.append(sbStateQuery);
    sbDetailQuery.append(" AND V700.C205_PART_NUMBER_ID  = RETN.PNUM (+) ");
    sbDetailQuery.append(" AND V700.C205_PART_NUMBER_ID =  CONS.PNUM (+) ");
    sbDetailQuery.append(" AND NVL((CONS.PRICE - NVL(RETN.PRICE,0)),0) <> 0 ");
    sbDetailQuery.append(" ORDER BY ID ");

    log.debug("Inside the reportConsignByDistributor sql " + sbDetailQuery.toString());
    if (bFilterFlag) {
      alReportConsignByDistributor = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());
    }

    return alReportConsignByDistributor;
  }


}
