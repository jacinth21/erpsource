package com.globus.sales.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;

public class GmBaselineValueBean {
    
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
    GmCommonClass gmCommon = new GmCommonClass();
    
    /**
      * loadbaselineValue - This method will 
      * @return ArrayList
      * @exception AppError
    **/
     
    public ArrayList loadBaselineValue(String strBaselineType)throws AppError
    {
        DBConnectionWrapper dbCon =new DBConnectionWrapper();        
        CallableStatement csmt = null;
        Connection conn = null;
        String strPrepareQuery = null;
        ArrayList alResult = new ArrayList();
        ResultSet rs = null;

        try
        {            
            conn = dbCon.getConnection();
            strPrepareQuery = dbCon.getStrPrepareString("GM_PKG_SM_BASELINE.GM_SM_RPT_BASELINEVALUE",2);
            csmt = conn.prepareCall(strPrepareQuery);            
            csmt.registerOutParameter(2,OracleTypes.CURSOR); //register out parameter    
            csmt.setString(1,strBaselineType);
            csmt.execute();
            rs = (ResultSet)csmt.getObject(2);
            alResult = dbCon.returnArrayList(rs);         
        }catch(Exception e)
        {
            e.printStackTrace();
            GmLogError.log("Exception in GmBaselineValueBean:loadbaselineValue","Exception is:"+e);
            throw new AppError(e);
        }
        finally
        {
            try
            {
                if (csmt != null)
                    csmt.close();                  
                if(conn!=null)                
                    conn.close();                       
            }
            catch(Exception e)
            {
                throw new AppError(e);
            }
            finally
            {
                 csmt = null;
                 conn = null;
                 dbCon = null;
            }
        }
        return alResult;   
    } // End of loadPendingTransfer

    public void saveBaselineValue(String strBaselineParameters, String strType, String strUserID)throws AppError
    {
        DBConnectionWrapper dbCon =new DBConnectionWrapper();        
        CallableStatement csmt = null;
        Connection conn = null;
        String strPrepareQuery = null;
        
        try
        {            
            conn = dbCon.getConnection();
            conn.setAutoCommit(false);
            strPrepareQuery = dbCon.getStrPrepareString("GM_PKG_SM_BASELINE.GM_SM_SAV_BASELINEVALUE",3);
            csmt = conn.prepareCall(strPrepareQuery);            
            csmt.setString(1,strBaselineParameters);
            csmt.setString(2,strType);
            csmt.setString(3,strUserID);
            csmt.execute();  
            conn.commit();
            
        }catch(Exception e)
        {
            e.printStackTrace();
            GmLogError.log("Exception in GmBaselineValueBean:savebaselineValue","Exception is:"+e);
            throw new AppError(e);
        }
        finally
        {
            try
            {
                if (csmt != null)
                    csmt.close();                  
                if(conn!=null)                
                    conn.close();                       
            }
            catch(Exception e)
            {
                throw new AppError(e);
            }
            finally
            {
                 csmt = null;
                 conn = null;
                 dbCon = null;
            }
        }   
    } // End of saveBaselineValue

}
