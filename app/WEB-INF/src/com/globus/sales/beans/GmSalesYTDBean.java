/*
 * Module: GmSalesYTDBean.java Author: RichardK Project: Globus Medical App Date-Written: Mar 2005
 * Security: Unclassified Description: This beans will be used to generate all kind of Sales YTD
 * Report bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.sales.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSalesYTDBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
                                                               // Initialize
                                                               // the
                                                               // Logger
                                                               // Class.

  public GmSalesYTDBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmSalesYTDBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Territory and AD Will have
   * additional Filter
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDBySummary(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    // Below variable is used for cross tab format
    ArrayList alRowDetail = new ArrayList();
    ArrayList alColDetail = new ArrayList();
    ArrayList alDetailField = new ArrayList();

    HashMap hmapGroupInfo = new HashMap();
    HashMap hmapValue = new HashMap();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strAccessFilter = (String) hmparam.get("AccessFilter");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));
    log.debug("strAccessFilter  " + strAccessFilter);

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the territory header information
    // ***********************************************************
    sbDetailQuery
        .append(" SELECT max(v700.region_id)||'_'||NVL(DECODE(v700.AD_ID,-9999,'9999',v700.AD_ID),0) || 'L3' AD_ID ");
    sbDetailQuery
        .append(" ,v700.REGION_NAME || ' - (' || NVL(v700.AD_NAME,'*No AD') || ')'  AD_NAME ");
    sbDetailQuery.append(" ,NVL(v700.D_ID,0) || 'L2' D_ID  ");
    sbDetailQuery.append(" ,NVL(" + strDistNmColumn + ",' *No Distributor') D_NAME ");
    sbDetailQuery.append(" ,NVL(v700.REP_ID,0) REP_ID  ");
    sbDetailQuery.append(" ,NVL(" + strRepNmColumn + ", '*No Rep Name') REP_NAME ");
    sbDetailQuery.append(" ,v700.TER_ID || 'L1' TER_ID");
    sbDetailQuery.append(" ,v700.TER_NAME || ' - (' || NVL(" + strRepNmColumn
        + ", '*No Rep Name')  || ')'  TER_NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY')  R_DATE ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append(" FROM   V700_TERRITORY_MAPPING_DETAIL V700, ");
    sbDetailQuery.append(" T501_ORDER  T501, ");
    sbDetailQuery.append(" T502_ITEM_ORDER T502     ");
    sbDetailQuery.append(" WHERE V700.AC_ID = T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND 	T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
    sbDetailQuery.append(" AND  T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL  ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL    ");
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
      sbDetailQuery.append(" AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // Filter by Rep
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // Filter Condition to fetch record based on access code
    /*
     * if (!strCondition.toString().equals("")) { sbDetailQuery.append(" AND ");
     * sbDetailQuery.append(strCondition); }
     */

    sbDetailQuery.append(" GROUP BY V700.REGION_NAME , V700.AD_ID, V700.AD_NAME ");
    sbDetailQuery.append(" , V700.D_ID ," + strDistNmColumn + " ");
    sbDetailQuery.append(" , V700.TER_ID ,V700.TER_NAME ");
    sbDetailQuery.append(" , v700.REP_ID ," + strRepNmColumn + " ");
    sbDetailQuery.append(" , TO_CHAR(C501_ORDER_DATE,'Mon YY') ");

    sbDetailQuery.append(" UNION ");

    // ****************************************************
    // Below Query to fetch no sales made rep information
    // *****************************************************
    sbDetailQuery
        .append(" SELECT v700.region_id||'_'||NVL(DECODE(v700.AD_ID,-9999,'9999',v700.AD_ID),0) || 'L3' AD_ID ");
    sbDetailQuery
        .append(" ,v700.REGION_NAME || ' - (' || NVL(v700.AD_NAME,'*No AD') || ')'  AD_NAME ");
    sbDetailQuery.append(" ,NVL(v700.D_ID,0) || 'L2' D_ID  ");
    sbDetailQuery.append(" ,NVL(" + strDistNmColumn + ",' *No Distributor') D_NAME ");
    sbDetailQuery.append(" ,NVL(v700.REP_ID,0) REP_ID  ");
    sbDetailQuery.append(" ,NVL(" + strRepNmColumn + ", '*No Rep Name') REP_NAME ");
    sbDetailQuery.append(" ,v700.TER_ID || 'L1' TER_ID");
    sbDetailQuery.append(" ,v700.TER_NAME || ' - (' || NVL(" + strRepNmColumn
        + ", '*No Rep Name')  || ')'  TER_NAME ");
    // sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbDetailQuery.append(" ,TO_CHAR(CURRENT_DATE,'Mon YY') R_DATE ");
    sbDetailQuery.append(" ,0 AMOUNT ");
    sbDetailQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
    sbDetailQuery.append(" WHERE V700.REP_ID  IN ( ");
    sbDetailQuery.append(" SELECT  C703_SALES_REP_ID  FROM T703_SALES_REP ");
    sbDetailQuery.append(" WHERE C703_SALES_FL = 'N' AND C703_ACTIVE_FL  = 'Y') ");

    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery.append(" AND	 V700.D_ID = " + strDistributorID);
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	  V700.AC_ID = " + strAccountID);
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 V700.REP_ID  = " + strRepID);
    }


    // sbDetailQuery.append(" ORDER BY REP_NAME");
    // Final Condition
    sbDetailQuery.append(" ORDER BY AD_NAME, D_NAME, TER_NAME ");

    /***********************************************************************
     * Report setting details to specify row and column information alRowDetail Holds Row
     * Information alColDetail Holds Column Information alDetailField Holds detail field value
     * (Month Data)
     **********************************************************************/

    // Territory Information Level I
    hmapValue.put("KEY", "TER_NAME");
    hmapValue.put("ID", "TER_ID");
    alRowDetail.add(hmapValue);

    // Distributor Information Level II
    hmapValue = null;
    hmapValue = new HashMap();
    hmapValue.put("KEY", "D_NAME");
    hmapValue.put("ID", "D_ID");
    alRowDetail.add(hmapValue);

    // Area Director Information Level III
    hmapValue = null;
    hmapValue = new HashMap();
    hmapValue.put("KEY", "AD_NAME");
    hmapValue.put("ID", "AD_ID");
    alRowDetail.add(hmapValue);

    alColDetail.add("R_DATE");

    alDetailField.add("AMOUNT");

    hmapGroupInfo.put("ROW", alRowDetail);
    hmapGroupInfo.put("COLUMN", alColDetail);
    hmapGroupInfo.put("DETAILFIELD", alDetailField);

    log.debug(sbDetailQuery.toString());    
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString(),
            hmapGroupInfo);

    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Territory List Will have
   * additional Filter, Passed thru hmparam
   * 
   * @param HashMap hmparam Holds filter information
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByTerritory(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strAccessFilter = (String) hmparam.get("AccessFilter");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
    String strCmpLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Territory month report
    // ***********************************************************

    // To fetch the order information
    sbDetailQuery.append(" SELECT V700.TER_ID    ");
    sbDetailQuery
        .append(" ,v700.TER_NAME || ' - (' || NVL("+strRepNmColumn+", '*No Rep Name')  || ')'  NAME");
    sbDetailQuery.append("  ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE   ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }
    sbDetailQuery.append(" FROM	T501_ORDER  T501   ");
    sbDetailQuery.append(" 		,T502_ITEM_ORDER T502   ");
    sbDetailQuery.append(" 		,V700_TERRITORY_MAPPING_DETAIL V700 ");
    sbDetailQuery.append(" WHERE T502.C501_ORDER_ID       = T501.C501_ORDER_ID   ");
    sbDetailQuery
        .append(" AND   T501.C501_VOID_FL IS NULL   AND T502.C502_VOID_FL IS NULL AND   T501.C501_DELETE_FL IS NULL   ");
    sbDetailQuery.append(" AND	   V700.AC_ID	= T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");
    sbDetailQuery.append(getSalesFilterClause());
    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
      sbDetailQuery.append(" AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }
    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Filter by Rep
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // Filter Condition to fetch record based on access code
    /*
     * if (!strCondition.toString().equals("")) { sbDetailQuery.append(" AND ");
     * sbDetailQuery.append(strCondition); }
     */
    sbDetailQuery.append(" GROUP BY V700.TER_ID    ,V700.TER_NAME, "+strRepNmColumn+",  ");
    sbDetailQuery.append(" TO_CHAR(C501_ORDER_DATE,'Mon YY') ");
    sbDetailQuery.append(" ORDER BY NAME,  TO_DATE(R_DATE,'MON YY') ");

    log.debug("Territory Mapping $$$$$$$$$ " + sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Distributor List Will have
   * additional Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByDist(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strAccessFilter = (String) hmparam.get("AccessFilter");
    String strReportType = (String) hmparam.get("RPTTYPE");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "V700");

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER  T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    if (strReportType.equals("DIST")) {
      // To fetch the Distributor information
      sbDetailQuery.append(" SELECT D_ID  ID  ," + strDistNmColumn + " NAME ");
    } else if (strReportType.equals("AD")) {
      // To fetch the AD information
      sbDetailQuery.append(" SELECT REGION_ID  ID, REGION_NAME NAME ");
    } else if (strReportType.equals("VP")) {
      // To fetch the VP information
      sbDetailQuery.append(" SELECT C902_CODE_NM_ALT ID , GET_CODE_NAME(C902_CODE_NM_ALT) NAME ");
    }

    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE   ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }
    sbDetailQuery.append(" FROM	T501_ORDER T501   ");
    sbDetailQuery.append(" 		,T502_ITEM_ORDER T502   ");
    sbDetailQuery.append(" 		,V700_TERRITORY_MAPPING_DETAIL V700 ");
    if (strReportType.equals("VP")) {
      // To fetch the VP information
      sbDetailQuery.append(" , T901_CODE_LOOKUP T901 ");
      sbDetailQuery.append(" WHERE V700.REGION_ID = T901.C901_CODE_ID AND ");
    } else {
      sbDetailQuery.append(" WHERE ");
    }

    sbDetailQuery.append(" T502.C501_ORDER_ID       = T501.C501_ORDER_ID   ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery
        .append(" AND   T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL AND   T501.C501_DELETE_FL IS NULL   ");
    sbDetailQuery.append(" AND	   V700.AC_ID	= T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("") && !strSetNumber.equals("null")
        && !strSetNumber.equals("0")) {
      sbDetailQuery.append(" AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Filter by Rep
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // Filter Condition to fetch record based on access code
    /*
     * if (!strCondition.toString().equals("")) { sbDetailQuery.append(" AND ");
     * sbDetailQuery.append(strCondition); }
     */

    // Final Condition
    if (strReportType.equals("DIST")) {
      sbDetailQuery.append(" GROUP BY D_ID," + strDistNmColumn
          + ",  TO_CHAR(C501_ORDER_DATE,'Mon YY')  ");
    } else if (strReportType.equals("AD")) {
      sbDetailQuery
          .append(" GROUP BY REGION_ID, REGION_NAME,  TO_CHAR(C501_ORDER_DATE,'Mon YY')  ");
    } else if (strReportType.equals("VP")) {
      sbDetailQuery.append(" GROUP BY C902_CODE_NM_ALT,  TO_CHAR(C501_ORDER_DATE,'Mon YY')  ");
    }
    sbDetailQuery.append(" ORDER BY NAME, TO_DATE(R_DATE,'MON YY') ");

    log.debug("SQL FOR DISTRIBUTOR / AD -- " + sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Distributor List Actual value
   * Will have additional Filter, Yet to Decide on the same Fetchs values based on the sales made by
   * the distributor and not by there territory
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByDistActual(HashMap hmparam) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strMSetNumber = (String) hmparam.get("MSetNumber");
    log.debug("strMSetNumber *" + strMSetNumber);
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "T701");


    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER  T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // Distributor Query updated as on Apr 06 (reason query was very slow)
    // ***********************************************************
    sbDetailQuery.append(" SELECT   DISTRIBUTOR_ID  ");
    sbDetailQuery.append(" ,NAME ");
    sbDetailQuery.append(" ,R_DATE ");
    sbDetailQuery.append(" ,SUM(AMOUNT ) AMOUNT ");
    sbDetailQuery.append("  FROM ");

    // Query to fetch distributor information
    sbDetailQuery.append(" ( SELECT T703.C703_SALES_REP_ID,  ");
    sbDetailQuery.append(" T701.C701_DISTRIBUTOR_ID DISTRIBUTOR_ID  ,  ");
    sbDetailQuery.append(" " + strDistNmColumn + " NAME   ");
    sbDetailQuery.append(" FROM T703_SALES_REP T703,  ");
    sbDetailQuery.append("  	T701_DISTRIBUTOR T701  ");
    sbDetailQuery.append(" WHERE T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID ");

    // Distributor Filter
    if (strDistributorID != null && !strDistributorID.equals("")
        && !strDistributorID.equals("null")) {
      sbDetailQuery.append(" AND	 T701.C701_DISTRIBUTOR_ID = " + strDistributorID);
    }

    sbDetailQuery.append(" )  REP_DISP_DETAILS,  ");

    // To fetch the order information
    sbDetailQuery.append(" ( SELECT T501.C703_SALES_REP_ID   ");
    sbDetailQuery.append("  ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE   ");

    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }
    sbDetailQuery.append(" FROM	T501_ORDER  T501   ");
    sbDetailQuery.append(" 		,T502_ITEM_ORDER T502   ");

    sbDetailQuery.append(" WHERE T502.C501_ORDER_ID       = T501.C501_ORDER_ID   ");
    sbDetailQuery
        .append(" AND   T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL AND   T501.C501_DELETE_FL IS NULL   ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // To filter by main set id
    if (strMSetNumber != null && !strMSetNumber.equals("null") && !strMSetNumber.equals("0")
        && !strMSetNumber.equals("")) {
      sbDetailQuery.append(" AND T208.C207_SET_ID  IN ");
      sbDetailQuery.append(" ( SELECT  C207_LINK_SET_ID FROM T207A_SET_LINK  ");
      sbDetailQuery.append(" WHERE   C207_MAIN_SET_ID = '");
      sbDetailQuery.append(strMSetNumber);
      sbDetailQuery.append("' AND C901_TYPE = 20001 )");

    }

    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")
        && !strSetNumber.equals("")) {
      sbDetailQuery.append(" AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");
    // Below if
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Filter by Rep
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }

    sbDetailQuery.append(" GROUP BY T501.C703_SALES_REP_ID, ");
    sbDetailQuery.append(" TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY' ) ORD_DETAIL   ");

    // Final Condition
    sbDetailQuery
        .append(" WHERE 	REP_DISP_DETAILS.C703_SALES_REP_ID  = ORD_DETAIL.C703_SALES_REP_ID ");
    sbDetailQuery.append(" GROUP BY DISTRIBUTOR_ID,NAME,  R_DATE  ");
    sbDetailQuery.append(" ORDER BY NAME, TO_DATE(R_DATE,'MON YY') ");

    log.debug("Actual Distributor SQL -- " + sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * reportYTDByAccount - This Method is used to fetch the sales YTD for the selected account
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByAccount(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strADID = (String) hmparam.get("ADID");
    String strTerritoryID = (String) hmparam.get("TerritoryID");
    String strAccessFilter = (String) hmparam.get("AccessFilter");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "V700");

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Holds the SQL Report
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER  T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Account month report
    // ***********************************************************
    sbDetailQuery.append("SELECT   V700.AC_ID ID");
    sbDetailQuery.append(" ," + strAccountNmColumn + " NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    // sbDetailQuery.append(" ,SUM(C501_TOTAL_COST) AMOUNT ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append("  FROM T501_ORDER  T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 "); // Item Order Link
    sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
    sbDetailQuery.append(" WHERE  V700.AC_ID	= T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	  T502.C501_ORDER_ID	   = T501.C501_ORDER_ID "); // Item
    // Order
    // Link
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");

    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // To add the part number filter
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208, ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // to fetch record based on filter code
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")
        && !strSetNumber.equals("")) {
      sbDetailQuery.append("AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // Distributor Filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery.append(" AND	 V700.D_ID = " + strDistributorID);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // Rep Filter
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    // Territory Filter
    if (strTerritoryID != null && !strTerritoryID.equals("null") && !strTerritoryID.equals("")) {
      sbDetailQuery.append(" AND	 V700.TER_ID = ");
      sbDetailQuery.append(strTerritoryID);
    }

    // Area Director filter
    if (strADID != null && !strADID.equals("null") && !strADID.equals("")) {
      sbDetailQuery.append(" AND	 V700.AD_ID  = ");
      sbDetailQuery.append(strADID);
    }

    // Filter Condition to fetch record based on access code
    /*
     * if (!strCondition.toString().equals("")) { sbDetailQuery.append(" AND ");
     * sbDetailQuery.append(strCondition); }
     */

    sbDetailQuery.append(" GROUP BY V700.AC_ID");
    sbDetailQuery.append(" , " + strAccountNmColumn + "");
    sbDetailQuery.append(" , TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");
    sbDetailQuery.append(" ORDER BY NAME,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug(sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByAccountActual - This Method is used to fetch the sales YTD for the selected account
   * based on actual value
   * 
   * @param HashMap hmparam (Contains the user filter information)
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByAccountActual(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));
    String strGroupAccId = GmCommonClass.parseNull((String) hmparam.get("GroupAccId"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Holds the SQL Report
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Account month report
    // ***********************************************************
    sbDetailQuery.append("SELECT   T704.C704_ACCOUNT_ID ID");
    sbDetailQuery.append(" ," + strAccountNmColumn + " NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    // sbDetailQuery.append(" ,SUM(C501_TOTAL_COST) AMOUNT ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append("  FROM T501_ORDER T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 "); // Item Order Link
    sbDetailQuery.append(" ,T703_SALES_REP T703 ");
    sbDetailQuery.append(" ,T701_DISTRIBUTOR T701 ");
    sbDetailQuery.append(" ,T704_ACCOUNT T704 ");
    sbDetailQuery.append(" WHERE T703.C703_SALES_REP_ID   = T501.C703_SALES_REP_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	 T701.C701_DISTRIBUTOR_ID = T703.C701_DISTRIBUTOR_ID ");
    sbDetailQuery.append(" AND	 T704.C704_ACCOUNT_ID	  = T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(" AND	  T502.C501_ORDER_ID	   = T501.C501_ORDER_ID "); // Item
    // Order
    // Link
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL   AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");

    // To add the part number filter
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208, ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // to fetch record based on filter code
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")
        && !strSetNumber.equals("")) {
      sbDetailQuery.append("AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");

    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }

    // Distributor Filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery.append(" AND	 T701.C701_DISTRIBUTOR_ID = " + strDistributorID);
    }

    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // Rep Filter
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }
    if (!strGroupAccId.equals("")) {
      sbDetailQuery
          .append(" AND t501.c704_account_id IN   (SELECT c704_account_id   FROM T740_GPO_ACCOUNT_MAPPING  WHERE c101_party_id = '");
      sbDetailQuery.append(strGroupAccId);
      sbDetailQuery.append("') ");
    }
    sbDetailQuery.append(" GROUP BY T704.C704_ACCOUNT_ID");
    sbDetailQuery.append(" , " + strAccountNmColumn + "");
    sbDetailQuery.append(" , TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");
    sbDetailQuery.append(" ORDER BY NAME,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug(sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD for the selected account
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDBySalesRep(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strTerritoryID = (String) hmparam.get("TerritoryID");
    String strAccessFilter = (String) hmparam.get("AccessFilter");
    String strADID = (String) hmparam.get("ADID");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
    String strCompLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));
    String strRepColumnNm = gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCompLangId, "V700");

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Holds the SQL Report
    GmCrossTabReport gmCrossReport = new GmCrossTabReport(getGmDataStoreVO());

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER  T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Account month report
    // ***********************************************************
    sbDetailQuery.append("SELECT T501.C703_SALES_REP_ID ID");
    sbDetailQuery.append(" ,GET_REP_NAME(T501.C703_SALES_REP_ID) REP_NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    // sbDetailQuery.append(" ,SUM(C501_TOTAL_COST) AMOUNT ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append("  FROM T501_ORDER T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 "); // Item Order Link
    sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
    sbDetailQuery.append(" WHERE  V700.AC_ID	= T501.C704_ACCOUNT_ID");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	  T502.C501_ORDER_ID	   = T501.C501_ORDER_ID "); // Item
    // Order
    // Link
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");

    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // To add the part number filter
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208, ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // to fetch record based on set filter code
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")
        && !strSetNumber.equals("")) {
      sbDetailQuery.append("AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }

    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery.append(" AND	 V700.D_ID = " + strDistributorID);
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Territory Filter
    if (strTerritoryID != null && !strTerritoryID.equals("null") && !strTerritoryID.equals("")) {
      sbDetailQuery.append(" AND	 V700.TER_ID = ");
      sbDetailQuery.append(strTerritoryID);
    }

    // Area Director filter
    if (strADID != null && !strADID.equals("null") && !strADID.equals("")) {
      sbDetailQuery.append(" AND   V700.AD_ID  = ");
      sbDetailQuery.append(strADID);
    }

    sbDetailQuery.append(" GROUP BY T501.C703_SALES_REP_ID");
    // sbDetailQuery.append(" , V700.REP_NAME");
    sbDetailQuery.append(" , TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");

    sbDetailQuery.append(" UNION ");

    // ****************************************************
    // Below Query to fetch no sales made rep information
    sbDetailQuery.append(" SELECT  DISTINCT V700.REP_ID ID ");
    sbDetailQuery.append(" ,"+strRepColumnNm+"  REP_NAME ");
    sbDetailQuery.append(" ,TO_CHAR(CURRENT_DATE,'Mon YY') R_DATE ");
    sbDetailQuery.append(" ,0 AMOUNT ");
    sbDetailQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
    sbDetailQuery.append(" WHERE V700.REP_ID  IN ( ");
    sbDetailQuery.append(" SELECT  C703_SALES_REP_ID  FROM T703_SALES_REP ");
    sbDetailQuery.append(" WHERE C703_SALES_FL = 'N' AND C703_ACTIVE_FL  = 'Y') ");

    // Filter Condition to fetch record based on access code
    /*
     * if (!strCondition.toString().equals("")) { sbDetailQuery.append(" AND ");
     * sbDetailQuery.append(strCondition); }
     */

    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery.append(" AND	 V700.D_ID = " + strDistributorID);
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	  V700.AC_ID = " + strAccountID);
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 V700.REP_ID  = " + strRepID);
    }

    // Territory Filter
    if (strTerritoryID != null && !strTerritoryID.equals("null") && !strTerritoryID.equals("")) {
      sbDetailQuery.append(" AND	 V700.TER_ID = ");
      sbDetailQuery.append(strTerritoryID);
    }

    // Area Director filter
    if (strADID != null && !strADID.equals("null") && !strADID.equals("")) {
      sbDetailQuery.append(" AND   V700.AD_ID  = ");
      sbDetailQuery.append(strADID);
    }

    sbDetailQuery.append(" ORDER BY REP_NAME");
    // sbDetailQuery.append(" .TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon
    // YY'),'MON YY')");

    log.debug(sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD for the selected account
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDBySalesRepActual(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strTerritoryID = (String) hmparam.get("TerritoryID");
    String strAccessFilter = (String) hmparam.get("AccessFilter");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmparam.get("COMP_LANG_ID"));

    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");

    String strRepNmColumnVw =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "V700");

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Holds the SQL Report
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Rep monthly report
    // ***********************************************************
    sbDetailQuery.append("SELECT   T703.C703_SALES_REP_ID ID");
    sbDetailQuery.append(" ," + strRepNmColumn + " REP_NAME");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    // sbDetailQuery.append(" ,SUM(C501_TOTAL_COST) AMOUNT ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append("  FROM T501_ORDER T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 "); // Item Order Link
    sbDetailQuery.append(" ,T703_SALES_REP T703 ");
    sbDetailQuery.append(" ,T701_DISTRIBUTOR T701 ");
    sbDetailQuery.append(" ,T704_ACCOUNT T704 ");
    sbDetailQuery.append(" WHERE T703.C703_SALES_REP_ID   = T501.C703_SALES_REP_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	 T701.C701_DISTRIBUTOR_ID = T703.C701_DISTRIBUTOR_ID ");
    sbDetailQuery.append(" AND	 T704.C704_ACCOUNT_ID	  = T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(" AND	  T502.C501_ORDER_ID	   = T501.C501_ORDER_ID "); // Item
    // Order
    // Link
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL   AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");

    // To add the part number filter
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208, ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // to fetch record based on set filter code
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")
        && !strSetNumber.equals("")) {
      sbDetailQuery.append("AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery.append(" AND	 T701.C701_DISTRIBUTOR_ID = " + strDistributorID);
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }


    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    sbDetailQuery.append(" GROUP BY T703.C703_SALES_REP_ID ");
    sbDetailQuery.append(" , " + strRepNmColumn + " ");
    sbDetailQuery.append(" , TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");

    // ****************************************************
    // Below Query to fetch no sales made rep information
    // ****************************************************
    sbDetailQuery.append(" UNION ");
    sbDetailQuery.append(" SELECT  DISTINCT V700.REP_ID ID ");
    sbDetailQuery.append(" ," + strRepNmColumnVw + "  REP_NAME ");
    sbDetailQuery.append(" ,TO_CHAR(CURRENT_DATE,'Mon YY') R_DATE ");
    sbDetailQuery.append(" ,0 AMOUNT ");
    sbDetailQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
    sbDetailQuery.append(" WHERE V700.REP_ID  IN ( ");
    sbDetailQuery.append(" SELECT  C703_SALES_REP_ID  FROM T703_SALES_REP ");
    sbDetailQuery.append(" WHERE C703_SALES_FL = 'N' AND C703_ACTIVE_FL  = 'Y') ");

    // Filter Condition to fetch record based on access code
    if (!strAccessFilter.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strAccessFilter);
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery.append(" AND	 V700.D_ID = " + strDistributorID);
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	  V700.AC_ID = " + strAccountID);
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 V700.REP_ID  = " + strRepID);
    }

    // Territory Filter
    if (strTerritoryID != null && !strTerritoryID.equals("null") && !strTerritoryID.equals("")) {
      sbDetailQuery.append(" AND	 V700.TER_ID = ");
      sbDetailQuery.append(strTerritoryID);
    }

    sbDetailQuery.append(" ORDER BY REP_NAME");

    log.debug(" ********* Sales Rep Actual Query ********* " + sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Part Will have additional
   * Filter
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByPart(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    HashMap hmapFinalValue = new HashMap();
    /*
     * Access condition is removed from all Territory Reports.This method is invoking common method
     * to appending filter conditions.So, Condition is replaced as empty string in HashMap before
     * calling it.
     */
    hmparam.put("Condition", "");

    // To get the filter condition information
    String strFilterCondition = getCrossTabFilterCondition(hmparam); // Call the super class method
                                                                     // to get the filter condition

    // If no value passed unload to fecth 7 month of information
    String strWhereCondtion = getFromToDate(7);

    boolean bFilterFlag = false;

    // ***********************************************************
    // Query to Fetch the Header Info
    // ***********************************************************
    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(strWhereCondtion);
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Detail Info
    // ***********************************************************
    sbDetailQuery.append("SELECT   T205.C205_PART_NUMBER_ID ID ");
    sbDetailQuery.append(" ,T205.C205_PART_NUMBER_ID  || ' - ' || T205.C205_PART_NUM_DESC NAME ");
    sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE ");
    if (strType.equals("0")) {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");

    } else {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    }
    sbDetailQuery.append(" FROM     T501_ORDER T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 ");
    sbDetailQuery.append(" ,T205_PART_NUMBER T205 ");
    sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
    sbDetailQuery.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	 V700.AC_ID	= T501.C704_ACCOUNT_ID");
    sbDetailQuery.append(" AND	 T502.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID");
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL   AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
    sbDetailQuery.append(" AND	");
    sbDetailQuery.append(strWhereCondtion);
    sbDetailQuery.append(strFilterCondition);
    sbDetailQuery.append(" GROUP BY T205.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(" ,T205.C205_PART_NUM_DESC ");
    sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY')");
    sbDetailQuery.append(" ORDER BY T205.C205_PART_NUMBER_ID ,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug("Inside the part sql " + sbDetailQuery.toString());
    if (bFilterFlag == true) {
      log.debug("Part SQL" + sbDetailQuery.toString());
      // bFilterFlag will always be false. So moving the below code outside the loop
      // hmapFinalValue = gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(),
      // sbDetailQuery.toString());
    }
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;
  }

  /**
   * reportYTDByDetail - This Method is used to fetch the sales YTD by Part or Distributor have
   * additional Filter
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByDetail(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap();

    // To get the filter condition information
    String strFilterCondition = getCrossTabFilterCondition(hmparam); // Call the super class method
                                                                     // to get the filter condition

    // If no value passed unload to fecth 7 month of information
    String strWhereCondtion = getFromToDate(7);

    // get the SELECT attributes depending on the strAction value. strAction is set from
    // getCrossTabFilterCondition(hmparam)
    // The SELECT would be either for part or distributor
    log.debug(" Action is " + strAction);

    String strFrameColumQuery = getFrameColumn(strAction);

    String strFrameGroupQuery = getFrameGroup(strAction);


    String strFrameOrderQuery = getFrameOrder(strAction);

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    // ***********************************************************
    // Query to Fetch the Header Info
    // ***********************************************************
    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(strWhereCondtion);
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Detail Info
    // ***********************************************************
    // SELECT values depending on part or distributor
    sbDetailQuery.append(strFrameColumQuery);
    sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE ");
    if (strType.equals("0")) {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");

    } else {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    }
    sbDetailQuery.append(" FROM     T501_ORDER T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 ");
    sbDetailQuery.append(" ,T205_PART_NUMBER T205 ");
    sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700");
    sbDetailQuery.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND   V700.AC_ID = T501.C704_ACCOUNT_ID");
    sbDetailQuery.append(" AND   T502.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID");
    sbDetailQuery.append(" AND   T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
    sbDetailQuery.append(" AND   T501.C1900_COMPANY_ID =" + getCompId());
    sbDetailQuery.append(" AND  ");
    sbDetailQuery.append(strWhereCondtion);
    sbDetailQuery.append(strFilterCondition);
    if (strRegion != null && !strRegion.equals("") && !strRegion.equals("null")
        && !strRegion.equals("0")) {
      sbDetailQuery.append(" AND V700.REGION_ID = " + strRegion);
      bFilterFlag = true;
    }
    if (strState != null && !strState.equals("") && !strState.equals("null")
        && !strState.equals("0")) {
      sbDetailQuery
          .append(" AND V700.D_ID  IN ( SELECT  T701.C701_DISTRIBUTOR_ID FROM T701_DISTRIBUTOR  T701 WHERE T701.C701_BILL_STATE = '"
              + strState + "' AND T701.C701_VOID_FL IS NULL AND T701.C701_ACTIVE_FL = 'Y') ");
      bFilterFlag = true;
    }
    // Use the GroupBY and Order By query which was framed depending on the part or distributor
    sbDetailQuery.append(strFrameGroupQuery);
    sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY')");
    sbDetailQuery.append(strFrameOrderQuery);

    log.debug("Inside the reportYTDByDetail sql " + sbDetailQuery.toString());

    if (bFilterFlag == true) {
      log.debug("Part SQL Header Query" + sbHeaderQuery.toString());
      log.debug("Part SQL" + sbDetailQuery.toString());
      hmapFinalValue =
          gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    }


    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;
  }

  /**
   * reportYTDByPartActual - This Method is used to fetch the sales YTD by Part Will have additional
   * Filter
   * 
   * @param HashMap hmparam (Holds additioanl filter information)
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByPartActual(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue = new HashMap();; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossTabRep = new GmCrossTabReport();
    boolean bFilterFlag = false;

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strADID = (String) hmparam.get("ADID");
    String strTerritoryID = (String) hmparam.get("TerritoryID");
    String strSalesType = GmCommonClass.parseZero((String) hmparam.get("SalesType"));

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossTabRep.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery.append("SELECT   T205.C205_PART_NUMBER_ID DISTRIBUTOR_ID ");
    sbDetailQuery.append(" ,T205.C205_PART_NUMBER_ID  || ' - ' || T205.C205_PART_NUM_DESC NAME ");
    sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY') R_DATE ");
    if (strType.equals("0")) {
      // sbDetailQuery.append(" ,SUM(C502_ITEM_QTY) AMOUNT ");
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");

    } else {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    }
    sbDetailQuery.append(" FROM     T501_ORDER  T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 ");
    sbDetailQuery.append(" ,T205_PART_NUMBER T205 ");

    sbDetailQuery.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbDetailQuery.append(" AND	   T502.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
    sbDetailQuery.append(" AND	");
    sbDetailQuery.append(sbWhereCondtion.toString());

    // To add the part number filter
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(" T502.C205_PART_NUMBER_ID IN ");
    sbDetailQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208, ");
    sbDetailQuery.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
    sbDetailQuery.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");

    // to fetch record based on set filter code
    if (strSetNumber != null && !strSetNumber.equals("null") && !strSetNumber.equals("0")
        && !strSetNumber.equals("")) {
      sbDetailQuery.append("AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("' ");
    }
    sbDetailQuery.append(") ");

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
      bFilterFlag = true;
    }

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")
        && !strDistributorID.equals("")) {
      sbDetailQuery
          .append(" AND	T501.C703_SALES_REP_ID IN  ( SELECT C703_SALES_REP_ID FROM T703_SALES_REP  T703 ");
      sbDetailQuery.append(" WHERE T703.C701_DISTRIBUTOR_ID = ");
      sbDetailQuery.append(strDistributorID);
      sbDetailQuery.append(" )");
      bFilterFlag = true;
    }

    // Account Filter
    if (strAccountID != null && !strAccountID.equals("null") && !strAccountID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
      bFilterFlag = true;
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null") && !strRepID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
      bFilterFlag = true;
    }

    // Filter Sales Type - Sales Consignment or Loaner
    if (!strSalesType.equals("0")) {
      sbDetailQuery.append(" AND T502.C901_TYPE = ");
      sbDetailQuery.append(strSalesType);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null") && !strPartNumber.equals("")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
      bFilterFlag = true;
    }

    // Territory Information
    if (strTerritoryID != null && !strTerritoryID.equals("null") && !strTerritoryID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID IN ");
      sbDetailQuery.append(" (SELECT  AC_ID FROM V700_TERRITORY_MAPPING_DETAIL WHERE TER_ID = "
          + strTerritoryID + ")");
      bFilterFlag = true;
    }

    // Area Director filter
    if (strADID != null && !strADID.equals("null") && !strADID.equals("")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID IN ");
      sbDetailQuery.append(" (SELECT  AC_ID FROM V700_TERRITORY_MAPPING_DETAIL WHERE AD_ID = "
          + strADID + ")");
      bFilterFlag = true;
    }

    sbDetailQuery.append(" GROUP BY T205.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(" ,T205.C205_PART_NUM_DESC ");
    sbDetailQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'Mon YY')");
    sbDetailQuery.append(" ORDER BY T205.C205_PART_NUMBER_ID ,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(T501.C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug("report by Part " + sbDetailQuery.toString());

    if (bFilterFlag == true) {
      log.debug("Part SQL " + sbDetailQuery.toString());
      hmapFinalValue =
          gmCrossTabRep.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    }
    hmapFinalValue.put("FromDate", hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Distributor List Will have
   * additional Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDBySet(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear, String strCondition, String strDispID, String strType, HashMap hmparam)
      throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***********************************************************
    // Query to Fetch the Distributor month report
    // ***********************************************************
    sbDetailQuery.append("SELECT   T207.C207_SET_ID DISTRIBUTOR_ID ");
    sbDetailQuery.append(" ,T207.C207_SET_NM NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    // sbDetailQuery.append(" ,SUM(C501_TOTAL_COST) AMOUNT ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append(" FROM     T501_ORDER  T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 "); // Item Order Link
    sbDetailQuery.append(" ,T207_SET_MASTER T207 ");
    sbDetailQuery.append(" ,T208_SET_DETAILS T208 ");
    sbDetailQuery.append(" WHERE T207.C207_SET_ID = T208.C207_SET_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	T208.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(" AND	  T502.C501_ORDER_ID	   = T501.C501_ORDER_ID "); // Item
    // Order
    // Link
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL   AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
    sbDetailQuery.append(" AND   T207.C207_SKIP_RPT_FL IS NULL");
    // Filter Condition to fetch record based on access code
    if (!strDispID.toString().equals("0")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(" T501.C703_SALES_REP_ID IN ");
      sbDetailQuery
          .append(" (SELECT T703.C703_SALES_REP_ID FROM T703_SALES_REP T703 WHERE T703.C701_DISTRIBUTOR_ID = ");
      sbDetailQuery.append(strDispID);
      sbDetailQuery.append(" ) ");
    }
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }

    sbDetailQuery.append(" GROUP BY T207.C207_SET_ID,");
    sbDetailQuery.append(" T207.C207_SET_NM,");
    sbDetailQuery.append(" TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");
    sbDetailQuery.append(" ORDER BY T207.C207_SET_NM,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug(sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());

    sbDetailQuery = new StringBuffer();

    if (strDispID.toString().equals("0")) {
      strDispID = "NULL";
    }

    sbDetailQuery.append(" SELECT  C207_SET_ID Id ");
    sbDetailQuery.append(" ,C207_SET_NM Name  ");
    sbDetailQuery.append(" ,GET_DIST_SET_CONSIGN_COUNT(C207_SET_ID, ");
    sbDetailQuery.append(strDispID);
    sbDetailQuery.append(") Sets  ");
    sbDetailQuery.append(" ,GET_DIST_SET_CONSIGN_VALUE(C207_SET_ID, ");
    sbDetailQuery.append(strDispID);
    sbDetailQuery.append(") Value  ");
    sbDetailQuery.append(" FROM T207_SET_MASTER  ");
    sbDetailQuery.append(" WHERE  C207_SKIP_RPT_FL IS NULL");
    sbDetailQuery.append(" ORDER BY Name");

    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());

    hmapFinalValue = CrossTab_Rearrange(hmapFinalValue, alReturn, strType);
    // hmapFinalValue.put("Add_Details",alReturn);

    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByDetailSummary - This Method is used to fetch the sales YTD by Distributor List Will
   * have additional Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByDetailSummary(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    String strCompFilter = GmCommonClass.parseNull((String) hmparam.get("COMPID"));
    String strFilterCondition = "";
    String strWhereCondtion = "";
    HashMap hmapFinalValue; // Final value report details

    log.debug(" params are " + hmparam);

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    // To get the filter condition information
    strFilterCondition = getCrossTabFilterCondition(hmparam); // Call the super class method to get
                                                              // the filter condition
    if (strCompFilter.contains(",")) {
      strCompFilter = "";
    }

    if (strTypeConignment.equals("50401")) {
      hmparam.put("T501B_FLAG", "Y");
      strFilterCondition = getCrossTabFilterCondition(hmparam); // Call the super class method to
                                                                // get the filter condition
    }
    log.debug(" Filter condn " + strFilterCondition);

    // If no value passed unload to fecth 7 month of information
    strWhereCondtion = getFromToDate(4);

    String strFrameColumQuery = getFrameColumn(strAction);
    log.debug(" strFrameColumQuery is " + strFrameColumQuery + " strAction" + strAction);

    String strFrameGroupQuery = getFrameGroup(strAction);
    log.debug(" strFrameGroupQuery is " + strFrameGroupQuery);

    String strFrameOrderQuery = getFrameOrder(strAction);
    log.debug(" strFrameOrderQuery is " + strFrameOrderQuery);

    String strGroupOrderClause = " ID  , NAME , R_DATE";
    strGroupOrderClause =
        strAction.equals("LoadGCONS") ? strGroupOrderClause + " , C207_SEQ_NO "
            : strGroupOrderClause;

    // ***************************************************************************
    // Query to Fetch Header information
    // ***************************************************************************

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER  T501 WHERE ");
    sbHeaderQuery.append(strWhereCondtion);
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");
    // ***************************************************************************
    // Query to Fetch Detail information
    // ***************************************************************************
    // Procedure then call below one
    if (strTypeConignment.equals("50401")) {
      HashMap hmProcParam = new HashMap();
      hmProcParam.put("strFrameColumQuery", strFrameColumQuery);
      hmProcParam.put("strFilterCondition", strFilterCondition);
      hmProcParam.put("strWhereCondtion", strWhereCondtion);
      hmProcParam.put("strFrameGroupQuery", strFrameGroupQuery);
      hmProcParam.put("strGroupOrderClause", strGroupOrderClause);
      sbDetailQuery = reportYTDByDetailSummaryByProc(hmProcParam);
    } else {

      sbDetailQuery.append(" SELECT ID, NAME,  R_DATE, SUM (AMOUNT) AMOUNT ");
      sbDetailQuery.append(" FROM ( ");

      sbDetailQuery.append(strFrameColumQuery);
      sbDetailQuery.append(",TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
      log.debug(" strTypeConignment value is " + strTypeConignment);
      // Revenue
      if (strTypeConignment.equals("50400")) {
        sbDetailQuery.append("," + strItemPriceColumn + " AMOUNT ");
      }
      /*
       * // Procedure else if (strTypeConignment.equals("50401")) { // 2524 Maps to adjustment
       * sbDetailQuery.append(
       * ",SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) / DECODE(V207.C204_VALUE,0,1,V207.C204_VALUE) ) AMOUNT "
       * ); // sbDetailQuery.append(
       * ",DECODE(V207.C204_VALUE, 0, 0, (SUM(T502.C502_ITEM_QTY)/V207.C204_VALUE)) AMOUNT "); }
       */
      // Tracking Implants
      else if (strTypeConignment.equals("50402")) {
        sbDetailQuery.append(",SUM(T502.C502_ITEM_QTY) AMOUNT ");
      }
      // Units
      else if (strTypeConignment.equals("50403")) {
        sbDetailQuery.append(",SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY )) AMOUNT ");
      } else {
        sbDetailQuery.append("," + strItemPriceColumn + " AMOUNT ");
      }

      // sbDetailQuery.append(",V207.C207_SEQ_NO SNO ");
      sbDetailQuery.append("FROM ");
      sbDetailQuery.append("T501_ORDER T501 ");
      sbDetailQuery.append(",T502_ITEM_ORDER T502 ");
      //Removing the table join T2080_SET_COMPANY_MAPPING since it is slowing down the field inventory reports
      
     sbDetailQuery.append(", V207B_SET_PART_DETAILS V207 ");

      sbDetailQuery.append(" , (SELECT DISTINCT DECODE(" + strCompFilter
          + ", 100803, 100803, compid) compid, divid, ter_id  ");
      sbDetailQuery
          .append(" , ter_name, rep_name, rep_id ,rep_compid,d_compid, d_id ,d_name, vp_id, vp_name ");
      sbDetailQuery.append(" , region_id, region_name, ad_id, ad_name, gp_id, gp_name  ");
      //PC-3905 Field Inventory Report performance improvement
      sbDetailQuery.append(" FROM V703_REP_MAPPING_DETAIL) V700 ");

     sbDetailQuery.append(" WHERE  V207.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
    //  sbDetailQuery.append(" AND V207.C207_SET_ID = T2080.C207_SET_ID ");
   //   sbDetailQuery.append(" AND T2080.C1900_COMPANY_ID = V700.REP_COMPID ");
 //     sbDetailQuery.append(" AND T2080.C2080_VOID_FL IS NULL ");
      sbDetailQuery.append(getSalesFilterClause());
      sbDetailQuery.append(" AND V700.REP_ID  = T501.C703_SALES_REP_ID ");
      sbDetailQuery.append(" AND T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
      sbDetailQuery.append(" AND T501.C501_VOID_FL IS NULL  AND T502.C502_VOID_FL IS NULL ");
      sbDetailQuery.append(" AND T501.C501_DELETE_FL IS NULL ");

      // To get other filter condition
      sbDetailQuery.append(strFilterCondition);

      // To get the date Query
      sbDetailQuery.append(" AND  ");
      sbDetailQuery.append(strWhereCondtion);

      sbDetailQuery.append(strFrameGroupQuery);
   //   sbDetailQuery.append(", V207.C207_SEQ_NO, ");
      // If not revenue 50400
      if (!strTypeConignment.equals("50400")) {
     sbDetailQuery.append(" , V207.C204_VALUE ");
      }
      sbDetailQuery.append(", TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY' ");


      sbDetailQuery.append(" ) GROUP BY ");
      sbDetailQuery.append(strGroupOrderClause);
      sbDetailQuery.append(" ORDER BY ");
      sbDetailQuery.append(strGroupOrderClause);
    }
    log.debug("******* Error " + sbHeaderQuery.toString());
    log.debug(" DetailsQuery is " + sbDetailQuery.toString());

    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    // log.debug("******* FromDate " + hmapFromToDate);

    return hmapFinalValue;

  }

  public StringBuffer reportYTDByDetailSummaryByProc(HashMap hmParam) throws AppError {

    String strFilterCondition = "";
    String strWhereCondtion = "";
    String strFrameColumQuery = "";
    String strFrameGroupQuery = "";
    String strGroupOrderClause = "";
    StringBuffer sbDetailQuery = new StringBuffer();

    strFrameColumQuery = (String) hmParam.get("strFrameColumQuery");
    strFilterCondition = (String) hmParam.get("strFilterCondition");
    strWhereCondtion = (String) hmParam.get("strWhereCondtion");
    strFrameGroupQuery = (String) hmParam.get("strFrameGroupQuery");
    strGroupOrderClause = (String) hmParam.get("strGroupOrderClause");

    sbDetailQuery.append(" SELECT ID, NAME,  R_DATE, SUM (AMOUNT) AMOUNT ");
    sbDetailQuery.append(" FROM ( ");

    sbDetailQuery.append(strFrameColumQuery);
    sbDetailQuery.append(",TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbDetailQuery.append(",sum (t502.c501b_qty) AMOUNT ");
    // sbDetailQuery.append(",SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) / DECODE(V207.C204_VALUE,0,1,V207.C204_VALUE) ) AMOUNT ");

    sbDetailQuery.append("FROM ");
    sbDetailQuery.append("T501_ORDER T501 ");
    sbDetailQuery.append(",t501b_order_by_system t502 ");
    // sbDetailQuery.append(" , (SELECT DISTINCT compid, divid, ter_id ");
    sbDetailQuery.append(" , (SELECT DISTINCT DECODE(" + strCompFilter
        + ", 100803, 100803, compid) compid, divid, ter_id ");
    sbDetailQuery
        .append(" , ter_name, rep_name, rep_id ,rep_compid,d_compid, d_id ,d_name, vp_id, vp_name ");
    sbDetailQuery.append(" , region_id, region_name, ad_id, ad_name, gp_id, gp_name  ");
    sbDetailQuery.append(" FROM v700_territory_mapping_detail) V700, ");

    sbDetailQuery.append(" t207_set_master v207 ");
    // sbDetailQuery.append("WHERE  V207.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(" WHERE t502.C501_ORDER_ID = T501.C501_ORDER_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND V700.REP_ID  = T501.C703_SALES_REP_ID ");
    sbDetailQuery.append(" AND t502.c501b_void_fl IS NULL ");
    sbDetailQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbDetailQuery.append(" AND t502.c207_system_id = v207.c207_set_id ");
    log.debug("Str Type Con :" + strTypeCon);
    // To get other filter condition
    sbDetailQuery.append(strFilterCondition);

    // To get the date Query
    sbDetailQuery.append(" AND  ");
    sbDetailQuery.append(strWhereCondtion);

    sbDetailQuery.append(strFrameGroupQuery);
    // sbDetailQuery.append(", V207.C207_SEQ_NO, ");
    sbDetailQuery.append(", TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY' ");


    sbDetailQuery.append(" ) GROUP BY ");
    sbDetailQuery.append(strGroupOrderClause);
    sbDetailQuery.append(" ORDER BY ");
    sbDetailQuery.append(strGroupOrderClause);

    return sbDetailQuery;
  }


  /**
   * reportYTDByCOGS - This Method is used to fetch the sales YTD by COGS for a distributor or
   * Project Distributor List Will have additional Filter, Yet to Decide on the same
   * 
   * @param HashMap hmparam
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByCOGS(HashMap hmparam) throws AppError {
    // Method commented as discussed with Richard
    HashMap hmapFinalValue = null; // Final value report details
    /*
     * StringBuffer sbHeaderQuery = new StringBuffer(); StringBuffer sbDetailQuery = new
     * StringBuffer();
     * 
     * String strFilterCondition = ""; String strWhereCondtion = ""; String strFrameOrderQuery = "";
     * HashMap hmapFinalValue; // Final value report details
     * 
     * // To get the filter condition information strFilterCondition =
     * getCrossTabFilterCondition(hmparam); // Call the super class method to get // the filter
     * condition
     * 
     * // If no value passed unload to fecth 7 month of information strWhereCondtion =
     * getFromToDate(4);
     * 
     * String strFrameColumQuery = getFrameColumn(strAction); log.debug(" strFrameColumQuery is " +
     * strFrameColumQuery);
     * 
     * String strFrameGroupQuery = getFrameGroup(strAction); log.debug(" strFrameGroupQuery is " +
     * strFrameGroupQuery);
     * 
     * // *************************************************************************** // Query to
     * Fetch Header information //
     * ***************************************************************************
     * 
     * sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
     * sbHeaderQuery.append("FROM T501_ORDER  T501 WHERE "); sbHeaderQuery.append(strWhereCondtion);
     * sbHeaderQuery.append(getSalesFilterClause());
     * sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");
     * 
     * // *************************************************************************** // Query to
     * Fetch Detail informatio //
     * ***************************************************************************
     * 
     * sbDetailQuery.append(" SELECT ID, NAME,  R_DATE, SUM (AMOUNT) AMOUNT ");
     * sbDetailQuery.append(" FROM ( "); sbDetailQuery.append(strFrameColumQuery);
     * sbDetailQuery.append(",TO_CHAR(T502.C810_TXN_DATE,'Mon YY') R_DATE "); // 2524 Maps to
     * adjustment if (strType.toString().equals("1")) { sbDetailQuery .append(
     * ",NVL(SUM(T502.C810_QTY * DECODE (NVL(T502.C810_DR_AMT,0),0 ,(-1 * NVL(T502.C810_CR_AMT,0)),T502.C810_DR_AMT)),0)  AMOUNT "
     * ); } else { // 2524 to skip the price adjustment count sbDetailQuery .append(
     * " ,NVL(SUM(DECODE (NVL(T502.C810_DR_AMT,0),0 ,(-1 * NVL(T502.C810_QTY,0)),T502.C810_QTY)),0) AMOUNT "
     * ); } // sbDetailQuery.append(",V207.C207_SEQ_NO SNO "); sbDetailQuery.append(" FROM ");
     * sbDetailQuery.append(" T810_INVENTORY_TXN T502 ");
     * sbDetailQuery.append(" ,V207B_SET_PART_DETAILS V207 ");
     * sbDetailQuery.append(" ,V700_TERRITORY_MAPPING_DETAIL V700  ");
     * sbDetailQuery.append(" WHERE  V207.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID  ");
     * sbDetailQuery.append(" AND V700.AC_ID  = T502.C810_PARTY_ID  ");
     * sbDetailQuery.append(" AND T502.C801_ACCOUNT_ELEMENT_ID = 5 "); // Element Id 5 refers to
     * sales. sbDetailQuery.append(" AND T502.C810_TXN_DATE >= TO_DATE('");
     * sbDetailQuery.append(strFromMonth + "/" + strFromYear);
     * sbDetailQuery.append("','MM/YYYY') "); sbDetailQuery.append(" AND ");
     * sbDetailQuery.append(" T502.C810_TXN_DATE <= LAST_DAY(TO_DATE('");
     * sbDetailQuery.append(strToMonth + "/" + strToYear); sbDetailQuery.append("','MM/YYYY')) ");
     * 
     * // To get other filter condition if (strDistributorID != null &&
     * !strDistributorID.equals("")) { sbDetailQuery.append("  AND V700.D_ID = ");
     * sbDetailQuery.append(strDistributorID); } if (strADID != null && !strADID.equals("") &&
     * !strADID.equals("null") && !strADID.equals("0")) { sbDetailQuery.append(" AND V700.AD_ID = "
     * + strADID); } if (strSetNumber != null && !strSetNumber.equals("") &&
     * !strSetNumber.equals("null") && !strSetNumber.equals("0")) {
     * sbDetailQuery.append(" AND V207.C207_SET_ID = " + strSetNumber); }
     * sbDetailQuery.append(strFilterCondition); sbDetailQuery.append(strFrameGroupQuery); if
     * (strAction.equals("LoadGCONS")) { sbDetailQuery.append(" , C207_SEQ_NO "); }
     * sbDetailQuery.append(" , TO_CHAR(T502.C810_TXN_DATE,'Mon YY') ");
     * sbDetailQuery.append(" ) GROUP BY "); sbDetailQuery.append("  ID  , NAME , R_DATE ");
     * sbDetailQuery.append(" ORDER BY NAME ");
     * 
     * 
     * log.debug(" Header Query for COGS is " + sbHeaderQuery.toString());
     * log.debug(" DetailsQuery for COGS is " + sbDetailQuery.toString()); hmapFinalValue =
     * gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
     * hmapFinalValue.put("FromDate", hmapFromToDate);
     */

    // log.debug("******* FromDate " + hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Distributor List Will have
   * additional Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByGroup(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strFilterCondition = "";
    String strWhereCondtion = "";
    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    HashMap hmapFinalValue; // Final value report details
    /*
     * Access condition is removed from all Territory Reports.This method is invoking common method
     * to appending filter conditions.So, Condition is replaced as empty string in HashMap before
     * calling it.
     */
    hmparam.put("Condition", "");
    // To get the filter condition information
    strFilterCondition = getCrossTabFilterCondition(hmparam); // Call the super class method to get
                                                              // the filter condition

    // If no value passed unload to fecth 11 month of information
    strWhereCondtion = getFromToDate(11);

    // ***************************************************************************
    // Query to Fetch Header information
    // ***************************************************************************

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(strWhereCondtion);
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***************************************************************************
    // Query to Fetch Detail informatio
    // ***************************************************************************

    sbDetailQuery.append("SELECT V207B.C207_SET_ID DISTRIBUTOR_ID ");
    sbDetailQuery.append(",V207B.C207_SET_NM NAME ");
    sbDetailQuery.append(",TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");

    // Baselinecase
    if (strTypeConignment.equals("50401")) {
      sbDetailQuery
          .append(",DECODE(C204_VALUE, 0, 0, (SUM(T502.C502_ITEM_QTY)/V207B.C204_VALUE)) AMOUNT ");
    }
    // Unit Price
    else if (strType.toString().equals("1")) {
      sbDetailQuery.append("," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 Maps to adjustment
      sbDetailQuery.append(",SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append(",V207B.C207_SEQ_NO SNO ");
    sbDetailQuery.append("FROM ");
    sbDetailQuery.append("T501_ORDER T501 ");
    sbDetailQuery.append(",T502_ITEM_ORDER T502 ");
    sbDetailQuery.append(",V207B_SET_PART_DETAILS V207B, T2080_SET_COMPANY_MAPPING T2080 ");
    sbDetailQuery.append(",V700_TERRITORY_MAPPING_DETAIL V700 ");
    sbDetailQuery.append("WHERE  V207B.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(" AND V207B.C207_SET_ID = T2080.C207_SET_ID ");
    sbDetailQuery.append(" AND T2080.C1900_COMPANY_ID = V700.REP_COMPID ");
    sbDetailQuery.append(" AND T2080.C2080_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND V700.AC_ID  = T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(" AND T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
    sbDetailQuery.append(" AND T501.C501_VOID_FL IS NULL AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbDetailQuery.append(getSalesFilterClause());
    // To get other filter condition
    sbDetailQuery.append(strFilterCondition);

    // To get the date Query
    sbDetailQuery.append(" AND	");
    sbDetailQuery.append(strWhereCondtion);

    sbDetailQuery.append(" GROUP BY V207B.C207_SET_ID,");
    sbDetailQuery.append(" V207B.C207_SEQ_NO, ");
    sbDetailQuery.append(" V207B.C207_SET_NM,");
    if (strTypeConignment.equals("50401")) {
      sbDetailQuery.append(" C204_VALUE,");
    }
    sbDetailQuery.append(" TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");
    sbDetailQuery.append(" ORDER BY V207B.C207_SEQ_NO,");
    sbDetailQuery.append(" V207B.C207_SET_NM, TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug("******* Error " + sbDetailQuery.toString());
    log.debug("******* Error " + sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    log.debug("******* FromDate " + hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by Distributor List Will have
   * additional Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByGroupActual(HashMap hmparam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strProjectID = (String) hmparam.get("ProjectID");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strADID = (String) hmparam.get("ADID");
    String strTerritoryID = (String) hmparam.get("TerritoryID");
    String strGroupAccId = (String) hmparam.get("GroupAccId");

    String strCurrType = GmCommonClass.parseNull((String) hmparam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***************************************************************************
    // Query to Fetch the Account month report *** Need to work on Alias
    // Name ***
    // ***************************************************************************
    sbDetailQuery.append("SELECT   T207.C207_SET_ID DISTRIBUTOR_ID ");
    sbDetailQuery.append(" ,T207.C207_SET_NM NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    // sbDetailQuery.append(" ,SUM(C501_TOTAL_COST) AMOUNT ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ," + strItemPriceColumn + " AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append(" FROM     T501_ORDER  T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 "); // Item Order Link
    sbDetailQuery.append(" ,T207_SET_MASTER T207 ");
    sbDetailQuery.append(" ,T208_SET_DETAILS T208 ");

    sbDetailQuery.append(" WHERE T207.C207_SET_ID = T208.C207_SET_ID ");
    sbDetailQuery.append(" AND	T208.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(" AND	  T502.C501_ORDER_ID	   = T501.C501_ORDER_ID "); // Item Order Link
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL   AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
    sbDetailQuery.append(" AND   T207.C207_SKIP_RPT_FL IS NULL");
    sbDetailQuery.append(" AND   T207.C901_SET_GRP_TYPE = 1600 "); // Only filter specified report
                                                                   // group


    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")) {
      sbDetailQuery
          .append(" AND	T501.C703_SALES_REP_ID IN  ( SELECT C703_SALES_REP_ID FROM T703_SALES_REP  T703 ");
      sbDetailQuery.append(" WHERE T703.C701_DISTRIBUTOR_ID = ");
      sbDetailQuery.append(strDistributorID);
      sbDetailQuery.append(" )");
    }

    // to fecth based on account information
    if (strAccountID != null && !strAccountID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Territory Information
    if (strTerritoryID != null && !strTerritoryID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID IN ");
      sbDetailQuery.append(" (SELECT  AC_ID FROM V700_TERRITORY_MAPPING_DETAIL WHERE TER_ID = "
          + strTerritoryID + ")");
    }

    // Area Director filter
    if (strADID != null && !strADID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID IN ");
      sbDetailQuery.append(" (SELECT  AC_ID FROM V700_TERRITORY_MAPPING_DETAIL WHERE AD_ID = "
          + strADID + ")");
    }

    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("null")) {
      sbDetailQuery.append(" AND	 T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery
          .append(" ( SELECT C205_PART_NUMBER_ID FROM T207_SET_MASTER T207,  T208_SET_DETAILS T208 ");
      sbDetailQuery.append(" WHERE T207.C207_SET_ID = T208.C207_SET_ID ");
      sbDetailQuery.append(" AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("') ");
    }
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }
    if (!strGroupAccId.equals("")) {
      sbDetailQuery
          .append(" AND t501.c704_account_id IN   (SELECT c704_account_id   FROM T740_GPO_ACCOUNT_MAPPING  WHERE c101_party_id = '");
      sbDetailQuery.append(strGroupAccId);
      sbDetailQuery.append("') ");
    }
    sbDetailQuery.append(" GROUP BY T207.C207_SET_ID,");

    sbDetailQuery.append(" T207.C207_SET_NM,T207.C207_SEQ_NO,");
    sbDetailQuery.append(" TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");
    sbDetailQuery.append(" ORDER BY T207.C207_SEQ_NO,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug(" Query for report By Group " + sbDetailQuery.toString());
    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales YTD by MainGroup Will have additional
   * Filter, Yet to Decide on the same
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportYTDByMainGroup(HashMap hmparam) throws AppError {

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();

    String strFromMonth = (String) hmparam.get("FromMonth");
    String strFromYear = (String) hmparam.get("FromYear");
    String strToMonth = (String) hmparam.get("ToMonth");
    String strToYear = (String) hmparam.get("ToYear");
    String strCondition = (String) hmparam.get("Condition");
    String strProjectID = (String) hmparam.get("ProjectID");
    String strType = (String) hmparam.get("Type");
    String strSetNumber = (String) hmparam.get("SetNumber");
    String strDistributorID = (String) hmparam.get("DistributorID");
    String strAccountID = (String) hmparam.get("AccountID");
    String strRepID = (String) hmparam.get("RepID");
    String strPartNumber = (String) hmparam.get("PartNumber");
    String strADID = (String) hmparam.get("ADID");
    String strTerritoryID = (String) hmparam.get("TerritoryID");



    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {
      // Below if is added for cookie because the local cookie refer the null but other environment
      // refer the empty string.
      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }

    // If the From and To Dates values are not passed then system will
    // perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -11)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    sbHeaderQuery.append("SELECT DISTINCT TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append("FROM T501_ORDER  T501 WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(getSalesFilterClause());
    sbHeaderQuery.append("ORDER BY TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    // ***************************************************************************
    // Query to Fetch the Account month report *** Need to work on Alias
    // Name ***
    // ***************************************************************************
    sbDetailQuery.append("SELECT   T207M.C207_SET_ID DISTRIBUTOR_ID ");
    sbDetailQuery.append(" ,T207M.C207_SET_NM NAME ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'Mon YY') R_DATE ");
    // sbDetailQuery.append(" ,SUM(C501_TOTAL_COST) AMOUNT ");
    // To get the amount or unit based on user selection
    if (strType.toString().equals("1")) {
      sbDetailQuery.append(" ,SUM(T502.C502_ITEM_PRICE * T502.C502_ITEM_QTY) AMOUNT ");
    } else {
      // 2524 to skip the price adjustment count
      sbDetailQuery.append(" ,SUM(DECODE(C901_ORDER_TYPE, 2524, 0, T502.C502_ITEM_QTY ) ) AMOUNT ");
    }

    sbDetailQuery.append(" FROM     T501_ORDER  T501 ");
    sbDetailQuery.append(" ,T502_ITEM_ORDER T502 "); // Item Order Link
    // sbDetailQuery.append(" ,T207_SET_MASTER T207 ");
    sbDetailQuery.append(" ,T208_SET_DETAILS T208 ");
    sbDetailQuery.append(" , T207A_SET_LINK T207A, T207_SET_MASTER T207M");

    // sbDetailQuery.append(" WHERE T207.C207_SET_ID = T208.C207_SET_ID ");
    sbDetailQuery.append(" WHERE	T208.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" AND	  T502.C501_ORDER_ID	   = T501.C501_ORDER_ID "); // Item
    // Order
    // Link

    sbDetailQuery.append(" AND	 T501.C501_VOID_FL IS NULL   AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T501.C501_DELETE_FL IS NULL");
    // sbDetailQuery.append(" AND T207.C207_SKIP_RPT_FL IS NULL");
    // sbDetailQuery.append(" AND T207.C901_SET_GRP_TYPE = 1600 " ); // Only
    // filter specified report group
    sbDetailQuery.append(" AND  T207A.C207_LINK_SET_ID =   T208.C207_SET_ID");
    sbDetailQuery.append(" AND  T207A.C901_TYPE = 20001 ");
    sbDetailQuery.append(" AND  T207M.C207_SET_ID = T207A.C207_MAIN_SET_ID ");

    // to fetch based on distributor rep filter
    if (strDistributorID != null && !strDistributorID.equals("null")) {
      sbDetailQuery
          .append(" AND	T501.C703_SALES_REP_ID IN  ( SELECT C703_SALES_REP_ID FROM T703_SALES_REP  T703 ");
      sbDetailQuery.append(" WHERE T703.C701_DISTRIBUTOR_ID = ");
      sbDetailQuery.append(strDistributorID);
      sbDetailQuery.append(" )");
    }

    // to fecth based on account information
    if (strAccountID != null && !strAccountID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID = " + strAccountID);
    }

    // To fetch based on sales rep information
    if (strRepID != null && !strRepID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C703_SALES_REP_ID = " + strRepID);
    }

    // Part number filter
    if (strPartNumber != null && !strPartNumber.equals("null")) {
      sbDetailQuery.append(" AND	T502.C205_PART_NUMBER_ID = '" + strPartNumber + "' ");
    }

    // Territory Information
    if (strTerritoryID != null && !strTerritoryID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID IN ");
      sbDetailQuery.append(" (SELECT  AC_ID FROM V700_TERRITORY_MAPPING_DETAIL WHERE TER_ID = "
          + strTerritoryID + ")");
    }

    // Area Director filter
    if (strADID != null && !strADID.equals("null")) {
      sbDetailQuery.append(" AND	 T501.C704_ACCOUNT_ID IN ");
      sbDetailQuery.append(" (SELECT  AC_ID FROM V700_TERRITORY_MAPPING_DETAIL WHERE AD_ID = "
          + strADID + ")");
    }

    // To filter the information based on the set number
    if (strSetNumber != null && !strSetNumber.equals("null")) {
      sbDetailQuery.append(" AND	 T502.C205_PART_NUMBER_ID IN ");
      sbDetailQuery
          .append(" ( SELECT C205_PART_NUMBER_ID FROM T207_SET_MASTER T207,  T208_SET_DETAILS T208 ");
      sbDetailQuery.append(" WHERE T207.C207_SET_ID = T208.C207_SET_ID ");
      sbDetailQuery.append(" AND T208.C207_SET_ID = '");
      sbDetailQuery.append(strSetNumber);
      sbDetailQuery.append("') ");
    }
    if (!sbWhereCondtion.toString().equals("")) {
      sbDetailQuery.append(" AND	");
      sbDetailQuery.append(sbWhereCondtion.toString());
    }
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }
    sbDetailQuery.append(" GROUP BY T207M.C207_SET_ID,");

    sbDetailQuery.append(" T207M.C207_SET_NM,");
    sbDetailQuery.append(" TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY'");
    sbDetailQuery.append(" ORDER BY T207M.C207_SET_NM,");
    sbDetailQuery.append(" TO_DATE(TO_CHAR(C501_ORDER_DATE,'Mon YY'),'MON YY')");

    log.debug("reportYTDByMainGroup ****** " + sbDetailQuery.toString());

    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    hmapFinalValue.put("FromDate", hmapFromToDate);

    return hmapFinalValue;

  }

  // To rearrange Consignment Value
  /**
   * CrossTab_Rearrange - This Method is used to rearrange crosstab value and to append consignment
   * value
   * 
   * @param HashMap hmapCrossTabValue Holds the Cross Tab Header, Details and Footer value
   * @param ArrayList alConsignmentValue Holds the new fetched consignment value
   * @param String strType Holds the report type (1 -- Amount 0 -- Qty) If Qty Skip the calculation
   * 
   * @return HashMap
   * @exception AppError
   */
  private HashMap CrossTab_Rearrange(HashMap hmapCrossTabValue, ArrayList alConsignmentValue,
      String strType) {

    ArrayList alHeader = new ArrayList(1); // Contains the header
                                           // information
    ArrayList alFinalResult = new ArrayList(1); // Contains the Final Cross
                                                // Tab Value
    HashMap hmapFooterValue = new HashMap(); // Contains header section
                                             // information
    HashMap hmapFinalValue = new HashMap(); // Contains the final altered
                                            // value

    ArrayList alUpdatedList = new ArrayList(1); // Contains the Final Cross
                                                // Tab Value

    HashMap hmapValue = new HashMap();
    HashMap hmapValue1 = new HashMap();
    HashMap hmapUpdateValue = new HashMap();

    String strSetName = "";
    String strCrossTabName = "";
    String strTemp = "";

    double dblTotalSales = 0;
    double dblSetvalue = 0;

    double dblFinalSales = 0;
    double dblFinalSetvalue = 0;
    double dblFinalSetCount = 0;

    // Final Value to be passed to the report
    alHeader = (ArrayList) hmapCrossTabValue.get("Header");
    alFinalResult = (ArrayList) hmapCrossTabValue.get("Details");
    hmapFooterValue = (HashMap) hmapCrossTabValue.get("Footer");

    /***********************************************************************
     * To rearrange header with Set Information
     **********************************************************************/
    alHeader.add("Sets");
    alHeader.add("Value");
    alHeader.add("Turns");

    /***********************************************************************
     * To rearrange detail Column information
     **********************************************************************/
    int Detailsize = alConsignmentValue.size();
    int FinalIntCount = alFinalResult.size();

    int FinalResult = 0;

    for (int i = 0; i < Detailsize; i++) {
      // Set Distributed
      hmapValue = (HashMap) alConsignmentValue.get(i);
      strSetName = (String) hmapValue.get("NAME");
      strTemp = (String) hmapValue.get("VALUE");
      dblSetvalue = Double.parseDouble(strTemp);
      dblFinalSetvalue = dblFinalSetvalue + dblSetvalue;

      // Set Sales Information
      if (FinalResult < FinalIntCount) {
        hmapValue1 = (HashMap) alFinalResult.get(FinalResult);
        strCrossTabName = (String) hmapValue1.get("Name");
        strTemp = (String) hmapValue1.get("Total");
        dblTotalSales = Double.parseDouble(strTemp);
        dblFinalSales = dblFinalSales + dblTotalSales;
      }

      // If the Set Name matches
      if (strSetName.equals(strCrossTabName)) {
        hmapUpdateValue = hmapValue1;
        hmapUpdateValue.put("Sets", hmapValue.get("SETS"));
        hmapUpdateValue.put("Value", hmapValue.get("VALUE"));

        dblFinalSetCount = dblFinalSetCount + Double.parseDouble((String) hmapValue.get("SETS"));
        strTemp = "0";
        if (dblSetvalue != 0) {
          strTemp = String.valueOf(dblTotalSales / dblSetvalue);
        }

        // Only if the report is by Amount then
        // calculate the turns else skip the calculation
        if (strType.toString().equals("1")) {
          hmapUpdateValue.put("Turns", strTemp);
        } else {
          hmapUpdateValue.put("Turns", "0");
        }
        alUpdatedList.add(hmapUpdateValue);
        FinalResult++;
      } else if (dblSetvalue > 0) {
        hmapUpdateValue = new HashMap();
        hmapUpdateValue.put("Name", strSetName);
        hmapUpdateValue.put("Sets", hmapValue.get("SETS"));
        hmapUpdateValue.put("Value", hmapValue.get("VALUE"));
        hmapUpdateValue.put("Turns", "0");
        alUpdatedList.add(hmapUpdateValue);

        dblFinalSetCount = dblFinalSetCount + Double.parseDouble((String) hmapValue.get("SETS"));
      }
    }
    /***********************************************************************
     * To rearrange detail footer Information
     **********************************************************************/
    hmapFooterValue.put("Sets", String.valueOf(dblFinalSetCount));
    hmapFooterValue.put("Value", String.valueOf(dblFinalSetvalue));
    // Only if the report is by Amount then
    // calculate the turns else skip the calculation
    if (strType.toString().equals("1")) {
      hmapFooterValue.put("Turns", String.valueOf(dblFinalSales / dblFinalSetvalue));
    } else {
      hmapFooterValue.put("Turns", "0");
    }

    // Final Value to be passed to the report
    hmapFinalValue.put("Header", alHeader);
    hmapFinalValue.put("Details", alUpdatedList);
    hmapFinalValue.put("Footer", hmapFooterValue);

    return hmapFinalValue;

  }


}
