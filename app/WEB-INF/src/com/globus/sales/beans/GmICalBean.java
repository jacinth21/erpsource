package com.globus.sales.beans;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.Action;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Status;
import net.fortuna.ical4j.model.property.Categories;
import net.fortuna.ical4j.util.UidGenerator;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.sales.CaseManagement.forms.GmICalForm;
public class GmICalBean {
	/**
	 * This method used to creatING ICal Event based on the in parameter send by the user
	 * 
	 * @param String
	 *            strUserName - USERNAME
	 * @param ArrayList[]
	 *            ArrayList - EVENT DETAILS
	 
	 * @exception AppError
	 * @return Boolean
	 * @throws IOException 
	 */
	public static boolean createICalEvent(String strUserName, ArrayList alEvent)  throws AppError, IOException {
			boolean boolReturn = false;
			List events = new ArrayList();
			//Create iCalendar message
			StringBuffer messageText = new StringBuffer();
			
			for (Iterator i = alEvent.iterator(); i.hasNext();) {
				 GmICalForm gmICalForm = (GmICalForm) i.next();
			
				// Create a TimeZone
				TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
				TimeZone timezone = registry.getTimeZone("America/New_York");
				VTimeZone tz = timezone.getVTimeZone();
				
				java.util.Calendar calendar = java.util.Calendar.getInstance();
				calendar.set(java.util.Calendar.MONTH, gmICalForm.getStartmonth()-1);
				calendar.set(java.util.Calendar.DAY_OF_MONTH, gmICalForm.getStartday());
				calendar.set(java.util.Calendar.YEAR, gmICalForm.getStartyear());
				calendar.set(java.util.Calendar.HOUR_OF_DAY, gmICalForm.getStartHR());
				calendar.set(java.util.Calendar.MINUTE, gmICalForm.getStartMIN());
				
				net.fortuna.ical4j.model.DateTime start = new net.fortuna.ical4j.model.DateTime(calendar.getTime());
				
				calendar = java.util.Calendar.getInstance();
				calendar.set(java.util.Calendar.MONTH, gmICalForm.getEndmonth()-1);
				calendar.set(java.util.Calendar.DAY_OF_MONTH, gmICalForm.getEndday());
				calendar.set(java.util.Calendar.YEAR, gmICalForm.getEndyear());
				calendar.set(java.util.Calendar.HOUR_OF_DAY, gmICalForm.getEndHR());
				calendar.set(java.util.Calendar.MINUTE, gmICalForm.getEndMIN());
				
				net.fortuna.ical4j.model.DateTime end = new net.fortuna.ical4j.model.DateTime(calendar.getTime());
	
				// initialise as  event..
				VEvent meeting  = new VEvent(start, end, gmICalForm.getSubject());
				
				// add timezone info..
				meeting.getProperties().add(tz.getTimeZoneId());
	
				// Generate a UID for the event..
				UidGenerator ug = new UidGenerator("uidGen");
				meeting.getProperties().add(ug.generateUid());
				meeting.getProperties().add(new Description(gmICalForm.getDesc()));
				meeting.getProperties().add(new Status("CONFIRMED"));
				meeting.getProperties().add(new Action("DISPLAY"));
				meeting.getProperties().add(new Categories(gmICalForm.getCategories()));
				events.add(meeting);
			}

			net.fortuna.ical4j.model.Calendar cal = new net.fortuna.ical4j.model.Calendar();
			cal.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
			cal.getProperties().add(net.fortuna.ical4j.model.property.Version.VERSION_2_0);
			//Organizer o = new Organizer(URI.create("mailto:someone@example.com"));
			//o.getParameters().add(new Cn("John Doe"));
			cal.getProperties().add(new net.fortuna.ical4j.model.property.Method("REQUEST"));
			cal.getProperties().add(CalScale.GREGORIAN);


			cal.getComponents().addAll(events);
			messageText.append(cal.toString());

			File f = new File(GmCommonClass.getString("ICALDIR")); 
			if(f.exists()==false){ 
				f.mkdir(); 
		    } 

			FileWriter fstream = new FileWriter(GmCommonClass.getString("ICALDIR")+strUserName+".ics");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(messageText.toString());
			out.close();

			boolReturn = true;
			return boolReturn;
	} // End of createICalEvent		
}
