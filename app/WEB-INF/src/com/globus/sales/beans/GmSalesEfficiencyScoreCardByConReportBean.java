
package com.globus.sales.beans;

import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * GmSalesEfficiencyScoreCardByConReportBean - Contains the methods used for fetch the Scorecard - Exp. Consignment usage report screen
 * @author rajan
 *
 */
public class GmSalesEfficiencyScoreCardByConReportBean extends GmSalesFilterConditionBean { 

/**
 * Code to Initialize the Logger Class.
 */
  Logger log = GmLogger.getInstance(this.getClass().getName());
  
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesEfficiencyScoreCardByConReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * fetchConUsageScoreCardDtls - used to fetch the  Scorecard - Exp. Consignment usage details 
   * @return
   * @throws AppError
   */
  public String fetchConUsageScoreCardDtls(HashMap hmParam) throws AppError {
	    initializeParameters(hmParam); 
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strDateFmt = GmCommonClass.parseZero((String) hmParam.get("APPLNDATEFMT"));
	    
	    // to get the drilldown values
	    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
	    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
	    String strADId = GmCommonClass.parseNull((String) hmParam.get("ADID")); 
	    
	    StringBuffer sbQuery = new StringBuffer();
	    String queryJSONRecord = "";
	    
	    sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT ('ad_name' value ad_name, 'dist_name' value D_NAME, 'tag_id' value ");
	    sbQuery.append(" tag_id, 'missing' value '', 'system_name' value CRITICAL_SYSTEM_NAME, 'system_type_name' ");
	    sbQuery.append(" value CRITICAL_SYSTEM_TYPE_NAME, 'set_id' value SET_ID, 'set_name' value ");
	    sbQuery.append(" SET_NAME, 'cn_id' value CONSIGNMENT_ID, 'placement_date' value PLACEMENT_DATE, 'return_date' value RETURN_DATE, ");
	    sbQuery.append(" 'adj_placement_date' value ADJUSTED_PLACEMENT_DATE, 'adj_return_date' value ");
	    sbQuery.append("  ADJUSTED_RETURN_DATE, 'days_kept' value DAYS_KEPT, ");
	    sbQuery.append(" 'last_2_d_kept' value LAST2MON_DAYS_KEPT, 'proj_d_kept' value PROJ_DAYS_KEPT, 'cn_cmpl' ");
	    sbQuery.append(" value cn_cmpl, 'last_2_cn_cmpl' value LAST2MON_EXP_USAGE, 'cn_tot_proj' value TOT_PROJ_EXP_USAGE ");
	    sbQuery.append(" ) order by ad_name RETURNING CLOB) FROM ( ");
	    
	    sbQuery.append(" SELECT v700.ad_name, v700.D_NAME, pbi_cn_usage.tag_id ");
	    sbQuery.append(" , pbi_cn_usage.CRITICAL_SYSTEM_NAME, pbi_cn_usage.CRITICAL_SYSTEM_TYPE_NAME, pbi_cn_usage.SET_ID ");
	    sbQuery.append(" , pbi_cn_usage.SET_NAME, pbi_cn_usage.CONSIGNMENT_ID, TO_CHAR (pbi_cn_usage.PLACEMENT_DATE, '"+strDateFmt+"') PLACEMENT_DATE ");
	    sbQuery.append(", TO_CHAR (pbi_cn_usage.RETURN_DATE, '"+strDateFmt+"') RETURN_DATE, TO_CHAR (pbi_cn_usage.ADJUSTED_PLACEMENT_DATE,'"+strDateFmt+"') ADJUSTED_PLACEMENT_DATE, ");
	    sbQuery.append(" TO_CHAR (pbi_cn_usage.ADJUSTED_RETURN_DATE,'"+strDateFmt+"') ADJUSTED_RETURN_DATE, pbi_cn_usage.DAYS_KEPT ");
	    //
	    sbQuery.append(" , pbi_cn_usage.LAST2MON_DAYS_KEPT LAST2MON_DAYS_KEPT, pbi_cn_usage.PROJ_DAYS_KEPT PROJ_DAYS_KEPT ");
	    sbQuery.append(" , pbi_cn_usage.TURN_RATE cn_cmpl, pbi_cn_usage.LAST2MON_EXP_USAGE LAST2MON_EXP_USAGE ");
	    sbQuery.append(" , pbi_cn_usage.TOT_PROJ_EXP_USAGE  TOT_PROJ_EXP_USAGE ");
	    sbQuery.append(" FROM pbi_fam_consignment_usage_fact pbi_cn_usage ");
	    sbQuery.append( getAccessFilterClause() );  
	    sbQuery.append(" WHERE pbi_cn_usage.DISTRIBUTOR_ID = v700.d_id " );
		
	    // to exclude the On board distributors
	    sbQuery.append(" AND v700.VP_ID <> 4271942 ");
		
	    // to apply the drill down filter condition
	    if(!strSystemId.equals("")){
	    	sbQuery.append(" AND pbi_cn_usage.critical_system_id = '"+ strSystemId +"' ");
	    }
	    
	    if(!strDistId.equals("")){
	    	sbQuery.append(" AND pbi_cn_usage.distributor_id = "+ strDistId);
	    }
	    
	    if(!strADId.equals("")){
	    	sbQuery.append(" AND v700.ad_id = "+ strADId);
	    }
	    
	    sbQuery.append(" group by v700.ad_name, v700.D_NAME, pbi_cn_usage.tag_id ");
	    sbQuery.append(" , pbi_cn_usage.CRITICAL_SYSTEM_NAME, pbi_cn_usage.CRITICAL_SYSTEM_TYPE_NAME, pbi_cn_usage.SET_ID ");
	    sbQuery.append(" , pbi_cn_usage.SET_NAME, pbi_cn_usage.CONSIGNMENT_ID, pbi_cn_usage.PLACEMENT_DATE ");
	    sbQuery.append(" , pbi_cn_usage.RETURN_DATE, pbi_cn_usage.ADJUSTED_PLACEMENT_DATE, pbi_cn_usage.ADJUSTED_RETURN_DATE , pbi_cn_usage.DAYS_KEPT  ");
	    sbQuery.append(" , pbi_cn_usage.LAST2MON_DAYS_KEPT, pbi_cn_usage.PROJ_DAYS_KEPT ");
	    sbQuery.append(" , pbi_cn_usage.TURN_RATE, pbi_cn_usage.LAST2MON_EXP_USAGE ");
	    sbQuery.append(" , pbi_cn_usage.TOT_PROJ_EXP_USAGE ) ");
	    
	    log.debug("fetchConUsageScoreCardDtls  Query  " + sbQuery.toString());
	  
	    queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));
	    gmDBManager.close();
	    return queryJSONRecord;
   }
  }
