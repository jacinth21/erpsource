package com.globus.sales.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmSalesEfficiencyScoreCardByActualProcReportBean - Contains the methods used for fetch the Scorecard - Actual Procedure Report screen
 * @author Mkosalram
 *
 */

public class GmSalesEfficiencyScoreCardByActualProcReportBean extends GmSalesFilterConditionBean{


	/**
	 * Code to Initialize the Logger Class.
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Default GmSalesEfficiencyScoreCardByActualProcReportBean constructor
	 */
	public GmSalesEfficiencyScoreCardByActualProcReportBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}


	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmSalesEfficiencyScoreCardByActualProcReportBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}


	/**
	 * fetchActualProcedureScoreCardDtls - used to fetch the  Scorecard - Actual Procedure Report details 
	 * @return
	 * @throws AppError
	 */
	public String fetchActualProcUsageScoreCardDtls(HashMap hmParam) throws AppError {

		initializeParameters(hmParam); 
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());

		// to get the drilldown values
		String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
		String strADId = GmCommonClass.parseNull((String) hmParam.get("ADID"));
		String strAllowedTag = GmCommonClass.parseZero((String) hmParam.get("ALLOWEDTAG"));
		
		log.debug("strAllowedTag:" + strAllowedTag);

		StringBuffer sbQuery = new StringBuffer();
		String queryJSONRecord = "";

		// Added allowed tag field to the query according to PC-5101: Scorecard DO classification changes

		sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT ('ad_name' value ad_name, 'system_name' value CRITICAL_SYSTEM_NAME, 'tag_id' value tag_id,");
		sbQuery.append(" 'set_id' value set_id, 'set_name' value SET_NAME, 'order_id' value order_id, 'order_date' value order_date,  ");
		sbQuery.append(" 'order_owner' value order_owner, 'tag_owner' value tag_owner, 'invalid_share' value INVALD_SHARE, 'comments' value comments, ");

		sbQuery.append(" 'credit_owner' value PROCEDURE_CREDIT_OWNER, 'type' value type, 'proc_cmpl' value PROCEDURE_name, 'proc_last_2mnth' value LAST2MON_PROCEDURE, ");
		sbQuery.append(" 'proc_mtd_proj' value PROJ_PROCEDURE, 'proc_tot_proj' value Procedure_Projected, 'allowed_tag' value ALLOWED_TAG) ORDER BY ad_name RETURNING CLOB) ");
		sbQuery.append(" FROM ( ");

		sbQuery.append("SELECT v700.ad_name, pbi.CRITICAL_SYSTEM_NAME, pbi.tag_id ");
		sbQuery.append(", pbi.set_id, pbi.SET_NAME, pbi.order_id ");
		sbQuery.append(", TO_CHAR (pbi.order_date,'"+strCompDateFmt +"') order_date, pbi.order_owner, pbi.tag_owner ");
		sbQuery.append(", pbi.INVALD_SHARE, pbi.comments, v700.d_name PROCEDURE_CREDIT_OWNER, decode(pbi.order_owner, v700.d_name, 'Used', 'Shared')type ");
		//
		sbQuery.append(",sum(pbi.PROCEDURE) PROCEDURE_name , sum(pbi.LAST2MON_PROCEDURE) LAST2MON_PROCEDURE, sum(pbi.PROJ_PROCEDURE) PROJ_PROCEDURE, ");
		// PC-3528: Scorecard projection values changes (to fix the drill down issue)
		sbQuery.append(" sum ( NVL(pbi.LAST2MON_PROCEDURE,0) + ((nvl(PROJ_PROCEDURE,0)) / (COMPLETED_BUS_DAYS) * (CUR_MONTH_NO_OF_BUS_DAYS))) Procedure_Projected, pbi.ALLOWED_TAG ALLOWED_TAG ");
		sbQuery.append("FROM PBI_FAM_ORDER_TAG_USAGE_FACT pbi, pbi_fam_critical_system_dim pbi_sys_dim ");

		sbQuery.append(  getAccessFilterClause() );
		sbQuery.append(" WHERE pbi.DISTRIBUTOR_ID = v700.d_id ");

		// to include only critical system
		// PC-3676: Bulk DO changes (to check set id to system id)
		sbQuery.append(" AND pbi_sys_dim.critical_system_id  = pbi.critical_system_id ");
		sbQuery.append(" AND (pbi.order_category_id IS NULL OR pbi.order_category_id = 111160) ");

		// to exclude the On board distributors
		sbQuery.append(" AND v700.VP_ID <> 4271942 ");

		// to apply the drill down filter condition
		if(!strSystemId.equals("")){
			sbQuery.append(" AND pbi.critical_system_id = '"+ strSystemId +"' ");
		}

		if(!strDistId.equals("")){
			sbQuery.append(" AND pbi.distributor_id = "+ strDistId);
		}

		if(!strADId.equals("")){
			sbQuery.append(" AND v700.ad_id = "+ strADId);
		}

		if(!strAllowedTag.equals("0")){
			sbQuery.append(" AND pbi.ALLOWED_TAG = '"+ strAllowedTag + "'");
		}

		sbQuery.append(" GROUP BY v700.ad_name, pbi.CRITICAL_SYSTEM_NAME, pbi.tag_id ");
		sbQuery.append(" , pbi.set_id, pbi.SET_NAME, pbi.order_id ");
		sbQuery.append(" , pbi.order_date, pbi.order_owner, pbi.tag_owner ");
		sbQuery.append(" , pbi.INVALD_SHARE, pbi.comments,  v700.d_name, pbi.ALLOWED_TAG) ");

		if(!strAllowedTag.equals("0")){
			log.debug("Applying Allowed tag: "+strAllowedTag);
		}
		log.debug("fetchActualProcedureScoreCardDtls  Query  " + sbQuery.toString());

		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));

		gmDBManager.close();
		return queryJSONRecord;
	}

	/**
	 * fetchBorrowTagScoreCardDtls - used to fetch the  Scorecard - Borrow Tag details 
	 * @return
	 * @throws AppError
	 */
	public String fetchBorrowTagScoreCardDtls(HashMap hmParam) throws AppError {

		initializeParameters(hmParam); 
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());

		log.debug(GmCommonClass.ADD_BUTTON);

		// to get the drilldown values
		String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
		String strADId = GmCommonClass.parseNull((String) hmParam.get("ADID"));

		// Added allowed tag field to the query according to PC-5101: Scorecard DO classification changes
		String strAllowedTag = GmCommonClass.parseZero((String) hmParam.get("ALLOWEDTAG"));

		StringBuffer sbQuery = new StringBuffer();
		String queryJSONRecord = "";

		// Added allowed tag field to the query according to PC-5101: Scorecard DO classification changes

		sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT ( 'ad_name' value ad_name, 'system_name' value CRITICAL_SYSTEM_NAME, 'tag_id' value tag_id ");
		sbQuery.append(" , 'set_id' value set_id, 'set_name' value SET_NAME, 'order_id' value order_id  ");
		sbQuery.append(" , 'order_date' value order_date, 'order_owner' value order_owner, 'tag_owner' value tag_owner, 'allowed_tag' value allowed_tag ) ORDER BY ad_name RETURNING CLOB) ");

		sbQuery.append(" FROM ( SELECT v700.ad_name, pbi.CRITICAL_SYSTEM_NAME, pbi.tag_id ");
		sbQuery.append(" , pbi.set_id, pbi.SET_NAME, pbi.order_id ");
		sbQuery.append(" , TO_CHAR (pbi.order_date,'"+strCompDateFmt +"') order_date, pbi.order_owner, pbi.tag_owner ");
		sbQuery.append(" , v700.d_name PROCEDURE_CREDIT_OWNER, pbi.allowed_tag ");
		sbQuery.append(" FROM PBI_FAM_ORDER_TAG_USAGE_FACT pbi, pbi_fam_critical_system_dim pbi_system_dim ");

		sbQuery.append(  getAccessFilterClause() );
		sbQuery.append(" WHERE pbi.ORDER_OWNER_ID = v700.d_id ");

		// to include only critical system
		// PC-3676: Bulk DO changes (to check set id to system id)
		sbQuery.append(" AND pbi_system_dim.critical_system_id  = pbi.critical_system_id ");
		sbQuery.append(" AND (pbi.order_category_id IS NULL OR pbi.order_category_id = 111160) ");

		sbQuery.append(" AND to_char(pbi.ORDER_OWNER_ID) <> to_char(pbi.TAG_OWNER_ID) ");

		// to exclude the On board distributors
		sbQuery.append(" AND v700.VP_ID <> 4271942  ");

		// to apply the drill down filter condition

		if(!strSystemId.equals("")){
			sbQuery.append(" AND pbi.critical_system_id = '"+ strSystemId +"' "); 
		}

		if(!strDistId.equals("")){ 
			sbQuery.append(" AND pbi.distributor_id = "+strDistId); 
		}

		if(!strADId.equals("")){ 
			sbQuery.append(" AND v700.ad_id = "+ strADId); 
		}

		if(!strAllowedTag.equals("0")){
			sbQuery.append(" AND pbi.ALLOWED_TAG = '"+ strAllowedTag + "'");
		}

		// duplicating the records - JSON not support the DISTINCT.
		// to adding group by clause (PBUG-4607)
		sbQuery.append(" group by v700.ad_name, pbi.CRITICAL_SYSTEM_NAME, pbi.tag_id ");
		sbQuery.append(" , pbi.set_id, pbi.SET_NAME, pbi.order_id ");
		sbQuery.append(" , TO_CHAR (pbi.order_date, 'MM/dd/yyyy'), pbi.order_owner, pbi.tag_owner ");
		sbQuery.append(" , v700.d_name, pbi.allowed_tag ");
		sbQuery.append(" ) ");      

		log.debug("fetchBorrowTagScoreCardDtls  Query  " + sbQuery.toString());

		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));

		gmDBManager.close();
		return queryJSONRecord;
	}


	/**
	 * fetchBulkDOScoreCardDtls - used to fetch the Scorecard - Bulk DO details
	 * 
	 * @return
	 * @throws AppError
	 */
	public String fetchBulkDOScoreCardDtls(HashMap hmParam) throws AppError {

		initializeParameters(hmParam);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());


		// to get the drilldown values
		String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
		String strADId = GmCommonClass.parseNull((String) hmParam.get("ADID"));

		// Added allowed tag field to the query according to PC-5101: Scorecard DO classification changes
		String strAllowedTag = GmCommonClass.parseZero((String) hmParam.get("ALLOWEDTAG"));

		StringBuffer sbQuery = new StringBuffer();
		String queryJSONRecord = "";

		sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT ( 'ad_name' value ad_name, 'system_name' value CRITICAL_SYSTEM_NAME ");
		sbQuery.append(" , 'bulk_do_mon' value bulk_do_mon, 'order_owner' value order_owner, 'tag_owner' value tag_owner, 'allowed_tag' value ALLOWED_TAG ");
		sbQuery.append(" , 'proc_name' value procedure_name, 'proc_tot_proj' value Procedure_Projected ) ");
		sbQuery.append(" ORDER BY ad_name, critical_system_name RETURNING CLOB) ");
		sbQuery.append(" FROM (  SELECT v700.ad_name, pbi.CRITICAL_SYSTEM_NAME, pbi.order_owner, pbi.tag_owner ");
		sbQuery.append(" , v700.d_name PROCEDURE_CREDIT_OWNER ");
		sbQuery.append(" , TO_CHAR (pbi.ORDER_DATE, 'Mon YY') bulk_do_mon ");
		sbQuery.append(" , SUM (pbi.PROCEDURE) procedure_name ");
		sbQuery.append(" , SUM (NVL (pbi.LAST2MON_PROCEDURE, 0) + ( (NVL (PROJ_PROCEDURE, 0)) / (COMPLETED_BUS_DAYS) * (CUR_MONTH_NO_OF_BUS_DAYS))) Procedure_Projected, pbi.ALLOWED_TAG  ");
		sbQuery.append(" FROM PBI_FAM_ORDER_TAG_USAGE_FACT pbi, pbi_fam_critical_system_dim critical_system_dim ");

		sbQuery.append(getAccessFilterClause());
		sbQuery.append(" WHERE pbi.ORDER_OWNER_ID = v700.d_id ");

		// to include only critical system
		sbQuery.append(" AND critical_system_dim.critical_system_id = pbi.critical_system_id ");

		// to exclude the On board distributors
		sbQuery.append(" AND v700.VP_ID <> 4271942  ");

		// to apply the bulk DO changes
		sbQuery.append(" AND pbi.order_category_id = 111161 ");

		// to apply the drill down filter condition

		if (!strSystemId.equals("")) {
			sbQuery.append(" AND pbi.critical_system_id = '" + strSystemId + "' ");
		}

		if (!strDistId.equals("")) {
			sbQuery.append(" AND pbi.distributor_id = " + strDistId);
		}

		if (!strADId.equals("")) {
			sbQuery.append(" AND v700.ad_id = " + strADId);
		}

		if(!strAllowedTag.equals("0")){
			sbQuery.append(" AND pbi.ALLOWED_TAG = '"+ strAllowedTag + "'");
		}

		// duplicating the records - JSON not support the DISTINCT.

		sbQuery.append(" group by v700.ad_name, pbi.CRITICAL_SYSTEM_NAME, pbi.order_owner, pbi.tag_owner ");
		sbQuery.append(" , v700.d_name ");
		sbQuery.append(" , TO_CHAR (pbi.ORDER_DATE, 'Mon YY'), pbi.ALLOWED_TAG ");
		sbQuery.append(" ) ");

		log.debug("fetchBulkDOScoreCardDtls  Query  " + sbQuery.toString());

		queryJSONRecord = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(sbQuery.toString()));

		gmDBManager.close();
		return queryJSONRecord;
	}

}
