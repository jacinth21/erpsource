/*
 * Module: GmSalesReportBean.java Author: RichardK Project: Globus Medical App Date-Written: Mar
 * 2005 Security: Unclassified Description: This beans will be used to generate all kind of sames
 * report
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.sales.beans;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmSalesReportBean extends GmSalesFilterConditionBean {


  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  // this will be removed all place changed with Data Store VO constructor

  public GmSalesReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales for the selected period for the entire
   * state
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadSalesByState(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear, String strCondition, String strCurrType) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    HashMap hmSalesValue = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Holds the SQL Report
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    if (strFromMonth == null && strFromYear == null && strToMonth == null && strToYear == null) {
      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    // ***********************************************************
    // Query to Fetch the Account month report
    // ***********************************************************
    sbDetailQuery
        .append("  SELECT sales.*,  DECODE(sales.AMT,0,0,ROUND ( (sales.AMT / SUM (sales.AMT) OVER ()) * 100, 2)) PERCENTAGE  FROM    ( ");
    sbDetailQuery.append(" SELECT " + strItemPriceColumn + " AMT, ");
    sbDetailQuery.append(" C704_BILL_STATE ID,  ");
    sbDetailQuery.append(" GET_CODE_NAME_ALT(C704_BILL_STATE) CD, ");
    sbDetailQuery.append(" GET_CODE_NAME(C704_BILL_STATE) NAME ");
    sbDetailQuery.append(" from T501_ORDER T501,  T704_ACCOUNT T704 ,  T502_ITEM_ORDER T502");
    sbDetailQuery.append(" WHERE T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID  ");
    sbDetailQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbDetailQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbDetailQuery.append(" AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND  ");
    sbDetailQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strToMonth, strToYear));
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" GROUP BY C704_BILL_STATE  ");
    sbDetailQuery.append("  ORDER BY AMT    )    sales  ");
    log.debug(" Query for sales by state is " + sbDetailQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());
    hmSalesValue.put("STATESALE", alReturn);
    hmSalesValue.put("TOPAGENT",
        loadTopNAgent(strFromMonth, strFromYear, strToMonth, strToYear, strCondition, strCurrType));
    hmSalesValue
        .put(
            "TOPACCOUNT",
            loadTopNAccount(strFromMonth, strFromYear, strToMonth, strToYear, strCondition,
                strCurrType));
    hmSalesValue.put("FromDate", hmapFromToDate);
    // Below Code has been added for getting the ADS and if is added because of cookie.
    if (strFromMonth == null || strFromYear == null) {
      hmSalesValue.put("WORKING_DAYS", fetchWorkingDays(""));
    } else {
      hmSalesValue.put("WORKING_DAYS", fetchWorkingDays(strFromMonth + "/" + strFromYear));
    }
    // hmSalesValue.put("TOPACCOUNT",load
    return hmSalesValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales for the selected month Displays
   * Monthly Dashboard
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadMonthlySales(String strToMonth, String strToYear, String strCondition,
      String strCurrType) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    HashMap hmSalesValue = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Holds the SQL Report
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    if (strToMonth == null && strToYear == null) {
      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
      strToMonth = (String) hmapFromToDate.get("TOMONTH");
      strToYear = (String) hmapFromToDate.get("TOYEAR");
    }

    hmSalesValue =
        loadSalesByState(strToMonth, strToYear, strToMonth, strToYear, strCondition, strCurrType);

    hmSalesValue.put("MONTHLYSALES",
        DetailMonthlySalesReport(strToMonth, strToYear, strCondition, strCurrType));
    hmSalesValue.put("PROJECTSALES",
        DetailMonthlyProjectReport(strToMonth, strToYear, strCondition, strCurrType));
    hmSalesValue.put("FromDate", hmapFromToDate);
    return hmSalesValue;
  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales for the selected period for the entire
   * state
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadSalesByProject(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear, String strCondition, String strCurrType) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    HashMap hmSalesValue = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmapFinalValue; // Final value report details
    HashMap hmapFromToDate = new HashMap(); // Holds the SQL Report
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    if (strFromMonth == null && strFromYear == null && strToMonth == null && strToYear == null) {
      // To Store the from and the to date
      hmapFromToDate = gmCrossReport.getFromToDate();
    }

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);


    // ***********************************************************
    // Query to Fetch the Account month report
    // ***********************************************************
    sbDetailQuery.append(" SELECT T202.C202_PROJECT_ID ID ");
    sbDetailQuery.append(" ,T202.C202_PROJECT_NM NAME  ");
    sbDetailQuery.append(" ," + strItemPriceColumn + " AMT ");
    sbDetailQuery.append(" FROM T501_ORDER T501,  T502_ITEM_ORDER T502  ");
    sbDetailQuery.append(" ,T205_PART_NUMBER T205 ,T202_PROJECT T202 ");
    sbDetailQuery.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbDetailQuery.append(" AND T502.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID ");
    sbDetailQuery.append(" AND T205.C202_PROJECT_ID = T202.C202_PROJECT_ID ");
    sbDetailQuery.append(" AND   T202.C202_PROJECT_ID <> 'QB00-00' ");
    sbDetailQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbDetailQuery.append("  AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND  ");

    sbDetailQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strToMonth, strToYear));
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" GROUP BY T202.C202_PROJECT_ID, T202.C202_PROJECT_NM  ");
    sbDetailQuery.append(" ORDER BY NAME ");

    log.debug(" Query for loadSalesByProject " + sbDetailQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());


    hmSalesValue.put("PROJECTSALES", alReturn);
    hmSalesValue.put(
        "PROJECTBYMONTH",
        loadSalesByProjectByMonth(strFromMonth, strFromYear, strToMonth, strToYear, strCondition,
            strCurrType));

    hmSalesValue.put("FromDate", hmapFromToDate);
    // hmSalesValue.put("TOPACCOUNT",load
    return hmSalesValue;

  }

  /**
   * reportYTDByDist - This Method is used to fetch the sales for the selected period for the entire
   * project and by month
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadSalesByProjectByMonth(String strFromMonth, String strFromYear,
      String strToMonth, String strToYear, String strCondition, String strCurrType) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();
    StringBuffer sbDateQuery = new StringBuffer();
    StringBuffer sbWhereCondition = new StringBuffer();

    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // ***********************************************************
    // Query to Fetch the PRODUCT IINFORMATION FOR SELECTED MONTH
    // ***********************************************************
    sbDetailQuery.append(" SELECT T202.C202_PROJECT_ID ID    ");
    sbDetailQuery.append(" ,T202.C202_PROJECT_NM NAME    ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'MON-YYYY') R_DATE ");
    sbDetailQuery.append(" ," + strItemPriceColumn + " AMT ");

    // Filter Condition used in 2 places
    sbWhereCondition.append(" FROM     T501_ORDER T501 ");
    sbWhereCondition.append("  ,T502_ITEM_ORDER T502 ");
    sbWhereCondition.append("  ,T205_PART_NUMBER T205 ");
    sbWhereCondition.append("  ,T202_PROJECT T202 ");
    sbWhereCondition.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID  ");
    sbWhereCondition.append(" AND   T502.C205_PART_NUMBER_ID  = T205.C205_PART_NUMBER_ID ");
    sbWhereCondition.append("  AND   T205.C202_PROJECT_ID = T202.C202_PROJECT_ID  ");
    sbWhereCondition.append("  AND   T202.C202_PROJECT_ID <> 'QB00-00' ");
    sbWhereCondition.append("  AND	 T501.C501_VOID_FL IS NULL ");
    sbWhereCondition.append("  AND   T501.C501_DELETE_FL IS NULL ");
    sbWhereCondition.append("  AND   T502.C502_VOID_FL IS NULL ");
    sbWhereCondition.append(getSalesFilterClause());
    sbWhereCondition.append(" AND  ");

    sbDetailQuery.append(sbWhereCondition.toString());

    sbDetailQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strToMonth, strToYear));

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }

    sbDetailQuery.append(" GROUP BY T202.C202_PROJECT_ID ");
    sbDetailQuery.append(" ,T202.C202_PROJECT_NM ");
    sbDetailQuery.append(" ,TO_CHAR(C501_ORDER_DATE,'MON-YYYY') ");
    sbDetailQuery.append(" ORDER BY NAME, TO_DATE(R_DATE,'MON-YYYY') ");

    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());
    hmReturn.put("SERIES", alReturn);

    // Query to fecth the unique date
    sbDateQuery.append("SELECT  DISTINCT TO_CHAR(C501_ORDER_DATE,'MON-YYYY') R_DATE");

    // Query to add the filter condition
    sbDateQuery.append(sbWhereCondition.toString());
    sbDateQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strToMonth, strToYear));

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDateQuery.append(" AND ");
      sbDateQuery.append(strCondition);
    }
    sbDateQuery.append(" GROUP BY TO_CHAR(C501_ORDER_DATE,'MON-YYYY')");
    sbDateQuery.append(" ORDER BY  TO_DATE(R_DATE,'MON-YYYY') ");

    alReturn = gmDBManager.queryMultipleRecords(sbDateQuery.toString());
    hmReturn.put("DATELIST", alReturn);

    return hmReturn; // alReturn;
  }


  /**
   * reportYTDByDist - This Method is used to fetch the sales for the selected period for the entire
   * state
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList loadTopNAgent(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear, String strCondition, String strCurrType) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    // ***********************************************************
    // Query to Fetch the Account month report
    // ***********************************************************
    sbDetailQuery.append(" SELECT * FROM ( SELECT TEMP.AMT, TEMP.ID,   ");
    sbDetailQuery.append(" GET_CODE_NAME_ALT(T701.C701_BILL_STATE) || ', ' ||  ");
    sbDetailQuery.append("nvl(decode('"+getComplangid()+"',103097,T701.c701_distributor_name_en), nvl(T701.c701_dist_sh_name,T701.c701_distributor_name)) NAME");
      
    sbDetailQuery.append(" FROM  ");
    sbDetailQuery.append(" ( SELECT " + strItemPriceColumn + " AMT, ");
    sbDetailQuery.append(" 	T701.C701_DISTRIBUTOR_ID  ID   ");
    sbDetailQuery.append(" FROM	   T501_ORDER T501,   ");
    sbDetailQuery.append(" T703_SALES_REP T703,  ");
    sbDetailQuery.append(" T701_DISTRIBUTOR T701, T502_ITEM_ORDER T502   ");
    sbDetailQuery.append(" WHERE T501.C703_SALES_REP_ID  	= T703.C703_SALES_REP_ID ");
    sbDetailQuery.append(" AND	T703.C701_DISTRIBUTOR_ID = T701.C701_DISTRIBUTOR_ID	 ");
    sbDetailQuery.append(" AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbDetailQuery.append(" AND T501.C501_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbDetailQuery.append(" AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND  ");
    sbDetailQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strToMonth, strToYear));

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" GROUP BY T701.C701_DISTRIBUTOR_ID ");
    sbDetailQuery.append(" ORDER BY AMT DESC ) TEMP,  ");
    sbDetailQuery.append(" T701_DISTRIBUTOR T701  ");
    sbDetailQuery.append(" WHERE T701.C701_DISTRIBUTOR_ID = TEMP.ID ");
    sbDetailQuery.append(" ORDER BY AMT DESC) WHERE ROWNUM <= 5 ");
    
    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());

    return alReturn;
  }

  /**
   * reportYTDByDist - This Method is used to fetch Top N Account for the selected period
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList loadTopNAccount(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear, String strCondition, String strCurrType) throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    // ***********************************************************
    // Query to Fetch the Account month report
    // ***********************************************************
    sbDetailQuery.append(" SELECT * FROM ( SELECT TEMP.AMT, TEMP.ID,  ");
    sbDetailQuery.append(" GET_CODE_NAME_ALT(T704.C704_BILL_STATE) || ', ' ||  ");
    sbDetailQuery.append("nvl(decode('"+getComplangid()+"',103097,T704.c704_account_nm_en), nvl(T704.c704_account_sh_name,T704.c704_account_nm)) NAME");
    
    sbDetailQuery.append(" FROM  ");
    sbDetailQuery.append(" ( SELECT " + strItemPriceColumn + " AMT, ");
    sbDetailQuery.append(" T501.C704_ACCOUNT_ID ID   ");
    sbDetailQuery.append(" FROM 	   T501_ORDER T501  , T502_ITEM_ORDER T502 ");
    sbDetailQuery.append(" WHERE T501.C501_VOID_FL IS NULL ");
    sbDetailQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbDetailQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
    sbDetailQuery.append(" AND T502.C502_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND ");
    sbDetailQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strToMonth, strToYear));
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbDetailQuery.append(" AND ");
      sbDetailQuery.append(strCondition);
    }
    sbDetailQuery.append(getSalesFilterClause());
    sbDetailQuery.append(" GROUP BY T501.C704_ACCOUNT_ID ");
    sbDetailQuery.append(" ORDER BY AMT DESC ) TEMP ");
    sbDetailQuery.append(" , T704_ACCOUNT T704 ");
    sbDetailQuery.append(" WHERE TEMP.ID = T704.C704_ACCOUNT_ID ");
    sbDetailQuery.append(" ORDER BY AMT DESC) WHERE ROWNUM <= 5 ");


    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());

    return alReturn;
  }

  /**
   * reportDashboard - This method will fetch values for the Sales Homepage
   * 
   * @param String strUserId
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap reportDashboard(HashMap hmParam) throws AppError {
    log.debug(hmParam);
    HashMap hmReturn = new HashMap();
    HashMap hmSales = new HashMap();
    ArrayList alReturn = new ArrayList();

    hmParam.put("Condition", hmParam.get("COND"));
    hmSales = todaysSales(hmParam); // Todays and Month to Date sales figures
    hmReturn.put("SALES", hmSales);

    alReturn = todaysSalesReport(hmParam);
    hmReturn.put("TODAY", alReturn);

    alReturn = dailySalesReport(hmParam);
    hmReturn.put("MONTH", alReturn);
    hmReturn.put("WORKING_DAYS", fetchWorkingDays(""));
    return hmReturn;
  } // End of reportDashboard

  /**
   * fetchWorkingDays - This method will return the number of working days in the month upto current
   * date
   * 
   * @param
   * @return int
   * @exception AppError
   **/
  public int fetchWorkingDays(String strMonth) throws AppError {

    int intWorkingDays = 0;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_curr_month_work_days", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strMonth);
    gmDBManager.execute();
    intWorkingDays = gmDBManager.getInt(1);
    gmDBManager.close();
    return intWorkingDays;
  }// End of fetchWorkingDays

  /**
   * todaysSale - This method will return the Todays sales based on the access code
   * 
   * @param String strCondition
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap todaysSales(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());

    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    String strDaySales = "";
    String strMonthSales = "";
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    String strCondition = (String) hmParam.get("Condition");
    String strRegnFilter = (String) hmParam.get("REGN");
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
    //PMT-17911 , getting current date based on company time zone
    String strCompanyCurDate = (String) hmParam.get("TDATE");

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT " + strItemPriceColumn + " SALES ");
      sbQuery.append(" FROM  T501_ORDER T501, T502_ITEM_ORDER T502 ");
      sbQuery.append(" WHERE T501.C501_ORDER_DATE = to_date('");
      sbQuery.append(strCompanyCurDate);
      sbQuery.append("','");
      sbQuery.append(strDateFormat);
      sbQuery.append("') ");
      sbQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
      sbQuery.append(getSalesFilterClause());
      // Filter Condition to fetch record based on access value
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }

      // Filter Condition to fetch record based on Sales Hierarchy
      // sbQuery.append(getHierarchyWhereCondition(hmParam,"T501."));

      sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
      sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      log.debug("Today Sale 0 :-" + sbQuery.toString());
      strDaySales = (String) hmResult.get("SALES");
      hmReturn.put("DAYSALES", strDaySales);

      /*
       * Calendar cal = new GregorianCalendar();
       * 
       * int year = cal.get(Calendar.YEAR); int month = cal.get(Calendar.MONTH); month = month + 1;
       * String strMonth = "" + month; if (strMonth.length() == 1) { strMonth =
       * "0".concat(strMonth); } String strYear = "" + year; String strDate =
       * strMonth.concat("/01/"); strDate = strDate.concat(strYear);
       */

      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String strDate = GmCalenderOperations.getFirstDayOfMonth(currMonth, strDateFormat);

      hmResult = new HashMap();
      sbQuery.setLength(0);
      sbQuery.append(" SELECT " + strItemPriceColumn + " SALES ");
      sbQuery.append(" FROM  T501_ORDER T501, T502_ITEM_ORDER T502 ");
      sbQuery.append(" WHERE T501.C501_ORDER_DATE >= to_date('");
      sbQuery.append(strDate);
      sbQuery.append("','" + strDateFormat + "') ");
      sbQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");

      // Filter Condition to fetch record based on access value
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(getSalesFilterClause());

      // Filter Condition to fetch record based on Sales Hierarchy
      // sbQuery.append(getHierarchyWhereCondition(hmParam,"T501."));

      // Filter Condition to fetch record based on Region Filter
      // sbQuery.append(getRegionWhereCondition(strRegnFilter,"T501."));

      sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
      sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
      log.debug("Today Sale :-" + sbQuery.toString());
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      strMonthSales = (String) hmResult.get("SALES");
      hmReturn.put("MONTHSALES", strMonthSales);

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:todaysSales", "Exception is:" + e);
    }
    return hmReturn;
  } // End of todaysSale


  /**
   * todaysSalesReport - This method will return the
   * 
   * @param String strDate
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList todaysSalesReport(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();

    String strDate = (String) hmParam.get("TDATE");
    String strCondition = (String) hmParam.get("COND");
    String strFormat = (String) hmParam.get("FORMAT");
    String strDistSalesAmtGrp = GmCommonClass.parseNull((String) hmParam.get("DISTSALEAMTGRP"));
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "T704");

    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "T701");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();

    if (strDistSalesAmtGrp.equals("Y")) {
      sbQuery
          .append("    SELECT sales.*,SUM (sales.SALES) OVER (PARTITION BY sales.DID) distsales   FROM   ( ");
    }
    sbQuery.append(" SELECT t703.C701_DISTRIBUTOR_ID DID, " + strDistNmColumn + " DNAME, ");
    sbQuery.append(" " + strAccountNmColumn + " NAME, " + strItemPriceColumn + " SALES ");
    sbQuery.append(", T704.C704_ACCOUNT_ID ACC_ID");
    sbQuery
        .append(" FROM  T501_ORDER T501, T704_ACCOUNT T704, T703_SALES_REP t703, T701_DISTRIBUTOR t701 , T502_ITEM_ORDER T502 ");
    sbQuery.append(" WHERE T501.C501_ORDER_DATE = to_date('");
    sbQuery.append(strDate);
    sbQuery.append("','");
    sbQuery.append(strFormat);
    sbQuery.append("') ");
    sbQuery.append(" AND T501.C703_SALES_REP_ID = t703.C703_SALES_REP_ID ");
    sbQuery.append(" AND t703.C701_DISTRIBUTOR_ID = t701.C701_DISTRIBUTOR_ID ");
    sbQuery.append(" AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
    sbQuery.append(getSalesFilterClause());
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }

    // Filter Condition to fetch record based on Sales Hierarchy
    // sbQuery.append(getHierarchyWhereCondition(hmParam,"T501."));

    sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
    sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
    sbQuery.append(" AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID");
    sbQuery.append(" GROUP BY t703.C701_DISTRIBUTOR_ID, " + strDistNmColumn
        + ",T704.C704_ACCOUNT_ID, " + strAccountNmColumn + " , T501.C703_SALES_REP_ID ");
    sbQuery.append(" ORDER BY DNAME, NAME ");
    if (strDistSalesAmtGrp.equals("Y")) {
      sbQuery.append("   )sales ORDER BY distsales DESC, sales.NAME ");
    }

    log.debug("QUERY:" + sbQuery.toString());

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));


    return alReturn;
  } // End of todaysSalesReport


  /**
   * monthSalesReport - This method will
   * 
   * @param String strFromDate
   * @param String strToDate
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList monthSalesReport(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();
    String strFromDate = (String) hmParam.get("FRMDT");
    String strToDate = (String) hmParam.get("TODT");
    String strFormat = (String) hmParam.get("FORMAT");
    String strCondition = (String) hmParam.get("ACCESS");
    String strFilter = (String) hmParam.get("FILTER");
    String strReportType = (String) hmParam.get("REPORT");
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);
    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "T701");
    String strRepNmColumn =
        gmSalesFilterConditionBean.getSalesRepNameFilterClause(strCmpLangId, "T703");
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCmpLangId, "V700");

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();

      sbQuery.append(" SELECT t701.c701_distributor_id DID, " + strDistNmColumn + " DNAME, ");
      if (strReportType.equals("S")) {
        sbQuery.append(" T501.C501_ORDER_ID ORDID, T501.C501_PARENT_ORDER_ID PARENTID, ");
        sbQuery.append(" GET_TOTAL_ORDER_AMT(t501.c501_order_id," + strCurrType + ") GRANDTOTAL, ");
      } else {
        sbQuery.append(" NVL (t501.c501_parent_order_id, t501.c501_order_id) ORDID, ");
        // Added for PMT-17909 to display grand total based on currency filter type
        sbQuery
            .append(" GET_GRAND_TOTAL_ORDER_AMT (NVL (t501.c501_parent_order_id, t501.c501_order_id) ,"
                + strCurrType + ") GRANDTOTAL, ");
      }

      sbQuery.append(" " + strItemPriceColumn + " SALES, " + strAccountNmColumn
          + " NAME, V700.AC_ID ID ");
      sbQuery.append(" ,TO_CHAR(T501.C501_ORDER_DATE,'" + getCompDateFmt() + "') ORDDATE, ");
      sbQuery.append(" " + strRepNmColumn + " RNAME , '' PERCENT");
      if (strReportType.equals("G")) {
        sbQuery.append(" ,get_set_name(t207.c207_set_id) GROUPNM ");
      }
      sbQuery
          .append(" FROM T501_ORDER T501 , T502_ITEM_ORDER T502 , V700_TERRITORY_MAPPING_DETAIL V700");
      sbQuery.append(" ,T703_SALES_REP t703, T701_DISTRIBUTOR T701 ");
      if (strReportType.equals("G")) {
        sbQuery.append(", t208_set_details t208, t207_set_master t207 ");
        sbQuery.append(" WHERE t208.C205_PART_NUMBER_ID = t502.C205_PART_NUMBER_ID ");
        sbQuery.append(" AND t208.c207_set_id = t207.c207_set_id ");
        sbQuery.append(" AND t207.c901_set_grp_type = 1600 ");
        sbQuery.append(" AND ");
      } else {
        sbQuery.append(" WHERE ");
      }
      sbQuery.append(" T501.C501_ORDER_DATE BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDate);
      sbQuery.append("',' ");
      sbQuery.append(strFormat);
      sbQuery.append("') AND  to_date('");
      sbQuery.append(strToDate);
      sbQuery.append("',' ");
      sbQuery.append(strFormat);
      sbQuery.append("') AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
      sbQuery.append(" AND  T502.C502_VOID_FL IS NULL ");
      sbQuery.append(" and t703.C703_SALES_REP_ID = t501.C703_SALES_REP_ID ");
      sbQuery.append(" and t703.C701_DISTRIBUTOR_ID = t701.C701_DISTRIBUTOR_ID ");
      sbQuery.append(getSalesFilterClause());
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }

      // Filter Condition to fetch record based on Region Filter
      // sbQuery.append(getRegionWhereCondition(strRegnFilter,"T501."));

      // Filter Condition to fetch record based on Sales Hierarchy
      // sbQuery.append(getHierarchyWhereCondition(hmParam,"T501."));

      // Filter Condition to fetch record based on Filters
      if (!strFilter.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strFilter);
      }

      // sbQuery.append(" AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID ");
      sbQuery.append(" AND T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
      sbQuery.append(" AND T501.C704_ACCOUNT_ID = V700.AC_ID ");
      sbQuery.append(" GROUP BY ");
      if (strReportType.equals("S")) {
        sbQuery.append(" T501.C501_ORDER_ID,T501.C501_PARENT_ORDER_ID ");
      } else if (strReportType.equals("G")) {
        sbQuery.append(" NVL(T501.C501_PARENT_ORDER_ID, T501.C501_ORDER_ID) ,t207.c207_set_id ");
      } else {
        sbQuery.append(" NVL(T501.C501_PARENT_ORDER_ID, T501.C501_ORDER_ID) ");
      }
      sbQuery.append(" ," + strRepNmColumn + ", V700.AC_ID ," + strAccountNmColumn
          + ", t701.c701_distributor_id, " + strDistNmColumn + ", T501.C501_ORDER_DATE ");
      sbQuery.append(" ORDER BY DNAME, NAME, ORDID DESC");

      log.debug("QUERY:" + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:monthSalesReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of monthSalesReport

  /**
   * dailySalesReport - This method will load the Monthly Sales Information Based on the parameter
   * passed
   * 
   * @param String strFromDate
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList DetailMonthlySalesReport(String strFromMonth, String strFromYear,
      String strCondition, String strCurrType) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    try {


      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT T501.C501_ORDER_DATE SDATE, to_char(T501.C501_ORDER_DATE,'DD') DAY, ");
      sbQuery.append(" " + strItemPriceColumn + " AMT ");
      sbQuery.append(" FROM T501_ORDER T501, T502_ITEM_ORDER T502");
      sbQuery.append(" WHERE ");
      sbQuery.append("  T501.C501_VOID_FL IS NULL ");
      sbQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
      sbQuery.append("  AND T501.C501_DELETE_FL IS NULL ");
      sbQuery.append("  AND T502.C502_VOID_FL IS NULL ");
      sbQuery.append(" AND  ");
      sbQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strFromMonth, strFromYear));
      sbQuery.append(getSalesFilterClause());
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(" GROUP BY C501_ORDER_DATE,to_char(C501_ORDER_DATE,'DD') ");
      sbQuery.append(" ORDER BY C501_ORDER_DATE ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());


    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:dailySalesReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of dailySalesReport

  /**
   * dailySalesReport - This method will load the Monthly Project Information Based on the parameter
   * passed
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strCondition
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList DetailMonthlyProjectReport(String strFromMonth, String strFromYear,
      String strCondition, String strCurrType) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    try {


      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT * FROM ( ");
      sbQuery.append(" SELECT GET_PROJECT_NAME(T205.C202_PROJECT_ID) NAME , ");
      sbQuery.append(" " + strItemPriceColumn + " AMT ");
      sbQuery.append(" FROM T501_ORDER T501, ");
      sbQuery.append("  T502_ITEM_ORDER T502,");
      sbQuery.append("  T205_PART_NUMBER T205");
      sbQuery.append(" WHERE ");
      sbQuery.append("  T501.C501_VOID_FL IS NULL ");
      sbQuery.append("  AND T501.C501_DELETE_FL IS NULL ");
      sbQuery.append("  AND T502.C502_VOID_FL IS NULL ");
      sbQuery.append("  AND T501.C501_ORDER_ID	= T502.C501_ORDER_ID ");
      sbQuery.append("  AND T205.C205_PART_NUMBER_ID = T502.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND  ");
      sbQuery.append(getOrderWhereCondtion(strFromMonth, strFromYear, strFromMonth, strFromYear));
      sbQuery.append(getSalesFilterClause());
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(" GROUP BY T205.C202_PROJECT_ID ");
      sbQuery.append(" ) ORDER BY AMT DESC ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:dailySalesReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of dailySalesReport

  /**
   * dailySalesReport - This method will load the daily report for
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList dailySalesReport(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    String strCondition = (String) hmParam.get("Condition");
    String strRegnFilter = (String) hmParam.get("REGN");
    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strItemPriceColumn =
        gmSalesFilterConditionBean.getOrderItemPriceFilterClause(strCurrType);

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      /*
       * Calendar cal = new GregorianCalendar(); int year = cal.get(Calendar.YEAR); int month =
       * cal.get(Calendar.MONTH); month = month + 1; String strMonth = "" + month; if
       * (strMonth.length() == 1) { strMonth = "0".concat(strMonth); } String strYear = "" + year;
       * String strDate = strMonth.concat("/01/"); strDate = strDate.concat(strYear);
       */

      String strDateFmt = getCompDateFmt();

      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String strDate = GmCalenderOperations.getFirstDayOfMonth(currMonth, strDateFmt);

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT T501.C501_ORDER_DATE SDATE, to_char(T501.C501_ORDER_DATE,'Day') DAY, ");
      sbQuery.append(" to_char(T501.C501_ORDER_DATE ,'" + strDateFmt + "') SDATEFMT,");
      sbQuery.append(" " + strItemPriceColumn
          + " SALES , to_char(C501_ORDER_DATE,'D') DOF, to_char(C501_ORDER_DATE,'DD') DOM");
      sbQuery.append(" FROM T501_ORDER T501, T502_ITEM_ORDER T502 ");
      sbQuery.append(" WHERE T501.C501_ORDER_DATE >= to_date('");
      sbQuery.append(strDate);
      sbQuery.append("','" + strDateFormat + "') ");
      sbQuery.append("   AND T501.C501_ORDER_ID =  T502.C501_ORDER_ID ");
      sbQuery.append(getSalesFilterClause());
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }

      // Filter Condition to fetch record based on Sales Hierarchy
      // sbQuery.append(getHierarchyWhereCondition(hmParam,"T501."));

      sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T501.C501_DELETE_FL IS NULL ");
      sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
      sbQuery.append(" GROUP BY C501_ORDER_DATE ");
      sbQuery.append(" ORDER BY C501_ORDER_DATE ");

      log.debug("Daily Sale:" + sbQuery.toString());

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:dailySalesReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of dailySalesReport


  /**
   * loadTerritoryQuota - This method will
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadTerritoryQuota() throws AppError {
    ArrayList alReturn = new ArrayList();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C701_DISTRIBUTOR_ID DID, GET_DISTRIBUTOR_NAME (C701_DISTRIBUTOR_ID) DNAME, ");
      sbQuery
          .append(" C702_TERRITORY_ID TID, C702_TERRITORY_NAME TNAME, C702_TERRITORY_QUOTA TQUOTA, ");
      sbQuery.append(" C702_COUNTIES TCOUNTIES ");
      sbQuery.append(" FROM T702_TERRITORY ");
      sbQuery.append(" ORDER BY DNAME ");

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:loadTerritoryQuota", "Exception is:" + e);
    }
    return alReturn;
  } // End of loadTerritoryQuota

  /**
   * getOrderWhereCondtion - This Method is used to fetch the sales for the selected period for the
   * entire state
   * 
   * @param String strFromMonth
   * @param String strFromYear
   * @param String strToMonth
   * @param String strToYear
   * @param String strDistributorID (Contains the Selected Distributor ID)
   * @return HashMap
   * @exception AppError
   **/
  private String getOrderWhereCondtion(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear) {
    StringBuffer sbWhereCondtion = new StringBuffer();
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= TO_DATE('");
      sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
      sbWhereCondtion.append("','MM/YYYY') ");
    }

    if (strToMonth != null && strToYear != null) {
      if (sbWhereCondtion != null) {
        sbWhereCondtion.append(" AND");
      }
      sbWhereCondtion.append(" T501.C501_ORDER_DATE <= LAST_DAY(TO_DATE('");
      sbWhereCondtion.append(strToMonth + "/" + strToYear);
      sbWhereCondtion.append("','MM/YYYY')) ");
    }

    // If the From and To Dates values are not passed then system will perform the below operation
    if (sbWhereCondtion.toString().equals("")) {
      sbWhereCondtion.append(" T501.C501_ORDER_DATE >= ");
      sbWhereCondtion.append("ADD_MONTHS(TRUNC(CURRENT_DATE ,'MONTH') , -12)  ");
      sbWhereCondtion.append("AND T501.C501_ORDER_DATE <= CURRENT_DATE ");

    }
    return sbWhereCondtion.toString();
  }

  /**
   * loadProjectGroup - This Method is used to load project group information
   * 
   * @param ArrayList
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadProjectGroup() throws AppError {
    StringBuffer sbDetailQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strCompanyId = getCompId();

    sbDetailQuery.append(" SELECT T207.C207_SET_ID ID  ");
    sbDetailQuery.append(" ,C207_SET_NM NAME ");
    sbDetailQuery.append(" FROM  T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080");
    sbDetailQuery.append(" WHERE T207.C207_SET_ID = T2080.C207_SET_ID");
    sbDetailQuery.append(" AND C901_SET_GRP_TYPE = 1600");
    sbDetailQuery.append(" AND T2080.C1900_COMPANY_ID = '");
    sbDetailQuery.append(strCompanyId);
    sbDetailQuery.append("'");
    sbDetailQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
    sbDetailQuery.append(" ORDER BY NAME ");

    log.debug("Load Project Query==" + sbDetailQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());

    return alReturn;
  }
  
  /**
   * loadProjectGroup - This Method is used to load project group information with Corporate codeName
   * 
   * @param ArrayList
   * @return ArrayList
   * @exception AppError
   **/
  //PC-3631 Corporate Files Upload on SpineIT - Globus App
  public ArrayList loadProjectGroup(String strSetGroupType) throws AppError {
	    StringBuffer sbDetailQuery = new StringBuffer();

	    ArrayList alReturn = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	    String strCompanyId = getCompId();	    
	    
	    sbDetailQuery.append(" SELECT T207.C207_SET_ID ID  ");
	    sbDetailQuery.append(" ,C207_SET_NM NAME ");
	    sbDetailQuery.append(" FROM  T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080");
	    sbDetailQuery.append(" WHERE T207.C207_SET_ID = T2080.C207_SET_ID");
	    sbDetailQuery.append(" AND C901_SET_GRP_TYPE IN ("+strSetGroupType+")");
	    sbDetailQuery.append(" AND T2080.C1900_COMPANY_ID = '");
	    sbDetailQuery.append(strCompanyId);
	    sbDetailQuery.append("'");
	    sbDetailQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
	    sbDetailQuery.append(" ORDER BY NAME ");

	    log.debug("New Load Project Query With Arguments==" + sbDetailQuery.toString());
	    alReturn = gmDBManager.queryMultipleRecords(sbDetailQuery.toString());

	    return alReturn;
	  }
  
  /***********************************************************************************************************
   * reportOrderDetail - This Method is used to get information on the Order details Used in
   * GmSalesOrderDetailServlet to display Distributor / Account sales information
   * 
   * @param HashMap containing the following data key strCondition - Authorizes if a User can view
   *        the data strFromDate - The Date FROM where the user wants to pull the data - currently
   *        set to current date strToDate - The Date TO where the user wants to pull the data -
   *        currently set to current date strAccId - The Account ID strDistId - The Distributor ID
   * @return DynaSet
   * @exception AppError
   *************************************************************************************************************/

  public RowSetDynaClass reportOrderDetail(HashMap hmAccData) throws AppError {
    String strCondition = (String) hmAccData.get("strCondition");
    String strFromDate = (String) hmAccData.get("strFromDate");
    String strToDate = (String) hmAccData.get("strToDate");
    String strAccId = (String) hmAccData.get("strAccId");
    String strDistId = (String) hmAccData.get("strDistId");
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;



    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C501_ORDER_ID ID");
    sbQuery.append(", GET_ACCOUNT_NAME(T501.C704_ACCOUNT_ID) NAME");
    sbQuery.append(", C501_ORDER_DATE_TIME ODT ");
    sbQuery.append(", C501_TRACKING_NUMBER TRACK ");
    sbQuery.append(", T501.C501_TOTAL_COST COST");
    sbQuery.append(", NVL(T501.C501_SHIP_COST,0) SHIPCOST");
    sbQuery.append(", DECODE(C501_STATUS_FL");
    sbQuery.append(",6, 'Marked as Duplicate Order' ");
    sbQuery.append(",5, 'Paid and Closed' ");
    sbQuery.append(",4, 'Pending Payment' ");
    sbQuery.append(",3, 'Pending Invoice' ");
    sbQuery.append(",2, DECODE (C501_SHIPPING_DATE, ' ','Pending Shipping','Shipped ') ");
    sbQuery.append(",1, 'Pending Control Number'");
    sbQuery.append(",0, 'Back Order' ) Order_Status ");
    sbQuery.append(", GET_REP_NAME(C703_SALES_REP_ID) REP_NAME ");
    sbQuery.append(", GET_COUNT_SUM(C501_ORDER_ID) CNT ");
    sbQuery.append(", C501_CUSTOMER_PO PO ");
    sbQuery.append(", GET_ORDER_INVOICE_NUM(C704_ACCOUNT_ID,C501_CUSTOMER_PO)  INVID  ");
    sbQuery.append(", GET_CODE_NAME(C901_ORDER_TYPE) ORDERTYPE ");
    sbQuery.append("FROM T501_ORDER T501 ");
    sbQuery.append(" WHERE C501_VOID_FL IS NULL ");
    sbQuery.append(" AND C501_DELETE_FL IS NULL ");
    sbQuery.append(getSalesFilterClause());
    // Fetch value based on filter conditon
    if (!strFromDate.equals("")) {
      sbQuery.append("AND  C501_ORDER_DATE BETWEEN TO_DATE ('");
      sbQuery.append(strFromDate);
      sbQuery.append("', '" + strDateFormat + "') ");
    }

    if (!strToDate.equals("")) {
      sbQuery.append("AND TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("','" + strDateFormat + "'') ");
    }


    if (!strDistId.equals("")) {
      sbQuery
          .append(" AND  T501.C703_SALES_REP_ID IN  ( SELECT C703_SALES_REP_ID FROM T703_SALES_REP  T703 ");
      sbQuery.append(" WHERE T703.C701_DISTRIBUTOR_ID = " + strDistId + ")");
    }


    if (!strAccId.equals("")) {
      sbQuery.append(" AND C704_ACCOUNT_ID =" + strAccId);
    }

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }

    sbQuery.append("ORDER BY C501_ORDER_DATE_TIME DESC ");


    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return resultSet;
  }



  /**
   * This Method is used to get information on the Pricing information for parts in a project Used
   * in GmSalesAcctPriceReportServlet to display Part pricing information
   * 
   * @param hmParam the hm param
   * @return the hash map
   * @throws AppError the app error
   */
  public HashMap reportAccPrice(HashMap hmParam) throws AppError {
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strProjId = GmCommonClass.parseNull((String) hmParam.get("PROJID"));
    String strAccessCondition = GmCommonClass.parseNull((String) hmParam.get("ACCESSCONDITION"));
    String strIncreasePrice = GmCommonClass.parseNull((String) hmParam.get("INCREASEPRICE"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("MODE"));
    String strQueryBy = GmCommonClass.parseNull((String) hmParam.get("QUERYBY"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strCompanyID = getCompId();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;
    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    StringBuffer sbQuery = new StringBuffer();
    if (strQueryBy.equals("System Name")) {
      sbQuery
          .append(" SELECT  /*+ ordered use_hash(T705) push_subq */ T207.C207_SET_ID SYSID, T207.C207_SET_NM SYSNM ");
    } else {
      sbQuery
          .append(" SELECT  /*+ ordered use_hash(T705) push_subq */ T202.C202_PROJECT_ID PJID, T202.C202_PROJECT_NM PJNM ");
    }

    sbQuery.append(" , T205.C205_PART_NUMBER_ID PID ");
    sbQuery.append(" , DECODE(t705_GPO.uprice_gpo,NULL,'','*') GPO ");
    sbQuery.append(" , get_partdesc_by_company(T205.C205_PART_NUMBER_ID)  PDESC ");
    sbQuery.append(" , T2052.C2052_LIST_PRICE TIERI  ");
    sbQuery.append(" , '' PLINE  ");
    sbQuery.append(" , T2052.C2052_LOANER_PRICE  TIERII ");
    sbQuery
        .append(" , DECODE(NVL(T2052.C2052_LIST_PRICE,0),0 , 0,(  (T2052.C2052_LIST_PRICE - T2052.C2052_LOANER_PRICE ) / T2052.C2052_LIST_PRICE) * 100  )   TIERIID ");
    sbQuery.append(" , T2052.C2052_CONSIGNMENT_PRICE TIERIII  ");
    sbQuery
        .append(" , DECODE(NVL(T2052.C2052_LIST_PRICE,0),0 , 0,(  (T2052.C2052_LIST_PRICE - T2052.C2052_CONSIGNMENT_PRICE ) / T2052.C2052_LIST_PRICE)* 100 )  TIERIIID ");
    sbQuery.append(" , T2052.C2052_EQUITY_PRICE TIERIV ");
    sbQuery
        .append(" , DECODE(NVL(T2052.C2052_LIST_PRICE,0),0 , 0,(  (T2052.C2052_LIST_PRICE - T2052.C2052_EQUITY_PRICE ) / T2052.C2052_LIST_PRICE)* 100 )  TIERIVD ");
    sbQuery
        .append(" , DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo) UPRICE ");

    if (!strIncreasePrice.equals("")) {
      sbQuery
          .append(" , CEIL (DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo) + ( ( "
              + strIncreasePrice
              + " * DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo) ) / 100  )) PRICE2008 ");
    }
    // Below code to fetch tier /custom tier calculation
    sbQuery
        .append(" , DECODE( DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo) ,T2052.C2052_LIST_PRICE, 'T1' ");
    sbQuery.append(" ,T2052.C2052_LOANER_PRICE, 'T2'   ");
    sbQuery.append(" , T2052.C2052_CONSIGNMENT_PRICE,'T3'  ");
    sbQuery.append(" ,  T2052.C2052_EQUITY_PRICE, 'T4' ");
    sbQuery.append(" ,0, '-', ");
    // Below code to find custom level
    // Level T4
    sbQuery
        .append(" DECODE(SIGN( DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo) - T2052.C2052_EQUITY_PRICE),-1,'C*T4' ,  ");

    // Level T3
    sbQuery
        .append(" DECODE(SIGN( DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo) - T2052.C2052_CONSIGNMENT_PRICE),-1,'C*T3' , ");

    // Level T2
    sbQuery
        .append(" DECODE(SIGN( DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo) - T2052.C2052_LOANER_PRICE),-1,'C*T2', ");

    sbQuery.append(" 'C*T1' ) ) ))  TLIST ");
    sbQuery
        .append(" , DECODE(DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo), 0, 0, 1 - (DECODE(t705_GPO.uprice_gpo,NULL,NVL (t705.uprice, 0), t705_GPO.uprice_gpo)/T2052.C2052_LIST_PRICE) ) DOFFERED ");
    if (strQueryBy.equals("System Name")) {
      sbQuery
          .append(" FROM T205_PART_NUMBER T205,T207_SET_MASTER t207, T208_SET_DETAILS t208 ,T2052_PART_PRICE_MAPPING T2052 , ");
    } else {
      sbQuery
          .append(" FROM T205_PART_NUMBER T205, T202_PROJECT T202, T2052_PART_PRICE_MAPPING T2052, ");
    }

    sbQuery.append(" (SELECT C205_PART_NUMBER_ID, T705.C705_UNIT_PRICE UPRICE  ");
    sbQuery.append(" , T705.C705_DISCOUNT_OFFERED DOFFERED   ");
    sbQuery.append(" FROM T705_ACCOUNT_PRICING T705 , t704_account t704 ");
    /*
     * AccessConditions are changed for VP and AD as part of PMT-28. So,VP and AD Ids are getting
     * from view with alias name of t501.
     */
    sbQuery
        .append(" ,(SELECT DISTINCT ac_id c704_account_id, rep_id c703_sales_rep_id, ad_id c501_ad_id, vp_id c501_vp_id ");
    sbQuery.append(" FROM v700_territory_mapping_detail) t501 ");
    sbQuery.append(" WHERE t705.c101_party_id = t704.c101_party_id ");
    // sbQuery.append("AND t704.c101_party_id = t705.c101_party_id ");
    sbQuery.append(" AND t704.C704_ACCOUNT_ID = t501.C704_ACCOUNT_ID ");
    sbQuery.append(" AND t704.c704_account_id = '");
    sbQuery.append(strAccId);
    sbQuery.append("'");
    // Filter Condition to fetch record based on access code
    if (!strAccessCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strAccessCondition);
    }
    sbQuery.append(" AND t705.c705_void_fl IS NULL ");
    sbQuery.append(" AND t704.c704_void_fl IS NULL ");

    sbQuery.append(") T705 , (SELECT c205_part_number_id, t705.c705_unit_price uprice_gpo ");
    sbQuery.append(" FROM t705_account_pricing t705 ");
    sbQuery.append(" WHERE C101_PARTY_ID = get_account_gpo_id('");
    sbQuery.append(strAccId);
    sbQuery.append("') AND t705.c705_void_fl IS NULL ");

    sbQuery.append(" ) t705_GPO ");

    sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID = T705.C205_PART_NUMBER_ID (+)  ");
    sbQuery.append(" AND t205.c205_part_number_id = t705_GPO.c205_part_number_id(+) ");
    if (!strPartNum.equals("")) {
    	sbQuery.append(" AND REGEXP_LIKE (T205.C205_PART_NUMBER_ID,REGEXP_REPLACE('" + strPartNum + "','[+]','\\+'))");
    }
    sbQuery.append(" AND T2052.C2052_LIST_PRICE IS NOT NULL ");
    sbQuery.append(" AND T2052.C2052_LIST_PRICE <> 0 ");
    if (!strQueryBy.equals("System Name")) {
      sbQuery.append(" AND T202.C901_STATUS_ID = '20302' ");
    }
    // Fetch value based on filter conditon
    if (!strProjId.equals("")) {
      if (strQueryBy.equals("System Name")) {
        sbQuery.append(" AND T207.C207_SET_ID IN ('");
      } else {
        sbQuery.append(" AND T205.C202_PROJECT_ID IN ('");
      }
      sbQuery.append(strProjId);
      sbQuery.append("')");
    }
    sbQuery.append(" AND T205.C205_PART_NUMBER_ID  = T2052.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T2052.C1900_COMPANY_ID = '");
    sbQuery.append(strCompanyID);
    sbQuery.append("'");
    if (strQueryBy.equals("System Name")) {
      sbQuery.append(" AND T207.C207_SET_ID         = T208.C207_SET_ID ");
      sbQuery.append(" AND t208.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND  (t705.uprice <> 0 OR t705_gpo.uprice_gpo <> 0) ");
      sbQuery.append(" ORDER BY T207.C207_SET_NM,T205.C205_PART_NUMBER_ID ");
    } else {
      sbQuery
          .append(" AND T202.C202_PROJECT_ID = T205.C202_PROJECT_ID   AND  (t705.uprice <> 0 OR t705_gpo.uprice_gpo <> 0) ");
      sbQuery.append(" ORDER BY T202.C202_PROJECT_NM,T205.C205_PART_NUMBER_ID ");
    }


    log.debug(" Query for getting part pricing details is " + sbQuery.toString());
    if (!strMode.equals("") && strMode.equals("ACC_PRICE")) {
      ArrayList alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("PRICINGLISTREPORT", alResult);
    } else {
      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
      hmReturn.put("PRICINGLIST", resultSet);
    }

    // Below code to fecth Excel header information
    sbQuery.setLength(0);

    /*
     * .
     * 
     * sbQuery.append("SELECT '"); sbQuery.append( strAccId + "' ID  "); sbQuery.append(
     * " , GET_ACCOUNT_NAME( " + strAccId + " ) NAME ") ; sbQuery.append(
     * " , TO_CHAR(CURRENT_DATE, 'MM/DD/YYYY HH:MI:SS' ) DT" ) ; sbQuery.append( " FROM DUAL ");
     * 
     * .
     */

    sbQuery
        .append("  SELECT AC_ID ID, AC_NAME NAME, REGION_NAME REGION, AD_NAME AREADIR, D_NAME DISTRIBUTOR,  TER_NAME  TERRITORY, REP_NAME  REP ");
    sbQuery.append("  , CURRENT_DATE DT  ");
    sbQuery.append("  FROM V700_TERRITORY_MAPPING_DETAIL  X WHERE X.AC_ID = '" + strAccId + "'  ");

    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

    hmReturn.put("ACCDETAIL", hmResult);

    return hmReturn;
  }

  /**
   * getRegionWhereCondition - This Method is used to frame the additional filters for Region
   * (east/west)
   * 
   * @param String strRegion
   * @return String
   * @exception AppError
   **/
  public String getRegionWhereCondition(String strRegion, String strType) {
    StringBuffer sbWhereCondtion = new StringBuffer();

    // Code to frame the Where Condition
    if (!strRegion.equals("")) {
      sbWhereCondtion.append(" AND ");
      if (!strType.equals("")) {
        sbWhereCondtion.append(strType);
      }
      sbWhereCondtion.append("C703_SALES_REP_ID IN ( SELECT REP_ID ");
      sbWhereCondtion.append(" FROM V700_TERRITORY_MAPPING_DETAIL");
      sbWhereCondtion.append(" WHERE REGION_ID IN (SELECT C901_CODE_ID");
      sbWhereCondtion.append(" FROM T901_CODE_LOOKUP WHERE C902_CODE_NM_ALT ='");
      sbWhereCondtion.append(strRegion);
      sbWhereCondtion.append("'))");
    }
    return sbWhereCondtion.toString();
  }


  /**
   * getHierarchyWhereCondition - This Method is used to frame the additional filters for Region
   * (east/west)
   * 
   * @param String strRegion
   * @return String
   * @exception AppError
   **/
  /*
   * public String getHierarchyWhereCondition (HashMap hmParam, String strType) { StringBuffer
   * sbWhereCondtion = new StringBuffer(); String strCompFilter = (String)hmParam.get("COMP");
   * 
   * // Code to frame the Where Condition if (!strCompFilter.equals("")) {
   * sbWhereCondtion.append(" AND "); if (!strType.equals("")) { sbWhereCondtion.append(strType); }
   * sbWhereCondtion.append("C704_ACCOUNT_ID IN ( SELECT C704_ACCOUNT_ID ");
   * sbWhereCondtion.append(" FROM T704_ACCOUNT " );
   * sbWhereCondtion.append(" WHERE C901_COMPANY_ID IN (" ); sbWhereCondtion.append(strCompFilter);
   * sbWhereCondtion.append("))"); } return sbWhereCondtion.toString(); } // End of
   * getHierarchyWhereCondition
   */
  /**
   * fetchWorkingDaysByRange - This method will return the number of working days between two given
   * dates
   * 
   * @param strFromMonth, strToMonth
   * @return int business days
   * @exception AppError
   **/
  public int fetchWorkingDaysByRange(String strFromMonth, String strToMonth) throws AppError {

    int intWorkingDays = 0;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_work_days_by_range", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strFromMonth);
    gmDBManager.setString(3, strToMonth);
    gmDBManager.execute();
    intWorkingDays = gmDBManager.getInt(1);
    gmDBManager.close();
    return intWorkingDays;
  }// End of fetchWorkingDaysByRange

  /**
   * Generate out put.
   * 
   * @param hmTemplate the hm template
   * @param strName the str name
   * @return the string
   * @throws Exception the exception
   */
  public String generateOutPut(HashMap hmTemplate, String strName) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmTemplate", hmTemplate);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setTemplateName(strName);
    return templateUtil.generateOutput();
  }

  /**
   * fetchTotalBusinessDaysByMonth - This method will return the total number of working days for
   * the passed in Month/Year.
   * 
   * @return int total Business days
   * @exception AppError
   **/
  public int fetchTotalBusinessDaysByMonth(String strDate) throws AppError {

    int intWorkingDays = 0;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_total_work_days_by_month", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strDate);
    gmDBManager.setString(3, getCompId());
    gmDBManager.execute();
    intWorkingDays = gmDBManager.getInt(1);
    gmDBManager.close();
    return intWorkingDays;
  }// End of fetchTotalBusinessDaysByMonth
}
