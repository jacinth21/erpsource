/*
 * Module: GmSalesReportBean.java Author: RichardK Project: Globus Medical App Date-Written: Mar
 * 2005 Security: Unclassified Description: This beans will be used to generate all kind of sames
 * report
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.sales.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmTerritoryBean extends GmBean {



  // this will be removed all place changed with GmDataStoreVO constructor

  public GmTerritoryBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmTerritoryBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * getRepAccountsList - This method will list account name based on account type
   * 
   * @param String strId (Holds id to be used to filter he value
   * @param String strType -- Holds Filter type information 'TERR' Territory 'DIST' Distributor
   *        'REP' Rep Information 'AD' Area Director
   * @return ArrayList
   * @exception AppError
   **/
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ArrayList getAccountsList(String strId, String strType, String strAccessFilter,
      String strCompLangId) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCompLangId, "V700");
    try {
      DBConnectionWrapper dbCon = null;
      dbCon = new DBConnectionWrapper();


      sbQuery.append(" SELECT ");
      sbQuery.append("  AC_ID AID ");
      sbQuery.append(" ," + strAccountNmColumn + " ANAME ");
      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery.append(" WHERE ");
      if (strType.equals("REP")) {
        sbQuery.append(" V700.REP_ID =");
      } else if (strType.equals("TERR")) {
        sbQuery.append(" V700.TER_ID =");
      } else if (strType.equals("DIST")) {
        sbQuery.append(" V700.D_ID =");
      } else if (strType.equals("AD")) {
        sbQuery.append(" V700.AD_ID =");
      }

      sbQuery.append(strId);

      // Filter Condition to fetch record based on access code
      if (!strAccessFilter.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strAccessFilter);
      }

      sbQuery.append(" AND V700.AC_ID  IS NOT NULL ORDER BY ANAME ");
      log.debug(strType + "######### Account List" + sbQuery.toString());
      alReturn = dbCon.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmTerritoryBean:getAccountsList", sbQuery.toString()
          + "Exception is:" + e);
    }
    return alReturn;
  } // End of getRepAccountsList

  /**
   * saveTerritory - This method will
   * 
   * @param HashMap hmParam
   * @param String strUserId
   * @param String strAction
   * @return HashMap
   * @exception AppError
   **/
  public HashMap saveTerritory(HashMap hmParam, String strUserId, String strAction) throws AppError {
    GmCommonBean gmCom = new GmCommonBean();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";
    HashMap hmReturn = new HashMap();

    String strTerrNm = (String) hmParam.get("TNAME");
    String strTerrId = (String) hmParam.get("TID");
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strYear = (String) hmParam.get("YEAR");
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));
    String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("AFLAG"));
    strRepId = strRepId.equals("0") ? "" : strRepId;
    log.debug("strInputString-----------" + strInputString);
    log.debug("strYear-----------" + strYear);
    gmDBManager.setPrepareString("GM_DELETE_QUOTA", 2);

    gmDBManager.setString(1, strRepId);
    gmDBManager.setString(2, strYear.trim());

    gmDBManager.execute();

    log.debug("GM_SAVE_TERRITORY-----------");
    gmDBManager.setPrepareString("GM_SAVE_TERRITORY", 10);

    gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(10, java.sql.Types.CHAR);

    gmDBManager.setString(1, strTerrId);
    gmDBManager.setString(2, strTerrNm);
    gmDBManager.setInt(3, 0);
    gmDBManager.setString(4, strInputString);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strActiveFl);
    gmDBManager.setString(7, strAction);
    gmDBManager.setString(8, strRepId);
    gmDBManager.execute();

    strTerrId = gmDBManager.getString(9);
    strMsg = gmDBManager.getString(10);

    hmReturn.put("TID", strTerrId);
    hmReturn.put("MSG", strMsg);
    log.debug("strTerrId-----------" + strTerrId);
    if (!strLog.equals("")) {
      gmCom.saveLog(gmDBManager, strTerrId, strLog, strUserId, "1212");
    }

    gmDBManager.commit();
    log.debug("---hmReturn-----------" + hmReturn);
    return hmReturn;
  } // End of saveTerritory



  /**
   * loadTerritory - This method is used to retrieve load details for the Territory Setup & Quota
   * screen
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadTerritory() throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alTypes = new ArrayList();
    GmCommonClass gmCommon = new GmCommonClass();
    try {
      ArrayList alTerritory = getTerritoryList("ALL");
      hmReturn.put("TERRITORYLIST", alTerritory);
      alTypes = gmCommon.getCodeList("YEAR");
      hmReturn.put("YEARLIST", alTypes);
    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadTerritory", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadTerritory

  /**
   * getTerritoryList - This method will territory list
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList getTerritoryList(String strType) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT ");
    sbQuery.append(" C702_TERRITORY_ID TID, C702_TERRITORY_NAME TNAME");
    sbQuery.append(" FROM T702_TERRITORY WHERE  ");
    sbQuery.append(" C702_DELETE_FL IS NULL AND C1900_COMPANY_ID = " + getCompId());

    if (strType.equals("UnAssigned")) {
      sbQuery.append(" AND C702_TERRITORY_ID NOT IN ( SELECT nvl(C702_TERRITORY_ID,0)");
      sbQuery.append(" FROM T703_SALES_REP)  ");
    }
    sbQuery.append(" ORDER BY UPPER(TNAME) ");

    log.debug(" # # #  getTerritoryList" + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alReturn;
  } // End of getTerritoryList

  /**
   * loadTerritoryDetails - This method will
   * 
   * @param String Territory Id
   * @param String Year
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadTerritoryDetails(String strRepId, String strTerrId, String strYear,
      String strCurrTypeId, String strCompLangId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alResult = new ArrayList();
    String strRepNmColumn = "B.C703_SALES_REP_NAME";

    GmTerritoryBean gmTerr = new GmTerritoryBean();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      if (strCompLangId.equals("103097")) {
        strRepNmColumn = "  NVL(B.C703_SALES_REP_NAME_EN,B.C703_SALES_REP_NAME) ";
      }


      log.debug("====strRepId=========" + strRepId);
      log.debug("====strTerrId=========" + strTerrId);
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT A.C702_TERRITORY_ID TERRID, A.C702_TERRITORY_NAME TNAME, GET_AD_REP_NAME(B.C703_SALES_REP_ID) ADNAME,");
      sbQuery.append(" B.C703_SALES_REP_ID REPID, " + strRepNmColumn
          + " RNAME, GET_DIST_REP_NAME(B.C703_SALES_REP_ID) DNAME ");
      sbQuery.append(" ,A.C702_ACTIVE_FL AFLAG ");
      sbQuery.append(" FROM T702_TERRITORY A, T703_SALES_REP B ");
      sbQuery.append(" WHERE A.C702_TERRITORY_ID = B.C702_TERRITORY_ID(+) ");
      // sbQuery.append(" AND A.C901_CURN_TYPE ="+strCurrTypeId );
      if (!(strRepId.equals("") || strRepId.equals("0"))) {
        sbQuery.append(" AND B.c703_sales_rep_id  =");
        sbQuery.append(strRepId);
      } else if (!strTerrId.equals("")) {
        sbQuery.append(" AND A.C702_TERRITORY_ID =");
        sbQuery.append(strTerrId);
      }
      log.debug("====sbQuery=========" + sbQuery);
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      gmDBManager.close();

      hmReturn.put("TERRITORYDETAILS", hmResult);
      if (!strRepId.equals("")) {
        alResult =
            gmTerr.getAccountsList(GmCommonClass.parseNull(strRepId), "REP", "", strCompLangId);
        hmReturn.put("ACCOUNTSLIST", alResult);

        alResult = getQuotaBreakup(strRepId, strYear, strCurrTypeId);
        hmReturn.put("QUOTA", alResult);
      }


    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadTerritoryDetails", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadTerritoryDetails


  /**
   * getQuotaBreakup - This method will
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList getQuotaBreakup(String strRepId, String strYear, String strCurrTypeId)
      throws AppError {
    ArrayList alReturn = new ArrayList();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C709_QUOTA_BREAKUP_ID ID, to_char(C709_START_DATE,'mm/dd/yyyy') SDATE, ");
      sbQuery.append(" to_char(C709_END_DATE,'mm/dd/yyyy') EDATE, DECODE('" + strCurrTypeId
          + "',105460,C709_QUOTA_BREAKUP_AMT_BASE,C709_QUOTA_BREAKUP_AMT) AMT,");
      sbQuery.append(" to_char(C709_START_DATE,'yyyy') YEAR");
      sbQuery.append(" FROM T709_QUOTA_BREAKUP ");
      sbQuery.append(" WHERE c703_sales_rep_id =");
      sbQuery.append(strRepId);
      sbQuery.append(" AND C901_TYPE = 20750 ");
      /*
       * sbQuery.append(" AND C709_START_DATE BETWEEN to_date('01/01/"); sbQuery.append(strYear);
       * sbQuery.append("','mm/dd/yyyy') and to_date('12/31/"); sbQuery.append(strYear);
       * sbQuery.append("','mm/dd/yyyy') ");
       */
      sbQuery.append(" ORDER BY C709_START_DATE ");

      log.debug("quote data qry ------" + sbQuery);

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      gmDBManager.close();


    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:getQuotaBreakup", "Exception is:" + e);
    }
    return alReturn;
  } // End of getQuotaBreakup

  /**
   * fetchReportDistributorList - This method will
   * 
   * @return RowSetDynaClass
   * @exception AppError
   **/

  public RowSetDynaClass fetchReportDistributorList(HashMap hmParam) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    RowSetDynaClass alReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    String strCompanyId = getGmDataStoreVO().getCmpid();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCategories = null;
    String strRegions = null;
    String strStates = null;
    String strActive = null;
    String strSalesFilter = "";

    String strCmpLangId = GmCommonClass.parseNull((String) hmParam.get("COMP_LANG_ID"));
    String strDistNmColumn =
        gmSalesFilterConditionBean.getDistNameFilterClause(strCmpLangId, "T701");

    if (hmParam != null) {
      strCategories =
          GmCommonClass.getStringWithQuotes(GmCommonClass.createInputString((String[]) hmParam
              .get("SELECTEDCATEGORIES")));
      strStates =
          GmCommonClass.getStringWithQuotes(GmCommonClass.createInputString((String[]) hmParam
              .get("SELECTEDSTATES")));
      strActive = GmCommonClass.parseNull((String) hmParam.get("SELECTEDACTIVE"));
      strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("SALESFILTER"));
    }
    sbQuery.append(" SELECT t701.C701_DISTRIBUTOR_ID DISTID," + strDistNmColumn + " DISTNAME, ");
    sbQuery
        .append("t701.c701_ship_add1|| ' ' ||c701_ship_add2 ADDRESS,NVL(t701.c701_ship_city,' ') City, ");
    sbQuery
        .append("  GET_CODE_NAME(t701.C701_REGION) REGION,  GET_CODE_NAME(t701.C901_DISTRIBUTOR_TYPE) DISTTYPE, ");
    sbQuery.append(" GET_CODE_NAME(t701.C701_SHIP_STATE) STATE, ");
    sbQuery.append(" decode(t701.C701_ACTIVE_FL,'Y','Active','N','Inactive') STATUS ");
    sbQuery
        .append(" FROM T701_distributor t701, (SELECT DISTINCT D_ID FROM v700_territory_mapping_detail v700 ");
    if (!strSalesFilter.equals("")) {
      sbQuery.append(" WHERE ");
      sbQuery.append(strSalesFilter);
    }
    sbQuery.append(" ) v700 ");
    sbQuery.append(" WHERE t701.C701_VOID_FL IS NULL ");
    sbQuery.append(" AND t701.C701_DISTRIBUTOR_ID = V700.D_ID ");

    if (!strCategories.equals("")) {
      sbQuery.append(" AND t701.C901_DISTRIBUTOR_TYPE IN ('");
      sbQuery.append(strCategories);
      sbQuery.append("') ");
    }
    if (!strStates.equals("")) {
      sbQuery.append(" AND t701.C701_SHIP_STATE IN ('");
      sbQuery.append(strStates);
      sbQuery.append("') ");
    }
    if (!strActive.equals("")) {
      if (strActive.equalsIgnoreCase("on")) {
        strActive = "Y";
      }
      sbQuery.append(" AND t701.C701_ACTIVE_FL = '");
      sbQuery.append(strActive);
      sbQuery.append("' ");
    }
    sbQuery.append(" AND t701.C1900_COMPANY_ID = ");
    sbQuery.append("'" + strCompanyId + "'");
    sbQuery.append(" ORDER BY DISTNAME ");
    log.debug("GmTerritoryBean.fetchReportDistributorList-- sbQuery " + sbQuery.toString());
    alReturn = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return alReturn;
  }


}
