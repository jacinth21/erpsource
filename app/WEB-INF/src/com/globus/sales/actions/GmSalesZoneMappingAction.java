package com.globus.sales.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.beans.GmSalesMappingBean;
import com.globus.sales.forms.GmSalesZoneMappingForm;

public class GmSalesZoneMappingAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    // TODO Auto-generated method stub
    Logger log = GmLogger.getInstance(this.getClass().getName());
    log.debug("GmSalesZoneMappingAction");
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    String strOpt = "";
    String strFrdPage = "GmZoneSetup";
    String strZoneID = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean();
    GmSalesZoneMappingForm gmSalesZoneMappingForm = (GmSalesZoneMappingForm) form;
    gmSalesZoneMappingForm.loadSessionParameters(request);
    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmSalesZoneMappingForm));
    strOpt = GmCommonClass.parseNull(gmSalesZoneMappingForm.getStrOpt());
    if (strOpt.equals("save")) {
      strZoneID = GmCommonClass.parseNull(gmSalesMappingBean.saveZone(hmParam));
      gmSalesZoneMappingForm.setZoneID(strZoneID);
      strOpt = "edit";
      gmSalesZoneMappingForm.setStrOpt(strOpt);
    } else if (strOpt.equals("edit")) {
      hmReturn = GmCommonClass.parseNullHashMap(gmSalesMappingBean.fetchZone(hmParam));
      gmSalesZoneMappingForm.setZoneID(GmCommonClass.parseNull((String) hmReturn.get("ZONE_ID")));
      gmSalesZoneMappingForm.setZoneNM(GmCommonClass.parseNull((String) hmReturn.get("ZONE_NAME")));
    } else if (strOpt.equals("report")) {
      alReturn = GmCommonClass.parseNullArrayList(gmSalesMappingBean.fetchZoneList());
      gmSalesZoneMappingForm.setGridXmlData(generateOutPut(alReturn, strSessCompanyLocale));
      strFrdPage = "GmZoneReport";
    }
    gmSalesZoneMappingForm.setStrOpt(strOpt);
    return mapping.findForward(strFrdPage);
  }

  /**
   * getXmlGridData - This method will be used to generate Output for grid data.
   * 
   * @return String - will be used by dhtmlx grid
   * @exception AppError
   */
  private String generateOutPut(ArrayList alParam, String strSessCompanyLocale) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.GmZoneReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmZoneReport.vm");
    templateUtil.setTemplateSubDir("sales/templates");

    return templateUtil.generateOutput();
  }


}
