package com.globus.sales.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.util.StringUtils;

import com.globus.sales.forms.GmSalesEfficiencyScoreCardForm;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardRptBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;

/**
* GmSalesEfficiencyScoreCardAction : Contains the methods used for the Efficiency Score By System, Field Sales and Area Director (AD) score card Report screen
* author : Raja
*/
public class GmSalesEfficiencyScoreCardAction extends GmSalesDispatchAction {

	/**
	 * Code to Initialize the Logger Class.
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * loadSystemScoreCardDtls - used to load the system score card screen
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
    public ActionForward loadSystemScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
    	  GmSalesEfficiencyScoreCardForm gmSalesEfficiencyScoreCardForm = (GmSalesEfficiencyScoreCardForm) form;
    	  gmSalesEfficiencyScoreCardForm.loadSessionParameters(request);
    	  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
    	  gmSalesEfficiencyScoreCardForm.setScreenName("Efficiency Score By System");
    	  gmSalesEfficiencyScoreCardForm.setScreenType("SYSTEM");
    	  gmSalesEfficiencyScoreCardForm.setStrOpt("/gmSalesEfficiencyScoreCard.do?method=loadSystemScoreCardDtls");
	      
          HashMap hmSalesFilters = new HashMap();
          String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
          String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
          
          strFilterCondition = getAccessFilter(request, response, false);
		  hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);	
		  //below method used to set the more filter values to cookies and request.
		  strFilterCondition = getAccessFilter(request, response, true);
		  
		  request.setAttribute("hmSalesFilters", hmSalesFilters);
    	  
        return mapping.findForward("GmSalesEfficiencyScoreCardReport");
    }
	
    /**
     * fetchSystemScoreCardDtls - used to fetch the system score card details
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward fetchSystemScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
  	      HttpServletResponse response) throws Exception {
    	  GmSalesEfficiencyScoreCardForm gmSalesEfficiencyScoreCardForm = (GmSalesEfficiencyScoreCardForm) form;
 	      gmSalesEfficiencyScoreCardForm.loadSessionParameters(request);
 	      GmSalesEfficiencyScoreCardRptBean gmSalesEfficiencyScoreCardRptBean = new GmSalesEfficiencyScoreCardRptBean(getGmDataStoreVO());
 	      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
 	     	    
  	      String strJSONGridData = "";
  	      HashMap hmParam = new HashMap();
  	      HashMap hmSalesFilters = new HashMap();
          String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
          String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
          // To get the Access Level information
		  strFilterCondition = getAccessFilter(request, response, true);
		  
    	  hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardForm);
    	  hmParam.put("AccessFilter", strFilterCondition);
    	  
	  	  strJSONGridData = gmSalesEfficiencyScoreCardRptBean.fetchSystemScoreCardDtls(hmParam);
		  response.setContentType("text/json");
		  PrintWriter pw = response.getWriter();
		  if (!strJSONGridData.equals("")) {
			 pw.write(strJSONGridData);
			}
		  pw.flush();
					
	    return null;
    }
	
    /**
     * loadFieldSalesScoreCardDtls  - used to load the Field Sales score card screen
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward loadFieldSalesScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
  	      HttpServletResponse response) throws Exception {
    	  GmSalesEfficiencyScoreCardForm gmSalesEfficiencyScoreCardForm = (GmSalesEfficiencyScoreCardForm) form;
  	      gmSalesEfficiencyScoreCardForm.loadSessionParameters(request);
  	      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
  	      gmSalesEfficiencyScoreCardForm.setScreenName("Efficiency Score By Field Sales");
  	      gmSalesEfficiencyScoreCardForm.setScreenType("FS");
  	      gmSalesEfficiencyScoreCardForm.setStrOpt("/gmSalesEfficiencyScoreCard.do?method=loadFieldSalesScoreCardDtls");
	      
          HashMap hmSalesFilters = new HashMap();
          String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
          String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
        
          strFilterCondition = getAccessFilter(request, response, false);
		  hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
		  //below method used to set the more filter values to cookies and request.
		  strFilterCondition = getAccessFilter(request, response, true);
		  
		  request.setAttribute("hmSalesFilters", hmSalesFilters);
		    
		 return mapping.findForward("GmSalesEfficiencyScoreCardReport");
      }
    
    
    /**
     * fetchFieldSalesScoreCardDtls - used to fetch the Field Sales score card details
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward fetchFieldSalesScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    	  HttpServletResponse response) throws Exception {
	      GmSalesEfficiencyScoreCardForm gmSalesEfficiencyScoreCardForm = (GmSalesEfficiencyScoreCardForm) form;
		  gmSalesEfficiencyScoreCardForm.loadSessionParameters(request);
		  GmSalesEfficiencyScoreCardRptBean gmSalesEfficiencyScoreCardRptBean = new GmSalesEfficiencyScoreCardRptBean(getGmDataStoreVO());
		  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
				
		  String strJSONGridData = "";
		  HashMap hmParam = new HashMap();
		  HashMap hmSalesFilters = new HashMap();
          String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
          String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
          // To get the Access Level information
		  strFilterCondition = getAccessFilter(request, response, true);

	      hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardForm);
	      hmParam.put("AccessFilter", strFilterCondition); 
	      
		  strJSONGridData = gmSalesEfficiencyScoreCardRptBean.fetchFieldSalesScoreCardDtls(hmParam);
		  response.setContentType("text/json");
		  PrintWriter pw = response.getWriter();
		  if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		  }
		  pw.flush();
		 return null;
    }
    
    /**
     * loadADScoreCardDtls  - used to load the Area Director (AD) score card screen
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward loadADScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
  	      HttpServletResponse response) throws Exception {
	      GmSalesEfficiencyScoreCardForm gmSalesEfficiencyScoreCardForm = (GmSalesEfficiencyScoreCardForm) form;
		  gmSalesEfficiencyScoreCardForm.loadSessionParameters(request);
		  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		  gmSalesEfficiencyScoreCardForm.setScreenName("Efficiency Score By AD");
    	  gmSalesEfficiencyScoreCardForm.setScreenType("AD");
    	  gmSalesEfficiencyScoreCardForm.setStrOpt("/gmSalesEfficiencyScoreCard.do?method=loadADScoreCardDtls");
		    
		  String strFilterCondition = "";
		  // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
		  String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
    	  HashMap hmSalesFilters = new HashMap();
    	 
    	  strFilterCondition = getAccessFilter(request, response, false);
      	  hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      	  //below method used to set the more filter values to cookies and request.
		  strFilterCondition = getAccessFilter(request, response, true);
      	  request.setAttribute("hmSalesFilters", hmSalesFilters);
		    
   	    return mapping.findForward("GmSalesEfficiencyScoreCardReport");
       }
    
    /**
     * fetchADScoreCardDtls  - used to fetch the Area Director (AD) score card details
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward fetchADScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
    	  HttpServletResponse response) throws Exception {
		  GmSalesEfficiencyScoreCardForm gmSalesEfficiencyScoreCardForm = (GmSalesEfficiencyScoreCardForm) form;
		  gmSalesEfficiencyScoreCardForm.loadSessionParameters(request);
		  GmSalesEfficiencyScoreCardRptBean gmSalesEfficiencyScoreCardRptBean = new GmSalesEfficiencyScoreCardRptBean(getGmDataStoreVO());
		  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		  
	      String strJSONGridData = "";
  	      HashMap hmParam = new HashMap();
  	      HashMap hmSalesFilters = new HashMap();
          String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
          String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
          // To get the Access Level information
		  strFilterCondition = getAccessFilter(request, response, true);
		  
    	  hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardForm);
    	  hmParam.put("AccessFilter", strFilterCondition);
    	  
	  	  strJSONGridData = gmSalesEfficiencyScoreCardRptBean.fetchADScoreCardDtls(hmParam);
		  response.setContentType("text/json");
		  PrintWriter pw = response.getWriter();
		  if (!strJSONGridData.equals("")) {
			 pw.write(strJSONGridData);
			}
		  pw.flush();
		 return null;
    }
}
