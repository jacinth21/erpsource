package com.globus.sales.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.util.StringUtils;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.beans.GmTerritoryBean;
import com.globus.sales.forms.GmDistributorReportForm;


public class GmDistributorReportAction extends GmSalesDispatchAction {


  String[] selectedCategories = new String[5];
  String[] selectedRegions = new String[5];
  String[] selectedStates = new String[5];

  ArrayList allCategories = new ArrayList();
  ArrayList allRegions = new ArrayList();
  ArrayList allStates = new ArrayList();
  String selectedActive = "";

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDistributorReportForm gmDistributorReportForm = (GmDistributorReportForm) form;
    gmDistributorReportForm.loadSessionParameters(request);

    Logger log = GmLogger.getInstance(this.getClass().getName());

    GmFramework gmFramework = new GmFramework();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCommonClass gmCommonClass = new GmCommonClass();
    GmTerritoryBean gmTerritoryBean = new GmTerritoryBean(getGmDataStoreVO());
    ArrayList allCategories = gmCommonClass.getCodeList("DSTYP");
    ArrayList allStates = gmCommonClass.getCodeList("STATE", getGmDataStoreVO());
    String defaultActive = "on";
    gmDistributorReportForm.setAllCategories(allCategories);
    gmDistributorReportForm.setAllStates(allStates);
    String strFilterCondition = "";
    HashMap hmSalesFilters = new HashMap();
    strFilterCondition = getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = getAccessFilter(request, response, true);
    request.setAttribute("hmSalesFilters", hmSalesFilters);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    String strOpt = gmDistributorReportForm.getStrOpt();
    log.debug(" stropt " + strOpt);
    if (strOpt.equals("")) {
      gmDistributorReportForm.setSelectedActive(defaultActive);
    }

    // Get from request or from cookie if not found
    selectedCategories = gmDistributorReportForm.getSelectedCategories();
    selectedStates = gmDistributorReportForm.getSelectedStates();
    selectedActive = gmDistributorReportForm.getSelectedActive();

    log.debug("Before settings =======> "
        + StringUtils.arrayToDelimitedString(selectedCategories, ",") + "/"
        + StringUtils.arrayToDelimitedString(selectedStates, ",") + "/" + selectedActive);

    if (request.getParameter("Comp") == null) {
      log.debug("request.getParameter(Comp) == null");
      // TODO Need to handle nulls
      String[] ckCategoriesTemp = new String[5];
      String[] ckStatesTemp = new String[5];
      ckCategoriesTemp =
          StringUtils.tokenizeToStringArray(GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cselectedCategories")), ",");
      ckStatesTemp =
          StringUtils.tokenizeToStringArray(
              GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cselectedStates")),
              ",");

      for (int i = 0; i < ckCategoriesTemp.length; i++) {
        if (ckCategoriesTemp[i] != null && !ckCategoriesTemp[i].equals("null")) {
          selectedCategories[i] = ckCategoriesTemp[i];
        }
      }

      for (int i = 0; i < ckStatesTemp.length; i++) {
        if (ckStatesTemp[i] != null && !ckStatesTemp[i].equals("null")) {
          selectedStates[i] = ckStatesTemp[i];
        }
      }
      selectedActive =
          GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cselectedActive"));
      gmDistributorReportForm.setSelectedActive(selectedActive);
    } else {
      log.debug("request.getParameter(Comp) NOT null");
    }

    log.debug("After settings =======> "
        + StringUtils.arrayToDelimitedString(selectedCategories, ",") + "/"
        + StringUtils.arrayToDelimitedString(selectedStates, ",") + "/" + selectedActive);


    HashMap hmParam = new HashMap();
    hmParam.put("SELECTEDCATEGORIES", selectedCategories);
    hmParam.put("SELECTEDSTATES", selectedStates);
    hmParam.put("SELECTEDACTIVE", selectedActive);
    hmParam.put("SALESFILTER", strFilterCondition);

    gmFramework.getRequestParams(request, response, session, hmParam);

    RowSetDynaClass gmDistributorReportList = gmTerritoryBean.fetchReportDistributorList(hmParam);
    // gmDistributorReportForm.setReportResultList(gmDistributorReportList.getRows());
    String strXmlData =
        generateOutPut((ArrayList) gmDistributorReportList.getRows(), strSessCompanyLocale);
    gmDistributorReportForm.setStrXmlString(strXmlData);
    // request.setAttribute("gridData", strXmlData);
    log.debug(" values for gmDistributorReportList " + gmDistributorReportList);

    // Set Cookies
    log.debug("Saving settings =======> "
        + StringUtils.arrayToDelimitedString(selectedCategories, ",") + "/"
        + StringUtils.arrayToDelimitedString(selectedStates, ",") + "/" + selectedActive);
    gmCookieManager.setCookieValue(response, "cselectedCategories",
        StringUtils.arrayToDelimitedString(selectedCategories, ","));
    gmCookieManager.setCookieValue(response, "cselectedStates",
        StringUtils.arrayToDelimitedString(selectedStates, ","));
    gmCookieManager.setCookieValue(response, "cselectedActive", selectedActive);

    return mapping.findForward("reportDistributor");
  }

  // @SuppressWarnings("unchecked")
  private String generateOutPut(ArrayList alGridData, String strSessCompanyLocale) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.GmDistributorReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmDistributorReport.vm");
    return templateUtil.generateOutput();
  }
}
