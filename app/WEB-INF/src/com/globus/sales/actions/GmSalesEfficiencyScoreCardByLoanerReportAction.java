package com.globus.sales.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.sales.forms.GmSalesEfficiencyScoreCardByLoanerReportForm;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardByLoanerReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;

/**
* GmSalesEfficiencyScoreCardByLoanerReportAction : Contains the methods used for the Efficiency Score By Exp.Loaner usage Report screen
* author : Raja
*/
public class GmSalesEfficiencyScoreCardByLoanerReportAction extends GmSalesDispatchAction {

	/**
	 * Code to Initialize the Logger Class.
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * loadLoanerUsageScoreCardDtls - used to load the Exp. Loaner usage screen
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
    public ActionForward loadLoanerUsageScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
    	  instantiate(request, response);
    	  GmSalesEfficiencyScoreCardByLoanerReportForm gmSalesEfficiencyScoreCardByLoanerReportForm = (GmSalesEfficiencyScoreCardByLoanerReportForm) form;
    	  gmSalesEfficiencyScoreCardByLoanerReportForm.loadSessionParameters(request);
    	  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
	      
          HashMap hmSalesFilters = new HashMap();
          String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
          String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
          strFilterCondition = getAccessFilter(request, response, false);
		  hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);	
		  //below method used to set the more filter values to cookies and request.
		  strFilterCondition = getAccessFilter(request, response, true);
		  
		  request.setAttribute("hmSalesFilters", hmSalesFilters);
    	  
        return mapping.findForward("GmSalesEfficiencyScoreCardByLoanerReport");
    }                               
	
    /**
     * fetchConUsageScoreCardDtls - used to fetch the Exp.Consignment usage report details
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward fetchLoanerUsageScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
  	      HttpServletResponse response) throws Exception {
    	  instantiate(request, response);
    	  GmSalesEfficiencyScoreCardByLoanerReportForm gmSalesEfficiencyScoreCardByLoanerReportForm = (GmSalesEfficiencyScoreCardByLoanerReportForm) form;
    	  gmSalesEfficiencyScoreCardByLoanerReportForm.loadSessionParameters(request);
 	      GmSalesEfficiencyScoreCardByLoanerReportBean gmSalesEfficiencyScoreCardByLoanerReportBean = new GmSalesEfficiencyScoreCardByLoanerReportBean(getGmDataStoreVO());
 	      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
 	     	    
  	      String strJSONGridData = "";
  	      String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
  	      String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
  	      HashMap hmParam = new HashMap();
  	      HashMap hmSalesFilters = new HashMap();
        
          // To get the Access Level information
		  strFilterCondition = getAccessFilter(request, response, true);
		  
    	  hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardByLoanerReportForm);
    	  hmParam.put("AccessFilter", strFilterCondition);
    	  
	  	  strJSONGridData = gmSalesEfficiencyScoreCardByLoanerReportBean.fetchLoanerUsageScoreCardDtls(hmParam);
		  response.setContentType("text/json");
		  PrintWriter pw = response.getWriter();
		  if (!strJSONGridData.equals("")) {
			 pw.write(strJSONGridData);
			}
		  pw.flush();
					
	    return null;
    }
}
