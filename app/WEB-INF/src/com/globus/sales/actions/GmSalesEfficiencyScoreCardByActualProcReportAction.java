package com.globus.sales.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardByActualProcReportBean;
import com.globus.sales.forms.GmSalesEfficiencyScoreCardByActualProcReportForm;
/**
 * GmSalesEfficiencyScoreCardAction : Contains the methods used for the Actual Procedures score card Report screen
 * author : MKosalram
 */
public class GmSalesEfficiencyScoreCardByActualProcReportAction extends GmSalesDispatchAction{

	/**
	 * Code to Initialize the Logger Class.
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

	/**
	 * loadActualProcUsageScoreCardDtls - used to load the Actual Procedures Report
	 * score card screen
	 * 
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */

	public ActionForward loadActualProcUsageScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {

		GmSalesEfficiencyScoreCardByActualProcReportForm gmSalesEfficiencyScoreCardByActualProcReportForm = (GmSalesEfficiencyScoreCardByActualProcReportForm) form;
		gmSalesEfficiencyScoreCardByActualProcReportForm.loadSessionParameters(request);
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		gmSalesEfficiencyScoreCardByActualProcReportForm.setScreenName("Actual Procedures Report");

		HashMap hmSalesFilters = new HashMap();
		String strFilterCondition = "";
		// PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
		String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
		request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
		strFilterCondition = getAccessFilter(request, response, false);
		hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
		// below method used to set the more filter values to cookies and request.
		strFilterCondition = getAccessFilter(request, response, true);

		request.setAttribute("hmSalesFilters", hmSalesFilters);

		return mapping.findForward("GmActualProceduresScoreCardReport");
	}


	/**
	 * fetchActualProcUsageScoreCardDtls - used to fetch the Actual Procedure score card details
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward  fetchActualProcUsageScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmSalesEfficiencyScoreCardByActualProcReportForm gmSalesEfficiencyScoreCardByActualProcReportForm = (GmSalesEfficiencyScoreCardByActualProcReportForm) form;
		gmSalesEfficiencyScoreCardByActualProcReportForm.loadSessionParameters(request);
		GmSalesEfficiencyScoreCardByActualProcReportBean gmSalesEfficiencyScoreCardByActualProcReportBean = new GmSalesEfficiencyScoreCardByActualProcReportBean(getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		HashMap hmSalesFilters = new HashMap();
		String strFilterCondition = "";
		// PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
		String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
		request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);


		// To get the Access Level information
		strFilterCondition = getAccessFilter(request, response, true);

		hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardByActualProcReportForm);
		hmParam.put("AccessFilter", strFilterCondition);

		strJSONGridData = gmSalesEfficiencyScoreCardByActualProcReportBean.fetchActualProcUsageScoreCardDtls(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();

		return null;
	}

	/**
	 * loadBorrowTagScoreCardDtls - used to load the Borrow Tag Information
	 * score card screen
	 * 
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */

	public ActionForward loadBorrowTagScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmSalesEfficiencyScoreCardByActualProcReportForm gmSalesEfficiencyScoreCardByActualProcReportForm = (GmSalesEfficiencyScoreCardByActualProcReportForm) form;
		gmSalesEfficiencyScoreCardByActualProcReportForm.loadSessionParameters(request);
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		gmSalesEfficiencyScoreCardByActualProcReportForm.setScreenName("Borrowed Tag Information");

		HashMap hmSalesFilters = new HashMap();
		String strFilterCondition = "";
		// PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
		String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
		request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);

		strFilterCondition = getAccessFilter(request, response, false);
		hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
		// below method used to set the more filter values to cookies and request.
		strFilterCondition = getAccessFilter(request, response, true);

		request.setAttribute("hmSalesFilters", hmSalesFilters);

		return mapping.findForward("GmBorrowTagScoreCardDetails");
	}

	/**
	 * fetchBorrowTagScoreCardDtls - used to fetch the Borrow Tag ScoreCard Details
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward  fetchBorrowTagScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmSalesEfficiencyScoreCardByActualProcReportForm gmSalesEfficiencyScoreCardByActualProcReportForm = (GmSalesEfficiencyScoreCardByActualProcReportForm) form;
		gmSalesEfficiencyScoreCardByActualProcReportForm.loadSessionParameters(request);
		GmSalesEfficiencyScoreCardByActualProcReportBean gmSalesEfficiencyScoreCardByActualProcReportBean = new GmSalesEfficiencyScoreCardByActualProcReportBean(getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		HashMap hmSalesFilters = new HashMap();
		String strFilterCondition = "";
		// PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
		String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
		request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);

		// To get the Access Level information
		strFilterCondition = getAccessFilter(request, response, true);

		hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardByActualProcReportForm);
		hmParam.put("AccessFilter", strFilterCondition);

		strJSONGridData = gmSalesEfficiencyScoreCardByActualProcReportBean.fetchBorrowTagScoreCardDtls(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();

		return null;
	}



	/**
	 * loadBulkDOScoreCardDtls - used to load the Bulk DO score card
	 * screen
	 * 
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */

	public ActionForward loadBulkDOScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmSalesEfficiencyScoreCardByActualProcReportForm gmSalesEfficiencyScoreCardByActualProcReportForm = (GmSalesEfficiencyScoreCardByActualProcReportForm) form;
		gmSalesEfficiencyScoreCardByActualProcReportForm.loadSessionParameters(request);
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		gmSalesEfficiencyScoreCardByActualProcReportForm.setScreenName("Bulk DO");

		HashMap hmSalesFilters = new HashMap();
		String strFilterCondition = "";
		// PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
		String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
		request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);

		strFilterCondition = getAccessFilter(request, response, false);
		hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
		// below method used to set the more filter values to cookies and request.
		strFilterCondition = getAccessFilter(request, response, true);

		request.setAttribute("hmSalesFilters", hmSalesFilters);

		return mapping.findForward("GmSalesEfficiencyScoreCardByBulkDO");
	}

	/**
	 * fetchBulkDOScoreCardDtls - used to fetch the Bulk DO ScoreCard Details
	 * 
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchBulkDOScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmSalesEfficiencyScoreCardByActualProcReportForm gmSalesEfficiencyScoreCardByActualProcReportForm = (GmSalesEfficiencyScoreCardByActualProcReportForm) form;
		gmSalesEfficiencyScoreCardByActualProcReportForm.loadSessionParameters(request);
		GmSalesEfficiencyScoreCardByActualProcReportBean gmSalesEfficiencyScoreCardByActualProcReportBean = new GmSalesEfficiencyScoreCardByActualProcReportBean(
				getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		HashMap hmSalesFilters = new HashMap();
		String strFilterCondition = "";
		// PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
		String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
		request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);

		// To get the Access Level information
		strFilterCondition = getAccessFilter(request, response, true);

		hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardByActualProcReportForm);
		hmParam.put("AccessFilter", strFilterCondition);

		strJSONGridData = gmSalesEfficiencyScoreCardByActualProcReportBean.fetchBulkDOScoreCardDtls(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();

		return null;
	}

}