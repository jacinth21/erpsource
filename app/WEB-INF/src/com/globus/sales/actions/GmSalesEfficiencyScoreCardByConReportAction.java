package com.globus.sales.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.util.StringUtils;

import com.globus.sales.forms.GmSalesEfficiencyScoreCardByConReportForm;
import com.globus.sales.beans.GmSalesEfficiencyScoreCardByConReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;

/**
* GmSalesEfficiencyScoreCardByConReportAction : Contains the methods used for the Efficiency Score By Exp.Consignment usage Report screen
* author : Raja
*/
public class GmSalesEfficiencyScoreCardByConReportAction extends GmSalesDispatchAction {

	/**
	 * Code to Initialize the Logger Class.
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * loadConUsageScoreCardDtls - used to load the Exp.Consignment usage screen
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
    public ActionForward loadConUsageScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
    	  GmSalesEfficiencyScoreCardByConReportForm gmSalesEfficiencyScoreCardByConReportForm = (GmSalesEfficiencyScoreCardByConReportForm) form;
    	  gmSalesEfficiencyScoreCardByConReportForm.loadSessionParameters(request);
    	  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
	      
          HashMap hmSalesFilters = new HashMap();
          String strFilterCondition = "";
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
          String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
          request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
         strFilterCondition = getAccessFilter(request, response, false);
	     hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);	
		  //below method used to set the more filter values to cookies and request.
		 strFilterCondition = getAccessFilter(request, response, true);
		  
	     request.setAttribute("hmSalesFilters", hmSalesFilters);
    	  
        return mapping.findForward("GmSalesEfficiencyScoreCardByConReport");
    }
	
    /**
     * fetchConUsageScoreCardDtls - used to fetch the Exp.Consignment usage report details
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward fetchConUsageScoreCardDtls(ActionMapping mapping, ActionForm form, HttpServletRequest request,
  	      HttpServletResponse response) throws Exception {
    	  GmSalesEfficiencyScoreCardByConReportForm gmSalesEfficiencyScoreCardByConReportForm = (GmSalesEfficiencyScoreCardByConReportForm) form;
    	  gmSalesEfficiencyScoreCardByConReportForm.loadSessionParameters(request);
 	      GmSalesEfficiencyScoreCardByConReportBean gmSalesEfficiencyScoreCardByConReportBean = new GmSalesEfficiencyScoreCardByConReportBean(getGmDataStoreVO());
 	      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());  
 	     	    
  	      String strJSONGridData = "";
  	      String strFilterCondition = "";
  	      HashMap hmParam = new HashMap();
  	      HashMap hmSalesFilters = new HashMap();
          // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
  	      String strSalesFilterFullAcc = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessSalesFullAccess"));
  	      request.setAttribute("strSalesFilterFullAcc", strSalesFilterFullAcc);
          // To get the Access Level information
		  strFilterCondition = getAccessFilter(request, response, true);
		  
    	  hmParam = GmCommonClass.getHashMapFromForm(gmSalesEfficiencyScoreCardByConReportForm);
    	  hmParam.put("AccessFilter", strFilterCondition);
    	  
	  	  strJSONGridData = gmSalesEfficiencyScoreCardByConReportBean.fetchConUsageScoreCardDtls(hmParam);
		  response.setContentType("text/json");
		  PrintWriter pw = response.getWriter();
		  if (!strJSONGridData.equals("")) {
			 pw.write(strJSONGridData);
			}
		  pw.flush();
					
	    return null;
    }
}
