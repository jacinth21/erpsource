package com.globus.sales.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.beans.GmSalesMappingBean;
import com.globus.sales.forms.GmRegionSetupForm;

public class GmRegionMappingAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError {

    GmRegionSetupForm gmRegionForm = (GmRegionSetupForm) form;
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean();
    gmRegionForm.loadSessionParameters(request);
    String strOpt = GmCommonClass.parseNull(gmRegionForm.getStrOpt());
    String strDispacthTo = "success";
    String strRegId = gmRegionForm.getReg_id();
    log.debug("Region >>>>>>> "+ strRegId);
    log.debug("strOpt >>>>>>> "+ strOpt);
//    PC-3478 PC Changes
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    /*Get code Lookup drop-down values*/
    ArrayList alComapany = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COMP"));
    alComapany.remove(0); // remove "All" Company Type
    gmRegionForm.setAlCompany(alComapany);
    gmRegionForm.setAlDivision(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DIV")));
    gmRegionForm.setAlCountry(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CNTY")));
    gmRegionForm.setAlZone(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ZONE")));
    
    if (strOpt.equals("report")) {
      ArrayList alReport = gmSalesMappingBean.fetchRegionList();
      String strXmlData = generateOutPut(alReport, strSessCompanyLocale);
      gmRegionForm.setGridXmlData(strXmlData);
      strDispacthTo = "report";
    } else if (strOpt.equals("Save")) {
      HashMap hmParams = GmCommonClass.getHashMapFromForm(gmRegionForm);
      strRegId = gmSalesMappingBean.saveRegion(hmParams);
      gmRegionForm.setReg_id(strRegId);
      strOpt = "Edit";
      
      // View refresh JMS called
      GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
      HashMap hmViewRefresh = new HashMap();
      hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
      hmViewRefresh.put("companyInfo",
          GmCommonClass.parseNull(request.getParameter("companyInfo")));

      String strViewConsumerClass =
          GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
      hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
      gmConsumerUtil.sendMessage(hmViewRefresh);

    }
    if (strOpt.equals("Edit")) {
      
      HashMap hmReturn = gmSalesMappingBean.fetchRegion(strRegId);
      gmRegionForm.setRegName(GmCommonClass.parseNull((String) hmReturn.get("REGION_NAME")));
      gmRegionForm.setAdType(GmCommonClass.parseNull((String) hmReturn.get("AD_ID")));
      gmRegionForm.setVpID(GmCommonClass.parseNull((String) hmReturn.get("VP_ID")));
      gmRegionForm.setZoneType(GmCommonClass.parseZero((String) hmReturn.get("ZONE_ID")));
      gmRegionForm.setDivisionType(GmCommonClass.parseZero((String) hmReturn.get("DIVISION_ID")));
      gmRegionForm.setCountryType(GmCommonClass.parseZero((String) hmReturn.get("COUNTRY_ID")));
      gmRegionForm.setCompanyType(GmCommonClass.parseZero((String) hmReturn.get("COMAPANY_ID")));
      gmRegionForm.setAdPlusNames(GmCommonClass.parseZero((String) hmReturn.get("ADPLUSUSERS")));
      gmRegionForm.setReg_id(strRegId);
    }
    gmRegionForm.setStrOpt(strOpt);
    
    gmRegionForm.setAlAD(GmCommonClass.parseNullArrayList(gmSalesMappingBean.fetchADList(strRegId)));
    gmRegionForm.setAlVP(GmCommonClass.parseNullArrayList(gmSalesMappingBean.fetchVPList(strRegId)));
    gmRegionForm.setAlADPlus(GmCommonClass.parseNullArrayList(gmSalesMappingBean.fetchADPlusList(strRegId)));
    
    return mapping.findForward(strDispacthTo);
  }

  private String generateOutPut(ArrayList alGridData, String strSessCompanyLocale) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.GmRegionReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmRegionReport.vm");
    return templateUtil.generateOutput();
  }
}
