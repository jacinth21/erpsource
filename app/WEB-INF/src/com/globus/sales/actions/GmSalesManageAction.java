package com.globus.sales.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.sales.beans.GmSalesManageBean;
import com.globus.sales.forms.GmSalesManageForm;

public class GmSalesManageAction extends GmSalesDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		instantiate(request, response);
		GmSalesManageForm gmSalesManageForm = (GmSalesManageForm) form;
		gmSalesManageForm.loadSessionParameters(request);
		GmSalesManageBean gmADReplacementBean = new GmSalesManageBean();
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmSalesManageForm);
		String strUserName = "";
		String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
		String strAction = GmCommonClass.parseNull(request
				.getParameter("hAction"));
		String strZoneId = GmCommonClass.parseNull(request
				.getParameter("zoneid"));
		String strRegionId = GmCommonClass.parseNull(request
				.getParameter("regionid"));
		String strRegionName = GmCommonClass.parseNull(request
				.getParameter("hRegionNm"));
		String strUserId = GmCommonClass.parseNull(request
				.getParameter("newadid"));
		String strZoneNm = GmCommonClass.parseNull(request
				.getParameter("hZoneNm"));
		String strUserNm = GmCommonClass.parseNull(request
				.getParameter("hUserNm"));
		String strURL = "/gmSalesManage.do?";
		String strDisplayNm = "AD Replacement Screen";
		String strFrdPage = "GmSalesReplaceAD";
		log.debug("strRegionId===========" + strRegionId);
		log.debug("strOpt=" + strOpt);

		if (strOpt.equals("ReplaceAD")) {
			if (strAction.equals("save")) { // Save AD Name

				gmADReplacementBean.adreplacement(hmParam);
				request.setAttribute("hType", "S");
				request.setAttribute("hRedirectURL", strURL);
				request.setAttribute("hDisplayNm", strDisplayNm);

				String msg = "Successfully Replace AD " + "<br>Zone Name : "
						+ strZoneNm + "<br>Region Name : " + strRegionName
						+ "<br>User Name :" + strUserNm;
				GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
				// View refresh JMS called
				HashMap hmViewRefresh = new HashMap();
				hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
				hmViewRefresh.put("companyInfo", GmCommonClass.parseNull(request
						.getParameter("companyInfo")));
				String strViewConsumerClass = GmCommonClass.parseNull(GmCommonClass
						.getString("VIEW_REFRESH_CONSUMER_CLASS"));
				hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
				gmConsumerUtil.sendMessage(hmViewRefresh);

				throw new AppError(msg, "", 'S');

			} else if (strAction.equals("createad")) {// Create Open AD
				if (strRegionName.indexOf("(") > 0) {
					strRegionName = strRegionName.substring(0,
							strRegionName.indexOf('('));
				}

				hmParam.put("REGIONNAME", strRegionName);
				strUserId = gmADReplacementBean.createAD(hmParam);
				hmParam.put("NEWADID", strUserId);
				gmADReplacementBean.adreplacement(hmParam);
				request.setAttribute("hType", "S");
				request.setAttribute("hRedirectURL", strURL);
				request.setAttribute("hDisplayNm", strDisplayNm);
				GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
				// View refresh JMS called
				HashMap hmViewRefresh = new HashMap();
				hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
				hmViewRefresh.put("companyInfo", GmCommonClass.parseNull(request
						.getParameter("companyInfo")));
				String strViewConsumerClass = GmCommonClass.parseNull(GmCommonClass
						.getString("VIEW_REFRESH_CONSUMER_CLASS"));
				hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
				gmConsumerUtil.sendMessage(hmViewRefresh);

				String msg = "Successfully Replace AD " + "<br>Zone Name : "
						+ strZoneNm + "<br>Region Name : " + strRegionName
						+ "<br>User Name :" + strRegionName+"-TBH";
				throw new AppError(msg, "", 'S');
			}
			
			// set Region and AD name
			if (strAction.equals("fetchRegion")) {
				strFrdPage = "GmSalesReplaceAD";

				String strJSON = "";
				ArrayList alRegion = GmCommonClass
						.parseNullArrayList(gmADReplacementBean.getRegionAD(
								strZoneId, "ReplaceAD"));
				gmSalesManageForm.setAlRegion(alRegion);
				GmWSUtil gmWSUtil = new GmWSUtil();
				strJSON = gmWSUtil.parseArrayListToJson(gmADReplacementBean
						.getRegionAD(strZoneId, "ReplaceAD"));
				if (!strJSON.equals("")) {
					response.setContentType("text/plain");
					PrintWriter pw = response.getWriter();
					pw.write(strJSON);
					pw.flush();
					return null;
				}

				strFrdPage = "GmSalesReplaceAD";
			}

		}

		// set zone and User List
		ArrayList alUser = GmCommonClass.parseNullArrayList(gmADReplacementBean
				.getUserList("ReplaceAD"));
		ArrayList alzone = GmCommonClass.parseNullArrayList(gmADReplacementBean
				.getZone());
		gmSalesManageForm.setAlZone(alzone);
		gmSalesManageForm.setAlNewAD(alUser);
		gmSalesManageForm.setAlUnSelectedDS(alUser);

		return mapping.findForward(strFrdPage);
	}

}
