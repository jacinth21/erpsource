package com.globus.sales.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmCookieManager;
import com.globus.sales.beans.GmSalesPerformanceReportBean;
import com.globus.sales.forms.GmSalesRunRateReportForm;

public class GmSalesRunRateReportDAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  // Get Settings from cookie
  private final GmCookieManager gmCookieManager = new GmCookieManager();

  public ActionForward salesRepRunRate(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    try {
      instantiate(request, response);
      HashMap hmValues = new HashMap();
      GmSalesRunRateReportForm gmSalesRunRateReportForm = (GmSalesRunRateReportForm) form;
      gmSalesRunRateReportForm.loadSessionParameters(request);
      GmSalesPerformanceReportBean gmSalesPerformanceReportBean =
          new GmSalesPerformanceReportBean(getGmDataStoreVO());
      GmFramework gmFramework = new GmFramework();


      String strAccessFilter = "";
      String strAccessCondition = "";

      // To get the Access Level information from Materialized view
      strAccessFilter = getAccessFilter(request, response, false);
      setDropDownValues(gmSalesRunRateReportForm, strAccessFilter, request);
      strAccessFilter = getAccessFilter(request, response, true);
      strAccessCondition = getAccessCondition(request, response, true);

      gmSalesRunRateReportForm.setAccessFilter(strAccessCondition);

      HashMap hmParam = GmCommonClass.getHashMapFromForm(gmSalesRunRateReportForm);

      getCheckBoxValuesFromForm(gmSalesRunRateReportForm, hmParam);
      gmFramework.getRequestParams(request, response, session, hmParam);
      log.debug(" checkbox values from form " + hmParam);
      log.debug(" values from i/p is " + hmParam);

      hmValues = gmSalesPerformanceReportBean.reportSalesRunRate(hmParam);
      String strMonthRRColumn = GmCommonClass.parseNull((String) hmValues.get("XMONTHRRCOLUMN"));
      String strShowInactiveFl = gmSalesRunRateReportForm.getShowInActiveRepsFlag();

      // Check cookie
      String strMonthRR = gmSalesRunRateReportForm.getMonthRR();
      strMonthRR =
          strMonthRR.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,
              "cMonthRR")) : strMonthRR;
      // If not found in cookie use default of 18
      strMonthRR = strMonthRR.equals("") ? "18" : strMonthRR;
      // Set Month Value
      gmSalesRunRateReportForm.setMonthRR(strMonthRR);

      gmCookieManager.setCookieValue(response, "cShowInActiveRepsFlag", strShowInactiveFl);
      String strShowInactiveFlTemp =
          GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, "cShowInActiveRepsFlag"));

      strShowInactiveFlTemp =
          strShowInactiveFlTemp.equals("") ? strShowInactiveFl : strShowInactiveFlTemp;
      gmSalesRunRateReportForm.setShowInActiveRepsFlag(strShowInactiveFlTemp);

      // Get Values for Sales/Hire From/To Dates
      String strSalesFromMonth = gmSalesRunRateReportForm.getSalesFromMonth();
      String strSalesFromYear = gmSalesRunRateReportForm.getSalesFromYear();
      String strSalesToMonth = gmSalesRunRateReportForm.getSalesToMonth();
      String strSalesToYear = gmSalesRunRateReportForm.getSalesToYear();
      String strHireFromMonth = gmSalesRunRateReportForm.getHireFromMonth();
      String strHireFromYear = gmSalesRunRateReportForm.getHireFromYear();
      String strHireToMonth = gmSalesRunRateReportForm.getHireToMonth();
      String strHireToYear = gmSalesRunRateReportForm.getHireToYear();

      strSalesFromMonth =
          strSalesFromMonth.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cSalesFromMonth")) : strSalesFromMonth;
      strSalesFromYear =
          strSalesFromYear.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cSalesFromYear")) : strSalesFromYear;
      strSalesToMonth =
          strSalesToMonth.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cSalesToMonth")) : strSalesToMonth;
      strSalesToYear =
          strSalesToYear.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cSalesToYear")) : strSalesToYear;
      strHireFromMonth =
          strHireFromMonth.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cHireFromMonth")) : strHireFromMonth;
      strHireFromYear =
          strHireFromYear.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cHireFromYear")) : strHireFromYear;
      strHireToMonth =
          strHireToMonth.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cHireToMonth")) : strHireToMonth;
      strHireToYear =
          strHireToYear.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              request, "cHireToYear")) : strHireToYear;

      gmSalesRunRateReportForm.setSalesFromMonth(strSalesFromMonth);
      gmSalesRunRateReportForm.setSalesFromYear(strSalesFromYear);
      gmSalesRunRateReportForm.setSalesToMonth(strSalesToMonth);
      gmSalesRunRateReportForm.setSalesToYear(strSalesToYear);
      gmSalesRunRateReportForm.setHireFromMonth(strHireFromMonth);
      gmSalesRunRateReportForm.setHireFromYear(strHireFromYear);
      gmSalesRunRateReportForm.setHireToMonth(strHireToMonth);
      gmSalesRunRateReportForm.setHireToYear(strHireToYear);


      gmSalesRunRateReportForm.setXmonthRRColumn(strMonthRRColumn);
      gmSalesRunRateReportForm.setHmReportData(hmValues);

      // Set Cookies
      gmCookieManager.setCookieValue(response, "cMonthRR", gmSalesRunRateReportForm.getMonthRR());


      // Date Ranges (Sales Date Filter)
      gmCookieManager.setCookieValue(response, "cSalesFromMonth",
          gmSalesRunRateReportForm.getSalesFromMonth(), GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cSalesFromYear",
          gmSalesRunRateReportForm.getSalesFromYear(), GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cSalesToMonth",
          gmSalesRunRateReportForm.getSalesToMonth(), GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cSalesToYear",
          gmSalesRunRateReportForm.getSalesToYear(), GmCookieManager.MAX_AGE_8);

      // Date Ranges (Hire Date Filter)
      gmCookieManager.setCookieValue(response, "cHireFromMonth",
          gmSalesRunRateReportForm.getHireFromMonth(), GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cHireFromYear",
          gmSalesRunRateReportForm.getHireFromYear(), GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cHireToMonth",
          gmSalesRunRateReportForm.getHireToMonth(), GmCookieManager.MAX_AGE_8);
      gmCookieManager.setCookieValue(response, "cHireToYear",
          gmSalesRunRateReportForm.getHireToYear(), GmCookieManager.MAX_AGE_8);

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("GmSalesRunRateReport");
  }

  public void setDropDownValues(GmSalesRunRateReportForm gmSalesRunRateReportForm,
      String strAccessFilter, HttpServletRequest request) {
    gmSalesRunRateReportForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    try {
      HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      ArrayList alArea = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REGN"));
      gmSalesRunRateReportForm.setAreaSize(String.valueOf(alArea.size()));

      gmSalesRunRateReportForm.setAlZone((ArrayList) hmSalesFilters.get("ZONE"));
      gmSalesRunRateReportForm.setAlArea(alArea);
      gmSalesRunRateReportForm.setAlFieldSalesType(GmCommonClass.getCodeList("SCAT", "1"));
      gmSalesRunRateReportForm.setAlRepDesignation(GmCommonClass.getCodeList("DSGTP"));
      gmSalesRunRateReportForm.setAlMonthList(GmCommonClass.getCodeList("MONTH"));
      gmSalesRunRateReportForm.setAlYearList(GmCommonClass.getCodeList("YEAR"));
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  public void getCheckBoxValuesFromForm(GmSalesRunRateReportForm gmSalesRunRateReportForm,
      HashMap hmParam) {
    String strSelectedZone =
        GmCommonClass.createInputString(gmSalesRunRateReportForm.getCheckZone());
    String strSelectedArea =
        GmCommonClass.createInputString(gmSalesRunRateReportForm.getCheckArea());
    String strSelectedFSType =
        GmCommonClass.createInputString(gmSalesRunRateReportForm.getCheckFieldSalesType());
    String strSelectedRepDesignation =
        GmCommonClass.createInputString(gmSalesRunRateReportForm.getCheckRepDesignation());

    hmParam.put("ZONE", strSelectedZone);
    hmParam.put("AREA", strSelectedArea);
    hmParam.put("FIELDSALESTYPE", strSelectedFSType);
    hmParam.put("REPDESIGNATION", strSelectedRepDesignation);
  }
}
