package com.globus.sales.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.beans.GmSalesSystemBean;
import com.globus.sales.forms.GmAIQuotaReportForm;

public class GmAIQuotaReportAction extends GmSalesDispatchAction {
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	    
	    public ActionForward quarterlyQuotaDrillDownByAD(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	                    HttpServletResponse response) 
	    {
	    	List rdQuarterlyQuotaDrillDownByAD = new ArrayList();
	    	ArrayList alDistributorList = new ArrayList();
	    	GmSalesSystemBean gmSalesSystemBean = new GmSalesSystemBean();
	    	GmAIQuotaReportForm gmAIQuotaReportForm = (GmAIQuotaReportForm)form;
	    	gmAIQuotaReportForm.loadSessionParameters(request);
	    	String strAccessFilter = "";
	    	String strComma = "";
	    	String strCheckFieldSalesList = "";
	    	
	    	try
	        {
	    		// To get the Access Level information from Materialized view
	    		strAccessFilter = getAccessFilter(request, response);
	    		gmAIQuotaReportForm.setAccessFilter(strAccessFilter);
	    		
	    		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmAIQuotaReportForm);
	    		strCheckFieldSalesList = GmCommonClass.createInputString(gmAIQuotaReportForm.getCheckFieldSales());
	    		if (strCheckFieldSalesList.length() > 0 ){
	                strComma = ",";
	            }
	            
	            strCheckFieldSalesList = strCheckFieldSalesList.concat(strComma).concat(gmAIQuotaReportForm.getFieldSales());
	    		strCheckFieldSalesList = GmCommonClass.getStringWithQuotes(strCheckFieldSalesList);
	    		hmParam.put("CHECKFIELDSALES", strCheckFieldSalesList);
	    		log.debug(" HashMap valies are " + hmParam);
	    		
	    		
	    		alDistributorList = gmSalesSystemBean.getDistributorList(strAccessFilter);
	    		
	        	rdQuarterlyQuotaDrillDownByAD = gmSalesSystemBean.fetchQuarterlyQuotaDrillDownByAD(hmParam).getRows();
	        	gmAIQuotaReportForm.setRdQuarterlyQuotaDrillDownByAD(rdQuarterlyQuotaDrillDownByAD);
	        	gmAIQuotaReportForm.setAlFieldSales(alDistributorList);
	        	request.setAttribute("hFilterDetails",hmParam);
	        }
	        catch(Exception ex)
	        {
	        	ex.printStackTrace();
	        	//throw new AppError(ex);
	        }
	    	
	        return mapping.findForward("GmAIQuarterQuotaDrillDown");
	    }
}
