  /*
   * GmSalesDispatchAction.java This class is used to add the Access filter condition based on the v700 view
   */ 
package com.globus.sales.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.filters.GmAccessFilter;

public class GmSalesDispatchAction extends GmDispatchAction {

  public GmSalesDispatchAction() {
    super();
    // TODO Auto-generated constructor stub
  }

  /*
   * This function is used to get filter condition and data based on the request specified the user
   * defaults filter based on access condition and adds other filter
   */
  @Override
  public String getAccessFilter(HttpServletRequest req, HttpServletResponse res) {
    // To get the Session Information
    HttpSession session = req.getSession(false);

    // Holds the where condition information
    StringBuffer strQuery = new StringBuffer();
    HashMap hmParam = new HashMap();

    // To get the Employee Information
    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    int intOverrideAccLevel =
        Integer.parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideAccLvl")));
    int intOverrideASSAccLevel =
        Integer
            .parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideASSAccLvl")));
    String strRptRepId = GmCommonClass.parseNull((String) session.getAttribute("strSessRptRepId"));

    String strOverrideAccessTo =
        GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsTo"));
    String strOverrideAccessId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsId"));
    String strOverrideAccessLvl =
        GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsLvl"));
    String strPrimaryCmpyId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    String strBBASalesAcsFl =
        GmCommonClass.parseNull((String) session.getAttribute("strSessBBASalesAcsFl"));

    String strAddnlCompFl =
            GmCommonClass.parseNull((String) session.getAttribute("strSessAddnlCompFl"));
    log.debug("strAddnlCompFl in salesdispatch action"+strAddnlCompFl);
    // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
    String strSalesFilterFullAccFl = GmCommonClass.parseNull((String) req.getAttribute("strSalesFilterFullAcc"));
    
    hmParam.put("PRIMARY_CMPY", strPrimaryCmpyId);
    hmParam.put("BBA_SALES_ACS_FL", strBBASalesAcsFl);
    hmParam.put("USER_ID", strEmpId);
    hmParam.put("ACCES_LVL", intAccLevel + "");
    hmParam.put("DEPT_ID", strDepartMentID);
    hmParam.put("OVERRIDE_ACCS_LVL", intOverrideAccLevel + "");
    hmParam.put("OVERRIDE_ASS_LVL", intOverrideASSAccLevel + "");
    hmParam.put("RPT_REPID", strRptRepId);
    hmParam.put("OVERRIDE_ACS_TO", strOverrideAccessTo);
    hmParam.put("OVERRIDE_ACS_ID", strOverrideAccessId);
    hmParam.put("OVERRIDE_ACS_LVL", strOverrideAccessLvl);
    hmParam.put("OVERRIDE_ACS_LVL", strOverrideAccessLvl);
    hmParam.put("ADDNL_COMP_IN_SALES", strAddnlCompFl);
    // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
    hmParam.put("SALES_FILTER_FULL_ACC_FL", strSalesFilterFullAccFl);    
    return this.getAccessFilter(hmParam);
  }

 /* Modification: Changing the IN clause to EXISTS for better performance
  * @ Author: gpalani. PMT-20656 Jun 2018
 */
  public String getAccessFilter(HashMap hmParam) {

    StringBuffer strQuery = new StringBuffer();
    GmCommonClass gmcc = new GmCommonClass();

    String strPrimaryCmpyId = GmCommonClass.parseNull((String) hmParam.get("PRIMARY_CMPY"));
    String strBBASalesAcsFl = GmCommonClass.parseNull((String) hmParam.get("BBA_SALES_ACS_FL"));
    String strAddnlCompFl = GmCommonClass.parseNull((String) hmParam.get("ADDNL_COMP_IN_SALES"));

    String strCompanyIds = "";
    String strEmpId = gmcc.parseNull((String) hmParam.get("USER_ID"));
    int intAccLevel = Integer.parseInt(gmcc.parseZero((String) hmParam.get("ACCES_LVL")));
    String strDepartMentID = gmcc.parseNull((String) hmParam.get("DEPT_ID"));
    int intOverrideAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ACCS_LVL")));
    int intOverrideASSAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ASS_LVL")));
    String strRptRepId = gmcc.parseNull((String) hmParam.get("RPT_REPID"));

    String strOverrideAccessTo = gmcc.parseNull((String) hmParam.get("OVERRIDE_ACS_TO"));
    String strOverrideAccessId = gmcc.parseNull((String) hmParam.get("OVERRIDE_ACS_ID"));
    int intSessOverrideAcsLvl =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ACS_LVL")));
    // PC-4014 Scorecard and Field Inventory Report full access (mschmitt)
    String strSalesFilterFullAccFl = GmCommonClass.parseNull((String) hmParam.get("SALES_FILTER_FULL_ACC_FL"));
    
    if (intOverrideAccLevel == 1) {
      intAccLevel = 2;
    }
    
    // PC-318: To remove the associate sales rep override access level 
    // To show the correct sales amount (Daily Sales/ Dashboard/ Quota reports)
    // So, Remove the below override access level if condition.
    
    /*if (intOverrideASSAccLevel == 7) {
      strEmpId = strRptRepId;
      intAccLevel = 1;
    }*/

    if (strOverrideAccessTo.equals("VP")) {
      // Assign VP Access
      intAccLevel = intSessOverrideAcsLvl;
      strDepartMentID = "S";
      strCompanyIds = strOverrideAccessId;
    } else if (strOverrideAccessTo.equals("AD")) {
      // AD - Assign AD Access for other than sales dept users from OUS
      // BBA - Assign override access for other than sales dept users from BBA
      intAccLevel = intSessOverrideAcsLvl;
      strDepartMentID = "S";
      strCompanyIds = strOverrideAccessId;
    }

    // *********************************************************
    // Will Execute the below Query if the Department is Sales
    // *********************************************************
    // PC-4014 Scorecard and Field Inventory Report full access (mschmitt) checking strSalesFilterFullAccFl value checking
    if (strDepartMentID.equals("S") && strSalesFilterFullAccFl.equals("")) {
      switch (intAccLevel) {

      // Sales Rep Filter Condition
        case 1: {
          strQuery.append(" V700.REP_ID = " + strEmpId);
          break;
        }

        // Distributor Filter Condition
        case 2: {
          strQuery.append("  EXISTS  ");
          strQuery.append("  ( SELECT T101S.C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM T101_USER T101S WHERE C101_USER_ID = " + strEmpId);
          strQuery.append("  AND T101S.C701_DISTRIBUTOR_ID = V700.D_ID )  ");
          log.debug("***GmSalesDispatchAction*** Access Level 2");

          break;
        }

        // AD Filter Condition
        case 3: {
          strQuery.append("  V700.AD_ID = " + strEmpId);
          log.debug("***GmSalesDispatchAction*** Access Level 3");

          break;
        }

        // VP Filter Condition
        case 4: {
          strQuery.append("  EXISTS  ");
          strQuery.append("  (SELECT  T708S.C901_REGION_ID ");
          strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S ");
          strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
          strQuery.append("  AND   T708S.C901_REGION_ID=V700.REGION_ID ");
          strQuery.append("  AND   T708S.C708_DELETE_FL IS NULL ");
          strQuery.append("  AND   T708S.C901_USER_ROLE_TYPE = 8001 )");
          log.debug("***GmSalesDispatchAction*** Access Level 4");

          break;
        }

        // other AD's like Kirk Tovey want to see other region
        case 6: {
        
          strQuery.append("  EXISTS  ");
          strQuery.append("  (SELECT  T708S.C901_REGION_ID ");
          strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S");
          strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
          strQuery.append("  AND T708S.C901_REGION_ID=V700.REGION_ID ");
          strQuery.append("  AND T708S.C708_DELETE_FL IS NULL ");
          strQuery.append("  AND T708S.C901_USER_ROLE_TYPE = 8002 )");
          log.debug("***GmSalesDispatchAction*** Access Level 6");

          break;
        }

        // ASS Filter condition
        case 7: {
          // PC-318: Associate sales rep - To change the query instead of
          // account to modified Sales rep to get the data.

          // To join the t704a and T704 table	
          strQuery.append("  EXISTS  ");
          strQuery.append("  ( SELECT distinct t704.c703_sales_rep_id FROM ");
          strQuery.append(" t704a_account_rep_mapping t704a, t704_account t704 ");
          // to add below tables to get the rep id - based on user
          //strQuery.append(" , t101_user t101u, t101_party t101p, t703_sales_rep t703 ");
          strQuery.append(" WHERE t704.c704_account_id    = t704a.c704_account_id ");
          // to join user and party details
          
          // **** To remove the User and sales rep join (Query execute takes more times)          
          /*strQuery.append(" AND t101u.c101_party_id = t101p.c101_party_id ");
          strQuery.append(" AND t101p.c101_party_id  = t703.c101_party_id ");
          strQuery.append(" AND t704a.c703_sales_rep_id = t703.c703_sales_rep_id ");
          strQuery.append(" AND t101u.c101_user_id = " + strEmpId);*/
          
          strQuery.append(" AND t704a.c703_sales_rep_id = " + strEmpId);
          strQuery.append(" AND t704.c703_sales_rep_id   = V700.rep_id " );
          strQuery.append(" AND t704.c704_void_fl      IS NULL ");
          strQuery.append(" AND t704a.c704a_void_fl    IS NULL ) ");
          // to remove the void flag check - Once added query execute takes more time
          /*strQuery.append(" and t101p.c101_void_fl  is null ");
          strQuery.append(" and t703.c703_void_fl  is null ) "); */
          
          log.debug("***GmSalesDispatchAction*** Access Level 7");

          
          break;
        }
        // other more filter users like Agmar Esser want to see other region
        case 8: {
          strQuery.append("  EXISTS  ");
          strQuery.append("  (SELECT DISTINCT V700S.REGION_ID ");
          strQuery.append("  FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
          strQuery.append("  WHERE V700S.REGION_ID=V700.REGION_ID ");
          strQuery.append("  AND V700S.REP_COMPID IN ( " + strCompanyIds + ") )");
          log.debug("***GmSalesDispatchAction*** Access Level 8");

          break;
        }

      }

    }

    log.debug("strQuery.toString()==" + strQuery.toString());
    return strQuery.toString();
  }

  /*
   * This function is used to get filter condition and data based on the request specified the user
   * defaults filter based on access contion and adds other filter
   */
  public String getAccessFilter(HttpServletRequest req, HttpServletResponse res, boolean flag) {
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    // Holds the where condition information
    String strQuery = "";
    StringBuffer strCondtion = new StringBuffer();

    strQuery = getAccessFilter(req, res);
    if (flag) {
      strCondtion = gmAccessFilter.getHierarchyWhereConditionV700(req, res, strQuery);
      log.debug("strCondtion.toString()"+strCondtion.toString());
      return strCondtion.toString();
    }
    log.debug("strCondition := " + strCondtion.toString());
    return strQuery;
  }
  /*
   * This method is used to get the access condition for the reports which run based on the V700
   * view.
   */
  public String getAccessCondition(HttpServletRequest req, HttpServletResponse res) {
    // To get the Session Information
    HttpSession session = req.getSession(false);

    // Holds the where condition information
    StringBuffer strQuery = new StringBuffer();

    GmAccessFilter gmAccessFilter = new GmAccessFilter();

    // To get the Employee Information
    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    int intOverrideAccLevel =
        Integer.parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideAccLvl")));
    if (intOverrideAccLevel == 1) {
      intAccLevel = 2;
    }

    // *********************************************************
    // Will Execute the below Query if the Department is Sales
    // *********************************************************
    if (strDepartMentID.equals("S")) {
      switch (intAccLevel) {

      // Sales Rep Filter Condition
        case 1: {
          strQuery.append(" V700.REP_ID = " + strEmpId);
          break;
        }

        // Distributor Filter Condition
        case 2: {
          
            strQuery.append("  EXISTS  ");
          strQuery.append("  ( SELECT T101S.C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM T101_USER T101S WHERE C101_USER_ID = " + strEmpId);
          strQuery.append("  AND T101S.C701_DISTRIBUTOR_ID=V700.D_ID  )  ");
          log.debug("***GmSalesDispatchAction*** Access Level 2***getAccessCondition");


          break;
        }

        // AD Filter Condition
        case 3: {
          strQuery.append("  V700.AD_ID = " + strEmpId);
          log.debug("***GmSalesDispatchAction*** Access Level 3***getAccessCondition");

          break;
        }

        // VP Filter Condition
        case 4: {
          strQuery.append("  EXISTS  ");
          strQuery.append("  (SELECT  T708S.C901_REGION_ID ");
          strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S ");
          strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
          strQuery.append("  AND   T708S.C708_DELETE_FL IS NULL ");
          strQuery.append("  AND T708S.C901_USER_ROLE_TYPE = 8001 ");
          strQuery.append("  AND T708S.C901_REGION_ID = V700.REGION_ID )");
          log.debug("***GmSalesDispatchAction*** Access Level 4***getAccessCondition");

          break;
        }

        // other AD's like Kirk Tovey want to see other region
        case 6: {
          strQuery.append("  EXISTS  ");
          strQuery.append("  (SELECT  T708S.C901_REGION_ID ");
          strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S ");
          strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
          strQuery.append("  AND T708S.C708_DELETE_FL IS NULL ");
          strQuery.append("  AND T708S.C901_USER_ROLE_TYPE = 8002 ");
          strQuery.append("  AND T708S.C901_REGION_ID=V700.REGION_ID ) ");
          log.debug("***GmSalesDispatchAction*** Access Level 6***getAccessCondition");

          break;
        }

        // ASS Filter condition
        case 7: {
			// PC-318: Associate sales rep - To change the query instead of
			// account to modified Sales rep to get the data.
        	
            // To join the t704a and T704 table	
            strQuery.append("  EXISTS  ");
            strQuery.append("  ( SELECT distinct t704.c703_sales_rep_id FROM ");
            strQuery.append(" t704a_account_rep_mapping t704a, t704_account t704 ");
            // to add below tables to get the rep id - based on user
            //strQuery.append(" , t101_user t101u, t101_party t101p, t703_sales_rep t703 ");
            strQuery.append(" WHERE t704.c704_account_id    = t704a.c704_account_id ");
            // to join user and party details
            // **** To remove the User and sales rep join (Query execute takes more times)
            /*strQuery.append(" AND t101u.c101_party_id = t101p.c101_party_id ");
            strQuery.append(" AND t101p.c101_party_id  = t703.c101_party_id ");
            strQuery.append(" AND t704a.c703_sales_rep_id = t703.c703_sales_rep_id ");
            strQuery.append(" AND t101u.c101_user_id = " + strEmpId);*/
            
            strQuery.append(" AND t704a.c703_sales_rep_id = " + strEmpId);
            strQuery.append(" AND t704.c703_sales_rep_id   = V700.rep_id " );
            strQuery.append(" AND t704.c704_void_fl      IS NULL ");
            strQuery.append(" AND t704a.c704a_void_fl    IS NULL ) ");
            // to remove the void flag check - Once added query execute takes more time
            /*strQuery.append(" and t101p.c101_void_fl  is null ");
            strQuery.append(" and t703.c703_void_fl  is null ) "); */
            
          log.debug("***GmSalesDispatchAction*** Access Level 7***getAccessCondition");

          break;
        }

      }
    }

    // strQuery = gmAccessFilter.getHierarchyWhereConditionV700(req,res,strQuery.toString());
    // log.debug("strCondition := "+ strQuery.toString());

    return strQuery.toString();
  }

  /*
   * This function is used to get access condition and data based on the request specified the user
   * defaults access based on access contion and adds other filter
   */
  public String getAccessCondition(HttpServletRequest req, HttpServletResponse res, boolean flag) {
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    // Holds the where condition information
    String strQuery = "";
    StringBuffer strCondtion = new StringBuffer();

    strQuery = getAccessFilter(req, res);
    if (flag) {
      strCondtion = gmAccessFilter.getHierarchyWhereConditionV700(req, res, strQuery);
      return strCondtion.toString();
    }
    log.debug("strCondition := " + strCondtion.toString());
    return strQuery;
  }

}
