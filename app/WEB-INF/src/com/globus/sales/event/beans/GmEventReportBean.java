package com.globus.sales.event.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmEventReportBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmEventReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmEventReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchEventName This method will fetch the EventName only active
   * 
   * @return ArrayList alEventInfo
   * @exception AppError Author Jignesh Shah
   */
  public ArrayList fetchEventNameList() throws AppError {
    ArrayList alEventInfo = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_name_list", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alEventInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alEventInfo;
  }// End of fetchEventNameList

  /**
   * fetchEventName This method will fetch the EventName without rescheduled
   * 
   * @return ArrayList alEventInfo
   * @exception AppError Author Jignesh Shah
   */
  public ArrayList fetchEventNameAllList() throws AppError {
    ArrayList alEventInfo = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_name_all_list", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alEventInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alEventInfo;
  }// End of fetchEventNameList

  /**
   * 
   * fetchEventListReport This method will fetch the Event List Report
   * 
   * @param HashMap hmParam
   * @return HashMap hmReturn
   * @exception AppError Author Jignesh Shah
   */
  public HashMap fetchEventListReport(HashMap hmParam) throws AppError, Exception {

    ArrayList alAuditInfo = new ArrayList();
    ArrayList alEventInfo = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("EVENTNAME"));
    String strPurOfEvent = GmCommonClass.parseNull((String) hmParam.get("PUPEVENT"));
    String strLoanTo = GmCommonClass.parseNull((String) hmParam.get("LOANTO"));
    String strLoanToName = GmCommonClass.parseNull((String) hmParam.get("LOANTONAME"));
    String strEDtType = GmCommonClass.parseNull((String) hmParam.get("DTTYPE"));
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDT"));
    String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDT"));

    if (strStartDt.equals("") && strEndDt.equals("")) {
      Date dtlockedFromDT = (Date) hmParam.get("EVENTSTARTDT");
      Date dtlockedToDT = (Date) hmParam.get("EVENTENDDT");
      if (dtlockedFromDT != null && dtlockedToDT != null) {
        strStartDt = GmCommonClass.getStringFromDate(dtlockedFromDT, strDateFmt);
        strEndDt = GmCommonClass.getStringFromDate(dtlockedToDT, strDateFmt);
      }
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_list_rpt", 8);
    gmDBManager.setString(1, strCaseInfoId);
    gmDBManager.setString(2, strPurOfEvent);
    gmDBManager.setString(3, strLoanTo);
    gmDBManager.setString(4, strLoanToName);
    gmDBManager.setString(5, strEDtType);
    gmDBManager.setString(6, strStartDt);
    gmDBManager.setString(7, strEndDt);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.execute();
    alEventInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
    gmDBManager.close();
    hmReturn.put("ALFIELDSALESINFO", alEventInfo);
    return hmReturn;
  }// End of fetchEventListReport

  /**
   * fetchEventSetDetails This method used to fetch the Event Set Details.
   * 
   * @param String strEventInfoID
   * @return ArrayList alEventSetInfo
   * @exception AppError Author Elango p
   */
  public ArrayList fetchEventSetDetails(String strEventInfoID) throws AppError {
    ArrayList alEventSetInfo = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_set_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strEventInfoID);
    gmDBManager.execute();
    alEventSetInfo =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    log.debug(" values in alShipEventInfo " + alEventSetInfo);
    return alEventSetInfo;
  }// End of fetchEventSetDetails

  /**
   * fetchEventItemDetails This method used to fetch the Event Item Details.
   * 
   * @param String strEventInfoID
   * @return ArrayList alEventItemInfo
   * @exception AppError Author Elango p
   */
  public ArrayList fetchEventItemDetails(String strEventInfoID) throws AppError {
    ArrayList alEventItemInfo = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_item_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strEventInfoID);
    gmDBManager.execute();
    alEventItemInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" values in alShipEventInfo " + alEventItemInfo);
    return alEventItemInfo;
  }// End of fetchEventItemDetails

  /**
   * sendEventCancellationEmail This method will Send the Automatic Event Cancelation Email afetr
   * cancel an Event.
   * 
   * @param String stCaseInfoID,strCancelReason,strUserId
   * @return String stCaseInfoID
   * @exception AppError Author Jignesh Shah
   */
  public String sendEventCancellationEmail(String stCaseInfoID, String strCancelReason,
      String strUserId) throws AppError {
    String strCanReason = GmCommonClass.getCodeNameFromCodeId(strCancelReason);
    String strTEMPLATE_NAME = "GmEventCancellationEmailTemplate";
    GmEmailProperties emailProps = new GmEmailProperties();
    GmJasperMail jasperMail = new GmJasperMail();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_fch_event_info", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    ArrayList alDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    if (alDetails != null && alDetails.size() > 0) {
      for (Iterator iter = alDetails.iterator(); iter.hasNext();) {
        HashMap hmEmailParams = (HashMap) iter.next();
        String strEveName = GmCommonClass.parseNull((String) hmEmailParams.get("EVENTNAME"));
        String strEveStartDt =
            GmCommonClass.parseNull((String) hmEmailParams.get("EVENTSTARTDATE"));
        String strEveEndDt = GmCommonClass.parseNull((String) hmEmailParams.get("EVENTENDDATE"));
        String strPurpose = GmCommonClass.parseNull((String) hmEmailParams.get("PURPOSE"));
        String strStatus = GmCommonClass.parseNull((String) hmEmailParams.get("STATUS"));
        String strEveLoanTo = GmCommonClass.parseNull((String) hmEmailParams.get("LOANTOTYPE"));
        String strEveLoanToName = GmCommonClass.parseNull((String) hmEmailParams.get("LOANTONAME"));
        String strCancelledBy = GmCommonClass.parseNull((String) hmEmailParams.get("CANCELLEDBY"));
        String strFromUserEmail =
            GmCommonClass.parseNull((String) hmEmailParams.get("FROMUSEREMAIL"));
        String strCanUserEmail =
            GmCommonClass.parseNull((String) hmEmailParams.get("CANUSEREMAIL"));
        String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
        GmResourceBundleBean gmResourceBundleBean =
        		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

        HashMap hmJasperDetails = new HashMap();
        hmJasperDetails.put("EVENTNAME", strEveName);
        hmJasperDetails.put("EVESTARTDT", strEveStartDt);
        hmJasperDetails.put("EVEENDDT", strEveEndDt);
        hmJasperDetails.put("EVEPURPOSE", strPurpose);
        hmJasperDetails.put("EVESTATUS", strStatus);
        hmJasperDetails.put("EVELOANTO", strEveLoanTo);
        hmJasperDetails.put("EVELOANTONAME", strEveLoanToName);
        hmJasperDetails.put("CANCELLEDBY", strCancelledBy);
        hmJasperDetails.put("REASON", strCanReason);

        String strJasperName = "/GmCancelEventDetails.jasper";

        emailProps.setMimeType(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
            + GmEmailProperties.MIME_TYPE));
        emailProps.setSender(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
            + GmEmailProperties.FROM));
        emailProps.setRecipients(strCanUserEmail);
        emailProps.setCc(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
            + GmEmailProperties.CC));
        emailProps.setSubject(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
            + GmEmailProperties.SUBJECT));

        jasperMail.setJasperReportName(strJasperName);
        jasperMail.setAdditionalParams(hmJasperDetails);
        jasperMail.setReportData(null);
        jasperMail.setEmailProperties(emailProps);
        HashMap hmReturnDetails = jasperMail.sendMail();

      }
    }
    return stCaseInfoID;
  }// End of sendEventCancellationEmail

  /**
   * sendEventSetRejectionEmail This method is used for overloading sendEventSetRejectionEmail.
   * 
   * @param strRequestIds, strCancelFrom, strCancelType, strComments
   * @exception AppError Author Manoj
   */
  public void sendEventSetRejectionEmail(String strRequestIds, String strCancelFrom,
      String strCancelType) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("TXNID", strRequestIds);
    hmParam.put("CANCELACTION", strCancelFrom);
    hmParam.put("CANCELTYPE", strCancelType);
    sendEventSetRejectionEmail(hmParam);
  }

  /**
   * sendEventSetRejectionEmail This method will Send the Automatic Event set Rejection Email after
   * Rejection of a set in an Event.
   * 
   * @param hmParam
   * @exception AppError Author Elango
   */
  public void sendEventSetRejectionEmail(HashMap hmParam) throws AppError {
    // log.debug(" strRequestIds :::" + strRequestIds);
    // String strCanReason = GmCommonClass.getCodeNameFromCodeId(strCancelReason);
    String strTEMPLATE_NAME = "GmEventSetRejectionEmailTemplate";
    String strHeader = "";
    String strContent = "";
    String strOverrideHeader = "";
    String strSubject = "";
    String strQty = "";
    String strDate = "";
    String strReqType = "";
    String strRequestIds = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strCancelFrom = GmCommonClass.parseNull((String) hmParam.get("CANCELACTION"));
    String strCancelType = GmCommonClass.parseNull((String) hmParam.get("CANCELTYPE"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strCompanyId = getGmDataStoreVO().getCmpid();

    ArrayList alDetailsList = new ArrayList();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    GmEmailProperties emailProps = new GmEmailProperties();
    GmJasperMail jasperMail = new GmJasperMail();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmParams = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_set_rejected_dtls", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestIds);
    gmDBManager.setString(2, strCancelType);
    gmDBManager.setString(3, strCancelFrom);
    gmDBManager.execute();
    ArrayList alRejectedSetDetails =
        gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    // log.debug(" values in alRejectedSetDetails " + alRejectedSetDetails);
    gmDBManager.close();
    int intSize = alRejectedSetDetails.size();



    if (alRejectedSetDetails != null && intSize > 0) {
      for (int i = 0; i < intSize; i++) {
        String strCreatedBy = "";
        String strCreatedByNext = "";
        String strEmailId = "";
        String strDefaultCCEmailId = "";
        HashMap hmDetailsSubParams = new HashMap();
        HashMap hmDetailsSubParamsNext = new HashMap();

        hmDetailsSubParams = (HashMap) alRejectedSetDetails.get(i);
        strCreatedBy = GmCommonClass.parseNull((String) hmDetailsSubParams.get("CREATEDBY"));
        if (i != intSize - 1) {
          log.debug(" intSize inside next value taken" + intSize);
          hmDetailsSubParamsNext = (HashMap) alRejectedSetDetails.get(i + 1);
          strCreatedByNext =
              GmCommonClass.parseNull((String) hmDetailsSubParamsNext.get("CREATEDBY"));
        }
        hmDetailsSubParams.put("COMMENTS", strComments);
        if (strCreatedBy.equals(strCreatedByNext) && i != intSize - 1) {
          alDetailsList.add(hmDetailsSubParams);
        } else {
          alDetailsList.add(hmDetailsSubParams);

          String strJasperName = "/GmRejectEventSetDetails.jasper";
          strEmailId = GmCommonClass.parseNull((String) hmDetailsSubParams.get("EMAILID"));
          log.debug(strEmailId + " values in alDetailsList before sending email" + alDetailsList);
          emailProps.setMimeType(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
              + GmEmailProperties.MIME_TYPE));
          emailProps.setSender(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
              + GmEmailProperties.FROM));
          emailProps.setRecipients(strEmailId);
          emailProps.setReplyTo(GmCommonClass.parseNull(GmCommonClass
              .getString("EVENTREPLYTOEMAILID")));

          if (strCancelFrom.equals("OVERRIDE")) {
            strContent = "The following CN's that were allocated to the event was overridden,";
            strHeader = "Overridden By";
            strOverrideHeader = "CN #";
            strSubject = gmResourceBundleBean.getProperty(strTEMPLATE_NAME + ".OverrideSubject");
            strQty = "Qty";
            strDate = "Date";
          } else {
            if (strCancelType.equals("VILNRQ") || strCancelType.equals("VLNRQ")
                || strCancelType.equals("SETRQ") || strCancelType.equals("ITEMRQ")) {
              strContent = "The following set(s)/item(s) are rejected,";
              strHeader = "Rejected By";
              strSubject = gmResourceBundleBean.getProperty(strTEMPLATE_NAME + ".RejectSubject");
              strQty = "Req Qty";
              strDate = "Req Date";
            } else if (strCancelType.equals("VDIHLN")) {
              strContent = "The following inventory requested by you were voided,";
              strHeader = "Voided By";
              strSubject = gmResourceBundleBean.getProperty(strTEMPLATE_NAME + ".VoidSubject");
              strQty = "Qty";
              strDate = "Date";
            }
            strOverrideHeader = "Reason";
          }
          strReqType = GmCommonClass.parseNull((String) hmDetailsSubParams.get("REQTYPE"));
          if (strReqType.equals("4127")) {
            strSubject = "Loaner Set Rejected Details";
            // To fetch email id from rules. In this way, incase if anybody need explicit access we
            // can add them.
            strDefaultCCEmailId =
                GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("LOANER_REQ_REJECT",
                    "DEFAULT_CC", strCompanyId));
          }
          if (strReqType.equals("400087")) {
            strSubject = "Consignment Set Rejected Details";
            // To fetch email id from rules. In this way, incase if anybody need explicit access we
            // can add them.
            strDefaultCCEmailId =
                GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SET_REQ_REJECT",
                    "DEFAULT_CC", strCompanyId));
          }
          if (strReqType.equals("400088")) {
            strSubject = "Consignment Item Rejected Details";
            // To fetch email id from rules. In this way, incase if anybody need explicit access we
            // can add them.
            strDefaultCCEmailId =
                GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("ITEM_REQ_REJECT",
                    "DEFAULT_CC", strCompanyId));
          }
          if (strReqType.equals("4127") || strReqType.equals("400088")
              || strReqType.equals("400087")) {
            hmParams.put("EID", "Event ID");
            hmParams.put("FTRNM", gmResourceBundleBean.getProperty("GmInvMgmReqRejEmail."
                + GmEmailProperties.MESSAGE));
          } else {
            hmParams.put("EID", "Event #");
            hmParams.put("FTRNM", "");
          }
          emailProps.setCc(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
              + GmEmailProperties.CC));
          emailProps.setCc(strDefaultCCEmailId);
          emailProps.setSubject(strSubject);
          hmParams.put("OVERRIDEHEADER", strOverrideHeader);
          hmParams.put("TYPE", strCancelFrom);
          hmParams.put("HEADER", strHeader);
          hmParams.put("CONTENT", strContent);
          hmParams.put("QTY", strQty);
          hmParams.put("DATE", strDate);
          hmParams
              .put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
          hmParams
              .put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSEmail"));
          jasperMail.setJasperReportName(strJasperName);
          jasperMail.setAdditionalParams(hmParams);
          jasperMail.setReportData(alDetailsList);
          jasperMail.setEmailProperties(emailProps);
          HashMap hmReturnDetails = jasperMail.sendMail();
          alDetailsList = new ArrayList();
        }
      }
    }
  }// End of sendEventSetRejectionEmail

  /**
   * fetchRequestType This method will return event request type for 525 request ID.
   * 
   * @param String strProductRequestID
   * @return String strRequestType
   * @exception AppError Author Elango
   */
  public String fetchRequestType(String strProductRequestID) throws AppError {
    String strRequestType = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_eventbook_rpt.get_request_type", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, GmCommonClass.parseNull(strProductRequestID));
    gmDBManager.execute();
    strRequestType = gmDBManager.getString(1);
    gmDBManager.close();
    log.debug("..strProductRequestID.." + strProductRequestID + "..strRequestType.."
        + strRequestType);
    return strRequestType;
  }

  public String fetchCaseRequestID(String strCaseInfoID) throws AppError {
    String strRequestID = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_eventbook_rpt.get_case_request_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, GmCommonClass.parseNull(strCaseInfoID));
    gmDBManager.execute();
    strRequestID = gmDBManager.getString(1);
    gmDBManager.close();
    log.debug("..strCaseInfoID.." + strCaseInfoID + "..strRequestID.." + strRequestID);
    return strRequestID;
  }

  /**
   * fetchBookedItems This method used to fetch the Event Item Details.
   * 
   * @param String strEventInfoID
   * @return ArrayList alEventItemInfo
   * @exception AppError Author Gopinathan
   */
  public ArrayList fetchBookedItems(String strCaseInfoID) throws AppError {
    ArrayList alEventItemInfo = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_booked_items", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.execute();
    alEventItemInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" values in alEventItemInfo " + alEventItemInfo);
    return alEventItemInfo;
  }// End of fetchEventItemDetails

  /**
   * saveEventItemInfo This method will save the Event Item Info
   * 
   * @param String strType
   * @return String
   * @exception AppError
   * @throws Exception
   * @Author Gopinathan
   */
  public void saveEventItemInfo(HashMap hmParam) throws AppError {

    log.debug("saveEventItemInfo *************  " + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveEventItemInfo(hmParam, gmDBManager);
    gmDBManager.commit();
  } // End of saveEventItemInfo

  /**
   * saveEventItemInfo This method will save the Event Item Info
   * 
   * @param String strType
   * @return String
   * @exception AppError
   * @throws Exception
   * @Author Gopinathan
   */
  public void saveEventItemInfo(HashMap hmParam, GmDBManager gmDBManager) throws AppError {

    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strPartNumString = GmCommonClass.parseNull((String) hmParam.get("PARTNUMINPUTSTRING"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strSystemType = GmCommonClass.parseNull((String) hmParam.get("PRODUCTIONTYPE"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strCaseDtl = GmCommonClass.parseNull((String) hmParam.get("CASEDETINPUTSTR"));
    String strCaseShipDtl = GmCommonClass.parseNull((String) hmParam.get("CASESHIPINPUTSTR"));
    log.debug("saveEventItemInfo *************  " + hmParam);

    gmDBManager.setPrepareString("gm_pkg_sm_casebook_txn.gm_sav_item_booking", 7);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setString(2, strPartNumString);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, strSystemType);
    gmDBManager.setString(5, strUserID);
    gmDBManager.setString(6, strCaseDtl);
    gmDBManager.setString(7, strCaseShipDtl);
    gmDBManager.execute();
  } // End of saveEventItemInfo


}
