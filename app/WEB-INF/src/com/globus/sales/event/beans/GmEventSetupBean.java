package com.globus.sales.event.beans;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.jobs.GmJob;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmEventSetupBean extends GmCommonBean {
  GmCommonClass gmCommon = new GmCommonClass();
  GmLogger log = GmLogger.getInstance();

  /*
   * Initialize the Logger Class.This will be removed all place changed with GmDataStoreVO
   * constructor
   */

  public GmEventSetupBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmEventSetupBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveEventBookingInfo This method will save the Event Info
   * 
   * @param String strType
   * @return String
   * @exception AppError
   * @throws Exception
   * @Author Hreddi
   */
  public String saveEventBookingInfo(HashMap hmParam) throws AppError, Exception {
    String strCaseInfoIDFrmDB = "";
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strEventName = GmCommonClass.parseNull((String) hmParam.get("EVENTNAME"));
    String strEventPurpose = GmCommonClass.parseNull((String) hmParam.get("EVENTPURPOSE"));
    String strEventStartDt = GmCommonClass.parseNull((String) hmParam.get("EVENTSTARTDATE"));
    String strEventEndDt = GmCommonClass.parseNull((String) hmParam.get("EVENTENDDATE"));
    String strEventVenue = GmCommonClass.parseNull((String) hmParam.get("EVENTVENUE"));
    String strEventLoanTo = GmCommonClass.parseNull((String) hmParam.get("EVENTLOANTO"));
    String strEventLoanToName = GmCommonClass.parseNull((String) hmParam.get("EVENTLOANTONAME"));
    String strEventMeterReq = GmCommonClass.parseNull((String) hmParam.get("SELECTEDCATGR"));
    String strEventFavoriteID = GmCommonClass.parseNull((String) hmParam.get("FAVORITEEVENT"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    Date dtEventStartDt = null;
    Date dtEventEndDt = null;
    dtEventStartDt = GmCommonClass.getStringToDate(strEventStartDt, strDateFormat);
    dtEventEndDt = GmCommonClass.getStringToDate(strEventEndDt, strDateFormat);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_sav_event_info", 12);
    gmDBManager.registerOutParameter(12, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setString(2, strEventName);
    gmDBManager.setInt(3, Integer.parseInt(strEventPurpose));
    gmDBManager.setDate(4, dtEventStartDt);
    gmDBManager.setDate(5, dtEventEndDt);
    gmDBManager.setString(6, strEventVenue);
    gmDBManager.setInt(7, Integer.parseInt(strEventLoanTo));
    gmDBManager.setString(8, strEventLoanToName);
    gmDBManager.setString(9, strEventMeterReq);
    gmDBManager.setString(10, strEventFavoriteID);
    gmDBManager.setString(11, strUserID);
    gmDBManager.execute();
    strCaseInfoIDFrmDB = GmCommonClass.parseNull(gmDBManager.getString(12));

    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strCaseInfoIDFrmDB, strLogReason, strUserID, "1287");
    }
    gmDBManager.commit();
    return strCaseInfoIDFrmDB;
  } // End of saveEventBookingInfo

  /**
   * fetchEventReqApproved This method will return the Status regarding the atleaset one request
   * might approved or not for an Event
   * 
   * @param HashMap hmParam
   * @return String strStatus
   * @exception AppError
   * @Author Hreddi
   */
  public String fetchEventReqApproved(HashMap hmParam) throws AppError {
    String strStatus = "";
    String strCaseInfoID = GmCommonClass.parseZero((String) hmParam.get("CASEINFOID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_req_approved", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setInt(1, Integer.parseInt(strCaseInfoID));
    gmDBManager.execute();
    strStatus = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.commit();
    return strStatus;
  }// End of fetchEventReqApproved

  /**
   * getEventAccess This method will return the Access Condition to Perticular Users to Access the
   * Event Setup Page as weel as Edit Event and Reschedule Event Screens
   * 
   * @param String strUserID,StrGroupType,StrGroupName
   * @return String strAccessFl
   * @exception AppError
   * @Author Hreddi
   */
  public String getEventAccess(String strUserID, String StrGroupType, String StrGroupName)
      throws AppError {
    String strAccessFl = "";
    HashMap hmAccess = new HashMap();
    ArrayList alGroup = new ArrayList();
    log.debug("The User ID comming from the Action is * * *  * * * *" + strUserID);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    if (StrGroupType.equals("EVENT")) {
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
              StrGroupName));
      strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    } else if (StrGroupType.equals("GROUP")) {
      HashMap hmParam = new HashMap();
      hmParam.put("USERNM", strUserID);
      hmParam.put("GROUPNAME", StrGroupName);
      alGroup =
          GmCommonClass.parseNullArrayList((gmAccessControlBean.fetchGroupMappingDetails(hmParam)));
      if (alGroup.size() <= 0) {
        strAccessFl = "N";
      } else {
        strAccessFl = "Y";
      }
    }
    return strAccessFl;
  }// End of getEventAccess

  /**
   * saveEventRescheduleInfo This method will Save the Rescheduled Event Information
   * 
   * @param Hashmap hmParam
   * @return String stCaseInfoID
   * @exception AppError
   * @throws Exception
   * @Author Hreddi
   */
  public String saveEventRescheduleInfo(HashMap hmParam) throws AppError, Exception {
    String strCaseInfoIDFrmDB = "";
    String stCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strEventStartDt = GmCommonClass.parseNull((String) hmParam.get("EVENTSTARTDATE"));
    String strEventEndDt = GmCommonClass.parseNull((String) hmParam.get("EVENTENDDATE"));
    String strPlannedShipedDt = GmCommonClass.parseNull((String) hmParam.get("PLANNEDSHIPEDDATE"));
    String strExpectedRtnDt = GmCommonClass.parseNull((String) hmParam.get("EXPECTEDRETURNDT"));
    String strRescheduleReason = GmCommonClass.parseNull((String) hmParam.get("RESCHEDULEREASON"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    Date dtEventStartDt = null;
    Date dtEventEndDt = null;
    Date dtPlanShipDt = null;
    Date dtExpecReturnDt = null;
    dtEventStartDt = GmCommonClass.getStringToDate(strEventStartDt, strDateFormat);
    dtEventEndDt = GmCommonClass.getStringToDate(strEventEndDt, strDateFormat);
    dtPlanShipDt = GmCommonClass.getStringToDate(strPlannedShipedDt, strDateFormat);
    dtExpecReturnDt = GmCommonClass.getStringToDate(strExpectedRtnDt, strDateFormat);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_sav_reschedule_event", 8);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.setString(1, stCaseInfoID);
    gmDBManager.setDate(2, dtEventStartDt);
    gmDBManager.setDate(3, dtEventEndDt);
    gmDBManager.setDate(4, dtPlanShipDt);
    gmDBManager.setDate(5, dtExpecReturnDt);
    gmDBManager.setString(6, strRescheduleReason);
    gmDBManager.setString(7, strUserID);
    gmDBManager.execute();
    strCaseInfoIDFrmDB = GmCommonClass.parseNull(gmDBManager.getString(8));

    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strCaseInfoIDFrmDB, strLogReason, strUserID, "1287");
    }
    gmDBManager.commit();
    return strCaseInfoIDFrmDB;
  }// End of saveEventRescheduleInfo

  /**
   * fetchEventBookingInfo This method will fetch the Booked Event Information
   * 
   * @param String stCaseInfoID
   * @return HashMap hmEventInfo
   * @exception AppError
   * @Author Hreddi
   */
  public HashMap fetchEventBookingInfo(String stCaseInfoID, String strUserId) throws AppError {
    HashMap hmEventInfo = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_fch_event_info", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    hmEventInfo = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.commit();
    return hmEventInfo;
  }// End of fetchEventBookingInfo

  /**
   * fetchEventEditStatus This method will fetch the Booked Event Information
   * 
   * @param String stCaseInfoID
   * @return count
   * @exception AppError
   * @Author Hreddi
   */
  public String fetchEventEditStatus(String stCaseInfoID) throws AppError {
    String strStatus = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_eventbook_rpt.get_event_edit_status", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setInt(2, Integer.parseInt(stCaseInfoID));
    gmDBManager.execute();
    strStatus = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strStatus;
  }// End of fetchEventBookingInfo

  public String fetchEventMaterialAddSts(String strCaseInfoID) throws AppError {
    String strStatus = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_eventbook_rpt.get_event_resource_add_status", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setInt(2, Integer.parseInt(strCaseInfoID));
    gmDBManager.execute();
    strStatus = gmDBManager.getString(1);
    gmDBManager.close();
    return strStatus;
  }

  /**
   * saveEventRequestDetails This method will Save the Event Request Details
   * 
   * @param HashMap hmParam
   * @return nothing
   * @exception AppError
   * @Author APrasath
   */
  public void saveEventRequestDetails(HashMap hmParam) throws AppError, Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveEventRequestDetails(hmParam, gmDBManager);
    gmDBManager.commit();
  }// End of saveEventRequestDetails

  /**
   * saveEventRequestDetails This method will Save the Event Request Details
   * 
   * @param HashMap hmParam
   * @return nothing
   * @exception AppError
   * @Author APrasath
   */
  public void saveEventRequestDetails(HashMap hmParam, GmDBManager gmDBManager) throws AppError,
      Exception {

    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSaveShipDetails = GmCommonClass.parseNull((String) hmParam.get("SAVESHIPDETAILS"));
    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_sav_event_request_details", 2);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    if (!strSaveShipDetails.equalsIgnoreCase("NO")) {
      saveEventShippingDetails(hmParam, gmDBManager);
    }
  }// End of saveEventRequestDetails

  /**
   * saveEventShippingDetails This method will Save the Event Shipping Details
   * 
   * @param HashMap hmParam
   * @return nothing
   * @exception AppError
   * @Author APrasath
   */
  public void saveEventShippingDetails(HashMap hmParam, GmDBManager gmDBManager) throws AppError,
      Exception {


    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strPlannedShipDate = GmCommonClass.parseNull((String) hmParam.get("PLANNEDSHIPDATE"));
    String strExpReturnDate = GmCommonClass.parseNull((String) hmParam.get("EXPRETURNDATE"));
    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strShipToID = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strShipCarrier = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strAddressID = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    Date dtPlanShipDt = null;
    Date dtExpecReturnDt = null;
    dtPlanShipDt = GmCommonClass.getStringToDate(strPlannedShipDate, strDateFormat);
    dtExpecReturnDt = GmCommonClass.getStringToDate(strExpReturnDate, strDateFormat);
    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_sav_event_shipping_details", 9);
    gmDBManager.setString(1, strCaseInfoID);
    gmDBManager.setDate(2, dtPlanShipDt);
    gmDBManager.setDate(3, dtExpecReturnDt);
    gmDBManager.setString(4, strShipTo);
    gmDBManager.setString(5, strShipToID);
    gmDBManager.setString(6, strShipMode);
    gmDBManager.setString(7, strShipCarrier);
    gmDBManager.setString(8, strAddressID);
    gmDBManager.setString(9, strUserID);
    gmDBManager.execute();

  }// End of saveEventShippingDetails

  /**
   * sendRequestRecievedMail This method will send the email
   * 
   * @param HashMap hmParam
   * @return nothing
   * @exception AppError
   * @Author Jignesh Shah
   */
  public void sendRequestRecievedMail(HashMap hmParam) throws AppError, Exception {

    ArrayList alRequestDetails = new ArrayList();

    String strEmailShipToId = "";
    String strAdd = "";

    GmCustomerBean custBean = new GmCustomerBean(getGmDataStoreVO());

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strAddId = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID"));
    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strEmpShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));

    if (strShipTo.equals("4121")) {
      strEmailShipToId = strAddId;
    } else {
      strEmailShipToId = strEmpShipToId;
    }

    if (!strShipTo.equals("4124")) {
      strAdd = GmCommonClass.parseNull(custBean.getAddress(strShipTo, strEmailShipToId));
    }
    sendRequestRecievedMail(strCaseInfoID, strShipMode, strAdd);

  }

  public void sendRequestRecievedMail(String strCaseInfoID, String strShippingMode,
      String strAddress) {
    log.debug("strRequestId = " + strCaseInfoID);

    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strrepID = "";
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    try {
      String strDateForamt = getGmDataStoreVO().getCmpdfmt();
      SimpleDateFormat df = new SimpleDateFormat(strDateForamt);
      ArrayList alRequestDetails = new ArrayList();
      HashMap hmRepDetails = new HashMap();
      HashMap hmParams = new HashMap();

      String strRepName = "";
      String strRepEmail = "";

      hmParams =
          GmCommonClass.parseNullHashMap(gmEventSetupBean.fetchEventBookingInfo(strCaseInfoID, ""));
      gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_req_details", 2);
      gmDBManager.setString(1, strCaseInfoID);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.execute();

      alRequestDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
      log.debug("alRequestDetails------- " + alRequestDetails);

      gmDBManager.close();

      hmParams.put("UNAVAILABLE_FL", "N");

      if (alRequestDetails != null && alRequestDetails.size() > 0) {
        hmRepDetails = (HashMap) alRequestDetails.get(0);
        strRepName = (String) hmRepDetails.get("REP_NAME");
        strrepID = (String) hmRepDetails.get("REP_ID");
        strRepEmail = (String) hmRepDetails.get("EMAIL");

        for (Iterator iter = alRequestDetails.iterator(); iter.hasNext();) {
          HashMap hmTemp = (HashMap) iter.next();
          String strStatus = (String) hmTemp.get("STATUS");
          if (strStatus != null && strStatus.equals("Unavailable")) {
            hmParams.put("UNAVAILABLE_FL", "Y");
            break;
          }
        }
      }

      GmEmailProperties emailProps = new GmEmailProperties();
      emailProps.setMimeType(gmResourceBundleBean.getProperty("GmEmailRequestDetail."
          + GmEmailProperties.MIME_TYPE));
      emailProps.setSender(gmResourceBundleBean.getProperty("GmEmailRequestDetail."
          + GmEmailProperties.FROM));
      emailProps.setRecipients(strRepEmail);
      emailProps.setCc(gmResourceBundleBean.getProperty("GmEmailRequestDetail."
          + GmEmailProperties.CC));

      String strSubject =
          gmResourceBundleBean.getProperty("GmEmailRequestDetail." + GmEmailProperties.SUBJECT);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", strRepName);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<RQDATE>", df.format(new Date()));
      emailProps.setSubject(strSubject);
      emailProps
          .setReplyTo(GmCommonClass.parseNull(GmCommonClass.getString("EVENTREPLYTOEMAILID")));

      hmParams.put("SHIPMODE", GmCommonClass.getCodeNameFromCodeId(strShippingMode));
      hmParams.put("SHIPADD", strAddress);
      GmJasperMail jasperMail = new GmJasperMail();
      jasperMail.setJasperReportName("/GmEmailRequestDetail.jasper");
      jasperMail.setAdditionalParams(hmParams);
      jasperMail.setReportData(alRequestDetails);
      jasperMail.setEmailProperties(emailProps);

      hmReturn = jasperMail.sendMail();

    } catch (Exception e) {
      log.error("Exception in sendRequestRecievedMail : " + e.getMessage());
      hmReturn.put(GmJob.EXCEPTION, e);
    }
    Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
    if (ex != null) {
      try {
        GmEmailProperties emailProps = new GmEmailProperties();
        emailProps.setRecipients(gmResourceBundleBean
            .getProperty("GmEmailRequestDetail.ExceptionMailTo"));
        emailProps.setSender("notification@globusmedical.com");
        emailProps.setSubject(gmResourceBundleBean
            .getProperty("GmEmailRequestDetail.ExceptionMailSubject") + " " + strCaseInfoID);
        emailProps.setMessage("Exception : " + GmCommonClass.getExceptionStackTrace(ex, "\n")
            + "\nOriginal Mail : " + hmReturn.get("ORIGINALMAIL"));
        emailProps.setMimeType("text/html");

        GmCommonClass.sendMail(emailProps);
      } catch (Exception e) {
        log.error("Exception in sending mail to Rep for new request and also notification to IT"
            + e.getMessage());
      }
    }
  }

  public String fetchCaseInfoID(String strProductRequestID) throws AppError {
    String strEventInfoID = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_case_info_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, GmCommonClass.parseNull(strProductRequestID));
    gmDBManager.execute();
    strEventInfoID = gmDBManager.getString(1);
    gmDBManager.close();
    log.debug("..strProductRequestID.." + strProductRequestID + "..strEventInfoID.."
        + strEventInfoID);
    return strEventInfoID;
  }

  /**
   * @param strCaseInfoID
   * @return
   * @throws AppError to fetch shipping id if all the sets shipping to same address
   */
  public String fecthEventShipId(String strCaseInfoID) throws AppError {
    String strShipId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_eventbook_rpt.get_event_ship_add_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setInt(2, Integer.parseInt(strCaseInfoID));
    gmDBManager.execute();
    strShipId = gmDBManager.getString(1);
    gmDBManager.close();
    return strShipId;
  }

  /**
   * fecthEventPlannedShipDate - to fetch planned shipping date if all the sets in a request has
   * same ship date.
   * 
   * @param strCaseInfoID
   * @return String strShipDate
   * @throws AppError
   */
  public String fecthEventPlannedShipDate(String strCaseInfoID) throws AppError {
    String strShipDate = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_eventbook_rpt.get_event_planned_ship_date", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setInt(2, Integer.parseInt(strCaseInfoID));
    gmDBManager.execute();
    strShipDate = gmDBManager.getString(1);
    gmDBManager.close();
    return strShipDate;
  }


}
