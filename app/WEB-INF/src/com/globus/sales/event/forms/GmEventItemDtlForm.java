package com.globus.sales.event.forms;

import com.globus.common.forms.GmCommonForm;

public class GmEventItemDtlForm extends GmCommonForm{
	private String caseInfoID = "";
	private String gridData = "";
	private String productionType = "";
	private String fwdProductType = "";
	private String categoryList = "";
	private String fromscreen = "";
	private String partNumInputString = "";
	private String inputString = "";
	private String backScreen = "";
	
	

	/**
	 * @return the productionType
	 */
	public String getProductionType() {
		return productionType;
	}

	/**
	 * @param productionType the productionType to set
	 */
	public void setProductionType(String productionType) {
		this.productionType = productionType;
	}

	/**
	 * @return the partNumInputString
	 */
	public String getPartNumInputString() {
		return partNumInputString;
	}

	/**
	 * @param partNumInputString the partNumInputString to set
	 */
	public void setPartNumInputString(String partNumInputString) {
		this.partNumInputString = partNumInputString;
	}

	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	/**
	 * @return the fromscreen
	 */
	public String getFromscreen() {
		return fromscreen;
	}

	/**
	 * @param fromscreen the fromscreen to set
	 */
	public void setFromscreen(String fromscreen) {
		this.fromscreen = fromscreen;
	}

	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}

	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}

	/**
	 * @return the categoryList
	 */
	public String getCategoryList() {
		return categoryList;
	}

	/**
	 * @param categoryList the categoryList to set
	 */
	public void setCategoryList(String categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * @return the fwdProductType
	 */
	public String getFwdProductType() {
		return fwdProductType;
	}

	/**
	 * @param fwdProductType the fwdProductType to set
	 */
	public void setFwdProductType(String fwdProductType) {
		this.fwdProductType = fwdProductType;
	}

	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}

	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}

	/**
	 * @return the backScreen
	 */
	public String getBackScreen() {
		return backScreen;
	}

	/**
	 * @param backScreen the backScreen to set
	 */
	public void setBackScreen(String backScreen) {
		this.backScreen = backScreen;
	}
	
	

}
