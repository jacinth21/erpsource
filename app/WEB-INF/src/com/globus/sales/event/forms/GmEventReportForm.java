package com.globus.sales.event.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import com.globus.common.forms.GmCommonForm;

public class GmEventReportForm extends GmCommonForm {
	private int ecYear  ;
	private int ecMonth ;	
	private String endDt 		 			= "";
	private String hLoad 		 			= "";
	private String loanTo 		 			= "";
	private String startDt 		 			= "";
	private String pupEvent 	 			= "";	
	private String eventName 	 			= "";
	private String loanToName 	 			= "";	
	private String caseInfoID 	 			= "";	
	public  String xmlGridData 	 			= "";
	public  String gridCalendar  			= "";
	private String dnEventAction 			= "";	
	private Date eventEndDt 				= null;
	private Date eventStartDt 				= null;
	private String strRequestID 			= "";
	private String strStstusID 				= "";
	private String eventID 	 				= "";
	private String accessFl 	 			= "";
	private String dtType					= "";
	private String eventRequestedMeterial   = ""; 
	
	private HashMap hmEventHeaderInfo = new HashMap();
	
	private ArrayList alLoanTo 				= new ArrayList();
	private ArrayList alEmpList 			= new ArrayList();
	private ArrayList alRepList 			= new ArrayList();
	private ArrayList alPurEvent 			= new ArrayList();
	private ArrayList alEventName 			= new ArrayList();
	private ArrayList alSummaryAction 		= new ArrayList();
	private ArrayList alEventSetDetails 	= new ArrayList();
	private ArrayList alEventItemDetails 	= new ArrayList();
	private ArrayList alOUSList             = new ArrayList();
	
	/**
	 * @return the alOUSList
	 */
	public ArrayList getAlOUSList() {
		return alOUSList;
	}
	/**
	 * @param alOUSList the alOUSList to set
	 */
	public void setAlOUSList(ArrayList alOUSList) {
		this.alOUSList = alOUSList;
	}
	/**
	 * @return the ecYear
	 */
	public int getEcYear() {
		return ecYear;
	}
	/**
	 * @param ecYear the ecYear to set
	 */
	public void setEcYear(int ecYear) {
		this.ecYear = ecYear;
	}
	/**
	 * @return the ecMonth
	 */
	public int getEcMonth() {
		return ecMonth;
	}
	/**
	 * @param ecMonth the ecMonth to set
	 */
	public void setEcMonth(int ecMonth) {
		this.ecMonth = ecMonth;
	}
	/**
	 * @return the endDt
	 */
	public String getEndDt() {
		return endDt;
	}
	/**
	 * @param endDt the endDt to set
	 */
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	/**
	 * @return the hLoad
	 */
	public String gethLoad() {
		return hLoad;
	}
	/**
	 * @param hLoad the hLoad to set
	 */
	public void sethLoad(String hLoad) {
		this.hLoad = hLoad;
	}
	/**
	 * @return the loanTo
	 */
	public String getLoanTo() {
		return loanTo;
	}
	/**
	 * @param loanTo the loanTo to set
	 */
	public void setLoanTo(String loanTo) {
		this.loanTo = loanTo;
	}
	/**
	 * @return the startDt
	 */
	public String getStartDt() {
		return startDt;
	}
	/**
	 * @param startDt the startDt to set
	 */
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	/**
	 * @return the pupEvent
	 */
	public String getPupEvent() {
		return pupEvent;
	}
	/**
	 * @param pupEvent the pupEvent to set
	 */
	public void setPupEvent(String pupEvent) {
		this.pupEvent = pupEvent;
	}
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * @return the loanToName
	 */
	public String getLoanToName() {
		return loanToName;
	}
	/**
	 * @param loanToName the loanToName to set
	 */
	public void setLoanToName(String loanToName) {
		this.loanToName = loanToName;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the gridCalendar
	 */
	public String getGridCalendar() {
		return gridCalendar;
	}
	/**
	 * @param gridCalendar the gridCalendar to set
	 */
	public void setGridCalendar(String gridCalendar) {
		this.gridCalendar = gridCalendar;
	}
	/**
	 * @return the dnEventAction
	 */
	public String getDnEventAction() {
		return dnEventAction;
	}
	/**
	 * @param dnEventAction the dnEventAction to set
	 */
	public void setDnEventAction(String dnEventAction) {
		this.dnEventAction = dnEventAction;
	}
	/**
	 * @return the eventEndDt
	 */
	public Date getEventEndDt() {
		return eventEndDt;
	}
	/**
	 * @param eventEndDt the eventEndDt to set
	 */
	public void setEventEndDt(Date eventEndDt) {
		this.eventEndDt = eventEndDt;
	}
	/**
	 * @return the eventStartDt
	 */
	public Date getEventStartDt() {
		return eventStartDt;
	}
	/**
	 * @param eventStartDt the eventStartDt to set
	 */
	public void setEventStartDt(Date eventStartDt) {
		this.eventStartDt = eventStartDt;
	}
	/**
	 * @return the hmEventHeaderInfo
	 */
	public HashMap getHmEventHeaderInfo() {
		return hmEventHeaderInfo;
	}
	/**
	 * @param hmEventHeaderInfo the hmEventHeaderInfo to set
	 */
	public void setHmEventHeaderInfo(HashMap hmEventHeaderInfo) {
		this.hmEventHeaderInfo = hmEventHeaderInfo;
	}
	/**
	 * @return the alLoanTo
	 */
	public ArrayList getAlLoanTo() {
		return alLoanTo;
	}
	/**
	 * @param alLoanTo the alLoanTo to set
	 */
	public void setAlLoanTo(ArrayList alLoanTo) {
		this.alLoanTo = alLoanTo;
	}
	/**
	 * @return the alEmpList
	 */
	public ArrayList getAlEmpList() {
		return alEmpList;
	}
	/**
	 * @param alEmpList the alEmpList to set
	 */
	public void setAlEmpList(ArrayList alEmpList) {
		this.alEmpList = alEmpList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the alPurEvent
	 */
	public ArrayList getAlPurEvent() {
		return alPurEvent;
	}
	/**
	 * @param alPurEvent the alPurEvent to set
	 */
	public void setAlPurEvent(ArrayList alPurEvent) {
		this.alPurEvent = alPurEvent;
	}
	/**
	 * @return the alEventName
	 */
	public ArrayList getAlEventName() {
		return alEventName;
	}
	/**
	 * @param alEventName the alEventName to set
	 */
	public void setAlEventName(ArrayList alEventName) {
		this.alEventName = alEventName;
	}
	/**
	 * @return the alSummaryAction
	 */
	public ArrayList getAlSummaryAction() {
		return alSummaryAction;
	}
	/**
	 * @param alSummaryAction the alSummaryAction to set
	 */
	public void setAlSummaryAction(ArrayList alSummaryAction) {
		this.alSummaryAction = alSummaryAction;
	}
	/**
	 * @return the alEventSetDetails
	 */
	public ArrayList getAlEventSetDetails() {
		return alEventSetDetails;
	}
	/**
	 * @param alEventSetDetails the alEventSetDetails to set
	 */
	public void setAlEventSetDetails(ArrayList alEventSetDetails) {
		this.alEventSetDetails = alEventSetDetails;
	}
	/**
	 * @return the alEventItemDetails
	 */
	public ArrayList getAlEventItemDetails() {
		return alEventItemDetails;
	}
	/**
	 * @param alEventItemDetails the alEventItemDetails to set
	 */
	public void setAlEventItemDetails(ArrayList alEventItemDetails) {
		this.alEventItemDetails = alEventItemDetails;
	}
	/**
	 * @return the strRequestID
	 */
	public String getStrRequestID() {
		return strRequestID;
	}
	/**
	 * @param strRequestID the strRequestID to set
	 */
	public void setStrRequestID(String strRequestID) {
		this.strRequestID = strRequestID;
	}
	/**
	 * @return the strStstusID
	 */
	public String getStrStstusID() {
		return strStstusID;
	}
	/**
	 * @param strStstusID the strStstusID to set
	 */
	public void setStrStstusID(String strStstusID) {
		this.strStstusID = strStstusID;
	}
	public String getEventID() {
		return eventID;
	}
	public void setEventID(String eventID) {
		this.eventID = eventID;
	}
	public String getAccessFl() {
		return accessFl;
	}
	public void setAccessFl(String accessFl) {
		this.accessFl = accessFl;
	}
	public String getDtType() {
		return dtType;
	}
	public void setDtType(String dtType) {
		this.dtType = dtType;
	}
	/**
	 * @return the eventRequestedMeterial
	 */
	public String getEventRequestedMeterial() {
		return eventRequestedMeterial;
	}
	/**
	 * @param eventRequestedMeterial the eventRequestedMeterial to set
	 */
	public void setEventRequestedMeterial(String eventRequestedMeterial) {
		this.eventRequestedMeterial = eventRequestedMeterial;
	}
	
	
	
}