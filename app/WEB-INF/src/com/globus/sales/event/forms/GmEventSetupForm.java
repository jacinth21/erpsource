package com.globus.sales.event.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmEventSetupForm extends GmLogForm {
	private String repId					 = "";
	private String eventName 				 = "";		
	private String eventVenue 				 = "";
	private String eventLoanTo 				 = "";
	private String eventPurpose 			 = "";	
	private String eventEndDate 			 = "";
	private String favoriteEvent			 = "";
	private String eventSelectAll            = "";
	private String eventStartDate 			 = "";
	private String eventLoanToName 			 = "";
	private String eventRequestedMeterial[]  = new String[10];
	private String caseInfoID 				 = "";
	private String eventID					 = ""; 
	private String selectedCategories 		 = "";
	private String plannedShipeddate		 = "";
	private String plannedshippedDateFl		 = "";
	private String rescheduleSubmitBtnFl     = "";
	private String rescheduleReason			 = "";
	private String expectedReturnDt			 = "";
	private String purpose                   = "";
	private String favouriteCaseID			 = ""; 
	private String backScreen				 = "";
	private String companyInfo  = "";
	
	HashMap hmEventInfo						 = new HashMap();
	
	private ArrayList alFavoriteEventList	 = new ArrayList();
	private ArrayList alEventPurposeList 	 = new ArrayList();
	private ArrayList alEventLoanToList 	 = new ArrayList();
	private ArrayList alEventLoanToNameList  = new ArrayList();
	private ArrayList alEventMaterialReqList = new ArrayList();
	private ArrayList alLogReasons			 = new ArrayList();
	private ArrayList alEmployeeList         = new ArrayList();
	private ArrayList alRepList				 = new ArrayList();
	private ArrayList alrescheduleReasonList = new ArrayList();
	private ArrayList alOUSList = new ArrayList();
	
	/**
	 * @return the alOUSList
	 */
	public ArrayList getAlOUSList() {
		return alOUSList;
	}
	/**
	 * @param alOUSList the alOUSList to set
	 */
	public void setAlOUSList(ArrayList alOUSList) {
		this.alOUSList = alOUSList;
	}
	/**
	 * @return the repId
	 */
	public String getRepId() {
		return repId;
	}
	/**
	 * @param repId the repId to set
	 */
	public void setRepId(String repId) {
		this.repId = repId;
	}
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * @return the eventVenue
	 */
	public String getEventVenue() {
		return eventVenue;
	}
	/**
	 * @param eventVenue the eventVenue to set
	 */
	public void setEventVenue(String eventVenue) {
		this.eventVenue = eventVenue;
	}
	/**
	 * @return the eventLoanTo
	 */
	public String getEventLoanTo() {
		return eventLoanTo;
	}
	/**
	 * @param eventLoanTo the eventLoanTo to set
	 */
	public void setEventLoanTo(String eventLoanTo) {
		this.eventLoanTo = eventLoanTo;
	}
	/**
	 * @return the eventPurpose
	 */
	public String getEventPurpose() {
		return eventPurpose;
	}
	/**
	 * @param eventPurpose the eventPurpose to set
	 */
	public void setEventPurpose(String eventPurpose) {
		this.eventPurpose = eventPurpose;
	}
	/**
	 * @return the eventEndDate
	 */
	public String getEventEndDate() {
		return eventEndDate;
	}
	/**
	 * @param eventEndDate the eventEndDate to set
	 */
	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}
	/**
	 * @return the favoriteEvent
	 */
	public String getFavoriteEvent() {
		return favoriteEvent;
	}
	/**
	 * @param favoriteEvent the favoriteEvent to set
	 */
	public void setFavoriteEvent(String favoriteEvent) {
		this.favoriteEvent = favoriteEvent;
	}
	/**
	 * @return the eventSelectAll
	 */
	public String getEventSelectAll() {
		return eventSelectAll;
	}
	/**
	 * @param eventSelectAll the eventSelectAll to set
	 */
	public void setEventSelectAll(String eventSelectAll) {
		this.eventSelectAll = eventSelectAll;
	}
	/**
	 * @return the eventStartDate
	 */
	public String getEventStartDate() {
		return eventStartDate;
	}
	/**
	 * @param eventStartDate the eventStartDate to set
	 */
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	/**
	 * @return the eventLoanToName
	 */
	public String getEventLoanToName() {
		return eventLoanToName;
	}
	/**
	 * @param eventLoanToName the eventLoanToName to set
	 */
	public void setEventLoanToName(String eventLoanToName) {
		this.eventLoanToName = eventLoanToName;
	}
	/**
	 * @return the eventRequestedMeterial
	 */
	public String[] getEventRequestedMeterial() {
		return eventRequestedMeterial;
	}
	/**
	 * @param eventRequestedMeterial the eventRequestedMeterial to set
	 */
	public void setEventRequestedMeterial(String[] eventRequestedMeterial) {
		this.eventRequestedMeterial = eventRequestedMeterial;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the eventID
	 */
	public String getEventID() {
		return eventID;
	}
	/**
	 * @param eventID the eventID to set
	 */
	public void setEventID(String eventID) {
		this.eventID = eventID;
	}
	/**
	 * @return the selectedCategories
	 */
	public String getSelectedCategories() {
		return selectedCategories;
	}
	/**
	 * @param selectedCategories the selectedCategories to set
	 */
	public void setSelectedCategories(String selectedCategories) {
		this.selectedCategories = selectedCategories;
	}
	/**
	 * @return the plannedShipeddate
	 */
	public String getPlannedShipeddate() {
		return plannedShipeddate;
	}
	/**
	 * @param plannedShipeddate the plannedShipeddate to set
	 */
	public void setPlannedShipeddate(String plannedShipeddate) {
		this.plannedShipeddate = plannedShipeddate;
	}
	/**
	 * @return the plannedshippedDateFl
	 */
	public String getPlannedshippedDateFl() {
		return plannedshippedDateFl;
	}
	/**
	 * @param plannedshippedDateFl the plannedshippedDateFl to set
	 */
	public void setPlannedshippedDateFl(String plannedshippedDateFl) {
		this.plannedshippedDateFl = plannedshippedDateFl;
	}
	/**
	 * @return the rescheduleSubmitBtnFl
	 */
	public String getRescheduleSubmitBtnFl() {
		return rescheduleSubmitBtnFl;
	}
	/**
	 * @param rescheduleSubmitBtnFl the rescheduleSubmitBtnFl to set
	 */
	public void setRescheduleSubmitBtnFl(String rescheduleSubmitBtnFl) {
		this.rescheduleSubmitBtnFl = rescheduleSubmitBtnFl;
	}
	/**
	 * @return the rescheduleReason
	 */
	public String getRescheduleReason() {
		return rescheduleReason;
	}
	/**
	 * @param rescheduleReason the rescheduleReason to set
	 */
	public void setRescheduleReason(String rescheduleReason) {
		this.rescheduleReason = rescheduleReason;
	}
	/**
	 * @return the expectedReturnDt
	 */
	public String getExpectedReturnDt() {
		return expectedReturnDt;
	}
	/**
	 * @param expectedReturnDt the expectedReturnDt to set
	 */
	public void setExpectedReturnDt(String expectedReturnDt) {
		this.expectedReturnDt = expectedReturnDt;
	}
	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}
	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	/**
	 * @return the favouriteCaseID
	 */
	public String getFavouriteCaseID() {
		return favouriteCaseID;
	}
	/**
	 * @param favouriteCaseID the favouriteCaseID to set
	 */
	public void setFavouriteCaseID(String favouriteCaseID) {
		this.favouriteCaseID = favouriteCaseID;
	}
	/**
	 * @return the hmEventInfo
	 */
	public HashMap getHmEventInfo() {
		return hmEventInfo;
	}
	/**
	 * @param hmEventInfo the hmEventInfo to set
	 */
	public void setHmEventInfo(HashMap hmEventInfo) {
		this.hmEventInfo = hmEventInfo;
	}
	/**
	 * @return the alFavoriteEventList
	 */
	public ArrayList getAlFavoriteEventList() {
		return alFavoriteEventList;
	}
	/**
	 * @param alFavoriteEventList the alFavoriteEventList to set
	 */
	public void setAlFavoriteEventList(ArrayList alFavoriteEventList) {
		this.alFavoriteEventList = alFavoriteEventList;
	}
	/**
	 * @return the alEventPurposeList
	 */
	public ArrayList getAlEventPurposeList() {
		return alEventPurposeList;
	}
	/**
	 * @param alEventPurposeList the alEventPurposeList to set
	 */
	public void setAlEventPurposeList(ArrayList alEventPurposeList) {
		this.alEventPurposeList = alEventPurposeList;
	}
	/**
	 * @return the alEventLoanToList
	 */
	public ArrayList getAlEventLoanToList() {
		return alEventLoanToList;
	}
	/**
	 * @param alEventLoanToList the alEventLoanToList to set
	 */
	public void setAlEventLoanToList(ArrayList alEventLoanToList) {
		this.alEventLoanToList = alEventLoanToList;
	}
	/**
	 * @return the alEventLoanToNameList
	 */
	public ArrayList getAlEventLoanToNameList() {
		return alEventLoanToNameList;
	}
	/**
	 * @param alEventLoanToNameList the alEventLoanToNameList to set
	 */
	public void setAlEventLoanToNameList(ArrayList alEventLoanToNameList) {
		this.alEventLoanToNameList = alEventLoanToNameList;
	}
	/**
	 * @return the alEventMaterialReqList
	 */
	public ArrayList getAlEventMaterialReqList() {
		return alEventMaterialReqList;
	}
	/**
	 * @param alEventMaterialReqList the alEventMaterialReqList to set
	 */
	public void setAlEventMaterialReqList(ArrayList alEventMaterialReqList) {
		this.alEventMaterialReqList = alEventMaterialReqList;
	}
	/**
	 * @return the alLogReasons
	 */
	public ArrayList getAlLogReasons() {
		return alLogReasons;
	}
	/**
	 * @param alLogReasons the alLogReasons to set
	 */
	public void setAlLogReasons(ArrayList alLogReasons) {
		this.alLogReasons = alLogReasons;
	}
	/**
	 * @return the alEmployeeList
	 */
	public ArrayList getAlEmployeeList() {
		return alEmployeeList;
	}
	/**
	 * @param alEmployeeList the alEmployeeList to set
	 */
	public void setAlEmployeeList(ArrayList alEmployeeList) {
		this.alEmployeeList = alEmployeeList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the alrescheduleReasonList
	 */
	public ArrayList getAlrescheduleReasonList() {
		return alrescheduleReasonList;
	}
	/**
	 * @param alrescheduleReasonList the alrescheduleReasonList to set
	 */
	public void setAlrescheduleReasonList(ArrayList alrescheduleReasonList) {
		this.alrescheduleReasonList = alrescheduleReasonList;
	}
	/**
	 * @return the backScreen
	 */
	public String getBackScreen() {
		return backScreen;
	}
	/**
	 * @param backScreen the backScreen to set
	 */
	public void setBackScreen(String backScreen) {
		this.backScreen = backScreen;
	}
	/**
	 * @return the companyInfo
	 */
	public String getCompanyInfo() {
		return companyInfo;
	}
	/**
	 * @param companyInfo the companyInfo to set
	 */
	public void setCompanyInfo(String companyInfo) {
		this.companyInfo = companyInfo;
	}
	
	
}
