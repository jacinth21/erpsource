package com.globus.sales.event.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmAutoCompletePartyListRptBean;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.sales.CaseManagement.beans.GmCaseBuilderBean;
import com.globus.sales.event.beans.GmEventSetupBean;
import com.globus.sales.event.forms.GmEventSetupForm;

public class GmEventSetupAction extends GmDispatchAction {
  public ActionForward addEvent(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());
    log.debug("GmEventSetupAction---");
    instantiate(request, response);
    GmEventSetupForm gmEventSetupForm = (GmEventSetupForm) form;
    gmEventSetupForm.loadSessionParameters(request);
    GmLogonBean gmLogon = new GmLogonBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmCaseBuilderBean gmCaseBuilderBean = new GmCaseBuilderBean(getGmDataStoreVO());
    GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmEventInfo = new HashMap();
    ArrayList alEmpList = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alLoanToNamesList = new ArrayList();
    ArrayList alFavoriteEventList = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmEventSetupForm);
    String strCaseInfoId = "";
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPartyId = GmCommonClass.parseNull(gmEventSetupForm.getSessPartyId());

    String strBackScreen = GmCommonClass.parseNull((String) hmParam.get("BACKSCREEN"));
    if (strBackScreen.equals("")) {
      strBackScreen = "summaryscreen";
    }
    String strAccessFl = "";
    String strSelectedCategories = "";
    String strEventStatus = "";
    String strCreatedUser = "";
    String strForward = "";

    Boolean ErrorFl = false;
    log.debug("strOpt---" + strOpt);
    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "EVENT", "IH_EVENT_ACCESS");
    if (!strOpt.equals("reschedule")) {
      gmEventSetupForm.setAlEventPurposeList(GmCommonClass.parseNullArrayList(gmAutoCompleteReportBean
          .getCodeList("EVEPUR",null)));
      gmEventSetupForm.setAlEventLoanToList(GmCommonClass.parseNullArrayList(gmAutoCompleteReportBean
          .getCodeList("EVELON",null)));
      gmEventSetupForm.setAlEventMaterialReqList(GmCommonClass.parseNullArrayList(gmAutoCompleteReportBean
          .getCodeList("EVEMET",null)));
    }
    if (strAccessFl.equals("Y")) {
      // strEventStatus = gmEventSetupBean.fetchEventReqApproved(hmParam);
      strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "EVENT_APPROVER");
      log.debug("strAccessFl" + strAccessFl);

      /*
       * if(((strAccessFl.equals("Y") && strEventStatus.equals("Y"))) ||
       * ((strEventStatus.equals("N")) || (strEventStatus.equals("NR")))){
       */

      if (strOpt.equals("save") || strOpt.equals("next")) {
        strSelectedCategories =
            GmCommonClass.createInputString(gmEventSetupForm.getEventRequestedMeterial());
        if (strSelectedCategories.equals("")) {
          hmParam.put("SELECTEDCATGR", ",");
        } else {
          hmParam.put("SELECTEDCATGR", strSelectedCategories);
        }
        String strFavCaseID = gmEventSetupForm.getFavouriteCaseID();
        log.debug("strSelectedCategories------" + strSelectedCategories);
        log.debug("hmParam------" + hmParam);
        strCaseInfoId = gmEventSetupBean.saveEventBookingInfo(hmParam);
        hmParam.put("CASEINFOID", strCaseInfoId);
        gmEventSetupForm.setCaseInfoID(strCaseInfoId);
        if (strOpt.equals("save"))
          strOpt = "edit";
        gmEventSetupForm.setStrOpt(strOpt);
        gmCaseBuilderBean.saveCaseBuilder(hmParam);
        if (!strFavCaseID.equals("0") && !strFavCaseID.equals("")) {
          hmParam.put("FAVOURITECASEID", strFavCaseID);
          gmCaseBuilderBean.saveFavSets(hmParam);
        }
        if (strSelectedCategories.equals("") && !strOpt.equals("save")) {
          strForward =
              "/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=HTML&caseInfoID="
                  + strCaseInfoId;
          return actionRedirect(strForward, request);
        }
      }
      if (strOpt.equals("next")) {
        // ActionForward actionForward = mapping.findForward("savenext");
        // ActionForward nextForward = new ActionForward(actionForward);
        String strFwdProductType = "";
        String strPath = "";
        String[] arryCategories = strSelectedCategories.split(",");
        Arrays.sort(arryCategories);
        strFwdProductType = arryCategories[0];
        log.debug("strEventInfoId------" + strCaseInfoId);
        log.debug("strFwdProductType------" + strFwdProductType);
        log.debug("strSelectedCategories------" + strSelectedCategories);
        if (strFwdProductType.equals("19533")) {// item
          strPath =
              "/gmEventItemDtl.do?strOpt=load&fromscreen=eventsetup&backScreen=" + strBackScreen
                  + "&caseInfoID=" + strCaseInfoId + "&categoryList=" + strSelectedCategories
                  + "&productionType=" + strFwdProductType;
        } else {
          strPath =
              "/gmCaseBookSetDtls.do?strOpt=editEvent&fromscreen=eventsetup&backScreen="
                  + strBackScreen + "&caseInfoID=" + strCaseInfoId + "&caseID=&fwdProductType="
                  + strFwdProductType + "&categoryList=" + strSelectedCategories;// +"&screenNote="+screenTypeName;//+"&interBodyFL="+interBodyFL+"&biologicFL="+biologicFL+"&fixationFL="+fixationFL
          // +"&accessFL="+accessFL+"&favouriteCaseID="+
          // strFavCaseID;
        }
        log.debug("=====strPath==========" + strPath);
        // nextForward.setPath(strPath);
        // nextForward.setRedirect(true);
        return actionRedirect(strPath, request);
      }
      hmParam.put("STROPT", "EVENT");
      if (strOpt.equals("fetchFavorite")) {
        HashMap hmFavourite = gmCaseBuilderBean.fetchFavoriteDtls(hmParam);
        GmCommonClass.getFormFromHashMap(gmEventSetupForm, hmFavourite);
        gmEventSetupForm.setCaseInfoID(gmEventSetupForm.getCaseInfoID());
      }
      if (strOpt.equals("edit")) {
        strCaseInfoId = gmEventSetupForm.getCaseInfoID();
        hmEventInfo =
            GmCommonClass.parseNullHashMap(gmEventSetupBean.fetchEventBookingInfo(strCaseInfoId,
                strUserID));
        strCreatedUser = GmCommonClass.parseNull((String) hmEventInfo.get("CREATEDUSER"));
        if (!strCreatedUser.equals(strUserID) && !strAccessFl.equals("Y")) {
          ErrorFl = true;
        }
        GmCommonClass.getFormFromHashMap(gmEventSetupForm, hmEventInfo);
      }
      /*
       * }else { ErrorFl=true; }
       */
    } else {
      ErrorFl = true;
    }
    if (ErrorFl) {
      throw new AppError("", "20592");
    }
    GmAutoCompletePartyListRptBean gmAutoCompletePartyListRptBean= new GmAutoCompletePartyListRptBean(getGmDataStoreVO());
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());//PMt-57205 demo-sets-loan-to-dropdown
    GmResourceBundleBean gmResourceBundleBean =
            GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strFlag = "Y";
    ArrayList alOUSList = new ArrayList();
    HashMap hmData = new HashMap();
    hmData.put("USERTYPE","300");
    hmData.put("USERSTATUS","311");
    
    String strEventDtl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PROP_SHOW_EVENT_DTL"));
    alRepList = gmAutoCompletePartyListRptBean.getUSRepList(strFlag);
    alEmpList = gmAutoCompletePartyListRptBean.getEmployeeList(hmData);
  //PMT-57205 Demo Sets - Loan TO Dropdown should Load Japanese Sales rep
    alOUSList = (strEventDtl.equals("Y"))?gmAutoCompletePartyListRptBean.getJPNSalesList(strFlag):gmAutoCompletePartyListRptBean.getOUSSalesList(strFlag);
   //alRepList = gmCust.getRepList("Active");
   //alEmpList = gmLogon.getEmployeeList("300", "");
    gmEventSetupForm.setAlOUSList(alOUSList);
    gmEventSetupForm.setAlEmployeeList(alEmpList);
    gmEventSetupForm.setAlRepList(alRepList);
    gmEventSetupForm.setAlFavoriteEventList(GmCommonClass.parseNullArrayList(gmCaseBuilderBean
        .fetchFavoriteCase(hmParam)));
    gmEventSetupForm.setCaseInfoID(strCaseInfoId);
    alLogReasons = gmCommonBean.getLog(strCaseInfoId, "1287");
    gmEventSetupForm.setAlLogReasons(alLogReasons);
    return mapping.findForward("gmEventSetup");
  }

  public ActionForward rescheduleEvent(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmEventSetupForm gmEventSetupForm = (GmEventSetupForm) form;
    gmEventSetupForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmEventInfo = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmEventSetupForm);
    String strEventStatus = "";
    String strAccessFl = "";
    String strShippedDtFl = "Y";
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strCreatedUser = "";
    Boolean ErrorFl = false;
    String strForward = "";
    log.debug("The Event ID comming from the HiperLInk is * * * *  " + strCaseInfoId);
    gmEventSetupForm.setAlrescheduleReasonList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("EVERSN")));
    strAccessFl = gmEventSetupBean.getEventAccess(strUserID, "EVENT", "IH_EVENT_ACCESS");
    // strCaseStatus = get_case_status(strCaseInfoId);
    if (strAccessFl.equals("Y")) {
      if (strOpt.equals("reschedule")) {
        // strEventStatus = gmEventSetupBean.fetchEventReqApproved(hmParam);
        strAccessFl = gmEventSetupBean.getEventAccess(strUserID, "GROUP", "EVENT_APPROVER");
        log.debug("strAccessFl" + strAccessFl);
        log.debug("strEventStatus" + strEventStatus);
        // if(((strAccessFl.equals("Y") && strEventStatus.equals("Y"))) ||
        // ((strEventStatus.equals("N")) || (strEventStatus.equals("NR")))){

        hmEventInfo = gmEventSetupBean.fetchEventBookingInfo(strCaseInfoId, strUserID);
        strCreatedUser = GmCommonClass.parseNull((String) hmEventInfo.get("CREATEDUSER"));
        if (!strCreatedUser.equals(strUserID) && !strAccessFl.equals("Y")) {
          throw new AppError("", "20592");
        }

        GmCommonClass.getFormFromHashMap(gmEventSetupForm, hmEventInfo);
        alLogReasons = gmCommonBean.getLog(strCaseInfoId, "1287");
        strEventStatus = gmEventSetupBean.fetchEventReqApproved(hmParam);
        if (strEventStatus.equals("NR")) {
          strShippedDtFl = "N";
        }
        /*
         * }else { ErrorFl=true; }
         */
      }
      if (strOpt.equals("savereschedule")) {
        strCaseInfoId = gmEventSetupBean.saveEventRescheduleInfo(hmParam);
        gmEventSetupForm.setCaseInfoID(strCaseInfoId);
        strForward =
            "/gmEventReport.do?method=downloadSummaryAsPDF&strOpt=HTML&caseInfoID=" + strCaseInfoId;
        return actionRedirect(strForward, request);
      }
    } else {
      ErrorFl = true;
    }
    if (ErrorFl) {
      throw new AppError("", "20592");
    }
    gmEventSetupForm.setStrOpt(strOpt);
    gmEventSetupForm.setAlLogReasons(alLogReasons);
    gmEventSetupForm.setPlannedshippedDateFl(strShippedDtFl);
    return mapping.findForward("gmEventSetup");
  }

  public ActionForward voidFavorites(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmEventSetupForm gmEventSetupForm = (GmEventSetupForm) form;
    gmEventSetupForm.loadSessionParameters(request);
    GmCaseBuilderBean gmCaseBuilderBean = new GmCaseBuilderBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alFavoritesList = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmEventSetupForm);
    alFavoritesList =
        GmCommonClass.parseNullArrayList(gmCaseBuilderBean.fetchFavoriteCase(hmParam));
    gmEventSetupForm.setAlFavoriteEventList(alFavoritesList);
    return mapping.findForward("gmEventFavoriteVoid");
  }
}
