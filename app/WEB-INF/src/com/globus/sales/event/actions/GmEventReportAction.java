package com.globus.sales.event.actions;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.event.beans.GmEventReportBean;
import com.globus.sales.event.beans.GmEventSetupBean;
import com.globus.sales.event.forms.GmEventReportForm;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmAutoCompletePartyListRptBean;

public class GmEventReportAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward listEvent(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    String strOpt = "";
    String strApplDateFmt = "";
    String strStartDt = "";
    String strEndDt = "";
    int currMonth = 0;
    String strForward = "GmEventListRpt";

    ArrayList alResult = new ArrayList();
    ArrayList alEmpList = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alEventName = new ArrayList();
    ArrayList alList = new ArrayList();
    RowSetDynaClass rdresult = null;

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();

    GmEventReportForm gmEventReportForm = (GmEventReportForm) actionForm;
    gmEventReportForm.loadSessionParameters(request);

    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean();
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
    GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmAutoCompletePartyListRptBean gmAutoCompletePartyListRptBean= new GmAutoCompletePartyListRptBean();
    hmParam = GmCommonClass.getHashMapFromForm(gmEventReportForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDT"));
    strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alEventName = GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventNameList());
    gmEventReportForm.setAlEventName(alEventName); // set the AuditName ArrayList into form property

    gmEventReportForm.setAlPurEvent(GmCommonClass.parseNullArrayList(gmAutoCompleteReportBean
        .getCodeList("EVEPUR",null)));
    gmEventReportForm.setAlLoanTo(GmCommonClass.parseNullArrayList(gmAutoCompleteReportBean
        .getCodeList("EVELON",null)));
    String strFlag = "Y";
    ArrayList alOUSList = new ArrayList();
    HashMap hmData = new HashMap();
    hmData.put("USERTYPE","300");
    hmData.put("USERSTATUS","311");
    alRepList = gmAutoCompletePartyListRptBean.getUSRepList(strFlag);
    alEmpList = gmAutoCompletePartyListRptBean.getEmployeeList(hmData);
    alOUSList = gmAutoCompletePartyListRptBean.getOUSSalesList(strFlag);
    gmEventReportForm.setAlOUSList(alOUSList);
   // alEmpList = gmLogonBean.getEmployeeList();
    gmEventReportForm.setAlEmpList(alEmpList);

    alRepList = gmCust.getRepList();
    gmEventReportForm.setAlRepList(alRepList);

    if (strOpt.equals("Reload")) {
      hmParam.put("SESSDATEFMT", strApplDateFmt);

      hmResult = gmEventRptBean.fetchEventListReport(hmParam);
      hmParam.putAll(hmResult);
      request.setAttribute("hmParam", hmParam);
      hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      gmEventReportForm.setXmlGridData(getXmlGridData(hmParam));
    }
    return actionMapping.findForward(strForward);
  }

  public ActionForward downloadSummaryAsPDF(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    String strOpt = "";
    String strForward = "GmEventSummary";
    String strJasperName = "/GmEventSummary.jasper";

    HashMap hmEventHeaderInfo = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmLoop = new HashMap();
    ArrayList alEventSetDetails = new ArrayList();
    ArrayList alEventItemDetails = new ArrayList();
    ArrayList alSummaryAction = new ArrayList();

    GmEventReportForm gmEventReportForm = (GmEventReportForm) actionForm;
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());

    String strCaseInfoID = GmCommonClass.parseNull(gmEventReportForm.getCaseInfoID());

    gmEventReportForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmEventReportForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strRequestID = GmCommonClass.parseNull(gmEventRptBean.fetchCaseRequestID(strCaseInfoID));
    gmEventReportForm.setStrRequestID(strRequestID);
    log.debug(strOpt + "::strOpt::" + strCaseInfoID + ":strCaseInfoID:" + strJasperName
        + ":strJasperName");
    String strUserID = GmCommonClass.parseNull(gmEventReportForm.getUserId());
    String strPartyId = GmCommonClass.parseNull(gmEventReportForm.getSessPartyId());
    hmEventHeaderInfo =
        GmCommonClass.parseNullHashMap(gmEventSetupBean.fetchEventBookingInfo(strCaseInfoID,
            strUserID));
    String strEventName = GmCommonClass.parseNull((String) hmEventHeaderInfo.get("EVENTNAME"));
    String strEventID = GmCommonClass.parseNull((String) hmEventHeaderInfo.get("EVENTID"));
    alEventSetDetails =
        GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventSetDetails(strCaseInfoID));
    alEventItemDetails =
        GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventItemDetails(strCaseInfoID));
    /*
     * The Code from Line 152 to Line 162 is added for resolving the BUG-1814 [Jasper Alignments] .
     * Here we are setting empty values to the corresponding arrayList to call the new
     * JRBeanCollectionDataSource() from Main Jasper report.
     */
    int intItemArraySize = alEventItemDetails.size();
    int intSetArraySize = alEventSetDetails.size();
    hmLoop.put("NO", "");
    if (intSetArraySize == 0) {
      alEventSetDetails.add(hmLoop);
    }
    if (intItemArraySize == 0) {
      alEventItemDetails.add(hmLoop);
    }
    hmEventHeaderInfo.put("ALSHIPARRAYSIZE", intSetArraySize);
    hmEventHeaderInfo.put("ALITEMARRAYSIZE", intItemArraySize);
    hmEventHeaderInfo.put(
        "SUBREPORT_DIR",
        request.getSession().getServletContext()
            .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
            + "\\");
    hmEventHeaderInfo.put("ALSHIPSETLIST", alEventSetDetails);
    hmEventHeaderInfo.put("ALSHIPITEMLIST", alEventItemDetails);
    gmEventReportForm.setEventName(strEventName);
    gmEventReportForm.setHmEventHeaderInfo(hmEventHeaderInfo);
    gmEventReportForm.setAlEventSetDetails(alEventSetDetails);
    gmEventReportForm.setAlEventItemDetails(alEventItemDetails);
    String StrStatusId = GmCommonClass.parseNull((String) hmEventHeaderInfo.get("STATUSID"));
    gmEventReportForm.setStrStstusID(StrStatusId);
    gmEventReportForm.setEventRequestedMeterial(GmCommonClass.parseNull((String) hmEventHeaderInfo
        .get("SELECTEDCATEGORIES")));
    String strEventAccessFl =
        gmEventSetupBean.getEventAccess(strPartyId, "EVENT", "IH_EVENT_ACCESS");
    String strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "EVENT_APPROVER");
    String strCreatedUser = GmCommonClass.parseNull((String) hmEventHeaderInfo.get("CREATEDUSER"));
    if (!strCreatedUser.equals(strUserID) && !strAccessFl.equals("Y")
        || !strEventAccessFl.equals("Y")) {
      gmEventReportForm.setAccessFl("Y");
    }
    gmEventReportForm.setEventID(strEventID);
    if (strOpt.equals("HTML")) {
      alSummaryAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("EVESUM"));
      gmEventReportForm.setAlSummaryAction(alSummaryAction);
    } else if (strOpt.equals("PDF")) {
      GmJasperReport gmJasperReport = new GmJasperReport();
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setJasperReportName(strJasperName);
      gmJasperReport.setHmReportParameters(hmEventHeaderInfo);
      gmJasperReport.setReportDataList(null);
      gmJasperReport.createJasperPdfReport();
      return null;
    }
    return actionMapping.findForward(strForward);
  }

  private String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = new ArrayList();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALFIELDSALESINFO"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateSubDir("sales/event/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.Event.GmEventListReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmEventListReport.vm");
    return templateUtil.generateOutput();
  }

  public ActionForward viewEventCalendar(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);

    GmEventReportForm gmEventListReportForm = (GmEventReportForm) form;
    gmEventListReportForm.loadSessionParameters(request);
    gmEventListReportForm.setApplnDateFmt(getGmDataStoreVO().getCmpdfmt());
    log.debug(" * * * * *  welcome to View Event Calendar Method  * * *  * * ");
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmEventReportBean gmEventReportBean = new GmEventReportBean(getGmDataStoreVO());
    HttpSession session = request.getSession(false);
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmTemplateData = new HashMap();
    HashMap hmResult = new HashMap();
    String strXmlData = "";
    String strClientSysType =
        GmCommonClass.parseNull((String) session.getAttribute("strSessClientSysType"));
    log.debug("inside of GmCaseCalendarAction");
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    log.debug("currMonth >>> " + currMonth);
    String strMonthFirstDay =
        GmCalenderOperations.getFirstDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());
    String strMonthLastDay =
        GmCalenderOperations.getLastDayOfMonth(currMonth, getGmDataStoreVO().getCmpdfmt());

    log.debug("strMonthFirstDay >>> " + strMonthFirstDay);
    log.debug("strMonthLastDay >>> " + strMonthLastDay);
    String strAccessCondition = "";
    String strOpt = gmEventListReportForm.getStrOpt();
    String strAction = GmCommonClass.parseNull(gmEventListReportForm.getHaction());
    String strhLoad = GmCommonClass.parseNull(gmEventListReportForm.gethLoad());
    // Date currDate = GmCommonClass.getCurrentDate(strApplnDateFmt);

    // GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    // Date currDate = (Date)GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    Date currDate =
        GmCommonClass.getCurrentDate(getGmDataStoreVO().getCmpdfmt(), getGmDataStoreVO()
            .getCmptzone());
    log.debug("currDate >>> " + currDate);

    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());


    log.debug(" # ## currDate " + currDate + " #># # " + currDate.toString() + ": strTodaysDate : "
        + strTodaysDate + " ## # TZ : " + getGmDataStoreVO().getCmptzone() + " Dt Fmt : "
        + getGmDataStoreVO().getCmpdfmt());

    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    strAccessCondition = getAccessFilter(request, response);
    log.debug("strAccessCondition >>> " + strAccessCondition);
    log.debug("strOpt >>> " + strOpt);

    if (strOpt.equals("load")) {
      int intYear = gmEventListReportForm.getEcYear();
      int intMonth = gmEventListReportForm.getEcMonth();
      log.debug("intMonth-" + intMonth + "intYear" + intYear);
      strMonthFirstDay =
          getFirstDayOfMonth(((intMonth - 1 <= 0) ? 11 : (intMonth - 1)),
              ((intMonth - 1 <= 0) ? (intYear - 1) : intYear), strApplnDateFmt);
      gmEventListReportForm.setEventStartDt((GmCalenderOperations.getDate(strMonthFirstDay)));
      strMonthLastDay =
          getLastDayOfMonth(((intMonth + 1 > 11) ? 0 : (intMonth + 1)),
              ((intMonth + 1 > 11) ? (intYear + 1) : intYear), strApplnDateFmt);
      gmEventListReportForm.setEventEndDt(GmCalenderOperations.getDate(strMonthLastDay));
      log.debug("str Start Date-" + GmCalenderOperations.getDate(strMonthFirstDay)
          + "str end Date-" + GmCalenderOperations.getDate(strMonthLastDay));
      currDate =
          GmCalenderOperations.getDate(getFirstDayOfMonth(intMonth, intYear, strApplnDateFmt));
    }
    if (strAction.equals("day") || strAction.equals("week")) {
      currDate = gmEventListReportForm.getEventStartDt();
    }
    gmEventListReportForm.setEventEndDt(GmCommonClass.getStringToDate(strMonthLastDay,
        getGmDataStoreVO().getCmpdfmt()));
    gmEventListReportForm.setEventStartDt(GmCommonClass.getStringToDate(strMonthFirstDay,
        getGmDataStoreVO().getCmpdfmt()));

    hmParam = GmCommonClass.getHashMapFromForm(gmEventListReportForm);
    hmParam.put("AccessFilter", strAccessCondition);
    hmParam.put("DATEFORMAT", strApplnDateFmt);
    hmParam.put("CLIENTSYSTYPE", strClientSysType);
    hmParam.put("DTTYPE", "ESD");
    hmResult = gmEventReportBean.fetchEventListReport(hmParam);
    hmTemplateData.putAll(hmResult);
    hmTemplateData.put("DATEFORMAT", strApplnDateFmt);
    gmEventListReportForm.setEventStartDt(currDate);
    strXmlData = GmCommonClass.parseNull(generateOutPut(hmTemplateData));

    gmEventListReportForm.setGridCalendar(strXmlData);
    if (strClientSysType.equals("IPAD") && strhLoad.equals("SAF")) {
      PrintWriter pw = response.getWriter();
      pw.write(strXmlData);
      pw.flush();
      pw.close();
      return null;
    }
    return mapping.findForward("gmEventCalendar");
  }

  private String generateOutPut(HashMap hmTemplateValues) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = new ArrayList();
    alResult =
        GmCommonClass.parseNullArrayList((ArrayList) hmTemplateValues.get("ALFIELDSALESINFO"));
    templateUtil.setDataMap("hmParam", hmTemplateValues);
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateSubDir("sales/event/templates");
    templateUtil.setTemplateName("GmEventCalendar.vm");
    return templateUtil.generateOutput();
  }

  public String getFirstDayOfMonth(int intMonth, int intYear, String strApplnDateFmt) {
    // Logger log = GmLogger.getInstance(this.getClass().getName());
    // log.debug("coming here");
    SimpleDateFormat fmt = new SimpleDateFormat(strApplnDateFmt);
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.MONTH, intMonth);
    calendar.set(Calendar.YEAR, intYear);
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    return fmt.format(calendar.getTime());
  }

  public static String getLastDayOfMonth(int intMonth, int intYear, String strApplnDateFmt) {
    // Logger log = GmLogger.getInstance(this.getClass().getName());
    // log.debug("coming here");
    SimpleDateFormat fmt = new SimpleDateFormat(strApplnDateFmt);
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.MONTH, intMonth);
    calendar.set(Calendar.YEAR, intYear);
    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    return fmt.format(calendar.getTime());
  }

}
