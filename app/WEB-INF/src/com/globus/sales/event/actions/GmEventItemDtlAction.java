package com.globus.sales.event.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.event.beans.GmEventReportBean;
import com.globus.sales.event.beans.GmEventSetupBean;
import com.globus.sales.event.forms.GmEventItemDtlForm;

public class GmEventItemDtlAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmEventItemDtlForm gmEventItemDtlForm = (GmEventItemDtlForm) actionForm;
    gmEventItemDtlForm.loadSessionParameters(request);

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmEventItemDtlForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strCaseInfoID = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strFromScreen = GmCommonClass.parseNull((String) hmParam.get("FROMSCREEN"));
    String strGridData = "";
    String strCategoryList = GmCommonClass.parseNull((String) hmParam.get("CATEGORYLIST"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    if (strOpt.equals("nextConfirm") || strOpt.equals("eventNextConfirm")) {
      gmEventRptBean.saveEventItemInfo(hmParam);
      gmEventSetupBean.saveEventRequestDetails(hmParam);
      return actionMapping.findForward("savenext");
    }

    alResult = gmEventRptBean.fetchBookedItems(strCaseInfoID);

    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateSubDir("sales/event/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.Event.GmEventItemDtl", strSessCompanyLocale));
    templateUtil.setTemplateName("GmEventItemDtl.vm");
    strGridData = templateUtil.generateOutput();
    gmEventItemDtlForm.setGridData(strGridData);
    return actionMapping.findForward("GmEventItemDtl");
  }
}
