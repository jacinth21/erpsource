package com.globus.sales.areaset.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.forms.GmCommonForm;

public class GmAreaSetForm extends GmCommonForm{
	
	
	private ArrayList alShipCarr = new ArrayList();
	private ArrayList alSetStatus = new ArrayList();
	private ArrayList alSetActualStatus = new ArrayList();
	private ArrayList alDates = new ArrayList();
	private ArrayList alResult = new ArrayList();
	
	protected String method = "";
	
	private String carrier="";
	private String status="";
	private String actualStatus="";
	private String dates="";
	private String txtFromDate="";
	private String txtToDate="";
	private String tagFrom="";
	private String tagTo="";
	private String repID="";
	private String type="";
	private String hTxnVal="";
	private String refID="";
	private String addID="";
	private String newAddID="";
	private String oldAddID="";
	private String shipID="";
	private String caseID="";
	private String delMode="";
	private String delCarrier="";
	private String tNo="";
	private String address="";
	private String account="";
	private String tagID="";
	private String consignTo="";
	private String surgeryDate="";
	private String shipCarrier="";
	private String shipMode="";
	private String freightAmt="";
	private String trackingNo="";
	private String popup="";
	private String shipType="";
	private String newShipType="";
	private String newRepID="";
	private String email="";
	private String statusfl="";
	private String parentFrmName = "";
	private String chkAppFl = "";
	private String caseRepID = "";
	private String caseAccID = "";
	private String caseDistID = "";
	private String caseAccNm = "";
	private String caseRepNm = "";
	private String addIdReturned="";
	private String hAddrId ="";
	
	public String gethAddrId() {
		return hAddrId;
	}
	public void sethAddrId(String hAddrId) {
		this.hAddrId = hAddrId;
	}
	public String getAddIdReturned() {
		return addIdReturned;
	}
	public void setAddIdReturned(String addIdReturned) {
		this.addIdReturned = addIdReturned;
	}
	public String getCaseAccNm() {
		return caseAccNm;
	}
	public void setCaseAccNm(String caseAccNm) {
		this.caseAccNm = caseAccNm;
	}
	public String getCaseRepNm() {
		return caseRepNm;
	}
	public void setCaseRepNm(String caseRepNm) {
		this.caseRepNm = caseRepNm;
	}
	public String getCaseRepID() {
		return caseRepID;
	}
	public void setCaseRepID(String caseRepID) {
		this.caseRepID = caseRepID;
	}
	public String getCaseAccID() {
		return caseAccID;
	}
	public void setCaseAccID(String caseAccID) {
		this.caseAccID = caseAccID;
	}
	public String getCaseDistID() {
		return caseDistID;
	}
	public void setCaseDistID(String caseDistID) {
		this.caseDistID = caseDistID;
	}
	public String getChkAppFl() {
		return chkAppFl;
	}
	public void setChkAppFl(String chkAppFl) {
		this.chkAppFl = chkAppFl;
	}
		
	/**
	 * @return the parentFrmName
	 */
	public String getParentFrmName() {
		return parentFrmName;
	}
	/**
	 * @param parentFrmName the parentFrmName to set
	 */
	public void setParentFrmName(String parentFrmName) {
		this.parentFrmName = parentFrmName;
	}
	public String getStatusfl() {
		return statusfl;
	}
	public void setStatusfl(String statusfl) {
		this.statusfl = statusfl;
	}
	public String getOldAddID() {
		return oldAddID;
	}
	public void setOldAddID(String oldAddID) {
		this.oldAddID = oldAddID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNewShipType() {
		return newShipType;
	}
	public void setNewShipType(String newShipType) {
		this.newShipType = newShipType;
	}
	public String getNewRepID() {
		return newRepID;
	}
	public void setNewRepID(String newRepID) {
		this.newRepID = newRepID;
	}
	public String getShipType() {
		return shipType;
	}
	public void setShipType(String shipType) {
		this.shipType = shipType;
	}
	public String getPopup() {
		return popup;
	}
	public void setPopup(String popup) {
		this.popup = popup;
	}
	public String getShipCarrier() {
		return shipCarrier;
	}
	public void setShipCarrier(String shipCarrier) {
		this.shipCarrier = shipCarrier;
	}
	public String getShipMode() {
		return shipMode;
	}
	public void setShipMode(String shipMode) {
		this.shipMode = shipMode;
	}
	public String getFreightAmt() {
		return freightAmt;
	}
	public void setFreightAmt(String freightAmt) {
		this.freightAmt = freightAmt;
	}
	public String getTrackingNo() {
		return trackingNo;
	}
	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCaseID() {
		return caseID;
	}
	public void setCaseID(String caseID) {
		this.caseID = caseID;
	}
	public String getDelMode() {
		return delMode;
	}
	public void setDelMode(String delMode) {
		this.delMode = delMode;
	}
	public String getDelCarrier() {
		return delCarrier;
	}
	public void setDelCarrier(String delCarrier) {
		this.delCarrier = delCarrier;
	}
	public String gettNo() {
		return tNo;
	}
	public void settNo(String tNo) {
		this.tNo = tNo;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getTagID() {
		return tagID;
	}
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}
	public String getConsignTo() {
		return consignTo;
	}
	public void setConsignTo(String consignTo) {
		this.consignTo = consignTo;
	}
	public String getSurgeryDate() {
		return surgeryDate;
	}
	public void setSurgeryDate(String surgeryDate) {
		this.surgeryDate = surgeryDate;
	}
	public String getShipID() {
		return shipID;
	}
	public void setShipID(String shipID) {
		this.shipID = shipID;
	}
	public String getNewAddID() {
		return newAddID;
	}
	public void setNewAddID(String newAddID) {
		this.newAddID = newAddID;
	}
	public String getRefID() {
		return refID;
	}
	public void setRefID(String refID) {
		this.refID = refID;
	}
	public String getAddID() {
		return addID;
	}
	public void setAddID(String addID) {
		this.addID = addID;
	}
	public String gethTxnVal() {
		return hTxnVal;
	}
	public void sethTxnVal(String hTxnVal) {
		this.hTxnVal = hTxnVal;
	}
	public String getTxtFromDate() {
		return txtFromDate;
	}
	public void setTxtFromDate(String txtFromDate) {
		this.txtFromDate = txtFromDate;
	}
	public String getTxtToDate() {
		return txtToDate;
	}
	public void setTxtToDate(String txtToDate) {
		this.txtToDate = txtToDate;
	}
	public String getTagFrom() {
		return tagFrom;
	}
	public void setTagFrom(String tagFrom) {
		this.tagFrom = tagFrom;
	}
	public String getTagTo() {
		return tagTo;
	}
	public void setTagTo(String tagTo) {
		this.tagTo = tagTo;
	}
	
	public String getRepID() {
		return repID;
	}
	public void setRepID(String repID) {
		this.repID = repID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ArrayList getAlResult() {
		return alResult;
	}
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getActualStatus() {
		return actualStatus;
	}
	public void setActualStatus(String actualStatus) {
		this.actualStatus = actualStatus;
	}
	public String getDates() {
		return dates;
	}
	public void setDates(String dates) {
		this.dates = dates;
	}
	public ArrayList getAlSetStatus() {
		return alSetStatus;
	}
	public void setAlSetStatus(ArrayList alSetStatus) {
		this.alSetStatus = alSetStatus;
	}
	public ArrayList getAlSetActualStatus() {
		return alSetActualStatus;
	}
	public void setAlSetActualStatus(ArrayList alSetActualStatus) {
		this.alSetActualStatus = alSetActualStatus;
	}
	public ArrayList getAlDates() {
		return alDates;
	}
	public void setAlDates(ArrayList alDates) {
		this.alDates = alDates;
	}
	
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public ArrayList getAlShipCarr() {
		return alShipCarr;
	}
	public void setAlShipCarr(ArrayList alShipCarr) {
		this.alShipCarr = alShipCarr;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
}
