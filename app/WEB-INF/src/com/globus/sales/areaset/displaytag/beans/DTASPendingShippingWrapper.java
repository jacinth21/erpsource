package com.globus.sales.areaset.displaytag.beans;

import java.math.BigDecimal;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import org.apache.commons.beanutils.DynaBean;
import java.util.HashMap;
import java.util.Date;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTASPendingShippingWrapper extends TableDecorator {
	private HashMap db;
	private String strTAGID = "";
	private String strSOURCE = "";
	private String strLOCATIONID="";
	private String strStatusFl="";
	private String strAcceptStatus="";
	GmLogger log = GmLogger.getInstance();
    public StringBuffer strValue = new StringBuffer();
    public StringBuffer strPostValue = new StringBuffer();
	private int i = 0;
	
	

	public DTASPendingShippingWrapper() {
		super();
	}

	public String getTAGID() {
		db = (HashMap) this.getCurrentRowObject();
		strTAGID = GmCommonClass.parseNull((String) db.get("TAGID"));
		strSOURCE = GmCommonClass.parseNull((String) db.get("STROPT"));
		strStatusFl= GmCommonClass.parseNull((String) db.get("STATUSFLAG"));
		strAcceptStatus= GmCommonClass.parseNull((String) db.get("ACCEPTSTATUS"));
		strValue.setLength(0);
        if(!((strSOURCE.equals("11401")||strSOURCE.equals("11403")) &&(strStatusFl.equals("30")||(strStatusFl.equals("40") && strAcceptStatus.equals("11460")))) && !((strSOURCE.equals("11400")||strSOURCE.equals("11402")) &&strStatusFl.equals("40")))
        {
		strValue.append("	  <input type=\"checkbox\" value=\"" + strTAGID + "\"  name=\"Chk_tagid" + i + "\" >");
		
        }
		strValue.append("	  <img src='/images/jpg_icon.jpg' alt='Tag' width='16' height='16' id='imgEdit' style='cursor:hand' title='View Image' onclick=\"return popitup('"+strTAGID+"')\" />");
		strValue.append("  <a href='#none'  style='color: #000; text-decoration: none; cursor:default'>" + strTAGID +"</a>");
		
		return strValue.toString();
	}
	
	
	public String getSOURCE() {
		db = (HashMap) this.getCurrentRowObject();
		
		strSOURCE = GmCommonClass.parseNull((String) db.get("STROPT"));
		strPostValue.setLength(0);
		strPostValue.append(strSOURCE+"~");
		strPostValue.append(GmCommonClass.parseNull((String) db.get("SHIPID"))+"~");
		strPostValue.append(GmCommonClass.parseNull((String) db.get("REFID")));
	    strValue.setLength(0);
	    strValue.append("	  <input type=\"hidden\" value=\"" + GmCommonClass.parseNull((String) db.get("SHIPID")) + "\"  name=\"Hdn_shipid" + i + "\" >");
		strValue.append("	  <input type=\"hidden\" value=\"" + GmCommonClass.parseNull((String) db.get("REFID")) + "\"  name=\"Hdn_refid" + i + "\" >");
		strValue.append("	  <input type=\"hidden\" value=\"" + GmCommonClass.parseNull((String) db.get("CASEID")) + "\"  name=\"Hdn_caseid" + i + "\" >");
		strValue.append("	  <input type=\"hidden\" value=\"" + GmCommonClass.parseNull((String) db.get("SHIPFROMID")) + "\"  name=\"Hdn_shipfromid" + i + "\" >");
		strValue.append("	  <input type=\"button\" value=Info  onClick=\"fnMoreInfo('"+strPostValue+"');\" name=\"Btn_tagid" + i + "\" >");
		
		i++;
		
		return strValue.toString();
	}

}