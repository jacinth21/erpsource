package com.globus.sales.areaset.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.areaset.beans.GmAreaSetBean;
import com.globus.sales.areaset.forms.GmAreaSetForm;



public class GmAreaSetShippingDispatchAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * load - This method loads areaset info
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   * @throws ServletException
   */
  public ActionForward load(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    instantiate(request, response);
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strOpt = "";

    GmAreaSetBean gmAreaSetBean = new GmAreaSetBean(getGmDataStoreVO());
    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);


    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strAccessCondition = getAccessFilter(request, response);

    strOpt = gmAreaSetForm.getStrOpt();

    gmAreaSetForm.setRepID(getRepID(request, response));
    gmAreaSetForm.setApplnDateFmt((String) request.getSession().getAttribute("strSessApplDateFmt"));


    HashMap hmParams = GmCommonClass.getHashMapFromForm(gmAreaSetForm);
    hmParams.put("AccessFilter", strAccessCondition);
    hmReturn = gmAreaSetBean.loadAreaSet(strOpt);
    gmAreaSetForm.setAlShipCarr((ArrayList) hmReturn.get("CARRIER"));
    gmAreaSetForm.setAlSetStatus((ArrayList) hmReturn.get("STATUS"));
    gmAreaSetForm.setAlSetActualStatus((ArrayList) hmReturn.get("ACTUALSTATUS"));
    gmAreaSetForm.setAlDates((ArrayList) hmReturn.get("DATE"));
    gmAreaSetForm.setType((String) hmReturn.get("HEADER"));
    gmAreaSetForm.setStrOpt(strOpt);


    alResult = gmAreaSetBean.fetchAreaSetPendShip(hmParams);
    gmAreaSetForm.setAlResult(alResult);

    return actionMapping.findForward("load");
  }



  /**
   * HandDeliver - This method update the accept and handdeliver status
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   * @throws ServletException
   */

  public ActionForward status(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, ServletException, IOException {

    instantiate(request, response);
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    String strOpt = "", popup = "", emailSts = "";

    GmAreaSetBean gmAreaSetBean = new GmAreaSetBean(getGmDataStoreVO());
    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);

    strOpt = gmAreaSetForm.getStrOpt();
    popup = gmAreaSetForm.getPopup();
    emailSts = GmCommonClass.parseNull(gmAreaSetForm.getEmail());
    gmAreaSetForm.setRepID(getRepID(request, response));
    gmAreaSetForm.setApplnDateFmt((String) request.getSession().getAttribute("strSessApplDateFmt"));


    HashMap hmParams = GmCommonClass.getHashMapFromForm(gmAreaSetForm);
    hmReturn = gmAreaSetBean.saveAreaSetStatus(hmParams);
    log.debug("emailSts" + emailSts);
    if (emailSts.equals("true")) {
      alResult = (ArrayList) hmReturn.get("EMAILDATA");
      hmParam.put("REPORTNAME", "/GmAreaSetShipToEmail.jasper");
      hmParam.put("TEMPLATENAME", "GmAreaSetShipTo");
      gmAreaSetBean.mailInfo(alResult, hmParam);
    }
    load(mapping, form, request, response);
    gmAreaSetForm.setPopup(popup);
    return mapping.findForward("status");
  }

  /**
   * shipTo - This method retrieves shipto info
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward shipTo(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError {
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();



    String strOpt = "";

    GmAreaSetBean gmAreaSetBean = new GmAreaSetBean();
    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);

    strOpt = gmAreaSetForm.getStrOpt();
    return mapping.findForward("shipTo");
  }

  /**
   * Reject - This method retrieves reject info
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward reject(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError {
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();



    String strOpt = "", caseID = "", hTxnVal = "";

    GmAreaSetBean gmAreaSetBean = new GmAreaSetBean();
    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);

    strOpt = gmAreaSetForm.getStrOpt();
    caseID = gmAreaSetForm.getCaseID();
    hTxnVal = gmAreaSetForm.gethTxnVal();
    HashMap hmParams = GmCommonClass.getHashMapFromForm(gmAreaSetForm);
    hmReturn = gmAreaSetBean.loadReject(hmParams);

    gmAreaSetForm.setAlSetStatus((ArrayList) hmReturn.get("REASON"));
    gmAreaSetForm.setStrOpt(strOpt);
    gmAreaSetForm.setAlResult((ArrayList) hmReturn.get("TAGIDS"));
    gmAreaSetForm.setCaseID(caseID);
    gmAreaSetForm.sethTxnVal(hTxnVal);
    log.debug("reject success");
    return mapping.findForward("reject");
  }

  /**
   * Reject - This method save reject info
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward saveReject(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    ArrayList alResult = new ArrayList();
    String strOpt = "", popup = "";
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    GmAreaSetBean gmAreaSetBean = new GmAreaSetBean();
    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);

    strOpt = gmAreaSetForm.getStrOpt();
    popup = gmAreaSetForm.getPopup();
    gmAreaSetForm.setRepID(getRepID(request, response));
    HashMap hmParams = GmCommonClass.getHashMapFromForm(gmAreaSetForm);
    hmReturn = gmAreaSetBean.saveAreaSetReject(hmParams);
    alResult = (ArrayList) hmReturn.get("EMAILDATA");
    hmParam.put("REPORTNAME", "/GmAreaSetRejectEmail.jasper");
    hmParam.put("TEMPLATENAME", "GmAreaSetRejection");
    gmAreaSetBean.mailInfo(alResult, hmParam);
    gmAreaSetForm.setPopup(popup);
    return mapping.findForward("status");
  }

  /**
   * Edit Address - This method modify address of rep
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward EditAddress(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);
    HashMap hmParams = GmCommonClass.getHashMapFromForm(gmAreaSetForm);

    gmAreaSetForm.setParentFrmName((String) hmParams.get("PARENTFRMNAME"));
    return mapping.findForward("EditAddress");
  }

  /**
   * Save Address - This method save address of rep
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward SaveAddress(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);
    GmAreaSetBean gmAreaSetBean = new GmAreaSetBean();
    HashMap hmReturn = new HashMap();
    hmReturn = GmCommonClass.getHashMapFromForm(gmAreaSetForm);
    gmAreaSetBean.SaveShipInfo(hmReturn);
    gmAreaSetForm.setStrOpt("");

    String strParentForm = (String) hmReturn.get("PARENTFRMNAME");

    if (strParentForm.equals("frmCasePost")) {
      gmAreaSetForm.setHaction("reload");
      return mapping.findForward("EditAddress");
    } else {
      return mapping.findForward("moreInfo");
    }
  }

  /**
   * more info - This method more info popup
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward moreInfo(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError {
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strOpt = "";
    String strRepID = "";
    String strAddID = "";
    String strShipType = "";
    GmAreaSetBean gmAreaSetBean = new GmAreaSetBean();
    GmAreaSetForm gmAreaSetForm = (GmAreaSetForm) form;
    gmAreaSetForm.loadSessionParameters(request);

    strOpt = gmAreaSetForm.getStrOpt();

    gmAreaSetForm.setRepID(getRepID(request, response));
    gmAreaSetForm.setApplnDateFmt((String) request.getSession().getAttribute("strSessApplDateFmt"));


    HashMap hmParams = GmCommonClass.getHashMapFromForm(gmAreaSetForm);



    alResult = gmAreaSetBean.fetchAreaSetPendShip(hmParams);
    if (alResult.size() > 0) {

      hmReturn = (HashMap) alResult.get(0);
      gmAreaSetForm.setCaseID(GmCommonClass.parseNull((String) hmReturn.get("CASEID")));
      gmAreaSetForm.setDelMode(GmCommonClass.parseNull((String) hmReturn.get("DELMODE")));
      gmAreaSetForm.setDelCarrier(GmCommonClass.parseNull((String) hmReturn.get("DELCARRIER")));
      gmAreaSetForm.settNo(GmCommonClass.parseNull((String) hmReturn.get("TNO")));
      gmAreaSetForm.setAccount(GmCommonClass.parseNull((String) hmReturn.get("ACCOUNT")));
      gmAreaSetForm.setTagID(GmCommonClass.parseNull((String) hmReturn.get("TAGID")));
      gmAreaSetForm.setConsignTo(GmCommonClass.parseNull((String) hmReturn.get("CONSIGNTO")));
      gmAreaSetForm.setSurgeryDate(GmCommonClass.parseNull((String) hmReturn.get("SURGERYDATE")));


      if (strOpt.equals("11400") || strOpt.equals("11402"))// pending shipping or pending return
      {
        gmAreaSetForm.setAddress(GmCommonClass.parseNull((String) hmReturn.get("RETURNADD")));
        strShipType = GmCommonClass.parseNull((String) hmReturn.get("SHIPFROM"));
        strRepID = GmCommonClass.parseNull((String) hmReturn.get("SHIPFROMID"));
        strAddID = GmCommonClass.parseNull((String) hmReturn.get("RETURNADDID"));
      } else if (strOpt.equals("11401") || strOpt.equals("11403"))// receive shipping or receive
                                                                  // return
      {
        gmAreaSetForm.setAddress(GmCommonClass.parseNull((String) hmReturn.get("SHIPADD")));
        strShipType = GmCommonClass.parseNull((String) hmReturn.get("SHIPTO"));
        strRepID = GmCommonClass.parseNull((String) hmReturn.get("SHIPTOID"));
        strAddID = GmCommonClass.parseNull((String) hmReturn.get("ADDID"));
      }
      gmAreaSetForm.setShipType(strShipType);
      gmAreaSetForm.setRepID(strRepID);
      gmAreaSetForm.setAddID(strAddID);
      gmAreaSetForm.setRefID(GmCommonClass.parseNull((String) hmReturn.get("REFID")));
      gmAreaSetForm.setShipID(GmCommonClass.parseNull((String) hmReturn.get("SHIPID")));
      gmAreaSetForm.setStatusfl(GmCommonClass.parseNull((String) hmReturn.get("STATUSFLAG")));

    }
    gmAreaSetForm.setStrOpt(strOpt);

    return actionMapping.findForward("moreInfo");
  }

  public String getRepID(HttpServletRequest req, HttpServletResponse res) {
    // To get the Session Information
    HttpSession session = req.getSession(false);


    // To get the Employee Information
    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));


    return strEmpId;

  }
}
