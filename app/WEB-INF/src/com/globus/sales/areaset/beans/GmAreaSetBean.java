package com.globus.sales.areaset.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmAreaSetBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmAreaSetBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAreaSetBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public HashMap loadAreaSet(String StrOpt) throws AppError {
    HashMap hmReturn = new HashMap();

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.getCodeList("DCAR", getGmDataStoreVO());
    hmReturn.put("CARRIER", alReturn);
    alReturn = GmCommonClass.getCodeList("CMSARS");
    hmReturn.put("STATUS", alReturn);
    alReturn = GmCommonClass.getCodeList("CMSARA");
    hmReturn.put("ACTUALSTATUS", alReturn);
    alReturn = GmCommonClass.getCodeList("CMSARD");
    hmReturn.put("DATE", alReturn);
    hmReturn.put("HEADER", GmCommonClass.getCodeNameFromCodeId(StrOpt));

    return hmReturn;
  }


  public ArrayList fetchAreaSetPendShip(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    ArrayList alReturn = new ArrayList();
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strAcceptStatus = GmCommonClass.parseNull((String) hmParam.get("ACTUALSTATUS"));
    String strTagFrom = GmCommonClass.parseZero((String) hmParam.get("TAGFROM"));
    String strTagTo = GmCommonClass.parseZero((String) hmParam.get("TAGTO"));
    String strDates = GmCommonClass.parseNull((String) hmParam.get("DATES"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("TXTFROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TXTTODATE"));
    String strCarrier = GmCommonClass.parseNull((String) hmParam.get("CARRIER"));
    String strShipID = GmCommonClass.parseNull((String) hmParam.get("SHIPID"));
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    strStatus = strStatus.equals("0") ? "" : strStatus;
    strAcceptStatus = strAcceptStatus.equals("0") ? "" : strAcceptStatus;
    strDates = strDates.equals("0") ? "" : strDates;
    strCarrier = strCarrier.equals("0") ? "" : strCarrier;

    if (strStatus.equals("11450")) {
      strStatus = "30";
    } else if (strStatus.equals("11451")) {
      strStatus = "40";
    }


    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append("select t7104.c5010_tag_id tagid,t207.c207_set_nm setdesc,decode(t907.c907_status_fl,30,get_code_name(11450),40,get_code_name(11451),t907.c907_status_fl) status,t907.c907_status_fl statusflag,"
            + " to_char(t7104.c7104_tag_lock_from_date,'"
            + strApplDateFmt
            + "') startdate,to_char(t7104.c7104_tag_lock_to_date,'"
            + strApplDateFmt
            + "') enddate,"
            + "t907.c901_ship_to, "
            + " get_rep_name (decode('"
            + strOpt
            + "','11401',t907.c907_ship_from_id,'11403',t907.c907_ship_from_id,t907.c907_ship_to_id)) shipowner, "
            + "t7100.c7100_case_id caseid,get_code_name(t907.c901_delivery_mode) delmode,get_code_name(t907.c901_delivery_carrier) delcarrier,t907.c907_tracking_number tno,"
            + "GET_ACCOUNT_NAME(t7100.c704_account_id) account,(select get_distributor_name(c5010_location_id)  from t5010_tag where c5010_tag_id=t7104.c5010_tag_id AND c5010_void_fl IS NULL  AND "
            + "c5010_location_id NOT IN ('52099', '52098')   AND c901_status = 51012) consignto,t907.c901_ship_from,to_char(t7100.c7100_surgery_date,'"
            + strApplDateFmt
            + "') surgerydate,"
            + "t907.c907_shipping_id, t907.c901_source source,"
            + "t907.c901_accept_status acceptstatus,t7104.c7104_case_set_id,t907.c907_ref_id refid,t907.c907_ship_from_id shipfromid,t907.c907_ship_to_id shiptoid,t7104.c207_set_id, '"
            + strOpt
            + "' stropt, "
            + "t907.c907_shipping_id shipid,t907.c106_address_id addid ,(select c106_address_id from  t907_shipping_info where c907_ref_id=t907.c907_ref_id and c907_ship_to_id=t907.c907_ship_from_id and c907_void_fl is null) returnaddid, "
            + "  ( SELECT name || '<br>' || address1 || '<br>' ||DECODE (address2, '', '', address2||'<br>') || DECODE (NVL ( GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', zipcode, city) || ' ' || "
            + "get_code_name_alt ( state) || ' ' || DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', city, zipcode) || '<br>'||'Ph:-'||phonenumber|| '<br>'||'Email:-'||emailid  "
            + "FROM v5010_curr_address  WHERE tagid = t5010.C5010_TAG_ID ) curr_location,"
            + " ( SELECT name || '<br>' || address1 || '<br>' ||DECODE (address2, '', '', address2||'<br>') || "
            + "DECODE (NVL ( GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', zipcode, city) || ' ' || get_code_name_alt (state) || ' ' || DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', city, zipcode)"
            + "|| '<br>'||'Ph:-'||phonenumber|| '<br>'||'Email:-'||emailid from v7100_ship_address where shiptoid=t907.c907_ship_to_id and refid=t907.c907_ref_id AND t907.c901_source in(11385,11388) and rownum = 1)shipadd,"
            + " ( SELECT decode(shiplcntoid, 4122, get_rep_name(t907.c907_ship_from_id),'')||'<br>' ||name || '<br>' || address1 || '<br>' ||"
            + "DECODE (address2, '', '', address2||'<br>') || DECODE (NVL ( GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', zipcode, city) || ' ' || get_code_name_alt ( state) || ' ' || "
            + "DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', city, zipcode) || '<br>'||'Ph:-'||phonenumber|| '<br>'||'Email:-'||emailid "
            + "from v7100_ship_address where refid=t907.c907_ref_id AND source = 11388) returnadd , t907.c901_ship_from shipfrom,t907.c901_ship_to shipto "
            + " from t907_shipping_info t907 ,t7104_case_set_information t7104,t5010_tag t5010,t7100_case_information t7100,t207_set_master t207 "
            + getAccessFilterClauseWithRepID()
            + "where  t7104.c7104_case_set_id=t907.c907_ref_id "
            + "and t7104.c7104_void_fl is null  and t5010.c5010_tag_id=t7104.c5010_tag_id  and "
            + "t7100.c7100_case_info_id=t7104.c7100_case_info_id and t7104.c207_set_id = t207.c207_set_id and t207.c207_void_fl is null  and t907.c907_void_fl is null and t907.c907_ship_from_id <> t907.c907_ship_to_id ");
    if (strOpt.equals("11400")) // Pending Shipping
    {
      sbQuery.append(" and t907.c907_ship_from_id = V700.REP_ID ");
      sbQuery.append(" and t907.c907_status_fl IN('" + (strStatus.length() > 0 ? strStatus : "30")
          + "') ");
      sbQuery
          .append(" and (t907.c901_accept_status"
              + (strAcceptStatus.length() > 0 ? ("=" + strAcceptStatus + ")")
                  : " is null or t907.c901_accept_status='11462') ")
              + " AND t907.c901_source = '11385' AND (t7100.c901_case_status = 11091 OR (t7100.c901_case_status IN (11092,11093) AND t7104.c7104_shipped_del_fl ='Y')) AND t7100.c901_type = 1006503 ");
    } else if (strOpt.equals("11401")) // Receive Shipping
    {
      sbQuery
          .append(" and  decode(t907.c901_ship_to, 4122, t907.c703_sales_rep_id, 4120, t907.c703_sales_rep_id, t907.c907_ship_to_id)   = V700.REP_ID "
              + " AND t907.c907_status_fl   IN("
              + (strStatus.length() > 0 ? strStatus : "30 ,40")
              + ")"
              + " AND "
              + (strAcceptStatus.length() > 0 ? ("t907.c901_accept_status=" + strAcceptStatus)
                  : "(t907.c901_accept_status IS NULL or t907.c901_accept_status='11462')")
              + "AND t907.c901_source = '11385' AND (t7100.c901_case_status = 11091 OR (t7100.c901_case_status IN (11092,11093) AND t7104.c7104_shipped_del_fl ='Y')) AND t7100.c901_type = 1006503 ");

    } else if (strOpt.equals("11402")) // Pending Return
    {
      sbQuery
          .append(" and decode(t907.c901_ship_from, 4122, t7100.c703_sales_rep_id, 4120, t7100.c703_sales_rep_id, t907.c907_ship_from_id) = V700.REP_ID "
              + " and t907.c907_status_fl IN("
              + (strStatus.length() > 0 ? strStatus : "30")
              + ") and (t907.c901_accept_status"
              + (strAcceptStatus.length() > 0 ? ("=" + strAcceptStatus + ")")
                  : " is null  or t907.c901_accept_status='11462') ")
              + " AND t907.c901_source = '11388' AND (t7100.c901_case_status = 11091 OR (t7100.c901_case_status IN (11092,11093) AND t7104.c7104_shipped_del_fl ='Y')) AND t7100.c901_type = 1006503 ");
    } else if (strOpt.equals("11403")) // Receive Return
    {
      sbQuery
          .append(" and  decode(t907.c901_ship_to, 4122, t907.c703_sales_rep_id, 4120, t907.c703_sales_rep_id, t907.c907_ship_to_id)   = V700.REP_ID "
              + " AND t907.c907_status_fl   IN("
              + (strStatus.length() > 0 ? strStatus : "30 ,40")
              + ")"
              + " AND "
              + (strAcceptStatus.length() > 0 ? ("t907.c901_accept_status=" + strAcceptStatus)
                  : "(t907.c901_accept_status IS NULL or t907.c901_accept_status='11462')")
              + " AND t907.c901_source = '11388' AND (t7100.c901_case_status = 11091 OR (t7100.c901_case_status IN (11092,11093) AND t7104.c7104_shipped_del_fl ='Y')) AND t7100.c901_type = 1006503 ");

    }

    if (!strTagFrom.equals("0") && !strTagTo.equals("0")) {
      sbQuery.append(" and t7104.c5010_tag_id between " + strTagFrom + " and " + strTagTo);
    }

    if (strCarrier.length() > 0) {
      sbQuery.append(" and t907.c901_delivery_carrier=" + strCarrier);
    }

    if (strDates.length() > 0) {
      if (strDates.equals("11470")) {
        sbQuery.append(" and t7100.c7100_surgery_date ");
      } else if (strDates.equals("11471")) {
        sbQuery.append(" and t7104.c7104_tag_lock_from_date ");
      } else if (strDates.equals("11472")) {

        sbQuery.append(" and t7104.c7104_tag_lock_to_date ");
      }
      sbQuery.append(" between to_date('" + strFromDate + "','" + strApplDateFmt
          + "') and to_date('" + strToDate + "','" + strApplDateFmt + "')");
    }

    if (strShipID.length() > 0 && strRefID.length() > 0) {
      sbQuery.append(" and t907.c907_shipping_id=" + strShipID + " and t907.c907_ref_id='"
          + strRefID + "' ");
    }
    sbQuery.append(" ORDER BY decode('" + strOpt
        + "','11400',t907.c907_ship_from_id,'11402',t907.c907_ship_from_id,t907.c907_ship_to_id)");
    log.debug("Query for BO report : " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());



    return alReturn;
  } // End of fetchAreaSetPendShip


  public HashMap saveAreaSetStatus(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();

    ArrayList alReturn = new ArrayList();

    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strInput = GmCommonClass.parseNull((String) hmParam.get("HTXNVAL"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strShipCarrier = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strTrackingNo = GmCommonClass.parseNull((String) hmParam.get("TRACKINGNO"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_areaset_txn.gm_sav_areaset_shipinfo", 7);
    gmDBManager.setString(1, strOpt);
    gmDBManager.setString(2, strInput);
    gmDBManager.setString(3, strUserID);
    gmDBManager.setString(4, strShipCarrier);
    gmDBManager.setString(5, strShipMode);
    gmDBManager.setString(6, strTrackingNo);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.execute();
    // 5031-N/A(only for shipout), 5030-Handdeliver
    if (strShipMode.equals("5031")) {
      alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    }
    gmDBManager.commit();
    log.debug("success");
    gmDBManager.close();
    hmReturn.put("EMAILDATA", alReturn);

    return hmReturn;

  }


  public void SaveShipInfo(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager();

    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strNewRepId = GmCommonClass.parseNull((String) hmParam.get("NEWREPID"));
    String strAddressID = GmCommonClass.parseNull((String) hmParam.get("ADDID"));
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strNewAddressID = GmCommonClass.parseNull((String) hmParam.get("NEWADDID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strShipType = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
    String strNewShipType = GmCommonClass.parseNull((String) hmParam.get("NEWSHIPTYPE"));
    String strAllSets = GmCommonClass.parseNull((String) hmParam.get("CHKAPPFL"));
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));

    gmDBManager.setPrepareString("gm_pkg_sm_areaset_txn.gm_sav_set_shipping", 11);

    gmDBManager.setString(1, strRefID);
    gmDBManager.setString(2, strRepId);
    gmDBManager.setString(3, strAddressID);
    gmDBManager.setString(4, strNewAddressID);
    gmDBManager.setString(5, strOpt);
    gmDBManager.setString(6, strUserID);
    gmDBManager.setString(7, strNewRepId);
    gmDBManager.setString(8, strShipType);
    gmDBManager.setString(9, strNewShipType);
    gmDBManager.setString(10, strShipMode);
    gmDBManager.setString(11, strAllSets);
    gmDBManager.execute();

    gmDBManager.commit();

  }

  public HashMap loadReject(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();

    ArrayList alReturn = new ArrayList();

    alReturn = GmCommonClass.getCodeList("CMSARR");
    hmReturn.put("REASON", alReturn);


    GmDBManager gmDBManager = new GmDBManager();

    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strInput = GmCommonClass.parseNull((String) hmParam.get("HTXNVAL"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_sm_areaset_info.gm_fch_areaset_rejectinfo", 4);

    gmDBManager.setString(1, strOpt);
    gmDBManager.setString(2, strInput);
    gmDBManager.setString(3, strUserID);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    hmReturn.put("TAGIDS", alReturn);

    return hmReturn;
  }


  public HashMap saveAreaSetReject(HashMap hmParam) throws AppError {

    HashMap hmReturn = new HashMap();

    ArrayList alReturn = new ArrayList();
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strInput = GmCommonClass.parseNull((String) hmParam.get("HTXNVAL"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strCaseID = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_areaset_txn.gm_sav_areaset_rejectinfo", 7);
    gmDBManager.setString(1, strOpt);
    gmDBManager.setString(2, strInput);
    gmDBManager.setString(3, strUserID);
    gmDBManager.setString(4, strStatus);
    gmDBManager.setString(5, strRepId);
    gmDBManager.setString(6, strCaseID);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    gmDBManager.commit();
    log.debug("success");
    gmDBManager.close();
    hmReturn.put("EMAILDATA", alReturn);
    return hmReturn;
  }

  public void mailInfo(ArrayList alResult, HashMap hmReportParam) throws AppError {

    HashMap hmParams = new HashMap();

    ArrayList alReturn = new ArrayList();
    HashMap hmResult = new HashMap();
    String strShipFromID = "", strShipToEmail = "", strShipFromEmail = "", strEMail = "", strRepName =
        "", StrOldShipFrom = "";

    if (alResult.size() > 0) {
      int size = alResult.size();
      for (int i = 1; i <= size; i++) {
        hmResult = (HashMap) alResult.get(i - 1);
        strShipFromID = (String) hmResult.get("SHIPFROMID");

        if (i > 1 && !StrOldShipFrom.equals(strShipFromID)) {
          strEMail = strEMail + "," + strShipFromEmail + "," + strShipToEmail;
          hmParams.put("REPNAME", strRepName);
          sendEmail(hmParams, alReturn, strEMail, hmReportParam);
          hmParams = new HashMap();
          alReturn = new ArrayList();
        }

        strShipFromID = (String) hmResult.get("SHIPFROMID");
        strShipToEmail = (String) hmResult.get("SHIPTOEMAIL");
        strShipFromEmail = (String) hmResult.get("SHIPFROMEMAIL");
        strEMail = (String) hmResult.get("EMAIL");
        strRepName = (String) hmResult.get("REPNAME");
        alReturn.add(hmResult);

        if (i == size) {
          strEMail = strEMail + "," + strShipFromEmail + "," + strShipToEmail;;
          hmParams.put("REPNAME", strRepName);
          sendEmail(hmParams, alReturn, strEMail, hmReportParam);
        }

        StrOldShipFrom = strShipFromID;
      }
    }
    log.debug("success");


  }

  public HashMap sendEmail(HashMap hmParams, ArrayList alReturn, String strEMail,
      HashMap hmReportParam) {
    HashMap hmReturn = new HashMap();
    GmEmailProperties emailProps = getEmailProperties((String) hmReportParam.get("TEMPLATENAME"));
    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName((String) hmReportParam.get("REPORTNAME"));
    emailProps.setRecipients(strEMail);
    jasperMail.setAdditionalParams(hmParams);
    jasperMail.setReportData(alReturn);
    jasperMail.setEmailProperties(emailProps);
    hmReturn = jasperMail.sendMail();
    return hmReturn;
  }

  public GmEmailProperties getEmailProperties(String strTemplate) {
    GmEmailProperties emailProps = new GmEmailProperties();
    emailProps
        .setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
    emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    emailProps.setCc(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.CC));
    return emailProps;
  }



}
