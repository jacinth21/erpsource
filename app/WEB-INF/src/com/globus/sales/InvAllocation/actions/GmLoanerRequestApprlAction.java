package com.globus.sales.InvAllocation.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmAutoCompletePartyListRptBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.requests.beans.GmLoanerReqEditBean;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.InvAllocation.beans.GmLoanerRequestApprovalBean;
import com.globus.sales.InvAllocation.beans.GmPendAllocationBean;
import com.globus.sales.InvAllocation.forms.GmLoanerRequestApprovalForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.sales.event.beans.GmEventReportBean;

public class GmLoanerRequestApprlAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    log.debug("GmLoanerRequestApproval");
    GmLoanerRequestApprovalForm gmLoanerRequestApprovalForm =
        (GmLoanerRequestApprovalForm) actionForm;
    gmLoanerRequestApprovalForm.loadSessionParameters(request);
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
    String strOpt = "";
    String strUserId = "";
    String strAccessFl = "";
    String strCondition = "";
    String strApplDateFmt = "";
    String strxmlGridData = "";
    String strApprvGrpNm = "LNREQ_APPVL";
    String strRejectGrpNm = "LNREQ_REJECT";
    String strStartDt = "";
    String strEndDt = "";
    int currMonth = 0;
    String strTempAction = "";
    String strDistCondition = "";
    String strRAID = "";
    String strDepartMentID = "";
    String strFilterCondition = null;
    String strVMTemplate = "";


    String strDispatch = "GmLoanerRequestApproval";
    ArrayList alResult = new ArrayList();
    ArrayList alEventName = new ArrayList();
    ArrayList alRequestTypes = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alDistributorList = new ArrayList();
    ArrayList alSystemList = new ArrayList();
    ArrayList alDSOwner = new ArrayList();


    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmFilter = new HashMap();
    HashMap hmExclusion = new HashMap();

    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    GmLoanerRequestApprovalBean gmLoanerRequestApprovalBean =
        new GmLoanerRequestApprovalBean(getGmDataStoreVO());
    GmLoanerReqEditBean gmLoanerReqEditBean = new GmLoanerReqEditBean(getGmDataStoreVO());
    GmAutoCompletePartyListRptBean gmAutoCompletePartyListRptBean =
        new GmAutoCompletePartyListRptBean(getGmDataStoreVO());

    HttpSession session = request.getSession(false);

    strDepartMentID = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
    String strSessTodayDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strAccess = GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getAppvlAccessFl());

    GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
    HashMap hmSalesFilters = new HashMap();
    strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
    request.setAttribute("hmSalesFilters", hmSalesFilters);

    strCondition = getAccessFilter(request, response);
    strUserId = gmLoanerRequestApprovalForm.getUserId();
    String strReqType = gmLoanerRequestApprovalForm.getRequestType();

    alDSOwner = GmCommonClass.parseNullArrayList(gmLogonBean.getEmployeeList("300", "W','P"));
    gmLoanerRequestApprovalForm.setAlDSOwner(alDSOwner);

    String strAction = GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getHaction());
    strOpt = gmLoanerRequestApprovalForm.getStrOpt();
    // The Below Code is added for PMT-5988 When Approve with Return with Return Quantity StrOpt
    // will be appwithra.
    if (!strOpt.equals("save") && !strOpt.equals("load") && !strOpt.equals("appwithra")
        && !strOpt.equals("Approvalview")) {
      // below condition works only for 'Pending PD Loaner Request' Screen
      if (strOpt.equals("PDREJLNREQAPPR")) {
        strTempAction = "PDREJLNREQAPPR";
        strApprvGrpNm = "PD_LOANREQ_APPRL";
        strRejectGrpNm = "PD_LOANREQ_REJECT";
        gmLoanerRequestApprovalForm.setStrOpt("4127");
      }
      // below condition works only for 'Pending Set Approval' Screen
      if (strOpt.equals("setreq")) {
        gmLoanerRequestApprovalForm.setStrOpt("400087");
      }
      String strTxnType = gmLoanerRequestApprovalForm.getStrOpt();
      gmLoanerRequestApprovalForm.setStrTxnType(strTxnType);

    }

    String strType = GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getStrTxnType());
    if (strType.equals("4119")) {
      strApprvGrpNm = "IHLNREQ_APPVL";
      strRejectGrpNm = "IHLNREQ_REJECT";
    }

    if (strType.equals("400088")) {

      strApprvGrpNm = "ITMREQ_APPVL";
      strRejectGrpNm = "ITMREQ_REJECT";
      // alRepList = GmCommonClass.parseNullArrayList(gmCustomerBean.getSalesRepList("Active"));
      // gmLoanerRequestApprovalForm.setAlSalesReps(alRepList);
    }
    // To retrieve the Hierarchy based Distributor List for CN SET/ITEM Report screen.
    if (strType.equals("400087") || strType.equals("400088")) {
      strDistCondition = getAccessFilter(request, response, false);
      hmFilter.put("STATUS", "Active");
      hmFilter.put("CONDITION", strDistCondition);
      hmExclusion.put("COMPANY", " AND v700.COMPID NOT IN (100801) "); // /Excluding the Algea
                                                                       // companies
      hmFilter.put("EXCLUSION_MAP", hmExclusion);
      hmFilter.put("FLTR_DIST_BY_COMP", "YES");// Filter Distributor Based on company
      alDistributorList =
          GmCommonClass.parseNullArrayList(gmAutoCompletePartyListRptBean
              .getSalesFilterDist(hmFilter));
      alSystemList = gmSales.loadProjectGroup();
      gmLoanerRequestApprovalForm.setAlFieldSales(alDistributorList);
      gmLoanerRequestApprovalForm.setAlSalesReps(GmCommonClass.parseNullArrayList(gmCommonBean
          .getSalesFilterRep(strCondition, "Active")));
      gmLoanerRequestApprovalForm.setAlSystemList(alSystemList);
    }
    // below condition works only for 'Pending PD Loaner Request' Screen when click on load
    if (strAction.equals("PDREJLNREQAPPR")) {
      strTempAction = strAction;
      strAction = "Approved";
      strApprvGrpNm = "PD_LOANREQ_APPRL";
      strRejectGrpNm = "PD_LOANREQ_REJECT";
    }

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            strApprvGrpNm));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmLoanerRequestApprovalForm.setAppvlAccessFl(strAccessFl);

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            strRejectGrpNm));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmLoanerRequestApprovalForm.setRejectAccessFl(strAccessFl);

    currMonth = gmCal.getCurrentMonth() - 1;

    alEventName = GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventNameList());
    gmLoanerRequestApprovalForm.setAlEventName(alEventName); // set the AuditName ArrayList into
                                                             // form property


    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerRequestApprovalForm);

    String strInputString = gmLoanerRequestApprovalForm.gethApproveInputString();
    hmParam =
        GmCommonClass.parseNullHashMap(setRequestStatus(hmParam, session,
            gmLoanerRequestApprovalForm));
    // The Below Code is Changed for PMT-5988 Where while Approving Item Request With Return
    // Quantity StrOpt will be appwithra.
    if (strOpt.equals("save") || strOpt.equals("appwithra")) {
      if (strType.equals("400088")) {
        // saving the item quantity which is approved by AD/PD.
        saveApprovalQuantity(gmLoanerRequestApprovalForm);
      }

      // comments is getting from form
      String strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

      hmReturn = GmCommonClass.parseNullHashMap(gmPendAllocationBean.saveLoanerReqAppv(hmParam));
      String strReqDetId = GmCommonClass.parseNull((String) hmReturn.get("PRODREQID"));
      String strPrReqDetLitePnum =
          GmCommonClass.parseNull((String) hmReturn.get("PRREQDETLITEPNUM"));

      if (!strPrReqDetLitePnum.equals("")) {
        gmLoanerRequestApprovalForm.setPrReqDetLitePnum(strPrReqDetLitePnum);
      }
      if (strType.equals("400088")) {
        gmLoanerRequestApprovalForm.setStrReqIds(GmCommonClass.parseNull((String) hmReturn
            .get("PRODREQDID")));
        // sending item request approval notification email.
        sendApprovalNotification(gmLoanerRequestApprovalForm,strCompanyInfo);
      }

      String strReqIds = GmCommonClass.parseNull((String) hmParam.get("STRREQIDS"));
      sendRequestNotification(strReqIds, strAction, "1006505", getGmDataStoreVO().getCmpid(),
          strComments); // 1006505
      // -
      // Product
      // Loaner
      sendRequestNotification(strReqIds, strAction, "1006506", getGmDataStoreVO().getCmpid(),
          strComments); // 1006506
      // -
      // Consignment
      // Set

      alResult =
          GmCommonClass.parseNullArrayList(gmCaseBookRptBean.fetchSetReqNotificationInfo(strReqIds,
              "1006506"));
      int alResultSize = alResult.size();
      if (alResultSize != 0) {
        strType = "400087";
      }
      // The Below Code is Changed for PMT-5988 Where while Approving Item Request With Return
      // Quantity StrOpt will be appwithra.
      // While Approving It needs to Send E-mail So below code is added.
      if (strOpt.equals("appwithra")) {
        HashMap hmRADetails = new HashMap();
        ArrayList alRAMailDecide = new ArrayList();
        String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
        GmProcessReturnsBean gmProcessReturnBean = new GmProcessReturnsBean(getGmDataStoreVO());
        // saveApprovalQuantity(gmLoanerRequestApprovalForm);
        String strPartString = GmCommonClass.parseNull((String) hmParam.get("PARTSTRING"));
        String strRAQtyString = GmCommonClass.parseNull((String) hmParam.get("RAQTYSTRING"));
        hmParam.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        strRAID = gmPendAllocationBean.saveApprReqWithRA(hmParam);

      }
      // 400087- ConsignMent Set, 400088 - ConsignMent Item
      if (!strReqDetId.equals("") && !strType.equals("400087") && !strType.equals("400088")) {
        // ActionForward actionForward = actionMapping.findForward("editsuccess");
        // ActionForward itemForward = new ActionForward(actionForward);
        // itemForward.setPath("/gmOprLoanerReqEdit.do?strOpt=load&requestId=" + strReqDetId
        // + "&txnType=" + strType);
        // itemForward.setRedirect(true);
        String strForward =
            "/gmOprLoanerReqEdit.do?strOpt=load&requestId=" + strReqDetId + "&txnType=" + strType;
        return actionRedirect(strForward, request);
      }
      gmLoanerRequestApprovalForm.setRaId(strRAID);
      gmLoanerRequestApprovalForm
          .setMessage("The request has been approved and e-mail has sent to respective person(s). ");
      String strdisplayscreen = gmLoanerRequestApprovalForm.getChkbox_display();
      if (strdisplayscreen.equals("true")) {
        strOpt = "Approvalview";
      } else {
        strOpt = "load";
      }

    }

    // Append the Sales filter only when Sales Team is approving request.
    // For In-house Employee, no filter condition required.
    if (strDepartMentID.equals("S"))
      hmParam.put("ACCESSFILTER", strCondition);

    if (strOpt.equals("load")) {

      String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
      String strTxnType = GmCommonClass.parseNull((String) hmParam.get("STRTXNTYPE"));
      gmLoanerReqEditBean.validateProdRequestID(strRequestId, strTxnType);
      hmParam.put("APPROVE", "IHLNREQ_APPVL");
      hmParam.put("REJECT", "IHLNREQ_REJECT");
      hmParam.put("PDREJLNREQAPPR", strTempAction);
      hmParam.put("Filter", strFilterCondition);
      alResult =
          GmCommonClass.parseNullArrayList(gmPendAllocationBean
              .fetchPendingApprlLoanerReqDtl(hmParam));
      gmLoanerRequestApprovalForm.setAlResult(alResult);

      hmReturn.put("REQTYPEID", strType);
      hmReturn.put("PENDLNRDATA", alResult);
      hmReturn.put("SESSDATEFMT", strApplDateFmt);
      hmReturn.put("SESSTODAYDATE", strSessTodayDate);
      hmReturn.put("STRACCESSFL",
          GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getAppvlAccessFl()));

      if (strType.equals("4119")) {// In-House Loaner
        hmReturn.put("VMFILEPATH", "properties.labels.sales.GmInHouseLoanerReqApproval");
        strxmlGridData = generateOutPut(hmReturn, "GmInHouseLoanerReqApproval.vm");
      } else if (strType.equals("4127") || strType.equals("400087")) {// 4127:Product Loaner,
                                                                      // 400087:Set Consignment
        hmReturn.put("VMFILEPATH",
            "properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest");
        strVMTemplate =
            strType.equals("400087") ? "GmConsignSetReqApproval.vm" : "GmLoanerReqApproval.vm";

        strxmlGridData = generateOutPut(hmReturn, strVMTemplate);
      } else if (strType.equals("400088")) {// Item Consignment
        hmReturn.put("VMFILEPATH",
            "properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest");
        hmReturn
            .put("USERTYPE", GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getUserType()));
        strxmlGridData = generateOutPut(hmReturn, "GmLoanerItemReqApproval.vm");
      }
    }

    /*
     * The Below code is added as part of PMT-6432.When PD Wants to Approve the Request with
     * comments The below mentioned code will open a dhtmlx window ,where we can provide comments
     * and Approve the RA.
     */
    if (strOpt.equals("Approvalview")) {
      String strmessage = GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getMessage());
      if (strmessage.equals("")) {
        String strRequestorname =
            GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getRequestor_name());
        String strSubject = "Take an action for Item request ";
        if (!strRequestorname.equals("")) {
          strSubject += "from " + strRequestorname;
        }
        gmLoanerRequestApprovalForm.setEmail_subject(strSubject);
      }
      strDispatch = "ApprovalView";
    }
    if (strTempAction.equals("PDREJLNREQAPPR")) {
      gmLoanerRequestApprovalForm.setStrOpt("PDREJLNREQAPPR");
    }
    gmLoanerRequestApprovalForm.setXmlGridData(strxmlGridData);

    return actionMapping.findForward(strDispatch);
  }

  private String generateOutPut(HashMap hmReturn, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmReturn.get("VMFILEPATH"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }

  /**
   * sendRequestNotification will get the required information that is used for sending out the
   * email, when the Loaner/Consignment Set Request is approved.
   * 
   * @param String strInputString
   * @return
   * @exception AppError
   */
  public void sendRequestNotification(String strReqIDs, String strAction, String strType,
      String strCompanyId, String strComments) throws AppError, Exception {
    log.debug("===========calling sendRequestNotification Method================");
    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    ArrayList alResult = new ArrayList();
    ArrayList alList = new ArrayList();
    ArrayList alCommonList = new ArrayList();
    GmEmailProperties emailProps = null;
    String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    String strJasperNm = "";
    int intArLength = 0;
    String strRepId = "";
    String strRepNm = "";
    String strReqId = "";
    String strSetId = "";
    String strSetNm = "";
    String strRepEmail = "";
    String strLoanReqId = "";
    String strMessage = "";
    String strAcctNm = "";
    String strShipDt = "";
    String strSurgDt = "";
    String strSubject = "";
    String strStatusFl = "";
    String strUserNm = "";
    String strTxnType = "";
    String strRejDt = "";
    String strFieldSales = "";
    String strShipSrc = "";
    String strDefaultEmailID = "";
    String strRequestorNm = "";
    if (strType.equals("1006505")) {
      strTxnType = "4127";
      strJasperNm = "/GmLoanerReqApprEmail.jasper";
      strShipSrc = "50182";
      // To fetch email id from rules. In this way, incase if anybody need explicit access we can
      // add them.
      strDefaultEmailID =
          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("LOANER_REQ_APPROVAL",
              "DEFAULT_CC", strCompanyId));
    } else if (strType.equals("1006506")) {
      strTxnType = "400087";
      strJasperNm = "/GmConsignmentReqApprEmail.jasper";
      strShipSrc = "50181";
      // To fetch email id from rules. In this way, incase if anybody need explicit access we can
      // add them.
      strDefaultEmailID =
          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SET_REQ_APPROVAL",
              "DEFAULT_CC", strCompanyId));
    }

    alResult =
        GmCommonClass.parseNullArrayList(gmCaseBookRptBean.fetchSetReqNotificationInfo(strReqIDs,
            strType));

    intArLength = alResult.size();

    if (intArLength > 0) {
      HashMap hmTempLoop = new HashMap();
      HashMap hmLoop = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmEmailData = new HashMap();
      String strPrevId = "";
      String strReqDetailId = "";
      int intCount = 0;
      HashMap hmShipParam = new HashMap();
      boolean blFlag = false;

      for (int i = 0; i < intArLength; i++) {
        hmShipParam = new HashMap();
        hmLoop = (HashMap) alResult.get(i);
        if (i == 0) {
          hmTempLoop = (HashMap) alResult.get(i);
          strPrevId = GmCommonClass.parseNull((String) hmTempLoop.get("REPID"));
        }
        strRepId = GmCommonClass.parseNull((String) hmLoop.get("REPID"));

        strRepNm = GmCommonClass.parseNull((String) hmLoop.get("REPNM"));
        strReqId = GmCommonClass.parseNull((String) hmLoop.get("REQID"));
        strSetId = GmCommonClass.parseNull((String) hmLoop.get("SETID"));
        strSetNm = GmCommonClass.parseNull((String) hmLoop.get("SETNM"));
        strLoanReqId = GmCommonClass.parseNull((String) hmLoop.get("LOANREQID"));
        strAcctNm = GmCommonClass.parseNull((String) hmLoop.get("ACCTNM"));
        strShipDt = GmCommonClass.parseNull((String) hmLoop.get("SHIPDT"));
        strSurgDt = GmCommonClass.parseNull((String) hmLoop.get("SURGDT"));
        strStatusFl = GmCommonClass.parseNull((String) hmLoop.get("STATUS"));
        strUserNm = GmCommonClass.parseNull((String) hmLoop.get("UNAME"));
        strRejDt = GmCommonClass.parseNull((String) hmLoop.get("REJDATE"));
        strFieldSales = GmCommonClass.parseNull((String) hmLoop.get("FIELDSALES"));
        strReqDetailId = GmCommonClass.parseNull((String) hmLoop.get("REQDETAILID"));
        strRequestorNm = GmCommonClass.parseNull((String) hmLoop.get("REQUESTORNM"));

        // Call the below bean method & get the shipping info.
        hmShipParam =
            GmCommonClass.parseNullHashMap(gmShippingInfoBean.fetchShipAddress(strReqDetailId,
                strShipSrc));

        if (strRepId.equals(strPrevId)) {
          hmParam = new HashMap();
          hmParam.put("REQID", strReqId);
          hmParam.put("LOANREQID", strLoanReqId);
          hmParam.put("SETID", strSetId);
          hmParam.put("SETNM", strSetNm);
          hmParam.put("ACCTNM", strAcctNm);
          hmParam.put("SURGDT", strSurgDt);
          hmParam.put("SHIPDT", strShipDt);
          hmParam.put("STATUS", strStatusFl);
          hmParam.put("FIELDSALES", strFieldSales);
          hmParam.put("TXNTYPE", strTxnType);
          hmParam.put("REQUESTORNM", strRequestorNm);
          hmParam.putAll(hmShipParam);
          alList.add(hmParam);
          intCount++;

        } else {
          // start
          emailProps = new GmEmailProperties();
          String TEMPLATE_NAME = "GmInvMgm" + strType + "Email";

          emailProps.setSender(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.FROM));
          emailProps.setRecipients(strRepEmail);
          emailProps.setCc(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.CC));
          emailProps.setCc(strDefaultEmailID);
          emailProps.setMimeType(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.MIME_TYPE));
          strSubject =
              gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
          strSubject = GmCommonClass.replaceAll(strSubject, "<#STATUS>", strAction);

          emailProps.setSubject(strSubject);
          strMessage =
              gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE);
          strMessage = GmCommonClass.replaceAll(strMessage, "<#REPNAME>", strUserNm);
          strMessage = GmCommonClass.replaceAll(strMessage, "<#APPRDATE>", strRejDt);
          strMessage = GmCommonClass.replaceAll(strMessage, "<#STATUS>", strAction);

          hmEmailData.put("MESSAGE", strMessage);
          hmEmailData.put("EMAILPROPS", emailProps);
          hmEmailData.put("COMMENTS", strComments);

          GmJasperMail jasperMail = new GmJasperMail();
          jasperMail.setJasperReportName(strJasperNm);
          jasperMail.setAdditionalParams(hmEmailData);
          jasperMail.setReportData(alList);
          jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));

          HashMap hmjasperReturn = jasperMail.sendMail();
          strSubject = "";// Subject was showing multiple times, so need to remove previous value
          blFlag = false;
          alList.clear();
          // end

          hmParam = new HashMap();
          hmParam.put("REQID", strReqId);
          hmParam.put("LOANREQID", strLoanReqId);
          hmParam.put("SETID", strSetId);
          hmParam.put("SETNM", strSetNm);
          hmParam.put("ACCTNM", strAcctNm);
          hmParam.put("SURGDT", strSurgDt);
          hmParam.put("SHIPDT", strShipDt);
          hmParam.put("STATUS", strStatusFl);
          hmParam.put("FIELDSALES", strFieldSales);
          hmParam.put("TXNTYPE", strTxnType);
          hmParam.put("REQUESTORNM", strRequestorNm);
          hmParam.putAll(hmShipParam);
          alList.add(hmParam);
          intCount = 1;
        }
        strRepEmail = GmCommonClass.parseNull((String) hmLoop.get("EMAILID"));
        strPrevId = strRepId;
      }
      if (intCount > 0) {
        emailProps = new GmEmailProperties();
        String TEMPLATE_NAME = "GmInvMgm" + strType + "Email";

        emailProps.setSender(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
            + GmEmailProperties.FROM));
        emailProps.setRecipients(strRepEmail);
        emailProps.setCc(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
            + GmEmailProperties.CC));
        emailProps.setCc(strDefaultEmailID);
        emailProps.setMimeType(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
            + GmEmailProperties.MIME_TYPE));
        strSubject =
            gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
        strSubject = GmCommonClass.replaceAll(strSubject, "<#STATUS>", strAction);

        emailProps.setSubject(strSubject);
        emailProps.setEmailHeaderName(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
                + GmEmailProperties.EMAIL_HEADER_NM));
        strMessage =
            gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE);
        strMessage = GmCommonClass.replaceAll(strMessage, "<#REPNAME>", strUserNm);
        strMessage = GmCommonClass.replaceAll(strMessage, "<#APPRDATE>", strRejDt);
        strMessage = GmCommonClass.replaceAll(strMessage, "<#STATUS>", strAction);

        hmEmailData.put("MESSAGE", strMessage);
        hmEmailData.put("EMAILPROPS", emailProps);
        hmEmailData.put("COMMENTS", strComments);

        GmJasperMail jasperMail = new GmJasperMail();
        jasperMail.setJasperReportName(strJasperNm);
        jasperMail.setAdditionalParams(hmEmailData);
        jasperMail.setReportData(alList);
        jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));

        HashMap hmjasperReturn = jasperMail.sendMail();
        strSubject = "";// Subject was showing multiple times, so need to remove previous value
        blFlag = false;
      }
    }
  }

  /**
   * setRequestStatus will set the required information that is used for process transaction.
   * 
   * @param HashMap,session,form
   * @return HashMap
   * @exception AppError
   */
  private HashMap setRequestStatus(HashMap hmParam, HttpSession session,
      GmLoanerRequestApprovalForm gmLoanerRequestApprovalForm) throws AppError {
    String strStatus = "'2','5'";
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));

    if (!gmLoanerRequestApprovalForm.getAppvlAccessFl().equals("")
        || !gmLoanerRequestApprovalForm.getRejectAccessFl().equals("")) {
      gmLoanerRequestApprovalForm.setUserType("PD");
      gmLoanerRequestApprovalForm.setAppvlAccessFl("Y");
      gmLoanerRequestApprovalForm.setRejectAccessFl("Y");
      strStatus = "'5'";
    }
    if (strDepartMentID.equals("S")) {
      gmLoanerRequestApprovalForm.setUserType("AD");
      gmLoanerRequestApprovalForm.setAppvlAccessFl("Y");
      gmLoanerRequestApprovalForm.setRejectAccessFl("Y");
      gmLoanerRequestApprovalForm.setNextApprStatus("5");
      strStatus = "'2'";
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerRequestApprovalForm);
    hmParam.put("REQ_STATUS", strStatus);
    return hmParam;
  }

  private void saveApprovalQuantity(GmLoanerRequestApprovalForm gmLoanerRequestApprovalForm)
      throws AppError {
    String strInputStr =
        GmCommonClass.parseNull(gmLoanerRequestApprovalForm.gethApproveInputString());
    String strUserId = GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getUserId());
    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean(getGmDataStoreVO());
    gmPendAllocationBean.saveRequestItemQty(strInputStr, strUserId);
  }

  private void sendApprovalNotification(GmLoanerRequestApprovalForm gmLoanerRequestApprovalForm , String strCompanyInfo)
      throws AppError {
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean(getGmDataStoreVO());
    GmLoanerRequestApprovalBean gmLoanerRequestApprovalBean =
        new GmLoanerRequestApprovalBean(getGmDataStoreVO());
    String strUserType = GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getUserType());
    String strReqIds = GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getStrReqIds());
    String strPrReqDetLitePnum =
        GmCommonClass.parseNull(gmLoanerRequestApprovalForm.getPrReqDetLitePnum());
    String strRequestId = "";
    HashMap hmdata = new HashMap();
    /*
     * The below mentioned code is changed for PMT-6432. The Approval Notification methods are moved
     * to Bean File
     */
    hmdata = GmCommonClass.getHashMapFromForm(gmLoanerRequestApprovalForm);
    if (strUserType.equals("AD")) {
      gmLoanerRequestApprovalBean.fetchADApprovalDtl(hmdata);
    }
    if (strUserType.equals("PD") || !strPrReqDetLitePnum.equals("")) {

      if (!strUserType.equals("PD")) {
        strReqIds = strPrReqDetLitePnum;
        // The Below Code is Added For PMT-6432 BUG-5574. When Literature Part is Approved by AD
        // mail needs to be sent.
        hmdata.put("STRREQIDS", strReqIds);
      }
      // The Below Code is changed for PMT-6432 In Approval Comments Page After Sending mail we need
      // to show the Request ID.
      strRequestId = gmCaseBookTxnBean.saveRequestInfo(strReqIds, strCompanyInfo);
      gmLoanerRequestApprovalForm.setRequest_id(strRequestId);
      gmLoanerRequestApprovalBean.fetchPDApprovalDtl(hmdata);

    }

  }
}
