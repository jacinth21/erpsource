package com.globus.sales.InvAllocation.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.InvAllocation.beans.GmPendAllocationBean;
import com.globus.sales.InvAllocation.forms.GmPendAllocationForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmPendAllocationAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  HashMap hmParam = new HashMap();
  HashMap hmSalesFilters = new HashMap();
  ArrayList alReturn = new ArrayList();

  public ActionForward loadPendingAllocationRpt(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

	instantiate(request, response); 
	GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean(getGmDataStoreVO());
    GmPendAllocationForm gmPendAllocationForm = (GmPendAllocationForm) form;
    gmPendAllocationForm.loadSessionParameters(request);

    String strOpt = "";
    RowSetDynaClass rdResult = null;
    hmParam = GmCommonClass.getHashMapFromForm(gmPendAllocationForm);

    HttpSession session = request.getSession(false);
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));

    String strAccessCondition = getAccessFilter(request, response);
    HashMap hmFilter = new HashMap();
    hmFilter.put("CONDITION",strAccessCondition);
    hmFilter.put("FLTR_DIST_BY_COMP","YES");//Filter Distributor Based on company
    hmSalesFilters = gmCommonBean.getSalesFilterLists(hmFilter);
    alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
    gmPendAllocationForm.setAlDist(alReturn);
    alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
    gmPendAllocationForm.setAlRepList(alReturn);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    if (strOpt.equals("reload")) {
      hmParam.put("AccessFilter", strAccessCondition);
      rdResult = gmPendAllocationBean.fetchPendAllocation(hmParam);
      gmPendAllocationForm.setReturnList(rdResult.getRows());
    }

    return mapping.findForward("gmPendingAllocation");
  }
}
