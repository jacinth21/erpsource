package com.globus.sales.InvAllocation.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.InvAllocation.beans.GmSetOverrideRptBean;
import com.globus.sales.InvAllocation.forms.GmSetOverrideRptForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmSetOverrideRptAction extends GmSalesDispatchAction {
  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());
    log.debug("GmSetOverrideRptAction");
    instantiate(request, response);
    GmSetOverrideRptForm gmSetOverrideRptForm = (GmSetOverrideRptForm) actionForm;
    gmSetOverrideRptForm.loadSessionParameters(request);
    String strCondition = "";
    String strOpt = "";
    String strInitLoanFl = "";
    String strForward = "GmSetOverrideRpt";

    Boolean blMapFlag = false;
    ArrayList alTemp = new ArrayList();
    ArrayList alResult = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSetOverrideRptBean gmSetOverrideRptBean = new GmSetOverrideRptBean(getGmDataStoreVO());



    strCondition = getAccessFilter(request, response);

    strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
    strOpt = strOpt.equals("") ? gmSetOverrideRptForm.getStrOpt() : strOpt;

    if (strOpt.equals("load")) {
      gmSetOverrideRptForm.setSetId(GmCommonClass.parseNull(request.getParameter("setId")));
      gmSetOverrideRptForm.setSetName(GmCommonClass.parseNull(request.getParameter("setName")));
      gmSetOverrideRptForm
          .setCaseDistId(GmCommonClass.parseNull(request.getParameter("caseDistId")));
      gmSetOverrideRptForm.setCaseSetId(GmCommonClass.parseNull(request.getParameter("caseSetId")));
      gmSetOverrideRptForm.setParentFrmName(GmCommonClass.parseNull(request
          .getParameter("parentFrmName")));
      gmSetOverrideRptForm
          .setCaseInfoId(GmCommonClass.parseNull(request.getParameter("caseInfoId")));
      gmSetOverrideRptForm
          .setLockFromDt(GmCommonClass.parseNull(request.getParameter("lockFromDt")));
      gmSetOverrideRptForm.setLockToDt(GmCommonClass.parseNull(request.getParameter("lockToDt")));
    }

    hmParam = GmCommonClass.getHashMapFromForm(gmSetOverrideRptForm);
    log.debug("hmParam =" + hmParam);
    

	HashMap hmFilter = new HashMap();
	
	
	hmFilter.put("CONDITION",strCondition);
	hmFilter.put("FLTR_DIST_BY_COMP","YES");//Filter Distributor Based on company
    
    alTemp = GmCommonClass.parseNullArrayList(gmCommonBean.getSalesFilterDist(hmFilter));
    gmSetOverrideRptForm.setAlDistributor(alTemp);

    if (strOpt.equals("save")) {
      gmSetOverrideRptBean.saveSetOverrideDtl(hmParam);
      gmSetOverrideRptForm.setStrOpt("reload");
    } else if (strOpt.equals("initLoaner")) {
      gmSetOverrideRptBean.saveLoanerTagRequest(hmParam);
      gmSetOverrideRptForm.setStrOpt("reload");
    } else if (strOpt.equals("load") || strOpt.equals("loadMap")) {
      hmParam.put("ACCESSFILTER", strCondition);
      alResult = GmCommonClass.parseNullArrayList(gmSetOverrideRptBean.fetchSetTagListDtl(hmParam));
      if (alResult.size() > 0) {
        int i = 0;
        do {
          HashMap hmTemp = GmCommonClass.parseNullHashMap((HashMap) alResult.get(i));
          String strShipEmpty = GmCommonClass.parseNull((String) hmTemp.get("SHIPEMPTY"));
          String strShipDist = GmCommonClass.parseZero((String) hmTemp.get("SHIP_DIST"));
          String strBillDist = GmCommonClass.parseZero((String) hmTemp.get("BILL_DIST"));
          if (!strShipDist.equals("0")
              || (strShipEmpty.equals("SHIPEMPTY") && !strBillDist.equals("0"))) {
            blMapFlag = true;
            break;
          }
          i++;
        } while (i < alResult.size());
        if (blMapFlag) {
          gmSetOverrideRptForm.setMapFlag("Y");
        } else {
          gmSetOverrideRptForm.setMapFlag("");
        }
      }
      gmSetOverrideRptForm.setAlResult(alResult);
      if (strOpt.equals("loadMap")) {
        String strXmlData = getXMLString(alResult);
        gmSetOverrideRptForm.setXmlData(strXmlData);
        strForward = "GmSetOverrideMap";
      }
    }

    return actionMapping.findForward(strForward);
  }

  public String getXMLString(ArrayList alResult) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateName("GmSetOverrideMap.vm");
    templateUtil.setTemplateSubDir("sales/templates");
    return templateUtil.generateOutput();
  }

}
