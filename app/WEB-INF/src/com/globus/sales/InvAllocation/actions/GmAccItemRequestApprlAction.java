package com.globus.sales.InvAllocation.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.requests.beans.GmLoanerReqEditBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;
import com.globus.sales.InvAllocation.beans.GmLoanerRequestApprovalBean;
import com.globus.sales.InvAllocation.beans.GmPendAllocationBean;
import com.globus.sales.InvAllocation.forms.GmAccItemRequestApprovalForm;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmAccItemRequestApprlAction extends GmSalesDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  public ActionForward accItemPendingApprRequests(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    log.debug("GmAccItemRequestApproval");
    GmAccItemRequestApprovalForm gmAccItemRequestApprovalForm =
        (GmAccItemRequestApprovalForm) actionForm;
    gmAccItemRequestApprovalForm.loadSessionParameters(request);
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
    String strOpt = "";
    String strUserId = "";
    String strAccessFl = "";
    String strCondition = "";
    String strApplDateFmt = "";
    String strApprvGrpNm = "ACC_ITMREQ_APPVL";
    String strRejectGrpNm = "ACC_ITMREQ_REJECT";
    String strDistCondition = "";
    String strDepartMentID = "";
    String strFilterCondition = null;
    String strSessTodayDate = "";   
    String strDispatch = "GmAccItemRequestApproval";

    ArrayList alSystemList = new ArrayList();  
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmFilter = new HashMap();
    HashMap hmExclusion = new HashMap();
    HashMap hmSalesFilters = new HashMap();

    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
    GmLoanerRequestApprovalBean gmLoanerRequestApprovalBean =
        new GmLoanerRequestApprovalBean(getGmDataStoreVO());
    GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();


    HttpSession session = request.getSession(false);

    strDepartMentID = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    request.setAttribute("strOverrideAccLvl",
        GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
    strSessTodayDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());

    //getting more filter access
    strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
    hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
    strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
    request.setAttribute("hmSalesFilters", hmSalesFilters);

    strCondition = getAccessFilter(request, response);
    strUserId = gmAccItemRequestApprovalForm.getUserId();


    strOpt = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getStrOpt());
    // The Below Code is added for PMT-29408 When Approve  with Return Quantity StrOpt
    // will be appwithra.
    if (!strOpt.equals("save") && !strOpt.equals("load") && !strOpt.equals("appwithra")
        && !strOpt.equals("Approvalview")) {

      String strTxnType = gmAccItemRequestApprovalForm.getStrOpt();
      gmAccItemRequestApprovalForm.setStrTxnType(strTxnType);

    }
     

      strDistCondition = getAccessFilter(request, response, false);
      hmFilter.put("STATUS", "Active");
      hmFilter.put("CONDITION", strDistCondition);
      hmExclusion.put("COMPANY", " AND v700.COMPID NOT IN (100801) "); // /Excluding the Algea
                                                                       // companies
      hmFilter.put("EXCLUSION_MAP", hmExclusion);
      hmFilter.put("FLTR_DIST_BY_COMP", "YES");// Filter Distributor Based on company

	  alSystemList = GmCommonClass.parseNullArrayList(gmSales.loadProjectGroup()); 
      gmAccItemRequestApprovalForm.setAlSystemList(alSystemList);

    //approval/rejection user access flag
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            strApprvGrpNm));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmAccItemRequestApprovalForm.setAppvlAccessFl(strAccessFl);

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            strRejectGrpNm));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmAccItemRequestApprovalForm.setRejectAccessFl(strAccessFl);

    hmParam = GmCommonClass.getHashMapFromForm(gmAccItemRequestApprovalForm);

    hmParam =
        GmCommonClass.parseNullHashMap(setRequestStatus(hmParam, session,
            gmAccItemRequestApprovalForm));
    //for Initiate RA mail
    hmParam.put(
        "SUBREPORT_DIR",
        request.getSession().getServletContext()
            .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
    hmParam.put("COMPANY_INFO", strCompanyInfo);
    // The Below Code is Changed for PMT-5988 Where while Approving Item Request With Return
    // Quantity StrOpt will be appwithra.
    if (strOpt.equals("save") || strOpt.equals("appwithra")) {
     hmReturn= saveAccItemRequestApproval(gmAccItemRequestApprovalForm,hmParam);
     hmParam.putAll(hmReturn);      
    }
   
    // Append the Sales filter only when Sales Team is approving request.
    // For In-house Employee, no filter condition required.
    if (strDepartMentID.equals("S"))
      hmParam.put("ACCESSFILTER", strCondition);
    
      hmParam.put("Filter", strFilterCondition);
      hmParam.put("SESSDATEFMT", strApplDateFmt);
      hmParam.put("SESSTODAYDATE", strSessTodayDate);
      strOpt = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getStrOpt());
      //load the Pending Account Item 
    if (strOpt.equals("load")) {
      loadAccItemRequestApproval(gmAccItemRequestApprovalForm,hmParam);
    }

    /*
     * When PD Wants to Approve the Request with
     * comments The below mentioned code will open a dhtmlx window ,where we can provide comments
     * and Approve the RA.
     */
    if (strOpt.equals("Approvalview")) {
      String strmessage = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getMessage());
      if (strmessage.equals("")) {
        String strRequestorname =
            GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getRequestor_name());
        String strSubject = "Take an action for Account Item request ";
        if (!strRequestorname.equals("")) {
          strSubject += "from " + strRequestorname;
        }
        gmAccItemRequestApprovalForm.setEmail_subject(strSubject);
      }
      strDispatch = "ApprovalView";
    }

    return actionMapping.findForward(strDispatch);
  }
  
  /**
   * saveAccItemRequestApproval - method to save or approve the Account Item Pending Request 
   * 
   * @param HashMap,form
   * @return HashMap
   * @exception AppError
   */ 

  private HashMap saveAccItemRequestApproval(GmAccItemRequestApprovalForm gmAccItemRequestApprovalForm ,HashMap hmParam)
      throws AppError {

    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean(getGmDataStoreVO());
    String strExcRAFlag = "";
    String strRequestIds = "";
    String strRAID = "";
    String strOpt = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getStrOpt());
    String strUserId = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getUserId());
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANY_INFO"));
    HashMap hmReturn = new HashMap();
    
     // to save approval 
    saveApprovalQuantity(gmAccItemRequestApprovalForm);

    //to save request approval
    hmReturn = GmCommonClass.parseNullHashMap(gmPendAllocationBean.saveLoanerReqAppv(hmParam));
    String strPrReqDetLitePnum =
        GmCommonClass.parseNull((String) hmReturn.get("PRREQDETLITEPNUM"));
  
    if (!strPrReqDetLitePnum.equals("")) {
      gmAccItemRequestApprovalForm.setPrReqDetLitePnum(strPrReqDetLitePnum);
    }

    gmAccItemRequestApprovalForm.setStrReqIds(GmCommonClass.parseNull((String) hmReturn
        .get("PRODREQDID")));
    // sending item request approval notification email.
    sendApprovalNotification(gmAccItemRequestApprovalForm,strCompanyInfo);
    
    //for not creating GM-RA for the requests 
    strExcRAFlag = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("WHOLE_RQ_RA", "RA_CREATION",
        getGmDataStoreVO().getCmpid()));

  // The Below Code is  for PMT-29408 Where while Approving Account Item Request With Return
  // Quantity StrOpt will be appwithra.
  // While Approving It needs to Send E-mail So below code is added.
  if (strOpt.equals("appwithra")) {
    
    if(strExcRAFlag.equals("Y")){
      strRequestIds = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getRequest_id());
      gmPendAllocationBean.updateRAFlagForReturns(strRequestIds, strUserId);
           
    }else{
            
    HashMap hmRADetails = new HashMap();
    ArrayList alRAMailDecide = new ArrayList();
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strPartString = GmCommonClass.parseNull((String) hmParam.get("PARTSTRING"));
    String strRAQtyString = GmCommonClass.parseNull((String) hmParam.get("RAQTYSTRING"));

    strRAID = gmPendAllocationBean.saveApprReqWithRA(hmParam);
    gmAccItemRequestApprovalForm.setRaId(strRAID);
  }
   
}
  gmAccItemRequestApprovalForm
      .setMessage("The request has been approved and e-mail has sent to respective person(s). ");
  String strdisplayscreen = gmAccItemRequestApprovalForm.getChkbox_display();
  if (strdisplayscreen.equals("true")) {
    strOpt = "Approvalview";
  } else {
    strOpt = "load";
  }
  gmAccItemRequestApprovalForm.setStrOpt(strOpt);
  return hmReturn;

}
  /**
   * loadAccItemRequestApproval -method to load the Account Item Pending Report 
   * 
   * @param HashMap,form
   * @return non-return type
   * @exception AppError
   */ 
  private void loadAccItemRequestApproval(GmAccItemRequestApprovalForm gmAccItemRequestApprovalForm ,HashMap hmParam)
      throws AppError {
    GmLoanerReqEditBean gmLoanerReqEditBean = new GmLoanerReqEditBean(getGmDataStoreVO());
    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    HashMap hmParams = new HashMap();
    String strxmlGridData = "";
    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParam.get("STRTXNTYPE"));
    String strType = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getStrTxnType());
    gmLoanerReqEditBean.validateProdRequestID(strRequestId, strTxnType);

  
    alResult =
        GmCommonClass.parseNullArrayList(gmPendAllocationBean
            .fetchPendingAccItemApprlReqDtl(hmParam));
    gmAccItemRequestApprovalForm.setAlResult(alResult);
    hmParams.putAll(hmParam);
    hmParams.put("REQTYPEID", strType);
    hmParams.put("PENDLNRDATA", alResult);
    hmParams.put("STRACCESSFL",
        GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getAppvlAccessFl()));


    hmParams.put("VMFILEPATH",
          "properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest");
    hmParams
          .put("USERTYPE", GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getUserType()));
      strxmlGridData = generateOutPut(hmParams, "GmAccItemReqApproval.vm");
      gmAccItemRequestApprovalForm.setXmlGridData(strxmlGridData);

  }
  /**
   * generateOutPut xml content generation for Account Item Pending Report grid
   * 
   * @param HashMap,session,form
   * @return HashMap
   * @exception AppError
   */ 
  
  private String generateOutPut(HashMap hmReturn, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmReturn.get("VMFILEPATH"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setTemplateSubDir("sales/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }



  /**
   * setRequestStatus will set the required information that is used for process transaction.
   * 
   * @param HashMap,session,form
   * @return HashMap
   * @exception AppError
   */
  private HashMap setRequestStatus(HashMap hmParam, HttpSession session,
      GmAccItemRequestApprovalForm gmAccItemRequestApprovalForm) throws AppError {
    String strStatus = "'2','5'";


    if (!gmAccItemRequestApprovalForm.getAppvlAccessFl().equals("")
        || !gmAccItemRequestApprovalForm.getRejectAccessFl().equals("")) {
      gmAccItemRequestApprovalForm.setUserType("PD");
      gmAccItemRequestApprovalForm.setAppvlAccessFl("Y");
      gmAccItemRequestApprovalForm.setRejectAccessFl("Y");
      strStatus = "'5'";
    }

    hmParam = GmCommonClass.getHashMapFromForm(gmAccItemRequestApprovalForm);
    hmParam.put("REQ_STATUS", strStatus);
    return hmParam;
  }
  /**
   * saveApprovalQuantity- used to save the requested item qty.
   * 
   * @param form-gmAccItemRequestApprovalForm
   * @return non return type
   * @exception AppError
   */
  private void saveApprovalQuantity(GmAccItemRequestApprovalForm gmAccItemRequestApprovalForm)
      throws AppError {
    String strInputStr =
        GmCommonClass.parseNull(gmAccItemRequestApprovalForm.gethApproveInputString());
    String strUserId = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getUserId());
    GmPendAllocationBean gmPendAllocationBean = new GmPendAllocationBean(getGmDataStoreVO());
    gmPendAllocationBean.saveRequestItemQty(strInputStr, strUserId);
  }
  /**
   * sendApprovalNotification- used to create request
   * 
   * @param form - gmAccItemRequestApprovalForm,strCompanyInfo
   * @return non return type
   * @exception AppError
   */
  private void sendApprovalNotification(GmAccItemRequestApprovalForm gmAccItemRequestApprovalForm , String strCompanyInfo)
      throws AppError {
    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean(getGmDataStoreVO());
    GmLoanerRequestApprovalBean gmLoanerRequestApprovalBean =
        new GmLoanerRequestApprovalBean(getGmDataStoreVO());
    String strUserType = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getUserType());
    String strReqIds = GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getStrReqIds());
    String strPrReqDetLitePnum =
        GmCommonClass.parseNull(gmAccItemRequestApprovalForm.getPrReqDetLitePnum());
    String strRequestId = "";
    HashMap hmdata = new HashMap();
    /*
     * The below mentioned code is changed  The Approval Notification methods are moved
     * to Bean File
     */
    hmdata = GmCommonClass.getHashMapFromForm(gmAccItemRequestApprovalForm);

    if (strUserType.equals("PD") || !strPrReqDetLitePnum.equals("")) {

      if (!strUserType.equals("PD")) {
        strReqIds = strPrReqDetLitePnum;
        // The Below Code is Added  When Literature Part is Approved by AD
        // mail needs to be sent.
        hmdata.put("STRREQIDS", strReqIds);
      }
      // The Below Code is changed  In Approval Comments Page After Sending mail we need
      // to show the Request ID.
      strRequestId = gmCaseBookTxnBean.saveRequestInfo(strReqIds, strCompanyInfo);
      gmAccItemRequestApprovalForm.setRequest_id(strRequestId);
      gmLoanerRequestApprovalBean.fetchPDApprovalDtl(hmdata);

    }

  }
  
}
