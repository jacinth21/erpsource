package com.globus.sales.InvAllocation.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSetOverrideRptBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  public GmSetOverrideRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmSetOverrideRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /*
   * saveSetOverrideDtl method is used to save the override details for a tag.
   */
  public void saveSetOverrideDtl(HashMap hmParams) throws AppError {

    log.debug("========hmParams=========" + hmParams);
    String strCaseSetId = GmCommonClass.parseNull((String) hmParams.get("CASESETID"));
    String strNewTagId = GmCommonClass.parseNull((String) hmParams.get("TAGID"));
    String strTagDistId = GmCommonClass.parseNull((String) hmParams.get("TAGDISTID"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_sm_setmgmt_txn.gm_sav_setoverride", 4);
    gmDBManager.setString(1, strCaseSetId);
    gmDBManager.setString(2, strNewTagId);
    gmDBManager.setString(3, strTagDistId);
    gmDBManager.setString(4, strUserID);

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /*
   * fetchSetTagListDtl is used to load the sets for the self inventory and also from area inventory
   * CASEDISTID --- is specific to the case and to load that region tag ids DISTID --- is specific
   * for that distributor to load tags for that rep.
   */
  public ArrayList fetchSetTagListDtl(HashMap hmParam) throws AppError {

    ArrayList alReturn = null;
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strCaseDistId = GmCommonClass.parseNull((String) hmParam.get("CASEDISTID"));
    String strDistId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
    String strFilterCondition = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
    String strLat = GmCommonClass.parseNull((String) hmParam.get("LAT"));
    String strLng = GmCommonClass.parseNull((String) hmParam.get("LNG"));
    String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
    String strLockFromDt = GmCommonClass.parseNull((String) hmParam.get("LOCKFROMDT"));
    String strLockToDt = GmCommonClass.parseNull((String) hmParam.get("LOCKTODT"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strDepId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));

    log.debug("<<<<<< fetchSetTagListDtl >>>> strDepId " + strDepId);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    // sbQuery.append(" SELECT * FROM ( ");
    sbQuery.append(" SELECT tagid, distseq, distname ");
    sbQuery.append(" , distid, repname, repid ");
    sbQuery.append(" , LOCK_INV_FL, DECOMSIN_FL, ADDR_NM ");
    sbQuery.append(" , ADDRESS1, ADDRESS2, CITY ");
    sbQuery.append(" , STATE, ZIPCODE, PHONENUMBER ");
    sbQuery.append(" , EMAILID, billname, billaddress1 ");
    sbQuery.append(" , billaddress2, billcity, billstate ");
    sbQuery.append(" , billcountry, billzipcode, SHIPEMPTY ");
    sbQuery.append(" , case_date, caserep_lat, caserep_lng ");
    sbQuery.append(" , ship_lat, ship_long, bill_lat ");
    sbQuery
        .append(" , bill_long, GET_DISTANCE (caserep_lat, caserep_lng, ship_lat, ship_long) ship_dist, GET_DISTANCE ( ");
    sbQuery.append(" caserep_lat, caserep_lng, bill_lat, bill_long) bill_dist  FROM ( ");
    sbQuery.append(" SELECT t5010.c5010_tag_id tagid, DECODE (t5010.c5010_location_id, "
        + strCaseDistId + ", 1, 2) distseq ");
    sbQuery
        .append(" , get_distributor_name (t5010.c5010_location_id) distname, t5010.c5010_location_id distid, get_rep_name (t5010.c703_sales_rep_id) repname ");
    sbQuery
        .append(" , t5010.c703_sales_rep_id repid, NVL (v5010a.c5010_lockset, 'N') LOCK_INV_FL ");
    sbQuery
        .append(" , NVL (v5010a.c5010_decommissioned, 'N') DECOMSIN_FL, v5010.name ADDR_NM, v5010.address1 ADDRESS1 ");
    sbQuery.append(" , v5010.address2 ADDRESS2, v5010.city CITY, v5010.state STATE ");
    sbQuery
        .append(" , v5010.zipcode ZIPCODE, v5010.phonenumber PHONENUMBER, v5010.emailid EMAILID ");
    sbQuery.append(" , v5010.billname, v5010.billaddress1, v5010.billaddress2 ");
    sbQuery.append(" , v5010.billcity, v5010.billstate, v5010.billcountry ");
    sbQuery
        .append(" , v5010.billzipcode, v5010.SHIPEMPTY shipempty, gm_pkg_sm_setmgmt_txn.get_next_case_date (t5010.c5010_tag_id) case_date ");
    sbQuery
        .append(" , DECODE ( t5010.c901_sub_location_type, 50222, get_latitude (t5010.c106_address_id, 'ADDR', ''), 50221 ");
    sbQuery
        .append(" ,get_latitude ( t5010.c5010_sub_location_id, 'DIST', ''), 11301, get_latitude (t5010.c5010_sub_location_id, 'ACC', '')) ship_lat ");
    sbQuery
        .append(" , DECODE (t5010.c901_sub_location_type, 50222, get_latitude (t5010.c106_address_id , 'ADDR', '') ");
    sbQuery
        .append(" , 50221, get_latitude (t5010.c5010_sub_location_id, 'DIST', 'BILL'), 11301, get_latitude (t5010.c5010_sub_location_id ");
    sbQuery
        .append(" , 'ACC', 'BILL')) bill_lat, DECODE (t5010.c901_sub_location_type, 50222,get_longitude (t5010.c106_address_id, 'ADDR', '') ");
    sbQuery.append(" , 50221, get_longitude (t5010.c5010_sub_location_id,'DIST', '') ");
    sbQuery.append(" , 11301, get_longitude ( t5010.c5010_sub_location_id, 'ACC', '')) ship_long ");
    sbQuery
        .append(" , DECODE ( t5010.c901_sub_location_type, 50222, get_longitude (t5010.c106_address_id, 'ADDR', ''), 50221 ");
    sbQuery.append(" , get_longitude (t5010.c5010_sub_location_id, 'DIST', 'BILL'), 11301 ");
    sbQuery
        .append(" , get_longitude (t5010.c5010_sub_location_id, 'ACC', 'BILL')) bill_long, get_latitude (get_address_id (get_case_rep_id ('"
            + strCaseInfoId + "', NULL)) ");
    sbQuery.append(" , 'ADDR', '') CASEREP_LAT, get_longitude (get_address_id (get_case_rep_id ('"
        + strCaseInfoId + "', NULL)), 'ADDR', '') CASEREP_LNG ");
    // Comment for MNTTASK-7882 Auto Allocation
    /*
     * sbQuery.append(" , gm_pkg_sm_setmgmt_txn.get_chk_avail_tag(t5010.c5010_tag_id ");
     * sbQuery.append(" ,to_date('"+strLockFromDt+"', GET_RULE_VALUE('DATEFMT','DATEFORMAT')) ");
     * sbQuery.append(" ,to_date('"+strLockToDt+"', GET_RULE_VALUE('DATEFMT','DATEFORMAT'))) cnt ");
     */
    sbQuery.append(" FROM t5010_tag t5010, v5010a_tag_attribute v5010a, ( ");
    sbQuery.append(" SELECT tagid, name, address1 ");
    sbQuery
        .append(" , address2, DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE', 'SWAP_ZIP_CODE'), 'N'), 'Y', zipcode, city) city ");
    sbQuery
        .append(" , get_code_name_alt (state) state, DECODE (NVL (GET_RULE_VALUE ('SWAP_ZIP_CODE' ");
    sbQuery.append(" , 'SWAP_ZIP_CODE'), 'N'), 'Y', city, zipcode) zipcode, phonenumber, emailid ");
    sbQuery.append(" , billname, billaddress1, billaddress2 ");
    sbQuery.append(" , billcity, billstate, billcountry ");
    sbQuery.append(" , billzipcode, SHIPEMPTY ");
    sbQuery.append(" FROM v5010_curr_address   ) v5010 ");
    sbQuery.append(" WHERE t5010.c207_set_id        = '" + strSetId + "' ");
    sbQuery.append(" AND t5010.c5010_void_fl     IS NULL ");
    sbQuery.append(" AND t5010.c5010_tag_id       = v5010.tagid (+) ");
    sbQuery.append(" AND t5010.c5010_tag_id       = v5010a.c5010_tag_id (+) ");
    sbQuery.append(" AND t5010.c901_status        = 51012 ");
    sbQuery.append(" AND  NVL (v5010a.c5010_lockset, 'N') = 'N' ");
    sbQuery.append(" AND NVL (v5010a.c5010_decommissioned, 'N') = 'N' ");


    /*
     * if (!strFilterCondition.equals("")) { sbQuery.append(" AND T5010.c703_sales_rep_id IN (");
     * sbQuery.append(" SELECT	DISTINCT REP_ID FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
     * sbQuery.append(" WHERE  REP_ID IS NOT NULL  AND "); sbQuery.append(strFilterCondition);
     * sbQuery.append(")");
     * 
     * }
     */
    if (!strDistId.equals("0") && !strDistId.equals("")) {
      sbQuery.append(" AND t5010.c5010_location_id = '" + strDistId + "'");
    }

    else {
      sbQuery.append(" AND t5010.c5010_location_id IN ( ");
      sbQuery.append(" SELECT c701_distributor_id ");
      sbQuery.append(" FROM t701_distributor ");
      sbQuery.append(" WHERE c701_region = (SELECT c701_region ");
      sbQuery.append(" FROM t701_distributor ");
      sbQuery.append(" WHERE c701_distributor_id = " + strCaseDistId
          + " and c701_void_fl IS NULL) ");
      sbQuery.append(" ) ");
    }
    sbQuery.append(" ) avail_tags ");
    // Comment for MNTTASK-7882 Auto Allocation
    // sbQuery.append(" WHERE avail_tags.cnt < 1 ");
    if (strDepId.equals("2005")) {
      sbQuery
          .append(" WHERE NVL(DECODE(SHIPEMPTY,'SHIPEMPTY',GET_DISTANCE ( caserep_lat, caserep_lng, bill_lat, bill_long) ");
      sbQuery.append(" ,GET_DISTANCE (caserep_lat, caserep_lng, ship_lat, ship_long)),0) < 500 ");
    }
    sbQuery
        .append(" order by DECODE(DECODE(SHIPEMPTY,'SHIPEMPTY',billaddress1,ADDRESS1),NULL,9999999,nvl(ship_dist,bill_dist)),ADDRESS1 ");
    log.debug("fetchSetTagListDtl query is   " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }



  /*
   * saveLoanerTagRequest method is used to initiate a loaner tag.
   */
  public void saveLoanerTagRequest(HashMap hmParams) throws AppError {

    log.debug("========hmParams=========" + hmParams);
    String strCaseInfoId = GmCommonClass.parseNull((String) hmParams.get("CASEINFOID"));
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_loaner_allocation.gm_initiate_multiple_loaners", 3);
    gmDBManager.setInt(1, Integer.parseInt(strCaseInfoId));
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }
}
