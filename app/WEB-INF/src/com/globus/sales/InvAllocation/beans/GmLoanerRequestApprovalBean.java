package com.globus.sales.InvAllocation.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.sales.CaseManagement.beans.GmCaseBookRptBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerRequestApprovalBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmLoanerRequestApprovalBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmLoanerRequestApprovalBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * FetchADApprovalDtl will Fetch the Details of Rep List and it will be Processed in
   * ProcessApproval Details Method And Email will be sent in Send Notification Method.
   * 
   * @param HashMap hmDetails
   * @return
   * @exception AppError
   */
  public void fetchADApprovalDtl(HashMap hmDetails) throws AppError {
    GmCaseBookRptBean gmcaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();


    String strJasperNm = "/GmItemRequestADApprEmail.jasper";
    String strDefaultEmailID = "";
    String strStatus = "5,10";
    String strReqIds = GmCommonClass.parseNull((String) hmDetails.get("STRREQIDS"));
    String TEMPLATE_NAME = "GmInvMgmItemReqNotifyEmail";
    String strSubject = "";
    String strComapnyId = "";

    strComapnyId = getGmDataStoreVO().getCmpid();
    strDefaultEmailID =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("ITEM_REQ_AD_APPROVAL",
            "DEFAULT_CC", strComapnyId));


    HashMap hmEmailData = new HashMap();
    HashMap hmParam = new HashMap();

    GmEmailProperties emailProps = new GmEmailProperties();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    strSubject = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
    strSubject = GmCommonClass.replaceAll(strSubject, "<#STATUS>", "Approved");

    hmEmailData.put("JASPERNAME", strJasperNm);
    hmEmailData.put("TEMPLATE_NAME", TEMPLATE_NAME);
    hmEmailData.put("CC_DEFAULT_EMAIL", strDefaultEmailID);
    hmEmailData.put("SUBJECT", strSubject);

    /*
     * The Below HashMap Details are Added as part of PMT-6432 Here We are in need to Add the
     * ArrayList Alias Name as value which will be used in processing the ArrayList
     */
    hmParam.put("REP_ID", "REP_ID");
    hmParam.put("ID", "ID");
    hmParam.put("REQID", "REQ_ID");
    hmParam.put("PARREQID", "PAR_REQID");
    hmParam.put("PNUM", "PNUM");
    hmParam.put("PDESC", "PDESC");
    hmParam.put("REQQTY", "REQ_QTY");
    hmParam.put("APPROVEDQTY", "APPROVED_QTY");
    hmParam.put("APPROVEDBY", "APPROVED_BY");
    hmParam.put("SHEET_OWNER_NAME", "SHEET_OWNER_NAME");
    hmParam.put("COMMENTS", "COMMENTS");
    hmParam.put("SHEET_OWNER_EMAIL", "SHEET_OWNER_EMAIL");
    hmParam.put("APPROVALBY", "AD");
    hmParam.put("hmEmailData", hmEmailData);
    hmParam.put("REP_EMAIL", "REP_EMAIL");
    hmParam.put("USER_MAILID", "USER_MAILID");
    // Fetching The Rep List Data when AD is Approving the Request
    alResult =
        GmCommonClass.parseNullArrayList(gmcaseBookRptBean.fetchADApprovalNotification(strReqIds,
            strStatus));
    /*
     * The Below method is used to Process the Array List For Single Sales Rep and Email will be
     * send to Rep.
     */
    processApprovalDetails(alResult, hmParam);
  }

  /**
   * FetchPDApprovalDtl will Fetch the Details of Rep List and it will be Processed in
   * ProcessApproval Details Method And Email will be sent in Send Notification Method.
   * 
   * @param HashMap hmDetails
   * @return
   * @exception AppError
   */
  public void fetchPDApprovalDtl(HashMap hmDetails) throws AppError {

    GmCaseBookRptBean gmCaseBookRptBean = new GmCaseBookRptBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    ArrayList alList = new ArrayList();
    ArrayList alReportList = new ArrayList();
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();
    GmCommonBean gmCommonBean = new GmCommonBean();

    String strJasperNm = "/GmItemRequestPDApprEmail.jasper";
    String strDefaultEmailID = "";
    String strReqIds = GmCommonClass.parseNull((String) hmDetails.get("STRREQIDS"));
    String strCompanyId = "";

    strCompanyId = getGmDataStoreVO().getCmpid();
    strDefaultEmailID =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("ITEM_REQ_PD_APPROVAL",
            "DEFAULT_CC", strCompanyId));
    String TEMPLATE_NAME = "GmInvMgmItemReqEmail";
    String strSubject = "";

    HashMap hmEmailData = new HashMap();
    HashMap hmParam = new HashMap();

    GmEmailProperties emailProps = new GmEmailProperties();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    strSubject = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
    strSubject = GmCommonClass.replaceAll(strSubject, "<#STATUS>", "Approved");

    hmEmailData.put("JASPERNAME", strJasperNm);
    hmEmailData.put("TEMPLATE_NAME", TEMPLATE_NAME);
    hmEmailData.put("CC_DEFAULT_EMAIL", strDefaultEmailID);
    hmEmailData.put("SUBJECT", strSubject);
    
    /*
     * The Below HashMap Details are Added as part of PMT-6432 Here We are in need to Add the
     * ArrayList Alias Name as value which will be used in processing the ArrayList
     */

    hmParam = new HashMap();
    hmParam.put("REP_ID", "REP_ID");
    hmParam.put("RQID", "RQID");
    hmParam.put("REQID", "REQ_ID");
    hmParam.put("PARREQID", "PAR_REQID");
    hmParam.put("PNUM", "PNUM");
    hmParam.put("PDESC", "PDESC");
    hmParam.put("APPROVEDQTY", "APPROVED_QTY");
    hmParam.put("SPLITAPROVEDQTY", "SPLITAPROVEDQTY");
    hmParam.put("APPROVEDBY", "APPROVED_BY");
    hmParam.put("COMMENTS", "COMMENTS");
    hmParam.put("STATUS", "STATUS");
    hmParam.put("APPROVALBY", "PD");
    hmParam.put("hmEmailData", hmEmailData);
    hmParam.put("REP_EMAIL", "REP_EMAIL");
    hmParam.put("USER_MAILID", "USER_MAILID");
    // Fetching The Rep List Data when PD is Approving the Request
    alResult =
        GmCommonClass.parseNullArrayList(gmCaseBookRptBean.fetchPDApprovalNotification(strReqIds));

    /*
     * The Below method is used to Process the Array List For Single Sales Rep and Email will be
     * send to Sales Rep.
     */
    processApprovalDetails(alResult, hmParam);
    sendInhouseEmail(alResult, hmDetails);
  }

  /**
   * sendInhouseEmail The Below code is Added as Part of PMT-6432, Here When we are sending Inhouse
   * Email Request We are in need to Send GM-RQ as Request Id to Email Notification.
   * 
   * @param HashMap hmParam,ArrayList alRepList
   * @return
   * @exception AppError
   */
  private void sendInhouseEmail(ArrayList alRepList, HashMap hmParam) {

    int intArLength = 0;
    String strRQID = "";
    String strRequestid = "";
    String strInhsecomments = "";
    String strTo = "";
    String strCC = "";
    String strUsermailId = "";
    String strInhouseSubject = "";
    String strDefaultEmailID = "";
    String strCompanyId = "";

    strCompanyId = getGmDataStoreVO().getCmpid();
    strDefaultEmailID =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("ITEM_REQ_PD_APPROVAL",
            "DEFAULT_CC", strCompanyId));

    strInhsecomments = GmCommonClass.parseNull((String) hmParam.get("EMAIL_COMMENTS"));

    HashMap hmJasperDetails = new HashMap();
    ArrayList alTemp = new ArrayList();
    if (!strInhsecomments.equals("")) {

      strTo = GmCommonClass.parseNull((String) hmParam.get("EMAIL_TO"));
      strCC = GmCommonClass.parseNull((String) hmParam.get("EMAIL_CC"));
      strInhouseSubject = GmCommonClass.parseNull((String) hmParam.get("EMAIL_SUBJECT"));


      intArLength = alRepList.size();
      if (intArLength > 0) {
        HashMap hmLoop = new HashMap();
        for (int i = 0; i < intArLength; i++) {
          hmLoop = (HashMap) alRepList.get(i);
          strUsermailId = GmCommonClass.parseNull((String) hmLoop.get("USER_MAILID"));
          strRQID = GmCommonClass.parseNull((String) hmLoop.get("RQID"));
          if (!strRequestid.equals("")) {
            if (!strRequestid.contains(strRQID)) {
              strRequestid = strRequestid + "," + strRQID;
            }
          } else {
            strRequestid = strRQID;
          }
        }
      }
      strDefaultEmailID = strUsermailId + "," + strDefaultEmailID;
      if (!strCC.equals("")) {
        strDefaultEmailID = strDefaultEmailID + "," + strCC;
      }
      String strTemplateName = "GmInvMgmInhouseEmail";
      String strJasperName = "/GmItemRequestInhouseEmail.jasper";

      hmJasperDetails.put("COMMENTS", strInhsecomments);
      hmJasperDetails.put("REP_EMAIL", strTo);
      hmJasperDetails.put("REQUESTID", strRequestid);
      hmJasperDetails.put("CC_EMAIL", strDefaultEmailID);
      hmJasperDetails.put("SUBJECT", strInhouseSubject);
      hmJasperDetails.put("TEMPLATE_NAME", strTemplateName);
      hmJasperDetails.put("JASPERNAME", strJasperName);
      sendNotification(alTemp, hmJasperDetails);

    }

  }

  /**
   * processApprovalDetails will Process the Details of Rep List And Email will be sent in Send
   * Notification Method.
   * 
   * @param HashMap hmKeys,ArrayList alApprDataList
   * @return
   * @exception AppError
   */
  public void processApprovalDetails(ArrayList alApprDataList, HashMap hmKeys) {

    String strRepId = "";
    String strCCEmail = "";
    String strDefaultEmailId = "";
    String strEmailVisitedFl = "";
    HashMap hmEmailData = new HashMap();
    ArrayList alEmailData = new ArrayList();
    HashMap hmReturn = new HashMap();
    Iterator itr = alApprDataList.iterator();
    hmEmailData = GmCommonClass.parseNullHashMap((HashMap) hmKeys.get("hmEmailData"));
    HashMap hmParam = new HashMap();
    while (itr.hasNext()) {
      HashMap hmLoop = new HashMap();
      hmLoop = (HashMap) itr.next();
      strRepId = GmCommonClass.parseNull((String) hmLoop.get(hmKeys.get("REP_ID")));
      strEmailVisitedFl = GmCommonClass.parseNull((String) hmLoop.get("EMAILVISITED"));
      /*
       * Initially the strEmailVisitedFl will be Null,so it will be calling the method
       * getEmailValues In this method it will be preparing the data for Single Rep and mail will be
       * sent to that single Rep. After sending mail to single Rep if there is any other Rep for
       * those multiple Rep Data will be grouped and mail will be sent.
       */
      if (!strEmailVisitedFl.equals("Y")) {
        hmReturn = getEmailValues(strRepId, alApprDataList, hmKeys);
        // We are in need to Append User Mail id to CC the below code is added for that reason.
        // strDefaultEmailId - Mail Id From Rule Table.
        strDefaultEmailId = GmCommonClass.parseNull((String) hmEmailData.get("CC_DEFAULT_EMAIL"));
        // strCCEmail - User Mail ID.
        strCCEmail = GmCommonClass.parseNull((String) hmReturn.get("CC_USER_EMAIL"));
        strDefaultEmailId = strCCEmail + ',' + strDefaultEmailId;
        hmEmailData.put("REP_EMAIL", hmReturn.get("REP_EMAIL"));
        hmEmailData.put("CC_EMAIL", strDefaultEmailId);
        alEmailData = (ArrayList) hmReturn.get("ALREPLIST");
        // Sending Mail For Single Rep List
        sendNotification(alEmailData, hmEmailData);
      }
    }
  }

  /**
   * getEmailValues - This method will be used to group the arraylist based on the rep id
   * 
   * @param String repId, ArrayList alEmailData,HashMap hmKeys
   * @return HashMap
   */
  private HashMap getEmailValues(String repId, ArrayList alEmailData, HashMap hmKeys) {

    String strReqId = "";
    String strNextApprEmail = "";
    String strRepEmail = "";
    String strCCEmail = "";
    String strTempApprEmail = "";
    String strApprovalBy = "";
    String strShipSrc = "50181";
    String strEmailVisitedFl = "";

    Iterator itrValues = alEmailData.iterator();
    HashMap hmValues = new HashMap();
    ArrayList alRepList = new ArrayList();
    HashMap hmParam = null;
    HashSet hsRepDtl = null;
    HashMap hmShipParam = new HashMap();
    HashSet hsTemp = new HashSet();
    HashMap hmReturn = new HashMap();

    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());


    strApprovalBy = GmCommonClass.parseNull((String) hmKeys.get("APPROVALBY"));

    while (itrValues.hasNext()) {

      hmValues = (HashMap) itrValues.next();
      strEmailVisitedFl = GmCommonClass.parseNull((String) hmValues.get("EMAILVISITED"));

      /*
       * Initially The PD Approval Details and AD Approval Details Data are Selected and Added into
       * HashMap The Same HashMap is added into ArrayList and that ArrayList is added into HashMap .
       * The Final HashMap Contains Email Data For which mail needs to be sent and Array List for
       * Single Rep.
       */
      if (repId.equals(hmValues.get(hmKeys.get("REP_ID"))) && !strEmailVisitedFl.equals("Y")) {
        strTempApprEmail =
            GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("SHEET_OWNER_EMAIL")));
        strRepEmail = GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("REP_EMAIL")));
        // Getting The user Mail ID from Array List and Adding it into HashMap
        strCCEmail = GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("USER_MAILID")));
        strReqId = GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("REQID")));

        hmParam = new HashMap();
        // The below Method is used to Get the Hash Map Key and value which will be added into Array
        // List and sent in Notification Emails.
        hmParam = getInputParams(hmValues, hmKeys);
        if (strApprovalBy.equals("PD")) {
          hmShipParam =
              GmCommonClass.parseNullHashMap(gmShippingInfoBean.fetchShipAddress(strReqId,
                  strShipSrc));
          hmParam.putAll(hmShipParam);
        }
        if (strApprovalBy.equals("AD")) {
          hsTemp.add(strTempApprEmail);
          strCCEmail = strRepEmail;
          strRepEmail = GmCommonClass.parseNull((String) hmValues.get("AD_EMAIL_ID"));
        }
        alRepList.add(hmParam);
        // When Data for Particular Rep is completed we are setting Flag to Y.So for the Respective
        // Rep Mail will not be sent when the flag is enabled.
        hmValues.put("EMAILVISITED", "Y");

      }
    }

    /*
     * When AD Approving the Request we are in need to Append the Sheet Owner Email For The
     * Respective Part. The Below Code is added for that scenario. When PD Approves we are in need
     * to set only Rep mail id in To section.
     */
    if (strApprovalBy.equals("AD")) {
      for (Object str : hsTemp) {
        strNextApprEmail += ";" + str.toString();
      }
      strRepEmail =
          strNextApprEmail.equals("") ? strRepEmail : strRepEmail + "," + strNextApprEmail;
    }
    hmReturn.put("REP_EMAIL", strRepEmail);
    hmReturn.put("CC_USER_EMAIL", strCCEmail);
    hmReturn.put("ALREPLIST", alRepList);

    return hmReturn;
  }

  /**
   * getInputParams The below code will be called when AD/PD E-mails are sent Common Template used
   * to Return HashMap Which will Contain All The values which are displayed when Notification Mail
   * is sent.
   * 
   * @param HashMap hmValues,hmKeys
   * @return HashMap
   * @exception AppError
   */
  public HashMap getInputParams(HashMap hmValues, HashMap hmKeys) throws AppError {
    HashMap hmParam = new HashMap();

    hmParam.put("RQID", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("RQID"))));
    hmParam.put("ID", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("ID"))));
    hmParam.put("REQID", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("REQID"))));
    hmParam.put("PARREQID", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("PARREQID"))));
    hmParam.put("PNUM", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("PNUM"))));
    hmParam.put("PDESC", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("PDESC"))));
    hmParam.put("REQQTY", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("REQQTY"))));
    hmParam.put("APPROVEDQTY",
        GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("APPROVEDQTY"))));
    hmParam.put("APPROVEDBY",
        GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("APPROVEDBY"))));
    hmParam.put("SHEET_OWNER_NAME",
        GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("SHEET_OWNER_NAME"))));

    hmParam.put("COMMENTS", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("COMMENTS"))));
    hmParam.put("STATUS", GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("STATUS"))));
    hmParam.put("SPLITAPROVEDQTY",
        GmCommonClass.parseNull((String) hmValues.get(hmKeys.get("SPLITAPROVEDQTY"))));
    return hmParam;
  }

  /**
   * sendNotification The below code will be called when AD/PD/Inhoude E-mails are sending Common
   * Template used for Three different Email Scenarios.
   * 
   * @param HashMap hmDetails ,ArrayList alList
   * @return
   * @exception AppError
   */
  public void sendNotification(ArrayList alDataList, HashMap hmDetails) throws AppError {

    GmEmailProperties emailProps = null;

    emailProps = new GmEmailProperties();

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    String TEMPLATE_NAME = GmCommonClass.parseNull((String) hmDetails.get("TEMPLATE_NAME"));
    emailProps.setSender(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.FROM));
    emailProps.setRecipients(GmCommonClass.parseNull((String) hmDetails.get("REP_EMAIL")));
    emailProps.setCc(GmCommonClass.parseNull((String) hmDetails.get("CC_EMAIL")));
    emailProps.setMimeType(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.MIME_TYPE));
    emailProps.setSubject(GmCommonClass.parseNull((String) hmDetails.get("SUBJECT")));
    HashMap hmEmailData = new HashMap();
    hmEmailData.put("EMAILPROPS", emailProps);
    hmEmailData.put("REQUESTID", GmCommonClass.parseNull((String) hmDetails.get("REQUESTID")));
    hmEmailData.put("COMMENTS", GmCommonClass.parseNull((String) hmDetails.get("COMMENTS")));
    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName(GmCommonClass.parseNull((String) hmDetails.get("JASPERNAME")));
    jasperMail.setAdditionalParams(hmEmailData);
    if ((alDataList.size() > 0)) {
      jasperMail.setReportData(alDataList);
    }
    // jasperMail.setReportData(alList);
    jasperMail.setEmailProperties((GmEmailProperties) hmEmailData.get("EMAILPROPS"));
    HashMap hmjasperReturn = jasperMail.sendMail();
  }
}
