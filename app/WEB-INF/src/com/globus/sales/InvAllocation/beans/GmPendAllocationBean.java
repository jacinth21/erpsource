package com.globus.sales.InvAllocation.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.operations.returns.beans.GmReturnsBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPendAllocationBean extends GmSalesFilterConditionBean {

  public GmPendAllocationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmPendAllocationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public RowSetDynaClass fetchPendAllocation(HashMap hmParam) throws AppError {
    initializeParameters(hmParam);
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DIST"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
    String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
    Date dtSurgeryDt = (Date) hmParam.get("SUGERYDATE");
    Date dtLoanDt = (Date) hmParam.get("LOANDATE");
    String strSurgeryDt = "";
    String strLoanDt = "";
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));


    StringBuffer sbQuery = new StringBuffer();

    strSurgeryDt = GmCommonClass.getStringFromDate(dtSurgeryDt, strDateFmt);
    strLoanDt = GmCommonClass.getStringFromDate(dtLoanDt, strDateFmt);

    sbQuery
        .append("SELECT t7104.c7104_case_set_id casesetid, t7104.c7100_case_info_id caseinfoid, t7100.c7100_case_id case_id");
    sbQuery
        .append(" , t7100.c7100_surgery_date surgerydt, t7104.c207_set_id setid, get_set_name (t7104.c207_set_id) setname, c901_set_location_type settype");
    sbQuery
        .append("  , get_distributor_name (t7100.c701_distributor_id) distname, GET_REP_NAME (t7100.c703_sales_rep_id) repname");
    sbQuery.append("  ,t7100.c701_distributor_id casedistid");
    sbQuery
        .append("  ,t7104.c7104_tag_lock_from_date lockfromdt, t7104.c7104_tag_lock_to_date locktodt");
    sbQuery.append("  FROM t7104_case_set_information t7104, t7100_case_information t7100 ");
    sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery
        .append(" WHERE t7104.c7104_void_fl IS NULL AND t7104.c5010_tag_id IS NULL AND c526_product_request_detail_id IS NULL");
    sbQuery.append(" AND t7100.c7100_surgery_date > = trunc(CURRENT_DATE) ");
    sbQuery.append(" AND t7100.c901_type  = 1006503 "); // CASE
    sbQuery.append(" AND t7100.c703_sales_rep_id   = V700.REP_ID ");
    sbQuery.append("    AND t7104.c7100_case_info_id  = t7100.c7100_case_info_id ");
    sbQuery.append("    AND t7100.c901_case_status = 11091 AND t7100.c7100_void_fl IS NULL ");
    if (!strCaseId.equals("")) {
      sbQuery.append(" AND REGEXP_LIKE(t7100.c7100_case_id, '" + strCaseId + "')");
    }
    if (!strDistId.equals("0")) {
      sbQuery.append("   AND t7100.c701_distributor_id =" + strDistId);
    }
    if (!strRepID.equals("0")) {
      sbQuery.append("    AND t7100.c703_sales_rep_id = " + strRepID);
    }
    if (!strSurgeryDt.equals("")) {
      sbQuery.append(" AND TRUNC (t7100.c7100_surgery_date) = to_date('" + strSurgeryDt + "','"
          + strDateFmt + "')");
    }
    if (!strLoanDt.equals("")) {
      sbQuery
          .append("    AND TRUNC (get_lock_date (t7100.c7100_surgery_date, 'LOCK_FROM_DATE')-1) = to_date('"
              + strLoanDt + "','" + strDateFmt + "') ");
    }
    sbQuery.append("    Order by t7100.c7100_surgery_date");
    log.debug("Query inside fetchPendAllocation is " + sbQuery.toString());
    rdResult = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return rdResult;
  }
  
  
  public ArrayList fetchPendingApprlLoanerReqDtl(HashMap hmParam) throws AppError {

	    ArrayList alReturn = null;

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	    String strReqType = "4127";
	    String strStatus = "5";

	    String strFilterCondition = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
	    String strStrType = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
	    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
	    String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("EVENTNAME"));
	    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("SESSDATEFMT"));
	    String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDT"));
	    String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDT"));
	    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
	    String strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPE"));
	    String strReqStatus = GmCommonClass.parseNull((String) hmParam.get("REQ_STATUS"));
	    String strSalesRepID = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
	    String strPDApprReq = GmCommonClass.parseNull((String) hmParam.get("PDREJLNREQAPPR"));
	    String strFieldSales = GmCommonClass.parseNull((String) hmParam.get("FIELDSALES"));
	    String strSystemID = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
	    String strSheetOwner = GmCommonClass.parseNull((String) hmParam.get("SHEETOWNER"));
	    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("Filter"));

	    String strFilter =
	        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_REQ_APPVL"));/*
	                                                                                              * LOANER_REQ_APPVL
	                                                                                              * - To
	                                                                                              * fetch
	                                                                                              * the
	                                                                                              * records
	                                                                                              * for
	                                                                                              * EDC
	                                                                                              * entity
	                                                                                              * countries
	                                                                                              * based
	                                                                                              * on
	                                                                                              * plant
	                                                                                              */
	    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

	    if (strStrType.equals("save") || strStrType.equals("load") || strStrType.equals("appwithra")) {
	      strStrType = GmCommonClass.parseNull((String) hmParam.get("STRTXNTYPE"));
	    }

	    log.debug("the value inside strStrType after *** " + strStrType);
	    if (!strReqStatus.equals("")) {
	      strStatus = strReqStatus;
	    }

	    if (strStrType.equals("4119")) {
	      strReqType = "4119,9110";
	    }
	    if (strStrType.equals("400088")) {// ConsignmentItem
	      strReqType = "400088";
	      strRequestType = "1006507";
	    }
	    if (strStrType.equals("400087")) {// Consignment Set
	      strReqType = "400087";
	      strRequestType = "1006506";
	    }
	    // below condition works only for 'Pending PD Loaner Request' Screen
	    if (!strPDApprReq.equals("")) {
	      strReqType = "4127";
	      strStatus = "7";
	    }
	    if (strStrType.equals("400088")) {
	      sbQuery
	          .append(" SELECT get_set_name(lnappr.SYSTEM_ID) systemname, get_user_name(lnappr.SHEET_OWNER_ID) SHEET_OWNER, lnappr.* FROM( ");
	    } else {
	      sbQuery.append(" SELECT * FROM ( ");
	    }
	    sbQuery
	        .append(" ( SELECT t7104.c526_product_request_detail_id LNREQID, t7100.c7100_case_id CASEID,  t7100.c7100_created_date REQDATE, ");
	    sbQuery
	        .append(" decode(T7104.C207_Set_Id,null,1,0) seq, Nvl(T7104.C207_Set_Id,T7104.C205_Part_Number_Id) setid, decode(T7104.C207_Set_Id,");
	    sbQuery
	        .append(" null,get_partnum_desc (T7104.c205_part_number_id),get_set_name (t7104.c207_set_id)) setname,");
	    sbQuery
	        .append(" DECODE(T7104.C207_Set_Id,null,t7104.c7104_item_qty,1) qty, NVL(T526.C526_QTY_REQUESTED,t7104.c7104_item_qty)approve_qty ");

	    sbQuery
	        .append(" ,DECODE(T526.C526_STATUS_FL,'2','Pending AD Approval','5','Pending Approval',10,'Open',20,'Allocated')STATUS ,");
	    sbQuery
	        .append(" DECODE("
	            + strStrType
	            + ", 400088, get_user_name(t7100.c7100_created_by),400087,get_user_name(t7100.c7100_created_by) ,get_rep_name (t7100.c703_sales_rep_id))  repname, ");
	    sbQuery
	        .append("  nvl(t7100.c703_sales_rep_id, get_rep_id_from_user_id(t7100.c7100_created_by)) repid ,GET_REP_PHONE (t7100.c703_sales_rep_id) repphone, ");
	    sbQuery
	        .append("  get_account_name ( t7100.c704_account_id) acctname, t7100.c7100_surgery_date surgerydate ,DECODE(t526.c901_request_type, '400088',t526.C526_PLANNED_SHIP_DT,'400087',t526.C526_PLANNED_SHIP_DT,t526.c526_required_date)  requireddate ");
	    sbQuery
	        .append(" ,get_ad_rep_id(t7100.c703_sales_rep_id) adid ,t526.c525_product_request_id requestid ,get_system_from_setid(t7104.c207_set_id) sysid");
	    sbQuery
	        .append(" ,t7103.c7103_name eventname ,t7103.c7100_case_info_id caseinfoid,t526.c901_request_type reqtype, t901.c901_code_nm reqnm, get_rep_name (t525.c703_ass_rep_id) assocrepnm ");
	    sbQuery
	        .append(" ,t526.c205_part_number_id pnum,GET_PARTNUM_DESC (T526.C205_PART_NUMBER_ID) pdesc, GET_DISTRIBUTOR_NAME(t525.c525_request_for_id) fieldSales ");
	    if (strStrType.equals("400088")) {// Consignment Item
	    sbQuery
	    .append(" ,GET_CODE_NAME(T907.C901_DELIVERY_MODE) DELIVERYMODE , GET_CODE_NAME(T907.C901_DELIVERY_CARRIER) DELIVERYCARRIER "); //
	    sbQuery
	    .append(" ,t907.C106_ADDRESS_ID ADDID, T907.C901_SHIP_TO SHIP_TO,T907.C907_SHIP_TO_ID SHIP_ID,t907.C907_SHIP_TO_DT SHIPDT"); //
	    }
	    if (strStrType.equals("4119")) {
	      sbQuery.append("  ,t7100.c7100_case_id EVENTID, t7103.c7103_start_date eventstartdate ");
	      sbQuery.append("  ,t7103.c7103_end_date eventenddate  ");
	      sbQuery.append("  ,get_user_name(t7100.c7100_created_by) createdby ");
	      sbQuery.append("  ,get_code_name(t7103.c901_loan_to_org) eventloanto ");
	      sbQuery
	          .append("  , DECODE(t7103.c901_loan_to,'19518',get_rep_name(t7103.c101_loan_to_id),get_user_name(t7103.c101_loan_to_id)) eventloantoname ");
	    }
	    if (strStrType.equals("400088")) {// Consignment Item
	      sbQuery
	          .append(" ,NVL(GET_PART_PRICE (t526.c205_part_number_id, 'C'),'0') consprice, T526.C526_STATUS_FL statusid   ");

	      sbQuery
	          .append(" , get_set_id_from_part(T526.C205_PART_NUMBER_ID) SYSTEM_ID, GET_DMD_SHEET_OWNER (T526.C205_PART_NUMBER_ID) SHEET_OWNER_ID ");
	      sbQuery
	          .append(" , get_latest_log_comments(c7100_case_id,4000760) LASTCOMMENTS  ,t525.c525_request_for_id fieldsalesid");
	      sbQuery
	          .append(" ,DECODE(T7104.C7104_RETN_FLAG,'Y', t7104.c7104_item_qty,NULL) retnqty , T7104.C7104_RETN_FLAG RETFLAG ");
	    }
	    if (strStrType.equals("400087")) {// Consignment Set
	      // 26240267: Consignment Set
	      sbQuery.append(" , get_latest_log_comments(c7100_case_id,26240283) LASTCOMMENTS");
	      sbQuery
	          .append(" , GET_REP_NAME(get_rep_id_by_ass_id(t7100.c7100_created_by)) REPFROMASSOCREP");
	    }
	    sbQuery
	        .append(" FROM t7100_case_information t7100, t7104_case_set_information t7104, t526_product_request_detail t526 ");
	    sbQuery
	        .append(" , t7103_schedule_info t7103, t901_code_lookup t901, t525_product_request t525 ");
	    if (strStrType.equals("400088")) {
	    	sbQuery
	        .append(" ,T907_SHIPPING_INFO T907");
	    }

	    if (strStrType.equals("400088") && !strSystemID.equals("") && !strSystemID.equals("0")) {
	      sbQuery.append(" , t4011_group_detail t4011, t4010_group t4010, t207_set_master t207 ");
	    }

	    // Filter Condition to fetch record based on Filters
	    // 4127 strStrType for pending loaner approval
	    if (!strSalesFilter.toString().equals("") && strStrType.equals("4127")) {
	      sbQuery
	          .append(", (select distinct v700.rep_id from v700_territory_mapping_detail v700 where ");
	      sbQuery.append(strSalesFilter);
	      sbQuery.append(" ) v700 ");
	    }

	    // Filter Condition to fetch record based on Filters
	    // 400087 strStrType for pending set approval and 400088 strStrType for pending item request
	    if (!strSalesFilter.toString().equals("")
	        && (strStrType.equals("400087") || strStrType.equals("400088"))) {

	      sbQuery
	          .append(" , (select distinct v700.d_id from v700_territory_mapping_detail v700 where ");
	      sbQuery.append(strSalesFilter);
	      sbQuery.append(" ) v700 ");
	    }

	    sbQuery.append(" WHERE t7104.c7100_case_info_id = t7100.c7100_case_info_id ");
	    sbQuery
	        .append(" AND t7104.c526_product_request_detail_id  = t526.c526_product_request_detail_id ");
	    sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
	    sbQuery.append(" AND t7104.c526_product_request_detail_id IS NOT NULL ");
	    // sbQuery.append(" AND T526.C205_PART_NUMBER_ID = sheet.c205_part_number_id (+)");
	    sbQuery.append(" AND t7103.c7100_case_info_id (+) = t7100.c7100_case_info_id ");
	    
	    if (strStrType.equals("400088")) {
	      sbQuery.append(" and T525.C525_PRODUCT_REQUEST_ID = T907.C525_PRODUCT_REQUEST_ID  ");
	      sbQuery.append(" and t526. c526_product_request_detail_id = T907.C907_REF_ID  ");
	      sbQuery.append(" and t907.c907_void_fl IS NULL ");
	    }
	    if (!strFieldSales.equals("") && !strFieldSales.equals("0")) {
	      sbQuery.append(" AND t7100.c701_distributor_id ='" + strFieldSales + "'");
	    }
	    sbQuery.append(" AND t526.c901_request_type IN (");// 9110-IH Loaner Item
	    // sbQuery.append(strStrType.equals("4119")?" (4119,9110) ":"(4127,400087)");//9110-IH Loaner
	    // Item
	    sbQuery.append(strReqType);
	    sbQuery.append(")  AND t7104.c7104_shipped_del_fl IS NULL ");
	    sbQuery.append("  AND t7100.c901_case_status IN ");
	    sbQuery.append(strStrType.equals("4119") ? " (19524) " : " (11091,19524) ");
	    // sbQuery.append("  AND t7100.c901_type  = 1006503 "); // CASE
	    sbQuery.append("  AND t526.c526_status_fl  IN (");
	    sbQuery.append(strStatus);
	    sbQuery.append(")  AND t526.c526_void_fl    IS NULL ");
	    sbQuery.append("  AND t7100.c7100_void_fl  IS NULL ");
	    sbQuery.append("  AND t7104.c7104_void_fl  IS NULL ");
	    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
	    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
	    sbQuery.append(") ");
	    sbQuery.append("  AND t7100.c901_type = t901.c901_code_id ");

	    if (!strSalesFilter.toString().equals("") && strStrType.equals("4127")) {
	      sbQuery.append("AND (t7100.c901_type=1006504 OR t525.C703_SALES_REP_ID  =  v700.rep_id)");
	    }
	    if (!strRequestType.equals("0") && !strRequestType.equals("")) {
	      sbQuery.append(" AND t7100.c901_type IN (" + strRequestType + ")");
	    }
	    // below filter condition should be added only for Loaner not for consignment.
	    if (!strFilterCondition.equals("") && !strStrType.equals("400087")) {
	      sbQuery
	          .append(" AND DECODE('"
	              + strRequestType
	              + "','1006507',t7100.C701_DISTRIBUTOR_ID,'1006506',t7100.C701_DISTRIBUTOR_ID,t7100.c703_sales_rep_id ) IN (");
	      sbQuery
	          .append(" SELECT	DISTINCT DECODE('"
	              + strRequestType
	              + "','1006507',D_ID,'1006506',D_ID, REP_ID) RID FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
	      sbQuery.append(" WHERE  REP_ID IS NOT NULL  AND ");
	      sbQuery.append(strFilterCondition);
	      sbQuery.append(")");
	    }
	    if (strStrType.equals("4127")) {
	      sbQuery.append(" UNION ");
	      sbQuery
	          .append(" SELECT t526.c526_product_request_detail_id LNREQID, '' CASEID, NULL REQDATE ,0 seq, t526.c207_set_id setid ");
	      sbQuery
	          .append(" ,get_set_name (t526.c207_set_id) setname,1 qty, null approve_qty,DECODE(T526.C526_STATUS_FL,'2','Pending AD Approval','5','Pending Approval',");
	      sbQuery
	          .append(" 10,'Open',20,'Allocated')STATUS, get_rep_name (t525.c703_sales_rep_id) repname,t525.c703_sales_rep_id repid, GET_REP_PHONE (t525.c703_sales_rep_id) repphone,");
	      sbQuery
	          .append(" get_account_name(t525.c704_account_id) acctname, t525.c525_surgery_date surgerydate, t526.c526_required_date requireddate ");
	      sbQuery
	          .append(" ,get_ad_rep_id(t525.c703_sales_rep_id) adid , t526.c525_product_request_id requestid ,get_system_from_setid(t526.c207_set_id) sysid");
	      sbQuery
	          .append("  ,NULL eventname,NULL caseinfoid, t526.c901_request_type reqtype, get_code_name(t526.c901_request_type) reqnm, get_rep_name (t525.c703_ass_rep_id) assocrepnm ");
	      sbQuery
	          .append(" ,t526.c205_part_number_id pnum,GET_PARTNUM_DESC (T526.C205_PART_NUMBER_ID) pdesc, null fieldSales ");
	      if (strStrType.equals("400088")) {
	      sbQuery
	      .append(" ,null DELIVERYMODE , null DELIVERYCARRIER ,null ADDID, null SHIP_TO,null SHIP_ID,null SHIPDT"); // shipping details for item consignment     
	      }
	      if (strStrType.equals("4119")) {
	        sbQuery
	            .append("  ,NULL EVENTID ,NULL eventstartdate ,NULL eventenddate  ,NULL createdby  , NULL eventloanto  , NULL eventLOANTONAME");
	      }
	      sbQuery.append(" FROM t525_product_request t525, t526_product_request_detail t526 ");
	      if (!strSalesFilter.toString().equals("")) {
	        sbQuery
	            .append(", (select distinct v700.rep_id from v700_territory_mapping_detail v700 where ");
	        sbQuery.append(strSalesFilter);
	        sbQuery.append(" ) v700");
	      }

	      sbQuery
	          .append(" WHERE t525.c525_product_request_id             = t526.c525_product_request_id ");
	      sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
	      sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
	      sbQuery.append(") ");
	      sbQuery.append(" AND t525.C1900_company_id = t526.C1900_company_id ");
	      sbQuery.append("  AND t526.c901_request_type IN (");
	      sbQuery.append(strReqType);
	      sbQuery.append(") AND t526.c526_status_fl IN (");
	      sbQuery.append(strStatus);
	      sbQuery.append(") ");
	      if (!strSalesFilter.toString().equals("")) {
	        sbQuery.append(" AND  t525.C703_SALES_REP_ID = v700.rep_id ");
	      }

	      sbQuery
	          .append("AND t526.c526_product_request_detail_id NOT IN ( SELECT t7104.c526_product_request_detail_id ");
	      sbQuery.append(" FROM t7100_case_information t7100, t7104_case_set_information t7104 ");

	      sbQuery.append(" WHERE t7104.c7100_case_info_id              = t7100.c7100_case_info_id ");
	      if (!strFieldSales.equals("") && !strFieldSales.equals("0")) {
	        sbQuery.append(" AND t525.c525_request_for_id ='" + strFieldSales + "'");
	      }



	      sbQuery.append(" AND t7104.c526_product_request_detail_id IS NOT NULL ");
	      sbQuery.append("  AND t7100.c901_case_status IN (11091,19524) ) "); // active case/event
	                                                                          // loaner
	      // sbQuery.append(" AND t7100.c901_type                       = 1006503) "); // CASE
	      sbQuery.append(" AND t526.c526_void_fl                       IS NULL ");
	      sbQuery.append(" AND t525.c525_void_fl                       IS NULL ");

	    }

	    if (!strSalesRepID.equals("") && !strSalesRepID.equals("0") && strStrType.equals("400088")) {
	      sbQuery.append(" AND t7100.c7100_created_by ='" + strSalesRepID + "'");
	    }
	    if (strStrType.equals("400088") && !strSystemID.equals("") && !strSystemID.equals("0")) {

	      sbQuery.append(" AND  t526.c205_part_number_id = t4011.c205_part_number_id ");
	      sbQuery.append(" AND t4010.c4010_group_id = t4011.c4010_group_id ");
	      sbQuery.append(" AND t207.c207_set_id = t4010.c207_set_id ");
	      sbQuery.append(" AND t207.c901_set_grp_type = 1600 ");
	      sbQuery.append(" AND t4010.c4010_void_fl IS NULL ");
	      sbQuery.append(" AND t4010.c901_type = 40045 ");
	      sbQuery.append(" AND t4011.c901_part_pricing_type = 52080 ");
	      sbQuery.append(" AND t4010.c207_set_id ='" + strSystemID + "'");
	    }


	    // below filter condition should be added only for Loaner not for consignment.
	    if (!strFilterCondition.equals("") && !strStrType.equals("400087")) {
	      sbQuery
	          .append(" AND DECODE('"
	              + strRequestType
	              + "','1006507',t525.c525_request_for_id,'1006506',t525.c525_request_for_id,t525.c703_sales_rep_id  ) IN (");
	      sbQuery
	          .append(" SELECT	DISTINCT  DECODE('"
	              + strRequestType
	              + "','1006507',D_ID,'1006506',D_ID, REP_ID) RID FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
	      sbQuery.append(" WHERE  REP_ID IS NOT NULL  AND ");
	      sbQuery.append(strFilterCondition);
	      sbQuery.append(")");
	    }

	    if (!strSalesFilter.toString().equals("")
	        && (strStrType.equals("400087") || strStrType.equals("400088"))) {
	      sbQuery.append(" AND  t7100.c701_distributor_id = v700.d_id ");
	    }
	    if (strStrType.equals("400088")) {
	      sbQuery.append(") LNAPPR ) ");
	    } else {
	      sbQuery.append(" )  ) ");
	    }

	    sbQuery.append(" where LNREQID IS NOT NULL ");

	    if (!strSheetOwner.equals("") && !strSheetOwner.equals("0")) {
	      sbQuery.append("AND lnappr.SHEET_OWNER_ID = " + strSheetOwner);
	    }

	    if (!strRequestId.equals("")) {
	      sbQuery.append(" AND requestid = " + strRequestId);
	    }
	    if (!strCaseInfoId.equals("0") && !strCaseInfoId.equals("")) {
	      sbQuery.append(" AND caseinfoid = " + strCaseInfoId);
	    }
	    if (!strStartDt.equals("") && !strEndDt.equals("")) {
	      sbQuery.append(" And Trunc (DECODE('" + strStrType
	          + "','400087',REQDATE,'400088',REQDATE,requireddate)) >= Trunc (To_Date('" + strStartDt
	          + "','" + strApplDateFmt + "')) ");
	      sbQuery.append(" And Trunc (DECODE('" + strStrType
	          + "','400087',REQDATE,'400088',REQDATE,requireddate)) <= Trunc (TO_DATE('" + strEndDt
	          + "','" + strApplDateFmt + "')) ");
	    }
	    sbQuery.append(" ORDER BY requireddate,requestid,seq,setid ");
	    log.debug("fetchPendingApprlLoanerReqDt query is   " + sbQuery.toString());
	    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
	    return alReturn;
	  }


  /**
   * This method used to fetch Pending Item request details to approval process
   * 
   * @param hmParam
   * return ArrayList
   * @throws AppError
   */

  public ArrayList fetchPendingApprlItemReqDtl(HashMap hmParam) throws AppError {

    ArrayList alReturn = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strReqType = "4127";
    String strStatus = "5";

    String strFilterCondition = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
    String strStrType = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("EVENTNAME"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("SESSDATEFMT"));
    String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDT"));
    String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDT"));
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPE"));
    String strReqStatus = GmCommonClass.parseNull((String) hmParam.get("REQ_STATUS"));
    String strSalesRepID = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
    String strPDApprReq = GmCommonClass.parseNull((String) hmParam.get("PDREJLNREQAPPR"));
    String strFieldSales = GmCommonClass.parseNull((String) hmParam.get("FIELDSALES"));
    String strSystemID = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
    String strSheetOwner = GmCommonClass.parseNull((String) hmParam.get("SHEETOWNER"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("Filter"));

    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_REQ_APPVL"));/*
                                                                                              * LOANER_REQ_APPVL
                                                                                              * - To
                                                                                              * fetch
                                                                                              * the
                                                                                              * records
                                                                                              * for
                                                                                              * EDC
                                                                                              * entity
                                                                                              * countries
                                                                                              * based
                                                                                              * on
                                                                                              * plant
                                                                                              */
   
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();


    if (strStrType.equals("save") || strStrType.equals("load") || strStrType.equals("appwithra")) {
      strStrType = GmCommonClass.parseNull((String) hmParam.get("STRTXNTYPE"));
    }

    log.debug("the value inside strStrType after *** " + strStrType);
    if (!strReqStatus.equals("")) {
      strStatus = strReqStatus;
    }

    if (strStrType.equals("4119")) {
      strReqType = "4119,9110";
    }
    if (strStrType.equals("400088")) {// ConsignmentItem
      strReqType = "400088";
      strRequestType = "1006507";
    }
    if (strStrType.equals("400087")) {// Consignment Set
      strReqType = "400087";
      strRequestType = "1006506";
    }
    // below condition works only for 'Pending PD Loaner Request' Screen
    if (!strPDApprReq.equals("")) {
      strReqType = "4127";
      strStatus = "7";
    }
    if (strStrType.equals("400088")) {
      sbQuery
          .append(" SELECT get_set_name(lnappr.SYSTEM_ID) systemname, get_user_name(lnappr.SHEET_OWNER_ID) SHEET_OWNER, lnappr.* FROM( ");
    } else {
      sbQuery.append(" SELECT * FROM ( ");
    }
    sbQuery
        .append(" ( SELECT t7104.c526_product_request_detail_id LNREQID, t7100.c7100_case_id CASEID,  t7100.c7100_created_date REQDATE, ");
    sbQuery
        .append(" decode(T7104.C207_Set_Id,null,1,0) seq, Nvl(T7104.C207_Set_Id,T7104.C205_Part_Number_Id) setid,REPLACE( decode(T7104.C207_Set_Id,");
    sbQuery
        .append(" null,get_partnum_desc (T7104.c205_part_number_id),get_set_name (t7104.c207_set_id)),'\"','\\\"') setname,");
    sbQuery
        .append(" DECODE(T7104.C207_Set_Id,null,t7104.c7104_item_qty,1) qty, NVL(T526.C526_QTY_REQUESTED,t7104.c7104_item_qty)approve_qty ");

    sbQuery
        .append(" ,DECODE(T526.C526_STATUS_FL,'2','Pending AD Approval','5','Pending Approval',10,'Open',20,'Allocated')STATUS ,");
    sbQuery
        .append(" DECODE("
            + strStrType
            + ", 400088, get_user_name(t7100.c7100_created_by),400087,get_user_name(t7100.c7100_created_by) ,get_rep_name (t7100.c703_sales_rep_id))  repname, ");
    sbQuery
        .append("  nvl(t7100.c703_sales_rep_id, get_rep_id_from_user_id(t7100.c7100_created_by)) repid ,GET_REP_PHONE (t7100.c703_sales_rep_id) repphone, ");
    sbQuery
        .append("  get_account_name ( t7100.c704_account_id) acctname, t7100.c7100_surgery_date surgerydate ,DECODE(t526.c901_request_type, '400088',t526.C526_PLANNED_SHIP_DT,'400087',t526.C526_PLANNED_SHIP_DT,t526.c526_required_date)  requireddate ");
    sbQuery
        .append(" ,get_ad_rep_id(t7100.c703_sales_rep_id) adid ,t526.c525_product_request_id requestid ,get_system_from_setid(t7104.c207_set_id) sysid");
    sbQuery
        .append(" ,t7103.c7103_name eventname ,t7103.c7100_case_info_id caseinfoid,t526.c901_request_type reqtype, t901.c901_code_nm reqnm, get_rep_name (t525.c703_ass_rep_id) assocrepnm ");
    sbQuery
        .append(" ,t526.c205_part_number_id pnum,REPLACE( GET_PARTNUM_DESC (T526.C205_PART_NUMBER_ID), '\"', '\\\"' ) pdesc, GET_DISTRIBUTOR_NAME(t525.c525_request_for_id) fieldSales ");
    if (strStrType.equals("400088")) {// Consignment Item
    sbQuery
    .append(" ,GET_CODE_NAME(T907.C901_DELIVERY_MODE) DELIVERYMODE , GET_CODE_NAME(T907.C901_DELIVERY_CARRIER) DELIVERYCARRIER "); //
    sbQuery
    .append(" ,t907.C106_ADDRESS_ID ADDID, T907.C901_SHIP_TO SHIP_TO,T907.C907_SHIP_TO_ID SHIP_ID,t907.C907_SHIP_TO_DT SHIPDT"); //
    }
    if (strStrType.equals("4119")) {
      sbQuery.append("  ,t7100.c7100_case_id EVENTID, t7103.c7103_start_date eventstartdate ");
      sbQuery.append("  ,t7103.c7103_end_date eventenddate  ");
      sbQuery.append("  ,get_user_name(t7100.c7100_created_by) createdby ");
      sbQuery.append("  ,get_code_name(t7103.c901_loan_to_org) eventloanto ");
      sbQuery
          .append("  , DECODE(t7103.c901_loan_to,'19518',get_rep_name(t7103.c101_loan_to_id),get_user_name(t7103.c101_loan_to_id)) eventloantoname ");
    }
    if (strStrType.equals("400088")) {// Consignment Item
      sbQuery
          .append(" ,NVL(GET_PART_PRICE (t526.c205_part_number_id, 'C'),'0') consprice, T526.C526_STATUS_FL statusid   ");

      sbQuery
          .append(" , get_set_id_from_part(T526.C205_PART_NUMBER_ID) SYSTEM_ID, GET_DMD_SHEET_OWNER (T526.C205_PART_NUMBER_ID) SHEET_OWNER_ID ");
      sbQuery
          .append(" , translate(get_latest_log_comments(c7100_case_id,4000760), chr(10)||chr(11)||chr(13)||chr(39), ' ') LASTCOMMENTS  ,t525.c525_request_for_id fieldsalesid");
      sbQuery
          .append(" ,DECODE(T7104.C7104_RETN_FLAG,'Y', t7104.c7104_item_qty,NULL) retnqty , T7104.C7104_RETN_FLAG RETFLAG ");
    }
    if (strStrType.equals("400087")) {// Consignment Set
      // 26240267: Consignment Set
      sbQuery.append(" , get_latest_log_comments(c7100_case_id,26240283) LASTCOMMENTS");
      sbQuery
          .append(" , GET_REP_NAME(get_rep_id_by_ass_id(t7100.c7100_created_by)) REPFROMASSOCREP");
    }
    sbQuery
        .append(" FROM t7100_case_information t7100, t7104_case_set_information t7104, t526_product_request_detail t526 ");
    sbQuery
        .append(" , t7103_schedule_info t7103, t901_code_lookup t901, t525_product_request t525 ");
    if (strStrType.equals("400088")) {
    	sbQuery
        .append(" ,T907_SHIPPING_INFO T907");
    }

    if (strStrType.equals("400088") && !strSystemID.equals("") && !strSystemID.equals("0")) {
      sbQuery.append(" , t4011_group_detail t4011, t4010_group t4010, t207_set_master t207 ");
    }

    // Filter Condition to fetch record based on Filters
    // 4127 strStrType for pending loaner approval
    if (!strSalesFilter.toString().equals("") && strStrType.equals("4127")) {
      sbQuery
          .append(", (select distinct v700.rep_id from v700_territory_mapping_detail v700 where ");
      sbQuery.append(strSalesFilter);
      sbQuery.append(" ) v700 ");
    }

    // Filter Condition to fetch record based on Filters
    // 400087 strStrType for pending set approval and 400088 strStrType for pending item request
    if (!strSalesFilter.toString().equals("")
        && (strStrType.equals("400087") || strStrType.equals("400088"))) {

      sbQuery
          .append(" , (select distinct v700.d_id from v700_territory_mapping_detail v700 where ");
      sbQuery.append(strSalesFilter);
      sbQuery.append(" ) v700 ");
    }

    sbQuery.append(" WHERE t7104.c7100_case_info_id = t7100.c7100_case_info_id ");
    sbQuery
        .append(" AND t7104.c526_product_request_detail_id  = t526.c526_product_request_detail_id ");
    sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    sbQuery.append(" AND t7104.c526_product_request_detail_id IS NOT NULL ");
    // sbQuery.append(" AND T526.C205_PART_NUMBER_ID = sheet.c205_part_number_id (+)");
    sbQuery.append(" AND t7103.c7100_case_info_id (+) = t7100.c7100_case_info_id ");
    
    if (strStrType.equals("400088")) {
      sbQuery.append(" and T525.C525_PRODUCT_REQUEST_ID = T907.C525_PRODUCT_REQUEST_ID  ");
      sbQuery.append(" and t526. c526_product_request_detail_id = T907.C907_REF_ID  ");
      sbQuery.append(" and t907.c907_void_fl IS NULL ");
    }
    if (!strFieldSales.equals("") && !strFieldSales.equals("0")) {
      sbQuery.append(" AND t7100.c701_distributor_id ='" + strFieldSales + "'");
    }
    sbQuery.append(" AND t526.c901_request_type IN (");// 9110-IH Loaner Item
    // sbQuery.append(strStrType.equals("4119")?" (4119,9110) ":"(4127,400087)");//9110-IH Loaner
    // Item
    sbQuery.append(strReqType);
    sbQuery.append(")  AND t7104.c7104_shipped_del_fl IS NULL ");
    sbQuery.append("  AND t7100.c901_case_status IN ");
    sbQuery.append(strStrType.equals("4119") ? " (19524) " : " (11091,19524) ");
    // sbQuery.append("  AND t7100.c901_type  = 1006503 "); // CASE
    sbQuery.append("  AND t526.c526_status_fl  IN (");
    sbQuery.append(strStatus);
    sbQuery.append(")  AND t526.c526_void_fl    IS NULL ");
    sbQuery.append("  AND t7100.c7100_void_fl  IS NULL ");
    sbQuery.append("  AND t7104.c7104_void_fl  IS NULL ");
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    sbQuery.append("  AND t7100.c901_type = t901.c901_code_id ");

    if (!strSalesFilter.toString().equals("") && strStrType.equals("4127")) {
      sbQuery.append("AND (t7100.c901_type=1006504 OR t525.C703_SALES_REP_ID  =  v700.rep_id)");
    }
    if (!strRequestType.equals("0") && !strRequestType.equals("")) {
      sbQuery.append(" AND t7100.c901_type IN (" + strRequestType + ")");
    }
    // below filter condition should be added only for Loaner not for consignment.
    if (!strFilterCondition.equals("") && !strStrType.equals("400087")) {
      sbQuery
          .append(" AND DECODE('"
              + strRequestType
              + "','1006507',t7100.C701_DISTRIBUTOR_ID,'1006506',t7100.C701_DISTRIBUTOR_ID,t7100.c703_sales_rep_id ) IN (");
      sbQuery
          .append(" SELECT	DISTINCT DECODE('"
              + strRequestType
              + "','1006507',D_ID,'1006506',D_ID, REP_ID) RID FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery.append(" WHERE  REP_ID IS NOT NULL  AND ");
      sbQuery.append(strFilterCondition);
      sbQuery.append(")");
    }
    if (strStrType.equals("4127")) {
      sbQuery.append(" UNION ");
      sbQuery
          .append(" SELECT t526.c526_product_request_detail_id LNREQID, '' CASEID, NULL REQDATE ,0 seq, t526.c207_set_id setid ");
      sbQuery
          .append(" ,get_set_name (t526.c207_set_id) setname,1 qty, null approve_qty,DECODE(T526.C526_STATUS_FL,'2','Pending AD Approval','5','Pending Approval',");
      sbQuery
          .append(" 10,'Open',20,'Allocated')STATUS, get_rep_name (t525.c703_sales_rep_id) repname,t525.c703_sales_rep_id repid, GET_REP_PHONE (t525.c703_sales_rep_id) repphone,");
      sbQuery
          .append(" get_account_name(t525.c704_account_id) acctname, t525.c525_surgery_date surgerydate, t526.c526_required_date requireddate ");
      sbQuery
          .append(" ,get_ad_rep_id(t525.c703_sales_rep_id) adid , t526.c525_product_request_id requestid ,get_system_from_setid(t526.c207_set_id) sysid");
      sbQuery
          .append("  ,NULL eventname,NULL caseinfoid, t526.c901_request_type reqtype, get_code_name(t526.c901_request_type) reqnm, get_rep_name (t525.c703_ass_rep_id) assocrepnm ");
      sbQuery
          .append(" ,t526.c205_part_number_id pnum,GET_PARTNUM_DESC (T526.C205_PART_NUMBER_ID) pdesc, null fieldSales ");
      if (strStrType.equals("400088")) {
      sbQuery
      .append(" ,null DELIVERYMODE , null DELIVERYCARRIER ,null ADDID, null SHIP_TO,null SHIP_ID,null SHIPDT"); // shipping details for item consignment     
      }
      if (strStrType.equals("4119")) {
        sbQuery
            .append("  ,NULL EVENTID ,NULL eventstartdate ,NULL eventenddate  ,NULL createdby  , NULL eventloanto  , NULL eventLOANTONAME");
      }
      sbQuery.append(" FROM t525_product_request t525, t526_product_request_detail t526 ");
      if (!strSalesFilter.toString().equals("")) {
        sbQuery
            .append(", (select distinct v700.rep_id from v700_territory_mapping_detail v700 where ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" ) v700");
      }

      sbQuery
          .append(" WHERE t525.c525_product_request_id             = t526.c525_product_request_id ");
      sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
      sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
      sbQuery.append(") ");
      sbQuery.append(" AND t525.C1900_company_id = t526.C1900_company_id ");
      sbQuery.append("  AND t526.c901_request_type IN (");
      sbQuery.append(strReqType);
      sbQuery.append(") AND t526.c526_status_fl IN (");
      sbQuery.append(strStatus);
      sbQuery.append(") ");
      if (!strSalesFilter.toString().equals("")) {
        sbQuery.append(" AND  t525.C703_SALES_REP_ID = v700.rep_id ");
      }

      sbQuery
          .append("AND t526.c526_product_request_detail_id NOT IN ( SELECT t7104.c526_product_request_detail_id ");
      sbQuery.append(" FROM t7100_case_information t7100, t7104_case_set_information t7104 ");

      sbQuery.append(" WHERE t7104.c7100_case_info_id              = t7100.c7100_case_info_id ");
      if (!strFieldSales.equals("") && !strFieldSales.equals("0")) {
        sbQuery.append(" AND t525.c525_request_for_id ='" + strFieldSales + "'");
      }



      sbQuery.append(" AND t7104.c526_product_request_detail_id IS NOT NULL ");
      sbQuery.append("  AND t7100.c901_case_status IN (11091,19524) ) "); // active case/event
                                                                          // loaner
      // sbQuery.append(" AND t7100.c901_type                       = 1006503) "); // CASE
      sbQuery.append(" AND t526.c526_void_fl                       IS NULL ");
      sbQuery.append(" AND t525.c525_void_fl                       IS NULL ");

    }

    if (!strSalesRepID.equals("") && !strSalesRepID.equals("0") && strStrType.equals("400088")) {
      sbQuery.append(" AND t7100.c7100_created_by ='" + strSalesRepID + "'");
    }
    if (strStrType.equals("400088") && !strSystemID.equals("") && !strSystemID.equals("0")) {

      sbQuery.append(" AND  t526.c205_part_number_id = t4011.c205_part_number_id ");
      sbQuery.append(" AND t4010.c4010_group_id = t4011.c4010_group_id ");
      sbQuery.append(" AND t207.c207_set_id = t4010.c207_set_id ");
      sbQuery.append(" AND t207.c901_set_grp_type = 1600 ");
      sbQuery.append(" AND t4010.c4010_void_fl IS NULL ");
      sbQuery.append(" AND t4010.c901_type = 40045 ");
      sbQuery.append(" AND t4011.c901_part_pricing_type = 52080 ");
      sbQuery.append(" AND t4010.c207_set_id ='" + strSystemID + "'");
    }


    // below filter condition should be added only for Loaner not for consignment.
    if (!strFilterCondition.equals("") && !strStrType.equals("400087")) {
      sbQuery
          .append(" AND DECODE('"
              + strRequestType
              + "','1006507',t525.c525_request_for_id,'1006506',t525.c525_request_for_id,t525.c703_sales_rep_id  ) IN (");
      sbQuery
          .append(" SELECT	DISTINCT  DECODE('"
              + strRequestType
              + "','1006507',D_ID,'1006506',D_ID, REP_ID) RID FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery.append(" WHERE  REP_ID IS NOT NULL  AND ");
      sbQuery.append(strFilterCondition);
      sbQuery.append(")");
    }

    if (!strSalesFilter.toString().equals("")
        && (strStrType.equals("400087") || strStrType.equals("400088"))) {
      sbQuery.append(" AND  t7100.c701_distributor_id = v700.d_id ");
    }
    if (strStrType.equals("400088")) {
      sbQuery.append(") LNAPPR ) ");
    } else {
      sbQuery.append(" )  ) ");
    }

    sbQuery.append(" where LNREQID IS NOT NULL ");

    if (!strSheetOwner.equals("") && !strSheetOwner.equals("0")) {
      sbQuery.append("AND lnappr.SHEET_OWNER_ID = " + strSheetOwner);
    }

    if (!strRequestId.equals("")) {
      sbQuery.append(" AND requestid = " + strRequestId);
    }
    if (!strCaseInfoId.equals("0") && !strCaseInfoId.equals("")) {
      sbQuery.append(" AND caseinfoid = " + strCaseInfoId);
    }
    if (!strStartDt.equals("") && !strEndDt.equals("")) {
      sbQuery.append(" And Trunc (DECODE('" + strStrType
          + "','400087',REQDATE,'400088',REQDATE,requireddate)) >= Trunc (To_Date('" + strStartDt
          + "','" + strApplDateFmt + "')) ");
      sbQuery.append(" And Trunc (DECODE('" + strStrType
          + "','400087',REQDATE,'400088',REQDATE,requireddate)) <= Trunc (TO_DATE('" + strEndDt
          + "','" + strApplDateFmt + "')) ");
    }
    sbQuery.append(" ORDER BY requireddate,requestid,seq,setid ");
    log.debug("fetchPendingApprlItemReqDtl query is   " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alReturn;
  }

  public HashMap saveLoanerReqAppv(HashMap hmParams) throws AppError {
    log.debug("========hmParams=========" + hmParams);
    String strProdReqId = "";
    String strPrReqDetLitePnum = "";
    String strProdReqDetId = "";
    HashMap hmReturn = new HashMap();
    String strReqIds = GmCommonClass.parseNull((String) hmParams.get("STRREQIDS"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strNextApprStatus = GmCommonClass.parseNull((String) hmParams.get("NEXTAPPRSTATUS"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_loaner_allocation.gm_sav_loaner_req_appv", 6);
    gmDBManager.setString(1, strReqIds);
    gmDBManager.setString(2, strUserID);
    gmDBManager.setString(3, strNextApprStatus);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strProdReqId = GmCommonClass.parseNull(gmDBManager.getString(4));
    strPrReqDetLitePnum = GmCommonClass.parseNull(gmDBManager.getString(5));
    strProdReqDetId = GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.commit();

    hmReturn.put("PRODREQID", strProdReqId);
    hmReturn.put("PRREQDETLITEPNUM", strPrReqDetLitePnum);
    hmReturn.put("PRODREQDID", strProdReqDetId);

    return hmReturn;
  }

  public void saveRequestItemQty(String strInputString, String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_loaner_allocation.gm_sav_req_item_qty", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * Save appr req with ra.This Method is called when we Approve with Return Quantity for an Item
   * Request.
   * 
   * @param hmParam
   * @throws AppError
   */
  public String saveApprReqWithRA(HashMap hmParam) throws AppError {
    log.debug("the value inside hashMap  hmParam **** " + hmParam);
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmProcessReturnsBean gmProcessReturnBean = new GmProcessReturnsBean(getGmDataStoreVO());
    String strPartString = GmCommonClass.parseNull((String) hmParam.get("PARTSTRING"));
    String strRAQtyString = GmCommonClass.parseNull((String) hmParam.get("RAQTYSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDistId = "";
    String strPartNum = "";
    String strReturnQty = "";
    String strPartqty = "";
    String strRAqty = "";
    String strRAID = "";
    String strQty = "";
    String strid = "";
    String strRepid = "";
    String strOutputRaId = "";
    HashMap hmRADetails = new HashMap();
    ArrayList alRAMailDecide = new ArrayList();

    String strPartArray[] = strPartString.split("\\|");
    String strRAQTYArray[] = strRAQtyString.split("\\|");
    for (int i = 0; i < strPartArray.length; i++) {
      strPartqty = strPartArray[i];
      strRAqty = strRAQTYArray[i];

      String strPartSplitArray[] = strPartqty.split("\\^", 2);
      String strRAQtySplitArray[] = strRAqty.split("\\^", 2);
      for (int j = 0; j < strPartSplitArray.length; j++) {
        strid = strPartSplitArray[0];
        String strArrayID[] = strid.split("@");
        strDistId = strArrayID[0];
        if (strArrayID.length == 2) {
          strRepid = strArrayID[1];
        }
        strPartNum = strPartSplitArray[1];
        strPartNum = strPartNum.replaceAll("\\^", ",");
      }
      for (int k = 0; k < strRAQtySplitArray.length; k++) {
        strid = strRAQtySplitArray[0];
        String strArrayID[] = strid.split("@");
        strDistId = strArrayID[0];
        if (strArrayID.length == 2) {
          strRepid = strArrayID[1];
        }
        strReturnQty = strRAQtySplitArray[1];
        strReturnQty = strReturnQty.replaceAll("\\^", ",");
      }
      hmParam.put("DISTID", strDistId);
      hmParam.put("PARTID", strPartNum);
      hmParam.put("PARTQTY", strReturnQty);
      hmParam.put("TYPE", "3302");
     // hmParam.put("EMPNAME", strRepid);//REPID
      hmParam.put("REPID", strRepid);//PC-3847 Rename EMPNAME AS RepID


      hmRADetails =
          GmCommonClass.parseNullHashMap(gmProcessReturnBean.saveInitiateWithRepId(gmDBManager, hmParam,
              strUserId));//PC-3847--Call New Method saveInitiateWithRepId
      gmDBManager.commit();

      strRAID = GmCommonClass.parseNull((String) hmRADetails.get("RAID"));
      strOutputRaId = strOutputRaId + strRAID + ',';
      hmReturn = gmProcessReturnBean.loadCreditReport(strRAID, "PRINT");
      alRAMailDecide =
          GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RAINITMAILDECIDE"));
      if ((alRAMailDecide.size() > 0)) {
        hmReturn.put("EMAILFILENM", "GmRAInitialEmail");
        hmReturn.put("JASPERNAME", "/GmRAStatusEmail.jasper");
        hmReturn.put("ISCREDIT", "NOTCREDIT");
        hmReturn.put("PDFL", "TRUE");
        hmReturn.put("SUBREPORT_DIR", hmParam.get("SUBREPORT_DIR"));
        gmProcessReturnBean.sendRAStatusEmail(hmReturn);

      }

    }
    strOutputRaId = strOutputRaId.substring(0, strOutputRaId.length() - 1);
    return strOutputRaId;
  }
  
  // new method to fetch the shipping address when clicked on the address look
  // up button
  public String getShipAddress(String strShipTo, String strShipToId, Integer intAddLen) throws AppError {
    String strShipAdd = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_cm_shipping_info.get_address", 3);

    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strShipTo);
    gmDBManager.setString(3, strShipToId);
    gmDBManager.setInt(4, intAddLen);
    gmDBManager.execute();
    strShipAdd = gmDBManager.getString(1);
    log.debug("strShipAdd to display is   " + strShipAdd);
    gmDBManager.close();

    return strShipAdd;
  } // End of getShipAddress
  /**
   * fetchPendingAccItemApprlReqDtl - This method is used to fetch Pending Account Item Consignment 
   * approval requests
   * @param hmParam - HashMap
   * @return alReturn - ArrayList
   * @throws AppError
   */
  public ArrayList fetchPendingAccItemApprlReqDtl(HashMap hmParam) throws AppError {

    ArrayList alReturn = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strReqType = "";
    String strStatus = "5";
    String strRequestType ="";
    String strStrType = GmCommonClass.parseNull((String) hmParam.get("STRTXNTYPE"));
    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDT"));
    String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDT"));
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strReqStatus = GmCommonClass.parseNull((String) hmParam.get("REQ_STATUS"));

    String strSystemID = GmCommonClass.parseZero((String) hmParam.get("SYSTEMID"));

    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("Filter"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));

    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_REQ_APPVL"));/*
                                                                                              * LOANER_REQ_APPVL
                                                                                              * - To
                                                                                              * fetch
                                                                                              * the
                                                                                              * records
                                                                                              * for
                                                                                              * EDC
                                                                                              * entity
                                                                                              * countries
                                                                                              * based
                                                                                              * on
                                                                                              * plant
                                                                                              */
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    log.debug("the value inside strStrType *** " + strStrType);
    //if strReqStatus is empty then override strStatus, otherwise it will be default 5--Pending approval
    if (!strReqStatus.equals("")) {
      strStatus = strReqStatus;
    }

    if (strStrType.equals("40050")) {// Account Item Consignment
      strReqType = "40050";
      strRequestType = "107286";
    }


      sbQuery
          .append(" SELECT lnappr.* FROM( ");
   
    sbQuery
        .append(" ( SELECT t7104.c526_product_request_detail_id LNREQID, t7100.c7100_case_id CASEID,  t7100.c7100_created_date REQDATE, ");

    sbQuery
        .append(" t7104.c7104_item_qty qty, NVL(T526.C526_QTY_REQUESTED,t7104.c7104_item_qty)approve_qty ");

    sbQuery
        .append(" ,DECODE(T526.C526_STATUS_FL,'2','Pending AD Approval','5','Pending Approval',10,'Open',20,'Allocated')STATUS ,");

    sbQuery
        .append("  get_account_name (t7100.c704_account_id) acctname,C526_PLANNED_SHIP_DT  requireddate ");
    sbQuery
        .append(" ,t526.c525_product_request_id requestid ");
    sbQuery.append(" ,t526.c901_request_type reqtype, t901.c901_code_nm reqnm");
    sbQuery
        .append(" ,t526.c205_part_number_id pnum,T205.C205_PART_NUM_DESC pdesc ");

    sbQuery
    .append(" ,GET_CODE_NAME(T907.C901_DELIVERY_MODE) DELIVERYMODE , GET_CODE_NAME(T907.C901_DELIVERY_CARRIER) DELIVERYCARRIER "); //
    sbQuery
    .append(" ,t907.C106_ADDRESS_ID ADDID, T907.C901_SHIP_TO SHIP_TO,T907.C907_SHIP_TO_ID SHIP_ID,t907.C907_SHIP_TO_DT SHIPDT"); //


    sbQuery
        .append(" ,NVL(GET_PART_PRICE (t526.c205_part_number_id, 'C'),'0') consprice, T526.C526_STATUS_FL statusid,t7100.c703_sales_rep_id repid    ");

    sbQuery
        .append(" , T207.C207_SET_ID SYSTEM_ID,T207.C207_SET_NM systemname, get_user_name(t7100.c7100_created_by)REPNAME ");
    sbQuery
        .append(" , get_latest_log_comments(c7100_case_id,4000760) LASTCOMMENTS  ,t525.c525_request_for_id accountid");
    sbQuery
        .append(" ,DECODE(T7104.C7104_RETN_FLAG,'Y', t7104.c7104_item_qty,NULL) retnqty , T7104.C7104_RETN_FLAG RETFLAG ");


    sbQuery
        .append(" FROM t7100_case_information t7100, t7104_case_set_information t7104, t526_product_request_detail t526 ");
    sbQuery.append(" , t901_code_lookup t901, t525_product_request t525 ");

        sbQuery
        .append(" ,T907_SHIPPING_INFO T907, T205_PART_NUMBER T205, T207_SET_MASTER T207, t208_set_details t208");

    if (!strSystemID.equals("0")) {
      sbQuery.append(" , t4011_group_detail t4011, t4010_group t4010 ");
    }


    // Filter Condition to fetch record based on Filters
  
    if (!strSalesFilter.equals("")) {

      sbQuery
          .append(" , (select distinct v700.ac_id from v700_territory_mapping_detail v700 where ");
      sbQuery.append(strSalesFilter);
      sbQuery.append(" ) v700 ");
    }

    sbQuery.append(" WHERE t7104.c7100_case_info_id = t7100.c7100_case_info_id ");
    sbQuery
        .append(" AND t7104.c526_product_request_detail_id  = t526.c526_product_request_detail_id ");
    sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    sbQuery.append(" AND t7104.c526_product_request_detail_id IS NOT NULL ");


    sbQuery.append(" and T525.C525_PRODUCT_REQUEST_ID = T907.C525_PRODUCT_REQUEST_ID  ");
    sbQuery.append(" and t526. c526_product_request_detail_id = T907.C907_REF_ID  ");
    
    sbQuery.append(" AND T526.C205_PART_NUMBER_ID = T205.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T208.C207_SET_ID = T207.C207_SET_ID ");
    sbQuery.append(" AND T208.C205_PART_NUMBER_ID = T526.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND t207.c901_set_grp_type = 1600 ");
    sbQuery.append(" and t907.c907_void_fl IS NULL ");
   
    sbQuery.append(" AND t526.c901_request_type IN (");  
    sbQuery.append(strReqType);
    sbQuery.append(")  AND t7104.c7104_shipped_del_fl IS NULL ");
    sbQuery.append("  AND t7100.c901_case_status IN ");
    sbQuery.append(" (11091,19524) ");
   
    sbQuery.append("  AND t526.c526_status_fl  IN (");
    sbQuery.append(strStatus);
    sbQuery.append(")  AND t526.c526_void_fl    IS NULL ");
    sbQuery.append("  AND t7100.c7100_void_fl  IS NULL ");
    sbQuery.append("  AND t7104.c7104_void_fl  IS NULL ");
    sbQuery.append(" AND T207.C207_VOID_FL  IS NULL ");
    sbQuery.append(" AND T208.C208_VOID_FL  IS NULL ");
    
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    sbQuery.append("  AND t7100.c901_type = t901.c901_code_id ");
    sbQuery.append(" AND t7100.c901_type IN (" + strRequestType + ")");
    
    if (!strSystemID.equals("0")) {

      sbQuery.append(" AND  t526.c205_part_number_id = t4011.c205_part_number_id ");
      sbQuery.append(" AND t4010.c4010_group_id = t4011.c4010_group_id ");
      sbQuery.append(" AND t207.c207_set_id = t4010.c207_set_id ");
     
      sbQuery.append(" AND t4010.c4010_void_fl IS NULL ");
      sbQuery.append(" AND t4010.c901_type = 40045 ");
      sbQuery.append(" AND t4011.c901_part_pricing_type = 52080 ");
      sbQuery.append(" AND t4010.c207_set_id ='" + strSystemID + "'");
    }
    if (!strAccountID.equals("")) {
      sbQuery.append(" AND  t7100.C704_ACCOUNT_ID = '" + strAccountID + "'");
    }

    if (!strSalesFilter.equals("")) {
      sbQuery.append(" AND  t7100.C704_ACCOUNT_ID = v700.ac_id ");
    }

      sbQuery.append(") LNAPPR ) ");
  

    sbQuery.append(" where LNREQID IS NOT NULL ");


    if (!strRequestId.equals("")) {
      sbQuery.append(" AND requestid = " + strRequestId);
    }

    if (!strStartDt.equals("") && !strEndDt.equals("")) {
      sbQuery.append(" And Trunc (REQDATE) >= Trunc (To_Date('" + strStartDt + "','"
          + strApplDateFmt + "')) ");
      sbQuery.append(" And Trunc (REQDATE) <= Trunc (TO_DATE('" + strEndDt + "','" + strApplDateFmt
          + "')) ");
    }
    sbQuery.append(" ORDER BY requireddate,requestid ");
    log.debug("fetchPendingApprlLoanerReqDt query is   " + sbQuery.toString());
    alReturn = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    return alReturn;
  } //End of fetchPendingAccItemApprlReqDtl
  /**
   * updateRAFlagForReturns - This method is used to update RA flag 
   * for each request
   * @param strRequestIds - Request Ids to update(comma separated)
   * @return non return type
   * @throws AppError
   */
  public void updateRAFlagForReturns(String strRequestIds,String strUserId)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmReturnsBean gmReturnBean = new GmReturnsBean(getGmDataStoreVO());
    String strReqId = "";
    String[] alReqIds = (GmCommonClass.parseNull(strRequestIds)).split(",");
    int alSize = alReqIds.length;
    for (int i = 0; i < alSize; i++) {
      strReqId = GmCommonClass.parseNull(alReqIds[i]);
      log.debug("Request ID:" + strReqId);
      gmReturnBean.initiateReturnItemConsign(gmDBManager, strReqId, "Y", strUserId);
    }
    gmDBManager.commit();
  }//End of updateRAFlagForReturns
}
