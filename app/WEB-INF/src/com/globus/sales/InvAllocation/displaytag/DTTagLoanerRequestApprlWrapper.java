package com.globus.sales.InvAllocation.displaytag;

import java.util.HashMap;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import java.util.Date;
import com.globus.accounts.physicalaudit.forms.GmTagReportForm;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.InvAllocation.forms.GmLoanerRequestApprovalForm;

import javax.servlet.jsp.PageContext;

public class DTTagLoanerRequestApprlWrapper extends TableDecorator{
	 private HashMap hmRow ;
	 private String strAccess = "";
	 private String strForeColor ="<span style=\"color:red;\">";
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		GmLoanerRequestApprovalForm gmLoanerRequestApprovalForm ;
		public String getREQINFO() {
			hmRow = (HashMap) this.getCurrentRowObject();
			gmLoanerRequestApprovalForm = (GmLoanerRequestApprovalForm) getPageContext().getAttribute("frmLoanerRequestApproval", PageContext.REQUEST_SCOPE);
			strAccess = gmLoanerRequestApprovalForm.getAppvlAccessFl();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			String strLnReqID = GmCommonClass.parseNull((String) hmRow.get("LNREQID"));
			String strSetID = GmCommonClass.parseNull((String) hmRow.get("SETID"));
			String strSysId = GmCommonClass.parseNull((String) hmRow.get("SYSID"));
		    String strAdId = GmCommonClass.parseNull((String) hmRow.get("ADID"));
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			StringBuffer sbHtml = new StringBuffer();
			if (strAccess.equals("Y"))
			{
				sbHtml.append("<input type=\"checkbox\" value=\"" + strLnReqID + "\" onClick=\"fnSelectReqID(this);\">");
				sbHtml.append("	<img src='/images/s.gif' alt='Sales Report' width='16' height='17' id='imgEdit' style='cursor:hand' title='View Sales Report' onclick=\"fnSalesReport('"+strSetID+"','"+strAdId+"','"+strSysId+"')\" />");
			}
			if(shippingDt.equals(strTodaysDt))
			{
			sbHtml.append(" "+strForeColor+strLnReqID+"</span>");
			}else
			{
			sbHtml.append(" "+strLnReqID);
			}
			return sbHtml.toString();
		}
		public String getREQUESTID(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			String strRequestId = GmCommonClass.parseNull((String) hmRow.get("REQUESTID"));
			if(shippingDt.equals(strTodaysDt))
			{
				strRequestId = strForeColor+strRequestId+"</span>";
			}
			return strRequestId;
		}
		public String getCASEID(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			String strCaseId = GmCommonClass.parseNull((String) hmRow.get("CASEID"));
			if(shippingDt.equals(strTodaysDt))
			{
				strCaseId = strForeColor+strCaseId+"</span>";
			}
			return strCaseId;
		}
		public String getSETID(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			String strSetId = GmCommonClass.parseNull((String) hmRow.get("SETID"));
			if(shippingDt.equals(strTodaysDt))
			{
				strSetId = strForeColor+strSetId+"</span>";
			}
			return strSetId;
		}
		public String getSETNAME(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			String strSetNm = GmCommonClass.parseNull((String) hmRow.get("SETNAME"));
			if(shippingDt.equals(strTodaysDt))
			{
				strSetNm = strForeColor+strSetNm+"</span>";
			}
			return strSetNm;
		}
		public String getREPNAME(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			String strRepNm = GmCommonClass.parseNull((String) hmRow.get("REPNAME"));
			if(shippingDt.equals(strTodaysDt))
			{
				strRepNm = strForeColor+strRepNm+"</span>";
			}
			return strRepNm;
		}
		public String getREPPHONE(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			String strRepPhone = GmCommonClass.parseNull((String) hmRow.get("REPPHONE"));
			if(shippingDt.equals(strTodaysDt))
			{
				strRepPhone = strForeColor+strRepPhone+"</span>";
			}
			return strRepPhone;
		}
		public String getACCTNAME(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			String strAccNm = GmCommonClass.parseNull((String) hmRow.get("ACCTNAME"));
			if(shippingDt.equals(strTodaysDt))
			{
				strAccNm = strForeColor+strAccNm+"</span>";
			}
			return strAccNm;
		}
		public String getSURGERYDATE(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			Date surgDt = (Date)hmRow.get("SURGERYDATE");
			String strSurgDt = sdFormat.format(surgDt);
			if(shippingDt.equals(strTodaysDt))
			{
				strSurgDt = strForeColor+strSurgDt+"</span>";
			}
			return strSurgDt;
		}
		public String getREQUIREDDATE(){
			hmRow = (HashMap) this.getCurrentRowObject();
			String strApplDtFmt = (String)getPageContext().getSession().getAttribute("strSessApplDateFmt");
			String strTodaysDt = (String)getPageContext().getSession().getAttribute("strSessTodaysDate");
			Date dtShipDate= (Date)(hmRow.get("REQUIREDDATE"));
			SimpleDateFormat sdFormat = new SimpleDateFormat(strApplDtFmt);
			String shippingDt = sdFormat.format(dtShipDate);
			if(shippingDt.equals(strTodaysDt))
			{
				shippingDt = strForeColor+shippingDt+"</span>";
			}
			return shippingDt;
		}
}