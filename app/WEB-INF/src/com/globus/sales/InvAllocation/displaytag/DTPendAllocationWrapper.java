package com.globus.sales.InvAllocation.displaytag;

import java.util.HashMap;
import java.util.Date;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTPendAllocationWrapper extends TableDecorator{
   	 private DynaBean db;	 
   	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code
	public String getCASE_ID(){
		db = (DynaBean) this.getCurrentRowObject();
		
		String strSetID =  GmCommonClass.parseNull((String) db.get("SETID"));
		String strCaseID =  GmCommonClass.parseNull((String) db.get("CASE_ID"));
		String strSetName = GmCommonClass.parseNull((String) db.get("SETNAME"));
		String strCaseDistId = GmCommonClass.parseNull((String) db.get("CASEDISTID"));
		String strCaseSetId = GmCommonClass.parseNull(db.get("CASESETID").toString());
		String strCaseInfoId = GmCommonClass.parseNull(db.get("CASEINFOID").toString());
		Date dtLockFrom = (Date) db.get("LOCKFROMDT");
        Date dtLockTo = (Date) db.get("LOCKTODT");
        String strLockFromDt = (dtLockFrom == null)?"":GmCommonClass.parseNull(dtLockFrom.toString());
        String strLockToDt = (dtLockTo == null)?"":GmCommonClass.parseNull(dtLockTo.toString());

		
		StringBuffer strValue = new StringBuffer();
		
		strValue.append("<input type='radio' name='radSetId' id='radSetId' value='");
		strValue.append(strSetID);
		strValue.append("' onClick=\"fnCallRadio('"+strSetID+"','");
		strValue.append(strSetName+"','");
		strValue.append(strCaseDistId+"','");
		strValue.append(strCaseSetId+"','");
		strValue.append(strCaseInfoId+"','");
		strValue.append(strLockFromDt+"','");
		strValue.append(strLockToDt+"');\">");
		strValue.append(strCaseID);
		
		return strValue.toString();
	}

}
