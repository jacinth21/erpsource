package com.globus.sales.InvAllocation.displaytag;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTTagSetOverrideWrapper extends TableDecorator{
	 private HashMap hmRow ;
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		Boolean blMapFlag = false;
		public String getTAGID() {
			hmRow = (HashMap) this.getCurrentRowObject();
			String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAGID"));
			String strTagDistID = GmCommonClass.parseNull((String) hmRow.get("DISTID"));
			StringBuffer sbHtml = new StringBuffer();
			sbHtml.append("	<input type=\"radio\" value=\"" + strTAGID + "\"  name=\"rad_tagid\" onClick=\"fnSelectedTagID(this);\"> " );
			sbHtml.append("	  <img src='/images/jpg_icon.jpg' alt='Tag' width='16' height='16' id='imgEdit' style='cursor:hand' title='View Image' onclick=\"return popitup('"+strTAGID+"')\" />");
			sbHtml.append( "  " +strTAGID);
			sbHtml.append("<input type=\"hidden\" value=\"" +strTagDistID+ "\"  name=\"tagDistId_" +strTAGID+ "\" >");
			return sbHtml.toString();
		}	
		public String getDECOMSIN() {
			hmRow =  	(HashMap) this.getCurrentRowObject();  
			String strDecommission = GmCommonClass.parseNull(String.valueOf(hmRow.get("DECOMSIN_FL"))); 
			String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAGID"));
			StringBuffer sbHtml = new StringBuffer();
			if (strDecommission.equals("Y")){
				sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/hold.png' title='Decommissioned' width=35' height='38'/>");				
			}else{
				sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/success.gif' title='Active' width='30' height='35'/>");;
			}
			sbHtml.append("<input type=\"hidden\" value=\"" +strDecommission+ "\"  name=\"Decomsin_" +strTAGID+ "\" >");
			return sbHtml.toString();	
		}
		
		
		public String getLOCKINV() {
			hmRow =  	(HashMap) this.getCurrentRowObject();  
			String strLockInv = GmCommonClass.parseNull(String.valueOf(hmRow.get("LOCK_INV_FL"))); 
			String strTAGID = GmCommonClass.parseNull((String) hmRow.get("TAGID"));
			StringBuffer sbHtml = new StringBuffer();
			if (strLockInv.equals("Y")){
				sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/lock.jpg' title='Locked' width='32' height='37'/>");				
			}else{
				sbHtml.append("<img id='imgEdit' style='cursor:hand' src='/images/unlock.gif' title='Unlock' width='28' height='32'/>");;
			}	
			sbHtml.append("<input type=\"hidden\" value=\"" +strLockInv+ "\"  name=\"Lock_" +strTAGID+ "\" >");
			return sbHtml.toString();	
		}
		public String getCURADDR() {
			hmRow =  	(HashMap) this.getCurrentRowObject();  
			StringBuffer sbHtml = new StringBuffer();
			String strAddrNM = "";
			String strAddr1 = "";
			String strAddr2 = "";
			String strCity = "";
			String strState = "";
			String strZipCode = "";
			String strPhoneNumber = "";
			String strEmailId = "";
			String strshipempty = "";
			
			strshipempty = GmCommonClass.parseNull((String) hmRow.get("SHIPEMPTY"));
			if (strshipempty.equals("SHIPEMPTY")){
			strAddrNM = GmCommonClass.parseNull((String) hmRow.get("BILLNAME"));
				 strAddr1 = GmCommonClass.parseNull((String) hmRow.get("BILLADDRESS1"));
				 strAddr2 = GmCommonClass.parseNull((String) hmRow.get("BILLADDRESS2"));
				 strCity = GmCommonClass.parseNull((String) hmRow.get("BILLCITY"));
				 strState = GmCommonClass.parseNull((String) hmRow.get("BILLSTATE"));
				 strZipCode = GmCommonClass.parseNull((String) hmRow.get("BILLZIPCODE"));
				 strPhoneNumber = GmCommonClass.parseNull((String) hmRow.get("BILLPHONENUMBER"));
				 strEmailId = GmCommonClass.parseNull((String) hmRow.get("BILLEMAILID"));
			
			}else{
			
				 strAddrNM = GmCommonClass.parseNull((String) hmRow.get("ADDR_NM"));
				 strAddr1 = GmCommonClass.parseNull((String) hmRow.get("ADDRESS1"));
				 strAddr2 = GmCommonClass.parseNull((String) hmRow.get("ADDRESS2"));
				 strCity = GmCommonClass.parseNull((String) hmRow.get("CITY"));
				 strState = GmCommonClass.parseNull((String) hmRow.get("STATE"));
				 strZipCode = GmCommonClass.parseNull((String) hmRow.get("ZIPCODE"));
				 strPhoneNumber = GmCommonClass.parseNull((String) hmRow.get("PHONENUMBER"));
				 strEmailId = GmCommonClass.parseNull((String) hmRow.get("EMAILID"));
			}
			
			if(!strAddrNM.equals("")){
				sbHtml.append(strAddrNM);
				sbHtml.append("<BR>");
			}
			if(!strAddr1.equals("")){
				sbHtml.append(strAddr1);
				sbHtml.append("<BR>");
			}
			if(!strAddr2.equals("")){
				sbHtml.append(strAddr2);
				sbHtml.append("<BR>");
			}
			if(!strCity.equals("")){
				sbHtml.append(strCity);
				sbHtml.append(" ");
			}
			if(!strState.equals("")){
				sbHtml.append(strState);
				sbHtml.append(" ");
			}
			if(!strZipCode.equals("")){
				sbHtml.append(strZipCode);
				sbHtml.append("<BR>");
			}
			if(!strAddrNM.equals("")){
				sbHtml.append("Ph:-"+strPhoneNumber);
				sbHtml.append("<BR>");
			}
			if(!strAddrNM.equals("")){
				sbHtml.append("Email:-"+strEmailId);
				sbHtml.append("<BR>");
			}
			sbHtml.append("<BR>");

			return sbHtml.toString();	
		}
		public String getDIST() {
			hmRow = (HashMap) this.getCurrentRowObject();
			StringBuffer sbHtml = new StringBuffer();
			String strDistance = "";
			String strshipempty = "";
			
			strshipempty = GmCommonClass.parseNull((String) hmRow.get("SHIPEMPTY"));
			if (strshipempty.equals("SHIPEMPTY")){
				strDistance = GmCommonClass.parseNull((String) hmRow.get("BILL_DIST"));}
			else{
				strDistance = GmCommonClass.parseNull((String) hmRow.get("SHIP_DIST"));
			}
			if (!strDistance.equals("")){
				if (!blMapFlag)
				{
					sbHtml.append("<input type=\"hidden\" value=\"Y\"  name=\"hMapFlag\" >");
					blMapFlag = true;
				}
			sbHtml.append(strDistance);
			
			sbHtml.append(" miles");
			}
			return sbHtml.toString();
		}
		
		
}
