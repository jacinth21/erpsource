package com.globus.sales.InvAllocation.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmSetOverrideRptForm extends GmCommonForm {
	
	private String distId = "";
	private String caseDistId = "";
	private String setId = "";
	private String setName = "";
	private String caseSetId = "";
	private String tagId = "";
	private String parentFrmName = "";
	private String caseInfoId = "";
	private String initLoanFl = "";
	private String tagDistId = "";
	private String xmlData="";
	private String lockFromDt = "";
	private String lockToDt = "";
	private String mapFlag = "";
	
	private ArrayList alResult = new ArrayList();
	private ArrayList alDistributor = new ArrayList();
	
	
	public String getMapFlag() {
		return mapFlag;
	}

	public void setMapFlag(String mapFlag) {
		this.mapFlag = mapFlag;
	}

	public String getLockFromDt() {
		return lockFromDt;
	}

	public void setLockFromDt(String lockFromDt) {
		this.lockFromDt = lockFromDt;
	}

	public String getLockToDt() {
		return lockToDt;
	}

	public void setLockToDt(String lockToDt) {
		this.lockToDt = lockToDt;
	}
	
	public String getXmlData() {
		return xmlData;
	}

	public void setXmlData(String xmlData) {
		this.xmlData = xmlData;
	}
	public String getTagDistId() {
		return tagDistId;
	}
	public void setTagDistId(String tagDistId) {
		this.tagDistId = tagDistId;
	}
	public String getInitLoanFl() {
		return initLoanFl;
	}
	public void setInitLoanFl(String initLoanFl) {
		this.initLoanFl = initLoanFl;
	}
	public String getCaseInfoId() {
		return caseInfoId;
	}
	public void setCaseInfoId(String caseInfoId) {
		this.caseInfoId = caseInfoId;
	}
	public String getParentFrmName() {
		return parentFrmName;
	}
	public void setParentFrmName(String parentFrmName) {
		this.parentFrmName = parentFrmName;
	}
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	public String getCaseSetId() {
		return caseSetId;
	}
	public void setCaseSetId(String caseSetId) {
		this.caseSetId = caseSetId;
	}
	
	public String getSetName() {
		return setName;
	}
	public void setSetName(String setName) {
		this.setName = setName;
	}
	public String getCaseDistId() {
		return caseDistId;
	}
	public void setCaseDistId(String caseDistId) {
		this.caseDistId = caseDistId;
	}
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public String getDistId() {
		return distId;
	}
	public void setDistId(String distId) {
		this.distId = distId;
	}
	public ArrayList getAlResult() {
		return alResult;
	}
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	public ArrayList getAlDistributor() {
		return alDistributor;
	}
	public void setAlDistributor(ArrayList alDistributor) {
		this.alDistributor = alDistributor;
	}
	
	
}
