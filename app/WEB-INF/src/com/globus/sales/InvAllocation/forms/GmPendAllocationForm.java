package com.globus.sales.InvAllocation.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmPendAllocationForm extends GmCommonForm {

	String dist = "";
	String salesRep = "";
	String caseId = "";
	
	String hSetId = "";
	String hsetName = "";
	String hcaseDistId = "";
	String hcaseSetId = "";
	String hcaseInfoId = "";
	
	
	Date sugeryDate = null;
	Date loanDate =null;
	
	
	ArrayList alDist = new ArrayList();
	ArrayList alRepList = new ArrayList();
	private List returnList = new ArrayList();
	
	
	public String getHcaseInfoId() {
		return hcaseInfoId;
	}
	public void setHcaseInfoId(String hcaseInfoId) {
		this.hcaseInfoId = hcaseInfoId;
	}
	public List getReturnList() {
		return returnList;
	}
	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}
	public String getSalesRep() {
		return salesRep;
	}
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}	
	public Date getSugeryDate() {
		return sugeryDate;
	}
	public void setSugeryDate(Date sugeryDate) {
		this.sugeryDate = sugeryDate;
	}
	public Date getLoanDate() {
		return loanDate;
	}
	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	
	public String gethSetId() {
		return hSetId;
	}
	public void sethSetId(String hSetId) {
		this.hSetId = hSetId;
	}
	public ArrayList getAlDist() {
		return alDist;
	}
	public void setAlDist(ArrayList alDist) {
		this.alDist = alDist;
	}
	public ArrayList getAlRepList() {
		return alRepList;
	}
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	public String getCaseId() {
		return caseId;
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	public String getHsetName() {
		return hsetName;
	}
	public void setHsetName(String hsetName) {
		this.hsetName = hsetName;
	}
	public String getHcaseDistId() {
		return hcaseDistId;
	}
	public void setHcaseDistId(String hcaseDistId) {
		this.hcaseDistId = hcaseDistId;
	}
	public String getHcaseSetId() {
		return hcaseSetId;
	}
	public void setHcaseSetId(String hcaseSetId) {
		this.hcaseSetId = hcaseSetId;
	}
	
}
