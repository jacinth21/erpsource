package com.globus.sales.InvAllocation.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmAccItemRequestApprovalForm extends GmLogForm {

  private ArrayList alResult = new ArrayList();
  private String strReqIds = "";
  private String appvlAccessFl = "";
  private String rejectAccessFl = "";
  private String xmlGridData = "";
  private String strTxnType = "";
  private String requestId = "";
  private String eventName = "";
  private String startDt = "";
  private String endDt = "";
  private String requestType = "";
  private String salesRep = "";
  private String fieldSales = "";
  private String nextApprStatus = "";
  private String userType = "";
  private String hApproveInputString = "";
  private String prReqDetLitePnum = "";
  private String systemid = "";
  private String partString = "";
  private String raqtyString = "";
  private String sheetOwner = "";
  private String chk_ShowArrowFl = "";
  private String email_to = "";
  private String email_cc = "";
  private String email_subject = "";
  private String email_comments = "";
  private String requestor_name = "";
  private String message = "";
  private String raId = "";
  private String chkbox_display = "";
  private String request_id = "";
  private String accountID = "";
  private String searchaccountID = "";

  /**
   * @return the accountID
   */
  public String getAccountID() {
    return accountID;
  }

  /**
   * @param accountID the accountID to set
   */
  public void setAccountID(String accountID) {
    this.accountID = accountID;
  }



  /**
   * @return the searchaccountID
   */
  public String getSearchaccountID() {
    return searchaccountID;
  }

  /**
   * @param searchaccountID the searchaccountID to set
   */
  public void setSearchaccountID(String searchaccountID) {
    this.searchaccountID = searchaccountID;
  }



  private ArrayList alEventName = new ArrayList();
  private ArrayList alSystemList = new ArrayList();



  public String getFieldSales() {
    return fieldSales;
  }

  public void setFieldSales(String fieldSales) {
    this.fieldSales = fieldSales;
  }


  public String getRejectAccessFl() {
    return rejectAccessFl;
  }

  public void setRejectAccessFl(String rejectAccessFl) {
    this.rejectAccessFl = rejectAccessFl;
  }

  public String getAppvlAccessFl() {
    return appvlAccessFl;
  }

  public void setAppvlAccessFl(String appvlAccessFl) {
    this.appvlAccessFl = appvlAccessFl;
  }

  public String getStrReqIds() {
    return strReqIds;
  }

  public void setStrReqIds(String strReqIds) {
    this.strReqIds = strReqIds;
  }

  public ArrayList getAlResult() {
    return alResult;
  }

  public void setAlResult(ArrayList alResult) {
    this.alResult = alResult;
  }

  /**
   * @return the xmlGridData
   */
  public String getXmlGridData() {
    return xmlGridData;
  }

  /**
   * @param xmlGridData the xmlGridData to set
   */
  public void setXmlGridData(String xmlGridData) {
    this.xmlGridData = xmlGridData;
  }

  /**
   * @return the strTxnType
   */
  public String getStrTxnType() {
    return strTxnType;
  }

  /**
   * @param strTxnType the strTxnType to set
   */
  public void setStrTxnType(String strTxnType) {
    this.strTxnType = strTxnType;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public String getEventName() {
    return eventName;
  }

  public void setEventName(String eventName) {
    this.eventName = eventName;
  }

  public String getStartDt() {
    return startDt;
  }

  public void setStartDt(String startDt) {
    this.startDt = startDt;
  }

  public String getEndDt() {
    return endDt;
  }

  public void setEndDt(String endDt) {
    this.endDt = endDt;
  }

  public ArrayList getAlEventName() {
    return alEventName;
  }

  public void setAlEventName(ArrayList alEventName) {
    this.alEventName = alEventName;
  }

  public String getRequestType() {
    return requestType;
  }

  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }

  public String getSalesRep() {
    return salesRep;
  }

  public void setSalesRep(String salesRep) {
    this.salesRep = salesRep;
  }


  public String getNextApprStatus() {
    return nextApprStatus;
  }

  public void setNextApprStatus(String nextApprStatus) {
    this.nextApprStatus = nextApprStatus;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

  public String gethApproveInputString() {
    return hApproveInputString;
  }

  public void sethApproveInputString(String hApproveInputString) {
    this.hApproveInputString = hApproveInputString;
  }

  /**
   * @return the prReqDetLitePnum
   */
  public String getPrReqDetLitePnum() {
    return prReqDetLitePnum;
  }

  /**
   * @param prReqDetLitePnum the prReqDetLitePnum to set
   */
  public void setPrReqDetLitePnum(String prReqDetLitePnum) {
    this.prReqDetLitePnum = prReqDetLitePnum;
  }

  public ArrayList getAlSystemList() {
    return alSystemList;
  }

  public void setAlSystemList(ArrayList alSystemList) {
    this.alSystemList = alSystemList;
  }

  public String getSystemid() {
    return systemid;
  }

  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }

  public String getPartString() {
    return partString;
  }

  public void setPartString(String partString) {
    this.partString = partString;
  }

  public String getRaqtyString() {
    return raqtyString;
  }

  public void setRaqtyString(String raqtyString) {
    this.raqtyString = raqtyString;
  }

  public String getSheetOwner() {
    return sheetOwner;
  }

  public void setSheetOwner(String sheetOwner) {
    this.sheetOwner = sheetOwner;
  }

  public String getChk_ShowArrowFl() {
    return chk_ShowArrowFl;
  }

  public void setChk_ShowArrowFl(String chk_ShowArrowFl) {
    this.chk_ShowArrowFl = chk_ShowArrowFl;
  }

  public String getEmail_to() {
    return email_to;
  }

  public void setEmail_to(String email_to) {
    this.email_to = email_to;
  }

  public String getEmail_cc() {
    return email_cc;
  }

  public void setEmail_cc(String email_cc) {
    this.email_cc = email_cc;
  }

  public String getEmail_subject() {
    return email_subject;
  }

  public void setEmail_subject(String email_subject) {
    this.email_subject = email_subject;
  }

  public String getEmail_comments() {
    return email_comments;
  }

  public void setEmail_comments(String email_comments) {
    this.email_comments = email_comments;
  }

  public String getRequestor_name() {
    return requestor_name;
  }

  public void setRequestor_name(String requestor_name) {
    this.requestor_name = requestor_name;
  }


  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return the raId
   */
  public String getRaId() {
    return raId;
  }

  /**
   * @param raId the raId to set
   */
  public void setRaId(String raId) {
    this.raId = raId;
  }

  /**
   * @return the chkbox_display
   */
  public String getChkbox_display() {
    return chkbox_display;
  }

  /**
   * @param chkbox_display the chkbox_display to set
   */
  public void setChkbox_display(String chkbox_display) {
    this.chkbox_display = chkbox_display;
  }

  public String getRequest_id() {
    return request_id;
  }

  public void setRequest_id(String request_id) {
    this.request_id = request_id;
  }


}
