package com.globus.sales.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
/**
 * GmSalesEfficiencyScoreCardForm : Contains the fields used for the Efficiency Score By System, Field Sales and Area Director (AD) score card Report screen
 * @author Raja
 * 
 */
public class GmSalesEfficiencyScoreCardForm extends GmCommonForm {
	
	/**
	 * screenName String type for Screen Title
	 */
	private String screenName = "";
	
	/**
	 * screenType String type for Screen type used to diff. ajax calling
	 */
	private String screenType = "";
	
	private String systemId = "";
	private String distId = "";
	private String adId = "";
	private String drillDownName = "";
	
	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	private String allowedTag = "";
	
	public String getAllowedTag() {
		return allowedTag;
	}

	public void setAllowedTag(String allowedTag) {
		this.allowedTag = allowedTag;
	}

	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}

	/**
	 * @param screenName the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	/**
	 * @return the screenType
	 */
	public String getScreenType() {
		return screenType;
	}

	/**
	 * @param screenType the screenType to set
	 */
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param systemId the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	/**
	 * @return the distId
	 */
	public String getDistId() {
		return distId;
	}

	/**
	 * @param distId the distId to set
	 */
	public void setDistId(String distId) {
		this.distId = distId;
	}

	/**
	 * @return the adId
	 */
	public String getAdId() {
		return adId;
	}

	/**
	 * @param adId the adId to set
	 */
	public void setAdId(String adId) {
		this.adId = adId;
	}

	/**
	 * @return the drillDownName
	 */
	public String getDrillDownName() {
		return drillDownName;
	}

	/**
	 * @param drillDownName the drillDownName to set
	 */
	public void setDrillDownName(String drillDownName) {
		this.drillDownName = drillDownName;
	}
	
	
	
	
}