package com.globus.sales.forms;

import com.globus.common.forms.GmCommonForm;

public class GmSalesReportForm extends GmCommonForm {
	protected String adId = "";
	protected String adName = "";
	protected String vpId = "";
	protected String vpName = "";
	protected String fieldSales = "";
	protected String fieldSalesName = "";
	protected String hfromPage = "";
	protected String accessFilter = "";
	/**
	 * @return the adId
	 */
	public String getAdId() {
		return adId;
	}
	/**
	 * @param adId the adId to set
	 */
	public void setAdId(String adId) {
		this.adId = adId;
	}
	/**
	 * @return the adName
	 */
	public String getAdName() {
		return adName;
	}
	/**
	 * @param adName the adName to set
	 */
	public void setAdName(String adName) {
		this.adName = adName;
	}
	/**
	 * @return the vpId
	 */
	public String getVpId() {
		return vpId;
	}
	/**
	 * @param vpId the vpId to set
	 */
	public void setVpId(String vpId) {
		this.vpId = vpId;
	}
	/**
	 * @return the vpName
	 */
	public String getVpName() {
		return vpName;
	}
	/**
	 * @param vpName the vpName to set
	 */
	public void setVpName(String vpName) {
		this.vpName = vpName;
	}
	/**
	 * @return the fieldSales
	 */
	public String getFieldSales() {
		return fieldSales;
	}
	/**
	 * @param fieldSales the fieldSales to set
	 */
	public void setFieldSales(String fieldSales) {
		this.fieldSales = fieldSales;
	}
	/**
	 * @return the fieldSalesName
	 */
	public String getFieldSalesName() {
		return fieldSalesName;
	}
	/**
	 * @param fieldSalesName the fieldSalesName to set
	 */
	public void setFieldSalesName(String fieldSalesName) {
		this.fieldSalesName = fieldSalesName;
	}
	/**
	 * @return the hfromPage
	 */
	public String getHfromPage() {
		return hfromPage;
	}
	/**
	 * @param hfromPage the hfromPage to set
	 */
	public void setHfromPage(String hfromPage) {
		this.hfromPage = hfromPage;
	}
	/**
	 * @return the accessFilter
	 */
	public String getAccessFilter() {
		return accessFilter;
	}
	/**
	 * @param accessFilter the accessFilter to set
	 */
	public void setAccessFilter(String accessFilter) {
		this.accessFilter = accessFilter;
	}
	
}
