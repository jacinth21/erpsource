package com.globus.sales.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
/**
 * @author Angela Xiang
 * 
 */
public class GmDistributorReportForm extends GmCommonForm {
	private String strXmlString="";
	/**
	 * @return the strXmlString
	 */
	public String getStrXmlString() {
		return strXmlString;
	}
	/**
	 * @param strXmlString the strXmlString to set
	 */
	public void setStrXmlString(String strXmlString) {
		this.strXmlString = strXmlString;
	}
	private String[] selectedCategories = new String[5];
	private String[] selectedStates = new String[5];

	private ArrayList allCategories = new ArrayList();
	private ArrayList allStates = new ArrayList();

	private String selectedActive = "";
	private List reportResultList = new ArrayList();

	public String[] getSelectedCategories() {
		return selectedCategories;
	}
	public void setSelectedCategories(String[] selectedCategories) {
		this.selectedCategories = selectedCategories;
	}
	public String[] getSelectedStates() {
		return selectedStates;
	}
	public void setSelectedStates(String[] selectedStates) {
		this.selectedStates = selectedStates;
	}
	public ArrayList getAllStates() {
		return allStates;
	}
	public void setAllStates(ArrayList allStates) {
		this.allStates = allStates;
	}
	public String getSelectedActive() {
		return selectedActive;
	}
	
	public void setSelectedActive(String selectedActive) {
		this.selectedActive = selectedActive;
	}
	/**
	 * @return Returns the alSelectedCategories.
	 */
	public ArrayList getAllCategories() {
		return allCategories;
	}
	/**
	 * @param alSelectedCategories
	 *            The alSelectedCategories to set.
	 */
	public void setAllCategories(ArrayList allCategories) {
		this.allCategories = allCategories;
	}
	public List getReportResultList() {
		return reportResultList;
	}
	public void setReportResultList(List reportResultList) {
		this.reportResultList = reportResultList;
	}

}