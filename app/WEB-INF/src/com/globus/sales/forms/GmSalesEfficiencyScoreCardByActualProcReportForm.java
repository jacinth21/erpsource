package com.globus.sales.forms;

	import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
	/**
	 * GmSalesEfficiencyScoreCardByActualProcReportForm : Contains the fields used for the  score card Actural Procedures Report screen
	 * @author Mkosalram
	 * 
	 */
	public class GmSalesEfficiencyScoreCardByActualProcReportForm extends GmCommonForm	 {
		
		/**
		 * screenName String type for Screen Title
		 */
		private String screenName = "";
		private String systemId = "";
		private String distributorId = "";
		private String adId = "";
		
		// Added allowed tag field according to PC-5101: Scorecard DO classification changes
		
		private String allowedTag = "";
		private String hAllowedTag = "";
		
		
		
		// Added allowed tag field according to PC-5101: Scorecard DO classification changes
		public String getAllowedTag() {
			return allowedTag;
		}
		public void setAllowedTag(String allowedTag) {
			this.allowedTag = allowedTag;
		}
		/**
		 * @return the systemId
		 */
		public String getSystemId() {
			return systemId;
		}
		/**
		 * @param systemId the systemId to set
		 */
		public void setSystemId(String systemId) {
			this.systemId = systemId;
		}
		/**
		 * @return the distributorId
		 */
		public String getDistributorId() {
			return distributorId;
		}
		/**
		 * @param distributorId the distributorId to set
		 */
		public void setDistributorId(String distributorId) {
			this.distributorId = distributorId;
		}
		/**
		 * @return the adId
		 */
		public String getAdId() {
			return adId;
		}
		/**
		 * @param adId the adId to set
		 */
		public void setAdId(String adId) {
			this.adId = adId;
		}
		
		/**
		 * @return the screenName
		 */
		public String getScreenName() {
			return screenName;
		}

		/**
		 * @param screenName the screenName to set
		 */
		public void setScreenName(String screenName) {
			this.screenName = screenName;
		}
		/**
		 * @return the hAllowedTag
		 */
		public String gethAllowedTag() {
			return hAllowedTag;
		}
		/**
		 * @param hAllowedTag the hAllowedTag to set
		 */
		public void sethAllowedTag(String hAllowedTag) {
			this.hAllowedTag = hAllowedTag;
		}

}

