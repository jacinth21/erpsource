package com.globus.sales.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmRegionSetupForm extends GmCommonForm {
	private String regName = ""; 
	private String zoneType = "";
	private String adType = "";
	private ArrayList alZone = new ArrayList();
	private ArrayList alAD = new ArrayList();
	private ArrayList alADPlus = new ArrayList();
	private String gridXmlData="";
	private String reg_id = "";
	private String vpID = "";
	private String adPlus = "";
	private ArrayList alVP = new ArrayList();
	private ArrayList alDivision = new ArrayList();
	private ArrayList alCountry = new ArrayList();
	private ArrayList alCompany = new ArrayList();
	private String divisionType = "";
	private String countryType = "";
	private String companyType = "";
	private String newADFL="";
	private String newVPFL="";
	private String regHiddenName = "";
	private String adPlusNames = "";
	/**
	 * @return the reg_id
	 */
	public String getReg_id() {
		return reg_id;
	}
	/**
	 * @param regId the reg_id to set
	 */
	public void setReg_id(String regId) {
		reg_id = regId;
	}
	/**
	 * @return the vpID
	 */
	public String getVpID() {
		return vpID;
	}
	/**
	 * @param vpID the vpID to set
	 */
	public void setVpID(String vpID) {
		this.vpID = vpID;
	}
	/**
	 * @return the alVP
	 */
	public ArrayList getAlVP() {
		return alVP;
	}
	/**
	 * @param alVP the alVP to set
	 */
	public void setAlVP(ArrayList alVP) {
		this.alVP = alVP;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the alZone
	 */
	public ArrayList getAlZone() {
		return alZone;
	}
	/**
	 * @param alZone the alZone to set
	 */
	public void setAlZone(ArrayList alZone) {
		this.alZone = alZone;
	}
	/**
	 * @return the alAD
	 */
	public ArrayList getAlAD() {
		return alAD;
	}
	/**
	 * @param alAD the alAD to set
	 */
	public void setAlAD(ArrayList alAD) {
		this.alAD = alAD;
	}
	/**
	 * @return the regName
	 */
	public String getRegName() {
		return regName;
	}
	/**
	 * @param regName the regName to set
	 */
	public void setRegName(String regName) {
		this.regName = regName;
	}
	/**
	 * @return the zoneType
	 */
	public String getZoneType() {
		return zoneType;
	}
	/**
	 * @param zoneType the zoneType to set
	 */
	public void setZoneType(String zoneType) {
		this.zoneType = zoneType;
	}
	/**
	 * @return the adType
	 */
	public String getAdType() {
		return adType;
	}
	/**
	 * @param adType the adType to set
	 */
	public void setAdType(String adType) {
		this.adType = adType;
	}
	/**
	 * @return the alDivision
	 */
	public ArrayList getAlDivision() {
		return alDivision;
	}
	/**
	 * @param alDivision the alDivision to set
	 */
	public void setAlDivision(ArrayList alDivision) {
		this.alDivision = alDivision;
	}

	/**
	 * @return the divisionType
	 */
	public String getDivisionType() {
		return divisionType;
	}
	/**
	 * @param divisionType the divisionType to set
	 */
	public void setDivisionType(String divisionType) {
		this.divisionType = divisionType;
	}
	/**
	 * @return the alADPlus
	 */
	public ArrayList getAlADPlus() {
		return alADPlus;
	}
	/**
	 * @param alADPlus the alADPlus to set
	 */
	public void setAlADPlus(ArrayList alADPlus) {
		this.alADPlus = alADPlus;
	}
	/**
	 * @return the alCompany
	 */
	public ArrayList getAlCompany() {
		return alCompany;
	}
	/**
	 * @param alCompany the alCompany to set
	 */
	public void setAlCompany(ArrayList alCompany) {
		this.alCompany = alCompany;
	}
	/**
	 * @return the companyType
	 */
	public String getCompanyType() {
		return companyType;
	}
	/**
	 * @param companyType the companyType to set
	 */
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	/**
	 * @return the newADFL
	 */
	public String getNewADFL() {
		return newADFL;
	}
	/**
	 * @param newADFL the newADFL to set
	 */
	public void setNewADFL(String newADFL) {
		this.newADFL = newADFL;
	}
	/**
	 * @return the newVPFL
	 */
	public String getNewVPFL() {
		return newVPFL;
	}
	/**
	 * @param newVPFL the newVPFL to set
	 */
	public void setNewVPFL(String newVPFL) {
		this.newVPFL = newVPFL;
	}
	/**
	 * @return the adPlus
	 */
	public String getAdPlus() {
		return adPlus;
	}
	/**
	 * @param adPlus the adPlus to set
	 */
	public void setAdPlus(String adPlus) {
		this.adPlus = adPlus;
	}
	/**
	 * @return the regHiddenName
	 */
	public String getRegHiddenName() {
		return regHiddenName;
	}
	/**
	 * @param regHiddenName the regHiddenName to set
	 */
	public void setRegHiddenName(String regHiddenName) {
		this.regHiddenName = regHiddenName;
	}
	/**
	 * @return the adPlusNames
	 */
	public String getAdPlusNames() {
		return adPlusNames;
	}
	/**
	 * @param adPlusNames the adPlusNames to set
	 */
	public void setAdPlusNames(String adPlusNames) {
		this.adPlusNames = adPlusNames;
	}
	
	/**
	 * @return the countryType
	 */
	public String getCountryType() {
		return countryType;
	}
	/**
	 * @param countryType the countryType to set
	 */
	public void setCountryType(String countryType) {
		this.countryType = countryType;
	}
	
	/**
	 * @return the alCountry
	 */
	public ArrayList getAlCountry() {
		return alCountry;
	}
	/**
	 * @param alCountry the alCountry to set
	 */
	public void setAlCountry(ArrayList alCountry) {
		this.alCountry = alCountry;
	}
	
	
	

	
	
}