package com.globus.sales.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
/**
 * GmSalesEfficiencyScoreCardForm : Contains the fields used for the Efficiency Score By Exp. Consignment Usage Report screen
 * @author Raja
 * 
 */
public class GmSalesEfficiencyScoreCardByConReportForm extends GmCommonForm {
	
	private String systemId = "";
	private String distributorId = "";
	private String adId = "";
	
	/**
	 * @return the systemId
	 */
	public String getSystemId() {
		return systemId;
	}
	/**
	 * @param systemId the systemId to set
	 */
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	/**
	 * @return the distributorId
	 */
	public String getDistributorId() {
		return distributorId;
	}
	/**
	 * @param distributorId the distributorId to set
	 */
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}
	/**
	 * @return the adId
	 */
	public String getAdId() {
		return adId;
	}
	/**
	 * @param adId the adId to set
	 */
	public void setAdId(String adId) {
		this.adId = adId;
	}
	
}