package com.globus.sales.forms;

import java.util.ArrayList;
import java.util.HashMap;

public class GmSalesRunRateReportForm extends GmSalesReportForm {
	
	private String showInActiveRepsFlag = "";
	private String monthRR = "18";
	
	private String salesFromMonth = "";
	private String salesFromYear = "";
	private String salesToMonth = "";
	private String salesToYear = "";
	private String hireFromMonth = "";
	private String hireFromYear = "";
	private String hireToMonth = "";
	private String hireToYear = "";
	private String areaSize = "";
	private String xmonthRRColumn = "";
	
	private String hsortColumn = "";
	private String hsortOrder = "";
	
	private String[] checkZone = new String [5];
	private String[] checkArea = new String [5];
	private String[] checkFieldSalesType = new String [5];
	private String[] checkRepDesignation = new String [5];
	private String selectAllZone = "";
	private String selectAllArea = "";
	private String selectAllFieldSalesType = "";
	private String selectAllRepDesignation = "";
	
	private ArrayList alMonthList = new ArrayList();
	private ArrayList alYearList = new ArrayList();
	private ArrayList alZone = new ArrayList();
	private ArrayList alArea = new ArrayList();
	private ArrayList alFieldSalesType = new ArrayList();
	private ArrayList alRepDesignation = new ArrayList();
	
	private HashMap hmReportData = new HashMap();
	
	public GmSalesRunRateReportForm(){
		// Initializing for onLoad. We dont need reps in other type like AD etc
		checkFieldSalesType[0] =  "4020";
		checkFieldSalesType[1] =  "4021";
		selectAllFieldSalesType ="on";
	}
	
	/**
	 * @return the showInActiveRepsFlag
	 */
	public String getShowInActiveRepsFlag() {
		return showInActiveRepsFlag;
	}
	/**
	 * @param showInActiveRepsFlag the showInActiveRepsFlag to set
	 */
	public void setShowInActiveRepsFlag(String showInActiveRepsFlag) {
		this.showInActiveRepsFlag = showInActiveRepsFlag;
	}
	/**
	 * @return the hmReportData
	 */
	public HashMap getHmReportData() {
		return hmReportData;
	}
	/**
	 * @param hmReportData the hmReportData to set
	 */
	public void setHmReportData(HashMap hmReportData) {
		this.hmReportData = hmReportData;
	}
	/**
	 * @return the monthRR
	 */
	public String getMonthRR() {
		return monthRR;
	}
	/**
	 * @param monthRR the monthRR to set
	 */
	public void setMonthRR(String monthRR) {
		this.monthRR = monthRR;
	}
	/**
	 * @return the salesFromMonth
	 */
	public String getSalesFromMonth() {
		return salesFromMonth;
	}
	/**
	 * @param salesFromMonth the salesFromMonth to set
	 */
	public void setSalesFromMonth(String salesFromMonth) {
		this.salesFromMonth = salesFromMonth;
	}
	/**
	 * @return the salesFromYear
	 */
	public String getSalesFromYear() {
		return salesFromYear;
	}
	/**
	 * @param salesFromYear the salesFromYear to set
	 */
	public void setSalesFromYear(String salesFromYear) {
		this.salesFromYear = salesFromYear;
	}
	/**
	 * @return the salesToMonth
	 */
	public String getSalesToMonth() {
		return salesToMonth;
	}
	/**
	 * @param salesToMonth the salesToMonth to set
	 */
	public void setSalesToMonth(String salesToMonth) {
		this.salesToMonth = salesToMonth;
	}
	/**
	 * @return the salesToYear
	 */
	public String getSalesToYear() {
		return salesToYear;
	}
	/**
	 * @param salesToYear the salesToYear to set
	 */
	public void setSalesToYear(String salesToYear) {
		this.salesToYear = salesToYear;
	}
	/**
	 * @return the hireFromMonth
	 */
	public String getHireFromMonth() {
		return hireFromMonth;
	}
	/**
	 * @param hireFromMonth the hireFromMonth to set
	 */
	public void setHireFromMonth(String hireFromMonth) {
		this.hireFromMonth = hireFromMonth;
	}
	/**
	 * @return the hireFromYear
	 */
	public String getHireFromYear() {
		return hireFromYear;
	}
	/**
	 * @param hireFromYear the hireFromYear to set
	 */
	public void setHireFromYear(String hireFromYear) {
		this.hireFromYear = hireFromYear;
	}
	/**
	 * @return the hireToMonth
	 */
	public String getHireToMonth() {
		return hireToMonth;
	}
	/**
	 * @param hireToMonth the hireToMonth to set
	 */
	public void setHireToMonth(String hireToMonth) {
		this.hireToMonth = hireToMonth;
	}
	/**
	 * @return the hireToYear
	 */
	public String getHireToYear() {
		return hireToYear;
	}
	/**
	 * @param hireToYear the hireToYear to set
	 */
	public void setHireToYear(String hireToYear) {
		this.hireToYear = hireToYear;
	}
	/**
	 * @return the checkZone
	 */
	public String[] getCheckZone() {
		return checkZone;
	}
	/**
	 * @param checkZone the checkZone to set
	 */
	public void setCheckZone(String[] checkZone) {
		this.checkZone = checkZone;
	}
	/**
	 * @return the checkArea
	 */
	public String[] getCheckArea() {
		return checkArea;
	}
	/**
	 * @param checkArea the checkArea to set
	 */
	public void setCheckArea(String[] checkArea) {
		this.checkArea = checkArea;
	}
	/**
	 * @return the checkFieldSalesType
	 */
	public String[] getCheckFieldSalesType() {
		return checkFieldSalesType;
	}
	/**
	 * @param checkFieldSalesType the checkFieldSalesType to set
	 */
	public void setCheckFieldSalesType(String[] checkFieldSalesType) {
		this.checkFieldSalesType = checkFieldSalesType;
	}
	/**
	 * @return the checkRepDesignation
	 */
	public String[] getCheckRepDesignation() {
		return checkRepDesignation;
	}
	/**
	 * @param checkRepDesignation the checkRepDesignation to set
	 */
	public void setCheckRepDesignation(String[] checkRepDesignation) {
		this.checkRepDesignation = checkRepDesignation;
	}
	/**
	 * @return the alZone
	 */
	public ArrayList getAlZone() {
		return alZone;
	}
	/**
	 * @param alZone the alZone to set
	 */
	public void setAlZone(ArrayList alZone) {
		this.alZone = alZone;
	}
	/**
	 * @return the alArea
	 */
	public ArrayList getAlArea() {
		return alArea;
	}
	/**
	 * @param alArea the alArea to set
	 */
	public void setAlArea(ArrayList alArea) {
		this.alArea = alArea;
	}
	/**
	 * @return the alFieldSalesType
	 */
	public ArrayList getAlFieldSalesType() {
		return alFieldSalesType;
	}
	/**
	 * @param alFieldSalesType the alFieldSalesType to set
	 */
	public void setAlFieldSalesType(ArrayList alFieldSalesType) {
		this.alFieldSalesType = alFieldSalesType;
	}
	/**
	 * @return the alRepDesignation
	 */
	public ArrayList getAlRepDesignation() {
		return alRepDesignation;
	}
	/**
	 * @param alRepDesignation the alRepDesignation to set
	 */
	public void setAlRepDesignation(ArrayList alRepDesignation) {
		this.alRepDesignation = alRepDesignation;
	}
	/**
	 * @return the alMonthList
	 */
	public ArrayList getAlMonthList() {
		return alMonthList;
	}
	/**
	 * @param alMonthList the alMonthList to set
	 */
	public void setAlMonthList(ArrayList alMonthList) {
		this.alMonthList = alMonthList;
	}
	/**
	 * @return the alYearList
	 */
	public ArrayList getAlYearList() {
		return alYearList;
	}
	/**
	 * @param alYearList the alYearList to set
	 */
	public void setAlYearList(ArrayList alYearList) {
		this.alYearList = alYearList;
	}
	/**
	 * @return the hsortColumn
	 */
	public String getHsortColumn() {
		return hsortColumn;
	}
	/**
	 * @param hsortColumn the hsortColumn to set
	 */
	public void setHsortColumn(String hsortColumn) {
		this.hsortColumn = hsortColumn;
	}
	/**
	 * @return the hsortOrder
	 */
	public String getHsortOrder() {
		return hsortOrder;
	}
	/**
	 * @param hsortOrder the hsortOrder to set
	 */
	public void setHsortOrder(String hsortOrder) {
		this.hsortOrder = hsortOrder;
	}
	/**
	 * @return the selectAllZone
	 */
	public String getSelectAllZone() {
		return selectAllZone;
	}
	/**
	 * @param selectAllZone the selectAllZone to set
	 */
	public void setSelectAllZone(String selectAllZone) {
		this.selectAllZone = selectAllZone;
	}
	/**
	 * @return the selectAllArea
	 */
	public String getSelectAllArea() {
		return selectAllArea;
	}
	/**
	 * @param selectAllArea the selectAllArea to set
	 */
	public void setSelectAllArea(String selectAllArea) {
		this.selectAllArea = selectAllArea;
	}
	/**
	 * @return the selectAllFieldSalesType
	 */
	public String getSelectAllFieldSalesType() {
		return selectAllFieldSalesType;
	}
	/**
	 * @param selectAllFieldSalesType the selectAllFieldSalesType to set
	 */
	public void setSelectAllFieldSalesType(String selectAllFieldSalesType) {
		this.selectAllFieldSalesType = selectAllFieldSalesType;
	}
	/**
	 * @return the selectAllRepDesignation
	 */
	public String getSelectAllRepDesignation() {
		return selectAllRepDesignation;
	}
	/**
	 * @param selectAllRepDesignation the selectAllRepDesignation to set
	 */
	public void setSelectAllRepDesignation(String selectAllRepDesignation) {
		this.selectAllRepDesignation = selectAllRepDesignation;
	}
	/**
	 * @return the areaSize
	 */
	public String getAreaSize() {
		return areaSize;
	}
	/**
	 * @param areaSize the areaSize to set
	 */
	public void setAreaSize(String areaSize) {
		this.areaSize = areaSize;
	}
	/**
	 * @return the xmonthRRColumn
	 */
	public String getXmonthRRColumn() {
		return xmonthRRColumn;
	}
	/**
	 * @param xmonthRRColumn the xmonthRRColumn to set
	 */
	public void setXmonthRRColumn(String xmonthRRColumn) {
		this.xmonthRRColumn = xmonthRRColumn;
	}
	

}
