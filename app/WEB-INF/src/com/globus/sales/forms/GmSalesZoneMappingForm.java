package com.globus.sales.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmSalesZoneMappingForm extends GmCommonForm{
	private String zoneID = "";
	private String zoneNM = "";
	private String gridXmlData = "";
	/**
	 * @return the zoneID
	 */
	public String getZoneID() {
		return zoneID;
	}
	/**
	 * @param zoneID the zoneID to set
	 */
	public void setZoneID(String zoneID) {
		this.zoneID = zoneID;
	}
	/**
	 * @return the zoneNM
	 */
	public String getZoneNM() {
		return zoneNM;
	}
	/**
	 * @param zoneNM the zoneNM to set
	 */
	public void setZoneNM(String zoneNM) {
		this.zoneNM = zoneNM;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
}
