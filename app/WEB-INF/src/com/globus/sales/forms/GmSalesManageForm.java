package com.globus.sales.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmSalesManageForm extends GmCommonForm {
String	zoneid="";
String	regionid="";
String  oldadid="";
String  newadid="";
String  superaduserid="";
String hAction="";
String strOpt="";
String hRegionNm="";
String hZoneNm="";
String hUserNm="";
ArrayList alUnSelectedDS=new ArrayList();

String checkUnSelectedDS="";
String NAME="";
ArrayList alSelectedDS=new ArrayList();
String checkSelectedDS="";

ArrayList	alZone=new ArrayList();
ArrayList	alRegion=new ArrayList();
ArrayList	alNewAD=new ArrayList();
String checkedFL="";

/**
 * @return the zoneid
 */
public String getZoneid() {
	return zoneid;
}
/**
 * @param zoneid the zoneid to set
 */
public void setZoneid(String zoneid) {
	this.zoneid = zoneid;
}
/**
 * @return the regionid
 */
public String getRegionid() {
	return regionid;
}
/**
 * @param regionid the regionid to set
 */
public void setRegionid(String regionid) {
	this.regionid = regionid;
}
/**
 * @return the oldadid
 */
public String getOldadid() {
	return oldadid;
}
/**
 * @param oldadid the oldadid to set
 */
public void setOldadid(String oldadid) {
	this.oldadid = oldadid;
}
/**
 * @return the newadid
 */
public String getNewadid() {
	return newadid;
}
/**
 * @param newadid the newadid to set
 */
public void setNewadid(String newadid) {
	this.newadid = newadid;
}
/**
 * @return the superaduserid
 */
public String getSuperaduserid() {
	return superaduserid;
}
/**
 * @param superaduserid the superaduserid to set
 */
public void setSuperaduserid(String superaduserid) {
	this.superaduserid = superaduserid;
}
/**
 * @return the alUnSelectedDS
 */
public ArrayList getAlUnSelectedDS() {
	return alUnSelectedDS;
}
/**
 * @param alUnSelectedDS the alUnSelectedDS to set
 */
public void setAlUnSelectedDS(ArrayList alUnSelectedDS) {
	this.alUnSelectedDS = alUnSelectedDS;
}
/**
 * @return the checkUnSelectedDS
 */
public String getCheckUnSelectedDS() {
	return checkUnSelectedDS;
}
/**
 * @param checkUnSelectedDS the checkUnSelectedDS to set
 */
public void setCheckUnSelectedDS(String checkUnSelectedDS) {
	this.checkUnSelectedDS = checkUnSelectedDS;
}
/**
 * @return the nAME
 */
public String getNAME() {
	return NAME;
}
/**
 * @param nAME the nAME to set
 */
public void setNAME(String nAME) {
	NAME = nAME;
}
/**
 * @return the alSelectedDS
 */
public ArrayList getAlSelectedDS() {
	return alSelectedDS;
}
/**
 * @param alSelectedDS the alSelectedDS to set
 */
public void setAlSelectedDS(ArrayList alSelectedDS) {
	this.alSelectedDS = alSelectedDS;
}
/**
 * @return the checkSelectedDS
 */
public String getCheckSelectedDS() {
	return checkSelectedDS;
}
/**
 * @param checkSelectedDS the checkSelectedDS to set
 */
public void setCheckSelectedDS(String checkSelectedDS) {
	this.checkSelectedDS = checkSelectedDS;
}
/**
 * @return the alZone
 */
public ArrayList getAlZone() {
	return alZone;
}
/**
 * @return the hAction
 */
public String gethAction() {
	return hAction;
}
/**
 * @param hAction the hAction to set
 */
public void sethAction(String hAction) {
	this.hAction = hAction;
}
/**
 * @param alZone the alZone to set
 */
public void setAlZone(ArrayList alZone) {
	this.alZone = alZone;
}
/**
 * @return the alRegion
 */
public ArrayList getAlRegion() {
	return alRegion;
}
/**
 * @param alRegion the alRegion to set
 */
public void setAlRegion(ArrayList alRegion) {
	this.alRegion = alRegion;
}
/**
 * @return the alNewAD
 */
public ArrayList getAlNewAD() {
	return alNewAD;
}
/**
 * @param alNewAD the alNewAD to set
 */
public void setAlNewAD(ArrayList alNewAD) {
	this.alNewAD = alNewAD;
}
/**
 * @return the strOpt
 */
public String getStrOpt() {
	return strOpt;
}
/**
 * @param strOpt the strOpt to set
 */
public void setStrOpt(String strOpt) {
	this.strOpt = strOpt;
}
/**
 * @return the hRegionNm
 */
public String gethRegionNm() {
	return hRegionNm;
}
/**
 * @param hRegionNm the hRegionNm to set
 */
public void sethRegionNm(String hRegionNm) {
	this.hRegionNm = hRegionNm;
}
/**
 * @return the checkedFL
 */
public String getCheckedFL() {
	return checkedFL;
}
/**
 * @param checkedFL the checkedFL to set
 */
public void setCheckedFL(String checkedFL) {
	this.checkedFL = checkedFL;
}
/**
 * @return the hZoneNm
 */
public String gethZoneNm() {
	return hZoneNm;
}
/**
 * @param hZoneNm the hZoneNm to set
 */
public void sethZoneNm(String hZoneNm) {
	this.hZoneNm = hZoneNm;
}
/**
 * @return the hUserNm
 */
public String gethUserNm() {
	return hUserNm;
}
/**
 * @param hUserNm the hUserNm to set
 */
public void sethUserNm(String hUserNm) {
	this.hUserNm = hUserNm;
}




}
