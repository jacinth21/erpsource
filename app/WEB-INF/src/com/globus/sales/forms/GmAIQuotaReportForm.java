package com.globus.sales.forms;

import java.util.ArrayList;
import java.util.List;

import org.displaytag.decorator.TotalTableDecorator;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

public class GmAIQuotaReportForm extends GmSalesReportForm {

	private String showDetailsFlag = "";
	private String quotaFromDate = GmCalenderOperations.getFirstDayOfCurrentQuarter();
	private String quotaToDate = GmCalenderOperations.getLastDayOfCurrentQuarter();
	private String partNumberId = "";
	private String partNumberDescription = "";
	private String checkFieldSales[] = new String[5];
	private String selectAll = "";
	
	private List rdQuarterlyQuotaDrillDownByAD = new ArrayList();
	private ArrayList alFieldSales = new ArrayList();
	private TotalTableDecorator dynadecorator = null;
	
	/**
	 * @return the rdQuarterlyQuotaDrillDownByAD
	 */
	public List getRdQuarterlyQuotaDrillDownByAD() {
		return rdQuarterlyQuotaDrillDownByAD;
	}
	/**
	 * @param rdQuarterlyQuotaDrillDownByAD the rdQuarterlyQuotaDrillDownByAD to set
	 */
	public void setRdQuarterlyQuotaDrillDownByAD(List rdQuarterlyQuotaDrillDownByAD) {
		this.rdQuarterlyQuotaDrillDownByAD = rdQuarterlyQuotaDrillDownByAD;
	}
	
	/**
	 * @return the showDetailsFlag
	 */
	public String getShowDetailsFlag() {
		return showDetailsFlag;
	}
	/**
	 * @param showDetailsFlag the showDetailsFlag to set
	 */
	public void setShowDetailsFlag(String showDetailsFlag) {
		this.showDetailsFlag = showDetailsFlag;
	}
	/**
	 * @return the dynadecorator
	 */
	public TotalTableDecorator getDynadecorator() {
		return dynadecorator;
	}
	/**
	 * @param dynadecorator the dynadecorator to set
	 */
	public void setDynadecorator(TotalTableDecorator dynadecorator) {
		this.dynadecorator = dynadecorator;
	}
	
	/**
	 * @return the quotaFromDate
	 */
	public String getQuotaFromDate() {
		return quotaFromDate;
	}
	/**
	 * @param quotaFromDate the quotaFromDate to set
	 */
	public void setQuotaFromDate(String quotaFromDate) {
		this.quotaFromDate = quotaFromDate;
	}
	/**
	 * @return the quotaToDate
	 */
	public String getQuotaToDate() {
		return quotaToDate;
	}
	/**
	 * @param quotaToDate the quotaToDate to set
	 */
	public void setQuotaToDate(String quotaToDate) {
		this.quotaToDate = quotaToDate;
	}
	/**
	 * @return the partNumberId
	 */
	public String getPartNumberId() {
		return partNumberId;
	}
	/**
	 * @param partNumberId the partNumberId to set
	 */
	public void setPartNumberId(String partNumberId) {
		this.partNumberId = partNumberId;
	}
	/**
	 * @return the partNumberDescription
	 */
	public String getPartNumberDescription() {
		return partNumberDescription;
	}
	/**
	 * @param partNumberDescription the partNumberDescription to set
	 */
	public void setPartNumberDescription(String partNumberDescription) {
		this.partNumberDescription = partNumberDescription;
	}
	/**
	 * @return the alFieldSales
	 */
	public ArrayList getAlFieldSales() {
		return alFieldSales;
	}
	/**
	 * @param alFieldSales the alFieldSales to set
	 */
	public void setAlFieldSales(ArrayList alFieldSales) {
		this.alFieldSales = alFieldSales;
	}
	
	/**
	 * @return the checkFieldSales
	 */
	public String[] getCheckFieldSales() {
		return checkFieldSales;
	}
	/**
	 * @param checkFieldSales the checkFieldSales to set
	 */
	public void setCheckFieldSales(String[] checkFieldSales) {
		this.checkFieldSales = checkFieldSales;
	}
	
	/**
	 * @return the selectAll
	 */
	public String getSelectAll() {
		return selectAll;
	}
	/**
	 * @param selectAll the selectAll to set
	 */
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}
	}
