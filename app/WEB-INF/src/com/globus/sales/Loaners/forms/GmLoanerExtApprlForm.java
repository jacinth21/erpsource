package com.globus.sales.Loaners.forms;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.globus.common.forms.GmCommonForm;
import com.globus.common.beans.GmLogger;

/**
 * GmLoanerExtApprlForm class for fields and properties the Loaner Extension Approve and Reject screen
 */
public class GmLoanerExtApprlForm  extends GmCommonForm {
	
	/**
	 * reqJsonString for LoanerExtension grid
	 */
    private String reqJsonString ="";
	/**
	 * xmlGridData for LoanerExtension grid
	 */
	private String xmlGridData = "";
	
   /**
    * strRequestId for String data LoanerExtension grid 
    */
	private String strRequestId = "";
	
	/**
	 * strRequestDetId for String data LoanerExtension grid 
	 */
	private String strRequestDetId = "";
	
	/**
	 * strApprovalComments for String data LoanerExtension grid 
	 */
	private String strApprovalComments = "";
	
	/**
	 * strApprReturnDate for String data LoanerExtension grid 
	 */
	private String strApprReturnDate = "";
	
	/**
	 * strSurgeryDate for String data LoanerExtension grid 
	 */
	private String strSurgeryDate = "";
	
	/**
	 * strDivision for String data LoanerExtension grid 
	 */
	private String strDivision = "";
	
	/**
	 * strZone for String data LoanerExtension grid 
	 */
	private String strZone = "";
	
	/**
	 * strRegion for String data LoanerExtension grid 
	 */
	private String strRegion = "";
	
	/**
	 * alPendApprDetails for ArrayList data LoanerExtension grid 
	 */
	private ArrayList alPendApprDetails = new ArrayList();
	
	/**
	 * alDivision for ArrayList data LoanerExtension filter field 
	 */
	private ArrayList alDivision = new ArrayList();
	
	/**
	 * alZone for ArrayList data LoanerExtension filter field 
	 */
	private ArrayList alZone = new ArrayList();
	
	/**
	 * alRegions for ArrayList data LoanerExtension filter field 
	 */
	private ArrayList alRegions = new ArrayList();
	/**
	 * strRegion for String data LoanerExtension grid 
	 */
	private String strFieldSales = "";
	/**
	 * alFieldSales for ArrayList data LoanerExtension filter field 
	 */
	private ArrayList alFieldSales = new ArrayList();
	
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the strRequestId
	 */
	public String getStrRequestId() {
		return strRequestId;
	}
	/**
	 * @param strRequestId the strRequestId to set
	 */
	public void setStrRequestId(String strRequestId) {
		this.strRequestId = strRequestId;
	}
	/**
	 * @return the strRequestDetId
	 */
	public String getStrRequestDetId() {
		return strRequestDetId;
	}
	/**
	 * @param strRequestDetId the strRequestDetId to set
	 */
	public void setStrRequestDetId(String strRequestDetId) {
		this.strRequestDetId = strRequestDetId;
	}
	/**
	 * @return the strApprovalComments
	 */
	public String getStrApprovalComments() {
		return strApprovalComments;
	}
	/**
	 * @param strApprovalComments the strApprovalComments to set
	 */
	public void setStrApprovalComments(String strApprovalComments) {
		this.strApprovalComments = strApprovalComments;
	}
	/**
	 * @return the strApprReturnDate
	 */
	public String getStrApprReturnDate() {
		return strApprReturnDate;
	}
	/**
	 * @param strApprReturnDate the strApprReturnDate to set
	 */
	public void setStrApprReturnDate(String strApprReturnDate) {
		this.strApprReturnDate = strApprReturnDate;
	}
	/**
	 * @return the strDivision
	 */
	public String getStrDivision() {
		return strDivision;
	}
	/**
	 * @param strDivision the strDivision to set
	 */
	public void setStrDivision(String strDivision) {
		this.strDivision = strDivision;
	}
	/**
	 * @return the strZone
	 */
	public String getStrZone() {
		return strZone;
	}
	/**
	 * @param strZone the strZone to set
	 */
	public void setStrZone(String strZone) {
		this.strZone = strZone;
	}
	/**
	 * @return the strRegion
	 */
	public String getStrRegion() {
		return strRegion;
	}
	/**
	 * @param strRegion the strRegion to set
	 */
	public void setStrRegion(String strRegion) {
		this.strRegion = strRegion;
	}
	/**
	 * @return the alPendApprDetails
	 */
	public ArrayList getAlPendApprDetails() {
		return alPendApprDetails;
	}
	/**
	 * @param alPendApprDetails the alPendApprDetails to set
	 */
	public void setAlPendApprDetails(ArrayList alPendApprDetails) {
		this.alPendApprDetails = alPendApprDetails;
	}
	/**
	 * @return the alDivision
	 */
	public ArrayList getAlDivision() {
		return alDivision;
	}
	/**
	 * @param alDivision the alDivision to set
	 */
	public void setAlDivision(ArrayList alDivision) {
		this.alDivision = alDivision;
	}
	/**
	 * @return the alZone
	 */
	public ArrayList getAlZone() {
		return alZone;
	}
	/**
	 * @param alZone the alZone to set
	 */
	public void setAlZone(ArrayList alZone) {
		this.alZone = alZone;
	}
	/**
	 * @return the alRegions
	 */
	public ArrayList getAlRegions() {
		return alRegions;
	}
	/**
	 * @param alRegions the alRegions to set
	 */
	public void setAlRegions(ArrayList alRegions) {
		this.alRegions = alRegions;
	}
	/**
	 * @return the reqJsonString
	 */
	public String getReqJsonString() {
		return reqJsonString;
	}
	
	/**
	 * @return the strSurgeryDate
	 */
	public String getStrSurgeryDate() {
		return strSurgeryDate;
	}
	
	/**
	 * @param strSurgeryDate the strSurgeryDate to set
	 */
	public void setStrSurgeryDate(String strSurgeryDate) {
		this.strSurgeryDate = strSurgeryDate;
	}
	/**
	 * @param reqJsonString the reqJsonString to set
	 */
	public void setReqJsonString(String reqJsonString) {
		this.reqJsonString = reqJsonString;
	}
	/**
	 * @return the strFieldSales
	 */
	public String getStrFieldSales() {
		return strFieldSales;
	}
	/**
	 * @param strFieldSales the strFieldSales to set
	 */
	public void setStrFieldSales(String strFieldSales) {
		this.strFieldSales = strFieldSales;
	}
	/**
	 * @return the alFieldSales
	 */
	public ArrayList getAlFieldSales() {
		return alFieldSales;
	}
	/**
	 * @param alFieldSales the alFieldSales to set
	 */
	public void setAlFieldSales(ArrayList alFieldSales) {
		this.alFieldSales = alFieldSales;
	}
	
}
