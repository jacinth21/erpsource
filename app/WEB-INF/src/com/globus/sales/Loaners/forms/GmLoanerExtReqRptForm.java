package com.globus.sales.Loaners.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.beans.GmLogger;

/**
 * GmLoanerExtReqRptForm class for fields and properties the Loaner Extension Request report screen
 */
public class GmLoanerExtReqRptForm  extends GmCommonForm {
	
	/**
	 * reqJsonString for LoanerExtension grid
	 */
	private String reqJsonString ="";
	/**
	 * xmlGridData for LoanerExtension grid
	 */
	private String xmlGridData = "";
	
   /**
    * strRequestId for String data LoanerExtension grid 
    */
	private String strRequestId = "";
	/**
	 * strConsignId for String data LoanerExtension grid
	 */
    private String strConsignId ="";
	/**
	 * @return the strConsignId
	 */
	public String getStrConsignId() {
		return strConsignId;
	}
	/**
	 * @param strConsignId the strConsignId to set
	 */
	public void setStrConsignId(String strConsignId) {
		this.strConsignId = strConsignId;
	}
	/**
	 * strRequestDetId for String data LoanerExtension grid 
	 */
	private String strRequestDetId = "";
		
	/**
	 * strDivision for String data LoanerExtension grid 
	 */
	private String strDivision = "";
	
	/**
	 * strZone for String data LoanerExtension grid 
	 */
	private String strZone = "";
	
	/**
	 * strRegion for String data LoanerExtension grid 
	 */
	private String strRegion = "";
	
	/**
	 * alZone for ArrayList data LoanerExtension filter field 
	 */
	private ArrayList alZone = new ArrayList();
	
	/**
	 * alRegions for ArrayList data LoanerExtension filter field 
	 */
	private ArrayList alRegions = new ArrayList();
	/**
	 * alRegions for ArrayList data LoanerExtension filter field 
	 */
	private HashMap hmHistoryDtls = new HashMap();
	
	/**
	 * fromDt for date data LoanerExtension filter field 
	 */
	private String fromDt = "";
	/**
	 * toDt for date data LoanerExtension filter field 
	 */
	private String toDt = "";
	/**
	 * strRegion for String data LoanerExtension grid 
	 */
	private String strFieldSales = "";
	/**
	 * alFieldSales for ArrayList data LoanerExtension filter field 
	 */
	private ArrayList alFieldSales = new ArrayList();
	/**
	 * hmHistoryDtl for ArrayList data LoanerExtension log details
	 */
	private ArrayList hmHistoryDtl = new ArrayList();
	/**
	 * strfl for String data used to diff log and history 
	 */
	private String strfl = "";
	
	/**
	 * @return the strfl
	 */
	public String getStrfl() {
		return strfl;
	}
	/**
	 * @param strfl the strfl to set
	 */
	public void setStrfl(String strfl) {
		this.strfl = strfl;
	}
	/**
	 * @return the strRequestId
	 */
	
	public String getStrRequestId() {
		return strRequestId;
	}
	/**
	 * @return the hmHistoryDtl
	 */
	public ArrayList getHmHistoryDtl() {
		return hmHistoryDtl;
	}
	/**
	 * @param hmHistoryDtl the hmHistoryDtl to set
	 */
	public void setHmHistoryDtl(ArrayList hmHistoryDtl) {
		this.hmHistoryDtl = hmHistoryDtl;
	}
	/**
	 * @param strRequestId the strRequestId to set
	 */
	public void setStrRequestId(String strRequestId) {
		this.strRequestId = strRequestId;
	}
	/**
	 * @return the strRequestDetId
	 */
	public String getStrRequestDetId() {
		return strRequestDetId;
	}
	/**
	 * @param strRequestDetId the strRequestDetId to set
	 */
	public void setStrRequestDetId(String strRequestDetId) {
		this.strRequestDetId = strRequestDetId;
	}
	/**
	 * @return the strDivision
	 */
	public String getStrDivision() {
		return strDivision;
	}
	/**
	 * @param strDivision the strDivision to set
	 */
	public void setStrDivision(String strDivision) {
		this.strDivision = strDivision;
	}
	/**
	 * @return the strZone
	 */
	public String getStrZone() {
		return strZone;
	}
	/**
	 * @param strZone the strZone to set
	 */
	public void setStrZone(String strZone) {
		this.strZone = strZone;
	}
	/**
	 * @return the strRegion
	 */
	public String getStrRegion() {
		return strRegion;
	}
	/**
	 * @param strRegion the strRegion to set
	 */
	public void setStrRegion(String strRegion) {
		this.strRegion = strRegion;
	}

	/**
	 * @return the alZone
	 */
	public ArrayList getAlZone() {
		return alZone;
	}
	/**
	 * @param alZone the alZone to set
	 */
	public void setAlZone(ArrayList alZone) {
		this.alZone = alZone;
	}
	/**
	 * @return the alRegions
	 */
	public ArrayList getAlRegions() {
		return alRegions;
	}
	/**
	 * @param alRegions the alRegions to set
	 */
	public void setAlRegions(ArrayList alRegions) {
		this.alRegions = alRegions;
	}
	/**
	 * @return the fromDt
	 */
	public String getFromDt() {
		return fromDt;
	}
	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}
	/**
	 * @return the toDt
	 */
	public String getToDt() {
		return toDt;
	}
	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(String toDt) {
		this.toDt = toDt;
	}
	/**
	 * @return the hmHistoryDtls
	 */
	public HashMap getHmHistoryDtls() {
		return hmHistoryDtls;
	}
	/**
	 * @param hmHistoryDtls the hmHistoryDtls to set
	 */
	public void setHmHistoryDtls(HashMap hmHistoryDtls) {
		this.hmHistoryDtls = hmHistoryDtls;
	}
	/**
	 * @return the reqJsonString
	 */
	public String getReqJsonString() {
		return reqJsonString;
	}
	/**
	 * @param reqJsonString the reqJsonString to set
	 */
	public void setReqJsonString(String reqJsonString) {
		this.reqJsonString = reqJsonString;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the strFieldSales
	 */
	public String getStrFieldSales() {
		return strFieldSales;
	}
	/**
	 * @param strFieldSales the strFieldSales to set
	 */
	public void setStrFieldSales(String strFieldSales) {
		this.strFieldSales = strFieldSales;
	}
	/**
	 * @return the alFieldSales
	 */
	public ArrayList getAlFieldSales() {
		return alFieldSales;
	}
	/**
	 * @param alFieldSales the alFieldSales to set
	 */
	public void setAlFieldSales(ArrayList alFieldSales) {
		this.alFieldSales = alFieldSales;
	}
	
}
