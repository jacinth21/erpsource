package com.globus.sales.Loaners.beans;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.jms.consumers.processors.GmLoanerExtnApprConsumerJob;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmLoanerExtBean class for fetching, saving, rejecting the Loaner Extension details
 * 
 */
public class GmLoanerExtBean extends GmBean {
	
  /** This used to get the instance of this class for enabling logging */
  Logger log = GmLogger.getInstance(this.getClass().getName());
 
  /**
   * default Constructor GmLoanerExtBean
   */
  public GmLoanerExtBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

    /**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
  public GmLoanerExtBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchPendingApprlLoanerExtDtl This method will fetch the Loaner Extension Request Details
   * 
   * @return ArrayList alDetails
   * @exception AppError
   * @author hreddi
   */
  public String fetchPendingApprlLoanerExtDtls(HashMap hmParam) throws AppError {
	    String strZone = GmCommonClass.parseNull((String)hmParam.get("STRZONE"));
	    String strRegion = GmCommonClass.parseNull((String)hmParam.get("STRREGION"));
	    String strDateFmt =  GmCommonClass.parseNull((String)hmParam.get("APPLNDATEFMT"));
	    String strCompId =  GmCommonClass.parseNull((String)hmParam.get("CMPID"));
	    String strFieldSales = GmCommonClass.parseNull((String)hmParam.get("STRFIELDSALES"));
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();  
	        
	    sbQuery.append("SELECT JSON_ARRAYAGG (JSON_OBJECT('reqdtlid' VALUE t526.c526_product_request_detail_id ,'reqid' VALUE t525.c525_product_request_id , ");
      sbQuery.append(" 'cnid' VALUE t504a.c504_consignment_id ,'comments' VALUE c504a_extension_comments, " );
      sbQuery.append(" 'setid' VALUE t526.c207_set_id ,'setname' VALUE t207.c207_set_nm , " );
      sbQuery.append(" 'repid' VALUE t525.c703_sales_rep_id ,'repname' VALUE t703.c703_Sales_rep_name, " );
      sbQuery.append(" 'acctname' VALUE t704.c704_account_nm ,'requname' VALUE c101_user_f_name || ' ' || c101_user_l_name, ");
      sbQuery.append(" 'loanerdt' VALUE TO_CHAR(t504a.c504a_loaner_dt,'"+strDateFmt+"') ,'extnreqdt' VALUE TO_CHAR(t504a.C504A_EXT_REQUESTED_DATE,'"+strDateFmt+"'), " );  
      sbQuery.append(" 'surgdt' VALUE TO_CHAR(t504a.c504a_surgery_date,'"+strDateFmt+"') ,'extnreqretdt' VALUE TO_CHAR(t504a.c504a_ext_required_date,'"+strDateFmt+"'), " );  
      sbQuery.append(" 'apprretdt' VALUE TO_CHAR(t504a.c504a_expected_return_dt,'"+strDateFmt+"') ,'oldexpdt' VALUE TO_CHAR(c504a_old_expected_retdt,'"+strDateFmt+"'), " ); 
      sbQuery.append(" 'extcount' VALUE TO_CHAR(NVL(t526.C526_EXTEND_COUNT,0)),  " );
      sbQuery.append(" 'statusid' VALUE t504a.c901_status , 'sysid' VALUE get_system_from_setid (t526.c207_set_id), 'adid' VALUE get_ad_rep_id (t525.c703_sales_rep_id) " );
      sbQuery.append(" ,'replnfl' VALUE DECODE(t504a.c901_reason_type ,50322 ,'Y','') " );
      sbQuery.append(" ,'hisfl' VALUE gm_pkg_op_loaner_ext_rpt.get_history_fl(t504a.c504_consignment_id),'etchid' value t504ac.c504a_etch_id  ");
      sbQuery.append( ") ORDER BY t504a.C504A_EXT_REQUESTED_DATE DESC,t504a.C504A_LAST_UPDATED_DATE_UTC DESC RETURNING CLOB)");
      sbQuery.append(" FROM t526_product_request_detail t526,t504a_loaner_transaction t504a,t504a_consignment_loaner t504ac,t525_product_request t525,t704_account t704,t703_sales_rep t703,t207_set_master t207,t101_user t101");
      if(((!(strZone.equals("0") || strZone.equals("")))|| (!(strRegion.equals("0") || strRegion.equals(""))) || (!(strFieldSales.equals("0") || strFieldSales.equals(""))))){
      sbQuery.append(" ,(SELECT DISTINCT v700.rep_id FROM v700_territory_mapping_detail v700  WHERE "
      		+ " ( ( compid = decode(100800, 100803, NULL, 100800) OR decode(100800, 100803, 1, 2) = 1 ) )");

		if(!((strZone.equals("0")) || strZone.equals(""))){	
			strZone = strZone.replaceAll(",", "','");
			 sbQuery.append(" AND gp_id IN ('");
			 sbQuery.append(strZone);
		     sbQuery.append("')");
	    }
		
		if(!((strRegion.equals("0")) || strRegion.equals(""))){	
			strRegion = strRegion.replaceAll(",", "','");
			 sbQuery.append(" AND region_id IN ('");
			 sbQuery.append(strRegion);
		     sbQuery.append("')");
	    }
		if(!((strFieldSales.equals("0")) || strFieldSales.equals(""))){	
			strFieldSales = strFieldSales.replaceAll(",", "','");
			 sbQuery.append(" AND D_ID IN ('");
			 sbQuery.append(strFieldSales);
		     sbQuery.append("')");
	    }
		 sbQuery.append(" ) v700 ");
    }
		 sbQuery.append(" WHERE t525.c525_product_request_id = t526.c525_product_request_id ");	 
		 sbQuery.append(" AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id ");
		 sbQuery.append(" AND t504a.c504_consignment_id = t504ac.c504_consignment_id ");
		 sbQuery.append(" AND t504a.c901_status = 1901 " );  //1901 - Pending Approval
		 sbQuery.append(" AND t525.c704_account_id =  t704.c704_account_id(+)" );
		 sbQuery.append(" AND t525.c703_sales_rep_id = t703.c703_sales_rep_id(+)" );
		 sbQuery.append(" AND t207.c207_set_id = t526.c207_set_id" );
		 sbQuery.append(" AND t101.c101_user_id = t504a.c504a_ext_requested_by" );
		 sbQuery.append(" AND t504a.c504a_return_dt IS NULL ");
		 sbQuery.append(" AND t526.c526_void_fl  IS NULL ");
		 sbQuery.append(" AND t525.c525_void_fl  IS NULL ");
		 sbQuery.append(" AND t704.c704_void_fl  IS NULL ");
		 sbQuery.append("AND t703.c703_void_fl   IS NULL ");
		 sbQuery.append("AND t207.c207_void_fl   IS NULL ");
		 sbQuery.append(" AND t504a.c504a_void_fl IS NULL AND t504ac.c504a_void_fl IS NULL AND t504a.c1900_company_id = '"+strCompId+"'");
		 if(((!(strZone.equals("0") || strZone.equals("")))|| (!(strRegion.equals("0") || strRegion.equals(""))) || (!(strFieldSales.equals("0") || strFieldSales.equals(""))))){
			 sbQuery.append(" AND t525.c703_sales_rep_id = v700.rep_id");
		 }
		 log.debug("Query>>> " + sbQuery);
		 String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

	return strReturn;
}
  /**
   * saveLoanerExtAppv This method will Approve the Loaner Extension Request.
   * 
   * @param HashMap
   * @return strReqDetId
   * @exception AppError
   * @author hreddi
   */
    public String saveLoanerExtAppv(HashMap hmParam) throws AppError, ParseException {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strReqDetId = "";
	  String strApplDateFmt =getCompDateFmt();
      String  strRequestDetId = GmCommonClass.parseNull((String) hmParam.get("STRREQUESTDETID"));
      String  strRequestId = GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID"));
	  String  strApprReturnDate = GmCommonClass.parseNull((String) hmParam.get("STRAPPRRETURNDATE"));
	  String  strApprComment =  GmCommonClass.parseNull((String) hmParam.get("STRAPPROVALCOMMENTS"));
	  String  strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	  String  strSurgeryDate = GmCommonClass.parseNull((String) hmParam.get("STRSURGERYDATE"));
	  Date  dtSurgeryDt = GmCommonClass.getStringToDate(strSurgeryDate, strApplDateFmt);
	  Date  dtApprReturnDt = GmCommonClass.getStringToDate(strApprReturnDate, strApplDateFmt); 
	  String  strApproveFl = GmCommonClass.parseNull((String) hmParam.get("APPROVEFL"));
	  gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_op_sav_lnext_req_det", 8);
	  gmDBManager.setString(1, strRequestId);
	  gmDBManager.setString(2, strRequestDetId);
	  gmDBManager.setDate(3, dtApprReturnDt);
	  gmDBManager.setString(4, strApprComment);
	  gmDBManager.setInt(5, Integer.parseInt(strUserId));
	  gmDBManager.setDate(6, dtSurgeryDt);
	  gmDBManager.setString(7, strApproveFl);
	  gmDBManager.registerOutParameter(8, OracleTypes.CLOB);
	  gmDBManager.execute();
	   
	  strReqDetId = GmCommonClass.parseNull(gmDBManager.getString(8));
	  log.debug("strReqDetId==="+strReqDetId);
	  gmDBManager.commit();
	  
      return strReqDetId;
   }
 
  /**
   * sendLoanerExtensionEmail Sending Email to the Sales Rep's regarding the Loaner Extension
   * Request Approval
   * 
   * @param hmParams
   * @return
   * @exception AppError, ParseException
   * @author hreddi
   */
  /* */
  public void sendLoanerExtensionEmail(HashMap hmParam) throws AppError, ParseException {

    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
    GmInHouseSetsTransBean gmInHouseSetsTransBean = new GmInHouseSetsTransBean(getGmDataStoreVO());

    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFMT"));

    log.debug("strConsignId::::" + strConsignId);

    String strExtnDate = "";
    String strConsignString = "";
    String strExtnDateString = "";
    String strWExtnFlString = "";
    String strRepIdsString = "";

    String[] strCombineString = strConsignId.split("@");

    strConsignString = strCombineString[0];
    strExtnDateString = strCombineString[1];
    strWExtnFlString = strCombineString[2];
    strRepIdsString = strCombineString[3];

    strConsignId = strConsignString.substring(0, strConsignString.length() - 1);
    strExtnDate = strExtnDateString.substring(0, strExtnDateString.length() - 1);
    strWExtnFlString = strWExtnFlString.substring(0, strWExtnFlString.length() - 1);
    strRepIdsString = strRepIdsString.substring(0, strRepIdsString.length() - 1);

    String[] strConsignIds = strConsignId.split(",");
    String[] strExpDateOnly = strExtnDate.split(",");
    String[] strWExtnFl = strWExtnFlString.split(",");
    String[] strRepIds = strRepIdsString.split(",");

    String strNConsignIds = "";
    String strNExpDateOnly = "";
    String strNWExtnFl = "";
    String strNRepIds = "";

    int repIdLength = strRepIds.length;
    // Arrays[] qwer = new Arrays[length];
    boolean[] blIsChecked = new boolean[repIdLength];

    for (int i = 0; i < repIdLength; i++) {
      // if this value has already by processed pass to the next one
      if (blIsChecked[i]) {
        continue;
      }
      // scan from our actual position to the end of the array
      for (int j = i; j < repIdLength; j++) {
        // if this value has already been processed go check next ar[j]
        if (blIsChecked[j]) {
          continue;
        }
        // if both are equal
        if (strRepIds[j] == strRepIds[i]) {
          blIsChecked[j] = true; // flag this value as having been processed
          strNConsignIds = strNConsignIds + strConsignIds[j] + ","; // increment the counter for
                                                                    // this value
          strNExpDateOnly = strNExpDateOnly + strExpDateOnly[j] + ",";
          strNWExtnFl = strNWExtnFl + strWExtnFl[j] + ",";
          strNRepIds = strNRepIds + strRepIds[j] + ",";
        }
      }
    }

    strConsignIds = strNConsignIds.split(",");
    strExpDateOnly = strNExpDateOnly.split(",");
    strWExtnFl = strNWExtnFl.split(",");
    strRepIds = strNRepIds.split(",");

    String strPrevRepId = "";
    Date dtExtDate = null;
    DateFormat formatter = new SimpleDateFormat(strDateFmt);
    HashMap hmAddParam = new HashMap();
    ArrayList alReqDetail = null;
    ArrayList alReqExtDetail = new ArrayList();
    ArrayList alTemp = new ArrayList();

    int repLength = strRepIds.length;
    int j = 0;
    int intCount = 0;
    for (j = 0; j < repLength; j++) {
      String strRepId = strRepIds[j];
      if (j == 0) {
        strPrevRepId = strRepId;
      }
      alReqDetail = new ArrayList();
      alReqDetail =
          GmCommonClass.parseNullArrayList(gmInHouseSetsTransBean.fetchLoanerRequestDetail(
              strConsignIds[j], strWExtnFl[j]));
      if (!strExpDateOnly[j].equals("")) {
        dtExtDate = formatter.parse(strExpDateOnly[j]);
      }
      String strExtDate = GmCommonClass.getStringFromDate(dtExtDate, strDateFmt);
      hmAddParam.put("OLDEXTDATE", strExtDate);
      alReqExtDetail =
          GmCommonClass.parseNullArrayList(gmInHouseRptBean.getConsignDetails(alReqDetail));
      alReqDetail = prepareLoanerExtnEmailList(alReqExtDetail, hmAddParam);

      if (strPrevRepId.equals(strRepId)) {
        alTemp.addAll(alReqDetail);
        intCount++;
      } else {
        hmAddParam.put("TODAYSDATE", GmCommonClass.parseNull((String) hmParam.get("TODAYSDATE")));
        gmInHouseSetsTransBean.sendLoanerExtensionEmail(alTemp, hmAddParam);
        alTemp = new ArrayList();
        alTemp.addAll(alReqDetail);
        intCount = 1;
      }
      strPrevRepId = strRepId;
    }
    if (intCount > 0) {
      hmAddParam.put("TODAYSDATE", GmCommonClass.parseNull((String) hmParam.get("TODAYSDATE")));
      gmInHouseSetsTransBean.sendLoanerExtensionEmail(alTemp, hmAddParam);
    }
  }

  /**
   * prepareLoanerExtnEmailList This method is used to prepare Loaner Extn Email List with the OLD &
   * NEW extn date populated
   * 
   * @param alDetails, hmDetails
   * @return ArrayList
   * @exception AppError
   * @author hreddi
   */
  public ArrayList prepareLoanerExtnEmailList(ArrayList alDetails, HashMap hmDetails) {

    ArrayList alReturn = new ArrayList();
    HashMap hmAddParam = null;
    String strOldExtDt = (String) hmDetails.get("OLDEXTDATE");
    String strNewExtRtnDt = "";
    int alSize = alDetails.size();
    for (int i = 0; i < alSize; i++) {
      hmAddParam = new HashMap();
      hmAddParam = GmCommonClass.parseNullHashMap((HashMap) alDetails.get(i));
      strNewExtRtnDt = GmCommonClass.parseNull((String) hmAddParam.get("EREDATE"));
      hmAddParam.put("EREDATE", strOldExtDt);
      hmAddParam.put("NEWEXTDATE", strNewExtRtnDt);
      alReturn.add(hmAddParam);
    }
    return alReturn;
  }

  /**
   * fetchLoanerExt This method is used to fetch loaner Extension Request details
   * 
   * @param hmParams
   * @return HashMap
   * @exception AppError
   * @author hreddi
   */
  public HashMap fetchLoanerExt(HashMap hmParams) throws AppError {
    HashMap hmReturnData = new HashMap();
    ArrayList alDetails = new ArrayList();
    ArrayList alNewExtnDetails = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_op_fch_lnext_req_requests", 3);
    gmDBManager.setString(1, (String) hmParams.get("INPUTSTRING"));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    alNewExtnDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    hmReturnData.put("EXTNDETAILS", alDetails);
    hmReturnData.put("NEWEXTNDETIALS", alNewExtnDetails);
    return hmReturnData;
  } // End of fetchLoanerExt

  /**
   * saveLoanerExt This method will Extend the Loaner Extension Request.
   * 
   * @param HashMap
   * @return String
   * @exception AppError
   * @author hreddi
   */
  public void saveLoanerExt(HashMap hmParam) throws AppError, ParseException {
    // String strMessage = "Loaner extension requests are submitted successfully";
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("EXTENDSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_op_sav_loaner_ext_only", 4);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strLog);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strConsignIds = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();

    String strDateFormat =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    Date todaysDt = GmCommonClass.getCurrentDate(strDateFormat);
    hmParam.put("CONSIGNID", strConsignIds);
    hmParam.put("DATEFMT", strDateFormat);
    hmParam.put("TODAYSDATE", GmCommonClass.getStringFromDate(todaysDt, strDateFormat));
    String strPendingApprlFl = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("50321","LEEXTAPPR",getGmDataStoreVO().getCmpid()));
    if ((!strConsignIds.equals("") && !strPendingApprlFl.equals("Y"))) {
      sendLoanerExtensionEmail(hmParam);
    }
    // Removing since we cannot throw exception in web service but Succes message in Portal will be
    // handled in Action class
    // if (!strMessage.equals("")){
    // throw new AppError(strMessage, "", 'S');
    // }
    // return strMessage;
  } // End of saveLoanerExt
 
  /**
   * rejectLoanerExtApprl - to Reject Loaner Extension Request Details
   * @param hmParam
   * @return strReqDetId
   * @throws AppError, ParseException
   */
  public String rejectLoanerExtAppv(HashMap hmParam) throws AppError, ParseException {
	     GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	     String strReqDetId = "";
	     String strRequestDetId = GmCommonClass.parseNull((String) hmParam.get("STRREQUESTDETID"));
	     String strRequestId = GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID"));
		 String strApprComment =  GmCommonClass.parseNull((String) hmParam.get("STRAPPROVALCOMMENTS"));
		 String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		 
		 gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_op_sav_loaner_ext_rejt", 5);
	     gmDBManager.setString(1, strRequestId);
		 gmDBManager.setString(2, strRequestDetId);
	     gmDBManager.setString(3, strApprComment);
	     gmDBManager.setInt(4, Integer.parseInt(strUserId));
	     gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
	     gmDBManager.execute();
	     strReqDetId = GmCommonClass.parseNull(gmDBManager.getString(5));
	     gmDBManager.commit();
        
		 return strReqDetId;
  }
  
  /**
   * getZone - to get zones based on logged in party id associated to company
   * @param strPartyId
   * @return
   * @throws AppError
   */
  public ArrayList getZone(String strCmpId) throws AppError {
	    ArrayList alResult = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_ext_rpt.gm_fch_zone_cmp", 2);
		gmDBManager.setString(1, strCmpId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
	    return alResult;
 }
  
  /**
   * getRegion - to get region based on logged in party id associated to company
   * @param strPartyId
   * @return
   * @throws AppError
   */
  public ArrayList getRegion(String strCmpId) throws AppError {
	  ArrayList alResult = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_ext_rpt.gm_fch_region_cmp", 2);
		gmDBManager.setString(1, strCmpId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
	    return alResult;
  }
 
  /**
	 * sendLoanerExtnEmailJMS - this method used to call the JMS for send
	 * Email
	 * 
	 * @param hmvalue
	 * @throws AppError
	 */
	public void sendLoanerExtnEmailJMS(HashMap hmvalue) throws AppError {

		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		HashMap hmParam = new HashMap();
		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("LOANER_EXTN_APPR_REJ_CONSUMER_CLASS"));
		String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOANER_EXTENSION_QUEUE"));
		hmParam.put("QUEUE_NAME", strQueueName);
		hmParam.put("HMVALUES", hmvalue);
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		gmConsumerUtil.sendMessage(hmParam);
	}

	/**
	 * sendLoanerExtnStatusEmail - This method used to fetch the Loaner Extn
	 * approve or reject Details and Send Email Notification to Sales Rep
	 * 
	 * @param hmValParam
	 * @throws AppError
	 */
	public void sendLoanerExtnStatusEmail(HashMap hmValParam) throws AppError {

		GmLoanerExtnApprConsumerJob gmLoanerExtnApprConsumerJob = new GmLoanerExtnApprConsumerJob();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();
		ArrayList alHederDtls = new ArrayList();
		HashMap hmLoop = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmResult = new HashMap();

		String strReqDtlId = GmCommonClass.parseNull((String) hmValParam.get("STRREQUESTDETID"));
		String strStatusID = GmCommonClass.parseNull((String) hmValParam.get("STATUS"));
		String strUserId = GmCommonClass.parseNull((String) hmValParam.get("USERID"));
		String strcmpId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
		String strCompanyInfo = GmCommonClass.parseNull((String) hmValParam.get("COMPANYINFO"));
		String strTodayDt = GmCommonClass.parseNull((String) hmValParam.get("TODAYSDATE"));
		String strNextRepId = "";
		String strParentReqId = "";
		String strAccountNm = "";
		String strStatus = "";

		gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_fch_loaner_details", 5);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.setString(1, strReqDtlId);
		gmDBManager.setInt(2,Integer.parseInt(strUserId));
		gmDBManager.setString(3, strStatusID);
		gmDBManager.execute();
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4)));
		alHederDtls = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5)));
		gmDBManager.close();
		
		int intParentDtlSize = alHederDtls.size();
		log.debug("intParentIDSize >>>>>>>>"+intParentDtlSize);
		for (int i = 0; i < intParentDtlSize; i++) {
			hmLoop = (HashMap) alHederDtls.get(i);
			hmLoop.put("COMPANYINFO", strCompanyInfo);
			hmLoop.put("TODAYSDATE", strTodayDt);
			gmLoanerExtnApprConsumerJob.populateEmailData(hmLoop, alReturn);
		}
		//to update the approve reject email flag after mail sent
		if(alHederDtls.size() > 0 &&  alReturn.size() > 0){
			updateApprRejectEmailFl(hmValParam);
		}
		
}

/**
 * getFieldSales - to get Filed Sales based on logged in party id associated to company
 * @param strPartyId
 * @return
 * @throws AppError
 */
public ArrayList getFieldSales(String strCmpId) throws AppError {
	  ArrayList alResult = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strCompLangId = GmCommonClass.parseNull(getComplangid());
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_ext_rpt.gm_fch_field_sales_cmp", 3);
		gmDBManager.setString(1, strCmpId);
		gmDBManager.setString(2, strCompLangId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
	    return alResult;
}


	/**
	 * updateApprRejectEmailFl - this method used to update the approve reject
	 * Email flag after sent mail to seles rep
	 * @param hmParam
	 * @throws AppError
	 */
	public void updateApprRejectEmailFl(HashMap hmParam) throws AppError {
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("STRREQUESTDETID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_loaner_extn_txn.gm_op_upd_appr_rej_email_flg", 2);
		gmDBManager.setString(1, strInputString);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}

} 
