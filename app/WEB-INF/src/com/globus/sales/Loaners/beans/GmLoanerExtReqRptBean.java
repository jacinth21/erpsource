package com.globus.sales.Loaners.beans;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;
import com.globus.valueobject.common.GmDataStoreVO;
/**
 * GmLoanerExtReqRptBean class for fetching the Loaner Extension Request Reports and log details foe Approve/Reject request
 * @author Agilan S
 */
public class GmLoanerExtReqRptBean extends GmBean {
  /** This used to get the instance of this class for enabling logging */
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * default Constructor GmLoanerExtReqRptBean
   */
  public GmLoanerExtReqRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
  public GmLoanerExtReqRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fchLoanerExtnReqRpt This method will fetch the Loaner Extension Request Report Details
   * 
   * @return ArrayList alDetails
   * @exception AppError
   * @author hreddi
   */

  public String fchLoanerExtnReqRpt(HashMap hmParam) throws AppError {
	    ArrayList alDetails = new ArrayList();
	    String strDiv = GmCommonClass.parseNull((String)hmParam.get("STRDIVISION"));
	    String strZone = GmCommonClass.parseNull((String)hmParam.get("STRZONE"));
	    String strRegion = GmCommonClass.parseNull((String)hmParam.get("STRREGION"));
	    String strDateFmt =  GmCommonClass.parseNull((String)hmParam.get("APPLNDATEFMT"));
	    String strFromDate = GmCommonClass.parseNull((String)hmParam.get("FROMDT"));
	    String strToDate = GmCommonClass.parseNull((String)hmParam.get("TODT"));
	    String strFieldSales = GmCommonClass.parseNull((String)hmParam.get("STRFIELDSALES"));
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strCompId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
	    StringBuffer sbQuery = new StringBuffer();  
	        
	    sbQuery.append("SELECT JSON_ARRAYAGG (JSON_OBJECT('reqdtlid' VALUE t526.c526_product_request_detail_id ,'reqid' VALUE t525.c525_product_request_id , ");
        sbQuery.append(" 'cnid' VALUE t504a.c504_consignment_id ,'comments' VALUE c504a_extension_comments, " );
        sbQuery.append(" 'setid' VALUE t526.c207_set_id ,'setname' VALUE t207.c207_set_nm , " );
        sbQuery.append(" 'repid' VALUE t525.c703_sales_rep_id ,'repname' VALUE t703.c703_Sales_rep_name, " );
        sbQuery.append(" 'loanerdt' VALUE TO_CHAR(t504a.c504a_loaner_dt,'"+strDateFmt+"') ,'extnreqdt' VALUE TO_CHAR(t504a.C504A_EXT_REQUESTED_DATE,'"+strDateFmt+"'), " );  
        sbQuery.append(" 'surgdt' VALUE TO_CHAR(t504a.c504a_surgery_date,'"+strDateFmt+"') ,'extnreqretdt' VALUE TO_CHAR(t504a.c504a_ext_required_date,'"+strDateFmt+"'), " );  
        sbQuery.append(" 'apprretdt' VALUE decode(t504a.c901_status,'1902',TO_CHAR(t504a.c504a_expected_return_dt,'"+strDateFmt+"'),TO_CHAR(t504a.c504a_ext_required_date,'"+strDateFmt+"')) ,'oldexpdt' VALUE TO_CHAR(c504a_old_expected_retdt,'"+strDateFmt+"'), " ); 
        sbQuery.append(" 'statusid' VALUE t504a.c901_status ,'statusnm' VALUE get_code_name(t504a.c901_status), " );
        sbQuery.append(" 'extcount' VALUE to_char(nvl(t526.c526_extend_count, 0)), 'acctname' VALUE t704.c704_account_nm ,'requname' VALUE c101_user_f_name || ' ' || c101_user_l_name , " );
        sbQuery.append(" 'statusid' VALUE t504a.c901_status ,'statusnm' VALUE get_code_name(t504a.c901_status) " );
        sbQuery.append(" ,'replnfl' VALUE DECODE(t504a.c901_reason_type ,50322 ,'Y','') " );    
        sbQuery.append(" ,'hisfl' VALUE gm_pkg_op_loaner_ext_rpt.get_history_fl(t504a.c504_consignment_id),'etchid' VALUE t504ac.c504a_etch_id ");
        sbQuery.append( ") ORDER BY t504a.C504A_EXT_REQUESTED_DATE DESC,t504a.C504A_LAST_UPDATED_DATE_UTC DESC RETURNING CLOB) ");
        sbQuery.append(" FROM t526_product_request_detail t526,t504a_loaner_transaction t504a,t504a_consignment_loaner t504ac, t525_product_request t525,t704_account t704,t703_sales_rep t703,t207_set_master t207,t101_user t101");
        if(((!(strZone.equals("0") || strZone.equals("")))|| (!(strRegion.equals("0") || strRegion.equals(""))) || (!(strFieldSales.equals("0") || strFieldSales.equals(""))))){
        sbQuery.append(" ,(SELECT DISTINCT v700.rep_id FROM v700_territory_mapping_detail v700  WHERE "
        		+ " ( ( compid = decode(100800, 100803, NULL, 100800) OR decode(100800, 100803, 1, 2) = 1 ) )");
       
		if(!((strZone.equals("0")) || strZone.equals(""))){	
			strZone = strZone.replaceAll(",", "','");
			 sbQuery.append(" AND gp_id IN ('");
			 sbQuery.append(strZone);
		     sbQuery.append("')");
	    }
		
		if(!((strRegion.equals("0")) || strRegion.equals(""))){	
			strRegion = strRegion.replaceAll(",", "','");
			 sbQuery.append(" AND region_id IN ('");
			 sbQuery.append(strRegion);
		     sbQuery.append("')");
	    }
		if(!((strFieldSales.equals("0")) || strFieldSales.equals(""))){	
			strFieldSales = strFieldSales.replaceAll(",", "','");
			 sbQuery.append(" AND D_ID IN ('");
			 sbQuery.append(strFieldSales);
		     sbQuery.append("')");
	    }
		 sbQuery.append(" ) v700 ");
  }
		 sbQuery.append(" WHERE t525.c525_product_request_id = t526.c525_product_request_id ");	 
		 sbQuery.append(" AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id ");
		 sbQuery.append(" AND t504a.c504_consignment_id = t504ac.c504_consignment_id ");
		 sbQuery.append(" AND t525.c704_account_id =  t704.c704_account_id(+)" );
		 sbQuery.append(" AND t525.c703_sales_rep_id = t703.c703_sales_rep_id(+)" );
		 sbQuery.append(" AND t207.c207_set_id = t526.c207_set_id" );
		 sbQuery.append(" AND t101.c101_user_id = t504a.c504a_ext_requested_by" );
		 sbQuery.append(" AND t504a.c901_status IS NOT NULL" );
		// sbQuery.append(" AND t504a.c504a_return_dt IS NULL ");
		 sbQuery.append(" AND t526.c526_void_fl  IS NULL ");
		 sbQuery.append(" AND t525.c525_void_fl  IS NULL ");
		 
		 if(!(strFromDate.equals("") && strToDate.equals("") )){
			 sbQuery.append(" AND t504a.C504A_EXT_REQUESTED_DATE BETWEEN TO_DATE('"+strFromDate+"','"+strDateFmt+"') AND TO_DATE('"+strToDate+"','"+strDateFmt+"') ");
		 }
		 sbQuery.append(" AND t504a.c504a_void_fl IS NULL ");
		 sbQuery.append(" AND t504ac.c504a_void_fl IS NULL ");
		 sbQuery.append(" AND t704.c704_void_fl  IS NULL ");
		 sbQuery.append(" AND t703.c703_void_fl   IS NULL ");
		 sbQuery.append(" AND t207.c207_void_fl   IS NULL ");
		 if(!strCompId.equals("")){
			 sbQuery.append(" AND t504a.c1900_company_id = '"+strCompId+"' ");
		 }
		 if(((!(strZone.equals("0") || strZone.equals("")))|| (!(strRegion.equals("0") || strRegion.equals(""))) || (!(strFieldSales.equals("0") || strFieldSales.equals(""))))){
			 sbQuery.append(" AND t525.c703_sales_rep_id = v700.rep_id");
			 }
		 log.debug("Query>>> " + sbQuery);
		 String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());

	return strReturn;
}
  /**
   * checkLoanerExtReq - to fetch previous extension status is open or not
   * @param hmParam
   * @return strPreExtnReq
   * @throws AppError, ParseException
   */
  public String checkLoanerExtReq(HashMap hmParam) throws AppError, ParseException {
	     GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	     String strPreExtnReq = "";
	     String strConsignId = GmCommonClass.parseNull((String) hmParam.get("STRCONSIGNID"));
		 
		 gmDBManager.setPrepareString("gm_pkg_op_loaner_ext_rpt.gm_fetch_previous_his", 2);
	     gmDBManager.setString(1, strConsignId);
	     gmDBManager.registerOutParameter(2,OracleTypes.VARCHAR);
	     gmDBManager.execute();
	     strPreExtnReq = GmCommonClass.parseNull(gmDBManager.getString(2));
	     gmDBManager.commit();
  
		 return strPreExtnReq;
} 
  /**
   * fetchHistoryDtls- to Fetch the Loaner Extension Approve/Reject comments
   * 
   * @return ArrayList alDetails
   * @exception AppError
   * @author Agilan
   */
  public ArrayList fetchHistoryDtls(String strReqDtlId,String strFl) throws AppError {
	  HashMap hmValues = new HashMap();
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String gmCmpDtFmt = getGmDataStoreVO().getCmpdfmt();
	  ArrayList alList = new ArrayList();
	  
	  gmDBManager.setPrepareString("gm_pkg_op_loaner_ext_rpt.gm_fch_app_rej_log", 4);
	  gmDBManager.setString(1, strReqDtlId);
	  gmDBManager.setString(2, strFl);
	  gmDBManager.setString(3, gmCmpDtFmt);
	  gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
	  gmDBManager.execute();
	  alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
	  gmDBManager.close();
    
	  return alList;
  }
  
//PMT-50520 Display historical records for previous loaner extensions
  /**
   * fetchLoanerHistory- to Fetch the history detailed Loaner Extension 
   * 
   * @return ArrayList alDetails
   * @exception AppError
   * @author Angeline
   */
  public ArrayList fetchLoanerHistory(String strReqDtlId) throws AppError {
	  HashMap hmValues = new HashMap();
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String gmCmpDtFmt = getGmDataStoreVO().getCmpdfmt();
	  ArrayList alList = new ArrayList();
	  gmDBManager.setPrepareString("gm_pkg_op_loaner_ext_rpt.gm_fch_loaner_log", 2);
	  gmDBManager.setString(1, strReqDtlId);
	  gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	  gmDBManager.execute();
	  alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	  gmDBManager.close();
    
	  return alList;
  }
  
//PMT-50520 Display historical records for previous loaner extensions
}

