package com.globus.sales.Loaners.actions;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.sales.Loaners.beans.GmLoanerExtBean;
import com.globus.sales.Loaners.beans.GmLoanerExtReqRptBean;
import com.globus.sales.Loaners.forms.GmLoanerExtReqRptForm;
import com.globus.common.actions.GmDispatchAction;

/**
 * GmLoanerExtReqAction class for loading, fetching the Loaner Extension Request Reports
 * @author Agilan S
 */
public class GmLoanerExtReqAction extends GmDispatchAction {
	
	/** This used to get the instance of this class for enabling logging */
	Logger log = GmLogger.getInstance(this.getClass().getName());
  
	/**
	 * loadLoanerExtApprl - to Load the Initial Loaner Extension Request details
	 * @param actionMapping, actionForm, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLoanerExtnReqRpt(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmLoanerExtReqRptForm gmLoanerExtReqRptForm = (GmLoanerExtReqRptForm) actionForm;
		    gmLoanerExtReqRptForm.loadSessionParameters(request);
		    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
		    // Get Settings from cookie
		    GmCookieManager gmCookieManager = new GmCookieManager();
		    String strApplDateFmt = "";
		    String strSessTodaysDate = "";
		    String strxmlGridData = "";
		    String strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
		    HashMap hmReturn = new HashMap();
		    ArrayList alRequestDtls = new ArrayList();

		    strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
		    strSessTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
		    String strApplJSDateFmt = (String) session.getAttribute("strSessJSDateFmt");
			int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
	 	    String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplDateFmt);
	 	    String strMonthLastDay = GmCalenderOperations.getCurrentDate(strApplDateFmt);
	 	    gmLoanerExtReqRptForm.setFromDt(strMonthFirstDay);
	 	    gmLoanerExtReqRptForm.setToDt(strMonthLastDay);
		    fetchDropdownValues(gmLoanerExtReqRptForm);
		    
		    String strZoneFilter = GmCommonClass.parseNull(request.getParameter("strZone"));
		    String strRegnFilter = GmCommonClass.parseNull(request.getParameter("strRegion"));
		    String strFieldSalesFilter = GmCommonClass.parseNull(request.getParameter("strFieldSales"));
		    //If Filter is empty get the filter from cookie
		    strZoneFilter =
	    			strZoneFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
	    					request, "chLEZoneFilter")) : strZoneFilter;
	    	strRegnFilter =
	    			strRegnFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
			    			request, "chLERegnFilter")) : strRegnFilter;
	    	strFieldSalesFilter =
	    					strFieldSalesFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
					    			request, "chLEFieldSalesFilter")) : strFieldSalesFilter;
		    
		    // Set Cookies
		    gmCookieManager.setCookieValue(response, "chLEZoneFilter", strZoneFilter);
		    gmCookieManager.setCookieValue(response, "chLERegnFilter", strRegnFilter);
		    gmCookieManager.setCookieValue(response, "chLEFieldSalesFilter", strFieldSalesFilter);		    
		    // Set Filters into the Form Object		    				    
		    gmLoanerExtReqRptForm.setStrRegion(strRegnFilter);
		    gmLoanerExtReqRptForm.setStrZone(strZoneFilter);
			gmLoanerExtReqRptForm.setStrFieldSales(strFieldSalesFilter);		
		    
		return actionMapping.findForward("GmLoanerExtReqRpt");
	}

  /**
   * fetchLoanerExtApprl - to Fetch the Loaner Extension Request details
   * @param actionMapping, actionForm, request, request, response
   * @return
   * @throws Exception
   */
public ActionForward fchLoanerExtnReqRpt(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
			instantiate(request, response);
			GmLoanerExtReqRptForm gmLoanerExtReqRptForm = (GmLoanerExtReqRptForm) actionForm;
			gmLoanerExtReqRptForm.loadSessionParameters(request);
			GmLoanerExtReqRptBean gmLoanerExtReqRptBean = new GmLoanerExtReqRptBean(getGmDataStoreVO());
			// Get Settings from cookie
		    GmCookieManager gmCookieManager = new GmCookieManager();
			String strApplDateFmt = "";
			String strSessTodaysDate = "";
			String strxmlGridData = "";
			String strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
			HashMap hmReturn = new HashMap();
			HashMap hmParam = new HashMap();
			ArrayList alRequestDtls = new ArrayList();

			strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
			strSessTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
			String strApplJSDateFmt = (String) session.getAttribute("strSessJSDateFmt");
			
			String strZoneFilter = GmCommonClass.parseNull(request.getParameter("strZone"));
		    String strRegnFilter = GmCommonClass.parseNull(request.getParameter("strRegion"));
		    String strFieldSalesFilter = GmCommonClass.parseNull(request.getParameter("strFieldSales"));
		    
		    // Set Cookies
		    gmCookieManager.setCookieValue(response, "chLEZoneFilter", strZoneFilter);
		    gmCookieManager.setCookieValue(response, "chLERegnFilter", strRegnFilter);
		    gmCookieManager.setCookieValue(response, "chLEFieldSalesFilter", strFieldSalesFilter);
		  	// Set Filters into the Form Object		
		    gmLoanerExtReqRptForm.setStrRegion(strRegnFilter);
		    gmLoanerExtReqRptForm.setStrZone(strZoneFilter);
		    gmLoanerExtReqRptForm.setStrFieldSales(strFieldSalesFilter);		
			
			hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLoanerExtReqRptForm));
			String strJsonValues = GmCommonClass.parseNull(gmLoanerExtReqRptBean.fchLoanerExtnReqRpt(hmParam));
			gmLoanerExtReqRptForm.setReqJsonString(strJsonValues);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonValues.equals("")) {
				pw.write(strJsonValues);
			}
			pw.flush();
			return null;
}
/**
 * fetchExtensionDtls - to fetch previous extension status is open or not
 * @param actionMapping, actionForm, request, response
 * @return
 * @throws Exception
 */
public ActionForward fetchExtensionDtls(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
		instantiate(request, response);
		
		GmLoanerExtReqRptForm gmLoanerExtReqRptForm = (GmLoanerExtReqRptForm) actionForm;
		gmLoanerExtReqRptForm.loadSessionParameters(request);
		GmLoanerExtReqRptBean gmLoanerExtReqRptBean = new GmLoanerExtReqRptBean(getGmDataStoreVO());
		String strResult = "";
		HashMap hmParam = new HashMap();
			    
		hmParam = GmCommonClass.getHashMapFromForm(gmLoanerExtReqRptForm);
		strResult = gmLoanerExtReqRptBean.checkLoanerExtReq(hmParam);
			
		if (!strResult.equals("")){
			response.setContentType("text/plain");
		    PrintWriter pw = response.getWriter();
			pw.write(strResult);
			pw.flush();
			return null;
		}
		return null;
}

/**
 * fetchHistoryDtls - to Fetch the previous Loaner Extension request details before approval
 * @param actionMapping, actionForm, request, request, response
 * @return
 * @throws Exception
 */
public ActionForward fetchHistoryDtls(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
    HttpServletResponse response) throws Exception {
			instantiate(request, response);
			GmLoanerExtReqRptForm gmLoanerExtReqRptForm = (GmLoanerExtReqRptForm) actionForm;
			gmLoanerExtReqRptForm.loadSessionParameters(request);
			GmLoanerExtReqRptBean gmLoanerExtReqRptBean = new GmLoanerExtReqRptBean(getGmDataStoreVO());
	
			HashMap hmValues = new HashMap();
			ArrayList alList = new ArrayList();
			String strReqDtlId = GmCommonClass.parseNull((String) request.getParameter("ReddtlId"));
			String strfl = GmCommonClass.parseNull((String) request.getParameter("fl"));
			gmLoanerExtReqRptForm.setStrfl(strfl);
			alList = GmCommonClass.parseNullArrayList((ArrayList)(gmLoanerExtReqRptBean.fetchHistoryDtls(strReqDtlId,strfl)));
			gmLoanerExtReqRptForm.setHmHistoryDtl(alList);

		return actionMapping.findForward("GmLoanerHisDtl");
}

//PMT-50520 Display historical records for previous loaner extensions

/**
 * fetchLoanerHistory - to Fetch the historical Loaner Extension request details 
 * @param actionMapping, actionForm, request, request, response
 * @return
 * @throws Exception
 * @author Angeline
 */
public ActionForward fetchLoanerHistory(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
    HttpServletResponse response) throws Exception {
			instantiate(request, response);
			GmLoanerExtReqRptForm gmLoanerExtReqRptForm = (GmLoanerExtReqRptForm) actionForm;
			gmLoanerExtReqRptForm.loadSessionParameters(request);
			GmLoanerExtReqRptBean gmLoanerExtReqRptBean = new GmLoanerExtReqRptBean(getGmDataStoreVO());
	
			HashMap hmValues = new HashMap();
			ArrayList alList = new ArrayList();
			HashMap hmParam = new HashMap();
			
			String strReqDtlId = GmCommonClass.parseNull((String) request.getParameter("ReddtlId"));
			alList = GmCommonClass.parseNullArrayList((ArrayList)(gmLoanerExtReqRptBean.fetchLoanerHistory(strReqDtlId)));
			gmLoanerExtReqRptForm.setHmHistoryDtl(alList);
			

		return actionMapping.findForward("GmLoanerHisLogDtl");
}
//PMt-50520
  /**
   * fetchdropdownvalues - to load division, zone and region values
   * @param 
   * @return
   * @throws AppError
   */ 
  public void fetchDropdownValues(GmLoanerExtReqRptForm gmLoanerExtReqRptForm) throws AppError{
	
	    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
        String strCompId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());       
        gmLoanerExtReqRptForm.setAlZone(gmLoanerExtBean.getZone(strCompId));  
        gmLoanerExtReqRptForm.setAlRegions(gmLoanerExtBean.getRegion(strCompId));
        gmLoanerExtReqRptForm.setAlFieldSales(gmLoanerExtBean.getFieldSales(strCompId));
  }
}
