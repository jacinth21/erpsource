package com.globus.sales.Loaners.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.sales.Loaners.beans.GmLoanerExtBean;
import com.globus.sales.Loaners.forms.GmLoanerExtApprlForm;
import com.globus.common.actions.GmDispatchAction;

/**
 * GmLoanerExtApprlAction class for loading, fetching, saving, rejecting the Loaner Extension details
 * @author N RAJA
 */
public class GmLoanerExtApprlAction extends GmDispatchAction {
	
	/** This used to get the instance of this class for enabling logging */
	Logger log = GmLogger.getInstance(this.getClass().getName());
  
	/**
	 * loadLoanerExtApprl - to Load the Initial Loaner Extension Request details
	 * @param actionMapping, actionForm, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLoanerExtApprl(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmLoanerExtApprlForm gmLoanerExtApprlForm = (GmLoanerExtApprlForm) actionForm;
		    gmLoanerExtApprlForm.loadSessionParameters(request);
		    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
		    // Get Settings from cookie
		    GmCookieManager gmCookieManager = new GmCookieManager();
		    fetchDropdownValues(gmLoanerExtApprlForm);
		    
		    String strZoneFilter = GmCommonClass.parseNull(request.getParameter("strZone"));
		    String strRegnFilter = GmCommonClass.parseNull(request.getParameter("strRegion"));
		    String strFieldSalesFilter = GmCommonClass.parseNull(request.getParameter("strFieldSales"));
		    //If Filter is empty get the filter from cookie
		    strZoneFilter =
	    			strZoneFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
	    					request, "chLEZoneFilter")) : strZoneFilter;
	    	strRegnFilter =
	    			strRegnFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
			    			request, "chLERegnFilter")) : strRegnFilter;
	    	strFieldSalesFilter =
	    					strFieldSalesFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
					    			request, "chLEFieldSalesFilter")) : strFieldSalesFilter;
		    
		    // Set Cookies
		    gmCookieManager.setCookieValue(response, "chLEZoneFilter", strZoneFilter);
		    gmCookieManager.setCookieValue(response, "chLERegnFilter", strRegnFilter);
		    gmCookieManager.setCookieValue(response, "chLEFieldSalesFilter", strFieldSalesFilter);		    
		    // Set Filters into the Form Object		    				    
		    gmLoanerExtApprlForm.setStrRegion(strRegnFilter);
			gmLoanerExtApprlForm.setStrZone(strZoneFilter);
			gmLoanerExtApprlForm.setStrFieldSales(strFieldSalesFilter);				    

		return actionMapping.findForward("GmLoanerExtApproval");
	}

	/**
	 * saveLoanerExtApprl - to Save the Loaner Extension Request details
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
  public ActionForward saveLoanerExtApprl(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmLoanerExtApprlForm gmLoanerExtApprlForm = (GmLoanerExtApprlForm) actionForm;
		    gmLoanerExtApprlForm.loadSessionParameters(request);
		    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
		    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());  
		    String strSessTodaysDate = "";
		    String strApplDateFmt = "";
		    String strSessCompanyLocale = "";
		    String strResult = "";
		    HashMap hmParam = new HashMap();
		    HashMap hmReturn = new HashMap();
		    
		    strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
		    strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
		    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
		    strSessTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
		    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerExtApprlForm);
		    hmParam.put("DATEFMT", strApplDateFmt);
		    hmParam.put("TODAYSDATE", strSessTodaysDate);
		    hmParam.put("APPROVEFL", "Y");
		    strResult = gmLoanerExtBean.saveLoanerExtAppv(hmParam);
	    
	        if (!strResult.equals("")){
	        	response.setContentType("text/plain");
	        	PrintWriter pw = response.getWriter();
			    pw.write(strResult);
			    pw.flush();
			    return null;
	        }
	        return null;
  }
  /**
   * fetchLoanerExtApprl - to Fetch the Loaner Extension Request details
   * @param actionMapping, actionForm, request, request, response
   * @return
   * @throws Exception
   */
public ActionForward fetchLoanerExtApprl(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
			instantiate(request, response);
			GmLoanerExtApprlForm gmLoanerExtApprlForm = (GmLoanerExtApprlForm) actionForm;
			gmLoanerExtApprlForm.loadSessionParameters(request);
			GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
			// Get Settings from cookie
		    GmCookieManager gmCookieManager = new GmCookieManager();
			String strApplDateFmt = "";
			String strSessTodaysDate = "";
			String strxmlGridData = "";
			String strCmpId = "";
			String strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
			HashMap hmParam = new HashMap();
			ArrayList alRequestDtls = new ArrayList();

			strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
			strCmpId =  GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
			strSessTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
			String strApplJSDateFmt = (String) session.getAttribute("strSessJSDateFmt");	
		    
		    String strZoneFilter = GmCommonClass.parseNull(request.getParameter("strZone"));
		    String strRegnFilter = GmCommonClass.parseNull(request.getParameter("strRegion"));
		    String strFieldSalesFilter = GmCommonClass.parseNull(request.getParameter("strFieldSales"));
		    
		    // Set Cookies
		    gmCookieManager.setCookieValue(response, "chLEZoneFilter", strZoneFilter);
		    gmCookieManager.setCookieValue(response, "chLERegnFilter", strRegnFilter);
		    gmCookieManager.setCookieValue(response, "chLEFieldSalesFilter", strFieldSalesFilter);
		  	// Set Filters into the Form Object		
			gmLoanerExtApprlForm.setStrRegion(strRegnFilter);
			gmLoanerExtApprlForm.setStrZone(strZoneFilter);
			gmLoanerExtApprlForm.setStrFieldSales(strFieldSalesFilter);
			
			hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLoanerExtApprlForm));
			hmParam.put("CMPID", strCmpId);
			String strJsonValues = GmCommonClass.parseNull(gmLoanerExtBean.fetchPendingApprlLoanerExtDtls(hmParam));
			gmLoanerExtApprlForm.setReqJsonString(strJsonValues);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonValues.equals("")) {
				pw.write(strJsonValues);
			}
			pw.flush();
			return null;
}
    /**
     * rejectLoanerExtApprl - to Reject Loaner Extension Details 
     * @param actionMapping, actionForm, request, response
     * @return
     * @throws Exception
     */
  public ActionForward rejectLoanerExtApprl(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
			    instantiate(request, response);
			    GmLoanerExtApprlForm gmLoanerExtApprlForm = (GmLoanerExtApprlForm) actionForm;
			    gmLoanerExtApprlForm.loadSessionParameters(request);
			    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
			    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());  
			    String strSessTodaysDate = "";
			    String strApplDateFmt = "";
			    String strSessCompanyLocale = "";
			    String strResult = "";
			    HashMap hmParam = new HashMap();
			    HashMap hmReturn = new HashMap();
			    
			    strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
			    strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
			    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
			    strSessTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
			    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerExtApprlForm);
			    hmParam.put("DATEFMT", strApplDateFmt);
			    hmParam.put("TODAYSDATE", strSessTodaysDate);
			    strResult = gmLoanerExtBean.rejectLoanerExtAppv(hmParam);
			
		      if (!strResult.equals("")){
		      	response.setContentType("text/plain");
		      	PrintWriter pw = response.getWriter();
				    pw.write(strResult);
				    pw.flush();
				    return null;
		      }
		      return null;
  }
  
  /**
   * fetchdropdownvalues - to load division, zone and region values
   * @param 
   * @return
   * @throws AppError
   */ 
  public void fetchDropdownValues(GmLoanerExtApprlForm gmLoanerExtApprlForm) throws AppError{
	    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
        String strCompId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
	    gmLoanerExtApprlForm.setAlZone(gmLoanerExtBean.getZone(strCompId));  
	    gmLoanerExtApprlForm.setAlRegions(gmLoanerExtBean.getRegion(strCompId));
	    gmLoanerExtApprlForm.setAlFieldSales(gmLoanerExtBean.getFieldSales(strCompId));
  }
  
  /**
	 * sendLoanerExtnEmail - to send the Loaner Extension Approval or Reject Email
	 * @param actionMapping, actionForm, request, response
	 * @return
	 * @throws Exception
	 */
  public ActionForward sendLoanerExtnEmail(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
	  instantiate(request, response);
	    GmLoanerExtApprlForm gmLoanerExtApprlForm = (GmLoanerExtApprlForm) actionForm;
	    gmLoanerExtApprlForm.loadSessionParameters(request);
	    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
	    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerExtApprlForm);
	    String strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
	    String strSessTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
	    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
	    String strStatus=GmCommonClass.parseNull(request.getParameter("strStatusId"));
	    hmParam.put("COMPANYINFO", strCompanyInfo);
	    hmParam.put("STATUS", strStatus);
	    hmParam.put("TODAYSDATE", strSessTodaysDate);
	    gmLoanerExtBean.sendLoanerExtnEmailJMS(hmParam);
	    return null;
  }
}
