package com.globus.sales.pricing.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.pricing.beans.GmPartPriceAdjBean;
import com.globus.sales.pricing.beans.GmPartPriceAdjRptBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.sales.pricing.forms.GmPartPriceAdjRptForm;

public class GmPartPriceAdjRptAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /*
   * fetchGrpAccHeaderDetails(): Method is written for populating the all the Drop down values
   * available in the JSP. ANd setting the Default values to the corresponding fields. Author:
   * HReddi
   */

  public ActionForward fetchGrpAccHeaderDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartPriceAdjRptForm gmPartPriceAdjRptForm = (GmPartPriceAdjRptForm) form;
    gmPartPriceAdjRptForm.loadSessionParameters(request);
    GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjRptForm);

    ArrayList alType = new ArrayList();
    ArrayList alAccName = new ArrayList();
    ArrayList alGrpAccName = new ArrayList();
    ArrayList alProjectList = new ArrayList();
    ArrayList alSystemList = new ArrayList();
    ArrayList alLikeOptions = new ArrayList();
    ArrayList alQyeryTypeList = new ArrayList();

    String strQueryType = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getQueryByType());
    String strSystemString = GmCommonClass.parseNull((String) hmParam.get("SYSTEMLIST"));
    String strProjectString = GmCommonClass.parseNull((String) hmParam.get("PROJECTLIST"));

    alLikeOptions = GmCommonClass.getCodeList("LIKES");
    gmPartPriceAdjRptForm.setAlLikeOptions(alLikeOptions);

    alQyeryTypeList = GmCommonClass.getCodeList("PRCFLT");
    gmPartPriceAdjRptForm.setAlQueryTypeList(alQyeryTypeList);

    if (strQueryType.equals("0")) {
      gmPartPriceAdjRptForm.setQueryByType("120001");
    }

    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301");
    alProjectList = gmProject.reportProject(hmTemp);
    alSystemList = gmPricingRequestBean.loadSystemsList();
    alType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PADJTP"));
    alAccName = GmCommonClass.parseNullArrayList(gmSalesBean.reportAccount());
    alGrpAccName =
        GmCommonClass.parseNullArrayList(gmPartyInfoBean.fetchPartyNameList("7003", "Y"));
    gmPartPriceAdjRptForm.setAlType(alType);
    gmPartPriceAdjRptForm.setAlAccName(alAccName);
    gmPartPriceAdjRptForm.setAlGrpAccName(alGrpAccName);
    gmPartPriceAdjRptForm.setAlProjectList(alProjectList);
    gmPartPriceAdjRptForm.setAlSystemList(alSystemList);
    gmPartPriceAdjRptForm.setAccId(GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId()));
    gmPartPriceAdjRptForm.setAccName(GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId()));
    gmPartPriceAdjRptForm.setGrpAccName(GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId()));
    gmPartPriceAdjRptForm.setSystemList(strSystemString);
    gmPartPriceAdjRptForm.setProjectList(strProjectString);

    return mapping.findForward("gmPartPriceAdjustment");
  }

  /*
   * fetchPartPriceAdjDetails(): Method is written for fetching the Account/Group Book Part Price
   * Taking the all the Filter Values from the JSP and passing the same to the Bean Method. after
   * bean method communicates with DB and returned the result to here and the same result are
   * sending back to JSP. Author: HReddi
   */

  public ActionForward fetchPartPriceAdjDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartPriceAdjRptForm gmPartPriceAdjRptForm = (GmPartPriceAdjRptForm) form;
    gmPartPriceAdjRptForm.loadSessionParameters(request);
    GmPartPriceAdjRptBean gmPartPriceAdjRptBean = new GmPartPriceAdjRptBean(getGmDataStoreVO());
    RowSetDynaClass rdresult = null;
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjRptForm);
    String strPartNumber = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getPartNum());
    String strPartLike = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getPartLike());
    String strQueryType = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getQueryByType());
    String strPartNumFormat = "";
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    if (!strPartNumber.equals(""))
      strPartNumFormat = formatPartNumber(strPartNumber, strPartLike);
    String strQueryByList = "";
    String strSystemString = GmCommonClass.parseNull((String) hmParam.get("SYSTEMLIST"));
    String strProjectString = GmCommonClass.parseNull((String) hmParam.get("PROJECTLIST"));
    strQueryByList = formatQueryNames(strSystemString, strProjectString, strQueryType);

    hmParam.put("PARTNUM", strPartNumFormat);
    hmParam.put("SYSPROJLIST", strQueryByList);
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    alResult = gmPartPriceAdjRptBean.fetchPartPriceAdjDetails(hmParam);
    hmReturn.put("PARTADJDETAILS", alResult);
    hmReturn.put("APPLNDATEFMT", strApplDateFmt);
    hmReturn.put("gmDataStoreVO", getGmDataStoreVO());

    String strxmlGridData = generateOutPut(hmReturn);
    gmPartPriceAdjRptForm.setXmlGridData(strxmlGridData);

    gmPartPriceAdjRptForm.setAccId(GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId()));
    gmPartPriceAdjRptForm.setAccName(GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId()));
    gmPartPriceAdjRptForm.setGrpAccName(GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId()));
    gmPartPriceAdjRptForm.setSystemList(strSystemString);
    gmPartPriceAdjRptForm.setProjectList(strProjectString);
    gmPartPriceAdjRptForm.setQueryByType(strQueryType);
    return fetchGrpAccHeaderDetails(mapping, form, request, response);
  }


  /**
   * generateOutPut xml content generation for Set Cost Report grid
   * 
   * @param ArrayList
   * @return String
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmPartPriceAdjReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPartPriceAdjRpt.vm");
    return templateUtil.generateOutput();
  }

  /*
   * fetchHeaderDetails(): For Populating the Header section based on the Account Name/Group Price
   * Book Name. on Changing the Account Name/Group Name, populating the Header section details.
   * Author: HReddi
   */

  public void fetchHeaderDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    HashMap hmHeaderValues = new HashMap();

    GmPartPriceAdjRptForm gmPartPriceAdjRptForm = (GmPartPriceAdjRptForm) form;
    gmPartPriceAdjRptForm.loadSessionParameters(request);
    GmPartPriceAdjBean gmPartPriceAdjBean = new GmPartPriceAdjBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmCart = new HashMap();
    String strSystemString = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getSystemList());
    String strProjectString = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getProjectList());

    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjRptForm);

    gmPartPriceAdjRptForm.setSystemList(strSystemString);
    gmPartPriceAdjRptForm.setProjectList(strProjectString);
    try {
      StringBuffer sbXML = new StringBuffer();
      hmHeaderValues =
          GmCommonClass.parseNullHashMap(gmPartPriceAdjBean.loadPartPriceAdjHeader(hmParam));
      hmCart = GmCommonClass.parseNullHashMap((HashMap) hmHeaderValues.get("ACCSUMMARY"));

      String strAccId = "";
      String strRepId = "";
      String strGpoId = "";
      String strRegn = "";
      String strAdName = "";
      String strDistName = "";
      String strTerrName = "";
      String strRepNm = "";
      String strGpoNm = "";
      String strGpoCatId = "";
      String strAccName = "";
      String strDistId = "";
      String strShipMode = "";
      String strGprAff = "";
      String strHlcAff = "";
      String strAccType = "";
      String strAccComments = "";
      String strAccCurrSign = "";
      String strShipCarr = "";
      int intSize = 0;

      if (!hmCart.isEmpty()) {
        strRegn = GmCommonClass.replaceForXML((String) hmCart.get("RGNAME"));
        strAdName = GmCommonClass.replaceForXML((String) hmCart.get("ADNAME"));
        strDistName = GmCommonClass.replaceForXML((String) hmCart.get("DNAME"));
        strTerrName = GmCommonClass.replaceForXML((String) hmCart.get("TRNAME"));
        strRepNm = GmCommonClass.parseNull((String) hmCart.get("RPNAME"));
        strRepId = GmCommonClass.parseNull((String) hmCart.get("REPID"));
        strGpoNm = GmCommonClass.parseNull((String) hmCart.get("PRNAME"));
        strGpoId = GmCommonClass.parseNull((String) hmCart.get("PRID"));
        strGpoCatId = GmCommonClass.parseNull((String) hmCart.get("GPOID"));
        strAccName = GmCommonClass.replaceForXML((String) hmCart.get("ANAME"));
        strAccId = GmCommonClass.parseNull((String) hmCart.get("ACCID"));
        strDistId = GmCommonClass.parseNull((String) hmCart.get("DISTID"));
        strShipMode = GmCommonClass.parseNull((String) hmCart.get("SHIPMODE"));
        strShipCarr = GmCommonClass.parseNull((String) hmCart.get("SHIPCARR"));
        // MNTTASK - 6074:Account Affiliation
        strGprAff = GmCommonClass.parseNull((String) hmCart.get("GPRAFF"));
        strHlcAff = GmCommonClass.parseNull((String) hmCart.get("HLCAFF"));
        strAccType = GmCommonClass.parseNull((String) hmCart.get("ACCTYPE"));
        strAccComments = GmCommonClass.parseNull((String) hmCart.get("ACCCOMMENTS"));
        strAccCurrSign = GmCommonClass.parseNull((String) hmCart.get("ACC_CURRENCY")); // Currency
                                                                                       // symbol
                                                                                       // added
        if (!strAccComments.equals("")) {
          strAccComments = GmCommonClass.replaceForXML((String) hmCart.get("ACCCOMMENTS"));
        }
        sbXML.append("<data>");
        sbXML.append("<acc>");
        sbXML.append("<regn>");
        sbXML.append(strRegn);
        sbXML.append("</regn>");
        sbXML.append("<adname>");
        sbXML.append(strAdName);
        sbXML.append("</adname>");
        sbXML.append("<dname>");
        sbXML.append(strDistName);
        sbXML.append("</dname>");
        sbXML.append("<trname>");
        sbXML.append(strTerrName);
        sbXML.append("</trname>");
        sbXML.append("<rpname>");
        sbXML.append(strRepNm);
        sbXML.append("</rpname>");
        sbXML.append("<repid>");
        sbXML.append(strRepId);
        sbXML.append("</repid>");
        sbXML.append("<prname>");
        sbXML.append(strGpoNm);
        sbXML.append("</prname>");
        sbXML.append("<prid>");
        sbXML.append(strGpoId);
        sbXML.append("</prid>");
        sbXML.append("<gpocatid>");
        sbXML.append(strGpoCatId);
        sbXML.append("</gpocatid>");
        sbXML.append("<aname>");
        sbXML.append(strAccName);
        sbXML.append("</aname>");
        sbXML.append("<accid>");
        sbXML.append(strAccId);
        sbXML.append("</accid>");
        sbXML.append("<distid>");
        sbXML.append(strDistId);
        sbXML.append("</distid>");
        sbXML.append("<ShipMode>");
        sbXML.append(strShipMode);
        sbXML.append("</ShipMode>");
        sbXML.append("<GprAff>");
        sbXML.append(strGprAff);
        sbXML.append("</GprAff>");
        sbXML.append("<HlcAff>");
        sbXML.append(strHlcAff);
        sbXML.append("</HlcAff>");
        sbXML.append("<accType>");
        sbXML.append(strAccType);
        sbXML.append("</accType>");
        sbXML.append("<accComments>");
        sbXML.append(strAccComments);
        sbXML.append("</accComments>");
        sbXML.append("<acccurrency>");
        sbXML.append(strAccCurrSign);
        sbXML.append("</acccurrency>");
        sbXML.append("<ShipCarr>");
        sbXML.append(strShipCarr);
        sbXML.append("</ShipCarr>");
        sbXML.append("</acc>");
        sbXML.append("</data>");
      }
      response.setContentType("text/xml;charset=utf-8");
      response.setHeader("Cache-Control", "no-cache");
      gmPartPriceAdjRptForm.setAccName(gmPartPriceAdjRptForm.getAccId());
      gmPartPriceAdjRptForm
          .setGrpAccName(GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId()));
      PrintWriter writer = response.getWriter();
      writer.println(sbXML.toString());
      log.debug("sbXML.toString()::" + sbXML.toString());
      writer.close();
    } catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      String strDispatchTo = strComnPath + "/GmError.jsp";
      // gotoPage(strDispatchTo,request,response);
    }// End of catch
  }

  /*
   * fetchPartPriceHystoryDetails(): For Populating the Part Price Adjustment History. Taking the
   * Party ID as In-Param and sending it to Bean method. Author: HReddi
   */

  public ActionForward fetchPartPriceHystoryDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartPriceAdjRptForm gmPartPriceAdjRptForm = (GmPartPriceAdjRptForm) form;
    gmPartPriceAdjRptForm.loadSessionParameters(request);
    GmPartPriceAdjRptBean gmPartPriceAdjRptBean = new GmPartPriceAdjRptBean(getGmDataStoreVO());
    RowSetDynaClass rdresult = null;
    HashMap hmParam = new HashMap();
    List alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjRptForm);
    String strPartyID = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getPartyId());
    String strPartNum = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getPartNum());
    hmParam.put("PARTYID", strPartyID);
    hmParam.put("PARTNUM", strPartNum);
    rdresult = gmPartPriceAdjRptBean.fetchPartPriceHistoryDetails(hmParam);
    alResult = rdresult.getRows();

    gmPartPriceAdjRptForm.setLdtHistoryResult(alResult);
    return mapping.findForward("gmPartPriceAdjustmentHistory");
  }

  /*
   * formatPartNumber(): Formating the part number based on the wild-card search option, IF we
   * select Like - Prefix, we are returning the part like %partnumber IF we select Like - Suffix, we
   * are returning the part like partnumber% ELSE we are simply returning the Input part. Author:
   * HReddi
   */
  public String formatPartNumber(String strPartNUmber, String strPartLike) throws AppError,
      Exception {

    if (strPartLike.equals("90182")) { // Suffix
      strPartNUmber = "%" + strPartNUmber;
    } else if (strPartLike.equals("90181")) { // Prefix
      strPartNUmber = strPartNUmber + "%";
    } else {
      strPartNUmber = strPartNUmber;
    }
    return strPartNUmber;
  }

  /*
   * formatQueryNames(): Removing the Last comma from the In-Put comma separated System/Project
   * Names Return the System Names and Project Names list as one based on the Query Type Author:
   * HReddi
   */

  public String formatQueryNames(String strSystemList, String strProjectList, String strQueryType)
      throws AppError, Exception {
    String strQueryList = "";
    if (!strSystemList.equals("")) {
      strSystemList = strSystemList.substring(0, strSystemList.length() - 1);
    }
    if (!strProjectList.equals("")) {
      strProjectList = strProjectList.substring(0, strProjectList.length() - 1);
    }
    strQueryList = strProjectList;
    if (strQueryType.equals("120001")) { // SYSTEM NAME
      strQueryList = strSystemList;
    }
    return strQueryList;
  }

  /*
   * fetchPartPriceHystoryDetails(): For Populating the Part Price Adjustment History. Taking the
   * Party ID as In-Param and sending it to Bean method. Author: HReddi
   */

  public ActionForward fetchAdjustmentDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartPriceAdjRptForm gmPartPriceAdjRptForm = (GmPartPriceAdjRptForm) form;
    gmPartPriceAdjRptForm.loadSessionParameters(request);
    GmDOBean gmDOBean = new GmDOBean();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmHeaderDetails = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alReturn = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjRptForm);
    String strOrderId = GmCommonClass.parseNull(gmPartPriceAdjRptForm.getAccId());
    String strCurrSymbol =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol"));
    HashMap hmInParams = new HashMap();
    hmInParams.put("ORDERID", strOrderId);
    hmInParams.put("ACTIONTYPE", "AdjPopup");
    hmInParams.put("gmDataStoreVO", getGmDataStoreVO());
    alResult = gmDOBean.fetchCartDetails(hmInParams);
    alReturn = gmDOBean.fetchOrderSummary(strOrderId);
    hmHeaderDetails.put("ORDERSUMMARY", alReturn);
    hmReturn.put("ADJUSTMENTDATA", alResult);
    hmReturn.put("CURRSYMBOL", strCurrSymbol);
    String strxmlGridData = generateAdjustmentOutPut(hmReturn);
    gmPartPriceAdjRptForm.setXmlGridData(strxmlGridData);
    gmPartPriceAdjRptForm.setLdtHistoryResult(alResult);
    request.setAttribute("HEADERDETAILS", hmHeaderDetails);
    return mapping.findForward("gmSalesOrderAdjDetails");
  }

  /**
   * generateAdjustmentOutPut xml content generation for Set Cost Report grid
   * 
   * @param ArrayList
   * @return String
   */
  private String generateAdjustmentOutPut(HashMap hmParam) throws AppError {

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmSalesOrderAdjustmentDetails", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setTemplateName("GmSalesOrderAdjustmentRpt.vm");
    return templateUtil.generateOutput();
  }

}
