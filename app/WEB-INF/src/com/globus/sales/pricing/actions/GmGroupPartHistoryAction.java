package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmGroupPartMapForm;
import com.globus.prodmgmnt.beans.GmProjectBean;


public class GmGroupPartHistoryAction extends GmAction {
	
	
	/**
	    * @return org.apache.struts.action.ActionForward
	    * @roseuid 
	    */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

			
			Logger log = GmLogger.getInstance(this.getClass().getName());
			GmGroupPartMapForm gmGroupPartMapForm = (GmGroupPartMapForm) form;
			GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
			HashMap hmParam = new HashMap();
			
			hmParam = GmCommonClass.getHashMapFromForm(gmGroupPartMapForm);
			
			log.debug("hmParam is >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>)"+ hmParam);
			String strPnum = gmGroupPartMapForm.getHpnum();
			String strGroupId = gmGroupPartMapForm.getGroupId();
			RowSetDynaClass rdResult= null;
			
		 	if(!strPnum.equals(""))
			{
		 		rdResult = gmDemandSetupBean.fetchGroupPartHistory(hmParam);
			}
			else
			if(!strGroupId.equals("0"))
			{
				rdResult = gmDemandSetupBean.fetchAllGroupPartHistory(hmParam);
			} 
		 	
		 	if(rdResult==null)
			{
				gmGroupPartMapForm.setLdtResult(new ArrayList());
			}
			 
		 	else
		 	{
		 		gmGroupPartMapForm.setLdtResult(rdResult.getRows());
		 	}
			 
		return mapping.findForward("GmGroupPartHistory");
	}
}
