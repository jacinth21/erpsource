/**
 * 
 */
package com.globus.sales.pricing.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.event.beans.GmEventSetupBean;
import com.globus.sales.pricing.beans.GmPartPriceAdjBean;
import com.globus.sales.pricing.forms.GmPartPriceAdjForm;

/**
 * @author arajan
 * 
 */
public class GmPartPriceAdjSaveAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * savePartPriceAdj To save the transaction details to the database
   * 
   * @exception AppError
   */
  public ActionForward savePartPriceAdj(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartPriceAdjForm gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
    gmPartPriceAdjForm.loadSessionParameters(request);
    GmPartPriceAdjBean gmPartPriceAdjBean = new GmPartPriceAdjBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();

    String strAccId = "";
    String strType = "";
    String strTxnId = "";
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjForm);

    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));

    strTxnId = gmPartPriceAdjBean.savePartPriceAdj(hmParam);
    String strRedirectURL =
        "/gmPartPriceAdjLoad.do?method=fetchPartPriceDtls&type=" + strType + "&accId=" + strAccId
            + "&txnId=" + strTxnId;

    return actionRedirect(strRedirectURL, request);

  }

  /**
   * submitPartPriceAdj To submit the transaction for approval
   * 
   * @exception AppError
   */
  public ActionForward submitPartPriceAdj(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartPriceAdjForm gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
    gmPartPriceAdjForm.loadSessionParameters(request);
    GmPartPriceAdjBean gmPartPriceAdjBean = new GmPartPriceAdjBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();

    String strAccId = "";
    String strType = "";
    String strTxnId = "";
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjForm);

    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));

    strTxnId = gmPartPriceAdjBean.submitPartPriceAdj(hmParam);
    String strRedirectURL =
        "/gmPartPriceAdjLoad.do?method=fetchPartPriceDtls&type=" + strType + "&accId=" + strAccId
            + "&txnId=" + strTxnId;

    return actionRedirect(strRedirectURL, request);

  }

  /**
   * approvePartPriceAdj To approve the transaction
   * 
   * @exception AppError
   */
  public ActionForward approvePartPriceAdj(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmPartPriceAdjForm gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
    gmPartPriceAdjForm.loadSessionParameters(request);
    GmPartPriceAdjBean gmPartPriceAdjBean = new GmPartPriceAdjBean(getGmDataStoreVO());
    String strAccId = "";
    String strType = "";
    String strTxnId = "";
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjForm);
    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strOutString = "";
    strOutString = gmPartPriceAdjBean.approvePartPriceAdj(hmParam);
    String strRedirectURL =
        "/gmPartPriceAdjLoad.do?method=fetchPartPriceDtls&type=" + strType + "&accId=" + strAccId
            + "&txnId=" + strTxnId + "&successMessage=" + strOutString;

    return actionRedirect(strRedirectURL, request);
  }

  /**
   * denyPartPriceAdj To deny the transaction
   * 
   * @exception AppError
   */
  public ActionForward denyPartPriceAdj(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmPartPriceAdjForm gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
    gmPartPriceAdjForm.loadSessionParameters(request);
    GmPartPriceAdjBean gmPartPriceAdjBean = new GmPartPriceAdjBean(getGmDataStoreVO());
    String strAccId = "";
    String strType = "";
    String strTxnId = "";
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjForm);
    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strOutString = "";
    strOutString = gmPartPriceAdjBean.denyPartPriceAdj(hmParam);
    String strRedirectURL =
        "/gmPartPriceAdjLoad.do?method=fetchPartPriceDtls&type=" + strType + "&accId=" + strAccId
            + "&txnId=" + strTxnId + "&successMessage=" + strOutString;
    return actionRedirect(strRedirectURL, request);
  }
}
