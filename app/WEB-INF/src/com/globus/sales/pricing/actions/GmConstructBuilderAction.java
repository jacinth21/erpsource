package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.pricing.beans.GmConstructBuilderBean;
import com.globus.sales.pricing.forms.GmConstructBuilderForm;

public class GmConstructBuilderAction extends Action {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {

      Logger log = GmLogger.getInstance(this.getClass().getName());

      GmConstructBuilderForm gmConstructBuilderForm = (GmConstructBuilderForm) form;
      gmConstructBuilderForm.loadSessionParameters(request);
      GmConstructBuilderBean gmConstructBuilderBean = new GmConstructBuilderBean();
      GmProjectBean gmProjectBean = new GmProjectBean();
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

      String strAction = gmConstructBuilderForm.getHaction();
      String strConstructId = gmConstructBuilderForm.getConstructId();
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));
      String strVmProPath = "properties.labels.sales.pricing.GmConstructBuilder";

      HashMap hmParam = GmCommonClass.getHashMapFromForm(gmConstructBuilderForm);
      HashMap hmTemp = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmAccessfl = new HashMap();
      String strPublishFL = "";
      String accessFlag = "";
      String strVoidAcessFlag = "";
      String strPartyId = gmConstructBuilderForm.getSessPartyId();
      // in case of new construct set the id and name
      if (GmCommonClass.getCheckBoxValue(gmConstructBuilderForm.getNewConstructFlag()).equals("Y")) {
        hmParam.put("CONSTRUCTID", "0");
        hmParam.put("CONSTRUCTNAME", gmConstructBuilderForm.getHconstructName());
      }
      // save the construct details
      if ("save".equals(strAction)) {
        hmParam.put("INPUTSTRING", gmConstructBuilderForm.getHinputString());
        strConstructId = gmConstructBuilderBean.saveConstructDetails(hmParam);
        gmConstructBuilderForm.setNewConstructFlag("");
      }

      // no need to fetch details on click of left link
      if (!"0".equals(strConstructId)) {
        // load the master details
        gmConstructBuilderForm.setConstructId(strConstructId);
        HashMap hmValues =
            GmCommonClass.parseNullHashMap(gmConstructBuilderBean
                .fetchConstructMasterDetails(strConstructId));
        gmConstructBuilderForm =
            (GmConstructBuilderForm) GmCommonClass.getFormFromHashMap(gmConstructBuilderForm,
                hmValues);

        HashMap hmParams = new HashMap();
        hmParams.put("strConstructId", strConstructId);

        ArrayList alResult = gmConstructBuilderBean.fetchConstructGroupsDetails(hmParams);

        for (int i = 0; i < alResult.size(); i++) {
          hmTemp = (HashMap) alResult.get(i);
          strPublishFL = GmCommonClass.parseNull((String) hmTemp.get("PUBLISHFL"));
          if (strPublishFL.equals("Y"))
            break;
        }

        hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1163");
        accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("hmAccess--------->" + hmAccess);
        if (!accessFlag.equals("Y") && strPublishFL.equals("Y")) {
          gmConstructBuilderForm.setStrEnable("disabled");
        }

        gmConstructBuilderForm.setGridXmlData(gmConstructBuilderBean.getXmlGridData(alResult,
            strSessCompanyLocale, strVmProPath));
      } else {
        hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1163");
        accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        if (!accessFlag.equals("Y")) {
          gmConstructBuilderForm.setStrEnable("disabled");
        }
        gmConstructBuilderForm.setGridXmlData(gmConstructBuilderBean.getXmlGridData(null,
            strSessCompanyLocale, strVmProPath));
      }


      hmAccessfl = gmAccessControlBean.getAccessPermissions(strPartyId, "CONSTRUCT_VOID");
      strVoidAcessFlag = GmCommonClass.parseNull((String) hmAccessfl.get("UPDFL"));

      gmConstructBuilderForm.setVoidAccess(strVoidAcessFlag);

      // fetch all systems
      ArrayList alSalesGrp = gmProjectBean.loadSetMap("1600");
      gmConstructBuilderForm.setAlSystemList(alSalesGrp);

      // fetch code id's for construct levels
      ArrayList alConstuctLevelList = GmCommonClass.getCodeList("CNTLVL");
      gmConstructBuilderForm.setAlConstLevelList(alConstuctLevelList);

      // fetch constructs for the system selected
      ArrayList alConstuctList =
          gmConstructBuilderBean.loadConstructsList(gmConstructBuilderForm.getSystemListId());
      gmConstructBuilderForm.setAlConstructList(alConstuctList);

      // fetch statuses of the constructs
      ArrayList alStatusList = GmCommonClass.getCodeList("CNTST");
      gmConstructBuilderForm.setAlStatusList(alStatusList);

      // fetch all code id's group types
      ArrayList alGrpTypeList = GmCommonClass.getCodeList("GRPTYP");
      gmConstructBuilderForm.setAlGrpTypeList(alGrpTypeList);

      // fetch all the groups
      ArrayList alGroupList = gmConstructBuilderBean.loadGroupsList();
      gmConstructBuilderForm.setAlGroupList(alGroupList);


    } catch (Exception exp) {
      exp.printStackTrace();
    }
    return mapping.findForward("GmConstructBuilder");
  }
}
