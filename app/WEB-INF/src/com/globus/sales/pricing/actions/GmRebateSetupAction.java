package com.globus.sales.pricing.actions;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.io.PrintWriter;
import org.apache.struts.action.ActionRedirect;


import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.pricing.beans.GmRebateSetupBean;
import com.globus.sales.pricing.forms.GmRebateSetupForm;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.sales.pricing.beans.GmRebateReportBean;
import com.globus.common.beans.GmAccessControlBean;

/**
 * @author tramasamy
 *
 */
public class GmRebateSetupAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /** loadRebateManagmentDtls - to Load the rebate Management details
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return to JSP
		 * @throws AppError
		 * @throws ServletException
		 * @throws IOException
		 * @throws ParseException
		 */
	 public  ActionForward loadRebateManagmentDtls (ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException, ParseException {
		 // call the instantiate method
		    instantiate(request, response);
		 GmRebateSetupForm gmRebateSetupForm = (GmRebateSetupForm) form;
		 gmRebateSetupForm.loadSessionParameters(request);
		 GmRebateSetupBean gmRebateManagementBean = new GmRebateSetupBean(getGmDataStoreVO());
		 GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(getGmDataStoreVO());
		 GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
		 GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		 HashMap hmParam = new HashMap();
		 HashMap hmResult = new HashMap();
		 HashMap hmAccess = new HashMap();
		 ArrayList alLogReason =new ArrayList();
		 String strOpt="";
		 String strUserId ="";
		 String strAccessFlag ="";
		 String strRebateId ="";
		 String strGrpType = "";
		 hmParam =  GmCommonClass.getHashMapFromForm(gmRebateSetupForm);
		 strOpt = GmCommonClass.parseNull(gmRebateSetupForm.getStrOpt());
		 strRebateId=GmCommonClass.parseNull((String)hmParam.get("REBATEID"));
		 strGrpType =gmRebateSetupForm.getGroupTypeId();
		 gmRebateSetupForm.setGroupTypeId(strGrpType);
		String strPartyId =gmRebateSetupForm.getPartyId();
		log.debug("strPartyId==="+strPartyId);
		 //For Access
		 strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		 hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "REBATE_SAVE_ACCESS")); 
		 strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		 if(strAccessFlag.equals("Y")){
			 gmRebateSetupForm.setAccessFl("Y");
		 }
		 //to set default value for Account type
		 if(strGrpType == ""){
			 gmRebateSetupForm.setGroupTypeId("107461");
		}
		 //get Code Group Value
		 gmRebateSetupForm.setAlAccountType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBGRTY", getGmDataStoreVO())));
		 gmRebateSetupForm.setAlRebateParts(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBPRTY", getGmDataStoreVO())));
		 gmRebateSetupForm.setAlRebateCategory(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBCATG", getGmDataStoreVO())));
		 gmRebateSetupForm.setAlRebateType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBTYPE", getGmDataStoreVO())));
		 gmRebateSetupForm.setAlPayment(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBPYMT", getGmDataStoreVO())));
		 // to fetch the rebate management details 
		 if(strOpt.equals("Load")){
			 hmResult = gmRebateReportBean.fetchRebateManagmentDtls(hmParam);
			strRebateId=GmCommonClass.parseNull((String)hmResult.get("REBATEID"));
		 }
			// to reset the form when hmresult return empty 
		 if(hmResult.size()== 0){
			 gmRebateSetupForm.reset();
		 }
		 // get Comments Log Details
		  alLogReason = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strRebateId, "26240503"));
		  gmRebateSetupForm.setAlLogReasons(alLogReason);
		  GmCommonClass.getFormFromHashMap(gmRebateSetupForm, hmResult) ;
		 
		 return actionMapping.findForward("GmRebateSetup");
	 }
	 
	 /** saveRebateManagmentDtls - to save the rebate Management details
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return forward to loadRebateManagmentDtls
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward saveRebateManagmentDtls (ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException, ParseException {
		 // call the instantiate method
		    instantiate(request, response);
		 GmRebateSetupForm gmRebateSetupForm = (GmRebateSetupForm) form;;
		 gmRebateSetupForm.loadSessionParameters(request);
		 GmRebateSetupBean gmRebateManagementBean = new GmRebateSetupBean(getGmDataStoreVO());
		 GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		 HashMap hmParam = new HashMap();
		 String strRebateId="";
		 String strLogReason ="";
		 String strUserId ="";
		 hmParam =  GmCommonClass.getHashMapFromForm(gmRebateSetupForm);
		 strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		 strLogReason = GmCommonClass.parseNull(gmRebateSetupForm.getTxt_LogReason());
		 strRebateId =  GmCommonClass.parseNull((String)gmRebateManagementBean.saveRebateManagementDtls(hmParam));
		 // to save comments log details
		 if(!strLogReason.equals("")){
		   gmCommonBean.saveLog(strRebateId, strLogReason, strUserId, "26240503");
		 }
		 gmRebateSetupForm.setRebateId(strRebateId);
		String strRedirectURL = "/gmRebateSetup.do?method=loadRebateManagmentDtls&rebateId="+strRebateId+"&strOpt=Load";
		return actionRedirect(strRedirectURL, request);
	 }
	
	
	/**This method is used to load rebate part mapping details
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward loadRebatepartMappingDtls(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response)throws AppError, ServletException,
		      IOException {

		 // call the instantiate method
		 instantiate(request, response);
		 GmRebateSetupForm gmRebateSetupForm = (GmRebateSetupForm) form;
		 gmRebateSetupForm.loadSessionParameters(request);
		 GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		 GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
		 
		 HashMap hmParam = new HashMap();
		 HashMap hmAccess = new HashMap();
		 ArrayList alLogReason=new ArrayList();
		 String strUserId="";
		 String strAccessFlag="";
		 
		 hmParam = GmCommonClass.getHashMapFromForm(gmRebateSetupForm);
		 //For Access
		 strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		 hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "REBATE_SAVE_ACCESS")); 
		 strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
	     gmRebateSetupForm.setAccessFl(strAccessFlag);
		
	
		String strFromDate = GmCommonClass.parseNull((String) hmParam.get("REBATEEFFECTIVEDATE"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("REBATETODATE"));
		String strRebateId = GmCommonClass.parseNull((String) hmParam.get("REBATEID"));
		String strAccountTypeName = GmCommonClass.parseNull((String) hmParam.get("HACCOUNTTYPENAME"));
		String strPartyName = GmCommonClass.parseNull((String) hmParam.get("SEARCHPARTYID"));
		//for Get Log Comments Details
		alLogReason = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strRebateId, "107660"));
		
		
		gmRebateSetupForm.setAlLogReasons(alLogReason);
		gmRebateSetupForm.setHcontractFromDate(strFromDate);
		gmRebateSetupForm.setHcontractToDate(strToDate);
		gmRebateSetupForm.sethRebateId(strRebateId);
		gmRebateSetupForm.sethAccName(strAccountTypeName);
		gmRebateSetupForm.setHpartyName(strPartyName);
		return actionMapping.findForward("GmRebateAddParts");
		 
		 
	 }

	 /**This Method is used to save rebart part number details
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	 public ActionForward saveRebatepartMappingDtls(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response)throws AppError, ServletException,
		      IOException {
		    instantiate(request, response);
			 GmRebateSetupForm gmRebateSetupForm = (GmRebateSetupForm) form;;
			 gmRebateSetupForm.loadSessionParameters(request);
			 GmRebateSetupBean gmRebateManagementBean = new GmRebateSetupBean(getGmDataStoreVO());
			 GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(getGmDataStoreVO());
			 GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
			 GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
			 HashMap hmParam = new HashMap();
			 ArrayList alLogReason=new ArrayList();
			 String strForwardPage = "";
			 String strInvalidParts="";
			 hmParam =  GmCommonClass.getHashMapFromForm(gmRebateSetupForm);
			 String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
			 String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
			 String strRebateId = GmCommonClass.parseNull((String) hmParam.get("HREBATEID"));
			 String strAccName = GmCommonClass.parseNull((String) hmParam.get("HACCNAME"));
			 String strPartyName = GmCommonClass.parseNull((String) hmParam.get("HPARTYNAME"));
			 String strEffectiveDate = GmCommonClass.parseNull((String) hmParam.get("HCONTRACTFROMDATE"));
			 String strContractToDate = GmCommonClass.parseNull((String) hmParam.get("HCONTRACTTODATE"));
			 //for Save Log Comments Details
			 String strLogReason = GmCommonClass.parseNull(gmRebateSetupForm.getTxt_LogReason());
			 if(!strLogReason.equals("")){
			   gmCommonBean.saveLog(strRebateId, strLogReason, strUserId, "107660");
			 }
			 /*Forward to Rebate Add Parts Screen*/
			 String strURL = "/gmRebateSetup.do?method=loadRebatepartMappingDtls&rebateId="+ strRebateId+
					          "&haccountTypeName="+strAccName+"&searchpartyId="+strPartyName+"&rebateEffectiveDate="+strEffectiveDate+
					          "&rebateToDate="+strContractToDate;
			 String strDisplayNm = "Add Parts to Rebate";
	
		     strInvalidParts = gmRebateManagementBean.saveRebatePartMapping(hmParam);
			
			 if(strInvalidParts.equals("")){
			//for throw Success Message
			request.setAttribute("hType", "S");
			request.setAttribute("hRedirectURL", strURL);
			request.setAttribute("hDisplayNm", strDisplayNm);
		    String msg = "Parts successfully added to Rebate "+strRebateId;
			throw new AppError(msg, "", 'S');  
	        }
		
			 return actionMapping.findForward(strForwardPage);
	 }
}
