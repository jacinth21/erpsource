package com.globus.sales.pricing.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.excel.GmExcelManipulation;
import com.globus.sales.pricing.beans.GmPricingFileGenBean;
import com.globus.sales.pricing.forms.GmSalesPricingFileGenForm;

public class GmSalesPricingFileGenAction extends Action {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String strMsg = "";
		synchronized (this){
		
		try {
			log.debug("Default Session Timeout:" + request.getSession().getMaxInactiveInterval());
			request.getSession().setMaxInactiveInterval(12*60*60);
			String strMasterFile = GmCommonClass.getString("PRICEINCREASEMASTERFILE");
			String strBaseDirToUpload = GmCommonClass.getString("PRICEINCREASEUPLOADDIR");
			String strLogo = GmCommonClass.getString("GMLOGO");
			String strTagline = GmCommonClass.getString("GMTAGLINE");
			String strJasperLoc = request.getSession().getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION"));
			String strSign = System.getProperty("ENV_PRICE_SIGNDIR");
			String strSignExt = ".gif";
			String fileExtPdf = ".pdf";
			String fileExtEx = ".xls";
			
			GmSalesPricingFileGenForm gmSalesPricingFileGenForm = (GmSalesPricingFileGenForm) form;
			gmSalesPricingFileGenForm.loadSessionParameters(request);
			String strAcids = GmCommonClass.parseNull(gmSalesPricingFileGenForm.getInputStringAccId());
			
			HashMap hmParam = GmCommonClass.getHashMapFromForm(gmSalesPricingFileGenForm);
			
			GmJasperReport gmJasperReport = null;
			ArrayList alPriceInfoList = new ArrayList();
			ArrayList alPriceHeadList = new ArrayList();
			ArrayList alAccountIdList = new ArrayList();
			HashMap hmReturn = new HashMap();
			String strMsgSuccess ="Files successfully generated for the Account ID(s):";
			String strMsgFailed = "No Systems found to update or Invalid Account Id(s):";
			//String strSuccessAcids = "";
			//String strFailedAcids = "";
			boolean boolSucc = false;
			boolean boolFail = false;
			GmPricingFileGenBean gmFileGenBean= new GmPricingFileGenBean();
			
			String strOpt = gmSalesPricingFileGenForm.getStrOpt();
			log.debug(" stropt " + strOpt);
			
			if (!strOpt.equals("")){
				
			if(strAcids.equals("all")){
				alAccountIdList = gmFileGenBean.fetchAllPriceIncAccids();
				strAcids = ArrayListToString(alAccountIdList);
				log.debug("All strAccIds: " + strAcids);
			}
			String [] strAccIds =GmCommonClass.StringtoArray(strAcids, ",");
			log.debug("strAccIds.length:"+strAccIds.length);
			for(int i=0 ;i <strAccIds.length;i++ ){
				log.debug(" strAccIds: " + strAccIds[i]);	
				hmReturn = gmFileGenBean.fetchPriceInfo(strAccIds[i],gmSalesPricingFileGenForm.getUserId());
				alPriceHeadList=(ArrayList)hmReturn.get("HEAD");
				alPriceInfoList = (ArrayList)hmReturn.get("DETAIL");
				
				
				if(!GmCommonClass.parseNullArrayList(alPriceInfoList).isEmpty()){
					
				HashMap hmPriceInfoFilenm = (HashMap)alPriceHeadList.get(0);
				String strRegionFilenm=(String)hmPriceInfoFilenm.get("RNAME");
				String strAccountIdFilenm=(String)hmPriceInfoFilenm.get("ACCID");
				String strAccountFilenm=(String)hmPriceInfoFilenm.get("ACNAME");
				String strAdName=(String)hmPriceInfoFilenm.get("ADNAME");
				String strAdEmail=(String)hmPriceInfoFilenm.get("EMAILID");
				String strAdPhoneNo=(String)hmPriceInfoFilenm.get("PHONENO");
				String strAdtitle=(String)hmPriceInfoFilenm.get("TITLE");
				String strAdId=(String)hmPriceInfoFilenm.get("ADID");
				String strAcAdd=(String)hmPriceInfoFilenm.get("ACADD");
				
				String strDirs =strBaseDirToUpload + "\\" + strRegionFilenm+"\\"+ strAccountIdFilenm; 
				String strDirPath = createRequiredDirectory(strDirs);
				
				strAccountFilenm = strAccountFilenm.replaceAll("/", "");
				
				String  strFileNameToPopulate = strDirPath +"\\" + strAccountIdFilenm+"_"+ strAccountFilenm+"_"+ GmCalenderOperations.getCurrentDate("yyyyMMdd");
				
				if(GmCommonClass.getCheckBoxValue(gmSalesPricingFileGenForm.getGenPdf()).equals("Y"))
				{
					HashMap hmRptParam = new HashMap();
					
					String pdfFileName =strFileNameToPopulate+fileExtPdf;
					
					String newFileName = createCreateFileVersions(pdfFileName,strDirs,fileExtPdf);
					
					String curDate = GmCalenderOperations.getCurrentDate("MM/dd/yyyy");
					hmRptParam.put("CUR_DATE", curDate);
					hmRptParam.put("ADNAME", strAdName);
					hmRptParam.put("EMAILID", strAdEmail);
					hmRptParam.put("PHONENO", strAdPhoneNo);
					hmRptParam.put("TITLE", strAdtitle);
					hmRptParam.put("SUBREPORT_DIR",strJasperLoc );
					hmRptParam.put("ADID", strSign+strAdId+strSignExt);
					hmRptParam.put("LOGO",strLogo );
					hmRptParam.put("TAGLINE",strTagline );
					hmRptParam.put("ACCID", strAccountIdFilenm);
					hmRptParam.put("ACNAME",strAccountFilenm );
					hmRptParam.put("ACADD",strAcAdd );
					
					log.debug("ADNAME:"+strAdName);
					log.debug("EMAILID:"+strAdEmail);
					log.debug("PHONENO:"+strAdPhoneNo);
					log.debug("TITLE:"+strAdtitle);
					log.debug("ADID:"+strSign+strAdId+strSignExt);
					log.debug("LOGO:"+strLogo);
					log.debug("TAGLINE:"+strTagline);
					log.debug("ACCID:" +strAccountIdFilenm);
					log.debug("ACNAME:"+strAccountFilenm );
					log.debug("ACADD:"+strAcAdd);
					
					
					gmJasperReport = new GmJasperReport();								
					gmJasperReport.setRequest(request);
					gmJasperReport.setResponse(response);
					gmJasperReport.setJasperReportName("/GmSalesPriceIncPDF.jasper");
					gmJasperReport.setHmReportParameters(hmRptParam);
					gmJasperReport.setReportDataList(null);				
					gmJasperReport.exportJasperReportToPdf(newFileName);
				}
	
				if(GmCommonClass.getCheckBoxValue(gmSalesPricingFileGenForm.getGenExcel()).equals("Y"))
				{
					
					
					String excelFileName =strFileNameToPopulate+fileExtEx;
					
					
					String newFileName = createCreateFileVersions(excelFileName,strDirs,fileExtEx);
					GmExcelManipulation.copyFile(strMasterFile, newFileName);
					
					log.debug("strFileNameToPopulate EXCEL:"+newFileName);
					
					GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
					gmExcelManipulation.setExcelFileName(newFileName);
					gmExcelManipulation.setSheetNumber(0);
					gmExcelManipulation.setRowNumber(1);
					gmExcelManipulation.setColumnNumber(2);
					gmExcelManipulation.writeToExcel(strAccountIdFilenm);
					
					gmExcelManipulation.setSheetNumber(0);
					gmExcelManipulation.setRowNumber(2);
					gmExcelManipulation.setColumnNumber(2);		
					gmExcelManipulation.writeToExcel(strAccountFilenm);
					
					//For End of File
					LinkedHashMap hmEndMap = new LinkedHashMap();
					ArrayList alLastLine = new ArrayList();
					hmEndMap.put("PNAME", "--------------------");
					hmEndMap.put("PNUM", "----------------");
					hmEndMap.put("PDESC", "----------------END OF FILE------------");
					hmEndMap.put("UPRICE", "----------------");
					hmEndMap.put("NEWPRICE", "------------------------");
					alLastLine.add(hmEndMap);
					
					gmExcelManipulation.setSheetNumber(0);
					gmExcelManipulation.setRowNumber(alPriceInfoList.size()+4);
					gmExcelManipulation.setColumnNumber(1);
					gmExcelManipulation.setColumnType("UPRICE", GmExcelManipulation.CELL_TYPE_STRING);
					gmExcelManipulation.setColumnType("NEWPRICE", GmExcelManipulation.CELL_TYPE_STRING);
					gmExcelManipulation.writeToExcel(alLastLine);
					
					gmExcelManipulation.setSheetNumber(0);
					gmExcelManipulation.setRowNumber(4);
					gmExcelManipulation.setColumnNumber(1);
					gmExcelManipulation.setLeftBorder(true);
					gmExcelManipulation.setRightBorder(true);
					//gmExcelManipulation.setTopBorder(true);
					//gmExcelManipulation.setBottomBorder(true);
					//gmExcelManipulation.skipBorder("UPRICE");
					//gmExcelManipulation.skipBorder("NEWPRICE");
					gmExcelManipulation.setExtraFormatToColumn("UPRICE", "CURRENCY");
					gmExcelManipulation.setExtraFormatToColumn("NEWPRICE", "CURRENCY");
					gmExcelManipulation.setColumnType("UPRICE", GmExcelManipulation.CELL_TYPE_NUMERIC);
					gmExcelManipulation.setColumnType("NEWPRICE", GmExcelManipulation.CELL_TYPE_NUMERIC);
					
					
					gmExcelManipulation.writeToExcel(alPriceInfoList);
					
					log.debug("Total A/C Processed:"+(i+1));
					}
					strMsgSuccess = strMsgSuccess +","+strAccIds[i];
					boolSucc = true;
				}else{
					strMsgFailed=strMsgFailed+","+strAccIds[i];
					boolFail = true;
				}
				
				
				
			 }
			if(boolSucc && boolFail){
				strMsg = (strMsgSuccess.replaceFirst(",", ""))+"<BR>"+(strMsgFailed.replaceFirst(",", ""))+"</BR>";
			}else if(boolSucc && !boolFail){
				strMsg = strMsgSuccess.replaceFirst(",", "");
			}else{
				strMsg = strMsgFailed.replaceFirst(",", "");
			}
			   
			throw new AppError(strMsg, "",'S');	
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			throw new AppError(strMsg , "",'S');
		}
		};
		return mapping.findForward("GmSalesPricingFileGen");
	}
		
	private String createRequiredDirectory(String strName) {

			File dir = new File(strName);
			if (!dir.exists()) {
				dir.mkdirs();

			} 
			return strName;
		}
	
	private String createCreateFileVersions(String strFileNameToPopulate,String strDirs,String fileExt) {

		if(new File(strFileNameToPopulate).exists()){
			
			File chkFile = new File(strDirs);
			String strFileList[] = chkFile.list();
			//log.debug("strFileList.length"+strFileList.length);
				for(int j=0; j<strFileList.length; j++)
				{
					if(strFileNameToPopulate.endsWith(fileExt)){
						String strTempFileName = strFileNameToPopulate.substring(0, strFileNameToPopulate.lastIndexOf("."))+"_VER_"+(j+1)+fileExt;
						//log.debug("strTempFileName:"+strTempFileName);
							if(!new File(strTempFileName).exists()){
								strFileNameToPopulate = strTempFileName;
								log.debug("strFileNameToPopulate:"+strFileNameToPopulate);
								break;
								
							}
						
					}
				}
		}
		return strFileNameToPopulate;
	}
	
	private String ArrayListToString(List arrList) {
		String strAccids = "";
		HashMap hmAccids = new HashMap();
		Iterator itIds = arrList.iterator();
		while(itIds.hasNext()){
			hmAccids =(HashMap)itIds.next();
			strAccids=strAccids+","+(String)hmAccids.get("ACIDS");
		}
		
		return strAccids.replaceFirst(",", "");
	}

}
