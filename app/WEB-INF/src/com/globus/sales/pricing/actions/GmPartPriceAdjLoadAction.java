/**
 * 
 */
package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.sales.pricing.beans.GmPartPriceAdjBean;
import com.globus.sales.pricing.forms.GmPartPriceAdjForm;

/**
 * @author arajan
 * 
 */
public class GmPartPriceAdjLoadAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadPartPriceDtls To load the filter part to the screen and loading the grid
   * 
   * @exception AppError
   */
  public ActionForward loadPartPriceDtls(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmPartPriceAdjForm gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
    gmPartPriceAdjForm.loadSessionParameters(request);

    GmPartPriceAdjBean gmPartPriceAdjBean = new GmPartPriceAdjBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strType = "";
    String strTxnId = "";
    ArrayList alResult = new ArrayList();
    ArrayList alAdjCodeLst = new ArrayList();
    ArrayList alLogReasons = new ArrayList();

    // Get the value for Adjusment code
    alAdjCodeLst = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ADJCOD"));

    // Load the dropdown values
    loadDropDownList(gmPartPriceAdjForm);

    strType = GmCommonClass.parseNull(gmPartPriceAdjForm.getType());

    strType = strType.equals("") ? "903111" : strType; // 903111: Group Price Book
    strTxnId = GmCommonClass.parseNull(gmPartPriceAdjForm.getTxnId());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alResult = GmCommonClass.parseNullArrayList(gmPartPriceAdjBean.fetchPartPriceAdjDtls(strTxnId));

    HashMap hmTemplateParams = new HashMap();
    hmTemplateParams.put("ADJCOD", alAdjCodeLst);
    hmTemplateParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmTemplateParams.put("VMFILEPATH", "properties.labels.sales.pricing.GmPartPriceAdj");
    hmTemplateParams.put("TEMPLATE", "GmPartPriceAdj.vm");
    hmTemplateParams.put("alResult", alResult);
    hmTemplateParams.put("TEMPLATEPATH", "sales/pricing/templates");

    String strXmlString = gmPartPriceAdjBean.getXmlGridData(hmTemplateParams);
    gmPartPriceAdjForm.setStrXMLGrid(strXmlString);

    alLogReasons = gmCommonBean.getLog(strTxnId, "4000898"); // To get the log details
    gmPartPriceAdjForm.setAlLogReasons(alLogReasons);

    gmPartPriceAdjForm.setType(strType);
    gmPartPriceAdjForm.sethTxnId(strTxnId);

    return mapping.findForward("GmPartPriceAdjInitiate");
  }

  /**
   * loadDropDownList To load the dropdown values
   * 
   * @exception AppError
   */
  public void loadDropDownList(GmPartPriceAdjForm gmPartPriceAdjForm) throws AppError {

    GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    ArrayList alType = new ArrayList();
    ArrayList alAccName = new ArrayList();
    ArrayList alGrpAccName = new ArrayList();
    ArrayList alActions = new ArrayList();
    ArrayList alRemoveCodeIDs = new ArrayList();

    String strSesPartyId = GmCommonClass.parseNull(gmPartPriceAdjForm.getSessPartyId());
    HashMap hmPricingSetupAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
            "PRICE_APPROVE_SUBMT"));
    String strPrcSubmitFl = GmCommonClass.parseNull((String) hmPricingSetupAccess.get("UPDFL"));

    // Get the value of Type drop down
    alType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PADJTP"));
    // Get the value of Account drop down
    alAccName = GmCommonClass.parseNullArrayList(gmSalesBean.reportAccount());
    // Get the value of Group Account drop down
    alGrpAccName =
        GmCommonClass.parseNullArrayList(gmPartyInfoBean.fetchPartyNameList("7003", "Y"));

    alActions = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ADJACT", ""));

    if (!strPrcSubmitFl.equals("Y")) {
      // to remove "Approved" and "Denied" from "Choose Option" drop down if the user is not having
      // access
      alRemoveCodeIDs.add("105302"); // Approved
      alRemoveCodeIDs.add("105303"); // Denied
      alActions =
          GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(
              alRemoveCodeIDs, alActions));
    }
    gmPartPriceAdjForm.setAlType(alType);
    gmPartPriceAdjForm.setAlAccName(alAccName);
    gmPartPriceAdjForm.setAlGrpAccName(alGrpAccName);
    gmPartPriceAdjForm.setAlActions(alActions);
  }

  /**
   * fetchPartPriceDtls To fetch the header part to the screen
   * 
   * @exception AppError
   */
  public ActionForward fetchPartPriceDtls(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmPartPriceAdjForm gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
    gmPartPriceAdjForm.loadSessionParameters(request);

    GmPartPriceAdjBean gmPartPriceAdjBean = new GmPartPriceAdjBean(getGmDataStoreVO());


    HashMap hmParam = new HashMap();
    HashMap hmHeaderDtls = new HashMap();
    HashMap hmAcctDtls = new HashMap();
    String strAccId = "";
    String strType = "";
    String strPartyId = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjForm);
    strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));

    strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    if (strType.equals("")) {
      strType = GmCommonClass.parseNull(gmPartPriceAdjBean.fetchPartyType(strPartyId));
      hmParam.put("TYPE", strType);
    }
    if (strAccId.equals("")) {
      strAccId = GmCommonClass.parseNull(gmPartPriceAdjBean.getAccIdFromPartyId(strPartyId));
      hmParam.put("ACCID", strAccId);
    }

    hmHeaderDtls =
        GmCommonClass.parseNullHashMap(gmPartPriceAdjBean.loadPartPriceAdjHeader(hmParam));
    GmCommonClass.getFormFromHashMap(gmPartPriceAdjForm,
        GmCommonClass.parseNullHashMap((HashMap) hmHeaderDtls.get("ACCSUMMARY")));
    GmCommonClass.getFormFromHashMap(gmPartPriceAdjForm,
        GmCommonClass.parseNullHashMap((HashMap) hmHeaderDtls.get("ACCDTLS")));

    hmAcctDtls = GmCommonClass.parseNullHashMap((HashMap) hmHeaderDtls.get("ACCDTLS"));

    request.setAttribute("INITDATE", hmAcctDtls.get("INITDATE"));
    request.setAttribute("IMPDATE", hmAcctDtls.get("IMPDATE"));
    gmPartPriceAdjForm.setType(strType);
    gmPartPriceAdjForm.setAccId(strAccId);

    return loadPartPriceDtls(mapping, form, request, response);
  }

}
