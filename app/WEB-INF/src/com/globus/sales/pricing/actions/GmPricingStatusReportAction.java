package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.pricing.beans.GmGroupPricingBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.sales.pricing.forms.GmPricingStatusReportForm;

public class GmPricingStatusReportAction extends GmSalesDispatchAction {

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid
   */
  public ActionForward pricingStatusReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmPricingStatusReportForm gmPricingStatusReportForm = (GmPricingStatusReportForm) form;
    gmPricingStatusReportForm.loadSessionParameters(request);

    GmGroupPricingBean gmGroupPricingBean = new GmGroupPricingBean(getGmDataStoreVO());
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alPriceRequestStatus = new ArrayList();
    ArrayList alPendingSystemAccountPrice = new ArrayList();
    String strAccessFilter = "";
    String inputVPIDs = "";
    String strOpt = gmPricingStatusReportForm.getStrOpt();
    String strPendingSystemCount = "UnPriced System(s) ";
    String strPendingGroupCount = "UnPriced Group(s) ";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // String inputAccounts = gmPricingStatusReportForm.getHinputstring() ;
    inputVPIDs = request.getParameter("hinputstring");

    String strDetailFlag = GmCommonClass.parseNull(gmPricingStatusReportForm.getStrDetailFlag());
    String strOption = "";

    // To get the Access Level information from Materialized view
    strAccessFilter = getAccessFilter(request, response);

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);

    // GmCommonClass.parseNullArrayList((ArrayList)hmSalesFilters.get("REGN"));
    // gmPricingStatusReportForm.setAlADId(GmCommonClass.parseNullArrayList((ArrayList)
    // hmSalesFilters.get("REGN")));
    gmPricingStatusReportForm.setAlVPId(GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters
        .get("ZONE")));
    // gmPricingStatusReportForm.setAlDistributor(GmCommonClass.parseNullArrayList((ArrayList)
    // hmSalesFilters.get("DIST")));
    // gmPricingStatusReportForm.setAlAccount(gmPricingRequestBean.loadAccountList(strAccessFilter));
    // gmPricingStatusReportForm.setAlAccount(GmCommonClass.parseNullArrayList((ArrayList)
    // hmSalesFilters.get("ACCT")));
    // gmPricingStatusReportForm.setAlAccount(GmCommonClass.parseNullArrayList((ArrayList)gmCommonBean.getAccountList(strAccessFilter)));
    gmPricingStatusReportForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("PRQSTS")));
    ArrayList alSalesGrp = gmProjectBean.loadSetMap("GROUPPARTMAPPING");
    gmPricingStatusReportForm.setAlSystem(alSalesGrp);
    gmPricingStatusReportForm.setAlGrpDateRange(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("GPPSDR")));

    hmParam = GmCommonClass.getHashMapFromForm(gmPricingStatusReportForm);

    if (strOpt.equals("reload")) {
      HashMap hmPendingSystemAccountPrice = null;
      HashMap hmPendingGroupAccountPrice = null;

      ArrayList alPendingGroupAccountPrice = new ArrayList();

      String strPendingSystemAccountPriceGridXML = "";
      String strPendingGroupAccountPriceGridXML = "";
      String strSelectedVPIDs = inputVPIDs;// GmCommonClass.createInputString(gmPricingStatusReportForm.getCheckAccounts());
      // log.debug(" The values of strSelectedAccts " + strSelectedAccts);
      String strSelectedStatus =
          GmCommonClass.createInputString(gmPricingStatusReportForm.getCheckStatus());
      // String strSelectedSystem =
      // GmCommonClass.createInputString(gmPricingStatusReportForm.getCheckSystems());

      // String strSelectedAD =
      // GmCommonClass.createInputString(gmPricingStatusReportForm.getCheckADs());
      // String strSelectedFS =
      // GmCommonClass.createInputString(gmPricingStatusReportForm.getCheckFSs());
      String strSelectedVP =
          GmCommonClass.createInputString(gmPricingStatusReportForm.getCheckVPs());
      // String strSelectedAC =
      // GmCommonClass.createInputString(gmPricingStatusReportForm.getCheckAccounts());

      // log.debug("strSelectedAD:.........." + strSelectedAD);
      // log.debug("strSelectedFS:.........." + strSelectedFS);
      // log.debug("strSelectedVP:.........." + strSelectedVP);
      // log.debug("strSelectedAC:.........." + strSelectedAC);

      gmPricingStatusReportForm.setSelectedVP(strSelectedVP);
      // gmPricingStatusReportForm.setSelectedAD(strSelectedAD);
      // gmPricingStatusReportForm.setSelectedFS(strSelectedFS);
      // gmPricingStatusReportForm.setSelectedAC(strSelectedAC);
      hmParam.put("INPUTVPIDS", strSelectedVPIDs);
      hmParam.put("STATUS", strSelectedStatus);
      // hmParam.put("SYSTEMS", strSelectedSystem);
      hmParam.put("ACCESSFILTER", strAccessFilter);
      // log.debug("hmParam:.........." + hmParam);
      // Get data for Price Pending By Account
      alPriceRequestStatus = gmGroupPricingBean.fetchPricingStatusReport(hmParam);
      String strPriceRequestStatusGridXML =
          GmCommonClass.replaceForXML(getPriceStatusGrid(alPriceRequestStatus,
              "GmPriceRequestStatusReport.vm", strSessCompanyLocale));

      // MNTTASK-3690 - Commented the method and others for no need the filters and Pending by Group
      // Tab,Pending by System.

      // Get data for Price Pending By System
      /*
       * to add the new method
       * 
       * hmPendingSystemAccountPrice = gmGroupPricingBean.fetchPendingSystemAccountPrice(hmParam);
       * alPendingSystemAccountPrice =
       * (ArrayList)hmPendingSystemAccountPrice.get("ALNOPRICESYSTEM");
       * //log.debug(" alPendingSystemAccountPrice ------- " +alPendingSystemAccountPrice);
       * strPendingSystemCount = strPendingSystemCount + " - "
       * +(String)hmPendingSystemAccountPrice.get("STRPENDINGSYSTEMCOUNT");
       * strPendingSystemAccountPriceGridXML =
       * GmCommonClass.replaceForXML(getPriceStatusGrid(alPendingSystemAccountPrice
       * ,"GmPendingSystemAccountPrice.vm"));
       * 
       * //Get data for Price Pending By Group hmPendingGroupAccountPrice =
       * gmGroupPricingBean.fetchPendingGroupAccountPrice(hmParam); alPendingGroupAccountPrice =
       * (ArrayList)hmPendingGroupAccountPrice.get("ALNOPRICEGROUP"); strPendingGroupCount =
       * strPendingGroupCount + " - "
       * +(String)hmPendingGroupAccountPrice.get("STRPENDINGGROUPCOUNT");
       * //log.debug(" strPendingSystemCount req data " + strPendingGroupCount);
       * strPendingGroupAccountPriceGridXML =
       * GmCommonClass.replaceForXML(getPriceStatusGrid(alPendingGroupAccountPrice
       * ,"GmPendingGroupAccountPrice.vm"));
       */
      gmPricingStatusReportForm.setPriceRequestStatusXML(strPriceRequestStatusGridXML);
      // gmPricingStatusReportForm.setPendingSystemAccountPriceGridXML(strPendingSystemAccountPriceGridXML);
      // gmPricingStatusReportForm.setPendingGroupAccountPriceGridXML(strPendingGroupAccountPriceGridXML);
    }
    // gmPricingStatusReportForm.setPendingSystemAccountCount(strPendingSystemCount);
    // gmPricingStatusReportForm.setPendingGroupAccountCount(strPendingGroupCount);
    strOption = "GmPricingStatusReport";

    /*
     * if (strDetailFlag.equals("Y")) strOption = "GmPricingStatusDetail";
     */

    return mapping.findForward(strOption);
  }


  /**
   * getPriceStatusGrid xml content generation for grid
   * 
   */
  private String getPriceStatusGrid(ArrayList alGridData, String strTemplateName,
      String strSessCompanyLocale) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmPricingStatusReport", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/pricing/templates");

    templateUtil.setTemplateName(strTemplateName);

    return templateUtil.generateOutput();
  }


}
