package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.pricing.beans.GmGroupPricingBean;
import com.globus.sales.pricing.forms.GmPricingAnnualChangeForm;


public class GmPricingAnnualChangeAction extends GmDispatchAction {


  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {


    Logger log = GmLogger.getInstance(this.getClass().getName());
    instantiate(request, response);
    GmPricingAnnualChangeForm gmPricingAnnualChangeForm = (GmPricingAnnualChangeForm) form;
    gmPricingAnnualChangeForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    GmGroupPricingBean gmGroupPricingBean = new GmGroupPricingBean(getGmDataStoreVO());

    String strPartyId = gmPricingAnnualChangeForm.getSessPartyId();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    HashMap hmAccess = new HashMap();
    String accessFlag = "";

    gmPricingAnnualChangeForm.loadSessionParameters(request);
    String strAccessFilter = "";
    String strOpt = gmPricingAnnualChangeForm.getStrOpt();
    // To get the Access Level information from Materialized view
    
    HashMap hmFilter = new HashMap();
    hmFilter.put("FLTR_DIST_BY_COMP","YES");//Filter Distributor Based on company
    HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(hmFilter);
    gmPricingAnnualChangeForm.setAlADId(GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters
        .get("REGN")));
    gmPricingAnnualChangeForm.setAlDistributor(GmCommonClass
        .parseNullArrayList((ArrayList) hmSalesFilters.get("DIST")));
    gmPricingAnnualChangeForm.setAlAccount(GmCommonClass
        .parseNullArrayList((ArrayList) hmSalesFilters.get("ACCT")));
    // gmPricingAnnualChangeForm.setAlAccount(GmCommonClass.parseNullArrayList((ArrayList)gmCommonBean.getAccountList("")));
    gmPricingAnnualChangeForm.setAlChangeType(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("PCHTY")));
    gmPricingAnnualChangeForm.setAlSets(gmProj.loadSetMap("1600")); // set list

    HashMap hmParam = new HashMap();

    // code for access rights
    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1016");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug("Permissions = " + hmAccess);
    if (accessFlag.equals("Y")) {
      gmPricingAnnualChangeForm.setStrEnableSubmit("true");
    }

    if (strOpt.equals("save")) {

      hmParam = GmCommonClass.getHashMapFromForm(gmPricingAnnualChangeForm);
      String strSelectedSets =
          GmCommonClass.createInputString(gmPricingAnnualChangeForm.getCheckSelectedSets());
      // log.debug(" The values of strSelectedSets "+ strSelectedSets);

      hmParam.put("INPUTSETS", strSelectedSets);
      gmGroupPricingBean.saveMultiAcctRequests(hmParam);
    }

    return mapping.findForward("GmPricingAnnualChange");
  }


}
