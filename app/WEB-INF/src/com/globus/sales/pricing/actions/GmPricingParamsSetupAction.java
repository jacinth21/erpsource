package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.sales.pricing.beans.GmPricingParamsSetupBean;
import com.globus.sales.pricing.forms.GmPricingParamsSetupForm;

public class GmPricingParamsSetupAction extends GmDispatchAction {

  GmCustSalesSetupBean gmCust = new GmCustSalesSetupBean();
  String strId = "";
  String strContract = "";

  /**
   * fetchPricingParams - This method will load the pricing parameter setup
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws Exception
   */
  public ActionForward fetchPricingParams(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmPricingParamsSetupForm gmPricingParamsSetupForm = (GmPricingParamsSetupForm) form;
    gmPricingParamsSetupForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmPricingParamsSetupBean gmPricingParamsSetupBean =
        new GmPricingParamsSetupBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alPriceList = new ArrayList();
    ArrayList alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPricingParamsSetupForm);
    String strhScreenType = GmCommonClass.parseNull(gmPricingParamsSetupForm.gethScreenType()); // alternatively
                                                                                                // switch
                                                                                                // the
                                                                                                // screnn
                                                                                                // types
                                                                                                // for
                                                                                                // AccountSetup
                                                                                                // &
                                                                                                // GroupAccountSetup

    gmPricingParamsSetupForm.sethScreenType(strhScreenType);
    String strOpt = gmPricingParamsSetupForm.getStrOpt();

    String strAccId = gmPricingParamsSetupForm.getAccid();
    String strPartyId = gmPricingParamsSetupForm.getPartyId();
    String strSecEventName = "ACC_PRIC_PARAM_SUBMT";

    // Dropdown for Pricelist
    gmPricingParamsSetupForm.setAlWastePriceList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("PLIST")));
    gmPricingParamsSetupForm.setAlRevisionPriceList(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("PLIST")));

    String strSesPartyId = GmCommonClass.parseNull(gmPricingParamsSetupForm.getSessPartyId());

    if (strhScreenType.equals("groupAccountSetup")) {
      strSecEventName = "GPB_PRC_PRMTR_SUBMIT";
    }
    // ACC_PRIC_PARAM_SUBMT - Users in this security group will have the ability to enter Pricing
    // Parameter information

    HashMap hmPricingSetupAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
            strSecEventName));
    String strPrcSubmitFl = GmCommonClass.parseNull((String) hmPricingSetupAccess.get("UPDFL"));
    request.setAttribute("SUBMITACCFL", strPrcSubmitFl);

    if (!strhScreenType.equals("groupAccountSetup")) {
      fetchAccountAffiliationLists(gmPricingParamsSetupForm);
    }


    alResult =
        GmCommonClass.parseNullArrayList(gmPricingParamsSetupBean.fetchPricingParams(hmParam));

    if (alResult.size() > 0) {
      for (int i = 0; i < alResult.size(); i++) {
        HashMap hmResult = (HashMap) alResult.get(i);
        String strAdjValue = "";
        String strPriceVal = "";
        String strPrcAdjId = "";
        int hisCount;
        String strAdjType = (String) hmResult.get("ADJTYPEID");
        if (strAdjType.equals("107970")) { // Wasted
          strAdjValue = (String) hmResult.get("ADJVALUE");
          strPriceVal = (String) hmResult.get("PRICEVALUE");
          strPrcAdjId = (String) hmResult.get("PRICADJID");
          hisCount = (Integer.parseInt((String) hmResult.get("HISCOUNT")));
          gmPricingParamsSetupForm.setWasted(strAdjValue);
          gmPricingParamsSetupForm.setWastedPrice(strPriceVal);
          if (hisCount > 1)
            gmPricingParamsSetupForm.setShowWasedHistory("Y");
          gmPricingParamsSetupForm.setWastePrcAdjID(strPrcAdjId);
        } else if (strAdjType.equals("107971")) { // Revision
          strAdjValue = (String) hmResult.get("ADJVALUE");
          strPriceVal = (String) hmResult.get("PRICEVALUE");
          strPrcAdjId = (String) hmResult.get("PRICADJID");
          hisCount = (Integer.parseInt((String) hmResult.get("HISCOUNT")));
          gmPricingParamsSetupForm.setRevision(strAdjValue);
          gmPricingParamsSetupForm.setRevisionPrice(strPriceVal);
          if (hisCount > 1)
            gmPricingParamsSetupForm.setShowRevisionHistory("Y");
          gmPricingParamsSetupForm.setRevisionPrcAdjID(strPrcAdjId);
        }
      }
    }

    gmPricingParamsSetupForm.setAccid(strAccId);
    gmPricingParamsSetupForm.setPartyId(strPartyId);
    if (!strPartyId.equals("")) {
      alLogReasons = gmCommonBean.getLog(strPartyId, "10304529332"); // 10304529332-LogType
    } else {
      alLogReasons = gmCommonBean.getLog(strAccId, "10304529332"); // 10304529332-LogType
    }
    gmPricingParamsSetupForm.setAlLogReasons(alLogReasons);
    return mapping.findForward("gmPricingParamsSetup");
  }

  /**
   * fetchAccountAffiliationLists - This method will load all the Account Affiliation details in
   * Pricing Parameter Screen
   * 
   * @return org.apache.struts.action.ActionForward
   * @throws Exception
   */
  // For Loading all dropdowns
  public void fetchAccountAffiliationLists(GmPricingParamsSetupForm gmPricingParamsSetupForm)
      throws AppError, Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());
    String strId = "";
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alGroupPriceBook = new ArrayList();
    ArrayList alIdn = new ArrayList();
    ArrayList alGpo = new ArrayList();
    ArrayList alContract = new ArrayList();
    ArrayList alAdminFeeFlag=new ArrayList();
    GmPricingParamsSetupBean gmPricingParamsSetupBean =
        new GmPricingParamsSetupBean(getGmDataStoreVO());

    hmReturn = GmCommonClass.parseNullHashMap(gmPricingParamsSetupBean.loadPricingAccount()); // For
                                                                                              // getting
                                                                                              // Group
                                                                                              // Price
                                                                                              // Book
                                                                                              // Dropdown
                                                                                              // values
    alGroupPriceBook = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("GPOLIST"));
    gmPricingParamsSetupForm.setAlGroupPriceBook(alGroupPriceBook);

    alIdn = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALGPRAFF"));
    gmPricingParamsSetupForm.setAlIdn(alIdn);

    alGpo = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALHLCAFF"));
    gmPricingParamsSetupForm.setAlGpo(alGpo);
    
    strContract = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId("903103")); // For
                                                                                          // Contract
                                                                                          // Dropdown
                                                                                          // Label
                                                                                          // name

    // Contract
    // Dropdown
    // Label
    // name
    gmPricingParamsSetupForm.setStrContractNm(strContract);

    alContract = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONTFL")); // Dropdown
                                                                                        // values
                                                                                        // for
                                                                                        // Contract

    gmPricingParamsSetupForm.setAlContract(alContract);
    
    alAdminFeeFlag= GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTTYP","7003"));// Dropdown
    																			                // values
    				                                                                            // for
    																						   // Admin Fee Flag
    
    gmPricingParamsSetupForm.setAlAdminFeeFlag(alAdminFeeFlag);
    

    strId = gmPricingParamsSetupForm.getAccid();
    hmParam =
        GmCommonClass.parseNullHashMap(gmPricingParamsSetupBean.loadPricingEditAccount(strId));

    hmResult = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("ACCOUNTDETAILS"));
    String strGpoId = GmCommonClass.parseNull((String) hmResult.get("GPOID"));
    gmPricingParamsSetupForm.setGrouppricebook(strGpoId);

    String strIdn = GmCommonClass.parseNull((String) hmResult.get("GRPAFF"));
    gmPricingParamsSetupForm.setIdn(strIdn);

    String strContractfl = GmCommonClass.parseNull((String) hmResult.get("CONTRACTFL"));
    gmPricingParamsSetupForm.setStrContract(strContractfl);

    String strGpo = GmCommonClass.parseNull((String) hmResult.get("HLCAFF"));
    gmPricingParamsSetupForm.setGpo(strGpo);
    
    String strAdFeeFlag=GmCommonClass.parseNull((String) hmResult.get("ADMINFLAG"));
    gmPricingParamsSetupForm.setAdminFeeFlag(strAdFeeFlag);
    
    String strAdFee=GmCommonClass.parseNull((String) hmResult.get("ADMINFEE"));
    gmPricingParamsSetupForm.setAdminFee(strAdFee);
    
    String strGrpPriceBkNm=GmCommonClass.parseNull((String) hmResult.get("GROUPPRICEBOOKNAME"));
    gmPricingParamsSetupForm.setGroupPriceBookName(strGrpPriceBkNm);
    String strGpoNm=GmCommonClass.parseNull((String) hmResult.get("GPONAME"));
    gmPricingParamsSetupForm.setGpoName(strGpoNm);
    String strIdnName=GmCommonClass.parseNull((String) hmResult.get("IDNNAME"));
    gmPricingParamsSetupForm.setIdnName(strIdnName);

    String strid=GmCommonClass.parseNull((String) hmResult.get("GLN"));
    gmPricingParamsSetupForm.setStrId(strid);
    
    String strDetails=GmCommonClass.parseNull((String) hmResult.get("GPOIDD"));
    gmPricingParamsSetupForm.setStrDetails(strDetails);
    //PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen
    String strRpcName=GmCommonClass.parseNull((String) hmResult.get("RPCNAME"));
    gmPricingParamsSetupForm.setRpcName(strRpcName);
    
    String strHlthName=GmCommonClass.parseNull((String) hmResult.get("HLTHNAME"));
    gmPricingParamsSetupForm.setHlthName(strHlthName);
    
    String strRpc=GmCommonClass.parseNull((String) hmResult.get("RPC"));
    gmPricingParamsSetupForm.setRpc(strRpc);
    
    
    String strHlthId=GmCommonClass.parseNull((String) hmResult.get("HLTHID"));
    gmPricingParamsSetupForm.setHlthId(strHlthId);
    
    
  }



  /**
   * savePricingParams - This method will save the pricing parameter setup details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws Exception
   */
  public ActionForward savePricingParams(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());
    instantiate(request, response);
    GmPricingParamsSetupForm gmPricingParamsSetupForm = (GmPricingParamsSetupForm) actionForm;
    GmPricingParamsSetupBean gmPricingParamsSetupBean =
        new GmPricingParamsSetupBean(getGmDataStoreVO());
    gmPricingParamsSetupForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    String strAccId = gmPricingParamsSetupForm.getAccid();
    String strScreenType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmPricingParamsSetupForm);
    
    log.debug("==savePricingParams==hmParam== "+hmParam);
    gmPricingParamsSetupBean.savePricingParam(hmParam);
    gmPricingParamsSetupForm.setAccid(strAccId);
    String strPartyIds = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    if (!strPartyIds.equals("")) {
      strScreenType = "groupAccountSetup";
    }
    gmPricingParamsSetupForm.setPartyId(strPartyIds);
    String strRedirectPath =
        "/gmPricingParamsSetup.do?method=fetchPricingParams&accid=" + strAccId + "&partyId="
            + strPartyIds + "&hScreenType=" + strScreenType;
    return actionRedirect(strRedirectPath, request);
  }


  /*
   * fetchPriceParamHystory(): Method is written for fetching the Price Adjustment Code History
   * details
   */

  public ActionForward fetchPriceParamHystory(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPricingParamsSetupForm gmPricingParamsSetupForm = (GmPricingParamsSetupForm) form;
    GmPricingParamsSetupBean gmPricingParamsSetupBean =
        new GmPricingParamsSetupBean(getGmDataStoreVO());
    gmPricingParamsSetupForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    RowSetDynaClass rdresult = null;
    List alResult = new ArrayList();
    String strAuditTrailId = GmCommonClass.parseNull(gmPricingParamsSetupForm.getAuditTrailId());

    String strAuditTransId =
        GmCommonClass.parseNull(gmPricingParamsSetupForm.getAuditTrailTransID());
    hmParam.put("AUDITTRAILID", strAuditTrailId);
    hmParam.put("AUDITTRANSID", strAuditTransId);
    rdresult = gmPricingParamsSetupBean.fetchPriceParamHistoryDetails(hmParam);
    alResult = rdresult.getRows();
    gmPricingParamsSetupForm.setLdtHistoryResult(alResult);
    return mapping.findForward("gmPricingParamsHistory");
  }

}
