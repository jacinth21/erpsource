package com.globus.sales.pricing.actions;

import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.HashMap;
import java.text.SimpleDateFormat;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.pricing.beans.GmRebateReportBean;
import com.globus.sales.pricing.beans.GmRebateSetupBean;
import com.globus.sales.pricing.forms.GmRebateReportForm;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;



public class GmRebateReportAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /**
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward viewRebateparts (ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException, ParseException {
		 // call the instantiate method
		 instantiate(request, response);
		 GmRebateReportForm gmRebateMgntRptForm = (GmRebateReportForm) form;
		 gmRebateMgntRptForm.loadSessionParameters(request);
		 GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(getGmDataStoreVO());
		 
		 HashMap hmParam = new HashMap();
		 HashMap hmApplnParam = new HashMap();
		 ArrayList alReturn = new ArrayList();
		 
		 String strSessCompanyLocale="";
		 String strApplnDateFmt="";

		
		 hmParam =  GmCommonClass.getHashMapFromForm(gmRebateMgntRptForm);
		 strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
		 strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();	 
		
		 alReturn=gmRebateReportBean.fetchRebateList(hmParam);
		 hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
		 hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
		 hmApplnParam.put("ALRETURN", alReturn);
		 hmApplnParam.put("TEMPNAME", "GmRebateViewParts.vm");
		 hmApplnParam.put("TEMPPATH", "sales/pricing/templates");
		 hmApplnParam.put("RESOURCEBNDLPATH", "properties.labels.sales.pricing.GmRebateViewParts");
         String strXmlData = getXmlGridData(alReturn,hmApplnParam);
         gmRebateMgntRptForm.setGridData(strXmlData);
		 return actionMapping.findForward("GmRebateViewParts");
	 }

	 
	 
	
	 /**
	 * @param alResult
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {
	   
	        GmTemplateUtil templateUtil = new GmTemplateUtil();
	        String strSessCompanyLocale =
	            GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	        String strTempletPath = GmCommonClass.parseNull((String) hmParam.get("TEMPPATH"));
	        String strTempletName = GmCommonClass.parseNull((String) hmParam.get("TEMPNAME"));
	        String strResourceBdlePath = GmCommonClass.parseNull((String) hmParam.get("RESOURCEBNDLPATH"));
	        templateUtil.setDataList("alResult", alResult);
	        templateUtil.setDataMap("hmApplnParam", hmParam);
	        templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strResourceBdlePath, strSessCompanyLocale));
	        templateUtil.setTemplateSubDir(strTempletPath);
	        templateUtil.setTemplateName(strTempletName);
	        return templateUtil.generateOutput();

	 }
	
	 /**loadRebateManagmentReport - this method will load rebate management Report 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward  loadRebateManagmentReport(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException, ParseException {
		 // call the instantiate method
		    instantiate(request, response);
		    GmRebateReportForm gmRebateReportForm = (GmRebateReportForm) form;;
		    gmRebateReportForm.loadSessionParameters(request);
			GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(getGmDataStoreVO());
			 GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
			 HashMap hmParam = new HashMap();
			 String strOpt="";
			 
			 hmParam =  GmCommonClass.getHashMapFromForm(gmRebateReportForm);
			 strOpt = GmCommonClass.parseNull(gmRebateReportForm.getStrOpt());
		
		 //get Code Grroup Value
		 gmRebateReportForm.setAlAccountType(GmCommonClass.parseNullArrayList(GmCommonClass
			        .getCodeList("RBGRTY", getGmDataStoreVO())));
		 gmRebateReportForm.setAlRebateParts(GmCommonClass.parseNullArrayList(GmCommonClass
			        .getCodeList("RBPRTY", getGmDataStoreVO())));
		 gmRebateReportForm.setAlRebateCategory(GmCommonClass.parseNullArrayList(GmCommonClass
			        .getCodeList("RBCATG", getGmDataStoreVO())));
		 gmRebateReportForm.setAlRebateType(GmCommonClass.parseNullArrayList(GmCommonClass
			        .getCodeList("RBTYPE", getGmDataStoreVO())));
		 gmRebateReportForm.setAlPayment(GmCommonClass.parseNullArrayList(GmCommonClass
			        .getCodeList("RBPYMT", getGmDataStoreVO())));
		 gmRebateReportForm.setAlDateType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBDTTY", getGmDataStoreVO())));

		 return actionMapping.findForward("GmRebateMasterReport");
	 }
	 
	/**reloadRebateManagmentReport - This method Will Load Rebate Management Report Details based on Account type , Party Name
	 *                                Rebate Rate, Rebate Type , Rebate parts , Effective Date , Contract date
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward  reloadRebateManagmentReport(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException, ParseException {
		 // call the instantiate method
		    instantiate(request, response);
		    GmRebateReportForm gmRebateReportForm = (GmRebateReportForm) form;;
		    gmRebateReportForm.loadSessionParameters(request);
		    GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(getGmDataStoreVO());
			 HashMap hmParam = new HashMap();
			 String strOpt="";
			 hmParam =  GmCommonClass.getHashMapFromForm(gmRebateReportForm);
			 strOpt = GmCommonClass.parseNull(gmRebateReportForm.getStrOpt());
		      String strRebateReportJsonString =
		          GmCommonClass.parseNull(gmRebateReportBean.fetchRebateManagmentReport(hmParam));
		   
		        response.setContentType("text/json");
		        PrintWriter pw = response.getWriter();
		        if (!strRebateReportJsonString.equals("")) {
		        pw.write(strRebateReportJsonString);
		        }
		        pw.flush();
		      return null;   	 
	 }
	 
	 
	 /** loadRebatePartReportsDtls - this method will load rebate part list Report Screen
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward  loadRebatePartReportsDtls(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException, ParseException {
		     
		 // call the instantiate method
		   instantiate(request, response);
		    GmRebateReportForm gmRebateReportForm = (GmRebateReportForm) form;;
		    gmRebateReportForm.loadSessionParameters(request);
		    GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(getGmDataStoreVO());
			 GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
			 HashMap hmParam = new HashMap();
			 String strOpt="";
		
		   hmParam =  GmCommonClass.getHashMapFromForm(gmRebateReportForm);
		  strOpt = GmCommonClass.parseNull(gmRebateReportForm.getStrOpt());
		 gmRebateReportForm.setAlAccountType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBGRTY", getGmDataStoreVO())));
		 gmRebateReportForm.setAlDateType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RBDTTY", getGmDataStoreVO())));

		 return actionMapping.findForward("GmRebatePartListRpt");
	 }
	
	
	/**reloadRebatePartReportsDtls - This method will diaplay Part list details based on Account type ,Party name , part number, Effective Date , Contract To Date
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward  reloadRebatePartReportsDtls(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
		      IOException, ParseException {
		     
		 // call the instantiate method
		   instantiate(request, response);
		    GmRebateReportForm gmRebateReportForm = (GmRebateReportForm) form;;
		    gmRebateReportForm.loadSessionParameters(request);
		    GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(getGmDataStoreVO());
		    HashMap hmParam = new HashMap();
		    HashMap hmRegStr = new HashMap();
			String strOpt="";
			 String strRebateReportJsonString ="";
			 String strPartNumFormat ="";
			 hmParam =  GmCommonClass.getHashMapFromForm(gmRebateReportForm);
			 strOpt = GmCommonClass.parseNull(gmRebateReportForm.getStrOpt());
             //Part Number Search
		      hmRegStr.put("PARTNUM", gmRebateReportForm.getBatchPartNum());
		      hmRegStr.put("SEARCH", gmRebateReportForm.getPnumSuffix());
		      strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
		      hmParam.put("PARTNUM", strPartNumFormat);
		      strRebateReportJsonString = GmCommonClass.parseNull(gmRebateReportBean.loadRebatePartReportsDtls(hmParam));
	       	 response.setContentType("text/json");
		     PrintWriter pw = response.getWriter();
		     if (!strRebateReportJsonString.equals("")) {
	         pw.write(strRebateReportJsonString);
		     }
	         pw.flush();
		     return null;
	 }
	

}

