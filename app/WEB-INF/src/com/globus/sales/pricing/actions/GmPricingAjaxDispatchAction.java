package com.globus.sales.pricing.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.sales.pricing.beans.GmConstructBuilderBean;

public class GmPricingAjaxDispatchAction extends GmDispatchAction {

  /**
   * Get construct data method will returns the construct list for the selected system This is a
   * ajax call
   * 
   * @author rmayya
   * @date Nov 20, 2009
   * @param
   */
  public ActionForward getConstructData(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String systemId = request.getParameter("systemId");
    GmConstructBuilderBean gmConstructBuilderBean = new GmConstructBuilderBean();
    ArrayList systemList = gmConstructBuilderBean.loadConstructsList(systemId);
    int len = 0;
    HashMap hmMap = null;
    StringBuffer dataBuf = new StringBuffer();
    if (systemList != null) {
      len = systemList.size();
    }
    for (int i = 0; i < len; i++) {
      hmMap = (HashMap) systemList.get(i);
      dataBuf.append(hmMap.get("CONSTRUCTID")).append(":").append(hmMap.get("CONSTRUCTNAME"))
          .append(":");
    }
    if (dataBuf.length() > 1) {
      dataBuf.deleteCharAt(dataBuf.length() - 1);
    }
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(dataBuf.toString());
    pw.flush();
    return null;
  }

  /**
   * getGroupsPricing Method will return the price details of the groups passed This is a ajax call
   * made when user adds groups to the construct
   * 
   * @author rmayya
   * @date Nov 20, 2009
   * @param
   */
  public ActionForward getGroupsPricing(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    String strGropusInpStr = request.getParameter("groupInputStr");
    String effectiveDate = request.getParameter("effectiveDate");
    String strConstructId = request.getParameter("strConstructId");
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    String strVmProPath = "properties.labels.sales.pricing.GmConstructBuilder";
    GmConstructBuilderBean gmConstructBuilderBean = new GmConstructBuilderBean();
    HashMap hmParams = new HashMap();
    hmParams.put("strConstructId", strConstructId);
    hmParams.put("effectiveDate", effectiveDate);
    hmParams.put("strInpGroupIds", strGropusInpStr);

    ArrayList alResult = gmConstructBuilderBean.fetchConstructGroupsDetails(hmParams);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(gmConstructBuilderBean.getXmlGridData(alResult, strSessCompanyLocale, strVmProPath));
    pw.flush();
    return null;
  }

}
