package com.globus.sales.pricing.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.pricing.beans.GmGroupPricingBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.sales.pricing.forms.GmPricingRequestForm;

public class GmPricingRequestDAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadPriceRequest Initializing the price request
   * 
   * @author rmayya
   * @date Dec 3, 2009
   * @param
   */
  public ActionForward loadPriceRequest(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    log.debug("Inside laodPriceRequest");
    try {
      GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
      gmPricingRequestForm.loadSessionParameters(request);
      GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
      GmPartyBean gmPartyBean = new GmPartyBean();
      HttpSession session = request.getSession(false);
      GmProjectBean gmProjectBean = new GmProjectBean();
      String strOpt = gmPricingRequestForm.getStrOpt();
      HashMap hmDetails = new HashMap();
      // To get the Access Level information from Materialized view
      String strAccessFilter = getAccessFilter(request, response);
      String strgprName = "";
      String strAccType = GmCommonClass.parseZero(gmPricingRequestForm.getStrAccType());
      String strGpoID = GmCommonClass.parseZero(gmPricingRequestForm.getStrGroupAcc());
      String strAccountId = GmCommonClass.parseZero(gmPricingRequestForm.getStrAccountId());
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      ArrayList alAccountList = new ArrayList();
      String requestId = GmCommonClass.parseNull(gmPricingRequestForm.getStrRequestId());
      if (strAccType.equals("0") || strAccType.equals("903107")) {
        alAccountList = gmPricingRequestBean.loadAccountList(strAccessFilter);

        gmPricingRequestForm.setStrGroupAcc("");
      } else if (strAccType.equals("903108") && !strGpoID.equals("0")) {
        alAccountList = gmPricingRequestBean.loadGroupAccountDetails(strGpoID, strAccessFilter);
        hmDetails = gmPricingRequestBean.loadGroupAccountDetailsMap(strGpoID, strAccessFilter);
        strgprName = GmCommonClass.parseNull(gmPricingRequestBean.getPartyNm(strGpoID));
        if (!strgprName.equals(""))
          gmPricingRequestForm.setStrGroupName(strgprName + "(" + strGpoID + ")");
      }
      if (!strAccountId.equals("0")) {
        hmDetails.putAll(gmPricingRequestBean.loadAccountDetails(
            gmPricingRequestForm.getStrAccountId(), "", ""));
      }

      if (strAccountId.equals("0")) {
        gmPricingRequestForm.setStrAccountId("");
      }
      String strContractFl = GmCommonClass.parseNull((String) hmDetails.get("CONTRACTFL"));
      // ArrayList alGroupList = gmPricingRequestBean.loadGroupAccountList(strAccessFilter);
      ArrayList alGroupList = gmPartyBean.reportPartyNameList("7003");
      ArrayList alAccountType =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ACCTYP"));
      gmPricingRequestForm.setAlAccountList(alAccountList);
      gmPricingRequestForm.setAlAccountType(alAccountType);
      gmPricingRequestForm.setAlGroupList(alGroupList);
      String strAction = gmPricingRequestForm.getHaction();
      ArrayList alSalesGrp = gmPricingRequestBean.loadSystemsList();
      gmPricingRequestForm.setAlSystemList(alSalesGrp);
      HashMap hmParam = null;
      if (strAction.equals("acntChange") && !strAccountId.equals("0")) {
        // fetch all systems
        // ArrayList alSalesGrp = gmProjectBean.loadSetMap("1600");

        if (requestId == null || requestId.equals("")) {
          requestId =
              gmPricingRequestBean.loadRequestId(gmPricingRequestForm.getStrAccountId(),
                  gmPricingRequestForm.getStrGroupAcc(), gmPricingRequestForm.getHstatusId());
          gmPricingRequestForm.setStrRequestId(requestId); // To set request id.
        }
        strGpoID = gmPricingRequestBean.loadAccountGpoID(gmPricingRequestForm.getStrAccountId());
        strgprName = GmCommonClass.parseNull(gmPricingRequestBean.getPartyNm(strGpoID));
        if (strAccType.equals("903107") && !strGpoID.equals("0")) {
          if (!strgprName.equals(""))
            gmPricingRequestForm.setStrGroupName(strgprName + "(" + strGpoID + ")");
          gmPricingRequestForm.setStrAccGPO(strGpoID);
        }


        log.debug("1 GPO ID::" + strGpoID + "::GropName:" + gmPricingRequestForm.getStrGroupName());
        hmDetails.putAll(gmPricingRequestBean.loadRequestMasterDetails(requestId));

        if (strContractFl.equals("903100")) {
          gmPricingRequestForm.setStrContractFl("YES");
        } else {
          gmPricingRequestForm.setStrContractFl("NO");
        }
        log.debug("inside acntchange condition:" + hmDetails);
        hmParam = GmCommonClass.getHashMapFromForm(gmPricingRequestForm);
        hmParam.put("AJAX", "NO");
        gmPricingRequestForm.setStrDispFlg("Y");
      } else {
        // gmPricingRequestForm.setStrAccountId("");
      }

      GmCommonClass.getFormFromHashMap(gmPricingRequestForm, hmDetails);
      gmPricingRequestForm.setGridXmlGroupData(getGroupByGridString(hmParam == null ? null
          : hmParam, request, gmPricingRequestForm.getStrAccountId(), strSessCompanyLocale));
      gmPricingRequestForm.setGridXmlAnalysisData(getAnalysisGridString(hmParam == null ? null
          : hmParam, request, gmPricingRequestForm.getStrAccountId(), strSessCompanyLocale));
      gmPricingRequestForm.setGridXmlAnalysisView(getAnalysisViewGridString(hmParam == null ? null
          : hmParam, request, strSessCompanyLocale));
      // gmPricingRequestForm.setGridXmlWorkflowData(getWorkflowGridString(null));
      gmPricingRequestForm.setGridXmlActivityData(getActivityGridString(hmParam == null ? null
          : hmParam, request, gmPricingRequestForm.getStrAccountId(), strSessCompanyLocale));

      HashMap hmQuestion =
          gmPricingRequestBean.fetchQuestionAnswer(GmCommonClass.parseNull((String) hmDetails
              .get("STRREQUESTID")));
      log.debug("2 GPO ID::" + strGpoID + "::GropName:" + gmPricingRequestForm.getStrGroupName());

      if (!strGpoID.equals("0") && strAccType.equals("903108"))
        gmPricingRequestForm.setStrGroupAcc(strGpoID);

      ArrayList alQuestion = (ArrayList) hmQuestion.get("QUESTION");
      gmPricingRequestForm.setAlQuestion(alQuestion);

      ArrayList alAnswer = (ArrayList) hmQuestion.get("ANSWER");
      gmPricingRequestForm.setAlAnswer(alAnswer);

      ArrayList alAnswerGroup = (ArrayList) hmQuestion.get("ANSWERGROUP");
      gmPricingRequestForm.setAlAnswerGroup(alAnswerGroup);

      ArrayList alQuestionAnswer = (ArrayList) hmQuestion.get("QUESTIONANSWER");
      gmPricingRequestForm.setAlQuestionAnswer(alQuestionAnswer);
      session.setAttribute("strAnaView", "NO");
      session.setAttribute("strActivityLog", "NO");
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("GmPricingRequest");
  }

  /**
   * openPriceRequest to open the already exiting request
   * 
   * @author rmayya
   * @date Dec 11, 2009
   * @param
   */
  public ActionForward openPriceRequest(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    log.debug("inside openPriceRequest");
    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    GmProjectBean gmProjectBean = new GmProjectBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmPartyBean gmPartyBean = new GmPartyBean();
    HttpSession session = request.getSession(false);
    // Get the request Id if it is call from status search report
    String requestId = GmCommonClass.parseNull(gmPricingRequestForm.getStrRequestId());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    if (requestId.equals(""))
      requestId = GmCommonClass.parseNull(gmPricingRequestForm.getHreqid());

    String strAccType = GmCommonClass.parseZero(gmPricingRequestForm.getStrAccType());
    String strGpoID = GmCommonClass.parseZero(gmPricingRequestForm.getStrGroupAcc());


    // get the request details
    if (requestId == null || requestId.equals("")) {
      requestId =
          gmPricingRequestBean.loadRequestId(gmPricingRequestForm.getStrAccountId(),
              gmPricingRequestForm.getStrGroupAcc(), gmPricingRequestForm.getHstatusId());
      gmPricingRequestForm.setStrRequestId(requestId);// To set request id.
    }
    HashMap hmDetails = gmPricingRequestBean.loadRequestMasterDetails(requestId);
    GmCommonClass.getFormFromHashMap(gmPricingRequestForm, hmDetails);
    log.debug("### hmDetails  " + hmDetails);
    if (strGpoID.equals("0")) {
      strGpoID = gmPricingRequestBean.loadAccountGpoID((String) hmDetails.get("STRACCOUNTID"));
      gmPricingRequestForm.setStrAccGPO(strGpoID);
    }
    // for pending review and initiated status show the systems - Sales rep case
    String strAccessFilter = getAccessFilter(request, response);
    if (!"52125".equals(gmPricingRequestForm.getHstatusId())) {
      // To get the Access Level information from Materialized view

      ArrayList alAccountList = new ArrayList();
      if (strAccType.equals("0") || strAccType.equals("903107")) {
        alAccountList = gmPricingRequestBean.loadAccountList(strAccessFilter);
      } else if (strAccType.equals("903108") && !strGpoID.equals("0")) {
        alAccountList = gmPricingRequestBean.loadGroupAccountDetails(strGpoID, strAccessFilter);

      }
      String strgprName = GmCommonClass.parseNull(gmPricingRequestBean.getPartyNm(strGpoID));
      if (!strgprName.equals("") && !strGpoID.equals("0")) {
        gmPricingRequestForm.setStrGroupName(strgprName + "(" + strGpoID + ")");
        gmPricingRequestForm.setStrAccGPO(strGpoID);
        log.debug("setStrGroupName::" + gmPricingRequestForm.getStrGroupName());
      } else {
        gmPricingRequestForm.setStrGroupName("");
      }
      gmPricingRequestForm.setAlAccountList(alAccountList);

    } else {
      // to make analysis view tab as default one
      gmPricingRequestForm.setHviewType("analysis");
    }

    // get the account details
    if (strAccType.equals("903107")) {
      hmDetails =
          gmPricingRequestBean.loadAccountDetails(gmPricingRequestForm.getStrAccountId(), "", "");
      if (!strGpoID.equals("") || !strGpoID.equals("0"))
        gmPricingRequestForm.setStrGroupAcc(strGpoID);
    } else if (strAccType.equals("903108") && !strGpoID.equals("0")) {
      hmDetails = gmPricingRequestBean.loadGroupAccountDetailsMap(strGpoID, strAccessFilter);
      ArrayList alGroupList = gmPartyBean.reportPartyNameList("7003");
      gmPricingRequestForm.setAlGroupList(alGroupList);
      gmPricingRequestForm.setStrGroupAcc(strGpoID);
    }
    hmDetails.put("STRREQUESTID", requestId);
    hmDetails.put("HSTATUSID", gmPricingRequestForm.getHstatusId());
    String strContractFl = GmCommonClass.parseNull((String) hmDetails.get("CONTRACTFL"));
    if (strContractFl.equals("903100")) {
      gmPricingRequestForm.setStrContractFl("YES");
    } else {
      gmPricingRequestForm.setStrContractFl("NO");
    }
    gmPricingRequestForm.setStrAccType(strAccType);
    gmPricingRequestForm.setStrGroupAcc(strGpoID);
    GmCommonClass.getFormFromHashMap(gmPricingRequestForm, hmDetails);

    // get comments
    ArrayList alLogReasons = gmCommonBean.getLog(requestId, "1252");
    gmPricingRequestForm.setAlLogReasons(alLogReasons);

    // get question answer

    HashMap hmQuestion = gmPricingRequestBean.fetchQuestionAnswer(requestId);

    ArrayList alQuestion = (ArrayList) hmQuestion.get("QUESTION");
    gmPricingRequestForm.setAlQuestion(alQuestion);

    ArrayList alAnswer = (ArrayList) hmQuestion.get("ANSWER");
    gmPricingRequestForm.setAlAnswer(alAnswer);

    ArrayList alAnswerGroup = (ArrayList) hmQuestion.get("ANSWERGROUP");
    gmPricingRequestForm.setAlAnswerGroup(alAnswerGroup);

    ArrayList alQuestionAnswer = (ArrayList) hmQuestion.get("QUESTIONANSWER");
    gmPricingRequestForm.setAlQuestionAnswer(alQuestionAnswer);
    // if (gmPricingRequestForm.getUserId().equals(gmPricingRequestForm.getStrInitiateById())) {
    // fetch all systems
    // ArrayList alSalesGrp = gmProjectBean.loadSetMap("1600");
    ArrayList alSalesGrp = gmPricingRequestBean.loadSystemsList();
    gmPricingRequestForm.setAlSystemList(alSalesGrp);
    // }
    ArrayList alAccountType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ACCTYP"));
    gmPricingRequestForm.setAlAccountType(alAccountType);
    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmPricingRequestForm);
    // only way to differentiate add system or get the group details from
    // existing request values
    hmParam.put("INPUTSTR", gmPricingRequestForm.getSysTranId());
    hmParam.put("FCHALLSYSFL", gmPricingRequestForm.getFchAllSysFl());
    hmParam.put("GROUPID", gmPricingRequestForm.getSysGrpId());
    hmParam.put("AJAX", "NO");
    hmParam.put("STRREQUESTID", requestId);

    session.setAttribute("strAnaView", "NO");
    session.setAttribute("strActivityLog", "NO");
    // get the grid details
    gmPricingRequestForm.setGridXmlGroupData(getGroupByGridString(hmParam, request,
        gmPricingRequestForm.getStrAccountId(), strSessCompanyLocale));
    gmPricingRequestForm.setGridXmlAnalysisData(getAnalysisGridString(hmParam, request,
        gmPricingRequestForm.getStrAccountId(), strSessCompanyLocale));
    gmPricingRequestForm.setGridXmlAnalysisView(getAnalysisViewGridString(hmParam, request,
        strSessCompanyLocale));
    // gmPricingRequestForm.setGridXmlWorkflowData(getWorkflowGridString(null));
    gmPricingRequestForm.setGridXmlActivityData(getActivityGridString(hmParam, request,
        gmPricingRequestForm.getStrAccountId(), strSessCompanyLocale));
    return mapping.findForward("GmPricingRequest");
  }

  /**
   * getSysReqDetails Ajax call to get the details of the added system
   * 
   * @author rmayya
   * @date Dec 15, 2009
   * @param
   */
  public ActionForward getSysReqDetails(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);
    String strRequestId = gmPricingRequestForm.getStrRequestId();
    String strAccountId = gmPricingRequestForm.getStrAccountId();
    String systemInputStr = GmCommonClass.parseNull(request.getParameter("systemInputStr"));
    String strGroupId = GmCommonClass.parseNull(request.getParameter("groupid"));
    String effectiveDate = gmPricingRequestForm.getEffectiveDate();
    String strGpoId = GmCommonClass.parseNull(gmPricingRequestForm.getStrGroupAcc());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    HashMap hmParams = new HashMap();
    hmParams.put("STRREQUESTID", strRequestId);
    hmParams.put("STRACCOUNTID", strAccountId);
    hmParams.put("EFFECTIVEDATE", effectiveDate);
    hmParams.put("INPUTSTR", systemInputStr);
    hmParams.put("GROUPID", strGroupId);
    hmParams.put("STRGROUPACC", strGpoId);
    hmParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(getGroupByGridString(hmParams, request, gmPricingRequestForm.getStrAccountId(),
        strSessCompanyLocale));
    pw.flush();
    return null;
  }

  /**
   * savePriceRequest saving the price request
   * 
   * @author rmayya
   * @date Dec 15, 2009
   * @param
   */
  public ActionForward savePriceRequest(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);
    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmPricingRequestForm);
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    HttpSession session = request.getSession(false);
    String strCancelType = GmCommonClass.parseNull(request.getParameter("hCancelType"));
    String strDenyRefId = GmCommonClass.parseNull(request.getParameter("strDenyRefId"));
    String strTxnId = GmCommonClass.parseNull(request.getParameter("hTxnId"));
    String strTxnName = GmCommonClass.parseNull(request.getParameter("hTxnName"));
    String hSkipCmnCnl = GmCommonClass.parseNull(request.getParameter("hSkipCmnCnl"));
    String strRemoveSysIds = GmCommonClass.parseNull(request.getParameter("strRemoveSysIds"));

    hmParam.put("STRACCTYPE", GmCommonClass.parseNull((String) hmParam.get("STRACCTYPE")));
    hmParam.put("STRGROUPACC", GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC")));
    hmParam.put("STRACCOUNTID", GmCommonClass.parseNull((String) hmParam.get("STRACCOUNTID")));
    hmParam.put("EFFECTIVEDATE", GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEDATE")));
    hmParam.put("INPUTSTR", GmCommonClass.parseNull(gmPricingRequestForm.getHinputStr()));
    hmParam.put("STRREQUESTID", GmCommonClass.parseNull(gmPricingRequestForm.getHreqid()));
    hmParam.put("HSTATUSID", GmCommonClass.parseNull(gmPricingRequestForm.getHstatusId()));
    hmParam.put("INPUTANSWER", GmCommonClass.parseNull(gmPricingRequestForm.getHinputAnswer()));
    hmParam.put("HDENIEDINPUTSTR",
        GmCommonClass.parseNull(gmPricingRequestForm.gethDeniedinputStr()));
    hmParam.put("STRREMOVESYSIDS", GmCommonClass.parseNull(strRemoveSysIds));

    hmParam.put("STRDENYREFID", strDenyRefId);
    hmParam
        .put("STRPUBLOVRRIDE", GmCommonClass.parseNull(gmPricingRequestForm.getStrPublOvrRide()));

    log.debug("BEFORE SAVE hmParam::" + hmParam);

    if (hSkipCmnCnl.equals("YES")) {
      session.setAttribute("requestvalue", hmParam);
      request.setAttribute("strTxnId", strTxnId);
      request.setAttribute("strTxnName", strTxnId);
      request.setAttribute("strCancelType", strCancelType);
      return mapping.findForward("GmCommonCancelServlet");

    }
    // status checks can be made here
    HashMap hmReturn = gmPricingRequestBean.savePricingRequest(hmParam);
    String hStatusId = GmCommonClass.parseNull(((String) hmReturn.get("HSTATUSID")));
    String requestId = GmCommonClass.parseNull(((String) hmReturn.get("STRREQUESTID")));
    String strException = GmCommonClass.parseNull(((String) hmReturn.get("ERRORMSG")));
    log.debug("strException::::" + strException);
    gmPricingRequestForm.setHstatusId(hStatusId);
    gmPricingRequestForm.setStrRequestId(requestId);
    gmPricingRequestForm.setErrorMsg(strException);


    return openPriceRequest(mapping, gmPricingRequestForm, request, response);
  }

  /**
   * getConstructDetails this method will fill the Construct Details for the grid
   * 
   * @author Velu
   * @date Mar 06, 2012
   * @param request, ArrayList
   */
  public HashMap getConstructDetails(HttpServletRequest request, ArrayList alConstResult)
      throws Exception {

    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    if (alConstResult != null) {
      for (Iterator it = alConstResult.iterator(); it.hasNext();) {
        hmTemp = (HashMap) it.next();
        hmResult.put(hmTemp.get("CONSTRUCT_ID"), hmTemp.get("CONSTRUCT_NAME"));
      }
    }
    return hmResult;
  }

  public HashMap getAccessType(HttpServletRequest request, HashMap hmParams, String accId)
      throws Exception {

    HttpSession session = request.getSession(false);
    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    String strAdVpFlg = "";
    String strDesign = "";

    strAdVpFlg = gmPricingRequestBean.setAdVpFlg(accId, strEmpId);
    log.debug("strDepartMentID:::" + strDepartMentID);
    log.debug("strAdVpFlg>>>>>>>>>" + strAdVpFlg);
    GmCommonBean gmCommonBean = new GmCommonBean();

    HashMap hmAccessMap = new HashMap();

    String userId = strEmpId;
    String status = "";
    if (hmParams != null) {
      userId = (String) hmParams.get("USERID");
      status = (String) hmParams.get("HSTATUSID");
    }
    status = status == null ? "" : status;

    /*
     * HashMap hmRuleParam = new HashMap(); hmRuleParam.put("RULEID", "PRPCGRP");
     * hmRuleParam.put("RULEGROUP", "PRGRP");
     * 
     * String strPcGroup = gmCommonBean.getRuleValue(hmRuleParam);
     */
    if (strAdVpFlg != null
        && (strAdVpFlg.equalsIgnoreCase("AD") || strAdVpFlg.equalsIgnoreCase("VP"))) {
      intAccLevel = 10;
      strDesign = strAdVpFlg;
    }
    /*
     * if(strPcGroup != null && strPcGroup.length()>0) { String [] pcUsers = strPcGroup.split(",");
     * for(int i=0;i<pcUsers.length;i++) { if(pcUsers[i].equals(strEmpId)){ intAccLevel=10;
     * strDesign = "PC"; } } }
     */
    if (strDepartMentID.equals("Y")) {
      strDesign = "PC";
      intAccLevel = 10;
    }
    // to set the access level. 1 Sales Rep, 2-Distributor, 3- AD, 4-VP, 6
    // other ADs
    // if (strDepartMentID.equals("S")) {
    hmAccessMap.put("acl", Integer.toString(intAccLevel));
    hmAccessMap.put("u", strEmpId);
    hmAccessMap.put("ru", userId);
    hmAccessMap.put("st", status);
    hmAccessMap.put("des", strDesign); // designations ad/vp/pc
    hmAccessMap.put("dept", strDepartMentID); // strDepartMentID
    // }

    return hmAccessMap;
  }



  /**
   * getGroupByGridString xml content generator for group by grid
   * 
   * @author rmayya
   * @date Dec 15, 2009
   * @param
   */

  private String getGroupByGridString(HashMap hmParams, HttpServletRequest request, String accId,
      String strSessCompanyLocale) throws Exception {

    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    HashMap hmSystemParam = new HashMap();
    if (hmParams != null) {
      hmSystemParam = gmPricingRequestBean.loadPricingByGroupGrid(hmParams);
    }
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alSystemResult", (ArrayList) hmSystemParam.get("alSystemResult"));
    templateUtil.setDataList("alGroupResult", (ArrayList) hmSystemParam.get("alGroupResult"));
    templateUtil.setDataList("alPartResult", (ArrayList) hmSystemParam.get("alPartResult"));
    templateUtil.setDataMap("hmAccessType", getAccessType(request, hmParams, accId));
    // fetch statuses of the constructs
    ArrayList alChngTypeList = GmCommonClass.getCodeList("PCHTY");
    ArrayList alDateTypeList = GmCommonClass.getCodeList("GDTYPE");
    templateUtil.setDropDownMaster("alChngType", alChngTypeList);
    templateUtil.setDropDownMaster("alDateType", alDateTypeList);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmPricingByGroup", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/pricing/templates");

    templateUtil.setTemplateName("GmPricingByGroup.vm");
    // log.debug("templateUtil.generateOutput() groupgripdpricing>>>>>>>>"+templateUtil.generateOutput());
    return GmCommonClass.replaceForXML(GmCommonClass.parseNull(templateUtil.generateOutput()));
  }

  /**
   * getAnalysisGridString xml content generation for analysis grid
   * 
   * @author rmayya
   * @date Dec 15, 2009
   * @param
   */
  private String getAnalysisGridString(HashMap hmParams, HttpServletRequest request, String accId,
      String strSessCompanyLocale) throws Exception {

    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();

    HashMap hmSystemParam = new HashMap();
    if (hmParams != null) {
      hmSystemParam = gmPricingRequestBean.loadPricingByAnalysisGrid(hmParams);
    }
    ArrayList alStatusType = GmCommonClass.getCodeList("PRSTA");

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alSystemResult", (ArrayList) hmSystemParam.get("alSystemResult"));
    templateUtil.setDataList("alConstructResult",
        (ArrayList) hmSystemParam.get("alConstructResult"));
    templateUtil.setDataList("alPartResult", (ArrayList) hmSystemParam.get("alPartResult"));
    templateUtil.setDataMap("hmAccessType", getAccessType(request, hmParams, accId));
    templateUtil.setDataList("alStatusType", alStatusType);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmAnalysisView", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("sales/pricing/templates");

    templateUtil.setTemplateName("GmAnalysisView.vm");
    // log.debug("templateUtil.generateOutput() analysis>>>>>>>>"+templateUtil.generateOutput());
    return GmCommonClass.replaceForXML(GmCommonClass.parseNull(templateUtil.generateOutput()));
  }

  /**
   * getAnalysisViewGridDetails Ajax call to get the details of request AnalysisView
   * 
   * @author
   * @date May 02, 2012
   * @param
   */
  public ActionForward getAnalysisViewGridDetails(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParam = new HashMap();
    HttpSession session = request.getSession(false);
    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmPricingRequestForm);
    hmParam.put("AJAX", "YES");
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(getAnalysisViewGridString(hmParam, request, strSessCompanyLocale));
    pw.flush();
    return null;
  }

  /**
   * getAnalysisViewGridString xml content generation for AnalysisViewData grid
   * 
   * @author Velu
   * @date Mar 5, 2012
   * @param HashMap,String
   */
  private String getAnalysisViewGridString(HashMap hmParams, HttpServletRequest request,
      String strSessCompanyLocale) throws Exception {

    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    HashMap hmSystemParam = new HashMap();
    if (hmParams != null) {
      String strAjaxCall = GmCommonClass.parseNull((String) hmParams.get("AJAX"));
      if (strAjaxCall.equals("YES")) {
        hmSystemParam = gmPricingRequestBean.loadPricingByAnalysisViewGrid(hmParams);
      }
    }

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alAnalysisResult", (ArrayList) hmSystemParam.get("alAnalysisResult"));
    templateUtil.setDataMap("hmConstructResult",
        getConstructDetails(request, (ArrayList) hmSystemParam.get("alConstResult")));
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmAnalysisDataView", strSessCompanyLocale));
    templateUtil.setTemplateName("GmAnalysisDataView.vm");
    // log.debug("templateUtil.generateOutput() analysis>>>>>>>>"+templateUtil.generateOutput());
    return GmCommonClass.parseNull(templateUtil.generateOutput());
  }

  /**
   * getWorkflowDetails Ajax call to get the details of request work flow
   * 
   * @author rmayya
   * @date Dec 21, 2009
   * @param
   */
  public ActionForward getWorkflowDetails(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    String strRequestId = request.getParameter("strRequestId");
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(getWorkflowGridString(strRequestId));
    pw.flush();
    return null;
  }

  /**
   * getWorkflowGridString xml content generator for work flow by grid
   * 
   * @author rmayya
   * @date Dec 15, 2009
   * @param
   */

  private String getWorkflowGridString(String requestId) throws Exception {

    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    HashMap hmResult = new HashMap();
    if (requestId != null) {
      hmResult = gmPricingRequestBean.loadWorkflowGrid(requestId);
    }
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGroupResult", (ArrayList) hmResult.get("alGroupResult"));
    templateUtil.setDataList("alPartResult", (ArrayList) hmResult.get("alPartResult"));

    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setTemplateName("GmWorkflow.vm");
    return templateUtil.generateOutput();
  }

  /**
   * getActivityGridString xml content generation for activity log grid
   * 
   * @author Elango
   * @date Mar 15, 2012
   * @param
   */
  private String getActivityGridString(HashMap hmParams, HttpServletRequest request, String accId,
      String strSessCompanyLocale) throws Exception {

    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    HashMap hmActivityParam = new HashMap();
    if (hmParams != null) {
      String strAjaxCall = GmCommonClass.parseNull((String) hmParams.get("AJAX"));
      if (strAjaxCall.equals("YES")) {
        hmActivityParam = gmPricingRequestBean.loadPricingByActivityGrid(hmParams);
      }
    }
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alActivityResult",
        (ArrayList) hmActivityParam.get("alActivityResult"));
    templateUtil.setTemplateSubDir("sales/pricing/templates");

    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmActivityLog", strSessCompanyLocale));
    templateUtil.setTemplateName("GmActivityLog.vm");
    // log.debug("templateUtil.generateOutput() activitydata>>>>>>>>"+templateUtil.generateOutput());
    return GmCommonClass.replaceForXML(GmCommonClass.parseNull(templateUtil.generateOutput()));
  }

  /**
   * getWorkflowDetails Ajax call to get the details of request work flow
   * 
   * @author rmayya
   * @date Dec 21, 2009
   * @param
   */
  public ActionForward exportToExcel(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    String requestId = gmPricingRequestForm.getHreqid();
    // get the request details
    String strAccessFilter = getAccessFilter(request, response);
    String strAccType = GmCommonClass.parseZero(gmPricingRequestForm.getStrAccType());
    String strGpoID = GmCommonClass.parseZero(gmPricingRequestForm.getStrGroupAcc());
    HashMap hmDetails = gmPricingRequestBean.loadRequestMasterDetails(requestId);
    GmCommonClass.getFormFromHashMap(gmPricingRequestForm, hmDetails);
    // get the account details
    if (strAccType.equals("903107")) {
      hmDetails =
          gmPricingRequestBean.loadAccountDetails(gmPricingRequestForm.getStrAccountId(), "", "");
      gmPricingRequestForm.setStrGroupAcc("0");
    } else if (strAccType.equals("903108") && !strGpoID.equals("0")) {
      hmDetails = gmPricingRequestBean.loadGroupAccountDetailsMap(strGpoID, strAccessFilter);
    }
    GmCommonClass.getFormFromHashMap(gmPricingRequestForm, hmDetails);

    request.setAttribute("anlData", request.getParameter("anlData"));
    return mapping.findForward("GmPricingReqExcelReport");

  }

  /*
   * private String getNewStatus(String status, boolean incr, boolean decr) { //get the new status
   * if(status==null || status.length()==0) status = "52120"; //if status is pending review to AD
   * approval else if(status.equals("52124") && incr) { status = 52121; } //pc approve else if(incr
   * && status!=52123) { status++; } //ad send back else if(decr && status!=52121) { status--; }
   * 
   * else if(decr && status==52121) { status = 52124; } }
   */


  /**
   * getPart details -Ajax call to get the part details of request pricing by group tab
   * 
   * @author
   * @date
   * @param
   */

  public ActionForward getPartDetails(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);

    String strRequestId = gmPricingRequestForm.getStrRequestId();
    String strAccountId = gmPricingRequestForm.getStrAccountId();
    String systemInputStr = request.getParameter("systemInputStr");
    String strGroupId = request.getParameter("groupStr");
    String effectiveDate = gmPricingRequestForm.getEffectiveDate();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HashMap hmParams = new HashMap();
    hmParams.put("STRREQUESTID", strRequestId);
    hmParams.put("STRACCOUNTID", strAccountId);
    hmParams.put("EFFECTIVEDATE", effectiveDate);
    hmParams.put("INPUTSTR", systemInputStr);
    hmParams.put("PARTFLAG", "Y");
    hmParams.put("GROUPID", strGroupId);
    hmParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    log.debug("getGroupByGridString "
        + (getGroupByGridString(hmParams, request, gmPricingRequestForm.getStrAccountId(),
            strSessCompanyLocale)));
    pw.write(getGroupByGridString(hmParams, request, gmPricingRequestForm.getStrAccountId(),
        strSessCompanyLocale));

    pw.flush();
    return null;
  }

  /**
   * get Un price system details
   * 
   * @author
   * @date
   * @param
   */

  public ActionForward UnPricingSystemReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);
    GmGroupPricingBean gmGroupPricingBean = new GmGroupPricingBean();
    HashMap hmPendingSystemAccountPrice = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alPendingSystemAccountPrice = new ArrayList();
    String strPendingSystemCount = "UnPriced System(s)";
    String strPendingSystemAccountPriceGridXML = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmPricingRequestForm);

    String strGroupAcc = gmPricingRequestForm.getStrGroupAcc();
    String strAccType = gmPricingRequestForm.getStrAccType();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    String strVmProPath = "properties.labels.sales.pricing.GmPendingSystemAccountPrice";

    hmParam.put("INPUTACCTS", gmPricingRequestForm.getStrAccountId());
    hmParam.put("STATUS", "");
    hmParam.put("SYSTEMS", gmPricingRequestForm.getSysTranId());
    hmParam.put("STRACCTYPE", strAccType);
    hmParam.put("STRGROUPACC", strGroupAcc);

    log.debug("	hmParam::::" + hmParam);
    if (strAccType.equals("903107")) {
      log.debug("IF strGroupAcc::" + strGroupAcc + ":::" + strAccType);
      hmPendingSystemAccountPrice = gmGroupPricingBean.fetchPendingSystemAccountPrice(hmParam);
    } else {
      log.debug("ELSE strGroupAcc::" + strGroupAcc + ":::" + strAccType);
      hmPendingSystemAccountPrice = gmGroupPricingBean.fetchGPOPendingSystemAccountPrice(hmParam);
    }
    alPendingSystemAccountPrice = (ArrayList) hmPendingSystemAccountPrice.get("ALNOPRICESYSTEM");
    // log.debug(" alPendingSystemAccountPrice ------- " +alPendingSystemAccountPrice);
    strPendingSystemCount =
        strPendingSystemCount + " - "
            + (String) hmPendingSystemAccountPrice.get("STRPENDINGSYSTEMCOUNT");
    strPendingSystemAccountPriceGridXML =
        GmCommonClass.replaceForXML(getPriceStatusGrid(alPendingSystemAccountPrice,
            "GmPendingSystemAccountPrice.vm", strSessCompanyLocale, strVmProPath));
    log.debug("Come to My Action " + strPendingSystemCount);
    gmPricingRequestForm.setPendingSystemAccountPriceGridXML(strPendingSystemAccountPriceGridXML);
    gmPricingRequestForm.setPendingSystemAccountCount(strPendingSystemCount);

    return mapping.findForward("GmUnPricingSystem");
  }

  /**
   * get Un price Group details
   * 
   * @author
   * @date
   * @param
   */

  public ActionForward UnPricingGroupReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);
    GmGroupPricingBean gmGroupPricingBean = new GmGroupPricingBean();
    HashMap hmPendingGroupAccountPrice = new HashMap();

    HashMap hmParam = new HashMap();
    ArrayList alPendingGroupAccountPrice = new ArrayList();
    String strPendingGroupCount = "UnPriced Group(s)";
    String strPendingGroupAccountPriceGridXML = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmPricingRequestForm);

    String strGroupAcc = gmPricingRequestForm.getStrGroupAcc();
    String strAccType = gmPricingRequestForm.getStrAccType();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    String strVmProPath = "properties.labels.sales.pricing.GmPendingGroupAccountPrice";

    hmParam.put("INPUTACCTS", gmPricingRequestForm.getStrAccountId());
    hmParam.put("STATUS", "");
    hmParam.put("GRPDATERANGE", "0"); // 90 Days -- to get the data
    hmParam.put("STRACCTYPE", strAccType);
    hmParam.put("STRGROUPACC", strGroupAcc);
    if (strAccType.equals("903107")) {
      log.debug("IF strGroupAcc::" + strGroupAcc + ":::" + strAccType);
      hmPendingGroupAccountPrice = gmGroupPricingBean.fetchPendingGroupAccountPrice(hmParam);
    } else {
      log.debug("ELSE strGroupAcc::" + strGroupAcc + ":::" + strAccType);
      hmPendingGroupAccountPrice = gmGroupPricingBean.fetchGPOPendingGroupAccountPrice(hmParam);
    }
    alPendingGroupAccountPrice = (ArrayList) hmPendingGroupAccountPrice.get("ALNOPRICEGROUP");
    strPendingGroupCount =
        strPendingGroupCount + " - "
            + (String) hmPendingGroupAccountPrice.get("STRPENDINGGROUPCOUNT");
    // log.debug(" strPendingSystemCount req data " + strPendingGroupCount);
    strPendingGroupAccountPriceGridXML =
        GmCommonClass.replaceForXML(getPriceStatusGrid(alPendingGroupAccountPrice,
            "GmPendingGroupAccountPrice.vm", strSessCompanyLocale, strVmProPath));

    gmPricingRequestForm.setPendingGroupAccountPriceGridXML(strPendingGroupAccountPriceGridXML);
    gmPricingRequestForm.setPendingGroupAccountCount(strPendingGroupCount);

    return mapping.findForward("GmUnPricingGroup");
  }

  /**
   * get GPO details
   * 
   * @author
   * @date
   * @param
   */

  public ActionForward GroupAccountDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    gmPricingRequestForm.loadSessionParameters(request);
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String requestId = gmPricingRequestForm.getHreqid();
    String strAccessFilter = getAccessFilter(request, response);
    // get the request details
    String strGpoId = GmCommonClass.parseNull(request.getParameter("hrpoId"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    log.debug("strGpoId --> " + strGpoId);
    ArrayList alReturn = new ArrayList();
    alReturn = gmPricingRequestBean.loadGroupAccountDetails(strGpoId, "");
    log.debug("alReturn size  --> " + alReturn.size());
    String strTest = "";
    if (alReturn != null) {

      templateUtil.setDataList("alGroupAccDetails", alReturn);
      templateUtil.setTemplateSubDir("sales/pricing/templates");
      templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
          "properties.labels.sales.pricing.GmGroupAccountDetails", strSessCompanyLocale));
      templateUtil.setTemplateName("GmGroupAccountDetails.vm");
      strTest = templateUtil.generateOutput();
    }
    log.debug("strTest --> " + strTest);
    request.setAttribute("GroupData", strTest);
    return mapping.findForward("GmPricingGroupAccount");

  }

  /**
   * getPriceStatusGrid xml content generation for grid
   * 
   */
  private String getPriceStatusGrid(ArrayList alGridData, String strTemplateName,
      String strSessCompanyLocale, String strVmProPath) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmProPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(strTemplateName);

    return templateUtil.generateOutput();
  }

  public ActionForward loadPricingRequestRules(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    String strRequestID = GmCommonClass.parseNull(request.getParameter("hReqID"));
    String strRuleID = GmCommonClass.parseNull(request.getParameter("hRuleID"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();
    ArrayList alRulesList = null;
    String strOutPut = "";
    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;

    alRulesList = gmPricingRequestBean.loadPricingRequestRules(strRequestID, strRuleID);

    log.debug(" alRulesListalRulesList " + alRulesList + " strRequestID " + strRequestID
        + " hRuleID" + strRuleID);

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alRulesList", alRulesList);
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmPricingRuleDetails", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPricingRuleDetails.vm");

    strOutPut = templateUtil.generateOutput();

    log.debug(" strOutPut " + strOutPut);
    gmPricingRequestForm.setPricingRequestRules(strOutPut);

    return mapping.findForward("GmPricingRequestRules");
  }

  /**
   * getAnalysisViewGridDetails Ajax call to get the details of request AnalysisView
   * 
   * @author
   * @date May 02, 2012
   * @param
   */
  public ActionForward getActivityGridDetails(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParam = new HashMap();
    HttpSession session = request.getSession(false);
    GmPricingRequestForm gmPricingRequestForm = (GmPricingRequestForm) form;
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmPricingRequestForm);
    hmParam.put("AJAX", "YES");
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(getActivityGridString(hmParam, request, gmPricingRequestForm.getStrAccountId(),
        strSessCompanyLocale));
    pw.flush();
    return null;
  }


}
