
package com.globus.sales.pricing.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmGroupPartMapForm;


public class GmMultiPartGroupAction extends GmAction {
	
	
	/**
	    * @return org.apache.struts.action.ActionForward
	    * @roseuid 
	    */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

				
			Logger log = GmLogger.getInstance(this.getClass().getName());
			GmGroupPartMapForm gmGroupPartMapForm = (GmGroupPartMapForm) form;
			GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
			HashMap hmParam = new HashMap();
			
			hmParam = GmCommonClass.getHashMapFromForm(gmGroupPartMapForm);
			RowSetDynaClass rdResult= null;
						
			//alReportList = GmCommonClass.parseNullArrayList(gmTagBean.fetchTagHistory(gmTagForm.getTagID()));
			rdResult = gmDemandSetupBean.fetchMultiPartGroup(hmParam);
			log.debug("rdResult:" + rdResult);
			gmGroupPartMapForm.setLdtResult(rdResult.getRows());
			
		
		return mapping.findForward("GmMultiPartGroup");
	}
}
