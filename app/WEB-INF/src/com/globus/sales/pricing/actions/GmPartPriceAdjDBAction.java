package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.sales.pricing.beans.GmPartPriceAdjDBBean;
import com.globus.sales.pricing.forms.GmPartPriceAdjForm;

public class GmPartPriceAdjDBAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadPartPriceAdjDtls : To load the Part Price Adjustment details to the dashboard
   * 
   * @exception AppError
   */
  public ActionForward loadPartPriceAdjDtls(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmPartPriceAdjForm gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
    gmPartPriceAdjForm.loadSessionParameters(request);
    GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    GmPartPriceAdjDBBean gmPartPriceAdjDBBean = new GmPartPriceAdjDBBean(getGmDataStoreVO());

    String strStatus = "";
    String strOpt = "";
    ArrayList alType = new ArrayList();
    ArrayList alAccName = new ArrayList();
    ArrayList alInitBy = new ArrayList();
    ArrayList alDateFld = new ArrayList();
    ArrayList alGrpAccName = new ArrayList();
    ArrayList alAdjDetails = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alActions = new ArrayList();

    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjForm);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strStatus = GmCommonClass.parseNull((String) hmParam.get("HSTATUS"));
    strStatus =
        strOpt.equals("Load") ? (strStatus.equals("") ? "105285,105286" : strStatus) : strStatus;
    hmParam.put("HSTATUS", strStatus);
    alType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PADJTP"));
    alAccName = GmCommonClass.parseNullArrayList(gmSalesBean.reportAccount());
    alGrpAccName =
        GmCommonClass.parseNullArrayList(gmPartyInfoBean.fetchPartyNameList("7003", "Y")); // 7003 :
                                                                                           // Group
                                                                                           // Account
    alDateFld = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ADJDT"));
    alInitBy = GmCommonClass.parseNullArrayList(gmPartPriceAdjDBBean.fetchUserNameList());
    alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PADJST"));
    alActions = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ADJACT"));

    alAdjDetails = gmPartPriceAdjDBBean.fetchPartPriceAdjDtlsToDash(hmParam); // Fetch the details
    // String strSesPartyId = GmCommonClass.parseNull((String)gmPartPriceAdjForm.getSessPartyId());

    /*
     * GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(); HashMap
     * hmPricingSetupAccess =
     * GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
     * "PRICE_APPROVE_SUBMT")); String strPrcSubmitFl = GmCommonClass.parseNull((String)
     * hmPricingSetupAccess.get("UPDFL")); request.setAttribute("SUBMITACCFL", strPrcSubmitFl);
     */

    gmPartPriceAdjForm.setAlType(alType);
    gmPartPriceAdjForm.setAlAccName(alAccName);
    gmPartPriceAdjForm.setAlGrpAccName(alGrpAccName);
    gmPartPriceAdjForm.setAlDateFld(alDateFld);
    gmPartPriceAdjForm.setAlInitBy(alInitBy);
    gmPartPriceAdjForm.setAlStatus(alStatus);
    gmPartPriceAdjForm.setAlActions(alActions);
    gmPartPriceAdjForm.setAlAdjDetails(alAdjDetails);
    gmPartPriceAdjForm.setStatus(strStatus);

    return mapping.findForward("GmPartPriceAdjDash");
  }

  /*
   * approveDeniePartPriceAdj -- Method for Approving/Denying the Part Adjustment Details
   */

  /*
   * public ActionForward approveDeniePartPriceAdj(ActionMapping mapping, ActionForm form,
   * HttpServletRequest request, HttpServletResponse response) throws Exception { GmPartPriceAdjForm
   * gmPartPriceAdjForm = (GmPartPriceAdjForm) form;
   * gmPartPriceAdjForm.loadSessionParameters(request); GmPartPriceAdjDBBean gmPartPriceAdjDBBean =
   * new GmPartPriceAdjDBBean(getGmDataStoreVO()); // successMessage HashMap hmParam = new
   * HashMap(); hmParam = GmCommonClass.getHashMapFromForm(gmPartPriceAdjForm); String strOutString
   * = ""; HashMap hmReslut = new HashMap(); hmReslut =
   * gmPartPriceAdjDBBean.apprDeniePartPriceAdj(hmParam); strOutString =
   * GmCommonClass.parseNull((String) hmReslut.get("OUTTRANSACTIONS")); if
   * (!strOutString.equals("")) { gmPartPriceAdjForm.setSuccessMessage(strOutString); } return
   * loadPartPriceAdjDtls(mapping, form, request, response); }
   */
}
