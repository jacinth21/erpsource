package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonControls;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.pricing.beans.GmGroupPartPricingBean;
import com.globus.sales.pricing.forms.GmGroupPartPricingForm;

public class GmGroupPartPricingDAction extends GmDispatchAction {

  /**
   * loadPriceRequest Initializing the price request
   * 
   * @author rmayya
   * @date Dec 3, 2009
   * @param
   */
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward loadSystemPrice(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmGroupPartPricingForm gmGroupPartPricingForm = (GmGroupPartPricingForm) form;
    gmGroupPartPricingForm.loadSessionParameters(request);
    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(getGmDataStoreVO());
    String strPartyId = gmGroupPartPricingForm.getSessPartyId();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    HashMap hmAccess = new HashMap();
    String accessFlag = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // Passing partyId to get access
    if (!gmGroupPartPricingBean.getAccessFlag(gmGroupPartPricingForm.getSessPartyId())) {
      throw new AppError("", "20690");
    }

    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    String strAction = gmGroupPartPricingForm.getHaction();
    String strStropt = GmCommonClass.parseNull(gmGroupPartPricingForm.getStrOpt());
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());

    /*
     * if (strAction.equals("sysChange") && !"0".equals(gmGroupPartPricingForm.getSystemListId())) {
     * 
     * HashMap hmReturn =
     * gmGroupPartPricingBean.fetchGrpPartPriceDetails(GmCommonClass.getHashMapFromForm
     * (gmGroupPartPricingForm)); gmGroupPartPricingForm.setGridXmlData(getGridString(hmReturn,
     * gmGroupPartPricingForm)); } else { gmGroupPartPricingForm.setGridXmlData(getGridString(new
     * HashMap(),gmGroupPartPricingForm)); }
     */
    if (!strStropt.equals("")) {
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1162");
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

      HashMap hmReturn =
          gmGroupPartPricingBean.fetchGrpPartPriceDetails(GmCommonClass
              .getHashMapFromForm(gmGroupPartPricingForm));

      if (accessFlag.equals("Y")) {
        hmReturn.put("acl", "10");
      }
      String strData = GmCommonClass.parseNull((String) hmReturn.get("NO_DATA"));
      gmGroupPartPricingForm.setStrNoData(strData);
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1164");
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

      hmReturn.put("ADVPVIEWACCESS", accessFlag);
      gmGroupPartPricingForm.setAdvpViewAccess(accessFlag);

      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1165");
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

      hmReturn.put("PRINCIPALVIEWACCESS", accessFlag);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

      gmGroupPartPricingForm.setGridXmlData(getGridString(hmReturn, gmGroupPartPricingForm));

    } else {
      gmGroupPartPricingForm.setStrNoData("YES");
      gmGroupPartPricingForm.setGridXmlData(getGridString(new HashMap(), gmGroupPartPricingForm));
    }
    // fetch all systems
    ArrayList alSalesGrp = gmProjectBean.loadSetMap("GROUPPARTMAPPING");
    gmGroupPartPricingForm.setAlSystemList(alSalesGrp);
    gmGroupPartPricingForm.setGridXmlSysData(GmCommonControls.getComboXml(alSalesGrp));

    HashMap hmParam = new HashMap();
    hmParam.put("STROPT", "40045");
    ArrayList alGroupList = gmDemandSetupBean.loadDemandGroupList(hmParam);
    gmGroupPartPricingForm.setAlGroupList(alGroupList);
    gmGroupPartPricingForm.setGridXmlGroupData(GmCommonControls.getComboXml(alGroupList));

    // fetch code id's for construct levels
    ArrayList alPricedTypeList = GmCommonClass.getCodeList("GPMTYP");
    gmGroupPartPricingForm.setAlPricedTypeList(alPricedTypeList);
    // fetch code id's for status
    ArrayList alStatusList = GmCommonClass.getCodeList("GRPSTS");
    gmGroupPartPricingForm.setAlStatusList(alStatusList);
    // added to make the "Active" as default status
    gmGroupPartPricingForm.setStatusList("1006430");
    ArrayList alGroupTypeList =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("GRPTYP"));
    gmGroupPartPricingForm.setAlGroupTypeList(alGroupTypeList);
    gmGroupPartPricingForm.setGridXmlGroupTypeData(GmCommonControls.getComboXml(alGroupTypeList));


    return mapping.findForward("GmGroupPartPriceSetup");
  }

  /**
   * savePriceRequest saving the price request
   * 
   * @author rmayya
   * @date Dec 15, 2009
   * @param
   */
  public ActionForward saveGroupPartPrice1(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmGroupPartPricingForm gmGroupPartPricingForm = (GmGroupPartPricingForm) form;
    gmGroupPartPricingForm.loadSessionParameters(request);
    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(getGmDataStoreVO());
    gmGroupPartPricingBean.savePriceDetails(GmCommonClass
        .getHashMapFromForm(gmGroupPartPricingForm));
    return loadSystemPrice(mapping, gmGroupPartPricingForm, request, response);
  }

  public ActionForward saveGroupPartPrice(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HashMap hmReturn = null;
    String strErrorGrpIds = "";
    String strSuccessGrpDtls = "";
    String strForward = "";

    GmGroupPartPricingForm gmGroupPartPricingForm = (GmGroupPartPricingForm) form;
    gmGroupPartPricingForm.loadSessionParameters(request);
    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(getGmDataStoreVO());
    String strStropt = GmCommonClass.parseNull(gmGroupPartPricingForm.getStrOpt());
    hmReturn =
        gmGroupPartPricingBean.savePriceDetails(GmCommonClass
            .getHashMapFromForm(gmGroupPartPricingForm));
    strErrorGrpIds = GmCommonClass.parseNull((String) hmReturn.get("ERRORGRP"));

    String strSysId = GmCommonClass.parseZero(gmGroupPartPricingForm.getSystemListId());
    String strPriceTypeId = GmCommonClass.parseNull(gmGroupPartPricingForm.getPricedTypeId());
    String strGroupTypeId = GmCommonClass.parseZero(gmGroupPartPricingForm.getGroupTypeId());
    String strGroupId = GmCommonClass.parseZero(gmGroupPartPricingForm.getGroupId());
    String strPartNums = GmCommonClass.parseNull(gmGroupPartPricingForm.getPartNumbers());
    String strHSearch = GmCommonClass.parseZero(gmGroupPartPricingForm.getHsearch());
    String strParams =
        "&systemListId=" + strSysId + "&pricedTypeId=" + strPriceTypeId + "&groupTypeId="
            + strGroupTypeId + "&groupId=" + strGroupId + "&partNumbers=" + strPartNums
            + "&hsearch=" + strHSearch + "&strErrorGrpIds=" + strErrorGrpIds;
    strForward = "/gmGroupPartPricingDAction.do?method=loadSystemPrice&strOpt=Reload" + strParams;


    return actionRedirect(strForward, request);

  }

  /**
   * getGroupByGridString xml content generator for group by grid
   * 
   * @author rmayya
   * @date Dec 15, 2009
   * @param
   */

  private String getGridString(HashMap hmParam, GmGroupPartPricingForm form) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    if (hmParam != null) {

      String strCurrSign =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol"));
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
      hmParam.put("CURRENCYSYMBOL", strCurrSign);
      templateUtil.setDataList("alGroupResult", (ArrayList) hmParam.get("alGroupResult"));
      templateUtil.setDataList("alPartResult", (ArrayList) hmParam.get("alPartResult"));
      templateUtil.setDataMap("hmAccessType", hmParam);
      templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
          "properties.labels.sales.pricing.GmGroupPartPricing", strSessCompanyLocale));
      // form.setAlGroupList(GmCommonClass.parseNullArrayList((ArrayList)
      // hmParam.get("alGroupResult")));
      // form.setAlPartList(GmCommonClass.parseNullArrayList((ArrayList)
      // hmParam.get("alPartResult")));
    }
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setTemplateName("GmGroupPartPricing.vm");

    return templateUtil.generateOutput();
  }

}
