package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sales.pricing.beans.GmGroupPartPricingBean;
import com.globus.sales.pricing.forms.GmSystemPriceBandForm;

public class GmSystemPriceBandAction extends GmAction {


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    instantiate(request, response);
    GmSystemPriceBandForm gmSystemPriceBandForm = (GmSystemPriceBandForm) form;
    gmSystemPriceBandForm.loadSessionParameters(request);
    GmGroupPartPricingBean gmGroupPartPricingBean = new GmGroupPartPricingBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();


    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    gmSystemPriceBandForm.loadSessionParameters(request);
    String strPartyId = gmSystemPriceBandForm.getSessPartyId();


    String hAction = gmSystemPriceBandForm.getHaction();
    String strInputString = gmSystemPriceBandForm.getHinputString();
    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1163");
    String accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if (!accessFlag.equals("Y")) {
      gmSystemPriceBandForm.setEnableADVPChange("disabled");
    }

    if (hAction.equals("save")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmSystemPriceBandForm);
      // saveSystemPriceBand
      gmGroupPartPricingBean.savePriceBandValue(hmParam);
    }
    // loadSystemPriceBand

    ArrayList alReturn = gmGroupPartPricingBean.loadPriceBand();

    gmSystemPriceBandForm.setGridXmlData(gmGroupPartPricingBean.getXmlGridData(alReturn,
        strSessCompanyLocale));


    return mapping.findForward("GmSystemPriceBand");
  }


}
