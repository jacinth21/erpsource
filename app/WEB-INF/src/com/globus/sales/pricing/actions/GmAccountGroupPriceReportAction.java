package com.globus.sales.pricing.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.pricing.beans.GmGroupPricingBean;
import com.globus.sales.pricing.forms.GmAccountGroupPriceReportForm;


public class GmAccountGroupPriceReportAction extends GmSalesDispatchAction {


  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid
   */
  public ActionForward accountGroupPriceReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmAccountGroupPriceReportForm gmAccountGroupPriceReportForm =
        (GmAccountGroupPriceReportForm) form;
    gmAccountGroupPriceReportForm.loadSessionParameters(request);
    GmGroupPricingBean gmGroupPricingBean = new GmGroupPricingBean(getGmDataStoreVO());

    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
    HashMap hmParam = new HashMap();
    String strAccessFilter = "";
    String strOpt = gmAccountGroupPriceReportForm.getStrOpt();
    String strAction = gmAccountGroupPriceReportForm.getHaction();
    String strSetID = gmAccountGroupPriceReportForm.getSetID();

    // To get the Access Level information from Materialized view
    strAccessFilter = getAccessFilter(request, response);

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists(strAccessFilter);

    gmAccountGroupPriceReportForm.setAlAccount(GmCommonClass
        .parseNullArrayList((ArrayList) hmSalesFilters.get("ACCT")));
    // gmPricingStatusReportForm.setAlAccount(GmCommonClass.parseNullArrayList((ArrayList)gmCommonBean.getAccountList(strAccessFilter)));

    gmAccountGroupPriceReportForm.setAlSets(gmProj.loadSetMap("GROUPPARTMAPPING"));

    hmParam = GmCommonClass.getHashMapFromForm(gmAccountGroupPriceReportForm);

    ArrayList alGroupList = gmDemandSetupBean.loadGroupList(hmParam);
    gmAccountGroupPriceReportForm.setAlGroupList(alGroupList);


    RowSetDynaClass rdResult = null;
    if (strAction.equals("reload")) {

      String strSelectedGrps =
          GmCommonClass.createInputString(gmAccountGroupPriceReportForm.getCheckGroups());
      // log.debug(" The values of strSelectedSets "+ strSelectedSets);
      hmParam.put("INPUTGROUPS", strSelectedGrps);
      log.debug("hmParam:.........." + hmParam);
      rdResult = gmGroupPricingBean.fetchAccountGroupPrice(hmParam);

      gmAccountGroupPriceReportForm.setLdtResult(rdResult.getRows());

    }
    return mapping.findForward("GmAccountGroupPriceReport");
  }
}
