package com.globus.sales.pricing.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.pricing.beans.GmImpactAnalysisBean;
import com.globus.sales.pricing.beans.GmPriceApprovalRequestBean;
import com.globus.sales.pricing.beans.GmPriceApprovalTransBean;
import com.globus.sales.pricing.forms.GmPriceApprovalRequestForm;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPriceApprovalRequestAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
  GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
  private static String strComnPath = GmCommonClass.getString("GMCOMMON");

  /*
   * fetchPriceReqDetails(): Method is written for populating the all the Drop down values available
   * in the JSP. ANd setting the Default values to the corresponding fields. Author: HReddi
   */

  public ActionForward fetchPriceReqDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);

    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    HashMap hmCompanyInfo =
        GmCommonClass.parseNullHashMap(GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO()
            .getCmpid()));
    String strCompanyCurrency = GmCommonClass.parseNull((String) hmCompanyInfo.get("CMPCURRSMB"));
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReqDetails = new ArrayList();
    ArrayList alSystemDetails = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmHeaderValues = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
    String strOpt = GmCommonClass.parseNull(gmPriceApprovalRequestForm.getStrOpt());
    String strRebatePrice = "";
    setAccessPermissions(mapping, form, request, response);
    String strRequestId = GmCommonClass.parseNull(gmPriceApprovalRequestForm.getPriceRequestId());
    if (!strRequestId.equals("")) {
      log.debug("pricerequestID>>>>" + strRequestId);

      strRequestId =
          strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
      hmResult = gmPriceApprovalRequestBean.loadAccoutDetails(strRequestId);
      GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmResult);
    }
    if (strOpt.equals("Load")) {
      log.debug("GmPriceApprovalRequestAction Enter Fetch-Load$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
      hmReturn = gmPriceApprovalRequestBean.fetchPriceRequestDetails(hmParam);
      log.debug("hmReturn>>>>>>>>>" + hmReturn);
      if (hmReturn.size() > 0) {
        gmPriceApprovalRequestForm.setCheckFlag("Y");
      }
      hmHeaderValues = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HEADERDATA"));
      alReqDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("PRICEREQDETAILS"));
      alSystemDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("SYSTEMDETAILS"));
      strRebatePrice = GmCommonClass.parseZero(gmPriceApprovalRequestForm.getRebate());
      GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmHeaderValues);
      hmReturn.put("PRICEREQDATA", alReqDetails);
      hmReturn.put("SYSTEMDATA", alSystemDetails);
      hmReturn.put("REBATEPRICE", strRebatePrice);
      hmReturn.put("APPROVEACCESS", gmPriceApprovalRequestForm.getApproveAccFl());
      hmReturn.put("DENYACCESS", gmPriceApprovalRequestForm.getDenyAccesFl());
      hmReturn.put("DELETEACCESS", gmPriceApprovalRequestForm.getDeleteAccFl());
      hmReturn.put("COMPANYCURRENCY", strCompanyCurrency);
      hmReturn.put("CURRVAL",hmHeaderValues.get("CURRVAL"));
      int getCount= Integer.parseInt((String) hmHeaderValues.get("GTCOUNT"));
      String strxmlGridData = generateOutPut(hmReturn);
      gmPriceApprovalRequestForm.setXmlGridData(strxmlGridData);
      gmPriceApprovalRequestForm.setRecountCount(Integer.toString(alReqDetails.size()));
      String strId = GmCommonClass.parseNull(gmPriceApprovalRequestForm.getPriceRequestId());
      String strType = "26240010";
      GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());

      ArrayList alResult = new ArrayList();
      alResult =
          GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo(strType, strId));
      log.debug("fetch upload data=====" + alResult.size());
      int count = alResult.size();
      String cunt = "" + count;
      gmPriceApprovalRequestForm.setCount(cunt);
      
      fchIdnNumber(mapping, form, request, response);
      
      if (getCount>0 && gmPriceApprovalRequestForm.getIdnUpdAccess().equals("Y"))
      {
      	gmPriceApprovalRequestForm.setStrDisabled("true");
      }
      else 
      {
        gmPriceApprovalRequestForm.setStrDisabled("false");  
      }
    }
    return mapping.findForward("gmPriceApprovalRequest");
  }

  /*
   * updatePriceDetails(): Method for updating the edited Price values from the VIEW Going to update
   * Price Change Type, Change value, Proposed Price, Status and Implement Flag for the selected
   * System and Group: (HReddi). Checking priceUpdateFl and creating impact analysis if > 1 (Matt)
   */
  public ActionForward updatePriceDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmImpactAnalysisBean gmImpactAnalysisBean = new GmImpactAnalysisBean(gmDataStoreVO);
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    GmPriceApprovalTransBean gmPriceApprovalTransBean =
        new GmPriceApprovalTransBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alReqDetails = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmHeaderValues = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
    HashMap hmCompanyInfo =
        GmCommonClass.parseNullHashMap(GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO()
            .getCmpid()));
    String strCompanyCurrency = GmCommonClass.parseNull((String) hmCompanyInfo.get("CMPCURRSMB"));

    setAccessPermissions(mapping, form, request, response);
    hmParam.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));

    String strPriceReqId = gmPriceApprovalTransBean.updatePriceRequestDetails(hmParam);
    HashMap hmAnlysis = new HashMap();
    hmAnlysis.put("PRICEREQUESTID", strPriceReqId);
    hmAnlysis.put("COMPANYINFO", GmCommonClass.parseNull(request.getParameter("companyInfo")));
    hmAnlysis.put("USERID", GmCommonClass.parseNull((String) hmParam.get("USERID")));

    /* IMPACT ANLAYSIS - Checking if price is updated creating impact analysis(PRT-R203) */
    gmImpactAnalysisBean.checkPriceUpdateFlag(hmAnlysis);

    String strRebatePrice = GmCommonClass.parseNull(gmPriceApprovalRequestForm.getRebate());
    String strPath =
        "/gmPriceApprovalRequestAction.do?method=fetchPriceReqDetails&strOpt=Load&priceRequestId="
            + strPriceReqId + "&rebate=" + strRebatePrice;
    return actionRedirect(strPath, request);

  }

  /*
   * updatePriceDetails(): Method is written for updating the edited Price values from the VIEW
   * Going to update Price Change Type, Change value, Proposed Price, Status and Implement Flag for
   * the selected System and Group: HReddi
   */

  public ActionForward removeSystems(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);

    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    GmPriceApprovalTransBean gmPriceApprovalTransBean =
        new GmPriceApprovalTransBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    ArrayList alReqDetails = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmHeaderValues = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
    HashMap hmCompanyInfo =
        GmCommonClass.parseNullHashMap(GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO()
            .getCmpid()));
    String strCompanyCurrency = GmCommonClass.parseNull((String) hmCompanyInfo.get("CMPCURRSMB"));

    setAccessPermissions(mapping, form, request, response);

    String strPriceReqId = gmPriceApprovalTransBean.updateSystemDetails(hmParam);
    String strRebatePrice = GmCommonClass.parseZero(gmPriceApprovalRequestForm.getRebate());

    hmParam.put("PRICEREQUESTID", strPriceReqId);
    hmParam.put("REBATE", strRebatePrice);
    hmReturn = gmPriceApprovalRequestBean.fetchPriceRequestDetails(hmParam);
    hmHeaderValues = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HEADERDATA"));
    alReqDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("PRICEREQDETAILS"));

    GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmHeaderValues);

    hmReturn.put("PRICEREQDATA", alReqDetails);
    hmReturn.put("REBATEPRICE", strRebatePrice);
    hmReturn.put("APPROVEACCESS", gmPriceApprovalRequestForm.getApproveAccFl());
    hmReturn.put("DENYACCESS", gmPriceApprovalRequestForm.getDenyAccesFl());
    hmReturn.put("DELETEACCESS", gmPriceApprovalRequestForm.getDeleteAccFl());
    hmReturn.put("COMPANYCURRENCY", strCompanyCurrency);
    String strxmlGridData = generateOutPut(hmReturn);
    gmPriceApprovalRequestForm.setXmlGridData(strxmlGridData);
    gmPriceApprovalRequestForm.setRecountCount(Integer.toString(alReqDetails.size()));

    return mapping.findForward("gmPriceApprovalRequest");
  }


  /**
   * generateOutPut xml content generation for Price Request Details Report grid
   * 
   * @param HashMap
   * @return String
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    ArrayList alStatusType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PRQSTS"));
    // Removing [52122]Pending VP Approval,[52189]Approved Not Implemented,[52190]Approved
    // Implemented, [52121]Pending Approval, [52124] Pending and [52188]Approved & Denied status
    // from the STAGE drop down.
    for (int i = 0; i < alStatusType.size(); i++) {
      HashMap hmTemp = GmCommonClass.parseNullHashMap((HashMap) alStatusType.get(i));
      String strCodeId = GmCommonClass.parseNull((String) hmTemp.get("CODEID"));
      if ((strCodeId.equals("52122")) || (strCodeId.equals("52189")) || (strCodeId.equals("52190"))
          || (strCodeId.equals("52121")) || (strCodeId.equals("52188"))) {
        alStatusType.remove(i);
        i--;
      }
    }
    ArrayList alPriceType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PCHTY"));
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setTemplateName("GmPriceApprovalRequest.vm");
    templateUtil.setDataList("alStatusType", alStatusType);
    templateUtil.setDropDownMaster("alPriceType", alPriceType);
    return templateUtil.generateOutput();
  }

  /**
   * setAccessPermissions method is ment for setting the required access to the Approve, Denied and
   * Status Updation Access.
   * 
   * @param HashMap
   * @return VOID
   */
  private void setAccessPermissions(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);

    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    GmPartyBean gmParty = new GmPartyBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmApproveAccess = new HashMap();
    HashMap hmDenyAccess = new HashMap();
    HashMap hmDeleteAccess = new HashMap();
    HashMap hmStatusUPDAccess = new HashMap();
    HashMap hmGenPriceFile = new HashMap();
    HashMap hmIdnUpdAccess = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
    ArrayList alGPORefList = new ArrayList();
    HashMap hmCompanyInfo =
        GmCommonClass.parseNullHashMap(GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO()
            .getCmpid()));

    // alGPORefList = GmCommonClass.parseNullArrayList((ArrayList)
    // gmParty.reportPartyNameList("7011"));
    gmPriceApprovalRequestForm.setAlGPORefList(alGPORefList);

    // Get the user access to Denied
    String strPartyId = (String) request.getSession().getAttribute("strSessPartyId");
    hmDenyAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_DENIED_ACCESS"));
    String strDenyAccessFl = GmCommonClass.parseNull((String) hmDenyAccess.get("UPDFL"));
    gmPriceApprovalRequestForm.setDenyAccesFl(strDenyAccessFl);

    // Get the user access to Approve
    hmApproveAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_APPRVL_ACCESS"));
    String strApprAccessFl = GmCommonClass.parseNull((String) hmApproveAccess.get("UPDFL"));
    gmPriceApprovalRequestForm.setApproveAccFl(strApprAccessFl);

    // Get the user access to Delete the System
    hmDeleteAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_DELETE_ACCESS"));
    String strDeleteAccessFl = GmCommonClass.parseNull((String) hmDeleteAccess.get("UPDFL"));
    gmPriceApprovalRequestForm.setDeleteAccFl(strDeleteAccessFl);


    // Get the user access stage dropdown
    hmStatusUPDAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_STATUS_UPD_ACCESS"));
    String strStatusUPDAccess = GmCommonClass.parseNull((String) hmStatusUPDAccess.get("UPDFL"));
    gmPriceApprovalRequestForm.setStatusUpdAccFl(strStatusUPDAccess);


    // Get the Pricing Committee user access to pending pc approval
    hmApproveAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_PC_ACCESS"));
    String strPCAccessFl = GmCommonClass.parseNull((String) hmApproveAccess.get("UPDFL"));
    gmPriceApprovalRequestForm.setPcAccFl(strPCAccessFl);

    // Access for Generating price file - Matt
    hmGenPriceFile =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PR_GEN_PRICEFILE"));
    String strGenPrtFileFl = GmCommonClass.parseNull((String) hmGenPriceFile.get("UPDFL"));
    log.debug("strGenPrtFileFl =================================> " + strGenPrtFileFl);
    gmPriceApprovalRequestForm.setGenPrtFileFl(strGenPrtFileFl);
    
 // Restricting Access for IDN
    hmIdnUpdAccess =  GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "IDN_ACCESS"));
    String strIdnUpdAccess = GmCommonClass.parseNull((String) hmIdnUpdAccess.get("UPDFL"));
    log.debug("strIdnUpdAccess==> "+strIdnUpdAccess);
    
    gmPriceApprovalRequestForm.setIdnUpdAccess(strIdnUpdAccess);

    
  }

  /**
   * fetchGeneratePriceFile(): Method is written for generate Account price report corresponding
   * price request ID. DSandeep
   **/
  public ActionForward fetchGeneratePriceFile(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmHeaderValues = new HashMap();
    ArrayList alResult = new ArrayList();
    HashMap hmParm = new HashMap();
    String strRequestId = GmCommonClass.parseNull(request.getParameter("priceRequestId"));
    hmParm = gmPriceApprovalRequestBean.loadAccoutDetails(strRequestId);
    GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmParm);
    hmParam = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);

    hmReturn = gmPriceApprovalRequestBean.fetchPriceRequestDetails(hmParam);
    log.debug("strGenPrtFileFl =================================> " + hmReturn);
    hmHeaderValues = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HEADERDATA"));
    GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmHeaderValues);
    alResult = gmPriceApprovalRequestBean.getGeneratePriceFile(hmParam);
    String strxmlGridData = getPriceRequestGridString(alResult);
    gmPriceApprovalRequestForm.setXmlGridData(strxmlGridData);
    return mapping.findForward("GmGeneratePriceFile");
  }

  private String getPriceRequestGridString(ArrayList alResult) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateName("GmGeneratePriceFile.vm");
    return templateUtil.generateOutput();
  }

  /**
   * fetchAccountInfoDetails(): Method is written for fetch Account information for corresponding
   * price request ID. Author: Jreddy
   **/
  public ActionForward fetchAccountInfoDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();
    String strRequestId = gmPriceApprovalRequestForm.getPriceRequestId();
    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    hmParam = gmPriceApprovalRequestBean.loadAccoutDetails(strRequestId);
    GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmParam);
    return mapping.findForward("gmPriceAccountDetails");
  }

  /**
   * fetchBusinessQuestions(): Method is written for fetch Business Questions and Answers for
   * corresponding price request ID. Author: Jreddy
   **/
  public ActionForward fetchBusinessQuestions(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();

    String strRequestId = GmCommonClass.parseZero(gmPriceApprovalRequestForm.getPriceRequestId());
    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    HashMap hmQuestion = gmPriceApprovalRequestBean.fetchQuestionAnswer(strRequestId);

    ArrayList alQuestion = (ArrayList) hmQuestion.get("QUESTION");
    gmPriceApprovalRequestForm.setAlQuestion(alQuestion);

    ArrayList alAnswer = (ArrayList) hmQuestion.get("ANSWER");
    gmPriceApprovalRequestForm.setAlAnswer(alAnswer);

    ArrayList alAnswerGroup = (ArrayList) hmQuestion.get("ANSWERGROUP");
    gmPriceApprovalRequestForm.setAlAnswerGroup(alAnswerGroup);

    ArrayList alQuestionAnswer = (ArrayList) hmQuestion.get("QUESTIONANSWER");
    gmPriceApprovalRequestForm.setAlQuestionAnswer(alQuestionAnswer);

    return mapping.findForward("gmBusinessQuestions");
  }

  /*
   * uploadFile(): Method is written for fetch the upload file information based on price request ID
   * Author: Dsandeep
   */

  public ActionForward fetchUploadFileList(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    String strType = "26240010";
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    String strPriceReqId = gmPriceApprovalRequestForm.getPriceRequestId();
    log.debug("priceReqId====" + strPriceReqId);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();
    alResult =
        GmCommonClass
            .parseNullArrayList(gmCommonUploadBean.fetchUploadInfo(strType, strPriceReqId));
    log.debug("alResult======" + alResult);
    String strxmlGridData = getUploadFileGridString(alResult);
    gmPriceApprovalRequestForm.setXmlGridData(strxmlGridData);
    return mapping.findForward("GmUploadFile");
  }

  private String getUploadFileGridString(ArrayList alResult) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    templateUtil.setTemplateName("GmUploadFile.vm");
    return templateUtil.generateOutput();

  }

  /*
   * checkFileExist(): This Method is written for save the file information based on price request
   * ID and code id-26230731 Author: Dsandeep
   */
  public void checkFileExist(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    String strXMLString = "";
    String strJSON = "";
    String strId = "";
    String strType = "26230731";
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    String strRefGrp = "26240010";
    String strFileName = GmCommonClass.parseNull(request.getParameter("fileUpload"));
    String strFileNm = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
    log.debug("strFileNm====" + strFileNm);
    strId = GmCommonClass.parseNull(request.getParameter("priceRequestId"));
    HashMap hmFlag = new HashMap();

    String strFlg = gmCommonUploadBean.validateFileNameSts(strId, strRefGrp, strFileNm);
    response.setContentType("text/json");
    PrintWriter pw = response.getWriter();
    pw.write(strFlg);
    pw.flush();
    pw.close();

  }

  /*
   * saveUploadFile(): This Method is written for save the file information based on price request
   * ID and code id-26230731 Author: Dsandeep
   */
  public ActionForward saveUploadFile(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    String strXMLString = "";
    String strId = "";
    String strType = "26230731";
    String strRefGrp = "26240010";
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    strId = gmPriceApprovalRequestForm.getPriceRequestId();

    String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    FormFile strFileNm = gmPriceApprovalRequestForm.getFile();
    log.debug("strFileNm====" + strFileNm);
    String strFileName = strFileNm.getFileName();
    gmCommonUploadBean.validateFile(strFileNm);

    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();


    // * Here we going to upload files at some location into the system and once files are upload we
    // * are going to store that information into the database table. path for PRICING_DOCS
    // * is available into constant properties file.
    String strJSON = "";
    HashMap hmFileProp = new HashMap();
    ArrayList alFilePath = new ArrayList();
    HashMap hmFlag = new HashMap();
    alFilePath.add(strId);
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    hmFileProp.put("PATHPROPERTY", "PRICING_DOCS");
    hmFileProp.put("SEPARATOR", "\\");
    hmFileProp.put("APPENDPATH", alFilePath);
    String strUploadSubDir = gmProdCatUtil.getFilePath(hmFileProp);
    gmCommonUploadBean.uploadFile(strFileNm, strUploadSubDir, strUploadSubDir + strFileNm);
    HashMap hmparams = new HashMap();
    hmparams.put("REFID", strId);
    hmparams.put("REFGRP", strRefGrp);
    hmparams.put("FILENAME", strFileName);
    hmparams.put("SUBTYPE", strType);
    hmparams.put("USERID", strUserId);
    hmparams.put("REFTYPE", strRefGrp);
    gmCommonUploadBean.saveUploadUniqueInfo(hmparams);
    ArrayList alResult = new ArrayList();
    alResult =
        GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo(strRefGrp, strId));
    log.debug("alResult=======" + alResult);
    String strxmlGridData = getUploadFileGridString(alResult);
    gmPriceApprovalRequestForm.setXmlGridData(strxmlGridData);
    return mapping.findForward("GmUploadFile");
  }

  /*
   * deleteUpdateFile(): This Method is written for delete the upload files Author: Dsandeep
   */
  public ActionForward deleteUpdateFile(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    String strId = "";
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    strId = GmCommonClass.parseNull(request.getParameter("UploadFileId"));
    String strRefId = gmPriceApprovalRequestForm.getPriceRequestId();
    String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    String strInputStr = strId + "|";
    gmCommonUploadBean.deleteUploadInfo(strInputStr, strUserId);
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo("", strRefId));
    log.debug("alResult======" + alResult);
    String strxmlGridData = getUploadFileGridString(alResult);
    gmPriceApprovalRequestForm.setXmlGridData(strxmlGridData);
    return mapping.findForward("GmUploadFile");
  }

  /*
   * fileView(): This Method is written for to view the uploaded file Author: Dsandeep
   */

  public ActionForward fileView(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    File file = null;
    String strUploadString = "";
    FileInputStream istr = null;
    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    String strFileName = "";
    String strUploadSubDir = "";
    String strArtifactNm = "";
    String strSubTypeNm = "";
    String strUploadHomeDir = "";
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
    GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
    String strFileId = GmCommonClass.parseNull(request.getParameter("fileID"));
    log.debug("strFileId=====" + strFileId);
    String strTypeId = GmCommonClass.parseNull(request.getParameter("typeID"));
    log.debug("strFileId=====" + strTypeId);
    ArrayList alFilePath = new ArrayList();
    HashMap hmFileProp = new HashMap();
    alFilePath.add(strTypeId);
    hmFileProp.put("PATHPROPERTY", "PRICING_DOCS");
    hmFileProp.put("SEPARATOR", "\\");
    hmFileProp.put("APPENDPATH", alFilePath);
    strUploadHomeDir = gmProdCatUtil.getFilePath(hmFileProp);
    strFileName = gmCommonUploadBean.fetchFileName(strFileId);
    ServletOutputStream out = response.getOutputStream();
    response.setContentType("application/binary");
    response.setHeader("Content-disposition", "attachment;filename=" + strFileName);
    log.debug("file name......" + strUploadHomeDir + strFileName);
    try {
      file = new File(strUploadHomeDir + strFileName);
      istr = new FileInputStream(file);
    } catch (FileNotFoundException ffe) {
      log.debug("CATCH EXE" + ffe);
      response.sendRedirect(response.encodeRedirectUrl(strErrorFileToDispatch));
      throw new AppError("", "20676");
    }
    response.setContentLength((int) file.length());
    int curByte = -1;
    while ((curByte = istr.read()) != -1)
      out.write(curByte);
    istr.close();
    out.flush();
    out.close();
    return mapping.findForward("GmUploadFile");
  }

  public ActionForward pricingDetailsExportToExcel(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());


    HashMap hmCompanyInfo =
        GmCommonClass.parseNullHashMap(GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO()
            .getCmpid()));
    String strCompanyCurrency = GmCommonClass.parseNull((String) hmCompanyInfo.get("CMPCURRSMB"));
    String strAction = GmCommonClass.parseNull(request.getParameter("actionType"));
    String strcurrval = "";
    HashMap hmParam = new HashMap();
    ArrayList alReqDetails = new ArrayList();
    ArrayList alSystemDetails = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmHeaderValues = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
    String strOpt = GmCommonClass.parseNull(gmPriceApprovalRequestForm.getStrOpt());
    String strRebatePrice = "";
    setAccessPermissions(mapping, form, request, response);
    hmReturn = gmPriceApprovalRequestBean.fetchPriceRequestDetails(hmParam);
    String strRequestId = gmPriceApprovalRequestForm.getPriceRequestId();
    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    hmParam = gmPriceApprovalRequestBean.loadAccoutDetails(strRequestId);
    GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmParam);
    hmHeaderValues = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HEADERDATA"));
    log.debug("header valuers1" + hmHeaderValues);
    alReqDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("PRICEREQDETAILS"));
    log.debug("header valuers2" + alReqDetails);
    alSystemDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("SYSTEMDETAILS"));
    log.debug("header valuers4" + alSystemDetails);

   if(strAction.equals("EXPEXCEL"))
		   {
	   strcurrval = "1";
		   }else
		   {
			   strcurrval =  GmCommonClass.parseNull((String)hmHeaderValues.get("CURRVAL"));
					   
		   }
    strRebatePrice = GmCommonClass.parseZero(gmPriceApprovalRequestForm.getRebate());
    GmCommonClass.getFormFromHashMap(gmPriceApprovalRequestForm, hmHeaderValues);
    hmReturn.put("PRICEREQDATA", alReqDetails);
    hmReturn.put("SYSTEMDATA", alSystemDetails);
    hmReturn.put("REBATEPRICE", strRebatePrice);
    hmReturn.put("APPROVEACCESS", gmPriceApprovalRequestForm.getApproveAccFl());
    hmReturn.put("DENYACCESS", gmPriceApprovalRequestForm.getDenyAccesFl());
    hmReturn.put("DELETEACCESS", gmPriceApprovalRequestForm.getDeleteAccFl());
    hmReturn.put("COMPANYCURRENCY", strCompanyCurrency);
    hmReturn.put("CURRVAL",strcurrval);
    hmReturn.put("HACTION",strAction);
    
    request.setAttribute("hmReturn", hmReturn);
    // Fetch Business Questions in Excel
    HashMap hmQuestion = gmPriceApprovalRequestBean.fetchQuestionAnswer(strRequestId);
    ArrayList alQuestion = (ArrayList) hmQuestion.get("QUESTION");
    gmPriceApprovalRequestForm.setAlQuestion(alQuestion);
    ArrayList alAnswer = (ArrayList) hmQuestion.get("ANSWER");
    gmPriceApprovalRequestForm.setAlAnswer(alAnswer);
    ArrayList alAnswerGroup = (ArrayList) hmQuestion.get("ANSWERGROUP");
    gmPriceApprovalRequestForm.setAlAnswerGroup(alAnswerGroup);
    ArrayList alQuestionAnswer = (ArrayList) hmQuestion.get("QUESTIONANSWER");
    gmPriceApprovalRequestForm.setAlQuestionAnswer(alQuestionAnswer);

    // GPB Comparision
    HashMap hmGpbCompare = gmPriceApprovalRequestBean.fetchGpbCompareDtls(strRequestId);
    request.setAttribute("hmResult", hmGpbCompare);

    return mapping.findForward("GmPricingDetailsExportToExcel");
  }

  public ActionForward saveGpoComparisionDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParams = new HashMap();
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    hmParams = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
    GmPriceApprovalTransBean gmPriceApprovalTransBean =
        new GmPriceApprovalTransBean(getGmDataStoreVO());
    String strRequestId = GmCommonClass.parseNull((String) request.getParameter("priceRequestId"));
    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    hmParams.put("PRICEREQID", strRequestId);
    String strSavedSys = gmPriceApprovalTransBean.saveGpoComparisionDetails(hmParams);
    log.debug("strSavedSys>>>" + strSavedSys);
    gmPriceApprovalRequestForm.setSaveSys(strSavedSys);
    gpoComparison(mapping, form, request, response);
    return mapping.findForward("GmGPOComparisonMapping");
  }

  public ActionForward gpoComparison(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmResult = new HashMap();
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    String strPriceReId = GmCommonClass.parseNull((String) request.getParameter("priceRequestId"));
    strPriceReId = strPriceReId.toUpperCase().contains("PR-") ? strPriceReId : "PR-" + strPriceReId;
    gmPriceApprovalRequestForm.setPriceRequestId(strPriceReId);
    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
        new GmPriceApprovalRequestBean(getGmDataStoreVO());
    ArrayList alSalesGrp = gmPriceApprovalRequestBean.fetchSystemNameList();
    gmPriceApprovalRequestForm.setAlSystemList(alSalesGrp);
    hmResult = gmPriceApprovalRequestBean.fetchSystemList(strPriceReId);
    request.setAttribute("SYSTEMLIST", hmResult);
    request.setAttribute("GPBLIST", hmResult);
    return mapping.findForward("GmGPOComparisonMapping");
  }

  public ActionForward saveGpbReMapDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParams = new HashMap();
    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
    gmPriceApprovalRequestForm.loadSessionParameters(request);
    hmParams = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
    GmPriceApprovalTransBean gmPriceApprovalTransBean =
        new GmPriceApprovalTransBean(getGmDataStoreVO());
    String strRequestId = GmCommonClass.parseNull((String) request.getParameter("priceRequestId"));
    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    hmParams.put("PRICEREQID", strRequestId);
    gmPriceApprovalTransBean.saveGpbReMapDetails(hmParams);
    gpoComparison(mapping, form, request, response);
    return mapping.findForward("GmGPOComparisonMapping");
  }
  
  private void fchIdnNumber(ActionMapping mapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
	    instantiate(request, response);

	    Logger log = GmLogger.getInstance(this.getClass().getName());
	    GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
	    gmPriceApprovalRequestForm.loadSessionParameters(request);
	    GmPriceApprovalRequestBean gmPriceApprovalRequestBean =
	        new GmPriceApprovalRequestBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmPriceApprovalRequestForm);
	    HashMap hmCompanyInfo =
	        GmCommonClass.parseNullHashMap(GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid()));
	    String strAcctid = GmCommonClass.parseZero(gmPriceApprovalRequestForm.getAccid());
	    // Access for Generating price file - Matt
	      String strIdn = GmCommonClass.parseNull(gmPriceApprovalRequestBean.getidn(strAcctid));

	    gmPriceApprovalRequestForm.setIdnNo(strIdn);
	  }

  /**
   * addReleasePRTSystem - This Method is Used to released/unreleased System
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   */
	public ActionForward addReleasePRTSystem(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError {
		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmPriceApprovalRequestForm gmPriceApprovalRequestForm = (GmPriceApprovalRequestForm) form;
		GmPriceApprovalRequestBean gmPriceApprovalRequestBean = new GmPriceApprovalRequestBean(
				getGmDataStoreVO());
		gmPriceApprovalRequestForm.loadSessionParameters(request);
		ArrayList alSegNameList = gmPriceApprovalRequestBean
				.loadSegmentIDList();
		gmPriceApprovalRequestForm.setAlSegNameList(alSegNameList);
		HashMap hmParam = new HashMap();
		String submitFL = GmCommonClass.parseNull(gmPriceApprovalRequestForm
				.getSubmitFl());
		if (submitFL.equals("Y")) {
			String strSetId = GmCommonClass
					.parseNull(gmPriceApprovalRequestForm.getSystemId());
			String strSegId = GmCommonClass
					.parseNull(gmPriceApprovalRequestForm.getSegmentId());
			String strReleaseFL = GmCommonClass
					.parseNull(gmPriceApprovalRequestForm.getReleaseFl());
			String strUserId = GmCommonClass.parseNull((String) request
					.getSession().getAttribute("strSessUserId"));
			hmParam.put("SYSTEMID", strSetId);
			hmParam.put("SEGMENTID", strSegId);
			hmParam.put("RELEASEFL", strReleaseFL);
			hmParam.put("USERID", strUserId);
			String strResult = GmCommonClass
					.parseNull(gmPriceApprovalRequestBean
							.addReleasePRTSystem(hmParam));
		}
		return mapping.findForward("gmAddOrRemoveSystems");
	}

}
