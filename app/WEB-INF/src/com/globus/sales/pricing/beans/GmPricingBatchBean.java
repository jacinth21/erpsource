/*
 * Module: A/R Name: GmARBatchBean
 */

package com.globus.sales.pricing.beans;

import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

// import com.globus.accounts.beans.GmInvoiceBean;
// import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
// import com.globus.common.beans.GmCommonUploadBean;
// import com.globus.common.beans.GmEmailProperties;
// import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;

// import com.globus.common.util.GmPrintService;

/**
 * @author Matt Balraj
 * 
 */
public class GmPricingBatchBean extends GmBean {
  GmCommonClass gmCommonClass = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // public static final String TEMPLATE_NAME = "GmInvoicePDFEmail";


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPricingBatchBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  /**
   * updateImplemetedPrice to create the batch and add the batch details when T705_Account_Pricing
   * is updated. This method also contains JMS code which put the batch on a queue and trigger a
   * background JOB on separate thread, The call is asynchronous. Based on the constant
   * BATCH_PROCESS_SERVICE in prop file is JMS need to be activated then "Jms" or QuartzJob
   * 
   * @author Matt
   * @param HashMap
   * @return String
   */
  public String updateImplemetedPrice(HashMap hmParam) throws AppError, Exception {
    String strUserId = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strRefid = gmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strBatchType = gmCommonClass.parseNull((String) hmParam.get("BATCHTYPE"));
    String strBatchStatus = "18742"; /* Status = Pending processing */
    String strCompanyInfo = gmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));

    String strBatchProcessService =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_PROCESS_SERVICE"));
    String strBatchMDBClass =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_CONSUMER_PRICING_CLASS"));

    GmDataStoreVO tmpGmDataStoreVO = null;
    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);

    /* Create record in T9600 table */
    GmBatchBean gmBatchBean = new GmBatchBean(tmpGmDataStoreVO);
    HashMap hmBatchMap = new HashMap();
    hmBatchMap.put("BATCHID", null);
    hmBatchMap.put("BATCHTYPE", strBatchType);
    hmBatchMap.put("USERID", strUserId);
    hmBatchMap.put("BATCHSTATUS", strBatchStatus);
    String strReturnBatchId = gmBatchBean.saveBatch(hmBatchMap);

    /* Create record in T9601 batch details table */
    hmBatchMap.put("BATCHID", strReturnBatchId);
    hmBatchMap.put("REFID", strRefid);
    gmBatchBean.saveBatchDetails(hmBatchMap);
    log.debug(" update batch & batch details record --> ");


    // This is JMS Code which put the batch on a queue and trigger a
    // background JOB on separate thread, The call is asynchronous.
    if (strBatchProcessService.equals("JMS")) {
      GmQueueProducer qprod = new GmQueueProducer();
      GmMessageTransferObject tf = new GmMessageTransferObject();
      HashMap hmMessageObj = new HashMap();
      hmMessageObj.put("BATCHID", strReturnBatchId);
      hmMessageObj.put("USERID", strUserId);
      hmMessageObj.put("BATCHTYPE", strBatchType);
      hmMessageObj.put("COMPANYINFO", strCompanyInfo);

      tf.setMessageObject(hmMessageObj);
      tf.setConsumerClass(strBatchMDBClass);
      qprod.sendMessage(tf);
      log.debug("Message Sent Batch #:" + strReturnBatchId);
    }
    return strReturnBatchId;
  }

  /**
   * savePricingBatchUpdate - to save Pricing Batch Update details in T7540_Account_pricing_table.
   * This method is called by JMS processor.
   * 
   * @author Matt Balraj (PRT-288)
   * @param hash map
   * @return
   */
  public void savePricingBatchUpdate(HashMap hmParam) throws AppError, Exception {

    String strBatchID = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strBatchStatus = GmCommonClass.parseNull((String) hmParam.get("BATCHSTATUS"));
    String strBatchType = GmCommonClass.parseNull((String) hmParam.get("BATCHTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug("(savePricingBatchUpdate hmParam ==>" + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    if (!strBatchID.equals("")) {
      log.debug("(Executing pkg gm_pkg_PRT_currentPrice_txn =======>");
      gmDBManager.setPrepareString("gm_pkg_PRT_currentPrice_txn.gm_prt_currentPrice_main", 3);
      gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
      gmDBManager.setString(1, strBatchID);
      gmDBManager.setString(2, strBatchType);
      gmDBManager.execute();
      String strRefId = GmCommonClass.parseNull(gmDBManager.getString(3));
      gmDBManager.commit();

      log.debug("(Changes committed - gm_pkg_PRT_currentPrice_txn ===> refid" + strRefId);
      // Update Status to processed in T9600_Batch table
      GmBatchBean gmbatchbean = new GmBatchBean();
      strBatchStatus = "18744"; // processed
      HashMap hmBatchMap = new HashMap();
      hmBatchMap.put("BATCHID", strBatchID);
      hmBatchMap.put("BATCHTYPE", strBatchType);
      hmBatchMap.put("USERID", strUserId);
      hmBatchMap.put("BATCHSTATUS", strBatchStatus);
      String strReturnBatchId = gmbatchbean.saveBatch(hmBatchMap);
      log.debug("Batch status updated BatchId=====>" + strReturnBatchId);

      // Update Batch Log (updating with Batchid instead of party_id)
      if (!strReturnBatchId.equals("")) {
        hmBatchMap.put("REFTYPE", strBatchType);
        hmBatchMap.put("INVID", strRefId);
        hmBatchMap.put("BATCHID", strReturnBatchId);
        String strActionType = strBatchType.equals("18753") ? "1804" : "1805";
        hmBatchMap.put("ACTIONTYPE", strActionType);
        gmbatchbean.saveBatchLog(hmBatchMap);
        log.debug("Batch log updated RefId=====>" + strRefId);
      }

    }
  }
}// end of class GmPricingBatchBean
