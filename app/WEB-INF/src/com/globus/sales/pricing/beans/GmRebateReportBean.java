package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmCrossTabReport;

public class GmRebateReportBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmRebateReportBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}


	/** fetchRebateManagmentDtls - This method will be Fetch Rebate Mamagement Details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public HashMap fetchRebateManagmentDtls(HashMap hmParam) throws AppError {
		
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    HashMap hmResult = new HashMap();
	    String strRebateId="";
	    String strPartId="";
	    strRebateId = GmCommonClass.parseNull((String) hmParam.get("REBATEID"));
	    strPartId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
        gmDBManager.setPrepareString("gm_pkg_sm_rebate_report.gm_fch_rebate_management_dtls", 3);
	    gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
		gmDBManager.setString(1, strRebateId);
		gmDBManager.setString(2, strPartId);
		gmDBManager.execute();
		hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return hmResult;
		
	}

	/**
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchRebateList(HashMap hmParam)throws AppError {
		ArrayList alResult = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strRebateId ="";
	    strRebateId = GmCommonClass.parseNull((String) hmParam.get("REBATEID"));
	    gmDBManager.setPrepareString("gm_pkg_sm_rebate_report.gm_fch_rebate_parts_mapping_dtls", 2);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.setString(1, strRebateId);
	    gmDBManager.execute();
	    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	    gmDBManager.close();
	    return alResult;
	}
	
	/**fetchRebateManagmentReport - this method will load rebate management report details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
  public String fetchRebateManagmentReport(HashMap hmParam) throws AppError {
		
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
          String strRebateRptJSONString = "";
		  String strRebateAccType = "";
		  String strPartId = "";
		  String strRebateCategory = "";
		  String strRebatePartType = "";
		  String strRebateFromRate = "";
		  String strRebatToRate = "";
		  String strRebateFrmDate = "";
		  String strRebateToDate = "";
		  String strRebateType = "";
		  String strDateType ="";
		 strRebateAccType = GmCommonClass.parseNull((String) hmParam.get("GROUPTYPEID"));
		 strPartId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		 strRebatePartType = GmCommonClass.parseNull((String) hmParam.get("REBATEPARTTYPEID"));
		 strRebateCategory = GmCommonClass.parseNull((String) hmParam.get("REBATECATEGORYID"));
		 strRebateFromRate = GmCommonClass.parseNull((String) hmParam.get("REBATEFROMRATE"));
		 strRebatToRate = GmCommonClass.parseNull((String) hmParam.get("REBATETORATE"));
		 strRebateFrmDate = GmCommonClass.parseNull((String) hmParam.get("REBATEFROMDATE"));
		 strRebateToDate = GmCommonClass.parseNull((String) hmParam.get("REBATETODATE"));
		 strRebateType = GmCommonClass.parseNull((String) hmParam.get("REBATETYPEID"));
		 strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
		 String strCompanyId = getGmDataStoreVO().getCmpid();
	     gmDBManager.setPrepareString("gm_pkg_sm_rebate_report.gm_fch_rebate_management_report", 12);
		 gmDBManager.registerOutParameter(12,OracleTypes.CLOB);
			gmDBManager.setString(1, strRebateAccType);
			gmDBManager.setString(2, strPartId);
			gmDBManager.setString(3, strRebateType);
			gmDBManager.setString(4, strRebateCategory);
			gmDBManager.setString(5, strRebateFromRate);
			gmDBManager.setString(6, strRebatToRate);
			gmDBManager.setString(7, strRebatePartType);
			gmDBManager.setString(8, strDateType);
			gmDBManager.setString(9, strRebateFrmDate);
			gmDBManager.setString(10, strRebateToDate);
			gmDBManager.setString(11, strCompanyId);
			gmDBManager.execute();
            strRebateRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(12));
			gmDBManager.close();
       
    return strRebateRptJSONString;
	}
	
	/**loadRebatePartReportsDtls - this method will load rebate part list details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String loadRebatePartReportsDtls(HashMap hmParam) throws AppError {

		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  String strRebateRptJSONString = "";
		    String strRebateAccType = "";
			String strPartId = "";
			String strDateType = "";
			String strFrmDate = "";
			String strToDate = "";
			String strPartNumber = "";
		    strRebateAccType = GmCommonClass.parseNull((String) hmParam.get("GROUPTYPEID"));
			strPartId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
			strFrmDate = GmCommonClass.parseNull((String) hmParam.get("REBATEFROMDATE"));
			strToDate = GmCommonClass.parseNull((String) hmParam.get("REBATETODATE"));
			strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
			strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
			String strCompanyId = getGmDataStoreVO().getCmpid();
	        gmDBManager.setPrepareString("gm_pkg_sm_rebate_report.gm_fch_rebate_part_reports_dtls", 8);
		    gmDBManager.registerOutParameter(8,OracleTypes.CLOB);
			gmDBManager.setString(1, strRebateAccType);
			gmDBManager.setString(2, strPartId);
			gmDBManager.setString(3, strDateType);
			gmDBManager.setString(4, strFrmDate);
			gmDBManager.setString(5, strToDate);
			gmDBManager.setString(6, strPartNumber);
			gmDBManager.setString(7, strCompanyId);
			gmDBManager.execute();
			strRebateRptJSONString  = GmCommonClass.parseNull(gmDBManager.getString(8));
			gmDBManager.close();
		return strRebateRptJSONString;
		
	}
	
	
	
	
	/** fetchRebateExpiryDetails - This method used to fetch rebate expiry details
	 * @param pRange
	 * @param strCompanyId
	 * @return
	 */
	public ArrayList fetchRebateExpiryDetails(String strRange, String strCompanyId) throws AppError
	{
		
		 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 ArrayList alList = new ArrayList();
		 
		 gmDBManager.setPrepareString("gm_pkg_sm_rebate_report.gm_fch_rebate_expiry_details", 3);
		 gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		 gmDBManager.setString(1, strRange);
		 gmDBManager.setString(2, strCompanyId);
		 gmDBManager.execute();
			
		 alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3)); 
		 gmDBManager.close();
		 
		 return alList;
		
	}
	
	
	}








