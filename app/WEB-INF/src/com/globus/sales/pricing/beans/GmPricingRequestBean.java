package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPricingRequestBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

  // this will be removed all place changed with Data Store VO constructor

  public GmPricingRequestBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPricingRequestBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
	 * fetchNextRequestId - This method will be fetch next request id
	 * 
	 * @param
	 * @return String
	 * @exception AppError
	 */
	public String fetchNextRequestId() throws AppError {

		String strRequestId = "";
		GmDBManager gmDBManager = new GmDBManager();

		try {

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append("select  gm_pkg_sm_pricing_request.get_next_price_request_id('0");
			sbQuery.append("') PRRQID FROM DUAL ");

			gmDBManager.setPrepareString(sbQuery.toString());
			HashMap hmReturn = gmDBManager.querySingleRecord();
			strRequestId = (String) hmReturn.get("PRRQID");
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return strRequestId;
	}

	/**
	 * fetchRequestStatusId - This method will be fetch request status id
	 * 
	 * @param
	 * @return String
	 * @exception AppError
	 */
	public String fetchRequestStatusId(String requestId) throws AppError {

		String requestStatusId = "";
		GmDBManager gmDBManager = new GmDBManager();

		try {

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append("select  gm_pkg_sm_pricing_request.get_request_status_id('" + requestId);
			sbQuery.append("') RQSTATUSID FROM DUAL ");

			gmDBManager.setPrepareString(sbQuery.toString());
			HashMap hmReturn = gmDBManager.querySingleRecord();
			requestStatusId = (String) hmReturn.get("RQSTATUSID");
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return requestStatusId;
	}

	/**
	 * loadAccountList To load the account list for given user
	 * 
	 * @author rmayya
	 * @date Dec 3, 2009
	 * @param
	 */
	public ArrayList loadAccountList(String strCondition) throws AppError {
		/*GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_accounts_list", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, userId);
		gmDBManager.execute();

		ArrayList alAccountList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alAccountList;*/
		ArrayList alReturn = new ArrayList();
		 
		 
		GmDBManager gmDBManager = new GmDBManager();
		StringBuffer sbQuery = new StringBuffer();
		
		sbQuery.append(" SELECT	DISTINCT AC_ID ID,AC_NAME ACNAME, AC_NAME||'-'||AC_ID NM, AD_ID ADID ");
		sbQuery.append(" , VP_ID VPID, D_ID DID, REP_ID repid, REP_NAME rname ");
		sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700, T704_ACCOUNT T704  ");
		 
		// Filter Condition to fetch record based on access code
		if (!strCondition.toString().equals("")) {
			sbQuery.append(" WHERE ");
			sbQuery.append(strCondition);
			sbQuery.append(" and ac_id is not null  ");
		}
		else{
			sbQuery.append("where ac_id is not null  ");
		}
		sbQuery.append(" AND v700.ac_id = c704_account_id ");
		sbQuery.append(" AND c704_active_fl = 'Y' AND (t704.C901_EXT_COUNTRY_ID is null ");
		sbQuery.append(" OR t704.C901_EXT_COUNTRY_ID  in (select country_id from v901_country_codes))");
		sbQuery.append(" ORDER BY AC_NAME ");
		log.debug(" Query to get accounts is " + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug(" alReturn inside GmCommonBean is .................. " + sbQuery.toString());
	 
	return alReturn;
	}
	/**
	 * loadAccountList To load the systems list for given acct
	 * 
	 * @author Himanshu
	 * @date Jul 9, 2010
	 * @param
	 */
	public ArrayList loadSystemsList() throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_systems_list", 1);

		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();

		ArrayList alSystemList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alSystemList;
	}

	/**
	 * loadAccountDetails To load the account details
	 * 
	 * @author rmayya
	 * @date Dec 7, 2009
	 * @param
	 */
	public HashMap loadAccountDetails(String strAccountId,String strGpoID, String strStatus) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_accounts_details", 4);

		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strAccountId);
		gmDBManager.setString(2, strGpoID);
		gmDBManager.setString(3, strStatus);
		gmDBManager.execute();

		HashMap alAccountDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		return alAccountDetails;
	}

	/**
	 * loadRequestMasterDetails To Fetch the request master details
	 * 
	 * @author rmayya
	 * @date Dec 7, 2009
	 * @param
	 */
	public HashMap loadRequestMasterDetails(String strRequestId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_request_master_details", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strRequestId);
		gmDBManager.execute();

		HashMap alRequestDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alRequestDetails;
	}

	/**
	 * loadPricingByGroupGrid To load the pricing by group grid
	 * 
	 * @author rmayya
	 * @date Dec 15, 2009
	 * @param
	 */

	public HashMap loadPricingByGroupGrid(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		log.debug(" GmCommonClass.parseNull((String) hmParam.get(\"STRREQUESTID\"))-->"+
				GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID")));
		log.debug(" GmCommonClass.parseNull((String) hmParam.get(\"STRACCOUNTID\"))-->"+
				GmCommonClass.parseNull((String) hmParam.get("STRACCOUNTID")));
		log.debug(" GmCommonClass.parseNull((String) hmParam.get(\"EFFECTIVEDATE\"))-->"+
				GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEDATE")));
		log.debug(" GmCommonClass.parseNull((String) hmParam.get(\"INPUTSTR\"))-->"+
				GmCommonClass.parseNull((String) hmParam.get("INPUTSTR")));
		log.debug(" GmCommonClass.parseNull((String) hmParam.get(\"FCHALLSYSFL\"))-->"+
				GmCommonClass.parseNull((String) hmParam.get("FCHALLSYSFL")));
		log.debug(" GmCommonClass.parseNull((String) hmParam.get(\"GROUPID\"))-->"+
				GmCommonClass.parseNull((String) hmParam.get("GROUPID")));
		
		log.debug(" GmCommonClass.parseNull((String) hmParam.get(\"AlSystemList\"))-->"+
				GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALSYSTEMLIST")));
		
		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_pricing_request_details", 11);
		
		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("STRACCOUNTID")));
		gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEDATE")));
		gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("INPUTSTR")));
		gmDBManager.setString(5, "Y");//GmCommonClass.parseNull((String) hmParam.get("FCHALLSYSFL"))
		gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("GROUPID")));
		gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("PARTFLAG")));
		gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC")));
		gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);

		gmDBManager.execute();

		ArrayList alSystemResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(9));
		ArrayList alGroupResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10));
		ArrayList alPartResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(11));
		gmDBManager.close();
		HashMap hmResult = new HashMap();
		log.debug("alSystemResult-->"+alSystemResult);
		log.debug("alGroupResult-->"+alGroupResult);
		log.debug("alPartResult-->"+alPartResult);
		hmResult.put("alSystemResult", alSystemResult);
		hmResult.put("alGroupResult", alGroupResult);
		hmResult.put("alPartResult", alPartResult);

		return hmResult;
	}
	
	/**
	 * loadRequestId method to load the pricing by Request ID.
	 * 
	 * @author Velu
	 * @date Dec 15, 2011
	 * @param String
	 */
	
		public String loadRequestId(String strAccID, String GpoID, String StrStatus) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("strAccID::"+strAccID + " str Group id "+GpoID);
	gmDBManager.setFunctionString("GET_PRICING_REQUEST_ID", 3);
		
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, GmCommonClass.parseNull(strAccID));
		gmDBManager.setString(3, GmCommonClass.parseNull(GpoID));
		gmDBManager.setString(4, GmCommonClass.parseNull(StrStatus));

		
		gmDBManager.execute();

		String strRequestIdFromDB = GmCommonClass.parseNull((String)gmDBManager.getString(1));
		log.debug("strRequestIdFromDB::"+strRequestIdFromDB);
		gmDBManager.close();
		return strRequestIdFromDB;
	}
		
		/**
		 * loadRequestId method to load the pricing by Request ID.
		 * 
		 * @author Velu
		 * @date Mar 29, 2012
		 * @param String
		 */
		
			public String loadAccountGpoID(String strAccID) throws AppError {
			GmDBManager gmDBManager = new GmDBManager();
			log.debug("strAccID::"+strAccID);
			gmDBManager.setFunctionString("get_account_gpo_id", 1);

			gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
			gmDBManager.setString(2, GmCommonClass.parseNull(strAccID));
			
			gmDBManager.execute();

			String strGpoIdFromDB = GmCommonClass.parseNull((String)gmDBManager.getString(1));
			log.debug("strGPOIDFromDB::"+strGpoIdFromDB);
			gmDBManager.close();
			return strGpoIdFromDB;
		}
			
	/**
	 * loadPricingByAnalysisGrid to load the pricing by analysis grid
	 * 
	 * @author rmayya
	 * @date Dec 15, 2009
	 * @param
	 */
	public HashMap loadPricingByAnalysisGrid(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("Load loadPricingByAnalysisGrid::");
		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_constrcut_pr_details", 8);

		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("STRACCOUNTID")));
		gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEDATE")));
		gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC")));
		gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("USERID")));
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);

		gmDBManager.execute();
		
		ArrayList alSystemResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		ArrayList alConstructResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
		ArrayList alPartResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
		gmDBManager.close();
		HashMap hmResult = new HashMap();
		log.debug("alSystemResult::"+ alSystemResult);
		log.debug("alConstructResult::"+ alConstructResult);
		log.debug("alPartResult::"+ alPartResult);
		
		hmResult.put("alSystemResult", alSystemResult);
		hmResult.put("alConstructResult", alConstructResult);
		hmResult.put("alPartResult", alPartResult);
		return hmResult;
	}
	
	/**
	 * loadPricingByAnalysisViewGrid to load the pricing by analysis grid
	 * 
	 * @author rmayya
	 * @date Dec 15, 2009
	 * @param
	 */
	public HashMap loadPricingByAnalysisViewGrid(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_request_analysis_view", 5);
		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("STRACCOUNTID")));
		gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC")));
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.execute();
		ArrayList alAnalysisResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		ArrayList alConstResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();
		HashMap hmResult = new HashMap();
		log.debug("alAnalysisResult::"+alAnalysisResult.size());
		log.debug("alConstResult::"+alConstResult.size());
		hmResult.put("alAnalysisResult", alAnalysisResult);
		hmResult.put("alConstResult", alConstResult);
		return hmResult;
	}
	/**
	 * loadWorkflowGrid to load the work flow grid
	 * 
	 * @author rmayya
	 * @date Dec 15, 2009
	 * @param
	 */
	public HashMap loadWorkflowGrid(String requestId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		
		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_workflow_details", 3);

		gmDBManager.setString(1, requestId);	

		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

		gmDBManager.execute();

		ArrayList alGroupResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		ArrayList alPartResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		HashMap hmResult = new HashMap();
		hmResult.put("alGroupResult", alGroupResult);
		hmResult.put("alPartResult", alPartResult);
		return hmResult;
	}
	/**
	 * loadPricingByActivityGrid to load the pricing by analysis grid
	 * 
	 * @author rmayya
	 * @date Dec 15, 2009
	 * @param
	 */
	public HashMap loadPricingByActivityGrid(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_activity_details", 3);

		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("STRSTATUS")));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();		
		ArrayList alActivityResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		HashMap hmResult = new HashMap();		
		hmResult.put("alActivityResult", alActivityResult);
		return hmResult;
	}
	
	/**
	 * saveDeniedPricingRequest saves the pricing denied request details
	 * 
	 * @author velu
	 * @date Nov 3, 2011
	 * @param HashMap
	 */
	 
	public void saveDeniedPricingRequest(HashMap hmParam) throws AppError {
		
		GmDBManager gmDBManager = new GmDBManager();
		GmCommonBean gmCommonBean = new GmCommonBean();
		
				
		String strRequestId = GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID"));
		String strAccountId = GmCommonClass.parseNull((String) hmParam.get("STRACCOUNTID"));
		String effectiveDate = GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEDATE"));
		String strInput = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
		String strInputAnswer = GmCommonClass.parseNull((String) hmParam.get("INPUTANSWER"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("HSTATUSID"));
		
		String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
		String strCancelReason = GmCommonClass.parseNull((String) hmParam.get("CANCELREASON"));
		String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
		String strCancelType = GmCommonClass.parseNull((String) hmParam.get("CANCELTYPE"));
		String strDenyInput = GmCommonClass.parseNull((String) hmParam.get("HDENIEDINPUTSTR"));
		String strRequestType = GmCommonClass.parseNull((String) hmParam.get("STRACCTYPE"));
		String strGpoID = GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC"));
		String strApprLvl = GmCommonClass.parseNull((String) hmParam.get("APPRLVL"));

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_save_denied_pricing_request", 18);
		
		gmDBManager.setString(1, strRequestId);
		gmDBManager.setString(2, strAccountId);
		gmDBManager.setString(3, effectiveDate);
		gmDBManager.setString(4, strInput);
		gmDBManager.setString(5, strInputAnswer);
		gmDBManager.setString(6, strUserId);
		gmDBManager.setString(7, strStatus);
		gmDBManager.setString(8, strTxnId);
		gmDBManager.setString(9, strCancelReason);
		gmDBManager.setString(10, strComments);
		gmDBManager.setString(11, strCancelType);
		gmDBManager.setString(12, strRequestType);
		gmDBManager.setString(13, strGpoID);
		gmDBManager.setString(14, strDenyInput);
		gmDBManager.setString(15, strApprLvl);
		gmDBManager.setString(16, (String)hmParam.get("STRPUBLOVRRIDE"));
		gmDBManager.registerOutParameter(17, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(18, OracleTypes.VARCHAR);

		gmDBManager.execute();
		//String strRequestIdFromDB = gmDBManager.getString(13);
		//String strStatusIdFromDB = gmDBManager.getString(14);
		gmDBManager.commit();
		
	}
	/**
	 * savePricingRequest saves the pricing request details
	 * 
	 * @author rmayya
	 * @date Dec 7, 2009
	 * @param
	 */
	public HashMap savePricingRequest(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager();
		GmCommonBean gmCommonBean = new GmCommonBean();
		HashMap hmReturn = new HashMap();
		String strRequestId = GmCommonClass.parseNull((String) hmParam.get("STRREQUESTID"));
		String strAccountId = GmCommonClass.parseNull((String) hmParam.get("STRACCOUNTID"));
		String effectiveDate = GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEDATE"));
		String strInput = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
		String strInputAnswer = GmCommonClass.parseNull((String) hmParam.get("INPUTANSWER"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("HSTATUSID"));
		String strGpoID = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
		String strApprLvl = GmCommonClass.parseNull((String) hmParam.get("APPRLVL"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strRemoveSysIds = GmCommonClass.parseNull((String) hmParam.get("STRREMOVESYSIDS"));
		
		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_save_pricing_request", 15);

		
		gmDBManager.setString(1, strRequestId);
		gmDBManager.setString(2, strAccountId);
		gmDBManager.setString(3, effectiveDate);
		gmDBManager.setString(4, strInput);
		gmDBManager.setString(5, strInputAnswer);
		gmDBManager.setString(6, strUserId);
		gmDBManager.setString(7, strStatus);
		gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("STRACCTYPE")));
		gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC")));
		gmDBManager.setString(10, strApprLvl);
		gmDBManager.setString(11, strRemoveSysIds);
		gmDBManager.setString(12, (String)hmParam.get("STRPUBLOVRRIDE"));
		gmDBManager.registerOutParameter(13, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(14, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(15, java.sql.Types.VARCHAR);
		gmDBManager.execute();
		String strRequestIdFromDB = gmDBManager.getString(13);
		String strStatusIdFromDB = gmDBManager.getString(14);
		String strErrorMsgFromDB = gmDBManager.getString(15);

		// general one
		/*if (!strLogReason.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strRequestIdFromDB, strLogReason, strUserId, "1252");
		}
		*/
		gmDBManager.commit();
		
		hmReturn.put("STRREQUESTID", strRequestIdFromDB);
		hmReturn.put("HSTATUSID", strStatusIdFromDB);
		hmReturn.put("ERRORMSG", strErrorMsgFromDB);
		return hmReturn;
	}
	public HashMap fetchQuestionAnswer(String strRequestId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_question_answer", 5); 
		
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		
		gmDBManager.setString(1, strRequestId);
		
		gmDBManager.execute();
		
		ArrayList alQuestion = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		ArrayList alAnswer = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		ArrayList alAnswerGroup = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		ArrayList alQuestionAnswer = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();
		HashMap hmResult = new HashMap();
		hmResult.put("QUESTION", alQuestion);
		hmResult.put("ANSWER", alAnswer);
		hmResult.put("ANSWERGROUP", alAnswerGroup);
		hmResult.put("QUESTIONANSWER", alQuestionAnswer);
		
		return hmResult;
	}
	/**
	 * setAdVpFlg which checks whether the user is ad/vp if yes returns 'Y'
	 * 
	 * @author 
	 * @date 
	 * @param
	 */
	public String setAdVpFlg(String accId, String userId) throws AppError {
		String strAdVpFlag = "";
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_ad_vp_flg", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.setString(1, accId);
		gmDBManager.setString(2, userId);

		
		
		gmDBManager.execute();
		strAdVpFlag =  gmDBManager.getString(3);
		gmDBManager.close();
		
		return strAdVpFlag;
	}
	
	/**
	 * GPO Account details to be set the variable
	 * 
	 * @author 
	 * @date 
	 * @param
	 */
	
	public HashMap loadGroupAccountDetailsMap(String strGPOIds,String strAccessFilter) throws AppError{
		String strAccIDS= "";  
		String strRegName = "";
		String strRepName = "";
		String strTerName = "";
		String strDName = "";
		String strAdName = "";
		String strVpName = "";
		String strTemp = "";
		String strAccName = "";
		HashMap hmTemp = new HashMap();
		HashMap hmReturn = new HashMap();
		ArrayList alReturn   = loadGroupAccountDetails(strGPOIds,strAccessFilter);
		log.debug("alReturn Size "+alReturn.size());
		int intAdNameCnt = 0;int intTerNameCnt = 0;
		int intRegNameCnt = 0;int intRepNameCnt = 0;
		int intVpNameCnt = 0;int intDNameCnt = 0;
		if(alReturn != null && alReturn.size() != 0){
			for(Iterator it=alReturn.iterator(); it.hasNext();){
				
				hmTemp = (HashMap)it.next();
				strTemp = GmCommonClass.parseNull((String)hmTemp.get("STRACCOUNTNAME"));
				if (!strAccName.contains(strTemp))
					strAccName +=strTemp +",";
				strTemp = GmCommonClass.parseNull((String)hmTemp.get("STRREGNAME"));
				if (!strRegName.contains(strTemp)){
					intRegNameCnt = intRegNameCnt +1;
					if(intRegNameCnt <= 2)	strRegName +=strTemp +", ";
					if(intRegNameCnt == 2)	strRegName = strRegName+"..."+"  ";	
				}
				strTemp = GmCommonClass.parseNull((String)hmTemp.get("STRREPNAME"));
				if (!strRepName.contains(strTemp)){
					intRepNameCnt = intRepNameCnt +1;
					if(intRepNameCnt <= 2)	strRepName +=strTemp +", ";
					if(intRepNameCnt == 2)	strRepName = strRepName+"..."+"  ";
				}
				strTemp = GmCommonClass.parseNull((String)hmTemp.get("STRTERNAME"));
				if (!strTerName.contains(strTemp)){
					intTerNameCnt = intTerNameCnt +1;
					if(intTerNameCnt <= 2)	strTerName +=strTemp +", ";
					if(intTerNameCnt == 2)	strTerName = strTerName+"..."+"  ";
				}
				strTemp = GmCommonClass.parseNull((String)hmTemp.get("STRADNAME"));
				if (!strAdName.contains(strTemp)){
					intAdNameCnt = intAdNameCnt +1;
					if(intAdNameCnt <= 2)	strAdName +=strTemp +", ";
					if(intAdNameCnt == 2)	strAdName = strAdName+"..."+"  ";
				}
				strTemp = GmCommonClass.parseNull((String)hmTemp.get("STRVPNAME"));
				if (!strVpName.contains(strTemp)){
					intVpNameCnt = intVpNameCnt +1;
					if(intVpNameCnt <= 2)	strVpName +=strTemp +", ";
					if(intVpNameCnt == 2)	strVpName = strVpName+"..."+"  ";
				}
				strTemp = GmCommonClass.parseNull((String)hmTemp.get("STRDNAME"));
				if (!strDName.contains(strTemp)){
					intDNameCnt = intDNameCnt +1;
					if(intDNameCnt <= 2)	strDName+= strTemp +", ";
					if(intDNameCnt == 2)	strDName = strDName+"..."+"  ";
				}
			}
		
		hmReturn.put("STRREGNAME", strRegName.substring(0, strRegName.length()-2));
		hmReturn.put("STRREPNAME", strRepName.substring(0,strRepName.length()-2));
		hmReturn.put("STRTERNAME", strTerName.substring(0, strTerName.length()-2));
		hmReturn.put("STRADNAME", strAdName.substring(0, strAdName.length()-2));
		hmReturn.put("STRVPNAME", strVpName.substring(0,strVpName.length()-2));
		hmReturn.put("STRDNAME", strDName.substring(0, strDName.length()-2));
		}
		
		return hmReturn;
		
	}
	
	/**
	 * GPO Account details to be loaded
	 * 
	 * @author 
	 * @date 
	 * @param
	 */

	public ArrayList loadGroupAccountDetails (String strGPOIds,String strCondition) throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" SELECT ac_id straccid,t740.c101_party_id straccountid, ac_name straccountname, ad_name stradname, region_name strregname, d_name strdname, ac_id id ");
		sbQuery.append(" , ac_name || '-' || ac_id  nm, ter_name strtername, rep_name strrepname , gp_name strgpname, vp_name strvpname, gm_pkg_sm_pricing_request.get_flg_req_id (v700.ac_id) flgreqid");

		sbQuery.append("  , GET_PRICING_REQUEST_ID (v700.ac_id,'"+strGPOIds+"') strRequestId, gm_pkg_sm_pricing_request.GET_ACCOUNT_CONTRACT (v700.ac_id) contractfl");
		sbQuery.append("   FROM v700_territory_mapping_detail v700, T740_GPO_ACCOUNT_MAPPING t740 ");
		
		// Filter Condition to fetch record based on access code
		if (!strCondition.equals("")) {
			sbQuery.append(" WHERE ");
			sbQuery.append(strCondition);
			sbQuery.append(" and ac_id is not null  ");
		}
		else{
			sbQuery.append("where ac_id is not null  ");
		}
		sbQuery.append(" AND t740.c704_account_id = v700.ac_id ");
		sbQuery.append(" AND t740.c101_party_id   = '"+strGPOIds+"'");
		sbQuery.append(" ORDER BY AC_NAME ");

		log.debug(" Query to get loadGroupAccountDetails " + sbQuery.toString());
		ArrayList alGroupAccDetails = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alGroupAccDetails;

	}
	
	/**
	 * Get the Party name 
	 * 
	 * @author 
	 * @date 
	 * @param - Gpo id
	 */
	
	public String getPartyNm(String strGpo) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		log.debug(" str Group id "+strGpo);
		gmDBManager.setFunctionString("get_party_name", 1);
		
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, GmCommonClass.parseNull(strGpo));

		gmDBManager.execute();

		String strPartyName = GmCommonClass.parseNull((String)gmDBManager.getString(1));
		log.debug("getPartyNm:: "+strPartyName);
		gmDBManager.close();
		return strPartyName;
	}

	/**
	 * loadAccountList To load the systems list for given acct
	 * 
	 * @author Himanshu
	 * @date Jul 9, 2010
	 * @param
	 */
	public ArrayList loadPricingRequestRules(String strRequestID,String strRuleID) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_pricing_rule_details", 3);

		gmDBManager.setString(1, GmCommonClass.parseNull(strRequestID));
		gmDBManager.setString(2, GmCommonClass.parseNull(strRuleID));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();

		ArrayList alRulesList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return alRulesList;
	}

	/**
	 * loadGPBAccountDetails To load the Group Account details
	 * 
	 * @author karthik
	 * @param strpartyID
	 */
	public ArrayList loadGPBAccountDetails(String strpartyID) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_fch_gpb_accounts_details", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strpartyID);
		gmDBManager.execute();
		ArrayList alccDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		log.debug("alccDetails in loadGPBAccountDetails"+alccDetails);
		gmDBManager.close();
		return alccDetails;
	}

}
