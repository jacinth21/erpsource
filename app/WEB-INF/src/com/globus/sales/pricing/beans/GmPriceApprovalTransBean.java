package com.globus.sales.pricing.beans;

import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPriceApprovalTransBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmPriceApprovalTransBean() {
	   super(GmCommonClass.getDefaultGmDataStoreVO());
	 }
	
	 /**
	  * Constructor will populate company info.
	  * 
	  * @param gmDataStore
	  */
	 public GmPriceApprovalTransBean(GmDataStoreVO gmDataStore) {
	   super(gmDataStore);
	 }  

  /**
   * updatePriceRequestDetails - This method for Modifying the Price details on the PR-XXXX
   * 
   * @param HashMap hmParams
   * @return String
   * @exception AppError , Exception
   * @author hreddy
   */
  public String updatePriceRequestDetails(HashMap hmParams) throws AppError, Exception {
    String strOutPriceReqId = "";
    String strPriceReqId = GmCommonClass.parseNull((String) hmParams.get("PRICEREQUESTID"));
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    log.debug("strInputString=======>" + strInputString);
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strImpleString = GmCommonClass.parseNull((String) hmParams.get("IMPLEMENTSTRING"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParams.get("companyInfo"));
    GmPricingBatchBean gmPricingBatchBean = new GmPricingBatchBean(getGmDataStoreVO());

    HashMap hmBatchParams = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_txn.gm_upd_price_req_details", 4);
    gmDBManager.setString(1, strPriceReqId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strOutPriceReqId = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();
    // Below code added for implementing the Price if we click on the Implement Check Box in T7504
    // table
    if (!strImpleString.equals("")) {
      hmBatchParams.put("USERID", strUserId);
      hmBatchParams.put("REFID", strPriceReqId);
      hmBatchParams.put("BATCHTYPE", "18754"); // Price Update From Pricing Tool
      hmBatchParams.put("COMPANYINFO", strCompanyInfo); // Price Update From Pricing Tool
      gmPricingBatchBean.updateImplemetedPrice(hmBatchParams);
    }

    gmDBManager.close();
    return strOutPriceReqId;
  }

  /**
   * updateSystemDetails - This method is written for removing the selected System from PR-XXXX
   * Details *
   * 
   * @param HashMap hmParams
   * @return String
   * @exception AppError , Exception
   * @author hreddy
   */
  public String updateSystemDetails(HashMap hmParams) throws AppError, Exception {
    String strOutPriceReqId = "";
    String strPriceReqId = GmCommonClass.parseNull((String) hmParams.get("PRICEREQUESTID"));
    String strSystemId = GmCommonClass.parseNull((String) hmParams.get("SYSTEMID"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_txn.gm_upd_system_details", 4);
    gmDBManager.setString(1, strPriceReqId);
    gmDBManager.setString(2, strSystemId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strOutPriceReqId = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();
    gmDBManager.close();
    return strOutPriceReqId;
  }



  /**
   * updateSystemDetails - This method is written for removing the selected System from PR-XXXX
   * Details *
   * 
   * @param HashMap hmParams
   * @return String
   * @exception AppError , Exception
   * @author Dsandeep
   */
  public String saveGpoComparisionDetails(HashMap hmParams) throws AppError, Exception {

    log.debug("inside saveGpoComparisionDetails>>>>>>>" + hmParams);
    String strSavedSys = "";
    String strPriceReqId = GmCommonClass.parseNull((String) hmParams.get("PRICEREQID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_txn.gm_sav_upd_add_systems", 4);
    gmDBManager.setString(1, strPriceReqId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
    gmDBManager.execute();
    strSavedSys = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();
    gmDBManager.close();

    return strSavedSys;

  }

  /**
   * saveGpbReMapDetails - This method is written for removing the selected System from PR-XXXX
   * Details *
   * 
   * @param HashMap hmParams
   * @return String
   * @exception AppError , Exception
   * @author Jreddy
   */
  public void saveGpbReMapDetails(HashMap hmParams) throws AppError, Exception {

    log.debug("inside saveGpoComparisionDetails>>>>>>>" + hmParams);
    String strPriceReqId = GmCommonClass.parseNull((String) hmParams.get("PRICEREQID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_txn.gm_sav_upd_gpb_dtls", 3);
    gmDBManager.setString(1, strPriceReqId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
    gmDBManager.close();


  }



}
