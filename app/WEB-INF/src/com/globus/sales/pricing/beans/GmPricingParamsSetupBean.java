package com.globus.sales.pricing.beans;


import java.sql.ResultSet; 
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPricingParamsSetupBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());


  public GmPricingParamsSetupBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPricingParamsSetupBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  /**
   * savePricingParam - This method will save the Pricing Parameter details *
   * 
   * @param HashMap hmParam
   * @exception AppError
   */

  public void savePricingParam(HashMap hmParam) throws AppError, Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());

    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strPriceAdjInStr = GmCommonClass.parseNull((String) hmParam.get("HPRICEADJINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAccAffliStr = GmCommonClass.parseNull((String) hmParam.get("HACCATTINPUTSTR"));
    String strAction = "";
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GROUPPRICEBOOK"));
    gmDBManager.setPrepareString("gm_pkg_sm_pricparam_setup.gm_save_pricing_params", 5);
    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strPartyId);
    gmDBManager.setString(3, strPriceAdjInStr);
    gmDBManager.setString(4, strGpoId);
    gmDBManager.setString(5, strUserId);
    gmDBManager.execute();

    if (!strLogReason.equals("") && !strAccId.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strAccId, strLogReason, strUserId, "10304529332");
    } else if (!strLogReason.equals("") && !strPartyId.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartyId, strLogReason, strUserId, "10304529332");
    }

    if (strAccAffliStr.length() > 0) {
//      gmCustSalesSetupBean.saveAccountAttribute(gmDBManager, strAccId, strUserId, strAccAffliStr,
//          strAction);
      // to sync the pricing screen parameters
//      gmCustSalesSetupBean.saveSyncAccountAttr(gmDBManager, strAccId, "PRICING_SETUP_PARAM",
//          strUserId);
    	saveAccountAfflnDtl(gmDBManager,strAccId,strAccAffliStr,strUserId);
    	
    }
    gmDBManager.commit();
  }
  
  
  /**
   * saveAccountAfflnDtl - This method will save the Account Affiliation details *
   * 
   * @param gmDBManager
   * @param strAccId
   * @param strAccAffliStr
   * @param strUserId
   * @exception AppError
   */
  public void saveAccountAfflnDtl(GmDBManager gmDBManager,String strAccId,String strAccAffliStr,String strUserId) throws AppError, Exception {
	    
	    gmDBManager.setPrepareString("gm_pkg_sm_pricparam_setup.gm_sav_acct_affln_dtl", 3);
	    gmDBManager.setString(1, strAccId);
	    gmDBManager.setString(2, strAccAffliStr);
	    gmDBManager.setString(3, strUserId);
	    gmDBManager.execute();
}

  /**
   * fetchPricingParams - This method will Load the Pricing Parameter details *
   * 
   * @param HashMap hmParams
   * @return alResult
   * @exception AppError
   */
  public ArrayList fetchPricingParams(HashMap hmParams) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    String strAccId = GmCommonClass.parseNull((String) hmParams.get("ACCID"));
    String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricparam_setup.gm_fch_pricing_params", 3);
    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strPartyId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList((gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3))));
    gmDBManager.close();
    return alResult;
  }


  /**
   * loadPricingAccount - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPricingAccount() throws AppError {
    HashMap hmReturn = new HashMap();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmPartyBean gmParty = new GmPartyBean(getGmDataStoreVO());
    ArrayList alGpoList = new ArrayList();
    ArrayList alGprAff = new ArrayList();
    ArrayList alHlcAff = new ArrayList();
    hmReturn.put("GPOLIST", alGpoList);
    // Account Affiliation in Account Setup
    hmReturn.put("ALGPRAFF", alGprAff);
    hmReturn.put("ALHLCAFF", alHlcAff);
    return hmReturn;
  } // End of loadAccount


  /**
   * loadPricingEditAccount - This method is to load the details of the account
   * 
   * @param String strId
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadPricingEditAccount(String strId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    GmCustomerBean gmCust = new GmCustomerBean();
    String strAcctPricing = GmCommonClass.parseNull(GmCommonClass.getString("LOAD_LIST_PRICE"));
    String strUploadAcctPrice =
        GmCommonClass.parseNull(GmCommonClass.getString("UPLOAD_ACCT_PRICE"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT t704.C704_ACCOUNT_ID ID,  C704_ACCOUNT_NM ANAME, C703_SALES_REP_ID REPID, C101_PARTY_ID PARTYID, ");
    sbQuery.append(" GET_DISTRIBUTOR_ID(C703_SALES_REP_ID) DISTID, ");
  //PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen
    sbQuery.append(" get_party_name(t704d.C101_RPC) rpcName,get_party_name(t704d.C101_HEALTH_SYSTEM) hlthName, ");
    sbQuery.append(" t704d.c101_rpc rpc,t704d.c101_health_system hlthId, ");
    sbQuery.append(" C704_ACCOUNT_SH_NAME SNAME, C704_CONTACT_PERSON CPERSON, C704_PHONE PHONE, ");
    sbQuery.append(" C704_FAX FAX, C704_PRICING_CATEGORY PCAT, C704_PAYMENT_TERMS PTERM, ");
    sbQuery.append(" C704_BILL_NAME BNAME, C704_BILL_ADD1 BADD1, C704_BILL_ADD2 BADD2, ");
    sbQuery.append(" C704_BILL_CITY BCITY, C704_BILL_STATE BSTATE, C704_BILL_COUNTRY BCOUNTRY, ");
    sbQuery.append(" C704_BILL_ZIP_CODE BZIP, C704_SHIP_NAME CNAME, C704_SHIP_ADD1 SADD1, ");
    sbQuery
        .append(" C704_SHIP_ADD2 SADD2, C704_SHIP_CITY SCITY, C704_SHIP_STATE SSTATE, C704_SHIP_COUNTRY SCOUNTRY, ");
    sbQuery
        .append(" C704_SHIP_ZIP_CODE SZIP, get_party_name(get_account_gpo_id (t704.C704_ACCOUNT_ID)) GROUPPRICEBOOKNAME, get_account_gpo_id(t704.C704_ACCOUNT_ID) GPOID, C901_CARRIER CARRIER, ");
    sbQuery
        .append(" C704_ACTIVE_FL AFLAG, c901_account_type ACCTYPE, C704_INCEPTION_DATE INCDATE, C704_COMMENTS COMMENTS, ");
    sbQuery.append(" C704_CREDIT_RATING CREDITRATING,C704_RATING_HISTORY_FL RATING_FL, ");

    if ((strUploadAcctPrice.equalsIgnoreCase("YES")) && (!strAcctPricing.equalsIgnoreCase("YES"))) {
      sbQuery.append(" GET_ACCOUNT_ATTRB_VALUE('");
      sbQuery.append(strId);
      sbQuery.append("' ,'4204') PRICELVL, ");
    }
    sbQuery.append(" C901_COMPANY_ID COMPID,get_party_name(t704d.C101_IDN) IDNNAME,get_party_name(t704d.C101_GPO) GPONAME,t704d.C101_IDN GRPAFF,t704d.C101_GPO HLCAFF,t704d.C901_ADMIN_FLAG ADMINFLAG, ");
    sbQuery.append(" t704d.C704D_ADMIN_FEE ADMINFEE,t704a.COLLECTORID, t704d.C704D_GLN GLN, t704d.C704D_GPOID GPOIDD, ");
    sbQuery
        .append(" t704d.C901_CONTRACT CONTRACTFL,t704a.ACCTSOURCE,t704a.SOURCEHISTFL,t704a.ACCGROUP FROM T704_ACCOUNT t704 ");
    sbQuery.append(" ,(SELECT C704_ACCOUNT_ID act_id, ");
    // 1006432 - Group Pricing Affiliation, 1006433 - HealthCare Affiliation,103050-Collector
    // Attribute and the preferred Carrier value for an account[PMT-4359],104800-Account Source
//    sbQuery.append("  MAX (DECODE (c901_attribute_type, 1006432, c704a_attribute_value)) GRPAFF,");
//    sbQuery.append("  MAX (DECODE (c901_attribute_type, 1006433, c704a_attribute_value)) HLCAFF,");
    sbQuery
        .append("  MAX (DECODE (c901_attribute_type, 103050, c704a_attribute_value)) COLLECTORID,");
//    sbQuery
//        .append("  MAX (DECODE (c901_attribute_type, 903103, c704a_attribute_value)) CONTRACTFL,");
    sbQuery
        .append("  MAX (DECODE (c901_attribute_type, 104800, c704a_attribute_value)) ACCTSOURCE,");
    sbQuery
        .append("  MAX (DECODE (c901_attribute_type, 104800, c704a_history_fl,'')) SOURCEHISTFL,");
    sbQuery.append("  MAX (DECODE (c901_attribute_type, 7008, c704a_attribute_value)) ACCGROUP");
    sbQuery.append("  FROM t704a_account_attribute");
    sbQuery.append(" WHERE C704_ACCOUNT_ID = '");
    sbQuery.append(strId);
    sbQuery.append("' AND c704a_void_fl    IS NULL");
    sbQuery.append("  GROUP BY C704_ACCOUNT_ID   )t704a, t704d_account_affln t704d ");
    sbQuery.append(" WHERE t704.C704_ACCOUNT_ID = '");
    sbQuery.append(strId);
    sbQuery.append("'");
    sbQuery.append("AND t704.C704_ACCOUNT_ID   = t704a.act_id(+)");
    sbQuery.append("and t704.C704_ACCOUNT_ID = t704d.C704_ACCOUNT_ID(+)");
    sbQuery.append("and t704d.c704d_void_fl is null");

    log.debug("loadAccount Query:--> " + sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("ACCOUNTDETAILS", hmResult);
    hmResult = loadPricingAccount();

    hmReturn.put("ACCLIST", hmResult.get("ACCLIST"));
    hmReturn.put("GPOLIST", hmResult.get("GPOLIST"));
    hmReturn.put("ALGPRAFF", hmResult.get("ALGPRAFF"));
    hmReturn.put("ALHLCAFF", hmResult.get("ALHLCAFF"));
    // PMT-4359 -- Setting the resulted HmResult value to the hmReturn object to use in JSP
    hmReturn.put("PREFERCARRIER", hmResult.get("PREFERCARRIER"));
    return hmReturn;
    
  } // End of loadEditAccount


  /**
   * fetchPriceParamHistoryDetails - This method is written for fetching the Price Adjustment Code
   * History details *
   * 
   * @param HashMap hmParams
   * @return RowSetDynaClass
   * @exception AppError , Exception
   */
  public RowSetDynaClass fetchPriceParamHistoryDetails(HashMap hmParams) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    RowSetDynaClass rdResult = null;
    String strAuditTrailId = GmCommonClass.parseNull((String) hmParams.get("AUDITTRAILID"));
    String strAuditTransID = GmCommonClass.parseNull((String) hmParams.get("AUDITTRANSID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricparam_setup.gm_fch_priceparam_history", 3);
    gmDBManager.setString(1, strAuditTransID);
    gmDBManager.setString(2, strAuditTrailId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return rdResult;
  }
  
}

