/**
 * FileName : GmPriceRequestRptBean.java Description : for fetching the details Author : Jreddy Date
 * : Copyright : Globus Medical Inc
 */
package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.pricing.GmGroupVO;
import com.globus.valueobject.pricing.GmPricingReportVO;

public class GmPriceRequestRptBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmPriceRequestRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPriceRequestRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * @param
   * @return strAccountName
   * @throws AppError
   * 
   *         This method is used to get the Account/Party Name
   * 
   */
  public String fetchAccountName(HashMap hmParams, String strCondition) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_account_name", 5);
    String strAccId = GmCommonClass.parseNull((String) hmParams.get("ACCID"));
    String strTypeId = GmCommonClass.parseNull((String) hmParams.get("TYPEID"));
    String strRepId = GmCommonClass.parseNull((String) hmParams.get("REPID"));
    log.debug("strCondition::" + strCondition);
    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strTypeId);
    gmDBManager.setString(3, strRepId);
    gmDBManager.setString(4, strCondition);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    String strAccountName = "";
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(5)));
    strAccountName = GmCommonClass.parseNull((String) hmReturn.get("ACCOUNTNAME"));
    log.debug("strAccountName::" + strAccountName);
    gmDBManager.close();
    return strAccountName;
  }

  /**
   * @param ArrayList
   * @return alReturn
   * @throws AppError
   * 
   *         This method is used to get the Threshold Questions
   * 
   */

  public ArrayList fetchThresholdQuestions(String strRerqId) throws AppError {
    ArrayList alReturn = new ArrayList();
    log.debug(":::ClassName::fetchThresholdQuestions:::--entering:");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_questionnair_details", 2);
    gmDBManager.setString(1, strRerqId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    log.debug(":::ClassName::fetchThresholdQuestions:::--exiting:");
    return alReturn;
  }

  /**
   * @param setId, setId, typeId
   * @return
   * @throws AppError
   * 
   *         This method is used to get the construct details
   */
  public List<GmGroupVO> fetchConstructDetails(HashMap hmParams) throws AppError {
    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    List<GmGroupVO> listGmGroupVO;
    GmGroupVO gmGroupVO = (GmGroupVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_construct_details", 5);

    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("COMPANYID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("ACCPARTYID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("SELECTSYSTEMS")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("TYPEID")));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmGroupVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmGroupVO,
            gmGroupVO.getGroupProperties());
    gmDBManager.close();
    return listGmGroupVO;
  }

  /**
   * @param ArrayList strGrpId
   * @return alReturn
   * @throws AppError
   * 
   *         This method is used to get the Group Part Mapping Details details
   * 
   */

  public ArrayList fetchGroupPartMappingDetails(String strGrpId) throws AppError {
    ArrayList alReturn = new ArrayList();
    log.debug(":::ClassName::fetchGroupPartMappingDetails:::--entering:");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_group_part_details", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strGrpId));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    log.debug(":::ClassName::fetchGroupPartMappingDetails:::--exiting:");
    return alReturn;
  }


  /**
   * @param statusId, fromDate, toDate
   * @return
   * @throws AppError
   * 
   *         This method is used to fetch the price report dash board
   */
  public List<GmPricingReportVO> fetchAllPricingRequests(HashMap hmParams) throws AppError,
      Exception {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    List<GmPricingReportVO> gmPriceReportVOList;
    GmPricingReportVO gmPricingReportVO = (GmPricingReportVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_price_report_details", 7);

    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("STATUS")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("FROMDATE")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("TODATE")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("COMPANY")));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("FILTERREPS")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.execute();

    gmPriceReportVOList =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(7), gmPricingReportVO,
            gmPricingReportVO.getPriceReportProperties());
    log.debug(":::ClassName::gmPriceReportVOList:::--exiting:" + gmPriceReportVOList);
    gmDBManager.close();
    return gmPriceReportVOList;
  }

  /**
   * @param priceRequestId
   * @return
   * @throws AppError
   * 
   *         This method is used to fetch the price Submitted request details
   */
  public HashMap fetchPriceRequestDetails(HashMap hmParams) throws AppError, Exception {
    ArrayList systemResult = new ArrayList();
    ArrayList groupResult = new ArrayList();
    ArrayList priceReqResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug(":::ClassName::fetchPriceRequestDetails:::--entering:");
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_selected_request_dtls", 6);

    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("PRICINGREQID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("FILTERREPS")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();
    systemResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(4)));
    groupResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    priceReqResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    gmDBManager.close();

    hmReturn.put("SYSTEMDETAILS", systemResult);
    hmReturn.put("GROUPDETAILS", groupResult);
    hmReturn.put("PRICEREQDETAILS", priceReqResult);
    log.debug(":::ClassName::fetchPriceRequestDetails:::--exiting:");
    return hmReturn;
  }


  /**
   * loadAccountList To load the account list for given user
   * 
   * @author jreddy
   * @date
   * @param
   */
  public ArrayList loadAccountList(String strCondition, String strSearch, String srtCompId,String strGPBAccId)
      throws AppError {
    ArrayList alReturn = new ArrayList();

    log.debug("strCondition::" + strCondition);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT DISTINCT AC_ID ID,AC_NAME ACNAME, AC_NAME||'-'||AC_ID NM, AD_ID ADID ");
    sbQuery.append(" , VP_ID VPID, D_ID DID, REP_ID repid, REP_NAME rname ");
    sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700, T704_ACCOUNT T704  ");

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" WHERE ");
      sbQuery.append(strCondition);
      sbQuery.append(" and ac_id is not null  ");
    } else {
      sbQuery.append("where ac_id is not null  ");
    }
    sbQuery.append(" AND v700.ac_id = c704_account_id ");// c704_account_id
    sbQuery.append(" AND T704.c1900_company_id = '" + srtCompId + "' ");
    sbQuery.append(" AND( (REGEXP_like(t704.c704_account_nm,");
    sbQuery.append(" '" + strSearch + "' ");
    sbQuery.append(",'i')) OR (REGEXP_like(t704.c704_account_id,");
    sbQuery.append(" '" + strSearch + "' ");
    sbQuery.append(",'i'))");
    sbQuery.append(" OR v700.ac_id = '" + strGPBAccId +"' )");
    sbQuery.append(" AND c704_active_fl = 'Y' AND (t704.C901_EXT_COUNTRY_ID is null ");
    sbQuery.append(" OR t704.C901_EXT_COUNTRY_ID  in (select country_id from v901_country_codes))");
    sbQuery.append(" ORDER BY AC_NAME ");
    log.debug(" Query to get accounts is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug(" alReturn inside GmCommonBean is ::" + sbQuery.toString());

    return alReturn;
  }

  /**
   * reportProject - This method will fetch party information from party table
   * 
   * @param String strType -- Type for party to be retrieved
   * 
   * @return ArrayList List of party name
   * @exception AppError
   **/
  public ArrayList reportPartyNameList(String strUserId ,String strType, String strSearch ,String strGPBAccId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    try {
      gmDBManager.setPrepareString("GM_PKG_SM_PRICEREQUEST_RPT.GM_FCH_GPB_ACCOUNT_DETAILS", 6);
      gmDBManager.registerOutParameter(5, java.sql.Types.VARCHAR);
      gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
      gmDBManager.setString(1, strUserId);
      gmDBManager.setString(2, strType);
      gmDBManager.setString(3, strSearch);
      gmDBManager.setString(4, strGPBAccId);
      

      gmDBManager.execute();
      strBeanMsg = gmDBManager.getString(5);
      rs = (ResultSet) gmDBManager.getObject(6);
      alReturn = gmDBManager.returnArrayList(rs);
      log.debug("alReturn::" + alReturn);
      gmDBManager.close();

    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }

    return alReturn;
  }

  /**
   * loadAccountList To load the systems list for given acct
   * 
   * @author Jreddy
   * @date
   * @param
   */
  public ArrayList loadSystemsList(String strSearch) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_systems_list", 2);

    gmDBManager.setString(1, strSearch);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();

    ArrayList alSystemList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alSystemList Systems::" + alSystemList);
    return alSystemList;
  }

  
  public ArrayList fetchOrderPriceRequest(String strOrderID) throws AppError
  {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_order_price_req",2);
	  
	  	gmDBManager.setString(1, strOrderID);
	  	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	  	gmDBManager.execute();
	  	
	  	ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	  	log.debug("gm_fch_order_price_request..alReturn "+alReturn);
	  	gmDBManager.close();
	  	
	return alReturn;
  }
}
