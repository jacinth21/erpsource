package com.globus.sales.pricing.beans;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.pricing.GmImpactAnalysisVO;
import com.globus.valueobject.pricing.GmSalesAnalysisVO;

/**
 * @Description: The GmImpactAnalysisBean. (PRT-R203)
 * 
 * @author Matt Balraj
 */
public class GmImpactAnalysisBean extends GmBean {

  GmCommonClass gmCommonClass = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName());
  ArrayList alReturn = new ArrayList();

	//this will be removed all place changed with GmDataStoreVO constructor
	
	 public GmImpactAnalysisBean() {
	   super(GmCommonClass.getDefaultGmDataStoreVO());
	 }
	
	 /**
	  * Constructor will populate company info.
	  * 
	  * @param gmDataStore
	  */
	 public GmImpactAnalysisBean(GmDataStoreVO gmDataStore) {
	   super(gmDataStore);
	 }  

  /**
   * checkPriceUpdateFlag used to check if there is any c7501_price_update_fl = 'Y' updated
   * 
   * @author : Matt B
   * @param hmparams- Requestid, CompanyInfo, UserId
   * @return int
   */
  public void checkPriceUpdateFlag(HashMap hmParams) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRequestId = gmCommonClass.parseNull((String) hmParams.get("PRICEREQUESTID"));

    gmDBManager.setPrepareString("gm_pkg_PRT_impact_analysis.gm_prt_fch_priceupdate_fl", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.NUMBER);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.execute();
    int updatedflag_count = gmDBManager.getInt(2);
    gmDBManager.close();
    log.debug("--------------> checkPriceUpdateFlag hmParams --> " + hmParams);
    log.debug("--------------> updatedflag_count --> " + updatedflag_count);

    if (updatedflag_count > 0) {
      createImpactAnalysisBatch(hmParams); // code to generate impact analysis message in JMS
    }
  }

  /**
   * createImpactAnalysisBatch - method to create a batch and add message in JMS que
   * 
   * @author Matt B (PRT-R203)
   * @param hash map
   * @return
   */
  public void createImpactAnalysisBatch(HashMap hmParams) throws AppError, Exception {

    String strUserId = gmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strCompanyInfo = gmCommonClass.parseNull((String) hmParams.get("COMPANYINFO"));
    String strRefid = gmCommonClass.parseNull((String) hmParams.get("PRICEREQUESTID"));

    String strBatchType = "18755"; // code = Impact Analysis Generation
    String strBatchStatus = "18742"; /* Status = Pending processing */
    log.debug("Enter createImpactAnalysisBatch hmParams --> " + hmParams);

    String strBatchProcessService =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_PROCESS_SERVICE"));
    String strBatchMDBClass =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_CONSUMER_IMPACT_ANALYSIS_CLASS"));

    GmDataStoreVO tmpGmDataStoreVO = null;

    // companyinfo is not filled when request is from IPAD
    if (strCompanyInfo.equals("")) {
      tmpGmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
    } else {
      tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    }

    /* Create record in T9600 table */
    GmBatchBean gmBatchBean = new GmBatchBean(tmpGmDataStoreVO);
    HashMap hmBatchMap = new HashMap();
    hmBatchMap.put("BATCHID", null);
    hmBatchMap.put("BATCHTYPE", strBatchType);
    hmBatchMap.put("USERID", strUserId);
    hmBatchMap.put("BATCHSTATUS", strBatchStatus);
    String strReturnBatchId = gmBatchBean.saveBatch(hmBatchMap);

    /* Create record in T9601 batch details table */
    hmBatchMap.put("BATCHID", strReturnBatchId);
    hmBatchMap.put("REFID", strRefid);
    gmBatchBean.saveBatchDetails(hmBatchMap);

    // This is JMS Code which put the batch on a queue and trigger a
    // background JOB on separate thread, The call is asynchronous.
    if (strBatchProcessService.equals("JMS")) {
      GmQueueProducer qprod = new GmQueueProducer();
      GmMessageTransferObject tf = new GmMessageTransferObject();
      HashMap hmMessageObj = new HashMap();
      hmMessageObj.put("BATCHID", strReturnBatchId);
      hmMessageObj.put("USERID", strUserId);
      hmMessageObj.put("BATCHTYPE", strBatchType);
      hmMessageObj.put("COMPANYINFO", strCompanyInfo);

      tf.setMessageObject(hmMessageObj);
      tf.setConsumerClass(strBatchMDBClass);
      qprod.sendMessage(tf);
      log.debug("Message Sent Batch #:" + strReturnBatchId);
    }

  } // end of createImpactAnalysisBatch

  /**
   * UpdateImpactAnalysis - method to update T7520 and T7521 tables. method is called by JMS
   * processor.
   * 
   * @author Matt B (PRT-R203)
   * @param hash map
   * @return
   */
  public void updateImpactAnalysis(HashMap hmParam) throws AppError, Exception {

    log.debug("JMS Consumer ----> Entered UpdateImpactAnalysis ========> ");
    String strOutRefId = "";
    String strBatchID = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strBatchStatus = GmCommonClass.parseNull((String) hmParam.get("BATCHSTATUS"));
    String strBatchType = GmCommonClass.parseNull((String) hmParam.get("BATCHTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmPriceRequestTransBean gmPriceRequestTransBean = new GmPriceRequestTransBean(getGmDataStoreVO());

    if (!strBatchID.equals("")) {
      gmDBManager.setPrepareString("gm_pkg_PRT_impact_analysis.gm_prt_sav_impact_analysis", 3);
      gmDBManager.setString(1, strBatchID);
      gmDBManager.setString(2, strUserId);
      gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
      gmDBManager.execute();
      strOutRefId = GmCommonClass.parseNull(gmDBManager.getString(3));
      gmDBManager.commit();

      log.debug("(Changes committed - gm_pkg_PRT_impact_analysis ===> refid" + strOutRefId);
      //Sending email to AD/VP for the request in pending ad approval
      gmPriceRequestTransBean.sendReqApprovalEmaillNotification(strOutRefId);
      // Update Status to processed in T9600_Batch table
      GmBatchBean gmbatchbean = new GmBatchBean();
      strBatchStatus = "18744"; // processed
      HashMap hmBatchMap = new HashMap();
      hmBatchMap.put("BATCHID", strBatchID);
      hmBatchMap.put("BATCHTYPE", strBatchType);
      hmBatchMap.put("USERID", strUserId);
      hmBatchMap.put("BATCHSTATUS", strBatchStatus);
      String strReturnBatchId = gmbatchbean.saveBatch(hmBatchMap);

      // Update Batch Log (updating with Batchid instead of party_id)
      if (!strReturnBatchId.equals("")) {
        hmBatchMap.put("REFTYPE", strBatchType);
        hmBatchMap.put("INVID", strOutRefId);
        hmBatchMap.put("BATCHID", strReturnBatchId);
        String strActionType = strBatchType.equals("18753") ? "1804" : "1805";
        hmBatchMap.put("ACTIONTYPE", strActionType);
        gmbatchbean.saveBatchLog(hmBatchMap);
        log.debug("Batch log updated RefId=====>" + strOutRefId);
      }

    }
  }// end of class GmPricingBatchBean

  /**
   * fetchImpactAnalysisData - method to fetch data from T7520 and T7521 tables.
   * 
   * @author Matt B (PRT-R203)
   * @param hash map
   * @return
   */
  public List<GmImpactAnalysisVO> fetchImpactAnalysisData(HashMap hmParams) {
    log.debug("ImpactAnalysisBean->fetchImpactAnalysisData hmParams>>>>>" + hmParams);
    String strRequestId = GmCommonClass.parseNull((String) hmParams.get("REQUESTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    List<GmImpactAnalysisVO> impactAnalysisVO;
    GmImpactAnalysisVO gmImpactAnalysisVO = (GmImpactAnalysisVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_PRT_impact_analysis.gm_prt_fch_impact_analysis", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    impactAnalysisVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(3), gmImpactAnalysisVO,
            gmImpactAnalysisVO.getMappingProperties());
    gmDBManager.close();

    return impactAnalysisVO;
  }

  /**
   * fetchImpactEmailData - Send mail with respective Price Request PDF.
   * 
   * @author Jreddy
   * @param hash map
   * @return
   * @throws IOException
   */
  public ArrayList fetchImpactEmailData(HashMap hmParams) throws IOException {
    log.debug("ImpactAnalysisBean->fetchImpactAnalysisData hmParams>>>>>" + hmParams);
    String strRequestId = GmCommonClass.parseNull((String) hmParams.get("REQUESTID"));
    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    String strFrom = GmCommonClass.parseNull((String) hmParams.get("FROM"));
    String strCc = GmCommonClass.parseNull((String) hmParams.get("CC"));
    String strCheckBtn = GmCommonClass.parseNull((String) hmParams.get("CHECKBTN"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_PRT_impact_analysis.gm_prt_fch_impact_analysis", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    ArrayList aImpactAnlyResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    //String getCompId = "1000";
    String strCompanyId = getCompId();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    GmEmailProperties gmEmailProps = new GmEmailProperties();
    HashMap hmResult = new HashMap();
    hmResult.put("REQID", strRequestId);
    hmResult.put("ACCOUNTNM", GmCommonClass.parseNull((String) hmParams.get("ACCOUNTNM")));
    hmResult.put("CURR", GmCommonClass.parseNull((String) hmParams.get("CURR")));
    hmResult.put("PRE12MNTHSALE", GmCommonClass.parseNull((String) hmParams.get("PRE12MNTHSALE")));
    hmResult.put("PROJPROPRICE", GmCommonClass.parseNull((String) hmParams.get("PROJPROPRICE")));
    hmResult.put("PROJIMPACT", GmCommonClass.parseNull((String) hmParams.get("PROJIMPACT")));
    hmResult.put("PROJIMPATPRPPRC",
        GmCommonClass.parseNull((String) hmParams.get("PROJIMPATPRPPRC")));
    hmResult.put("ADNAME", GmCommonClass.parseNull((String) hmParams.get("ADNAME")));
    hmResult.put("GENDATE", GmCommonClass.parseNull((String) hmParams.get("GENDATE")));
    hmResult.put("ALLDATA", aImpactAnlyResult);
    String strFileName =
        createPDFFile(strRequestId, new HashMap(hmResult), new ArrayList(aImpactAnlyResult),
            "/GmPriceImpactPDFForm.jasper");

    // Open PDF file
    if (strCheckBtn.equals("pdf")) {
      File file = new File(strFileName);
      try {
        if (file.toString().endsWith(".pdf"))
          Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file);
        else {
          Desktop desktop = Desktop.getDesktop();
          desktop.open(file);
        }
      } catch (FileNotFoundException ffe) {
        throw new AppError("", "20686");
      }
    }

    // Send PDF file as Mail
    if (strCheckBtn.equals("mail")) {
      gmEmailProps.setRecipients(strFrom);
      gmEmailProps.setSender(gmResourceBundleBean.getProperty("GmRecievedRequest."
          + GmEmailProperties.FROM));
      gmEmailProps.setCc(strCc);
      gmEmailProps.setMimeType(gmResourceBundleBean.getProperty("GmRecievedRequest."
          + GmEmailProperties.MIME_TYPE));
      gmEmailProps.setSubject("Imapct Analysis " + strRequestId + ".Pdf");
      gmEmailProps.setAttachment(strFileName);
      sendMailToRep(aImpactAnlyResult, hmResult, gmEmailProps, "/GmPriceImpactPDFForm.jasper");
      removeFile(strFileName);
    }
    return aImpactAnlyResult;
  }

  // create PDF
  public String createPDFFile(String strRequestId, HashMap hmOrderSummary,
      ArrayList aImpactAnlyResult, String strJasperName) throws AppError {
    String fileName = "";
    GmJasperReport gmJasperReport = new GmJasperReport();
    try {
      hmOrderSummary.put("SUBREPORT_DIR", GmCommonClass.getString("GMJASPERFILELOCATION"));
      String strFilePath = GmCommonClass.getString("UPLOADHOME");
      fileName = strFilePath + strRequestId + ".pdf";
      File newFile = new File(strFilePath);
      if (!newFile.exists()) {
        newFile.mkdir();
      }
      gmJasperReport.setJasperReportName(strJasperName);
      gmJasperReport.setHmReportParameters(hmOrderSummary);
      gmJasperReport.setReportDataList(null);
      gmJasperReport.exportJasperToPdf(fileName);
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex.getMessage(), "", 'E');
    }
    return fileName;
  }

  // send Jasper mail with PDF file
  public void sendMailToRep(ArrayList aImpactAnlyResult, HashMap hmResult,
      GmEmailProperties gmEmailProperties, String strJasperName) {

    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName(strJasperName);
    jasperMail.setAdditionalParams(hmResult);
    jasperMail.setReportData(null);
    jasperMail.setEmailProperties(gmEmailProperties);
    HashMap hmjasperReturn = jasperMail.sendMail();
  }

  // Remove file from location
  public void removeFile(String strFileName) {
    File newPDFFile = new File(strFileName);
    newPDFFile.delete();
  }
  
  /**
   * fetchImpactAnalysisData - method to fetch data from T7520 and T7521 tables.
   * 
   * @author Matt B (PRT-R203)
   * @param hash map
   * @return
   */
  public HashMap fetchSalesAnalysisData(HashMap hmParams) {
    log.debug("ImpactAnalysisBean->fetchSalesAnalysisData hmParams>>>>>" + hmParams);
    String strRequestId = GmCommonClass.parseNull((String) hmParams.get("REQUESTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    HashMap hmReturn = new HashMap();
    ArrayList salesData = new ArrayList();
    ArrayList headerData = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_sales_analysis.gm_fch_12months_sales_data", 4);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    	salesData =
    			 GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
                .getObject(3)));
        headerData =
            GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
                .getObject(4)));
        hmReturn.put("SALESDATA", salesData);
        hmReturn.put("HEADERDATA", headerData);
    gmDBManager.close();
    return hmReturn;
  }
  
  /**
   * fetchImpactAnalysisData - method to fetch data from T7520 and T7521 tables.
   * 
   * @author Matt B (PRT-R203)
   * @param hash map
   * @return
   */
  public HashMap fetchSalesSegmentData(HashMap hmParams) {
    log.debug("ImpactAnalysisBean->fetchSalesSegmentData hmParams>>>>>" + hmParams);
    String strRequestId = GmCommonClass.parseNull((String) hmParams.get("REQUESTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    strRequestId = strRequestId.toUpperCase().contains("PR-") ? strRequestId : "PR-" + strRequestId;
    HashMap hmReturn = new HashMap();
    ArrayList segmentData = new ArrayList();
    ArrayList headerData = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_sales_analysis.gm_fch_sales_segment_data", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    segmentData =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    headerData =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(4)));
    hmReturn.put("SEGMENTDATA", segmentData);
    hmReturn.put("HEADERDATA", headerData);
    gmDBManager.close();
    return hmReturn;
  }
  

}
