package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPartPriceAdjDBBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartPriceAdjDBBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  /**
   * fetchPartPriceAdjDtlsToDash
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   * @author arajan
   */
  public ArrayList fetchPartPriceAdjDtlsToDash(HashMap hmParam) {
    // String strDateFmt = GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT",
    // "DATEFORMAT"));
    String strInitBy = GmCommonClass.parseZero((String) hmParam.get("INITBY"));
    String strDateFilter = GmCommonClass.parseZero((String) hmParam.get("DATEFLD"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("STARTDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("ENDDT"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("HSTATUS"));
    String strDateCol = "";

    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    if (strType.equals("903110")) {
      sbQuery.setLength(0);
      sbQuery.append(" SELECT gm_pkg_sm_price_adj_rpt.get_party_id('" + strPartyId
          + "') PARTYID FROM DUAL ");
      HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      strPartyId = GmCommonClass.parseNull((String) hmResult.get("PARTYID"));
      log.debug("sbQuery.toString()>>>" + sbQuery.toString() + ">>" + strPartyId);
    }

    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT t7020.C7020_PART_PRICE_ADJ_REQ_ID TXNID, t7020.C101_PARTY_ID PARTYID, GET_PARTY_NAME(t101.C101_PARTY_ID) PARTYNAME ");
    sbQuery.append(" , GET_USER_NAME(t7020.C7020_INITIATED_BY) INITBY, t7020.C7020_INITIATED_DATE "
        + " INITDATE ");
    sbQuery.append(" , GET_USER_NAME(t7020.C7020_APPROVED_BY) APPRBY,  t7020.C7020_APPROVED_DATE "
        + " APPRDATE, GET_CODE_NAME(t7020.C901_REQUEST_STATUS_CD) STATUS ");
    /*
     * sbQuery
     * .append(" , gm_pkg_sm_price_adj_rpt.get_part_count(C7020_PART_PRICE_ADJ_REQ_ID) partcnt ");
     */
    sbQuery.append(" , GET_LOG_FLAG(t7020.C7020_PART_PRICE_ADJ_REQ_ID,4000898) CALL_FLAG"); // 4000898:Price
    // Adjustment Log
    sbQuery.append(" FROM T7020_PART_PRICE_ADJ_REQUEST t7020,t101_party t101");
    sbQuery.append(" WHERE C7020_VOID_FL IS NULL ");
    sbQuery.append(" AND t7020.C101_PARTY_ID = t101.C101_PARTY_ID (+)");
    sbQuery.append(" AND t101.c1900_company_id = '" + getCompId() + "'");
    if (!strPartyId.equals("")) {
      sbQuery.append(" AND t7020.C101_PARTY_ID = '" + strPartyId + "'");
    }
    if (!strStatus.equals("")) {
      sbQuery.append(" AND t7020.C901_REQUEST_STATUS_CD IN (" + strStatus + ")");
    }
    if (!strInitBy.equals("0")) {
      sbQuery.append(" AND t7020.C7020_INITIATED_BY = '" + strInitBy + "'");
    }
    if (!strDateFilter.equals("0") && !strFromDt.equals("") && !strToDt.equals("")) {
      strDateCol =
          strDateFilter.equals("105300") ? "t7020.C7020_INITIATED_DATE"
              : "t7020.C7020_APPROVED_DATE";
      sbQuery.append(" AND TRUNC(" + strDateCol + ") BETWEEN TRUNC(TO_DATE('" + strFromDt + "','"
          + getCompDateFmt() + "'))");
      sbQuery.append(" AND TRUNC(TO_DATE('" + strToDt + "','" + getCompDateFmt() + "'))");
    }

    sbQuery.append("  order by PARTYNAME ASC, INITDATE DESC "); // Sales BackOrder Email Time

    log.debug("sbQuery.toString()>>>>>>" + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchUserNameList: This method is to fetch the list of initiated users
   * 
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList fetchUserNameList() throws AppError {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_rpt.gm_fch_initiated_users", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;
  }
}
