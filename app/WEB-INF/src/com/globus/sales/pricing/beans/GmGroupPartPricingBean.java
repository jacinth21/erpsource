package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.pricing.GmAccountPriceVO;

public class GmGroupPartPricingBean extends GmBean {
  // this will be removed all place changed with Data Store VO constructor
	 Logger log = GmLogger.getInstance(this.getClass().getName());
  public GmGroupPartPricingBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmGroupPartPricingBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * 
   * fetchGrpPartPriceDetails
   * 
   * @author rmayya
   * @date Feb 15, 2010
   * @param
   */
  public HashMap fetchGrpPartPriceDetails(HashMap hmParams) throws AppError {

    ArrayList alGroupResult, alPartResult = null;
    HashMap hmReturn = new HashMap();
    HashMap hmSearchParams = new HashMap();

    String strSearch = GmCommonClass.parseNull((String) hmParams.get("HSEARCH"));
    String strPartNumFormat = GmCommonClass.parseNull((String) hmParams.get("PARTNUMBERS"));
    String strSystemListID = GmCommonClass.parseNull((String) hmParams.get("SYSTEMLISTID"));
    strSystemListID = strSystemListID.equals("0") ? "" : strSystemListID;

    String strGroupID = GmCommonClass.parseNull((String) hmParams.get("GROUPID"));
    strGroupID = strGroupID.equals("0") ? "" : strGroupID;

    String strPricedTypeID = GmCommonClass.parseNull((String) hmParams.get("PRICEDTYPEID"));
    strPricedTypeID = strPricedTypeID.equals("0") ? "" : strPricedTypeID;

    String strGrpTypeID = GmCommonClass.parseNull((String) hmParams.get("GROUPTYPEID"));
    strGrpTypeID = strGrpTypeID.equals("0") ? "" : strGrpTypeID;

    hmSearchParams.put("PARTNUM", strPartNumFormat);
    hmSearchParams.put("SEARCH", strSearch);

    strPartNumFormat = GmCommonClass.parseNull(GmCommonClass.createRegExpString(hmSearchParams));
    System.out.println(strPartNumFormat + "::strPartNumFormat::hmParams::");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_grppartprice_dtls", 8);

    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);

    gmDBManager.setString(1, strSystemListID);
    gmDBManager.setString(2, strPartNumFormat);
    gmDBManager.setString(3, strGroupID);
    gmDBManager.setString(4, strPricedTypeID);
    gmDBManager.setString(5, strGrpTypeID);
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.execute();

    alGroupResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    alPartResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
    gmDBManager.close();
    hmReturn.put("alGroupResult", alGroupResult);
    hmReturn.put("alPartResult", alPartResult);
    if (alGroupResult.size() == 0 && alPartResult.size() == 0) {
      hmReturn.put("NO_DATA", "YES");
    }
    return hmReturn;
  }

  /**
   * savePriceDetails saves the details for the construct
   * 
   * @author rmayya
   * @date Feb 15, 2010
   * @param
   */
  public HashMap savePriceDetails(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strGroup = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRINGGROUP"));
    String strPart = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRINGPART"));
    String strUser = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strGrpIds = "";
    String strPartIds = "";
    String strSuccessGrpDtls = "";
    ArrayList outList = null;
    String strTemp = "";
    HashMap hmReturn = new HashMap();

    // update the individual part prices
    outList = null;
    strTemp = "";
    // System.out.println("strPart:"+strPart);
    if (strPart != null && !strPart.equals("") && strPart.length() > 4000) {

      outList = GmCommonClass.convertStringToList(strPart, 4000, "|");
    } else if (!strPart.equals("")) {
      outList = new ArrayList();
      outList.add(strPart);
    }
    if (outList != null) {
      // System.out.println("Inside Part Condition"+outList);
      for (Iterator itr = outList.iterator(); itr.hasNext();) {
        // System.out.println("Inside for loop");
        strTemp = (String) itr.next();

        gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_sav_part_price_dtls", 4);
        gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
        gmDBManager.setString(1, strTemp);
        gmDBManager.setString(2, strUser);
        gmDBManager.setString(4, strComments);
        gmDBManager.execute();
        strPartIds = gmDBManager.getString(3);
      }
      gmDBManager.commit();
    }


    // Update the group prices

    if (strGroup != null && strGroup.length() > 4000) {

      outList = GmCommonClass.convertStringToList(strGroup, 4000, "|");
    } else {
      outList = new ArrayList();
      outList.add(strGroup);
    }
    if (outList != null) {
      for (Iterator itr = outList.iterator(); itr.hasNext();) {
        strTemp = (String) itr.next();
        // System.out.println("Inside for loop strTemp:"+strTemp);
        gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_sav_grp_price_dtls", 5);
        gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
        gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
        gmDBManager.setString(1, strTemp);
        gmDBManager.setString(2, strUser);
        gmDBManager.setString(5, strComments);
        gmDBManager.execute();
        strGrpIds = gmDBManager.getString(3);
        strSuccessGrpDtls = gmDBManager.getString(4);
        // System.out.println("Inside for loop strGrpIds:"+strGrpIds);
      }
    }
    hmReturn.put("ERRORGRP", strGrpIds);
    hmReturn.put("SUCCESSGRPANDPARTS", strSuccessGrpDtls + strPartIds);
    gmDBManager.commit();
    return hmReturn;
  }

  public boolean getAccessFlag(String strUserId) {
    boolean accessFl = false;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    try {

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append("select  get_price_update_access('14','15','" + strUserId);
      sbQuery.append("') accessfl FROM DUAL ");

      gmDBManager.setPrepareString(sbQuery.toString());
      HashMap hmReturn = gmDBManager.querySingleRecord();
      accessFl = "0".equals(hmReturn.get("ACCESSFL")) ? false : true;
    }

    catch (Exception e) {
      e.printStackTrace();
    }
    return accessFl;
  }

  /**
   * getXmlGridData xml content generator for AD/VP price band by grid
   * 
   * @param
   */

  public String getXmlGridData(ArrayList alParam, String strSessCompanyLocale) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.sales.pricing.GmSystemPriceBand", strSessCompanyLocale));
    templateUtil.setTemplateName("GmSystemPriceBand.vm");
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    return templateUtil.generateOutput();
  }


  public void savePriceBandValue(HashMap hmParam) throws AppError {

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strUser = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSystemPriority =
        GmCommonClass.parseNull((String) hmParam.get("HSYSTEMPRIORITYSTRING"));
    String strPriorityType = "20181";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    if (!strInputString.equals("")) {
      gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_sav_pricebandvalue", 2);

      gmDBManager.setString(1, strInputString);
      gmDBManager.setString(2, strUser);
      gmDBManager.execute();
      gmDBManager.commit();
    }

    if (!strSystemPriority.equals("")) {
      gmDBManager.setPrepareString("gm_pkg_sm_baseline.gm_sm_sav_baselinevalue", 3);

      gmDBManager.setString(1, strSystemPriority);
      gmDBManager.setString(2, strPriorityType);
      gmDBManager.setString(3, strUser);
      gmDBManager.execute();
      gmDBManager.commit();
    }

  }

  /**
   * fetch AD/VP Price Band Detail
   * 
   * @param
   */
  public ArrayList loadPriceBand() throws AppError {

    ArrayList alResult = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_price_band", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();

    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();

    return alResult;
  }

  /**
   * fetchAccountsDetail
   * 
   * @return ArrayList
   * @method This method will returns Account values while calling web service.The Account Values
   *         will be retrieved based on Active Acount id.
   */
  public List<GmAccountPriceVO> fetchAccountPriceDetail(HashMap hmParams) throws AppError {

    String strToken = GmCommonClass.parseNull((String) hmParams.get("TOKEN"));
    String strLanguage = GmCommonClass.parseNull((String) hmParams.get("LANGUAGE"));
    String strPageNo = GmCommonClass.parseNull((String) hmParams.get("PAGENO"));
    String strUUID = GmCommonClass.parseNull((String) hmParams.get("UUID"));
    String strAcctId = GmCommonClass.parseNull((String) hmParams.get("ACCTID"));
    String strRefType = GmCommonClass.parseNull((String) hmParams.get("REFTYPE"));
    String strRepId = GmCommonClass.parseNull((String) hmParams.get("REPID"));
    String strSyncAll = GmCommonClass.parseNull((String) hmParams.get("SYNCALL"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    List<GmAccountPriceVO> listGmAcctPartPriceVO;
    ArrayList alResult = new ArrayList();
    GmAccountPriceVO gmAccountPriceVO = (GmAccountPriceVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_sm_acctprice_info.gm_fch_all_acctprice", 9);

    gmDBManager.setString(1, strToken);
    gmDBManager.setString(2, strLanguage);
    gmDBManager.setString(3, strPageNo);
    gmDBManager.setString(4, strUUID);
    gmDBManager.setString(5, strAcctId);
    gmDBManager.setString(6, strRepId);
    gmDBManager.setString(7, strRefType);
    gmDBManager.setString(8, strSyncAll);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmAcctPartPriceVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(9), gmAccountPriceVO,
            gmAccountPriceVO.getAcctPriceProperties());

    gmDBManager.close();
    return listGmAcctPartPriceVO;
  }

  /**
   * fetchGpoPriceDetail
   * 
   * @return ArrayList
   * @method This method will returns Account values while calling web service.The Account Values
   *         will be retrieved based on Active Acount id.
   */
  public List<GmAccountPriceVO> fetchGpoPriceDetail(HashMap hmParams) throws AppError {
    String strToken = GmCommonClass.parseNull((String) hmParams.get("TOKEN"));
    String strLanguage = GmCommonClass.parseNull((String) hmParams.get("LANGUAGE"));
    String strPageNo = GmCommonClass.parseNull((String) hmParams.get("PAGENO"));
    String strUUID = GmCommonClass.parseNull((String) hmParams.get("UUID"));
    String strAcctId = GmCommonClass.parseNull((String) hmParams.get("ACCTID"));
    String strRefType = GmCommonClass.parseNull((String) hmParams.get("REFTYPE"));
    String strRepId = GmCommonClass.parseNull((String) hmParams.get("REPID"));
    String strSyncAll = GmCommonClass.parseNull((String) hmParams.get("SYNCALL"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmAccountPriceVO> listGmAcctGrpPriceVO;
    GmAccountPriceVO gmAccountPriceVO = (GmAccountPriceVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_sm_acctprice_info.gm_fch_all_gpoprice", 9);

    gmDBManager.setString(1, strToken);
    gmDBManager.setString(2, strLanguage);
    gmDBManager.setString(3, strPageNo);
    gmDBManager.setString(4, strUUID);
    gmDBManager.setString(5, strAcctId);
    gmDBManager.setString(6, strRepId);
    gmDBManager.setString(7, strRefType);
    gmDBManager.setString(8, strSyncAll);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmAcctGrpPriceVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(9), gmAccountPriceVO,
            gmAccountPriceVO.getGPOPriceProperties());

    gmDBManager.close();
    return listGmAcctGrpPriceVO;
  }
  
	/**
	 * 
	 * fetchPartSystemPricingDetails Suganthi
	 * 
	 * @param
	 */
	public HashMap fetchPartSystemPriceDetails(HashMap hmParams)
			throws AppError {

		ArrayList alPartResult = null;
		HashMap hmReturn = new HashMap();
		HashMap hmSearchParams = new HashMap();

		String strPartNumFormat = GmCommonClass.parseNull((String) hmParams
				.get("PARTNUMBERS"));
		String strSystem = GmCommonClass.parseNull((String) hmParams
				.get("SYSTEMNAME"));
		String strSystemListID = GmCommonClass.parseNull((String) hmParams
				.get("SYSTEMLISTID"));
		String strAccId = GmCommonClass.parseNull((String) hmParams
				.get("ACCID")); // change
		strSystemListID = strSystemListID.equals("0") ? "" : strSystemListID;

		

		hmSearchParams.put("PARTNUM", strPartNumFormat);
		hmSearchParams.put("SYSTEM", strSystem);
		hmSearchParams.put("ACCID", strAccId);

		// strPartNumFormat =
		// GmCommonClass.parseNull(GmCommonClass.createRegExpString(hmSearchParams));

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString(
				"gm_pkg_pd_group_pricing.gm_fch_sys_partprice_dtls", 4);

		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

		gmDBManager.setString(1, strSystem);
		gmDBManager.setString(2, strPartNumFormat);
		gmDBManager.setString(3, strAccId);
		gmDBManager.execute();

		alPartResult = gmDBManager.returnArrayList((ResultSet) gmDBManager
				.getObject(4));
		gmDBManager.close();
		hmReturn.put("alPartResult", alPartResult);
		if (alPartResult.size() == 0) {
			hmReturn.put("NO_DATA", "YES");
		}
		return hmReturn;
	}
}
