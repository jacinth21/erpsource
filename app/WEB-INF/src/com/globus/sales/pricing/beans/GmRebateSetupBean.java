package com.globus.sales.pricing.beans;


import java.text.ParseException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmRebateSetupBean extends GmBean{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public GmRebateSetupBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**saveRebateManagementDtls - this method for save the rebate management account Details
	 * @param hmParam
	 * @return String
	 * @throws AppError
	 * @throws ParseException
	 */
	public String saveRebateManagementDtls(HashMap hmParam) throws AppError{

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		String strRebateId = GmCommonClass.parseNull((String) hmParam.get("REBATEID"));
		String strRebateAccType = GmCommonClass.parseNull((String) hmParam
				.get("GROUPTYPEID"));
		String strPartId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strRebatePartType = GmCommonClass.parseNull((String) hmParam
				.get("REBATEPARTTYPEID"));
		String strRebateCategory = GmCommonClass.parseNull((String) hmParam
				.get("REBATECATEGORYID"));
		String strRebateRate = GmCommonClass.parseNull((String) hmParam
				.get("REBATERATE"));
		String strContractFrmDate = GmCommonClass.parseNull((String) hmParam
				.get("REBATEFROMDATE"));
		String strContractToDate = GmCommonClass.parseNull((String) hmParam
				.get("REBATETODATE"));
		String strEffectiveDate = GmCommonClass.parseNull((String) hmParam
				.get("REBATEEFFECTIVEDATE"));
		String strRebateType = GmCommonClass.parseNull((String) hmParam
				.get("REBATETYPEID"));
		String strPaymentId = GmCommonClass.parseNull((String) hmParam
				.get("REBATEPAYMENTID"));
		String strContractInfo = GmCommonClass.parseNull((String) hmParam
				.get("REBATECONTRACTINFO"));
		String strRebateCmnts = GmCommonClass.parseNull((String) hmParam
				.get("REBATECOMMENTS"));
		String strTier1Percent = GmCommonClass.parseNull((String) hmParam
				.get("TIER1PERCENT"));
		String strTier1frmAmt = GmCommonClass.parseNull((String) hmParam
				.get("TIER1FROMAMT"));
		String strTier1ToAmt = GmCommonClass.parseNull((String) hmParam
				.get("TIER1TOAMT"));
		String strTier2Percent = GmCommonClass.parseNull((String) hmParam
				.get("TIER2PERCENT"));
		String strTier2frmAmt = GmCommonClass.parseNull((String) hmParam
				.get("TIER2FROMAMT"));
		String strTier2ToAmt = GmCommonClass.parseNull((String) hmParam
				.get("TIER2TOAMT"));
		String strTier3Percent = GmCommonClass.parseNull((String) hmParam
				.get("TIER3PERCENT"));
		String strTier3frmAmt = GmCommonClass.parseNull((String) hmParam
				.get("TIER3FROMAMT"));
		String strTier3ToAmt = GmCommonClass.parseNull((String) hmParam
				.get("TIER3TOAMT"));
		String strActiveFl = GmCommonClass.getCheckBoxValue((String) hmParam.get("ACTIVEFL"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		  String strCompanyId = getGmDataStoreVO().getCmpid();
		gmDBManager.setPrepareString(
				"gm_pkg_sm_rebate_setup.gm_sav_rebate_managment_dtls", 25);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		gmDBManager.setString(1, strRebateId);
		gmDBManager.setString(2, strRebateAccType);
		gmDBManager.setString(3, strPartId);
		gmDBManager.setString(4, strRebatePartType);
		gmDBManager.setString(5, strRebateCategory);
		gmDBManager.setString(6, strRebateRate);
		gmDBManager.setString(7, strContractFrmDate);
		gmDBManager.setString(8, strContractToDate);
		gmDBManager.setString(9, strEffectiveDate);
		gmDBManager.setString(10, strRebateType);
		gmDBManager.setString(11, strPaymentId);
		gmDBManager.setString(12, strContractInfo);
		gmDBManager.setString(13, strRebateCmnts);
		gmDBManager.setString(14, strTier1Percent);
		gmDBManager.setString(15, strTier1frmAmt);
		gmDBManager.setString(16, strTier1ToAmt);
		gmDBManager.setString(17, strTier2Percent);
		gmDBManager.setString(18, strTier2frmAmt);
		gmDBManager.setString(19, strTier2ToAmt);
		gmDBManager.setString(20, strTier3Percent);
		gmDBManager.setString(21, strTier3frmAmt);
		gmDBManager.setString(22, strTier3ToAmt);
		gmDBManager.setString(23, strActiveFl);
		gmDBManager.setString(24, strUserId);
		gmDBManager.setString(25, strCompanyId);
		gmDBManager.execute();
		strRebateId = GmCommonClass.parseNull(gmDBManager.getString(1));
		gmDBManager.commit();
		return strRebateId;
	}
	

	  /**This method is used to save part Numbers
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String saveRebatePartMapping(HashMap hmParam) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strPartReturnStr="";
		    String strRebateId= GmCommonClass.parseNull((String) hmParam.get("HREBATEID"));
		    String strPartInputStr = GmCommonClass.parseNull((String) hmParam.get("PARTINPUTSTRING"));
		    String strEffectiveDate = GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEPARTDATE"));
		    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		    gmDBManager.setPrepareString("gm_pkg_sm_rebate_setup.gm_sav_rebate_part_mapping_dtls", 5);
		    gmDBManager.setString(1, strRebateId);
		    gmDBManager.setString(2, strPartInputStr);
		    gmDBManager.setString(3, strEffectiveDate);
		    gmDBManager.setString(4, strUserID);
			gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
		    gmDBManager.execute();
		    strPartReturnStr = GmCommonClass.parseNull(gmDBManager.getString(5));
		    gmDBManager.commit();
			
			if(!strPartReturnStr.equals(""))
				{	
					throw new AppError("The following part number(s) are invalid parts:<b>"+strPartReturnStr+"" +
							"</b>." +
							"<br>Please remove the part number(s) mentioned from your excel in order to save. " +
							"<br>For parts unable to be loaded, please check with the Project Manager.", "", 'E');
				}
			else
			{
		    return strPartReturnStr;	
			}
		 
	  }
}
	
