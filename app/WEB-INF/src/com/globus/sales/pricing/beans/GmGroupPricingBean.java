package com.globus.sales.pricing.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmGroupPricingBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	  public GmGroupPricingBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmGroupPricingBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	 /**
     * fetchPricingStatusReport - This method will report request status
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchPricingStatusReport(HashMap hmParam)throws AppError
    {
       ArrayList alPriceStatusReport = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager (); 
        StringBuffer sbQuery = new StringBuffer();
        String strVPIDs = GmCommonClass.parseNull((String) hmParam.get("INPUTVPIDS")); 
        String strPricingStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS")); 
        String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        String strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
        
 		sbQuery.append(" SELECT  C901_REQUEST_TYPE REQ_TYPE_ID, DECODE(C901_REQUEST_TYPE,'903107','Account','Group') REQ_TYPE,");
 		sbQuery.append(" DECODE(C101_GPO_ID,'',c704_account_id,C101_GPO_ID) REQ_ID");
 		sbQuery.append(" ,DECODE(C101_GPO_ID,'',get_account_name (c704_account_id),get_party_name(C101_GPO_ID)) ACCOUNTNM ");
 		sbQuery.append(" ,DECODE(C101_GPO_ID,'',decode(get_code_name(gm_pkg_sm_pricing_request.GET_ACCOUNT_CONTRACT(c704_account_id)),'Yes','Yes','No') ,'N/A') CONTRACT ");
 		sbQuery.append(" ,get_account_gpo_id (c704_account_id) GRP_ACC_ID, DECODE(C101_GPO_ID,'',get_party_name(get_account_gpo_id (c704_account_id)) ) GRP_ACC ");
 		sbQuery.append(" ,GET_CODE_NAME(c901_aprl_lvl) REQD_APPROVAL ");
 		sbQuery.append(" ,GET_CODE_NAME (c901_request_status) STATUS ");
 		sbQuery.append(" ,GET_USER_NAME(GET_REP_ID(c704_account_id)) REPNAME, GET_AD_REP_NAME(GET_REP_ID(c704_account_id)) ADNM ");
 		sbQuery.append(" ,GET_REP_ID(c704_account_id) REP_ID, GET_VP_NAME(GET_REP_ID(c704_account_id)) VPNAME ");
 		sbQuery.append(" ,c7000_account_price_request_id REQID, c704_account_id ACCOUNTID , GET_USER_NAME (c7000_created_by) CREATEDBY  ");
 		sbQuery.append(" ,TO_CHAR (c7000_created_date,GET_RULE_VALUE('DATEFMT','DATEFORMAT') || ' HH24:MI:SS') CREATEDDATE "); 
 		sbQuery.append(" FROM t7000_account_price_request t7000 ,v700_territory_mapping_detail v700 ");
 		sbQuery.append(" WHERE  c704_account_id = v700.ac_id ");
 		sbQuery.append(" AND c7000_void_fl IS NULL ");
 		sbQuery.append(" AND (DECODE(c901_request_status,52186,v700.rep_id,1) = decode(c901_request_status,52186,");
 		sbQuery.append(strUserId);
 		sbQuery.append(" ,1) OR "); 
 		sbQuery.append(" DECODE(c901_request_status,52186,c7000_created_by,1) = c7000_created_by ");
 		//sbQuery.append(strUserId);
 		sbQuery.append(" ) ");
 		sbQuery.append(" AND  (( c704_account_id IN (SELECT	DISTINCT AC_ID ID FROM V700_TERRITORY_MAPPING_DETAIL V700, T704_ACCOUNT T704 ");  
 		sbQuery.append(" WHERE  v700.ac_id = c704_account_id AND c704_active_fl = 'Y' ");
 		if (!strVPIDs.equals("")){
 			sbQuery.append("  AND ");
 			sbQuery.append(" v700.vp_id  IN ( ");
 			sbQuery.append(strVPIDs);
 			sbQuery.append(" )" );
    	}
 		if (!strAccessFilter.equals(""))
		{
 			sbQuery.append(" AND ");
			sbQuery.append(strAccessFilter);
		}
 		sbQuery.append(" )))");
 		if (!strPricingStatus.equals("")){
 			sbQuery.append(" AND c901_request_status IN (");
 			sbQuery.append(strPricingStatus);
 			sbQuery.append(" ) ");
    	}
 		sbQuery.append(" ORDER BY createddate asc, accountnm, c901_request_status ");
		log.debug( "Query for getting Status Report " + sbQuery.toString());
		alPriceStatusReport = gmDBManager.queryMultipleRecords(sbQuery.toString());

		return alPriceStatusReport;
    }
	
    /**
     * fetchPendingSystemPrice - This method will report all Systems pending price for an account
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchPendingSystemAccountPrice(HashMap hmParam)throws AppError
    {
        HashMap hmResult = new HashMap();
    	ArrayList alPendingSystemAccountPrice = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager (); 
        
        String strAccountIDs = GmCommonClass.parseNull((String) hmParam.get("INPUTACCTS")); 
        String strPricingStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
        String strSystemIDs = GmCommonClass.parseNull((String) hmParam.get("SYSTEMS"));
        int intPricedSystemCount = 0;
        int intPendingSystemCount = 0;
        int intTotalSystemCount = 0;
        String strPendingSystemCount = "";
        
        gmDBManager.setPrepareString("gm_pkg_sm_pricing_report.gm_fch_pend_sys_account_price",5);
        gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(5,OracleTypes.INTEGER);
        
        gmDBManager.setString(1, strAccountIDs);
        gmDBManager.setString(2, strPricingStatus);
        gmDBManager.setString(3, strSystemIDs);
        gmDBManager.execute();
        
        alPendingSystemAccountPrice = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4)));
        intPricedSystemCount = gmDBManager.getInt(5);
        gmDBManager.close();
        
        intPendingSystemCount = alPendingSystemAccountPrice.size();
        intTotalSystemCount = intPendingSystemCount + intPricedSystemCount;
        strPendingSystemCount = "(" + intPendingSystemCount + "/" + intTotalSystemCount + ")";
        log.debug(" alPendingSystemAccountPrice " + alPendingSystemAccountPrice + " intPendingSystemCount " +intPendingSystemCount + " intPricedSystemCount " +intPricedSystemCount);
        
        hmResult.put("ALNOPRICESYSTEM", alPendingSystemAccountPrice);
        hmResult.put("STRPENDINGSYSTEMCOUNT", strPendingSystemCount);
        
        return hmResult;
    }
    /**
     * fetchPendingSystemPrice - This method will report all Systems pending price for an account
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchGPOPendingSystemAccountPrice(HashMap hmParam)throws AppError
    {
        HashMap hmResult = new HashMap();
    	ArrayList alPendingSystemAccountPrice = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager (); 
        
        String strAccountIDs = GmCommonClass.parseNull((String) hmParam.get("INPUTACCTS")); 
        String strPricingStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
        String strSystemIDs = GmCommonClass.parseNull((String) hmParam.get("SYSTEMS"));
        String strGPOId = GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC"));
        int intPricedSystemCount = 0;
        int intPendingSystemCount = 0;
        int intTotalSystemCount = 0;
        String strPendingSystemCount = "";
        
        gmDBManager.setPrepareString("gm_pkg_sm_pricing_report.gm_fch_gpo_pend_sys_account_pr",5);
        gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(5,OracleTypes.INTEGER);
        
        gmDBManager.setString(1, strGPOId);
        gmDBManager.setString(2, strPricingStatus);
        gmDBManager.setString(3, strSystemIDs);
        gmDBManager.execute();
        
        alPendingSystemAccountPrice = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4)));
        intPricedSystemCount = gmDBManager.getInt(5);
        gmDBManager.close();
        
        intPendingSystemCount = alPendingSystemAccountPrice.size();
        intTotalSystemCount = intPendingSystemCount + intPricedSystemCount;
        strPendingSystemCount = "(" + intPendingSystemCount + "/" + intTotalSystemCount + ")";
        log.debug(" alPendingSystemAccountPrice " + alPendingSystemAccountPrice + " intPendingSystemCount " +intPendingSystemCount + " intPricedSystemCount " +intPricedSystemCount);
         
        hmResult.put("ALNOPRICESYSTEM", alPendingSystemAccountPrice);
        hmResult.put("STRPENDINGSYSTEMCOUNT", strPendingSystemCount);
       
        return hmResult;
    }
    /**
     * fetchPendingGroupAccountPrice - This method will report all Groups pending price for an account
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchPendingGroupAccountPrice(HashMap hmParam)throws AppError
    {
        HashMap hmResult = new HashMap();
    	ArrayList alNoPriceGroup = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager (); 
        
        String strAccountIDs = GmCommonClass.parseNull((String) hmParam.get("INPUTACCTS")); 
        String strPricingStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
        String strGrpDateRange = GmCommonClass.parseNull((String) hmParam.get("GRPDATERANGE")); 
        int intPricedGroupCount = 0;
        int intPendingGroupCount = 0;
        int intTotalGroupCount = 0;
        String strPendingGroupCount = "";
        
        gmDBManager.setPrepareString("gm_pkg_sm_pricing_report.gm_fch_pend_grp_account_price",5);
        gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(5,OracleTypes.INTEGER);
        
        gmDBManager.setString(1, strAccountIDs);
        gmDBManager.setString(2, strPricingStatus);
        gmDBManager.setString(3, strGrpDateRange);
        gmDBManager.execute();
        
        alNoPriceGroup = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4)));
        intPricedGroupCount = gmDBManager.getInt(5);
        log.debug(" intPricedGroupCount  ----------------- "+intPricedGroupCount);
        gmDBManager.close();
        
        intPendingGroupCount = alNoPriceGroup.size();
        intTotalGroupCount = intPendingGroupCount + intPricedGroupCount;
        strPendingGroupCount = "(" + intPendingGroupCount + "/" + intTotalGroupCount + ")";
        
        hmResult.put("ALNOPRICEGROUP", alNoPriceGroup);
        hmResult.put("STRPENDINGGROUPCOUNT", strPendingGroupCount);
        
        return hmResult;
    }
    
    /**
     * fetchPendingGroupAccountPrice - This method will report all Groups pending price for an account
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchGPOPendingGroupAccountPrice(HashMap hmParam)throws AppError
    {
        HashMap hmResult = new HashMap();
    	ArrayList alNoPriceGroup = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager (); 
        
        String strAccountIDs = GmCommonClass.parseNull((String) hmParam.get("INPUTACCTS")); 
        String strPricingStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
        String strGrpDateRange = GmCommonClass.parseNull((String) hmParam.get("GRPDATERANGE"));
        String strGPOId = GmCommonClass.parseNull((String) hmParam.get("STRGROUPACC"));
        int intPricedGroupCount = 0;
        int intPendingGroupCount = 0;
        int intTotalGroupCount = 0;
        String strPendingGroupCount = "";
        
        gmDBManager.setPrepareString("gm_pkg_sm_pricing_report.gm_fch_gpo_pend_grp_acc_pr",5);
        gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(5,OracleTypes.INTEGER);
        
        gmDBManager.setString(1, strGPOId);
        gmDBManager.setString(2, strPricingStatus);
        gmDBManager.setString(3, strGrpDateRange);
        gmDBManager.execute();
        
        alNoPriceGroup = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4)));
        intPricedGroupCount = gmDBManager.getInt(5);
        log.debug(" intPricedGroupCount  ----------------- "+intPricedGroupCount);
        gmDBManager.close();
        
        intPendingGroupCount = alNoPriceGroup.size();
        intTotalGroupCount = intPendingGroupCount + intPricedGroupCount;
        strPendingGroupCount = "(" + intPendingGroupCount + "/" + intTotalGroupCount + ")";
        
        hmResult.put("ALNOPRICEGROUP", alNoPriceGroup);
        hmResult.put("STRPENDINGGROUPCOUNT", strPendingGroupCount);
        
        return hmResult;
    }
    
    
    /**
     * saveMultiAcctRequests - This method will save multiple pricing request
     * @return void
     * @exception AppError
     */
    public void saveMultiAcctRequests(HashMap hmParam)throws AppError
    { 
        GmDBManager gmDBManager = new GmDBManager (); 
        
        String strAccountIDs = GmCommonClass.parseNull((String) hmParam.get("HACCOUNTS"));  
        String strSetIDs = GmCommonClass.parseNull((String) hmParam.get("INPUTSETS")); 
        String strChangeType = GmCommonClass.parseNull((String) hmParam.get("CHANGETYPE")); 
        String strChangeValue = GmCommonClass.parseNull((String) hmParam.get("CHANGEVALUE")); 
        String strEffectiveDate = GmCommonClass.parseNull((String) hmParam.get("EFFECTIVEDATE")); 
        String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        
        gmDBManager.setPrepareString("gm_pkg_sm_pricing_request.gm_save_multiple_requests",6); 
        
        gmDBManager.setString(1, strAccountIDs);
        gmDBManager.setString(2, strSetIDs);
        gmDBManager.setString(3, strChangeType);
        gmDBManager.setString(4, strChangeValue);
        gmDBManager.setString(5, strEffectiveDate);
        gmDBManager.setString(6, strUserId);
        gmDBManager.execute();
         
        gmDBManager.commit(); 
         
    }
    
    
    /**
     * fetchPricingStatusReport - This method will report request status
     * @return RowSetDynaClass
     * @exception AppError
     */
    public RowSetDynaClass fetchAccountGroupPrice(HashMap hmParam)throws AppError
    {	Logger log = GmLogger.getInstance(this.getClass().getName());
        RowSetDynaClass rdResult = null;
        GmDBManager gmDBManager = new GmDBManager ();  

        String strGroups = GmCommonClass.parseNull((String) hmParam.get("INPUTGROUPS")); 
        String strAccountID  = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID")); 
        String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID")); 
        String strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM")); 
        
        
    	if (!strPnum.equals("")) {
    		
    		strPnum = strPnum.replaceAll(",", "|^");
    		strPnum = "^".concat(strPnum);
		}	
    	log.debug(" values in hmParam >>>>>>>>>>>>>>>>>>>>>>>>>>>" + strPnum);
        gmDBManager.setPrepareString("gm_pkg_sm_pricing_report.gm_fch_account_price_report",5);
        gmDBManager.registerOutParameter(5,OracleTypes.CURSOR);
        
        gmDBManager.setString(1, strAccountID);
        gmDBManager.setString(2, strGroups);
        gmDBManager.setString(3, strPnum);
        gmDBManager.setString(4, strSetID);
        gmDBManager.execute();
        
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(5));
        gmDBManager.close();
        
        return rdResult;
    }
    /**
     * fetchGrpPartPricingType - This method will returns Pricing Type, Part Group ID 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchGrpPartPricingType(String strPartNum) throws AppError{
    	ArrayList alReturn = new ArrayList();
    	GmDBManager gmDBManager = new GmDBManager();
    	
    	gmDBManager.setPrepareString("gm_pkg_sm_pricing_report.gm_fch_grp_part_pricing_type",2);
    	gmDBManager.setString(1, strPartNum);
    	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); 
		gmDBManager.execute();	
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		gmDBManager.close();
		
		return alReturn;
    }
}
