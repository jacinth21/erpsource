package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

public class GmConstructBuilderBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadConstructsList - This method will be used to load the Constructs List for the System
   * Selected
   * 
   * @param String - systemId
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList loadConstructsList(String systemId) throws AppError {
    ArrayList alConstructList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_system_constructlist", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, systemId);
    gmDBManager.execute();

    alConstructList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alConstructList;
  }

  /**
   * loadGroupsList - This method will be used to load the group lists for the System Selected
   * 
   * @param
   * @return ArrayList - will be used by multi check box
   * @exception AppError
   */
  public ArrayList loadGroupsList() throws AppError {
    ArrayList alGroupsList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_system_groups", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();

    alGroupsList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alGroupsList;
  }

  /**
   * loadGroupPartDetail - This method will be used to load the parts details for a group
   * 
   * @param String - groupId
   * @return ArrayList - will be used pop-up
   * @exception AppError
   */
  public ArrayList loadGroupPartDetail(String groupId) throws AppError {
    ArrayList alGroupPartList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_group_part_detail", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, groupId);
    gmDBManager.execute();

    alGroupPartList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alGroupPartList;
  }

  /**
   * Fetches the master details of the construct
   * 
   * @author rmayya
   * @date Nov 17, 2009
   * @param
   */
  public HashMap fetchConstructMasterDetails(String strConstructId) throws AppError {

    HashMap hmResult = null;

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_constructmaster_details", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConstructId);
    gmDBManager.execute();

    hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmResult;
  }

  /**
   * fetchConstructGroupsDetails will fecth the constructs groups details
   * 
   * @author rmayya
   * @date Nov 17, 2009
   * @param
   */
  public ArrayList fetchConstructGroupsDetails(HashMap hmParams) throws AppError {

    ArrayList alResult = null;

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_constructgrp_details", 3);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("strConstructId")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("strInpGroupIds")));
    gmDBManager.execute();

    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alResult;
  }

  /**
   * saveConstructDetails saves the details for the construct
   * 
   * @author rmayya
   * @date Nov 17, 2009
   * @param
   */
  public String saveConstructDetails(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager();

    String strConstructId = GmCommonClass.parseNull((String) hmParam.get("CONSTRUCTID"));
    String strConstructName = GmCommonClass.parseNull((String) hmParam.get("CONSTRUCTNAME"));
    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMLISTID"));
    String strStatusId = GmCommonClass.parseNull((String) hmParam.get("STATUSID"));
    String strLevelId = GmCommonClass.parseNull((String) hmParam.get("CONSTLEVELID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPrincialConst =
        GmCommonClass.getCheckBoxValue((String) hmParam.get("PRINCIPALCONSTRUCT"));

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));

    gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_sav_construct_builder", 9);

    gmDBManager.registerOutParameter(9, java.sql.Types.VARCHAR);
    gmDBManager.setString(1, strConstructId);
    gmDBManager.setString(2, strConstructName);
    gmDBManager.setString(3, strSystemId);
    gmDBManager.setString(4, strStatusId);
    gmDBManager.setString(5, strLevelId);
    gmDBManager.setString(6, strInputString);
    gmDBManager.setString(7, strUserId);
    gmDBManager.setString(8, strPrincialConst);

    gmDBManager.execute();
    String strConstructIdFromDb = gmDBManager.getString(9);
    gmDBManager.commit();

    return strConstructIdFromDb;
  }

  /**
   * common method used to get the grid xml data
   * 
   * @author rmayya
   * @date Nov 24, 2009
   * @param
   */
  public String getXmlGridData(ArrayList alParam, String strSessCompanyLocale, String strVmProPath)
      throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmProPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName("GmConstructBuilder.vm");
    templateUtil.setTemplateSubDir("sales/pricing/templates");
    return templateUtil.generateOutput();
  }
}
