package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPartPriceAdjBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j



  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartPriceAdjBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchPartNumberDtls - To fetcht the part number details
   * 
   * @param hmParam
   * @return String
   * @exception AppError
   **/
  public String fetchPartNumberDtls(HashMap hmParam) throws AppError {

    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strGpoId = GmCommonClass.parseZero((String) hmParam.get("PRID"));
    String strAdjCode = GmCommonClass.parseZero((String) hmParam.get("ADJCODE"));
    String strAdjVal = GmCommonClass.parseZero((String) hmParam.get("ADJVALUE"));
    String strPartString = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_price_adj_rpt.get_part_dtls", 5);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strAccId);
    gmDBManager.setString(3, strGpoId);
    gmDBManager.setString(4, strPartNum);
    gmDBManager.setString(5, strAdjCode);
    gmDBManager.setString(6, strAdjVal);

    gmDBManager.execute();
    strPartString = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strPartString;
  }


  /**
   * loadPartPriceAdjHeader - To fetch the header details
   * 
   * @param hmParam
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadPartPriceAdjHeader(HashMap hmParam) throws AppError {

    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strGrpName = "";
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmAccDtls = new HashMap();
    HashMap hmAccSummary = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean();

    if (strType.equals("903110")) {// Account
      hmAccSummary = GmCommonClass.parseNullHashMap(gmCustomerBean.getAccSummary(strAccId));
      hmAccSummary = GmCommonClass.parseNullHashMap((HashMap) hmAccSummary.get("ACCSUM"));
    } else {// Group Account
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      sbQuery.append(" SELECT get_party_name('" + strAccId + "') PARTYNAME FROM DUAL ");
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      gmDBManager.close();
      strGrpName = (String) hmResult.get("PARTYNAME");
      hmAccSummary.put("PRNAME", strGrpName);
      hmAccSummary.put("PARTYID", strAccId);
      hmAccSummary.put("ACCID", strAccId);
      hmAccSummary.put("PRID", strAccId);
    }

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_rpt.gm_fch_part_price_adj_header", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strType);
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("TXNID")));

    gmDBManager.execute();
    hmAccDtls = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();

    hmReturn.put("ACCSUMMARY", hmAccSummary);
    hmReturn.put("ACCDTLS", hmAccDtls);

    return hmReturn;
  }

  /**
   * savePartPriceAdj : To save the Part Price Adjustment detials
   * 
   * @return String
   * @throws AppError
   * */
  public String savePartPriceAdj(HashMap hmParam) throws AppError {

    String strTxnID = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_txn.gm_sav_part_price_adj", 8);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("PARTYID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("HREMSTRING")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("ACCID")));
    gmDBManager.setString(7, GmCommonClass.parseZero((String) hmParam.get("PRID")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON")));

    gmDBManager.execute();
    strTxnID = GmCommonClass.parseNull(gmDBManager.getString(5));
    log.debug("strTxnID>>>" + strTxnID);
    gmDBManager.commit();
    return strTxnID;
  }

  /**
   * submitPartPriceAdj : To submit the Part Price Adjustment details for approval
   * 
   * @return String
   * @throws AppError
   * */
  public String submitPartPriceAdj(HashMap hmParam) throws AppError {

    String strTxnID = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_txn.gm_submit_part_price_adj", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("PARTYID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON")));

    gmDBManager.execute();
    strTxnID = GmCommonClass.parseNull(gmDBManager.getString(3));
    log.debug("strTxnID>>>" + strTxnID);
    gmDBManager.commit();
    return strTxnID;
  }

  /**
   * fetchPartPriceAdjDtls : To fetch the part price adjustment details
   * 
   * @return String
   * @throws AppError
   * */
  public ArrayList fetchPartPriceAdjDtls(String strTxnId) throws AppError {

    ArrayList alAccDtls = new ArrayList();
    ArrayList alDtls = new ArrayList();
    ArrayList alErrAccDtls = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_rpt.gm_fch_part_price_adj_dtls", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTxnId);

    gmDBManager.execute();
    alDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    alErrAccDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    alAccDtls.addAll(alDtls);
    alAccDtls.addAll(alErrAccDtls);
    return alAccDtls;
  }

  /**
   * fetchPartyType : To get the party type
   * 
   * @return String
   * @throws AppError
   * */
  public String fetchPartyType(String strPartyId) throws AppError {

    String strType = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_price_adj_rpt.get_party_type", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.NUMBER);
    gmDBManager.setString(2, strPartyId);

    gmDBManager.execute();
    strType = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strType;
  }

  /**
   * getAccIdFromPartyId : Get the accout id from party id
   * 
   * @return String
   * @throws AppError
   * */
  public String getAccIdFromPartyId(String strPartyId) throws AppError {

    String strAccId = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_sm_price_adj_rpt.get_account_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.NUMBER);
    gmDBManager.setString(2, strPartyId);

    gmDBManager.execute();
    strAccId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strAccId;
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParam) throws AppError {

    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE"));
    String strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATEPATH"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    ArrayList alResult = new ArrayList();

    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alResult"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setTemplateName(strTemplateName);
    return templateUtil.generateOutput();
  }

  /**
   * approvePartPriceAdj : To approve the Part Price adjustment transaction
   * 
   * @throws AppError
   * */
  public String approvePartPriceAdj(HashMap hmParam) throws AppError {
    String strOutString = "";
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_txn.gm_approv_part_price_adj", 4);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON")));
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strOutString = GmCommonClass.parseNull(gmDBManager.getString(4));
    if (strType.equals("903110")) { // 903110:
                                    // Account

      updateAccountPricing(hmParam, gmDBManager);
    }
    gmDBManager.commit();
    return strOutString;
  }

  /**
   * denyPartPriceAdj : To deny the Part Price adjustment transaction
   * 
   * @throws AppError
   * */
  public String denyPartPriceAdj(HashMap hmParam) throws AppError {
    String strOutString = "";
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_txn.gm_deny_part_price_adj", 4);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON")));
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strOutString = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();
    return strOutString;
  }

  /**
   * updateAccountPricing : To insert record in t705_account_pricing table while implementing the
   * transaction
   * 
   * @throws AppError
   * */
  public void updateAccountPricing(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_txn.gm_sav_acc_partnum_price", 1);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.execute();
  }

}
