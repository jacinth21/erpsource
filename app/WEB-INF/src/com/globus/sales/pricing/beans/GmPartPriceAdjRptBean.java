package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPartPriceAdjRptBean extends GmBean{

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());
  

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmPartPriceAdjRptBean(GmDataStoreVO gmDataStore) {
	  super(gmDataStore);
	}

	  /**
  /**
   * fetchPartPriceAdjDetails - This method is written for fetching the Part Price Adj Details *
   * 
   * @param HashMap hmParams
   * @return RowSetDynaClass
   * @exception AppError , Exception
   * @author hreddy
   */
  public ArrayList fetchPartPriceAdjDetails(HashMap hmParams) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    String strQueryList = GmCommonClass.parseNull((String) hmParams.get("SYSPROJLIST"));
    String strAccId = GmCommonClass.parseNull((String) hmParams.get("ACCID"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPE"));
    String strPartNum = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));    
    String strQueryType = GmCommonClass.parseNull((String) hmParams.get("QUERYBYTYPE"));
    
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_rpt.gm_fch_part_price_adj_details", 6);
    gmDBManager.setString(1, strQueryType);
    gmDBManager.setString(2, strQueryList);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strAccId);
    gmDBManager.setString(5, strPartNum);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchPartPriceHistoryDetails - This method is written for fetching the Part Price Adj History
   * Details *
   * 
   * @param HashMap hmParams
   * @return RowSetDynaClass
   * @exception AppError , Exception
   * @author hreddy
   */
  public RowSetDynaClass fetchPartPriceHistoryDetails(HashMap hmParams) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    RowSetDynaClass rdResult = null;
    String strPartyID = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    String strPartNumber = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_rpt.gm_fch_part_price_adj_history", 3);
    gmDBManager.setString(1, strPartyID);
    gmDBManager.setString(2, strPartNumber);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return rdResult;
  }
}
