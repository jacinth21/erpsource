package com.globus.sales.pricing.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPriceApprovalRequestBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmPriceApprovalRequestBean() {
	   super(GmCommonClass.getDefaultGmDataStoreVO());
	 }
	
	 /**
	  * Constructor will populate company info.
	  * 
	  * @param gmDataStore
	  */
	 public GmPriceApprovalRequestBean(GmDataStoreVO gmDataStore) {
	   super(gmDataStore);
	 }  
  /**
   * fetchPriceRequestDetails - This method is written for fetching the Part Price Adj History
   * Details *
   * 
   * @param HashMap hmParams
   * @return HashMap
   * @exception AppError , Exception
   * @author hreddy
   */
  public HashMap fetchPriceRequestDetails(HashMap hmParams) throws AppError, Exception {
    ArrayList alConstructResult = new ArrayList();
    ArrayList alSystemResult = new ArrayList();
    HashMap hmHeaderValues = new HashMap();
    HashMap hmReturn = new HashMap();
    String strPriceReqId = GmCommonClass.parseNull((String) hmParams.get("PRICEREQUESTID"));
    strPriceReqId = strPriceReqId.contains("PR-") ? strPriceReqId : "PR-" + strPriceReqId;
    String strRebatePrice = GmCommonClass.parseNull((String) hmParams.get("REBATE"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_price_req_details", 6);
    gmDBManager.setString(1, strPriceReqId);
    gmDBManager.setString(2, strRebatePrice);
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmHeaderValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    alConstructResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    alSystemResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
    hmReturn.put("HEADERDATA", hmHeaderValues);
    hmReturn.put("PRICEREQDETAILS", alConstructResult);
    hmReturn.put("SYSTEMDETAILS", alSystemResult);
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchPriceRequestDetails - This Method is written for generate Account price report
   * corresponding price request ID. Details *
   * 
   * @param HashMap hmParams
   * @return ArrayList
   * @exception AppError , Exception
   * @author dsandeep
   */
  public ArrayList getGeneratePriceFile(HashMap hmParams) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    String strPriceReqId = GmCommonClass.parseNull((String) hmParams.get("PRICEREQUESTID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_generate_price_file", 2);
    gmDBManager.setString(1, strPriceReqId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * loadAccoutDetails - This Method is written for fetch Account information for corresponding
   * price request ID. Details *
   * 
   * @param String strRequestId
   * @return HashMap
   * @exception AppError , Exception
   * @author Jreddy
   */
  public HashMap loadAccoutDetails(String strRequestId) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_account_info_details", 2);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    HashMap alAccountDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    log.debug("alResult--AccountDetails" + alAccountDetails);
    gmDBManager.close();
    return alAccountDetails;
  }

  /**
   * loadAccoutDetails - This Method is written for fetch Business Questions and Answers for
   * corresponding price request ID. Details *
   * 
   * @param String strRequestId
   * @return HashMap
   * @exception AppError
   * @author Jreddy
   */
  public HashMap fetchQuestionAnswer(String strRequestId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_question_answer", 5);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.execute();
    ArrayList alQuestion = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    ArrayList alAnswer = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    ArrayList alAnswerGroup = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    ArrayList alQuestionAnswer = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();
    HashMap hmResult = new HashMap();
    hmResult.put("QUESTION", alQuestion);
    hmResult.put("ANSWER", alAnswer);
    hmResult.put("ANSWERGROUP", alAnswerGroup);
    hmResult.put("QUESTIONANSWER", alQuestionAnswer);

    return hmResult;
  }

  /**
   * fetchGpbCompareDtls - This Method is written for fetch GPB Comparision details for
   * corresponding price request ID. Details *
   * 
   * @param String strRequestId
   * @return HashMap
   * @exception AppError
   * @author Jreddy
   */
  public HashMap fetchGpbCompareDtls(String strRequestId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_gpb_compare_dtls", 4);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.execute();
    ArrayList alGpbHeader = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    ArrayList alPriceDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    ArrayList alSystemDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    HashMap hmResult = new HashMap();
    hmResult.put("ALGPBHEADER", alGpbHeader);
    hmResult.put("ALPRICEDTLS", alPriceDtls);
    hmResult.put("ALSYSTEMDTLS", alSystemDtls);
    return hmResult;
  }

  /**
   * fetch SystemList - This Method is written for fetch System List for corresponding price request
   * ID. Details *
   * 
   * @param String strRequestId
   * @return HashMap
   * @exception AppError
   * @author Dsandeep
   */
  public HashMap fetchSystemList(String strRequestId) throws AppError {
    String strFinalErrMsg = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_price_mapped_system", 5);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    String strSaveSys = GmCommonClass.parseNull(gmDBManager.getString(4));
    strFinalErrMsg = GmCommonClass.parseNull(gmDBManager.getString(3));
    ArrayList alSystemList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    ArrayList alGpbList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();
    HashMap hmResult = new HashMap();
    hmResult.put("SYSTEMLIST", alSystemList);
    hmResult.put("GPBLIST", alGpbList);
    hmResult.put("SAVESYS", strFinalErrMsg);
    return hmResult;
  }

  /**
   * fetch SystemNames - This Method is written for fetch System Names for Details *
   * 
   * @return ArrayList
   * @exception AppError
   * @author Dsandeep
   */
  public ArrayList fetchSystemNameList() throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_systems_names", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alSystemNameList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alSystemNameList;
  }
  public String getidn(String strAccid) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager();
	    gmDBManager.setFunctionString("get_idn_no", 1);
	    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	    gmDBManager.setString(2, strAccid);
	    gmDBManager.execute();
	    String stridn = gmDBManager.getString(1);
	    gmDBManager.close();
	    return stridn;
	   
	  }
  /**
   * fetch addReleasePRTSystem - This Method is Used to released/unreleased System
   * 
   * @return String
   * @exception AppError
   */
  public String addReleasePRTSystem(HashMap hmParams) throws AppError{
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strSysId = GmCommonClass.parseNull((String) hmParams.get("SYSTEMID"));
	  String strSegId = GmCommonClass.parseNull((String) hmParams.get("SEGMENTID"));
	  String strReleaseFl = GmCommonClass.parseNull((String) hmParams.get("RELEASEFL"));
	  String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
	  gmDBManager.setPrepareString("gm_pkg_sm_pricparam_setup.gm_prt_add_release_system", 4);
	  gmDBManager.setString(1, strSysId);
	  gmDBManager.setString(2, strSegId);
	  gmDBManager.setString(3, strReleaseFl);
	  gmDBManager.setString(4, strUserId);
	  gmDBManager.execute();
	  gmDBManager.commit();
	  return "success";
  }
  
  public ArrayList loadSegmentIDList() throws AppError{
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  gmDBManager.setPrepareString("gm_pkg_sm_pricparam_setup.gm_fch_segment_names", 1);
	  gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	  gmDBManager.execute();
	  ArrayList alSegNameList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
	  gmDBManager.close();
	  return alSegNameList;
  }

}
