/**
 * 
 */
package com.globus.sales.pricing.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author tramasamy
 *PC-5504  -Revert Account or GPB Pricing 
 */
public class GmAccountPriceReportBean extends GmBean{
	
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	public GmAccountPriceReportBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**savePartpricing - this method for to revert the part price from current price to previous price
	 * 
	 * @param hmParams
	 * @throws AppError
	 */
	public void savePartpricing(HashMap hmParams) throws AppError {

		String strInputStr = GmCommonClass.parseNull((String) hmParams.get("INPUTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		strUserId = strUserId.equals("") ? getGmDataStoreVO().getUserid(): strUserId;
		log.debug("hmParams is " + hmParams);
		log.debug("strUserId is " + strUserId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_revert_account_part_pricing.gm_upd_part_pricing", 2);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}

	/**voidPartPricing - This method used to void the selected part price. 
	 * 
	 * @param hmValues
	 * @throws AppError
	 */
	public void voidPartPricing(HashMap hmValues) throws AppError {
		String strInputStr = GmCommonClass.parseNull((String) hmValues.get("INPUTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmValues.get("USERID"));
		strUserId = strUserId.equals("") ? getGmDataStoreVO().getUserid(): strUserId;
		log.debug("hmValues is " + hmValues);
		log.debug("strUserId is " + strUserId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_revert_account_part_pricing.gm_void_part_pricing", 2);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

	}

}
