/**
 * FileName : GmPriceRequestTransBean.java Description : for saving the details Author : hreddy Date
 * * : Copyright : Globus Medical Inc
 */
package com.globus.sales.pricing.beans;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.util.jobs.GmJob;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.pricing.GmImpactAnalysisVO;

public class GmPriceRequestTransBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  
//this will be removed all place changed with GmDataStoreVO constructor
	
	 public GmPriceRequestTransBean() {
	   super(GmCommonClass.getDefaultGmDataStoreVO());
	 }
	
	 /**
	  * Constructor will populate company info.
	  * 
	  * @param gmDataStore
	  */
	 public GmPriceRequestTransBean(GmDataStoreVO gmDataStore) {
	   super(gmDataStore);
	 }  

  /**
   * @param gmPricingReqDetailsVO
   * @throws Exception
   */
  public String saveOrUpdatePricingDetails(HashMap<String, String> hmParams) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
	HashMap hmParam = new HashMap();
	HashMap hmReturn = new HashMap(); 
	String strReqId = "";
	String strConsumerClass = GmCommonClass.parseNull(GmCommonClass.getString("NOTIFICATION_CONSUMER_CLASS"));	
	String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_NOTIFICATION_LOC_QUEUE"));
    String strSavedPRTID = "";
    log.debug(":::ClassName::saveOrUpdatePricingDetails:::--entering:");
    String strJSONInput = GmCommonClass.parseNull(hmParams.get("JSONDATA"));
    String strPriceReqID = GmCommonClass.parseNull(hmParams.get("PRCREQID"));
    String strPrcReqStatus = GmCommonClass.parseNull(hmParams.get("PRCREQUSTUS"));
    String strAccessLvl = GmCommonClass.parseNull(hmParams.get("ACCESSLVL"));
    String strUserID = GmCommonClass.parseNull(hmParams.get("USERID"));
    String strSystemsSlected = GmCommonClass.parseNull(hmParams.get("SELECTSYSTEMS"));
    log.debug("saveOrUpdatePricingDetails hmParams :: " + hmParams);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_txn.gm_sav_update_price_req_main", 6);
    gmDBManager.setString(1, strJSONInput);
    gmDBManager.setString(2, strPriceReqID);
    gmDBManager.setString(3, strPrcReqStatus);
    gmDBManager.setString(4, strUserID);
    gmDBManager.setString(5, strSystemsSlected);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strSavedPRTID = GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.commit();

    /* IMPACT ANLAYSIS - Checking if proposed price is updated & creating impact analysis(PRT-R203) */
    GmImpactAnalysisVO gmImpactAnalysisVO = new GmImpactAnalysisVO();
    GmImpactAnalysisBean gmImpactAnalysisBean = new GmImpactAnalysisBean(gmImpactAnalysisVO);
    log.debug("Checking PriceUpdateFlag in GmPriceRequestResource------------->" + strSavedPRTID);
    String[] strReqData = strSavedPRTID.split("@");
    if(strReqData.length > 0){
    strReqId = strReqData[0];
    strPrcReqStatus = strReqData[1];
    }
    log.debug("Checking PriceUpdateFlag for ------------->" + strReqId+" -------status"+strPrcReqStatus+" strAccessLvl"+strAccessLvl);
    HashMap hmAnlysis = new HashMap();
    hmAnlysis.put("PRICEREQUESTID", strReqId);
    hmAnlysis.put("COMPANYINFO", "");
    hmAnlysis.put("USERID", strUserID);
    gmImpactAnalysisBean.checkPriceUpdateFlag(hmAnlysis);

    // saleRep send push notification to AD for price request approval
    //52187 pending ad approval    
    if ((strAccessLvl.equals("1") || strAccessLvl.equals("2")) && strPrcReqStatus.equals("52187")) {
      hmReturn	= GmCommonClass.parseNullHashMap(sendReqApprovalPushNotification(strReqId));
      if(hmReturn.size() > 0){
      hmReturn.put("NOTIFICATION", "PRICING");
      GmQueueProducer qprod = new GmQueueProducer();
      GmMessageTransferObject tf = new GmMessageTransferObject();
      tf.setMessageObject(hmReturn);
      tf.setConsumerClass(strConsumerClass);
      qprod.sendMessage(tf, strQueueName);
      }
    }
    log.debug(":::ClassName::saveOrUpdatePricingDetails:::--exiting:" +strAccessLvl);
    return strSavedPRTID;

  } 

  /**
   * @param strRequestId
   * @author Jreddy Sending mail with price request details to AD, VP and SalesRep
   */
  public void sendReqApprovalEmaillNotification(String strRequestId) throws AppError {
    log.debug("sendReqApprovalEmaillNotification strRequestId = " + strRequestId);
    HashMap hmReturn = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmParam = new HashMap();
    //String getCimpId = "1000";
    String strCompanyId = getCompId();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());     
    String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    
      gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_request_dtls", 8);
      gmDBManager.setString(1, strRequestId);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
      gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
      gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
      gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
      gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
      gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
      gmDBManager.execute();

      hmParams = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
      String strRepMail = GmCommonClass.parseNull(gmDBManager.getString(3));
      String strAdMail = GmCommonClass.parseNull(gmDBManager.getString(4));
      String strVpMail = GmCommonClass.parseNull(gmDBManager.getString(5));
      // get the AD department and user id for push notification t9150.c9150_device_token for the
      // AD's user id
      String strDeviceId = GmCommonClass.parseNull(gmDBManager.getString(6));
      String strReqStatus = GmCommonClass.parseNull(gmDBManager.getString(7));
      String strCCEmail = GmCommonClass.parseNull(gmDBManager.getString(8));
      gmDBManager.close();
      if (strReqStatus.equals("52187")) //52187 pending ad approval
      {      
      GmEmailProperties emailProps = new GmEmailProperties();
      emailProps.setRecipients(strAdMail);
      emailProps.setSender(gmResourceBundleBean.getProperty("GmRecievedRequest."
          + GmEmailProperties.FROM));
      emailProps.setCc(strRepMail + ',' + strVpMail+','+strCCEmail);
      emailProps.setMimeType(gmResourceBundleBean.getProperty("GmRecievedRequest."
          + GmEmailProperties.MIME_TYPE));

      String strSubject = "Pricing Request Approval - " + strRequestId;
      String strHeader = "The following pricing request has been waiting for approval";

      emailProps.setSubject(strSubject);

      hmParams.put("REQID", strRequestId);
      hmParams.put("THEADER", strHeader);

      GmJasperMail jasperMail = new GmJasperMail();
      jasperMail.setJasperReportName("/GmPriceRequestApproval.jasper");
      jasperMail.setAdditionalParams(hmParams);
      jasperMail.setEmailProperties(emailProps);

      hmReturn = jasperMail.sendMail();	      
      }  
   
  }
  /**
   * @param strRequestId
   * @author Aprasath Sending push with price request details to AD IPAD
   */
  public HashMap sendReqApprovalPushNotification(String strRequestId) throws AppError {
    log.debug("sendReqApprovalPushNotification strRequestId = " + strRequestId);
    HashMap hmReturn = new HashMap();   
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());     
    String strPriceNotify =
            GmCommonClass.parseNull(GmCommonClass.getString("PRICEAZURENOTIFICATION"));  
      gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_req_notification_dtls", 2);
      gmDBManager.setString(1, strRequestId);
      gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
      gmDBManager.execute();      
      // get the AD department and user id for push notification t9150.c9150_device_token for the
      // AD's user id
      String strDeviceId = GmCommonClass.parseNull(gmDBManager.getString(2));
      gmDBManager.close();
      
      if (strPriceNotify.equalsIgnoreCase("YES") && !strDeviceId.equals("")) 
      {
    	// To send push notification while submitting the pricing request by sales rep	    
	    	  hmReturn.put("DEVICEID", strDeviceId);
	    	  hmReturn.put("MESSAGE", "Can you change phrase Pricing Request " + strRequestId
	            + " has been submitted and is awaiting approval.");    	  
      }
      return hmReturn;
  }

  /**
   * genereteHeldOrderPriceReq :to genereate the held order Price requst
   * @param hmParams
   * @author karthik 
   */
  public String genereteHeldOrderPriceReq(HashMap<String, String> hmParams) throws AppError,
      JsonParseException, JsonMappingException, IOException, Exception {
	HashMap hmParam = new HashMap();
	HashMap hmReturn = new HashMap(); 
	String strReqId = "";
    String strSavedPRTID = "";
    String strOrderSelected = GmCommonClass.parseNull(hmParams.get("ORDERIDLIST"));
    String struserId = GmCommonClass.parseNull(hmParams.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_txn.gm_sav_heldorder_price_req", 3);
    gmDBManager.setString(1, strOrderSelected);
    gmDBManager.setString(2, struserId);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strSavedPRTID = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.commit();

    return strSavedPRTID;

  } 
  
  /**
   * @param priceRequestId
   * @return
   * @throws AppError
   * @author karthik 
   * 
   * fetchHeldOrderPriceRequestDetails : This method is used to fetch theprice request held order details 
   */
  public HashMap fetchHeldOrderPriceRequestDetails(HashMap hmParams) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_order_price_req", 2);

    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("ORDERID")));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    hmReturn.put("HELDORDERDETAILS", alResult);
    return hmReturn;
  }
}
