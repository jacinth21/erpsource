package com.globus.sales.pricing.beans;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmPricingFileGenBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	public HashMap fetchPriceInfo(String strAcid,String strUserId) throws AppError{

		String strId =GmCommonClass.parseNull(strAcid);
		ArrayList alPriceInfoList = new ArrayList();
		ArrayList alPriceHeadList = new ArrayList();
		HashMap hmReturn = new HashMap();
		
		GmDBManager gmDBManager = new GmDBManager ();
		gmDBManager.setPrepareString("gm_pkg_sm_price.gm_fch_price_info",4);
		gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
		gmDBManager.setString(1, strId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		alPriceInfoList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
		alPriceHeadList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
		gmDBManager.commit();
		
		hmReturn.put("HEAD", alPriceHeadList);
		hmReturn.put("DETAIL", alPriceInfoList);
		
		log.debug("DETAIL:"+alPriceInfoList.size()) ;
		log.debug("HEAD::"+alPriceHeadList.size()) ;   
		return hmReturn;

	}
	
	public ArrayList fetchAllPriceIncAccids() throws AppError{

		
		ArrayList alAccList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager ();
		gmDBManager.setPrepareString("gm_pkg_sm_price.gm_fch_price_info_acids",1);
		gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
		gmDBManager.execute();
		alAccList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
		gmDBManager.commit();

		log.debug("alAccList:"+alAccList.size()) ;   
		return alAccList;

	}


		

}
