package com.globus.sales.pricing.forms;

import java.util.ArrayList;


import com.globus.common.forms.GmCommonForm;
import com.globus.common.beans.GmCalenderOperations;

public class GmGroupPartPricingForm extends GmCommonForm
{
	private String EMPTY_STRING = "";
	private String pricedTypeId = "0";
	private String systemListId = "0";
	private String groupId = "0";
	private String groupTypeId = "0";
	private String hsearch = "0";
	private String comments = "";
	private String strErrorGrpIds = "";
	private String strNoData = "";
	
    private String hinputStringPart = EMPTY_STRING;
    private String hinputStringGroup = EMPTY_STRING;
    
    private ArrayList alSystemList = new ArrayList();
    private ArrayList alPricedTypeList = null;
    private ArrayList alGroupList = new ArrayList();
    private ArrayList alPartList = new ArrayList(); 
    private ArrayList alGroupTypeList = new ArrayList();
    private ArrayList alStatusList = new ArrayList();
    
    private String gridXmlGroupData = EMPTY_STRING;
    private String gridXmlSysData = EMPTY_STRING;
    private String gridXmlData = EMPTY_STRING;
    private String gridXmlGroupTypeData = EMPTY_STRING;

    private String partNumbers = EMPTY_STRING;
    private String enableGrpType = EMPTY_STRING;
    private String advpViewAccess = EMPTY_STRING;
    private String statusList = EMPTY_STRING;
    
    private String hsysID="";
    
    
	public String getGridXmlGroupData() {
		return gridXmlGroupData;
	}

	public void setGridXmlGroupData(String gridXmlGroupData) {
		this.gridXmlGroupData = gridXmlGroupData;
	}


	public String getSystemListId() {
		return systemListId;
	}

	public void setSystemListId(String systemListId) {
		this.systemListId = systemListId;
	}


	public ArrayList getAlSystemList() {
		return alSystemList;
	}

	public String getHinputStringPart() {
		return hinputStringPart;
	}

	public void setHinputStringPart(String hinputStringPart) {
		this.hinputStringPart = hinputStringPart;
	}

	public String getHinputStringGroup() {
		return hinputStringGroup;
	}

	public void setHinputStringGroup(String hinputStringGroup) {
		this.hinputStringGroup = hinputStringGroup;
	}

	public void setAlSystemList(ArrayList alSystemList) {
		this.alSystemList = alSystemList;
	}


	public String getPricedTypeId() {
		return pricedTypeId;
	}

	public void setPricedTypeId(String pricedTypeId) {
		this.pricedTypeId = pricedTypeId;
	}

	public ArrayList getAlPricedTypeList() {
		return alPricedTypeList;
	}

	public void setAlPricedTypeList(ArrayList alPricedTypeList) {
		this.alPricedTypeList = alPricedTypeList;
	}

	public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	public ArrayList getAlGroupList() {
		return alGroupList;
	}

	public void setAlGroupList(ArrayList alGroupList) {
		this.alGroupList = alGroupList;
	}

	public ArrayList getAlPartList() {
		return alPartList;
	}

	public void setAlPartList(ArrayList alPartList) {
		this.alPartList = alPartList;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getPartNumbers() {
		return partNumbers;
	}

	public void setPartNumbers(String partNumbers) {
		this.partNumbers = partNumbers;
	}

	public String getGridXmlSysData() {
		return gridXmlSysData;
	}

	public void setGridXmlSysData(String gridXmlSysData) {
		this.gridXmlSysData = gridXmlSysData;
	}

	/**
	 * @return the enableGrpType
	 */
	public String getEnableGrpType() {
		return enableGrpType;
	}

	/**
	 * @param enableGrpType the enableGrpType to set
	 */
	public void setEnableGrpType(String enableGrpType) {
		this.enableGrpType = enableGrpType;
	}

	public ArrayList getAlGroupTypeList() {
		return alGroupTypeList;
	}

	public void setAlGroupTypeList(ArrayList alGroupTypeList) {
		this.alGroupTypeList = alGroupTypeList;
	}

	public String getGridXmlGroupTypeData() {
		return gridXmlGroupTypeData;
	}

	public void setGridXmlGroupTypeData(String gridXmlGroupTypeData) {
		this.gridXmlGroupTypeData = gridXmlGroupTypeData;
	}

	public String getAdvpViewAccess() {
		return advpViewAccess;
	}

	public void setAdvpViewAccess(String advpViewAccess) {
		this.advpViewAccess = advpViewAccess;
	}


	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}
	public String getStatusList() {
		return statusList;
	}

	public void setStatusList(String StatusList) {
		this.statusList = StatusList;
	}

	/**
	 * @return the groupTypeId
	 */
	public String getGroupTypeId() {
		return groupTypeId;
	}

	/**
	 * @param groupTypeId the groupTypeId to set
	 */
	public void setGroupTypeId(String groupTypeId) {
		this.groupTypeId = groupTypeId;
	}

	/**
	 * @return the hsearch
	 */
	public String getHsearch() {
		return hsearch;
	}

	/**
	 * @param hsearch the hsearch to set
	 */
	public void setHsearch(String hsearch) {
		this.hsearch = hsearch;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the strErrorGrpIds
	 */
	public String getStrErrorGrpIds() {
		return strErrorGrpIds;
	}

	/**
	 * @param strErrorGrpIds the strErrorGrpIds to set
	 */
	public void setStrErrorGrpIds(String strErrorGrpIds) {
		this.strErrorGrpIds = strErrorGrpIds;
	}

	/**
	 * @return the strNoData
	 */
	public String getStrNoData() {
		return strNoData;
	}

	/**
	 * @param strNoData the strNoData to set
	 */
	public void setStrNoData(String strNoData) {
		this.strNoData = strNoData;
	}

	public String getHsysID() {
		return hsysID;
	}

	public void setHsysID(String hsysID) {
		this.hsysID = hsysID;
	}
	
	
}

