package com.globus.sales.pricing.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmQuestionAnswerForm;

public class GmPricingRequestForm extends GmQuestionAnswerForm{
	
	private String EMPTY_STRING = "";
	
	private String strRequestId = EMPTY_STRING;
	private String strAccountId = EMPTY_STRING;
	private String strAccountName = EMPTY_STRING;
    private String strStatusName = EMPTY_STRING;
    private String effectiveDate = EMPTY_STRING;
    private String strContractFl = EMPTY_STRING;

    //Account Affiliation
    private String strGprAff = EMPTY_STRING;
    private String strHlcAff = EMPTY_STRING;
    
    private String strRegName = EMPTY_STRING; 
    private String strTerName = EMPTY_STRING;
    private String strGpName  = EMPTY_STRING;
    private String strAdName = EMPTY_STRING;
    private String strDName = EMPTY_STRING;
    private String strVpName = EMPTY_STRING;
    private String strRepName = EMPTY_STRING;
    
    private String strApprovedBy = EMPTY_STRING;
    private String strInitiateDate = EMPTY_STRING;
    private String strInitiateById = EMPTY_STRING;
    private String strInitiateByName = EMPTY_STRING;
    
    private String hstatusId = EMPTY_STRING;
    private String hinputStr = EMPTY_STRING;
   	private String hreqid = EMPTY_STRING;
	private String hDeniedinputStr = EMPTY_STRING;

    private String hviewType = EMPTY_STRING;
    private String fchAllSysFl = EMPTY_STRING;
    private String sysTranId = EMPTY_STRING;
    private String statusHistoryFl = EMPTY_STRING;
    private String sysGrpId = EMPTY_STRING;
    private String errorMsg = EMPTY_STRING;
    private String strDispFlg = EMPTY_STRING;
    private String pricingRequestRules = EMPTY_STRING;
    private String strRemoveSysIds = EMPTY_STRING;
   
    
    public String getStrGprAff() {
		return strGprAff;
	}

	public void setStrGprAff(String strGprAff) {
		this.strGprAff = strGprAff;
	}

	public String getStrHlcAff() {
		return strHlcAff;
	}

	public void setStrHlcAff(String strHlcAff) {
		this.strHlcAff = strHlcAff;
	}

	public String getStrRemoveSysIds() {
		return strRemoveSysIds;
	}

	public void setStrRemoveSysIds(String strRemoveSysIds) {
		this.strRemoveSysIds = strRemoveSysIds;
	}
	/**
	 * @return the apprlvl
	 */
	public String getApprlvl() {
		return apprlvl;
	}

	/**
	 * @param apprlvl the apprlvl to set
	 */
	public void setApprlvl(String apprlvl) {
		this.apprlvl = apprlvl;
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	private String apprlvl = EMPTY_STRING;

    private String apprLvl_fl = EMPTY_STRING;
    
    private String[] checkSystems = new String [10];
    
    private ArrayList alSystemList = new ArrayList();
    private ArrayList alAccountList = null;
    private ArrayList alCopyAccountList = null;
    private ArrayList alGroupList = null;
    private ArrayList alAccountType = null;
    
    private String gridXmlGroupData = EMPTY_STRING;
    private String gridXmlConstructData = EMPTY_STRING;
    private String gridXmlAnalysisData = EMPTY_STRING;
    private String gridXmlWorkflowData = EMPTY_STRING;
    private String gridXmlAnalysisView = EMPTY_STRING;
    private String gridXmlActivityData = EMPTY_STRING;
    private String pendingSystemAccountPriceGridXML = EMPTY_STRING;
	private String pendingGroupAccountPriceGridXML = EMPTY_STRING;
	private String pendingSystemAccountCount = EMPTY_STRING;
	private String pendingGroupAccountCount = EMPTY_STRING;
    
	/**
	 * @return the gridXmlAnalysisView
	 */
	public String getGridXmlAnalysisView() {
		return gridXmlAnalysisView;
	}

	/**
	 * @param gridXmlAnalysisView the gridXmlAnalysisView to set
	 */
	public void setGridXmlAnalysisView(String gridXmlAnalysisView) {
		this.gridXmlAnalysisView = gridXmlAnalysisView;
	}

	private String gridXmlQnAData = EMPTY_STRING;
    private String flgReqId=EMPTY_STRING;
    private String strAccType=EMPTY_STRING;
    private String strGroupAcc=EMPTY_STRING;
    private String strGroupName=EMPTY_STRING;
    private String strPartFlag = EMPTY_STRING;
    private String strAccGPO = EMPTY_STRING;
    private String strPublOvrRide = EMPTY_STRING;
    
	public String getStrPublOvrRide() {
		return strPublOvrRide;
	}

	public void setStrPublOvrRide(String strPublOvrRide) {
		this.strPublOvrRide = strPublOvrRide;
	}

	/**
	 * @return the hDeniedinputStr
	 */
	public String gethDeniedinputStr() {
		return hDeniedinputStr;
	}

	/**
	 * @return the sysGrpId
	 */
	public String getSysGrpId() {
		return sysGrpId;
	}

	/**
	 * @param sysGrpId the sysGrpId to set
	 */
	public void setSysGrpId(String sysGrpId) {
		this.sysGrpId = sysGrpId;
	}

	/**
	 * @param hDeniedinputStr the hDeniedinputStr to set
	 */
	public void sethDeniedinputStr(String hDeniedinputStr) {
		this.hDeniedinputStr = hDeniedinputStr;
	}

	/**
	 * @return the strContractFl
	 */
	public String getStrContractFl() {
		return strContractFl;
	}

	/**
	 * @param strContractFl the strContractFl to set
	 */
	public void setStrContractFl(String strContractFl) {
		this.strContractFl = strContractFl;
	}

	/**
	 * @return the strRequestId
	 */
	public String getStrRequestId() {
		return strRequestId;
	}

	/**
	 * @param strRequestId the strRequestId to set
	 */
	public void setStrRequestId(String strRequestId) {
		this.strRequestId = strRequestId;
	}

	/**
	 * @return the strAccountId
	 */
	public String getStrAccountId() {
		return strAccountId;
	}

	/**
	 * @param strAccountId the strAccountId to set
	 */
	public void setStrAccountId(String strAccountId) {
		this.strAccountId = strAccountId;
	}


	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	/**
	 * @return the strApprovedBy
	 */
	public String getStrApprovedBy() {
		return strApprovedBy;
	}

	/**
	 * @param strApprovedBy the strApprovedBy to set
	 */
	public void setStrApprovedBy(String strApprovedBy) {
		this.strApprovedBy = strApprovedBy;
	}

	/**
	 * @return the strInitiateDate
	 */
	public String getStrInitiateDate() {
		return strInitiateDate;
	}

	/**
	 * @param strInitiateDate the strInitiateDate to set
	 */
	public void setStrInitiateDate(String strInitiateDate) {
		this.strInitiateDate = strInitiateDate;
	}


	/**
	 * @return the checkSystems
	 */
	public String[] getCheckSystems() {
		return checkSystems;
	}

	/**
	 * @param checkSystems the checkSystems to set
	 */
	public void setCheckSystems(String[] checkSystems) {
		this.checkSystems = checkSystems;
	}

	/**
	 * @return the alSystemList
	 */
	public ArrayList getAlSystemList() {
		return alSystemList;
	}

	/**
	 * @param alSystemList the alSystemList to set
	 */
	public void setAlSystemList(ArrayList alSystemList) {
		this.alSystemList = alSystemList;
	}

	public String getStrRegName() {
		return strRegName;
	}

	public void setStrRegName(String strRegName) {
		this.strRegName = strRegName;
	}

	public String getStrTerName() {
		return strTerName;
	}

	public void setStrTerName(String strTerName) {
		this.strTerName = strTerName;
	}

	public String getStrGpName() {
		return strGpName;
	}

	public void setStrGpName(String strGpName) {
		this.strGpName = strGpName;
	}

	public String getStrAdName() {
		return strAdName;
	}

	public void setStrAdName(String strAdName) {
		this.strAdName = strAdName;
	}

	public String getStrDName() {
		return strDName;
	}

	public void setStrDName(String strDName) {
		this.strDName = strDName;
	}

	public String getStrVpName() {
		return strVpName;
	}

	public void setStrVpName(String strVpName) {
		this.strVpName = strVpName;
	}

	public String getStrRepName() {
		return strRepName;
	}

	public void setStrRepName(String strRepName) {
		this.strRepName = strRepName;
	}

	/**
	 * @return the alAccountList
	 */
	public ArrayList getAlAccountList() {
		return alAccountList;
	}

	/**
	 * @param alAccountList the alAccountList to set
	 */
	public void setAlAccountList(ArrayList alAccountList) {
		this.alAccountList = alAccountList;
	}

	/**
	 * @return the alCopyAccountList
	 */
	public ArrayList getAlCopyAccountList() {
		return alCopyAccountList;
	}

	/**
	 * @param alCopyAccountList the alCopyAccountList to set
	 */
	public void setAlCopyAccountList(ArrayList alCopyAccountList) {
		this.alCopyAccountList = alCopyAccountList;
	}

	/**
	 * @return the alGroupList
	 */
	public ArrayList getAlGroupList() {
		return alGroupList;
	}

	/**
	 * @param alGroupList the alGroupList to set
	 */
	public void setAlGroupList(ArrayList alGroupList) {
		this.alGroupList = alGroupList;
	}

	/**
	 * @return the gridXmlGroupData
	 */
	public String getGridXmlGroupData() {
		return gridXmlGroupData;
	}

	/**
	 * @param gridXmlGroupData the gridXmlGroupData to set
	 */
	public void setGridXmlGroupData(String gridXmlGroupData) {
		this.gridXmlGroupData = gridXmlGroupData;
	}

	/**
	 * @return the gridXmlConstructData
	 */
	public String getGridXmlConstructData() {
		return gridXmlConstructData;
	}

	/**
	 * @param gridXmlConstructData the gridXmlConstructData to set
	 */
	public void setGridXmlConstructData(String gridXmlConstructData) {
		this.gridXmlConstructData = gridXmlConstructData;
	}

	/**
	 * @return the gridXmlAnalysisData
	 */
	public String getGridXmlAnalysisData() {
		return gridXmlAnalysisData;
	}

	/**
	 * @param gridXmlAnalysisData the gridXmlAnalysisData to set
	 */
	public void setGridXmlAnalysisData(String gridXmlAnalysisData) {
		this.gridXmlAnalysisData = gridXmlAnalysisData;
	}

	/**
	 * @return the gridXmlWorkflowData
	 */
	public String getGridXmlWorkflowData() {
		return gridXmlWorkflowData;
	}

	/**
	 * @param gridXmlWorkflowData the gridXmlWorkflowData to set
	 */
	public void setGridXmlWorkflowData(String gridXmlWorkflowData) {
		this.gridXmlWorkflowData = gridXmlWorkflowData;
	}

	/**
	 * @return the gridXmlQnAData
	 */
	public String getGridXmlQnAData() {
		return gridXmlQnAData;
	}

	/**
	 * @param gridXmlQnAData the gridXmlQnAData to set
	 */
	public void setGridXmlQnAData(String gridXmlQnAData) {
		this.gridXmlQnAData = gridXmlQnAData;
	}

	public String getHinputStr() {
		return hinputStr;
	}

	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}


	public String getStrStatusName() {
		return strStatusName;
	}

	public void setStrStatusName(String strStatusName) {
		this.strStatusName = strStatusName;
	}

	public String getHreqid() {
		return hreqid;
	}

	public void setHreqid(String hreqid) {
		this.hreqid = hreqid;
	}

	public String getHstatusId() {
		return hstatusId;
	}

	public void setHstatusId(String hstatusId) {
		this.hstatusId = hstatusId;
	}

	public String getStrAccountName() {
		return strAccountName;
	}

	public void setStrAccountName(String strAccountName) {
		this.strAccountName = strAccountName;
	}

	public String getStrInitiateById() {
		return strInitiateById;
	}

	public void setStrInitiateById(String strInitiateById) {
		this.strInitiateById = strInitiateById;
	}

	public String getHviewType() {
		return hviewType;
	}

	public void setHviewType(String hviewType) {
		this.hviewType = hviewType;
	}

	public String getStatusHistoryFl() {
		return statusHistoryFl;
	}

	public void setStatusHistoryFl(String statusHistoryFl) {
		this.statusHistoryFl = statusHistoryFl;
	}

	public String getStrInitiateByName() {
		return strInitiateByName;
	}

	public void setStrInitiateByName(String strInitiateByName) {
		this.strInitiateByName = strInitiateByName;
	}

	/**
	 * @return the flgReqId
	 */
	public String getFlgReqId() {
		return flgReqId;
	}

	/**
	 * @param flgReqId the flgReqId to set
	 */
	public void setFlgReqId(String flgReqId) {
		this.flgReqId = flgReqId;
	}

	/**
	 * @return the fchAllSysFl
	 */
	public String getFchAllSysFl() {
		return fchAllSysFl;
	}

	/**
	 * @param fchAllSysFl the fchAllSysFl to set
	 */
	public void setFchAllSysFl(String fchAllSysFl) {
		this.fchAllSysFl = fchAllSysFl;
	}

	/**
	 * @return the sysTranId
	 */
	public String getSysTranId() {
		return sysTranId;
	}

	/**
	 * @param sysTranId the sysTranId to set
	 */
	public void setSysTranId(String sysTranId) {
		this.sysTranId = sysTranId;
	}


	/**
	 * @return the strAccType
	 */
	public String getStrAccType() {
		return strAccType;
	}

	/**
	 * @param strAccType the strAccType to set
	 */
	public void setStrAccType(String strAccType) {
		this.strAccType = strAccType;
	}

	/**
	 * @return the strGroupAcc
	 */
	public String getStrGroupAcc() {
		return strGroupAcc;
	}

	/**
	 * @param strGroupAcc the strGroupAcc to set
	 */
	public void setStrGroupAcc(String strGroupAcc) {
		this.strGroupAcc = strGroupAcc;
	}

	public String getStrGroupName() {
		return strGroupName;
	}

	public void setStrGroupName(String strGroupName) {
		this.strGroupName = strGroupName;
	}

	/**
	 * @return the alAccountType
	 */
	public ArrayList getAlAccountType() {
		return alAccountType;
	}

	/**
	 * @param alAccountType the alAccountType to set
	 */
	public void setAlAccountType(ArrayList alAccountType) {
		this.alAccountType = alAccountType;
	}


	public String getGridXmlActivityData() {
		return gridXmlActivityData;
	}

	public void setGridXmlActivityData(String gridXmlActivityData) {
		this.gridXmlActivityData = gridXmlActivityData;
	}

	public String getPendingSystemAccountPriceGridXML() {
		return pendingSystemAccountPriceGridXML;
	}

	public void setPendingSystemAccountPriceGridXML(
			String pendingSystemAccountPriceGridXML) {
		this.pendingSystemAccountPriceGridXML = pendingSystemAccountPriceGridXML;
	}

	public String getPendingGroupAccountPriceGridXML() {
		return pendingGroupAccountPriceGridXML;
	}

	public void setPendingGroupAccountPriceGridXML(
			String pendingGroupAccountPriceGridXML) {
		this.pendingGroupAccountPriceGridXML = pendingGroupAccountPriceGridXML;
	}

	public String getPendingSystemAccountCount() {
		return pendingSystemAccountCount;
	}

	public void setPendingSystemAccountCount(String pendingSystemAccountCount) {
		this.pendingSystemAccountCount = pendingSystemAccountCount;
	}

	public String getPendingGroupAccountCount() {
		return pendingGroupAccountCount;
	}

	public void setPendingGroupAccountCount(String pendingGroupAccountCount) {
		this.pendingGroupAccountCount = pendingGroupAccountCount;
	}

	public String getStrPartFlag() {
		return strPartFlag;
	}

	public void setStrPartFlag(String strPartFlag) {
		this.strPartFlag = strPartFlag;
	}

	public String getStrAccGPO() {
		return strAccGPO;
	}

	public void setStrAccGPO(String strAccGPO) {
		this.strAccGPO = strAccGPO;
	}
	
	public String getStrDispFlg() {
		return strDispFlg;
	}

	public void setStrDispFlg(String strDispFlg) {
		this.strDispFlg = strDispFlg;
	}

	public String getPricingRequestRules() {
		return pricingRequestRules;
	}

	public void setPricingRequestRules(String pricingRequestRules) {
		this.pricingRequestRules = pricingRequestRules;
	}	
    

}
