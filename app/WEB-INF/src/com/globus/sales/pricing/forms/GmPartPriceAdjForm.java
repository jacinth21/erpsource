/**
 * 
 */
package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmLogForm;

/**
 * @author arajan
 * 
 */
public class GmPartPriceAdjForm extends GmLogForm {

  private String strXMLGrid = "";
  private String partNum = "";
  private String type = "";
  private String accName = "";
  private String grpAccName = "";
  private String accId = "";
  private String status = "";
  private Date impDate = null;
  private String rgname = "";
  private String txnId = "";
  private String contract = "";
  private String trname = "";
  private String initiatedBy = "";
  private String gpraff = "";
  private String adname = "";
  private Date initDate = null;
  private String prName = "";
  private String rpname = "";
  private String implementedBy = "";
  private String prid = "";
  private String hlcaff = "";
  private String dname = "";
  private String hInputString = "";
  private String hRemString = "";
  private String hSaveAction = "";
  private String partyId = "";
  private String hTxnId = "";
  private String hDisplayNm = "";
  private String hRedirectURL = "";
  private String adjCode = "";
  private String adjValue = "";
  private String statusid = "";
  private String emptyAcc = "";
  // private String deniedComments = "";
  private String successMessage = "";
  private String submitAccess = "";

  private ArrayList alAdjCodeLst = new ArrayList();
  private ArrayList alType = new ArrayList();
  private ArrayList alAccName = new ArrayList();
  private ArrayList alGrpAccName = new ArrayList();
  private ArrayList alAdjDetails = new ArrayList();
  private ArrayList alEmptyAcc = new ArrayList();
  private ArrayList alLogReasons = new ArrayList();

  // For Dashboard
  private String initBy = "";
  private String dateFld = "";
  private String startDt = "";
  private String endDt = "";
  private String actions = "";
  private String hStatus = "";
  private String hAccIDs = "";

  private ArrayList alInitBy = new ArrayList();
  private ArrayList alDateFld = new ArrayList();
  private ArrayList alStatus = new ArrayList();
  private ArrayList alActions = new ArrayList();

  
  
  
  /**
 * @return the impDate
 */
public Date getImpDate() {
	return impDate;
}

/**
 * @param impDate the impDate to set
 */
public void setImpDate(Date impDate) {
	this.impDate = impDate;
}

/**
 * @return the initDate
 */
public Date getInitDate() {
	return initDate;
}

/**
 * @param initDate the initDate to set
 */
public void setInitDate(Date initDate) {
	this.initDate = initDate;
}

/**
   * @return the strXMLGrid
   */
  public String getStrXMLGrid() {
    return strXMLGrid;
  }

  /**
   * @param strXMLGrid the strXMLGrid to set
   */
  public void setStrXMLGrid(String strXMLGrid) {
    this.strXMLGrid = strXMLGrid;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the alAdjCodeLst
   */
  public ArrayList getAlAdjCodeLst() {
    return alAdjCodeLst;
  }

  /**
   * @param alAdjCodeLst the alAdjCodeLst to set
   */
  public void setAlAdjCodeLst(ArrayList alAdjCodeLst) {
    this.alAdjCodeLst = alAdjCodeLst;
  }

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the accName
   */
  public String getAccName() {
    return accName;
  }

  /**
   * @param accName the accName to set
   */
  public void setAccName(String accName) {
    this.accName = accName;
  }

  /**
   * @return the grpAccName
   */
  public String getGrpAccName() {
    return grpAccName;
  }

  /**
   * @param grpAccName the grpAccName to set
   */
  public void setGrpAccName(String grpAccName) {
    this.grpAccName = grpAccName;
  }

  /**
   * @return the alType
   */
  public ArrayList getAlType() {
    return alType;
  }

  /**
   * @param alType the alType to set
   */
  public void setAlType(ArrayList alType) {
    this.alType = alType;
  }

  /**
   * @return the alAccName
   */
  public ArrayList getAlAccName() {
    return alAccName;
  }

  /**
   * @param alAccName the alAccName to set
   */
  public void setAlAccName(ArrayList alAccName) {
    this.alAccName = alAccName;
  }

  /**
   * @return the alGrpAccName
   */
  public ArrayList getAlGrpAccName() {
    return alGrpAccName;
  }

  /**
   * @param alGrpAccName the alGrpAccName to set
   */
  public void setAlGrpAccName(ArrayList alGrpAccName) {
    this.alGrpAccName = alGrpAccName;
  }

  /**
   * @return the accId
   */
  public String getAccId() {
    return accId;
  }

  /**
   * @param accId the accId to set
   */
  public void setAccId(String accId) {
    this.accId = accId;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  
  /**
   * @return the txnId
   */
  public String getTxnId() {
    return txnId;
  }

  /**
   * @param txnId the txnId to set
   */
  public void setTxnId(String txnId) {
    this.txnId = txnId;
  }

  /**
   * @return the contract
   */
  public String getContract() {
    return contract;
  }

  /**
   * @param contract the contract to set
   */
  public void setContract(String contract) {
    this.contract = contract;
  }

  /**
   * @return the initiatedBy
   */
  public String getInitiatedBy() {
    return initiatedBy;
  }

  /**
   * @param initiatedBy the initiatedBy to set
   */
  public void setInitiatedBy(String initiatedBy) {
    this.initiatedBy = initiatedBy;
  }

   
  /**
   * @return the implementedBy
   */
  public String getImplementedBy() {
    return implementedBy;
  }

  /**
   * @param implementedBy the implementedBy to set
   */
  public void setImplementedBy(String implementedBy) {
    this.implementedBy = implementedBy;
  }

  /**
   * @return the rgname
   */
  public String getRgname() {
    return rgname;
  }

  /**
   * @param rgname the rgname to set
   */
  public void setRgname(String rgname) {
    this.rgname = rgname;
  }

  /**
   * @return the trname
   */
  public String getTrname() {
    return trname;
  }

  /**
   * @param trname the trname to set
   */
  public void setTrname(String trname) {
    this.trname = trname;
  }

  /**
   * @return the gpraff
   */
  public String getGpraff() {
    return gpraff;
  }

  /**
   * @param gpraff the gpraff to set
   */
  public void setGpraff(String gpraff) {
    this.gpraff = gpraff;
  }

  /**
   * @return the adname
   */
  public String getAdname() {
    return adname;
  }

  /**
   * @param adname the adname to set
   */
  public void setAdname(String adname) {
    this.adname = adname;
  }

  /**
   * @return the rpname
   */
  public String getRpname() {
    return rpname;
  }

  /**
   * @param rpname the rpname to set
   */
  public void setRpname(String rpname) {
    this.rpname = rpname;
  }

  /**
   * @return the hlcaff
   */
  public String getHlcaff() {
    return hlcaff;
  }

  /**
   * @param hlcaff the hlcaff to set
   */
  public void setHlcaff(String hlcaff) {
    this.hlcaff = hlcaff;
  }

  /**
   * @return the dname
   */
  public String getDname() {
    return dname;
  }

  /**
   * @param dname the dname to set
   */
  public void setDname(String dname) {
    this.dname = dname;
  }

  /**
   * @return the prName
   */
  public String getPrName() {
    return prName;
  }

  /**
   * @param prName the prName to set
   */
  public void setPrName(String prName) {
    this.prName = prName;
  }

  /**
   * @return the hInputString
   */
  public String gethInputString() {
    return hInputString;
  }

  /**
   * @param hInputString the hInputString to set
   */
  public void sethInputString(String hInputString) {
    this.hInputString = hInputString;
  }

  /**
   * @return the hSaveAction
   */
  public String gethSaveAction() {
    return hSaveAction;
  }

  /**
   * @param hSaveAction the hSaveAction to set
   */
  public void sethSaveAction(String hSaveAction) {
    this.hSaveAction = hSaveAction;
  }

  public String gethTxnId() {
    return hTxnId;
  }

  public void sethTxnId(String hTxnId) {
    this.hTxnId = hTxnId;
  }

  public String getPartyId() {
    return partyId;
  }

  public void setPartyId(String partyId) {
    this.partyId = partyId;
  }

  public ArrayList getAlAdjDetails() {
    return alAdjDetails;
  }

  public void setAlAdjDetails(ArrayList alAdjDetails) {
    this.alAdjDetails = alAdjDetails;
  }

  public String gethDisplayNm() {
    return hDisplayNm;
  }

  public void sethDisplayNm(String hDisplayNm) {
    this.hDisplayNm = hDisplayNm;
  }

  public String gethRedirectURL() {
    return hRedirectURL;
  }

  public void sethRedirectURL(String hRedirectURL) {
    this.hRedirectURL = hRedirectURL;
  }

  public String gethRemString() {
    return hRemString;
  }

  public void sethRemString(String hRemString) {
    this.hRemString = hRemString;
  }

  public String getPrid() {
    return prid;
  }

  public void setPrid(String prid) {
    this.prid = prid;
  }

  public String getAdjCode() {
    return adjCode;
  }

  public void setAdjCode(String adjCode) {
    this.adjCode = adjCode;
  }

  public String getAdjValue() {
    return adjValue;
  }

  public void setAdjValue(String adjValue) {
    this.adjValue = adjValue;
  }

  public String getStatusid() {
    return statusid;
  }

  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }

  /**
   * @return the initBy
   */
  public String getInitBy() {
    return initBy;
  }

  /**
   * @param initBy the initBy to set
   */
  public void setInitBy(String initBy) {
    this.initBy = initBy;
  }

  /**
   * @return the alInitBy
   */
  public ArrayList getAlInitBy() {
    return alInitBy;
  }

  /**
   * @param alInitBy the alInitBy to set
   */
  public void setAlInitBy(ArrayList alInitBy) {
    this.alInitBy = alInitBy;
  }

  /**
   * @return the dateFld
   */
  public String getDateFld() {
    return dateFld;
  }

  /**
   * @param dateFld the dateFld to set
   */
  public void setDateFld(String dateFld) {
    this.dateFld = dateFld;
  }

  /**
   * @return the startDt
   */
  public String getStartDt() {
    return startDt;
  }

  /**
   * @param startDt the startDt to set
   */
  public void setStartDt(String startDt) {
    this.startDt = startDt;
  }

  /**
   * @return the endDt
   */
  public String getEndDt() {
    return endDt;
  }

  /**
   * @param endDt the endDt to set
   */
  public void setEndDt(String endDt) {
    this.endDt = endDt;
  }

  /**
   * @return the alDateFld
   */
  public ArrayList getAlDateFld() {
    return alDateFld;
  }

  /**
   * @param alDateFld the alDateFld to set
   */
  public void setAlDateFld(ArrayList alDateFld) {
    this.alDateFld = alDateFld;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the alActions
   */
  public ArrayList getAlActions() {
    return alActions;
  }

  /**
   * @param alActions the alActions to set
   */
  public void setAlActions(ArrayList alActions) {
    this.alActions = alActions;
  }

  /**
   * @return the hStatus
   */
  public String gethStatus() {
    return hStatus;
  }

  /**
   * @param hStatus the hStatus to set
   */
  public void sethStatus(String hStatus) {
    this.hStatus = hStatus;
  }

  /**
   * @return the emptyAcc
   */
  public String getEmptyAcc() {
    return emptyAcc;
  }

  /**
   * @param emptyAcc the emptyAcc to set
   */
  public void setEmptyAcc(String emptyAcc) {
    this.emptyAcc = emptyAcc;
  }

  /**
   * @return the alEmptyAcc
   */
  public ArrayList getAlEmptyAcc() {
    return alEmptyAcc;
  }

  /**
   * @param alEmptyAcc the alEmptyAcc to set
   */
  public void setAlEmptyAcc(ArrayList alEmptyAcc) {
    this.alEmptyAcc = alEmptyAcc;
  }

  /**
   * @return the hAccIDs
   */
  public String gethAccIDs() {
    return hAccIDs;
  }

  /**
   * @param hAccIDs the hAccIDs to set
   */
  public void sethAccIDs(String hAccIDs) {
    this.hAccIDs = hAccIDs;
  }

  /**
   * @return the actions
   */
  public String getActions() {
    return actions;
  }

  /**
   * @param actions the actions to set
   */
  public void setActions(String actions) {
    this.actions = actions;
  }

  /**
   * @return the deniedComments
   */
  /*
   * public String getDeniedComments() { return deniedComments; }
   */

  /**
   * @param deniedComments the deniedComments to set
   */
  /*
   * public void setDeniedComments(String deniedComments) { this.deniedComments = deniedComments; }
   */

  /**
   * @return the successMessage
   */
  public String getSuccessMessage() {
    return successMessage;
  }

  /**
   * @param successMessage the successMessage to set
   */
  public void setSuccessMessage(String successMessage) {
    this.successMessage = successMessage;
  }

  /**
   * @return the submitAccess
   */
  public String getSubmitAccess() {
    return submitAccess;
  }

  /**
   * @param submitAccess the submitAccess to set
   */
  public void setSubmitAccess(String submitAccess) {
    this.submitAccess = submitAccess;
  }

  /**
   * @return the alLogReasons
   */
  @Override
  public ArrayList getAlLogReasons() {
    return alLogReasons;
  }

  /**
   * @param alLogReasons the alLogReasons to set
   */
  @Override
  public void setAlLogReasons(ArrayList alLogReasons) {
    this.alLogReasons = alLogReasons;
  }

}
