package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import org.apache.struts.upload.FormFile;
import com.globus.common.forms.GmQuestionAnswerForm;

public class GmPriceApprovalRequestForm extends GmQuestionAnswerForm {

  private String xmlGridData = "";
  private String priceRequestId = "";
  private String statusId = "";
  private String statusName = "";
  private String initiatedBy = "";
  private String initiatedDate = "";
  private String currency = "";
  private String last12MonthSales = "";
  private String rebate = "";
  private String gpoReferenceListId = "";
  private String priceInstruments = "";
  private String inputString = "";
  private String accountId = "";
  private String systemId = "";
  private String groupPriceId = "";
  private String recountCount = "";

  private String denyAccesFl = "";
  private String approveAccFl = "";
  private String deleteAccFl = "";
  private String statusUpdAccFl = "";
  private String pcAccFl = "";
  private String advpAccFl = "";
  private String genPrtFileFl = "";


  private String implementString = "";

  private String accid = "";
  private String gpbid = "";
  private String strparentacc = "";
  private String stradname = "";
  private String strdname = "";
  private String strtername = "";
  private String strrepname = "";
  private String strvpname = "";
  private String strhlcaff = "";
  private String contractflname = "";
  private String strcurrency = "";
  private String strgpb = "";
  private String strgpraff = "";
  private String strregname = "";
  private FormFile file;
  private String checkFlag = "";
  private String gpbName ="";
  private String projected12MonthSales = "";
  private String systemListId="";
  private String saveSys = "";
  ArrayList alGPORefList = new ArrayList();
  ArrayList alAccountInfo = new ArrayList();
  private ArrayList alSystemList = new ArrayList();
  private String idnUpdAccess = "";
  private String strDisabled ="";
  private String idnNo ="";
  
  private String segmentId="";
  private String releaseFl="";
  private String submitFl="";
  private ArrayList alSegNameList = new ArrayList();

/**
 * @return the saveSys
 */
public String getSaveSys() {
	return saveSys;
}

/**
 * @param saveSys the saveSys to set
 */
public void setSaveSys(String saveSys) {
	this.saveSys = saveSys;
}

/**
 * @return the projected12MonthSales
 */
public String getProjected12MonthSales() {
	return projected12MonthSales;
}

/**
 * @param projected12MonthSales the projected12MonthSales to set
 */
public void setProjected12MonthSales(String projected12MonthSales) {
	this.projected12MonthSales = projected12MonthSales;
}

/**
 * @return the gpbid
 */
public String getGpbid() {
	return gpbid;
}

/**
 * @param gpbid the gpbid to set
 */
public void setGpbid(String gpbid) {
	this.gpbid = gpbid;
}
  
  /**
 * @return the gpbName
 */
public String getGpbName() {
	return gpbName;
}

/**
 * @param gpbName the gpbName to set
 */
public void setGpbName(String gpbName) {
	this.gpbName = gpbName;
}

private String count = "";
  /**
 * @return the count
 */
public String getCount() {
	return count;
}

/**
 * @param count the count to set
 */
public void setCount(String count) {
	this.count = count;
}

/**
 * @return the file
 */
public FormFile getFile() {
	return file;
}

/**
 * @param file the file to set
 */
public void setFile(FormFile file) {
	this.file = file;
}

  /**
   * @return the xmlGridData
   */
  public String getXmlGridData() {
    return xmlGridData;
  }

  /**
   * @param xmlGridData the xmlGridData to set
   */
  public void setXmlGridData(String xmlGridData) {
    this.xmlGridData = xmlGridData;
  }

  /**
   * @return the priceRequestId
   */
  public String getPriceRequestId() {
    return priceRequestId;
  }

  /**
   * @param priceRequestId the priceRequestId to set
   */
  public void setPriceRequestId(String priceRequestId) {
    this.priceRequestId = priceRequestId;
  }


  /**
   * @return the statusId
   */
  public String getStatusId() {
    return statusId;
  }

  /**
   * @param statusId the statusId to set
   */
  public void setStatusId(String statusId) {
    this.statusId = statusId;
  }

  /**
   * @return the statusName
   */
  public String getStatusName() {
    return statusName;
  }

  /**
   * @param statusName the statusName to set
   */
  public void setStatusName(String statusName) {
    this.statusName = statusName;
  }

  /**
   * @return the initiatedBy
   */
  public String getInitiatedBy() {
    return initiatedBy;
  }

  /**
   * @param initiatedBy the initiatedBy to set
   */
  public void setInitiatedBy(String initiatedBy) {
    this.initiatedBy = initiatedBy;
  }

  /**
   * @return the initiatedDate
   */
  public String getInitiatedDate() {
    return initiatedDate;
  }

  /**
   * @param initiatedDate the initiatedDate to set
   */
  public void setInitiatedDate(String initiatedDate) {
    this.initiatedDate = initiatedDate;
  }

  /**
   * @return the currency
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * @param currency the currency to set
   */
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  /**
   * @return the last12MonthSales
   */
  public String getLast12MonthSales() {
    return last12MonthSales;
  }

  /**
   * @param last12MonthSales the last12MonthSales to set
   */
  public void setLast12MonthSales(String last12MonthSales) {
    this.last12MonthSales = last12MonthSales;
  }

  /**
   * @return the rebate
   */
  public String getRebate() {
    return rebate;
  }

  /**
   * @param rebate the rebate to set
   */
  public void setRebate(String rebate) {
    this.rebate = rebate;
  }

  /**
   * @return the gpoReferenceListId
   */
  public String getGpoReferenceListId() {
    return gpoReferenceListId;
  }

  /**
   * @param gpoReferenceListId the gpoReferenceListId to set
   */
  public void setGpoReferenceListId(String gpoReferenceListId) {
    this.gpoReferenceListId = gpoReferenceListId;
  }

  /**
   * @return the priceInstruments
   */
  public String getPriceInstruments() {
    return priceInstruments;
  }

  /**
   * @param priceInstruments the priceInstruments to set
   */
  public void setPriceInstruments(String priceInstruments) {
    this.priceInstruments = priceInstruments;
  }

  /**
   * @return the alGPORefList
   */
  public ArrayList getAlGPORefList() {
    return alGPORefList;
  }

  /**
   * @param alGPORefList the alGPORefList to set
   */
  public void setAlGPORefList(ArrayList alGPORefList) {
    this.alGPORefList = alGPORefList;
  }


  /**
   * @return the inputString
   */
  public String getInputString() {
    return inputString;
  }

  /**
   * @param inputString the inputString to set
   */
  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  /**
   * @return the denyAccesFl
   */
  public String getDenyAccesFl() {
    return denyAccesFl;
  }

  /**
   * @param denyAccesFl the denyAccesFl to set
   */
  public void setDenyAccesFl(String denyAccesFl) {
    this.denyAccesFl = denyAccesFl;
  }

  /**
   * @return the approveAccFl
   */
  public String getApproveAccFl() {
    return approveAccFl;
  }

  /**
   * @param approveAccFl the approveAccFl to set
   */
  public void setApproveAccFl(String approveAccFl) {
    this.approveAccFl = approveAccFl;
  }

  /**
   * @return the accountId
   */
  public String getAccountId() {
    return accountId;
  }

  /**
   * @param accountId the accountId to set
   */
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  /**
   * @return the systemId
   */
  public String getSystemId() {
    return systemId;
  }

  /**
   * @param systemId the systemId to set
   */
  public void setSystemId(String systemId) {
    this.systemId = systemId;
  }

  /**
   * @return the deleteAccFl
   */
  public String getDeleteAccFl() {
    return deleteAccFl;
  }

  /**
   * @param deleteAccFl the deleteAccFl to set
   */
  public void setDeleteAccFl(String deleteAccFl) {
    this.deleteAccFl = deleteAccFl;
  }

  /**
   * @return the groupPriceId
   */
  public String getGroupPriceId() {
    return groupPriceId;
  }

  /**
   * @param groupPriceId the groupPriceId to set
   */
  public void setGroupPriceId(String groupPriceId) {
    this.groupPriceId = groupPriceId;
  }

  /**
   * @return the statusUpdAccFl
   */
  public String getStatusUpdAccFl() {
    return statusUpdAccFl;
  }

  /**
   * @param statusUpdAccFl the statusUpdAccFl to set
   */
  public void setStatusUpdAccFl(String statusUpdAccFl) {
    this.statusUpdAccFl = statusUpdAccFl;
  }

  /**
   * @return the recountCount
   */
  public String getRecountCount() {
    return recountCount;
  }

  /**
   * @param recountCount the recountCount to set
   */
  public void setRecountCount(String recountCount) {
    this.recountCount = recountCount;
  }

  /**
   * @return the implementString
   */
  public String getImplementString() {
    return implementString;
  }

  /**
   * @param implementString the implementString to set
   */
  public void setImplementString(String implementString) {
    this.implementString = implementString;
  }

  public String getPcAccFl() {
    return pcAccFl;
  }

  public void setPcAccFl(String pcAccFl) {
    this.pcAccFl = pcAccFl;
  }

  public String getAdvpAccFl() {
    return advpAccFl;
  }

  public void setAdvpAccFl(String advpAccFl) {
    this.advpAccFl = advpAccFl;
  }

  /**
   * @return the genPrtFileFl
   */
  public String getGenPrtFileFl() {
    return genPrtFileFl;
  }

  /**
   * @param genPrtFileFl the genPrtFileFl to set
   */
  public void setGenPrtFileFl(String genPrtFileFl) {
    this.genPrtFileFl = genPrtFileFl;
  }

  /**
   * @return the accid
   */
  public String getAccid() {
    return accid;
  }

  /**
   * @param accid the accid to set
   */
  public void setAccid(String accid) {
    this.accid = accid;
  }

  /**
   * @return the strparentacc
   */
  public String getStrparentacc() {
    return strparentacc;
  }

  /**
   * @param strparentacc the strparentacc to set
   */
  public void setStrparentacc(String strparentacc) {
    this.strparentacc = strparentacc;
  }

  /**
   * @return the stradname
   */
  public String getStradname() {
    return stradname;
  }

  /**
   * @param stradname the stradname to set
   */
  public void setStradname(String stradname) {
    this.stradname = stradname;
  }

  /**
   * @return the strdname
   */
  public String getStrdname() {
    return strdname;
  }

  /**
   * @param strdname the strdname to set
   */
  public void setStrdname(String strdname) {
    this.strdname = strdname;
  }

  /**
   * @return the strtername
   */
  public String getStrtername() {
    return strtername;
  }

  /**
   * @param strtername the strtername to set
   */
  public void setStrtername(String strtername) {
    this.strtername = strtername;
  }

  /**
   * @return the strrepname
   */
  public String getStrrepname() {
    return strrepname;
  }

  /**
   * @param strrepname the strrepname to set
   */
  public void setStrrepname(String strrepname) {
    this.strrepname = strrepname;
  }

  /**
   * @return the strvpname
   */
  public String getStrvpname() {
    return strvpname;
  }

  /**
   * @param strvpname the strvpname to set
   */
  public void setStrvpname(String strvpname) {
    this.strvpname = strvpname;
  }

  /**
   * @return the strhlcaff
   */
  public String getStrhlcaff() {
    return strhlcaff;
  }

  /**
   * @param strhlcaff the strhlcaff to set
   */
  public void setStrhlcaff(String strhlcaff) {
    this.strhlcaff = strhlcaff;
  }

  /**
   * @return the contractflname
   */
  public String getContractflname() {
    return contractflname;
  }

  /**
   * @param contractflname the contractflname to set
   */
  public void setContractflname(String contractflname) {
    this.contractflname = contractflname;
  }

  /**
   * @return the strcurrency
   */
  public String getStrcurrency() {
    return strcurrency;
  }

  /**
   * @param strcurrency the strcurrency to set
   */
  public void setStrcurrency(String strcurrency) {
    this.strcurrency = strcurrency;
  }

  /**
   * @return the strgpb
   */
  public String getStrgpb() {
    return strgpb;
  }

  /**
   * @param strgpb the strgpb to set
   */
  public void setStrgpb(String strgpb) {
    this.strgpb = strgpb;
  }

  /**
   * @return the strgpraff
   */
  public String getStrgpraff() {
    return strgpraff;
  }

  /**
   * @param strgpraff the strgpraff to set
   */
  public void setStrgpraff(String strgpraff) {
    this.strgpraff = strgpraff;
  }

  /**
   * @return the strregname
   */
  public String getStrregname() {
    return strregname;
  }

  /**
   * @param strregname the strregname to set
   */
  public void setStrregname(String strregname) {
    this.strregname = strregname;
  }

  /**
   * @return the alAccountInfo
   */
  public ArrayList getAlAccountInfo() {
    return alAccountInfo;
  }

  /**
   * @param alAccountInfo the alAccountInfo to set
   */
  public void setAlAccountInfo(ArrayList alAccountInfo) {
    this.alAccountInfo = alAccountInfo;
  }

  /**
   * @return the checkFlag
   */
  public String getCheckFlag() {
    return checkFlag;
  }

  /**
   * @param checkFlag the checkFlag to set
   */
  public void setCheckFlag(String checkFlag) {
    this.checkFlag = checkFlag;
  }
  
	public String getSystemListId() {
		return systemListId;
	}

	public void setSystemListId(String systemListId) {
		this.systemListId = systemListId;
	}
	
	public ArrayList getAlSystemList() {
			return alSystemList;
	}

	public void setAlSystemList(ArrayList alSystemList) {
			this.alSystemList = alSystemList;
	}

	public String getIdnUpdAccess() {
		return idnUpdAccess;
	}

	public void setIdnUpdAccess(String idnUpdAccess) {
		this.idnUpdAccess = idnUpdAccess;
	}

	public String getStrDisabled() {
		return strDisabled;
	}

	public void setStrDisabled(String strDisabled) {
		this.strDisabled = strDisabled;
	}

	public String getIdnNo() {
		return idnNo;
	}

	public void setIdnNo(String idnNo) {
		this.idnNo = idnNo;
	}

	/**
	 * @return the segmentId
	 */
	public String getSegmentId() {
		return segmentId;
	}

	/**
	 * @param segmentId
	 *            the segmentId to set
	 */
	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}

	/**
	 * @return the releaseFl
	 */
	public String getReleaseFl() {
		return releaseFl;
	}

	/**
	 * @param releaseFl
	 *            the releaseFl to set
	 */
	public void setReleaseFl(String releaseFl) {
		this.releaseFl = releaseFl;
	}

	/**
	 * @return the alSegNameList
	 */
	public ArrayList getAlSegNameList() {
		return alSegNameList;
	}

	/**
	 * @param alSegNameList
	 *            the alSegNameList to set
	 */
	public void setAlSegNameList(ArrayList alSegNameList) {
		this.alSegNameList = alSegNameList;
	}

	/**
	 * @return the submitFl
	 */
	public String getSubmitFl() {
		return submitFl;
	}

	/**
	 * @param submitFl
	 *            the submitFl to set
	 */
	public void setSubmitFl(String submitFl) {
		this.submitFl = submitFl;
	}
	

}
