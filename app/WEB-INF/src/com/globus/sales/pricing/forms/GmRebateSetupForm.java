package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import java.util.HashMap;
import com.globus.common.forms.GmLogForm;

public class GmRebateSetupForm extends GmLogForm{

	
	private static final long serialVersionUID = 1L;
	
	private String rebateId ="";
	private String groupTypeId ="";
	private String groupTypeName = "";
	private String partyId = "";
	private String partyName ="";
	private String rebatePartTypeId = "";
	private String rebatePartTypeName = "";
	private String rebateCategoryId ="";
	private String rebateCategoryName ="";
	private String rebateRate ="";
	private String rebateFromDate="";
	private String rebateToDate ="";
	private String rebateEffectiveDate="";
	private String searchpartyId="";
	private String rebateTypeId ="";
	private String rebateTypeName ="";
	private String rebatePartNumber ="";
	private String rebatePaymentId="";
	private String rebatePaymentName ="";
	private String rebateContractInfo="";
	private String rebateComments ="";
	private String activeFl ="";
	private String accessFl ="";
	
	
	
	private ArrayList alAccountType = new ArrayList();
	private ArrayList alRebateParts = new ArrayList();
	private ArrayList alRebateCategory = new ArrayList();
	private ArrayList alRebateType = new ArrayList();
	private ArrayList alPayment = new ArrayList();
	private ArrayList alRebateNameList = new ArrayList();
	private ArrayList alresult = new ArrayList();
	

	//for tier Value
	private String tier1Percent = "";
	private String tier2Percent = "";
	private String tier3Percent = "";
	
	private String tier1FromAmt = "";	
	private String tier2FromAmt = "";	
	private String tier3FromAmt= "";
	
	private String tier1ToAmt = "";
	private String tier2ToAmt = "";
	private String tier3ToAmt = "";
	//For Rebate add parts
	private String batchPartNum="";
	private String haccountType="";
	private String haccountName="";
	private String hcontractFromDate="";
	private String hcontractToDate="";
	private String haccountTypeName="";
	private String heffectiveDate="";
	private String partInputString="";
	private String effectivePartDate="";
	private String rebatepartcomments="";
	private String hRebateId="";
	private String rebateCategoryFl ="";
	private String rebateRateFl ="";
	private String rebatePartFl ="";
	private String rebateEffectiveFl ="";
	private String hAccName="";
	private String hpartyName="";
	// to reset the form when no data found
	public void reset()
	{
		 rebatePartTypeId = "";
		 rebatePartTypeName = "";
		 rebateCategoryId ="";
		 rebateCategoryName ="";
		 rebateRate ="";
		 rebateFromDate="";
		 rebateToDate ="";
		 rebateEffectiveDate="";
		 rebateTypeId ="";
		 rebateTypeName ="";
		 rebatePartNumber ="";
		 rebatePaymentId="";
		 rebatePaymentName ="";
		 rebateContractInfo="";
		 rebateComments ="";
		 activeFl ="";
		 rebateId="";
		tier1Percent = "";
		tier2Percent = "";
		tier3Percent = "";
		tier1FromAmt = "";	
		tier2FromAmt = "";	
		tier3FromAmt= "";
	    tier1ToAmt = "";
		tier2ToAmt = "";
	    tier3ToAmt = "";
		
	}
	
	
	/**
	 * @return the rebateId
	 */
	public String getRebateId() {
		return rebateId;
	}
	/**
	 * @param rebateId the rebateId to set
	 */
	public void setRebateId(String rebateId) {
		this.rebateId = rebateId;
	}
	/**
	 * @return the groupTypeId
	 */
	public String getGroupTypeId() {
		return groupTypeId;
	}
	/**
	 * @param groupTypeId the groupTypeId to set
	 */
	public void setGroupTypeId(String groupTypeId) {
		this.groupTypeId = groupTypeId;
	}
	/**
	 * @return the groupTypeName
	 */
	public String getGroupTypeName() {
		return groupTypeName;
	}
	/**
	 * @param groupTypeName the groupTypeName to set
	 */
	public void setGroupTypeName(String groupTypeName) {
		this.groupTypeName = groupTypeName;
	}
	/**
	 * @return the partyId
	 */
	public String getPartyId() {
		return partyId;
	}
	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	/**
	 * @return the partyName
	 */
	public String getPartyName() {
		return partyName;
	}
	/**
	 * @param partyName the partyName to set
	 */
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	/**
	 * @return the rebatePartTypeId
	 */
	public String getRebatePartTypeId() {
		return rebatePartTypeId;
	}
	/**
	 * @param rebatePartTypeId the rebatePartTypeId to set
	 */
	public void setRebatePartTypeId(String rebatePartTypeId) {
		this.rebatePartTypeId = rebatePartTypeId;
	}
	/**
	 * @return the rebatePartTypeName
	 */
	public String getRebatePartTypeName() {
		return rebatePartTypeName;
	}
	/**
	 * @param rebatePartTypeName the rebatePartTypeName to set
	 */
	public void setRebatePartTypeName(String rebatePartTypeName) {
		this.rebatePartTypeName = rebatePartTypeName;
	}
	/**
	 * @return the rebateCategoryId
	 */
	public String getRebateCategoryId() {
		return rebateCategoryId;
	}
	/**
	 * @param rebateCategoryId the rebateCategoryId to set
	 */
	public void setRebateCategoryId(String rebateCategoryId) {
		this.rebateCategoryId = rebateCategoryId;
	}
	/**
	 * @return the rebateCategoryName
	 */
	public String getRebateCategoryName() {
		return rebateCategoryName;
	}
	/**
	 * @param rebateCategoryName the rebateCategoryName to set
	 */
	public void setRebateCategoryName(String rebateCategoryName) {
		this.rebateCategoryName = rebateCategoryName;
	}
	/**
	 * @return the rebateRate
	 */
	public String getRebateRate() {
		return rebateRate;
	}
	/**
	 * @param rebateRate the rebateRate to set
	 */
	public void setRebateRate(String rebateRate) {
		this.rebateRate = rebateRate;
	}
	/**
	 * @return the rebateFromDate
	 */
	public String getRebateFromDate() {
		return rebateFromDate;
	}
	/**
	 * @param rebateFromDate the rebateFromDate to set
	 */
	public void setRebateFromDate(String rebateFromDate) {
		this.rebateFromDate = rebateFromDate;
	}
	/**
	 * @return the rebateToDate
	 */
	public String getRebateToDate() {
		return rebateToDate;
	}
	/**
	 * @param rebateToDate the rebateToDate to set
	 */
	public void setRebateToDate(String rebateToDate) {
		this.rebateToDate = rebateToDate;
	}
	/**
	 * @return the rebateEffectiveDate
	 */
	public String getRebateEffectiveDate() {
		return rebateEffectiveDate;
	}
	/**
	 * @param rebateEffectiveDate the rebateEffectiveDate to set
	 */
	public void setRebateEffectiveDate(String rebateEffectiveDate) {
		this.rebateEffectiveDate = rebateEffectiveDate;
	}
	/**
	 * @return the searchpartyId
	 */
	public String getSearchpartyId() {
		return searchpartyId;
	}
	/**
	 * @param searchpartyId the searchpartyId to set
	 */
	public void setSearchpartyId(String searchpartyId) {
		this.searchpartyId = searchpartyId;
	}
	/**
	 * @return the rebateTypeId
	 */
	public String getRebateTypeId() {
		return rebateTypeId;
	}
	/**
	 * @param rebateTypeId the rebateTypeId to set
	 */
	public void setRebateTypeId(String rebateTypeId) {
		this.rebateTypeId = rebateTypeId;
	}
	/**
	 * @return the rebateTypeName
	 */
	public String getRebateTypeName() {
		return rebateTypeName;
	}
	/**
	 * @param rebateTypeName the rebateTypeName to set
	 */
	public void setRebateTypeName(String rebateTypeName) {
		this.rebateTypeName = rebateTypeName;
	}
	/**
	 * @return the rebatePartNumber
	 */
	public String getRebatePartNumber() {
		return rebatePartNumber;
	}
	/**
	 * @param rebatePartNumber the rebatePartNumber to set
	 */
	public void setRebatePartNumber(String rebatePartNumber) {
		this.rebatePartNumber = rebatePartNumber;
	}
	/**
	 * @return the rebatePaymentId
	 */
	public String getRebatePaymentId() {
		return rebatePaymentId;
	}
	/**
	 * @param rebatePaymentId the rebatePaymentId to set
	 */
	public void setRebatePaymentId(String rebatePaymentId) {
		this.rebatePaymentId = rebatePaymentId;
	}
	/**
	 * @return the rebatePaymentName
	 */
	public String getRebatePaymentName() {
		return rebatePaymentName;
	}
	/**
	 * @param rebatePaymentName the rebatePaymentName to set
	 */
	public void setRebatePaymentName(String rebatePaymentName) {
		this.rebatePaymentName = rebatePaymentName;
	}
	/**
	 * @return the rebateContractInfo
	 */
	public String getRebateContractInfo() {
		return rebateContractInfo;
	}
	/**
	 * @param rebateContractInfo the rebateContractInfo to set
	 */
	public void setRebateContractInfo(String rebateContractInfo) {
		this.rebateContractInfo = rebateContractInfo;
	}
	/**
	 * @return the rebateComments
	 */
	public String getRebateComments() {
		return rebateComments;
	}
	/**
	 * @param rebateComments the rebateComments to set
	 */
	public void setRebateComments(String rebateComments) {
		this.rebateComments = rebateComments;
	}
	/**
	 * @return the activeFl
	 */
	public String getActiveFl() {
		return activeFl;
	}
	/**
	 * @param activeFl the activeFl to set
	 */
	public void setActiveFl(String activeFl) {
		this.activeFl = activeFl;
	}
	/**
	 * @return the alAccountType
	 */
	public ArrayList getAlAccountType() {
		return alAccountType;
	}
	/**
	 * @param alAccountType the alAccountType to set
	 */
	public void setAlAccountType(ArrayList alAccountType) {
		this.alAccountType = alAccountType;
	}
	/**
	 * @return the alRebateParts
	 */
	public ArrayList getAlRebateParts() {
		return alRebateParts;
	}
	/**
	 * @param alRebateParts the alRebateParts to set
	 */
	public void setAlRebateParts(ArrayList alRebateParts) {
		this.alRebateParts = alRebateParts;
	}
	/**
	 * @return the alRebateCategory
	 */
	public ArrayList getAlRebateCategory() {
		return alRebateCategory;
	}
	/**
	 * @param alRebateCategory the alRebateCategory to set
	 */
	public void setAlRebateCategory(ArrayList alRebateCategory) {
		this.alRebateCategory = alRebateCategory;
	}
	/**
	 * @return the alRebateType
	 */
	public ArrayList getAlRebateType() {
		return alRebateType;
	}
	/**
	 * @param alRebateType the alRebateType to set
	 */
	public void setAlRebateType(ArrayList alRebateType) {
		this.alRebateType = alRebateType;
	}
	/**
	 * @return the alPayment
	 */
	public ArrayList getAlPayment() {
		return alPayment;
	}
	/**
	 * @param alPayment the alPayment to set
	 */
	public void setAlPayment(ArrayList alPayment) {
		this.alPayment = alPayment;
	}
	/**
	 * @return the alRebateNameList
	 */
	public ArrayList getAlRebateNameList() {
		return alRebateNameList;
	}
	/**
	 * @param alRebateNameList the alRebateNameList to set
	 */
	public void setAlRebateNameList(ArrayList alRebateNameList) {
		this.alRebateNameList = alRebateNameList;
	}
	/**
	 * @return the alresult
	 */
	public ArrayList getAlresult() {
		return alresult;
	}
	/**
	 * @param alresult the alresult to set
	 */
	public void setAlresult(ArrayList alresult) {
		this.alresult = alresult;
	}
	
	/**
	 * @return the tier1Percent
	 */
	public String getTier1Percent() {
		return tier1Percent;
	}
	/**
	 * @param tier1Percent the tier1Percent to set
	 */
	public void setTier1Percent(String tier1Percent) {
		this.tier1Percent = tier1Percent;
	}
	/**
	 * @return the tier2Percent
	 */
	public String getTier2Percent() {
		return tier2Percent;
	}
	/**
	 * @param tier2Percent the tier2Percent to set
	 */
	public void setTier2Percent(String tier2Percent) {
		this.tier2Percent = tier2Percent;
	}
	/**
	 * @return the tier3Percent
	 */
	public String getTier3Percent() {
		return tier3Percent;
	}
	/**
	 * @param tier3Percent the tier3Percent to set
	 */
	public void setTier3Percent(String tier3Percent) {
		this.tier3Percent = tier3Percent;
	}
	/**
	 * @return the tier1FromAmt
	 */
	public String getTier1FromAmt() {
		return tier1FromAmt;
	}
	/**
	 * @param tier1FromAmt the tier1FromAmt to set
	 */
	public void setTier1FromAmt(String tier1FromAmt) {
		this.tier1FromAmt = tier1FromAmt;
	}
	/**
	 * @return the tier2FromAmt
	 */
	public String getTier2FromAmt() {
		return tier2FromAmt;
	}
	/**
	 * @param tier2FromAmt the tier2FromAmt to set
	 */
	public void setTier2FromAmt(String tier2FromAmt) {
		this.tier2FromAmt = tier2FromAmt;
	}
	/**
	 * @return the tier3FromAmt
	 */
	public String getTier3FromAmt() {
		return tier3FromAmt;
	}
	/**
	 * @param tier3FromAmt the tier3FromAmt to set
	 */
	public void setTier3FromAmt(String tier3FromAmt) {
		this.tier3FromAmt = tier3FromAmt;
	}
	/**
	 * @return the tier1ToAmt
	 */
	public String getTier1ToAmt() {
		return tier1ToAmt;
	}
	/**
	 * @param tier1ToAmt the tier1ToAmt to set
	 */
	public void setTier1ToAmt(String tier1ToAmt) {
		this.tier1ToAmt = tier1ToAmt;
	}
	/**
	 * @return the tier2ToAmt
	 */
	public String getTier2ToAmt() {
		return tier2ToAmt;
	}
	/**
	 * @param tier2ToAmt the tier2ToAmt to set
	 */
	public void setTier2ToAmt(String tier2ToAmt) {
		this.tier2ToAmt = tier2ToAmt;
	}
	/**
	 * @return the tier3ToAmt
	 */
	public String getTier3ToAmt() {
		return tier3ToAmt;
	}
	/**
	 * @param tier3ToAmt the tier3ToAmt to set
	 */
	public void setTier3ToAmt(String tier3ToAmt) {
		this.tier3ToAmt = tier3ToAmt;
	}
	/**
	 * @return the accessFl
	 */
	public String getAccessFl() {
		return accessFl;
	}
	/**
	 * @param accessFl the accessFl to set
	 */
	public void setAccessFl(String accessFl) {
		this.accessFl = accessFl;
	}
	
	/**
	 * @return the batchPartNum
	 */
	public String getBatchPartNum() {
		return batchPartNum;
	}
	/**
	 * @param batchPartNum the batchPartNum to set
	 */
	public void setBatchPartNum(String batchPartNum) {
		this.batchPartNum = batchPartNum;
	}
	/**
	 * @return the haccountType
	 */
	public String getHaccountType() {
		return haccountType;
	}
	/**
	 * @param haccountType the haccountType to set
	 */
	public void setHaccountType(String haccountType) {
		this.haccountType = haccountType;
	}
	/**
	 * @return the haccountName
	 */
	public String getHaccountName() {
		return haccountName;
	}
	/**
	 * @param haccountName the haccountName to set
	 */
	public void setHaccountName(String haccountName) {
		this.haccountName = haccountName;
	}
	/**
	 * @return the hcontractFromDate
	 */
	public String getHcontractFromDate() {
		return hcontractFromDate;
	}
	/**
	 * @param hcontractFromDate the hcontractFromDate to set
	 */
	public void setHcontractFromDate(String hcontractFromDate) {
		this.hcontractFromDate = hcontractFromDate;
	}
	/**
	 * @return the hcontractToDate
	 */
	public String getHcontractToDate() {
		return hcontractToDate;
	}
	/**
	 * @param hcontractToDate the hcontractToDate to set
	 */
	public void setHcontractToDate(String hcontractToDate) {
		this.hcontractToDate = hcontractToDate;
	}
	/**
	 * @return the haccountTypeName
	 */
	public String getHaccountTypeName() {
		return haccountTypeName;
	}
	/**
	 * @param haccountTypeName the haccountTypeName to set
	 */
	public void setHaccountTypeName(String haccountTypeName) {
		this.haccountTypeName = haccountTypeName;
	}
	/**
	 * @return the heffectiveDate
	 */
	public String getHeffectiveDate() {
		return heffectiveDate;
	}
	/**
	 * @param heffectiveDate the heffectiveDate to set
	 */
	public void setHeffectiveDate(String heffectiveDate) {
		this.heffectiveDate = heffectiveDate;
	}
	/**
	 * @return the partInputString
	 */
	public String getPartInputString() {
		return partInputString;
	}
	/**
	 * @param partInputString the partInputString to set
	 */
	public void setPartInputString(String partInputString) {
		this.partInputString = partInputString;
	}
	/**
	 * @return the effectivePartDate
	 */
	public String getEffectivePartDate() {
		return effectivePartDate;
	}
	/**
	 * @param effectivePartDate the effectivePartDate to set
	 */
	public void setEffectivePartDate(String effectivePartDate) {
		this.effectivePartDate = effectivePartDate;
	}
	/**
	 * @return the rebatepartcomments
	 */
	public String getRebatepartcomments() {
		return rebatepartcomments;
	}
	/**
	 * @param rebatepartcomments the rebatepartcomments to set
	 */
	public void setRebatepartcomments(String rebatepartcomments) {
		this.rebatepartcomments = rebatepartcomments;
	}
	/**
	 * @return the hRebateId
	 */
	public String gethRebateId() {
		return hRebateId;
	}
	/**
	 * @param hRebateId the hRebateId to set
	 */
	public void sethRebateId(String hRebateId) {
		this.hRebateId = hRebateId;
	}
	/**
	 * @return the rebateCategoryFl
	 */
	public String getRebateCategoryFl() {
		return rebateCategoryFl;
	}
	/**
	 * @param rebateCategoryFl the rebateCategoryFl to set
	 */
	public void setRebateCategoryFl(String rebateCategoryFl) {
		this.rebateCategoryFl = rebateCategoryFl;
	}
	/**
	 * @return the rebateRateFl
	 */
	public String getRebateRateFl() {
		return rebateRateFl;
	}
	/**
	 * @param rebateRateFl the rebateRateFl to set
	 */
	public void setRebateRateFl(String rebateRateFl) {
		this.rebateRateFl = rebateRateFl;
	}
	/**
	 * @return the rebatePartsFl
	 */
	/**
	 * @return the rebatePartFl
	 */
	public String getRebatePartFl() {
		return rebatePartFl;
	}
	/**
	 * @param rebatePartFl the rebatePartFl to set
	 */
	public void setRebatePartFl(String rebatePartFl) {
		this.rebatePartFl = rebatePartFl;
	}
	/**
	 * @return the rebateEffectiveFl
	 */
	public String getRebateEffectiveFl() {
		return rebateEffectiveFl;
	}
	/**
	 * @param rebateEffectiveFl the rebateEffectiveFl to set
	 */
	public void setRebateEffectiveFl(String rebateEffectiveFl) {
		this.rebateEffectiveFl = rebateEffectiveFl;
	}
	/**
	 * @return the hAccName
	 */
	public String gethAccName() {
		return hAccName;
	}
	/**
	 * @param hAccName the hAccName to set
	 */
	public void sethAccName(String hAccName) {
		this.hAccName = hAccName;
	}
	/**
	 * @return the hpartyName
	 */
	public String getHpartyName() {
		return hpartyName;
	}
	/**
	 * @param hpartyName the hpartyName to set
	 */
	public void setHpartyName(String hpartyName) {
		this.hpartyName = hpartyName;
	}

	
}
