package com.globus.sales.pricing.forms;
import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmRebateReportForm extends GmCommonForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList alReturn = new ArrayList();
	
	private HashMap hmParam = new HashMap();
	
	private String rebateId ="";
	private String gridData="";
	private String groupTypeName ="";
	private String partyName ="";
	private String effectiveFrmDt ="";
	private String effectiveToDt ="";
	private String effectiveDate ="";
	private String contractToDate ="";
	
	private String groupTypeId ="";
	private String partyId = "";
    private String dateType ="";
	private String rebatePartTypeId = "";
	private String rebatePartTypeName = "";
	private String rebateCategoryId ="";
	private String rebateCategoryName ="";
	private String rebateRate ="";
	private String rebateFromDate="";
	private String rebateToDate ="";
	private String rebateEffectiveDate="";
	private String searchpartyId="";
	private String rebateTypeId ="";
    private String batchPartNum ="";
    
	private ArrayList alAccountType = new ArrayList();
	private ArrayList alRebateParts = new ArrayList();
	private ArrayList alRebateCategory = new ArrayList();
	private ArrayList alRebateType = new ArrayList();
	private ArrayList alPayment = new ArrayList();
	private ArrayList alresult = new ArrayList();
	private ArrayList alDateType = new ArrayList();
	

	//for tier Value
	private String tier1Percent = "";
	private String tier2Percent = "";
	private String tier3Percent = "";
	
	private String tier1FromAmt = "";	
	private String tier2FromAmt = "";	
	private String tier3FromAmt= "";
	
	private String tier1ToAmt = "";
	private String tier2ToAmt = "";
	private String tier3ToAmt = "";
	private String rebateFromRate="";
	private String rebateToRate="";
	private String pnumSuffix="";
	
	
	//Sales summary Report
	 private String xmlString = "";
	 private String fromDate ="";
	 private String toDate ="";
	 private HashMap hmCrossTabDtls = new HashMap();

	
	
	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}
	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}
	/**
	 * @return the hmParam
	 */
	public HashMap getHmParam() {
		return hmParam;
	}
	/**
	 * @param hmParam the hmParam to set
	 */
	public void setHmParam(HashMap hmParam) {
		this.hmParam = hmParam;
	}
	/**
	 * @return the rebateId
	 */
	public String getRebateId() {
		return rebateId;
	}
	/**
	 * @param rebateId the rebateId to set
	 */
	public void setRebateId(String rebateId) {
		this.rebateId = rebateId;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the groupTypeName
	 */
	public String getGroupTypeName() {
		return groupTypeName;
	}
	/**
	 * @param groupTypeName the groupTypeName to set
	 */
	public void setGroupTypeName(String groupTypeName) {
		this.groupTypeName = groupTypeName;
	}
	/**
	 * @return the partyName
	 */
	public String getPartyName() {
		return partyName;
	}
	/**
	 * @param partyName the partyName to set
	 */
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	/**
	 * @return the effectiveFrmDt
	 */
	public String getEffectiveFrmDt() {
		return effectiveFrmDt;
	}
	/**
	 * @param effectiveFrmDt the effectiveFrmDt to set
	 */
	public void setEffectiveFrmDt(String effectiveFrmDt) {
		this.effectiveFrmDt = effectiveFrmDt;
	}
	/**
	 * @return the effectiveToDt
	 */
	public String getEffectiveToDt() {
		return effectiveToDt;
	}
	/**
	 * @param effectiveToDt the effectiveToDt to set
	 */
	public void setEffectiveToDt(String effectiveToDt) {
		this.effectiveToDt = effectiveToDt;
	}
	
	/**
	 * @return the contractToDate
	 */
	public String getContractToDate() {
		return contractToDate;
	}
	/**
	 * @param contractToDate the contractToDate to set
	 */
	public void setContractToDate(String contractToDate) {
		this.contractToDate = contractToDate;
	}
	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}
	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	/**
	 * @return the groupTypeId
	 */
	public String getGroupTypeId() {
		return groupTypeId;
	}
	/**
	 * @param groupTypeId the groupTypeId to set
	 */
	public void setGroupTypeId(String groupTypeId) {
		this.groupTypeId = groupTypeId;
	}
	/**
	 * @return the partyId
	 */
	public String getPartyId() {
		return partyId;
	}
	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	/**
	 * @return the rebatePartTypeId
	 */
	public String getRebatePartTypeId() {
		return rebatePartTypeId;
	}
	/**
	 * @param rebatePartTypeId the rebatePartTypeId to set
	 */
	public void setRebatePartTypeId(String rebatePartTypeId) {
		this.rebatePartTypeId = rebatePartTypeId;
	}
	/**
	 * @return the rebatePartTypeName
	 */
	public String getRebatePartTypeName() {
		return rebatePartTypeName;
	}
	/**
	 * @param rebatePartTypeName the rebatePartTypeName to set
	 */
	public void setRebatePartTypeName(String rebatePartTypeName) {
		this.rebatePartTypeName = rebatePartTypeName;
	}
	/**
	 * @return the rebateCategoryId
	 */
	public String getRebateCategoryId() {
		return rebateCategoryId;
	}
	/**
	 * @param rebateCategoryId the rebateCategoryId to set
	 */
	public void setRebateCategoryId(String rebateCategoryId) {
		this.rebateCategoryId = rebateCategoryId;
	}
	/**
	 * @return the rebateCategoryName
	 */
	public String getRebateCategoryName() {
		return rebateCategoryName;
	}
	/**
	 * @param rebateCategoryName the rebateCategoryName to set
	 */
	public void setRebateCategoryName(String rebateCategoryName) {
		this.rebateCategoryName = rebateCategoryName;
	}
	/**
	 * @return the rebateRate
	 */
	public String getRebateRate() {
		return rebateRate;
	}
	/**
	 * @param rebateRate the rebateRate to set
	 */
	public void setRebateRate(String rebateRate) {
		this.rebateRate = rebateRate;
	}
	/**
	 * @return the rebateFromDate
	 */
	public String getRebateFromDate() {
		return rebateFromDate;
	}
	/**
	 * @param rebateFromDate the rebateFromDate to set
	 */
	public void setRebateFromDate(String rebateFromDate) {
		this.rebateFromDate = rebateFromDate;
	}
	/**
	 * @return the rebateToDate
	 */
	public String getRebateToDate() {
		return rebateToDate;
	}
	/**
	 * @param rebateToDate the rebateToDate to set
	 */
	public void setRebateToDate(String rebateToDate) {
		this.rebateToDate = rebateToDate;
	}
	/**
	 * @return the rebateEffectiveDate
	 */
	public String getRebateEffectiveDate() {
		return rebateEffectiveDate;
	}
	/**
	 * @param rebateEffectiveDate the rebateEffectiveDate to set
	 */
	public void setRebateEffectiveDate(String rebateEffectiveDate) {
		this.rebateEffectiveDate = rebateEffectiveDate;
	}
	/**
	 * @return the searchpartyId
	 */
	public String getSearchpartyId() {
		return searchpartyId;
	}
	/**
	 * @param searchpartyId the searchpartyId to set
	 */
	public void setSearchpartyId(String searchpartyId) {
		this.searchpartyId = searchpartyId;
	}
	/**
	 * @return the rebateTypeId
	 */
	public String getRebateTypeId() {
		return rebateTypeId;
	}
	/**
	 * @param rebateTypeId the rebateTypeId to set
	 */
	public void setRebateTypeId(String rebateTypeId) {
		this.rebateTypeId = rebateTypeId;
	}
	/**
	 * @return the alAccountType
	 */
	public ArrayList getAlAccountType() {
		return alAccountType;
	}
	/**
	 * @param alAccountType the alAccountType to set
	 */
	public void setAlAccountType(ArrayList alAccountType) {
		this.alAccountType = alAccountType;
	}
	/**
	 * @return the alRebateParts
	 */
	public ArrayList getAlRebateParts() {
		return alRebateParts;
	}
	/**
	 * @param alRebateParts the alRebateParts to set
	 */
	public void setAlRebateParts(ArrayList alRebateParts) {
		this.alRebateParts = alRebateParts;
	}
	/**
	 * @return the alRebateCategory
	 */
	public ArrayList getAlRebateCategory() {
		return alRebateCategory;
	}
	/**
	 * @param alRebateCategory the alRebateCategory to set
	 */
	public void setAlRebateCategory(ArrayList alRebateCategory) {
		this.alRebateCategory = alRebateCategory;
	}
	/**
	 * @return the alRebateType
	 */
	public ArrayList getAlRebateType() {
		return alRebateType;
	}
	/**
	 * @param alRebateType the alRebateType to set
	 */
	public void setAlRebateType(ArrayList alRebateType) {
		this.alRebateType = alRebateType;
	}
	/**
	 * @return the alPayment
	 */
	public ArrayList getAlPayment() {
		return alPayment;
	}
	/**
	 * @param alPayment the alPayment to set
	 */
	public void setAlPayment(ArrayList alPayment) {
		this.alPayment = alPayment;
	}

	/**
	 * @return the alresult
	 */
	public ArrayList getAlresult() {
		return alresult;
	}
	/**
	 * @param alresult the alresult to set
	 */
	public void setAlresult(ArrayList alresult) {
		this.alresult = alresult;
	}
	/**
	 * @return the tier1Percent
	 */
	public String getTier1Percent() {
		return tier1Percent;
	}
	/**
	 * @param tier1Percent the tier1Percent to set
	 */
	public void setTier1Percent(String tier1Percent) {
		this.tier1Percent = tier1Percent;
	}
	/**
	 * @return the tier2Percent
	 */
	public String getTier2Percent() {
		return tier2Percent;
	}
	/**
	 * @param tier2Percent the tier2Percent to set
	 */
	public void setTier2Percent(String tier2Percent) {
		this.tier2Percent = tier2Percent;
	}
	/**
	 * @return the tier3Percent
	 */
	public String getTier3Percent() {
		return tier3Percent;
	}
	/**
	 * @param tier3Percent the tier3Percent to set
	 */
	public void setTier3Percent(String tier3Percent) {
		this.tier3Percent = tier3Percent;
	}
	/**
	 * @return the tier1FromAmt
	 */
	public String getTier1FromAmt() {
		return tier1FromAmt;
	}
	/**
	 * @param tier1FromAmt the tier1FromAmt to set
	 */
	public void setTier1FromAmt(String tier1FromAmt) {
		this.tier1FromAmt = tier1FromAmt;
	}
	/**
	 * @return the tier2FromAmt
	 */
	public String getTier2FromAmt() {
		return tier2FromAmt;
	}
	/**
	 * @param tier2FromAmt the tier2FromAmt to set
	 */
	public void setTier2FromAmt(String tier2FromAmt) {
		this.tier2FromAmt = tier2FromAmt;
	}
	/**
	 * @return the tier3FromAmt
	 */
	public String getTier3FromAmt() {
		return tier3FromAmt;
	}
	/**
	 * @param tier3FromAmt the tier3FromAmt to set
	 */
	public void setTier3FromAmt(String tier3FromAmt) {
		this.tier3FromAmt = tier3FromAmt;
	}
	/**
	 * @return the tier1ToAmt
	 */
	public String getTier1ToAmt() {
		return tier1ToAmt;
	}
	/**
	 * @param tier1ToAmt the tier1ToAmt to set
	 */
	public void setTier1ToAmt(String tier1ToAmt) {
		this.tier1ToAmt = tier1ToAmt;
	}
	/**
	 * @return the tier2ToAmt
	 */
	public String getTier2ToAmt() {
		return tier2ToAmt;
	}
	/**
	 * @param tier2ToAmt the tier2ToAmt to set
	 */
	public void setTier2ToAmt(String tier2ToAmt) {
		this.tier2ToAmt = tier2ToAmt;
	}
	/**
	 * @return the tier3ToAmt
	 */
	public String getTier3ToAmt() {
		return tier3ToAmt;
	}
	/**
	 * @param tier3ToAmt the tier3ToAmt to set
	 */
	public void setTier3ToAmt(String tier3ToAmt) {
		this.tier3ToAmt = tier3ToAmt;
	}
	/**
	 * @return the dateType
	 */
	public String getDateType() {
		return dateType;
	}
	/**
	 * @param dateType the dateType to set
	 */
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}
	/**
	 * @return the alDateType
	 */
	public ArrayList getAlDateType() {
		return alDateType;
	}
	/**
	 * @param alDateType the alDateType to set
	 */
	public void setAlDateType(ArrayList alDateType) {
		this.alDateType = alDateType;
	}
	/**
	 * @return the rebateFromRate
	 */
	public String getRebateFromRate() {
		return rebateFromRate;
	}
	/**
	 * @param rebateFromRate the rebateFromRate to set
	 */
	public void setRebateFromRate(String rebateFromRate) {
		this.rebateFromRate = rebateFromRate;
	}
	/**
	 * @return the rebateToRate
	 */
	public String getRebateToRate() {
		return rebateToRate;
	}
	/**
	 * @param rebateToRate the rebateToRate to set
	 */
	public void setRebateToRate(String rebateToRate) {
		this.rebateToRate = rebateToRate;
	}
	/**
	 * @return the batchPartNum
	 */
	public String getBatchPartNum() {
		return batchPartNum;
	}
	/**
	 * @param batchPartNum the batchPartNum to set
	 */
	public void setBatchPartNum(String batchPartNum) {
		this.batchPartNum = batchPartNum;
	}
	/**
	 * @return the xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}
	/**
	 * @param xmlString the xmlString to set
	 */
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the hmCrossTabDtls
	 */
	public HashMap getHmCrossTabDtls() {
		return hmCrossTabDtls;
	}
	/**
	 * @param hmCrossTabDtls the hmCrossTabDtls to set
	 */
	public void setHmCrossTabDtls(HashMap hmCrossTabDtls) {
		this.hmCrossTabDtls = hmCrossTabDtls;
	}
	
	/**
	 * @return the pnumSuffix
	 */
	public String getPnumSuffix() {
		return pnumSuffix;
	}
	/**
	 * @param pnumSuffix the pnumSuffix to set
	 */
	public void setPnumSuffix(String pnumSuffix) {
		this.pnumSuffix = pnumSuffix;
	}
}
