package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonForm;

public class GmPricingStatusReportForm extends GmCommonForm
{
	private String adID = ""; 
	private String vpID = ""; 
	private String accountID = ""; 
	private String status = ""; 
	private String distributorID = "";   
	private String selectAcctAll = "";
	private String hsets = "";
	private String strDetailFlag = "";
	private String hinputstring = "" ;
	private String priceRequestStatusXML = "";
	private String pendingSystemAccountPriceGridXML = "";
	private String pendingGroupAccountPriceGridXML = "";
	private String pendingSystemAccountCount = "";
	private String pendingGroupAccountCount = "";

    private ArrayList alADId = new ArrayList();
	private List alVPId = new ArrayList();
	private ArrayList alAccount = new ArrayList();
	private List alDistributor = new ArrayList();
	private ArrayList alStatus = new ArrayList(); 
	
	private String[] checkAccounts = new String [10];
	private String[] checkVPs = new String [10];
	private String[] checkADs = new String [10];
	private String[] checkFSs = new String [10];
	private String[] checkStatus = new String [10];
	private String[] checkSystems = new String [10];
	private ArrayList alSystem= new ArrayList(); 
	private ArrayList alGrpDateRange= new ArrayList();
	
	private String selectVPAll = "";
	private String selectADAll = "";
	private String selectFSAll = "";
	private String selectStatusAll = "";
	private String selectSystemAll = "";
	private String grpDateRange = "";

	private String selectedVP="";
	private String selectedAD="";
	private String selectedFS="";
	private String selectedAC="";
	
	private List ldtResult = new ArrayList();

	/**
	 * @return the adID
	 */
	public String getAdID() {
		return adID;
	}

	/**
	 * @param adID the adID to set
	 */
	public void setAdID(String adID) {
		this.adID = adID;
	}

	/**
	 * @return the vpID
	 */
	public String getVpID() {
		return vpID;
	}

	/**
	 * @param vpID the vpID to set
	 */
	public void setVpID(String vpID) {
		this.vpID = vpID;
	}

	/**
	 * @return the accountID
	 */
	public String getAccountID() {
		return accountID;
	}

	/**
	 * @param accountID the accountID to set
	 */
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the distributorID
	 */
	public String getDistributorID() {
		return distributorID;
	}

	/**
	 * @param distributorID the distributorID to set
	 */
	public void setDistributorID(String distributorID) {
		this.distributorID = distributorID;
	}

	/**
	 * @return the alADId
	 */
	public ArrayList getAlADId() {
		return alADId;
	}

	/**
	 * @param alADId the alADId to set
	 */
	public void setAlADId(ArrayList alADId) {
		this.alADId = alADId;
	}

	/**
	 * @return the alVPId
	 */
	public List getAlVPId() {
		return alVPId;
	}

	/**
	 * @param alVPId the alVPId to set
	 */
	public void setAlVPId(List alVPId) {
		this.alVPId = alVPId;
	}

	/**
	 * @return the alAccount
	 */
	public ArrayList getAlAccount() {
		return alAccount;
	}

	/**
	 * @param alAccount the alAccount to set
	 */
	public void setAlAccount(ArrayList alAccount) {
		this.alAccount = alAccount;
	}

	/**
	 * @return the alDistributor
	 */
	public List getAlDistributor() {
		return alDistributor;
	}

	/**
	 * @param alDistributor the alDistributor to set
	 */
	public void setAlDistributor(List alDistributor) {
		this.alDistributor = alDistributor;
	}

	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}

	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}

	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

	/**
	 * @return the checkAccounts
	 */
	public String[] getCheckAccounts() {
		return checkAccounts;
	}

	/**
	 * @param checkAccounts the checkAccounts to set
	 */
	public void setCheckAccounts(String[] checkAccounts) {
		this.checkAccounts = checkAccounts;
	}

	/**
	 * @return the selectAcctAll
	 */
	public String getSelectAcctAll() {
		return selectAcctAll;
	}

	/**
	 * @param selectAcctAll the selectAcctAll to set
	 */
	public void setSelectAcctAll(String selectAcctAll) {
		this.selectAcctAll = selectAcctAll;
	}

	/**
	 * @return the hsets
	 */
	public String getHsets() {
		return hsets;
	}

	/**
	 * @param hsets the hsets to set
	 */
	public void setHsets(String hsets) {
		this.hsets = hsets;
	}

	/**
	 * @return the strDetailFlag
	 */
	public String getStrDetailFlag() {
		return strDetailFlag;
	}

	/**
	 * @param strDetailFlag the strDetailFlag to set
	 */
	public void setStrDetailFlag(String strDetailFlag) {
		this.strDetailFlag = strDetailFlag;
	}

	/**
	 * @return the hinputstring
	 */
	public String getHinputstring() {
		return hinputstring;
	}

	/**
	 * @param hinputstring the hinputstring to set
	 */
	public void setHinputstring(String hinputstring) {
		this.hinputstring = hinputstring;
	}

	/**
	 * @return the priceRequestStatusXML
	 */
	public String getPriceRequestStatusXML() {
		return priceRequestStatusXML;
	}

	/**
	 * @param priceRequestStatusXML the priceRequestStatusXML to set
	 */
	public void setPriceRequestStatusXML(String priceRequestStatusXML) {
		this.priceRequestStatusXML = priceRequestStatusXML;
	}

	/**
	 * @return the pendingSystemAccountPriceGridXML
	 */
	public String getPendingSystemAccountPriceGridXML() {
		return pendingSystemAccountPriceGridXML;
	}

	/**
	 * @param pendingSystemAccountPriceGridXML the pendingSystemAccountPriceGridXML to set
	 */
	public void setPendingSystemAccountPriceGridXML(String pendingSystemAccountPriceGridXML) {
		this.pendingSystemAccountPriceGridXML = pendingSystemAccountPriceGridXML;
	}

	/**
	 * @return the pendingSystemAccountCount
	 */
	public String getPendingSystemAccountCount() {
		return pendingSystemAccountCount;
	}

	/**
	 * @param pendingSystemAccountCount the pendingSystemAccountCount to set
	 */
	public void setPendingSystemAccountCount(String pendingSystemAccountCount) {
		this.pendingSystemAccountCount = pendingSystemAccountCount;
	}

	/**
	 * @return the pendingGroupAccountPriceGridXML
	 */
	public String getPendingGroupAccountPriceGridXML() {
		return pendingGroupAccountPriceGridXML;
	}

	/**
	 * @param pendingGroupAccountPriceGridXML the pendingGroupAccountPriceGridXML to set
	 */
	public void setPendingGroupAccountPriceGridXML(String pendingGroupAccountPriceGridXML) {
		this.pendingGroupAccountPriceGridXML = pendingGroupAccountPriceGridXML;
	}

	/**
	 * @return the pendingGroupAccountCount
	 */
	public String getPendingGroupAccountCount() {
		return pendingGroupAccountCount;
	}

	/**
	 * @param pendingGroupAccountCount the pendingGroupAccountCount to set
	 */
	public void setPendingGroupAccountCount(String pendingGroupAccountCount) {
		this.pendingGroupAccountCount = pendingGroupAccountCount;
	}

	public String[] getCheckVPs() {
		return checkVPs;
	}

	public void setCheckVPs(String[] checkVPs) {
		this.checkVPs = checkVPs;
	}

	public String[] getCheckADs() {
		return checkADs;
	}

	public void setCheckADs(String[] checkADs) {
		this.checkADs = checkADs;
	}

	public String[] getCheckFSs() {
		return checkFSs;
	}

	public void setCheckFSs(String[] checkFSs) {
		this.checkFSs = checkFSs;
	}

	public String[] getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String[] checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String[] getCheckSystems() {
		return checkSystems;
	}

	public void setCheckSystems(String[] checkSystems) {
		this.checkSystems = checkSystems;
	}

	public ArrayList getAlSystem() {
		return alSystem;
	}

	public void setAlSystem(ArrayList alSystem) {
		this.alSystem = alSystem;
	}

	public String getSelectVPAll() {
		return selectVPAll;
	}

	public void setSelectVPAll(String selectVPAll) {
		this.selectVPAll = selectVPAll;
	}

	public String getSelectADAll() {
		return selectADAll;
	}

	public void setSelectADAll(String selectADAll) {
		this.selectADAll = selectADAll;
	}

	public String getSelectFSAll() {
		return selectFSAll;
	}

	public void setSelectFSAll(String selectFSAll) {
		this.selectFSAll = selectFSAll;
	}

	public String getSelectStatusAll() {
		return selectStatusAll;
	}

	public void setSelectStatusAll(String selectStatusAll) {
		this.selectStatusAll = selectStatusAll;
	}

	public String getSelectSystemAll() {
		return selectSystemAll;
	}

	public void setSelectSystemAll(String selectSystemAll) {
		this.selectSystemAll = selectSystemAll;
	}

	public ArrayList getAlGrpDateRange() {
		return alGrpDateRange;
	}

	public void setAlGrpDateRange(ArrayList alGrpDateRange) {
		this.alGrpDateRange = alGrpDateRange;
	}

	public String getGrpDateRange() {
		return grpDateRange;
	}

	public void setGrpDateRange(String grpDateRange) {
		this.grpDateRange = grpDateRange;
	}

	public String getSelectedVP() {
		return selectedVP;
	}

	public void setSelectedVP(String selectedVP) {
		this.selectedVP = selectedVP;
	}

	public String getSelectedAD() {
		return selectedAD;
	}

	public void setSelectedAD(String selectedAD) {
		this.selectedAD = selectedAD;
	}

	public String getSelectedFS() {
		return selectedFS;
	}

	public void setSelectedFS(String selectedFS) {
		this.selectedFS = selectedFS;
	}

	public String getSelectedAC() {
		return selectedAC;
	}

	public void setSelectedAC(String selectedAC) {
		this.selectedAC = selectedAC;
	}
	
	 
    
   
}
