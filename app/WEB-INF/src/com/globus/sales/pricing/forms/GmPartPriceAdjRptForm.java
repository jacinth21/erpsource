package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmLogForm;

public class GmPartPriceAdjRptForm extends GmLogForm{
	
	private String xmlGridData = "";
	private String partNum = "";
	private String type = "";
	private String accName = "";
	private String grpAccName = "";
	private String accId = "";
	private String partLike = "";
	private String queryByType = "";
	private String systemList = "";
	private String projectList = "";
	private String grpAccCodeNm = "";
	private String partyId = "";
		
	private ArrayList alAdjCodeLst = new ArrayList();
	private ArrayList alType = new ArrayList();
	private ArrayList alAccName = new ArrayList();
	private ArrayList alGrpAccName = new ArrayList();
	private ArrayList alLikeOptions = new ArrayList();
	private ArrayList alProjectList = new ArrayList();
	private ArrayList alSystemList = new ArrayList();
	private ArrayList alQueryTypeList = new ArrayList();
	
	private List ldtResult = new ArrayList();
	private List ldtHistoryResult = new ArrayList();

	
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}

	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}

	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}

	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the accName
	 */
	public String getAccName() {
		return accName;
	}

	/**
	 * @param accName the accName to set
	 */
	public void setAccName(String accName) {
		this.accName = accName;
	}

	/**
	 * @return the grpAccName
	 */
	public String getGrpAccName() {
		return grpAccName;
	}

	/**
	 * @param grpAccName the grpAccName to set
	 */
	public void setGrpAccName(String grpAccName) {
		this.grpAccName = grpAccName;
	}

	/**
	 * @return the accId
	 */
	public String getAccId() {
		return accId;
	}

	/**
	 * @param accId the accId to set
	 */
	public void setAccId(String accId) {
		this.accId = accId;
	}

	/**
	 * @return the partLike
	 */
	public String getPartLike() {
		return partLike;
	}

	/**
	 * @param partLike the partLike to set
	 */
	public void setPartLike(String partLike) {
		this.partLike = partLike;
	}

	/**
	 * @return the queryByType
	 */
	public String getQueryByType() {
		return queryByType;
	}

	/**
	 * @param queryByType the queryByType to set
	 */
	public void setQueryByType(String queryByType) {
		this.queryByType = queryByType;
	}

	/**
	 * @return the systemList
	 */
	public String getSystemList() {
		return systemList;
	}

	/**
	 * @param systemList the systemList to set
	 */
	public void setSystemList(String systemList) {
		this.systemList = systemList;
	}

	/**
	 * @return the projectList
	 */
	public String getProjectList() {
		return projectList;
	}

	/**
	 * @param projectList the projectList to set
	 */
	public void setProjectList(String projectList) {
		this.projectList = projectList;
	}

	public String getGrpAccCodeNm() {
		return grpAccCodeNm;
	}

	public void setGrpAccCodeNm(String grpAccCodeNm) {
		this.grpAccCodeNm = grpAccCodeNm;
	}

	/**
	 * @return the partyId
	 */
	public String getPartyId() {
		return partyId;
	}

	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	/**
	 * @return the alAdjCodeLst
	 */
	public ArrayList getAlAdjCodeLst() {
		return alAdjCodeLst;
	}

	/**
	 * @param alAdjCodeLst the alAdjCodeLst to set
	 */
	public void setAlAdjCodeLst(ArrayList alAdjCodeLst) {
		this.alAdjCodeLst = alAdjCodeLst;
	}

	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}

	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}

	/**
	 * @return the alAccName
	 */
	public ArrayList getAlAccName() {
		return alAccName;
	}

	/**
	 * @param alAccName the alAccName to set
	 */
	public void setAlAccName(ArrayList alAccName) {
		this.alAccName = alAccName;
	}

	/**
	 * @return the alGrpAccName
	 */
	public ArrayList getAlGrpAccName() {
		return alGrpAccName;
	}

	/**
	 * @param alGrpAccName the alGrpAccName to set
	 */
	public void setAlGrpAccName(ArrayList alGrpAccName) {
		this.alGrpAccName = alGrpAccName;
	}

	/**
	 * @return the alLikeOptions
	 */
	public ArrayList getAlLikeOptions() {
		return alLikeOptions;
	}

	/**
	 * @param alLikeOptions the alLikeOptions to set
	 */
	public void setAlLikeOptions(ArrayList alLikeOptions) {
		this.alLikeOptions = alLikeOptions;
	}

	/**
	 * @return the alProjectList
	 */
	public ArrayList getAlProjectList() {
		return alProjectList;
	}

	/**
	 * @param alProjectList the alProjectList to set
	 */
	public void setAlProjectList(ArrayList alProjectList) {
		this.alProjectList = alProjectList;
	}

	/**
	 * @return the alSystemList
	 */
	public ArrayList getAlSystemList() {
		return alSystemList;
	}

	/**
	 * @param alSystemList the alSystemList to set
	 */
	public void setAlSystemList(ArrayList alSystemList) {
		this.alSystemList = alSystemList;
	}

	/**
	 * @return the alQueryTypeList
	 */
	public ArrayList getAlQueryTypeList() {
		return alQueryTypeList;
	}

	/**
	 * @param alQueryTypeList the alQueryTypeList to set
	 */
	public void setAlQueryTypeList(ArrayList alQueryTypeList) {
		this.alQueryTypeList = alQueryTypeList;
	}

	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}

	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

	/**
	 * @return the ldtHistoryResult
	 */
	public List getLdtHistoryResult() {
		return ldtHistoryResult;
	}

	/**
	 * @param ldtHistoryResult the ldtHistoryResult to set
	 */
	public void setLdtHistoryResult(List ldtHistoryResult) {
		this.ldtHistoryResult = ldtHistoryResult;
	}
	
}
