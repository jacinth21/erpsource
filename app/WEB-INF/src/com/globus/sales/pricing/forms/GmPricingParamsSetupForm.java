package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmLogForm;

public class GmPricingParamsSetupForm extends GmLogForm {

	private String accid = "";
	private String partyId = "";

	private String idn = "";
	private String strContract = "";
	private String strContractNm = "";
	private String gpo = "";
	private String grouppricebook = "";
	private String wasted = "";
	private String revision = "";
	private String hScreenType = "";
	private String hAccAttInputStr = "";
	private String hPriceAdjInputStr = "";
	private String showWasedHistory = "";
	private String showRevisionHistory = "";

	private String wastedPrice = "";
	private String revisionPrice = "";

	private String wastePrcAdjID = "";
	private String revisionPrcAdjID = "";

	private String auditTrailId = "";
	private String auditTrailTransID = "";

	private String adminFeeFlag = "";
	private String adminFee = "";
	
	private String strId="";
	private String strDetails="";
	
	private String groupPriceBookName="";
	 //PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen
	private String rpcName="";
	private String rpc="";
	private String hlthName="";
	private String hlthId="";
	/**
	 * @return the rpcName
	 */
	public String getRpcName() {
		return rpcName;
	}

	/**
	 * @param rpcName the rpcName to set
	 */
	public void setRpcName(String rpcName) {
		this.rpcName = rpcName;
	}

	/**
	 * @return the rpc
	 */
	public String getRpc() {
		return rpc;
	}

	/**
	 * @param rpc the rpc to set
	 */
	public void setRpc(String rpc) {
		this.rpc = rpc;
	}

	/**
	 * @return the hlthName
	 */
	public String getHlthName() {
		return hlthName;
	}

	/**
	 * @param hlthName the hlthName to set
	 */
	public void setHlthName(String hlthName) {
		this.hlthName = hlthName;
	}

	/**
	 * @return the hlthId
	 */
	public String getHlthId() {
		return hlthId;
	}

	/**
	 * @param hlthId the hlthId to set
	 */
	public void setHlthId(String hlthId) {
		this.hlthId = hlthId;
	}
	 //PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen
	public String getStrId() {
		return strId;
	}

	public void setStrId(String strId) {
		this.strId = strId;
	}

	public String getStrDetails() {
		return strDetails;
	}

	public void setStrDetails(String strDetails) {
		this.strDetails = strDetails;
	}
	private String idnName="";
	private String gpoName="";

	private ArrayList alGroupPriceBook = new ArrayList();
	private ArrayList alIdn = new ArrayList();
	private ArrayList alGpo = new ArrayList();
	private ArrayList alContract = new ArrayList();

	private ArrayList alWastePriceList = new ArrayList();
	private ArrayList alRevisionPriceList = new ArrayList();

	private ArrayList alAdminFeeFlag = new ArrayList();

	private List ldtHistoryResult = new ArrayList();

	/**
	 * @return the accid
	 */
	public String getAccid() {
		return accid;
	}

	/**
	 * @param accid
	 *            the accid to set
	 */
	public void setAccid(String accid) {
		this.accid = accid;
	}

	/**
	 * @return the partyId
	 */
	public String getPartyId() {
		return partyId;
	}

	/**
	 * @param partyId
	 *            the partyId to set
	 */
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	/**
	 * @return the idn
	 */
	public String getIdn() {
		return idn;
	}

	/**
	 * @param idn
	 *            the idn to set
	 */
	public void setIdn(String idn) {
		this.idn = idn;
	}

	/**
	 * @return the strContract
	 */
	public String getStrContract() {
		return strContract;
	}

	/**
	 * @param strContract
	 *            the strContract to set
	 */
	public void setStrContract(String strContract) {
		this.strContract = strContract;
	}

	/**
	 * @return the strContractNm
	 */
	public String getStrContractNm() {
		return strContractNm;
	}

	/**
	 * @param strContractNm
	 *            the strContractNm to set
	 */
	public void setStrContractNm(String strContractNm) {
		this.strContractNm = strContractNm;
	}

	/**
	 * @return the gpo
	 */
	public String getGpo() {
		return gpo;
	}

	/**
	 * @param gpo
	 *            the gpo to set
	 */
	public void setGpo(String gpo) {
		this.gpo = gpo;
	}

	/**
	 * @return the grouppricebook
	 */
	public String getGrouppricebook() {
		return grouppricebook;
	}

	/**
	 * @param grouppricebook
	 *            the grouppricebook to set
	 */
	public void setGrouppricebook(String grouppricebook) {
		this.grouppricebook = grouppricebook;
	}

	/**
	 * @return the wasted
	 */
	public String getWasted() {
		return wasted;
	}

	/**
	 * @param wasted
	 *            the wasted to set
	 */
	public void setWasted(String wasted) {
		this.wasted = wasted;
	}

	/**
	 * @return the revision
	 */
	public String getRevision() {
		return revision;
	}

	/**
	 * @param revision
	 *            the revision to set
	 */
	public void setRevision(String revision) {
		this.revision = revision;
	}

	/**
	 * @return the hScreenType
	 */
	public String gethScreenType() {
		return hScreenType;
	}

	/**
	 * @param hScreenType
	 *            the hScreenType to set
	 */
	public void sethScreenType(String hScreenType) {
		this.hScreenType = hScreenType;
	}

	/**
	 * @return the hAccAttInputStr
	 */
	public String gethAccAttInputStr() {
		return hAccAttInputStr;
	}

	/**
	 * @param hAccAttInputStr
	 *            the hAccAttInputStr to set
	 */
	public void sethAccAttInputStr(String hAccAttInputStr) {
		this.hAccAttInputStr = hAccAttInputStr;
	}

	/**
	 * @return the hPriceAdjInputStr
	 */
	public String gethPriceAdjInputStr() {
		return hPriceAdjInputStr;
	}

	/**
	 * @param hPriceAdjInputStr
	 *            the hPriceAdjInputStr to set
	 */
	public void sethPriceAdjInputStr(String hPriceAdjInputStr) {
		this.hPriceAdjInputStr = hPriceAdjInputStr;
	}

	/**
	 * @return the showWasedHistory
	 */
	public String getShowWasedHistory() {
		return showWasedHistory;
	}

	/**
	 * @param showWasedHistory
	 *            the showWasedHistory to set
	 */
	public void setShowWasedHistory(String showWasedHistory) {
		this.showWasedHistory = showWasedHistory;
	}

	/**
	 * @return the showRevisionHistory
	 */
	public String getShowRevisionHistory() {
		return showRevisionHistory;
	}

	/**
	 * @param showRevisionHistory
	 *            the showRevisionHistory to set
	 */
	public void setShowRevisionHistory(String showRevisionHistory) {
		this.showRevisionHistory = showRevisionHistory;
	}

	/**
	 * @return the wastedPrice
	 */
	public String getWastedPrice() {
		return wastedPrice;
	}

	/**
	 * @param wastedPrice
	 *            the wastedPrice to set
	 */
	public void setWastedPrice(String wastedPrice) {
		this.wastedPrice = wastedPrice;
	}

	/**
	 * @return the revisionPrice
	 */
	public String getRevisionPrice() {
		return revisionPrice;
	}

	/**
	 * @param revisionPrice
	 *            the revisionPrice to set
	 */
	public void setRevisionPrice(String revisionPrice) {
		this.revisionPrice = revisionPrice;
	}

	/**
	 * @return the wastePrcAdjID
	 */
	public String getWastePrcAdjID() {
		return wastePrcAdjID;
	}

	/**
	 * @param wastePrcAdjID
	 *            the wastePrcAdjID to set
	 */
	public void setWastePrcAdjID(String wastePrcAdjID) {
		this.wastePrcAdjID = wastePrcAdjID;
	}

	/**
	 * @return the revisionPrcAdjID
	 */
	public String getRevisionPrcAdjID() {
		return revisionPrcAdjID;
	}

	/**
	 * @param revisionPrcAdjID
	 *            the revisionPrcAdjID to set
	 */
	public void setRevisionPrcAdjID(String revisionPrcAdjID) {
		this.revisionPrcAdjID = revisionPrcAdjID;
	}

	/**
	 * @return the adminFeeFlag
	 */
	public String getAdminFeeFlag() {
		return adminFeeFlag;
	}

	/**
	 * @param adminFeeFlag
	 *            the adminFeeFlag to set
	 */
	public void setAdminFeeFlag(String adminFeeFlag) {
		this.adminFeeFlag = adminFeeFlag;
	}

	/**
	 * @return the adminFee
	 */
	public String getAdminFee() {
		return adminFee;
	}

	/**
	 * @param adminFee
	 *            the adminFee to set
	 */
	public void setAdminFee(String adminFee) {
		this.adminFee = adminFee;
	}

	/**
	 * @return the alGroupPriceBook
	 */
	public ArrayList getAlGroupPriceBook() {
		return alGroupPriceBook;
	}

	/**
	 * @param alGroupPriceBook
	 *            the alGroupPriceBook to set
	 */
	public void setAlGroupPriceBook(ArrayList alGroupPriceBook) {
		this.alGroupPriceBook = alGroupPriceBook;
	}

	/**
	 * @return the alIdn
	 */
	public ArrayList getAlIdn() {
		return alIdn;
	}

	/**
	 * @param alIdn
	 *            the alIdn to set
	 */
	public void setAlIdn(ArrayList alIdn) {
		this.alIdn = alIdn;
	}

	/**
	 * @return the alGpo
	 */
	public ArrayList getAlGpo() {
		return alGpo;
	}

	/**
	 * @param alGpo
	 *            the alGpo to set
	 */
	public void setAlGpo(ArrayList alGpo) {
		this.alGpo = alGpo;
	}

	/**
	 * @return the alContract
	 */
	public ArrayList getAlContract() {
		return alContract;
	}

	/**
	 * @param alContract
	 *            the alContract to set
	 */
	public void setAlContract(ArrayList alContract) {
		this.alContract = alContract;
	}

	/**
	 * @return the alWastePriceList
	 */
	public ArrayList getAlWastePriceList() {
		return alWastePriceList;
	}

	/**
	 * @param alWastePriceList
	 *            the alWastePriceList to set
	 */
	public void setAlWastePriceList(ArrayList alWastePriceList) {
		this.alWastePriceList = alWastePriceList;
	}

	/**
	 * @return the alRevisionPriceList
	 */
	public ArrayList getAlRevisionPriceList() {
		return alRevisionPriceList;
	}

	/**
	 * @param alRevisionPriceList
	 *            the alRevisionPriceList to set
	 */
	public void setAlRevisionPriceList(ArrayList alRevisionPriceList) {
		this.alRevisionPriceList = alRevisionPriceList;
	}

	/**
	 * @return the auditTrailId
	 */
	public String getAuditTrailId() {
		return auditTrailId;
	}

	/**
	 * @param auditTrailId
	 *            the auditTrailId to set
	 */
	public void setAuditTrailId(String auditTrailId) {
		this.auditTrailId = auditTrailId;
	}

	/**
	 * @return the auditTrailTransID
	 */
	public String getAuditTrailTransID() {
		return auditTrailTransID;
	}

	/**
	 * @param auditTrailTransID
	 *            the auditTrailTransID to set
	 */
	public void setAuditTrailTransID(String auditTrailTransID) {
		this.auditTrailTransID = auditTrailTransID;
	}

	/**
	 * @return the ldtHistoryResult
	 */
	public List getLdtHistoryResult() {
		return ldtHistoryResult;
	}

	/**
	 * @param ldtHistoryResult
	 *            the ldtHistoryResult to set
	 */
	public void setLdtHistoryResult(List ldtHistoryResult) {
		this.ldtHistoryResult = ldtHistoryResult;
	}

	/**
	 * @return the alAdminFeeFlag
	 */
	public ArrayList getAlAdminFeeFlag() {
		return alAdminFeeFlag;
	}

	/**
	 * @param alAdminFeeFlag
	 *            the alAdminFeeFlag to set
	 */
	public void setAlAdminFeeFlag(ArrayList alAdminFeeFlag) {
		this.alAdminFeeFlag = alAdminFeeFlag;
	}

	/**
	 * @param getGroupPriceBookName
	 *           
	 */
	public String getGroupPriceBookName() {
		return groupPriceBookName;
	}
	
	/**
	 * @param setGroupPriceBookName
	 *           
	 */

	public void setGroupPriceBookName(String groupPriceBookName) {
		this.groupPriceBookName = groupPriceBookName;
	}
	/**
	 * @param getIdnname
	 *           
	 */
	public String getIdnName() {
		return idnName;
	}
	/**
	 * @param setIdnName
	 *           
	 */
	public void setIdnName(String idnName) {
		this.idnName = idnName;
	}
	/**
	 * @param getGpoName
	 *           
	 */
	public String getGpoName() {
		return gpoName;
	}
	/**
	 * @param setGpoName
	 *           
	 */
	public void setGpoName(String gpoName) {
		this.gpoName = gpoName;
	}
}