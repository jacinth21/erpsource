package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonForm;

public class GmAccountGroupPriceReportForm extends GmCommonForm
{
	 
	private String accountID = ""; 
	 
	private String pnum = "";   
	private String selectGrpAll = "";
	 
	private String setID = ""; 
	private String groupID = "";
	
 
	private ArrayList alAccount = new ArrayList();
	 
	private ArrayList alSets = new ArrayList();
	private ArrayList alGroupList = new ArrayList();
	
	private String[] checkGroups = new String [10];
	
	private List ldtResult = new ArrayList();

	 
	/**
	 * @return the accountID
	 */
	public String getAccountID() {
		return accountID;
	}

	/**
	 * @param accountID the accountID to set
	 */
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	 
	/**
	 * @return the alAccount
	 */
	public ArrayList getAlAccount() {
		return alAccount;
	}

	/**
	 * @param alAccount the alAccount to set
	 */
	public void setAlAccount(ArrayList alAccount) {
		this.alAccount = alAccount;
	}

 
	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}

	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}
 
	 
	/**
	 * @return the selectGrpAll
	 */
	public String getSelectGrpAll() {
		return selectGrpAll;
	}

	/**
	 * @param selectGrpAll the selectGrpAll to set
	 */
	public void setSelectGrpAll(String selectGrpAll) {
		this.selectGrpAll = selectGrpAll;
	}

	/**
	 * @return the checkGroups
	 */
	public String[] getCheckGroups() {
		return checkGroups;
	}

	/**
	 * @param checkGroups the checkGroups to set
	 */
	public void setCheckGroups(String[] checkGroups) {
		this.checkGroups = checkGroups;
	}

	 
	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}

	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}

	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}

	/**
	 * @return the alSets
	 */
	public ArrayList getAlSets() {
		return alSets;
	}

	/**
	 * @param alSets the alSets to set
	 */
	public void setAlSets(ArrayList alSets) {
		this.alSets = alSets;
	}

	/**
	 * @return the groupID
	 */
	public String getGroupID() {
		return groupID;
	}

	/**
	 * @param groupID the groupID to set
	 */
	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	/**
	 * @return the alGroupList
	 */
	public ArrayList getAlGroupList() {
		return alGroupList;
	}

	/**
	 * @param alGroupList the alGroupList to set
	 */
	public void setAlGroupList(ArrayList alGroupList) {
		this.alGroupList = alGroupList;
	}
	
	 
    
   
}
