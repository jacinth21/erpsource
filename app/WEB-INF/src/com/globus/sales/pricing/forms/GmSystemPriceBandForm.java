package com.globus.sales.pricing.forms;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.globus.common.forms.GmCommonForm;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;

public class GmSystemPriceBandForm extends GmCommonForm
{
	private String EMPTY_STRING = "";
	 
    private String hinputString = EMPTY_STRING; 
    private String hsystemPriorityString = EMPTY_STRING; 
    private String gridXmlData = EMPTY_STRING;
    private String enableADVPChange = EMPTY_STRING;
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    
    public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	 

	public String getHinputString() {
		return hinputString;
	}

	public void setHinputString(String inputString) {
		hinputString = inputString;
	}

	public String getEnableADVPChange() {
		return enableADVPChange;
	}

	public void setEnableADVPChange(String enableADVPChange) {
		this.enableADVPChange = enableADVPChange;
	}

	public String getHsystemPriorityString() {
		return hsystemPriorityString;
	}

	public void setHsystemPriorityString(String hsystemPriorityString) {
		this.hsystemPriorityString = hsystemPriorityString;
	}

}
