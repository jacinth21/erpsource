package com.globus.sales.pricing.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonForm;

public class GmPricingAnnualChangeForm extends GmCommonForm
{
	private String adID = ""; 
	private String changeType = ""; 
	private String accountID = ""; 
	private String changeValue = ""; 
	private String distributorID = ""; 
	private String effectiveDate = ""; 
	private String selectAcctAll = "";
	private String selectSetAll = "";
	private String setID = ""; 
	private String selectAll = "";
	private String haccounts = "";
	private String strEnableSubmit = "false";
	
    private ArrayList alADId = new ArrayList();
	private List alChangeType = new ArrayList();
	private ArrayList alAccount = new ArrayList();
	private List alDistributor = new ArrayList(); 
	private ArrayList alSets = new ArrayList();
	
	private String[] checkAccounts = new String [10];
	private String[] checkSelectedSets = new String [10]; 
	/**
	 * @return the adID
	 */
	public String getAdID() {
		return adID;
	}

	/**
	 * @param adID the adID to set
	 */
	public void setAdID(String adID) {
		this.adID = adID;
	}

	 

	/**
	 * @return the accountID
	 */
	public String getAccountID() {
		return accountID;
	}

	/**
	 * @param accountID the accountID to set
	 */
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
 

	/**
	 * @return the distributorID
	 */
	public String getDistributorID() {
		return distributorID;
	}

	/**
	 * @param distributorID the distributorID to set
	 */
	public void setDistributorID(String distributorID) {
		this.distributorID = distributorID;
	}

	/**
	 * @return the alADId
	 */
	public ArrayList getAlADId() {
		return alADId;
	}

	/**
	 * @param alADId the alADId to set
	 */
	public void setAlADId(ArrayList alADId) {
		this.alADId = alADId;
	}
 

	/**
	 * @return the alAccount
	 */
	public ArrayList getAlAccount() {
		return alAccount;
	}

	/**
	 * @param alAccount the alAccount to set
	 */
	public void setAlAccount(ArrayList alAccount) {
		this.alAccount = alAccount;
	}

	/**
	 * @return the alDistributor
	 */
	public List getAlDistributor() {
		return alDistributor;
	}

	/**
	 * @param alDistributor the alDistributor to set
	 */
	public void setAlDistributor(List alDistributor) {
		this.alDistributor = alDistributor;
	}

	/**
	 * @return the changeType
	 */
	public String getChangeType() {
		return changeType;
	}

	/**
	 * @param changeType the changeType to set
	 */
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	/**
	 * @return the changeValue
	 */
	public String getChangeValue() {
		return changeValue;
	}

	/**
	 * @param changeValue the changeValue to set
	 */
	public void setChangeValue(String changeValue) {
		this.changeValue = changeValue;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the alChangeType
	 */
	public List getAlChangeType() {
		return alChangeType;
	}

	/**
	 * @param alChangeType the alChangeType to set
	 */
	public void setAlChangeType(List alChangeType) {
		this.alChangeType = alChangeType;
	}

	/**
	 * @return the checkAccounts
	 */
	public String[] getCheckAccounts() {
		return checkAccounts;
	}

	/**
	 * @param checkAccounts the checkAccounts to set
	 */
	public void setCheckAccounts(String[] checkAccounts) {
		this.checkAccounts = checkAccounts;
	}

	 

	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}

	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}

	/**
	 * @return the checkSelectedSets
	 */
	public String[] getCheckSelectedSets() {
		return checkSelectedSets;
	}

	/**
	 * @param checkSelectedSets the checkSelectedSets to set
	 */
	public void setCheckSelectedSets(String[] checkSelectedSets) {
		this.checkSelectedSets = checkSelectedSets;
	}

	/**
	 * @return the alSets
	 */
	public ArrayList getAlSets() {
		return alSets;
	}

	/**
	 * @param alSets the alSets to set
	 */
	public void setAlSets(ArrayList alSets) {
		this.alSets = alSets;
	}

	/**
	 * @return the selectAcctAll
	 */
	public String getSelectAcctAll() {
		return selectAcctAll;
	}

	/**
	 * @param selectAcctAll the selectAcctAll to set
	 */
	public void setSelectAcctAll(String selectAcctAll) {
		this.selectAcctAll = selectAcctAll;
	}

	/**
	 * @return the selectSetAll
	 */
	public String getSelectSetAll() {
		return selectSetAll;
	}

	/**
	 * @param selectSetAll the selectSetAll to set
	 */
	public void setSelectSetAll(String selectSetAll) {
		this.selectSetAll = selectSetAll;
	}

	/**
	 * @return the selectAll
	 */
	public String getSelectAll() {
		return selectAll;
	}

	/**
	 * @param selectAll the selectAll to set
	 */
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}

	/**
	 * @return the haccounts
	 */
	public String getHaccounts() {
		return haccounts;
	}

	/**
	 * @param haccounts the haccounts to set
	 */
	public void setHaccounts(String haccounts) {
		this.haccounts = haccounts;
	}

	/**
	 * @return the strEnableSubmit
	 */
	public String getStrEnableSubmit() {
		return strEnableSubmit;
	}

	/**
	 * @param strEnableSubmit the strEnableSubmit to set
	 */
	public void setStrEnableSubmit(String strEnableSubmit) {
		this.strEnableSubmit = strEnableSubmit;
	}

 
 
   
}
