package com.globus.sales.pricing.forms;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.globus.common.forms.GmCancelForm;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;

public class GmSalesPricingFileGenForm extends GmCancelForm
{
	 private String genPdf = "";
	 private String genExcel = "";
	 private String inputStringAccId = "";
	/**
	 * @return the genPdf
	 */
	public String getGenPdf() {
		return genPdf;
	}
	/**
	 * @param genPdf the genPdf to set
	 */
	public void setGenPdf(String genPdf) {
		this.genPdf = genPdf;
	}
	/**
	 * @return the genExcel
	 */
	public String getGenExcel() {
		return genExcel;
	}
	/**
	 * @param genExcel the genExcel to set
	 */
	public void setGenExcel(String genExcel) {
		this.genExcel = genExcel;
	}
	/**
	 * @return the inputStringAccId
	 */
	public String getInputStringAccId() {
		return inputStringAccId;
	}
	/**
	 * @param inputStringAccId the inputStringAccId to set
	 */
	public void setInputStringAccId(String inputStringAccId) {
		this.inputStringAccId = inputStringAccId;
	}

}
