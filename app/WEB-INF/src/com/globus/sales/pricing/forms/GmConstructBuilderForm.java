package com.globus.sales.pricing.forms;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.globus.common.forms.GmCancelForm;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;

public class GmConstructBuilderForm extends GmCancelForm
{
	private String EMPTY_STRING = "";
	private String systemListId = EMPTY_STRING;
    private String constructId = "0";
    private String hconstructName = EMPTY_STRING;
    private String constLevelId = EMPTY_STRING; 
    private String newConstructFlag = EMPTY_STRING;
    private String statusId = EMPTY_STRING;
    private String groupTypeId = EMPTY_STRING;
    private String hinputString = EMPTY_STRING;
    private String effectiveDate = GmCalenderOperations.getCurrentDate("MM/dd/yyyy");
    private String voidAccess="";
    
    private String principalConstruct = EMPTY_STRING;
    private String strEnable = EMPTY_STRING;
    
    private String[] checkGroup = new String [10];
    
    private ArrayList alSystemList = null;
    private ArrayList alConstLevelList = null;
    private ArrayList alConstructList = null;
    private ArrayList alStatusList = null;
    
    private ArrayList alGrpTypeList = null;
    private ArrayList alGroupList = new ArrayList();
    
    private String gridXmlData = EMPTY_STRING;
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    
    public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
     * @return Returns the alGroupList.
     */
    public ArrayList getAlGroupList()
    {
        return alGroupList;
    }

    /**
     * @param alGroupList The alGroupList to set.
     */
    public void setAlGroupList(ArrayList alGroupList)
    {
        this.alGroupList = alGroupList;
    }

	public String getSystemListId() {
		return systemListId;
	}

	public void setSystemListId(String systemListId) {
		this.systemListId = systemListId;
	}

	public String getConstructId() {
		return constructId;
	}

	public void setConstructId(String constructId) {
		this.constructId = constructId;
	}

	
	public String getConstLevelId() {
		return constLevelId;
	}

	public void setConstLevelId(String constLevelId) {
		this.constLevelId = constLevelId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public ArrayList getAlSystemList() {
		return alSystemList;
	}

	public void setAlSystemList(ArrayList alSystemList) {
		this.alSystemList = alSystemList;
	}

	public ArrayList getAlConstLevelList() {
		return alConstLevelList;
	}

	public void setAlConstLevelList(ArrayList alConstLevelList) {
		this.alConstLevelList = alConstLevelList;
	}

	public ArrayList getAlConstructList() {
		return alConstructList;
	}

	public void setAlConstructList(ArrayList alConstructList) {
		this.alConstructList = alConstructList;
	}

	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}

	public ArrayList getAlGrpTypeList() {
		return alGrpTypeList;
	}

	public void setAlGrpTypeList(ArrayList alGrpTypeList) {
		this.alGrpTypeList = alGrpTypeList;
	}

	public String getGroupTypeId() {
		return groupTypeId;
	}

	public void setGroupTypeId(String groupTypeId) {
		this.groupTypeId = groupTypeId;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getNewConstructFlag() {
		return newConstructFlag;
	}

	public void setNewConstructFlag(String newConstructFlag) {
		this.newConstructFlag = newConstructFlag;
	}

	public String[] getCheckGroup() {
		return checkGroup;
	}

	public void setCheckGroup(String[] checkGroup) {
		this.checkGroup = checkGroup;
	}

	public String getHconstructName() {
		return hconstructName;
	}

	public void setHconstructName(String constructName) {
		hconstructName = constructName;
	}

	public String getHinputString() {
		return hinputString;
	}

	public void setHinputString(String inputString) {
		hinputString = inputString;
	}

	/**
	 * @return the principalConstruct
	 */
	public String getPrincipalConstruct() {
		return principalConstruct;
	}

	/**
	 * @param principalConstruct the principalConstruct to set
	 */
	public void setPrincipalConstruct(String principalConstruct) {
		this.principalConstruct = principalConstruct;
	}

	/**
	 * @return the strEnable
	 */
	public String getStrEnable() {
		return strEnable;
	}

	/**
	 * @param strEnable the strEnable to set
	 */
	public void setStrEnable(String strEnable) {
		this.strEnable = strEnable;
	}

	/**
	 * @return the voidAccess
	 */
	public String getVoidAccess() {
		return voidAccess;
	}

	/**
	 * @param voidAccess the voidAccess to set
	 */
	public void setVoidAccess(String voidAccess) {
		this.voidAccess = voidAccess;
	}

}
