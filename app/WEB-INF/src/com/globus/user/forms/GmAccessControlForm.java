package com.globus.user.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmAccessControlForm extends GmCommonForm {
	private String groupId="";
	private String groupName="";
	private String groupDesc="";
	private String gridXmlData = "";
	private String groupType="";
	
	/*TO Enable/Disable Button Access-PMT 26730 -System Manager Changes*/
	private String strAccessFl="";
	private String hstrAccessFl="";
	
	private ArrayList alGroupName=new ArrayList();
	/**
	 * @return the alGroupName
	 */
	public ArrayList getAlGroupName() {
		return alGroupName;
	}
	/**
	 * @param alGroupName the alGroupName to set
	 */
	public void setAlGroupName(ArrayList alGroupName) {
		this.alGroupName = alGroupName;
	}
	/**
	 * @return the groupType
	 */
	public String getGroupType() {
		return groupType;
	}
	/**
	 * @param groupType the groupType to set
	 */
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * @return the groupDesc
	 */
	public String getGroupDesc() {
		return groupDesc;
	}
	/**
	 * @param groupDesc the groupDesc to set
	 */
	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}
	/**
	 * @return the strAccessFl
	 */
	public String getStrAccessFl() {
		return strAccessFl;
	}
	/**
	 * @param strAccessFl the strAccessFl to set
	 */
	public void setStrAccessFl(String strAccessFl) {
		this.strAccessFl = strAccessFl;
	}
	/**
	 * @return the hstrAccessFl
	 */
	public String getHstrAccessFl() {
		return hstrAccessFl;
	}
	/**
	 * @param hstrAccessFl the hstrAccessFl to set
	 */
	public void setHstrAccessFl(String hstrAccessFl) {
		this.hstrAccessFl = hstrAccessFl;
	}
	

}
