package com.globus.user.forms;

import com.globus.common.forms.GmCommonForm;


public class GmUserADStatusUpdateForm extends GmCommonForm
{
    
    private String gridXmlData = "";
    private String hinputString = "";
    private String searchUser="";
        
	public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	public String getHinputString() {
		return hinputString;
	}

	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}

	public String getSearchUser() {
		return searchUser;
	}

	public void setSearchUser(String searchUser) {
		this.searchUser = searchUser;
	}

}
