package com.globus.user.forms;

import java.util.ArrayList;
import com.globus.common.forms.GmCommonForm;


public class GmUserLockReportForm extends GmCommonForm
{
    
    private String gridXmlData = "";
    private String hinputString = "";
        
	public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	public String getHinputString() {
		return hinputString;
	}

	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}

}

