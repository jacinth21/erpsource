package com.globus.user.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;


public class GmUserADStatusReportForm extends GmCommonForm
{
    
    private String gridXmlData = "";
    
    private ArrayList alUserList = new ArrayList();
    private ArrayList alStatusList = new ArrayList();
    private String selectedUser="";
    private String selectedStatus="";    
    
        
	public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	public ArrayList getAlUserList() {
		return alUserList;
	}

	public void setAlUserList(ArrayList alUserList) {
		this.alUserList = alUserList;
	}

	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}

	public String getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(String selectedUser) {
		this.selectedUser = selectedUser;
	}

	public String getSelectedStatus() {
		return selectedStatus;
	}

	public void setSelectedStatus(String selectedStatus) {
		this.selectedStatus = selectedStatus;
	}


}
