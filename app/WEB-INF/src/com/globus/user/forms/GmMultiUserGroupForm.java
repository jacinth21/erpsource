package com.globus.user.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;


public class GmMultiUserGroupForm extends GmCommonForm {

  private String userDept = "";
  private String userList = "";
  private String secGrp = "";

  private String Chk_Grpsec = "";
  private String Chk_Grpuser = "";

  private String defaultgrp = "";
  private String companyId = "";
  
  private String strAccessFl="";

  private String gridXmlData="";
  private String secGrpName="";
  private String userName="";
  
 private ArrayList alCompany = new ArrayList();
public String getDefaultgrp() {
    return defaultgrp;
  }

  public void setDefaultgrp(String defaultgrp) {
    this.defaultgrp = defaultgrp;
  }

  private String userDeptId = "";
  private ArrayList alDept = new ArrayList();

  private String grpmapId = "";

  public String getGrpmapId() {
    return grpmapId;
  }

  public void setGrpmapId(String grpmapId) {
    this.grpmapId = grpmapId;
  }

  public ArrayList getAlDept() {
    return alDept;
  }

  public void setAlDept(ArrayList alDept) {
    this.alDept = alDept;
  }

  public String getUserDeptId() {
    return userDeptId;
  }

  public void setUserDeptId(String userDeptId) {
    this.userDeptId = userDeptId;
  }

  private String optVal = "";

  private ArrayList alUserList = new ArrayList();
  private ArrayList alSecGrp = new ArrayList();

  private HashMap hUserList = new HashMap();
  private HashMap hSecGrp = new HashMap();

  private String hinputstring = "";

  public String getHinputstring() {
    return hinputstring;
  }

  public void setHinputstring(String hinputstring) {
    this.hinputstring = hinputstring;
  }

  public String getOptVal() {
    return optVal;
  }

  public void setOptVal(String optVal) {
    this.optVal = optVal;
  }

  public String getChk_Grpsec() {
    return Chk_Grpsec;
  }

  public void setChk_Grpsec(String chk_Grpsec) {
    Chk_Grpsec = chk_Grpsec;
  }

  public String getChk_Grpuser() {
    return Chk_Grpuser;
  }

  public void setChk_Grpuser(String chk_Grpuser) {
    Chk_Grpuser = chk_Grpuser;
  }

  public String getUserDept() {
    return userDept;
  }

  public void setUserDept(String userDept) {
    this.userDept = userDept;
  }

  public String getUserList() {
    return userList;
  }

  /**
 * @return the gridXmlData
 */
public String getGridXmlData() {
	return gridXmlData;
}

/**
 * @return the secGrpName
 */
public String getSecGrpName() {
	return secGrpName;
}

/**
 * @param secGrpName the secGrpName to set
 */
public void setSecGrpName(String secGrpName) {
	this.secGrpName = secGrpName;
}

/**
 * @return the userName
 */
public String getUserName() {
	return userName;
}

/**
 * @param userName the userName to set
 */
public void setUserName(String userName) {
	this.userName = userName;
}

/**
 * @param gridXmlData the gridXmlData to set
 */
public void setGridXmlData(String gridXmlData) {
	this.gridXmlData = gridXmlData;
}

/**
 * @return the strAccessFl
 */
public String getStrAccessFl() {
	return strAccessFl;
}

/**
 * @param strAccessFl the strAccessFl to set
 */
public void setStrAccessFl(String strAccessFl) {
	this.strAccessFl = strAccessFl;
}

public void setUserList(String userList) {
    this.userList = userList;
  }

  public String getSecGrp() {
    return secGrp;
  }

  public void setSecGrp(String secGrp) {
    this.secGrp = secGrp;
  }

  public ArrayList getAlUserList() {
    return alUserList;
  }

  public void setAlUserList(ArrayList alUserList) {
    this.alUserList = alUserList;
  }

  public ArrayList getAlSecGrp() {
    return alSecGrp;
  }

  public void setAlSecGrp(ArrayList alSecGrp) {
    this.alSecGrp = alSecGrp;
  }

  public HashMap gethUserList() {
    return hUserList;
  }

  public void sethUserList(HashMap hUserList) {
    this.hUserList = hUserList;
  }

  public HashMap gethSecGrp() {
    return hSecGrp;
  }

  public void sethSecGrp(HashMap hSecGrp) {
    this.hSecGrp = hSecGrp;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public ArrayList getAlCompany() {
    return alCompany;
  }

  public void setAlCompany(ArrayList alCompany) {
    this.alCompany = alCompany;
  }
}
