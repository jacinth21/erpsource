package com.globus.user.forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmUserListForm extends GmLogForm {
  private String usrtype = "";
  private String usrstat = "";
  private String departmentid = "";
  private String accesslevel = "";
  private String fname = "";
  private String lname = "";
  private String gridXmlData = "";
  private String companyId = "";
  private String editUserAccess = "";
  private String switchUserAccess = "";
  private String strForwardName = "";
  private String strAssCompany = "";
  private String ldapID = "";
  private String strAccessFl="";
  private String lockPin=""; //--PMT-29459 Enable User PIN upon new user Creation

  private ArrayList alType = new ArrayList();
  private ArrayList alStatus = new ArrayList();
  private ArrayList alDept = new ArrayList();
  private ArrayList alTitle = new ArrayList();
  private ArrayList alUserType = new ArrayList();
  private ArrayList alSecGrp = new ArrayList();
  private ArrayList alCompany = new ArrayList();
  private ArrayList alAvailableComp = new ArrayList();
  private ArrayList alAsscompany = new ArrayList();
  private ArrayList alLdap = new ArrayList();



  public ArrayList getAlSecGrp() {
    return alSecGrp;
  }

  public void setAlSecGrp(ArrayList alSecGrp) {
    this.alSecGrp = alSecGrp;
  }

  // The code for edit user
  private String userStatus = "";
  private String userType = "";
  private String userLoginName = "";
  private final String huserLoginName = "";
  private String hpassword = "";

  // The code for edit user
  private String emailid = "";
  private String shortName = "";
  private String workPhone = "";
  private String title = "";
  private String huserid = "";
  private String externalAccFl = "";
  private String authtype = "";
  private String securityGrp = "";
  private String copyUserName = "";//PMT-51134
  private String hCopyUserName = "";//PMT-51134
  //PC-446: Refresh field sales - while create/update company details (redis)
  private String hOldCompanyId = "";
  private String hOldFirstName = "";
  private String hOldLastName = "";
  
  public String getSecurityGrp() {
    return securityGrp;
  }

  public void setSecurityGrp(String securityGrp) {
    this.securityGrp = securityGrp;
  }

  /**
   * @return the authtype
   */
  public String getAuthtype() {
    return authtype;
  }

  /**
   * @param authtype the authtype to set
   */
  public void setAuthtype(String authtype) {
    this.authtype = authtype;
  }

  /**
 * @return the strAccessFl
 */
public String getStrAccessFl() {
	return strAccessFl;
}

/**
 * @param strAccessFl the strAccessFl to set
 */
public void setStrAccessFl(String strAccessFl) {
	this.strAccessFl = strAccessFl;
}

/**
   * @return the externalAccFl
   */
  public String getExternalAccFl() {
    return externalAccFl;
  }

  /**
   * @param externalAccFl the externalAccFl to set
   */
  public void setExternalAccFl(String externalAccFl) {
    if (externalAccFl.equals("Y"))
      this.externalAccFl = "on";
    else
      this.externalAccFl = externalAccFl;
  }

  public String getHpassword() {
    return hpassword;
  }

  public void setHpassword(String hpassword) {
    this.hpassword = hpassword;
  }

  public ArrayList getAlTitle() {
    return alTitle;
  }

  public void setAlTitle(ArrayList alTitle) {
    this.alTitle = alTitle;
  }

  public ArrayList getAlUserType() {
    return alUserType;
  }

  public void setAlUserType(ArrayList alUserType) {
    this.alUserType = alUserType;
  }


  public String getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(String userStatus) {
    this.userStatus = userStatus;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

  public String getUserLoginName() {
    return userLoginName;
  }

  public void setUserLoginName(String userLoginName) {
    try {
      this.userLoginName = new String(userLoginName.getBytes("UTF-8"), "UTF-8");
    } catch (UnsupportedEncodingException ex) {

    }
  }


  public String getHuserLoginName() {
    return userLoginName;
  }

  public void setHuserLoginName(String userLoginName) {
    this.userLoginName = userLoginName;
  }

  /**
   * @return the emailid
   */
  public String getEmailid() {
    return emailid;
  }

  /**
   * @param emailid the emailid to set
   */
  public void setEmailid(String emailid) {
    this.emailid = emailid;
  }

  /**
   * @return the shortName
   */
  public String getShortName() {
    return shortName;
  }

  /**
   * @param shortName the shortName to set
   */
  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  /**
   * @return the workPhone
   */
  public String getWorkPhone() {
    return workPhone;
  }

  /**
   * @param workPhone the workPhone to set
   */
  public void setWorkPhone(String workPhone) {
    this.workPhone = workPhone;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the huserid
   */
  public String getHuserid() {
    return huserid;
  }

  /**
   * @param huserid the huserid to set
   */
  public void setHuserid(String huserid) {
    this.huserid = huserid;
  }


  /**
   * @return the usrtype
   */
  public String getUsrtype() {
    return usrtype;
  }

  /**
   * @param usrtype the usrtype to set
   */
  public void setUsrtype(String usrtype) {
    this.usrtype = usrtype;
  }

  /**
   * @return the usrstat
   */
  public String getUsrstat() {
    return usrstat;
  }

  /**
   * @param usrstat the usrstat to set
   */
  public void setUsrstat(String usrstat) {
    this.usrstat = usrstat;
  }

  /**
   * @return the departmentid
   */
  public String getDepartmentid() {
    return departmentid;
  }

  /**
   * @param departmentid the departmentid to set
   */
  public void setDepartmentid(String departmentid) {
    this.departmentid = departmentid;
  }

  /**
   * @return the accesslevel
   */
  public String getAccesslevel() {
    return accesslevel;
  }

  /**
   * @param accesslevel the accesslevel to set
   */
  public void setAccesslevel(String accesslevel) {
    this.accesslevel = accesslevel;
  }

  /**
   * @return the fName
   */
  public String getFname() {
    return fname;
  }

  /**
   * @param fName the fName to set
   */
  public void setFname(String fName) {
    this.fname = fName;
  }

  /**
   * @return the lName
   */
  public String getLname() {
    return lname;
  }

  /**
   * @param lName the lName to set
   */
  public void setLname(String lName) {
    this.lname = lName;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the alType
   */
  public ArrayList getAlType() {
    return alType;
  }

  /**
   * @param alType the alType to set
   */
  public void setAlType(ArrayList alType) {
    this.alType = alType;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the alDept
   */
  public ArrayList getAlDept() {
    return alDept;
  }

  /**
   * @param alDept the alDept to set
   */
  public void setAlDept(ArrayList alDept) {
    this.alDept = alDept;
  }

  public String getEditUserAccess() {
    return editUserAccess;
  }

  public void setEditUserAccess(String editUserAccess) {
    this.editUserAccess = editUserAccess;
  }

  public String getSwitchUserAccess() {
    return switchUserAccess;
  }

  public void setSwitchUserAccess(String switchUserAccess) {
    this.switchUserAccess = switchUserAccess;
  }


  public ArrayList getAlCompany() {
    return alCompany;
  }

  public void setAlCompany(ArrayList alCompany) {
    this.alCompany = alCompany;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setStrForwardName(String partstrForwardNameyComp) {
    this.strForwardName = strForwardName;
  }

  public String getStrForwardName() {
    return strForwardName;
  }

  public ArrayList getAlAvailableComp() {
    return alAvailableComp;
  }

  public void setalAvailableComp(ArrayList alAvailableComp) {
    this.alAvailableComp = alAvailableComp;
  }

  public ArrayList getAlAsscompany() {
    return alAsscompany;
  }

  public void setAlAsscompany(ArrayList alAsscompany) {
    this.alAsscompany = alAsscompany;
  }

  public String getStrAssCompany() {
    return strAssCompany;
  }

  public void setStrAssCompany(String strAssCompany) {
    this.strAssCompany = strAssCompany;
  }

  /**
   * @return the ldapID
   */
  public String getLdapID() {
    return ldapID;
  }

  /**
   * @param ldapID the ldapID to set
   */
  public void setLdapID(String ldapID) {
    this.ldapID = ldapID;
  }

  /**
   * @return the alLdap
   */
  public ArrayList getAlLdap() {
    return alLdap;
  }

  /**
   * @param alLdap the alLdap to set
   */
  public void setAlLdap(ArrayList alLdap) {
    this.alLdap = alLdap;
  }

public String getLockPin() {
	return lockPin;
}

public void setLockPin(String lockPin) {
	this.lockPin = lockPin;
}


/**
 * @return the copyUserName
 */
public String getCopyUserName() {
	return copyUserName;
}

/**
 * @param copyUserName the copyUserName to set
 */
public void setCopyUserName(String copyUserName) {
	this.copyUserName = copyUserName;
}

/**
 * @return the hCopyUserName
 */
public String gethCopyUserName() {
	return hCopyUserName;
}

/**
 * @param hCopyUserName the hCopyUserName to set
 */
public void sethCopyUserName(String hCopyUserName) {
	this.hCopyUserName = hCopyUserName;
}

/**
 * @return the hOldCompanyId
 */
public String gethOldCompanyId() {
	return hOldCompanyId;
}

/**
 * @param hOldCompanyId the hOldCompanyId to set
 */
public void sethOldCompanyId(String hOldCompanyId) {
	this.hOldCompanyId = hOldCompanyId;
}

/**
 * @return the hOldFirstName
 */
public String gethOldFirstName() {
	return hOldFirstName;
}

/**
 * @param hOldFirstName the hOldFirstName to set
 */
public void sethOldFirstName(String hOldFirstName) {
	this.hOldFirstName = hOldFirstName;
}

/**
 * @return the hOldLastName
 */
public String gethOldLastName() {
	return hOldLastName;
}

/**
 * @param hOldLastName the hOldLastName to set
 */
public void sethOldLastName(String hOldLastName) {
	this.hOldLastName = hOldLastName;
}


}
