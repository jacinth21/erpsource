package com.globus.user.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;


public class GmUserNavigationSetupForm extends GmCommonForm
{
    
    private String gridXmlData = "";
    
    private String nodeName;
    private String nodeUrl;
    private String nodeStropt;
    private String nodeId;
    private String nodeParentId;
    private String selectedGroup="";
    private String treeXmlData = "";
    private String userGroup ="";
    private String userGroupName ="";
    private String grpType="Portals: ";
    private String grpId="";
    private String funId="";
    private String myFavorites ="false";
    private String userGrpName = "";
	private String funName = "";
	private ArrayList alMenuList= new ArrayList();
	private HashMap hmAccessMap = new HashMap();
	private String strSelectedGroupId = "";
	private String strSelectedGroupName = "";
	private String strReadOnlyAccess = "";
	
	
	public String getStrSelectedGroupId() {
		return strSelectedGroupId;
	}

	public void setStrSelectedGroupId(String strSelectedGroupId) {
		this.strSelectedGroupId = strSelectedGroupId;
	}

	public String getStrSelectedGroupName() {
		return strSelectedGroupName;
	}

	public void setStrSelectedGroupName(String strSelectedGroupName) {
		this.strSelectedGroupName = strSelectedGroupName;
	}

	public String getFunName() {
		return funName;
	}

	public void setFunName(String funName) {
		this.funName = funName;
	}

	public String getUserGrpName() {
		return userGrpName;
	}

	public void setUserGrpName(String userGrpName) {
		this.userGrpName = userGrpName;
	}

	public String getMyFavorites() {
		return myFavorites;
	}

	public void setMyFavorites(String myFavorites) {
		this.myFavorites = myFavorites;
	}

	public String getGrpId() {
		return grpId;
	}

	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}

	public String getFunId() {
		return funId;
	}

	public void setFunId(String funId) {
		this.funId = funId;
	}

	/**
	 * @return the grpType
	 */
	public String getGrpType() {
		return grpType;
	}

	/**
	 * @param grpType the grpType to set
	 */
	public void setGrpType(String grpType) {
		this.grpType = grpType;
	}

	private ArrayList alGroupModuleList= new ArrayList();
    private ArrayList alUserGroup = new ArrayList();
    
    
    /**
	 * @return the userGroupName
	 */
	public String getUserGroupName() {
		return userGroupName;
	}

	/**
	 * @param userGroupName the userGroupName to set
	 */
	public void setUserGroupName(String userGroupName) {
		this.userGroupName = userGroupName;
	}
    public String getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}

	public ArrayList getAlUserGroup() {
		return alUserGroup;
	}

	public void setAlUserGroup(ArrayList alUserGroup) {
		this.alUserGroup = alUserGroup;
	}

	private ArrayList alFolderList= new ArrayList();
    private ArrayList alFileList= new ArrayList();
    
    private String selectedFile= "";
    private String selectedFolder= "";
    
    private String selectedMenuItem = "";        
	

	public ArrayList getAlGroupModuleList() {
		return alGroupModuleList;
	}

	public void setAlGroupModuleList(ArrayList alGroupModuleList) {
		this.alGroupModuleList = alGroupModuleList;
	}

	public String getGridXmlData() {
		return gridXmlData;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeParentId() {
		return nodeParentId;
	}

	public void setNodeParentId(String nodeParentId) {
		this.nodeParentId = nodeParentId;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getNodeUrl() {
		return nodeUrl;
	}

	public void setNodeUrl(String nodeUrl) {
		this.nodeUrl = nodeUrl;
	}

	public String getNodeStropt() {
		return nodeStropt;
	}

	public void setNodeStropt(String nodeStropt) {
		this.nodeStropt = nodeStropt;
	}

	public String getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(String selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public String getTreeXmlData() {
		return treeXmlData;
	}

	public void setTreeXmlData(String treeXmlData) {
		this.treeXmlData = treeXmlData;
	}	

	public String getSelectedMenuItem() {
		return selectedMenuItem;
	}

	public void setSelectedMenuItem(String selectedMenuItem) {
		this.selectedMenuItem = selectedMenuItem;
	}

	public ArrayList getAlFolderList() {
		return alFolderList;
	}

	public void setAlFolderList(ArrayList alFolderList) {
		this.alFolderList = alFolderList;
	}

	public ArrayList getAlFileList() {
		return alFileList;
	}

	public void setAlFileList(ArrayList alFileList) {
		this.alFileList = alFileList;
	}

	public String getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(String selectedFile) {
		this.selectedFile = selectedFile;
	}

	public String getSelectedFolder() {
		return selectedFolder;
	}

	public void setSelectedFolder(String selectedFolder) {
		this.selectedFolder = selectedFolder;
	}

	public ArrayList getAlMenuList() {
		return alMenuList;
	}

	public void setAlMenuList(ArrayList alMenuList) {
		this.alMenuList = alMenuList;
	}

	public HashMap getHmAccessMap() {
		return hmAccessMap;
	}

	public void setHmAccessMap(HashMap hmAccessMap) {
		this.hmAccessMap = hmAccessMap;
	}

	/**
	 * @return the strReadOnlyAccess
	 */
	public String getStrReadOnlyAccess() {
		return strReadOnlyAccess;
	}

	/**
	 * @param strReadOnlyAccess the strReadOnlyAccess to set
	 */
	public void setStrReadOnlyAccess(String strReadOnlyAccess) {
		this.strReadOnlyAccess = strReadOnlyAccess;
	}	
	
	

}