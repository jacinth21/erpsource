package com.globus.user.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmUserGroupForm extends GmCommonForm {

  private String userDept = "";
  private String userNm = "";
  private String groupName = "";
  private String defaultGrp = "";
  private String grpMappingID = "";
  private String gridXmlData = "";
  /**** these are for report ********/
  private String ruserNm = "";
  private String rgroupName = "";
  private String ruserDept = "";
  private String groupId = "";
  private String moduleName = "";
  private String grpType = "";
  private String companyId = "";
  private String companynm = "";
  private String exportExcelFl = "";
  private ArrayList alCompany = new ArrayList();
  private ArrayList alGrpMapDtls = new ArrayList();

 

/**Enable/disable for Button */
  
  private String strAccessFl="";
  /**
   * @return the grpType
   */
  public String getGrpType() {
    return grpType;
  }

  /**
   * @param grpType the grpType to set
   */
  public void setGrpType(String grpType) {
    this.grpType = grpType;
  }

  /**
   * @return the moduleName
   */
  public String getModuleName() {
    return moduleName;
  }

  /**
   * @param moduleName the moduleName to set
   */
  public void setModuleName(String moduleName) {
    this.moduleName = moduleName;
  }

  /**
   * @return the groupId
   */
  public String getGroupId() {
    return groupId;
  }

  /**
   * @param groupId the groupId to set
   */
  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  /**
   * @return the ruserNm
   */
  public String getRuserNm() {
    return ruserNm;
  }

  /**
   * @param ruserNm the ruserNm to set
   */
  public void setRuserNm(String ruserNm) {
    this.ruserNm = ruserNm;
  }

  /**
   * @return the rgroupName
   */
  public String getRgroupName() {
    return rgroupName;
  }

  /**
   * @param rgroupName the rgroupName to set
   */
  public void setRgroupName(String rgroupName) {
    this.rgroupName = rgroupName;
  }

  /**
   * @return the ruserDept
   */
  public String getRuserDept() {
    return ruserDept;
  }

  /**
   * @param ruserDept the ruserDept to set
   */
  public void setRuserDept(String ruserDept) {
    this.ruserDept = ruserDept;
  }

  //
  private ArrayList alUserDept = new ArrayList();
  private ArrayList alUser = new ArrayList();
  private ArrayList alGroupName = new ArrayList();
  private ArrayList alModuleName = new ArrayList();



  /**
   * @return the alModuleName
   */
  public ArrayList getAlModuleName() {
    return alModuleName;
  }

  /**
   * @param alModuleName the alModuleName to set
   */
  public void setAlModuleName(ArrayList alModuleName) {
    this.alModuleName = alModuleName;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the grpMappingID
   */
  public String getGrpMappingID() {
    return grpMappingID;
  }

  /**
   * @param grpMappingID the grpMappingID to set
   */
  public void setGrpMappingID(String grpMappingID) {
    this.grpMappingID = grpMappingID;
  }

  /**
   * @return the userDept
   */
  public String getUserDept() {
    return userDept;
  }

  /**
   * @param userDept the userDept to set
   */
  public void setUserDept(String userDept) {
    this.userDept = userDept;
  }

  /**
   * @return the user
   */
  public String getUserNm() {
    return userNm;
  }

  /**
   * @param user the user to set
   */
  public void setUserNm(String userNm) {
    this.userNm = userNm;
  }

  /**
   * @return the groupName
   */
  public String getGroupName() {
    return groupName;
  }

  /**
   * @param groupName the groupName to set
   */
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  /**
   * @return the defaultGrp
   */
  public String getDefaultGrp() {
    return defaultGrp;
  }


/**
 * @return the strAccessFl
 */
public String getStrAccessFl() {
	return strAccessFl;
}

/**
 * @param strAccessFl the strAccessFl to set
 */
public void setStrAccessFl(String strAccessFl) {
	this.strAccessFl = strAccessFl;
}

/**
   * @param defaultGrp the defaultGrp to set
   */
  public void setDefaultGrp(String defaultGrp) {
    defaultGrp = defaultGrp.equals("Y") ? "on" : defaultGrp;
    this.defaultGrp = defaultGrp;
  }

  /**
   * @return the alUserDept
   */
  public ArrayList getAlUserDept() {
    return alUserDept;
  }

  /**
   * @param alUserDept the alUserDept to set
   */
  public void setAlUserDept(ArrayList alUserDept) {
    this.alUserDept = alUserDept;
  }

  /**
   * @return the alUser
   */
  public ArrayList getAlUser() {
    return alUser;
  }

  /**
   * @param alUser the alUser to set
   */
  public void setAlUser(ArrayList alUser) {
    this.alUser = alUser;
  }

  /**
   * @return the alGroupName
   */
  public ArrayList getAlGroupName() {
    return alGroupName;
  }

  /**
   * @param alGroupName the alGroupName to set
   */
  public void setAlGroupName(ArrayList alGroupName) {
    this.alGroupName = alGroupName;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }


  public String getCompanynm() {
    return companynm;
  }

  public void setCompanynm(String companynm) {
    this.companynm = companynm;
  }

  public ArrayList getAlCompany() {
    return alCompany;
  }

  public void setAlCompany(ArrayList alCompany) {
    this.alCompany = alCompany;
  }
  /**
   * @return the exportExcelFl
   */
  public String getExportExcelFl() {
  	return exportExcelFl;
  }

  /**
   * @param exportExcelFl the exportExcelFl to set
   */
  public void setExportExcelFl(String exportExcelFl) {
  	this.exportExcelFl = exportExcelFl;
  }

  /**
   * @return the alGrpMapDtls
   */
  public ArrayList getAlGrpMapDtls() {
  	return alGrpMapDtls;
  }

  /**
   * @param alGrpMapDtls the alGrpMapDtls to set
   */
  public void setAlGrpMapDtls(ArrayList alGrpMapDtls) {
  	this.alGrpMapDtls = alGrpMapDtls;
  }

}
