package com.globus.user.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.user.forms.GmUserListForm;

public class GmPartyCompanyMapAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    instantiate(request, response);

    GmUserListForm gmUserListForm = (GmUserListForm) form;
    gmUserListForm.loadSessionParameters(request);

    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean(getGmDataStoreVO());//
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());

    ArrayList alAvailableComp = new ArrayList();
    ArrayList alAsscompany = new ArrayList();
    ArrayList alLogReason = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmAccess = new HashMap();
    String strOpt = "";
    String strDispatchTo = "company";
    String strUserName = "";
    String strPartyId = "";
    String strAccessFl = "";
    String strUserId="";
    hmParam = GmCommonClass.getHashMapFromForm(gmUserListForm);
    strOpt = GmCommonClass.parseNull(gmUserListForm.getStrOpt());
    strUserId = GmCommonClass.parseNull(gmUserListForm.getUserId());
    strUserName = GmCommonClass.parseNull((String) hmParam.get("USERLOGINNAME"));
    hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "USER_PROF_BTN_ACCESS"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmUserListForm.setStrAccessFl(strAccessFl);
    if (!strUserName.equals("")) {
      // Getting the party id .
      hmReturn = GmCommonClass.parseNullHashMap(gmUserMgmtBean.fetchUserdetail(hmParam));
      strPartyId = GmCommonClass.parseNull((String) hmReturn.get("PARTYID"));
      hmParam.put("PARTYID", strPartyId);
      hmParam.put("COMPTYP", "105401"); // Non Primary
      if (strOpt.equals("save")) {

        gmPartyBean.saveAsstCompany(hmParam);// Saving associate companies.
        gmPartyBean.saveSecurityGrpMap(hmParam);// updating group mapping for associated company.
        GmCommonClass.getFormFromHashMap(gmUserListForm, hmParam);
        

      }
      alLogReason =
              GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strPartyId, "10304548"));
      gmUserListForm.setAlLogReasons(alLogReason);    
      alAvailableComp = gmPartyBean.fetchNonAsstCompany(hmParam);
      alAsscompany = gmPartyBean.fetchPartyCompany(hmParam);
      gmUserListForm.setalAvailableComp(alAvailableComp);
      gmUserListForm.setAlAsscompany(alAsscompany);
    }

    return mapping.findForward(strDispatchTo);
  }
}
