package com.globus.user.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTokenManager;
import com.globus.logon.beans.GmUserTokenBean;
import com.globus.valueobject.sales.GmCompanyVO;

public class GmMobileUserNavigationAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /*
   * Desc: generating new token information if token coming as null Author: Jreddy
   */
  public void fetchSessionInformation(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    String strToken = "";
    String strUserId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    String strPartyId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessPartyId"));
    String strTopCompanyId =
            GmCommonClass.parseNull((String) request.getParameter("cmpid"));
    String strTopPlantId =
            GmCommonClass.parseNull((String) request.getParameter("plantid"));
    
    // Getting date format and Time zone based on the company
    GmCompanyVO compVO = (new GmCommonBean()).getCompanyVO(strTopCompanyId);
    String strTopDateFmt  = compVO.getCmpdfmt();
    String strTopTimeZone = compVO.getCmptzone();
    String strCurrSymbol  = compVO.getCmpcurrsmb();
        
    String strPlantId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessPlantId"));
    strPlantId = strTopPlantId.equals("") ? strPlantId: strTopPlantId;
    
    String strCompanyId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyId"));
    strCompanyId = strTopCompanyId.equals("") ? strCompanyId: strTopCompanyId;
    
    String strCompTimeZone =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCmpTimeZone"));
    strCompTimeZone = strTopTimeZone.equals("") ? strCompTimeZone: strTopTimeZone;
    
    String strCompDateFmt =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCmpDateFmt"));
    strCompDateFmt = strTopDateFmt.equals("") ? strCompDateFmt: strTopDateFmt;
    
    String strUserNm =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserNm"));
    String strDeptID =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptId"));
    String strDeptSeq =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptSeq"));
    String strSHNm =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessShName"));
    String strAcid =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessAccLvl"));
    String strLoginTS =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessLoginTS"));
    String strExtAccess =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strExtAccess"));
    String strDivID =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDivId"));
    String strFName =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessFName"));
    String strLName =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessLName"));
    String strSwitchUserFl =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSwitchUserFl"));
    String strCompanyLandId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLangId"));
    String strSessToken =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessToken"));
    log.debug("strSessToken" + strSessToken);
    if (strSessToken.equals("")) {
      strToken = GmTokenManager.getNewToken();
      HashMap hmTkn = new HashMap();
      hmTkn.put("TOKEN", strToken);
      hmTkn.put("STATUS", "103101");// Active Token Status
      hmTkn.put("USERID", strUserId); // This method will save the User Token
      (new GmUserTokenBean()).saveUserToken(hmTkn);
      log.debug("strNewToken" + strToken);
      request.getSession().setAttribute("strSessToken", strToken);
    } else {
      strToken =
          GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessToken"));
    }

    String strInputString =
        strToken + "^" + strUserId + "^" + strUserNm + "^" + strDeptID + "^" + strDeptSeq + "^"
            + strSHNm + "^" + strAcid + "^" + strLoginTS + "^" + strPartyId + "^" + strExtAccess
            + "^" + strDivID + "^" + strFName + "^" + strLName + "^" + strCompanyId + "^"
            + strSwitchUserFl + "^" + strPlantId + "^" + strCompTimeZone + "^" + strCompDateFmt
            + "^" + strCompanyLandId  + "^" + strCurrSymbol;

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    if (!strInputString.equals("")) {
      pw.write(strInputString);
    }
    pw.flush();
    pw.close();

  }
}
