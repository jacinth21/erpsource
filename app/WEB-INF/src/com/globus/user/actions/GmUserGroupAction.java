package com.globus.user.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.user.forms.GmUserGroupForm;

public class GmUserGroupAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());

    String strOpt = "";
    String strDepID = "";
    String strGrpMappingID = "";
    String haction = "";
    String groupId = "";
    String strGroupId = "";
    String strGroupType = "92264";
    String strForwardJsp = "GmUserGroupSetup";
    String strCompanyId = "";
    String strUserId="";
    String strAccessFl="";
    String strExportExcelFl = "";
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    ArrayList alUserDept = new ArrayList();
    ArrayList alGroupName = new ArrayList();
    ArrayList alUserList = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    GmUserGroupForm gmUserGroupForm = (GmUserGroupForm) form;
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    gmUserGroupForm.loadSessionParameters(request);

    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmUserGroupForm));

    strOpt = gmUserGroupForm.getStrOpt();
    strDepID = gmUserGroupForm.getUserDept();
    strCompanyId = GmCommonClass.parseZero(gmUserGroupForm.getCompanyId());
    groupId =
        GmCommonClass.parseNull(gmUserGroupForm.getGroupId()).equals("") ? "0" : GmCommonClass
            .parseNull(gmUserGroupForm.getGroupId());
    haction = gmUserGroupForm.getHaction();
    strExportExcelFl = GmCommonClass.parseNull(gmUserGroupForm.getExportExcelFl());
    haction = haction.equals("") ? strOpt : haction;
    gmUserGroupForm.setHaction(haction);
    alUserDept = GmCommonClass.getCodeList("DEPID");
    gmUserGroupForm.setAlUserDept(alUserDept);
    gmUserGroupForm.setAlCompany(gmCommonBean.getCompanyList());
    /*TO Enable/disable button access-PMT-26730*/
    strUserId = GmCommonClass.parseNull(gmUserGroupForm.getUserId());
    hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,"SYS_ADMIN_BTN_ACS"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmUserGroupForm.setStrAccessFl(strAccessFl);
    // alGroupName=GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchACGroupDetails(strGroupType));
    if (strOpt.equals("UAGM") || haction.equals("UAGM")) {
      strGroupType = "92260";
    } else if (strOpt.equals("SCGRP")) {
      strGroupType = "92264";
    } else if (strOpt.equals("MDGRP")) {
      strGroupType = "92265";
    }
    alGroupName =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchACGroupDetails(strGroupType));
    ArrayList alModuleName =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchACGroupDetails("92265"));
    gmUserGroupForm.setAlGroupName(alGroupName);
    gmUserGroupForm.setAlModuleName(alModuleName);
    gmUserGroupForm.setAlGroupName(alGroupName);
    alUserList = GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchUserList(strDepID));
    gmUserGroupForm.setAlUser(alUserList);
    gmUserGroupForm.setGroupId(groupId);

    if (strOpt.equals("save")) {
      strGrpMappingID = gmAccessControlBean.saveUserGroup(hmParam);
      gmUserGroupForm.setGrpMappingID(strGrpMappingID);
      strOpt = "edit";
    } else if (strOpt.equals("edit")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmParam));
      hmParam = GmCommonClass.parseNullHashMap((HashMap) alResult.get(0));
      String chkboxValue = (String) hmParam.get("DEFAULTGRPFORCB");
      gmUserGroupForm =
          (GmUserGroupForm) GmCommonClass.getFormFromHashMap(gmUserGroupForm, hmParam);

      strDepID = gmUserGroupForm.getUserDept();

      if (chkboxValue.equals("Y")) {
        gmUserGroupForm.setDefaultGrp("on");
      }

    } else if (strOpt.equals("load")) {
      strDepID = gmUserGroupForm.getUserDept();
      strOpt = "save";
    } else if (strOpt.equals("report")) {
      strForwardJsp = "GmUserGroupReport";
    } else if (strOpt.equals("ReloadReport")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmParam));
      //if strExportExcelFl is Y, then call forward the jsp[GmUserGroupReportExcel] and export the excel
      //not able to perform dhtmlx export if it is more data - PMT-36095
      if(strExportExcelFl.equals("Y")) {
    	  gmUserGroupForm.setAlGrpMapDtls(alResult);
    	  response.setContentType("application/vnd.ms-excel;charset=UTF-8");
          response.setHeader("Content-disposition", "attachment;filename=UserGroupReportExcel.xls");
          strForwardJsp = "GmUserGroupReportExcel";
      }else {
      String strGridXmlData = generateOutPut(alResult);
      gmUserGroupForm.setGridXmlData(strGridXmlData);

      strDepID = gmUserGroupForm.getUserDept();
      strForwardJsp = "GmUserGroupReport";

      gmUserGroupForm.setHaction(haction);
      }

    } else if (strOpt.equals("UGAJAX")) {
      log.debug("groupId" + groupId);
      String strDesc = gmCommonBean.getGroupDesc(groupId);
      PrintWriter writer = response.getWriter();
      writer.println(strDesc);
      writer.close();
      return null;
    } else if (strOpt.equals("SCMD") && haction.equals("save")) {
      gmAccessControlBean.saveSecModGroup(hmParam);
      strOpt = "save";
      strForwardJsp = "GmSecGrpModMapping";
    } else if (strOpt.equals("SCMD")) {
      strForwardJsp = "GmSecGrpModMapping";
    } else if (strOpt.equals("SMREPORT")) {
    	
      strGroupId = GmCommonClass.parseNull(gmUserGroupForm.getGroupName());
      alResult =
          GmCommonClass
              .parseNullArrayList(gmAccessControlBean.fetchSecGrpModuleMapList(strGroupId));
      String strGridXmlData = generateModScGrpOutPut(alResult);
      gmUserGroupForm.setGridXmlData(strGridXmlData);
      strForwardJsp = "GmSecurityGrpModReport";
    } else if (strOpt.equals("MSREPORT")) {
      strGroupId = GmCommonClass.parseNull(gmUserGroupForm.getGroupName());
      log.debug("strGroupId==>" + strGroupId);
      alResult =
          GmCommonClass
              .parseNullArrayList(gmAccessControlBean.fetchModuleSecGrpMapList(strGroupId));
      String strGridXmlData = generateModScGrpOutPut(alResult);
      gmUserGroupForm.setGridXmlData(strGridXmlData);
      strForwardJsp = "GmSecurityGrpModReport";
    } else if (strOpt.equals("SCMMAP")) {
      String strMappedId = "";
      String strGrpMapId = GmCommonClass.parseNull(gmUserGroupForm.getGrpMappingID());
      alResult =
          GmCommonClass
              .parseNullArrayList(gmAccessControlBean.fetchSecGrpModuleMapList(strGroupId));
      for (int i = 0; i < alResult.size(); i++) {
        hmParam = GmCommonClass.parseNullHashMap((HashMap) alResult.get(i));
        strMappedId = GmCommonClass.parseNull((String) hmParam.get("GRPMAPPINGID"));
        if (strGrpMapId.equals(strMappedId)) {
          gmUserGroupForm =
              (GmUserGroupForm) GmCommonClass.getFormFromHashMap(gmUserGroupForm, hmParam);
        }
      }
      strForwardJsp = "GmSecGrpModMapping";
    } else if (strOpt.equals("PDFDownload")) {// Pdf download for security group to user mapping
                                              // report Screen.
      // For getting the report values based on filter and those values we going to show in PDF by
      // using the
      // GmSecurityGroupUserListReport.jasper.
      alResult =
          GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmParam));

      GmJasperReport gmJasperReport = new GmJasperReport();

      gmJasperReport.setJasperReportName("/GmSecurityGroupUserListReport.jasper");
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setHmReportParameters(hmParam);
      gmJasperReport.setReportDataList(alResult);
      gmJasperReport.createJasperPdfReport();

      return null;
    }
    strOpt = strOpt.equals("") ? "load" : strOpt;
    gmUserGroupForm.setStrOpt(strOpt);
    return mapping.findForward(strForwardJsp);
  }

  /**
   * getXmlGridData - This method will be used to generate Output for grid data.
   * 
   * @return String - will be used by dhtmlx grid
   * @exception AppError
   */
  private String generateOutPut(ArrayList alParam) throws Exception {

	  
	 
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setTemplateName("GmUserMapping.vm");
    templateUtil.setTemplateSubDir("user/templates");

    return templateUtil.generateOutput();
  }

  /**
   * getXmlGridData - This method will be used to generate Output for grid data.
   * 
   * @return String - will be used by dhtmlx grid
   * @exception AppError
   */
  private String generateModScGrpOutPut(ArrayList alParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setTemplateName("GmModSecurityGrpMapping.vm");
    templateUtil.setTemplateSubDir("user/templates");

    return templateUtil.generateOutput();
  }

}
