package com.globus.user.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.user.forms.GmUserADStatusReportForm;


public class GmUserADStatusReportAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmUserADStatusReportForm gmUserADStatusReportForm = (GmUserADStatusReportForm) form;
    //
    gmUserADStatusReportForm.loadSessionParameters(request);
    String strOpt = gmUserADStatusReportForm.getStrOpt();
    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmUserADStatusReportForm);
    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean(getGmDataStoreVO());

    ArrayList alUserList = new ArrayList();
    alUserList = gmUserMgmtBean.fetchADAccessUserList();
    gmUserADStatusReportForm.setAlUserList(alUserList);

    log.debug("alUserList:" + alUserList);

    ArrayList alADStatusList = GmCommonClass.getCodeList("ADSTUS");
    gmUserADStatusReportForm.setAlStatusList(alADStatusList);

    if (strOpt.equals("search")) {
      ArrayList alUserStatusList = gmUserMgmtBean.fetchUserADStatusReportList(hmParam);
      String strGridXmlData = generateOutPut(alUserStatusList);
      strGridXmlData = GmCommonClass.replaceForXML(strGridXmlData);
      gmUserADStatusReportForm.setGridXmlData(strGridXmlData);

    }

    return mapping.findForward("load");
  }

  /**
   * generateOutPut This method will generate xml content for grid from given arraylist data
   * 
   * @param alGridData ArrayList *
   * @return String
   */
  private String generateOutPut(ArrayList alGridData) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("user/templates");
    templateUtil.setTemplateName("GmUserADStatusReport.vm");
    return templateUtil.generateOutput();
  }
}
