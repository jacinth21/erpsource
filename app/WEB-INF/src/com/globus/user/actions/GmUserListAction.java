package com.globus.user.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.it.beans.GmLdapBean;
import com.globus.logon.auth.GmKeycloakManager;
import com.globus.logon.auth.GmLDAPManager;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.user.forms.GmUserListForm;

public class GmUserListAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {

    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=UTF-8");
    response.setCharacterEncoding("UTF-8");
    instantiate(request, response);

    GmUserListForm gmUserListForm = (GmUserListForm) form;
    gmUserListForm.loadSessionParameters(request);
    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean(getGmDataStoreVO());//
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLDAPManager gmLDAPManager = null;
    GmCustSalesSetupBean gmCustSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
    GmLdapBean gmLdapBean = new GmLdapBean();


    String strOpt = "";
    String strDispatchTo = "success";
    String strLoginName = "";
    String strUserID = "";
    String strSessUserID = "";
    String strGrpMappingID = "";
    String strUserId = "";
    String strEditUserAccessFl = "";
    String strSwitchUserAccessFl = "";
    String strPartyId = "";
    String strUserName = "";
    String strSalesRepID = "";
    String strLdapId = "";
    String  strAccessFl="";
    ArrayList alLogReason = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmTemplateParam = new HashMap();
    HashMap hmUserDetails = new HashMap();
    HashMap hmUserGrp = new HashMap();
    HashMap hmUserlist = gmUserMgmtBean.loadUserMgmtLists();
    HashMap hmResult = new HashMap();
    ArrayList alLdap = GmCommonClass.parseNullArrayList(gmLdapBean.fetchLdapDetails("", ""));

    gmUserListForm.setAlDept((ArrayList) hmUserlist.get("DEPT"));
    gmUserListForm.setAlStatus((ArrayList) hmUserlist.get("STATUS"));
    gmUserListForm.setAlType((ArrayList) hmUserlist.get("TYPE"));
    gmUserListForm.setAlUserType((ArrayList) hmUserlist.get("USERTYPE"));
    gmUserListForm.setAlTitle((ArrayList) hmUserlist.get("USERTITLE"));
    gmUserListForm.setAlSecGrp((ArrayList) hmUserlist.get("SECGRP"));
    gmUserListForm.setAlCompany((ArrayList) hmUserlist.get("COMPANY"));
    gmUserListForm.setAlLdap(alLdap);

    hmParam = GmCommonClass.getHashMapFromForm(gmUserListForm);
    strOpt = GmCommonClass.parseNull(gmUserListForm.getStrOpt());
    strUserId = GmCommonClass.parseNull(gmUserListForm.getUserId());

    String strUserType = GmCommonClass.parseNull((String) hmParam.get("USERTYPE"));

    if (strOpt.equals("save")) {
      strUserID = GmCommonClass.parseNull(gmUserListForm.getHuserid());
      hmResult = gmUserMgmtBean.saveUser(hmParam);
      if (strUserID.equals("")) {
        strUserID = GmCommonClass.parseNull((String) hmResult.get("USERID"));
      }
      
      /*PMT-51134--Copy Security Group access
       * Copy security details from one user to another user.
       * Based on copy from user - all the access clone to a new user.
       */
      strUserName = GmCommonClass.parseNull((String) hmParam.get("USERLOGINNAME"));
      String strCopyUserNm = GmCommonClass.parseNull((String) hmParam.get("COPYUSERNAME"));
      String strUpdateUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
      
      hmUserGrp.put("USERNM", strUserID);
      hmUserGrp.put("GROUPNAME", hmParam.get("SECURITYGRP"));
      hmUserGrp.put("USERID", hmParam.get("USERID"));
      hmUserGrp.put("DEFAULTGRP", "on");
      hmUserGrp.put("COMPANYID", GmCommonClass.parseNull((String) hmParam.get("COMPANYID")));

      strGrpMappingID = gmAccessControlBean.saveUserGroup(hmUserGrp);

      // getting the partyid
      hmReturn = GmCommonClass.parseNullHashMap(gmUserMgmtBean.fetchUserdetail(hmParam));
      strPartyId = GmCommonClass.parseNull((String) hmReturn.get("PARTYID"));
      hmParam.put("PARTYID", strPartyId);
      // To save the default company for the user.
      gmPartyBean.savePartyCompany(hmParam);

    //Added for PMT-51134 copy security group access BUG-12483
      if(!strCopyUserNm.equals("")){
          gmUserMgmtBean.copyAccessGrp(strUserName,strCopyUserNm,strUpdateUserId);
       }
      
      strSalesRepID = GmCommonClass.parseNull((String) hmResult.get("SALESREPID"));
      log.debug("strSalesRepID:::" + strSalesRepID);

      // Currently we are sending the welcome email manually so, comment the below code.

      // if (!strSalesRepID.equals("")) {
      // String strUsrLoginName = GmCommonClass.parseNull((String) hmParam.get("HUSERLOGINNAME"));
      // String strFirstName = GmCommonClass.parseNull((String) hmParam.get("FNAME"));
      // HashMap hmValues = new HashMap();
      // hmValues.put("FNAME", strFirstName);
      // hmValues.put("ID", strSalesRepID);
      // hmValues.put("USERLOGINNAME", strUsrLoginName);
      // gmCustSetupBean.sendEmailToNewSalesRep(hmValues);
      // }
      strOpt = "edit";

      // 301-Direct Sales Rep, 302 - Agent Sales Rep
      if (strUserType.equals("301") || strUserType.equals("302")) {
        GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
        // View refresh JMS called
        HashMap hmViewRefresh = new HashMap();
        hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
        hmViewRefresh.put("companyInfo",
            GmCommonClass.parseNull(request.getParameter("companyInfo")));

        String strViewConsumerClass =
            GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
        hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
        gmConsumerUtil.sendMessage(hmViewRefresh);

        // PC-446: To company company and refresh Sales Rep and Field sales - redis key
        // to get the sales rep company id 
        String strSalesRepCompanyId= GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
        String strOldCompanyId = GmCommonClass.parseZero((String) hmParam.get("HOLDCOMPANYID"));
        // to get the first and last name
        String strFirstName = GmCommonClass.parseNull((String) hmParam.get("FNAME"));
        String strLastName = GmCommonClass.parseNull((String) hmParam.get("LNAME"));
        
        String strOldFirstName = GmCommonClass.parseNull((String) hmParam.get("HOLDFIRSTNAME"));
        String strOldLastName = GmCommonClass.parseNull((String) hmParam.get("HOLDLASTNAME"));
    	
        // to check the company/First/Last name and remove the redis key
        if (!strSalesRepCompanyId.equals(strOldCompanyId) || !strFirstName.equalsIgnoreCase(strOldFirstName)  || !strLastName.equalsIgnoreCase(strLastName)){
        	log.debug(" To call the Other company Redis refresh, strSalesRepCompanyId ==>  "+
        			strSalesRepCompanyId +" strUserOldCompanyId ==> "+strOldCompanyId);
        	GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
        	
            // To remove the Sales Rep keys
            gmCacheManager.removePatternKeys("REP_LIST*"+ strSalesRepCompanyId +"*");
            
            // To remove the Field sales keys
            gmCacheManager.removePatternKeys("DIST_LIST*"+ strSalesRepCompanyId +"*");
            
            // to refresh old company details
            if (!strSalesRepCompanyId.equals(strOldCompanyId) && !strOldCompanyId.equals("0")){
            	// To remove the Sales Rep keys
                gmCacheManager.removePatternKeys("REP_LIST*"+ strOldCompanyId +"*");
                
                // To remove the Field sales keys
                gmCacheManager.removePatternKeys("DIST_LIST*"+ strOldCompanyId +"*");
            } // end of company check
            
        } // end of SalesRep Company check

    } // end of strUserType check
      
    } // end of strOpt save

    if (strOpt.equals("ROLLBACK")) { // to reactivate user
      strDispatchTo = "GmUserEdit";
      strUserID = GmCommonClass.parseNull(gmUserListForm.getHuserid());
     
      String strUserNm =
          gmUserMgmtBean.reactivateUser(strUserID,
              GmCommonClass.parseNull(gmUserListForm.getUserId()));
      hmParam.put("USERLOGINNAME", strUserNm);
      hmReturn = GmCommonClass.parseNullHashMap(gmUserMgmtBean.fetchUserdetail(hmParam));
      
      // Access flag to check user profile button access
      hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  "USER_PROF_BTN_ACCESS"));
      strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      gmUserListForm.setStrAccessFl(strAccessFl);
   
      hmAccess =
          gmAccessControlBean.getAccessPermissions(
              GmCommonClass.parseNull(gmUserListForm.getUserId()), "USER_ED_ACS");
      request.setAttribute("USERDETAILS", hmReturn);
      request.setAttribute("USER_EDIT_ACCESS", hmAccess.get("UPDFL"));

      alLogReason =
          GmCommonClass.parseNullArrayList(gmCommonBean.getLog(
              GmCommonClass.parseNull((String) hmReturn.get("USERID")), "1252"));
      GmCommonClass.getFormFromHashMap(gmUserListForm, hmReturn);
      gmUserListForm.setHuserid(GmCommonClass.parseNull((String) hmReturn.get("USERID")));
      gmUserListForm.setHuserLoginName((String) hmReturn.get("USERLOGINNAME"));
      gmUserListForm.setAlLogReasons(alLogReason);
      gmUserListForm.sethCopyUserName((String) hmReturn.get("COPYUSERNAME"));
    }

    if (strOpt.equals("Reload")) {
      ArrayList alReport = gmUserMgmtBean.getUserList(hmParam);

      // EDIT_USER Security Group for Edit Icon
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "EDIT_USER"));
      strEditUserAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      hmTemplateParam.put("EDITACCESSFLAG", strEditUserAccessFl);
      // SWITCH_USER Security Group for Switch User Icon
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "SWITCH_USER"));
      strSwitchUserAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      hmTemplateParam.put("SWITCHACCESSFLAG", strSwitchUserAccessFl);
      hmTemplateParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
      // hmReturn.put("DATEFORMAT", strDateFormat));
      String strXmlData = generateOutPut(alReport, hmTemplateParam);
      gmUserListForm.setGridXmlData(strXmlData);
      gmUserListForm.setEditUserAccess(strEditUserAccessFl);
      gmUserListForm.setSwitchUserAccess(strSwitchUserAccessFl);
    } else if (strOpt.equals("edit")) {
      strDispatchTo = "GmUserEdit";
      strUserID = GmCommonClass.parseNull(gmUserListForm.getHuserid());
      strUserName = GmCommonClass.parseNull(gmUserListForm.getUserLoginName());
      String strUserNm = GmCommonClass.parseNull(request.getParameter("userLoginName"));
      strUserName = strUserName.equals("") ? strUserNm : strUserName;
      //set flag to acces button values-PMT26730
      hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  "USER_PROF_BTN_ACCESS"));
      strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      gmUserListForm.setStrAccessFl(strAccessFl);
      log.debug("strUserName in  STROPT USERPROFILE>> " + strUserName);

      if (!strUserName.equals("")) {
        hmReturn = GmCommonClass.parseNullHashMap(gmUserMgmtBean.fetchUserdetail(hmParam));
        log.debug("hmReturn:" + hmReturn);
        if (hmReturn.size() == 0) {
          String strDbLdapId =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue("DB_AUTH", "LDAP"));
          strLoginName = GmCommonClass.parseNull(gmUserListForm.getUserLoginName());
          strLdapId = GmCommonClass.parseNull((String) hmParam.get("LDAPID"));
          if (strLdapId.equals(strDbLdapId)) {
            // DB user selected and data not present in DB then throw an error message
            request.setAttribute("hRedirectURL", "/gmUserListAction.do?strOpt=edit");
            throw new AppError(AppError.APPLICATION, "1054");
          }
          //If the id is 4-(Keycloak) , it will fetch user details from Keycloak
          if (strLdapId.equals("4")) {
              GmKeycloakManager gmKeycloakManager = new GmKeycloakManager();
             hmUserDetails = gmKeycloakManager.fetchKeycloakUserDetail(strLoginName);
          }
              else{ 
          gmLDAPManager = new GmLDAPManager(strLdapId);
          log.debug("before AD : ");
          hmUserDetails =
              GmCommonClass.parseNullHashMap(gmLDAPManager.getADUserDetails(strLoginName));
          log.debug("after AD : " + hmUserDetails);
              }
          if (hmUserDetails.size() == 0) {
            // URL added for the PBUG-476 For invalid user,When ever click on
            // "Back to previous screen" inside the tab container is loading again
            String strRedirestUrl = "/gmUserListAction.do?strOpt=edit";
            request.setAttribute("hRedirectURL", strRedirestUrl);
            throw new AppError(AppError.APPLICATION, "1054");
          } else {

            hmReturn.put("EMAILID",
                GmCommonClass.parseNull((String) hmUserDetails.get("EMAILADDRESS")));
            hmReturn.put("FNAME", GmCommonClass.parseNull((String) hmUserDetails.get("FIRSTNAME")));
            hmReturn.put("LNAME", GmCommonClass.parseNull((String) hmUserDetails.get("LASTNAME")));
            hmReturn.put("SHORTNAME",
                GmCommonClass.parseNull((String) hmUserDetails.get("FIRSTNAME")));
            hmReturn.put("HPASSWORD",
                GmCommonClass.parseNull((String) hmUserDetails.get("PASSWORD")));
            hmReturn.put("USERSTATUS", "New");
            hmReturn.put("DEPARTMENTID", "0");
            hmReturn.put("USERTYPE", "300");
            hmReturn.put("ACCESSLEVEL", "");
            hmReturn.put("USERLOGINNAME", strLoginName);
            hmReturn.put("LDAPID", strLdapId);
          }
        }
        hmAccess =
            gmAccessControlBean.getAccessPermissions(
                GmCommonClass.parseNull(gmUserListForm.getUserId()), "USER_ED_ACS");
        request.setAttribute("USERDETAILS", hmReturn);
        request.setAttribute("USER_EDIT_ACCESS", hmAccess.get("UPDFL"));

        alLogReason =
            GmCommonClass.parseNullArrayList(gmCommonBean.getLog(
                GmCommonClass.parseNull((String) hmReturn.get("USERID")), "1252"));
        GmCommonClass.getFormFromHashMap(gmUserListForm, hmReturn);
        gmUserListForm.setHuserid(GmCommonClass.parseNull((String) hmReturn.get("USERID")));
        gmUserListForm.setHuserLoginName((String) hmReturn.get("USERLOGINNAME"));
        gmUserListForm.setLdapID(GmCommonClass.parseNull((String) hmReturn.get("LDAPID")));
        gmUserListForm.setAlLogReasons(alLogReason);
        gmUserListForm.sethCopyUserName((String) hmReturn.get("COPYUSERNAME"));
      }

    } else if (strOpt.equals("New")) {
      gmUserListForm.setHuserid("");
      strDispatchTo = "GmUserEdit";
    }
    if (strOpt.equals("checkusernm")) {
      strUserName = "";
      hmReturn = gmUserMgmtBean.fetchUserdetail(hmParam);
      strUserName = GmCommonClass.parseNull((String) hmReturn.get("USERLOGINNAME"));
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (!strUserName.equals(""))
        pw.write(strUserName);
      pw.flush();
      pw.close();
      return null;
    }

    if (strOpt.equals("USERPROFILE")) {
      strDispatchTo = "UserProfile";
      strUserID = GmCommonClass.parseNull(gmUserListForm.getHuserid());
      String strUserNm = GmCommonClass.parseNull(request.getParameter("userLoginName"));

      strUserName = GmCommonClass.parseNull((String) hmParam.get("USERLOGINNAME"));
      strUserName = strUserName.equals("") ? strUserNm : strUserName;
      log.debug("strUserName in  STROPT USERPROFILE>> " + strUserName);
      String strTempLdapId = gmLdapBean.getLdapIdFromUserName(strUserName);
      if (!strTempLdapId.equals("")) {
        gmUserListForm.setLdapID(strTempLdapId);
      }
      if (!strUserID.equals("")) {
        hmReturn = gmUserMgmtBean.fetchUserdetail(hmParam);
        strUserName = GmCommonClass.parseNull((String) hmReturn.get("USERLOGINNAME"));
        /*
         * request.setAttribute("USERDETAILS", hmReturn); alLogReason =
         * GmCommonClass.parseNullArrayList(gmCommonBean.getLog( GmCommonClass.parseNull((String)
         * hmReturn.get("USERID")), "1252"));
         */
        GmCommonClass.getFormFromHashMap(gmUserListForm, hmReturn);
        gmUserListForm.setHuserid(GmCommonClass.parseNull((String) hmReturn.get("USERID")));
        gmUserListForm.setHuserLoginName((String) hmReturn.get("USERLOGINNAME"));
        gmUserListForm.sethCopyUserName((String) hmReturn.get("COPYUSERNAME"));
        // gmUserListForm.setAlLogReasons(alLogReason);
      }
      gmUserListForm.setStrOpt("edit");
      gmUserListForm.setUserLoginName(strUserName);
    }


    if (strOpt.equals("VALIDATEUSERLOGIN")) {// to validate username already exists
      String strUserNm = GmCommonClass.parseNull(request.getParameter("userLoginName"));
      PrintWriter pw = response.getWriter();
      response.setContentType("text/xml");
      int intUserCnt = gmUserMgmtBean.validateUserName(strUserNm);
      pw.write(Integer.toString(intUserCnt));
      pw.flush();
      return null;
    }

    return mapping.findForward(strDispatchTo);
  }

  private String generateOutPut(ArrayList alGridData, HashMap hmParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    hmParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("user/templates");
    templateUtil.setTemplateName("GmUserListReport.vm");
    return templateUtil.generateOutput();
  }
}
