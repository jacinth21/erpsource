package com.globus.user.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.db.GmCacheManager;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.user.forms.GmUserNavigationSetupForm;


public class GmUserNavigationAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmUserNavigationSetupForm gmUserNavigationSetupForm = (GmUserNavigationSetupForm) form;
    gmUserNavigationSetupForm.loadSessionParameters(request);
    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmUserNavigationSetupForm);
    String strOpt = gmUserNavigationSetupForm.getStrOpt();
    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    ArrayList alAccess = new ArrayList();
    ArrayList alUserGroups = new ArrayList();
    ArrayList alList = new ArrayList();
    String struserId = gmUserNavigationSetupForm.getUserId();
    String strPartyID = gmUserNavigationSetupForm.getSessPartyId();
    String strDispatch = "";
    if (strOpt.equals("AddFav")) {
      String strGrpId = GmCommonClass.parseNull(gmUserNavigationSetupForm.getGrpId());
      String strFunId = GmCommonClass.parseNull(gmUserNavigationSetupForm.getFunId());
      String strFunName = GmCommonClass.parseNull(gmUserNavigationSetupForm.getFunName());
      gmUserMgmtBean.addFavorites(strGrpId, strFunId, struserId, strFunName);
      
      //PMT-40908: Add tool tip for favorites
      gmUserMgmtBean.saveLinkPath(strGrpId, strFunId, struserId);
      
   // to remove the keys for cache server
      GmCacheManager gmCacheManager = new GmCacheManager();
      gmCacheManager.removePatternKeys("001_" + strPartyID + "_FAV");
      gmUserNavigationSetupForm.setStrOpt("Saved");
      strOpt = "Saved";
      // response.getWriter().print("Selected Link added to favorites");
    } else if (strOpt.equals("RemFav")) {
      String strGrpId = GmCommonClass.parseNull(gmUserNavigationSetupForm.getGrpId());
      String strFunId = GmCommonClass.parseNull(gmUserNavigationSetupForm.getFunId());
      gmUserMgmtBean.removeFavorites(strGrpId, strFunId, strPartyID);
      // response.getWriter().print("Selected Link removed from favorites");
   // to remove the keys for cache server
      GmCacheManager gmCacheManager = new GmCacheManager();
      gmCacheManager.removePatternKeys(strGrpId);
    }

    String strSelectedGroupId = GmCommonClass.parseNull((String) hmParam.get("USERGROUP"));
    String selectedGroupName = GmCommonClass.parseNull((String) hmParam.get("USERGRPNAME"));
    if (gmUserMgmtBean.isUserMappedToSecurityGroup(struserId) || strOpt.equals("SEQLOOKUP")) {
      alUserGroups = gmUserMgmtBean.fetchActiveModules(hmParam);
      if (strSelectedGroupId.equals("") || strSelectedGroupId.equals("0")
          || strOpt.equals("AddFav") || strOpt.equals("RemFav")) {
        if (strOpt.equals("AddFav")) {
          HashMap hmGroup = (HashMap) alUserGroups.get(0);
          strSelectedGroupId = GmCommonClass.parseNull((String) hmGroup.get("GROUP_ID"));
          selectedGroupName = GmCommonClass.parseNull((String) hmGroup.get("GROUP_NM"));
        } else {
          strSelectedGroupId = "001_" + strPartyID + "_FAV";
          selectedGroupName = "My Favorites";
          gmUserNavigationSetupForm.setUserGroup(strSelectedGroupId);
        }
        hmParam.put("USERGROUP", strSelectedGroupId);
      }
      gmUserNavigationSetupForm.setStrSelectedGroupId(strSelectedGroupId);
      gmUserNavigationSetupForm.setStrSelectedGroupName(selectedGroupName);
      gmUserNavigationSetupForm.setGrpType("Modules: ");
      log.debug("outside ++ "+selectedGroupName);
      if (selectedGroupName.equals("My Favorites")  || selectedGroupName.equals("'My Favorites'")) {
    	  log.debug("yes ++ "+selectedGroupName);
          gmUserNavigationSetupForm.setMyFavorites("true");
      }

      if (strOpt.equals("SEQLOOKUP")) {
        alAccess = new ArrayList();
      } else {
        alAccess = gmUserMgmtBean.fetchUserAccess(struserId);
      }

      if (!strSelectedGroupId.equals("")) {
        alList = gmUserMgmtBean.fetchUserNavigationLink(hmParam);
      }
    } else {
      alUserGroups = gmUserMgmtBean.fetchUserGroups(hmParam);
      alList = gmUserMgmtBean.fetchUserNavigationLink(hmParam);
    }
    int j=9;
    String groupId = gmUserNavigationSetupForm.getUserGroup();
    String strGroupId = "";
    for (int i = 0; i < alUserGroups.size(); i++) {
      HashMap hmValues = (HashMap) alUserGroups.get(i);
      strGroupId = GmCommonClass.parseNull((String) hmValues.get("GROUP_ID"));
      if (groupId.equals(strGroupId)) {
        gmUserNavigationSetupForm.setUserGroupName(GmCommonClass.parseNull((String) hmValues
            .get("GROUP_NM")));
      }
    }

    if (strOpt.equals("SEQLOOKUP")) {
      strDispatch = "lookup";
    } else if (strOpt.equals("SwitchUser")) {
      strDispatch = "switchuser";
    } else if (strOpt.equals("RENAME") || strOpt.equals("Saved")) {
      strDispatch = "rename";
    } else {
      strDispatch = "load";
    }
    // get the access permission for Read only access to portal
    HashMap hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(struserId,
            "READ_ONLY_ACCESS"));
    String strReadOnlyAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    String strTreeXML =
    gmXMLParserUtility.createLeftMenuTree(alList, strOpt, alAccess, strReadOnlyAccess);
    gmUserNavigationSetupForm.setTreeXmlData(strTreeXML);
    gmUserNavigationSetupForm.setAlUserGroup(alUserGroups);
    gmUserNavigationSetupForm.setAlMenuList(alList);
    HashMap hmAccessMap = convertAccesstoHashMap(alAccess);
    gmUserNavigationSetupForm.setHmAccessMap(hmAccessMap);
    gmUserNavigationSetupForm.setStrReadOnlyAccess(strReadOnlyAccess);  // Re-Branding Read only user access need for GmUserNavigationNew.jsp
    return mapping.findForward(strDispatch);
  }
  private HashMap convertAccesstoHashMap(ArrayList alAccess)
	{
		Iterator alAccessItr = alAccess.iterator();
		HashMap hmAccess = null;
		HashMap hmResult = new HashMap();
		
		String strFnId ="";
		String strUpdAcc ="";
		String strRdAcc ="";
		String strVdAcc ="";
		
		while(alAccessItr.hasNext())
		{
			hmAccess = new HashMap();
			hmAccess = (HashMap)alAccessItr.next();
			
			strFnId =	GmCommonClass.parseNull((String)hmAccess.get("FN_ID"));
			strUpdAcc =	GmCommonClass.parseNull((String)hmAccess.get("UPD_ACC"));
			strRdAcc =	GmCommonClass.parseNull((String)hmAccess.get("RD_ACC"));
			strVdAcc =	GmCommonClass.parseNull((String)hmAccess.get("VD_ACC"));
			
			hmResult.put(strFnId+"_UPD_ACC", strUpdAcc);
			hmResult.put(strFnId+"_RD_ACC", strRdAcc);
			hmResult.put(strFnId+"_VD_ACC", strVdAcc);
		}
		
		return hmResult;
	}
  
}