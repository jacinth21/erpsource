package com.globus.user.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmUserBean;
import com.globus.user.forms.GmUserLockReportForm;

public class GmUserLockReportAction extends GmAction
{

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   	
        
    	GmUserLockReportForm gmUserLockReportForm = (GmUserLockReportForm)form;
    	log.debug("gmUserLockReportForm :"+gmUserLockReportForm);  
    	gmUserLockReportForm.loadSessionParameters(request);        
        String strOpt = gmUserLockReportForm.getStrOpt();
        log.debug("Department Id :"+gmUserLockReportForm.getDeptId());        
        
    	HashMap hmParam = GmCommonClass.getHashMapFromForm(gmUserLockReportForm);    
 	    GmUserBean gmUserBean = new GmUserBean(); 
 	    
 	   if (!gmUserLockReportForm.getDeptId().equals("2006")) {
			throw new AppError("No Access to View the page", "", 'E');
		}
 	    
 	   if(strOpt.equals("save"))
       {   
 		   gmUserBean.updateUserLockList(hmParam);     
       }
                  	
    	ArrayList alUserList = new ArrayList();
    	alUserList = gmUserBean.fetchUserList();
    	String strGridXmlData = generateOutPut(alUserList);
    	strGridXmlData = GmCommonClass.replaceForXML(strGridXmlData);
    	gmUserLockReportForm.setGridXmlData(strGridXmlData);       
      
        return mapping.findForward("load");
    }   
    
    /**
	 * generateOutPut This method will generate xml content for grid from given arraylist data
	 * @param alGridData ArrayList
	 * @return String 
	 */
	private String generateOutPut(ArrayList alGridData) throws Exception {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setDataList("alGridData", alGridData);
		templateUtil.setTemplateSubDir("user/templates");
		templateUtil.setTemplateName("GmUserLockReport.vm");
		return templateUtil.generateOutput();
	}
}