package com.globus.user.actions;

import org.apache.struts.action.ActionForm;

import java.util.HashMap;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.user.forms.GmAccessControlForm;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmAccessControlGroupAction extends GmAction{

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		Logger log = GmLogger.getInstance(this.getClass().getName());
		String strPage="";
		String strOpt="";
		String haction="";
		String strUserId="";
		String strAccessFl="";
		String strGroupType="";
		String strGroupId="";
		HashMap hmParam = new HashMap();
		HashMap hmTemp = new HashMap();
		HashMap hmAccess = new HashMap();
		ArrayList alResult = new ArrayList();
		ArrayList alGroupName=new ArrayList();
		
		

		GmAccessControlForm gmAccessControlForm=(GmAccessControlForm)form;
		GmAccessControlBean gmAccessControlBean= new GmAccessControlBean();
		gmAccessControlForm.loadSessionParameters(request);
		strOpt=gmAccessControlForm.getStrOpt();
		log.debug("strOpt==>"+strOpt);
		hmParam = GmCommonClass.getHashMapFromForm(gmAccessControlForm);
		haction=gmAccessControlForm.getHaction();
		log.debug("haction==>"+haction);
		haction=haction.equals("")?strOpt:haction;
		gmAccessControlForm.setHaction(haction);
		//default page to load
		strPage="GmAccessControlGroupSetup";
		/*TO Enable/Disable Button Access-PMT 26730 -System Manager Changes*/
		strUserId = GmCommonClass.parseNull(gmAccessControlForm.getUserId());
	    hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,"SYS_ADMIN_BTN_ACS"));
	    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
	    gmAccessControlForm.setStrAccessFl(strAccessFl);
	    
		if(haction.equals("SCGRP")){
			hmParam.put("GROUPTYPE","92264");
			strGroupType="92264";
		}else if(haction.equals("MDGRP")){
			hmParam.put("GROUPTYPE","92265");
			strGroupType="92265";
		}
		
		if(strOpt.equals("save")||strOpt.equals("edit")){
			log.debug("hmParam==> "+hmParam + "strOpt==>"+strOpt);
			String strGrpId=gmAccessControlBean.saveACGroup(hmParam);
			strGroupType = GmCommonClass.parseNull((String)hmParam.get("GROUPTYPE"));
			gmAccessControlForm.setGroupId(strGrpId);
			strOpt="edit";
			
		}
	
	if(strOpt.equals("report")){
			
			alResult=gmAccessControlBean.fetchAccessActionGroupDetails();
			String strGridXmlData = generateOutPut(alResult,hmTemp);
			gmAccessControlForm.setGridXmlData(strGridXmlData);
			strPage="GmAccessControlGroupReport";
	}else if(strOpt.equals("SGREPORT")){
		strGroupType="92264";
		alResult=gmAccessControlBean.fetchSecurityModuleList(strGroupType);
		log.debug("alResult==>"+alResult);
		hmTemp.put("GRPTYPE", strGroupType);
		String strGridXmlData = generateOutPut(alResult,hmTemp);
		gmAccessControlForm.setGridXmlData(strGridXmlData);
		strPage="GmAccessControlGroupReport";
	}else if(strOpt.equals("MREPORT")){
		strGroupType="92265";
		alResult=gmAccessControlBean.fetchSecurityModuleList(strGroupType);
		hmTemp.put("GRPTYPE", strGroupType);
		String strGridXmlData = generateOutPut(alResult,hmTemp);
		gmAccessControlForm.setGridXmlData(strGridXmlData);
		strPage="GmAccessControlGroupReport";
	}
		
		if(strOpt.equals("editload")){
			strGroupId = GmCommonClass.parseNull((String)hmParam.get("GROUPID"));
			strGroupType = GmCommonClass.parseNull((String)hmParam.get("GROUPTYPE"));
			log.debug("group id====="+strGroupId+"strGroupType==>"+strGroupType);
			hmTemp = gmAccessControlBean.fetchACGroup(strGroupId);
			GmCommonClass.getFormFromHashMap(gmAccessControlForm, hmTemp);
			strOpt="edit";
		}
		
		alGroupName=GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchACGroupDetails(strGroupType));
		gmAccessControlForm.setAlGroupName(alGroupName);
		strOpt=strOpt.equals("")?"save":strOpt;
		if(strOpt.equals("")||strOpt.equals("SCGRP")||strOpt.equals("MDGRP")){
			strOpt="save";
		}
		gmAccessControlForm.setStrOpt(strOpt);
		gmAccessControlForm.setGroupType(strGroupType);
		return mapping.findForward(strPage);
	}
	
	/**
	 * getXmlGridData - This method will be used to generate Output for grid data.
	 * 
	 * @return String - will be used by dhtmlx grid
	 * @exception AppError
	 */
	private String generateOutPut(ArrayList alParam,HashMap hmTemp) throws Exception {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setDataMap("hmTemp", hmTemp);
		templateUtil.setTemplateName("GmAccessControlSetup.vm");
		templateUtil.setTemplateSubDir("user/templates");

		return templateUtil.generateOutput();
	}

}
