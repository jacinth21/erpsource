package com.globus.user.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.user.forms.GmUserNavigationSetupForm;


public class GmUserNavigationSetupAction extends GmAction
{

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   	
        
    	GmUserNavigationSetupForm gmUserNavigationSetupForm = (GmUserNavigationSetupForm)form;
    	  
    	gmUserNavigationSetupForm.loadSessionParameters(request);        
        String strOpt = gmUserNavigationSetupForm.getStrOpt();        
    	String struserId = gmUserNavigationSetupForm.getUserId();
    	HashMap hmParam = GmCommonClass.getHashMapFromForm(gmUserNavigationSetupForm);
 	    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean();
 	   GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
 	    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
 	    
	 	  if(strOpt.equals("add") || strOpt.equals("update"))
	      {  
	 		 gmUserMgmtBean.updateNode(hmParam);	  		
	 		
	      }
	 	  if(strOpt.equals("delete"))
	      {  
	 		 gmUserMgmtBean.deleteNode(hmParam);
	      }

	 	 
	    ArrayList alUserNavigationList = gmUserMgmtBean.fetchUserNavigationLink(hmParam);
	    log.debug("alUserNavigationList:"+alUserNavigationList);
    	// get the access permission for Read only access to portal
	 	HashMap	hmAccess= GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(struserId,"READ_ONLY_ACCESS"));
	 	String strReadOnlyAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
	    String strTreeXML =gmXMLParserUtility.createLeftMenuTree(alUserNavigationList,"",new ArrayList(),strReadOnlyAccess);
	    log.debug("strTreeXML:"+strTreeXML);
	    gmUserNavigationSetupForm.setTreeXmlData(strTreeXML);
	    
	    ArrayList alGroupModuleList = gmUserMgmtBean.fetchModuleGroups();
	    log.debug("alGroupModuleList:"+alGroupModuleList);
	    gmUserNavigationSetupForm.setAlGroupModuleList(alGroupModuleList);
	    
	    HashMap hmReturn = gmUserMgmtBean.fetchMenuList();
	    
	    ArrayList alFolderList = (ArrayList)hmReturn.get("FOLDERLIST");
	    ArrayList alFileList = (ArrayList)hmReturn.get("FILELIST");	    	
	    	 	    	
	    
	    log.debug("alFolderList:"+alFolderList);
	    log.debug("alFileList:"+alFileList);
	    
	    gmUserNavigationSetupForm.setAlFolderList(alFolderList);
	    gmUserNavigationSetupForm.setAlFileList(alFileList);
	    
	   
        return mapping.findForward("load");
    }    	
}