package com.globus.user.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.user.forms.GmMultiUserGroupForm;
import com.globus.common.util.GmTemplateUtil;

public class GmMultiUserGroupAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    String strOpt = "";
    String strDepID = "";
    String strUserId = "";
    String strUserList = "";
    String strSecGrp = "";
    String strUser = "";
    String strSec = "";
    String strGrpId = "";
    String strId = "";
    String strDispatchTo = "GmMultiUserGroupSetup";
    String strOptVal = "";
    String strResult = "";
    String strAction = "";
    String strInputString = "";
    String strUserNm = "";
    String strDefGrp = "";
    String strDefResult = "";
    String strPartyId = "";
    String strAccessFl="";
    String strXmlGridData="";
    
    HashMap hmParam = new HashMap();
    HashMap hmVal = new HashMap();
    HashMap hmValue = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alUser = new ArrayList();
    ArrayList alSecGrp = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alDept = new ArrayList();

    GmMultiUserGroupForm gmMultiUserGroupForm = (GmMultiUserGroupForm) form;
    gmMultiUserGroupForm.loadSessionParameters(request);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean(getGmDataStoreVO());

    strOpt = gmMultiUserGroupForm.getStrOpt();
    strDepID = GmCommonClass.parseNull(gmMultiUserGroupForm.getUserDeptId());
    strGrpId = GmCommonClass.parseNull(gmMultiUserGroupForm.getGrpmapId());   
    HashMap hmUserlist = gmUserMgmtBean.loadUserMgmtLists();
    gmMultiUserGroupForm.setAlCompany((ArrayList) hmUserlist.get("COMPANY"));
    if (strDepID.equals("0") || strDepID.equals("")) {
      strDepID = gmMultiUserGroupForm.getUserDept();
    }
   
    strAction = GmCommonClass.parseNull(gmMultiUserGroupForm.getHaction());
    strUserId = GmCommonClass.parseNull(gmMultiUserGroupForm.getUserId());
    String strCompanyId = GmCommonClass.parseNull(gmMultiUserGroupForm.getCompanyId());

    alUser = GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchUserList(strDepID));
    alSecGrp = GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchACGroupDetails("92264"));
    strOptVal = GmCommonClass.parseNull(gmMultiUserGroupForm.getOptVal());
    alDept = gmCommon.getCodeList("DEPID");
    
    /*TO Enable/disable button access-PMT-26730*/
    hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,"SYS_ADMIN_BTN_ACS"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmMultiUserGroupForm.setStrAccessFl(strAccessFl);
    gmMultiUserGroupForm.setAlDept(alDept);
    gmMultiUserGroupForm.setAlUserList(alUser);
    gmMultiUserGroupForm.setAlSecGrp(alSecGrp);

    if (strAction.equals("save")) {
      strInputString = GmCommonClass.parseNull(gmMultiUserGroupForm.getHinputstring());

      if (strOpt.equals("SECUSR")) {
        strId = GmCommonClass.parseNull(gmMultiUserGroupForm.getSecGrp());
      } else {
        strId = GmCommonClass.parseNull(gmMultiUserGroupForm.getUserList());
        strDefGrp = GmCommonClass.parseNull(gmMultiUserGroupForm.getDefaultgrp());
      }

      hmParam.put("USERID", strUserId);
      hmParam.put("HINPSTR", strInputString);
      hmParam.put("PID", strId);
      hmParam.put("OPT", strOpt);
      hmParam.put("COMPANYID", strCompanyId);
      hmParam.put("DEFAULTGRP", strDefGrp);
      gmAccessControlBean.saveMultiUserGroup(hmParam);

    } else if (strOpt.equals("edit")) {
      hmParam.put("GRPMAPPINGID", strGrpId);
      hmParam.put("USERNM", "0");
      hmParam.put("GROUPNAME", "0");
      hmParam.put("USERDEPT", strDepID);
      hmParam.put("COMPANYID", strCompanyId);
      alResult =
          GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmParam));

      if (alResult.size() > 0) {
        hmValue = GmCommonClass.parseNullHashMap((HashMap) alResult.get(0));
        strUserNm = GmCommonClass.parseNull((String) hmValue.get("USERNM"));
        gmMultiUserGroupForm.setStrOpt("USRSEC");
        gmMultiUserGroupForm.setUserList(strUserNm);
        gmMultiUserGroupForm.setCompanyId(strCompanyId);
      }

    } else if (strOpt.equals("SECUSR") && !strOptVal.equals("")) {
      hmParam.put("USERNM", "0");
      hmParam.put("GROUPNAME", strOptVal);
      hmParam.put("USERDEPT", "0");
      hmParam.put("COMPANYID", strCompanyId);
      strResult = "";
      alResult =
          GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmParam));
      for (int i = 0; i < alResult.size(); i++) {
        hmVal = GmCommonClass.parseNullHashMap((HashMap) alResult.get(i));
        strResult += (String) hmVal.get("USERNM") + ",";
      }
      PrintWriter writer = response.getWriter();
      writer.println(strResult);
      writer.close();
      return null;

    } else if (strOpt.equals("USRSEC") && !strOptVal.equals("")) {
      hmParam.put("USERNM", strOptVal);
      hmParam.put("GROUPNAME", "0");
      hmParam.put("USERDEPT", "0");
      hmParam.put("COMPANYID", strCompanyId);
      hmParam.put("HUSERID", strOptVal);
      // Getting the partyId for the user.
      hmReturn = GmCommonClass.parseNullHashMap(gmUserMgmtBean.fetchUserdetail(hmParam));
      strPartyId = GmCommonClass.parseNull((String) hmReturn.get("PARTYID"));
      hmParam.put("PARTYID", strPartyId);
      // Getting Default group based on party not company in the User To Security group mapping
      // Screen.
      strDefResult =
          GmCommonClass.parseNull(gmAccessControlBean.getDefaultGroup(strOptVal) + "^" + "Y") + ",";

      alResult =
          GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmParam));
      for (int i = 0; i < alResult.size(); i++) {
        hmVal = GmCommonClass.parseNullHashMap((HashMap) alResult.get(i));
        strResult +=
            GmCommonClass.parseNull((String) hmVal.get("GROUPNAME")) + "^"
                + GmCommonClass.parseNull((String) hmVal.get("DEFAULTGRPFORCB")) + ",";
      }
      // Default group is mapped to any of the company.That default group will come to all the
      // companies in Default group section in user to security Mapping screen.
      strResult = strResult + '@' + strDefResult;
      PrintWriter writer = response.getWriter();
      writer.println(strResult);
      writer.close();
      return null;
    }
    strUserId = gmMultiUserGroupForm.getUserId();
    /*TO Enable/disable button access-PMT-26730*/
    if (strAction.equals("history")) {
    	  strDispatchTo = "GmMultiUserGroupHistory";
    	  if (strOpt.equals("SECUSR") || strOpt.equals("USRSEC")) {
    	    hmParam = GmCommonClass.getHashMapFromForm(gmMultiUserGroupForm);
    	    alResult = GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchLogDetails(hmParam));
    	    hmReturn.put("STROPT",strOpt);
    	    hmReturn.put("ALRESULT", alResult);
    	    hmReturn.put("TEMPLATENAME", "GmSecGrpHistoryReport.vm");
 			strXmlGridData = generateOutPut(hmReturn);
 			gmMultiUserGroupForm.setGridXmlData(strXmlGridData); 
    	     }  
    }

    return mapping.findForward(strDispatchTo);
  }
  
  /** 
 * @param hmParam
 * @return
 * @throws AppError
 */
public String generateOutPut(HashMap hmParam) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		ArrayList alList = new ArrayList();
		alList = GmCommonClass.parseNullArrayList((ArrayList) hmParam
				.get("ALRESULT"));
	    String strTempName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATENAME"));
		templateUtil.setDataMap("hmParam", hmParam);
		templateUtil.setDataList("alList", alList);
		templateUtil.setTemplateSubDir("user/templates");	
		templateUtil.setTemplateName(strTempName);
		return templateUtil.generateOutput();
	}
  
  
}
