package com.globus.user.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmUserBean;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.user.forms.GmUserADStatusUpdateForm;

public class GmUserADStatusUpdateAction extends GmAction
{

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws Exception
    {   	
        
    	GmUserADStatusUpdateForm gmUserADStatusUpdateForm = (GmUserADStatusUpdateForm)form;
    	  
    	gmUserADStatusUpdateForm.loadSessionParameters(request);        
        String strOpt = gmUserADStatusUpdateForm.getStrOpt();
                
        
    	HashMap hmParam = GmCommonClass.getHashMapFromForm(gmUserADStatusUpdateForm);  
    	
    	String strCipherPassword = (String)request.getSession().getAttribute("strJunk");
    	String strCipherUserName = (String)request.getSession().getAttribute("strLogonName");
    	log.debug("In ADStatus Update Action strCipherUserName: "+strCipherUserName);
    	log.debug("In ADStatus Update Action strCipherPassword: "+strCipherPassword);
    	
    	hmParam.put("ENCUSERNAME", strCipherUserName);
    	hmParam.put("ENCPASSWORD", strCipherPassword);

 	    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean();
 	    GmAccessControlBean	gmAccessControlBean = new GmAccessControlBean();
 	    String strPartyId = gmUserADStatusUpdateForm.getSessPartyId(); 	    
 	    ArrayList alPartyList = gmAccessControlBean.getPartyList("20",strPartyId);
 	    
 	    if (alPartyList.size()==0) {
			throw new AppError("No Access to View the page", "", 'E');
		}
 	    
 	   if(strOpt.equals("load"))
       {   
 		   	ArrayList alUserList = new ArrayList();
 	    	alUserList = (ArrayList)gmUserMgmtBean.fetchUserADStatusList(hmParam);
 	    	
 	    	log.debug("alUserList:"+alUserList);   
 	    	
 	    	ArrayList alADStatusList= GmCommonClass.getCodeList("ADSTUS");
 	    	ArrayList alUserStatusList= GmCommonClass.getCodeList("EMPST"); 	    	
 	    	
 	    	String strGridXmlData = generateOutPut(alUserList,alADStatusList,alUserStatusList);
 	    	strGridXmlData = GmCommonClass.replaceForXML(strGridXmlData);
 	    	gmUserADStatusUpdateForm.setGridXmlData(strGridXmlData);            
       }  
 	  else  if(strOpt.equals("save"))
      {  
 		 HashMap hmReturn = gmUserMgmtBean.updateUserADStatusList(hmParam);
 		 
 		 log.debug("hmReturn:"+hmReturn); 
 		 
 		 String strEnableUsers = (String)hmReturn.get("ENABLEDUSERS");
 		 String strDisableUsers = (String)hmReturn.get("DISABLEDUSERS");
 		 String strSuccessMessage="";
 		 
 		 if(strEnableUsers.length()>0)
 			strSuccessMessage = strSuccessMessage + "Following users are enabled successfully:"+strEnableUsers;
 		 if(strDisableUsers.length()>0)
 			strSuccessMessage = strSuccessMessage + "\nFollowing users are disabled successfully:"+strDisableUsers;
 		
 		 throw new AppError(strSuccessMessage, "S", 'S');
      }
    	
        return mapping.findForward("load");
    }   
    
    /**
	 * generateOutPut This method will generate xml content for grid from given arraylist data
	 * @param alGridData ArrayList
	 * @param alADStatusList ArrayList
	 * @return String 
	 */
	private String generateOutPut(ArrayList alGridData,ArrayList alADStatusList,ArrayList alUserStatusList) throws Exception {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setDataList("alGridData", alGridData);
		templateUtil.setDataList("alADStatusData", alADStatusList);
		templateUtil.setDataList("alUserStatusData", alUserStatusList);
		templateUtil.setTemplateSubDir("user/templates");
		templateUtil.setTemplateName("GmUserADStatusUpdate.vm");
		return templateUtil.generateOutput();
	}
}