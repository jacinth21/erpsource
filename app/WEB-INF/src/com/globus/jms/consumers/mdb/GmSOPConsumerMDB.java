package com.globus.jms.consumers.mdb;

import java.util.HashMap;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;


import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.jms.consumers.controller.GmConsumerController;

//The below annotation is should work for JBOSS as well as WEBLOGIC.
/**
 * @author Mselvamani
 * GmSOPConsumerMDB :This MDB class used for JMS Call
 */

@MessageDriven(activationConfig = {
@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
@ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/gmsopemailqueue"),
@ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")})  
//This indicates that this MDB will participate in Bean managed transaction.The default attribute is CONTAINER
@TransactionManagement(TransactionManagementType.BEAN)

public class GmSOPConsumerMDB implements MessageListener  {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
    This method is called asynchronously by JMS when a message arrives
    at the queue. Client applications must not throw any exceptions in
    the onMessage method.
    @param message A JMS message.
  */
	//Means that the method will not run in an transaction, if one already exists then this is suspended until the method completes then resumes., this is useful for an MDB supporting a JMS provider in a non-transactional, autoknowledge mode.
	//specifies if CMT or BMT is used for a particular bean, the default is CMT, the options are
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		log.debug("inside onMessage..");
		HashMap hmReturn = new HashMap();
		Object msgObject = null;
		GmMessageTransferObject msgTransferObject =  new GmMessageTransferObject();
		GmConsumerController consumerController = new GmConsumerController();
		try {
			
			ObjectMessage msg = (ObjectMessage) message;
			//Get transfer object passed from client
			log.debug("Null="+(msg == null) );
			msgObject = getTransferObjectFrmHashMap(msg.getObject());
			consumerController.setMessageTransferObject(msgObject);
			
			//Controller class will dispatch message to appropriate class set in GmMessagetransferObject property CounsumerClass
	
			consumerController.dispathMessage();
			log.debug("======3====");
		
	} catch (Exception ex) {
		hmReturn.put("EXCEPTIONOBJ",ex);
		hmReturn.put("MDBCLASS","GmSOPConsumerMDB");
		Exception excep = (Exception)hmReturn.get("EXCEPTIONOBJ");
		
		if(excep != null)
		{
	        	GmConsumerUtil conutil = new GmConsumerUtil();
				conutil.sendJMSConExceptionEmail((GmMessageTransferObject)msgObject,hmReturn);
		}
		throw new RuntimeException("Exception in GmSOPConsumerMDB onMessage()");
	}
		
	}
	
	/**
	 * @param msgObject
	 * @return
	 * @throws Exception
	 */
	private GmMessageTransferObject getTransferObjectFrmHashMap(Object msgObject) throws Exception{
		if(msgObject instanceof HashMap){
			GmMessageTransferObject msgTransferObject =  new GmMessageTransferObject();
			HashMap<String, String> hmParam = new HashMap<String, String>();
			hmParam =(HashMap<String, String>) msgObject;
			msgTransferObject.setConsumerClass(hmParam.get("CONSUMERCLASS"));
			msgTransferObject.setMessageObject(hmParam);
			return msgTransferObject;
		}else{
			return (GmMessageTransferObject) msgObject;
		}
	}
}

