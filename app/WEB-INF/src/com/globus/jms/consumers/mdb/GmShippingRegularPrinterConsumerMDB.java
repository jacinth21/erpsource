package com.globus.jms.consumers.mdb;

import java.util.HashMap;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.jms.consumers.controller.GmConsumerController;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destination",
        propertyValue = "jms/gmShippingRegularPrintqueue"),
    @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")})
@TransactionManagement(TransactionManagementType.BEAN)
public class GmShippingRegularPrinterConsumerMDB implements MessageListener {
  /**
   * This method is called asynchronously by JMS when a message arrives at the queue. Client
   * applications must not throw any exceptions in the onMessage method.
   * 
   * @param message A JMS message.
   */
  @Override
  @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
  public void onMessage(Message message) {
    HashMap hmReturn = new HashMap();
    Object msgObject = null;
    try {


      ObjectMessage msg = (ObjectMessage) message;
      // Get transfer object passed from client
      msgObject = msg.getObject();
      GmConsumerController consumerController = new GmConsumerController();
      consumerController.setMessageTransferObject(msgObject);
      consumerController.dispathMessage();

    } catch (Exception ex) {
      hmReturn.put("EXCEPTIONOBJ", ex);
      hmReturn.put("MDBCLASS", "GmShippingRegularPrinterConsumerMDB");
      Exception excep = (Exception) hmReturn.get("EXCEPTIONOBJ");
      if (excep != null) {
        GmConsumerUtil conutil = new GmConsumerUtil();
        conutil.sendJMSConExceptionEmail((GmMessageTransferObject) msgObject, hmReturn);
      }
      throw new RuntimeException("Exception in GmShippingRegularPrinterConsumerMDB onMessage()");
    }
  }
}
