package com.globus.jms.consumers.mdb;



import java.util.HashMap;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.jms.consumers.controller.GmConsumerController;
// import org.apache.log4j.Logger;

// The below annotation is specific to Weblogic
/*
 * @MessageDriven(name ="GmNotificationConsumerMDB",mappedName = "jms/gmqueue",activationConfig = {
 * 
 * @ActivationConfigProperty(propertyName="initialContextFactory",
 * propertyValue="weblogic.jndi.WLInitialContextFactory"),
 * 
 * @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
 */

// The below annotation is should work for JBOSS as well as WEBLOGIC.
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "destination",
        propertyValue = "jms/gmnotificationqueue"),
    @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")})
// This indicates that this MDB will participate in Bean managed transaction.The default attribute
// is CONTAINER
@TransactionManagement(TransactionManagementType.BEAN)
public class GmNotificationConsumerMDB implements MessageListener {

  // Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This method is called asynchronously by JMS when a message arrives at the queue. Client
   * applications must not throw any exceptions in the onMessage method.
   * 
   * @param message A JMS message.
   */
  @Override
  // Means that the method will not run in an transaction, if one already exists then this is
  // suspended until the method completes then resumes., this is useful for an MDB supporting a JMS
  // provider in a non-transactional, autoknowledge mode.
  // specifies if CMT or BMT is used for a particular bean, the default is CMT, the options are
  @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
  public void onMessage(Message message) {
    HashMap hmReturn = new HashMap();
    Object msgObject = null;
    try {


      ObjectMessage msg = (ObjectMessage) message;
      // Get transfer object passed from client
      msgObject = msg.getObject();
      // Controller class will dispatch message to appropriate class set in GmMessagetransferObject
      // property CounsumerClass
      GmConsumerController consumerController = new GmConsumerController();
      consumerController.setMessageTransferObject(msgObject);
      consumerController.dispathMessage();

    } catch (Exception ex) {

      // log.error("Exception JMS GmNotificationConsumerMDB onMessage(): " + ex.getMessage());
      hmReturn.put("EXCEPTIONOBJ", ex);
      hmReturn.put("MDBCLASS", "GmNotificationConsumerMDB");

      Exception excep = (Exception) hmReturn.get("EXCEPTIONOBJ");

      if (excep != null) {
        // log.error("Exception in sendJasperMail " + excep.getMessage());
        GmConsumerUtil conutil = new GmConsumerUtil();
        conutil.sendJMSConExceptionEmail((GmMessageTransferObject) msgObject, hmReturn);

      }
      throw new RuntimeException("Exception in GmNotificationConsumerMDB onMessage()");
    }

  }

  /**
   * To implement this method this class has to implement ExceptionListener This method is called
   * asynchronously by JMS when some error occurs. Since JMS have no way to report errors.This
   * method will later be used to implement exception going further.
   * 
   * @param exception A JMS exception.
   * 
   @Override public void onException(JMSException exception) {
   *           System.out.println("GmNotificationConsumerMDB............onException");
   *           exception.getLinkedException(); exception.printStackTrace(); throw new
   *           RuntimeException("onException");
   * 
   *           }
   */

}
