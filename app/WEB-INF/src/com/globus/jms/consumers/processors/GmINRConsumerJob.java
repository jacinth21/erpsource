package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.sales.inrquote.bean.GmSetAllocationBean;
import com.globus.valueobject.common.GmDataStoreVO;
 

/**
 * 
 * @author asingaravel
 * Class Name: GmINRConsumerJob.java
 * Description:This class is used to update INR,SET and PLACE order confirm and send notification email
 * 
 */
public class GmINRConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public static final String TEMPLATE_RFSFAILURE = "GmQuoteRFSFailure";	  
	public static final String TEMPLATE_CONFIRM = "GmQuoteSetConfirm";
	/* (non-Javadoc)
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
	 */
	
	 @Override
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		HashMap hmParam = (HashMap) objMessageObject;
		String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
		if(strAction.equals("SET_CONFIRM")) {
			setConfirmation(objMessageObject, null);
		}
		if(strAction.equals("INR_CONFIRM")) {
			inrConfirmation(objMessageObject, null);
		}
		if(strAction.equals("SAVEORDER")) {
			saveOrderJMS(objMessageObject, null);
		}
	}

	/* (non-Javadoc)
	 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
	 */
	
	
	public void execute(JobDataMap jobDataMap) throws Exception {
		setConfirmation(null, jobDataMap); 
		inrConfirmation(null, jobDataMap);
		saveOrderJMS(null, jobDataMap);
	}
	 
	
	  /**PC-4062
	 * Method - setConfirmation used to update confirmation flag details and send email notification
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @return 
	 * @throws AppError
	 */
	
	public void setConfirmation(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    
		
	    HashMap hmParam = (HashMap) objMessageObject; 
	    GmSetAllocationBean gmSetAllocationBean = new GmSetAllocationBean();
	    
		String strMsg="";
		HashMap hmHeaderDtls = new HashMap();
		HashMap hmRFSErrorDtls = new HashMap();
		
		strMsg = gmSetAllocationBean.checkRFSValidation(hmParam);
		
		hmHeaderDtls = gmSetAllocationBean.fetchHeaderDtls(hmParam);
		hmParam.put("HMHEADERDTLS",hmHeaderDtls);
		hmParam.put("STRMSG",strMsg);
				
		if (strMsg.equals("Y")) { //Quote set allocation confirm
			hmParam.put("CNFL","Y");
			gmSetAllocationBean.updateCNConfirmFlag(hmParam);
			gmSetAllocationBean.updateSetConfirmedbyDtls(hmParam);
			gmSetAllocationBean.updateQuoteCategoryStatus(hmParam); 
			senMail(hmParam);
		}else { //RFS validation failed
			hmParam.put("CNFL","");
			gmSetAllocationBean.updateCNConfirmFlag(hmParam);
			hmRFSErrorDtls = gmSetAllocationBean.fetchRFSPartsDtls(hmParam);
			hmParam.put("HMRFSERRORDTLS", hmRFSErrorDtls);	
			senMail(hmParam);
		}	
}
	
	  /**PC-4062
	 * Method - inrConfirmation used to RFS failure send email notification
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @return 
	 * @throws AppError
	 */
	
	public void inrConfirmation(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    
	    HashMap hmParam = (HashMap) objMessageObject; 
	    GmSetAllocationBean gmSetAllocationBean = new GmSetAllocationBean();
	    
		String strMsg="";
		HashMap hmHeaderDtls = new HashMap();
		HashMap hmRFSErrorDtls = new HashMap();
		
		strMsg = gmSetAllocationBean.checkRFSValidation(hmParam);
		
		hmHeaderDtls = gmSetAllocationBean.fetchHeaderDtls(hmParam);
		hmParam.put("HMHEADERDTLS",hmHeaderDtls);
		hmParam.put("STRMSG",strMsg);
				
		if (strMsg.equals("N")) { //RFS validation failed
			hmRFSErrorDtls = gmSetAllocationBean.fetchRFSPartsDtls(hmParam);
			hmParam.put("HMRFSERRORDTLS", hmRFSErrorDtls);	
			senMail(hmParam);
		}	
}	
	  /**
	 * Method - senMail is used to send email notification 
	 * 
	 * @param hmParam
	 * @return 
	 * @throws AppError
	 */
		
	public void senMail(HashMap hmParam) throws AppError{
		GmEmailProperties emailProps = new GmEmailProperties();
		GmJasperMail jasperMail = new GmJasperMail();
		
		ArrayList alResult = new ArrayList();
		
		String strQuoteId = GmCommonClass.parseNull((String) hmParam.get("QUOTEID"));		
		String strMsg = GmCommonClass.parseNull((String) hmParam.get("STRMSG"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strCmpCurrFmt = GmCommonClass.parseNull((String) hmParam.get("CMPCURRFMT"));
		
		if(strMsg.equals("Y")) {  //Quote set allocation confirm email notification
			sendSuccessEmail(emailProps); 
		}else { //RFS validation failed
			sendFailureEmail(emailProps,strQuoteId); 
		}
		
		HashMap hmHeaderDtls =  (HashMap) hmParam.get("HMHEADERDTLS");
		hmHeaderDtls.put("COMPANYINFO", strCompanyInfo);
		hmHeaderDtls.put("CMPCURRFMT", strCmpCurrFmt);
		if(strMsg.equals("Y")) { //Quote set allocation confirm
			jasperMail.setJasperReportName("/GmQuoteSetAllocationConfirm.jasper");		    
		    jasperMail.setReportData(null);		   
		}else {
			HashMap hmRFSdetails =  (HashMap) hmParam.get("HMRFSERRORDTLS");
			ArrayList arrayRFSErrorDtls = (ArrayList) hmRFSdetails.get("RFSdetails");
			jasperMail.setJasperReportName("/GmQuoteRFSFailure.jasper");
		    jasperMail.setReportData(arrayRFSErrorDtls);		    
		}  
		jasperMail.setAdditionalParams(hmHeaderDtls);
		jasperMail.setEmailProperties(emailProps);
	    jasperMail.sendMail();
	}
	
	//RFS error details email notification
	private void sendFailureEmail(GmEmailProperties emailProps,String strQuoteId) {
		
		String strTo = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ROQTTO", "ROQTRFS"));
		String strFrom = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_RFSFAILURE + "."+ GmEmailProperties.FROM));
		String strMimeType = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_RFSFAILURE + "." + GmEmailProperties.MIME_TYPE));
		String strSubject = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_RFSFAILURE+ "." + GmEmailProperties.SUBJECT));
		String strEmailHeader = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_RFSFAILURE+ "." + GmEmailProperties.EMAIL_HEADER_NM));
		
		strSubject = strSubject+" "+strQuoteId; 
		emailProps.setMimeType(strMimeType);
		emailProps.setRecipients(strTo);
		emailProps.setSender(strFrom);
		emailProps.setSubject(strSubject);
		emailProps.setEmailHeaderName(strEmailHeader);
	}

	//Quote set allocation confirm email notification
	private void sendSuccessEmail(GmEmailProperties emailProps) {
		
		String strTo = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ROQTTO", "ROQT"));
		String strCc = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ROQTCC", "ROQT"));
		String strFrom = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_CONFIRM + "."+ GmEmailProperties.FROM));
		String strMimeType = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_CONFIRM + "." + GmEmailProperties.MIME_TYPE));
		String strSubject = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_CONFIRM+ "." + GmEmailProperties.SUBJECT));
		String strEmailHeader = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_CONFIRM+ "." + GmEmailProperties.EMAIL_HEADER_NM));
	
		emailProps.setMimeType(strMimeType);
		emailProps.setRecipients(strTo);
		emailProps.setSender(strFrom);
		emailProps.setCc(strCc);
		emailProps.setSubject(strSubject);
		emailProps.setEmailHeaderName(strEmailHeader);		
	}
	  /**PC-4812
		 * Method - saveOrderJMS used to Create order 
		 * 
		 * @param objMessageObject
		 * @param jobDataMap
		 * @return 
		 * @throws AppError
		 */
		
		public void saveOrderJMS(Object objMessageObject, JobDataMap jobDataMap)
			      throws AppError {
			    
		    HashMap hmParam = (HashMap) objMessageObject; 
		    GmSetAllocationBean gmSetAllocationBean = new GmSetAllocationBean();
		    gmSetAllocationBean.saveOrderJms(hmParam);		
	}	
	
}
