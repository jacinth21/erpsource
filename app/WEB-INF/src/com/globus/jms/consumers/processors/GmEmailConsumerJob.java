
package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.email.GmEmailBean;
import com.globus.common.email.GmEmailTrackingApiTransBean;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author karthik
 * Class Name: GmEmailConsumerJob.java
 * Description:This class is used to process the email with the jobDataMap
 */
public class GmEmailConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
	  processEmail(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
	  processEmail(objMessageObject, null);
  }
  /*
   * processEmail(): This method will used to call the methods initialized in the GmEmailManagerInterface class
   * */
  public void processEmail(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
	  	HashMap hmParam = (HashMap) objMessageObject;
	  	
	  	// PC-3000: integrate email to sendgrid api
	    String strEmailTrackingFl = "";
	    String strEnvEmailTracking = "";
	    String strCompanyInfo = "";
	    
	    strEmailTrackingFl = GmCommonClass.parseNull((String) hmParam.get("EMAIL_TRACKING_FL"));
	    strEnvEmailTracking = GmCommonClass.parseNull((String) System.getProperty("ENV_EMAIL_TRACK_FL"));
	    
	  	log.debug(" strEmailTrackingFl ====>>> "+ strEmailTrackingFl +" strEnvEmailTracking ==> "+ strEnvEmailTracking);
	  	
		if (strEmailTrackingFl.equals("Y") && strEnvEmailTracking.equals("Y")) {
			
			GmDataStoreVO tmpGmDataStoreVO = null;
			strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo"));
			if (strCompanyInfo.equals("")) {
				tmpGmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
			} else {
				tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
			}

			// to call the API
			GmEmailTrackingApiTransBean gmEmailTrackingApiTransBean = new GmEmailTrackingApiTransBean(tmpGmDataStoreVO);
			try {
				gmEmailTrackingApiTransBean.sendEmail(hmParam);
			} catch (Exception Ex) {
				// to call the AWS email
				GmEmailBean.gmEmailManager.setUpEmail(hmParam);
				GmEmailBean.gmEmailManager.sendEmail();
			}

		} else {
			GmEmailBean.gmEmailManager.setUpEmail(hmParam);
			GmEmailBean.gmEmailManager.sendEmail();
		}
	  	
  		GmEmailBean.gmEmailManager.saveMailInDB();
  			  	
  } 
}