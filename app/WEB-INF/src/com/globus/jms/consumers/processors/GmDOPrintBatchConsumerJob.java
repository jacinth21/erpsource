package com.globus.jms.consumers.processors;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDOPrintBatchConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    if (strProcessType.equals("QUARTZJOB")) {
      processBatchCommon(null, jobDataMap);
    }
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {

    processBatchCommon(objMessageObject, null);
  }

  public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

    HashMap hmMessageObj = (HashMap) objMessageObject;
    String strBatchID = GmCommonClass.parseNull((String) hmMessageObj.get("BATCHID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmMessageObj.get("companyInfo"));
    GmDataStoreVO tmpGmDataStoreVO = null;
    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    String resourcename = "GmEditOrderServlet";
    GmARBatchBean gmARBatchBean = new GmARBatchBean(tmpGmDataStoreVO);
    GmBatchBean gmBatchBean = new GmBatchBean(tmpGmDataStoreVO);
    ArrayList alResult = new ArrayList();
    HashMap hmDOSummary = new HashMap();

    String strOrderID = "";
    String strPara = "&hMode=PrintPrice&type=savePDFfile";
    String strAddtlPara = "";
    String strPDFFileName = "";
    String strDOSummaryPDFLocation = "";
    String strErrorOrders = "";
    int intArLength = 0;

    HashMap hmStatus = new HashMap();
    // to get the batch details
    hmDOSummary.put("BATCHID", strBatchID);
    hmDOSummary.put("BATCHTYPE", "18752"); // DO Summary Print
    hmDOSummary.put("BATCHSTATUS", "18743"); // Process in progress
    alResult = GmCommonClass.parseNullArrayList(gmBatchBean.fetchBatchDetailsReport(hmDOSummary));
    intArLength = alResult.size();

    if (intArLength > 0) {
      HashMap hmLoop = new HashMap();
      strDOSummaryPDFLocation = GmCommonClass.getString("BATCH_DO_PRINT");
      for (int i = 0; i < intArLength; i++) {
        strAddtlPara = "";
        hmLoop = (HashMap) alResult.get(i);
        strBatchID = GmCommonClass.parseNull((String) hmLoop.get("BATCHID"));
        strOrderID = GmCommonClass.parseNull((String) hmLoop.get("REFID"));
        strAddtlPara = "&hOrdId=" + strOrderID + "&hBatchID=" + strBatchID;

        hmLoop.put("RESOURCE", resourcename);
        hmLoop.put("PARAMETER", strPara + strAddtlPara);
        hmLoop.put("companyInfo", strCompanyInfo);

        boolean bPDFFileCreated = false;

        strPDFFileName = strDOSummaryPDFLocation + "\\" + strOrderID + ".pdf";

        hmLoop.put("PDFFILENAME", strPDFFileName);
        hmLoop.put("REFTYPE", "18752"); // Do Summary Batch
        
        // PMT-33783: To avoid duplicate print - DO Summary batch
        // If any special char on order comments, to create PDF/printing not happen JMS called 10 times and printing the Duplicate paper.
        // So, now to handle the exception - we can avoid the duplicate print.
        
        try{
	        // save the PDF file
	        bPDFFileCreated = gmARBatchBean.saveDOSummaryasPDF(hmLoop);
	        // to save the status for files created
	        hmLoop.put("ACTIONTYPE", "1801"); // 1801 - File
	        gmBatchBean.saveBatchLog(hmLoop);
	        // to print the PDF file
	        gmARBatchBean.print(hmLoop);
	        // to save the status for printed successfully
	        hmLoop.remove("ACTIONTYPE");
	        hmLoop.put("ACTIONTYPE", "1802"); // 1802 Print
	        gmBatchBean.saveBatchLog(hmLoop);
	        // after print the PDF file to be deleted.
	        File newPDFFile = new File(strPDFFileName);
	        if (newPDFFile.exists()) {
	          newPDFFile.deleteOnExit();
	        } 
	        
        }catch (Exception ex){
        	strErrorOrders = strErrorOrders.concat(strOrderID.concat(", "));
        	ex.printStackTrace();
        } 
        
      } // End of FOR loop

      hmStatus.put("BATCHID", strBatchID);
      gmBatchBean.saveBatchStatus(hmStatus);
      // PMT-33783 : To avoid duplicate print
      // Check error string and send email (error batch and order details)
      
      if(!strErrorOrders.equals("")){
    	  // to remove the comma symbol
    	  strErrorOrders = strErrorOrders.substring(0, strErrorOrders.length() -2 );
    	  // call the send email
     	  sendDOBatchExceptionEmail ( strBatchID, strErrorOrders, tmpGmDataStoreVO.getCmpid());
      }
      
    } // end if (length check)
  }

  /**
 *
 * sendDOBatchExceptionEmail - This method used to send the DO summary exception email
 * 
 * @param hmParam
 * @throws AppError
 */
  
	private void sendDOBatchExceptionEmail (String strBatchId,String strErrorOrders, String strCompanyID) throws Exception{
		GmEmailProperties gmEmailProperties = new GmEmailProperties();
		String TEMPLATE_NAME = "GmDOSummaryExceptionMail";
		String strCompanyLocale = "";
		 String strMailTo = "";
		 String strFrom = "";
		 String strMimeType = "";
		 String strSubject = "";
		 String strMsgBody = "";
		 
		 try{
			 // based on company to get company Locale
			 strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyID);
			 // get email properties object
			 GmResourceBundleBean gmResourceBundleBean =
					 GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
		     
		     //	get the values from properties
		     strFrom = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM));
		     strMimeType = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
		     strSubject = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT));
		     strMsgBody  = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_BODY));
		     
		      strMailTo = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue("DO_SUMMARY_EXCEPTION", "JMS_EXCEP_NOTIFY")) ;
		      // add dynamic values
		      strSubject = GmCommonClass.replaceAll(strSubject, "#<BATCHID>", strBatchId);
		      strMsgBody = GmCommonClass.replaceAll(strMsgBody, "#<ORDERS>", strErrorOrders);
		      
		      // to set the email details
		      gmEmailProperties.setSender(strFrom);
		      gmEmailProperties.setRecipients(strMailTo);
		      gmEmailProperties.setMimeType(strMimeType);
		      gmEmailProperties.setSubject(strSubject);
		      gmEmailProperties.setMessage(strMsgBody);
		      
		      GmCommonClass.sendMail(gmEmailProperties);
		      
		 } catch (Exception ex){
			 ex.printStackTrace();
		 }
	      
	  } // end sendDOBatchExceptionEmail
  
}
