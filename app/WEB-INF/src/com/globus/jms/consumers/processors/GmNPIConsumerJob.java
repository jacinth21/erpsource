/**
 * 
 */
package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.beans.GmSplitTransInterface;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.party.beans.GmNPIProcessBean;
import com.globus.party.forms.GmNPIProcessForm;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author sindhu
 *
 */
public class GmNPIConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
	    processNPIConsumer(null, jobDataMap);
	  }
	
	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processNPIConsumer(objMessageObject, null);
	  }
	  
	  
	  /**
	   * processNPIConsumer - The below method to validate NPI number and save status
	   * 
	   * @param -objMessageObject:it contains CONSUMERCLASS, transaction id, transaction type
	   * @exception - Exception
	   */ 
	  public void processNPIConsumer(Object objMessageObject, JobDataMap jobDataMap)
		      throws Exception {
	       log.debug("GmNPIConsumerJob TransactionId is ");
		   HashMap hmParam = (HashMap) objMessageObject;  
		   HashMap hmResult = new HashMap();
		   ArrayList alNPIDetails = new ArrayList();
		   String strStatusfl = "";
		   
		   String strTransactionId = GmCommonClass.parseNull((String)hmParam.get("TRANSID"));//Transaction ID 
		   String strNpiNum = GmCommonClass.parseNull((String)hmParam.get("NPINO")); //NPI number
		   String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID")); 
		   String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		   
		   GmDataStoreVO gmDataStoreVO = null;
           gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
           GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(gmDataStoreVO);
		   hmParam.put("TXNID", strTransactionId);
		   log.debug("GmNPIConsumerJob TransactionId is "+strTransactionId);
		   try{
    		   if(!strNpiNum.equals("")){
        		     hmResult.put("NPIID", strNpiNum);
        		     alNPIDetails.add(hmResult);
    		   }else{
    		         alNPIDetails = gmNPIProcessBean.fetchNPITransactionDetails(hmParam);
    		   }
    		         log.debug("GmNPIConsumerJob alNPIDetails is "+alNPIDetails.size());
      		   for (int i=0;i<alNPIDetails.size();i++ ){
        		    hmResult = (HashMap) alNPIDetails.get(i);
        		    strNpiNum = (String) hmResult.get("NPIID");
        		    log.debug("GmNPIConsumerJob strNpiNum "+strNpiNum);
        	        hmParam.put("NPINO", strNpiNum);
        	        strStatusfl = gmNPIProcessBean.saveNewNPI(strNpiNum,strTransactionId,strUserId);
        	            
        	        if(strStatusfl.equals("N")){
        	          gmNPIProcessBean.saveNewNPIMasterStatus(strNpiNum,strTransactionId,strStatusfl,strUserId);
        	          gmNPIProcessBean.invalidNPISendMail(hmParam);
        	        }
      		    }
		   }catch(Exception exe){
             log.error("Exception occured in the class GmNPIConsumerJob : "  + exe.getMessage());
     }
	  }
}
