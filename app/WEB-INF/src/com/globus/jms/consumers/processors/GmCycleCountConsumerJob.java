/**
 * 
 */

package com.globus.jms.consumers.processors;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.accounts.cyclecount.beans.GmCycleCountLockTransBean;

/**
 * @author asingaravel
 *
 */
public class GmCycleCountConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	// instantiate(request, response);
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {
		processCycleCountLotUpdate(null, jobDataMap);

	}
	
	@Override
	public void processMessage(Object objMessageObject) throws Exception {
		processCycleCountLotUpdate(objMessageObject, null);
	}
	
	/**
	 * Method - processCycleCountLotUpdate used once when NCMR created or closed
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */

	public void processCycleCountLotUpdate(Object objMessageObject,
			JobDataMap jobDataMap) throws Exception {
		HashMap hmParam = (HashMap) objMessageObject;
		GmDataStoreVO gmDataStoreVO = null;

	    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
	    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmCycleCountLockTransBean gmCycleCountLockTransBean = new GmCycleCountLockTransBean(gmDataStoreVO);

		gmCycleCountLockTransBean.processLotUpdate(hmParam);

	}
}
