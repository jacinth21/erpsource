package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmInsertConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
	  processInsert(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
	  processInsert(objMessageObject, null);
  }

  public void processInsert(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
    log.debug("inside GmInsertConsumerJob..");
    HashMap hmParam = (HashMap) objMessageObject;   
    String strOrderId = GmCommonClass.parseNull((String)hmParam.get("REFID"));
    String strScreenType = GmCommonClass.parseNull((String)hmParam.get("SCREENTYP"));
    String strTransType = GmCommonClass.parseNull((String)hmParam.get("TRANSTYP"));
    String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
    String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    String strConsignSTR = GmCommonClass.parseNull((String) hmParam.get("TXNIDSTR"));
    GmDataStoreVO gmDataStoreVO = null;
    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(gmDataStoreVO);
    
    gmItemControlTxnBean.saveInsertTransaction(strOrderId,strTransType,strStatus,strUserId,strScreenType,strConsignSTR);

  }

}
