package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmShippingLabelPrinterConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
	  printLblJob(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
	  printLblJob(objMessageObject, null);
  }

  public void printLblJob(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
    log.debug("inside GmShippingLabelPrinterConsumerJob...");
    HashMap hmPrintParam = (HashMap) objMessageObject; 
    log.debug("GmShipping-LabelPrinterConsumerJob  hmPrintParam= " + hmPrintParam);
    GmPrintService gmPrintService = new GmPrintService();
	try{
		log.debug("GmShipping-LabelPrinterConsumerJob  Inside Try printByteData()...");
		gmPrintService.printByteData(hmPrintParam);
	}catch(Exception e){
		log.error("Exception on GmShippingLabelPrinterConsumerJob hmPrintParam :" + hmPrintParam );
		log.error("Exception 1 on GmShippingLabelPrinterConsumerJob:" + e );
		log.error("Exception 2 on GmShippingLabelPrinterConsumerJob:" + e.getMessage() );
	}

  }

}
