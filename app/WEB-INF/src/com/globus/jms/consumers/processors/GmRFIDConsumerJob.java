package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.operations.rfid.beans.GmRFIDTransBean;


public class GmRFIDConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
	  processRFID(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
	  processRFID(objMessageObject, null);
  }

  public void processRFID(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
    log.debug("inside GmRFIDConsumerJob..");
    HashMap hmParam = (HashMap) objMessageObject;  
    log.debug("hmParam.."+hmParam);
    String strRFIDBatchId = GmCommonClass.parseNull((String)hmParam.get("RFIDBATCHID"));
    String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    GmDataStoreVO gmDataStoreVO = null;
   // gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    GmRFIDTransBean gmRFIDTransBran = new GmRFIDTransBean();
    
    gmRFIDTransBran.saveRFIDTagDetails(strRFIDBatchId,strUserId);

  }

}
