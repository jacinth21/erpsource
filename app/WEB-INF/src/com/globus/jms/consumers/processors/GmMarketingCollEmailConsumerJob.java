package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;



import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.prodmgmnt.beans.GmShareEngineRptBean;
import com.globus.prodmgmnt.beans.GmShareEngineTransBean;
import com.globus.valueobject.common.GmEgnyteSendVO;


/**
 * @author mpatel
 * The below is the Algo for this Share Engine which is used for sharing files
 * 1.This class is called when the message arrives and well as using quartz job
 * 2.Fetch all the shares based on status if quartz job or one share if messaging is used
 * 3.For each share
 * 		1.Change the status of share to Processing In Progress
 * 		2.Fetch all the share details, email details and files details
 * 		3.For each file in share
 * 			1. create Egnyte link
 * 			2.If token error send email
 * 			3.If file missing send an email
 * 			4.Update the details of the links
 * 4.Compose Jasper mail
 * 5.Send Email
 * 6.Update the status of the email
 * 7.Update status of share  to processed if all the links are found else Partially processed.
 * 8.For all the shares in Partially processed state retry using quartz job until one of the link in this share is tried 3 times
 * 9.Send email to spineIT if link could not be create after trying more than three times.
 * 		
 * 
 *
 */
public  class GmMarketingCollEmailConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void execute(JobDataMap jobDataMap ) throws Exception {

		//For quartz Job to process share that are partially processed
		GmShareEngineTransBean gmShareEngineTransBean = new GmShareEngineTransBean();
		//Added Queued status also
		String strShareStatus = "103226,103228";//103226,103228-Partially Processed 103226-Queued

		ArrayList alShareIds = new ArrayList();
		String strShareIDEach ="";

		HashMap hmInParam = new HashMap();
		hmInParam.put("SHARESTATUS",strShareStatus);
		

		GmShareEngineRptBean gmShareEngineRptBean = new GmShareEngineRptBean();
		alShareIds = gmShareEngineRptBean.fetchShareIdsEmail(hmInParam);
		int inShareSize = alShareIds.size();
		log.debug("alShareIds:::"+alShareIds);
		log.debug("inShareSize:::"+inShareSize);
		
		for (int intShareId = 0;intShareId <inShareSize  ;intShareId++ )
		{
			HashMap hmParamShareIds = new HashMap();
			hmParamShareIds = (HashMap)alShareIds.get(intShareId);  
			strShareIDEach = GmCommonClass.parseNull((String)hmParamShareIds.get("SHAREID"));
			int strLinkRetryCnt=Integer.parseInt(GmCommonClass.parseNull((String)hmParamShareIds.get("LINKRETRYCOUNT")));
			if(strLinkRetryCnt >0){
				
				hmInParam.put("SHARESTATUS","");
				hmInParam.put("SHAREID",strShareIDEach);
				hmInParam.put("FIRSTNM","MARKTCOLL");
				HashMap hmShDetParam = GmCommonClass.parseNullHashMap(gmShareEngineRptBean.fetchSharedStatusDtls(hmInParam));
				log.debug("JOB hmShDetParam:::"+hmShDetParam);
				gmShareEngineTransBean.sendJiraEmail(hmShDetParam);
				
				HashMap hmProcessStatusParam = new HashMap();
				hmProcessStatusParam.put("SHARESTATUS", "103229");
				hmProcessStatusParam.put("SHAREID", strShareIDEach);
				gmShareEngineTransBean.saveShareDtls(hmProcessStatusParam);
				//break;
			}else{
				processBatchCommon(strShareIDEach,jobDataMap);
			}
			
			
		}
		 
	}

	public void processMessage(Object objMessageObject) throws Exception   {
		processBatchCommon(objMessageObject,null);
	}
	public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

		String strShareID = (objMessageObject == null) ? "" : (String)objMessageObject;
		String strShareStatus = "";
		log.debug("Received Share:"+strShareID);


		HashMap hmParamShareIds = new HashMap();
		GmShareEngineRptBean gmShareEngineRptBean = new GmShareEngineRptBean();
		GmShareEngineTransBean gmShareEngineTransBean = new GmShareEngineTransBean();

		//Link Exp date
		int intDaysToExpire =  Integer.parseInt(GmCommonClass.getRuleValue("ADDDAYSTOLINKEXPIRY", "MARKETINGCOLL"));
		Date dtCurrDate = GmCommonClass.getCurrentDate("yyyy-MM-dd");
		String strExpDate= GmCommonClass.getAddDaysToCurrentDateWithFormat(intDaysToExpire,dtCurrDate,"yyyy-MM-dd");
		log.debug("GmMarketingCollEmailConsumerJob: strExpDate:"+strExpDate);

		HashMap hmInParam = new HashMap();
		hmInParam.put("SHAREID",strShareID);
		hmInParam.put("FIRSTNM","MARKTCOLL");//marketing coll

		//Change status of share to 103227-Processing In Progress
		HashMap hmPipStatusParam = new HashMap();
		hmPipStatusParam.put("SHARESTATUS", "103227");
		hmPipStatusParam.put("SHAREID", strShareID);
		gmShareEngineTransBean.saveShareDtls(hmPipStatusParam);

		HashMap hmShDetParam = new HashMap();
		hmShDetParam = GmCommonClass.parseNullHashMap(gmShareEngineRptBean.fetchSharedStatusDtls(hmInParam));
		hmShDetParam.put("EXPDATE", strExpDate);//Add Link Expiry date
		hmShDetParam.put("SHARESTATUS", strShareStatus);//103228--Partially Processed retry logic
		
		// PC-3000: integrate email to sendgrid api
		hmShDetParam.put("SHAREID",strShareID);
		//110680	Marketing Email
		hmShDetParam.put("EMAIL_TRACKING_TYPE","110680");
		// based on the flag to call the sendGrid api
		hmShDetParam.put("EMAIL_TRACKING_FL","Y"); 
		
		log.debug("hmShDetParam:"+hmShDetParam);

		//Method to loop all the file details  and create a egnyte link
		HashMap hmEmailMsg = fetchEgnyteResource(hmShDetParam);
		String  strInvalidEgnyteToken = GmCommonClass.parseNull((String)hmEmailMsg.get("INVALIDEGNYTETOKEN"));
		log.debug("strInvalidEgnyteToken Process:"+strInvalidEgnyteToken);
		if (strInvalidEgnyteToken.equals("INVALIDEGNYTETOKEN")){
			hmShDetParam.put("SUBJECT", "");
			gmShareEngineTransBean.sendJiraTokenAndFileEmail(hmShDetParam);
		}
		
		ArrayList alEmailMsg = (ArrayList)hmEmailMsg.get("ALEMAILPARAM");
		String  strEgnyteShareStatus = (String)hmEmailMsg.get("STRCKSHARESTATUS");
		//hmShDetParam.put("EMAILMSG", alEmailMsg);//Add the hashmap for email
		log.debug("alEmailMsg:"+alEmailMsg);

		//Change status of share to 103228-Partially Processed
		// if link is not available 103243; -Link not Available
		if(strEgnyteShareStatus.equals("103243")){
			HashMap hmPartProcessStatusParam = new HashMap();
			hmPartProcessStatusParam.put("SHARESTATUS", "103228");
			hmPartProcessStatusParam.put("SHAREID", strShareID);
			gmShareEngineTransBean.saveShareDtls(hmPartProcessStatusParam);
		}

		//If token is valid then send this email.
		if (!strInvalidEgnyteToken.equals("INVALIDEGNYTETOKEN")){
			
			//spilt link available and not available
			HashMap hmSplitreportParam = gmShareEngineTransBean.splitMarketingCollEmailParam (alEmailMsg);
			ArrayList alLinkFound = (ArrayList)	hmSplitreportParam.get("ALLINKFOUND");
			ArrayList alLinkNotFound = (ArrayList)	hmSplitreportParam.get("ALLINKNOTFOUND");
			
			//Links are found then only send an email
			if (alLinkFound.size()>0){
				
				hmShDetParam.put("ALLINKFOUND", alLinkFound);
				hmShDetParam.put("ALLINKNOTFOUND", alLinkNotFound);
				//Send marketing coll. email
				String strInputstrStatus = gmShareEngineTransBean.sendMarketingCollEmail(hmShDetParam);
				log.debug("sendMarketingCollEmail:"+strInputstrStatus);
				
				//update the status of the email to  103234 sent
				gmShareEngineTransBean.saveSharedEmailStaus(strInputstrStatus,"103234");
			}
						
		}
		

		//Change status of share to 103229-Processed but if any of the link is not created by egnyte then the share status should not change and should remain partially processed.
		if(!strEgnyteShareStatus.equals("103243")){
			HashMap hmProcessStatusParam = new HashMap();
			hmProcessStatusParam.put("SHARESTATUS", "103229");
			hmProcessStatusParam.put("SHAREID", strShareID);
			gmShareEngineTransBean.saveShareDtls(hmProcessStatusParam);
		}

	}


	/** 
	 * fetchEgnyteResource is used to loop all the files details and for each file create egnyte link
	 * @param hmShDetParam
	 * @return HashMap
	 * @throws Exception
	 */
	private HashMap fetchEgnyteResource(HashMap hmShDetParam) throws Exception {


		//Egnyte VO
		GmEgnyteSendVO gmEgnyteSendVO = new GmEgnyteSendVO();
		//Prepare Egnyte param
		HashMap hmEgnyteSendParam = new HashMap();
		HashMap hmEgnyteReceiveParam = new HashMap();

		//Prepare ArrayList form email
		ArrayList alEmailParam = new ArrayList();
		HashMap hmOutParam = new HashMap();

		//Fetch share files details declaration
		HashMap hmFileParam = new HashMap();
		String strGrpNm = "";
		String strRefIdFile = "";
		String strTypeId = ""; //
		String strRefTpId = "";
		String strRefTpNm = "";
		String strFileNm = "";
		String strShareFileId = "";
		String strFileTitle = "";
		String strFileExt = "";
		String strShareId ="";
		int intLinkRetryCnt ; 

		//For Quartz declaration ...If link not found 
		String strLinkId = "";
		String strShareFileStatus = "";


		GmShareEngineRptBean gmShareEngineRptBean = new GmShareEngineRptBean();
		GmShareEngineTransBean gmShareEngineTransBean = new GmShareEngineTransBean();

		String strProdCatFilePath = GmCommonClass.getString("PRODCATALOGIMAGEURL");
		String strEgnyteBaseDir = GmCommonClass.getString("EGNYTEMARKETINGCOLLBASEDIR");

		ArrayList alShareFileDtls = new ArrayList();
		alShareFileDtls = (ArrayList)hmShDetParam.get("SHAREFILEDTLS");
		String strExpDate =(String)hmShDetParam.get("EXPDATE");
		String strShareStatus =(String)hmShDetParam.get("SHARESTATUS");//103228--Partially Processed retry logic
		String strCkShareStatus = "103242";//103242 -Link Shared

		//Loop the details of all the files to create a egnyte link
		for (int intFile = 0;intFile <alShareFileDtls.size() ;intFile++ )
		{
			//Preprare Hashmap for Email
			HashMap hmEmailParam = new HashMap();

			hmFileParam = (HashMap)alShareFileDtls.get(intFile);  
			strGrpNm = GmCommonClass.parseNull((String)hmFileParam.get("REFGRPNM"));
			strRefIdFile = GmCommonClass.parseNull((String)hmFileParam.get("REFIDFILE"));
			strRefTpId = GmCommonClass.parseNull((String)hmFileParam.get("REFTPID"));
			strRefTpNm = GmCommonClass.parseNull((String)hmFileParam.get("REFTPNM"));
			strTypeId = GmCommonClass.parseNull((String)hmFileParam.get("TYPEID")); //
			strFileNm = GmCommonClass.parseNull((String)hmFileParam.get("FILENM"));
			strShareFileId = GmCommonClass.parseNull((String)hmFileParam.get("SHAREFILEID"));
			strFileTitle = GmCommonClass.parseNull((String)hmFileParam.get("FILETITLE"));
			strFileExt = GmCommonClass.parseNull((String)hmFileParam.get("FILEEXTENSION"));
			strShareId = GmCommonClass.parseNull((String)hmFileParam.get("SHAREID"));
			intLinkRetryCnt = Integer.parseInt(GmCommonClass.parseZero((String)hmFileParam.get("LINKRETRYCNT")));

			////For Quartz declaration ...If link not found  ..Check if Link is created or status is 103243 i.e Link not Available
			strLinkId = GmCommonClass.parseNull((String)hmFileParam.get("LINKID"));
			strShareFileStatus = GmCommonClass.parseNull((String)hmFileParam.get("SHAREFILESTATUS"));

			// If condition for quartz ... create a link only if the link is not available and status is 103243 i.e Link not Available
			if ((strLinkId.equals("")  && (strShareFileStatus.equals("103243"))  ||  strShareFileStatus.equals("")) ){


				String strEgnyteResPath = strEgnyteBaseDir+strProdCatFilePath+strGrpNm+"/"+strRefIdFile+"/"+strRefTpId+"/"+strTypeId+"/"+strFileNm;

				hmEgnyteSendParam.put("EXPDATE", strExpDate);
				hmEgnyteSendParam.put("RESPATH", strEgnyteResPath);
				log.debug("hmEgnyteSendParam:"+hmEgnyteSendParam);


				//Create Egnyte Link and prepare to save link to DB
				hmEgnyteReceiveParam = gmShareEngineTransBean.createEgnyteResourceLink(hmEgnyteSendParam);
				
				//Break the loop since token was invalid and could not connect to egnyte.
				String strInvalidToken = GmCommonClass.parseNull((String)hmEgnyteReceiveParam.get("INVALIDEGNYTETOKEN"));
				if (strInvalidToken.equals("INVALIDEGNYTETOKEN")){
					break;
				}
				hmEgnyteReceiveParam.put("SHAREFILEID", strShareFileId);
				hmEgnyteReceiveParam.put("SHAREID", strShareId);

				//IF link is blank then set the status of the link to not avaialble
				//Increment the retry count.
				String strLink = GmCommonClass.parseNull((String)hmEgnyteReceiveParam.get("LINK"));
				log.debug("strLink::::"+strLink);
				if (strLink.equals("")){
					intLinkRetryCnt = intLinkRetryCnt+1;
					strCkShareStatus = "103243";//103243 i.e Link not Available
				}
				hmEgnyteReceiveParam.put("LINKRETRYCNT", Integer.toString(intLinkRetryCnt));
				
				
				//example strLink ="https://www.google.com";
				//Preprare Hashmap for Email
				hmEmailParam.put("FILETITLE", strFileTitle);
				hmEmailParam.put("FILEEXTENSION", strFileExt);
				hmEmailParam.put("ARTIFACTNM", strRefTpNm);
				hmEmailParam.put("LINK",strLink);//Put strLink
				hmEmailParam.put("HYPERLINK",strFileTitle);
				hmEmailParam.put("LINKEXPDATE",hmEgnyteReceiveParam.get("LINKEXPDATE"));
				hmEmailParam.put("LINKSTATUS",hmEgnyteReceiveParam.get("LINKSTATUS"));

				//Add each hashmap of link and its details to arraylist, for email purpose.
				alEmailParam.add(hmEmailParam);

				//Save the details of the link
				gmShareEngineTransBean.saveSharedFileDtls(hmEgnyteReceiveParam);

			}// if condition

		}// End For loop

		hmOutParam.put("STRCKSHARESTATUS", strCkShareStatus);
		hmOutParam.put("ALEMAILPARAM", alEmailParam);
		log.debug("hmOutParam::::"+hmOutParam);
		return hmOutParam;
	}
}
