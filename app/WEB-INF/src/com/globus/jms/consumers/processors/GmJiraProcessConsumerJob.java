package com.globus.jms.consumers.processors;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.logistics.beans.GmTicketBean;

public class GmJiraProcessConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	// instantiate(request, response);
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {
		processIssue(null, jobDataMap);

	}
	
	@Override
	public void processMessage(Object objMessageObject) throws Exception {
		processIssue(objMessageObject, null);
	}
	
	/**
	 * Method - processIssue used once when NCMR created or closed
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */

	public void processIssue(Object objMessageObject,
			JobDataMap jobDataMap) throws Exception {

		HashMap hmParam = (HashMap) objMessageObject;
		GmTicketBean gmTicketBean = new GmTicketBean();

		String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strIssueType = GmCommonClass.parseNull((String) hmParam.get("ISSUETYPE"));
		log.debug("strIssueType----->" + strIssueType);
		
		if(strIssueType.equals("CREATE")) {  // To Create Jira Ticket
			gmTicketBean.createNCMREvalTicket(hmParam);	
		}
		else {
			gmTicketBean.closeNCMREvalTicket(hmParam);	 // To Close Jira Ticket
		}

}

}
