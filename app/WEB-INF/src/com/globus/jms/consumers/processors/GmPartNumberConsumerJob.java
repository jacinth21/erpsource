package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.prodmgmnt.beans.GmPartNumberServiceBean;

/**
 * This Consumer Class will be used to receive Message that needs to be processed for the part
 * Related Functionalities. We are using this class to sync part,Group,System and project for all
 * the Companies.
 * 
 * @throws Exception
 */

public class GmPartNumberConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    log.debug("entering processexecute ");
    syncPartCompanyMapping(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
    log.debug("entering processMessage ");
    syncPartCompanyMapping(objMessageObject, null);
  }

  /**
   * syncPartCompanyMapping - The below method is calling from processPartVisibility method.This
   * method is used to sync part,System,Group and project in part number setup screen and Release
   * for sale screen.
   * 
   * @param -objMessageObject:it contains CONSUMERCLASS,strUserId
   * @exception - Exception
   */

  public void syncPartCompanyMapping(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
    log.debug("entering processMessage --> syncPartCompanyMapping");
    GmPartNumberServiceBean gmPartNumberServiceBean = new GmPartNumberServiceBean();
    HashMap hmParam = (HashMap) objMessageObject;

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("STRUSERID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    // This method is used to sync part,Sytem,Group and project in part number setup and Release for
    // sale screens
    gmPartNumberServiceBean.syncPartCompanyMapping(strUserId);
    log.debug("ending processMessage --> syncPartCompanyMapping");
    gmPartNumberServiceBean.syncProjectPartMapping(strPartNum, strUserId);
  }
}
