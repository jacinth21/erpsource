package com.globus.jms.consumers.processors;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.standard.MediaName;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmFileReader;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.loaners.beans.GmReconfigLoanerBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerPrintConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

	 Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to

	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
	  
	      processLoanerPrint(null, jobDataMap);
	   
	  }

	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {

		  processLoanerPrint(objMessageObject, null);
	  }

	  public void processLoanerPrint(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

		  log.debug("Inside ReconfigureLoaner Consumer Job ");
	    HashMap hmMessageObj = (HashMap) objMessageObject;
	    HashMap hmCONSIGNMENT= new HashMap();
	    HashMap hmLoop = new HashMap();
	    String strPDFFileName="";
	    
	    String strCompanyInfo = GmCommonClass.parseNull((String) hmMessageObj.get("COMPANYINFO"));
	    String strPrintOpt = GmCommonClass.parseNull((String) hmMessageObj.get("PRINTOPT"));
	    log.debug("Inside ReconfigureLoaner Consumer Job hmMessageObj "+hmMessageObj);
	   
	    GmDataStoreVO tmpGmDataStoreVO = null;
	    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
	    GmReconfigLoanerBean gmReconfigLoanerBean = new GmReconfigLoanerBean(tmpGmDataStoreVO);
	    
	   String strConsignId = GmCommonClass.parseNull((String) hmMessageObj.get("CONSID"));
	   String strSetId = GmCommonClass.parseNull((String) hmMessageObj.get("SETID"));
	   String strCompanyId = GmCommonClass.parseNull((String) hmMessageObj.get("COMPANYID"));
	   String strUserId = GmCommonClass.parseNull((String) hmMessageObj.get("USERID"));
	   
	   log.debug("strCompanyId=="+strCompanyId);
	   log.debug("strSetId=="+strSetId);
	   log.debug("strUserId=="+strUserId);
	    String strLoanerSetPDFLocation = GmCommonClass.getString("RECONFIG_LOANER");
	    String strPrinterName = gmReconfigLoanerBean.getPrinterName(strUserId);
	    if(strPrintOpt.equals("PrintPPW")) { 
	    	strPDFFileName = strLoanerSetPDFLocation+ "\\PPW" + strConsignId + ".pdf"; 
	    	}else if(strPrintOpt.equals("ViewPrint")){
	    		strPDFFileName = strLoanerSetPDFLocation + "\\Print" + strConsignId + ".pdf"; 
	    	}else if(strPrintOpt.equals("InsViewer")){
	    		
	    		log.debug("strPrintOpt=="+strPrintOpt);
	            String fileStatus = "Y";
	            String strLoanerSet = GmCommonClass.getString("GMLNINSHEET");
	            String strFileName = "TXT";
	            HashMap hmMap = new HashMap();
	            GmFileReader fileReader = new GmFileReader();
	            log.debug("strLoanerSet=="+strLoanerSet);

	    	    String strCompanyLocale = "";
	    	    strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
	    	    // Locale
	    	    GmResourceBundleBean gmResourceBundleBean =
	    	        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	    	    GmResourceBundleBean gmResourceBundleBeanComp =
	    	        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	            String strCompLNINMainSubDir = "";
	            String strCompLNINMainDir =
	                GmCommonClass.parseNull(gmResourceBundleBeanComp.getProperty("GMLNINSHEET"));
	            String strCompLNINSubDir =
	                GmCommonClass.parseNull(gmResourceBundleBeanComp.getProperty("GMLNINSHEET_SUB_DIR"));
	            
	            
	            if ((!strCompLNINMainDir.equals("")) && (!strCompLNINSubDir.equals(""))) {
	                strCompLNINMainSubDir = strCompLNINMainDir + strCompLNINSubDir;
	              }

	            
	            GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(tmpGmDataStoreVO);
	            ArrayList alList = gmCommonUploadBean.fetchUploadInfo("91184", strSetId);
				    log.debug("strSetId=="+strSetId);
				    log.debug("alList=="+alList);
	            if (alList.size() > 0) {
	              hmMap = (HashMap) alList.get(0);
	              strFileName = (String) hmMap.get("NAME");
	            }
	            log.debug("strFileName=="+strFileName);
	            if (!strFileName.equals("TXT")) {
	              // below code is added for Print Loaner Inspection sheet in company language
	              // Get file from company loaner Inspection sheet in sub directory by company properties
	              // if not file present in sub directory it will get from main directory
	              File fldisp = new File("");
	              fldisp = fileReader.findFile(strCompLNINMainSubDir, strFileName);
	              if (fldisp == null || !fldisp.exists()) {
	                fldisp = fileReader.findFile(strCompLNINMainDir, strFileName);
	                if (fldisp == null || !fldisp.exists()) {
	                  fldisp = fileReader.findFile(strLoanerSet, strFileName);
	                  if (fldisp == null || !fldisp.exists()) {
	                    fileStatus = "N";
	                  }
	                }
	              }
	            } else {
	              fileStatus = "N";
	            }
	            log.debug("file status" + fileStatus);
	            if(!fileStatus.equals("N")) {
	            	 strPDFFileName = strLoanerSet + "\\"+strFileName +".pdf";//PC-5143 .PDF added
	            }
	           

	           
	    	}
	    if(!strPDFFileName.equals("")) {
	        print(strPDFFileName,strCompanyId,strPrinterName);
	    }
	  }
	  
	  /**
	   * print invoice pdf file
	   * 
	   * @author
	 * @param strCompanyId 
	 * @param strPDFFileName 
	   * @param HashMap
	   * @return String
	   * @throws Exception
	   */
	  public void print(String strPDFFileName, String strCompanyId,String strPrinterName) throws Exception{

	    GmDataStoreVO tmpGmDataStoreVO = null;
	    String printServiceName = "";
	  log.debug("strPDFFileName=="+strPDFFileName);
	    PrintService service = PrintServiceLookup.lookupDefaultPrintService();
	    log.debug("service=="+service);
        if (service != null) {
             printServiceName = service.getName();
             log.debug("Print Service Name = " + printServiceName);
        } else {
        	log.debug("No default print service found.");
        }
        log.debug("printServiceName=="+printServiceName);
      
	    GmPrintService gmPrintService = new GmPrintService();
	    gmPrintService.setNumofCopies(1);

		/*
		 * String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
		 * GmResourceBundleBean gmResourceBundleBean =
		 * GmCommonClass.getResourceBundleBean("properties.Paperwork",
		 * strCompanyLocale); String strA4PaperSizeFl =
		 * GmCommonClass.parseNull(gmResourceBundleBean.getProperty(
		 * "INVOICE.PAPER_SIZE_A4"));
		 */
	    log.debug("strPrinterName=="+strPrinterName);
	    gmPrintService.setPrinterName(strPrinterName);
	    gmPrintService.setFileName(strPDFFileName);
	    gmPrintService.setPaperSize(MediaName.ISO_A4_WHITE);
	    gmPrintService.setFontName("Verdana");
	    gmPrintService.setFontSize(10);
	    gmPrintService.setImgArea_x_offset(-9);
	    gmPrintService.setImgArea_y_offset(-3);
	    // PMT-19992: OUS print - bottom space issue fix.
	    // A4 paper size to reset the Y offset values.
	   /* if (strA4PaperSizeFl.equals("Y")) {
	      gmPrintService.setImgArea_y_offset(10);
	      gmPrintService.setImgArea_width_offset(-30);
	      gmPrintService.setImgArea_height_offset(-32);
	      // to set the A4 paper size
	      // Width 595 pixels x height 842 pixels
	      gmPrintService.setPaperSizeHeight(842);
	      gmPrintService.setPaperSizeWidth(595);
	    } else {*/
	      gmPrintService.setImgArea_width_offset(-10);
	      gmPrintService.setImgArea_height_offset(-12);
	   // }
	    gmPrintService.setGraphics2D_x(20);
	    gmPrintService.setGraphics2D_y(10);
	    gmPrintService.print();
	  }
}
