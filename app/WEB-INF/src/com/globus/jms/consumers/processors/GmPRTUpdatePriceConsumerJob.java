package com.globus.jms.consumers.processors;

/**
 * GmPRTUpdatePriceConsumerJob - JMS interface to updated current prices in
 * T7540_account_group_pricing table
 * 
 * @author Matt Balraj (PRT-288)
 */
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.sales.pricing.beans.GmPricingBatchBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * The Class GmPRTUpdatePriceConsumerJob. (PRT-288)
 * 
 * @author Matt Balraj
 */
public class GmPRTUpdatePriceConsumerJob extends GmActionJob implements
    GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");


  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    if (strProcessType.equals("QUARTZJOB")) {
      processBatchCommon(null, jobDataMap);
    }
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {

    processBatchCommon(objMessageObject, null);
  }

  /* Method that is called by processMessage to call package and update batch status */
  public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
    log.debug("<========Entering processBatchCommon====>");
    HashMap hmMessageObj = (HashMap) objMessageObject;
    String strBatchID = GmCommonClass.parseNull((String) hmMessageObj.get("BATCHID"));
    String strBatchType = GmCommonClass.parseNull((String) hmMessageObj.get("BATCHTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmMessageObj.get("USERID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmMessageObj.get("COMPANYINFO"));
    GmDataStoreVO tmpGmDataStoreVO = null;
    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);

    log.debug(" JMS strBatchID====>" + strBatchID);
    // GmBatchBean gmBatchBean = new GmBatchBean();
    GmPricingBatchBean gmPricingBatchBean = new GmPricingBatchBean(tmpGmDataStoreVO);

    // Call bean method
    hmMessageObj.put("BATCHID", strBatchID);
    hmMessageObj.put("BATCHTYPE", strBatchType);
    hmMessageObj.put("BATCHSTATUS", "18742"); // Process in progress
    hmMessageObj.put("USERID", strUserId); // Process in progress
    gmPricingBatchBean.savePricingBatchUpdate(hmMessageObj);
  }


}
