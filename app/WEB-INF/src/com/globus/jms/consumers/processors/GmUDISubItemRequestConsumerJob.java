package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.prodmgmnt.udi.beans.GmUDISubmissionTransBean;

/**
 * The Class GmUDISubmissionConsumerJob.
 * 
 * @author mpatel
 */
public class GmUDISubItemRequestConsumerJob extends GmActionJob implements
    GmConsumerDispatcherInterface {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code.

  // String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");
  String strProcessType = "QUARTZJOB";

  /*
   * (non-Javadoc)
   * 
   * @see com.globus.common.util.jobs.GmActionJob#execute(org.quartz.JobDataMap)
   */
  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    if (strProcessType.equals("QUARTZJOB")) {
      processBatchCommon(null, jobDataMap);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.
   * Object)
   */
  public void processMessage(Object objMessageObject) throws Exception {

    processBatchCommon(objMessageObject, null);
  }

  /**
   * Process batch common.
   * 
   * @param objMessageObject the obj message object
   * @param jobDataMap the job data map
   * @throws Exception the exception
   */
  public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
    HashMap hmXmlGenParam = new HashMap();
    hmXmlGenParam.put("XMLTYPE", "ITEMXML");
    hmXmlGenParam.put("strInputstr", "");
    hmXmlGenParam.put("strIntStatus", "4000806");
    hmXmlGenParam.put("strExtStatus", "");
    hmXmlGenParam.put("strUserId", "30301");

    GmUDISubmissionTransBean gmUDISubmissionTransBean = new GmUDISubmissionTransBean();

    // Create Item Xml
    gmUDISubmissionTransBean.UDISubItemRequest(hmXmlGenParam);

  }

}
