package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmLogger;
import com.globus.common.email.GmEmailBean;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;

/**
 * @author Ramachandiran T.S
 * Class Name: GmEmailSMTPConsumerJob.java
 * Description:This class is used to process the trade show email with the jobDataMap
 */
public class GmEmailSMTPConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processEmail(null, jobDataMap);

	  }

	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processEmail(objMessageObject, null);
	  }
	  /*
	   * processEmail(): This method will used to call the methods initialized in the GmEmailManagerInterface class
	   * */
	  public void processEmail(Object objMessageObject, JobDataMap jobDataMap)
	      throws Exception {
		  	HashMap hmParam = (HashMap) objMessageObject; 
	  		GmEmailBean.gmEmailSMTPManager.setUpEmail(hmParam);
	  		GmEmailBean.gmEmailSMTPManager.sendEmail();
	  		GmEmailBean.gmEmailSMTPManager.saveMailInDB();
		  	
	  }

}
