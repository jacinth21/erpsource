package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.jms.consumers.beans.GmOrderBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.logistics.beans.GmChargesBean;
import com.globus.operations.logistics.beans.GmLoanerDisputedBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerLateFeeEmailConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{

    // TODO Auto-generated constructor stub
    Logger log = GmLogger.getInstance(this.getClass().getName());

    @Override
    public void execute(JobDataMap jobDataMap) throws Exception {
    	processLoanerLateFeeEmail (null, jobDataMap);
    }

    @Override
    public void processMessage(Object objMessageObject) throws Exception {
    	processLoanerLateFeeEmail(objMessageObject, null);
    }

    /**
     * processLoanerDisputedEmail - to send Disputed Loaner Mail 
     * by calling the method- sendLoanerDisputedMailProcess
     * 
     * @param -objMessageObject:it contains CONSUMERCLASS
     * @exception - Exception
     */

    public void processLoanerLateFeeEmail(Object objMessageObject, JobDataMap jobDataMap)
        throws Exception {
    	
      HashMap hmParam = (HashMap) objMessageObject;
      HashMap hmValParam = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMVALUES"));
      String strCompanyInfo = GmCommonClass.parseNull((String) hmValParam.get("COMPANYINFO"));
      // data store VO null then populate the Company Info
      GmDataStoreVO gmDataStoreVO = new GmWSUtil().populateCompanyInfo(strCompanyInfo);
      GmChargesBean gmChargesBean = new GmChargesBean(gmDataStoreVO);

      //send Loaners late fee Mail
      gmChargesBean.processLoanerLateFeeEmail(hmValParam);      

    }
  }


