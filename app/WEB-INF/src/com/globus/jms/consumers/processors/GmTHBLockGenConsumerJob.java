package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.beans.GmLotBulkExtBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.receiving.thbreceiving.beans.GmTHBPendAnalysisDashBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ssharmila
 * 
 */
public class GmTHBLockGenConsumerJob extends GmActionJob implements
		GmConsumerDispatcherInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmTHBPendAnalysisDashBean gmTHBPendAnalysisDashBean = new GmTHBPendAnalysisDashBean(
				gmDataStoreVO);
		processTHBLock(objMessageObject, null);
	}

	public void execute(JobDataMap jobDataMap) throws Exception {
		processTHBLock(null, jobDataMap);
	}

	public void processTHBLock(Object objMessageObject, JobDataMap jobDataMap)
			throws Exception {
		HashMap hmParam = (HashMap) objMessageObject;
	
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam
				.get("COMPANYINFO"));
		gmDataStoreVO = (gmDataStoreVO != null) ? gmDataStoreVO
				: (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmTHBPendAnalysisDashBean gmTHBPendAnalysisDashBean = new GmTHBPendAnalysisDashBean(
				gmDataStoreVO);
		// Call processTHBLock method when JMS CALL
		log.debug("processTHBLock-GmTHBLockGenConsumerJob"+hmParam);
		if (hmParam != null) {
			gmTHBPendAnalysisDashBean.saveLockAndGenerate(hmParam);
		}

	}

}
