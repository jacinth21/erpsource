package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmShippingRegularPrinterConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
	  printJob(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
	  printJob(objMessageObject, null);
  }

  public void printJob(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
    log.debug("inside GmShippingRegularPrinterConsumerJob..");
    HashMap hmPrintParam = (HashMap) objMessageObject;   
    log.debug("GmShipping-RegularPrinterConsumerJob  hmPrintParam= " + hmPrintParam);
    GmBatchBean gmBatchBean = new GmBatchBean();
	try{
		log.debug("GmShipping-RegularPrinterConsumerJob  Inside Try batchPrint()...");
		gmBatchBean.batchPrint(hmPrintParam);
	}catch(Exception e){
		log.error("Exception on GmShippingRegularPrinterConsumerJob hmPrintParam :" + hmPrintParam );
		log.error("Exception 1 on GmShippingRegularPrinterConsumerJob:" + e );
		log.error("Exception 2 on GmShippingRegularPrinterConsumerJob:" + e.getMessage() );
	}

  }

}
