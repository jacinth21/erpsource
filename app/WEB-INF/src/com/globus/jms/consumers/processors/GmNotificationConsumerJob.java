package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.beans.GmNotificationInterface;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;


public class GmNotificationConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    processNotification(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
    processNotification(objMessageObject, null);
  }

  public void processNotification(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
    log.debug("inside GmNotificationConsumerJob..");
    HashMap hmParam = (HashMap) objMessageObject;    
    String strNotification = GmCommonClass.parseNull((String)hmParam.get("NOTIFICATION"));
    GmNotificationInterface gmNotificationInterface =
            (GmNotificationInterface) GmCommonClass.getSpringBeanClass(
                "xml/Azure_Notification_Beans.xml", strNotification);
    gmNotificationInterface.processNotificationRequest(objMessageObject, jobDataMap);
  }

}
