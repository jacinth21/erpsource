package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisTransBean;

/**
 * This Consumer Class will be used to sync the master details.
 * 
 * @throws Exception
 */

public class GmRedisConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    syncMasterData(null, jobDataMap);
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
    syncMasterData(objMessageObject, null);
  }

  /**
   * syncMasterData - The below method is used to save the master details in Redis server.
   * 
   * @param -objMessageObject:it contains CONSUMERCLASS,strUserId
   * @exception - Exception
   */

  public void syncMasterData(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
    HashMap hmParam = (HashMap) objMessageObject;
    // get the Data store information
    GmDataStoreVO gmDataStoreVO = null;
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo"));
    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    GmAutoCompleteTransBean gmAutoCompleteTransBean = new GmAutoCompleteTransBean(gmDataStoreVO);
    GmMasterRedisTransBean gmMasterRedisTransBean = new GmMasterRedisTransBean(gmDataStoreVO);
    String strMethod = GmCommonClass.parseNull((String) hmParam.get("METHOD"));
    if (strMethod.equals("FS")) {
      // Field Sales Sync
      gmAutoCompleteTransBean.syncFieldSalesMasterData(hmParam);
    } else if (strMethod.equals("SalesRep")) {
      // Sales Rep Setup Sync
      gmAutoCompleteTransBean.syncSalesRepMasterData(hmParam);
    } else if (strMethod.equals("RepAccount")) {
      gmAutoCompleteTransBean.syncRepAccMasterData(hmParam);
    } else if (strMethod.equals("PartyList")) {
      gmAutoCompleteTransBean.syncPartyMasterData(hmParam);
    } else if (strMethod.equals("SetBomSetup")) {
      // Set/BOM sync
      gmAutoCompleteTransBean.syncSetBomMasterData(hmParam);
    } else if (strMethod.equals("ProjectList")) {
      // Project list sync
      gmAutoCompleteTransBean.syncProjectMasterData(hmParam);
    } else if (strMethod.equals("InspectSheet")) {
      // Sync Inspection sheet
      gmAutoCompleteTransBean.syncInspectionSheetMasterData(hmParam);
    } else if (strMethod.equals("CodeLookUp")) {
      // to sync the code lookup
      gmAutoCompleteTransBean.syncCodeLookupMasterData(hmParam);
    } else if (strMethod.equals("DemandSheet")) {
      
      // to sync the demand sheet setup
      gmAutoCompleteTransBean.syncDemandSheetMasterData(hmParam);
    } else if (strMethod.equals("synTTPList")){ 
      //to sync the TTP Name List
      gmAutoCompleteTransBean.syncTTPMasterData(hmParam);   	
    } else if (strMethod.equals("SetIdList")){
        // To get the SetMap drop down list based on Type 
   	  gmAutoCompleteTransBean.syncSetIdMasterData(hmParam);   	
    } else if (strMethod.equals("NPIValid")){ 
 	   //to sync the Valid NPI List
      gmAutoCompleteTransBean.syncNPIDetails(hmParam);
    } else if (strMethod.equals("synGroupList")) { 
	  gmAutoCompleteTransBean.synGroupList(hmParam);
    } else if (strMethod.equals("synSetBundleList")) { 
	  gmAutoCompleteTransBean.synSetBundleList(hmParam);
    } else if (strMethod.equals("synSetBundleNameKeyList")) { 
	  gmAutoCompleteTransBean.synSetBundleNameKeyList(hmParam);
    } else if (strMethod.equals("synKitNameList")) { 
	  gmAutoCompleteTransBean.synKitNameList(hmParam);
    } else if (strMethod.equals("syncSetTagList")) { 
    	//to sync the set List for surgery kit
	  gmMasterRedisTransBean.syncSetTagList(hmParam);
    } else if (strMethod.equals("syncPartNumList")) { 
    	//to sync the part num List for surgery kit
	  gmMasterRedisTransBean.syncPartNumList(hmParam);
    } else if (strMethod.equals("syncKitNameList")) { 
    	//to sync the kit name List for case scheduler on Mobile app
	  gmMasterRedisTransBean.syncKitNameList(hmParam);
    } else if (strMethod.equals("syncTagList")) {
    	//to sync the tag List for surgery kit
	  gmMasterRedisTransBean.syncTagList(hmParam);
    } else if (strMethod.equals("LoanerSetList")) {
    	//to sync the Loaner Set List
    	gmAutoCompleteTransBean.syncLoanerSetDetails(hmParam);
    } 
  }
}

