package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.custservice.beans.GmAddAccountBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import org.quartz.JobDataMap;

/**
 * This method will pass information using JMS
 * @author Mkosalram
 *
 */

public class GmAddAccountConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

	/**
	 * processMessage - This method will pass information using JMS
	 * updateAccount:This method update the New Account
	 * @param Object Mewwage
	 * @throws Exception 
	 *
	 */
	// TODO Auto-generated constructor stub
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		log.debug("In processMessage method");
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmAddAccountBean gmAddAccountBean = new GmAddAccountBean(gmDataStoreVO);
		gmAddAccountBean.addAccountIML(objMessageObject, null);
	}

	/**
	 * execute - This method will pass information using JMS
	 * updateAccount:This method update the New Account
	 * @param JobDataMap
	 * @throws Exception
	 *
	 */
	public void execute(JobDataMap jobDataMap) throws Exception {
		GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		log.debug("In execute method");
		HashMap hmParams = new HashMap();
		HashMap hmMessageObj = new HashMap();

		GmAddAccountBean gmAddAccountBean = new GmAddAccountBean(gmDataStoreVO);
		hmParams.put("GMDATASTOREVO", gmDataStoreVO);
		log.debug("gmDataStoreVO" + gmDataStoreVO);
		hmMessageObj.put("HMACCVALUES", hmParams);
		log.debug("hmParams" + hmParams);

		gmAddAccountBean.addAccountIML(hmMessageObj, jobDataMap); // TODO Auto-generated method stub

	}
}
