package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.ttpbyvendor.beans.GmTTPByVendorSummaryBean;
import com.globus.operations.ttpbyvendor.beans.GmTTPByVendorTransBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmTTPByVendorLockConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
		  Logger log = GmLogger.getInstance(this.getClass().getName());
		  
		 
		/* (non-Javadoc)
		 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
		 */
		  @Override
		public void processMessage(Object objMessageObject) throws Exception {
			// TODO Auto-generated method stub
		    processTTPByVendorLock(objMessageObject, null);
		}

		 /* (non-Javadoc)
		 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
		 */
		  @Override
		public void execute(JobDataMap jobDataMap) throws Exception {
			processTTPByVendorLock(null, jobDataMap);      
		}
		
		  /**
		 * Method - processTTPByVendorLock used to update the TTP vendor Status.
		 *   
		 *    
		 * @param objMessageObject
		 * @param jobDataMap
		 * @throws Exception
		 */
		
		public void processTTPByVendorLock(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
			
                  log.debug("INSIDE TTP BY VENDOR LOCK CONSUMER JOB...");
		    
		    HashMap hmParam = (HashMap) objMessageObject; 
		    HashMap hmDetail = new HashMap(); 
		    //Initialize
			String strUserId = "";
			String strLockPeriod = "";
			String strStatus="";
			String strTTPIdInputStr ="";
			String strTTPId="";
			
			hmDetail = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMPARAM"));
			
			strUserId = GmCommonClass.parseZero((String) hmDetail.get("USERID"));
			strLockPeriod = GmCommonClass.parseNull((String) hmDetail.get("LOCKPERIOD"));
			strStatus =GmCommonClass.parseNull((String) hmParam.get("STATUS"));
			strTTPIdInputStr  =GmCommonClass.parseNull((String) hmDetail.get("TTPID")); 
			
			String[] strTTPIdArr = strTTPIdInputStr.split(",");
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmDetail.get("COMPANYINFO"));
		   
		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
	        ArrayList alReturn = new ArrayList();
	        
	        GmTTPByVendorSummaryBean gmTTPbyVendorSummaryBean = new GmTTPByVendorSummaryBean(gmDataStoreVO);
	        GmTTPByVendorTransBean gmTTPbyVendorTransBean = new GmTTPByVendorTransBean(gmDataStoreVO);
	      
	        for (int i = 0; i < strTTPIdArr.length; i++) {
	        
	           strTTPId = GmCommonClass.parseNull(strTTPIdArr[i]);
	           alReturn = gmTTPbyVendorSummaryBean.fetchTTPbyVendorPartDetails(strTTPId,strLockPeriod,strStatus);
	           gmTTPbyVendorTransBean.updateTTPbyVendorPartDtls(strTTPId,strLockPeriod,strStatus,strUserId);
	           
	           //Iterator the TTP details (alReturn)  and update the Vendor order Qty
	           Iterator alDetailsItr = alReturn.iterator();
	           while (alDetailsItr.hasNext()) {
	             HashMap hmReturn = (HashMap) alDetailsItr.next();
	        	 hmReturn.put("LOADDATE", strLockPeriod);
	        	 gmTTPbyVendorTransBean.updateTTPbyVendorOrdQty(hmReturn);
	           }
	        }
	        
		   //This method used to update the vendor Status As locked or Lock Failed
	      //  gmTTPbyVendorTransBean.updateTTPbyVendorStatus(hmDetail);
		  

}
			
}

