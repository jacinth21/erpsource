package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.prodmgmnt.beans.GmProjectTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * This Consumer Class will be used to sync the project details to company master details.
 * 
 * @throws Exception
 */

public class GmProjectCompanyMapConsumerJob extends GmActionJob implements
    GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    syncProjectCompanyMapping(null, jobDataMap);
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
    syncProjectCompanyMapping(objMessageObject, null);
  }

  /**
   * syncProjectCompanyMapping - The below method is used to save the project details to company
   * 
   * @param -objMessageObject contains CONSUMERCLASS,strUserId
   * @exception - AppError
   */

  public void syncProjectCompanyMapping(Object objMessageObject, JobDataMap jobDataMap)
      throws AppError {
    HashMap hmParam = (HashMap) objMessageObject;
    // get the Data store information
    GmDataStoreVO gmDataStoreVO = null;
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));

    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    GmProjectTransBean gmProjectTransBean = new GmProjectTransBean(gmDataStoreVO);
    gmProjectTransBean.saveProjectReleaseToCompany(hmParam);
  }
}
