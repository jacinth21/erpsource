package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.sales.kit.bean.GmKitMappingReportBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingWrapperBean;

public class GmKitSetShipConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{

	 Logger log = GmLogger.getInstance(this.getClass().getName());

	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processInsert(null, jobDataMap);

	  }

	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processInsert(objMessageObject, null);
	  }

	  public void processInsert(Object objMessageObject, JobDataMap jobDataMap) throws AppError{
		    HashMap hmParam = (HashMap) objMessageObject;
		    GmDataStoreVO gmDataStoreVO = null;
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANY_INFO"));
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    log.debug("GmKitSetShipConsumerJob strCompanyInfo========== "+strCompanyInfo);
		    String strRefId = GmCommonClass.parseNull((String) hmParam.get("TRANS_ID"));
			String strUserId = GmCommonClass.parseNull((String) hmParam.get("USER_ID"));
		    GmKitMappingWrapperBean gmKitMappingWrapperBean = new GmKitMappingWrapperBean(gmDataStoreVO);
		    GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmDataStoreVO);
	  	//Update set list to redis cache
		    HashMap hmreturn = GmCommonClass.parseNullHashMap((HashMap)gmKitMappingReportBean.fetchSetDetails(strRefId,strUserId)); 
		    String strDId = GmCommonClass.parseNull((String)hmreturn.get("DID"));
		    String strSetId = GmCommonClass.parseNull((String)hmreturn.get("ID"));  
		    String strSetName = GmCommonClass.parseNull((String)hmreturn.get("NAME"));
		    if(!hmreturn.equals("")){
		    	gmKitMappingWrapperBean.syncSetTagList(strSetId, strSetId, strSetName, "syncSetTagList", strCompanyInfo,strDId);  	
		    }
	  	  	
	  	//Update tag list to redis cache
		    HashMap hmReturns = gmKitMappingReportBean.fetchTagDetails(strRefId,strDId);
		    String strDistId = GmCommonClass.parseNull((String)hmreturn.get("DID"));
		    String strSetID = GmCommonClass.parseNull((String)hmreturn.get("ID"));
		    String strTagId = GmCommonClass.parseNull((String)hmreturn.get("TID"));
		    String strSetNm = GmCommonClass.parseNull((String)hmreturn.get("NAME"));
		    if(!strTagId.equals("")){
		    	gmKitMappingWrapperBean.syncTagList(strSetId, strSetId, strSetName, "syncTagList", strCompanyInfo,strDistId);  
		    }
	  }
	}