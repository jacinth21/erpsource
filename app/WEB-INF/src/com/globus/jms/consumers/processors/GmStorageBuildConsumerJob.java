/**
 * 
 */
package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.beans.GmSplitTransInterface;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author sswetha
 *
 */
public class GmStorageBuildConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processStorageBuild(null, jobDataMap);
}
	
	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processStorageBuild(objMessageObject, null);
	}
	  
	  
	  /**
	   * processStorageBuild - The below method will get the class to be called to perform validation and process transaction by reading the xml file for PMT-33510, PMT-33511, PMT-33513.
	   * 
	   * @param -objMessageObject:it contains CONSUMERCLASS, transaction id, transaction type
	   * @exception - Exception
	   */ 
	  public void processStorageBuild(Object objMessageObject, JobDataMap jobDataMap)
		      throws Exception {

		   HashMap hmParam = (HashMap) objMessageObject;    
		    String strTransactionId = GmCommonClass.parseNull((String)hmParam.get("TRANSID"));//Transaction ID 
		    String strStorageClass = GmCommonClass.parseNull((String)hmParam.get("TRANSTYPE"));//Transaction type ORDER or RETURN or CONSIGNMENT or INHOUSE412 or INHOUSE504
		    log.debug("GmStorageBuildConsumerJob TransactionId is "+strTransactionId);
		    log.debug("GmStorageBuildConsumerJob transaction type is "+strStorageClass);
		    String strOrderValidation = "";
		    GmSplitTransInterface gmSplitTransInterface =
		            (GmSplitTransInterface) GmCommonClass.getSpringBeanClass(
		                "xml/Trans_Split_Beans.xml", strStorageClass);
		    strOrderValidation =  gmSplitTransInterface.validateTransaction(strTransactionId);
		    if(strOrderValidation.equals("YES")){
		    	gmSplitTransInterface.processTransaction(strTransactionId);
		    }
		    
		    
		  }




	  
	  
}
