package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.ttpbyvendor.beans.GmTTPVendorPOTransBean;
import com.globus.valueobject.common.GmDataStoreVO;
import org.quartz.JobDataMap;


/**PMT- 49117 
 * GmVendorPOGenerationConsumerJob - this class used to process the PO generation and 
 * update the status(PO generated / PO generation Failed)
 *                                   
 * @author tramasamy
 *
 */
public class GmVendorPOGenerationConsumerJob extends GmActionJob implements
		GmConsumerDispatcherInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#
	 * processMessage(java.lang.Object)
	 */

	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		processVendorPOGenerate(objMessageObject, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
	 */

	public void execute(JobDataMap jobDataMap) throws Exception {
		processVendorPOGenerate(null, jobDataMap);
	}

	/**
	 * Method - processVendorPOGenerate used to generate the PO by vendor or TTP 
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */

	public void processVendorPOGenerate(Object objMessageObject,
			JobDataMap jobDataMap) throws Exception {
		log.debug("INSIDE PROCESS VENDOR PO GENERATION...");

		HashMap hmParam = (HashMap) objMessageObject;
		// Initialize
		String strUserId = "";
		String strLoadDate = "";
		String strVendorPOStatus = "";
		HashMap hmTemp = null;
		ArrayList alReturn = null;
		Iterator itrVendorDtls = null;

		strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
		strLoadDate = GmCommonClass.parseNull((String) hmParam.get("STRLOADDATE"));
		strVendorPOStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		
		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		

		GmTTPVendorPOTransBean gmTTPVendorPOTransBean = new GmTTPVendorPOTransBean(gmDataStoreVO);
		try {
			
			//PC-376: PO creation only DCO parts - to exclude the non DCO parts.
			
			alReturn = GmCommonClass.parseNullArrayList(gmTTPVendorPOTransBean.createPOInputString(strVendorPOStatus,strLoadDate, strUserId));
			gmTTPVendorPOTransBean.generateVendorPO(strVendorPOStatus,strLoadDate, strUserId);
			
			
			// IF there are any Parts which have No WO DCO, then this method will set the status of PO as Failed,this way user can generate the PO again once the WO DCO is done.
			if (!alReturn.isEmpty()){
				
				itrVendorDtls = alReturn.iterator();
				
				while (itrVendorDtls.hasNext()) {
					hmTemp =  GmCommonClass.parseNullHashMap((HashMap) itrVendorDtls.next());
					
					gmTTPVendorPOTransBean.updateVendorPOStatus(
							GmCommonClass.parseNull((String)hmTemp.get("CAPA_SUMM_ID")),
							GmCommonClass.parseNull((String)hmTemp.get("STATUS")),
							GmCommonClass.parseNull((String)hmTemp.get("ERROR_DTLS")),
							strUserId
							);
				}
			}
			

		} catch (Exception ex) {
			log.error("Exception occurred in class" + ex.getMessage());
			throw new AppError(ex);
		}
	}

}
