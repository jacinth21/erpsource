package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * This Consumer Class will be used to receive Message that needs to be processed for the part
 * Related Functionalities. We are using this class to sync part,Group,System and project for all
 * the Companies.
 * 
 * @throws Exception
 */

public class GmLoanerWarehouseConsumerJob extends GmActionJob implements
    GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    syncLoanerWarehouseInfo(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
    syncLoanerWarehouseInfo(objMessageObject, null);
  }

  /**
   * syncPartCompanyMapping - The below method is calling from processPartVisibility method.This
   * method is used to sync part,System,Group and project in part number setup screen and Release
   * for sale screen.
   * 
   * @param -objMessageObject:it contains CONSUMERCLASS,strUserId
   * @exception - Exception
   */

  public void syncLoanerWarehouseInfo(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {
    HashMap hmParam = (HashMap) objMessageObject;
    HashMap hmLnParam = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMLNPARAM"));
    String strCompanyID = "";
    String strCompAccess = "";
    
    int transStatus = Integer.parseInt(GmCommonClass.parseZero(GmCommonClass.parseNull((String)hmLnParam.get("TRANS_STATUS")))); 
    String strCompanyInfo = GmCommonClass.parseNull((String) hmLnParam.get("COMPANY_INFO"));
    GmDataStoreVO tmpGmDataStoreVO = null;
   
    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(tmpGmDataStoreVO);
  
    // This method is used to sync loaner warehouse information
     gmInvLocationBean.syncLoanerWarehouse(hmLnParam);
   
    strCompanyID = GmCommonClass.parseNull(tmpGmDataStoreVO.getCmpid());  // PMT-38155 - Lot Track - Bulk to Set Inventory Changes
    strCompAccess = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strCompanyID,"LOT_TRK",strCompanyID));
    log.debug("LOT_TRK=> "+strCompAccess+" transStatus===>"+transStatus);
    if (transStatus > 3 && strCompAccess.equals("Y")){
	  gmInvLocationBean.syncLotTrackChanges(hmLnParam);
	}
  }
}
