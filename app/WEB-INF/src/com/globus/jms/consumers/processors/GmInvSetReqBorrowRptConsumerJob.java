/**
 * 
 */
package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.beans.GmSplitTransInterface;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.sales.GmInvSetReqBorrowEmailVO;
import com.globus.sales.beans.GmInvSetReqBorrowRptBean;

/**
 * @author mselvamani
 *
 */
public class GmInvSetReqBorrowRptConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processRequestToBorrowEmail(null, jobDataMap);
	  }
	
	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processRequestToBorrowEmail(objMessageObject, null);
	  }
	  
	  
	  /**
	   * processRequestToBorrowEmail - The below method to validate NPI number and save status
	   * 
	   * @param -objMessageObject:it contains CONSUMERCLASS, transaction id, transaction type
	   * @exception - Exception
	   */ 
	  public String processRequestToBorrowEmail(Object objMessageObject, JobDataMap jobDataMap)
		      throws Exception {
		  	   log.debug(" **Inside GmInvSetReqBorrowRptConsumerJob processRequestToBorrowEmail**  ");
		  	   String strReturnMessage= "";
		       HashMap hmParam = (HashMap) objMessageObject;  
	           GmInvSetReqBorrowRptBean gmInvSetReqBorrowRptBean  = new GmInvSetReqBorrowRptBean();
	           strReturnMessage = gmInvSetReqBorrowRptBean.sendRequestToBorrowEmails(hmParam);
	           
	           return strReturnMessage;
	  }
}
