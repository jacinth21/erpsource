package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.sales.CaseManagement.beans.GmCaseBookTxnBean;

/*
 * author Manikandan Rajasekaran The Below Method is added as part of PMT-5928. This is used to
 * Approve the Item/Loaner/Set Initiated from Device. This is used to remove the Approval Time Lag
 * between Device and Portal.
 */

public class GmInvRequestConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    processInvRequest(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
    processInvRequest(objMessageObject, null);
  }

  public void processInvRequest(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

    GmCaseBookTxnBean gmCaseBookTxnBean = new GmCaseBookTxnBean();
    HashMap hmParam = (HashMap) objMessageObject;
    gmCaseBookTxnBean.saveInvRequestApprove(hmParam);

  }
}
