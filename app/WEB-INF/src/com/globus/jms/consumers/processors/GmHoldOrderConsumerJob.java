package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.jms.consumers.beans.GmOrderBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmHoldOrderConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{

    // TODO Auto-generated constructor stub
    Logger log = GmLogger.getInstance(this.getClass().getName());

    @Override
    public void execute(JobDataMap jobDataMap) throws Exception {
      //For quartz Job to process share that are partially processed
      GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);

      ArrayList alOrderIds = new ArrayList();

      HashMap hmParams = new HashMap();
      HashMap hmHoldValParam = new HashMap();
      String strOrderId="";
      String strReceiveMode="";      

      GmOrderBean gmOrderBean = new GmOrderBean(gmDataStoreVO);
      alOrderIds = gmOrderBean.fetchUnprocessedHoldOrders();
      int alSize = alOrderIds.size();
      hmParams.put("GMDATASTOREVO", gmDataStoreVO);
      for (int i=0;i<alSize;i++)
        {
            HashMap hmParam = new HashMap();
            hmParam = (HashMap)alOrderIds.get(i);  
            strOrderId = GmCommonClass.parseNull((String)hmParam.get("ORDERID"));
            strReceiveMode = GmCommonClass.parseNull((String)hmParam.get("RECEIVE_MODE"));
   
            hmParams.put("ORDERID", strOrderId);
            hmParams.put("USERID", "30301");//Manual Load
            if(strReceiveMode.equals("26230683")){
              hmParams.put("IPADCOMMENTS", strOrderId+" On Hold from CSM");//CSM Comments for hold
            }
            hmHoldValParam.put("HMHOLDVALUES", hmParams);
    
            updateOrderHold(hmHoldValParam,jobDataMap);
           
        }
  
    }

    @Override
    public void processMessage(Object objMessageObject) throws Exception {
      updateOrderHold(objMessageObject, null);
    }

    /**
     * UpdateOrderHold - to update the hold status of the order by calling the method-saveOrderHold
     * 
     * @param -objMessageObject:it contains CONSUMERCLASS
     * @exception - Exception
     */

    public void updateOrderHold(Object objMessageObject, JobDataMap jobDataMap)
        throws Exception {

      HashMap hmParam = (HashMap) objMessageObject;
      HashMap hmReturn = new HashMap();
      HashMap hmHoldValParam = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMHOLDVALUES"));
      String strCompanyInfo = GmCommonClass.parseNull((String) hmHoldValParam.get("COMPANY_INFO"));
      String strOrderId = GmCommonClass.parseNull((String) hmHoldValParam.get("ORDERID"));
      GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmHoldValParam.get("GMDATASTOREVO");
   // data store VO null then populate the Company Info
      gmDataStoreVO = (gmDataStoreVO != null)?gmDataStoreVO:(new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
      
      GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);
      GmOrderBean gmOrderBean = new GmOrderBean(gmDataStoreVO);
      //fetching order details
      hmReturn = gmDOBean.fetchOrderDetails(strOrderId);
      String strOrdHoldFl= GmCommonClass.parseNull((String) hmReturn.get("HOLD_FL"));
     //if it is already a hold order, then no need to call the below method
      if (strOrdHoldFl.equals("")) {
            gmOrderBean.saveOrderHold(hmHoldValParam);
      }

    }
  }


