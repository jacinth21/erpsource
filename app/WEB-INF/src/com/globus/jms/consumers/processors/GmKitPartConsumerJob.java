package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.sales.kit.bean.GmKitMappingReportBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingWrapperBean;

public class GmKitPartConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{

	 Logger log = GmLogger.getInstance(this.getClass().getName());

	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processInsert(null, jobDataMap);

	  }

	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processInsert(objMessageObject, null);
	  }

	  public void processInsert(Object objMessageObject, JobDataMap jobDataMap) throws AppError{
		  log.debug("Inside GmKitPartConsumerJob---");
		  	HashMap hmParam = (HashMap) objMessageObject; 
		    GmDataStoreVO gmDataStoreVO = null;
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANY_INFO"));
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    log.debug("GmKitPartConsumerJob strCompanyInfo========== "+strCompanyInfo);
		  	GmKitMappingWrapperBean gmKitMappingWrapperBean = new GmKitMappingWrapperBean(gmDataStoreVO);
	        GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmDataStoreVO);
		    String strPartNm = GmCommonClass.parseNull((String) hmParam.get("PART_NUM"));
			String strCmpId = GmCommonClass.parseNull((String) hmParam.get("CMP_ID"));			
			
	        HashMap hmreturn = gmKitMappingReportBean.fetchPartDetails(strPartNm,strCmpId);	 
	    	String strPartNum = GmCommonClass.parseNull((String)hmreturn.get("ID"));
	    	String strPartName = GmCommonClass.parseNull((String)hmreturn.get("NAME"));
	    	gmKitMappingWrapperBean.syncPartNumList(strPartNm, strPartNm, strPartName, "syncPartNumList", strCompanyInfo);
	  }

	}
