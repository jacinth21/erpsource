package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.prodmgmnt.beans.GmGroupServiceBean;

/**
 * This Consumer Class will be used to receive Message that needs to be processed for the PD Group
 * Related Functionalities. We are using this class to create Group Visibility for all the Companies
 * when a new Group is created.
 * 
 * @throws Exception
 */

public class GmPDGroupConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    syncGroupCompanyMapping(null, jobDataMap);

  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {
    syncGroupCompanyMapping(objMessageObject, null);
  }


  /**
   * syncGroupCompanyMapping - This method is calling from GmGroupServiceBean.java.This Method will
   * call syncGroupCompanyMapping method to save new Group and to create Group Visibility for all
   * the Companies
   * 
   * @param -objMessageObject:it contains strGroupID,strSystemId,CONSUMERCLASS,strUserId
   * @exception - Exception
   */

  public void syncGroupCompanyMapping(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {

    GmGroupServiceBean gmGroupServiceBean = new GmGroupServiceBean();

    HashMap hmParam = (HashMap) objMessageObject;

    String strGroupID = GmCommonClass.parseNull((String) hmParam.get("STRGROUPID"));
    String strSystemId = GmCommonClass.parseNull((String) hmParam.get("STRSYSTEMID"));
    String strMapType = "105361"; // 105361:Released
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("STRUSERID"));

    // This method is used to save Group based on system .If the system is released for
    // Two companies .The Group will be visible for the two companies.
    gmGroupServiceBean.syncGroupCompanyMapping(strGroupID, strSystemId, strMapType, strUserId);

  }
}
