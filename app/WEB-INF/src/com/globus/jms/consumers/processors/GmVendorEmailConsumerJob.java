package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.beans.GmSplitTransInterface;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.beans.GmVendorEmailInterface;
import com.globus.operations.purchasing.beans.GmDHREmailBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.db.GmDBManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Parthiban PMT-35877 Vendor Purchase order /Work Order Transaction
 *         Mail Notification Class Name:GmWorkOrderUpdateConsumerJob * This
 *         class used to send a email notification to purchasing team. If
 *         quality team modify any of the Purchase order /Work Order status to
 *         send the notification to purchasing team (all the open WO)-Using JMS
 *         call
 */

public class GmVendorEmailConsumerJob extends GmActionJob implements
		GmConsumerDispatcherInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	// instantiate(request, response);
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {
		processVendorEmailMain(null, jobDataMap);

	}

	@Override
	public void processMessage(Object objMessageObject) throws Exception {
		processVendorEmailMain(objMessageObject, null);
	}

	/**
	 * Method - processVendorEmailMain used once change the Vendor Purchase
	 * order /Work Order Transaction, send a email notification to purchasing
	 * team (open work orders). Based on Transaction to send email notification/
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */

	public void processVendorEmailMain(Object objMessageObject,
			JobDataMap jobDataMap) throws Exception {

		HashMap hmParam = (HashMap) objMessageObject;
		GmDataStoreVO gmDataStoreVO = null;
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmDHREmailBean gmDHREmailBean = new GmDHREmailBean();
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));

		gmDataStoreVO = gmWSUtil.populateCompanyInfo(strCompanyInfo);
		ArrayList alReturn = new ArrayList();
		boolean bolResult = false;
		String strVendorFl = "";

		String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		
		String strNCMRID = GmCommonClass.parseNull((String) hmParam.get("NCMRID"));
		log.debug("strNCMRID-----ncmr" + strNCMRID);
		
		if(strRefType.equals("26240816")) {
			gmDHREmailBean.sendNCMRCreationMail(strNCMRID, hmParam);	
		}
		else {
		// data store VO null then populate the Company Info

		GmVendorEmailInterface gmVendorEmailInterface = (GmVendorEmailInterface) GmCommonClass
				.getSpringBeanClass("xml/Vendor_Email_Beans.xml", strRefType);
		log.debug("strPOType>>>>>>  " + strRefType);
		log.debug("hmParam-----" + hmParam);

		String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
		log.debug("strRefId-----" + strRefId);

		if (!strRefId.equals("")) { // to avoid insert when calling for job mail
			gmVendorEmailInterface.saveVendorEmail(hmParam);
		}
		alReturn = gmVendorEmailInterface.fetchEmailDetail(hmParam);
		hmParam.put("ALRETURN", alReturn);

		if (alReturn.size() > 0) {
			HashMap hmTemp = new HashMap();
			hmTemp = (HashMap) alReturn.get(0);
			log.debug("hmTemp========== " + hmTemp);
		}

		bolResult = sendEmail(hmParam);

		log.debug("bolResult========== " + bolResult);
		if (bolResult == true) {
			saveVendorEmailStatus(hmParam); // Update the Mail Status
		}

	}

}

	/**
	 * sendEmail - This Methods will send the email
	 * @param hmParam
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */

	
	public boolean sendEmail(HashMap hmParam) throws AppError, Exception {

		HashMap hmEmailProperty = new HashMap();
		HashMap hmTempLoop = new HashMap();
		HashMap hmLoop = null;
		boolean bolResult = false;
		String strCurrVendorId = "";
		String strNextVendorId = "";
		String strTo = "";
		String strNextRefAction = "";
		String strCurrRefAction = "";

		ArrayList alTempList = new ArrayList();
		ArrayList alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRETURN"));
		log.debug("alTemp--->" + alTemp.size());
		int intsize = alTemp.size();
		for (int i = 0; i < intsize; i++) {
			hmLoop = new HashMap();
			hmLoop = (HashMap) alTemp.get(i);
			strCurrVendorId = GmCommonClass.parseNull((String) hmLoop.get("VID"));
			strCurrRefAction = GmCommonClass.parseNull((String) hmLoop.get("REFACT"));
			strTo = GmCommonClass.parseNull((String) hmLoop.get("EMAILID"));

			if (i < intsize - 1) {
				hmTempLoop = (HashMap) alTemp.get(i + 1);
				strNextVendorId = GmCommonClass.parseNull((String) hmTempLoop.get("VID"));
				strNextRefAction = GmCommonClass.parseNull((String) hmTempLoop.get("REFACT"));
			}
			alTempList.add(hmLoop);
			if (!strCurrVendorId.equals(strNextVendorId) || i == intsize - 1 || (!strCurrRefAction.equals(strNextRefAction))) {
				hmParam.put("ALRETURN", alTempList);
				hmParam.put("STRTO", strTo);
				hmParam.put("REFACTION", strCurrRefAction);

				hmEmailProperty = populateEmailData(hmParam);
				log.debug(":hmEmailProperty=======:" + hmEmailProperty);
				try {
					GmCommonClass.sendMail(hmEmailProperty);
					bolResult = true;
				} catch (Exception ex) {
					bolResult = false;
					log.error("Exception in sending" + ex.getMessage());
					throw new AppError(ex);
				}
				alTempList = new ArrayList();
			}

		}
		return bolResult;
	}

	/**
	 * populateEmailData - This Methods is used to populate the mail content
	 * 
	 * @param hmParam
	 *  * @return
	 * @throws AppError
	 */

	
	public HashMap populateEmailData(HashMap hmParam) throws AppError {
		GmDataStoreVO gmDataStoreVO = null;
		GmWSUtil gmWSUtil = new GmWSUtil();
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));

		gmDataStoreVO = gmWSUtil.populateCompanyInfo(strCompanyInfo);
		String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Email.EmailTemplate",
						strCompanyLocale);
		boolean bolResult = false;
		HashMap hmEmailProperty = new HashMap();
		HashMap hmLoop = new HashMap();
		String strCurrRefAction = GmCommonClass.parseNull((String) hmParam.get("REFACTION"));
		String TEMPLATE_NAME = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCurrRefAction, "VENDOR_EXT"));
		String strMimeType = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
		String strFrom = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM));
		String strSubject = gmResourceBundleBean.getProperty(TEMPLATE_NAME+ "." + GmEmailProperties.SUBJECT);
		String strMessage = gmResourceBundleBean.getProperty(TEMPLATE_NAME+ "." + GmEmailProperties.MESSAGE);
		String strToCc = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."+ GmEmailProperties.CC);
		String strTo = GmCommonClass.parseNull((String) hmParam.get("STRTO"));
		String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strPOId="";
		ArrayList alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRETURN"));
		
		int intsize = alTemp.size();
		//For DHR Type get PO ID from alreturn else Get from REF ID (other types)
		 if(alTemp.size() == 1 && strRefType.equals("108442")){
			for (int i = 0; i < intsize; i++) {
				hmLoop = (HashMap) alTemp.get(i);
				strPOId = GmCommonClass.parseNull((String) hmLoop.get("POID"));
				}
			strSubject = strSubject.replaceAll("#<POID>",strPOId);
			strMessage = strMessage.replaceAll("#<POID>",strPOId);
		}else if (alTemp.size() == 1 && !strRefType.equals("108442")){
			strSubject = strSubject.replaceAll("#<POID>",GmCommonClass.parseNull((String) hmParam.get("REFID")));
			strMessage = strMessage.replaceAll("#<POID>",GmCommonClass.parseNull((String) hmParam.get("REFID")));
		}else{
			strSubject = strSubject.replaceAll("#<POID>", "");
			strMessage = strMessage.replaceAll("#<POID>", "PO");
		}
		hmParam.put("STRMESSAGE", strMessage);
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setMimeType(strMimeType);
		emailProps.setSender(strFrom);
		emailProps.setRecipients(strTo);
		emailProps.setCc(strToCc);
		emailProps.setSubject(strSubject);
		emailProps.setMessage(strMessage);
		hmEmailProperty = generateOutput(emailProps, hmParam);
		return hmEmailProperty;

	}

	/**
	 * generateOutPut This method is used to generate data which need to show
	 * email content.
	 * 
	 * @param hmParam
	 *            HashMap,GmEmailProperties emailProps
	 * @exception AppError
	 */
	public HashMap generateOutput(GmEmailProperties emailProps, HashMap hmParam)
			throws AppError {
		String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strTemplateName = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRefType, "VENDOR_EXT"));
		log.debug("strRefType---->>>" + strRefType + " strTemplateName---->"+ strTemplateName);
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setTemplateSubDir("operations/templates");
		templateUtil.setTemplateName(strTemplateName);
		templateUtil.setDataMap("hmEmailParam", hmParam);
		String strEmailContent = templateUtil.generateOutput();

		HashMap hmEmailProperty = new HashMap();
		hmEmailProperty.put("strFrom", emailProps.getSender());
		hmEmailProperty.put("strTo", emailProps.getRecipients());
		hmEmailProperty.put("strCc", emailProps.getCc());
		hmEmailProperty.put("strSubject", emailProps.getSubject());
		hmEmailProperty.put("strMessage", emailProps.getMessage());
		hmEmailProperty.put("strMessageDesc", strEmailContent);
		hmEmailProperty.put("strMimeType", emailProps.getMimeType());

		return hmEmailProperty;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.globus.operations.beans.GmVendorEmailInterface#saveVendorEmailStatus
	 * (java.util.HashMap)
	 */
	public void saveVendorEmailStatus(HashMap hmParam) throws AppError {

		GmDataStoreVO gmDataStoreVO = null;
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManger = new GmDBManager(gmDataStoreVO);

		log.debug("========emailupdate============= " + hmParam);
		String strRefId = "";
		HashMap hmLoop = null;
		ArrayList alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRETURN"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

		log.debug("alTemp--->" + alTemp.size());
		int intsize = alTemp.size();
		for (int i = 0; i < intsize; i++) {
			hmLoop = new HashMap();
			hmLoop = (HashMap) alTemp.get(i);
			strRefId += GmCommonClass.parseNull((String) hmLoop.get("REFID")) + ',';

		}
		log.debug("strRefId>>>on save status" + strRefId);

		gmDBManger.setPrepareString("gm_pkg_vendor_mail_txn.gm_sav_email_status", 2);
		gmDBManger.setString(1, strRefId);
		gmDBManger.setString(2, strUserId);// user id;
		gmDBManger.execute();
		gmDBManger.commit();

	}

}
