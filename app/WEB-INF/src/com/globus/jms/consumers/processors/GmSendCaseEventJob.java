
package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmExchangeManager;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.crm.bean.GmCRMActivityBean;
import com.globus.webservice.sales.casemanagement.bean.GmCaseRptBean;
import com.globus.webservice.sales.casemanagement.bean.GmCaseTxnBean;



/**
 * @author agilan
 * This class is called when case booking, edit and cancelled 
 * 
 */
public  class GmSendCaseEventJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code.
	
	String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");	
	
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {
		log.debug("strProcessType===>" + strProcessType);
		//if (strProcessType.equals("QUARTZJOB")) {
			processCaseEventUpdateCal(null, jobDataMap);
		//}
	}
	@Override
	public void processMessage(Object objMessageObject) throws Exception {
		processCaseEventUpdateCal(objMessageObject, null);
	}

/**
 * This method is used to send notification to rep based on case book, edit and cancel status
 */
	public void processCaseEventUpdateCal(Object objMessageObject,
			JobDataMap jobDataMap) throws Exception {
		log.debug("processCaseEventUpdateCal==>");
		 HashMap hmParam = (HashMap) objMessageObject;
		    GmDataStoreVO gmDataStoreVO = null;
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANY_INFO"));
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    log.debug("GmKitSetShipConsumerJob strCompanyInfo========== "+strCompanyInfo);
		GmExchangeManager gmExchangeManager = new GmExchangeManager();
		GmEmailProperties gmEmailProperties = new GmEmailProperties();
		GmCaseTxnBean gmCaseTxnBean = new GmCaseTxnBean(gmDataStoreVO);
		GmCaseRptBean gmCaseRptbean = new GmCaseRptBean(gmDataStoreVO);
		
		
		String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCmpdFmt = GmCommonClass.parseNull((String) hmParam.get("CMPDFMT"));
		ArrayList alList = gmCaseRptbean.fetchCaseEventCal(strCaseInfoId,strUserID,strCmpdFmt);
		sendNotificationEmail(alList, hmParam);
	}
	
	/**
	 * @param ArrayList, String
	 * This method is used to send notification to rep based on case book, edit and cancel status
	 */
	public void sendNotificationEmail(ArrayList alList, HashMap hmParam) throws AppError, Exception {
		GmExchangeManager gmExchangeManager = new GmExchangeManager();
		GmDataStoreVO gmDataStoreVO = null;
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANY_INFO"));
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmCaseTxnBean gmCaseTxnBean = new GmCaseTxnBean(gmDataStoreVO);
		GmEmailProperties gmEmailProperties = new GmEmailProperties();
		
		HashMap hmParams = new HashMap();
		ArrayList alParticipants = new ArrayList();
		String strSurgeon = "";
		String strAccId = "";
		String strDate = "";
		String strStartTime = "";
		String strEndTime = "";
		String strLocation = "";
		String strItemId = "";
		String strEventItemId = "";
		String strRepEmailId = "";
		String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
		String strMailStatus = GmCommonClass.parseNull((String) hmParam.get("MAILSTATUS"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strRecipients = GmCommonClass.parseNull((String) hmParam.get("RECIPIENTS"));
		String strSubjects = GmCommonClass.parseNull((String) hmParam.get("SUBJECT"));
		String strMessage = GmCommonClass.parseNull((String) hmParam.get("MESSAGE"));
		log.debug("alList===>" + alList.size());


		for (int i = 0; i < alList.size(); i++) {
			HashMap hmdata = new HashMap();
			hmdata = GmCommonClass.parseNullHashMap((HashMap) alList.get(i));
			strSurgeon += "," + hmdata.get("SURNM");
			strAccId = GmCommonClass.parseNull((String) hmdata.get("ACCID"));
			strStartTime = hmdata.get("SDATE") + " " + hmdata.get("SRTTM")+ " GMT";
			strEndTime = hmdata.get("SDATE") + " " + hmdata.get("ENDTM")+ " GMT";
			strLocation = GmCommonClass.parseNull((String) hmdata.get("LOC"));
			strEventItemId = GmCommonClass.parseNull((String) hmdata.get("EVTITEMID"));
			strRepEmailId = GmCommonClass.parseNull((String) hmdata.get("REPEMAILID"));
		}

		gmExchangeManager.setMailProperties("GMI_CASESCHE_MAILBOX","CRM_CASESCHE_EVENT_PASSWORD");
		String strSender = GmCommonClass.getString("CRM_CASESCHE_EVENT_USER");
	
		String strCategory = "";
		String strMessageBody = "";
		gmEmailProperties.setNotifyfl("Y");
		gmEmailProperties.setMimeType("HTML");
		gmEmailProperties.setSender(strSender);
		gmEmailProperties.setSubject(strSubjects);
		gmEmailProperties.setRecipients(strRecipients);
		gmEmailProperties.setStartTime(strStartTime);
		gmEmailProperties.setEndime(strEndTime);
		gmEmailProperties.setMessage(strMessage);
		gmEmailProperties.setEventItemId(strEventItemId);
		gmEmailProperties.setCategory(strCategory);
		gmEmailProperties.setLocation(strLocation);
		log.debug("strMailStatus--->"+strMailStatus);
		if (strMailStatus.equals("1")) {
			strItemId = gmExchangeManager.sendMeetingRequest(gmEmailProperties);
			hmParams.put("CASEINFOID", strCaseInfoId);
			hmParams.put("ITEMID", strItemId);
			hmParams.put("USERID", strUserID);
			gmCaseTxnBean.saveEventItemId(hmParams);
			gmEmailProperties.setEventItemId(strItemId);
			strItemId = gmExchangeManager.confirmMeetingRequest(gmEmailProperties);		
		} else if (strMailStatus.equals("2") && !strEventItemId.equals("")) {
			// strStatus = "UPDATED";
			strItemId = gmExchangeManager.updateMeetingRequest(gmEmailProperties);
		} else if (strMailStatus.equals("3") && !strEventItemId.equals("")) {
			// strStatus = "CANCELLED";
			gmExchangeManager.cancelMeetingRequest(gmEmailProperties);
		}
	}
}

