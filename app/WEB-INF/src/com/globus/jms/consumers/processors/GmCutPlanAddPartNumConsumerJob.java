package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;
 

/**
 * 
 * @author tramasamy
 * Class Name: GmCutPlanAddPartNumConsumerJob.java
 * Description:This class is used to process the Cut plan Analysis lock and generate
 * 
 */
public class GmCutPlanAddPartNumConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	/* (non-Javadoc)
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
	 */
	
	 @Override
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		processAddPartNum(objMessageObject, null);
	}

	/* (non-Javadoc)
	 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
	 */
	
	
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processAddPartNum(null, jobDataMap); }
	 
	
	  /**PC_3191
	 * Method - processAddPartNum used to sadd new part number in cut plan details
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @return 
	 * @throws AppError
	 */
	
	public String processAddPartNum(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    log.debug("INSIDE GmCutPlanAddPartNumConsumerJob...");
		   
		    HashMap hmParam = (HashMap) objMessageObject; 
		    //Initialize
			String strUserId = "";
			String strCompanyId="";
			String strPlantId= "";
			String strPartNum ="";
			String strMsg="";
			
			 log.debug(" GmCutPlanAddPartNumConsumerJob..." +hmParam);
			strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANT_ID"));
			strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));
			strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
			int cutplantLockId= (Integer) hmParam.get("CUTPLAN_LOCK_ID");
			int futureNeed =(Integer) hmParam.get("FUTURE_NEED");
			strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo")); 
		   
		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    
		    GmDBManager gmDBManager = new GmDBManager();
		    gmDBManager.setPrepareString("gm_pkg_cutplan_txn.gm_sav_new_part_num",7);
		    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
		    gmDBManager.setInt(1, cutplantLockId);
			gmDBManager.setString(2, strPartNum);
			gmDBManager.setInt(3, futureNeed);
			gmDBManager.setString(4, strCompanyId);
			gmDBManager.setString(5, strPlantId);
			gmDBManager.setString(6, strUserId);
			gmDBManager.execute();
			strMsg = GmCommonClass.parseNull(gmDBManager.getString(7));
			gmDBManager.commit();	
			log.debug("GmCutPlanAddPartNumConsumerJob.. strMsg ."+strMsg);
			return strMsg;

	}
}
