package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.common.util.jobs.GmJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityReportBean;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityTransBean;
import com.globus.sales.Loaners.beans.GmLoanerExtBean;
import com.globus.valueobject.common.GmDataStoreVO;
/**
 * @author tramasamy 
 * PMT-36078 Loaner Extension email notification changes
 *   Class GmLoanerExtnApprConsumerJob * This
 *   class used to send a email notification to Sales Rep.
 *   when Loaner Extension Requests are approved or rejected
 *   send the notification to that particular Sales rep -Using JMS call
 */
public class GmLoanerExtnApprConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{
	/**
	 * This used to get the instance of this class for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {
		processInsert(null, jobDataMap);
	}
	@Override
	public void processMessage(Object objMessageObject) throws Exception {
		processInsert(objMessageObject, null);
	}
	/**processInsert method used to send Loaner Extension Approval and rejection Email using JMS call
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	public void processInsert(Object objMessageObject, JobDataMap jobDataMap)
			throws AppError {
		log.debug("LOANER EXTN CONSUMERJOB");

		HashMap hmParam = (HashMap) objMessageObject; 
		HashMap hmResult = new HashMap();
		HashMap hmValParam = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMVALUES"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmValParam.get("COMPANYINFO"));
		GmDataStoreVO gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(gmDataStoreVO);
		gmLoanerExtBean.sendLoanerExtnStatusEmail(hmValParam);
	}


	/**populateEmailData - to get Email Notification data and send Jasper Email 
	 * @param hmParam
	 * @param alreturn
	 * @throws AppError
	 */
	public void populateEmailData(HashMap hmParam , ArrayList alreturn) throws AppError {
	
	    ArrayList alResult = new ArrayList();
		HashMap hmTempLoop = new HashMap();
		HashMap hmLoop = new HashMap();
		HashMap hmjasper = new HashMap();
		String strParentReqId = GmCommonClass.parseNull((String) hmParam.get("PARENTREQID"));
		String strStatus="";
		
	    int intReqDtlSize = alreturn.size();
	    log.debug("intReqDtlSize>>>>>>>>"+intReqDtlSize);
		for (int i = 0; i < intReqDtlSize; i++){
			hmLoop = (HashMap) alreturn.get(i);
			String strParentReqNewId = GmCommonClass.parseNull((String) hmLoop.get("PARENTREQID"));
			if(strParentReqId.equals(strParentReqNewId)){
				strStatus = GmCommonClass.parseNull((String) hmLoop.get("EXTNSTATUS"));
				hmParam.put("STATUS", strStatus);
				alResult.add(hmLoop);
			}
		}
		if(alResult.size() >0){
			sendMail(hmParam,alResult);
		}  
  }
	/**populateEmailData - to get Email Notification data and send Jasper Email 
	 * @param hmParam
	 * @param alreturn
	 * @throws AppError
	 */
	public void sendMail(HashMap hmParam , ArrayList alreturn) throws AppError {
		
		GmJasperMail jasperMail = new GmJasperMail();
		GmEmailProperties gmEmailProps = new GmEmailProperties();
		HashMap hmResult = new HashMap();
		
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		GmDataStoreVO gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		String strRepEmail = GmCommonClass.parseNull((String) hmParam.get("EMAILADD"));
		String  strTodayDt = GmCommonClass.parseNull((String) hmParam.get("TODAYSDATE"));
		String  strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
		String strRepName = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
		
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Email.EmailTemplate",strCompanyLocale);
		gmEmailProps.setRecipients(strRepEmail);
		gmEmailProps.setMimeType(gmResourceBundleBean.getProperty("GmLoanerExtensionEmail."+ GmEmailProperties.MIME_TYPE));
		gmEmailProps.setSender(gmResourceBundleBean.getProperty("GmLoanerExtensionEmail."+ GmEmailProperties.FROM));
		String strSubject ="";
		String strEmailHeader= gmResourceBundleBean.getProperty("GmLoanerExtensionEmail." + GmEmailProperties.EMAIL_HEADER_NM);
		strSubject =gmResourceBundleBean.getProperty("GmLoanerExtensionEmail." + GmEmailProperties.SUBJECT);
		gmEmailProps.setMessageFooter(gmResourceBundleBean.getProperty("GmLoanerExtensionEmail." + GmEmailProperties.MESSAGE_FOOTER));
		
		if(strStatus.equals("1902")){
			strSubject = GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", strRepName);
			strSubject = GmCommonClass.replaceAll(strSubject, "#<EXTDATE>", strTodayDt); 
			strSubject = GmCommonClass.replaceAll(strSubject, " #<STATUS>", "Approved");
			strEmailHeader = GmCommonClass.replaceAll(strEmailHeader, "#<STATUS>", "Approved");
			jasperMail.setJasperReportName("/GmEmailLoanerExtApproval.jasper");
		}else if(strStatus.equals("1903")){
			strSubject = GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", strRepName);
			strSubject = GmCommonClass.replaceAll(strSubject, "#<EXTDATE>", strTodayDt);
			strSubject = GmCommonClass.replaceAll(strSubject, " #<STATUS>", "Rejected");
			strEmailHeader = GmCommonClass.replaceAll(strEmailHeader, "#<STATUS>", "Rejected");
			jasperMail.setJasperReportName("/GmEmailLoanerExtReject.jasper");
		}
		gmEmailProps.setEmailHeaderName(strEmailHeader);
		gmEmailProps.setSubject(strSubject);
		jasperMail.setAdditionalParams(hmParam);
		jasperMail.setReportData(alreturn);
		jasperMail.setEmailProperties(gmEmailProps);
		hmResult = jasperMail.sendMail();
		log.debug("hmResult jasper Mail " +hmResult); 
	}
}

