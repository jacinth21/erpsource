package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.webservice.crm.bean.GmCRMActivityBean;



/**
 * @author gopinathan
 * This class will call when Activity (Physician Visit,Merc,Sales Call and training) created in CRM and 
 * Files shared in Marketing Collateral.It will sync records from CRM activity and Marketing collateral File share into Party activity table(t6650).
 * The Party Activity record will display in CRM Surgeon Module - Globus Activity Timeline 
 */
public  class GmCRMPartyActivitySyncConsumerJob extends GmActionJob implements
		      
		GmConsumerDispatcherInterface {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code.
	
	String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");	
	
	public void execute(JobDataMap jobDataMap ) throws Exception {
		 
		if (strProcessType.equals("QUARTZJOB"))
		{
			processCRMPartyActivitySync(null,jobDataMap);  
		}
	}
	 
	public void processMessage(Object objMessageObject) throws Exception   {
		processCRMPartyActivitySync(objMessageObject,null);
	}
	public void processCRMPartyActivitySync(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
		
			HashMap hmParam = (HashMap)objMessageObject;
		    GmCRMActivityBean gmCRMActivityBean = new GmCRMActivityBean();
		    gmCRMActivityBean.savePartyActivitySync(hmParam);

	} 

}
