package com.globus.jms.consumers.processors;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import com.globus.jms.consumers.beans.GmDHRPriorityUpdateBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import org.quartz.JobDataMap;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDHRPriorityConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

	
	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {

		  UpdateDHRPriority(null, jobDataMap);

	  }
  @Override
  public void processMessage(Object objMessageObject) throws Exception {

	  UpdateDHRPriority(objMessageObject,null);
  }

  public void UpdateDHRPriority(Object objMessageObject,JobDataMap jobDataMap) throws Exception {

    HashMap hmMessageObj = (HashMap) objMessageObject;
    String strDHRID = GmCommonClass.parseNull((String) hmMessageObj.get("DHR_ID"));
    String strUserID = GmCommonClass.parseNull((String) hmMessageObj.get("USER_ID"));

    GmDHRPriorityUpdateBean gmDHRPriorityUpdateBean = new GmDHRPriorityUpdateBean();
    gmDHRPriorityUpdateBean.SyncDHRPriority(strDHRID,strUserID);

    }


  }


