package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.jms.consumers.beans.GmOrderBean;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.sales.CaseManagement.beans.GmCaseSchedularBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCaseSchedulerTagConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{

	 Logger log = GmLogger.getInstance(this.getClass().getName());

	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processInsert(null, jobDataMap);

	  }

	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processInsert(objMessageObject, null);
	  }

	  public void processInsert(Object objMessageObject, JobDataMap jobDataMap)
	      throws Exception {
	    log.debug("inside GmCaseSchedulerTagConsumerJob..");
	    HashMap hmParam = (HashMap) objMessageObject;   
	    log.debug("hmParam GmCaseSchedulerTagConsumerJob========== "+hmParam);
	    String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
		String strKitId = GmCommonClass.parseNull((String) hmParam.get("KITID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
	    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
	    log.debug("hmParam strCompanyInfo========== "+strCompanyInfo);
	    GmDataStoreVO gmDataStoreVO = null;
	    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
	    GmCaseSchedularBean gmCaseSchedularBean = new GmCaseSchedularBean(gmDataStoreVO);
	    
	    gmCaseSchedularBean.updateSetDeatails(strCaseId,strKitId,strStatus,strUserId);

	  }

	}