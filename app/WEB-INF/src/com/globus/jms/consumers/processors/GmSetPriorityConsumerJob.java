package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.AppError;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityReportBean;
import com.globus.operations.purchasing.setpriority.beans.GmSetPriorityTransBean;


/**GmSetPriorityConsumerJob class used when status is save to allocate process 
 * via JMS call
 * @author pvigneshwaran
 *
 */
public class GmSetPriorityConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	 /**
	 * This used to get the instance of this class for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processSetPriority(null, jobDataMap);
}
	
	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  processSetPriority(objMessageObject, null);
	}
	  
	/**processSetPriority:This method used to process set priority using JMS call
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	public void processSetPriority(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    log.debug("SET PRIORITY CONSUMERJOB");
		    
		    HashMap hmParam = (HashMap) objMessageObject; 
		    
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		   
		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    GmSetPriorityReportBean gmSetPriorityReportBean = new GmSetPriorityReportBean(gmDataStoreVO);
		    GmSetPriorityTransBean gmSetPriorityTransBean = new GmSetPriorityTransBean(gmDataStoreVO);
		
			gmSetPriorityTransBean.processSetPriorityAllocation(hmParam);   
		    gmSetPriorityReportBean.sendMailNotification(hmParam);	    
  }
	
}
