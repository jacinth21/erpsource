package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.sales.pricing.beans.GmImpactAnalysisBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * The Class GmPRTImpactAnalysisConsumerJob. (PRT-R203)
 * 
 * @author Matt Balraj
 */
public class GmPRTImpactAnalysisConsumerJob extends GmActionJob implements
    GmConsumerDispatcherInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");


  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    if (strProcessType.equals("QUARTZJOB")) {
      processBatchCommon(null, jobDataMap);
    }
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {

    processBatchCommon(objMessageObject, null);
  }

  /**
   * processBatchCommon
   * 
   * @author : Matt B
   * @param String strPReqId
   * @return void
   */
  public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
    log.debug("<====Entering processBatchCommon (GmPRTImpactAnalysisConsumerJob) ====>");
    HashMap hmMessageObj = (HashMap) objMessageObject;
    String strBatchID = GmCommonClass.parseNull((String) hmMessageObj.get("BATCHID"));
    String strBatchType = GmCommonClass.parseNull((String) hmMessageObj.get("BATCHTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmMessageObj.get("USERID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmMessageObj.get("COMPANYINFO"));

    String strRefId = GmCommonClass.parseNull((String) hmMessageObj.get("INVID"));
    GmDataStoreVO tmpGmDataStoreVO = null;

    // companyinfo is not filled when request is from IPAD
    if (strCompanyInfo.equals("")) {
      tmpGmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
    } else {
      tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    }


    log.debug(" JMS (Impact Analysis) T9600 BatchID ====>" + strBatchID);
    // GmBatchBean gmBatchBean = new GmBatchBean();
    GmImpactAnalysisBean gmImpactAnalysisBean = new GmImpactAnalysisBean(tmpGmDataStoreVO);

    // Call bean method to create impact analysis
    hmMessageObj.put("BATCHID", strBatchID);
    hmMessageObj.put("BATCHTYPE", strBatchType);
    hmMessageObj.put("BATCHSTATUS", "18742"); // Process in progress
    hmMessageObj.put("USERID", strUserId); // Process in progress
    hmMessageObj.put("INVID", strRefId); // Process in progress
    gmImpactAnalysisBean.updateImpactAnalysis(hmMessageObj);
  }


}
