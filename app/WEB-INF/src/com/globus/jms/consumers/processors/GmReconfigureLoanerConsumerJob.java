package com.globus.jms.consumers.processors;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.print.attribute.standard.MediaName;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmURLConnection;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.loaners.beans.GmReconfigLoanerBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmReconfigureLoanerConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

	  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
  
      processReconfigLoaner(null, jobDataMap);
   
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {

	  processReconfigLoaner(objMessageObject, null);
  }

  public void processReconfigLoaner(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

	  log.debug("Inside ReconfigureLoaner Consumer Job ");
    HashMap hmMessageObj = (HashMap) objMessageObject;
    HashMap hmCONSIGNMENT= new HashMap();
    HashMap hmLoop = new HashMap();
  
    String strCompanyInfo = GmCommonClass.parseNull((String) hmMessageObj.get("COMPANYINFO"));
    String strUserId = GmCommonClass.parseNull((String) hmMessageObj.get("USERID"));
    
    GmDataStoreVO tmpGmDataStoreVO = null;
    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    GmCommonBean gmCommonBean = new GmCommonBean(tmpGmDataStoreVO);
    GmReconfigLoanerBean gmReconfigLoanerBean = new GmReconfigLoanerBean(tmpGmDataStoreVO);
    log.debug("PrintLoanerPPW + " + hmMessageObj);
    String strSubRptDir = GmCommonClass.parseNull((String) hmMessageObj.get("SUBREPORT_DIR"));
   String strConsignId = GmCommonClass.parseNull((String) hmMessageObj.get("CONSID"));
   String strUserName = gmCommonBean.getUserName(strUserId);
   String strSetID = GmCommonClass.parseNull((String) hmMessageObj.get("SETID"));
   String strPrintFl = GmCommonClass.parseNull((String) hmMessageObj.get("PRINTFL"));
   String strLoanerReqId = GmCommonClass.parseNull((String) hmMessageObj.get("LOANERREQID"));
   log.debug(" strConsignId " + strConsignId);
   gmReconfigLoanerBean.generatePDFPrintLoanerPPW(strConsignId,strSubRptDir,strUserName);
   log.debug(" strConsignId " + strConsignId);
   gmReconfigLoanerBean.generatePDFViewPrint(hmMessageObj,strUserName);
 
  }
  
 
}
