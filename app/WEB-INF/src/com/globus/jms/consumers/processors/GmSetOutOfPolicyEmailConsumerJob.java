package com.globus.jms.consumers.processors;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.beans.GmSplitTransInterface;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.beans.GmVendorEmailInterface;
import com.globus.operations.purchasing.beans.GmDHREmailBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.db.GmDBManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import oracle.jdbc.OracleTypes;

/**
 * @author MSelvamani PC-3072 Set Out Of Policy Report
 *         Mail Notification Class Name:GmSetOutOfPolicyEmailConsumerJob * This
 *         class used to send a email notification for flag/unflag action in SOP Report.
 */

public class GmSetOutOfPolicyEmailConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	// instantiate(request, response);
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {
		processSetOutOfPolicyEmail(null, jobDataMap);

	}

	@Override
	public void processMessage(Object objMessageObject) throws Exception {
		processSetOutOfPolicyEmail(objMessageObject, null);
	}

	/**
	 *@author MSelvamani PC-3072 Set Out Of Policy Report
     * Mail Notification Class Name:GmSetOutOfPolicyEmailConsumerJob * This
     * class used to send a email notification for flag/unflag action in SOP Report.
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */

	public void processSetOutOfPolicyEmail(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

		HashMap hmParam = (HashMap) objMessageObject;
		GmDataStoreVO gmDataStoreVO = null;
		GmWSUtil gmWSUtil = new GmWSUtil();
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strDateFormat = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
		String strPolicyIds = "";

		gmDataStoreVO = gmWSUtil.populateCompanyInfo(strCompanyInfo);
		ArrayList alReturn = new ArrayList();
		String strArray = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		JsonParser parser = new JsonParser();
		JsonElement tradeElement = parser.parse(strArray);
		JsonArray array = tradeElement.getAsJsonArray();

		for (JsonElement pa : array) {
			HashMap hmSendParam = new HashMap();
			JsonObject jobject = pa.getAsJsonObject();
			strPolicyIds = jobject.get("sopids").getAsString();
			hmSendParam.put("COMPANYINFO", strCompanyInfo);
			hmSendParam.put("USERID", strUserId);
			hmSendParam.put("DATEFORMAT", strDateFormat);
			hmSendParam.put("SOPIDS", jobject.get("sopids").getAsString());
			hmSendParam.put("SUBJECT", jobject.get("subject").getAsString());
			hmSendParam.put("TO", jobject.get("toemailids").getAsString());
			hmSendParam.put("CC", jobject.get("ccemailids").getAsString());
			hmSendParam.put("MESSAGE", jobject.get("message").getAsString());
			alReturn = fetchSetOutOfPolicyEmailData(strPolicyIds, strCompanyInfo,strDateFormat);
			boolean bolResult = false;
			hmSendParam.put("ALRETURN", alReturn);
			bolResult = sendEmail(hmSendParam);

			log.debug("hmSendParam========== " + hmSendParam);
		}

	}

	/**
	 * sendEmail - This Methods will send the email
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */

	public boolean sendEmail(HashMap hmParam) throws AppError, Exception {

		HashMap hmEmailProperty = new HashMap();
		HashMap hmTempLoop = new HashMap();
		HashMap hmLoop = null;
		boolean bolResult = false;
		ArrayList alTempList = new ArrayList();
		ArrayList alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRETURN"));
		log.debug("alTemp--->" + alTemp.size());

		hmEmailProperty = populateEmailData(hmParam);
		log.debug(":hmEmailProperty=======:" + hmEmailProperty);
		try {
			GmCommonClass.sendMail(hmEmailProperty);
			bolResult = true;
		} catch (Exception ex) {
			bolResult = false;
			log.error("Exception in sending" + ex.getMessage());
			throw new AppError(ex);
		}

		hmParam.put("MESSAGEDESC", GmCommonClass.parseNull((String) hmEmailProperty.get("strMessageDesc")));
		if (bolResult == true) {
			saveSetOutOfPolicyEmail(hmParam);
		}

		alTempList = new ArrayList();

		return bolResult;
	}

	/**
	 * populateEmailData - This Methods is used to populate the mail content
	 * 
	 * @param hmParam * @return
	 * @throws AppError
	 */

	public HashMap populateEmailData(HashMap hmParam) throws AppError {
		GmDataStoreVO gmDataStoreVO = null;
		GmWSUtil gmWSUtil = new GmWSUtil();
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));

		gmDataStoreVO = gmWSUtil.populateCompanyInfo(strCompanyInfo);
		String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass
				.getResourceBundleBean("properties.Email.EmailTemplate", strCompanyLocale);
		boolean bolResult = false;
		HashMap hmEmailProperty = new HashMap();
		HashMap hmLoop = new HashMap();
		String TEMPLATE_NAME = "GmSOPEmail";
		String strMimeType = GmCommonClass
				.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
		String strFrom = GmCommonClass
				.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM));
		String strSubject = GmCommonClass.parseNull((String) hmParam.get("SUBJECT"));
		String strMessage = GmCommonClass.parseNull((String) hmParam.get("MESSAGE"));
		String strToCc = GmCommonClass.parseNull((String) hmParam.get("CC"));
		String strTo = GmCommonClass.parseNull((String) hmParam.get("TO"));
		// String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strPOId = "";
		ArrayList alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRETURN"));

		hmParam.put("STRMESSAGE", strMessage);
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setMimeType(strMimeType);
		emailProps.setSender(strFrom);
		emailProps.setRecipients(strTo);
		emailProps.setCc(strToCc);
		emailProps.setSubject(strSubject);
		emailProps.setMessage(strMessage);
		hmEmailProperty = generateOutput(emailProps, hmParam);
		return hmEmailProperty;

	}

	/**
	 * generateOutPut This method is used to generate data which need to show email
	 * content.
	 * 
	 * @param hmParam HashMap,GmEmailProperties emailProps
	 * @exception AppError
	 */
	public HashMap generateOutput(GmEmailProperties emailProps, HashMap hmParam) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setTemplateSubDir("operations/templates");
		templateUtil.setTemplateName("GmSOPMailReport.vm");
		templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("ALRETURN"));
		templateUtil.setDataMap("hmEmailParam", hmParam);
		String strEmailContent = templateUtil.generateOutput();

		log.debug("strEmailContent---> " + strEmailContent);

		HashMap hmEmailProperty = new HashMap();
		hmEmailProperty.put("strFrom", emailProps.getSender());
		hmEmailProperty.put("strTo", emailProps.getRecipients());
		hmEmailProperty.put("strCc", emailProps.getCc());
		hmEmailProperty.put("strSubject", emailProps.getSubject());
		hmEmailProperty.put("strMessage", emailProps.getMessage());
		hmEmailProperty.put("strMessageDesc", strEmailContent);
		hmEmailProperty.put("strMimeType", emailProps.getMimeType());

		return hmEmailProperty;
	}

	/**
	 * fetchSetOutOfPolicyEmailData - will save email info flag/unflag set out policy ids
	 * raise SOP report
	 * 
	 * @author
	 * @param String strSetOutOfPolicyIds , strCompanyInfo
	 * @return String
	 */
	public void saveSetOutOfPolicyEmail(HashMap hmParam) throws AppError {

		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil())
				.populateCompanyInfo(GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO")));
		GmDBManager gmDBManger = new GmDBManager(gmDataStoreVO);

		String strSopIds = GmCommonClass.parseNull((String) hmParam.get("SOPIDS"));
		String strTo = GmCommonClass.parseNull((String) hmParam.get("TO"));
		String strCc = GmCommonClass.parseNull((String) hmParam.get("CC"));
		String strSubject = GmCommonClass.parseNull((String) hmParam.get("SUBJECT"));
		String strMessage = GmCommonClass.parseNull((String) hmParam.get("MESSAGEDESC"));
		String strUserid = GmCommonClass.parseNull((String) hmParam.get("USERID"));

		gmDBManger.setPrepareString("gm_pkg_sop_setoutofpolicy_txn.gm_sav_setoutofpolicy_email", 6);
		gmDBManger.setString(1, strSopIds);
		gmDBManger.setString(2, strTo);
		gmDBManger.setString(3, strCc);
		gmDBManger.setString(4, strSubject);
		gmDBManger.setString(5, strMessage);
		gmDBManger.setString(6, strUserid);
		gmDBManger.execute();
		gmDBManger.commit();

	}

	/**
	 * fetchSetOutOfPolicyEmailData - will fetch email info flag/unflag set out policy ids
	 * raise SOP report
	 * 
	 * @author
	 * @param String strSetOutOfPolicyIds , strCompanyInfo
	 * @return String
	 */
	public ArrayList fetchSetOutOfPolicyEmailData(String strSetOutOfPolicyIds, String strCompanyInfo,String strDateFmt) throws AppError {
		ArrayList alList = new ArrayList();
		GmDataStoreVO gmDataStoreVO = null;
        log.debug("strDateFmt---> "+strDateFmt);
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		gmDBManager.setPrepareString("gm_pkg_sop_setoutofpolicy_rpt.gm_fch_sop_email_data_info", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSetOutOfPolicyIds);
		gmDBManager.setString(2, strDateFmt);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		log.debug("alList========= " + alList);

		gmDBManager.close();
		return alList;
	}

}
