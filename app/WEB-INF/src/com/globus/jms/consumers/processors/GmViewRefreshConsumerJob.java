package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmViewRefreshTransBean;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;

public class GmViewRefreshConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    if (strProcessType.equals("QUARTZJOB")) {
      refreshMaterializeView(null, jobDataMap);
    }
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {

    refreshMaterializeView(objMessageObject, null);
  }

  public void refreshMaterializeView(Object objMessageObject, JobDataMap jobDataMap)
      throws Exception {

    HashMap hmMessageObj = new HashMap();
    hmMessageObj = (HashMap) objMessageObject;
    String strViewName = GmCommonClass.parseNull((String) hmMessageObj.get("M_VIEW"));
    GmViewRefreshTransBean gmViewRefreshTransBean = new GmViewRefreshTransBean();
    System.out.println(" Materialze view update method called ");
    gmViewRefreshTransBean.updateMaterializeView(strViewName);
    System.out.println(" Materialze view update done ");

  }



}
