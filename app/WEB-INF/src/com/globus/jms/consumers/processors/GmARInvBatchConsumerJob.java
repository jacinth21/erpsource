package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.batch.beans.GmARBatchSplitBean;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmURLConnection;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * Class Name: GmARInvBatchConsumerJob.java
 * Description:This class is used to process the batch and create the invoice 
 */

public class GmARInvBatchConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");
  
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    if (strProcessType.equals("QUARTZJOB")) {
      processBatchCommon(null, jobDataMap);
    }
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {

    processBatchCommon(objMessageObject, null);
  }

  public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

    HashMap hmMessageObj = new HashMap();
    HashMap hmParam = new HashMap();
    hmMessageObj = (HashMap) objMessageObject;
    String strSingleBatchID = GmCommonClass.parseNull((String) hmMessageObj.get("BATCHID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmMessageObj.get("companyInfo"));
    String strUserId = GmCommonClass.parseNull((String) hmMessageObj.get("USERID"));
    GmDataStoreVO tmpGmDataStoreVO = null;
    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    GmARBatchSplitBean gmARBatchSplitBean = new GmARBatchSplitBean(tmpGmDataStoreVO);
    
    String strMultiBatchId= "";
    String strErrorCustPos = "";
    String strARBatchId ="";
    String strErrorPOIds = "";
    String strBatchStatus = "18742"; /* Status = Pending processing */
    String strInputStr = "";
   
    String strCompanyLocale = GmCommonClass.getCompanyLocale(tmpGmDataStoreVO.getCmpid());
    // Locale
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

    log.error("strSingleBatchID --> " + strSingleBatchID);
    
    strMultiBatchId = gmARBatchSplitBean.saveARSplitBatch(strSingleBatchID,strUserId); 
    
    log.error("strMultiBatchId --> " + strMultiBatchId);
    
    if(strMultiBatchId.equals("")){
    	strMultiBatchId = strSingleBatchID;
    }
    StringTokenizer strTok = new StringTokenizer(strMultiBatchId, ",");
    String strBatchProcessService =
	        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_PROCESS_SERVICE"));
    String strBatchMDBClass =
	        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_CONSUMER_MDB_CLASS"));
    
    hmParam.put("companyInfo", strCompanyInfo);
    hmParam.put("USERID", strUserId);
    hmParam.put("INVBATCHSTATUS", strBatchStatus);
    hmParam.put("PROCESSSERVICE", strBatchProcessService);
    hmParam.put("MDBCLASS", strBatchMDBClass);
    
    while (strTok.hasMoreTokens()) {
    	strARBatchId = strTok.nextToken();
    	strErrorCustPos = "";
    	hmParam.put("INVBATCHID", strARBatchId);
    	strInputStr = gmARBatchSplitBean.fetchBatchEmailFl(strARBatchId);
    	log.error("strInputStr -- Inside While Loop --> " + strInputStr);
    	hmParam.put("INPUTSTRING", strInputStr);
    	strErrorCustPos = gmARBatchSplitBean.processARSplitBatch(hmParam,strBatchProcessService,strBatchMDBClass); 
    	if(!strErrorCustPos.equals("")){
    		strErrorPOIds = strErrorPOIds + ',' + strErrorCustPos;
    		continue;
    	}
    }
  }

}
