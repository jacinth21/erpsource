package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;
 

/**
 * 
 * @author tramasamy
 * Class Name: GmCutPlanLockAndGenarateConsumerJob.java
 * Description:This class is used to process the Cut plan Analysis lock and generate
 * 
 */
public class GmCutPlanLockAndGenarateConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	/* (non-Javadoc)
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
	 */
	
	 @Override
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		processCutPlanLock(objMessageObject, null);
	}

	/* (non-Javadoc)
	 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
	 */
	
	
	  public void execute(JobDataMap jobDataMap) throws Exception {
	  processCutPlanLock(null, jobDataMap); }
	 
	
	  /**PC_3169
	 * Method - processCutPlanLock used to schedule cut plan lock and generate 
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws AppError
	 */
	
	public void processCutPlanLock(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    log.debug("INSIDE GmCutPlanLockAndGenarateConsumerJob...");
		   
		    HashMap hmParam = (HashMap) objMessageObject; 
		    //Initialize
			String strUserId = "";
			String strCompanyId="";
			String strPlantId= "";
			log.debug("GmCutPlanLockAndGenarateConsumerJob..."+hmParam);
			strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANT_ID"));
			strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));
			strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
			
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo")); 
		   
		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		   
		    GmDBManager gmDBManager = new GmDBManager();
		    gmDBManager.setPrepareString("gm_pkg_op_cut_plan_forecast.gm_create_cut_plan_report",3);
		    gmDBManager.setString(1, strCompanyId);
			gmDBManager.setString(2, strPlantId);
			gmDBManager.setString(3, strUserId);
			gmDBManager.execute();
			gmDBManager.commit();	

	}
}
