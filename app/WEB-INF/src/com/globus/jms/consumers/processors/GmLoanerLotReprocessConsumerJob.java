/**
 * 
 */
package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.loaners.beans.GmLoanerLotReprocessBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author APrasath
 *
 */
public class GmLoanerLotReprocessConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	  @Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  updateLoanerLotDetails(null, jobDataMap);
	  }
	
	  @Override
	  public void processMessage(Object objMessageObject) throws Exception {
		  updateLoanerLotDetails(objMessageObject, null);
	  }
	  
	  
	  /**
	   * updateLoanerLotDetails - The below method to identify the missing lot from process check
	   * 
	   * @param -objMessageObject:it contains CONSUMERCLASS, transaction id, transaction type
	   * @exception - Exception
	   */ 
	  public void updateLoanerLotDetails(Object objMessageObject, JobDataMap jobDataMap)
		      throws Exception {
	       log.debug("GmLoanerLotReprocessConsumerJob TransactionId is ");
		   HashMap hmParam = (HashMap) objMessageObject;  
		   		   
		   String strConsignmentId = GmCommonClass.parseNull((String)hmParam.get("CONSIGNMENTID"));//Transaction ID 
		   String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID")); 
		   String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		   
		   GmDataStoreVO gmDataStoreVO = null;
           gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
           GmLoanerLotReprocessBean GmLoanerLotReprocessBean = new GmLoanerLotReprocessBean(gmDataStoreVO);
		   
		   log.debug("GmLoanerLotReprocessConsumerJob TransactionId is "+strConsignmentId);
		   try{
    		   if(!strConsignmentId.equals("")){
    			   GmLoanerLotReprocessBean.updateLoanerLotDetails(strConsignmentId, strUserId);
    		   }
		   }catch(Exception exe){
             log.error("Exception occured in the class GmLoanerLotReprocessConsumerJob : "  + exe.getMessage());
            }
	  }
}
