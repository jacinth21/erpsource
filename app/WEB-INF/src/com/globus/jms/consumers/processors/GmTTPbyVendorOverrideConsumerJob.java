package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;
 

/**
 * 
 * @author mselvamani
 * Class Name: GmTTPbyVendorOverrideConsumerJob.java
 * Description:This class is used to process the TTP Vendor Override Percentage and QTY
 * 
 */
public class GmTTPbyVendorOverrideConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	/* (non-Javadoc)
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
	 */
	  
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		processOverrideQty(objMessageObject, null);
	}

	/* (non-Javadoc)
	 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
	 */
	
	public void execute(JobDataMap jobDataMap) throws Exception {
		processOverrideQty(null, jobDataMap);      
	}
	
	  /**
	 * Method - processOverrideQty used to process the TTP Vendor Override Percentage and QTY.
	 *    Based on Calculation flag.
	 *    
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws AppError
	 */
	
	public void processOverrideQty(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    log.debug("INSIDE GmTTPbyVendorOverrideConsumerJob...");
		    HashMap hmParam = (HashMap) objMessageObject; 
		    //Initialize
			String strUserId = "";
			String strInputstr = "";
			String strMonth="";
			String strYear="";
			String strOpt= "";
			
			strInputstr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
			strMonth = GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
			strYear = GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));
			String strDate = strMonth.concat("/").concat(strYear);
			strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO")); 
		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
		    gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_process_override_split_qty",	2);
		    gmDBManager.setString(1, strDate);
			gmDBManager.setString(2, strUserId);
			gmDBManager.execute();
			gmDBManager.commit();		   
	}
}
