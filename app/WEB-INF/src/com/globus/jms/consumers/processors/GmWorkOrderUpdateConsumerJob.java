package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.operations.purchasing.beans.GmWorkOrderRevisionBean;

/**
 * @author pvigneshwaran
 * PMT-32340 Work order Update Revision dashboard
 * Class Name:GmWorkOrderUpdateConsumerJob
 *  * This class used to send a email notification to purchasing team. 
    * If quality team modify any of the part attribute 
    *(Revision, product Class, Material specification, Drawing number) - 
    * to send the notification to purchasing team (all the open WO)-Using JMS call
 */
public class GmWorkOrderUpdateConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	  Logger log = GmLogger.getInstance(this.getClass().getName());
	public void processMessage(Object objMessageObject) throws Exception {
		 //TODO Auto-generated method stub
		processWorkOrderUpdate(objMessageObject, null);
	}

	public void execute(JobDataMap jobDataMap) throws Exception {
		// TODO Auto-generated method stub
		processWorkOrderUpdate(null, jobDataMap);
	}
	/**Method - processWorkOrderUpdate used once changed Part number attribute, 
	 *          send a email notification to purchasing team (open work orders).
	 *          Based on strOpt to send email notification/pre-populate data to Revision dashboard
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	public void processWorkOrderUpdate(Object objMessageObject, JobDataMap jobDataMap)
		      throws Exception {
		    log.debug("INSIDE WORK ORDER REVISION CONSUMERJOB...");
		    
		    HashMap hmParam = (HashMap) objMessageObject; 
		 
		   
			String strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
		    String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		    String strPdraw = GmCommonClass.parseNull((String) hmParam.get("PDRAW"));
		    String strPmaterialDesc = GmCommonClass.parseNull((String) hmParam.get("PMATERIALSPEC"));
		    String strPrevNum = GmCommonClass.parseNull((String) hmParam.get("PREVNUM"));
		    String strPclass = GmCommonClass.parseNull((String) hmParam.get("PCLASS"));
		    String strPnum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		    String strPdesc = GmCommonClass.parseNull((String) hmParam.get("PDESC"));
		    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		  
		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    
		    GmWorkOrderRevisionBean gmWorkOrderRevisionBean = new GmWorkOrderRevisionBean(gmDataStoreVO);
		    try{
		    //Based on strOpt to send email notification/pre-populate data to Revision dashboard
		    if(strAction.equals("EMAIL")){
		    gmWorkOrderRevisionBean.sendWorkOrderUpdateEmail(strPdraw,strPmaterialDesc,strPrevNum,strPclass,strPnum,strUserId,strPdesc);   
		    }else{
			  gmWorkOrderRevisionBean.processWoRevision(strStatus,strUserId);
		         }
		    }catch(Exception exe){
			    log.error("Exception occured in the class GmWorkOrderUpdateConsumerJob : "  + exe.getMessage());
		}
	}

}
