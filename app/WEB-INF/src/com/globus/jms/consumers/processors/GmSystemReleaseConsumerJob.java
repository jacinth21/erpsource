package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.prodmgmnt.beans.GmSystemPublishTransBean;

/**GmSystemReleaseConsumerJob:This job Used to  process system relase details
 * @author pvigneshwaran
 *Note:For avoid PMD Report gave comments.
 */
public class GmSystemReleaseConsumerJob extends GmActionJob implements
		GmConsumerDispatcherInterface {
	/**
	 * Log:It is used to track error details
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/* (non-Javadoc)
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
	 */
	/**
	 * Log:It is used to track error details
	 * processMessage:This Method used to call via Java
	 */
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		processSystemRelease(objMessageObject, null);

	}

	/* (non-Javadoc)
	 * @see com.globus.common.util.jobs.GmActionJob#execute(org.quartz.JobDataMap)
	 */
	/**
	 * execute: This method used to call via JOB
	 */
	public void execute(JobDataMap jobDataMap) throws Exception {
		// TODO Auto-generated method stub
		processSystemRelease(null, jobDataMap);
	}

	/**processSystemRelease:This Job used to update system release type using JMS Queue
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	public void processSystemRelease(Object objMessageObject,
			JobDataMap jobDataMap) throws Exception {
		log.debug("INSIDE GmSystemReleaseConsumerJob...");
		HashMap hmParam = (HashMap) objMessageObject;
		// Initialize
		String strSystemId = "";
		String strType = "";
		String strUserId = "";
		String strAction = "";
		
		strSystemId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		strType = GmCommonClass.parseNull((String) hmParam.get("SETGROUPTYPE"));
		strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		//
		GmDataStoreVO gmDataStoreVO = null;
		gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmSystemPublishTransBean gmSystemPublishTransBean = new GmSystemPublishTransBean(gmDataStoreVO);
		//Db Call
		if (strAction.equals("PUBLISH")) { 
			gmSystemPublishTransBean.processSystemPublishDtls(strSystemId); 
		}
		else {
			gmSystemPublishTransBean.processSystemReleaseDtls(strSystemId,strType,strUserId); 
			}
		
	}
}
