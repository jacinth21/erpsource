package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import javax.print.attribute.standard.MediaName;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmProdCatCloudBean;
import com.globus.common.util.GmURLConnection;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.prodmgmnt.beans.GmShareEngineTransBean;

public  class GmMrktCollFileSyncConsumerJob extends GmActionJob implements
		GmConsumerDispatcherInterface {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code.
	
	String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");	
	
	public void execute(JobDataMap jobDataMap ) throws Exception {
		 
		if (strProcessType.equals("QUARTZJOB"))
		{
			processBatchCommon(null,jobDataMap);  
		}
	}
	 
	public void processMessage(Object objMessageObject) throws Exception   {
		    
		processBatchCommon(objMessageObject,null);
	}
	public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
		  
			HashMap hmParam = (HashMap)objMessageObject;
			GmShareEngineTransBean gmShareEngineTransBean = new GmShareEngineTransBean();
			ArrayList alResult = new ArrayList(); 
			
			if(hmParam!=null){
				// PC-4295: spineIT upload details sync to cloud
				String strCloudFileServer = GmCommonClass.parseNull(System.getProperty("CLOUD_FILE_SERVER"));
				strCloudFileServer = strCloudFileServer.equals("")? "AZURE": strCloudFileServer;
				
				// to comment the code - once go live (Azure enable the condition)
				// PC-3005: To remove Egnyte server
				if(strCloudFileServer.equals("EGNYTE")) {
					// saveEgnyteFileSync - is used to save upload files in Egnyte Folder
					gmShareEngineTransBean.saveEgnyteFileSync(hmParam);
				}else {
					GmProdCatCloudBean gmProdCatCloudBean = new GmProdCatCloudBean(GmCommonClass.getDefaultGmDataStoreVO());
					// to set the colud parameter
					hmParam.put("CLOUD_TYPE", 111380); // Azure
					hmParam.put("CLOUD_METHOD_NAME", "common/saveFileDtls");
					gmProdCatCloudBean.syncUploadedFilestoCloud(hmParam);
				}
			
			}
	} 

}
