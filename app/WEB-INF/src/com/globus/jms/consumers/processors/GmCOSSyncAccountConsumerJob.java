package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
 

/**
 * 
 * @author Vinoth
 * Class Name: GmCOSSyncAccountConsumerJob.java
 * Description:This class is used to Sync SpineIt Account to COS 
 * 
 */
public class GmCOSSyncAccountConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	/* (non-Javadoc)
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
	 */
	
	 @Override
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		processSyncAccount(objMessageObject, null);
	}

	/* (non-Javadoc)
	 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
	 */
	
	
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processSyncAccount(null, jobDataMap); }
	 
	
	  /**PC-2169
	 * Method - processSyncAccount used to sadd new part number in cut plan details
	 * 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @return 
	 * @throws AppError
	 */
	
	public String processSyncAccount(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    log.debug("INSIDE processSyncAccount GmCOSSyncAccountConsumerJob...");
		   
		    HashMap hmParam = (HashMap) objMessageObject; 
		    //Initialize
			String strUserId = "";
			String strCompanyId="";
			String strPlantId= "";
			String strPartNum ="";
			String strMsg="";
			
			 log.debug(" GmCOSSyncAccountConsumerJob..." +hmParam);
			strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANT_ID"));
			strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));
			strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
			String strAccountId = GmCommonClass.parseNull((String)hmParam.get("ACCOUNTID"));
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo")); 
		   
		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		    
		    GmDBManager gmDBManager = new GmDBManager();
		    gmDBManager.setPrepareString("gm_pkg_cos_lot_track.gm_sync_cos_order",1);
		    gmDBManager.setString(1, strAccountId);
			gmDBManager.execute();
			gmDBManager.commit();	
			return strMsg;
	}
}

