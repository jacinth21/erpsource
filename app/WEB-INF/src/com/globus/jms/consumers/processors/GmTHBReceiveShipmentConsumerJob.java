package com.globus.jms.consumers.processors;

import java.util.HashMap;
import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.receiving.thbreceiving.beans.GmDonorScheduleBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author JBalaraman
 * 
 */
public class GmTHBReceiveShipmentConsumerJob extends GmActionJob implements
		GmConsumerDispatcherInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	@Override
	  public void execute(JobDataMap jobDataMap) throws Exception {
		  processTHBDonorRS (null, jobDataMap);
	}
	@Override
	  public void processMessage(Object objMessageObject) throws Exception {
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmDonorScheduleBean gmDonorScheduleBean = new GmDonorScheduleBean(gmDataStoreVO);
        processTHBDonorRS(objMessageObject, null);
	}
	public void processTHBDonorRS(Object objMessageObject, JobDataMap jobDataMap)
			throws Exception {
		HashMap hmParam = (HashMap) objMessageObject;
	
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam
				.get("COMPANYINFO"));
		gmDataStoreVO = (gmDataStoreVO != null) ? gmDataStoreVO
				: (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		GmDonorScheduleBean gmDonorScheduleBean = new GmDonorScheduleBean(gmDataStoreVO);
		// Call processTHBLock method when JMS CALL
		log.debug("processTHBDomorRs-GmTHBReceiveShipmentConsumerJob"+hmParam);
		if (hmParam != null) {
			gmDonorScheduleBean.createRS(hmParam);
		}

	}

}
