package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.purchasing.orderplanning.beans.GmOPForecastTransBean;


/**
 * 
 * @author mmuthusamy
 * Class Name: GmTTPApprovalConsumerJob.java
 * Description:This class is used to process the TTP summary details (Approve/Lock)
 * 
 */

public class GmTTPApprovalConsumerJob  extends GmActionJob implements GmConsumerDispatcherInterface {
	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	/* (non-Javadoc)
	 * @see com.globus.jms.consumers.controller.GmConsumerDispatcherInterface#processMessage(java.lang.Object)
	 */
	  
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
	    processTTP(objMessageObject, null);
	}

	/* (non-Javadoc)
	 * @see com.globus.common.util.jobs.GmActionJob#execute(JobDataMap)
	 */
	
	public void execute(JobDataMap jobDataMap) throws Exception {
	    processTTP(null, jobDataMap);      
	}
	
	  /**
	 * Method - processTTP used to process the TTP summary.
	 *    Based on strOpt to Approve/Lock the TTP sheet.
	 *    
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	
	public void processTTP(Object objMessageObject, JobDataMap jobDataMap)
		      throws Exception {
		    log.debug("INSIDE TTPSUMMARYCONSUMERJOB...");
		    
		    HashMap hmParam = (HashMap) objMessageObject; 
		    //Initialize
			String strUserId = "";
			String strLockPeriod = "";
			String strMonth="";
			String strYear="";
			String strOpt= "";
			
			strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
			strMonth=GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
		    strYear=GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));	
		    strOpt=GmCommonClass.parseNull((String) hmParam.get("STROPT"));	
			strLockPeriod = strMonth.concat("/").concat(strYear);
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		  

		    GmDataStoreVO gmDataStoreVO = null;
		    gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);

	        // PMT-32804 : TTP Summary lock and generate
		    //Call ProcessTTPApproval method when JMS CALL
		    //Based on strOpt to call the different method (Approve/Lock)
		    
		    GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean(gmDataStoreVO);
		    
		    if(strOpt.equalsIgnoreCase("Lock")){
		    	gmOPTTPSetupBean.processTTPLockDetailsJMS(hmParam);
		    }else
		    {
		    	gmOPTTPSetupBean.processTTPApproval(strUserId,strLockPeriod);
		    }

		  }

}
