package com.globus.jms.consumers.processors;

import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.ttpbyvendor.beans.GmTTPVendorPOTransBean;
import com.globus.quality.beans.GmPartNumberAssemblyBean;
import com.globus.valueobject.common.GmDataStoreVO;


/**PMT- 50777 
 * GmBOMDetailsConsumerJob - this class used to process the BOM creation 
 *                                    
 * @author rajan
 *
 */
public class GmBOMDetailsConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	
	/**
	 * log Code to Initialize the Logger Class
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

    /**
     * this method start to job execution 	
     */
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {
		saveBOMPartDetails(null, jobDataMap);
	}
	
   /**
   * processMessage - used to call the saveBOMPartDetails
   */
	@Override
	public void processMessage(Object objMessageObject) throws Exception {
		saveBOMPartDetails(objMessageObject, null);
	}

	/**
	 * Method - saveBOMPartDetails used to BOM creation
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	public void saveBOMPartDetails(Object objMessageObject, JobDataMap jobDataMap) throws Exception {
	    log.debug("INSIDE PROCESS BOM CRERATION...");
		HashMap hmParam = (HashMap) objMessageObject;
		// Initialize
		String strUserId = "";
		String strBOMId = "";
		
		strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
		strBOMId = GmCommonClass.parseNull((String) hmParam.get("BOMID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		
		GmDataStoreVO gmTempDataStoreVO = null;
		gmTempDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
		
		GmPartNumberAssemblyBean gmPartNumberAssemblyBean = new GmPartNumberAssemblyBean(gmTempDataStoreVO);
		
		gmPartNumberAssemblyBean.saveBeanBOMPartDetails(strBOMId, strUserId);
	}
}
