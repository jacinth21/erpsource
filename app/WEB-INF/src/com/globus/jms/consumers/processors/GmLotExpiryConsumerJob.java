package com.globus.jms.consumers.processors;
import java.util.HashMap;
import java.io.File;

import org.apache.log4j.Logger;
import com.globus.jms.consumers.beans.GmLotBulkExtBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;

import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;
import org.quartz.JobDataMap;

import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;
/**
 * This method will pass information using JMS
 * @author pvigneshwaran
 *
 */

public class GmLotExpiryConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {
	  // TODO Auto-generated constructor stub
    Logger log = GmLogger.getInstance(this.getClass().getName());
	public void processMessage(Object objMessageObject) throws Exception {
		// TODO Auto-generated method stub
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		GmLotBulkExtBean gmLotBulkExtBean = new GmLotBulkExtBean(gmDataStoreVO);
	    log.debug("In JMS Message execute process Message method");

		gmLotBulkExtBean.SaveLotExpiry(objMessageObject,null);
	}

	public void execute(JobDataMap jobDataMap) throws Exception {
	    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
	    log.debug("In JMS Message execute method");
	      HashMap hmParams = new HashMap();
	      HashMap hmMessageObj = new HashMap();
	      GmLotBulkExtBean gmLotBulkExtBean = new GmLotBulkExtBean(gmDataStoreVO);
	      hmParams.put("GMDATASTOREVO", gmDataStoreVO);
		    log.debug("gmDataStoreVO"+gmDataStoreVO);

	      hmMessageObj.put("HMHOLDVALUES", hmParams);
	      log.debug("hmParams"+hmParams);

	      gmLotBulkExtBean.SaveLotExpiry(hmMessageObj, jobDataMap);	// TODO Auto-generated method stub
		
	}

}
