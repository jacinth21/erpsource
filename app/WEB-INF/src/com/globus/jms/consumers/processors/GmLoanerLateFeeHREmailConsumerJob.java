package com.globus.jms.consumers.processors;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;

import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.operations.logistics.beans.GmLoanerLateFeeHREmailBean;
import com.globus.operations.purchasing.beans.GmDHREmailBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerLateFeeHREmailConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

Logger log = GmLogger.getInstance(this.getClass().getName());



@Override
public void execute(JobDataMap jobDataMap) throws Exception {
	processLateFeeHREmail(null, jobDataMap);
	
}

@Override
public void processMessage(Object objMessageObject) throws Exception {
	processLateFeeHREmail(objMessageObject,null);
	
}

/**
 * processLateFeeHREmail - to send HR email direct and dist reps credit and submitted details
 * 
 * @param -objMessageObject:it contains CONSUMERCLASS
 * @exception - Exception
 */
public void processLateFeeHREmail(Object objMessageObject, JobDataMap jobDataMap)
        throws Exception {
	
	
	HashMap hmParam = (HashMap) objMessageObject;
	      log.debug("consumer job --> hmParam-------------> "+hmParam);
	      HashMap hmValParam = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMVALUES"));
	      String strCompanyInfo = GmCommonClass.parseNull((String) hmValParam.get("COMPANYINFO"));
	      String strChargeDtlIds= GmCommonClass.parseNull((String) hmValParam.get("INPUTSTRING"));
	      // data store VO null then populate the Company Info
	      GmDataStoreVO gmDataStoreVO = new GmWSUtil().populateCompanyInfo(strCompanyInfo);
	      GmLoanerLateFeeHREmailBean gmLoanerLateFeeHREmailBean = new GmLoanerLateFeeHREmailBean(gmDataStoreVO);
//	       Direct Rep LateFee Submitted
	      gmLoanerLateFeeHREmailBean.sendDirectRepLateFeeHREmail(strChargeDtlIds);
	      
//	  		Dist rep Late fee submitted
	      gmLoanerLateFeeHREmailBean.sendDistRepLateFeeHREmail(strChargeDtlIds);

//	       Direct rep Credit
	      gmLoanerLateFeeHREmailBean.sendDirectRepCreditHREmail(strChargeDtlIds);

//	       Dist rep Credit
	      gmLoanerLateFeeHREmailBean.sendDisttRepCreditHREmail(strChargeDtlIds);

	      }





}

