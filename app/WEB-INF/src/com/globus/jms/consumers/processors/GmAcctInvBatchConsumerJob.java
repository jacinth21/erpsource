package com.globus.jms.consumers.processors;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmURLConnection;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.jms.consumers.controller.GmConsumerDispatcherInterface;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAcctInvBatchConsumerJob extends GmActionJob implements GmConsumerDispatcherInterface {

  String strProcessType = GmCommonClass.getString("BATCH_PROCESS_SERVICE");
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {

    if (strProcessType.equals("QUARTZJOB")) {
      processBatchCommon(null, jobDataMap);
    }
  }

  @Override
  public void processMessage(Object objMessageObject) throws Exception {

    processBatchCommon(objMessageObject, null);
  }

  public void processBatchCommon(Object objMessageObject, JobDataMap jobDataMap) throws Exception {

    HashMap hmMessageObj = new HashMap();
    hmMessageObj = (HashMap) objMessageObject;
    String strBatchID = GmCommonClass.parseNull((String) hmMessageObj.get("BATCHID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmMessageObj.get("companyInfo"));
    GmDataStoreVO tmpGmDataStoreVO = null;
    tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    System.out.println("hmMessageObj --> " + hmMessageObj);
    log.error("hmMessageObj --> " + hmMessageObj);
    String resourcename = "GmInvoiceInfoServlet";
    GmARBatchBean gmARBatchBean = new GmARBatchBean(tmpGmDataStoreVO);
    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean();
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList(gmARBatchBean.fetchBatchInvoiceDtls(strBatchID));
//    log.error("alResult --> " + alResult);
    log.error("alResult --> " + alResult.size());
    String strInvID = "";
    String strEmailReq = "";
    String strEversion = "";
    String strPara = "&hAction=Print&type=savePDFfile";
    String strAddtlPara = "";
    String strLogType = "";
    String strLogInvID = "";
    String strRefType = "";
    String strRefID = "";
    String strTempInvID = "";

    int intArLength = alResult.size();

    HashMap hmLog = new HashMap();
    hmLog.put("REFTYPE", "18751");
    hmLog.put("BATCHID", strBatchID);
    ArrayList alReturnLog = gmARBatchBean.fetchLogDtls(hmLog);
    log.error("alReturnLog --> " + alReturnLog.size());
    ArrayList alReturnUpload = gmARBatchBean.fetchUploadDtls(hmLog);
    log.error("alReturnUpload --> " + alReturnUpload.size());
    String strCompanyLocale = GmCommonClass.getCompanyLocale(tmpGmDataStoreVO.getCmpid());
    // Locale
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

    if (intArLength > 0) {
      HashMap hmLoop = new HashMap();
      String strInvoicePDF = GmCommonClass.getString("BATCH_INVOICE");
      String year = GmCalenderOperations.getCurrentDate("yyyy");

      GmCommonClass gmCommonClass = new GmCommonClass();

      GmBatchBean gmBatchBean = new GmBatchBean();

      HashMap hmCompanyInvoice = new HashMap();

      for (int i = 0; i < intArLength; i++) {
        hmLoop = (HashMap) alResult.get(i);
        strBatchID = GmCommonClass.parseNull((String) hmLoop.get("BATCHID"));
        strInvID = GmCommonClass.parseNull((String) hmLoop.get("INVID"));
        strEversion = GmCommonClass.parseNull((String) hmLoop.get("EVERSION")); // 91994 PDF, 91995
                                                                                // CSV, 91996 PDF
                                                                                // and CSV
        strEmailReq = GmCommonClass.parseNull((String) hmLoop.get("BATEMAILREQ"));
        // Company information
        hmCompanyInvoice = gmInvoiceBean.fetchCompanyInvoiceDtls(strInvID);
        String strInvoiceCompanyId =
            GmCommonClass.parseNull((String) hmCompanyInvoice.get("COMPANY_ID"));
        String strCompanyCode = GmCommonClass.parseNull((String) hmCompanyInvoice.get("COMP_CODE"));

        GmURLConnection gmURLConnection = new GmURLConnection();
        strAddtlPara =
            strAddtlPara + "&hInv=" + strInvID + "&hEmailReq=" + strEmailReq + "&hEversion="
                + strEversion + "&hBatchID=" + strBatchID;

        hmLoop.put("RESOURCE", resourcename);
        hmLoop.put("PARAMETER", strPara + strAddtlPara);
        hmLoop.put("YEAR", year);
        hmLoop.put("companyInfo", strCompanyInfo);

        strAddtlPara = "";

        boolean bPDFFileCreated = false;
        boolean bCSVFileCreated = false;
        String strPDFUpload = "";
        String strCSVUpload = "";


        boolean bFile = false;
        boolean bEmail = false;
        boolean bPrint = false;
        if (strEmailReq.equals("Y")) {
          bPrint = true;
        }

        String pdffileName =
            strInvoicePDF + strCompanyCode + "\\" + year + "\\" + strInvID + ".pdf";
        String csvfileName =
            strInvoicePDF + strCompanyCode + "\\" + year + "\\" + strInvID + ".csv";

        hmLoop.put("PDFFILENAME", pdffileName);
        hmLoop.put("CSVFILENAME", csvfileName);
        hmLoop.put("COMPANY_CODE", strCompanyCode);
        hmLoop.putAll(hmCompanyInvoice);


        if (!strInvID.equals(strTempInvID)) {
        	log.error("strInvID -- Inside If stmt --> " + strInvID);
          // for loop to check if log record created for invoice
          for (int j = 0; j < alReturnLog.size(); j++) {
            HashMap hmRecord = (HashMap) alReturnLog.get(j);

            strLogInvID = gmCommonClass.parseNull((String) hmRecord.get("INVID"));
            if (strInvID.equals(strLogInvID)) {
              strLogType = gmCommonClass.parseNull((String) hmRecord.get("ACTIONTYPE"));

              if (strLogType.equals("1801"))
                bFile = true; // 1801 file
              if (strLogType.equals("1802"))
                bPrint = true; // 1802 print
              if (strLogType.equals("1803"))
                bEmail = true; // 1803 email
            }
          }
          // for loop to check if upload record created for invoice
          for (int k = 0; k < alReturnUpload.size(); k++) {
            HashMap hmRecord = (HashMap) alReturnUpload.get(k);

            strRefID = gmCommonClass.parseNull((String) hmRecord.get("INVID"));
            if (strInvID.equals(strRefID)) {
              strRefType = gmCommonClass.parseNull((String) hmRecord.get("REFTYPE"));

              if (strRefType.equals("90906"))
                strPDFUpload = "YES"; // 90906 pdf file
              if (strRefType.equals("90907"))
                strCSVUpload = "YES"; // 90907 csv file
            }
          }
          hmLoop.put("PDFUPLOAD", strPDFUpload);
          hmLoop.put("CSVUPLOAD", strCSVUpload);


          bPDFFileCreated = gmARBatchBean.saveInvoiceAsPDF(hmLoop);
          log.error("bPDFFileCreated ----> " + bPDFFileCreated);
          
          hmLoop.put("REFTYPE", "18751"); // Invoice Batch
          if (!bFile) {
            hmLoop.put("ACTIONTYPE", "1801"); // 1801 file
            gmBatchBean.saveBatchLog(hmLoop);
          }

          String strGenerateXmlFl =
              GmCommonClass.parseNull((String) hmCompanyInvoice.get("GENERATE_XML"));
          // if Company is creating the XML file
          if (strGenerateXmlFl.equals("Y")) {
            GmInvoiceXmlInterface gmInvoiceXmlInterface =
                (GmInvoiceXmlInterface) GmCommonClass.getSpringBeanClass(
                    "xml/Invoice_Xml_Beans.xml", tmpGmDataStoreVO.getCmpid());

            gmInvoiceXmlInterface.generateInvocieXML(strInvID, (String) hmLoop.get("USERID"));
          }
          if (strEmailReq.equals("Y")) {
            if (strEversion.equals("91995") || strEversion.equals("91996"))// 91994 PDF, 91995 CSV,
                                                                           // 91996 PDF and CSV
            {
              bCSVFileCreated = gmARBatchBean.saveInvoiceAsCSV(hmLoop);
            }
            if ((bPDFFileCreated || bCSVFileCreated) && !bEmail) {
              gmARBatchBean.sendEmail(hmLoop);
              hmLoop.put("ACTIONTYPE", "1803"); // 1803 email
              gmBatchBean.saveBatchLog(hmLoop);
            }
          }
          log.error("bPrint ----> " + bPrint);
          if (!bPrint) {
            hmLoop.put("PDFFILENAME", pdffileName);
            gmARBatchBean.print(hmLoop);
            hmLoop.put("ACTIONTYPE", "1802"); // 1802 print
            gmBatchBean.saveBatchLog(hmLoop);
          }

          strTempInvID = strInvID;
        }
      }
      HashMap hmStatus = new HashMap();
      hmStatus.put("BATCHID", strBatchID);
      gmBatchBean.saveBatchStatus(hmStatus);

    }
  }


}
