/**
 * 
 */
package com.globus.jms.consumers.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sswetha
 *
 */
public class GmOrderSplitTransBean implements GmSplitTransInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * validateTransaction - This method will validate the input order id  by validating the order type and the plant for PMT-33510
	 * 
	 * @param strOrderId
	 * 	 * @throws AppError
	 */
	public String validateTransaction(String strOrderId) {
		// TODO Auto-generated method stub
	    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		String strValidateOrder = gmTransSplitBean.orderValidateTransaction(strOrderId);
		return strValidateOrder;
	}

	/**
	 * processTransaction - This method will Process the input order id and split the order by creating transactions based on the building count for PMT-33510
	 * 
	 * @param strOrderId
	 * 	 * @throws AppError
	 */
	public Void processTransaction(String strOrderId) {
		// TODO Auto-generated method stub
	    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		gmTransSplitBean.orderProcessTransaction(strOrderId);
		//PMT-47807-allocate the priority for the transaction in device
		gmTransSplitBean.allocateOrderPriority(strOrderId);
		return null;
	}
	
	
}
