package com.globus.jms.consumers.beans;

import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmNotificationManager;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;
import com.globus.sales.pricing.beans.GmPriceRequestTransBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPricingNotificationBean implements GmNotificationInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	@Override
	public void processNotificationRequest(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    log.debug("inside Pricing GmPricingNotification..----------------------------------------");
		    GmNotificationManager gmNotificationManager = new GmNotificationManager();		   
		    HashMap hmParam = (HashMap) objMessageObject;
		    // Send the notification to the device
		    log.debug("inside Pricing GmPricingNotification..Device ID and message"+hmParam);
		    if (hmParam.size() > 0){
			    String strDeviceId = GmCommonClass.parseNull((String) hmParam.get("DEVICEID"));
			    String strMessage = GmCommonClass.parseNull((String) hmParam.get("MESSAGE"));		
			    gmNotificationManager.sendNotification(strDeviceId, strMessage);
		    }

		  }

}
