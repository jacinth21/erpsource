/**
 * 
 */
package com.globus.jms.consumers.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;

/**
 * @author sswetha
 *
 */
public class GmInhouse412SplitTransBean implements GmSplitTransInterface {
	

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**
	 * validateTransaction - This method will validate the input FGRW, FGIA, FGIH, FGLE, FGBL, FGRM, FGLN, FGIS, FGLT/FGRP Inhouse transaction id by validating the Inhouse transactions type and the plant for PMT-33513 
	 * 
	 * @param strInhouseId
	 * 	 * @throws AppError
	 */
	public String validateTransaction(String strInhouseTxnId) {
		
		GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		String strValidateInhouseId = gmTransSplitBean.inhouse412ValidateTransaction(strInhouseTxnId);
		return strValidateInhouseId;
	}

	
	/**
	 * processTransaction - This method will Process the input FGRW, FGIA, FGIH, FGLE, FGBL, FGRM, FGLN, FGIS, FGLT/FGRP Inhouse transaction id and split the Inhouse transactions by creating transactions based on the building count for PMT-33513 
	 * 
	 * @param strInhouseId
	 * 	 * @throws AppError
	 */
	public Void processTransaction(String strInhouseTxnId) {
		
		GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		gmTransSplitBean.inhouse412ProcessTransaction(strInhouseTxnId);
		return null;
	}

}
