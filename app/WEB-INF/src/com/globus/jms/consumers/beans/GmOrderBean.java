package com.globus.jms.consumers.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOrderBean  extends GmBean{

    Logger log = GmLogger.getInstance(this.getClass().getName());

    GmCommonClass gmCommon = new GmCommonClass();

    /**
     * Constructor will populate company info.
     * 
     * @param gmDataStore
     */
    public GmOrderBean(GmDataStoreVO gmDataStore) {
      super(gmDataStore);
    }
    
    
    /**
     * UpdateHoldOrderStatus - This method will form the message and send through JMS to Upadate the hold status 
     * for the Orders
     * 
     * @param - HashMap:hmHoldValParam
     * @return - Void
     * @exception - AppError
     */
    public void updateHoldOrderStatus(HashMap hmHoldValParam) throws AppError {

      HashMap hmParam = new HashMap();
      HashMap hmMailParams = new HashMap();
      ArrayList alMailParams = new ArrayList();
      log.debug( " HM Hold Order values :: "+hmHoldValParam);
      GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
      String strOrderId = GmCommonClass.parseNull((String)hmHoldValParam.get("ORDERID"));
      
      try{
     // updating hold process not yet completed
      updateOrderHoldProcess(hmHoldValParam);
      
      String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_HOLD_ORDER_QUEUE"));
    
      hmParam.put("QUEUE_NAME", strQueueName);
    
      // ORDER_CONSUMER_CLASS is mentioned in JMSConsumerConfig. properties file.This key contain the
      // path for : com.globus.jms.consumers.processors.GmOrderConsumerJob
      String strConsumerClass =
          GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("HOLD_ORDER_CONSUMER_CLASS"));
      hmParam.put("ORDERID",strOrderId);
      hmParam.put("COMPANY_ID", getCompId());
      hmParam.put("HMHOLDVALUES", hmHoldValParam);
      hmParam.put("CONSUMERCLASS", strConsumerClass);

      gmConsumerUtil.sendMessage(hmParam);
      }
      catch(Exception ex){
       //getting mail information
        hmMailParams.put("ORDERID", strOrderId);
        hmMailParams.put("CONSUMERCLASS", "com.globus.jms.consumers.beans.GmOrderBean");
        hmMailParams.put("HEADERPARAM", "GmOrderBean");
        hmMailParams.put("COMPANY_ID", getCompId());
        hmMailParams.put("JMSCONSUMEREXCEPTION", GmCommonClass.getExceptionStackTrace(ex, "<br>"));
        hmMailParams.put("EXP_MAIL_TO", GmCommonClass.getRuleValue("HELD_ORDER_NOTIFY", "JMS_EXCEP_NOTIFY"));//mail id from rule table
        alMailParams.add(hmMailParams);

          try {
            GmCommonClass
                .sendEmailFromTemplate("GmJMSOrderConsumerExceptionMail", alMailParams, hmMailParams);
            log.error(" Exception occured in the class GmOrderBean : "  + ex.getMessage());
          } catch (Exception e) {
            log.error("Mail not sent exception occured in GmOrderBean: "  + e.getMessage());
            
          }
      }

    }
   
    /**
     * saveOrderHold - This method is used to Update the hold status 
     * for the Orders
     * 
     * @param - HashMap:hmParam
     * @return - Void
     * @exception - AppError
     */
    public void saveOrderHold(HashMap hmParam) throws AppError {
      
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
      GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
      
      String strOrderID = "";
      String strSpecificComments = "";
      String strIPADComments = GmCommonClass.parseNull((String)  hmParam.get("IPADCOMMENTS"));
      String strTransId = GmCommonClass.parseNull((String)  hmParam.get("ORDERID"));
      ArrayList alResult = new ArrayList();
      HashMap hmParamVal = new HashMap();
      
      //below code is for fetching all the child orders, including Parent Order
      gmDBManager.setPrepareString("gm_pkg_op_order_rpt.gm_fch_all_orders", 2);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.setString(1, strTransId);
      gmDBManager.execute();
      alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
      gmDBManager.close();
      //looping all the orders to update order hold status if any price discrepancy
      int intAlSize = alResult.size();
      HashMap hmValues = new HashMap();
          for (int i=0;i<intAlSize;i++)
          {
              hmValues = GmCommonClass.parseNullHashMap((HashMap)alResult.get(i));
              //get the order id from arraylist and put into the hashmap
              strOrderID = GmCommonClass.parseNull((String)hmValues.get("ORDERID"));
              //child order comments from query
              strSpecificComments = GmCommonClass.parseNull((String)hmValues.get("SPECIFIC_COMMENTS"));//child order comments
              strSpecificComments = strSpecificComments.equals("")?strIPADComments:strSpecificComments;//Checking there is any IPAD Comments
          
              hmParamVal.putAll(hmParam);//for additional parameters
              hmParamVal.put("ORDERID", strOrderID);
              hmParamVal.put("SPECIFIC_COMMENTS", strSpecificComments);
              //to update order hold status
              gmCustomerBean.updateHoldStatus(hmParamVal);
          }
      
     }
    /**
     * updateOrderHoldProcess - This method is used to Update the hold process not yet completed 
     * for the Orders
     * 
     * @param - HashMap:hmParam
     * @return - Void
     * @exception - AppError
     */ 
    public void updateOrderHoldProcess(HashMap hmParam) throws AppError {
      
      String strOrderId = GmCommonClass.parseNull((String)hmParam.get("ORDERID"));
      String strUserId =  GmCommonClass.parseNull((String)hmParam.get("USERID"));
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());    
      gmDBManager.setPrepareString("gm_pkg_op_order_txn.gm_upd_process_hold", 2);
      gmDBManager.setString(1, strOrderId); 
      gmDBManager.setString(2, strUserId); 
      gmDBManager.execute();
      gmDBManager.commit();
      
    }
    /**
     * fetchUnprocessedHoldOrders - This method is used to fetch all the unprocessed hold orders
     * 
     * 
     * @param - 
     * @return - ArrayList
     * @exception - AppError
     */  
    public ArrayList fetchUnprocessedHoldOrders() throws AppError {
      ArrayList alResult = new ArrayList();
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      gmDBManager.setPrepareString("gm_pkg_op_order_rpt.gm_fch_unprocessed_hold_orders", 1);
      gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
      gmDBManager.execute();
      alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
      gmDBManager.close();
      return alResult;
    }
    
  }


