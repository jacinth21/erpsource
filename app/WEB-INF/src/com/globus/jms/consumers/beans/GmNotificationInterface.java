package com.globus.jms.consumers.beans;

import org.quartz.JobDataMap;
import com.globus.common.beans.AppError;

public interface GmNotificationInterface {
	public void processNotificationRequest (Object objMessageObject, JobDataMap jobDataMap) throws AppError;
}




