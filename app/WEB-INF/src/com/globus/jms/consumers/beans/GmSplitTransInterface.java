/**
 * 
 */
package com.globus.jms.consumers.beans;

import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;

/**
 * @author sswetha
 *
 */
public interface GmSplitTransInterface {
	/**
	 * validateTransaction - This method will validate the input order id  by validating the order type and the plant for PMT-33510
	 * 
	 * @param strOrderId
	 * 	 * @throws AppError
	 */
	public String validateTransaction(String strOrderId) throws AppError;
	
	/**
	 * processTransaction - This method will Process the input order id and split the order by creating transactions based on the building count for PMT-33510
	 * 
	 * @param strOrderId
	 * 	 * @throws AppError
	 */
	public Void processTransaction(String strOrderId) throws AppError;

}
