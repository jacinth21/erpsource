/**
 * 
 */
package com.globus.jms.consumers.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * @author sswetha
 *
 */
public class GmTransSplitBean extends GmBean {	

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	public GmTransSplitBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	
	public GmTransSplitBean(GmDataStoreVO gmDataStore) {
	      super(gmDataStore);
	    }
	
	/**
	 * updateStorageBuildingInfo - this method will call the JMS Storage building consumer class and send exception when JMS exception occurs For PMT-33510
	 * 
	 * @param hmStorageValParam
	 * 	 * @throws AppError
	 */
	public void updateStorageBuildingInfo(HashMap hmStorageValParam) throws AppError {
		
	      HashMap hmParam = new HashMap();
	      HashMap hmMailParams = new HashMap();
	      ArrayList alMailParams = new ArrayList();
	      log.debug( "  HM Storage Building Param values :: "+hmStorageValParam);
	      GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
	      String strTransactionId = GmCommonClass.parseNull((String)hmStorageValParam.get("TXNID"));
	      String strTransactionType = GmCommonClass.parseNull((String)hmStorageValParam.get("TXNTYPE"));
	      
	      try{
	     
	      String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_STORAGE_BUILDING_QUEUE"));
	      hmParam.put("QUEUE_NAME", strQueueName);
	    
	      // STORAGE_BUILDING_CONSUMER_CLASS is mentioned in JMSConsumerConfig. properties file.This key contain the
	      // path for : com.globus.jms.consumers.processors.GmStorageBuildConsumerJob
	      String strConsumerClass =
	          GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("STORAGE_BUILDING_CONSUMER_CLASS"));
	      hmParam.put("TRANSID",strTransactionId);
	      hmParam.put("TRANSTYPE",strTransactionType);
	      hmParam.put("CONSUMERCLASS", strConsumerClass);
	      gmConsumerUtil.sendMessage(hmParam);
	      }
	      catch(Exception ex){
	       //getting mail information
	        hmMailParams.put("ORDERID", strTransactionId);
	        hmMailParams.put("CONSUMERCLASS", "com.globus.jms.consumers.beans.GmTransSplitBean");
	        hmMailParams.put("HEADERPARAM", "GmTransSplitBean");
	        hmMailParams.put("COMPANY_ID", getCompId());
	        hmMailParams.put("JMSCONSUMEREXCEPTION", GmCommonClass.getExceptionStackTrace(ex, "<br>"));
	        hmMailParams.put("EXP_MAIL_TO", GmCommonClass.getRuleValue("ORDER_SPLIT_NOTIFY", "JMS_EXCEP_NOTIFY"));//mail id from rule table
	        alMailParams.add(hmMailParams);

	          try {
	            GmCommonClass
	                .sendEmailFromTemplate("GmJMSOrderConsumerExceptionMail", alMailParams, hmMailParams);
	            log.error(" Exception occured in the class GmTransSplitBean : "  + ex.getMessage());
	          } catch (Exception e) {
	            log.error("Mail not sent exception occured in GmTransSplitBean: "  + e.getMessage());
	            
	          }
	      }

}
	
	/**
	 * orderValidateTransaction - This method will validate the order based on order type and plant For PMT-33510
	 * 
	 * @param strOrderId
	 * @return String
	 * 	 * @throws AppError
	 */
	public String orderValidateTransaction(String strOrderId) throws AppError {
		
		String strValidateOrder = "";
		String strinputOrder = GmCommonClass.parseNull(strOrderId);
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_storage_building.gm_validate_order_transaction", 2);
	    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
	    gmDBManager.setString(1, strinputOrder);
	    
	    gmDBManager.execute();
	    strValidateOrder = GmCommonClass.parseNull(gmDBManager.getString(2));
	    gmDBManager.close();


		return strValidateOrder;
	}
		
	/**
	   * orderProcessTransaction - This method is used to process the input order id by getting the storage building id's
	   *  and splitting the orders based on the parts available in the building id's For PMT-33510
	   * 
	   * @param strOrderId
	   *  * @throws AppError
	   */
	
	public void orderProcessTransaction(String strOrderId) throws AppError{

	    String strinputOrder = GmCommonClass.parseNull(strOrderId);

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_storage_building.gm_order_process_transaction", 1);
	    gmDBManager.setString(1, strinputOrder);
	    
	    gmDBManager.execute();
	    gmDBManager.commit();
	    gmDBManager.close();
	     
	 }
	
	/**
	   * getOrderID - This method is will get the order id for the passed return id  For PMT-33510
	   * 
	   * @param strOrderId
	   * @param count
	   *  * @throws AppError
	   */
	public String getOrderID(String strRAID) {
		
		String strOrderId = "";
		String strReturnId = GmCommonClass.parseNull(strRAID);
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setFunctionString("gm_pkg_op_storage_building.get_order_info", 1);
	    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	    gmDBManager.setString(2, strReturnId);
	    gmDBManager.execute();
	     
	    strOrderId = GmCommonClass.parseNull(gmDBManager.getString(1));
	    gmDBManager.close();

        return strOrderId;
		
	}
	
	/**
	   * consignmentValidateTransaction - TThis method will validate the consignment based on consignment type and plant For PMT-33511
	   * 
	   * @param strConsignmentId
	   * @return String
	   *  * @throws AppError
	   */
	public String consignmentValidateTransaction(String strConsignmentId)  {
		
		String strValidateConsignment = "";
		String strinputConsignment = GmCommonClass.parseNull(strConsignmentId);
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_storage_building_cn.gm_validate_consignment_transaction", 2);
	    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
	    gmDBManager.setString(1, strinputConsignment);
	    
	    gmDBManager.execute();
	    strValidateConsignment = GmCommonClass.parseNull(gmDBManager.getString(2));
	    gmDBManager.close();


		return strValidateConsignment;
		
	 }
	
	/**
	   * consignmentProcessTransaction - This method is used to process the input consignment id by getting the storage building id's
	   *  and splitting the consignment based on the parts available in the building id's For PMT-33511
	   * 
	   * @param strConsignmentId
	   *  * @throws AppError
	   */
   public void consignmentProcessTransaction(String strConsignmentId)  {
	   
	    String strinputConsignment = GmCommonClass.parseNull(strConsignmentId);

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_storage_building_cn.gm_cn_process_transaction", 1);
	    gmDBManager.setString(1, strinputConsignment);
	    
	    gmDBManager.execute();
	    gmDBManager.commit();
	    gmDBManager.close();
		
	 }
   
   
   /**
	   * inhouse504ValidateTransaction - This method will validate the Inhouse transaction ID(FGQN, FGPN, FGRT) based on transaction type and plant For PMT-33513
	   * 
	   * @param strInhouseId
	   * @return String
	   *  * @throws AppError
	   */
	public String inhouse504ValidateTransaction(String strInhouseTxnId)  {
		
		String strValidateInhouseId = "";
		String strinputInhouseTxnId = GmCommonClass.parseNull(strInhouseTxnId);
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_storage_building_ih.gm_validate_inhouset504_transaction", 2);
	    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
	    gmDBManager.setString(1, strinputInhouseTxnId);
	    
	    gmDBManager.execute();
	    strValidateInhouseId = GmCommonClass.parseNull(gmDBManager.getString(2));
	    gmDBManager.close();


		return strValidateInhouseId;
		
	 }
	
	
	/**
	   * inhouse504ProcessTransaction - This method is used to process the input Inhouse transaction id (FGQN, FGPN, FGRT) by getting the storage building id's
	   *  and splitting the Inhouse transactions based on the parts available in the building id's For PMT-33513
	   * 
	   * @param strInhouseId
	   *  * @throws AppError
	   */
   public void inhouse504ProcessTransaction(String strInhouseTxnId)  {
	   
	    String strinputInhouseTxnId = GmCommonClass.parseNull(strInhouseTxnId);
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_storage_building_ih.gm_inhouset504_process_transaction", 1);
	    gmDBManager.setString(1, strinputInhouseTxnId);
	    
	    gmDBManager.execute();
	    gmDBManager.commit();
	    gmDBManager.close();
		
	 }
   
   

/**
 * inhouse412ProcessTransaction - This method will validate the Inhouse transaction ID(FGRW, FGIA, FGIH, FGLE, FGBL, FGRM, FGLN, FGIS, FGLT/FGRP) based on transaction type and plant For PMT-33513
 * 
 * @param strInhouseId
 * @return String
 *  * @throws AppError
 */
  public String inhouse412ValidateTransaction(String strInhouseTxnId)  {
	
	String strValidateInhouseId = "";
	String strinputInhouseTxnId = GmCommonClass.parseNull(strInhouseTxnId);
	
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_storage_building_ih.gm_validate_inhouset412_transaction", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strinputInhouseTxnId);
  
    gmDBManager.execute();
    strValidateInhouseId = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();


	return strValidateInhouseId;
	
}
  

/**
 * inhouse412ProcessTransaction - This method is used to process the input Inhouse transaction id (FGRW, FGIA, FGIH, FGLE, FGBL, FGRM, FGLN, FGIS, FGLT/FGRP) by getting the storage building id's
 *  and splitting the Inhouse transactions based on the parts available in the building id's For PMT-33513
 * 
 * @param strInhouseId
 *  * @throws AppError
 */
  public void inhouse412ProcessTransaction(String strInhouseTxnId)  {

   String strinputInhouseTxnId = GmCommonClass.parseNull(strInhouseTxnId);

   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
   gmDBManager.setPrepareString("gm_pkg_op_storage_building_ih.gm_inhouset412_process_transaction", 1);
   gmDBManager.setString(1, strinputInhouseTxnId);
  
   gmDBManager.execute();
   gmDBManager.commit();
   gmDBManager.close();
	
}
  	
  /**
   * //PMT-47807-allocate the priority for the transaction in device
   * 
   * @param strOrderId,strUserId
   *  * @throws AppError
   */
    public void allocateOrderPriority(String strOrderId)  {
     GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
     gmDBManager.setPrepareString("gm_pkg_op_txn_priority_allocation.gm_allocate_order_priority", 1);
     gmDBManager.setString(1, strOrderId);
     gmDBManager.execute();
     gmDBManager.commit();
  	
  }

}
