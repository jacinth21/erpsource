package com.globus.jms.consumers.beans;

import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;	
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author pvigneshwaran
 *This class  common JMS call for vendor portal.Initiating
 *JMS call via this class 
 */
public class GmInitiateVendorJMS extends GmBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	GmCommonClass gmCommon = new GmCommonClass();

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmInitiateVendorJMS(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	/**
	 * initVendorEmailJMS - This method will form the message and send through
	 * JMS to Upadate the mail status for the Orders
	 * 
	 * @param - HashMap:hmParam
	 * @return - Void
	 * @exception - AppError
	 */
	public void initVendorEmailJMS(HashMap hmParam) throws AppError {
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		// VENDOR_EMAIL_CONSUMER_CLASS is mentioned in JMSConsumerConfig.
		// properties file.This key contain the
		// path for :
		// com.globus.jms.consumers.processors.GmVendorEmailConsumerJob
		String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_VENDOR_PORTAL_QUEUE"));
		hmParam.put("QUEUE_NAME", strQueueName);
		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("VENDOR_EMAIL_CONSUMER_CLASS"));
		hmParam.put("HMHOLDVALUES", hmParam);
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		log.debug(" strConsumerClass=>" + strConsumerClass + " hmParam=>>>"+ hmParam + "strQueueName==>" + strQueueName);
		gmConsumerUtil.sendMessage(hmParam);
	}
}
  
    
  


