package com.globus.jms.consumers.beans;

import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmNotificationManager;
import com.globus.custservice.beans.GmDORptBean;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;


public class GmOrderNotificationBean implements GmNotificationInterface{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	@Override
	public void processNotificationRequest(Object objMessageObject, JobDataMap jobDataMap)
		      throws AppError {
		    log.debug("inside GmOrderNotificationBean..");
		    GmNotificationManager gmNotificationManager = new GmNotificationManager();
		    GmDORptBean gmDORptBean = new GmDORptBean();
		    HashMap hmParam = (HashMap) objMessageObject;
		    String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
		    String strMessage = GmCommonClass.parseNull((String) hmParam.get("MESSAGE"));		    
		    String strTokenStr = "";
		   
		    // Call the below method to fetch the device tokens based on the ref id
		    strTokenStr = gmDORptBean.fetchDeviceTokens(strRefId);
		   
		     // Send the notification to the device
		    gmNotificationManager.sendNotification(strTokenStr, strMessage);
		  }

}
