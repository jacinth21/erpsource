/**
 * 
 */
package com.globus.jms.consumers.beans;

import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;

/**
 * @author sswetha
 *
 */
public class GmConsignmentSplitTransBean implements GmSplitTransInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

	
	/**
	 * validateTransaction - This method will validate the input consignment id  by validating the consignment type and the plant for PMT-33511
	 * 
	 * @param strConsignmentId
	 * 	 * @throws AppError
	 */
	public String validateTransaction(String strConsignmentId) {
		// TODO Auto-generated method stub
		GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		String strValidateConsignment = gmTransSplitBean.consignmentValidateTransaction(strConsignmentId);
		log.debug("Inside GmConsignmentSplitTransBean validateTransaction() strValidateConsignment is  "+strValidateConsignment);
		return strValidateConsignment;
	}

	/**
	 * processTransaction - This method will Process the input consignment id and split the consignment by creating transactions based on the building count for PMT-33511
	 * 
	 * @param strConsignmentId
	 * 	 * @throws AppError
	 */
	public Void processTransaction(String strConsignmentId) {
		// TODO Auto-generated method stub
		log.debug("Inside GmConsignmentSplitTransBean processTransaction()");
		GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		gmTransSplitBean.consignmentProcessTransaction(strConsignmentId);
		return null;
	}

}
