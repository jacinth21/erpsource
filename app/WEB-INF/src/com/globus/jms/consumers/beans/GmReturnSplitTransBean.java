/**
 * 
 */
package com.globus.jms.consumers.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;

/**
 * @author sswetha
 *
 */
public class GmReturnSplitTransBean implements GmSplitTransInterface{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * validateTransaction - This method will get the order id by passing the input return id  
	 *  and validate that order id by validating the order type and the plant for PMT-33510
	 * 
	 * @param strRAID
	 * 	 * @throws AppError
	 */
	public String validateTransaction(String strRAID) {
		
		String strOrderId = "";
		GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		//get the order id by passing RMA id
		strOrderId = gmTransSplitBean.getOrderID(strRAID);
		
		String strValidateOrder = gmTransSplitBean.orderValidateTransaction(strOrderId);
		return strValidateOrder;
	}

	/**
	 * processTransaction - This method will et the order id by passing the input return id  and 
	 * Process that order id and split the order by creating transactions based on the building count for PMT-33510
	 * 
	 * @param strRAID
	 * 	 * @throws AppError
	 */
	public Void processTransaction(String strRAID) {
		
		String strOrderId = "";
		GmTransSplitBean gmTransSplitBean = new GmTransSplitBean();
		//get the order id by passing RMA id
		strOrderId = gmTransSplitBean.getOrderID(strRAID);
		
		gmTransSplitBean.orderProcessTransaction(strOrderId);
		return null;
	}

}
