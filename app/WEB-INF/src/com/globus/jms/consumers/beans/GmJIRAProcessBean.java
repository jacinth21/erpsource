package com.globus.jms.consumers.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;

public class GmJIRAProcessBean extends GmBean {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmJIRAProcessBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	
	public GmJIRAProcessBean(GmDataStoreVO gmDataStore) {
	      super(gmDataStore);
	    }
	
	/**
	 * createJiraticketForNCMR - This will call JMS to create/close JIRA Ticket for NCMR when Evaluation created
	 * 
	 * @param - HashMap:hmParam
	 * @return - Void
	 * @exception - AppError
	 */
	public void createJiraticketForNCMR(HashMap hmParam) throws AppError {
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		// JIRA_CONSUMER_CLASS is mentioned in JMSConsumerConfig.
		// properties file.This key contain the
		// path for :
		// com.globus.jms.consumers.processors.GmJiraProcessConsumerJob
		String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_JIRA_QUEUE"));
		hmParam.put("QUEUE_NAME", strQueueName);
		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("JIRA_CONSUMER_CLASS"));
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		log.debug(" strConsumerClass=>" + strConsumerClass + " hmParam=>>>"+ hmParam + "strQueueName==>" + strQueueName);
		gmConsumerUtil.sendMessage(hmParam);
	}

}
