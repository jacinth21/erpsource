package com.globus.jms.consumers.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ssharmila
 * 
 */
public class GmTHBLockGenBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmTHBLockGenBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  public void saveLockAndGenerate(HashMap hmParam) throws AppError

  {

    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    // HashMap hmMessageObj = new HashMap();
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    // to get the queue name
    String strTHBQueue =
        GmCommonClass.parseNull((String) GmCommonClass.getJmsConfig("JMS_THB_LOCK_GENERATE_QUEUE"));
    // to set the JMS class name and queue name
    String strTHBConsumerClass =
        GmCommonClass.parseNull(GmJMSConsumerConfigurationBean
            .getJmsConfig("THB_LOCK_GENERATE_CONSUMER_CLASS"));
    hmParam.put("HMHOLDVALUES", hmParam);
    hmParam.put("COMPANYINFO", strCompanyInfo);
    hmParam.put("CONSUMERCLASS", strTHBConsumerClass);
    hmParam.put("QUEUE_NAME", strTHBQueue);
    log.debug("saveLockAndGenerate-GmTHBLockGenBean->>" + hmParam);
    gmConsumerUtil.sendMessage(hmParam);

  }

}
