package com.globus.jms.consumers.beans;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.io.File;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmWSUtil;
import com.globus.common.util.excel.GmExcelManipulation;

import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;
import org.quartz.JobDataMap;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author pvigneshwaran
 *
 */

public class GmLotBulkExtBean  extends GmBean{




	public GmLotBulkExtBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}



	GmCommonClass gmCommonClass = new GmCommonClass();
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
	/**
	 * UpdateLotExpiry:This method update lot expiry using jms configuration
	 * @param strFileName
	 * @param strUploadHome
	 * @param strFile
	 * @param hmHoldValParam
	 * @throws AppError
	 */
	public void UpdateLotExpiry( String strFileName,String strUploadHome,FormFile strFile,HashMap hmHoldValParam) throws AppError 
	
	{
		  
		  GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		    HashMap hmMessageObj = new HashMap();

		   // to get the queue name
		   String strlotExpiryQueue = GmCommonClass.parseNull((String)GmCommonClass.getJmsConfig("JMS_LOT_EXPIRY_QUEUE"));	
		   // to set the JMS class name and queue name
		   String strlotExpiryQueueConsumerClass =GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("LOT_EXPIRY_CONSUMER_CLASS"));
		    hmMessageObj.put("FILENAME", strFileName);
		    hmMessageObj.put("UPLOADHOME", strUploadHome);
		    hmMessageObj.put("FILE", strFile);
		    hmMessageObj.put("CONSUMERCLASS", strlotExpiryQueueConsumerClass);   
		    hmMessageObj.put("QUEUE_NAME", strlotExpiryQueue);
		    hmMessageObj.put("COMPANY_ID", getCompId());
		    hmMessageObj.put("HMHOLDVALUES", hmHoldValParam);
    	    gmConsumerUtil.sendMessage(hmMessageObj);
    	    log.debug("JMS Message sent");
     
	  }
	/**
	 * lotOverideBulk:This  method update lot expiry 
	 * @param strFileName
	 * @param strUploadHome
	 * @param strFile
	 * @param strUserID
	 * @throws AppError
	 * @throws SQLException
	 * Inputstring:strInputstr=strInputstr+strLotNum+','+strPartNum+','+strExpiryDate+','+'|';
	 */
	  public void lotOverideBulk(String strFileName,String strUploadHome,FormFile strFile,String strUserID) 
			  throws Exception{
		  	ArrayList alReturn = new ArrayList();
		  	HashMap hmReturn = new HashMap();
		  	String strLotNum="";
		  	String strPartNum="";
		  	String strExpiryDate="";
		  	Date dtExpiryDate;
		  	String strInputstr="";
		  	try{
		  	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();  
		  	String strCompanyID = getCompId();
		  	String strExcelFileNameToPopulate = strUploadHome+strFileName;           
			gmExcelManipulation.setExcelFileName(strExcelFileNameToPopulate);
	        gmExcelManipulation.setSheetNumber(0);
	        gmExcelManipulation.setRowNumber(1);
	        gmExcelManipulation.setColumnNumber(0);         
	        gmExcelManipulation.setMaxColumnNumberToRead(2);
          	alReturn = (ArrayList)gmExcelManipulation.readFromExcel_lot_expiry(strUserID);
          	log.debug("lotOverideBulk-alReturn from Excel--"+alReturn);
          	if(alReturn != null){
          	    for (int i = 0; i < alReturn.size()-1; i++) { 	
          	     hmReturn = (HashMap) alReturn.get(i);
          	     strPartNum = GmCommonClass.parseNull((String) hmReturn.get("COLUMN0"));
        	     strLotNum = GmCommonClass.parseNull((String) hmReturn.get("COLUMN1"));
          	     dtExpiryDate = (Date) hmReturn.get("COLUMN2");
          
          	     if(!strPartNum.equals("") && !strLotNum.equals("") && strExpiryDate!=null)
                 {
                	 DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
                     strExpiryDate = df.format(dtExpiryDate);
                     
                     strInputstr=strInputstr+strPartNum+','+strLotNum+','+strExpiryDate+','+'|';
                 }
          	    } 
          	    log.debug("Input String before calling DB"+strInputstr);
              	gmDBManager.setPrepareString("gm_pkg_op_lot_expiry.gm_sav_exp_date", 3);	
      	    	gmDBManager.setString(1, strCompanyID);
      	    	gmDBManager.setString(2, strUserID);
      	    	gmDBManager.setString(3, strInputstr);	
      	    	gmDBManager.execute();
      	    	gmDBManager.commit();
          	} 
          	} catch(SQLException ex) {
          		log.error("SQLException: " + ex.getMessage(), ex);
                throw ex;
          	}
		  	
		  	catch(Exception ex) {
          		log.error("General exception: " + ex.getMessage(), ex);
                throw ex;
          	}
	  }	
	  
	  
	  /**This method will save lot expiry in bulk using JMS 
	 * @param objMessageObject
	 * @param jobDataMap
	 * @throws Exception
	 */
	public void SaveLotExpiry(Object objMessageObject,JobDataMap jobDataMap) throws Exception {

	      HashMap hmMessageObj = (HashMap) objMessageObject;
	      HashMap hmHoldValParam = GmCommonClass.parseNullHashMap((HashMap) hmMessageObj.get("HMHOLDVALUES"));
	      String strCompanyInfo = GmCommonClass.parseNull((String) hmHoldValParam.get("COMPANY_INFO"));
	      String strUserID = GmCommonClass.parseNull((String) hmHoldValParam.get("USERID"));
	      GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmHoldValParam.get("GMDATASTOREVO");
	   // data store VO null then populate the Company Info
	     gmDataStoreVO = (gmDataStoreVO != null)?gmDataStoreVO:(new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
	
	    String strFileName = GmCommonClass.parseNull((String) hmMessageObj.get("FILENAME"));
	    String strUploadHome = GmCommonClass.parseNull((String) hmMessageObj.get("UPLOADHOME"));
	    FormFile strFile = (FormFile) hmMessageObj.get("FILE");
	    GmLotBulkExtBean gmLotBulkExtBean = new GmLotBulkExtBean(gmDataStoreVO);
	    log.debug("strFileName"+strFileName);

	    log.debug("strUploadHome"+strUploadHome);


	    gmLotBulkExtBean.lotOverideBulk(strFileName,strUploadHome,strFile,strUserID); 

	    }
}
