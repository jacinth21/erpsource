package com.globus.jms.consumers.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;


public class GmDHRPriorityUpdateBean {

	
	
	public void UpdateDHRPriority(String strDHRID, String strUserID) throws AppError 
	
	{
		  
		  GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		    HashMap hmMessageObj = new HashMap();

		   // to get the queue name
		   String strDHRQueue = GmCommonClass.parseNull((String)GmCommonClass.getJmsConfig("JMS_DHR_QUEUE"));	
		   // to set the JMS class name and queue name
		   String strDHRConsumerClass =GmCommonClass
			            .parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("BATCH_CONSUMER_MDB_DHR_CLASS"));
		    hmMessageObj.put("DHR_ID", strDHRID);
		    hmMessageObj.put("USER_ID", strUserID);
		    hmMessageObj.put("CONSUMERCLASS", strDHRConsumerClass);   
		    hmMessageObj.put("QUEUE_NAME", strDHRQueue);
    	    gmConsumerUtil.sendMessage(hmMessageObj);
     
	  }
	  
	  

	  public void SyncDHRPriority(String strDHRID, String strUserID) throws AppError {
	  
		    GmDBManager gmDBManager = new GmDBManager();
	        ArrayList<String> strDHRIDList = null;
	        String strTempDHRID;
	        strDHRIDList = new ArrayList(Arrays.asList(strDHRID.split("\\s*,\\s*")));

		    for(int DHRIndex = 0; DHRIndex < strDHRIDList.size(); DHRIndex++)
		    {
		    	strTempDHRID=strDHRIDList.get(DHRIndex);
		        gmDBManager.setPrepareString("GM_PKG_DHR_PRIORITY.GM_DHR_PRIORITY_UPDATE", 2);
		        gmDBManager.setString(1, strTempDHRID);
		        gmDBManager.setString(2, strUserID);
		        gmDBManager.execute();
	     	    gmDBManager.commit();
		    }
	  }
}
