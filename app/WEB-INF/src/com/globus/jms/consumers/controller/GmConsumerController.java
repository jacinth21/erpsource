package com.globus.jms.consumers.controller;


import com.globus.common.jms.GmMessageTransferObject;


public class GmConsumerController {

	private Object messageTransferObject = null;

	public Object getMessageTransferObject() {
		return messageTransferObject;
	}

	public void setMessageTransferObject(Object messageTransferObject) {
		this.messageTransferObject = messageTransferObject;
	}
	
	
	public void dispathMessage() throws Exception{
		
		GmMessageTransferObject msgTransferObject = (GmMessageTransferObject)getMessageTransferObject();
		String strConsumerClass = msgTransferObject.getConsumerClass();
		Object  objMessageObject = msgTransferObject.getMessageObject();
		GmConsumerDispatcherInterface consumerDispatcher = GmConsumerDispatcherFactory.getInstance(strConsumerClass);
		//System.out.println("GmConsumerController Called");
		consumerDispatcher.processMessage(objMessageObject);
	}
	
	
}
