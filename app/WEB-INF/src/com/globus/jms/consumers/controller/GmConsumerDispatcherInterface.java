package com.globus.jms.consumers.controller;

public interface GmConsumerDispatcherInterface {
	public void processMessage(Object objMessageObject) throws Exception;
}
