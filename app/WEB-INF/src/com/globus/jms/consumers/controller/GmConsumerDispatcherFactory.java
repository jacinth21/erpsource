package com.globus.jms.consumers.controller;


public abstract  class GmConsumerDispatcherFactory implements GmConsumerDispatcherInterface {
	
	public abstract void processMessage(Object objMessageObject);
	
	
	public static GmConsumerDispatcherInterface getInstance(String strClassName) throws Exception
	
	{	
		//ClassLoader currClassLoader = GmConsumerDispatcherImpl.class.getClassLoader();
		//currClassLoader.loadClass(strClassName);
		//System.out.println("GmConsumerDispatcherImpl 1");
	  Class consumerDispatchClass = Class.forName(strClassName);
	  //System.out.println("GmConsumerDispatcherImpl 2");
	  return (GmConsumerDispatcherInterface)consumerDispatchClass.newInstance();
	}

	
}
