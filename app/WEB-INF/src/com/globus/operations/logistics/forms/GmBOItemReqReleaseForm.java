/**
 * 
 */
package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

/**
 * @author sswetha
 *
 */
public class GmBOItemReqReleaseForm extends GmCommonForm {
	
	private String strAccountName = "";
	private String strSalesRep = "";
	private String strPartNos = "";
	private String strReqId = "";
	private String chkFullQty = "";
	private String chkPartialQty = "";
	private String chkFGQty = "";
	private String searchstrAccountName = "";
	private String searchstrSalesRep = "";
	private String initialFullchk = "";
	private String strUpdateBillAction = "";
	
	/**
	 * @return the strAccountName
	 */
	public String getStrAccountName() {
		return strAccountName;
	}

	/**
	 * @param strAccountName the strAccountName to set
	 */
	public void setStrAccountName(String strAccountName) {
		this.strAccountName = strAccountName;
	}

	
	/**
	 * @return the strSalesRep
	 */
	public String getStrSalesRep() {
		return strSalesRep;
	}

	/**
	 * @param strSalesRep the strSalesRep to set
	 */
	public void setStrSalesRep(String strSalesRep) {
		this.strSalesRep = strSalesRep;
	}

	/**
	 * @return the strPartNos
	 */
	public String getStrPartNos() {
		return strPartNos;
	}

	/**
	 * @param strPartNos the strPartNos to set
	 */
	public void setStrPartNos(String strPartNos) {
		this.strPartNos = strPartNos;
	}

	/**
	 * @return the chkFullQty
	 */
	public String getChkFullQty() {
		return chkFullQty;
	}

	/**
	 * @param chkFullQty the chkFullQty to set
	 */
	public void setChkFullQty(String chkFullQty) {
		this.chkFullQty = chkFullQty;
	}

	/**
	 * @return the chkPartialQty
	 */
	public String getChkPartialQty() {  
		return chkPartialQty;
	}

	/**
	 * @param chkPartialQty the chkPartialQty to set
	 */
	public void setChkPartialQty(String chkPartialQty) {
		this.chkPartialQty = chkPartialQty;
	}

	/**
	 * @return the chkFGQty
	 */
	public String getChkFGQty() {
		return chkFGQty;
	}

	/**
	 * @param chkFGQty the chkFGQty to set
	 */
	public void setChkFGQty(String chkFGQty) {
		this.chkFGQty = chkFGQty;
	}

	/**
	 * @return the searchstrAccountName
	 */
	public String getSearchstrAccountName() {
		return searchstrAccountName;
	}

	/**
	 * @param searchstrAccountName the searchstrAccountName to set
	 */
	public void setSearchstrAccountName(String searchstrAccountName) {
		this.searchstrAccountName = searchstrAccountName;
	}

	/**
	 * @return the searchstrSalesRep
	 */
	public String getSearchstrSalesRep() {
		return searchstrSalesRep;
	}

	/**
	 * @param searchstrSalesRep the searchstrSalesRep to set
	 */
	public void setSearchstrSalesRep(String searchstrSalesRep) {
		this.searchstrSalesRep = searchstrSalesRep;
	}
	
	/**
	 * @return the initialFullchk
	 */
	public String getInitialFullchk() {
		return initialFullchk;
	}

	/**
	 * @param initialFullchk the initialFullchk to set
	 */
	public void setInitialFullchk(String initialFullchk) {
		this.initialFullchk = initialFullchk;
	}

	/**
	 * @return the strUpdateBillAction
	 */
	public String getStrUpdateBillAction() {
		return strUpdateBillAction;
	}

	/**
	 * @param strUpdateBillAction the strUpdateBillAction to set
	 */
	public void setStrUpdateBillAction(String strUpdateBillAction) {
		this.strUpdateBillAction = strUpdateBillAction;
	}

	public String getStrReqId() {
		return strReqId;
	}

	public void setStrReqId(String strReqId) {
		this.strReqId = strReqId;
	}
}
