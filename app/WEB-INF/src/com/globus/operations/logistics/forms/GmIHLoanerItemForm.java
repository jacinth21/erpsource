package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmIHLoanerItemForm extends GmCommonForm {
	private String eventName 				 = "";		
	private String eventPurpose 			 = "";	
	private String eventEndDate 			 = "";
	private String eventStartDate 			 = "";
	private String caseInfoID 				 = "";
	private String purpose                   = "";
	private String xmlGridData				 = "";
	private String loanType					 = "";
	private String eventLoanToName           = "";
	private String transactionID			 = "";
	private String partNumer				 = "";
	private String loanToName				 = "";
	private String dtType					 = "";
	
	private ArrayList alType 		= new ArrayList();
	private ArrayList alEventName 	= new ArrayList();
	private ArrayList alPurEvent 	= new ArrayList();
	private ArrayList alLoanTo 		= new ArrayList();
	private ArrayList alEmpList 	= new ArrayList();
	private ArrayList alRepList 	= new ArrayList();
    private ArrayList alOUSList     = new ArrayList();
    
	/**
	 * @return the alOUSList
	 */
	public ArrayList getAlOUSList() {
		return alOUSList;
	}
	/**
	 * @param alOUSList the alOUSList to set
	 */
	public void setAlOUSList(ArrayList alOUSList) {
		this.alOUSList = alOUSList;
	}
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * @return the eventPurpose
	 */
	public String getEventPurpose() {
		return eventPurpose;
	}
	/**
	 * @param eventPurpose the eventPurpose to set
	 */
	public void setEventPurpose(String eventPurpose) {
		this.eventPurpose = eventPurpose;
	}
	/**
	 * @return the eventEndDate
	 */
	public String getEventEndDate() {
		return eventEndDate;
	}
	/**
	 * @param eventEndDate the eventEndDate to set
	 */
	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}
	/**
	 * @return the eventStartDate
	 */
	public String getEventStartDate() {
		return eventStartDate;
	}
	/**
	 * @param eventStartDate the eventStartDate to set
	 */
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	/**
	 * @return the caseInfoID
	 */
	public String getCaseInfoID() {
		return caseInfoID;
	}
	/**
	 * @param caseInfoID the caseInfoID to set
	 */
	public void setCaseInfoID(String caseInfoID) {
		this.caseInfoID = caseInfoID;
	}
	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}
	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	
	/**
	 * @return the loanType
	 */
	public String getLoanType() {
		return loanType;
	}
	/**
	 * @param loanType the loanType to set
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	/**
	 * @return the eventLoanToName
	 */
	public String getEventLoanToName() {
		return eventLoanToName;
	}
	/**
	 * @param eventLoanToName the eventLoanToName to set
	 */
	public void setEventLoanToName(String eventLoanToName) {
		this.eventLoanToName = eventLoanToName;
	}
	
	/**
	 * @return the transactionID
	 */
	public String getTransactionID() {
		return transactionID;
	}
	/**
	 * @param transactionID the transactionID to set
	 */
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	/**
	 * @return the partNumer
	 */
	public String getPartNumer() {
		return partNumer;
	}
	/**
	 * @param partNumer the partNumer to set
	 */
	public void setPartNumer(String partNumer) {
		this.partNumer = partNumer;
	}
	
	/**
	 * @return the loanToName
	 */
	public String getLoanToName() {
		return loanToName;
	}
	/**
	 * @param loanToName the loanToName to set
	 */
	public void setLoanToName(String loanToName) {
		this.loanToName = loanToName;
	}
	
	/**
	 * @return the dtType
	 */
	public String getDtType() {
		return dtType;
	}
	/**
	 * @param dtType the dtType to set
	 */
	public void setDtType(String dtType) {
		this.dtType = dtType;
	}
	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}
	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}
	/**
	 * @return the alEventName
	 */
	public ArrayList getAlEventName() {
		return alEventName;
	}
	/**
	 * @param alEventName the alEventName to set
	 */
	public void setAlEventName(ArrayList alEventName) {
		this.alEventName = alEventName;
	}
	/**
	 * @return the alPurEvent
	 */
	public ArrayList getAlPurEvent() {
		return alPurEvent;
	}
	/**
	 * @param alPurEvent the alPurEvent to set
	 */
	public void setAlPurEvent(ArrayList alPurEvent) {
		this.alPurEvent = alPurEvent;
	}
	/**
	 * @return the alLoanTo
	 */
	public ArrayList getAlLoanTo() {
		return alLoanTo;
	}
	/**
	 * @param alLoanTo the alLoanTo to set
	 */
	public void setAlLoanTo(ArrayList alLoanTo) {
		this.alLoanTo = alLoanTo;
	}
	/**
	 * @return the alEmpList
	 */
	public ArrayList getAlEmpList() {
		return alEmpList;
	}
	/**
	 * @param alEmpList the alEmpList to set
	 */
	public void setAlEmpList(ArrayList alEmpList) {
		this.alEmpList = alEmpList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}	
	
		
}

