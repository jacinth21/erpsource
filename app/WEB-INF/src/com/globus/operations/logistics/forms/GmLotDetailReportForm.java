package com.globus.operations.logistics.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmLotDetailReportForm extends GmCommonForm {

	private String strLotNumber = "";
	private String strPartNumber = "";
	private String strPartLiteral = "";
	private String strInvType = "";
	private String companyId = "";
	private String fieldSalesNm = "";
	private String strSetId = "";
	private String gridDataTxnHisInfo = "";
	private ArrayList alPartSearch = new ArrayList();
	/**
	 * @return the strLotNumber
	 */
	public String getStrLotNumber() {
		return strLotNumber;
	}
	/**
	 * @return the strPartNumber
	 */
	public String getStrPartNumber() {
		return strPartNumber;
	}
	/**
	 * @return the strPartLiteral
	 */
	public String getStrPartLiteral() {
		return strPartLiteral;
	}
	/**
	 * @param strPartLiteral the strPartLiteral to set
	 */
	public void setStrPartLiteral(String strPartLiteral) {
		this.strPartLiteral = strPartLiteral;
	}
	/**
	 * @return the alPartSearch
	 */
	public ArrayList getAlPartSearch() {
		return alPartSearch;
	}
	/**
	 * @param alPartSearch the alPartSearch to set
	 */
	public void setAlPartSearch(ArrayList alPartSearch) {
		this.alPartSearch = alPartSearch;
	}
	/**
	 * @param strLotNumber the strLotNumber to set
	 */
	public void setStrLotNumber(String strLotNumber) {
		this.strLotNumber = strLotNumber;
	}
	/**
	 * @param strPartNumber the strPartNumber to set
	 */
	public void setStrPartNumber(String strPartNumber) {
		this.strPartNumber = strPartNumber;
	}
	/**
	 * @return the gridDataTxnHisInfo
	 */
	public String getGridDataTxnHisInfo() {
		return gridDataTxnHisInfo;
	}
	/**
	 * @param gridDataTxnHisInfo the gridDataTxnHisInfo to set
	 */
	public void setGridDataTxnHisInfo(String gridDataTxnHisInfo) {
		this.gridDataTxnHisInfo = gridDataTxnHisInfo;
	}
	/**
	 * @return the strInvType
	 */
	public String getStrInvType() {
		return strInvType;
	}
	/**
	 * @param strInvType the strInvType to set
	 */
	public void setStrInvType(String strInvType) {
		this.strInvType = strInvType;
	}
	/**
	 * @return the companyId
	 */
	public String getCompanyId() {
		return companyId;
	}
	/**
	 * @return the fieldSalesNm
	 */
	public String getFieldSalesNm() {
		return fieldSalesNm;
	}
	/**
	 * @param fieldSalesNm the fieldSalesNm to set
	 */
	public void setFieldSalesNm(String fieldSalesNm) {
		this.fieldSalesNm = fieldSalesNm;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the strSetId
	 */
	public String getStrSetId() {
		return strSetId;
	}
	/**
	 * @param strSetId the strSetId to set
	 */
	public void setStrSetId(String strSetId) {
		this.strSetId = strSetId;
	}

}
