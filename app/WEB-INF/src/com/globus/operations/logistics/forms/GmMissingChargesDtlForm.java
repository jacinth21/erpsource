package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmMissingChargesDtlForm extends GmCommonForm {
	private String distName = "";
	private String repName = "";
	private String requestID = "";
	private String fromDate = "";
	private String toDate = "";
	private String reconCommnets = "";
	private String chk_ticket = "";
	private String chk_repname = "";
	private String chk_listprice = "";
	private String chk_consignment = "";
	private String chk_deductiondt = "";
	private String chk_lastupdby = "";
	private String chk_lastupddt = "";
	private String chk_assocrepname = "";
	private String editCharge = "";
	private String deductionDT = "";
	private String inputString = "";
	private String selectAll = "";
	private String chk_dist = "";
	private String screenType = "";
	private String chargesFor = "";
	private String status = "";
	private String distType = "";
	private String strInputString = "";
	
	private ArrayList alDistList = new ArrayList();
	private ArrayList alRepList = new ArrayList();
	private ArrayList alCommnetsList = new ArrayList();
	private List ldResult = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private List alStatustListRpt = new ArrayList();
	
	/**
	 * @return the distName
	 */
	public String getDistName() {
		return distName;
	}
	/**
	 * @param distName the distName to set
	 */
	public void setDistName(String distName) {
		this.distName = distName;
	}
	/**
	 * @return the repName
	 */
	public String getRepName() {
		return repName;
	}
	/**
	 * @param repName the repName to set
	 */
	public void setRepName(String repName) {
		this.repName = repName;
	}
	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}	
	
	
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the reconCommnets
	 */
	public String getReconCommnets() {
		return reconCommnets;
	}
	/**
	 * @param reconCommnets the reconCommnets to set
	 */
	public void setReconCommnets(String reconCommnets) {
		this.reconCommnets = reconCommnets;
	}
	
	/**
	 * @return the chk_ticket
	 */
	public String getChk_ticket() {
		return chk_ticket;
	}
	/**
	 * @param chkTicket the chk_ticket to set
	 */
	public void setChk_ticket(String chkTicket) {
		chk_ticket = chkTicket;
	}
	/**
	 * @return the chk_repname
	 */
	public String getChk_repname() {
		return chk_repname;
	}
	/**
	 * @param chkRepname the chk_repname to set
	 */
	public void setChk_repname(String chkRepname) {
		chk_repname = chkRepname;
	}
	/**
	 * @return the chk_listprice
	 */
	public String getChk_listprice() {
		return chk_listprice;
	}
	/**
	 * @param chkListprice the chk_listprice to set
	 */
	public void setChk_listprice(String chkListprice) {
		chk_listprice = chkListprice;
	}
	/**
	 * @return the chk_consignment
	 */
	public String getChk_consignment() {
		return chk_consignment;
	}
	/**
	 * @param chkConsignment the chk_consignment to set
	 */
	public void setChk_consignment(String chkConsignment) {
		chk_consignment = chkConsignment;
	}
	/**
	 * @return the chk_deductiondt
	 */
	public String getChk_deductiondt() {
		return chk_deductiondt;
	}
	/**
	 * @param chkDeductiondt the chk_deductiondt to set
	 */
	public void setChk_deductiondt(String chkDeductiondt) {
		chk_deductiondt = chkDeductiondt;
	}
	/**
	 * @return the chk_lastupdby
	 */
	public String getChk_lastupdby() {
		return chk_lastupdby;
	}
	/**
	 * @param chkLastupdby the chk_lastupdby to set
	 */
	public void setChk_lastupdby(String chkLastupdby) {
		chk_lastupdby = chkLastupdby;
	}
	/**
	 * @return the chk_lastupddt
	 */
	public String getChk_lastupddt() {
		return chk_lastupddt;
	}
	/**
	 * @param chkLastupddt the chk_lastupddt to set
	 */
	public void setChk_lastupddt(String chkLastupddt) {
		chk_lastupddt = chkLastupddt;
	}
	
	/**
	 * @return the editCharge
	 */
	public String getEditCharge() {
		return editCharge;
	}
	/**
	 * @param editCharge the editCharge to set
	 */
	public void setEditCharge(String editCharge) {
		this.editCharge = editCharge;
	}
	/**
	 * @return the deductionDT
	 */
	public String getDeductionDT() {
		return deductionDT;
	}
	/**
	 * @param deductionDT the deductionDT to set
	 */
	public void setDeductionDT(String deductionDT) {
		this.deductionDT = deductionDT;
	}
	
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	
	/**
	 * @return the selectAll
	 */
	public String getSelectAll() {
		return selectAll;
	}
	/**
	 * @param selectAll the selectAll to set
	 */
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}
	
	/**
	 * @return the chk_dist
	 */
	public String getChk_dist() {
		return chk_dist;
	}
	/**
	 * @param chkDist the chk_dist to set
	 */
	public void setChk_dist(String chkDist) {
		chk_dist = chkDist;
	}
	
	/**
	 * @return the screenType
	 */
	public String getScreenType() {
		return screenType;
	}
	/**
	 * @param screenType the screenType to set
	 */
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	/**
	 * @return the alCommnetsList
	 */
	public ArrayList getAlCommnetsList() {
		return alCommnetsList;
	}
	/**
	 * @param alCommnetsList the alCommnetsList to set
	 */
	public void setAlCommnetsList(ArrayList alCommnetsList) {
		this.alCommnetsList = alCommnetsList;
	}
	/**
	 * @return the alDistList
	 */
	public ArrayList getAlDistList() {
		return alDistList;
	}
	/**
	 * @param alDistList the alDistList to set
	 */
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the ldResult
	 */
	public List getLdResult() {
		return ldResult;
	}
	/**
	 * @param ldResult the ldResult to set
	 */
	public void setLdResult(List ldResult) {
		this.ldResult = ldResult;
	}
	/**
	 * @return the chargesFor
	 */
	public String getChargesFor() {
		return chargesFor;
	}
	/**
	 * @param chargesFor the chargesFor to set
	 */
	public void setChargesFor(String chargesFor) {
		this.chargesFor = chargesFor;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alStatustListRpt
	 */
	public List getAlStatustListRpt() {
		return alStatustListRpt;
	}
	/**
	 * @param alStatustListRpt the alStatustListRpt to set
	 */
	public void setAlStatustListRpt(List alStatustListRpt) {
		this.alStatustListRpt = alStatustListRpt;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the distType
	 */
	public String getDistType() {
		return distType;
	}
	/**
	 * @param distType the distType to set
	 */
	public void setDistType(String distType) {
		this.distType = distType;
	}
	public String getChk_assocrepname() {
		return chk_assocrepname;
	}
	public void setChk_assocrepname(String chk_assocrepname) {
		this.chk_assocrepname = chk_assocrepname;
	}
	/**
	 * @return the strInputString
	 */
	public String getStrInputString() {
		return strInputString;
	}
	/**
	 * @param strInputString the strInputString to set
	 */
	public void setStrInputString(String strInputString) {
		this.strInputString = strInputString;
	}
	
	
		
}