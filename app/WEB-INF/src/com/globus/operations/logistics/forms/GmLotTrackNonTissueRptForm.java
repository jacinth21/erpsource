package com.globus.operations.logistics.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
* GmLotTrackNonTissueRptForm : Contains the methods used for the Lot Track report for Non Tissue
* author : Agilan Singaravel
*/
public class GmLotTrackNonTissueRptForm extends GmCommonForm {
	
	private String strLotNumber = "";
	private String strPartNumber = "";
	private String warehouseType = "";
	private String strPartLiteral = "";
	private String expiryDateFrom = "";
	private String expiryDateTo = "";
	private String gridXmlData = "";
	private String strLocationId = "";
	private String companyId= "";
	private String plantId = "";
	private String expDateRange = "";
	private String strFieldSales = "";
	private String strSetId = "";
	private String strCntrlNumInvId = "";
	private String strDisable = "";
	private String companyNm = "";
	private String plantNm = "";
	private String fieldSalesNm = "";
	private String accountNm = "";
	private String quantity = "";
	private String strAccess = "";
	private String strLocation = "";
	private String strExpired = "";
	private ArrayList alLotTrackDetail = new ArrayList();
	private ArrayList alCompanyList = new ArrayList();
	private ArrayList alPlantList = new ArrayList();
	private ArrayList alFieldSales = new ArrayList();
	private ArrayList alAccounts = new ArrayList();
	private ArrayList alWarehouseType = new ArrayList();
	private ArrayList alPartSearch = new ArrayList();
	/**
	 * @return the strLotNumber
	 */
	public String getStrLotNumber() {
		return strLotNumber;
	}
	/**
	 * @param strLotNumber the strLotNumber to set
	 */
	public void setStrLotNumber(String strLotNumber) {
		this.strLotNumber = strLotNumber;
	}
	/**
	 * @return the strPartNumber
	 */
	public String getStrPartNumber() {
		return strPartNumber;
	}
	/**
	 * @param strPartNumber the strPartNumber to set
	 */
	public void setStrPartNumber(String strPartNumber) {
		this.strPartNumber = strPartNumber;
	}
	/**
	 * @return the warehouseType
	 */
	public String getWarehouseType() {
		return warehouseType;
	}
	/**
	 * @param warehouseType the warehouseType to set
	 */
	public void setWarehouseType(String warehouseType) {
		this.warehouseType = warehouseType;
	}
	/**
	 * @return the expiryDateFrom
	 */
	public String getExpiryDateFrom() {
		return expiryDateFrom;
	}
	/**
	 * @param expiryDateFrom the expiryDateFrom to set
	 */
	public void setExpiryDateFrom(String expiryDateFrom) {
		this.expiryDateFrom = expiryDateFrom;
	}
	/**
	 * @return the expiryDateTo
	 */
	public String getExpiryDateTo() {
		return expiryDateTo;
	}
	/**
	 * @param expiryDateTo the expiryDateTo to set
	 */
	public void setExpiryDateTo(String expiryDateTo) {
		this.expiryDateTo = expiryDateTo;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the strLocationId
	 */
	public String getStrLocationId() {
		return strLocationId;
	}
	/**
	 * @param strLocationId the strLocationId to set
	 */
	public void setStrLocationId(String strLocationId) {
		this.strLocationId = strLocationId;
	}
	/**
	 * @return the strPartLiteral
	 */
	public String getStrPartLiteral() {
		return strPartLiteral;
	}
	/**
	 * @param strPartLiteral the strPartLiteral to set
	 */
	public void setStrPartLiteral(String strPartLiteral) {
		this.strPartLiteral = strPartLiteral;
	}
	/**
	 * @return the alWarehouseType
	 */
	public ArrayList getAlWarehouseType() {
		return alWarehouseType;
	}
	/**
	 * @param alWarehouseType the alWarehouseType to set
	 */
	public void setAlWarehouseType(ArrayList alWarehouseType) {
		this.alWarehouseType = alWarehouseType;
	}
	/**
	 * @return the alPartSearch
	 */
	public ArrayList getAlPartSearch() {
		return alPartSearch;
	}
	/**
	 * @param alPartSearch the alPartSearch to set
	 */
	public void setAlPartSearch(ArrayList alPartSearch) {
		this.alPartSearch = alPartSearch;
	}
	/**
	 * @return the companyId
	 */
	public String getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the plantId
	 */
	public String getPlantId() {
		return plantId;
	}
	/**
	 * @param plantId the plantId to set
	 */
	public void setPlantId(String plantId) {
		this.plantId = plantId;
	}
	/**
	 * @return the expDateRange
	 */
	public String getExpDateRange() {
		return expDateRange;
	}
	/**
	 * @param expDateRange the expDateRange to set
	 */
	public void setExpDateRange(String expDateRange) {
		this.expDateRange = expDateRange;
	}
	/**
	 * @return the strFieldSales
	 */
	public String getStrFieldSales() {
		return strFieldSales;
	}
	/**
	 * @param strFieldSales the strFieldSales to set
	 */
	public void setStrFieldSales(String strFieldSales) {
		this.strFieldSales = strFieldSales;
	}
	/**
	 * @return the strSetId
	 */
	public String getStrSetId() {
		return strSetId;
	}
	/**
	 * @param strSetId the strSetId to set
	 */
	public void setStrSetId(String strSetId) {
		this.strSetId = strSetId;
	}
	/**
	 * @return the strCntrlNumInvId
	 */
	public String getStrCntrlNumInvId() {
		return strCntrlNumInvId;
	}
	/**
	 * @param strCntrlNumInvId the strCntrlNumInvId to set
	 */
	public void setStrCntrlNumInvId(String strCntrlNumInvId) {
		this.strCntrlNumInvId = strCntrlNumInvId;
	}
	/**
	 * @return the strDisable
	 */
	public String getStrDisable() {
		return strDisable;
	}
	/**
	 * @param strDisable the strDisable to set
	 */
	public void setStrDisable(String strDisable) {
		this.strDisable = strDisable;
	}
	/**
	 * @return the companyNm
	 */
	public String getCompanyNm() {
		return companyNm;
	}
	/**
	 * @param companyNm the companyNm to set
	 */
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}
	/**
	 * @return the plantNm
	 */
	public String getPlantNm() {
		return plantNm;
	}
	/**
	 * @param plantNm the plantNm to set
	 */
	public void setPlantNm(String plantNm) {
		this.plantNm = plantNm;
	}
	/**
	 * @return the fieldSalesNm
	 */
	public String getFieldSalesNm() {
		return fieldSalesNm;
	}
	/**
	 * @param fieldSalesNm the fieldSalesNm to set
	 */
	public void setFieldSalesNm(String fieldSalesNm) {
		this.fieldSalesNm = fieldSalesNm;
	}
	/**
	 * @return the accountNm
	 */
	public String getAccountNm() {
		return accountNm;
	}
	/**
	 * @param accountNm the accountNm to set
	 */
	public void setAccountNm(String accountNm) {
		this.accountNm = accountNm;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the alLotTrackDetail
	 */
	public ArrayList getAlLotTrackDetail() {
		return alLotTrackDetail;
	}
	/**
	 * @param alLotTrackDetail the alLotTrackDetail to set
	 */
	public void setAlLotTrackDetail(ArrayList alLotTrackDetail) {
		this.alLotTrackDetail = alLotTrackDetail;
	}
	/**
	 * @return the alCompanyList
	 */
	public ArrayList getAlCompanyList() {
		return alCompanyList;
	}
	/**
	 * @param alCompanyList the alCompanyList to set
	 */
	public void setAlCompanyList(ArrayList alCompanyList) {
		this.alCompanyList = alCompanyList;
	}
	/**
	 * @return the alPlantList
	 */
	public ArrayList getAlPlantList() {
		return alPlantList;
	}
	/**
	 * @param alPlantList the alPlantList to set
	 */
	public void setAlPlantList(ArrayList alPlantList) {
		this.alPlantList = alPlantList;
	}
	/**
	 * @return the alFieldSales
	 */
	public ArrayList getAlFieldSales() {
		return alFieldSales;
	}
	/**
	 * @param alFieldSales the alFieldSales to set
	 */
	public void setAlFieldSales(ArrayList alFieldSales) {
		this.alFieldSales = alFieldSales;
	}
	/**
	 * @return the alAccounts
	 */
	public ArrayList getAlAccounts() {
		return alAccounts;
	}
	/**
	 * @param alAccounts the alAccounts to set
	 */
	public void setAlAccounts(ArrayList alAccounts) {
		this.alAccounts = alAccounts;
	}
	/**
	 * @return the strAccess
	 */
	public String getStrAccess() {
		return strAccess;
	}
	/**
	 * @param strAccess the strAccess to set
	 */
	public void setStrAccess(String strAccess) {
		this.strAccess = strAccess;
	}
	/**
	 * @return the strLocation
	 */
	public String getStrLocation() {
		return strLocation;
	}
	/**
	 * @param strLocation the strLocation to set
	 */
	public void setStrLocation(String strLocation) {
		this.strLocation = strLocation;
	}
	/**
	 * @return the strExpired
	 */
	public String getStrExpired() {
		return strExpired;
	}
	/**
	 * @param strExpired the strExpired to set
	 */
	public void setStrExpired(String strExpired) {
		this.strExpired = strExpired;
	}
}
