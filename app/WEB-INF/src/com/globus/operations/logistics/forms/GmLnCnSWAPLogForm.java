package com.globus.operations.logistics.forms;

import java.util.List;


import com.globus.common.forms.GmCommonForm;

public class GmLnCnSWAPLogForm extends GmCommonForm {
	
	
	private String gridXmlData = "";

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	

}
