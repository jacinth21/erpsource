/**
 * FileName    : GmLoanerIncidentReportForm.java 
 * Description :
 * Author      : rshah
 * Date        : Apr 2, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.logistics.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmIncidentReportForm extends GmCommonForm{
	private String repId = "";
	private String reportBy ="";
	private String fromDate = "";
	private String toDate= "";
	private String selectAll = "";
	private String incType = "";
	private String[] selectedIncidents = new String[15];
	
	private ArrayList alIncidentList = new ArrayList();
	private ArrayList alRepList = new ArrayList();
	private ArrayList alList = new ArrayList();
	private ArrayList alRptByList = new ArrayList();
	/**
	 * @return the alRptByList
	 */
	public ArrayList getAlRptByList() {
		return alRptByList;
	}
	/**
	 * @param alRptByList the alRptByList to set
	 */
	public void setAlRptByList(ArrayList alRptByList) {
		this.alRptByList = alRptByList;
	}
	/**
	 * @return the alList
	 */
	public ArrayList getAlList() {
		return alList;
	}
	/**
	 * @param alList the alList to set
	 */
	public void setAlList(ArrayList alList) {
		this.alList = alList;
	}
	/**
	 * @return the alIncidentList
	 */
	public ArrayList getAlIncidentList() {
		return alIncidentList;
	}
	/**
	 * @param alIncidentList the alIncidentList to set
	 */
	public void setAlIncidentList(ArrayList alIncidentList) {
		this.alIncidentList = alIncidentList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the repId
	 */
	public String getRepId() {
		return repId;
	}
	/**
	 * @param repId the repId to set
	 */
	public void setRepId(String repId) {
		this.repId = repId;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
	/**
	 * @return the selectedIncidents
	 */
	public String[] getSelectedIncidents() {
		return selectedIncidents;
	}
	/**
	 * @param selectedIncidents the selectedIncidents to set
	 */
	public void setSelectedIncidents(String[] selectedIncidents) {
		this.selectedIncidents = selectedIncidents;
	}
	/**
	 * @return the reportBy
	 */
	public String getReportBy() {
		return reportBy;
	}
	/**
	 * @param reportBy the reportBy to set
	 */
	public void setReportBy(String reportBy) {
		this.reportBy = reportBy;
	}
	/**
	 * @return the selectAll
	 */
	public String getSelectAll() {
		return selectAll;
	}
	/**
	 * @param selectAll the selectAll to set
	 */
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}
	/**
	 * @return the incType
	 */
	public String getIncType() {
		return incType;
	}
	/**
	 * @param incType the incType to set
	 */
	public void setIncType(String incType) {
		this.incType = incType;
	}
	
}
