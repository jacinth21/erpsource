package com.globus.operations.logistics.forms;

import java.util.ArrayList;

import com.globus.common.formbeans.GmCommonForm;

public class GmLoanerRequestReportForm extends GmCommonForm {
  private String setName = "";
  private String strEsclAccessFl = "";
  private String strEsclRemAccessFl = "";
  private String fromDt = "";
  private String distId = "";
  private String dtType = "";
  private String status = "";
  private String toDt = "";
  private String gridXmlData = "";
  private String reqAction = "";
  private String hStatus = "";
  private String hEscFl = "";
  private String hType = "";
  private String requestId = "";
  private String comments = "";
  private String reqStatus = "";
  private boolean showGrid;

  private int alSize;

  private ArrayList alDistributor = new ArrayList();
  private ArrayList alSet = new ArrayList();
  private ArrayList alType = new ArrayList();
  private ArrayList alStatus = new ArrayList();
  private ArrayList alReqType = new ArrayList();
  private ArrayList alLoanerRequest = new ArrayList();


  public String getReqStatus() {
    return reqStatus;
  }

  public void setReqStatus(String reqStatus) {
    this.reqStatus = reqStatus;
  }


  public boolean isShowGrid() {
    return showGrid;
  }

  public void setShowGrid(boolean showGrid)  {
    this.showGrid = showGrid;
  }

  public int getAlSize() {
    return alSize;
  }

  public void setAlSize(int alSize) {
    this.alSize = alSize;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public String gethType() {
    return hType;
  }

  public void sethType(String hType) {
    this.hType = hType;
  }


  /**
   * @return the hEscFl
   */
  public String gethEscFl() {
    return hEscFl;
  }

  /**
   * @param hEscFl the hEscFl to set
   */
  public void sethEscFl(String hEscFl) {
    this.hEscFl = hEscFl;
  }

  /**
   * @return the hStatus
   */
  public String gethStatus() {
    return hStatus;
  }

  /**
   * @param hStatus the hStatus to set
   */
  public void sethStatus(String hStatus) {
    this.hStatus = hStatus;
  }

  /**
   * @return the reqAction
   */
  public String getReqAction() {
    return reqAction;
  }

  /**
   * @param reqAction the reqAction to set
   */
  public void setReqAction(String reqAction) {
    this.reqAction = reqAction;
  }

  /**
   * @return the alLoanerRequest
   */
  public ArrayList getAlLoanerRequest() {
    return alLoanerRequest;
  }

  /**
   * @param alLoanerRequest the alLoanerRequest to set
   */
  public void setAlLoanerRequest(ArrayList alLoanerRequest) {
    this.alLoanerRequest = alLoanerRequest;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the dtType
   */
  public String getDtType() {
    return dtType;
  }

  /**
   * @param dtType the dtType to set
   */
  public void setDtType(String dtType) {
    this.dtType = dtType;
  }

  /**
   * @return the alReqType
   */
  public ArrayList getAlReqType() {
    return alReqType;
  }

  /**
   * @param alReqType the alReqType to set
   */
  public void setAlReqType(ArrayList alReqType) {
    this.alReqType = alReqType;
  }

  /**
   * @return the distId
   */
  public String getDistId() {
    return distId;
  }

  /**
   * @param distId the distId to set
   */
  public void setDistId(String distId) {
    this.distId = distId;
  }

  /**
   * @return the setName
   */
  public String getSetName() {
    return setName;
  }

  /**
   * @param setName the setName to set
   */
  public void setSetName(String setName) {
    this.setName = setName;
  }


  /**
   * @return the strEsclAccessFl
   */
  public String getStrEsclAccessFl() {
    return strEsclAccessFl;
  }

  /**
   * @param strEsclAccessFl the strEsclAccessFl to set
   */
  public void setStrEsclAccessFl(String strEsclAccessFl) {
    this.strEsclAccessFl = strEsclAccessFl;
  }

  /**
   * @return the strEsclRemAccessFl
   */
  public String getStrEsclRemAccessFl() {
    return strEsclRemAccessFl;
  }

  /**
   * @param strEsclRemAccessFl the strEsclRemAccessFl to set
   */
  public void setStrEsclRemAccessFl(String strEsclRemAccessFl) {
    this.strEsclRemAccessFl = strEsclRemAccessFl;
  }


  /**
   * @return the fromDt
   */
  public String getFromDt() {
    return fromDt;
  }

  /**
   * @param fromDt the fromDt to set
   */
  public void setFromDt(String fromDt) {
    this.fromDt = fromDt;
  }

  /**
   * @return the toDt
   */
  public String getToDt() {
    return toDt;
  }

  /**
   * @param toDt the toDt to set
   */
  public void setToDt(String toDt) {
    this.toDt = toDt;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the alType
   */
  public ArrayList getAlType() {
    return alType;
  }

  /**
   * @param alType the alType to set
   */
  public void setAlType(ArrayList alType) {
    this.alType = alType;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the alDistributor
   */
  public ArrayList getAlDistributor() {
    return alDistributor;
  }

  /**
   * @param alDistributor the alDistributor to set
   */
  public void setAlDistributor(ArrayList alDistributor) {
    this.alDistributor = alDistributor;
  }

  /**
   * @return the alEmpList
   */
  public ArrayList getAlSet() {
    return alSet;
  }

  /**
   * @param alEmpList the alEmpList to set
   */
  public void setAlSet(ArrayList alEmpList) {
    this.alSet = alEmpList;
  }


}
