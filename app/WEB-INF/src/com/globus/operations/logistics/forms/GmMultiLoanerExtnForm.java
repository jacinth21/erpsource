package com.globus.operations.logistics.forms;

import java.util.Date;
import com.globus.common.forms.GmCommonForm;

public class GmMultiLoanerExtnForm extends GmCommonForm{
	
	private String strSetName = "";
	private String strSetId = "";
	private String strConsignmentId = "";
	private String strSalesRep = "";
	private String strDistributor = "";
	private String strReasonType ="";
	private String strComments = "";
	private String strReqextfl = "";
	private String strApprovalFl ="";
	private String gridXmlData = "";
	private String strSurgeryDt = "";
	private String strExtOfDateRtn = "";
	
	
	/**
	 * @return the strSetName
	 */
	public String getStrSetName() {
		return strSetName;
	}
	/**
	 * @param strSetName the strSetName to set
	 */
	public void setStrSetName(String strSetName) {
		this.strSetName = strSetName;
	}
	/**
	 * @return the strSetId
	 */
	public String getStrSetId() {
		return strSetId;
	}
	/**
	 * @param strSetId the strSetId to set
	 */
	public void setStrSetId(String strSetId) {
		this.strSetId = strSetId;
	}
	/**
	 * @return the strConsignmentId
	 */
	public String getStrConsignmentId() {
		return strConsignmentId;
	}
	/**
	 * @param strConsignmentId the strConsignmentId to set
	 */
	public void setStrConsignmentId(String strConsignmentId) {
		this.strConsignmentId = strConsignmentId;
	}
	/**
	 * @return the strSalesRep
	 */
	public String getStrSalesRep() {
		return strSalesRep;
	}
	/**
	 * @param strSalesRep the strSalesRep to set
	 */
	public void setStrSalesRep(String strSalesRep) {
		this.strSalesRep = strSalesRep;
	}
	/**
	 * @return the strDistributor
	 */
	public String getStrDistributor() {
		return strDistributor;
	}
	/**
	 * @param strDistributor the strDistributor to set
	 */
	public void setStrDistributor(String strDistributor) {
		this.strDistributor = strDistributor;
	}
	/**
	 * @return the strReasonType
	 */
	public String getStrReasonType() {
		return strReasonType;
	}
	/**
	 * @param strReasonType the strReasonType to set
	 */
	public void setStrReasonType(String strReasonType) {
		this.strReasonType = strReasonType;
	}
	/**
	 * @return the strComments
	 */
	public String getStrComments() {
		return strComments;
	}
	/**
	 * @param strComments the strComments to set
	 */
	public void setStrComments(String strComments) {
		this.strComments = strComments;
	}
	/**
	 * @return the strReqextfl
	 */
	public String getStrReqextfl() {
		return strReqextfl;
	}
	/**
	 * @param strReqextfl the strReqextfl to set
	 */
	public void setStrReqextfl(String strReqextfl) {
		this.strReqextfl = strReqextfl;
	}
	/**
	 * @return the strApprovalFl
	 */
	public String getStrApprovalFl() {
		return strApprovalFl;
	}
	/**
	 * @param strApprovalFl the strApprovalFl to set
	 */
	public void setStrApprovalFl(String strApprovalFl) {
		this.strApprovalFl = strApprovalFl;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the strSurgeryDt
	 */
	public String getStrSurgeryDt() {
		return strSurgeryDt;
	}
	/**
	 * @param strSurgeryDt the strSurgeryDt to set
	 */
	public void setStrSurgeryDt(String strSurgeryDt) {
		this.strSurgeryDt = strSurgeryDt;
	}
	/**
	 * @return the strExtOfDateRtn
	 */
	public String getStrExtOfDateRtn() {
		return strExtOfDateRtn;
	}
	/**
	 * @param strExtOfDateRtn the strExtOfDateRtn to set
	 */
	public void setStrExtOfDateRtn(String strExtOfDateRtn) {
		this.strExtOfDateRtn = strExtOfDateRtn;
	}
}
