package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

import com.globus.common.forms.GmCommonForm;

public class GmChargesApprovalForm extends GmMissingChargesDtlForm{
	private ArrayList alDistList = new ArrayList();
	private ArrayList alRepList = new ArrayList();
	private ArrayList alStatus= new ArrayList();
	private List alStatustListRpt = new ArrayList(); // this list is used to populate the dropdown present on the display tag prt
	private ArrayList alIncidentList = new ArrayList();
	private List alReturn = new ArrayList();
	private ArrayList alType = new ArrayList();
	
	private String dist = "";
	private String rep = "";
	private String status = "";
	private String incident = "";
	private String chargesFor = "50891"; //Missing
	private String fromDate = "";
	private String toDate = "";
	private String updateString = "";
	private String shwDtl="";
	private String shwDist = "";
	private String shwDirRep = "";
	private String requestID = ""; // PMT-8847
	/**
	 * @return the alDistList
	 */
	public ArrayList getAlDistList() {
		return alDistList;
	}
	/**
	 * @param alDistList the alDistList to set
	 */
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the alStatustList
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatustList the alStatustList to set
	 */
	public void setAlStatus(ArrayList alStatustList) {
		this.alStatus= alStatustList;
	}
	/**
	 * @return the alIncidentList
	 */
	public ArrayList getAlIncidentList() {
		return alIncidentList;
	}
	/**
	 * @param alIncidentList the alIncidentList to set
	 */
	public void setAlIncidentList(ArrayList alIncidentList) {
		this.alIncidentList = alIncidentList;
	}
	/**
	 * @return the dist
	 */
	public String getDist() {
		return dist;
	}
	/**
	 * @param dist the dist to set
	 */
	public void setDist(String dist) {
		this.dist = dist;
	}
	/**
	 * @return the rep
	 */
	public String getRep() {
		return rep;
	}
	/**
	 * @param rep the rep to set
	 */
	public void setRep(String rep) {
		this.rep = rep;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the incident
	 */
	public String getIncident() {
		return incident;
	}
	/**
	 * @param incident the incident to set
	 */
	public void setIncident(String incident) {
		this.incident = incident;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the alReturn
	 */
	public List getAlReturn() {
		return alReturn;
	}
	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(List alReturn) {
		this.alReturn = alReturn;
	}
	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}
	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}
	/**
	 * @return the chargesFor
	 */
	public String getChargesFor() {
		return chargesFor;
	}
	/**
	 * @param chargesFor the chargesFor to set
	 */
	public void setChargesFor(String chargesFor) {
		this.chargesFor = chargesFor;
	}
	/**
	 * @return the alStatustListRpt
	 */
	public List getAlStatustListRpt() {
		return alStatustListRpt;
	}
	/**
	 * @param alStatustListRpt the alStatustListRpt to set
	 */
	public void setAlStatustListRpt(List alStatustListRpt) {
		this.alStatustListRpt = alStatustListRpt;
	}
	/**
	 * @return the updateString
	 */
	public String getUpdateString() {
		return updateString;
	}
	/**
	 * @param updateString the updateString to set
	 */
	public void setUpdateString(String updateString) {
		this.updateString = updateString;
	}
	/**
	 * @return the shwDtl
	 */
	public String getShwDtl() {
		return shwDtl;
	}
	/**
	 * @param shwDtl the shwDtl to set
	 */
	public void setShwDtl(String shwDtl) {
		this.shwDtl = shwDtl;
	}
	/**
	 * @return the shwDist
	 */
	public String getShwDist() {
		return shwDist;
	}
	/**
	 * @param shwDist the shwDist to set
	 */
	public void setShwDist(String shwDist) {
		this.shwDist = shwDist;
	}
	/**
	 * @return the shwDirRep
	 */
	public String getShwDirRep() {
		return shwDirRep;
	}
	/**
	 * @param shwDirRep the shwDirRep to set
	 */
	public void setShwDirRep(String shwDirRep) {
		this.shwDirRep = shwDirRep;
	}
	
	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}
	
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
		
}
