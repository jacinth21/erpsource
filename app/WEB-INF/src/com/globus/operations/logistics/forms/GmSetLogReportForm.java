package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmSetLogReportForm extends GmCommonForm {
	private String setId = "";
	private String type = "";
	private String frmDate = "";
	private String toDate = "";
	private List lresult = null;
	private ArrayList alType = new ArrayList();

	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}

	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}

	/**
	 * @return the lresult
	 */
	public List getLresult() {
		return lresult;
	}

	/**
	 * @param lresult the lresult to set
	 */
	public void setLresult(List lresult) {
		this.lresult = lresult;
	}

	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}

	/**
	 * @param setId
	 *            the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}
	
	/**
	 * @return the source
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param source
	 *            the source to set
	 */
	public void setType(String source) {
		this.type = source;
	}

	/**
	 * @return the frmDate
	 */
	public String getFrmDate() {
		return frmDate;
	}

	/**
	 * @param frmDate
	 *            the frmDate to set
	 */
	public void setFrmDate(String frmDate) {
		this.frmDate = frmDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate
	 *            the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

}
