package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.operations.shipping.forms.GmShipDetailsForm;

public class GmMultiProcConsignForm extends GmShipDetailsForm {
	
		private String strSetName = "";
		private String strSetId = "";
		private String strConsignmentId = "";
		private String strReqId = "";
		private String strStatus = ""; 
		private String gridXmlData = "";
		private String strReqDate = "";
		private String strSource = "";
		private String strReqTransIds ="";
		private String strSetIds = "";
		private String strSetNames ="";
		private String strReqType = "";
		private String strReqTo ="";
		private String strReqStatus = "";
		private String strRequestFor = "";
		private Date strPlanedShipDt = null;
		private Date strRequirDate = null;
		private String strInputString = "";
				
		private ArrayList alRequestStatus = new  ArrayList();
		private ArrayList alRequestTo = new ArrayList();
		
		/**
		 * @return the strSetName
		 */
		public String getStrSetName() {
			return strSetName;
		}
		/**
		 * @param strSetName the strSetName to set
		 */
		public void setStrSetName(String strSetName) {
			this.strSetName = strSetName;
		}
		/**
		 * @return the strSetId
		 */
		public String getStrSetId() {
			return strSetId;
		}
		/**
		 * @param strSetId the strSetId to set
		 */
		public void setStrSetId(String strSetId) {
			this.strSetId = strSetId;
		}
		/**
		 * @return the strConsignmentId
		 */
		public String getStrConsignmentId() {
			return strConsignmentId;
		}
		/**
		 * @param strConsignmentId the strConsignmentId to set
		 */
		public void setStrConsignmentId(String strConsignmentId) {
			this.strConsignmentId = strConsignmentId;
		}
		/**
		 * @return the strReqId
		 */
		public String getStrReqId() {
			return strReqId;
		}
		/**
		 * @param strReqId the strReqId to set
		 */
		public void setStrReqId(String strReqId) {
			this.strReqId = strReqId;
		}
		/**
		 * @return the strStatus
		 */
		public String getStrStatus() {
			return strStatus;
		}
		/**
		 * @param strStatus the strStatus to set
		 */
		public void setStrStatus(String strStatus) {
			this.strStatus = strStatus;
		}
		
		
		/**
		 * @return the gridXmlData
		 */
		public String getGridXmlData() {
			return gridXmlData;
		}
		/**
		 * @param gridXmlData the gridXmlData to set
		 */
		public void setGridXmlData(String gridXmlData) {
			this.gridXmlData = gridXmlData;
		}
		/**
		 * @return the alRequestStatus
		 */
		public ArrayList getAlRequestStatus() {
			return alRequestStatus;
		}
		/**
		 * @param alRequestStatus the alRequestStatus to set
		 */
		public void setAlRequestStatus(ArrayList alRequestStatus) {
			this.alRequestStatus = alRequestStatus;
		}
		/**
		 * @return the strReqDate
		 */
		public String getStrReqDate() {
			return strReqDate;
		}
		/**
		 * @param strReqDate the strReqDate to set
		 */
		public void setStrReqDate(String strReqDate) {
			this.strReqDate = strReqDate;
		}
		/**
		 * @return the strSource
		 */
		public String getStrSource() {
			return strSource;
		}
		/**
		 * @param strSource the strSource to set
		 */
		public void setStrSource(String strSource) {
			this.strSource = strSource;
		}
		/**
		 * @return the strReqTransIds
		 */
		public String getStrReqTransIds() {
			return strReqTransIds;
		}
		/**
		 * @param strReqTransIds the strReqTransIds to set
		 */
		public void setStrReqTransIds(String strReqTransIds) {
			this.strReqTransIds = strReqTransIds;
		}
		/**
		 * @return the strSetIds
		 */
		public String getStrSetIds() {
			return strSetIds;
		}
		/**
		 * @param strSetIds the strSetIds to set
		 */
		public void setStrSetIds(String strSetIds) {
			this.strSetIds = strSetIds;
		}
		/**
		 * @return the strSetNames
		 */
		public String getStrSetNames() {
			return strSetNames;
		}
		/**
		 * @param strSetNames the strSetNames to set
		 */
		public void setStrSetNames(String strSetNames) {
			this.strSetNames = strSetNames;
		}
		/**
		 * @return the strReqType
		 */
		public String getStrReqType() {
			return strReqType;
		}
		/**
		 * @param strReqType the strReqType to set
		 */
		public void setStrReqType(String strReqType) {
			this.strReqType = strReqType;
		}
		/**
		 * @return the strReqTo
		 */
		public String getStrReqTo() {
			return strReqTo;
		}
		/**
		 * @param strReqTo the strReqTo to set
		 */
		public void setStrReqTo(String strReqTo) {
			this.strReqTo = strReqTo;
		}
		/**
		 * @return the strReqStatus
		 */
		public String getStrReqStatus() {
			return strReqStatus;
		}
		/**
		 * @param strReqStatus the strReqStatus to set
		 */
		public void setStrReqStatus(String strReqStatus) {
			this.strReqStatus = strReqStatus;
		}
		/**
		 * @return the strRequestFor
		 */
		public String getStrRequestFor() {
			return strRequestFor;
		}
		/**
		 * @param strRequestFor the strRequestFor to set
		 */
		public void setStrRequestFor(String strRequestFor) {
			this.strRequestFor = strRequestFor;
		}
		
		/**
		 * @return the strPlanedShipDt
		 */
		public Date getStrPlanedShipDt() {
			return strPlanedShipDt;
		}
		/**
		 * @param strPlanedShipDt the strPlanedShipDt to set
		 */
		public void setStrPlanedShipDt(Date strPlanedShipDt) {
			this.strPlanedShipDt = strPlanedShipDt;
		}
		/**
		 * @return the strRequirDate
		 */
		public Date getStrRequirDate() {
			return strRequirDate;
		}
		/**
		 * @param strRequirDate the strRequirDate to set
		 */
		public void setStrRequirDate(Date strRequirDate) {
			this.strRequirDate = strRequirDate;
		}
		/**
		 * @return the strInputString
		 */
		public String getStrInputString() {
			return strInputString;
		}
		/**
		 * @param strInputString the strInputString to set
		 */
		public void setStrInputString(String strInputString) {
			this.strInputString = strInputString;
		}
		/**
		 * @return the alRequestTo
		 */
		public ArrayList getAlRequestTo() {
			return alRequestTo;
		}
		/**
		 * @param alRequestTo the alRequestTo to set
		 */
		public void setAlRequestTo(ArrayList alRequestTo) {
			this.alRequestTo = alRequestTo;
		}
}
