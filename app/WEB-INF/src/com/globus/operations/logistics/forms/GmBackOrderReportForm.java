package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmBackOrderReportForm extends GmCommonForm{

	private String requestId = "";
	 
    private String partNum = "";
    private String setID = "";
    private String consignID = "" ;
    private String controlNum = "";
    private String search = "" ;
    private String project = "";
    private String summaryVer = "";
    private String forecastPeriod = "3";
    private String selectAll = "";
    private String hconstatusNotcheck = "";
    private String hreqstatusNotcheck = "";
    private String hsetstatusNotcheck = "";
    
    private String fulFillType ="";
    private String fulfillQtyFlag ="";
    private String fulfillBoFlag ="";
    private String  checkType = "50242";
    private String hTxnId="";
    private String hSetId="";
    private String setNumSearch="";
    
    private String  strFromDate ="";
	private String  strToDate = "";
	private String  hInputString = "";
	private String  distType = "";
	private String  emailType = "";
	private String xmlGridData  = "";
	private String strRepString = "";
	private String accCurrId= "";
	private String accCurrSymb= "";
	
	private ArrayList alEmail = new ArrayList();
	private ArrayList alDistributorType = new ArrayList();
	private ArrayList alBODetails = new ArrayList();
	private ArrayList alCompCurr = new ArrayList();
	
	
	

	public String getAccCurrId() {
		return accCurrId;
	}
	public void setAccCurrId(String accCurrId) {
		this.accCurrId = accCurrId;
	}
	public String getAccCurrSymb() {
		return accCurrSymb;
	}
	public void setAccCurrSymb(String accCurrSymb) {
		this.accCurrSymb = accCurrSymb;
	}
	public ArrayList getAlCompCurr() {
		return alCompCurr;
	}
	public void setAlCompCurr(ArrayList alCompCurr) {
		this.alCompCurr = alCompCurr;
	}
	public String getSetNumSearch() {
		return setNumSearch;
	}
	public void setSetNumSearch(String setNumSearch) {
		this.setNumSearch = setNumSearch;
	}
	/**
	 * @return the hSetId
	 */
	public String gethSetId() {
		return hSetId;
	}
	/**
	 * @param hSetId the hSetId to set
	 */
	public void sethSetId(String hSetId) {
		this.hSetId = hSetId;
	}
	/**
	 * @return the hTxnId
	 */
	public String gethTxnId() {
		return hTxnId;
	}
	/**
	 * @param hTxnId the hTxnId to set
	 */
	public void sethTxnId(String hTxnId) {
		this.hTxnId = hTxnId;
	}
	private String[] checkSelectedCon = {"","1",""};
    private String[] checkSelectedReq = {"10","15","20"};
    
    private String[] checkSelectedSets = new String [5];
    private String[] checkSelectedProjects = new String [5];
    
    private String[] checkSelectedTrend = {"40020","40021","40022"};
    private String salesGrpType = "50272"; //80% selling parts
    private String trendType = "50277"; //foreast
    private String salesGrpId = "";
    private String trendDuration = "50282"; //15 days
       
    private ArrayList alTrendStatus = new ArrayList();
    private ArrayList alSalesGrpType = new ArrayList();
    private ArrayList alTrendType = new ArrayList();
    private ArrayList alSalesGrpId = new ArrayList();
    private ArrayList alTrendDuration = new ArrayList(); 
    private String trendPeriod = "3";
    private String backorderCheck = "";
    
    private ArrayList alSearch = new ArrayList();
    private ArrayList alProject = new ArrayList();
    
    private ArrayList alConStatus = new ArrayList();
    private ArrayList alReqStatus = new ArrayList();
    
    private ArrayList alSets = new ArrayList();
    private ArrayList alType = new ArrayList();
    
    private ArrayList alSetStatusReport = new ArrayList();
    private ArrayList alPartFulfillReport = new ArrayList();
       
    private List ldhrPartResult = new ArrayList();
    private List ldtResult = new ArrayList();
    private HashMap hmCrossTabReport = new HashMap();

    private ArrayList alCtrlNumOpers = new ArrayList();
    private String operator = "";
    
	public List getLdtResult() {
		return ldtResult;
	}
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getPartNum() {
		return partNum;
	}
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	public String getSetID() {
		return setID;
	}
	public void setSetID(String setID) {
		this.setID = setID;
	}
	public String getConsignID() {
		return consignID;
	}
	public void setConsignID(String consignID) {
		this.consignID = consignID;
	}
	public String getControlNum() {
		return controlNum;
	}
	public void setControlNum(String controlNum) {
		this.controlNum = controlNum;
	}
	public ArrayList getAlSearch() {
		return alSearch;
	}
	public void setAlSearch(ArrayList alSearch) {
		this.alSearch = alSearch;
	}
	public ArrayList getAlProject() {
		return alProject;
	}
	public void setAlProject(ArrayList alProject) {
		this.alProject = alProject;
	}
	 
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public ArrayList getAlConStatus() {
		return alConStatus;
	}
	public void setAlConStatus(ArrayList alConStatus) {
		this.alConStatus = alConStatus;
	}
	public ArrayList getAlReqStatus() {
		return alReqStatus;
	}
	public void setAlReqStatus(ArrayList alReqStatus) {
		this.alReqStatus = alReqStatus;
	}
	public String[] getCheckSelectedCon() {
		return checkSelectedCon;
	}
	public void setCheckSelectedCon(String[] checkSelectedCon) {
		this.checkSelectedCon = checkSelectedCon;
	}
	public String[] getCheckSelectedReq() {		 
		return checkSelectedReq;
	}
	public void setCheckSelectedReq(String[] checkSelectedReq) {
		this.checkSelectedReq = checkSelectedReq;
	}
	public ArrayList getAlSetStatusReport() {
		return alSetStatusReport;
	}
	public void setAlSetStatusReport(ArrayList alSetStatusReport) {
		this.alSetStatusReport = alSetStatusReport;
	}
	public String getSummaryVer() {
		return summaryVer;
	}
	public void setSummaryVer(String summaryVer) {
		this.summaryVer = summaryVer;
	}
	public ArrayList getAlSets() {
		return alSets;
	}
	public void setAlSets(ArrayList alSets) {
		this.alSets = alSets;
	}
	public String getForecastPeriod() {
		return forecastPeriod;
	}
	public void setForecastPeriod(String forecastPeriod) {
		this.forecastPeriod = forecastPeriod;
	}
	public String getSelectAll() {
		return selectAll;
	}
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}
	public String getHconstatusNotcheck() {
		return hconstatusNotcheck;
	}
	public void setHconstatusNotcheck(String hconstatusNotcheck) {
		this.hconstatusNotcheck = hconstatusNotcheck;
	}
	public String getHreqstatusNotcheck() {
		return hreqstatusNotcheck;
	}
	public void setHreqstatusNotcheck(String hreqstatusNotcheck) {
		this.hreqstatusNotcheck = hreqstatusNotcheck;
	}
	public List getLdhrPartResult() {
		return ldhrPartResult;
	}
	public void setLdhrPartResult(List ldhrPartResult) {
		this.ldhrPartResult = ldhrPartResult;
	}
	public HashMap getHmCrossTabReport() {
		return hmCrossTabReport;
	}
	public void setHmCrossTabReport(HashMap hmCrossTabReport) {
		this.hmCrossTabReport = hmCrossTabReport;
	}
	public String getFulFillType() {
		return fulFillType;
	}
	public void setFulFillType(String fulFillType) {
		this.fulFillType = fulFillType;
	}
	public String getFulfillQtyFlag() {
		return fulfillQtyFlag;
	}
	public void setFulfillQtyFlag(String fulfillQtyFlag) {
		this.fulfillQtyFlag = fulfillQtyFlag;
	}
	public String getHsetstatusNotcheck() {
		return hsetstatusNotcheck;
	}
	public void setHsetstatusNotcheck(String hsetstatusNotcheck) {
		this.hsetstatusNotcheck = hsetstatusNotcheck;
	}
	public String[] getCheckSelectedSets() {
		return checkSelectedSets;
	}
	public void setCheckSelectedSets(String[] checkSelectedSets) {
		this.checkSelectedSets = checkSelectedSets;
	}
	
	/**
	 * @return the checkSetArr
	 */
	/*public String[] getCheckSetArr() {
		return checkSetArr;
	}*/
	/**
	 * @param checkSetArr the checkSetArr to set
	 */
	/*public void setCheckSetArr(String[] checkSetArr) {
		this.checkSetArr = checkSetArr;
	}*/

	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}
	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}
	/**
	 * @return the checkType
	 */
	public String getCheckType() {
		return checkType;
	}
	/**
	 * @param checkType the checkType to set
	 */
	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}
	/**
	 * @return the alPartFulfillReport
	 */
	public ArrayList getAlPartFulfillReport() {
		return alPartFulfillReport;
	}
	/**
	 * @param alPartFulfillReport the alPartFulfillReport to set
	 */
	public void setAlPartFulfillReport(ArrayList alPartFulfillReport) {
		this.alPartFulfillReport = alPartFulfillReport;
	}
	/**
	 * @return the checkSelectedProjects
	 */
	public String[] getCheckSelectedProjects() {
		return checkSelectedProjects;
	}
	/**
	 * @param checkSelectedProjects the checkSelectedProjects to set
	 */
	public void setCheckSelectedProjects(String[] checkSelectedProjects) {
		this.checkSelectedProjects = checkSelectedProjects;
	}
	/**
	 * @return the fulfillBoFlag
	 */
	public String getFulfillBoFlag() {
		return fulfillBoFlag;
	}
	/**
	 * @param fulfillBoFlag the fulfillBoFlag to set
	 */
	public void setFulfillBoFlag(String fulfillBoFlag) {
		this.fulfillBoFlag = fulfillBoFlag;
	}
	public String getTrendType() {
		return trendType;
	}
	public void setTrendType(String trendType) {
		this.trendType = trendType;
	}
	public ArrayList getAlTrendType() {
		return alTrendType;
	}
	public void setAlTrendType(ArrayList alTrendType) {
		this.alTrendType = alTrendType;
	}
	public String[] getCheckSelectedTrend() {
		return checkSelectedTrend;
	}
	public void setCheckSelectedTrend(String[] checkSelectedTrend) {
		this.checkSelectedTrend = checkSelectedTrend;
	}
	public ArrayList getAlTrendStatus() {
		return alTrendStatus;
	}
	public void setAlTrendStatus(ArrayList alTrendStatus) {
		this.alTrendStatus = alTrendStatus;
	}
	public String getSalesGrpType() {
		return salesGrpType;
	}
	public void setSalesGrpType(String salesGrpType) {
		this.salesGrpType = salesGrpType;
	}
	public ArrayList getAlSalesGrpType() {
		return alSalesGrpType;
	}
	public void setAlSalesGrpType(ArrayList alSalesGrpType) {
		this.alSalesGrpType = alSalesGrpType;
	}
	public String getSalesGrpId() {
		return salesGrpId;
	}
	public void setSalesGrpId(String salesGrpId) {
		this.salesGrpId = salesGrpId;
	}
	public ArrayList getAlSalesGrpId() {
		return alSalesGrpId;
	}
	public void setAlSalesGrpId(ArrayList alSalesGrpId) {
		this.alSalesGrpId = alSalesGrpId;
	}
	public String getTrendDuration() {
		return trendDuration;
	}
	public void setTrendDuration(String trendDuration) {
		this.trendDuration = trendDuration;
	}
	public ArrayList getAlTrendDuration() {
		return alTrendDuration;
	}
	public void setAlTrendDuration(ArrayList alTrendDuration) {
		this.alTrendDuration = alTrendDuration;
	}
	public String getTrendPeriod() {
		return trendPeriod;
	}
	public void setTrendPeriod(String trendPeriod) {
		this.trendPeriod = trendPeriod;
	}
	public String getBackorderCheck() {
		return backorderCheck;
	}
	public void setBackorderCheck(String backorderCheck) {
		this.backorderCheck = backorderCheck;
	}

	public ArrayList getAlCtrlNumOpers() {
		return alCtrlNumOpers;
	}

	public void setAlCtrlNumOpers(ArrayList alCtrlNumOpers) {
		this.alCtrlNumOpers = alCtrlNumOpers;
	}

	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	/**
	 * @return the strFromDate
	 */
	public String getStrFromDate() {
		return strFromDate;
	}
	/**
	 * @param strFromDate the strFromDate to set
	 */
	public void setStrFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}
	/**
	 * @return the strToDate
	 */
	public String getStrToDate() {
		return strToDate;
	}
	/**
	 * @param strToDate the strToDate to set
	 */
	public void setStrToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	/**
	 * @return the distType
	 */
	public String getDistType() {
		return distType;
	}
	/**
	 * @param distType the distType to set
	 */
	public void setDistType(String distType) {
		this.distType = distType;
	}
	/**
	 * @return the emailType
	 */
	public String getEmailType() {
		return emailType;
	}
	/**
	 * @param emailType the emailType to set
	 */
	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the strRepString
	 */
	public String getStrRepString() {
		return strRepString;
	}
	/**
	 * @param strRepString the strRepString to set
	 */
	public void setStrRepString(String strRepString) {
		this.strRepString = strRepString;
	}
	/**
	 * @return the alEmail
	 */
	public ArrayList getAlEmail() {
		return alEmail;
	}
	/**
	 * @param alEmail the alEmail to set
	 */
	public void setAlEmail(ArrayList alEmail) {
		this.alEmail = alEmail;
	}
	/**
	 * @return the alDistributorType
	 */
	public ArrayList getAlDistributorType() {
		return alDistributorType;
	}
	/**
	 * @param alDistributorType the alDistributorType to set
	 */
	public void setAlDistributorType(ArrayList alDistributorType) {
		this.alDistributorType = alDistributorType;
	}
	/**
	 * @return the alBODetails
	 */
	public ArrayList getAlBODetails() {
		return alBODetails;
	}
	/**
	 * @param alBODetails the alBODetails to set
	 */
	public void setAlBODetails(ArrayList alBODetails) {
		this.alBODetails = alBODetails;
	}
	/**
	 * @return the hInputString
	 */
	public String gethInputString() {
		return hInputString;
	}
	/**
	 * @param hInputString the hInputString to set
	 */
	public void sethInputString(String hInputString) {
		this.hInputString = hInputString;
	}
	
}
