package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmLogForm;

public class GmIHItemRequestForm extends GmLogForm{
	
	private String reqtype ="";
	private String purpose ="";
	private String status ="";
	private String fromDt = "";
	private String toDt = "";
	public  String xmlGridData = "";
	private String chooseAction ="";
	private String requestID ="";
	private String requestTypeID ="";
	private String cname ="";
	private Date   cdate = new Date();
	private String replenish = "";
	private String inHouseTxns = "";
	private String txns = "";
	private String strFGString = "";
	private String strBLString = "";
	private String strIHString = "";
	private String strVDString = "";
	private String strTagID = "";
	
	private ArrayList alReqtype = new ArrayList();
	private ArrayList alPurpose = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alChooseAction = new ArrayList();
	private ArrayList alReplenish = new  ArrayList();
	private ArrayList alPartNmDtl = new ArrayList();
	
	
      /**
     * @return the strTagID
     */
    public String getStrTagID() {
      return strTagID;
    }
    /**
     * @param strTagID the strTagID to set
     */
    public void setStrTagID(String strTagID) {
      this.strTagID = strTagID;
    }	
	
	
	private String hStatus = "";
	
	public String gethStatus() {
		return hStatus;
	}
	public void sethStatus(String hStatus) {
		this.hStatus = hStatus;
	}
	/**
	 * @return the reqtype
	 */
	public String getReqtype() {
		return reqtype;
	}
	/**
	 * @param reqtype the reqtype to set
	 */
	public void setReqtype(String reqtype) {
		this.reqtype = reqtype;
	}
	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}
	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the fromDt
	 */
	public String getFromDt() {
		return fromDt;
	}
	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}
	/**
	 * @return the toDt
	 */
	public String getToDt() {
		return toDt;
	}
	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(String toDt) {
		this.toDt = toDt;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the chooseAction
	 */
	public String getChooseAction() {
		return chooseAction;
	}
	/**
	 * @param chooseAction the chooseAction to set
	 */
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}
	/**
	 * @return the requestID
	 */
	public String getRequestID() {
		return requestID;
	}
	/**
	 * @param requestID the requestID to set
	 */
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the requestTypeID
	 */
	public String getRequestTypeID() {
		return requestTypeID;
	}
	/**
	 * @param requestTypeID the requestTypeID to set
	 */
	public void setRequestTypeID(String requestTypeID) {
		this.requestTypeID = requestTypeID;
	}
	/**
	 * @return the cname
	 */
	public String getCname() {
		return cname;
	}
	/**
	 * @param cname the cname to set
	 */
	public void setCname(String cname) {
		this.cname = cname;
	}
	/**
	 * @return the cdate
	 */
	public Date getCdate() {
		return cdate;
	}
	/**
	 * @param cdate the cdate to set
	 */
	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}
	/**
	 * @return the replenish
	 */
	public String getReplenish() {
		return replenish;
	}
	/**
	 * @param replenish the replenish to set
	 */
	public void setReplenish(String replenish) {
		this.replenish = replenish;
	}
	/**
	 * @return the strFGString
	 */
	public String getStrFGString() {
		return strFGString;
	}
	/**
	 * @param strFGString the strFGString to set
	 */
	public void setStrFGString(String strFGString) {
		this.strFGString = strFGString;
	}
	/**
	 * @return the strBLString
	 */
	public String getStrBLString() {
		return strBLString;
	}
	/**
	 * @param strBLString the strBLString to set
	 */
	public void setStrBLString(String strBLString) {
		this.strBLString = strBLString;
	}
	/**
	 * @return the strIHString
	 */
	public String getStrIHString() {
		return strIHString;
	}
	/**
	 * @param strIHString the strIHString to set
	 */
	public void setStrIHString(String strIHString) {
		this.strIHString = strIHString;
	}
	/**
	 * @return the strVDString
	 */
	public String getStrVDString() {
		return strVDString;
	}
	/**
	 * @param strVDString the strVDString to set
	 */
	public void setStrVDString(String strVDString) {
		this.strVDString = strVDString;
	}
	/**
	 * @return the alReqtype
	 */
	public ArrayList getAlReqtype() {
		return alReqtype;
	}
	/**
	 * @param alReqtype the alReqtype to set
	 */
	public void setAlReqtype(ArrayList alReqtype) {
		this.alReqtype = alReqtype;
	}
	/**
	 * @return the alPurpose
	 */
	public ArrayList getAlPurpose() {
		return alPurpose;
	}
	/**
	 * @param alPurpose the alPurpose to set
	 */
	public void setAlPurpose(ArrayList alPurpose) {
		this.alPurpose = alPurpose;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alChooseAction
	 */
	public ArrayList getAlChooseAction() {
		return alChooseAction;
	}
	/**
	 * @param alChooseAction the alChooseAction to set
	 */
	public void setAlChooseAction(ArrayList alChooseAction) {
		this.alChooseAction = alChooseAction;
	}
	/**
	 * @return the alReplenish
	 */
	public ArrayList getAlReplenish() {
		return alReplenish;
	}
	/**
	 * @param alReplenish the alReplenish to set
	 */
	public void setAlReplenish(ArrayList alReplenish) {
		this.alReplenish = alReplenish;
	}
	/**
	 * @return the alPartNmDtl
	 */
	public ArrayList getAlPartNmDtl() {
		return alPartNmDtl;
	}
	/**
	 * @param alPartNmDtl the alPartNmDtl to set
	 */
	public void setAlPartNmDtl(ArrayList alPartNmDtl) {
		this.alPartNmDtl = alPartNmDtl;
	}
	/**
	 * @return the inHouseTxns
	 */
	public String getInHouseTxns() {
		return inHouseTxns;
	}
	/**
	 * @param inHouseTxns the inHouseTxns to set
	 */
	public void setInHouseTxns(String inHouseTxns) {
		this.inHouseTxns = inHouseTxns;
	}
	/**
	 * @return the txns
	 */
	public String getTxns() {
		return txns;
	}
	/**
	 * @param txns the txns to set
	 */
	public void setTxns(String txns) {
		this.txns = txns;
	}
	

}