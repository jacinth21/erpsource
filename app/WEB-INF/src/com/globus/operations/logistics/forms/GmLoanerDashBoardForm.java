package com.globus.operations.logistics.forms;

import com.globus.common.forms.GmCommonForm;

public class GmLoanerDashBoardForm extends GmCommonForm {
	
	private static final long serialVersionUID = 1L;
	
	private String open="";
	private String topick="";
	private String penship="";
	
	public String getOpen() {
		return open;
	}
	public void setOpen(String open) {
		this.open = open;
	}
	public String getTopick() {
		return topick;
	}
	public void setTopick(String topick) {
		this.topick = topick;
	}
	public String getPenship() {
		return penship;
	}
	public void setPenship(String penship) {
		this.penship = penship;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
