package com.globus.operations.logistics.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author ssharmila
 *
 */
public class GmLoanerSetAcceptanceRptForm extends GmCommonForm{

	 private static final long serialVersionUID = 1L;
	    
	  private String gridXmlData = "";
	    private String startDtFrom = "";
	    private String startDtTo = "";
	    private String strType = "";
	    private String strStatus = "";
	    private ArrayList alreturn = new ArrayList();
	    private ArrayList alType = new ArrayList();
	    private ArrayList alStatus = new ArrayList();
		/**
		 * @return the gridXmlData
		 */
		public String getGridXmlData() {
			return gridXmlData;
		}
		/**
		 * @param gridXmlData the gridXmlData to set
		 */
		public void setGridXmlData(String gridXmlData) {
			this.gridXmlData = gridXmlData;
		}
		/**
		 * @return the startDtFrom
		 */
		public String getStartDtFrom() {
			return startDtFrom;
		}
		/**
		 * @param startDtFrom the startDtFrom to set
		 */
		public void setStartDtFrom(String startDtFrom) {
			this.startDtFrom = startDtFrom;
		}
		/**
		 * @return the startDtTo
		 */
		public String getStartDtTo() {
			return startDtTo;
		}
		/**
		 * @param startDtTo the startDtTo to set
		 */
		public void setStartDtTo(String startDtTo) {
			this.startDtTo = startDtTo;
		}
		
		/**
		 * @return the serialversionuid
		 */
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		/**
		 * @return the alreturn
		 */
		public ArrayList getAlreturn() {
			return alreturn;
		}
		/**
		 * @param alreturn the alreturn to set
		 */
		public void setAlreturn(ArrayList alreturn) {
			this.alreturn = alreturn;
		}
		/**
		 * @return the alType
		 */
		public ArrayList getAlType() {
			return alType;
		}
		/**
		 * @param alType the alType to set
		 */
		public void setAlType(ArrayList alType) {
			this.alType = alType;
		}
		/**
		 * @return the alStatus
		 */
		public ArrayList getAlStatus() {
			return alStatus;
		}
		/**
		 * @param alStatus the alStatus to set
		 */
		public void setAlStatus(ArrayList alStatus) {
			this.alStatus = alStatus;
		}
		/**
		 * @return the strType
		 */
		public String getStrType() {
			return strType;
		}
		/**
		 * @param strType the strType to set
		 */
		public void setStrType(String strType) {
			this.strType = strType;
		}
		/**
		 * @return the strStatus
		 */
		public String getStrStatus() {
			return strStatus;
		}
		/**
		 * @param strStatus the strStatus to set
		 */
		public void setStrStatus(String strStatus) {
			this.strStatus = strStatus;
		}

		
}

