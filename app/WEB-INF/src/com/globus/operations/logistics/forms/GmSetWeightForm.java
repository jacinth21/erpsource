package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.List;

//import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCommonForm;

public class GmSetWeightForm extends GmCommonForm 
{
    private String setId = "";
    private String setNm = "";
    private String setType = "";
    private String updatefl    = "";
    private String hinputStr   = "";
    private List returnReport  = new ArrayList();
    
    private ArrayList alSetType = new ArrayList();
    
	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}
	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}
	/**
	 * @return the setNm
	 */
	public String getSetNm() {
		return setNm;
	}
	/**
	 * @param setNm the setNm to set
	 */
	public void setSetNm(String setNm) {
		this.setNm = setNm;
	}
	public String getSetType() {
		return setType;
	}
	public void setSetType(String setType) {
		this.setType = setType;
	}
	/**
	 * @return the updatefl
	 */
	public String getUpdatefl() {
		return updatefl;
	}
	/**
	 * @param updatefl the updatefl to set
	 */
	public void setUpdatefl(String updatefl) {
		this.updatefl = updatefl;
	}
	/**
	 * @return the hinputStr
	 */
	public String getHinputStr() {
		return hinputStr;
	}
	/**
	 * @param hinputStr the hinputStr to set
	 */
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}
	/**
	 * @return the returnReport
	 */
	public List getReturnReport() {
		return returnReport;
	}
	/**
	 * @param returnReport the returnReport to set
	 */
	public void setReturnReport(List returnReport) {
		this.returnReport = returnReport;
	}
	public ArrayList getAlSetType() {
		return alSetType;
	}
	public void setAlSetType(ArrayList alSetType) {
		this.alSetType = alSetType;
	}
    
}
