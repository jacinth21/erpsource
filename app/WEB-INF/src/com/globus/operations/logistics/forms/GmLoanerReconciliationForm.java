package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmLoanerReconciliationForm extends GmCommonForm {

  private List lresult = null;
  private ArrayList alDist = new ArrayList();
  private ArrayList alStatus = new ArrayList();
  private ArrayList alType = new ArrayList();
  private ArrayList alQtyType = new ArrayList();
  private String qtType = "10059";
  private String dist = "";
  private String dtType = "";
  private String fromDt = "";
  private String toDt = "";
  private String setName = "";
  private String status = "1006442";
  private String hreqids = "";
  private String transType = "";
  private String loanerType = "";
  private String type = "4127";// loaner type
  private ArrayList alRepList = new ArrayList();
  private ArrayList alUSRepList = new ArrayList();
  private ArrayList alOUSList = new ArrayList();

  private String rep = "";
  private String requestId = "";
  private String searchdist = "";
  private String searchrep = "";
  private String loanTo = "";
  private String loanToName = "";
  private String usLoanToName = "";
  private String ousLoanToName = "";
  private String empLoanToName = "";

  private ArrayList alEmployeeList = new ArrayList();
  private ArrayList alEventLoanToList = new ArrayList();

  /**
   * @return the alOUSList
   */
  public ArrayList getAlOUSList() {
    return alOUSList;
  }

  /**
   * @param alOUSList the alOUSList to set
   */
  public void setAlOUSList(ArrayList alOUSList) {
    this.alOUSList = alOUSList;
  }


  public ArrayList getAlEventLoanToList() {
    return alEventLoanToList;
  }

  public void setAlEventLoanToList(ArrayList alEventLoanToList) {
    this.alEventLoanToList = alEventLoanToList;
  }

  public ArrayList getAlEmployeeList() {
    return alEmployeeList;
  }

  public void setAlEmployeeList(ArrayList alEmployeeList) {
    this.alEmployeeList = alEmployeeList;
  }

  public String getLoanTo() {
    return loanTo;
  }

  public void setLoanTo(String loanTo) {
    this.loanTo = loanTo;
  }

  public String getLoanToName() {
    return loanToName;
  }

  public void setLoanToName(String loanToName) {
    this.loanToName = loanToName;
  }

  public String getRep() {
    return rep;
  }

  public void setRep(String rep) {
    this.rep = rep;
  }

  public ArrayList getAlRepList() {
    return alRepList;
  }

  public void setAlRepList(ArrayList alRepList) {
    this.alRepList = alRepList;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  private String gridData = "";

  /**
   * @return the type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * @return the setName
   */
  public String getSetName() {
    return setName;
  }

  /**
   * @param setName the setName to set
   */
  public void setSetName(String setName) {
    this.setName = setName;
  }

  /**
   * @return the lresult
   */
  public List getLresult() {
    return lresult;
  }

  /**
   * @param lresult the lresult to set
   */
  public void setLresult(List lresult) {
    this.lresult = lresult;
  }

  /**
   * @return the alDist
   */
  public ArrayList getAlDist() {
    return alDist;
  }

  /**
   * @param alDist the alDist to set
   */
  public void setAlDist(ArrayList alDist) {
    this.alDist = alDist;
  }

  /**
   * @return the dist
   */
  public String getDist() {
    return dist;
  }

  /**
   * @param dist the dist to set
   */
  public void setDist(String dist) {
    this.dist = dist;
  }

  /**
   * @return the fromDt
   */
  public String getFromDt() {
    return fromDt;
  }

  /**
   * @param fromDt the fromDt to set
   */
  public void setFromDt(String fromDt) {
    this.fromDt = fromDt;
  }

  /**
   * @return the toDt
   */
  public String getToDt() {
    return toDt;
  }

  /**
   * @param toDt the toDt to set
   */
  public void setToDt(String toDt) {
    this.toDt = toDt;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the dtType
   */
  public String getDtType() {
    return dtType;
  }

  /**
   * @param dtType the dtType to set
   */
  public void setDtType(String dtType) {
    this.dtType = dtType;
  }

  /**
   * @return the hreqids
   */
  public String getHreqids() {
    return hreqids;
  }

  /**
   * @param hreqids the hreqids to set
   */
  public void setHreqids(String hreqids) {
    this.hreqids = hreqids;
  }

  /**
   * @return the alType
   */
  public ArrayList getAlType() {
    return alType;
  }

  /**
   * @param alType the alType to set
   */
  public void setAlType(ArrayList alType) {
    this.alType = alType;
  }

  /**
   * @return the requestId
   */
  public String getRequestId() {
    return requestId;
  }

  /**
   * @param requestId the requestId to set
   */
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  /**
   * @return the alQtyType
   */
  public ArrayList getAlQtyType() {
    return alQtyType;
  }

  /**
   * @param alQtyType the alQtyType to set
   */
  public void setAlQtyType(ArrayList alQtyType) {
    this.alQtyType = alQtyType;
  }

  /**
   * @return the qtType
   */
  public String getQtType() {
    return qtType;
  }

  /**
   * @param qtType the qtType to set
   */
  public void setQtType(String qtType) {
    this.qtType = qtType;
  }

  /**
   * @return the transType
   */
  public String getTransType() {
    return transType;
  }

  /**
   * @param transType the transType to set
   */
  public void setTransType(String transType) {
    this.transType = transType;
  }

  /**
   * @return the loanerType
   */
  public String getLoanerType() {
    return loanerType;
  }

  /**
   * @param loanerType the loanerType to set
   */
  public void setLoanerType(String loanerType) {
    this.loanerType = loanerType;
  }

  /**
   * @return the alUSRepList
   */
  public ArrayList getAlUSRepList() {
    return alUSRepList;
  }

  /**
   * @param alUSRepList the alUSRepList to set
   */
  public void setAlUSRepList(ArrayList alUSRepList) {
    this.alUSRepList = alUSRepList;
  }

  public String getUsLoanToName() {
    return usLoanToName;
  }

  public void setUsLoanToName(String usLoanToName) {
    this.usLoanToName = usLoanToName;
  }

  public String getOusLoanToName() {
    return ousLoanToName;
  }

  public void setOusLoanToName(String ousLoanToName) {
    this.ousLoanToName = ousLoanToName;
  }

  public String getEmpLoanToName() {
    return empLoanToName;
  }

  public void setEmpLoanToName(String empLoanToName) {
    this.empLoanToName = empLoanToName;
  }

public String getSearchdist() {
	return searchdist;
}

public void setSearchdist(String searchdist) {
	this.searchdist = searchdist;
}

public String getSearchrep() {
	return searchrep;
}

public void setSearchrep(String searchrep) {
	this.searchrep = searchrep;
}

}
