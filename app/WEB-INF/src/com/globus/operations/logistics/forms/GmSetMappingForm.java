package com.globus.operations.logistics.forms;

import java.util.ArrayList;


import com.globus.common.forms.GmCommonForm;

public class GmSetMappingForm extends GmCommonForm {
	private String loanersetid = "";
	private String consignsetid = "";
	private String crdate = "";
	private String crby = "";
	private String gridXmlData = "";
	private ArrayList lresult = null;
	
	//For LoanerSetId get the all value in drop down
	private ArrayList alresultLSetId = new ArrayList(); 
	
	//For ConsignSetId get the all value in drop down
	private ArrayList alresultCSetId = new ArrayList();
	
	
	
	/**
	 * @return the alresultLSetId
	*/
	public ArrayList getAlresultLSetId() {
		return alresultLSetId;
	}

	/**
	 * @param alresultLSetId the alresultLSetId to set
	 */
	public void setAlresultLSetId(ArrayList alresultLSetId) {
		this.alresultLSetId = alresultLSetId;
	}

	/**
	 * @return the alresultCSetId
	 */
	public ArrayList getAlresultCSetId() {
		return alresultCSetId;
	}

	/**
	 * @param alresultCSetId the alresultCSetId to set
	 */
	public void setAlresultCSetId(ArrayList alresultCSetId) {
		this.alresultCSetId = alresultCSetId;
	}

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}


	/**
	 * @return the loanersetid
	 */
	public String getLoanersetid() {
		return loanersetid;
	}

	/**
	 * @param loanersetid the loanersetid to set
	 */
	public void setLoanersetid(String loanersetid) {
		this.loanersetid = loanersetid;
	}

	/**
	 * @return the consignsetid
	 */
	public String getConsignsetid() {
		return consignsetid;
	}

	/**
	 * @param consignsetid the consignsetid to set
	 */
	public void setConsignsetid(String consignsetid) {
		this.consignsetid = consignsetid;
	}

	/**
	 * @return the crdate
	 */
	public String getCrdate() {
		return crdate;
	}

	/**
	 * @param crdate the crdate to set
	 */
	public void setCrdate(String crdate) {
		this.crdate = crdate;
	}

	/**
	 * @return the crby
	 */
	public String getCrby() {
		return crby;
	}

	/**
	 * @param crby the crby to set
	 */
	public void setCrby(String crby) {
		this.crby = crby;
	}

	/**
	 * @return the lresult
	 */
	public ArrayList getLresult() {
		return lresult;
	}

	/**
	 * @param lresult the lresult to set
	 */
	public void setLresult(ArrayList lresult) {
		this.lresult = lresult;
	}



}
