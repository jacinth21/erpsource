package com.globus.operations.logistics.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
* GmLotTrackTxnLogRptForm : Contains the methods used for the Lot Track Transaction Log Report screen
* author : Agilan Singaravel
*/
public class GmLotTrackTxnLogRptForm extends GmCommonForm {
	
	private String strLotNumber = "";
	private String strPartNumber = "";
	private String warehouseType = "";
	private String strPartLiteral = "";
	private String expiryDateFrom = "";
	private String expiryDateTo = "";
	private String gridXmlData = "";
	private String strLocationId = "";
	private String strControlInvId = "";
	private String companyId= "";
	private String plantId = "";
	private String strLocation = "";
	private String strExpired = "";
	private ArrayList alWarehouseType = new ArrayList();
	private ArrayList alPartSearch = new ArrayList();
	/**
	 * @return the strLotNumber
	 */
	public String getStrLotNumber() {
		return strLotNumber;
	}
	/**
	 * @param strLotNumber the strLotNumber to set
	 */
	public void setStrLotNumber(String strLotNumber) {
		this.strLotNumber = strLotNumber;
	}
	/**
	 * @return the strPartNumber
	 */
	public String getStrPartNumber() {
		return strPartNumber;
	}
	/**
	 * @param strPartNumber the strPartNumber to set
	 */
	public void setStrPartNumber(String strPartNumber) {
		this.strPartNumber = strPartNumber;
	}
	/**
	 * @return the warehouseType
	 */
	public String getWarehouseType() {
		return warehouseType;
	}
	/**
	 * @param warehouseType the warehouseType to set
	 */
	public void setWarehouseType(String warehouseType) {
		this.warehouseType = warehouseType;
	}
	/**
	 * @return the expiryDateFrom
	 */
	public String getExpiryDateFrom() {
		return expiryDateFrom;
	}
	/**
	 * @param expiryDateFrom the expiryDateFrom to set
	 */
	public void setExpiryDateFrom(String expiryDateFrom) {
		this.expiryDateFrom = expiryDateFrom;
	}
	/**
	 * @return the expiryDateTo
	 */
	public String getExpiryDateTo() {
		return expiryDateTo;
	}
	/**
	 * @param expiryDateTo the expiryDateTo to set
	 */
	public void setExpiryDateTo(String expiryDateTo) {
		this.expiryDateTo = expiryDateTo;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the strLocationId
	 */
	public String getStrLocationId() {
		return strLocationId;
	}
	/**
	 * @param strLocationId the strLocationId to set
	 */
	public void setStrLocationId(String strLocationId) {
		this.strLocationId = strLocationId;
	}
	/**
	 * @return the strPartLiteral
	 */
	public String getStrPartLiteral() {
		return strPartLiteral;
	}
	/**
	 * @param strPartLiteral the strPartLiteral to set
	 */
	public void setStrPartLiteral(String strPartLiteral) {
		this.strPartLiteral = strPartLiteral;
	}
	/**
	 * @return the alWarehouseType
	 */
	public ArrayList getAlWarehouseType() {
		return alWarehouseType;
	}
	/**
	 * @param alWarehouseType the alWarehouseType to set
	 */
	public void setAlWarehouseType(ArrayList alWarehouseType) {
		this.alWarehouseType = alWarehouseType;
	}
	/**
	 * @return the alPartSearch
	 */
	public ArrayList getAlPartSearch() {
		return alPartSearch;
	}
	/**
	 * @param alPartSearch the alPartSearch to set
	 */
	public void setAlPartSearch(ArrayList alPartSearch) {
		this.alPartSearch = alPartSearch;
	}
	/**
	 * @return the strControlInvId
	 */
	public String getStrControlInvId() {
		return strControlInvId;
	}
	/**
	 * @param strControlInvId the strControlInvId to set
	 */
	public void setStrControlInvId(String strControlInvId) {
		this.strControlInvId = strControlInvId;
	}
	/**
	 * @return the companyId
	 */
	public String getCompanyId() {
		return companyId;
	}
	/**
	 * @param companyId the companyId to set
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	/**
	 * @return the plantId
	 */
	public String getPlantId() {
		return plantId;
	}
	/**
	 * @param plantId the plantId to set
	 */
	public void setPlantId(String plantId) {
		this.plantId = plantId;
	}
	/**
	 * @return the strLocation
	 */
	public String getStrLocation() {
		return strLocation;
	}
	/**
	 * @param strLocation the strLocation to set
	 */
	public void setStrLocation(String strLocation) {
		this.strLocation = strLocation;
	}
	/**
	 * @return the strExpired
	 */
	public String getStrExpired() {
		return strExpired;
	}
	/**
	 * @param strExpired the strExpired to set
	 */
	public void setStrExpired(String strExpired) {
		this.strExpired = strExpired;
	}
	
}
