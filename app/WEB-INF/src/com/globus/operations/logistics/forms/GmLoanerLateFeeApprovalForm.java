package com.globus.operations.logistics.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmLoanerLateFeeApprovalForm extends GmCommonForm{
	private ArrayList alStatus= new ArrayList();
	private ArrayList alReturn = new ArrayList();
	private ArrayList alStatustListRpt = new ArrayList();
	private ArrayList alWaiveReason = new ArrayList();
	
	private ArrayList alZone = new ArrayList();
	private ArrayList alRegions = new ArrayList();
	private ArrayList alDivision = new ArrayList();
	
	private String dist = "";
	private String rep = "";
	private String status = "110501";
	private String incident = "";
	private String chargesFor = "92068"; //Late Fee(By Set)
	private String fromDate = "";
	private String toDate = "";
	private String updateString = "";
	private String shwDtl="";
	private String shwDist = "";
	private String shwDirRep = "";
	private String requestID = ""; // PMT-8847
	private String incidentId = "";
	private String chargeFromDate = "";
	private String chargeToDate = "";
	private String creditAmount = "";
	private String creditAmt = "";
	private String creditNotes = "";
	private String creditDate = "";
	private String chargeDetailId = "";
	private String inputString = "";
	private String strPOHtml = "";
	private String strReasonHtml = "";
	private String strAvailAmount = "";
//	redis
	private String salesRep = "";
	private String searchsalesRep = "";
	private String fieldSales = "";
	private String searchfieldSales = "";
	private String strAmountAccess="";
	private String strCreditAccess = "";
	private String strNotesAccess = "";
	//access flag
	private String amounteventaccfl="";
	private String statuseventaccfl="";
	private String holdeventaccfl="";
	private String crediteventaccfl="";
	private String noteseventaccfl="";
	private String zone="";
	private String region="";
	private String division ="";
	
	
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public ArrayList getAlDivision() {
		return alDivision;
	}
	public void setAlDivision(ArrayList alDivision) {
		this.alDivision = alDivision;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public ArrayList getAlZone() {
		return alZone;
	}
	public void setAlZone(ArrayList alZone) {
		this.alZone = alZone;
	}
	public ArrayList getAlRegions() {
		return alRegions;
	}
	public void setAlRegions(ArrayList alRegions) {
		this.alRegions = alRegions;
	}
	
	public String getStrReasonHtml() {
		return strReasonHtml;
	}
	public void setStrReasonHtml(String strReasonHtml) {
		this.strReasonHtml = strReasonHtml;
	}
	public ArrayList getAlWaiveReason() {
		return alWaiveReason;
	}
	public void setAlWaiveReason(ArrayList alWaiveReason) {
		this.alWaiveReason = alWaiveReason;
	}
	public String getStrPOHtml() {
		return strPOHtml;
	}
	public void setStrPOHtml(String strPOHtml) {
		this.strPOHtml = strPOHtml;
	}
	public ArrayList getAlStatustListRpt() {
		return alStatustListRpt;
	}
	public void setAlStatustListRpt(ArrayList alStatustListRpt) {
		this.alStatustListRpt = alStatustListRpt;
	}
	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}
	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the amounteventaccfl
	 */
	public String getAmounteventaccfl() {
		return amounteventaccfl;
	}
	/**
	 * @param amounteventaccfl the amounteventaccfl to set
	 */
	public void setAmounteventaccfl(String amounteventaccfl) {
		this.amounteventaccfl = amounteventaccfl;
	}
	/**
	 * @return the statuseventaccfl
	 */
	public String getStatuseventaccfl() {
		return statuseventaccfl;
	}
	/**
	 * @param statuseventaccfl the statuseventaccfl to set
	 */
	public void setStatuseventaccfl(String statuseventaccfl) {
		this.statuseventaccfl = statuseventaccfl;
	}
	/**
	 * @return the holdeventaccfl
	 */
	public String getHoldeventaccfl() {
		return holdeventaccfl;
	}
	/**
	 * @param holdeventaccfl the holdeventaccfl to set
	 */
	public void setHoldeventaccfl(String holdeventaccfl) {
		this.holdeventaccfl = holdeventaccfl;
	}
	/**
	 * @return the crediteventaccfl
	 */
	public String getCrediteventaccfl() {
		return crediteventaccfl;
	}
	/**
	 * @param crediteventaccfl the crediteventaccfl to set
	 */
	public void setCrediteventaccfl(String crediteventaccfl) {
		this.crediteventaccfl = crediteventaccfl;
	}
	/**
	 * @return the noteseventaccfl
	 */
	public String getNoteseventaccfl() {
		return noteseventaccfl;
	}
	/**
	 * @param noteseventaccfl the noteseventaccfl to set
	 */
	public void setNoteseventaccfl(String noteseventaccfl) {
		this.noteseventaccfl = noteseventaccfl;
	}
	/**
	 * @return the strCreditAccess
	 */
	public String getStrCreditAccess() {
		return strCreditAccess;
	}
	/**
	 * @param strCreditAccess the strCreditAccess to set
	 */
	public void setStrCreditAccess(String strCreditAccess) {
		this.strCreditAccess = strCreditAccess;
	}
	/**
	 * @return the strNotesAccess
	 */
	public String getStrNotesAccess() {
		return strNotesAccess;
	}
	/**
	 * @param strNotesAccess the strNotesAccess to set
	 */
	public void setStrNotesAccess(String strNotesAccess) {
		this.strNotesAccess = strNotesAccess;
	}
	/**
	 * @return the strStatusAccess
	 */
	public String getStrStatusAccess() {
		return strStatusAccess;
	}
	/**
	 * @param strStatusAccess the strStatusAccess to set
	 */
	public void setStrStatusAccess(String strStatusAccess) {
		this.strStatusAccess = strStatusAccess;
	}
	/**
	 * @return the strHoldAccess
	 */
	public String getStrHoldAccess() {
		return strHoldAccess;
	}
	/**
	 * @param strHoldAccess the strHoldAccess to set
	 */
	public void setStrHoldAccess(String strHoldAccess) {
		this.strHoldAccess = strHoldAccess;
	}
	private String strStatusAccess = "";
	private String strHoldAccess = "";
	private String SessPartyId = "";
	private String accessfl="";
	//security event
	private String chargedtlId = "";
	//access check
	private String amount = "";
	private String userId = "";
	private String comment = "";
	private String credit = "";
	private String hold = "";
	//status dropdown
	private String statuslist = "";
//	private int amount;
	private int chargeDetailsId;
	private String notes;
	
	private String rowSize = "";
	
	
	
	public String getRowSize() {
		return rowSize;
	}
	public void setRowSize(String rowSize) {
		this.rowSize = rowSize;
	}
	public String getAmount() {
		return amount;
	}
	public String getStrAmountAccess() {
		return strAmountAccess;
	}
	public void setStrAmountAccess(String strAmountAccess) {
		this.strAmountAccess = strAmountAccess;
	}
	public String getSessPartyId() {
		return SessPartyId;
	}
	public void setSessPartyId(String sessPartyId) {
		SessPartyId = sessPartyId;
	}
	public String getAccessfl() {
		return accessfl;
	}
	public void setAccessfl(String accessfl) {
		this.accessfl = accessfl;
	}
	public String getChargedtlId() {
		return chargedtlId;
	}
	public void setChargedtlId(String chargedtlId) {
		this.chargedtlId = chargedtlId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public String getHold() {
		return hold;
	}
	public void setHold(String hold) {
		this.hold = hold;
	}
	public String getStatuslist() {
		return statuslist;
	}
	public void setStatuslist(String statuslist) {
		this.statuslist = statuslist;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getInputString() {
		return inputString;
	}
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	public String getChargeDetailId() {
		return chargeDetailId;
	}
	public void setChargeDetailId(String chargeDetailId) {
		this.chargeDetailId = chargeDetailId;
	}
	public String getCreditDate() {
		return creditDate;
	}
	public void setCreditDate(String creditDate) {
		this.creditDate = creditDate;
	}
	public String getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}
	public String getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(String creditAmt) {
		this.creditAmt = creditAmt;
	}
	public String getCreditNotes() {
		return creditNotes;
	}
	public void setCreditNotes(String creditNotes) {
		this.creditNotes = creditNotes;
	}
	public String getChargeFromDate() {
		return chargeFromDate;
	}
	public void setChargeFromDate(String chargeFromDate) {
		this.chargeFromDate = chargeFromDate;
	}
	public String getChargeToDate() {
		return chargeToDate;
	}
	public void setChargeToDate(String chargeToDate) {
		this.chargeToDate = chargeToDate;
	}
	public String getIncidentId() {
		return incidentId;
	}
	public void setIncidentId(String incidentId) {
		this.incidentId = incidentId;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getChargeDetailsId() {
		return chargeDetailsId;
	}
	public void setChargeDetailsId(int chargeDetailsId) {
		this.chargeDetailsId = chargeDetailsId;
	}
	public String getSalesRep() {
		return salesRep;
	}
	public void setSalesRep(String salesRep) {
		this.salesRep = salesRep;
	}
	public String getSearchsalesRep() {
		return searchsalesRep;
	}
	public void setSearchsalesRep(String searchsalesRep) {
		this.searchsalesRep = searchsalesRep;
	}
	public String getFieldSales() {
		return fieldSales;
	}
	public void setFieldSales(String fieldSales) {
		this.fieldSales = fieldSales;
	}
	public String getSearchfieldSales() {
		return searchfieldSales;
	}
	public void setSearchfieldSales(String searchfieldSales) {
		this.searchfieldSales = searchfieldSales;
	}
	
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	public String getRep() {
		return rep;
	}
	public void setRep(String rep) {
		this.rep = rep;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIncident() {
		return incident;
	}
	public void setIncident(String incident) {
		this.incident = incident;
	}
	public String getChargesFor() {
		return chargesFor;
	}
	public void setChargesFor(String chargesFor) {
		this.chargesFor = chargesFor;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getUpdateString() {
		return updateString;
	}
	public void setUpdateString(String updateString) {
		this.updateString = updateString;
	}
	public String getShwDtl() {
		return shwDtl;
	}
	public void setShwDtl(String shwDtl) {
		this.shwDtl = shwDtl;
	}
	public String getShwDist() {
		return shwDist;
	}
	public void setShwDist(String shwDist) {
		this.shwDist = shwDist;
	}
	public String getShwDirRep() {
		return shwDirRep;
	}
	public void setShwDirRep(String shwDirRep) {
		this.shwDirRep = shwDirRep;
	}
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * @return the strAvailAmount
	 */
	public String getStrAvailAmount() {
		return strAvailAmount;
	}
	/**
	 * @param strAvailAmount the strAvailAmount to set
	 */
	public void setStrAvailAmount(String strAvailAmount) {
		this.strAvailAmount = strAvailAmount;
	}
	
	
	
}
