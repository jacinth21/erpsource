package com.globus.operations.logistics.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmSetPrioritizationForm extends GmLogForm {

	String consignmentId = "";
	String setName = "";
	String currentPriority = "";
	String currentPriorityid = "";
	String setid = "";
	String strOpt = "";
	String changepriority="";
	
	/**
	 * @return the strOpt
	 */
	public String getStrOpt() {
		return strOpt;
	}
	/**
	 * @param strOpt the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}
	/**
	 * @return the changepriority
	 */
	public String getChangepriority() {
		return changepriority;
	}
	/**
	 * @param changepriority the changepriority to set
	 */
	public void setChangepriority(String changepriority) {
		this.changepriority = changepriority;
	}
	ArrayList alSetPriority = new ArrayList();
	private ArrayList alLogReasons = new ArrayList();
	
    /**
	* @return the setid
    */
	public String getSetid() {
		return setid;
	}
    /**
	* @param setid
    * the setid to set
	*/
	public void setSetid(String setid) {
		this.setid = setid;
	}
    /**
    * @return the currentPriorityid
	*/
	public String getCurrentPriorityid() {
		return currentPriorityid;
	}
    /**
	* @param currentPriorityid
	* the currentPriorityid to set
	*/
	public void setCurrentPriorityid(String currentPriorityid) {
		this.currentPriorityid = currentPriorityid;
	}
    /**
	* @return the alLogReasons
	*/
	public ArrayList getAlLogReasons() {
		return alLogReasons;
	}
    /**
	 * @param alLogReasons
	 * the alLogReasons to set
	 */
	public void setAlLogReasons(ArrayList alLogReasons) {
		this.alLogReasons = alLogReasons;
	}

	public String getConsignmentId() {
		return consignmentId;
	}

	public void setConsignmentId(String consignmentId) {
		this.consignmentId = consignmentId;
	}

	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName;
	}

	public String getCurrentPriority() {
		return currentPriority;
	}

	public void setCurrentPriority(String currentPriority) {
		this.currentPriority = currentPriority;
	}

	/**
    * @return the alSetPriority
	*/
	public ArrayList getAlSetPriority() {
		return alSetPriority;
	}
    /**
	* @param alSetPriority
	* the alSetPriority to set
	*/
	public void setAlSetPriority(ArrayList alSetPriority) {
		this.alSetPriority = alSetPriority;
	}

}
