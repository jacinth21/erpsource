package com.globus.operations.logistics.forms;

import com.globus.common.forms.GmLogForm;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
public class GmKitMappingReportForm extends GmLogForm{
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private String kitId = "";
	private String kitNm = "";
	private String activeFl = "";
	private String setId = "";
	private String setNm = "";
	private String tagId = "";
	private String txtComments = "";
	private String image = "";
	private String gridXmlData = "";
	private String searchkitId = "";
	private String searchsetId = "";
	private String caseId = "";
	private ArrayList alList =new ArrayList();
	public ArrayList getAlList() {
		return alList;
	}

	public void setAlList(ArrayList alList) {
		this.alList = alList;
	}

	public String getKitId() {
		return kitId;
	}

	public void setKitId(String kitId) {
		this.kitId = kitId;
	}

	public String getKitNm() {
		return kitNm;
	}

	public void setKitNm(String kitNm) {
		this.kitNm = kitNm;
	}

	public String getActiveFl() {
		return activeFl;
	}

	public void setActiveFl(String activeFl) {
		this.activeFl = activeFl;
	}

	public String getSetId() {
		return setId;
	}

	public void setSetId(String setId) {
		this.setId = setId;
	}

	public String getSetNm() {
		return setNm;
	}

	public void setSetNm(String setNm) {
		this.setNm = setNm;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public String getTxtComments() {
		return txtComments;
	}

	public void setTxtComments(String txtComments) {
		this.txtComments = txtComments;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	public String getSearchkitId() {
		return searchkitId;
	}

	public void setSearchkitId(String searchkitId) {
		this.searchkitId = searchkitId;
	}

	public String getSearchsetId() {
		return searchsetId;
	}

	public void setSearchsetId(String searchsetId) {
		this.searchsetId = searchsetId;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
}
