package com.globus.operations.logistics.forms;


import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * GmConsignmentLocationMapRptForm - this action used to get and set the values for WIP Set Print Location Report screen
 * 
 * @author rselvaraj
 *
 */
public class GmConsignmentLocationMapRptForm extends GmCommonForm{
	
	private static final long serialVersionUID = 1L;

	private String zoneID="";
	private String invTypeID="";
	private String warehouseID="";
	private String buildingID="";
	private String locationID="";
	private String cnID="";
	private String statusID="";
	private String saveFl="";
	private String aisleNum="";
	private String shelf="";

	private ArrayList alZones = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alInvLocTypes = new ArrayList();
	private ArrayList alWareHouse = new ArrayList();
	private ArrayList alBuilding = new ArrayList();
	private ArrayList alInvLocList = new ArrayList();
	
	
	
	/**
	 * @return the saveFl
	 */
	public String getSaveFl() {
		return saveFl;
	}
	/**
	 * @param saveFl the saveFl to set
	 */
	public void setSaveFl(String saveFl) {
		this.saveFl = saveFl;
	}
	/**
	 * @return the alInvLocList
	 */
	public ArrayList getAlInvLocList() {
		return alInvLocList;
	}
	/**
	 * @param alInvLocList the alInvLocList to set
	 */
	public void setAlInvLocList(ArrayList alInvLocList) {
		this.alInvLocList = alInvLocList;
	}
	/**
	 * @return the alZones
	 */
	public ArrayList getAlZones() {
		return alZones;
	}
	/**
	 * @param alZones the alZones to set
	 */
	public void setAlZones(ArrayList alZones) {
		this.alZones = alZones;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alInvLocTypes
	 */
	public ArrayList getAlInvLocTypes() {
		return alInvLocTypes;
	}
	/**
	 * @param alInvLocTypes the alInvLocTypes to set
	 */
	public void setAlInvLocTypes(ArrayList alInvLocTypes) {
		this.alInvLocTypes = alInvLocTypes;
	}
	/**
	 * @return the alWareHouse
	 */
	public ArrayList getAlWareHouse() {
		return alWareHouse;
	}
	/**
	 * @param alWareHouse the alWareHouse to set
	 */
	public void setAlWareHouse(ArrayList alWareHouse) {
		this.alWareHouse = alWareHouse;
	}
	/**
	 * @return the alBuilding
	 */
	public ArrayList getAlBuilding() {
		return alBuilding;
	}
	/**
	 * @param alBuilding the alBuilding to set
	 */
	public void setAlBuilding(ArrayList alBuildingID) {
		this.alBuilding = alBuildingID;
	}
	/**
	 * @return the zoneID
	 */
	public String getZoneID() {
		return zoneID;
	}
	/**
	 * @param zoneID the zoneID to set
	 */
	public void setZoneID(String zoneID) {
		this.zoneID = zoneID;
	}
	/**
	 * @return the invTypeID
	 */
	public String getInvTypeID() {
		return invTypeID;
	}
	/**
	 * @param invTypeID the invTypeID to set
	 */
	public void setInvTypeID(String invTypeID) {
		this.invTypeID = invTypeID;
	}
	/**
	 * @return the warehouseID
	 */
	public String getWarehouseID() {
		return warehouseID;
	}
	/**
	 * @param warehouseID the warehouseID to set
	 */
	public void setWarehouseID(String warehouseID) {
		this.warehouseID = warehouseID;
	}
	/**
	 * @return the buildingID
	 */
	public String getBuildingID() {
		return buildingID;
	}
	/**
	 * @param buildingID the buildingID to set
	 */
	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}
	/**
	 * @return the locationID
	 */
	public String getLocationID() {
		return locationID;
	}
	/**
	 * @param locationID the locationID to set
	 */
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}
	/**
	 * @return the cnID
	 */
	public String getCnID() {
		return cnID;
	}
	/**
	 * @param cnID the cnID to set
	 */
	public void setCnID(String cnID) {
		this.cnID = cnID;
	}
	/**
	 * @return the statusID
	 */
	public String getStatusID() {
		return statusID;
	}
	/**
	 * @param statusID the statusID to set
	 */
	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}
	/**
	 * @return the aisleNum
	 */
	public String getAisleNum() {
		return aisleNum;
	}
	/**
	 * @param aisleNum the aisleNum to set
	 */
	public void setAisleNum(String aisleNum) {
		this.aisleNum = aisleNum;
	}
	/**
	 * @return the shelf
	 */
	public String getShelf() {
		return shelf;
	}
	/**
	 * @param shelf the shelf to set
	 */
	public void setShelf(String shelf) {
		this.shelf = shelf;
	}
	
}
