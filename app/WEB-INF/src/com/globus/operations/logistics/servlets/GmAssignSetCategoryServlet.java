package com.globus.operations.logistics.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;


public class GmAssignSetCategoryServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Setting up the Logging System

    instantiate(request, response);

    String strAction = "";
    String strLogReason = "";
    String strConsignId = "";
    String strEtchId = "";
    String strPurpose = "";

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strLogisticsPath = GmCommonClass.getString("GMLOGISTICS");

    // String strLogisticsPath = "/operations/logistics";
    String strDispatchTo = strLogisticsPath.concat("/GmAssignSetCategory.jsp");

    HashMap hmCONSIGNMENT = new HashMap();
    ArrayList alConsignSetDetails = new ArrayList();
    ArrayList alPurpose = new ArrayList();
    ArrayList alReturn = new ArrayList();
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
    GmInHouseSetsTransBean gmInHouseTxnBean = new GmInHouseSetsTransBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());


    try {
      String strUserId = (String) session.getAttribute("strSessUserId");

      strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
      if (strAction == "" || strAction.equals(null)) {
        strAction = "Load";
      }

      // Code for Adding Consignment Details starts
      strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
      // strConsignId = "GM-CN-11545";
      hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
      log.debug(" Value of Consignment is " + hmCONSIGNMENT);
      request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
      // Code for Adding Consignment Details Ends

      // Code for Adding Set Details of the Consignment starts
      alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
      log.debug(" Count of Set Consign details is " + alConsignSetDetails.size());
      request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
      // Code for Adding Set Details of the Consignment Ends

      log.debug(" Action is " + strAction);
      // Getting the List of InHouse Buckets
      alPurpose = gmCommon.getCodeList("INPRP");
      request.setAttribute("PURPOSE", alPurpose);
      log.debug(" Count of Purpose value is  " + alPurpose);

      // Adding Call Log Information
      strLogReason =
          request.getParameter("Txt_LogReason") == null ? "" : request
              .getParameter("Txt_LogReason");
      log.debug("StrLogReason is " + strLogReason + "Order Id is " + strConsignId);

      if (!strLogReason.equals("")) {
        gmCommonBean.saveLog(strConsignId, strLogReason, strUserId, "1206");
      }

      if (strAction.equals("Update")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        strEtchId = GmCommonClass.parseNull(request.getParameter("Txt_EtchId"));
        strPurpose = GmCommonClass.parseNull(request.getParameter("Cbo_Purpose"));

        log.debug(" Inside Update values ConsignId " + strConsignId + " Etch " + strEtchId
            + " Purpose " + strPurpose);
        gmInHouseTxnBean.updateConsignDetails(strConsignId, strEtchId, strPurpose);

        request.setAttribute("CONSIGNID", strConsignId);
        request.setAttribute("STRPURPOSE", strPurpose);
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
      }

      // Retrieving Comments information
      alReturn = gmCommonBean.getLog(strConsignId);
      request.setAttribute("hmLog", alReturn);
      log.debug("Size of alReturn is " + alReturn.size());

      dispatch(strDispatchTo, request, response);
    }



    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmAssignSetCategory

