/*****************************************************************************
 * File : GmLoanerAjaxServlet Version : 1.0
 ******************************************************************************/
package com.globus.operations.logistics.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.cart.beans.GmCommonCartBean;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmInvReportBean;

public class GmLoanerAjaxServlet extends GmServlet {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  private ServletContext context;

  String strComnPath = GmCommonClass.getString("GMCOMMON");

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    this.context = config.getServletContext();
  }

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    try {
      instantiate(request, response);
      GmCommonCartBean gmCart = new GmCommonCartBean(getGmDataStoreVO());
      GmInvReportBean gmInvReportBean = new GmInvReportBean(getGmDataStoreVO());
      StringBuffer sbXML = new StringBuffer();

      String strOpt = GmCommonClass.parseNull(request.getParameter("opt"));
      String strPartNum = GmCommonClass.parseNull(request.getParameter("partNum"));
      log.debug("strOpt " + strOpt);
      log.debug("strPartNum " + strPartNum);
      int intQty = 0;

      intQty = gmInvReportBean.getQtyForPart(strPartNum, strOpt);
      sbXML.append("<data>");
      sbXML.append("<pnum>");
      sbXML.append("<qty>");
      sbXML.append(intQty);
      sbXML.append("</qty>");
      sbXML.append("</pnum>");
      sbXML.append("</data>");

      response.setContentType("text/xml");
      response.setHeader("Cache-Control", "no-cache");

      PrintWriter writer = response.getWriter();
      writer.println(sbXML.toString());
      writer.close();

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      String strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }
}
