/*****************************************************************************
 * File : GmSetSummaryReportServlet Desc : This Servlet will be used to view set summary report on
 * cross tab format
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.logistics.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;

public class GmSetSummaryReportServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strLogisticsPath = GmCommonClass.getString("GMLOGISTICS");
    String strDispatchTo = strLogisticsPath.concat("/GmSetSummaryReport.jsp");

    String strHeader = "";
    String strJSPName = "";

    try {
      GmCommonClass gmCommon = new GmCommonClass();
      GmInHouseSetsReportBean gmSetCount = new GmInHouseSetsReportBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();

      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      String strAction = request.getParameter("hAction");
      // Default selected value
      String strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      GmResourceBundleBean gmResourceBundleBeanlbl =
          GmCommonClass.getResourceBundleBean(
              "properties.labels.operations.logistics.GmSetSummaryReport", strSessCompanyLocale);
      if (strAction == null) {
        strAction = (String) session.getAttribute("strSessOpt");
      }

      strHeader = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_INHOUSE"));
      hmReturn = gmSetCount.reportCrossTabSetDetails(strType);


      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("strHeader", strHeader);
      request.setAttribute("Cbo_Type", strType);

      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmTerritoryServlet
