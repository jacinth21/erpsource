package com.globus.operations.logistics.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmFileReader;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.dashboard.beans.GmOperDashBoardBean;
import com.globus.common.printwork.inhouseSet.beans.GmInhouseSetPrintInterface;
import com.globus.common.servlets.GmShippingServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.loaners.beans.GmLoanerLotReprocessBean;
import com.globus.operations.logistics.beans.GmIHLoanerItemBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.event.beans.GmEventReportBean;



public class GmInHouseSetServlet extends GmShippingServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	  instantiate(request, response);//PC-2233 Japanese Character search in Manage Loaner changes to Junk
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strLogisticsPath = GmCommonClass.getString("GMLOGISTICS");
    String strDispatchTo = strLogisticsPath.concat("/GmInHouseSetStatus.jsp");
    String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptSeq"));
    String strLoanerSet = GmCommonClass.getString("GMLNINSHEET");
    String strCategories = "";
    String strSetIds = "";
    String strStatusGrp = "";
    String strStatus = "";
    String strCboOpt = "";
    String strLoanTo = "";
    String strLoanToId = "";
    String strShipTo = "";
    String strShipToId = "";
    String strExpDate = "";
    String strShipCarr = "";
    String strShipMode = "";
    String strInputStr = "";
    String strTransId = "";
    String strEtchId = "";
    String strConId = "";
    String strOverdue = "";
    String strType = "";
    String strSetId = "";
    String strReqId = "";
    String strRepId = "";
    String strAccId = "";
    String strSetName = "";
    String strRequestID = "";
    String strDueDays = "";
    String strDistIds = "";
    String strDistributorId = "";
    String strMonth = "";
    String strTurnsMonth = "";
    String strInput = "";
    String strHoldTxn = "";
    String strDplyFl = "";
    String strCaseInfoID = "";
    String strUpdateAccess = "";
    String strActualRequestID = "";
    java.sql.Date dtLDate = null;
    java.sql.Date dtEDate = null;
    String strCompDetails = "";
    String strCompleteAddress = "";

    String strCompAddress = "";
    String strCompPhone = "";
    String strCompFax = "";
    String strCountryCode = "";
    String strCompanyName = "";
    String strJasperName = "";
    String strCompanyId = "";
    String strCustServPhone = "";
    String strCompanyPhone = "";
    String strCompanyShortNm = "Globus";
    String strCompNm = "Globus Medical";
    String strDivisionId = "";
    String strLoanerStatus = "";
    String strLoanerStatusFlag = "";
    String strLoanerPaperworkFlag = "";
    String strCompanyIdLogoFlag = "";
    String strLoanerRequestId = GmCommonClass.parseNull(request.getParameter("loanerReqId"));   // added strLoanerRequestId need value if the company is ITALY  - PMT-42950
    String strITCountryCode  = "";
    String strShowLSalesInDB = "";
    String strShowCnumLSS = "";
    HashMap hmJasperValues = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmParamDtls = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alReasonTypes = new ArrayList();
    ArrayList alEventName = new ArrayList();
    ArrayList alLoanerOpts = new ArrayList();
    HashMap hmCONSIGNMENT = new HashMap();
    // ArrayList alLEDetail = new ArrayList();
    // HashMap hmLEdetail = new HashMap();
    RowSetDynaClass rdReturn = null;
    ArrayList alConsignSetDetails = new ArrayList();
    String strConsignId = "";
    String strConsignType = "";
   
    
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
    GmInHouseSetsTransBean gmInHouseTransBean = new GmInHouseSetsTransBean(getGmDataStoreVO());
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
    GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean(getGmDataStoreVO());
    GmIHLoanerItemBean gmIHLoanerItemBean = new GmIHLoanerItemBean(getGmDataStoreVO());
    RowSetDynaClass rowSetRpt = null;
    GmEventReportBean gmEventReportBean = new GmEventReportBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean();
    GmLoanerLotReprocessBean gmLoanerLotReprocessBean = new GmLoanerLotReprocessBean(getGmDataStoreVO());
    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();
    
    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33513
    HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33513
    HashMap hmLoop = new HashMap();
    ArrayList alPendPictureCam = new ArrayList();

    String strCamProp = "";
    String strComments = "";
    String strCookieName = "";
    String strPendPicCameraId = "";
    String strSelectedCamera = "";
    String strtxnType_temp = "";
    String strDefaultComment = "";
    String strPriorityStr = "";

    // getting the company id
    String strPortalCompanyId = getGmDataStoreVO().getCmpid();
    String strCompanyLocale = "";
    strCompanyLocale = GmCommonClass.getCompanyLocale(strPortalCompanyId);
    // Locale
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean gmResourceBunBeanVal =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strPortalCompanyId, "");

    GmResourceBundleBean gmResourceBundleBeanComp =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strCompLNINMainSubDir = "";
    String strCompLNINMainDir =
        GmCommonClass.parseNull(gmResourceBundleBeanComp.getProperty("GMLNINSHEET"));
    String strCompLNINSubDir =
        GmCommonClass.parseNull(gmResourceBundleBeanComp.getProperty("GMLNINSHEET_SUB_DIR"));
    String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    
    strITCountryCode = GmCommonClass.parseNull((String) hmCompanyAddress.get("COUNTRYCODE"));     // added strITCountryCode need value if the company is ITALY  - PMT-42950  
    if ((!strCompLNINMainDir.equals("")) && (!strCompLNINSubDir.equals(""))) {
      strCompLNINMainSubDir = strCompLNINMainDir + strCompLNINSubDir;
    }

    try {
      // checkSession(response, session); // Checks if the current session
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      // is valid, else redirecting to
      // SessionExpiry page
      /*
       * The hmAccess related code is added for PMT-2145 The RECONCOMMENTACCESS is one security
       * event to access the Commnets icon in Manage Loaner screen for Pending Return status. this
       * event is enable for C/S people to enter the Reconciliation commnets
       */
      HashMap hmAccess = new HashMap();
      String strPartyId = "";
      String strCommentIconAccess = "";
      strPartyId = (String) session.getAttribute("strSessPartyId");
      strLoanerStatus = GmCommonClass.parseNull(request.getParameter("status"));
      
      strLoanerStatusFlag =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strLoanerStatus, "LOANER_STATUS"));
      strLoanerPaperworkFlag =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER_PAPERWORK_FLAG"));
      strCompanyIdLogoFlag =
          GmCommonClass.parseNull(gmResourceBunBeanVal.getProperty("COMPANY_ID_LOGO_FL"));
      hmAccess =
          GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
              "RECONCOMMENTACCESS")));
      strCommentIconAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("UPDFL", strCommentIconAccess);
      String strAction = request.getParameter("hAction");
      String strRepName = GmCommonClass.parseNull((String)request.getParameter("hSalesRepName"));
      String strSalesRepId = GmCommonClass.parseNull((String)request.getParameter("Cbo_RepId")); //added for PC-884
      request.setAttribute("REPNAME", strRepName);
      request.setAttribute("REPID", strSalesRepId);
      
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;
      log.debug("Str Action==> " + strAction);

      if (strAction.equals("EditLoad") || strAction.equals("ReturnSet")) {
        strCookieName = "cAcceptReturnCameraID_" + strUserId;
      } else if (strAction.equals("PICTURE") || strAction.equals("Process_Picture")) {
        strCookieName = "cPendPictureCameraID_" + strUserId;
      }
      // Get Camera ID from cookie
      if (!strCookieName.equals("")) {
        strCamProp =
            GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, strCookieName));
        strComments = !strCamProp.equals("") ? strCamProp : "";
      }
      String strOpt = request.getParameter("hOpt");

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      log.debug("Str Opt==> " + strOpt);

      // The following code added for Getting the Distributor, Sales Rep and Employee List based on
      // the PLANT too if the Selected Company is EDC
      strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid()); 
      String strCompanyPlant =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "MANAGELOANERHEADER"));

      HashMap hmFilters = new HashMap();
      hmFilters.put("FILTER", "Active");
      hmFilters.put("USERCODE", "300");
      hmFilters.put("DEPT", "");
      hmFilters.put("STATUS", "311");
      if (strCompanyPlant.equals("Plant")) {
        hmFilters.put("DISTFILTER", "COMPANY-PLANT");
      }

      if (strAction.equals("Load")) {
        alReturn = gmCust.getDistributorList(hmFilters);
        hmReturn.put("DISTLIST", alReturn);
        // alReturn = gmProj.loadSetMap("1601");
        alReturn = gmCommonBean.getActiveLoanerSets(strOpt);
        hmReturn.put("SETLIST", alReturn);
        alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LNPRTY", "103808"));
        hmReturn.put("PRIORITY", alReturn);
        alReturn = gmCommon.getCodeList("INPRP");
        hmReturn.put("CATEGORIES", alReturn);

        if (isValid(strOpt)) {
          alReturn = gmCommon.getCodeList("LOANS", strOpt, getGmDataStoreVO());//PMT-27826 BUG-10513
        }
        alEventName = GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventNameAllList());
        hmReturn.put("EVENTNAME", alEventName);
        /*
         * if (strOpt.equals("4119")){//inhouse loaners alReturn =
         * gmCommon.getCodeList("LOANS",strOpt); }
         */
        hmReturn.put("STATUS", alReturn);
        // Get Manage Loaners choose action list
        alLoanerOpts = gmCommon.getCodeList("LOANOP", getGmDataStoreVO());
        hmReturn.put("LOANEROPT", alLoanerOpts);

        hmParam.put("TYPE", strOpt);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hmParam", hmParam);
        request.setAttribute("hType", strOpt);
      } else if (strAction.equals("Reload")) {
        alReturn = gmCust.getDistributorList(hmFilters);
        hmReturn.put("DISTLIST", alReturn);
        // alReturn = gmProj.loadSetMap("1601");
        alReturn = gmCommonBean.getActiveLoanerSets(strOpt);
        hmReturn.put("SETLIST", alReturn);
        alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LNPRTY", "103808"));
        hmReturn.put("PRIORITY", alReturn);
        alReturn = gmCommon.getCodeList("INPRP");
        hmReturn.put("CATEGORIES", alReturn);
        if (isValid(strOpt)) {
          alReturn = gmCommon.getCodeList("LOANS", strOpt, getGmDataStoreVO());//PMT-27826 BUG-10513
        }
        hmReturn.put("STATUS", alReturn);
        // Get Manage Loaners choose action list
        alLoanerOpts = gmCommon.getCodeList("LOANOP", getGmDataStoreVO());
        hmReturn.put("LOANEROPT", alLoanerOpts);

        alEventName = GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventNameAllList());
        hmReturn.put("EVENTNAME", alEventName);
        strCategories = GmCommonClass.parseNull(request.getParameter("hCategories"));
        strSetIds = GmCommonClass.parseNull(request.getParameter("hSetIds"));

        strStatus = GmCommonClass.parseNull(request.getParameter("hStatus"));
        String strActualStatus = strStatus;
        strStatusGrp = GmCommonClass.parseNull(request.getParameter("hStatusGrp"));
        log.debug("strStatus " + strStatus);
        strEtchId = GmCommonClass.parseNull(request.getParameter("Txt_EtchId"));
        strConId = GmCommonClass.parseNull(request.getParameter("Txt_ConId"));
        strOverdue = GmCommonClass.parseNull(request.getParameter("Chk_Overdue"));
        strSetName = GmCommonClass.parseNull(request.getParameter("Txt_SetNm"));
        strRequestID = GmCommonClass.parseNull(request.getParameter("Txt_ReqId"));
        strActualRequestID = strRequestID;
        strDueDays = GmCommonClass.parseNull(request.getParameter("Txt_DueDays"));
        strDistIds = GmCommonClass.parseNull(request.getParameter("hDistIds"));
        strHoldTxn = GmCommonClass.parseNull(request.getParameter("Chk_HoldTxn"));
        strCaseInfoID = GmCommonClass.parseNull(request.getParameter("eventName"));
        strPriorityStr = GmCommonClass.parseNull(request.getParameter("hPriorityStr"));
       
        if (!strCaseInfoID.equals("0") && !strCaseInfoID.equals("")) {
          strRequestID = GmCommonClass.parseNull(gmEventRptBean.fetchCaseRequestID(strCaseInfoID));
          if (!strRequestID.equals("")) {
            strStatus = "5,10,20";
          } else {
            strStatus = "";
          }
        }
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "LOANER_SET_MISSED"));
        strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        hmParam.put("MACCESSFL", strUpdateAccess);
        hmParam.put("TYPE", strOpt);
        hmParam.put("CATEGORY", strCategories);
        hmParam.put("SETID", strSetIds);
        hmParam.put("STATUS", strStatus);
        hmParam.put("ETCHID", strEtchId);
        hmParam.put("CONID", strConId);
        hmParam.put("OVERDUE", strOverdue);
        hmParam.put("STATUSGRP", strStatusGrp);
        hmParam.put("SETNAME", strSetName);
        hmParam.put("REQID", strRequestID);
        hmParam.put("DUEDAYS", strDueDays);
        hmParam.put("DISTIDS", strDistIds);
        hmParam.put("HOLDTXN", strHoldTxn);
        hmParam.put("CASEINFOID", strCaseInfoID);
        hmParam.put("PRIORITYSTR", strPriorityStr);
        hmParam.put("SALESREPID", strSalesRepId); //added for PC-884

        alReturn = gmInHouseRptBean.getInHouseSetStatus(hmParam);
        hmReturn.put("REPORT", alReturn);
        if (strActualRequestID.equals("")
            || (!strCaseInfoID.equals("0") && !strCaseInfoID.equals(""))) {
          hmParam.put("REQID", strActualRequestID);
          hmParam.put("STATUS", strActualStatus);
        }
        
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hmParam", hmParam);
      } else if (strAction.equals("EditLoad")) {
        ArrayList alCam = new ArrayList();
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        strStatus = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("LOANSFLCD"));
        log.debug("strStatus is " + strStatus);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        strSetId = (String) hmCONSIGNMENT.get("SETID");
        alCam = GmCommonClass.getCodeList("CAMERA");
        request.setAttribute("ALCAMERA", alCam);
        String strCameraId = GmCommonClass.parseNull(GmCommonClass.getCodeID(strCamProp, "CAMERA"));
        request.setAttribute("strCamId", strCameraId);
        request.setAttribute("strComments", strComments + "\n");
        strCboOpt = GmCommonClass.parseNull(request.getParameter("Cbo_Action"));
        log.debug("strCboOpt is " + strCboOpt);
        if (strCboOpt.equals("SHIP")) {
          alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
          request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
          // for Other Values
          hmResult = gmInHouseRptBean.loadInHouseSetShipLists(strConsignId);
          hmReturn.put("SETCONLISTS", hmResult);

          hmParam.put("SETID", strSetId);
          hmParam.put("APPLNDATEFMT ", strApplDateFmt);
          alReturn = gmInHouseRptBean.reportLoanerOpenRequests(hmParam);
          request.setAttribute("LOANERREQUESTS", alReturn);
          strDispatchTo = strLogisticsPath.concat("/GmInHouseSetShip.jsp");
        } else if (strCboOpt.equals("RETURN")|| strCboOpt.equals("PENDINGACCEPT")) {
          // alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "STOCK");
          // request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
          if (strOpt.equals("4127")) {
            request.setAttribute("header", "Process Loaner Sets - Accept Returns");
          } else if (strOpt.equals("4119")) {
            request.setAttribute("header", "Process In-House Sets - Accept Returns");
          }
          // This if condition is used to get the default comment from rule table and set to the
          // comments like CAM12 (inbound)
          if (!strComments.equals("")) {
            strDefaultComment =
                GmCommonClass.parseNull((String) GmCommonClass.getRuleValue("LOANER_SCREEN_AR",
                    "DEFAULT_COMMENT"));
            request.setAttribute("strComments", strComments + " " + strDefaultComment + "\n");
          }
          strDispatchTo = strLogisticsPath.concat("/GmInHouseSetReturn.jsp?hSetId=" + strSetId);
        } else if (strCboOpt.equals("PROCESSRETURN") || strCboOpt.equals("LNRECONFIG")
            || strCboOpt.equals("LOTRECONFIG")
            || (strCboOpt.equals("REFILL") && (strStatus.equals("5") || strStatus.equals("10")))) {
          alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "STOCK");
          request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
          request.setAttribute("hCounter", "" + alConsignSetDetails.size());
          if (isValid(strOpt)) {
            alReturn = GmCommonClass.getCodeList("RPLNFR", strOpt);
            request.setAttribute("alRplnFrm", alReturn);
            alReturn = GmCommonClass.getCodeList("MOVETO", strOpt);
            request.setAttribute("alMoveTo", alReturn);
          }
          // To Load only Back Order transaction in Process Transaction ----
          String strhScrnFrom = GmCommonClass.parseNull(request.getParameter("hScreenFrom"));
          ArrayList alResult = gmInHouseRptBean.fetchOpenInHouseTrans(strConsignId, strhScrnFrom);
          request.setAttribute("alResult", alResult);
          request.setAttribute("hScreenFrom", strhScrnFrom);
          // ------
          // String strAddChargesId="92075";
          if (strStatus.equals("25")) {
            String strAddChargesId = GmCommonClass.parseNull(request.getParameter("hAddChargeId"));
            ArrayList alIncidents = GmCommonClass.getCodeList("ADDCH", strAddChargesId);
            request.setAttribute("alReturn", alIncidents);
            request.setAttribute("hmCodeList", GmCommonClass.getAltGroupDetails(alIncidents));
          }
          if (strOpt.equals("4127")) {
            if (strStatus.equals("25")) {
              request.setAttribute("header", "Process Loaner Sets - Process Check");
            } else if ((strStatus.equals("5") || strStatus.equals("0") || strStatus.equals("10") || strStatus
                .equals("58")) && (!strCboOpt.equals("LOTRECONFIG"))) { // 58 - Pending PutAway
              request.setAttribute("header", "Process Loaner Sets - Reconfigure loaner");
            } else if (strCboOpt.equals("LOTRECONFIG")
                && (strStatus.equals("0") || strStatus.equals("20"))) { // PMT-15189 Loaner
                                                                        // reconfigure by Lot
              request.setAttribute("header", "Process Loaner Sets - Reconfigure by Lot #");
              alReturn = gmCommonBean.getLog(strConsignId, "26240289");
              request.setAttribute("hmLog", alReturn);
            }
          } else if (strOpt.equals("4119")) {
            if (strStatus.equals("25")) {
              request.setAttribute("header", "Process In-House Sets - Process Check");
            } else if (strStatus.equals("5") || strStatus.equals("0") || strStatus.equals("10")) {
              request.setAttribute("header", "Process In-House Sets - Reconfigure loaner");
            }
          }
          request.setAttribute("COMBO_OPT", strCboOpt);
          String strSet= GmCommonClass.parseNull((String) hmCONSIGNMENT.get("SETID"));
		  String strLotTrkfl = gmLoanerLotReprocessBean.validateSetLotTrack(strSet);
		  String strLotTrackCompFlag =
		            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strCompanyId,
		                "LOT_TRK", strCompanyId));
          log.debug("strLotTrkfl============ "+strLotTrkfl);
          log.debug("strLotTrackCompFlag============ "+strLotTrackCompFlag);
          String strReconfigFl =
		            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("RECONFIG_LOANER_FL", "RECONFIG_LOANER",strCompanyId));
          request.setAttribute("RECONFIGFL", strReconfigFl);
          log.debug("strReconfigFl============ "+strReconfigFl);
       //   hmIn.put("LOTTRKFL", strLotTrkfl);
          if(strLotTrkfl.equals("Y") && (strLotTrackCompFlag.equalsIgnoreCase("Y")) && strCboOpt.equals("PROCESSRETURN")){

        	  strDispatchTo = strLogisticsPath.concat("/GmLoanerLotReprocess.jsp?hSetId="+strSet);
          }else{
        	  strDispatchTo =
        			  strLogisticsPath.concat("/GmInHouseSetReturnProcessing.jsp?hSetId=" + strSetId);
          }
        } else if (strCboOpt.equals("EXTENSION")) {
          // Below all include files are included in
          // GmLoanerExtension.jsp
          // to support GmIncludeLEPartEdit.jsp, getting some
          // information here
          String strExtReason = GmCommonClass.parseNull(request.getParameter("Cbo_ReasonType"));
          Date dtExtDate =
              getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_ExtensionDate")));
          Date dtSurgDate =
              getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_SurgeryDate")));
          String strExtComments = GmCommonClass.parseNull(request.getParameter("txt_Comments"));
          hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                  "SURG_DATE_EDIT_ACS"));
          String strSurgDtAccFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

          // when "Extension w/Replenish" it should load "edit part"
          // section
          if (strExtReason.equals("50322") || strExtReason.equals("26240637")) {
            alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "STOCK");
            request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
            request.setAttribute("STATUS", "HIDE");
            request.setAttribute("TYPE", strOpt);
          }

          // Fetch loaner extension information from
          // t504b_loaner_extension_detail,
          // display info at GmIncludeLESummary.jsp
          rdReturn = gmInHouseTransBean.fetchLoanerExtensionSummary(strConsignId);
          request.setAttribute("LEDETAILS", rdReturn);

          // get Loaner Extension Reason types, display them in the
          // GmIncludeLEEdit.jsp
          alReasonTypes = gmCommon.getCodeList("LEEXT",getGmDataStoreVO());
          request.setAttribute("REASONTS", alReasonTypes);

          // Extention should be allowed above current date, Validation in jsp
          gmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
          String strCurrDate = gmCalenderOperations.getCurrentDate(strApplDateFmt);
          String showEditPart = GmCommonClass.parseNull(request.getParameter("hInputEditPart"));
          request.setAttribute("SHOWEDITPART", showEditPart);
          request.setAttribute("EXTDATE", dtExtDate);
          request.setAttribute("SURGDATE", dtSurgDate);
          request.setAttribute("EXTCOMMENT", strExtComments);
          request.setAttribute("ApplDateFmt", strApplDateFmt);
          request.setAttribute("CurrDate", strCurrDate);
          request.setAttribute("STRSURGERYACCESSFL", strSurgDtAccFl);
          HashMap hmPutGroupId = new HashMap();
          HashMap hmruleShipVal = new HashMap();
          GmCommonBean gmcommon = new GmCommonBean();
          hmPutGroupId.put("RULEID", "SHIP_CARR_MODE_VAL");
          hmruleShipVal = gmcommon.fetchTransactionRules(hmPutGroupId, getGmDataStoreVO());// ship
                                                                                           // carrier
                                                                                           // and
                                                                                           // mode
          // default value get from
          // rule

          hmReturn.put("DEFAULTSHIPMODECARRVAL", hmruleShipVal);
          // String strReasonType =
          // GmCommonClass.parseNull(request.getParameter("Cbo_ReasonType"));
          request.setAttribute("REASONTYPE", strExtReason);
          request.setAttribute("hmReturn", hmReturn);
          strDispatchTo = strLogisticsPath.concat("/GmLoanerExtension.jsp");
        } else if (strCboOpt.equals("VIEWLESUMMARY")) {
          rdReturn = gmInHouseTransBean.fetchLoanerExtensionSummary(strConsignId);
          request.setAttribute("LEDETAILS", rdReturn);
          strDispatchTo = strLogisticsPath.concat("/GmLoanerExtensionSummary.jsp");
        }
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("ShipOut")) {

        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        strLoanTo = GmCommonClass.parseNull(request.getParameter("Cbo_LoanTo"));
        strLoanToId = GmCommonClass.parseNull(request.getParameter("Cbo_ValuesCon"));
        // strShipTo =
        // GmCommonClass.parseNull(request.getParameter("Cbo_ShipTo"));
        // strShipToId =
        // GmCommonClass.parseNull(request.getParameter("Cbo_ValuesShip"));
        strExpDate = GmCommonClass.parseNull(request.getParameter("Txt_RetDate"));
        // strShipCarr =
        // GmCommonClass.parseNull(request.getParameter("Cbo_Carr"));
        // strShipMode =
        // GmCommonClass.parseNull(request.getParameter("Cbo_Mode"));
        strReqId = GmCommonClass.parseNull(request.getParameter("hReqId"));
        strRepId = GmCommonClass.parseNull(request.getParameter("Cbo_LoanToRep"));
        strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_LoanToAcc"));
        hmParam.putAll(loadShipReqParams(request));
        // loadShipReqParams(request, hmParam); // called to load the
        // shipping parameters

        hmParam.put("CONSIGNID", strConsignId);
        hmParam.put("LOANTO", strLoanTo);
        hmParam.put("LOANTOID", strLoanToId);

        // hmParam.put("SHIPTO", strShipTo);
        // hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("EDATE", strExpDate);
        // hmParam.put("SCARR", strShipCarr);
        // hmParam.put("SMODE", strShipMode);
        hmParam.put("REQID", strReqId);
        hmParam.put("REPID", strRepId);
        hmParam.put("ACCID", strAccId);
        hmParam.put("SOURCE", "50182");
        hmParam.put("USERID", strUserId);
        log.debug("calling saveLoanerShipTrans");
        hmReturn = gmInHouseTransBean.saveLoanerShipTrans(hmParam);

        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
        request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);

        strDispatchTo = strLogisticsPath.concat("/GmInHouseSetView.jsp");
      }

      else if (strAction.equals("ReturnSet") || strAction.equals("ProcessReturnSet")
          || strAction.equals("ExtenOnly") || strAction.equals("ExtenWithRefill")
          || strAction.equals("LOTRECONFIG")) {
        HashMap hmLoanerReturnTrans = new HashMap();
        String strReplenishId = "";
        strConsignType = GmCommonClass.parseNull(request.getParameter("hType"));
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        hmParam.put("MISSSTR", strInputStr);

        if (isValid(strConsignType)) {
          ArrayList alRplnFrm = GmCommonClass.getCodeList("RPLNFR", strConsignType);
          for (int iFrom = 0; iFrom < alRplnFrm.size(); iFrom++) {
            HashMap hmRplnfrm = (HashMap) alRplnFrm.get(iFrom);
            String strID = (String) hmRplnfrm.get("CODEID");
            hmParam.put("REPFROM" + strID,
                GmCommonClass.parseNull(request.getParameter("strRplnFrm_" + strID)));
          }

          ArrayList alMoveTo = GmCommonClass.getCodeList("MOVETO", strConsignType);

          for (int iTo = 0; iTo < alMoveTo.size(); iTo++) {
            HashMap hmMoveTo = (HashMap) alMoveTo.get(iTo);
            String strID = (String) hmMoveTo.get("CODEID");
            hmParam.put("MOVETO" + strID,
                GmCommonClass.parseNull(request.getParameter("strMoveTo_" + strID)));
          }
        }
        strExpDate = GmCommonClass.parseNull(request.getParameter("Txt_RetDate"));
        hmParam.put("RETDATE", strExpDate);

        hmParam.put("CTYPE", strConsignType);
        log.debug("hmParam is" + hmParam);
        // below code is for loaner Extension
        // added by angela on 10/05/2008
        if (strAction.equals("ExtenOnly") || strAction.equals("ExtenWithRefill")) {
          strInputStr = GmCommonClass.parseNull(request.getParameter("hInputRetStr"));
          hmParam.put("RETSTR", strInputStr);
          strInputStr = GmCommonClass.parseNull(request.getParameter("hInputQuarStr"));
          hmParam.put("QUARSTR", strInputStr);
          strInputStr = GmCommonClass.parseNull(request.getParameter("hInputRepStr"));
          hmParam.put("REPSTR", strInputStr);
          String strExtReason = GmCommonClass.parseNull(request.getParameter("Cbo_ReasonType"));
          String strHistoryFl = GmCommonClass.parseNull(request.getParameter("hHistoryFl"));
          hmParam.put("HISTORYFL", strHistoryFl);
          Date dtExtDate =
              getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_ExtensionDate")));
          Date dtSurDate =
              getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_SurgeryDate")));
          strComments += GmCommonClass.parseNull(request.getParameter("txt_Comments"));
          String strReqExt = GmCommonClass.getCheckBoxValue((request.getParameter("strChkTypeFl")));
          // code added for shipping
          hmParam.putAll(loadShipReqParams(request));
          // loadShipReqParams(request, hmParam);
          hmParam.put("SOURCE", "50183");
          // hmParam.put("STATUSFL", "15");
          // end of code for shipping
          log.debug("loaded shipping params");
          String strTodayDate = (String) session.getAttribute("strSessTodaysDate");
          Date dtTodayDate = getDateFromStr(strTodayDate);
          strTodayDate = GmCommonClass.getStringFromDate(dtTodayDate, strApplDateFmt);
          hmParam.put("RETDATE", dtExtDate);
          hmParam.put("CONSINID", strConsignId);
          hmParam.put("EXTDATE", dtExtDate);
          hmParam.put("RTYPE", strExtReason);
          hmParam.put("COMMEN", strComments);
          hmParam.put("USERID", strUserId);
          if (strConsignType.equals("4127")) {
            hmParam.put("CTYPE", "50320");
          } else {
            hmParam.put("CTYPE", "50324");
          }
          hmParam.put("TODAYSDATE", strTodayDate);
          hmParam.put("REQEXT", strReqExt);
          hmParam.put("DATEFMT", strApplDateFmt);
          hmParam.put("SURGDATE", dtSurDate);
          hmParam.put("COMPANYINFO", strCompanyInfo);
          hmParam.put("CONSIGNMENTTYPE", strConsignType);
          log.debug("calling savelesumm");
          hmLoanerReturnTrans = gmInHouseTransBean.saveLESummary(hmParam);
          strTransId = GmCommonClass.parseNull((String) hmLoanerReturnTrans.get("ID"));
          strReplenishId = GmCommonClass.parseNull((String) hmLoanerReturnTrans.get("REPLENISHID"));
          log.debug("strTransId " + strTransId);
          log.debug("strReplenishId " + strReplenishId);
          request.setAttribute("REPLENISHID", strReplenishId);

          if (strConsignType.equals("4127") || strConsignType.equals("50320")) {
              
              HashMap hmLnParam = new HashMap();

              // Assigning the transaction type to 50159 for both the GM-LN types.
              // 1. When returning the set, missing qty GMLN
              // 2. Loaner Extension with replenish GMLN
              strtxnType_temp = "50159";
              hmLnParam.put("TRANS_ID", strTransId);
              hmLnParam.put("REFID", strConsignId);
              hmLnParam.put("TRANS_TYPE", strtxnType_temp);
              hmLnParam.put("USER_ID", strUserId);
              hmLnParam.put("WAREHOUSE_TYPE", "6");
              hmLnParam.put("COMPANY_INFO",strCompanyInfo);
              gmInvLocationBean.updateLoanerStockSheetInfo(hmLnParam);
            }


          // End of Loaner Extension code
        } else if (strAction.equals("LOTRECONFIG")) {// used for reconfigure lot

          String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
          strInputStr = GmCommonClass.parseNull(request.getParameter("hInputReconLotStr"));
          hmParam.put("RECONLOTSTR", strInputStr);
          hmParam.put("USER_ID", strUserId);
          hmParam.put("REFID", strConsignId);
          hmParam.put("LOG", strLog);

          gmInHouseTransBean.updateLoanerLot(hmParam);

        } else if (strAction.equals("ReturnSet")) // Below code will be called if user returns set
        {
          log.debug("ReturnSet");
          String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
          String strLoanTransId = GmCommonClass.parseNull(request.getParameter("hLoanTransId"));
          Date dtRetDate = GmCommonClass.getStringToDate(strExpDate, strApplDateFmt);
          // to store the log comments as selected camera values.
          strSelectedCamera =
              GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(GmCommonClass
                  .parseNull(request.getParameter("Cbo_CameraId"))));
          strSelectedCamera = strSelectedCamera.equals("") ? strComments : strSelectedCamera;
          if (strLog.equals("") || (strLog.toUpperCase()).indexOf(strSelectedCamera) != 0) {
            strLog = strSelectedCamera + "\n" + strLog;
          }

          hmParam.put("CONSIGNID", strConsignId);
          hmParam.put("USERID", strUserId);
          hmParam.put("LOG", strLog);
          hmParam.put("LOANTRANSID", strLoanTransId);
          hmParam.put("RETDATE", dtRetDate);
          hmParam.put("COMPANYINFO", strCompanyInfo);
          gmInHouseTransBean.saveLoanerAcceptReturn(hmParam);
        } else {// Below code will be called if user processes a returned setstrLoanerSet
          log.debug("Process Returned Set");
          String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
          hmParam.put("LOG", strLog);

          strTransId = GmCommonClass.parseNull(request.getParameter("hLoanTransId"));
          log.debug("Processed Set 1 strTransId : " + strTransId);

          String strTransType = GmCommonClass.parseNull(request.getParameter("hType"));
          log.debug("Processed Set 2 strTransType : " + strTransType);

          String strInputString = GmCommonClass.parseNull(request.getParameter("hInputChgStr"));
          hmParam.put("TRANSID", strTransId);
          hmParam.put("TRANSTYPE", strTransType);
          hmParam.put("INPUTSTR", strInputString);
          hmLoanerReturnTrans =
              gmInHouseTransBean.saveLoanerReturnTrans(strConsignId, hmParam, strUserId);

          strTransId = GmCommonClass.parseNull((String) hmLoanerReturnTrans.get("ID"));
          
          if (strConsignType.equals("4127") || strConsignType.equals("50320")) {
              
              HashMap hmLnParam = new HashMap();

              // Assigning the transaction type to 50159 for both the GM-LN types.
              // 1. When returning the set, missing qty GMLN
              // 2. Loaner Extension with replenish GMLN
              strtxnType_temp = "50159";
              hmLnParam.put("TRANS_ID", strTransId);
              hmLnParam.put("REFID", strConsignId);
              hmLnParam.put("TRANS_TYPE", strtxnType_temp);
              hmLnParam.put("USER_ID", strUserId);
              hmLnParam.put("WAREHOUSE_TYPE", "6");
              hmLnParam.put("COMPANY_INFO",strCompanyInfo);
              gmInvLocationBean.updateLoanerStockSheetInfo(hmLnParam);
            }
        }

        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
        request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
        request.setAttribute("TRANSID", strTransId);
        String[] StrLnArr = new String[2];
        StrLnArr = strTransId.split(",");
        if (StrLnArr.length > 1) {
          request.setAttribute("CANCELTYP", "50322"); // setting this
          // here for
          // shipping
        }
        log.debug(" storing building info for FGLE,FGLN, FGIS...GmInHouseSetServlet");
        for (int i = 0; i < StrLnArr.length; i++) { //PMT-33513 For FGLN, FGIS, FGLE txn
        	hmStorageBuildInfo.put("TXNID", StrLnArr[i]); //consignment id
            hmStorageBuildInfo.put("TXNTYPE", "INHOUSE412");
            gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo); 
          }

        // If Reconfigure by lot screen , again reload the same screen after perfomed the operation
        if (strAction.equals("LOTRECONFIG")) {
          strDispatchTo =
              "/GmInHouseSetServlet?hAction=EditLoad&Cbo_Action=LOTRECONFIG&hConsignId="
                  + strConsignId;
        } else if (!strAction.equals("ExtenOnly")) { // "extension only" will not go to this screen
                                                     // but show shipOutInfo
          strDispatchTo = strLogisticsPath.concat("/GmInHouseSetView.jsp");
        }
      } else if (strAction.equals("ReloadHist") || strAction.equals("ReloadUsage")
          || strAction.equals("ViewUsageHistory")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        String strLNType = GmCommonClass.parseNull(request.getParameter("lnType"));
        String strScreenType = GmCommonClass.parseNull(request.getParameter("hScreen"));
        if (strAction.equals("ReloadHist")) {
          alReturn = gmInHouseRptBean.viewLoanerRefillHistory(strConsignId);
        } else if (strAction.equals("ReloadUsage") || strAction.equals("ViewUsageHistory")) {
          HashMap hmUsageDetailsParam = new HashMap();
          strMonth = GmCommonClass.parseNull(request.getParameter("month"));
          strDistributorId = GmCommonClass.parseNull(request.getParameter("distributorId"));
          strSetId = GmCommonClass.parseNull(request.getParameter("setId"));
          strTurnsMonth = GmCommonClass.parseNull(request.getParameter("hTurnsMonth"));
          String strAccessFilter = "";

          // The filter condition should be applied for ICS Dept users alone.
          if (strDeptId.equals("ICS"))
            strAccessFilter = getAccessFilter(request, response);

          hmUsageDetailsParam.put("CONSIGNMENTID", strConsignId);
          hmUsageDetailsParam.put("LNTYPE", strLNType);
          hmUsageDetailsParam.put("DISTRIBUTORID", strDistributorId);
          hmUsageDetailsParam.put("SETID", strSetId);
          hmUsageDetailsParam.put("MONTH", strMonth);
          hmUsageDetailsParam.put("ACTION", strAction);
          hmUsageDetailsParam.put("TURNSMONTH", strTurnsMonth);
          hmUsageDetailsParam.put("ACCESSFILTER", strAccessFilter);
          hmUsageDetailsParam.put("STROPT", strOpt);

          log.debug(" type " + strOpt + "  Hashmap for usage history " + hmUsageDetailsParam);
          // alReturn = gmInHouseRptBean.viewLoanerUsageHistory(hmUsageDetailsParam);
          rowSetRpt = gmInHouseRptBean.viewLoanerUsageHistory(hmUsageDetailsParam);
        }
        request.setAttribute("SETHISTORY", alReturn);
        request.setAttribute("RSDHISTORYRPT", rowSetRpt);
        request.setAttribute("hScreenType", strScreenType);
        if (strAction.equals("ViewUsageHistory")) {
          strDispatchTo = strLogisticsPath.concat("/GmViewLoanerUsageHistory.jsp");
        } else {
          hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
          request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
          alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
          request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
          strDispatchTo = strLogisticsPath.concat("/GmLoanerSetHistoryView.jsp");
        }
      } else if (strAction.equals("ViewSet") || strAction.equals("ViewPrint")
          || strAction.equals("ViewLetterVer") || strAction.equals("ViewLetter")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));

        strType = GmCommonClass.parseNull(request.getParameter("hType"));

        if (strType.equals("9110")) {// IHLN
          HashMap hmRefDetails = new HashMap();
          HashMap hmJasper = new HashMap();
          ArrayList alSetLoad = new ArrayList();
          ArrayList alChildReq = new ArrayList();
          String returnId = GmCommonClass.parseNull(request.getParameter("hReturnId"));
          log.debug("returnId   ...." + returnId);
          hmRefDetails = gmICTSummaryBean.fetchConsignmentDetails(strConsignId, "null");
          hmCONSIGNMENT =
              GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNDETAILS"));
          hmResult =
              GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNCHILDDETAILS"));
          alSetLoad = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("SETLOAD"));
          hmJasper.putAll(hmCONSIGNMENT);
          hmJasper.put("SETLOAD", alSetLoad);
          alChildReq =
              GmCommonClass.parseNullArrayList(gmIHLoanerItemBean.fetchChildRequestDetail(strReqId,
                  strConsignId, strType));
          log.debug("alChildReq   ...." + alChildReq);
          hmJasper.put("SUBCHILDREQUEST", alChildReq);
          request.setAttribute("HMRESULT", hmJasper);
          request.setAttribute("hReturnId", returnId);
        } else {
        	if( strITCountryCode.equals("it") && ((strLoanerStatus.equals("40") && strAction.equals("ViewPrint") || strAction.equals("ViewLetterVer")) 
        			|| (strLoanerStatus.equals("20") && strAction.equals("ViewPrint") || strAction.equals("ViewLetterVer")))){                 //   if the company is ITALY  - PMT-42950
      		  hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId, strLoanerRequestId);
      	    }else {
      		  hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
      	  }
        }
        String strUserName = GmCommonClass.parseNull(gmCommonBean.getUserName(strUserId));
        hmCONSIGNMENT.put("USERNAME", strUserName);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);

        String strSet= GmCommonClass.parseNull((String) hmCONSIGNMENT.get("SETID"));
        String strLotTrkfl = gmLoanerLotReprocessBean.validateSetLotTrack(strSet);
        // if (!strLoanerTransId.equals("")){//create a new func to return strConsignId when
        // strLoanerTransId is passed
        // strConsignId = gmInHouseTransBean.fetchConsignId(strLoanerTransId);
        // log.debug("strConsignId "+strConsignId);
        // }
        HashMap hmIn = new HashMap();
        hmIn.put("CONSIGNID", strConsignId);
        hmIn.put("gmResourceBundleBean", gmResourceBundleBean);
        hmIn.put("ACTION", strAction);
        hmIn.put("gmDataStoreVO", getGmDataStoreVO());
        hmIn.put("LOANERSTATUSFLAG", strLoanerStatusFlag);
        hmIn.put("LOANERPAPERWORKFLAG", strLoanerPaperworkFlag);
        hmIn.put("LOTTRKFL", strLotTrkfl);
        request.setAttribute("LOTTRKFL", strLotTrkfl);

        GmInhouseSetPrintInterface gmInhouseSetPrintInterface =
            (GmInhouseSetPrintInterface) GmCommonClass.getSpringBeanClass(
                "xml/Inhouse_Print_Beans.xml", getGmDataStoreVO().getCmpid());
        alConsignSetDetails = gmInhouseSetPrintInterface.fetchConsignSetDtls(hmIn);
        request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);
        request.setAttribute("hType", strType);

        // The following Code is added for showing the Loaner Print Letter based on the Country
        // Code.
        strCompAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYADDRESS"));
        strCompPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYPHONE"));
        strCompFax = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYFAX"));
        strCountryCode = GmCommonClass.parseNull((String) hmCompanyAddress.get("COUNTRYCODE"));
        strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYNAME"));
        strJasperName =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER.JAS_LNR_ACK"));
        strCompanyId = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYID"));
        strCustServPhone = "";
        strCompanyPhone = "";
        strCompanyShortNm = "Globus";
        strCompNm = "Globus Medical";
        strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
        strDivisionId = strCompanyId.equals("100801") ? "2001" : "2000";

        if (strCompanyIdLogoFlag.equals("YES")) {
          strCompanyId = GmCommonClass.parseNull(gmResourceBunBeanVal.getProperty("COMP_LOGO"));
        }

        if (!strCountryCode.equals("en")) {
          strCompleteAddress =
              strCompanyName + "/" + strCompAddress + "/Phone no:" + strCompPhone + "/FAX:"
                  + strCompFax;
        } else if (strCountryCode.equals("en")) {
          hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strPortalCompanyId, strDivisionId);
          strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
          strCompAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
          strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
          strCompFax = GmCommonClass.parseNull((String) hmCompanyAddress.get("FAX"));
          strCompPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("CUST_SERVICE"));
          strCompanyShortNm =
              GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_SHORT_NAME"));
          strCompAddress = strCompAddress + "/" + strCompanyPhone;
          strCompleteAddress = strCompanyName + "/" + strCompAddress + "/" + strCompFax;
          strCompNm = strCompanyName;
        }

        hmJasperValues.put("COMPADDRESS", strCompAddress);
        hmJasperValues.put("COMPADDRESS", strCompAddress);
        hmJasperValues.put("COMPPHONE", strCompPhone);
        hmJasperValues.put("COMPFAX", strCompFax);
        hmJasperValues.put("COMPDETAILS", strCompleteAddress);
        hmJasperValues.put("JASNAME", strJasperName);
        hmJasperValues.put("COMPSHNM", strCompanyShortNm);
        hmJasperValues.put("COMPANYNAME", strCompNm);
        hmJasperValues.put("COMPID", strCompanyId);
        request.setAttribute("HMJASPERPARAMS", hmJasperValues);


        if (strAction.equals("ViewSet")) {
          strDispatchTo = strLogisticsPath.concat("/GmInHouseSetView.jsp");
        } else if (strAction.equals("ViewPrint")) {
          if (strType.equals("9110")) {
            strDispatchTo = strLogisticsPath.concat("/GmInhouseLoanerItemPrint.jsp");
          } else {
            if (strAction.equals("ViewPrint") && strShowCnumLSS.equalsIgnoreCase("YES")) {
              strDispatchTo = strLogisticsPath.concat("/GmOUSLoanerSetPrint.jsp");
            } else if ((strAction.equals("ViewPrint") && strLoanerPaperworkFlag.equals("YES"))) {
              // Status 40=Completed(Shiped out)
              String strBarCodeFlag = GmCommonClass.parseNull(request.getParameter("barCodeFl"));
              request.setAttribute("hmCompanyAddress", hmCompanyAddress);
              request.setAttribute("strBarCodeFlag", strBarCodeFlag);
              strDispatchTo = strLogisticsPath.concat("/GmLoanerDetailList.jsp");
            } else {
            	log.debug("strCompanyId-ITALY==> "+strCompanyId);
            	if(strCountryCode.equals("it")){                 //   if the company is ITALY  - PMT-42950
            		strDispatchTo = strLogisticsPath.concat("/GmITLoanerSetPrint.jsp");
            	}else{
                    strDispatchTo = strLogisticsPath.concat("/GmLoanerSetPrint.jsp");
               }
            }
            String strSource = GmCommonClass.parseNull(request.getParameter("ruleSource"));
            String strAddId = GmCommonClass.parseNull(request.getParameter("addressid"));
            log.debug("ruleSource .......= " + strSource);
            if (strSource.equals("50182")) {
              request.setAttribute("SOURCE", strSource);
              request.setAttribute("ADDRESSID", strAddId);
              request.setAttribute("REFID", strConsignId);
              request.setAttribute("RE_FORWARD", "gmLoanerSetPrint");
              request.setAttribute("RE_TXN", "SHIPOUTWRAPPER");
              request.setAttribute("txnStatus", "PROCESS");
              strDispatchTo = "/gmRuleEngine.do";
            }
          }
        } else if (strAction.equals("ViewLetter")) {
          if (strType.equals("4119") || strType.equals("9110")) {
            log.debug("ViewLetter    frd->   GmInhouseLoanerAckPrint.jsp   ");
            strDispatchTo = strLogisticsPath.concat("/GmInhouseLoanerAckPrint.jsp");
          } else {
            strDispatchTo = strLogisticsPath.concat("/GmLoanerAckPrint.jsp");
          }
        } else if (strAction.equals("ViewLetterVer")) {
          if (strType.equals("9110")) {
            strDispatchTo = strLogisticsPath.concat("/GmInhouseLoanerItemPrint.jsp");
          } else {
            request.setAttribute("LOANERSTATUS", strLoanerStatus);
            request.setAttribute("STRLOANERSTATUSFLAG", strLoanerStatusFlag);
            request.setAttribute("STRLOANERPAPERWORKFLAG", strLoanerPaperworkFlag);
            request.setAttribute("hmCompanyAddress", hmCompanyAddress);
            if(strCountryCode.equals("it")){                 //  for print version and agreement button if the company is ITALY  - PMT-42950
            	 strDispatchTo = strLogisticsPath.concat("/GmITLoanerViewPrintLetter.jsp");
            }else{
                 strDispatchTo = strLogisticsPath.concat("/GmLoanerViewPrintLetter.jsp");
            }
          }
          log.debug("ViewLetterVerstrDispatchTo" + strDispatchTo);
        }
      } else if (strAction.equals("PrintLoanerPPW")) {
        log.debug("PrintLoanerPPW");
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        log.debug(" strConsignId " + strConsignId);
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        ArrayList alResult =
            GmCommonClass.parseNullArrayList(gmInHouseRptBean.fetchLoanerBOParts(strConsignId));
        request.setAttribute("alResult", alResult);
        String strLog = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        log.debug("strLog===>" + strLog);
        request.setAttribute("COMMENTS", strLog);
        strDispatchTo = strLogisticsPath.concat("/GmPrintLoanerPaperWork.jsp");
      } else if (strAction.equals("PrintAll")) {
        strInput = GmCommonClass.parseNull(request.getParameter("strConsignId"));
        strType = GmCommonClass.parseNull(request.getParameter("hType"));
        String strRequestId = GmCommonClass.parseNull(request.getParameter("requestId"));
        String[] strId = GmCommonClass.StringtoArray(strInput, ",");
        strType = GmCommonClass.parseNull(gmEventReportBean.fetchRequestType(strRequestId));
        for (int cnt = 0; cnt < strId.length; cnt++) {
          if (!strId[cnt].equals("")) {
            hmCONSIGNMENT =
                GmCommonClass.parseNullHashMap(gmInHouseRptBean.getConsignDetails(strId[cnt]));
            log.debug("hmCONSIGNMENT :" + hmCONSIGNMENT);
            alConsignSetDetails =
                GmCommonClass.parseNullArrayList(gmInHouseRptBean.getSetConsignDetails(strId[cnt],
                    ""));
            log.debug("alConsignSetDetails :" + alConsignSetDetails);
            hmParam.put("hmCONSIGNMENT" + cnt, hmCONSIGNMENT);
            hmParamDtls.put("CONSIGNSETDETAILS" + cnt, alConsignSetDetails);
          }
        }
        /* Inhouse Loaner Item print (IHLN) */
        String strIHLNInput = GmCommonClass.parseNull(request.getParameter("strIHLNId"));
        String strIHLNType = GmCommonClass.parseNull(request.getParameter("IHLNType"));
        String[] strIHLNId = GmCommonClass.StringtoArray(strIHLNInput, ",");
        log.debug("============strIHLNInput==========" + strIHLNInput);
        for (int cnt = 0, hcnt = (strInput.equals("")) ? 0 : strId.length; cnt < strIHLNId.length; cnt++, hcnt++) {
          if (!strIHLNId[cnt].equals("")) {
            hmCONSIGNMENT =
                GmCommonClass.parseNullHashMap(gmInHouseRptBean.getConsignDetails(strIHLNId[cnt]));
            HashMap hmParams = new HashMap();
            hmParams.put("CONSIGNID", strIHLNId[cnt]);
            hmParams.put("TYPE", strIHLNType);
            hmCONSIGNMENT =
                GmCommonClass.parseNullHashMap(gmIHLoanerItemBean
                    .fetchInhouseLoanerItemDetails(hmParams));
            log.debug("hmCONSIGNMENT :" + hmCONSIGNMENT);
            hmParam.put("hmCONSIGNMENT" + hcnt, hmCONSIGNMENT);
          }
        }
        /* IHLN */
        request.setAttribute("hmParamPrint", hmParam);
        request.setAttribute("hmParamDtls", hmParamDtls);
        request.setAttribute("hType", strType);
        request.setAttribute("printAll", "PrintAll");
        // The following Code is added for showing the Loaner Print Letter based on the Country
        // Code.
        strCompAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYADDRESS"));
        strCompPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYPHONE"));
        strCompFax = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYFAX"));
        strCountryCode = GmCommonClass.parseNull((String) hmCompanyAddress.get("COUNTRYCODE"));
        strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYNAME"));
        strJasperName =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER.JAS_LNR_ACK"));
        strCompanyId = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYID"));
        strCustServPhone = "";
        strCompanyPhone = "";
        strCompanyShortNm = "Globus";
        strCompNm = "Globus Medical";
        strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
        strDivisionId = strCompanyId.equals("100801") ? "2001" : "2000";

        if (!strCountryCode.equals("en")) {
          strCompleteAddress =
              strCompanyName + "/" + strCompAddress + "/Phone no:" + strCompPhone + "/FAX:"
                  + strCompFax;
        } else if (strCountryCode.equals("en")) {
          hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strPortalCompanyId, strDivisionId);
          strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
          strCompAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
          strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
          strCompFax = GmCommonClass.parseNull((String) hmCompanyAddress.get("FAX"));
          strCompPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("CUST_SERVICE"));
          strCompanyShortNm =
              GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_SHORT_NAME"));
          strCompAddress = strCompAddress + "/" + strCompanyPhone;
          strCompleteAddress = strCompanyName + "/" + strCompAddress + "/" + strCompFax;
          strCompNm = strCompanyName;
        }
        hmJasperValues.put("COMPADDRESS", strCompAddress);
        hmJasperValues.put("COMPADDRESS", strCompAddress);
        hmJasperValues.put("COMPPHONE", strCompPhone);
        hmJasperValues.put("COMPFAX", strCompFax);
        hmJasperValues.put("COMPDETAILS", strCompleteAddress);
        hmJasperValues.put("JASNAME", strJasperName);
        hmJasperValues.put("COMPSHNM", strCompanyShortNm);
        hmJasperValues.put("COMPANYNAME", strCompNm);
        hmJasperValues.put("COMPID", strCompanyId);
        request.setAttribute("HMJASPERPARAMS", hmJasperValues);
        strDispatchTo = strLogisticsPath.concat("/GmLoanerViewPrintLetter.jsp");
      } else if (strAction.equals("REPROCESS")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        ArrayList alSet = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
        request.setAttribute("CONSIGNSETDETAILS", alSet);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strLogisticsPath.concat("/GmInHouseSetReProcessing.jsp");
      } else if (strAction.equals("Process_Reprocess")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        strDplyFl = GmCommonClass.parseNull(request.getParameter("hDeployFl"));
        log.debug("hmCONSIGNMENT Values : " + hmCONSIGNMENT);
        hmParam.put("CONSIGNID", strConsignId);
        hmParam.put("USERID", strUserId);
        hmParam.put("DEPLOYFL", strDplyFl);
        log.debug("strDplyFl==>" + strDplyFl);
        gmInHouseTransBean.saveLoanerProcess(hmParam);
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        ArrayList alSet = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
        request.setAttribute("CONSIGNSETDETAILS", alSet);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        strDispatchTo = strLogisticsPath.concat("/GmInHouseSetView.jsp");
      } else if (strAction.equals("PICTURE")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        alPendPictureCam = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PEPCAM"));
        strPendPicCameraId = GmCommonClass.parseNull(GmCommonClass.getCodeID(strCamProp, "PEPCAM"));
        request.setAttribute("ALPENDPICTURECAM", alPendPictureCam);
        request.setAttribute("strPendPictureCamId", strPendPicCameraId);
        // This if condition is used to get the default comment from rule table and set to the
        // comments like CAM12 (outbound)
        if (!strComments.equals("")) {
          strDefaultComment =
              GmCommonClass.parseNull((String) GmCommonClass.getRuleValue("LOANER_SCREEN_PP",
                  "DEFAULT_COMMENT"));
          request.setAttribute("strComments", strComments + " " + strDefaultComment + "\n");
        }
        strDispatchTo = strLogisticsPath.concat("/GmInHouseSetPendingPicture.jsp");
      } else if (strAction.equals("Process_Picture")) {
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        String strLogReason = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        // to store the log comments as selected camera values.
        strSelectedCamera =
            GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(GmCommonClass
                .parseNull(request.getParameter("Cbo_CameraId"))));
        strSelectedCamera = strSelectedCamera.equals("") ? strComments : strSelectedCamera;
        if (strLogReason.equals("") || (strLogReason.toUpperCase()).indexOf(strSelectedCamera) != 0) {
          strLogReason = strSelectedCamera + "\n" + strLogReason;
        }
        String strMsg = "";

        hmParam.put("CONSIGNID", strConsignId);
        hmParam.put("USERID", strUserId);
        hmParam.put("TXT_LOGREASON", strLogReason);
        strMsg = gmInHouseTransBean.saveLoanerProcessPicture(hmParam);
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        hmCONSIGNMENT.put("MSG", strMsg);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        log.debug("hmCONSIGNMENT Pic ::: " + hmCONSIGNMENT);
        ArrayList alSet = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
        request.setAttribute("CONSIGNSETDETAILS", alSet);
        strDispatchTo = strLogisticsPath.concat("/GmInHouseSetView.jsp");
      } else if (strAction.equals("BOPROCESS")) {
        String strGridXmlData = "";
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        String strTxnType = GmCommonClass.parseNull(request.getParameter("txnType"));
        String strAgingVal = GmCommonClass.parseNull(request.getParameter("agingVal"));
        RowSetDynaClass rdResult = null;
        ArrayList alResult = null;
        GmOperDashBoardBean gmOperDashBoardBean = new GmOperDashBoardBean(getGmDataStoreVO());
        hmParam.put("TXNTYPE", strTxnType);
        hmParam.put("AGINGVAL", strAgingVal);
        hmParam.put("PRCTYPE", "PROCTRANS");
        hmParam.put("REFID", strConsignId);
        rdResult = gmOperDashBoardBean.reportDashboardItems(hmParam);
        alResult = (ArrayList) rdResult.getRows();
        request.setAttribute("dbResult", alResult);
        log.debug("alResult==" + alResult.size());

        String strSessCompanyLocale =
            GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
        String strVmPath = "properties.labels.custservice.GmDashBoardHome";

        if (alResult != null && alResult.size() > 0) {
          hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
          hmParam.put("STRVMPATH", strVmPath);
          hmParam.put("RLIST", alResult);
          hmParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
          hmParam.put("TEMPLATE", "GmConsignedItems.vm");
          hmParam.put("TEMPLATEPATH", "common/templates");
          strGridXmlData = gmOperDashBoardBean.getXmlGridData(hmParam);
          request.setAttribute("strGridXmlData", strGridXmlData);
        }
        // To Load only Back Order transaction in Process Transaction
        String strhScrnFrom = GmCommonClass.parseNull(request.getParameter("hScreenFrom"));
        ArrayList alBkResult = gmInHouseRptBean.fetchOpenInHouseTrans(strConsignId, strhScrnFrom);
        request.setAttribute("alResult", alBkResult);
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        request.setAttribute("hScreenFrom", strhScrnFrom);
        request.setAttribute("strConsignId", strConsignId);
        strDispatchTo = "/operations/GmProcessTrans.jsp";
      }
      request.setAttribute("hAction", strAction);

      if (strAction.equals("InsViewer")) {
        String fileStatus = "Y";
        strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));
        String strFileName = "TXT";
        HashMap hmMap = new HashMap();
        GmFileReader fileReader = new GmFileReader();
        GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
        ArrayList alList = gmCommonUploadBean.fetchUploadInfo("91184", strSetId);

        if (alList.size() > 0) {
          hmMap = (HashMap) alList.get(0);
          strFileName = (String) hmMap.get("NAME");
        }
        if (!strFileName.equals("TXT")) {
          // below code is added for Print Loaner Inspection sheet in company language
          // Get file from company loaner Inspection sheet in sub directory by company properties
          // if not file present in sub directory it will get from main directory
          File fldisp = new File("");
          fldisp = fileReader.findFile(strCompLNINMainSubDir, strFileName);
          if (fldisp == null || !fldisp.exists()) {
            fldisp = fileReader.findFile(strCompLNINMainDir, strFileName);
            if (fldisp == null || !fldisp.exists()) {
              fldisp = fileReader.findFile(strLoanerSet, strFileName);
              if (fldisp == null || !fldisp.exists()) {
                fileStatus = "N";
              }
            }
          }
        } else {
          fileStatus = "N";
        }
        log.debug("file status" + fileStatus);
        request.setAttribute("FILESTATUS", fileStatus);
        strDispatchTo = "/operations/logistics/GmPrintInspectionSheet.jsp";
        // strAction = "OpenDoc";
      }
      if (strAction.equals("OpenDoc")) {
        strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));
        String strFileName = "TXT";
        HashMap hmMap = new HashMap();
        GmFileReader fileReader = new GmFileReader();
        GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
        ArrayList alList = gmCommonUploadBean.fetchUploadInfo("91184", strSetId);
        if (alList.size() > 0) {
          hmMap = (HashMap) alList.get(0);
          strFileName = (String) hmMap.get("NAME");
        }
        File fldisp = new File("");
        fldisp = fileReader.findFile(strCompLNINMainSubDir, strFileName);

        if (fldisp == null || !fldisp.exists()) {
          fldisp = fileReader.findFile(strCompLNINMainDir, strFileName);

          if (fldisp == null || !fldisp.exists()) {
            fldisp = fileReader.findFile(strLoanerSet, strFileName);
          }

        }

        String strContentType = GmCommonClass.parseNull(fileReader.getContentType(fldisp));

        if (fldisp != null && fldisp.exists()) {
          FileInputStream fisStream = new FileInputStream(fldisp);

          if (!strContentType.equals("")) {
            response.setContentType(strContentType);
          }

          int read = 0;
          response.setHeader("Content-Disposition", "inline;filename=\"" + fldisp.getName() + "\"");
          response.setContentLength(fisStream.available());
          ServletOutputStream stream = response.getOutputStream();
          while ((read = fisStream.read()) != -1) {
            stream.write(read);
          }
          stream.close();
        } else {
          session.setAttribute("hAppErrorMessage",
              "Inspection Sheet for " + GmCommonClass.parseNull(request.getParameter("hSetId"))
                  + " not found");
          strDispatchTo = strComnPath + "/GmError.jsp";
          dispatch(strDispatchTo, request, response);
        }
      } else {
        // Save cookie if changed
        if (!strCookieName.equals("")) {
          if (!GmCommonClass.parseZero(request.getParameter("Cbo_CameraId")).equals("0")) {
            gmCookieManager.setCookieValue(response, strCookieName,
                GmCommonClass.getCodeNameFromCodeId(GmCommonClass.parseNull(request
                    .getParameter("Cbo_CameraId"))));
          } else {
            gmCookieManager.setCookieValue(response, strCookieName, strCamProp);
          }
        }
        // Need to transfer the request to reload the Reconfigure Lot screen after submitting the
        // changes.
        if (strAction.equals("LOTRECONFIG")) {
          gotoPage(strDispatchTo, request, response);
        } else {
          dispatch(strDispatchTo, request, response);
        }

      }

    } catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      strDispatchTo = strComnPath + "/GmError.jsp";
      dispatch(strDispatchTo, request, response);
    } catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  private boolean isValid(String strOpt) {
    strOpt = GmCommonClass.parseZero(strOpt);
    return strOpt.matches("^-?[0-9]+(\\.[0-9]+)?$");
  }

}// End of GmInHouseSetServlet

