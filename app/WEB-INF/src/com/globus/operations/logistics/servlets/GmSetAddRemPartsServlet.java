package com.globus.operations.logistics.servlets;

/*****************************************************************************
 * File : GmSetAddRemPartsServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.shipping.beans.GmShippingReportBean;
import com.globus.custservice.shipping.beans.GmShippingTransBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;

public class GmSetAddRemPartsServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strLogisticsPath = GmCommonClass.getString("GMLOGISTICS");
    String strOperationsPath = GmCommonClass.getString("GMOPERATIONS");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    

    String strDispatchTo = strLogisticsPath.concat("/GmSetAddRemParts.jsp");
    String strAccess = "";
    // String strUserId = (String)session.getAttribute("strSessUserId");
    try {
      instantiate(request, response);
      checkSession(response, session); // Checks if the current session
      // is valid, else redirecting to
      // SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
      // log4j
      String strAction = request.getParameter("hAction");
      log.debug(" JAMES >>>>> Action from request is " + strAction);
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
        log.debug(" Action from Session is " + strAction);
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strMode = request.getParameter("hMode") == null ? "" : request.getParameter("hMode");

      // String strOpt = (String)session.getAttribute("hOpt");
      String strRuleOpt = "";
      String strOpt = request.getParameter("hOpt") == null ? "" : request.getParameter("hOpt");
      // strOpt = strOpt == null ||
      // strOpt.equals("")?request.getParameter("hOpt"):strOpt;
      String strReasonCdGrp = "";
      String strOperCdGrp = "";
      String strTxnType = "";
      if (strOpt.equals("4119")) {
        strTxnType = "InHouseAddRem";
        strReasonCdGrp = "CONRT";
        strOperCdGrp = "CONOP";
      } else if (strOpt.equals("4127")) {
        strTxnType = "LoanerAddRem";
        strReasonCdGrp = "LONRT";
        strOperCdGrp = "LONOP";
      }

      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmLogisticsBean gmLogis = new GmLogisticsBean(getGmDataStoreVO());
      GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
      GmInHouseSetsTransBean gmInHouseTxnBean = new GmInHouseSetsTransBean(getGmDataStoreVO());
      GmCommonClass gmCommon = new GmCommonClass();
      GmShippingReportBean gmReport = new GmShippingReportBean();
      GmRuleBean gmRuleBean = new GmRuleBean();
      GmLocationBean gmLocationBean = new GmLocationBean();
      GmCommonBean gmCommonBean = new GmCommonBean();
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmCONSIGNMENT = new HashMap();

      ArrayList alReason = new ArrayList();
      ArrayList alInHouseOperation = new ArrayList();
      ArrayList alResult = new ArrayList();
      ArrayList alLcnPartMap = new ArrayList();
      ArrayList alReturn = new ArrayList();
      GmShippingTransBean gmShippingTransBean = new GmShippingTransBean();
      String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));

      String strConsignId = "";
      String strType = "";
      String strBillTo = "";
      String strPurpose = "";
      String strShipTo = "";
      String strShipToId = "";
      String strComments = "";
      String strActionFl = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPartNums = "";
      String strhPartNums = "";
      String strDelPartNum = "";
      String strInputStr = "";

      String strPartNum = "";
      String strTempPartNums = "";

      String strInputString = "";
      String strQty = "";
      String strPrice = "";
      String strControl = "";
      String strTransId = "";
      String strOprType = "";
      String strReason = "";
      String strTransIdDash = "";
      String strOperationType = ""; // This would be either Add or
      String strTransType = "0";
      String strLoanerType = "0";

      String strAccessFlag = "";
      String strCrComments = "";
      String strValidationFl = "N";
      String strCheckRule = "";
      String strRuleEntryFl = "N";
      String strTxnStatus = "";
      strCheckRule = GmCommonClass.parseNull((String) request.getAttribute("chkRule"));
      strCheckRule =
          strCheckRule.equals("") ? GmCommonClass.parseNull(request.getParameter("chkRule"))
              : strCheckRule;
      strTxnStatus = GmCommonClass.parseNull(request.getParameter("txnStatus"));

      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      // String strCreateShipFL = "";
      HashMap hmAccess = new HashMap();
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      // Remove is 50150 or 50151

      log.debug(" Action is " + strAction + " Option is " + strOpt);
      if (strOpt.equals("")) {
        strRuleOpt = GmCommonClass.parseNull(request.getParameter("hType"));
      }
      if (strRuleOpt.equals(""))
        strRuleOpt = strOpt;


      HashMap hmParams = new HashMap();
      hmParams.put("RULEID", strRuleOpt);
      HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParams);
      String strRulesOpt = "";
      String strIncludeRuleEng = "";
      log.debug(" Action is " + strAction + " Option is " + strOpt);
      strRulesOpt = GmCommonClass.parseNull((String) hmTransRules.get("STR_OPT"));
      request.setAttribute("TRANS_RULES", hmTransRules);
      session.setAttribute("TRANS_RULES", hmTransRules);
      strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
      log.debug("hmTransRules" + hmTransRules);

      if (!strRuleOpt.equals("") && hmTransRules != null) {
        // //Access Rule COde/////
        String strOperType =
            (strAction.equalsIgnoreCase("verify") || strAction.equalsIgnoreCase("LoadItemShip")) ? "VERIFY"
                : "CONTROL";
        // strOperType= (strOperType.equals("") || strOperType.equals("PROCESS"))?"CONTROL":
        // strOperType;
        strOperType = "FN_" + strOperType;
        // GmAccessControlBean gmAccessControlBean=new GmAccessControlBean();
        log.debug("User ID == " + strUserId);
        strOperType = (String) hmTransRules.get(strOperType);
        log.debug("Request URI == " + strOpt + "_" + strOperType);
        if (strOperType != null) {
          hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  strOperType));
          strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          log.debug("strAccess==>" + strAccess);
          request.setAttribute("ACCESSFL", strAccess);
          session.setAttribute("ACCESSFL", strAccess);
          if (!strAccess.equals("Y")) {
            throw new AppError("", "20685");
          }
        }
        // ////////////
      }
      if (strAction.equals("EditLoad") || strAction.equals("GoCart")
          || strAction.equals("UpdateCart") || strAction.equals("PlaceOrder")
          || strAction.equals("RemoveCart") || strAction.equals("UpdateOrder")) {
        strType = GmCommonClass.parseNull((String) session.getAttribute("strOPRTYPE"));
        strReason = GmCommonClass.parseNull((String) session.getAttribute("strREASON"));
        strTransId = GmCommonClass.parseNull((String) session.getAttribute("TXNID"));
        strShipTo =
            request.getParameter("Cbo_ShipTo") == null ? "" : request.getParameter("Cbo_ShipTo");
        strShipToId =
            request.getParameter("Cbo_Values") == null ? "0" : request.getParameter("Cbo_Values");
        strComments =
            request.getParameter("Txt_Comments") == null ? "" : request
                .getParameter("Txt_Comments");
        strActionFl = "1"; // hardcoding to this value. Need the
        // transaction to be verified - James
        strOperationType = GmCommonClass.parseZero(request.getParameter("Cbo_OprType"));

        hmParam.put("TYPE", strType);
        hmParam.put("strREASON", strReason);
        hmParam.put("SHIPTO", strShipTo);
        hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("TXNID", strTransId);
        hmParam.put("SHIPFL", strActionFl);
        session.setAttribute("PARAM", hmParam);
        session.setAttribute("OPERTYPE", strOperationType);

        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        session.setAttribute("CONSIGNID", strConsignId);
      }

      if (strAction.equals("Load")) {
        // Code for Adding Consignment Details starts

        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        // strConsignId = "GM-CN-11545";
        hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
        log.debug(" Value of Consignment Details is " + hmCONSIGNMENT);
        // Getting the List of InHouse Buckets
        alReason = gmCommon.getCodeList(strReasonCdGrp);
        session.setAttribute("REASON", alReason);

        alInHouseOperation = gmCommon.getCodeList(strOperCdGrp);
        session.setAttribute("OPRTYPE", alInHouseOperation);

        session.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
        session.setAttribute("hAction", strAction);
        session.setAttribute("CONSIGNID", strConsignId);
      } else if (strAction.equals("EditLoad")) {
        log.debug("strTxnType: " + strTxnType);

        strOprType = GmCommonClass.parseNull(request.getParameter("Cbo_OprType"));
        strTransId = gmLogis.loadNextConsignId(strOprType);
        log.debug(" New Transaction ID is " + strTransId);


        strReason = GmCommonClass.parseNull(request.getParameter("Cbo_Reason"));

        session.setAttribute("TXNID", strTransId);
        session.setAttribute("strOPRTYPE", strOprType);
        session.setAttribute("strREASON", strReason);
        session.setAttribute("hAction", strAction);
      } else if (strAction.equals("GoCart")) {
        strPartNums =
            request.getParameter("Txt_PartNum") == null ? "" : request.getParameter("Txt_PartNum");
        strhPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");
        strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));

        if (strhPartNums != null && strhPartNums != "") {
          strPartNums = strhPartNums.concat(",").concat(strPartNums);
        }

        hmReturn = gmInHouseRptBean.loadCartForConsign(strConsignId, strPartNums);

        session.setAttribute("hmOrder", hmReturn);
        session.setAttribute("hAction", "PopOrd");
        session.setAttribute("strPartNums", strPartNums);
      } else if (strAction.equals("UpdateCart") || strAction.equals("PlaceOrder")
          || strAction.equals("RemoveCart") || strAction.equals("UpdateOrder")) {
        strPartNums =
            request.getParameter("hPartNums") == null ? "" : request.getParameter("hPartNums");
        strDelPartNum =
            request.getParameter("hDelPartNum") == null ? "" : request.getParameter("hDelPartNum");
        strOperationType = GmCommonClass.parseZero((String) session.getAttribute("OPERTYPE"));
        log.debug(" Operation type OPERTYPE " + strOperationType);

        HashMap hmTempOrd = (HashMap) session.getAttribute("hmOrder");
        HashMap hmTempCart = new HashMap();

        StringTokenizer strTok = new StringTokenizer(strPartNums, ",");
        HashMap hmLoop = new HashMap();
        int i = 0;

        while (strTok.hasMoreTokens()) {
          hmLoop = new HashMap();
          i++;
          strPartNum = strTok.nextToken();
          if (strDelPartNum.equals(strPartNum)) {
            hmTempOrd.remove(strPartNum);
          } else {
            strQty =
                request.getParameter("Txt_Qty" + i) == null ? "" : request.getParameter("Txt_Qty"
                    + i);
            log.debug("Value of StrQty is " + strQty);

            /*
             * if(strOperationType.equals("50151") &&
             * Integer.parseInt(GmCommonClass.parseZero(strQty)) >= 0) strQty =
             * String.valueOf(Integer.parseInt(strQty) * -1); log.debug(" Qty Value is " + strQty);
             */
            strPrice =
                request.getParameter("hPrice" + i) == null ? "" : request
                    .getParameter("hPrice" + i);
            strControl = "";
            hmLoop.put("QTY", strQty);
            hmLoop.put("PRICE", strPrice);
            hmLoop.put("CONTROL", strControl);
            hmTempCart.put(strPartNum, hmLoop);
            strInputStr =
                strInputStr
                    + strPartNum.concat(",").concat(strQty).concat(",").concat(strControl)
                        .concat(",").concat(strPrice);
            strInputStr = strInputStr.concat("|");

            strTempPartNums = strTempPartNums.concat(strPartNum);
            strTempPartNums = strTempPartNums.concat(",");
          }

        }

        if (!strTempPartNums.equals("")) {
          strTempPartNums = strTempPartNums.substring(0, strTempPartNums.length() - 1);
        }
        session.setAttribute("hmCart", hmTempCart);
        session.setAttribute("hmOrder", hmTempOrd);

        log.debug(" Temp Part Nums in Cart  is " + hmTempCart);
        log.debug(" Temp Order value is " + hmTempOrd);

        if (strAction.equals("RemoveCart")) {
          session.setAttribute("strPartNums", strTempPartNums);
          session.setAttribute("hAction", "PopOrd");
        } else if (strAction.equals("PlaceOrder")) {
          strConsignId = (String) session.getAttribute("CONSIGNID");
          // strConsignId = "GM-CN-11545";

          log.debug(" Inside Update Order");
          hmReturn =
              gmInHouseTxnBean.saveInLoanerItemConsign(strConsignId, hmParam, strInputStr,
                  strUserId, strAction);
          session.setAttribute("hAction", "OrdPlaced");
          // session.removeAttribute("strSessOpt");
        } else if (strAction.equals("UpdateOrder")) {
          log.debug(" Inside Update Order");
        }

      }

      else if (strAction.equals("EditControl")) {
        // strTransIdDash =
        // (String)session.getAttribute("strSessConsignId")==null?"":(String)session.getAttribute("strSessConsignId");
        strTransIdDash = GmCommonClass.parseNull(request.getParameter("hId"));
        strType = GmCommonClass.parseNull(request.getParameter("hType"));
        // String strTransType =
        // GmCommonClass.getString((String)session.getAttribute("TRANS_ID"));
        // strConsignId = "GM-CN-11545";
        log.debug(" Inside Edit Control - Consign Id is " + strConsignId + " Txn Id "
            + strTransIdDash + ": TYPE is: " + strType);
        if (strType.equals("40051") || strType.equals("40052") || strType.equals("40053")
            || strType.equals("40054")) {
          hmResult = gmLogis.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);

        } else {
          hmResult = gmInHouseRptBean.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);
        }
        strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
        alResult = gmInHouseRptBean.loadSavedSetMaster(strTransIdDash);
        hmReturn.put("SETLOAD", alResult);

        log.debug(" hmReturn in Edit Load is " + hmReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "EditLoad");
        request.setAttribute("hConsignId", strConsignId);
        request.setAttribute("hFromPage", "GmSetAddRemPartsServlet");
        strDispatchTo = strLogisticsPath.concat("/GmSPControlVerify.jsp");
        log.debug("strTransType 1= " + strTransType);
        log.debug("strType = " + strType);
        if (strTransType.equals("50155") || strTransType.equals("50157")
            || strTransType.equals("50158")) {
          strConsignId = GmCommonClass.parseNull((String) hmResult.get("CONSIGNID"));
          strLoanerType = gmRuleBean.fetchLoanerType(strConsignId);
        }
        if (!strTransType.equals("") && !strLoanerType.equals("")) {
          strTransType = gmRuleBean.fetchTransId(strTransType, strLoanerType);
        }
        strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;
        log.debug("strTransType 2= " + strTransType);
        // if(strType.equals("50157") ||strType.equals("50158") || strType.equals("50155")
        // ||strType.equals("50160")
        // ||strType.equals("50161")||strType.equals("50162")||strType.equals("40053"))
        if (!strTransType.equals("")) {
          if (strType.equals("50161") || strType.equals("50162")) {
            strTransType = "SETRECONFIGWRAPPER";
          }
          if (!strIncludeRuleEng.equalsIgnoreCase("NO")) {
            request.setAttribute("RE_FORWARD", "gmSPControlVerify");
            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", "PROCESS");
            request.setAttribute("TxnTypeID", strType);
            strDispatchTo = "/gmRuleEngine.do";
          }
        }

      } else if (strAction.equals("SaveControl") || strAction.equals("ReleaseControl")) {
        // strTransIdDash =
        // (String)session.getAttribute("strSessConsignId")==null?"":(String)session.getAttribute("strSessConsignId");

        strTransIdDash = GmCommonClass.parseNull(request.getParameter("hTxnId"));
        strType = GmCommonClass.parseNull(request.getParameter("hType"));
        String strCount = "";
        int intCount = 0;
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
        strActionFl =
            request.getParameter("Chk_ShipFl") == null ? "2" : request.getParameter("Chk_ShipFl");
        strCrComments = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
        strTransType = gmRuleBean.fetchTransId(strTransType, strLoanerType);
        strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;
        log.debug("strTransType = " + strTransType);
        log.debug("strCrComments===>" + strCrComments);
        hmParam.put("TYPE", strType);
        hmParam.put("strREASON", strReason);
        hmParam.put("SHIPTO", strShipTo);
        hmParam.put("SHIPTOID", strShipToId);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("TXNID", strTransIdDash);
        hmParam.put("SHIPFL", strActionFl);
        hmParam.put("USERID", strUserId);
        hmParam.put("CRCOMMENTS", strCrComments);
        intCount = Integer.parseInt(strCount);

        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum"
                  + i);
          if (!strPartNum.equals("")) {
            strQty = request.getParameter("Txt_Qty" + i);
            strQty = strQty.equals("") ? "0" : strQty;
            strControl = request.getParameter("Txt_CNum" + i);
            strControl = strControl.equals("") ? " " : strControl;
            strPrice = request.getParameter("hPrice" + i);
            if (!strQty.equals("0")) {
              strInputString =
                  strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",")
                      .concat(strControl).concat(",").concat(strPrice).concat("|");
            }
            log.debug(" Input String Inside Loop is -- " + strInputString);
          }
        }
        log.debug(" Input String is " + strInputString);
        /*
         * First need to validations the lot number before calling rule engine or calling the actual
         * procedure Once all the validations are done, then call the rule engine and fi rule
         * messages are not there it will save the details in actual procedure
         */
        strValidationFl = gmInHouseTxnBean.saveItemLotNum(strConsignId, hmParam, strInputString);

        if (strValidationFl.equalsIgnoreCase("N") || strValidationFl.equals("")) {

          if (!strTransType.equals("") && !strIncludeRuleEng.equalsIgnoreCase("NO")
              && strCheckRule.equalsIgnoreCase("Yes")) {

            strRuleEntryFl = "Y";

            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", strTxnStatus);
            request.setAttribute("TxnTypeID", strType);
            request.setAttribute("RE_FORWARD", "gmSetAddRemPartsServlet");
            request.setAttribute("chkRule", "No");
            dispatch("/gmRuleEngine.do", request, response);
            return;// Should not proceed once go to rule engine
          }

          hmReturn =
              gmInHouseTxnBean.saveInLoanerItemConsign(strConsignId, hmParam, strInputString,
                  strUserId, strAction);
        }
        if (strType.equals("40051") || strType.equals("40052") || strType.equals("40053")
            || strType.equals("40054")) {
          hmResult = gmLogis.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);

        } else {
          hmResult = gmInHouseRptBean.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);
        }

        alResult = gmInHouseRptBean.loadSavedSetMaster(strTransIdDash);
        hmReturn.put("SETLOAD", alResult);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hConsignId", strConsignId);
        alReturn = gmCommonBean.getLog(strTransIdDash, "1268");
        request.setAttribute("hmLog", alReturn);
        strDispatchTo = strLogisticsPath.concat("/GmSPControlVerify.jsp");
        if (strTransType.equals("50155") || strTransType.equals("50157")
            || strTransType.equals("50158")) {
          strConsignId = GmCommonClass.parseNull((String) hmResult.get("CONSIGNID"));
          strLoanerType = gmRuleBean.fetchLoanerType(strConsignId);
        }
        log.debug("strLoanerType = " + strLoanerType);

        if (!strTransType.equals("")) {
          if (strType.equals("50161") || strType.equals("50162")) {
            strTransType = "SETRECONFIGWRAPPER";
          }
          if (!strIncludeRuleEng.equalsIgnoreCase("NO") && !strRuleEntryFl.equals("Y")) {
            request.setAttribute("RE_FORWARD", "gmSPControlVerify");
            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", "PROCESS");
            request.setAttribute("TxnTypeID", strType);
            strDispatchTo = "/gmRuleEngine.do";
          }
        }
      }

      else if (strAction.equals("LoadItemShip")) {
        // strTransIdDash =
        // (String)session.getAttribute("strSessConsignId")==null?"":(String)session.getAttribute("strSessConsignId");
        strTransIdDash = GmCommonClass.parseNull(request.getParameter("hId"));
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        strType = GmCommonClass.parseNull(request.getParameter("hType"));
        HashMap hmInHouseShipLists = new HashMap();

        log.debug("strType" + strType);
        log.debug("strConsignId: " + strConsignId + "strTransIdDash: " + strTransIdDash);
        if (strType.equals("40051") || strType.equals("40052") || strType.equals("40053")
            || strType.equals("40054")) {
          hmResult = gmLogis.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);
        } else {
          if (strType.equals("50154")) {
            request.setAttribute("EXTREPLE", "EXTREPLE");
            log.debug("strConsignId: " + strConsignId + "strTransIdDash: " + strTransIdDash);
            hmInHouseShipLists = gmInHouseRptBean.loadInHouseSetShipLists(strTransIdDash);
            hmReturn.put("INHOUSESHIPLIST", hmInHouseShipLists);
          }
          hmResult = gmInHouseRptBean.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);
        }
        strTransType = GmCommonClass.parseNull((String) hmResult.get("CTYPE"));
        // code for location part mapping detail
        if (strType.equals("50161") || strType.equals("50157") || strType.equals("50152")) {
          hmAccess = gmAccessControlBean.getAccessPermissions(strUserId, "1166");
          strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          request.setAttribute("ACCESSFLAG", strAccessFlag);

          hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
          hmParam.put("VMFILEPATH", "properties.labels.operations.inventory.GmPartLocationMap");
          hmParam.put("TXNTYPE", strType);
          hmParam.put("TXNID", strTransIdDash);
          alLcnPartMap = gmLocationBean.fetchPartLocationDetails(hmParam);
          hmParam.put("RLIST", alLcnPartMap);
          hmParam.put("TEMPLATE", "GmPartLocationMap.vm");
          hmParam.put("TEMPLATEPATH", "operations/inventory/templates");
          String strGridXmlData = gmLocationBean.getXmlGridData(hmParam);
          log.debug("strGridXmlData======" + strGridXmlData);
          hmReturn.put("GRIDXMLDATA", strGridXmlData);
        }
        // end...

        alResult = gmInHouseRptBean.loadSavedSetMaster(strTransIdDash);
        hmReturn.put("SETLOAD", alResult);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "Verification");
        request.setAttribute("hConsignId", strConsignId);
        request.setAttribute("hFromPage", "GmSetAddRemPartsServlet");
        strDispatchTo = strLogisticsPath.concat("/GmSPControlVerify.jsp");
        if (strTransType.equals("50155") || strTransType.equals("50157")
            || strTransType.equals("50158")) {
          strConsignId = GmCommonClass.parseNull((String) hmResult.get("CONSIGNID"));
          strLoanerType = gmRuleBean.fetchLoanerType(strConsignId);
        }
        log.debug("strLoanerType = " + strLoanerType);
        if (!strTransType.equals("") && !strLoanerType.equals("")) {
          strTransType = gmRuleBean.fetchTransId(strTransType, strLoanerType);
        }
        strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;
        log.debug("strTransType = " + strTransType);
        // if(strType.equals("50157") ||strType.equals("50158") || strType.equals("50155")
        // ||strType.equals("50160") ||strType.equals("50161")||strType.equals("50162")||
        // strType.equals("40053"))
        if (!strTransType.equals("")) {
          if (strType.equals("50161") || strType.equals("50162")) {
            strTransType = "SETRECONFIGWRAPPER";
          }
          if (!strIncludeRuleEng.equalsIgnoreCase("NO")) {
            request.setAttribute("RE_FORWARD", "gmSPControlVerify");
            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", "PROCESS");
            request.setAttribute("TxnTypeID", strType);
            strDispatchTo = "/gmRuleEngine.do";
          }
        }
      }

      else if (strAction.equals("Verify")) {

        strTransIdDash =
            request.getParameter("hTxnId") == null ? "" : request.getParameter("hTxnId");
        strConsignId =
            request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
        strType = GmCommonClass.parseNull(request.getParameter("hType"));
        String strPnumLcn = GmCommonClass.parseNull(request.getParameter("hpnumLcnStr"));
        hmParam.put("TXNID", strTransIdDash);

        // hmReturn =
        // gmInHouseTxnBean.saveInLoanerItemConsign(strConsignId,
        // hmParam, strInputString, strUserId, strAction);

        if (strType.equals("40051") || strType.equals("40052") || strType.equals("40053")
            || strType.equals("40054")) {
          hmResult = gmLogis.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);

        } else {
          // ***********************************************************
          // these block is for saving ship out information
          // when "loaner extension w/replenish"
          // below code added for loaner extension changes
          // ************************************************************
          if (strType.equals("50154")) {
            // 50154 = Move from Shelf to Loaner Replenish
            strShipTo = GmCommonClass.parseNull(request.getParameter("Cbo_ShipTo"));

            // strShipToId = GmCommonClass.parseNull(request.getParameter("Cbo_ValuesShip"));
            // String strShipCarr = GmCommonClass.parseNull(request.getParameter("Cbo_Carr"));
            // String strShipMode = GmCommonClass.parseNull(request.getParameter("Cbo_Mode"));
            // String strShipDT = GmCalenderOperations.addDays(0);

            hmParam.put("REFID", strConsignId);
            // 50128 Loaner Extension with Replenish
            hmParam.put("SOURCE", "50128");
            hmParam.put("EXTSHIPTO", strShipTo);
            // hmParam.put("EXTSHIPTOID", strShipToId);
            // hmParam.put("SHIPDT", strShipDT);
            // hmParam.put("SHIPCARR", strShipCarr);
            // hmParam.put("SHIPMODE", strShipMode);
            hmParam.put("SHIPTRACKNO", null);
            hmParam.put("VOIDFL", null);
            hmParam.put("MODE", "INSERT");
            hmParam.put("SHIPID", null);
            // will call saveLoanerShipInfo in the
            // gmInHouseTxnBean.saveInLoanerItemConsign
            // gmShippingTransBean.saveLoanerShipInfo(hmParas);
          }// end of loaner extension changes
           // after save ("verify") should go to same screen as before
           // only there is not "shipout info section"
          hmParam.put("USERID", strUserId);
          hmParam.put("TYPE", strType);
          hmParam.put("PNUMLCNSTR", strPnumLcn);
          hmParam.put("OPERATION", "93327");
          if (strType.equals("50161") || strType.equals("50157") || strType.equals("50152")) {
            hmAccess = gmAccessControlBean.getAccessPermissions(strUserId, "1166");
            strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
            if (!strAccessFlag.equals("Y")) {
              throw new AppError("", "20697");
            }
          }

          // strCreateShipFL = GmCommonClass.parseNull(request.getParameter("Chk_CreateShipFl"));
          // hmParam.put("SHIPFL", strCreateShipFL);
          hmReturn =gmInHouseTxnBean.saveInLoanerItemConsign(strConsignId, hmParam, strInputString,strUserId, strAction);
          
          hmResult = gmInHouseRptBean.viewInHouseTransDetails(strTransIdDash);
          hmReturn.put("CONDETAILS", hmResult);
          
          String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
          strTransType = GmCommonClass.parseZero((String) hmParam.get("TYPE"));
          //String strCnID = GmCommonClass.parseNull((String)hmParam.get("REFID"));
          //strTransType = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));
          String strStatus =   GmCommonClass.parseNull((String)hmResult.get("SFL"));  //When verifying transaction BLLN, LNQN status flag need to PMT-38155 
          if (strTransType.equals("100063")||strTransType.equals("50158")||strTransType.equals("50156"))
          {
          GmInvLocationBean gmInvLocationBean = new GmInvLocationBean();
          HashMap hmLnParam = new HashMap ();
          hmLnParam.put("TRANS_ID", strTxnId);
          hmLnParam.put("REFID", strConsignId);
          hmLnParam.put("TRANS_TYPE", strTransType);
          hmLnParam.put("USER_ID", strUserId);
          hmLnParam.put("WAREHOUSE_TYPE", "6");
          hmLnParam.put("COMPANY_INFO",strCompanyInfo);
          hmLnParam.put("TRANS_STATUS",strStatus);
          gmInvLocationBean.updateLoanerStockSheetInfo(hmLnParam);
          }
          
        }
        alResult = gmInHouseRptBean.loadSavedSetMaster(strTransIdDash);
        hmReturn.put("SETLOAD", alResult);
        // code for location part mapping detail
      //removed condition (50152) for PC-4273 change
        if (strType.equals("50161") || strType.equals("50157") ) {
          hmParam.put("TXNTYPE", strType);
          hmParam.put("TXNID", strTransIdDash);
          alLcnPartMap = gmLocationBean.fetchPartLocationDetails(hmParam);
          hmParam.put("RLIST", alLcnPartMap);
          hmParam.put("TEMPLATE", "GmPartLocationMap.vm");
          hmParam.put("TEMPLATEPATH", "operations/inventory/templates");
          String strGridXmlData = gmLocationBean.getXmlGridData(hmParam);
          hmReturn.put("GRIDXMLDATA", strGridXmlData);
        }
        // end..

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "Verification");
        request.setAttribute("VerificationStatus", "Verified");
        request.setAttribute("hConsignId", strConsignId);
        request.setAttribute("hFromPage", "GmSetAddRemPartsServlet");
        strDispatchTo = strLogisticsPath.concat("/GmSPControlVerify.jsp");
        if (strTransType.equals("50155") || strTransType.equals("50157")
            || strTransType.equals("50158")) {
          strConsignId = GmCommonClass.parseNull((String) hmResult.get("CONSIGNID"));
          strLoanerType = gmRuleBean.fetchLoanerType(strConsignId);
        }
        log.debug("strLoanerType = " + strLoanerType);
        if (!strTransType.equals("") && !strLoanerType.equals("")) {
          strTransType = gmRuleBean.fetchTransId(strTransType, strLoanerType);
        }
        strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;
        log.debug("strTransType = " + strTransType);
        if (!strTransType.equals("")) {
          if (strType.equals("50161") || strType.equals("50162")) {
            strTransType = "SETRECONFIGWRAPPER";
          }
          if (!strIncludeRuleEng.equalsIgnoreCase("NO")) {
            request.setAttribute("RE_FORWARD", "gmSPControlVerify");
            request.setAttribute("RE_TXN", strTransType);
            request.setAttribute("txnStatus", "VERIFY");
            request.setAttribute("TxnTypeID", strType);
            strDispatchTo = "/gmRuleEngine.do";
          }
        }

      }
      request.setAttribute("hType", strType);
      request.setAttribute("hOpt", strOpt);
      session.setAttribute("hOpt", strOpt);

      if (strAction.equals("EditControl") || strAction.equals("SaveControl")
          || strAction.equals("LoadItemShip") || strAction.equals("PicSlip")
          || strAction.equals("Verify") || strAction.equals("ReleaseControl")) {
        dispatch(strDispatchTo, request, response);
      } else {
        gotoPage(strDispatchTo, request, response);
      }

    }// End of try
    catch (AppError e) {

      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      gotoPageOld(strComnErrorPath, request, response);
      return;
    } catch (Exception e) {
      // session.setAttribute("hAppErrorMessage",e.getMessage());
      e.printStackTrace();
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmConsignInHouseServlet
