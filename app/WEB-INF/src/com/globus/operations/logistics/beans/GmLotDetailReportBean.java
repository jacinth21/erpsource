package com.globus.operations.logistics.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLotDetailReportBean extends GmBean {
	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	 /**
	  *  Default Constructor
	  */
	public GmLotDetailReportBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	/**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	public GmLotDetailReportBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	/**
	 * fetchLotDetailProdInfo - used to fetch Product Information for Lot detail report
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotDetailProdInfo(HashMap hmParam) throws AppError {
			
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
	    String strLotProductInfo = "";
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_detail_prod_info", 4);
		gmDBManager.setString(1, strLotNumber);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.setString(3, strSearch);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.execute();
		
		strLotProductInfo = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strLotProductInfo;

	  }
	/**
	 * fetchLotDetailReceivingInfo - used to fetch Receiving Information for Lot detail report
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotDetailReceivingInfo(HashMap hmParam) throws AppError {
		
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
	    String strLotRecInfo = "";
	   
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	    
	    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_detail_receiving_info", 4);
		gmDBManager.setString(1, strLotNumber);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.setString(3, strSearch);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.execute();
		
		strLotRecInfo = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strLotRecInfo;

	  }
	/**
	 * fetchLotDetailInventoryInfo - used to fetch Inventory inhouse Information for Lot detail report
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotDetailInventoryInfo(HashMap hmParam) throws AppError {
		
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strLotInvInfo = "";
		
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	    
	    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_detail_inventory_info", 4);
		gmDBManager.setString(1, strLotNumber);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.setString(3, strSearch);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.execute();
		
		strLotInvInfo = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strLotInvInfo;

	  }
	/**
	 * fetchLotDetailFieldSalesInfo - used to fetch Field Sales Information for Lot detail report
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotDetailFieldSalesInfo(HashMap hmParam) throws AppError {
		
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strLotFieldSalesInfo= "";
		
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	    
	    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_detail_fieldsales_info", 4);
		gmDBManager.setString(1, strLotNumber);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.setString(3, strSearch);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.execute();
		
		strLotFieldSalesInfo = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strLotFieldSalesInfo;

	 }
	/**
	 * fetchLotDetailConsignmentInfo - used to fetch Consignment Information for Lot detail report
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotDetailConsignmentInfo(HashMap hmParam) throws AppError {
		
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strLotConsignmentInfo = "";
		
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	    
	    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_detail_consignment_info", 4);
		gmDBManager.setString(1, strLotNumber);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.setString(3, strSearch);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.execute();
		
		strLotConsignmentInfo = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strLotConsignmentInfo;

	 }
	/**
	 * fetchLotDetailLoanerInfo - used to fetch Loaner Information for Lot detail report
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotDetailLoanerInfo(HashMap hmParam) throws AppError {
		
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strLotLoanerInfo = "";
		
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	    
	    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_detail_loaner_info", 4);
		gmDBManager.setString(1, strLotNumber);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.setString(3, strSearch);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.execute();
		
		strLotLoanerInfo = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strLotLoanerInfo;

	 }
	/**
	 * fetchLotTransHistory - used to fetch transaction history for lot detail report of inventory
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotTransHistory(HashMap hmParam) throws AppError {
		
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
	    String strInvType = GmCommonClass.parseNull((String) hmParam.get("STRINVTYPE"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		String strFieldSalesNm = GmCommonClass.parseNull((String) hmParam.get("FIELDSALESNM"));
		String strSetId = GmCommonClass.parseNull((String) hmParam.get("STRSETID"));
		String strLotTxnHistoryInfo = "";
		
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	    
	    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_txn_history",8);
		gmDBManager.setString(1, strLotNumber);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.setString(3, strInvType);
		gmDBManager.setString(4, strSearch);
		gmDBManager.setString(5, strCompanyId);
		gmDBManager.setString(6, strFieldSalesNm);
		gmDBManager.setString(7, strSetId);
		gmDBManager.registerOutParameter(8, OracleTypes.CLOB);
		gmDBManager.execute();
		
		strLotTxnHistoryInfo = GmCommonClass.parseNull(gmDBManager.getString(8));
		gmDBManager.close();

		return strLotTxnHistoryInfo;

	 }
		
}
