package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisReportBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingReportBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingWrapperBean;
import com.globus.common.beans.GmLogger;
public class GmKitMappingBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public GmKitMappingBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
    /**
	   * fetchSetNmList - To fetch the set List details for Kit Mapping Screen
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
	*/
	 public String fetchSetNmList(HashMap hmParam) throws AppError {

		  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		  String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		  int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		  String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		  strType = "1601";
		  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		  gmCacheManager.setKey(strKey);
		  gmCacheManager.setSearchPrefix(searchPrefix);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  HashMap hmdata = new HashMap();
		  hmdata.put("TYPE", strType);
		  hmParam.put("TYPE", strType);
		  String strReturn = gmCacheManager.returnJsonString(loadSetNmList(hmParam));
		
		  return strReturn; 
	  }
	  
	  /**
	   * loadSetNmList - To fetch the set List details
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
	 */
	  public String loadSetNmList(HashMap hmParam) throws AppError {
		    
		    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));;
		    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
		    String strGroupPartMapping = GmCommonClass.parseNull((String) hmParam.get("GROUPPART"));
		    String strCompanyId = getCompId();
		    ArrayList alReturn = new ArrayList();
		    HashMap hmRuleData = new HashMap();
		    hmRuleData.put("RULEID", strDeptId);
		    hmRuleData.put("RULEGROUP", "SETMAPACCESS");
		    String strStatus = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
		    strStatus = GmCommonClass.getStringWithQuotes(strStatus);
		    gmDBManager.close();
		    String strSearchSet = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
		    String strSystem = GmCommonClass.parseZero((String) hmParam.get("SYSTEM"));
		    String strHierarchy = GmCommonClass.parseZero((String) hmParam.get("HIERARCHY"));
		    String strCategory = GmCommonClass.parseZero((String) hmParam.get("CATEGORY"));
		    String strSetType = GmCommonClass.parseZero((String) hmParam.get("SETTYPE"));
		    String strSetStatus = GmCommonClass.parseZero((String) hmParam.get("SETSTATUS"));
		    StringBuffer sbQuery = new StringBuffer();
		    sbQuery
		        .append(" SELECT	T207.C207_SET_ID ID, T207.C207_SET_ID||' - ' ||C207_SET_NM NAME ");
		    sbQuery
		        .append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080, t207a_set_link t207a WHERE T207.C207_TYPE = 4070 AND C901_SET_GRP_TYPE ='");
		    sbQuery.append(strType);
		    sbQuery.append("' ");
		    sbQuery.append(" AND T207.C207_SET_ID = T2080.C207_SET_ID");
		    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
		    sbQuery.append(" AND C207_VOID_FL IS NULL ");
		    sbQuery.append(" AND C207_OBSOLETE_FL IS NULL ");
		    sbQuery.append("AND t207.c207_set_id  = t207a.c207_link_set_id (+)");
		    if (!strStatus.equals("")) {
		      sbQuery.append(" AND C901_STATUS_ID IN ('");
		      sbQuery.append(strStatus);
		      sbQuery.append("')");
		    }

		    if (!strSearchSet.equals("")) {
		      sbQuery.append(" AND UPPER(C207_SET_NM) LIKE ('%");
		      sbQuery.append(strSearchSet.toUpperCase());
		      sbQuery.append("%')");
		    }

		    if (!strSystem.equals("0")) {
		      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN ('");
		      sbQuery.append(strSystem);
		      sbQuery.append("')");
		    }

		    if (!strHierarchy.equals("0")) {
		      sbQuery.append(" AND C901_HIERARCHY IN ('");
		      sbQuery.append(strHierarchy);
		      sbQuery.append("')");
		    }

		    if (!strCategory.equals("0")) {
		      sbQuery.append(" AND C207_CATEGORY IN ('");
		      sbQuery.append(strCategory);
		      sbQuery.append("')");
		    }

		   if (!strSetType.equals("0")) {
		      sbQuery.append(" AND C207_TYPE IN ('");
		      sbQuery.append(strSetType);
		      sbQuery.append("')");
		    }

		    if (!strSetStatus.equals("0")) {
		      sbQuery.append(" AND C901_STATUS_ID IN ('");
		      sbQuery.append(strSetStatus);
		      sbQuery.append("')");
		    }


		    sbQuery.append(" AND T2080.C1900_COMPANY_ID = '");
		    sbQuery.append(strCompanyId);
		    sbQuery.append("'");
		    sbQuery.append(" ORDER BY ");

		    sbQuery.append(" UPPER(C207_SET_NM)");
		    log.debug(" Query for loading set map " + sbQuery.toString());
		    return sbQuery.toString();
		  }
	 /**
		   * fetchKitName - To fetch the Kit name to show availability
		   * 
		   * @param String
		   * @return String
		   * @throws AppError
	*/
	  public String fetchKitName(String strKitName) throws AppError {
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_kit_mapping.gm_fch_kit_name", 2);
			gmDBManager.setString(1, strKitName);
			gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
			gmDBManager.execute();
			strKitName  = GmCommonClass.parseNull(gmDBManager.getString(2));
			gmDBManager.close();
			
		return strKitName;
	  
	  }
	/**
	   * fetchSetDetails - To fetch the Set details based on set selection
	   * 
	   * @param String
	   * @return HashMap
	   * @throws AppError
   */
	  public HashMap fetchSetDetails(String strSetID) throws AppError {
		    HashMap hmValues = new HashMap();
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_kit_mapping.gm_fetch_set_details", 2);
			gmDBManager.setString(1, strSetID);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			hmValues  = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
			gmDBManager.close();
			
		return hmValues;
	  
	  } 
  /**
	   * fetchTagDetail - To fetch the Tag details 
	   * 
	   * @param String, String
	   * @return String
	   * @throws AppError
   */
	  public String fetchTagDetail(String strTagNumber,String strSetId,String strKitId) throws AppError {

			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_kit_mapping.gm_fch_tag_num", 4);
			gmDBManager.setString(1, strTagNumber);
			gmDBManager.setString(2, strSetId);
			gmDBManager.setString(4, strKitId);
			gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
			gmDBManager.execute();
			strTagNumber  = GmCommonClass.parseNull(gmDBManager.getString(3));
			gmDBManager.close();
			
		return strTagNumber;
	  
	  } 
	  /**
	   * fetchTagDetails - To fetch the Tag details 
	   * 
	   * @param String, String
	   * @return String
	   * @throws AppError
   */
	  public HashMap fetchTagDetails(String strTagNumber,String strSetId,String strKitId) throws AppError {

		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			HashMap hmReturn = new HashMap();
			gmDBManager.setPrepareString("gm_pkg_kit_mapping.gm_fch_tag_number", 6);
			gmDBManager.setString(1, strTagNumber);
			gmDBManager.setString(2, strSetId);
			gmDBManager.setString(6, strKitId);
			gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
			gmDBManager.execute();
			String strTagNumbers  = GmCommonClass.parseNull(gmDBManager.getString(3));
			String strsetID = GmCommonClass.parseNull(gmDBManager.getString(4));
			String strTagAssotoKit = GmCommonClass.parseNull(gmDBManager.getString(5));
			hmReturn.put("TagNumber",strTagNumbers);
			hmReturn.put("setID",strsetID);
			hmReturn.put("TagNum",strTagAssotoKit);
			gmDBManager.close();
			
		return hmReturn;
	  
	  }
  /**
	   * saveKitMapping - To Save the kit details 
	   * 
	   * @param HashMap
	   * @return HashMap
	   * @throws AppError
   */
	 public HashMap saveKitMapping(HashMap hmParam) throws AppError{

		 HashMap hmReturn = new HashMap();
		 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		 String strKitNm = GmCommonClass.parseNull((String) hmParam.get("KITNM"));
		 String strKitId = GmCommonClass.parseNull((String) hmParam.get("KITID"));
		 String strActiveFL = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFL"));
		 String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		 if(strActiveFL.equals("on")){
			 strActiveFL="Y";
		 }else{
			 strActiveFL="N";
		 }
		 String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		 String strTagID = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
		 String strKitName = "";
		 gmDBManager.setPrepareString("gm_pkg_kit_mapping.gm_sav_kit_details", 6);
		 gmDBManager.setString(1, strKitNm);	 
		 gmDBManager.setString(2, strActiveFL);
		 gmDBManager.setString(3, strSetID);
		 gmDBManager.setString(4, strTagID);
		 gmDBManager.setString(5, strKitId);
		 gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		 gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
		 gmDBManager.setString(6, strUserId);
		 gmDBManager.execute();
		 strKitName = GmCommonClass.parseNull(gmDBManager.getString(1));
		 strKitId  = GmCommonClass.parseNull(gmDBManager.getString(5));
		 hmReturn.put("KITNM",strKitName);
		 hmReturn.put("KITID",strKitId);
		 if (!strLogReason.equals("")) {
				GmCommonBean gmCommonBean = new GmCommonBean();
				gmCommonBean.saveLog(gmDBManager, strKitId, strLogReason, strUserId, "26240460"); // // 26240460 - Log Code Number for Kit Map.
			}
		 gmDBManager.commit();
		return hmReturn;
		 
	 }
  /**
	   * fetchKitNmList - To fetch the kit name for autocomplete dropdown kil list 
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
  */
	 public String fetchKitNmList(HashMap hmParam) throws AppError {

		  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		  String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		  int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		  String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		
		  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		  gmCacheManager.setKey(strKey);
		  gmCacheManager.setSearchPrefix(searchPrefix);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  HashMap hmdata = new HashMap();
		  hmdata.put("TYPE", strType);
		  
		  String strReturn = gmCacheManager.returnJsonString(loadKitNmList(hmParam));
		
		  return strReturn; 
	  }
 /**
	   * loadKitNmList - To fetch the kit name for autocomplete dropdown kil list 
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
 */
  public String loadKitNmList(HashMap hmParam) throws AppError {
		  
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		  StringBuffer sbQuery = new StringBuffer();
		  String strCompId = getGmDataStoreVO().getCmpid();
		  if(strCompId.equals("")){
			  strCompId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		  }
		  String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFL")); 
		  sbQuery.append(" SELECT t2078.C2078_KIT_MAP_ID ID, t2078.C2078_KIT_NM NAME");
		  sbQuery.append(" FROM T2078_KIT_MAPPING t2078 ");  
		  sbQuery.append(" WHERE C2078_VOID_FL is NULL ");
		  if(strActiveFl.equals("Y")){
			  sbQuery.append(" AND C2078_ACTIVE_FL ='Y' ");
		  }
		  sbQuery.append(" AND T2078.C1900_COMPANY_ID = '"+strCompId+"' order by NAME ASC"); 
		  log.debug("sbQuery.toString() --> "+sbQuery.toString());

		  return sbQuery.toString();	  
	} 
  /**
   * fetchKitMapDtls - To fetch the kit mapped details 
   * 
   * @param String
   * @return HashMap
   * @throws AppError
*/
  public ArrayList fetchKitMapDtls(String strKitName) throws AppError {
	
	  	ArrayList alList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_kit_mapping.gm_fetch_kit_details", 2);
		gmDBManager.setString(1, strKitName);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList  = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
	return alList;
  
  }  
  /**
   * fetchReport - To fetch the kit mapped details for report screen
   * 
   * @param 
   * @return ArrayList
   * @throws AppError
*/
  public ArrayList fetchReport(HashMap hmParam) throws AppError {

	    String strKitName = GmCommonClass.parseZero((String) hmParam.get("KITID"));
	    String strSetId = GmCommonClass.parseZero((String) hmParam.get("SETID"));
	    String strKitStatus = GmCommonClass.parseZero((String) hmParam.get("KITSTATUS"));
	    String strTagId = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
	    String strCaseId = GmCommonClass.parseNull((String) hmParam.get("CASEID"));
	    log.debug("strTagId=>"+strTagId+" strCaseId=>"+strCaseId);
	    StringBuffer sbQuery = new StringBuffer();
	    ArrayList alReturn = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strCompdfmt = getGmDataStoreVO().getCmpid();
	    sbQuery.append(" select t.*,GET_CODE_NAME(C901_CASE_STATUS) CASESTATUS from( ");
	    sbQuery.append(" select T2078.C2078_KIT_NM KNAME,T2079.C207_SET_ID SID,T207.C207_SET_NM SNAME,T2078.C2078_ACTIVE_FL STATUS,T2079.C5010_TAG_ID TAGID,gm_pkg_kit_mapping.GETCASE_INFO_ID(T2078.C2078_KIT_MAP_ID) CASEID,T2078.C2078_VOID_FL,T2079.C2079_LAST_UPDATED_DATE UPDATEDDATE, ");
	    sbQuery.append(" get_user_name(T2079.C2079_LAST_UPDATED_BY) UPDATEDBY from T7100_CASE_INFORMATION t7100,T2078_KIT_MAPPING T2078,T2079_KIT_MAP_DETAILS T2079,t207_set_master T207");
	    sbQuery.append(" WHERE T2078.C2078_KIT_MAP_ID = T2079.C2078_KIT_MAP_ID AND T2079.C207_SET_ID = T207.C207_SET_ID ");
	    sbQuery.append(" AND T2078.C2078_LAST_UPDATED_TRANS_ID        = T7100.C7100_CASE_ID(+)  ");	
	    sbQuery.append(" AND T2078.C1900_COMPANY_ID = '"+getCompId()+"'");

	    if ((!strSetId.equals("0")) && (!strSetId.equals(""))) {
		    sbQuery.append(" AND T2079.C207_SET_ID = '" + strSetId + "'");
		}
	    if(!strTagId.equals("")){
	    	sbQuery.append(" AND T2079.c5010_tag_id  in ( SELECT LPAD('" + strTagId + "','7','0') from dual) ");
	    }
	    if ((!strKitName.equals("0")) &&(!strKitName.equals(""))) {
	    	sbQuery.append(" AND T2078.C2078_KIT_MAP_ID = '" + strKitName + "'");
	    }
	    if (strKitStatus.equals("0")) { 
		    sbQuery.append(" AND T2078.C2078_ACTIVE_FL = 'Y' ");
		}else if(strKitStatus.equals("1")){
			sbQuery.append(" AND T2078.C2078_ACTIVE_FL = 'N' ");
		}
	    sbQuery.append(" AND T7100.C7100_VOID_FL IS NULL AND T2078.C2078_VOID_FL is NULL AND T2079.C2079_VOID_FL IS NULL AND T207.C207_VOID_FL IS NULL ");
	    sbQuery.append(" order by UPPER(KNAME) ASC"); 
	    sbQuery.append(")t,t7100_case_information t7100 where t.caseid=t7100.c7100_case_id (+) ");
	    if(!strCaseId.equals("")){
			sbQuery.append(" AND t7100.c7100_case_id like UPPER('%");
			sbQuery.append(strCaseId);
			sbQuery.append("%')");
		}
	    log.debug("sbQuery.toString()>>>>>>" + sbQuery.toString());
	    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
	    gmDBManager.close();
	    return alReturn;

}
  /**
   * removeKitMapping - To remove set from mapped kit 
   * 
   * @param String,String
   * @return 
   * @throws AppError
*/
 public String removeKitMapping(String strsetID, String strkitID,String strTagID) throws AppError{

	 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	 gmDBManager.setPrepareString("gm_pkg_kit_mapping.gm_rem_set_id", 4);
	 gmDBManager.setString(1, strsetID); 
	 gmDBManager.setString(2, strkitID);
	 gmDBManager.setString(3, strTagID);
	 gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	 gmDBManager.execute();
	 String strCount = GmCommonClass.parseNull(gmDBManager.getString(4)); 
	 gmDBManager.commit();
	 return strCount;
 } 
  
 /**
  * @param strKey
  * @param StrCompanyFl
  * @param strPrimaryCmpyId
  * @return
  * @throws AppError
  */
 public String getAutoCompleteKeys(String strKey, String StrCompanyFl,String strPrimaryCmpyId) throws AppError {
   String strReturnKey = "";
   log.debug(" before strReturnKey ==> " + strKey);
   String strLocale = "";
   if (getComplangid().equals("10306122")) { // 10306122 Japanese
     strLocale = "JP";
   }

   // based on locale to get the Redis key
   GmResourceBundleBean rbRedis =
       GmCommonClass.getResourceBundleBean("properties.redis.GmRedis", strLocale);
   String strPropertiesValue = GmCommonClass.parseNull(rbRedis.getProperty(strKey));
   strPropertiesValue = strPropertiesValue.equals("") ? strKey : strPropertiesValue;
   String strCompanyId = getCompId();
   if(strCompanyId.equals("")){
   	strCompanyId = strPrimaryCmpyId;
   }
   if (StrCompanyFl.equalsIgnoreCase("Y")) {
     strPropertiesValue = strPropertiesValue + ":" + strCompanyId;
   }
   strReturnKey = strPropertiesValue + ":AUTO";
   log.debug(" After strReturnKey ==> " + strReturnKey);
   return strReturnKey;
 }
 
 /**
  * UpdateDatatoRedisKeyJMS - This method used to update set details
  * 
  * @param - HashMap:hmParam
  * @return - Void
  * @exception - AppError
  */
 	public void UpdateDatatoRedisKeyJMS(HashMap hmParam) throws AppError {
 	    log.debug("UpdateDatatoRedisKeyJMS---------");
 		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();	      
 	      // KIT_SET_SHIP_CONSUMER_CLASS is mentioned in JMSConsumerConfig. properties file.This key contain the
 	      // path for : com.globus.jms.consumers.processors.GmKitSetShipConsumerJob
 	    String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("KIT_SET_SHIP_CONSUMER_CLASS"));
 	    hmParam.put("HMHOLDVALUES", hmParam);
 	    hmParam.put("CONSUMERCLASS", strConsumerClass);
 	    log.debug("strConsumerClass=>"+strConsumerClass+" hmParam=>>>"+hmParam);
 	    gmConsumerUtil.sendMessage(hmParam);      
 	}
 	 /**
 	  * UpdateParttoRedisKeyJMS - This method used to update part details
 	  * 
 	  * @param - HashMap:hmParam
 	  * @return - Void
 	  * @exception - AppError
 	  */
 	 	public void UpdateParttoRedisKeyJMS(HashMap hmParam) throws AppError {
 	 	    
 	 		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();	      	 	    
 	 	      // KIT_PART_CONSUMER_CLASS is mentioned in JMSConsumerConfig. properties file.This key contain the
 	 	      // path for : com.globus.jms.consumers.processors.GmKitPartConsumerJob
 	 	    String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("KIT_PART_CONSUMER_CLASS"));
 	 	    hmParam.put("HMLNPARAM", hmParam);
 	 	    hmParam.put("CONSUMERCLASS", strConsumerClass);
 	 	    log.debug(" strConsumerClass=>"+strConsumerClass+" hmParam=>>>"+hmParam);
 	 	    gmConsumerUtil.sendMessage(hmParam);   
 	 	}
 		 /**
 	 	  * removeSetKey - This method used to remove the redis key
 	 	  * 
 	 	  * @param - String
 	 	  * @return - Void
 	 	  * @exception - AppError
 	 	  */
 	 	 	public void removeSetKey(String strDistId) {
 	 	 		log.debug("strDistId---->"+strDistId);
 	 			if (!strDistId.equals("")) {
 	 				try {
 	 					GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(getGmDataStoreVO());
 	 					GmCacheManager gmCacheManager = new GmCacheManager();
 	 					String strKey = gmMasterRedisReportBean.getAutoCompleteKeys("SET_TAG_LIST", "Y", strDistId);
 	 					gmCacheManager.removeKey(strKey);
 	 				} catch (Exception e) {
 	 					e.printStackTrace();
 	 					log.error("Exception in Redis update key : " + e.getMessage());
 	 				}
 	 			}
 	 		}	
}
