package com.globus.operations.logistics.beans;

import java.util.HashMap;
import java.util.Date;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
	/**
	 * GmMultiLoanerExtnBean - multi loaner extension Bean class
	 * @author rajan
	 *
	 */
public class GmMultiLoanerExtnBean extends GmBean {
    
	/**
      *  it is used common functionality apply need for gmCommonClass 
      */
	 GmCommonClass gmCommonClass = new GmCommonClass();
	 
	 /**
	  * This used to get the instance of this class for enabling logging purpose
	  */
	 Logger log = GmLogger.getInstance(this.getClass().getName()); 
	
	 /**
	  * GmMultiLoanerExtnBean - constructor for gmDataStoreVO
	  * @param gmDataStoreVO
	  */
	 public GmMultiLoanerExtnBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
	}
	 
	 /**
	  * fetchLoanerList  - this is used to fetch the multi loaner extension details
	  * @param hmParam
	  * @return
	  * @throws AppError
	  */
	public String fetchLoanerList(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 
		String strReturn = "";
		String strSetId =  GmCommonClass.parseNull((String) hmParam.get("STRSETID"));
		String strSetName = GmCommonClass.parseNull((String) hmParam.get("STRSETNAME"));
		String strConsignId = GmCommonClass.parseNull((String) hmParam.get("STRCONSIGNMENTID")); 
		String strSalesRep = GmCommonClass.parseNull((String) hmParam.get("STRSALESREP"));
		String strDistributor = GmCommonClass.parseNull((String) hmParam.get("STRDISTRIBUTOR"));
		String strCompId =  GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
		String strCompPlantId =  GmCommonClass.parseNull((String)getGmDataStoreVO().getPlantid());
		String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
		
		try {
		StringBuffer strQuery = new StringBuffer();
		strQuery.append(" SELECT JSON_ARRAYAGG ( JSON_OBJECT ( 'CONSID' VALUE CONID, 'SETID' VALUE setid, ");
		strQuery.append(" 'NAME' VALUE sname, 'HOLDFL' VALUE holdfl, 'SFL' VALUE sfl, ");
		strQuery.append(" 'PNAME' VALUE pname, 'PID' VALUE pid, 'ETCHID' VALUE etchid, 'LSFL' VALUE lsfl, ");
		strQuery.append(" 'LOANSFL' VALUE loansfl, 'EDATE' VALUE edate, 'LDATE' VALUE ldate, 'LOANNM' VALUE loannm, ");
		strQuery.append(" 'OVERDUE' VALUE overdue, 'CMTCNT' VALUE cmtcnt, 'REQID' VALUE reqid, ");
		strQuery.append(" 'RECONCMTCNT' VALUE reconcmtcnt, 'PRIORITY' VALUE priority,'ACCID' VALUE accid, ");
		strQuery.append(" 'REPNAME' VALUE repname,'DNAME' VALUE dname ) RETURNING CLOB ) FROM ( ");
		strQuery.append("  SELECT t504.c504_consignment_id conid, t504.c207_set_id setid,  get_set_name(t504.c207_set_id) sname, ");   
		strQuery.append("  t504.c504_hold_fl holdfl,t504.c504_status_fl sfl, get_code_name(t504.c504_inhouse_purpose) pname, ");
		strQuery.append("  t504.c504_inhouse_purpose pid, t504a.c504a_etch_id etchid, t504a.c504a_status_fl lsfl, ");
		strQuery.append("  gm_pkg_op_loaner.get_loaner_status(t504a.c504a_status_fl) loansfl, TO_CHAR(t504a.c504a_expected_return_dt,'"+strCompDateFmt+"') edate, ");
		strQuery.append("  TO_CHAR(t504a.c504a_loaner_dt,'"+strCompDateFmt+"') ldate, get_cs_rep_loaner_bill_nm_add(t504.c504_consignment_id) loannm, ");
		strQuery.append("  get_work_days_count(TO_CHAR(trunc(t504a.c504a_expected_return_dt), get_rule_value('DATEFMT', 'DATEFORMAT')), ");
		strQuery.append("  TO_CHAR(trunc(current_date),  get_rule_value('DATEFMT', 'DATEFORMAT'))) overdue, ");
		strQuery.append("  get_log_flag(t504a.c504_consignment_id, 1222) cmtcnt, get_ln_req_det_id(t504a.c504_consignment_id) reqid, ");	
		strQuery.append("  get_log_flag(get_ln_req_det_id(t504a.c504_consignment_id),4000316) reconcmtcnt, ");
		strQuery.append("  get_code_name(t504a.c504a_loaner_priority) priority, t504.c704_account_id accid, ");
		strQuery.append("  DECODE('"+ strCompId + "','103097', NVL(t703.c703_sales_rep_name_en,t703.c703_sales_rep_name),t703.c703_sales_rep_name) repname, ");
		strQuery.append("  get_distributor_name(t504.C701_DISTRIBUTOR_ID) DNAME ");
		strQuery.append("  FROM ");
	    strQuery.append("  t504_consignment t504, ");
	    strQuery.append("  t504a_consignment_loaner t504a, ");
	    strQuery.append("  t207_set_master t207, ");
	    strQuery.append("  t703_sales_rep t703, ");
	    strQuery.append(" ( ");
	    strQuery.append(" SELECT t504b.c1900_company_id, t504b.c504_consignment_id, t504b.c703_sales_rep_id FROM t504a_loaner_transaction t504b ");
	    strQuery.append(" WHERE ");
	    if (!strConsignId.equals("")) { 
	    strQuery.append(" UPPER(t504b.c504_consignment_id) LIKE UPPER ('%" );    
	    strQuery.append(strConsignId);
	    strQuery.append("%') AND");
	    }
	    strQuery.append(" t504b.c504a_return_dt IS NULL ");
	    strQuery.append(" AND t504b.c504a_void_fl IS NULL ");
	    strQuery.append(" ) t504b ");
	    strQuery.append(" WHERE ");	
	    strQuery.append(" t504.c504_status_fl = '4' ");
	    strQuery.append(" AND t504.c504_verify_fl = '1' ");
	    strQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id (+) ");
	    strQuery.append(" AND (t504.C5040_PLANT_ID = "+strCompPlantId+" OR nvl(t504b.c1900_company_id , t504.c1900_company_id)  = "+strCompId +") ");
	    strQuery.append(" AND T504.C504_VOID_FL IS NULL ");
	    strQuery.append(" AND t504.c207_set_id IS NOT NULL ");
	    strQuery.append(" AND t504.c5040_plant_id = t504a.c5040_plant_id ");	
	    strQuery.append(" AND t504.c504_type = '4127' ");

	    if (!strSetId.equals("")) {                            // Set Id
	    strQuery.append(" AND T504.C207_SET_ID = '");
	    strQuery.append(strSetId);
	    strQuery.append("'");
	 	 }
	    strQuery.append(" AND t207.c207_set_id = t504.c207_set_id ");
	    strQuery.append(" AND ( t504.c5040_plant_id = "+strCompPlantId);		
	    strQuery.append(" OR nvl(t504b.c1900_company_id, t504.c1900_company_id) = "+strCompId +" ) ");	
	    strQuery.append(" AND t504.c504_void_fl IS NULL ");
	    strQuery.append(" AND t504.c207_set_id IS NOT NULL ");
	    strQuery.append(" AND t504.c5040_plant_id = t504a.c5040_plant_id ");
	    
	    if(!strSetName.equals("")) {                            // Set Name
	    strQuery.append(" AND UPPER(t207.c207_set_nm) LIKE UPPER ('%" ); 
	    strQuery.append(strSetName);
	    strQuery.append("%') ");
	    }
	    strQuery.append(" AND t504a.c504a_status_fl IN ( '20' ) ");
	    
	    if(!strConsignId.equals("")) {                          // Consignment id
		 strQuery.append(" AND UPPER(t504.c504_consignment_id) LIKE UPPER ('%" );    
		 strQuery.append(strConsignId);
		 strQuery.append("%') ");
	    }
	   if(!strDistributor.equals("")) {                          // Distributor
	    strQuery.append(" AND  t504.C701_DISTRIBUTOR_ID = '"); 
	    strQuery.append(strDistributor);
	    strQuery.append("'");
	 	}
	    strQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id ");
	   if(!strSalesRep.equals("")) {                           // Sales Rep
	    strQuery.append(" AND t504b.c703_sales_rep_id ='");
		strQuery.append(strSalesRep);
	    strQuery.append("'");
	   }
	   strQuery.append(" AND t703.c703_sales_rep_id = t504b.c703_sales_rep_id "); 
	   strQuery.append(" AND t703.c703_void_fl IS NULL ");
	   strQuery.append(" ORDER BY ");
	   strQuery.append(" setid, t504.c504_created_date, t504.c504_inhouse_purpose DESC ) ");
		
	   log.debug("fetchLoanerList==> "+strQuery.toString());
		
	   strReturn = gmDBManager.queryJSONRecord(strQuery.toString());
	   
	  }catch (Exception e) {
			e.printStackTrace();
		}
	   
	return strReturn;
	}
	
	/**
	 * saveLoanerExtDtl   - this is used to save the multi loaner extension details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public String saveLoanerExtDtl(HashMap hmParam) throws AppError, Exception {
		log.debug("saveLoanerExtDtl==> ");
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		String strSuccessMessage = "";
		String strConsignId =  GmCommonClass.parseNull((String) hmParam.get("STRCONSIGNMENTID"));
		String strReasonType = GmCommonClass.parseNull((String) hmParam.get("STRREASONTYPE"));
		String strComment = GmCommonClass.parseNull((String) hmParam.get("STRCOMMENTS"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID")); 
		String strReqTxFl =  GmCommonClass.parseNull((String) hmParam.get("STRREQEXTFL")); 
		String strApproveFl = GmCommonClass.parseNull((String) hmParam.get("STRAPPROVALFL"));
	
	    String strSurgeryDt = GmCommonClass.parseNull((String) hmParam.get("STRSURGERYDT"));
	    String strExtOfDtRtn = GmCommonClass.parseNull((String) hmParam.get("STREXTOFDATERTN"));
	    
	    String strApplNDateFmt = GmCommonClass.parseNull((String)hmParam.get("APPLNDATEFMT"));
	  
	    String strDateFmt = strApplNDateFmt.equals("")?getGmDataStoreVO().getCmpdfmt() : strApplNDateFmt;   
	    
	    Date dtSurgeryDt = (Date) GmCommonClass.getStringToDate(strSurgeryDt, strDateFmt);
	    Date dtExtOfDtRtn = (Date) GmCommonClass.getStringToDate(strExtOfDtRtn, strDateFmt);
	   
		gmDBManager.setPrepareString("gm_pkg_op_multi_loaner_extension.gm_op_sav_multi_loaner_ext", 8);
		gmDBManager.setString(1, strConsignId);
		gmDBManager.setDate(2,dtExtOfDtRtn);
		gmDBManager.setInt(3, Integer.parseInt(strReasonType));
		gmDBManager.setString(4, strComment);
		gmDBManager.setString(5, strUserId);
		gmDBManager.setString(6, strReqTxFl);
		gmDBManager.setDate(7,dtSurgeryDt );
		gmDBManager.setString(8, strApproveFl);
		
	    gmDBManager.execute();
	   
	    if (!strComment.equals("")) {
	        gmCommonBean.saveLog(gmDBManager, strConsignId, strComment, strUserId, "1222");
	    }
	     gmDBManager.commit();
	   
	    String strPendingApprlFl = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strReasonType,"LEEXTAPPR",getGmDataStoreVO().getCmpid()));
	    
	    if(strPendingApprlFl.equals("Y"))
  		  strSuccessMessage = "Loaner " + strConsignId + " submitted for approval.";
  	    else
  		  strSuccessMessage = "Loaner " + strConsignId + " Extended Successfully."; 

		return strSuccessMessage;
	}
}
