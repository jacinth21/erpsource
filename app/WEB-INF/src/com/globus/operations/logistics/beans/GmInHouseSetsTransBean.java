package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jira.beans.GmJIRAHelperBean;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.util.jobs.GmJob;
import com.globus.operations.beans.GmLoanerReconBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInHouseSetsTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  // log4j

  // this will be removed all place changed with GmDataStoreVO constructor

  public GmInHouseSetsTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmInHouseSetsTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public void updateConsignDetails(String strConsignId, String strEtchId, String strPurpose)
      throws AppError {
    HashMap hmReturn = new HashMap();
    ResultSet rs = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_LS_UPD_ASSIGNSETCGY", 3);
    /*
     * register out parameter and set input parameters
     */

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strEtchId);
    gmDBManager.setString(3, strPurpose);

    gmDBManager.execute();
    gmDBManager.commit();

  }


  public HashMap saveInLoanerItemConsign(String strConsignId, HashMap hmParam, String strInputStr,
      String strUsername, String strAction) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";

    HashMap hmReturn = new HashMap();
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    GmLocationBean gmLocationBean = new GmLocationBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strType = GmCommonClass.parseZero((String) hmParam.get("TYPE"));
    String strREASON = GmCommonClass.parseZero((String) hmParam.get("strREASON"));
    // String strShipTo = GmCommonClass.parseZero((String)
    // hmParam.get("SHIPTO"));
    // String strShipToId = GmCommonClass.parseNull((String)
    // hmParam.get("SHIPTOID"));
    String strFinalComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strSource = GmCommonClass.parseNull((String) hmParam.get("SOURCE")); // ,
    String strPnumLcn = GmCommonClass.parseNull((String) hmParam.get("PNUMLCNSTR"));
    String strOperation = GmCommonClass.parseNull((String) hmParam.get("OPERATION"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strCrComments = GmCommonClass.parseNull((String) hmParam.get("CRCOMMENTS"));
    log.debug(" strType is " + strTxnId);
    log.debug(" strType is " + strType);
    log.debug(" strREASON is " + strREASON);
    log.debug(" strTxnId is " + strTxnId);
    log.debug(" strFinalComments is " + strFinalComments);
    log.debug(" strUsername is " + strUsername);
    log.debug(" strInputStr is " + strInputStr);
    log.debug(" strConsignId is " + strConsignId);
    log.debug(" strAction is " + strAction);

    gmDBManager.setPrepareString("GM_LS_UPD_INLOANERCSG", 11);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);

    gmDBManager.setString(1, strTxnId);
    gmDBManager.setInt(2, Integer.parseInt(strType));
    gmDBManager.setInt(3, Integer.parseInt(strREASON));
    gmDBManager.setInt(4, 0);
    gmDBManager.setString(5, "");
    gmDBManager.setString(6, strFinalComments);
    gmDBManager.setString(7, strUsername);
    gmDBManager.setString(8, strInputStr);
    gmDBManager.setString(9, strConsignId);
    gmDBManager.setString(10, strAction);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(11);
    // if 50128: "Loaner Extension with Replenish" then save "ship out
    // infor"

    if (strSource.equals("50128")) {
      // no need to save the ship info now
      // gmShippingTransBean.saveLoanerShipInfo(gmDBManager, hmParam);
      // hmParam.put("SOURCE", "50183");// loaner Extension
      // hmParam.put("USERID", strUsername);
      // gmShippingTransBean.releaseShipping(gmDBManager, hmParam); // no
      // need to call here will be called in the prc
    }
    // code for location part mapping detail
    if (strType.equals("50161") || strType.equals("50157") || strType.equals("50152")) {
      gmLocationBean.savePartLocationDetails(gmDBManager, strTxnId, strPnumLcn, strOperation,
          strUserID);
    }
    if (!strCrComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strTxnId, strCrComments, strUserID, "1268");
    }
    gmDBManager.commit();

    hmReturn.put("MSG", strMsg);

    return hmReturn;
  } // End of saveInLoanerItemConsign

  /**
   * saveLoanerShipTrans - This method will
   * 
   * @param String strConsignId
   * @param HashMap hmParam
   * @param String strUsername
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveLoanerShipTrans(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strLoanerTransId = "";
    String strShipId = "";
    log.debug("Hmparam:" + hmParam);

    String strRefId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strLoanTo = GmCommonClass.parseNull((String) hmParam.get("LOANTO"));
    String strLoanToId = GmCommonClass.parseNull((String) hmParam.get("LOANTOID"));
    String strExpDate = GmCommonClass.parseNull((String) hmParam.get("EDATE"));
    String strReqId = GmCommonClass.parseZero((String) hmParam.get("REQID"));
    String strRepId = GmCommonClass.parseZero((String) hmParam.get("REPID"));
    String strAccId = GmCommonClass.parseZero((String) hmParam.get("ACCID"));
    String strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));

    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));
    String strShipCarr = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strAddressId = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID"));

    gmDBManager.setPrepareString("GM_CS_SAV_LOANER_SHIP", 15);

    gmDBManager.registerOutParameter(14, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(15, java.sql.Types.CHAR);

    gmDBManager.setString(1, strRefId);
    gmDBManager.setInt(2, Integer.parseInt(GmCommonClass.parseZero(strLoanTo)));
    gmDBManager.setString(3, strLoanToId);
    gmDBManager.setString(4, strExpDate);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strReqId);
    gmDBManager.setInt(7, Integer.parseInt(GmCommonClass.parseZero(strRepId)));
    gmDBManager.setInt(8, Integer.parseInt(GmCommonClass.parseZero(strAccId)));

    gmDBManager.setInt(9, Integer.parseInt(GmCommonClass.parseZero(strShipTo)));
    gmDBManager.setString(10, strShipToId);
    gmDBManager.setInt(11, Integer.parseInt(GmCommonClass.parseZero(strShipCarr)));
    gmDBManager.setInt(12, Integer.parseInt(GmCommonClass.parseZero(strShipMode)));
    gmDBManager.setInt(13, Integer.parseInt(GmCommonClass.parseZero(strAddressId)));
    gmDBManager.setString(14, strShipId);
    log.debug("1");
    gmDBManager.execute();
    log.debug("1");
    strLoanerTransId = gmDBManager.getString(15);
    log.debug("proc executed. strLoanerTransId  " + strLoanerTransId);
    strShipId = gmDBManager.getString(14);
    log.debug("1");
    hmReturn.put("LOANERTRANSID", strLoanerTransId);
    hmReturn.put("SHIPID", strShipId);
    // hmParam.put("REFID", strLoanerTransId);
    log.debug("proc executed. strLoanerTransId  " + strLoanerTransId);
    log.debug("proc executed. strShipId  " + strShipId);
    // save shipping details
    // gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
    // for loaner alone doing controlling forcefully as in shipping the
    // status needs to change from 15 to 30
    // hmParam.put("TXNID", strLoanerTransId);
    // gmShippingTransBean.releaseShipping(gmDBManager, hmParam);
    gmDBManager.commit();

    return hmReturn;
  } // End of saveLoanerShipTrans

  /**
   * saveLoanerReturnTrans - This method will
   * 
   * @exception AppError
   */
  public HashMap saveLoanerReturnTrans(String strConsignId, HashMap hmParam, String strUsername)
      throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("hmParam : " + hmParam);
    hmReturn = saveLoanerReturnTrans(gmDBManager, strConsignId, hmParam, strUsername);

    String strTransactionId = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    String strTransType = GmCommonClass.parseNull((String) hmParam.get("TRANSTYPE"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("strAction"));
    log.debug("strTransactionId = " + strTransactionId);
    log.debug("strTransType = " + strTransType);
    log.debug("strInputString = " + strInputString);
    if (!strAction.equalsIgnoreCase("PlaceOrder") && !strTransType.equals("4119")) { // when called
                                                                                     // from loaner
                                                                                     // reconcilation
      saveCharges(gmDBManager, strTransactionId, strTransType, strInputString, strUsername);
    }

    gmDBManager.commit();
    return hmReturn;
  } // End of saveLoanerReturnTrans

  public void saveCharges(GmDBManager gmDBManager, String strTransactionId, String strTransType,
      String strInputString, String strUserId) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_additional_charges", 4);
    gmDBManager.setString(1, strTransactionId);
    gmDBManager.setString(2, strTransType);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
  }

  /**
   * saveLoanerReturnTrans - This method will
   * 
   * @exception AppError
   */
  public HashMap saveLoanerReturnTrans(GmDBManager gmDBManager, String strConsignId,
      HashMap hmParam, String strUsername) throws AppError {
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strId = "";
    String strReplenishId = "";

    log.debug("hmParam: " + hmParam);


    Date dtRetDate = null;
    if (hmParam.get("RETDATE") instanceof java.util.Date) {
      dtRetDate = (Date) hmParam.get("RETDATE");
    }
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFMT"));
    String strRetDate = GmCommonClass.getStringFromDate(dtRetDate, strDateFmt);
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("MISSSTR"));
    String strRetStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100045"));// move to
                                                                                     // shelf
    String strQuarStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100047"));// move to
                                                                                      // Quarantine
    String strRepStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100040"));// repln from
                                                                                      // Shelf
    String strScrapStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100046"));// move to
                                                                                       // Scrap
    String strBckStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100061"));// move to
                                                                                      // Back Order
    String strBulkStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100041"));// repln from
                                                                                       // Bulk
    String strIHRepStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100039"));// repln
                                                                                        // from In
                                                                                        // House
                                                                                        // Shelf
    String strIHMoveStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100048"));// Move to
                                                                                        // In House
                                                                                        // shelf
    String strExtReason = GmCommonClass.parseNull((String) hmParam.get("RTYPE"));

    log.debug("strBckStr:" + strBckStr);
    
    if (strExtReason.equalsIgnoreCase("50322") || strExtReason.equalsIgnoreCase("26240637")) {
      strRetStr = GmCommonClass.parseNull((String) hmParam.get("RETSTR"));
      strQuarStr = GmCommonClass.parseNull((String) hmParam.get("QUARSTR"));
      strRepStr = GmCommonClass.parseNull((String) hmParam.get("REPSTR"));
    }
    String strType = GmCommonClass.parseNull((String) hmParam.get("CTYPE"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMEN"));
    log.debug("strScrapStr " + strScrapStr);
    gmDBManager.setPrepareString("GM_CS_SAV_LOANER_RETURN", 16);
    /*
     * register out parameter and set input parameters
     */


    log.debug("strType: " + strType);

    gmDBManager.registerOutParameter(14, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(15, java.sql.Types.CHAR);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strRetDate);
    gmDBManager.setString(3, strInputStr);
    gmDBManager.setString(4, strRetStr);
    gmDBManager.setString(5, strQuarStr);
    gmDBManager.setString(6, strRepStr);
    gmDBManager.setString(7, strScrapStr);
    gmDBManager.setString(8, strBckStr);
    gmDBManager.setString(9, strBulkStr);
    gmDBManager.setString(10, strIHRepStr);
    gmDBManager.setString(11, strIHMoveStr);
    gmDBManager.setString(12, strType);
    gmDBManager.setString(13, strUsername);
    gmDBManager.setString(16, strExtReason);
    gmDBManager.execute();

    strId = GmCommonClass.parseNull(gmDBManager.getString(14));
    strReplenishId = gmDBManager.getString(15);
    hmReturn.put("ID", strId);
    hmReturn.put("REPLENISHID", strReplenishId);
    log.debug("aft execute " + strId);

    String[] ArrId = new String[5];

    if (!strId.equals("")) {
      ArrId = strId.split(",");
    }
    log.debug("strLog " + strLog);
    if (!strLog.equals("") && !strId.equals("")) {
      if (!strInputStr.equals("")) { // if missing
        log.debug("save process return comments " + ArrId[0]);
        gmCom.saveLog(gmDBManager, ArrId[0], strLog, strUsername, "1246");
      } else {
        log.debug("save accept return comments " + ArrId[0]);
        gmCom.saveLog(gmDBManager, ArrId[0], strLog, strUsername, "1245");
      }
    }
    if (strExtReason.equalsIgnoreCase("50322") && !strComments.equals("")) {
      for (int i = 0; i < ArrId.length; i++) {
        gmCom.saveLog(gmDBManager, ArrId[i], strComments, strUsername, "1241");
      }
    }

    return hmReturn;
  } // End of saveLoanerReturnTrans

  public void saveLoanerAcceptReturn(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    GmLoanerDisputedBean gmLoanerDisputedBean = new GmLoanerDisputedBean(getGmDataStoreVO());
 
    HashMap hmEmailParam = new HashMap();
    Date dtRetDate = null;
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strUsername = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    dtRetDate = (Date) hmParam.get("RETDATE");
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));
    String strLoanTransId = GmCommonClass.parseNull((String) hmParam.get("LOANTRANSID"));
    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid()); 
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_cs_sav_accept_return", 3);
    log.debug("hmParam " + hmParam);
    log.debug("strConsignId " + strConsignId);
    log.debug("strLog " + strLog);
    log.debug("dtRetDate " + dtRetDate);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strUsername);
    gmDBManager.setDate(3, dtRetDate);

    gmDBManager.execute();

    if (!strLog.equals("")) {
    	gmCom.saveLog(gmDBManager, strConsignId , strLog, strUsername, " 1222 ");
    	gmCom.saveLog(gmDBManager, strLoanTransId, strLog, strUsername, "1245");
    }
    gmDBManager.commit();
    
    //initiate JMS call to send disputed loaner email
	hmEmailParam.put("USERID", strUsername);
	hmEmailParam.put("CONSID", strConsignId);
	hmEmailParam.put("COMPANYINFO", strCompanyInfo);
	hmEmailParam.put("LOANTRANSID",strLoanTransId);
	hmEmailParam.put("ACTION","RETURN");
	gmLoanerDisputedBean.initDisputedEmailJMS(hmEmailParam);
	
  }

  /**
   * saveLoanerReconTrans - This method is for Loaner Reconciliation
   * 
   * @param String strConsignId
   * @param String strInputStr
   * @param String strUsername
   * @return strId
   * @exception AppError
   */
  public String saveLoanerReconTrans(String strTransId, HashMap hmParam, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmJIRAHelperBean gmJIRAHelperBean = new GmJIRAHelperBean();
    GmLoanerReconBean gmLoanerReconBean = new GmLoanerReconBean(getGmDataStoreVO());

    String strId = "";

    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("SOLDSTR"));
    String strInputDoStr = GmCommonClass.parseNull((String) hmParam.get("STRRECDO"));
    String strRetStr = GmCommonClass.parseNull((String) hmParam.get("RETSTR"));
    String strWriteStr = GmCommonClass.parseNull((String) hmParam.get("WRITESTR"));
    String strConsignStr = GmCommonClass.parseNull((String) hmParam.get("CONSTR"));
    ArrayList alReconList = new ArrayList();
    ArrayList alMissinList = new ArrayList();
    String strTicket = new String();

    // conn.setAutoCommit(false);
    gmDBManager.setPrepareString("GM_CS_SAV_LOANER_RECON", 10);

    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strTransId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strConsignStr);
    gmDBManager.setString(4, strWriteStr);
    gmDBManager.setString(5, strRetStr);
    gmDBManager.setString(6, strUsername);

    gmDBManager.execute();
    strId = GmCommonClass.parseNull(gmDBManager.getString(7));
    alReconList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
    alMissinList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(9));
    strTicket = GmCommonClass.parseNull(gmDBManager.getString(10));

    if (!strInputDoStr.trim().equals("")) {
      gmLoanerReconBean.reconcileLoanerTrans(gmDBManager, strTransId, strInputDoStr, strUsername);
    }
    gmDBManager.commit();

    /*
     * if(!strTicket.equals("")) { gmJIRAHelperBean.updateReconDetailsInMT(strTicket, alReconList,
     * alMissinList,"");
     * 
     * if(alMissinList.size() == 0) { gmJIRAHelperBean.closeIssue(strTicket, "10056"); } }
     */

    return strId;
  } // End of saveLoanerReconTrans

  public RowSetDynaClass fetchLoanerExtensionSummary(String strConsignId) throws AppError {
    RowSetDynaClass rdReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_op_fch_le_summary", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);

    gmDBManager.execute();
    rdReturn = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdReturn;
  }

  public String fetchConsignId(String strLoanerTransId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strConsignId = "";
    gmDBManager.setFunctionString("gm_pkg_op_loaner.get_consignid", 1);

    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strLoanerTransId);
    gmDBManager.execute();
    strConsignId = gmDBManager.getString(1);
    log.debug("strShipAdd to display is   " + strConsignId);
    gmDBManager.close();
    return strConsignId;
  }

  /**
   * saveLESummary - This Method is used to Saving the loaner Extension info
   * 
   * @param HashMap hmParams
   * @return String
   * @exception AppError
   */
  public HashMap saveLESummary(HashMap hmParams) throws AppError {
    // need to call saveLoanerReturnTrans before gm_op_sav_loaner_extension,
    // as wrong LN id getting saved when missing part record entered into
    // t412 table

    HashMap hmReturn = new HashMap();
    ArrayList alReqDetail = new ArrayList();
    ArrayList alReqExtDetail = new ArrayList();
    HashMap hmAddParam = new HashMap();
    // Adding the new expected return date into ArrayList.
    ArrayList alList = new ArrayList();
    HashMap hmTempParam = new HashMap();
    
    String strReplenishId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());

    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
    String strLoanerReplTxnID = "";
    String strSuccessMessage = "";
    String strConsignId = "";
    String strHisComments = "";
    strConsignId = GmCommonClass.parseNull((String) hmParams.get("CONSINID"));
    Date dtExtDate = (Date) hmParams.get("EXTDATE");
    String strExtReason = GmCommonClass.parseNull((String) hmParams.get("RTYPE"));
    String strComments = GmCommonClass.parseNull((String) hmParams.get("COMMEN"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strReqExtFl = GmCommonClass.parseNull((String) hmParams.get("REQEXT"));
    String strHistoryFl = GmCommonClass.parseNull((String) hmParams.get("HISTORYFL"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParams.get("DATEFMT"));
    String	strConsignmentType = GmCommonClass.parseNull((String) hmParams.get("CONSIGNMENTTYPE"));
    Date dtSurgDate = (java.util.Date) hmParams.get("SURGDATE");
    String strCompanyInfo= GmCommonClass.parseNull((String) hmParams.get("COMPANYINFO"));
    log.debug("strReqExtFl:"+strReqExtFl);
    alReqDetail =
        GmCommonClass.parseNullArrayList(fetchLoanerRequestDetail(strConsignId, strReqExtFl));
    log.debug("alReqDetail:" + alReqDetail);


    alReqExtDetail = gmInHouseRptBean.getConsignDetails(alReqDetail);
    log.debug("alReqExtDetail:" + alReqExtDetail);
    int alSize = alReqExtDetail.size();
    // 50322 Extension w/ replenish  //26240637 -->Replenishment
    if (strExtReason.equalsIgnoreCase("50322") || strExtReason.equalsIgnoreCase("26240637")) {
      log.debug("strExtReason is 50322");
      log.debug("strConsignId " + strConsignId + "strUserId " + " hmParams " + hmParams);
      hmReturn = saveLoanerReturnTrans(gmDBManager, strConsignId, hmParams, strUserId);
      strLoanerReplTxnID = GmCommonClass.parseNull((String) hmReturn.get("ID"));
      strReplenishId = GmCommonClass.parseNull((String) hmReturn.get("REPLENISHID"));

      log.debug("strLoanerReplTxnID " + strLoanerReplTxnID);
      log.debug("REPLENISHID " + strReplenishId);

      // for loaners xtn alone call shipping details from here n not from
      // pkg
      if (!strReplenishId.equals("")) {
        hmParams.put("REFID", strReplenishId);
        gmShippingTransBean.saveShipDetails(gmDBManager, hmParams);
      }
    }
    gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_op_sav_loaner_extension", 7);
    //following code added to update all consignments id when click extend whole request check box 
    if(strReqExtFl.equals("Y")){
    	for (int i = 0; i < alSize; i++) {
    		HashMap hmTemp = new HashMap();
    		hmTemp = (HashMap) alReqExtDetail.get(i);
    		strConsignId = GmCommonClass.parseNull((String) hmTemp.get("CONSIGNID"));
    		log.debug("strConsignId "+strConsignId);
    		gmDBManager.setString(1, strConsignId);
    	 }
    }else{
    	gmDBManager.setString(1, strConsignId);
    }
    
    
    if(strExtReason.equalsIgnoreCase("26240637")){
        gmDBManager.setDate(2, null);
    }else{
        gmDBManager.setDate(2, new java.sql.Date(dtExtDate.getTime()));
    }

    gmDBManager.setInt(3, Integer.parseInt(strExtReason));
    gmDBManager.setString(4, strComments);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strReqExtFl);
    gmDBManager.setDate(7, dtSurgDate);
    gmDBManager.execute();
    String strExtDate = GmCommonClass.getStringFromDate(dtExtDate, strDateFmt);
    if(strHistoryFl.equals("History")){
    	strHisComments = strComments + "~" + strExtDate + "~"+strHistoryFl;
    }else{
    	strHisComments = strComments + "~" + strExtDate;
    }
    
    // Following code added to show the comments on manage loaner screen.
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strConsignId, strComments, strUserId, "1222");
      if (!strExtReason.equalsIgnoreCase("26240637")){ // Replenishment
      gmCommonBean.saveLog(gmDBManager, strConsignId, strHisComments, strUserId, "26240803");  //26240803 -- GPLOG
      }
    }
    gmDBManager.commit();

   
    hmAddParam.put("EXTDATE", strExtDate);
    hmAddParam.put("TODAYSDATE", GmCommonClass.parseNull((String) hmParams.get("TODAYSDATE")));

    for (int i = 0; i < alSize; i++) {
      hmTempParam = GmCommonClass.parseNullHashMap((HashMap) alReqExtDetail.get(i));
      hmTempParam.put("NEWEXTDATE", strExtDate);
      alList.add(hmTempParam);
    }
    String strPendingApprlFl = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strExtReason,"LEEXTAPPR",getGmDataStoreVO().getCmpid()));
    if((!strPendingApprlFl.equals("Y") && !strExtReason.equalsIgnoreCase("26240637"))){
    sendLoanerExtensionEmail(alList, hmAddParam);
    }
    
    // not loaner extension with replenish
    if (!(strExtReason.equalsIgnoreCase("50322") || strExtReason.equalsIgnoreCase("26240637"))) {
      if (!strReqExtFl.equals("Y")) {
    	  if(strPendingApprlFl.equals("Y") && strConsignmentType.equals("4127"))
    		  strSuccessMessage = "Loaner " + strConsignId + " submitted for approval.";
    	  else
    		  strSuccessMessage = "Loaner " + strConsignId + " Extended Successfully."; 

        throw new AppError(strSuccessMessage, "", 'S');
      } else {
    	  if(strPendingApprlFl.equals("Y") && strConsignmentType.equals("4127"))
    	      strSuccessMessage = "Loaner Request submitted for approval.";
    	  else
    	      strSuccessMessage = "Loaner Request Extended Successfully."; 

        throw new AppError(strSuccessMessage, "", 'S');
      }

    }
    
	// To Save the Insert part number details FOR FGLE Transaction in Loaner Extension screen Using JMS call
    // 50322 Extension w/ replenish
    if (strExtReason.equalsIgnoreCase("50322") || strExtReason.equalsIgnoreCase("26240637")) {
	    HashMap hmJMSParam = new HashMap();
	    hmJMSParam.put("TXNID", strReplenishId);
	    hmJMSParam.put("TXNTYPE", strExtReason);
	    hmJMSParam.put("STATUS", "93342");
	    hmJMSParam.put("USERID", strUserId);
	    hmJMSParam.put("COMPANYINFO", strCompanyInfo);
	    gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);
    }
    return hmReturn;
  }

  public void sendLoanerExtensionEmail(ArrayList alReqExtDetail, HashMap hmLEParams)
      throws AppError {

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    // *************** send email when Loaner Extension
    HashMap hmReturn = new HashMap();

    String strRepEmail = "";
    String strRepName = "";
    HashMap hmReqParams = null;

    try {

      if (alReqExtDetail.size() > 0) {
        hmReqParams = (HashMap) alReqExtDetail.get(0);
      }

      strRepEmail = GmCommonClass.parseNull((String) hmReqParams.get("EMAILADD"));
      String strBillName = GmCommonClass.parseNull((String) hmReqParams.get("BILLNM"));
      String strTodaysDate = GmCommonClass.parseNull((String) hmLEParams.get("TODAYSDATE"));
      String[] arrName = strBillName.split("R:");
      strRepName = arrName[1];
      String strExtensionDate = GmCommonClass.parseNull((String) hmLEParams.get("EXTDATE"));

      log.debug("strRepEmail:" + strRepEmail + "--strRepName: " + strRepName + "--strExtensionDate"
          + strExtensionDate);

      if (!strRepEmail.equals("")) {
        GmJasperMail jasperMail = new GmJasperMail();
        GmEmailProperties emailProps = new GmEmailProperties();

        emailProps.setRecipients(strRepEmail);

        emailProps.setMimeType(gmResourceBundleBean.getProperty("GmLoanerExtensionEmail."
            + GmEmailProperties.MIME_TYPE));
        emailProps.setSender(gmResourceBundleBean.getProperty("GmLoanerExtensionEmail."
            + GmEmailProperties.FROM));

        String strSubject =
            gmResourceBundleBean.getProperty("GmLoanerExtensionEmail." + GmEmailProperties.SUBJECT);
        if (strRepName.indexOf("<") != -1) {
          strRepName = strRepName.substring(0, strRepName.indexOf("<"));
        }
        strSubject = GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", strRepName);
        strSubject = GmCommonClass.replaceAll(strSubject, "#<EXTDATE>", strTodaysDate);
        emailProps.setSubject(strSubject);

        hmLEParams
            .put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
        hmLEParams
            .put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSEmail"));

        jasperMail.setJasperReportName("/GmEmailLoanerExtension.jasper");
        jasperMail.setAdditionalParams(hmLEParams);
        jasperMail.setReportData(alReqExtDetail);
        jasperMail.setEmailProperties(emailProps);
        hmReturn = jasperMail.sendMail();
      }

    } catch (Exception e) {
      log.error("Exception in LoanerExtensionEmail : " + e.getMessage());
      hmReturn.put(GmJob.EXCEPTION, e);
    }
    Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
    if (ex != null) {
      try {
        GmEmailProperties emailProps = new GmEmailProperties();
        emailProps.setRecipients(gmResourceBundleBean
            .getProperty("GmLoanerExtensionEmail.ExceptionMailTo"));
        emailProps.setSender(gmResourceBundleBean.getProperty("GmCommon.ExceptionMailFrom"));
        emailProps.setSubject(gmResourceBundleBean
            .getProperty("GmLoanerExtensionEmail.ExceptionMailSubject") + " " + strRepName);
        emailProps.setMessage("Exception : " + GmCommonClass.getExceptionStackTrace(ex, "\n")
            + "\nOriginal Mail : " + hmReturn.get("ORIGINALMAIL"));
        emailProps.setMimeType("text/html");

        GmCommonClass.sendMail(emailProps);
      } catch (Exception e) {
        log.error("Exception in sending Loaner Extension mail to Rep " + strRepName
            + " and also notification to IT" + e.getMessage());
      }
    }
  }


  /**
   * fetchLoanerRequestDetail - This method is used to fetch loaner request details
   * 
   * @param hmValues
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchLoanerRequestDetail(String strConsignId, String strExtFl) throws AppError {

    ArrayList alDetails = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner_extension.gm_op_fch_req_det", 3);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strExtFl);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    log.debug("alDetails........" + alDetails);

    gmDBManager.close();
    return alDetails;
  }

  public void saveLoanerProcess(HashMap hmParams) throws AppError {
    // CONSIGNID,USERID
    log.debug("Inside hmParam : " + hmParams);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_loaner_process", 3);
    gmDBManager.setString(1, (String) hmParams.get("CONSIGNID"));
    gmDBManager.setString(2, (String) hmParams.get("USERID"));
    gmDBManager.setString(3, (String) hmParams.get("DEPLOYFL"));
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public String saveLoanerProcessPicture(HashMap hmParams) throws AppError {
    // CONSIGNID,USERID
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strLogReason = (String) hmParams.get("TXT_LOGREASON");
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_process_picture", 3);
    gmDBManager.setString(1, (String) hmParams.get("CONSIGNID"));
    gmDBManager.setString(2, (String) hmParams.get("USERID"));
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.execute();
    String strMsg = GmCommonClass.parseNull(gmDBManager.getString(3));
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, (String) hmParams.get("CONSIGNID"), strLogReason,
          (String) hmParams.get("USERID"), "1222");
    }
    gmDBManager.commit();
    return strMsg;
  }

  public String saveInHouseReconTrans(String strTransId, HashMap hmParam, String strUsername)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmJIRAHelperBean gmJIRAHelperBean = new GmJIRAHelperBean();
    GmLoanerReconBean gmLoanerReconBean = new GmLoanerReconBean(getGmDataStoreVO());

    String strId = "";

    String strRetStr = GmCommonClass.parseNull((String) hmParam.get("RETSTR"));
    String strIhStr = GmCommonClass.parseNull((String) hmParam.get("IHSTR"));
    String strWriteStr = GmCommonClass.parseNull((String) hmParam.get("WRITESTR"));
    String strConsignStr = GmCommonClass.parseNull((String) hmParam.get("CONSTR"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPEID"));
    ArrayList alReconList = new ArrayList();
    ArrayList alMissinList = new ArrayList();
    String strTicket = new String();

    // conn.setAutoCommit(false);
    gmDBManager.setPrepareString("gm_pkg_op_process_recon.gm_sav_inhouse_recon", 8);

    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strTransId);
    gmDBManager.setString(2, strType);
    gmDBManager.setString(3, strRetStr);
    gmDBManager.setString(4, strConsignStr);
    gmDBManager.setString(5, strWriteStr);
    gmDBManager.setString(6, strIhStr);
    gmDBManager.setString(7, strUsername);

    gmDBManager.execute();
    strId = GmCommonClass.parseNull(gmDBManager.getString(8));
    /*
     * alReconList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8)); alMissinList
     * = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(9)); strTicket =
     * GmCommonClass.parseNull((String) gmDBManager.getString(10));
     * 
     * if(!strInputDoStr.trim().equals("")){ gmLoanerReconBean.reconcileLoanerTrans(gmDBManager,
     * strTransId, strInputDoStr, strUsername); } gmDBManager.commit();
     * 
     * if(!strTicket.equals("")) { gmJIRAHelperBean.updateReconDetailsInMT(strTicket, alReconList,
     * alMissinList,"");
     * 
     * if(alMissinList.size() == 0) { gmJIRAHelperBean.closeIssue(strTicket, "10056"); } }
     */
    gmDBManager.commit();
    return strId;
  } // End of saveLoanerReconTrans

  /*
   * saveItemLotNum - Method to call the procedure to validate the lot numbers save the lot numbers
   * 
   * @param HashMap
   * 
   * @return String
   * 
   * @exception AppError
   */
  public String saveItemLotNum(String strConsignId, HashMap hmParams, String strInputString)
      throws AppError {

    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TYPE"));
    String strRelVerFl = GmCommonClass.parseNull((String) hmParams.get("SHIPFL"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strComments = GmCommonClass.parseNull((String) hmParams.get("CRCOMMENTS"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strErrFl = "";

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    hmParams.put("CONNECTION", gmDBManager);// Getting the connection to a hashmap
    gmDBManager.setPrepareString("gm_pkg_op_inhouse_master.gm_sav_inhouse_lot_num", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, strUserID);
    gmDBManager.execute();
    strErrFl = GmCommonClass.parseNull(gmDBManager.getString(5));

    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strTxnId, strComments, strUserID, "1268");
    }

    gmDBManager.commit();
    return strErrFl;
  }

  /*
   * updateLoanerLot - Reconfiguring the Loaner set Lot numbers for Japan
   * 
   * @param HashMap
   * 
   * 
   * @exception AppError
   */

  public void updateLoanerLot(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USER_ID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("RECONLOTSTR"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));

    gmDBManager.setPrepareString("gm_pkg_set_lot_track.GM_PKG_UPDATE_LOANER_LOT", 3);

    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strInputStr);
    gmDBManager.execute();
    // 26240289 - log for Reconfigure by Lot
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strConsignId, strLog, strUserId, "26240289");
    }

    gmDBManager.commit();
  }

}// End of GmInHouseSetsTransBean
