package com.globus.operations.logistics.beans;

import java.util.HashMap;

import org.displaytag.decorator.TableDecorator;

public class DtOpenBackOrder extends TableDecorator {
	private HashMap hmRows=new HashMap();
	String strReturn="";
	public String getID(){
		hmRows=(HashMap) this.getCurrentRowObject();
		String strLN=(String)hmRows.get("ID");
		String strLnType =(String)hmRows.get("CTYPE");
		String strType="50156";
		String strRefId=(String)hmRows.get("MASTER_ID");
		strReturn="<a href=\"#\" onclick=\"fnPicSlip('" + strLN + "','" + strType + "','" + strRefId + "');\"><img src=\"/images/packslip.gif\" border=0 ></img></a>&nbsp;" + strLN;

		return strReturn;
	}
	public String getPNUM(){
		hmRows=(HashMap) this.getCurrentRowObject();
		String strType =(String)hmRows.get("CTYPE");
		strReturn="<font color=\"black\">" + (String)hmRows.get("PNUM") + "</font>";
		
		return strReturn;
	}
	public String getPDESC(){
		hmRows=(HashMap) this.getCurrentRowObject();
		String strType =(String)hmRows.get("CTYPE");
		strReturn="<font color=\"black\">" + (String)hmRows.get("PDESC") + "</font>";
			
		return strReturn;
	}
	
	public String getQTY(){
		hmRows=(HashMap) this.getCurrentRowObject();
		String strType =(String)hmRows.get("CTYPE");
		strReturn="<font color=\"black\">" + (String)hmRows.get("QTY") + "</font>";
			
		return strReturn;
	}
	public String getCRDATE(){
		hmRows=(HashMap) this.getCurrentRowObject();
		String strType =(String)hmRows.get("CTYPE");
		strReturn="<font color=\"black\">" + (String)hmRows.get("CRDATE") + "</font>";
			
		return strReturn;
	}
	public String getSTATUS(){
		hmRows=(HashMap) this.getCurrentRowObject();
		String strType =(String)hmRows.get("CTYPE");
		strReturn="<font color=\"black\">" + (String)hmRows.get("STATUS") + "</font>";
			
		return strReturn;
	}
	public String getTYPE(){
		hmRows=(HashMap) this.getCurrentRowObject();
		String strType =(String)hmRows.get("CTYPE");
		strReturn="<font color=\"black\">" + (String)hmRows.get("TYPE") + "</font>";
			
		return strReturn;
	}
	
	public String getBORPTFL(){
      hmRows=(HashMap) this.getCurrentRowObject();
      String strBORptFl=(String)hmRows.get("BORPTFL");
      String strRefId = (String)hmRows.get("MASTER_ID");
      strReturn="<a href=\"#\" onclick=\"fnOpenLoanerItemsBORpt('"+strRefId+"');\"><font color=\"red\">" +strBORptFl+"</font></a>&nbsp;" ;

      return strReturn;
  }
}