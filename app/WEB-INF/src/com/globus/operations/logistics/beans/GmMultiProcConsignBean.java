package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
/**
* GmMultiProcConsignBean : Contains the methods used for the Multi selection sets screen
* author : Raja
*/
public class GmMultiProcConsignBean extends GmBean {

	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	public GmMultiProcConsignBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		
	}
	
	/**
	 * loadMultiSetDetails - load the request details 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String loadMultiSetDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		String strRequestStatus = GmCommonClass.parseNull((String) hmParam.get("STRSTATUS")); 
		String strSetID = GmCommonClass.parseNull((String) hmParam.get("STRSETID"));
		String strConsignID = GmCommonClass.parseNull((String) hmParam.get("STRCONSIGNMENTID"));  
		String strSetNm = GmCommonClass.parseNull((String) hmParam.get("STRSETNAME"));
		String strRequestId = GmCommonClass.parseNull((String) hmParam.get("STRREQID"));
		String strCompId =  GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
		String strPlantID = GmCommonClass.parseNull((String)getGmDataStoreVO().getPlantid());
		String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
		StringBuffer strQuery = new StringBuffer();
		strQuery.append(" SELECT JSON_ARRAYAGG ( JSON_OBJECT ( 'REQID' VALUE RQID, 'REQDT' VALUE RQSTDT, 'SETID' VALUE setid, ");
		strQuery.append(" 'NAME' VALUE setnm, 'CONID' VALUE cnid, 'REQUDT' VALUE RQUDDT, ");
		strQuery.append(" 'REQSRC' VALUE REQSRC, 'REQSRCCID' VALUE srcid, 'REQFOR' VALUE RQFORNM, 'REQUESTFORID' VALUE RQFORID, ");
		strQuery.append(" 'RQSTATUS' VALUE RQSTATUS, 'CNSTFL' VALUE statusfl, 'CNSTATUS' VALUE CNSTATUS, ");
		strQuery.append(" 'REQTO' VALUE REQTO, 'STFLG' VALUE STFLG ) RETURNING CLOB ) FROM ( ");
		strQuery.append("  SELECT t520.c520_request_id rqid, TO_CHAR(t520.c520_request_date,'"+strCompDateFmt+"') rqstdt, t520.c207_set_id setid, "); 
		strQuery.append("  get_set_name(t520.c207_set_id) setnm, nvl(t504.c504_consignment_id, '-') cnid, ");
		strQuery.append("  TO_CHAR(t520.c520_required_date,'"+strCompDateFmt+"') rquddt, get_code_name(t520.c901_request_source) REQSRC, ");
		strQuery.append("  t520.c901_request_source srcid, get_code_name(t520.c520_request_for) rqfornm,  t520.c520_request_for rqforid, ");
		strQuery.append("  gm_pkg_op_request_summary.get_request_status(t520.c520_status_fl) rqstatus, t504.c504_status_fl statusfl, ");
		strQuery.append("  DECODE (t504.c504_status_fl, '0', 'Initiated', '1', 'WIP', '1.10', 'Completed Set', '1.20', 'Pending Putaway', '2', 'Built Set', ");
		strQuery.append(" '2.20', 'Pending Pick', '3', 'Pending Shipping', '3.30', 'Packing In Progress', '3.60', 'Ready For Pickup', '4', DECODE (c504_type, '26240144', 'Returned Stock', 'Closed'), NULL) CNSTATUS, ");
		strQuery.append(" DECODE(t520.c520_request_for, 40021, get_distributor_name(t520.c520_request_to), 40022, get_user_name (t520.c520_request_to), 102930, get_distributor_name(t520.c520_request_to), "); 
        strQuery.append(" 40025, get_account_name(t520.c520_request_to)) REQTO, ");
        strQuery.append(" t520.c520_status_fl STFLG ");
        strQuery.append(" FROM ");
        strQuery.append(" t520_request t520, ");
        strQuery.append(" t504_consignment  t504, ");
        strQuery.append(" t207_set_master t207 ");
        strQuery.append(" WHERE ");
        strQuery.append(" t520.c520_void_fl IS NULL ");
        strQuery.append(" AND t504.c504_void_fl IS NULL ");
        strQuery.append(" AND t520.c520_request_id = t504.c520_request_id (+) ");
        strQuery.append(" AND t520.c5040_plant_id = t504.c5040_plant_id (+) ");
        strQuery.append(" AND ( t520.c1900_company_id = "+strCompId +" OR t520.c5040_plant_id = "+strPlantID+" ) "  );
        strQuery.append(" AND t520.C520_REQUEST_FOR = '40021' ");     // 40021- Consignment (Request type)
        strQuery.append(" AND t520.C207_SET_ID = t207.c207_set_id ");
     // The Status dropdown is chosen by default (Ready To Consign), 
        strQuery.append(" AND t520.c520_status_fl = get_code_name_alt(" +strRequestStatus +") ");
        strQuery.append(" AND ((NVL(t504.c504_status_fl,0) = 2 AND t520.c520_status_fl IN (20)) OR t520.c520_status_fl IN (15,10)) ");  //  (Consignment status - Built Set - 20, Back Log - 15, Back Order - 10 )  
        strQuery.append(" AND t520.c520_master_request_id IS NULL "); //PC-5522 -Multi Set consignment should not include child request
        
        if (!strSetID.equals("")) { 
 		   strQuery.append(" AND t520.C207_SET_ID = '");
 		   strQuery.append(strSetID);
 		   strQuery.append("'");
 	      }
 	   if (!strConsignID.equals("")) {  
 		   strQuery.append(" AND t504.C504_CONSIGNMENT_ID = '");
 		   strQuery.append(strConsignID);
 		   strQuery.append("'");
 	      }
 	   if (!strRequestId.equals("")) {  
 		   strQuery.append(" AND t520.c520_request_id = '");
 		   strQuery.append(strRequestId);
 		   strQuery.append("'");
 	      }
 	   if (!strSetNm.equals("")) { 
 		   strQuery.append(" AND UPPER(C207_SET_NM) LIKE ('%" );
 		   strQuery.append(strSetNm.toUpperCase());
 		   strQuery.append("%') ");
 	      }
 	      strQuery.append(" ORDER BY t520.c520_request_id )"); 
		
	   log.debug("loadMultiSetDetails==> "+strQuery.toString());
		
		String strReturn = gmDBManager.queryJSONRecord(strQuery.toString());
	return strReturn;
	}
	
	/**
	 * fetchRequestDetails  - used to fetch the request details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchRequestDetails(HashMap hmParam) throws AppError {
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
		  String strReqId = GmCommonClass.parseNull((String) hmParam.get("STRREQTRANSIDS"));
		  String strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("STRCONSIGNMENTID")); 
		  String strReturn = "";
		  gmDBManager.setPrepareString("gm_pkg_op_multi_process_consign.gm_fch_request_details", 3);
		  gmDBManager.setString(1,strReqId);
		  gmDBManager.setString(2,strConsignmentId);
		  gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		  gmDBManager.execute(); 
		  strReturn = GmCommonClass.parseNull(gmDBManager.getString(3));
		  gmDBManager.close(); 
		  return strReturn;
	}
	
	/**
	 * processMultiConsignSets - used to save the multiple sets
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String processMultiConsignSets(HashMap hmParam) throws AppError {
		   log.debug("==processMultiConsignSets==");
		   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
		   String strValidReqId = "";
		   String strInValidReqId = "";
		   String strResult = "";
		   String strInputString = GmCommonClass.parseNull((String) hmParam.get("STRINPUTSTRING"));
		   String strSource = GmCommonClass.parseNull((String) "50184");                             //50184 - Request
		   String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
		   String strShipId = GmCommonClass.parseNull((String) hmParam.get("NAMES")); 
		   String strShipCarr = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER")); 
		   String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE")); 
		   String strAddressId = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID")); 
		   String strAttn = GmCommonClass.parseNull((String) hmParam.get("OVERRIDEATTNTO")); 
		   String strShipIns = GmCommonClass.parseNull((String) hmParam.get("SHIPINSTRUCTION")); 
		   String strType = GmCommonClass.parseNull((String) "4110");                              // 4110 - Consignment
		   String strPurpose = GmCommonClass.parseNull((String) "50060");                          // 50060 - Regular
		   String strBillTo = GmCommonClass.parseNull((String) hmParam.get("STRREQTO")); 
		   String strShipFl = GmCommonClass.parseNull((String) "1");                               // ShipFl - 1 - Ready to Consign
		   String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID")); 
		   Date strPlanedShipDt = (Date) hmParam.get("STRPLANEDSHIPDT"); 
		   Date strRequireDt =(Date) hmParam.get("STRREQUIRDATE"); 
		   
		    gmDBManager.setPrepareString("gm_pkg_op_multi_process_consign.gm_process_multi_consign_sets", 18);
		    gmDBManager.setString(1, strInputString);
		    gmDBManager.setInt(2, Integer.parseInt(strSource));
		    gmDBManager.setInt(3, Integer.parseInt(GmCommonClass.parseZero(strShipTo)));
		    gmDBManager.setString(4, strShipId);
		    gmDBManager.setInt(5, Integer.parseInt(GmCommonClass.parseZero(strShipCarr)));
		    gmDBManager.setInt(6, Integer.parseInt(GmCommonClass.parseZero(strShipMode)));
		    gmDBManager.setInt(7, Integer.parseInt(GmCommonClass.parseZero(strAddressId)));
		    gmDBManager.setString(8, strAttn);
		    gmDBManager.setString(9, strShipIns);
		    gmDBManager.setInt(10, Integer.parseInt(strType));
		    gmDBManager.setInt(11, Integer.parseInt(strPurpose));
		    gmDBManager.setString(12, strBillTo);
		    gmDBManager.setString(13, strShipFl);
		    gmDBManager.setString(14, strUserId);
		    gmDBManager.setDate(15, strPlanedShipDt);
		    gmDBManager.setDate(16, strRequireDt);
		    gmDBManager.registerOutParameter(17, OracleTypes.VARCHAR);
		    gmDBManager.registerOutParameter(18, OracleTypes.VARCHAR);
		    gmDBManager.execute();
		    strValidReqId = GmCommonClass.parseNull(gmDBManager.getString(17));
		    strInValidReqId = GmCommonClass.parseNull(gmDBManager.getString(18));
		    gmDBManager.commit();
		   
		    if((!strValidReqId.equals(""))||(!strInValidReqId.equals(""))) {
		    	strResult = strValidReqId+"#"+strInValidReqId;
		    }
		    
		  return  strResult;
	}
}
