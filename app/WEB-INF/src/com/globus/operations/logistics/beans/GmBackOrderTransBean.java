package com.globus.operations.logistics.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBackOrderTransBean extends GmSalesFilterConditionBean{

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
	
	  public GmBackOrderTransBean() {
		  super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmBackOrderTransBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	/**
	 * saveSalesBOEmail - This method is used to save the time
	 * @param hmParam
	 * @return HashMap
	 * @throws AppError
	 * @author arajan
	 */
  	public HashMap saveSalesBOEmail(HashMap hmParam) throws AppError{
  		
  		GmDBManager gmDBManager = new GmDBManager();
  		String strRepId = GmCommonClass.parseNull((String)hmParam.get("REPID"));
  		String strBOrderString = GmCommonClass.parseNull((String)hmParam.get("BOSTRING"));
  		String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
  		HashMap hmReturn = new HashMap();
  		
  		gmDBManager.setPrepareString("gm_pkg_op_backorder_txn.gm_save_backorder_email", 3);
  		gmDBManager.setString(1, strRepId);
  		gmDBManager.setString(2, strBOrderString);
  		gmDBManager.setString(3, strUserId);
  		gmDBManager.execute();
  		gmDBManager.commit();
  		
  		return hmReturn;
  	}
  	
  	/**
     * updateBackOrderToBillOnly This method is to convert the Back Order to Bill Only Sales Consignment
     * @param HashMap
     * @return void
     * @exception AppError
     * @author arajan
     */
    public void updateBackOrderToBillOnly(HashMap hmParam) throws AppError{
  	  String strOrderId    = GmCommonClass.parseNull((String)hmParam.get("ORDERID")) ;
  	  String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID")) ;
  	  GmDBManager gmDBManager = new GmDBManager();       
  	  gmDBManager.setPrepareString("gm_pkg_op_backorder_txn.gm_upd_bo_to_bill_only", 2);
  	  gmDBManager.setString(1, strOrderId);
  	  gmDBManager.setString(2, strUserId);
  	  gmDBManager.execute();
  	  gmDBManager.commit();

    }
}