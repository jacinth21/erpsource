package com.globus.operations.logistics.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
/**
* GmLotTrackTxnLogRptBean : Contains the methods used for the Lot Track Transaction Log Report screen
* author : Agilan Singaravel
*/
public class GmLotTrackTxnLogRptBean extends GmBean {

	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /**
	  *  Default Constructor
	  */
	public GmLotTrackTxnLogRptBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	 /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	public GmLotTrackTxnLogRptBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		
	}
	/**
	 * fetchLotTrackTxnLogDetails - used to fetch Lot Track Transaction log data
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotTrackTxnLogDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
	    String strWareHouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSETYPE"));
	    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATEFROM"));
	    String strToDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATETO"));
	    String strLocationId = GmCommonClass.parseNull((String) hmParam.get("STRLOCATIONID"));
	    String strLotTrackTxnRptJSONString = "";
	    StringBuffer strQuery = new StringBuffer();
	
	    strQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT('PARTNO' VALUE PARTNO, 'CNTROLNO' VALUE CNTROLNO, ");
	    strQuery.append(" 'TRANSID' VALUE TRANSID, 'TRANSTYPE' VALUE TRANSTYPE, ");
	    strQuery.append(" 'CREATEDATE' VALUE CREATEDATE, 'ORIGQTY' VALUE ORIGQTY, ");
	    strQuery.append(" 'TXNQTY' VALUE TXNQTY, 'NEWQTY' VALUE NEWQTY, 'EXPIRYDATE' VALUE EXPIRYDATE, ");
	    strQuery.append(" 'LOCATIONID' VALUE LOCATIONID, 'WAREHOUSETYPE' VALUE WAREHOUSETYPE, ");
	    strQuery.append(" 'COMPANYID' VALUE COMPANYID, 'PLANTID' VALUE PLANTID, ");
		strQuery.append(" 'CMPID' VALUE CMPID,'PLTID' value PLTID,'WARETYPE' value WARETYPE) ");
	    strQuery.append("  ORDER BY CMPID,PLTID,WARETYPE,CNTROLNO RETURNING CLOB) ");
	    strQuery.append("  FROM ");
	    	    
	    if(strWareHouseType.equals("106221")){ //Loaner Inventory
	    	getLoanerInvQuery(strQuery,hmParam);
	    }else if((!strWareHouseType.equals("106221")) && (!strWareHouseType.equals("0"))){   //Loaner Inventory
	    	getInvQuery(strQuery,hmParam);
	    }else{	   
	    	getInvQuery(strQuery,hmParam);
	    	strQuery.append("  UNION ");	    
	    	getLoanerInvQuery(strQuery,hmParam);
	    }
	    
	    strQuery.append(" )where rownum <= 2000");
	    log.debug(" Lot Track Transaction Log Report screen - fetchLotTrackTxnLogDetails =>"+ strQuery.toString());
	    String strReturn = gmDBManager.queryJSONRecord(strQuery.toString());
	   return strReturn;
	}
	/**
	 * getLoanerInvQuery - used to fetch Lot Track Transaction log data based on loaner inventory warehouse type
	 * @param StringBuffer strQuery, HashMap hmParam
	 * @return strQuery
	 * @throws AppError
	 */	
	public StringBuffer getLoanerInvQuery(StringBuffer strQuery, HashMap hmParam) throws AppError{
		
		String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strWareHouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSETYPE"));
		String strFromDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATEFROM"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATETO"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strControlInvId = GmCommonClass.parseNull((String) hmParam.get("STRCONTROLINVID"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
	    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
	    String strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANTID"));
	    //strLocation,strExpired added for PC-2786  - Add Field Sales Filter in Lot Inventory Report
	    String strExpired = GmCommonClass.parseNull((String) hmParam.get("STREXPIRED"));
	    String strLocation = GmCommonClass.parseNull((String) hmParam.get("STRLOCATION"));
	    
		if(strWareHouseType.equals("106221")){  //Loaner Inventory
			strQuery.append(" ( ");
		}
		
		strQuery.append("  SELECT distinct t5072a.C205_PART_NUMBER_ID PARTNO, t5072a.C5072_CONTROL_NUMBER CNTROLNO,");
	    strQuery.append("  t5072.C5072_LAST_UPDATED_TXN_ID TRANSID, get_code_name(t5072a.C901_TXN_TYPE) TRANSTYPE,");
	    strQuery.append("  to_char(t5072a.C5072A_CREATED_DATE,'"+strDateFmt+"') CREATEDATE, t5072a.C5072A_ORIG_QTY ORIGQTY,");
	    strQuery.append("  t5072a.C5072A_TXN_QTY TXNQTY, t5072a.C5072A_NEW_QTY NEWQTY,to_char(t2550.C2550_EXPIRY_DATE,'"+strDateFmt+"') EXPIRYDATE, ");
		strQuery.append("  nvl(t701.c701_distributor_name,GET_DIST_REP_NAME(c504_ship_to_id))  LOCATIONID, get_code_name('106221') WAREHOUSETYPE, ");
		strQuery.append("  get_company_name(t504.C1900_COMPANY_ID) COMPANYID, get_plant_name(t504.C5040_PLANT_ID) PLANTID, ");
		strQuery.append("  t504.c1900_company_id CMPID,t504.c5040_plant_id PLTID,106221 WARETYPE ");
	    strQuery.append("  FROM T5072_SET_PART_LOT_QTY t5072, T5072A_SET_PART_LOT_QTY_LOG t5072a, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550 ");
		strQuery.append("  ,T504_CONSIGNMENT t504, T5070_SET_LOT_MASTER t5070,t701_distributor t701, t703_sales_rep t703, V205_STERILE_PARTS v205 ");
	    strQuery.append("  WHERE t5072a.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID  ");
		strQuery.append("  AND t5072a.C205_PART_NUMBER_ID = v205.C205_PART_NUMBER_ID  ");
	    strQuery.append("  AND t5072a.C5072_CONTROL_NUMBER = t2550.C2550_CONTROL_NUMBER ");
	    strQuery.append("  AND t5072a.C5071_SET_PART_QTY_ID  =  t5072.C5071_SET_PART_QTY_ID ");
	    strQuery.append("  AND t5072a.C205_PART_NUMBER_ID = t5072.C205_PART_NUMBER_ID ");
	    strQuery.append("  AND t2550.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID  ");
	    strQuery.append("  AND t5072.C5072_LAST_UPDATED_TXN_ID = t504.c504_consignment_id  ");
	    strQuery.append("  AND t5070.C901_TXN_ID = t504.c504_consignment_id  ");
		strQuery.append("  AND t5072.C5070_SET_LOT_MASTER_ID = t5070.C5070_SET_LOT_MASTER_ID ");
		strQuery.append("  AND t504.c701_distributor_id  = t701.c701_distributor_id (+) ");
		strQuery.append("  AND t504.c504_ship_to_id  = t703.c703_sales_rep_id (+) ");
	    if(!strLotNumber.equals("")){
		    strQuery.append(" AND UPPER(t5072a.C5072_CONTROL_NUMBER)  = " );
		    strQuery.append(" UPPER('" );
		    strQuery.append(strLotNumber);
		    strQuery.append("')");
	    }
	    if (strSearch.equals("0") && (!strPartNum.equals(""))) {  	
	    	strQuery.append(" AND t5072a.c205_part_number_id like upper('%"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIT") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5072a.c205_part_number_id like upper('"+strPartNum+"')  ");
	    }else if(strSearch.equals("LIKEPRE") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5072a.c205_part_number_id like upper('"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIKESUF") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5072a.c205_part_number_id like upper('%"+strPartNum+"')  ");
	    }
	    if(!strFromDate.equals("") && !strToDate.equals("")){   //If Expiry Date (From & To) is selected in the filter
	        strQuery.append(" AND t2550.C2550_EXPIRY_DATE BETWEEN TO_DATE('" + strFromDate +"','"+strDateFmt+ "') AND TO_DATE('" + strToDate +"','"+strDateFmt+"')");
	    }
	    if(!strControlInvId.equals("")){
	    	strQuery.append(" AND t5072a.C5072_SET_PART_LOT_QTY_ID = '"+strControlInvId+"' ");
	    }
	    //strLocation,strExpired added for PC-2786  - Add Field Sales Filter in Lot Inventory Report
	    if(!strLocation.equals("")) {
	    	strLocation = strLocation.replaceAll("&", "' || chr(38) || '");
	    	strQuery.append("AND upper(nvl(t701.c701_distributor_name, get_dist_rep_name(c504_ship_to_id))) LIKE upper('%"+strLocation+"%') ");
	    }
	    if(strExpired.equals("Y")) {                                        
	    	 strQuery.append(" AND t2550.c2550_expiry_date < CURRENT_DATE ");
	    }
	    if(!strCompanyId.equals("") && !strCompanyId.equals("0")){
		   	strQuery.append(" AND t504.c1900_company_id = '"+strCompanyId+"' ");
		}
		if(!strPlantId.equals("") && !strPlantId.equals("0")){
		   	strQuery.append(" AND t504.c5040_plant_id = '"+strPlantId+"' ");
		}
	    
		strQuery.append(" AND t5072.C5072_VOID_FL IS NULL AND C504_VOID_FL IS NULL ");
		strQuery.append(" AND C5070_VOID_FL IS NULL AND C701_VOID_FL IS NULL ");

	    return strQuery;
	}
	/**
	 * getInvQuery - used to fetch Lot Track Transaction log data based on inventory warehouse type not equal to loaner and also choose one
	 * @param StringBuffer strQuery, HashMap hmParam
	 * @return strQuery
	 * @throws AppError
	 */	
	public StringBuffer getInvQuery(StringBuffer strQuery, HashMap hmParam) throws AppError{
		
		String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strWareHouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSETYPE"));
		String strFromDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATEFROM"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATETO"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		String strControlInvId = GmCommonClass.parseNull((String) hmParam.get("STRCONTROLINVID"));
	    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
	    String strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANTID"));	
	    //strLocation, strExpired for PC-2786 - Add Field Sales Filter in Lot Inventory Report
	    String strLocation = GmCommonClass.parseNull((String) hmParam.get("STRLOCATION"));
	    String strExpired = GmCommonClass.parseNull((String) hmParam.get("STREXPIRED"));
		
		strQuery.append("  (SELECT distinct t5081.C205_PART_NUMBER_ID PARTNO, t5081.C2550_CONTROL_NUMBER CNTROLNO,");
		strQuery.append("  t5081.C5081_LAST_UPDATE_TRANS_ID TRANSID, get_code_name(t5081.C901_LAST_TRANSACTION_TYPE) TRANSTYPE,");
		strQuery.append("  to_char(t5081.C5081_CREATED_DATE,'"+strDateFmt+"') CREATEDATE, t5081.C5081_ORIG_QTY ORIGQTY,");
		strQuery.append("  t5081.C5081_TXN_QTY TXNQTY, t5081.C5081_NEW_QTY NEWQTY,to_char(t2550.C2550_EXPIRY_DATE,'"+strDateFmt+"') EXPIRYDATE, ");
		strQuery.append("  t5081.C5080_LOCATION_NAME LOCATIONID, get_code_name(t5081.C901_WAREHOUSE_TYPE) WAREHOUSETYPE, ");
		strQuery.append("  get_company_name(t5081.C1900_COMPANY_ID) COMPANYID, get_plant_name(t5081.C5040_PLANT_ID) PLANTID, ");
		strQuery.append("  t5081.c1900_company_id CMPID,t5081.c5040_plant_id PLTID,t5081.c901_warehouse_type WARETYPE ");
		strQuery.append("  FROM T5080_LOT_TRACK_INV t5080,T5081_LOT_TRACK_INV_LOG t5081, T205_PART_NUMBER t205,  ");
		strQuery.append("  T2550_PART_CONTROL_NUMBER t2550, V205_STERILE_PARTS v205 ");
		strQuery.append("  WHERE nvl(t5081.C5080_LOCATION_ID,-9999) = nvl(t5080.C5080_LOCATION_ID,-9999) ");
		strQuery.append("  AND t5081.C5080_CONTROL_NUMBER_INV_ID = t5080.C5080_CONTROL_NUMBER_INV_ID ");
		strQuery.append("  AND t5081.C205_PART_NUMBER_ID = t205.C205_PART_NUMBER_ID ");
		strQuery.append("  AND t205.C205_PART_NUMBER_ID = t2550.C205_PART_NUMBER_ID(+) ");		
		strQuery.append("  AND t5081.C2550_CONTROL_NUMBER = t2550.C2550_CONTROL_NUMBER(+) ");
		strQuery.append("  AND t5081.C205_PART_NUMBER_ID = v205.C205_PART_NUMBER_ID ");
		    
		if(!strLotNumber.equals("")){
			strQuery.append(" AND UPPER(t5081.C2550_CONTROL_NUMBER)  = " );
			strQuery.append(" UPPER('" );
			strQuery.append(strLotNumber);
			strQuery.append("')");
		}
		if (strSearch.equals("0") && (!strPartNum.equals(""))) {  	
	    	strQuery.append(" AND t5081.c205_part_number_id like upper('%"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIT") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5081.c205_part_number_id like upper('"+strPartNum+"')  ");
	    }else if(strSearch.equals("LIKEPRE") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5081.c205_part_number_id like upper('"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIKESUF") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5081.c205_part_number_id like upper('%"+strPartNum+"')  ");
	    }
		if(!strWareHouseType.equals("0")){
		     strQuery.append(" AND t5081.C901_WAREHOUSE_TYPE = ");
		     strQuery.append(strWareHouseType);
		}
		if(!strFromDate.equals("") && !strToDate.equals("")){   //If Expiry Date (From & To) is selected in the filter
		     strQuery.append(" AND t2550.C2550_EXPIRY_DATE BETWEEN TO_DATE('" + strFromDate +"','"+strDateFmt+ "') AND TO_DATE('" + strToDate +"','"+strDateFmt+"')");
		}
		if(!strControlInvId.equals("")){
	    	strQuery.append(" AND t5081.C5080_CONTROL_NUMBER_INV_ID = '"+strControlInvId+"' ");
	    }
		//strLocation, strExpired added for PC-2786 - Add Field Sales Filter in Lot Inventory Report
	    if(!strLocation.equals("")) {
	    	strLocation = strLocation.replaceAll("&", "' || chr(38) || '");
	    	strQuery.append(" AND upper(t5080.c5080_location_name) like upper('%"+strLocation+"%')  ");
	    }
	    if(strExpired.equals("Y")){
	    	 strQuery.append(" AND t2550.c2550_expiry_date < CURRENT_DATE ");
	    }
		if(!strCompanyId.equals("") && !strCompanyId.equals("0")){
		   	strQuery.append(" AND t5081.c1900_company_id = '"+strCompanyId+"' ");
		}
		if(!strPlantId.equals("") && !strPlantId.equals("0")){
		   	strQuery.append(" AND t5081.c5040_plant_id = '"+strPlantId+"' ");
		}
		strQuery.append(" AND t5081.c5081_void_fl IS NULL and t5080.c5080_void_fl IS NULL ");
	    return strQuery;
	}
	/**
	 * This method is used to format the part number based on likes
	 * 
	 * @param hmParam
	 */	
	public String getPartNumFromLikes(HashMap hmParam) throws AppError {
		
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		String strPartNumFormat = strPartNum;
		
		if (strSearch.equals("LIT")) {
			strPartNumFormat = ("^").concat(strPartNumFormat);
			strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
			strPartNumFormat = strPartNumFormat.concat("$");
		} else if (strSearch.equals("LIKEPRE")) {
			strPartNumFormat = ("^").concat(strPartNumFormat);
			strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
		} else if (strSearch.equals("LIKESUF")) {
			strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
			strPartNumFormat = strPartNumFormat.concat("$");
		} else if (strSearch.equals("")) {
			strPartNumFormat = strPartNumFormat;
		} else {
			strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
		}

		return strPartNumFormat;
	}
}
