package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSetPropertyBean extends GmBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.  
    public GmSetPropertyBean() {
        super(GmCommonClass.getDefaultGmDataStoreVO());
      }

      /**
       * Constructor will populate company info.
       * 
       * @param gmDataStore
       */
      public GmSetPropertyBean(GmDataStoreVO gmDataStore) {
        super(gmDataStore);
      }

      // Initialize the Logger Class.
      
    /**
	 * fetchSetWeightDtls - This method will fetch the Weight Details for the Set ID's	 * 
	 * @param HashMap - hmParam
	 * @return RowSetDynaClass
	 * @exception AppError , Exception
	**/
	public RowSetDynaClass fetchSetWeightDtls(HashMap hmParam) throws AppError,Exception {
		RowSetDynaClass rdResult = null;
		String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		String strSetNm = GmCommonClass.parseNull((String) hmParam.get("SETNM"));
		String strSetType = GmCommonClass.parseNull((String) hmParam.get("SETTYPE"));
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_set_master.gm_fch_set_weight", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSetId);
		gmDBManager.setString(2, strSetNm);
		gmDBManager.setString(3, strSetType);
		gmDBManager.execute();
		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		return rdResult;
	} // End of fetchSheetSetParDtls
	
 	/**
     * saveSetWeight - This Method is used to save Weight for the Set's
     * @param HashMap hmParam  
     * @return void
     * @exception AppError
    **/ 
	public void saveSetWeight(HashMap hmParam)throws AppError  {
		String strInputString = GmCommonClass.parseNull((String)hmParam.get("HINPUTSTR"));
		String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));	       
        GmDBManager gmDBManager = new GmDBManager();                
        gmDBManager.setPrepareString("gm_pkg_op_set_master.gm_sav_set_weight",2);
        gmDBManager.setString(1,strInputString);
        gmDBManager.setString(2,strUserId);
        gmDBManager.execute();
        gmDBManager.commit();
    }// End of saveSetWeight
	
	/**
     * fetchSetTissueFlList - This Method is used to get the tissueflag
     * @param String strInputStr  
     * @return void
     * @exception AppError
    **/ 
	
	public String fetchSetTissueFlList(String strInputStr) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		// to get tissueflag with setid
		gmDBManager.setPrepareString(
				"gm_pkg_op_set_validate.gm_fch_set_tissue_fl", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.execute();
		String strReturn = gmDBManager.getString(2);
		log.debug("strReturn" + strReturn);
		gmDBManager.close();
		return strReturn;
	}

}