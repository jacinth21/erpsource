package com.globus.operations.logistics.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTSetWeightWrapper extends TableDecorator
{
    private String strWeight;
    private String strSetID;
    private DynaBean db ;
    private int intCount = 0;
    public StringBuffer  strValue = new StringBuffer();
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    
    /**
     * Returns the WEIGHT value with Text Filed and History Icon.
     * @return String
     */
    public String getWEIGHT(){
        db =    (DynaBean) this.getCurrentRowObject();
        strValue.setLength(0);
        strWeight = String.valueOf(db.get("WEIGHT"));
        strWeight = strWeight.equals("null")?"":strWeight;
        strSetID   = (String)db.get("SETID");        
        strValue.setLength(0);
        strValue.append("<input style='text-align:right' type = 'text' name = weightvalue");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" align = center size = 7  maxlength = 6 value='");
        strValue.append(strWeight);
        strValue.append("' onBlur ='fnWeightValidate(this)'>");  
        strValue.append("<input type = 'hidden' name = setid");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" value='");
        strValue.append(strSetID);
        strValue.append("'> ");
        strValue.append("<input type=hidden name ='hWeightVal");
		strValue.append(intCount);
		strValue.append("' value='");
		strValue.append(strWeight);
		strValue.append("'> ");
        
        intCount++;
        return strValue.toString();        
    }
    
    /**
     * Returns the string formated Set ID while downloading the report to Excel.
     * @return String
     */
    public String getEXCELSETID(){
        db = (DynaBean) this.getCurrentRowObject();
        strSetID   = (String)db.get("SETID");   
        strValue.setLength(0);
        strValue.append("=\""+strSetID+"\"");
        return strValue.toString();
    }  
    
    /**
     * Returns the only WEIGHT value without Textbox and History Icon while downloading the report to Excel. .
     * @return String
     */
    public String getEXCELWEIGHT(){
        db     =   (DynaBean) this.getCurrentRowObject();
        strWeight = String.valueOf(db.get("WEIGHT"));
        return strWeight;
    }
}


