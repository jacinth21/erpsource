package com.globus.operations.logistics.beans;

import com.globus.common.beans.GmBean;     
import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;
import com.globus.valueobject.common.GmDataStoreVO;
import java.util.ArrayList;   
import java.util.HashMap;
import oracle.jdbc.OracleTypes;
import java.sql.ResultSet;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerDashBoardBean extends GmBean {
	
	public GmLoanerDashBoardBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());

	 /**
	   * loadIndividualDash - To fetch the count of  today request of loaners and to show on loaner dashboard screen
	   * 
	   * @param HashMap
	   * @return HashMap
	   * @throws AppError
	*/
public HashMap loadIndividualDash() throws AppError {
		
		HashMap hmResult = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_loaner_dashboard.gm_fch_loaner_overall_dash", 8);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
		gmDBManager.execute();
		
	    String open = GmCommonClass.parseNull(gmDBManager.getString(1));
	    String allocated = GmCommonClass.parseNull(gmDBManager.getString(2));
	    String pendingpick = GmCommonClass.parseNull(gmDBManager.getString(3));
	    String pendingputaway = GmCommonClass.parseNull(gmDBManager.getString(4));
	    String pendingship = GmCommonClass.parseNull(gmDBManager.getString(5));
	    String opensameday = GmCommonClass.parseNull(gmDBManager.getString(6));
	    String topicksameday = GmCommonClass.parseNull(gmDBManager.getString(7));
	    String toshipsameday = GmCommonClass.parseNull(gmDBManager.getString(8));
		hmResult.put("OPEN", open);
		hmResult.put("ALLOCATED", allocated);
		hmResult.put("PENDINGPICK", pendingpick);
		hmResult.put("PENDINGPUTAWAY", pendingputaway);
		hmResult.put("PENDINGSHIP", pendingship);
		hmResult.put("OPENSAMEDAY", opensameday);
		hmResult.put("TOPICKSAMEDAY", topicksameday);
		hmResult.put("TOSHIPSAMEDAY", toshipsameday);
		log.debug("hmResult bean"+hmResult);
		gmDBManager.close();
		return hmResult;
		}

}
