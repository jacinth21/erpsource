/**
 * 
 */
package com.globus.operations.logistics.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sswetha
 *
 */
public class GmBackOrderReleaseBean extends GmBean {
	  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

	  
	  public GmBackOrderReleaseBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmBackOrderReleaseBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	  
	  public ArrayList fetchBackOrderDetails (HashMap hmParam) throws AppError{
		   ArrayList alReturn = new ArrayList();
		   String strAccountName = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTNAME"));
		   String strDistName = GmCommonClass.parseNull((String) hmParam.get("DISTNAME"));
		   String strSalesRepName = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
		   String strPartNos = GmCommonClass.parseNull((String) hmParam.get("PARTNOS"));
		   String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
		   String strBoPriority = GmCommonClass.parseNull((String) hmParam.get("BOPRIORITY"));
		   
			   
		   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());          
		   StringBuffer sbQuery = new StringBuffer();  
					  
		  sbQuery
	        .append("SELECT	T501.C501_ORDER_ID ID,"
	        		+ "v502A.C205_PART_NUMBER_ID NUM,v502A.INSHELF SHELFQTY,v502A.BOPRIORITY BOPRIORITY,"
	        		+ "v502A.BOQTY BOQTY,"
	        		+ "T502.C502_ITEM_QTY QTY,GET_ACCOUNT_NAME(T501.C704_ACCOUNT_ID) ANAME, "
	        		+ "GET_DIST_REP_NAME(T501.C703_SALES_REP_ID) REPDISTNM, "
	        		+ "TO_CHAR(T501.C501_CREATED_DATE,'MM/dd/yyyy HH:MI:SS') TRANSDT,"
	        		+ " GET_BILL_ADD(T501.C704_ACCOUNT_ID) BILLADD,  "
	        		+ "T501.C501_TOTAL_COST + NVL(T501.C501_SHIP_COST,0) SALES, "
	        		+ "GET_USER_NAME(T501.C501_CREATED_BY) UNAME,  "
	        		+ "T501.C501_SHIP_TO SHIPTO, "
	        		+ "T501.C501_SHIP_TO_ID SHIPTOID, T501.C704_ACCOUNT_ID ACCID,  "
	        		+ "T501.C501_PARENT_ORDER_ID MASTER_ID,  "
	        		+ "T502.C502_ITEM_PRICE PRICE ,"
	        		+ "get_partnum_desc(T502.C205_PART_NUMBER_ID) DSC, "
	        		+ "GET_LOG_FLAG(T501.C501_ORDER_ID,1233) LOG_FLAG, "
	        		+ "GET_LOG_FLAG(T501.C501_ORDER_ID,1234) INT_LOG_FLAG, " 
	        		+ "get_account_curr_symb (t501.c704_account_id) acc_currency  "
	        		+ "FROM T501_ORDER T501, T502_ITEM_ORDER T502, V502A_BO_fulfill_list v502A  "
	        		+ "WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID  "
	        		+ "AND t502.c205_part_number_id = v502A.c205_part_number_id "
	        		+ "AND T501.C901_ORDER_TYPE = 2525 "
	        		+ "AND T501.C501_DELETE_FL IS NULL AND T501.C501_VOID_FL IS NULL AND T501.C501_STATUS_FL = 0  "
	        		+ "AND T502.C502_VOID_FL IS NULL "
	        	    + "AND  (T501.C1900_COMPANY_ID = '" + getCompPlantId() + "' OR  T501.C5040_PLANT_ID = '" + getCompPlantId() + "' ) ");
		  	      
	         if(!strAccountName.equals(""))	{
	        	 sbQuery
	 	        .append("  AND T501.C704_ACCOUNT_ID = '" + strAccountName +"'");	
	        }
	         if(!strSalesRepName.equals(""))	{
	        	 sbQuery
	 	        .append("  AND T501.C703_SALES_REP_ID = '" + strSalesRepName +"'");	
	        }
	         if(!strOrderId.equals(""))	{
	        	 sbQuery
	 	        .append("  AND (T501.C501_ORDER_ID = '" + strOrderId +"' OR T501.c501_parent_order_id = '" + strOrderId +"')");	
	        }
	         if(!strPartNos.equals(""))	{
	        	 sbQuery
	 	        .append(" AND T502.C205_PART_NUMBER_ID IN (" + strPartNos +")" );	
	        }
	         if(!strBoPriority.equals(""))	{
	        	 sbQuery
	 	        .append(" AND v502a.BOPRIORITY IN (" + strBoPriority +")" );	
	        }
	         sbQuery
	 	        .append( " ORDER BY V502A.BOPRIORITY ASC,T502.C205_PART_NUMBER_ID, T501.C501_ORDER_DATE");
	        
	        
		    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		    log.debug("sbQuery.toString():"+sbQuery.toString());
		 	return alReturn;
		 	}	

	  public HashMap processBackOrderRelease(HashMap hmParam) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
		    String strBeanMsg = "";
		    String strPrepareString = null;
		    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
		    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO")); 
		    String strBOReleaseQty = GmCommonClass.parseNull((String) hmParam.get("BORELEASEQTY")); 
		    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		    HashMap hmReturn = new HashMap();


		    gmDBManager.setPrepareString("gm_pkg_op_bo_txn.gm_process_sales_backorder", 5);

		    /*
		     * register out parameter and set input parameters
		     */
		    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

		    gmDBManager.setString(1, strOrderId);
		    gmDBManager.setString(2, strPartNum);
		    gmDBManager.setString(3, strBOReleaseQty);
		    gmDBManager.setString(4, strUserId);
		    gmDBManager.execute();

		    strBeanMsg = GmCommonClass.parseNull(gmDBManager.getString(5));
		    gmDBManager.commit();
		     log.debug("strBeanMsg:"+strBeanMsg);

		    hmReturn.put("MSG", strBeanMsg);

		    if (strBeanMsg.equals("")){
			    // To Save the Insert part number details in Back order screen using JMS
			    HashMap hmJMSParam = new HashMap();
			    hmJMSParam.put("TXNID", strOrderId);
			   	hmJMSParam.put("STATUS", "93342");
			    hmJMSParam.put("USERID", strUserId);
			    hmJMSParam.put("COMPANYINFO", strCompanyInfo);
			    gmItemControlTxnBean.saveInsertByJMS(hmJMSParam);
		    }
		    return hmReturn;
		  } // End of processBackOrderRelease

	 
	 

}
