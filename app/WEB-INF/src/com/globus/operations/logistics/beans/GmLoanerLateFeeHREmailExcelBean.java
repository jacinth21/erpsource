package com.globus.operations.logistics.beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.excel.GmExcelManipulation;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerLateFeeHREmailExcelBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public String strExcelFilePath = GmCommonClass.getString("LATEFEE_HR_MAIL_EXCEL_FILEPATH_MASTER");
	public String strTempFilePath = GmCommonClass.getString("LATEFEE_HR_MAIL_EXCEL_FILEPATH");

//	String strExcelFilepath = System.getProperty("ENV_LATEFEE_HR_MAIL_EXCEL_FILEPATH");
	public GmLoanerLateFeeHREmailExcelBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
	}
	
	/**
	   * @description Generate excel with email summary data of Direct Rep Late Fee
	   * @param alSummary
	   * @return strTempFileName
	   * @throws Exception
	   */
	public String saveDirectRepLateFeeHRExcel(ArrayList alSummary) throws Exception {
		log.debug("saveDirectRepLateFeeHRExcel -->strExcelFilePath =" + strExcelFilePath);
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy_HH-mm-ss");
		String strDate = formatter.format(date);

//	    String strExcelFileName =   strExcelFilePath+"HR_DIRECT_REP_LATE_FEES"+" _"+ strDate+".xls";
		String strMasterFileName = strExcelFilePath + "HR_DIRECT_REP_LATE_FEES.xls";
		String strTempFileName = strTempFilePath + "HR_DIRECT_REP_LATE_FEES" + "_" + strDate + ".xls";
		GmExcelManipulation.copyFile(strMasterFileName, strTempFileName);
		populateDirectRepLateFeeHRExcelsummaryHeader(strTempFileName);
		populateDirectRepLateFeeHRExcelsummary(alSummary, strTempFileName);
		log.debug("saveDirectRepLateFeeHRExcel --> strTempFileName =" + strTempFileName);
		return strTempFileName;
	}
	
	/**
	   * @description Generate  Header for direct rep late fee excel 
	   * @param strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDirectRepLateFeeHRExcelsummaryHeader(String strExcelFileName) throws Exception {
		log.debug("populateDirectRepLateFeeHRExcelsummaryHeader " + strExcelFileName);
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel("Rep Name");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel("Amount");

	}
	
	/**
	   * @description Generate  Exel values for direct rep late fee excel 
	   * @param alSummary, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDirectRepLateFeeHRExcelsummary(ArrayList alSummary, String strExcelFileName) throws Exception {
		// � The excel should contain Rep name and amount column
		log.debug("populateDirectRepLateFeeHRExcelsummary " + strExcelFileName);
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(alSummary);

	}
	
	/**
	   * @description Generate excel with email summary and charges details data of Dist Rep Late Fee
	   * @param alSummary, alDetail
	   * @return strTempFileName
	   * @throws Exception
	   */
	public String saveDistRepLateFeeHRExcel(ArrayList alSummary, ArrayList alDetail) throws Exception {
		log.debug("saveDistRepLateFeeHRExcel =" + strExcelFilePath);
		String strMasterFileName = strExcelFilePath + "HR_DIST_LATE_FEES.xls";
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy_HH-mm-ss");
		String strDate = formatter.format(date);

		String strTempFileName = strTempFilePath + "HR_DIST_LATE_FEES" + " _" + strDate + ".xls";
		GmExcelManipulation.copyFile(strMasterFileName, strTempFileName);

		populateDistRepLateFeeHRExcelsummaryHeader(strTempFileName);
//	Generate Excell
		populateDistRepLateFeeHRExcelsummary(alSummary, strTempFileName);

		ArrayList alTempEmailData = new ArrayList();
		int alDetailSize = alDetail.size();
		for (int i = 0; i < alDetailSize; i++) {
			HashMap hmResultVal = (HashMap) alDetail.get(i);

			LinkedHashMap hmlateFeeVal = new LinkedHashMap();
			
			String strDistName = GmCommonClass.parseNull((String) hmResultVal.get("DISTRIBUTORNAME"));
			String strRepName = GmCommonClass.parseNull((String) hmResultVal.get("REPNAME"));
			String strAssocRepName = GmCommonClass.parseNull((String) hmResultVal.get("ASSREPNAME"));
			String strReqId = GmCommonClass.parseNull((String) hmResultVal.get("REQUESTID"));
			String strSetName = GmCommonClass.parseNull((String) hmResultVal.get("SETNAME"));
			String strIncidentType = GmCommonClass.parseNull((String) hmResultVal.get("INCIDENTTYPE"));
			String strIncidentDt = GmCommonClass.parseNull((String) hmResultVal.get("INCIDENTDATE"));
			String strLoanedDate = GmCommonClass.parseNull((String) hmResultVal.get("LOANEDDATE"));
			String strExpectedReturn = GmCommonClass.parseNull((String) hmResultVal.get("EXPECTEDRETURNDATE"));
			String strIncidentValue = GmCommonClass.parseNull((String) hmResultVal.get("CONSIGNID"));
			String strActualReturnDate = GmCommonClass.parseNull((String) hmResultVal.get("ACTUALRETURNDATE"));
			String strDaysOverdue = GmCommonClass.parseNull((String) hmResultVal.get("DAYSOVERDUE"));
			String strAmount = GmCommonClass.parseNull((String) hmResultVal.get("AMOUNT"));

			hmlateFeeVal.put("DISTRIBUTORNAME", strDistName);
			hmlateFeeVal.put("REPNAME", strRepName);
			hmlateFeeVal.put("ASSREPNAME", strAssocRepName);
			hmlateFeeVal.put("REQUESTID", strReqId);
			hmlateFeeVal.put("SETNAME", strSetName);
			hmlateFeeVal.put("INCIDENTTYPE", strIncidentType);
			hmlateFeeVal.put("LOANEDDATE", strLoanedDate);
			hmlateFeeVal.put("EXPECTEDRETURNDATE", strExpectedReturn);
			hmlateFeeVal.put("ACTUALRETURNDATE", strActualReturnDate);
			hmlateFeeVal.put("DAYSOVERDUE", strDaysOverdue);
			hmlateFeeVal.put("CONSIGNID", strIncidentValue);
			hmlateFeeVal.put("INCIDENTDATE", strIncidentDt);
			hmlateFeeVal.put("AMOUNT", strAmount);

			alTempEmailData.add(hmlateFeeVal);
		}

		populateDistRepLateFeeHRExcelDetailHeader(strTempFileName);

		populateDistRepLateFeeHRExcelDetail(alTempEmailData, strTempFileName);

		return strTempFileName;
	}
	
	/**
	   * @description Generate  Header for dist rep late fee excel 
	   * @param strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepLateFeeHRExcelDetailHeader(String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel("Distributor Name");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel("Rep Name");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.writeToExcel("Assoc. Name");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(3);
		gmExcelManipulation.writeToExcel("Parent Req ID");

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(4);
		gmExcelManipulation.writeToExcel("Set Name");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(5);
		gmExcelManipulation.writeToExcel("Incident Type");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(6);
		gmExcelManipulation.writeToExcel("Loaned Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(7);
		gmExcelManipulation.writeToExcel("Exp. Return Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(8);
		gmExcelManipulation.writeToExcel("Actual Return Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(9);
		gmExcelManipulation.writeToExcel("Days Overdue");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(10);
		gmExcelManipulation.writeToExcel("Etch ID/Trans ID");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(11);
		gmExcelManipulation.writeToExcel("Incident Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(12);
		gmExcelManipulation.writeToExcel("Amount");

	}
	
	/**
	   * @description Generate  Header for direct rep late fee excel 
	   * @param strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepLateFeeHRExcelsummaryHeader(String strExcelFileName) throws Exception {

		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel("Dist Name");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel("Rep Name");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.writeToExcel("Assoc.Rep");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(3);
		gmExcelManipulation.writeToExcel("Amount");

	}
	
	/**
	   * @description Generate  Excel values for dist rep late fee excel 
	   * @param alDetail, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepLateFeeHRExcelDetail(ArrayList alDetail, String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(alDetail);
	}
	
	/**
	   * @description Generate  Excel values for dist rep late fee excel 
	   * @param alSummary, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepLateFeeHRExcelsummary(ArrayList alSummary, String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(alSummary);
	}
	
	/**
	   * @description Generate excel with email summary and charges details data of Direct Rep credit Late Fee
	   * @param alSummary, alDetail
	   * @return strTempFileName
	   * @throws Exception
	   */
	public String saveDirectRepCreditHRExcel(ArrayList alSummary, ArrayList alDetail) throws Exception {
		String strMasterFileName = strExcelFilePath + "HR_CREDIT_DIRECT_DIST_REPS.xls";
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy_HH-mm-ss");
		String strDate = formatter.format(date);

		String strTempFileName = strTempFilePath + "HR_CREDITS_DIRECT_REPS" + " _" + strDate + ".xls";
		GmExcelManipulation.copyFile(strMasterFileName, strTempFileName);

		populateDirectRepCreditHRExcelSummaryHeader(strTempFileName);
		populateDirectRepCreditHRExcelSummary(alSummary, strTempFileName);

		ArrayList alTempEmailData = new ArrayList();
		int alDetailSize = alDetail.size();
		for (int i = 0; i < alDetailSize; i++) {
			HashMap hmResultVal = (HashMap) alDetail.get(i);

			LinkedHashMap hmlateFeeVal = new LinkedHashMap();
			
			String strRepName = GmCommonClass.parseNull((String) hmResultVal.get("REPNAME"));
			String strReqId = GmCommonClass.parseNull((String) hmResultVal.get("REQUESTID"));
			String strSetName = GmCommonClass.parseNull((String) hmResultVal.get("SETNAME"));
			String strIncidentDt = GmCommonClass.parseNull((String) hmResultVal.get("INCIDENTDATE"));
			String strLoanedDate = GmCommonClass.parseNull((String) hmResultVal.get("LOANEDDATE"));
			String strExpectedReturn = GmCommonClass.parseNull((String) hmResultVal.get("EXPECTEDRETURNDATE"));
			String strIncidentValue = GmCommonClass.parseNull((String) hmResultVal.get("CONSIGNID"));
			String strActualReturnDate = GmCommonClass.parseNull((String) hmResultVal.get("ACTUALRETURNDATE"));
			String strDaysOverdue = GmCommonClass.parseNull((String) hmResultVal.get("DAYSOVERDUE"));
			String strAmount = GmCommonClass.parseNull((String) hmResultVal.get("AMOUNT"));

			hmlateFeeVal.put("REPNAME", strRepName);
			hmlateFeeVal.put("REQUESTID", strReqId);
			hmlateFeeVal.put("SETNAME", strSetName);
			hmlateFeeVal.put("LOANEDDATE", strLoanedDate);
			hmlateFeeVal.put("EXPECTEDRETURNDATE", strExpectedReturn);
			hmlateFeeVal.put("ACTUALRETURNDATE", strActualReturnDate);
			hmlateFeeVal.put("DAYSOVERDUE", strDaysOverdue);
			hmlateFeeVal.put("CONSIGNID", strIncidentValue);
			hmlateFeeVal.put("INCIDENTDATE", strIncidentDt);
			hmlateFeeVal.put("AMOUNT", strAmount);

			alTempEmailData.add(hmlateFeeVal);
		}
		populateDirectRepCreditHRExcelDetailHeader(strTempFileName);
		populateDirectRepCreditHRExcelDetail(alTempEmailData, strTempFileName);
		log.debug("saveDirectRepCreditHRExcel "+ strTempFileName);
		return strTempFileName;
	}
	
	/**
	   * @description Generate  Header for direct rep credit late fee excel 
	   * @param strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDirectRepCreditHRExcelDetailHeader(String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel("Rep Name");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel("Parent Req Id");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.writeToExcel("Set Name");

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(3);
		gmExcelManipulation.writeToExcel("Loaned Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(4);
		gmExcelManipulation.writeToExcel("Exp. Return Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(5);
		gmExcelManipulation.writeToExcel("Actual Return Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(6);
		gmExcelManipulation.writeToExcel("Days Overdue");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(7);
		gmExcelManipulation.writeToExcel("Etch ID/Trans ID");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(8);
		gmExcelManipulation.writeToExcel("Incident date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(9);
		gmExcelManipulation.writeToExcel("Amount");

	}
	
	/**
	   * @description Generate  Header for direct rep credit late fee excel 
	   * @param strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDirectRepCreditHRExcelSummaryHeader(String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel("Rep Name");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel("Amount");
	}
	
	/**
	   * @description Generate Excel values for direct rep credit late fee excel 
	   * @param alDetail, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDirectRepCreditHRExcelDetail(ArrayList alDetail, String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(alDetail);

	}
	
	/**
	   * @description Generate  Excel values for direct rep credit late fee excel 
	   * @param alSummary, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDirectRepCreditHRExcelSummary(ArrayList alSummary, String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(alSummary);
	}
	
	/**
	   * @description Generate excel with email summary and charges details data of Dist Rep credit Late Fee
	   * @param alSummary, alDetail
	   * @return strTempFileName
	   * @throws Exception
	   */
	public String saveDistRepCreditHRExcel(ArrayList alSummary, ArrayList alDetail) throws Exception {
		String strMasterFileName = strExcelFilePath + "HR_CREDIT_DIRECT_DIST_REPS.xls";
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy_HH-mm-ss");
		String strDate = formatter.format(date);
		
		String strTempFileName = strTempFilePath + "HR_CREDITS_DIST" + " _" + strDate + ".xls";
		GmExcelManipulation.copyFile(strMasterFileName, strTempFileName);
		
		ArrayList alTempEmailData = new ArrayList();
		int alDetailSize = alDetail.size();
		for (int i = 0; i < alDetailSize; i++) {
			HashMap hmResultVal = (HashMap) alDetail.get(i);

			LinkedHashMap hmlateFeeVal = new LinkedHashMap();
			
			String strDistName = GmCommonClass.parseNull((String) hmResultVal.get("DISTRIBUTORNAME"));
			String strRepName = GmCommonClass.parseNull((String) hmResultVal.get("REPNAME"));
			String strAssocRepName = GmCommonClass.parseNull((String) hmResultVal.get("ASSREPNAME"));
			String strReqId = GmCommonClass.parseNull((String) hmResultVal.get("REQUESTID"));
			String strSetName = GmCommonClass.parseNull((String) hmResultVal.get("SETNAME"));
			String strIncidentDt = GmCommonClass.parseNull((String) hmResultVal.get("INCIDENTDATE"));
			String strLoanedDate = GmCommonClass.parseNull((String) hmResultVal.get("LOANEDDATE"));
			String strExpectedReturn = GmCommonClass.parseNull((String) hmResultVal.get("EXPECTEDRETURNDATE"));
			String strIncidentValue = GmCommonClass.parseNull((String) hmResultVal.get("CONSIGNID"));
			String strActualReturnDate = GmCommonClass.parseNull((String) hmResultVal.get("ACTUALRETURNDATE"));
			String strDaysOverdue = GmCommonClass.parseNull((String) hmResultVal.get("DAYSOVERDUE"));
			String strAmount = GmCommonClass.parseNull((String) hmResultVal.get("AMOUNT"));

			hmlateFeeVal.put("DISTRIBUTORNAME", strDistName);
			hmlateFeeVal.put("REPNAME", strRepName);
			hmlateFeeVal.put("ASSREPNAME", strAssocRepName);
			hmlateFeeVal.put("REQUESTID", strReqId);
			hmlateFeeVal.put("SETNAME", strSetName);
			hmlateFeeVal.put("INCIDENTDATE", strIncidentDt);
			hmlateFeeVal.put("EXPECTEDRETURNDATE", strExpectedReturn);
			hmlateFeeVal.put("ACTUALRETURNDATE", strActualReturnDate);
			hmlateFeeVal.put("DAYSOVERDUE", strDaysOverdue);
			hmlateFeeVal.put("CONSIGNID", strIncidentValue);
			hmlateFeeVal.put("LOANEDDATE", strLoanedDate);
			hmlateFeeVal.put("AMOUNT", strAmount);

			alTempEmailData.add(hmlateFeeVal);
		}

		populateDistRepCreditHRExcelSummaryHeader(strTempFileName);
		populateDistRepCreditHRExcelSummary(alSummary, strTempFileName);
		populateDistRepCreditHRExcelDetailHeader(strTempFileName);
		populateDistRepCreditHRExcelDetail(alTempEmailData, strTempFileName);
		log.debug("saveDistRepCreditHRExcel -->"+strTempFileName);
		return strTempFileName;
	}
	
	/**
	   * @description Generate  Excel values for dist rep credit late fee excel 
	   * @param alDetail, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepCreditHRExcelDetail(ArrayList alDetail, String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(alDetail);

	}
	
	/**
	   * @description Generate  Excel values for dist rep credit late fee excel 
	   * @param alSummary, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepCreditHRExcelSummary(ArrayList alSummary, String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(alSummary);

	}
	
	/**
	   * @description Generate  Header for dist rep credit late fee excel 
	   * @param alSummary, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepCreditHRExcelDetailHeader(String strExcelFileName) throws Exception {
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		
		
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		
		
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel("Distributor Name");
		
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel("Rep Name");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.writeToExcel("Assoc.Rep");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.writeToExcel("Assoc.Rep");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(3);
		gmExcelManipulation.writeToExcel("Parent Req ID");

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(4);
		gmExcelManipulation.writeToExcel("Set Name");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(5);
		gmExcelManipulation.writeToExcel("Incident Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(6);
		gmExcelManipulation.writeToExcel("Exp. Return Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(7);
		gmExcelManipulation.writeToExcel("Actual Return Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(8);
		gmExcelManipulation.writeToExcel("Days Overdue");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(9);
		gmExcelManipulation.writeToExcel("Etch ID/Trans ID");
		
		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(10);
		gmExcelManipulation.writeToExcel("Loaned Date");

		gmExcelManipulation.setSheetNumber(1);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(11);
		gmExcelManipulation.writeToExcel("Amount");
		

	}
	
	/**
	   * @description Generate  Header for dist rep credit late fee excel 
	   * @param alSummary, strExcelFileName
	   * @return 
	   * @throws Exception
	   */
	private void populateDistRepCreditHRExcelSummaryHeader(String strExcelFileName) throws Exception {

		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();

		gmExcelManipulation.setExcelFileName(strExcelFileName);
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel("Dist Name");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel("Rep Name");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.writeToExcel("Assoc.Rep");

		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(3);
		gmExcelManipulation.writeToExcel("Amount");

	}

}
