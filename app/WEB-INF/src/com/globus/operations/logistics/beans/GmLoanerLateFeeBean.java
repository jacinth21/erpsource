package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerLateFeeBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
	public GmLoanerLateFeeBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		
	}
	
	/**
	   * @description fetch loaner late fee charge details based on the filters
	   * @param hmParam
	   * @return
	   * @throws Exception
	   */
	public String fetchChargesLateFeeBySetReport(HashMap hmParam) {
	
		String strSalesRep = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
	    String strDist = GmCommonClass.parseNull((String) hmParam.get("FIELDSALES"));
	    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
	    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
	    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
	    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
	    String strSalesRepNm = GmCommonClass.parseNull((String) hmParam.get("SEARCHSALESREP"));
	    String strDistNm = GmCommonClass.parseNull((String) hmParam.get("SEARCHFIELDSALES"));
	    String strZone = GmCommonClass.parseNull((String) hmParam.get("ZONE"));
	    String strRegion = GmCommonClass.parseNull((String) hmParam.get("REGION"));
	    String strDivision = GmCommonClass.parseNull((String) hmParam.get("DIVISION"));
	    
	    if(!strZone.equals("")){
	    	strZone = strZone.replaceAll(",", "','");
		}
	    
	    if(!strRegion.equals("")){
	    	strRegion = strRegion.replaceAll(",", "','");
		}
	    if(!strDivision.equals("")){
	    	strDivision = strDivision.replaceAll(",", "','");
		}
	    
	    String strCompDFmt = getCompDateFmt();
	    String strStatusAlt = "";
	    
	    String strFilter = 
	    		GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_CHARGES_RPT"));/*LOANER_CHARGES_RPT- To fetch the records for EDC entity countries based on plant*/
	    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	
	    sbQuery.append(" SELECT JSON_ARRAYAGG( ");
	    sbQuery.append(" JSON_OBJECT( 'pkey' value t9200.c9200_ref_id || '-' || TO_CHAR(c9200_incident_date, 'YYYYMM'), ");
	    sbQuery.append(" 'id' value c9201_charge_details_id ,'salesrep' VALUE t703.c703_sales_rep_name,");
	    sbQuery.append(" 'distname' value t701.c701_distributor_name , 'etchid' VALUE mv.c504a_etch_id || '/' || mv.c504_consignment_id, ");
	    sbQuery.append(" 'dist_type' value t701.c901_distributor_type, 'loanertransid' value mv.c526_product_request_detail_id ,");
	    sbQuery.append(" 'incidenttype' value  get_code_name(t9200.c901_incident_type) ,'incidentvalue' value mv.c504_consignment_id,");
	    sbQuery.append(" 'stfl' VALUE t901.c901_code_id , 'stflname' value t901.c901_code_id , 'hiddenstflname' VALUE t901.c901_code_nm, ");
	    sbQuery.append(" 'incidentdate' value to_char(c9200_incident_date, '"+ getCompDateFmt() + "'),'amount' value c9201_amount , 'itype' value c901_incident_type ,");
	    sbQuery.append(" 'reqid' VALUE mv.c525_product_request_id , 'setdesc' VALUE t207.c207_set_nm ,'setid' value mv.c207_set_id, ");
	    sbQuery.append(" 'ldate' value to_char(mv.c504a_loaner_dt, '" + getCompDateFmt() + "'), 'erdate' value to_char(mv.c504a_expected_return_dt, '" + getCompDateFmt() + "'), 'rdate' value to_char(c504a_return_dt, '" + getCompDateFmt() + "'), ");
	    sbQuery.append(" 'chargestartdate' value to_char(c9201_charge_start_date, '" + getCompDateFmt() + "'), 'chargeenddate' value to_char(c9201_charge_end_date, '" + getCompDateFmt() + "'), 'notes' VALUE c9201_comment , ");
	    sbQuery.append(" 'count' value gm_pkg_op_loaner_charges_log.get_charge_history_count(c9201_charge_details_id) , ");
	    sbQuery.append(" 'hamtcnt' value gm_pkg_op_loaner_charges_log.get_charge_amount_count(c9201_charge_details_id) , ");
	    sbQuery.append(" 'elpdays' VALUE gm_pkg_op_loaner_charges.get_work_days_count(TRUNC(mv.c504a_expected_return_dt), ");
	    sbQuery.append(" TRUNC(NVL( mv.c504a_return_dt, CURRENT_DATE))) ,'lnstatus' value mv.lnstatus,'assocrep' VALUE get_rep_name(t9201.c703_ass_rep_id) , ");
	    sbQuery.append(" 'creditdate' VALUE to_char(t9201.c9201_credited_date, '"+ getCompDateFmt() + "') , 'tagid' VALUE mv.tagid,'waivereason' VALUE t9201.C901_RECON_COMMENTS,'hwaivereason' VALUE t9201.C901_RECON_COMMENTS)");
	    sbQuery.append(" ORDER BY t9201.c703_sales_rep_id, mv.c525_product_request_id,c9200_incident_date ");
	    sbQuery.append(" RETURNING CLOB  ) FROM ");
	    sbQuery.append(" t9200_incident t9200, t9201_charge_details t9201, v_loaner_late_fee mv, t701_distributor t701, ");
	    sbQuery.append(" t207_set_master t207, T901_CODE_LOOKUP t901, t703_sales_rep t703");
	    sbQuery.append(" WHERE t9200.c9200_incident_id = t9201.c9200_incident_id ");
	    sbQuery.append(" AND t9200.c9200_void_fl      IS NULL ");
	    sbQuery.append(" AND t9200.c901_incident_type  = 92068 "); //--Late Fee
	    sbQuery.append(" AND ( t9200.c1900_company_id = '" + getCompId() + "' ");
	    sbQuery.append(" OR t9200.c1900_company_id IN ( ");
	    sbQuery.append(" SELECT c1900_company_id ");
	    sbQuery.append(" FROM t5041_plant_company_mapping ");
	    sbQuery.append(" WHERE c5041_primary_fl = 'Y' ");
	    sbQuery.append(" AND c5041_void_fl IS NULL ");
	    sbQuery.append(" AND c5040_plant_id = '" + getCompId() + "' ))");
	    sbQuery.append(" AND t9201.c9201_void_fl                      IS NULL ");
	    if (!strFromDt.equals("") && !strToDt.equals("")) {
		      sbQuery.append(" AND ( ((t9201.c9201_charge_start_date) >= trunc(TO_DATE('");
		      sbQuery.append(strFromDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" )) ");
		      sbQuery.append(" AND (t9201.c9201_charge_start_date) < trunc(TO_DATE('");
		      sbQuery.append(strToDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" )+1) ");
		      sbQuery.append(" OR ((t9201.c9201_charge_end_date) >= trunc(TO_DATE('");
		      sbQuery.append(strFromDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" )) ");
		      sbQuery.append(" AND (t9201.c9201_charge_end_date) < trunc(TO_DATE('");
		      sbQuery.append(strToDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" )+1) ");
		      sbQuery.append(" )) )");
		    }

	    sbQuery.append(" AND   mv.c526_product_request_detail_id = t9200.c9200_ref_id ");
	    sbQuery.append(" AND t9201.c701_distributor_id = t701.c701_distributor_id ");
	    sbQuery.append(" AND mv.c207_set_id =t207.c207_set_id ");
	    sbQuery.append(" and t9201.c9201_status =  t901.c902_code_nm_alt ");
	    sbQuery.append(" AND t901.C901_CODE_GRP = 'FEE_ST' ");
	    sbQuery.append(" AND t901.c901_void_fl IS NULL ");
	    sbQuery.append(" AND  t9201.c703_sales_rep_id =  t703.c703_sales_rep_id ");
	    sbQuery.append(" AND t703.c703_void_fl IS NULL ");
	    
	    //to search based on Sales Rep name
	    if (strSalesRep.equals("") && !strSalesRepNm.equals("")) {
		      sbQuery.append(" and UPPER(t703.C703_SALES_REP_NAME) LIKE UPPER('%");
		      sbQuery.append(strSalesRepNm);
		      sbQuery.append("%') ");
		    }
	    
		    if (!strDist.equals("")) {
		      sbQuery.append(" and t701.C701_DISTRIBUTOR_ID ='");
		      sbQuery.append(strDist);
		      sbQuery.append("' ");
		    }
		    
		    if (!strSalesRep.equals("")) {
			      sbQuery.append(" and t9201.c703_sales_rep_id ='");
			      sbQuery.append(strSalesRep);
			      sbQuery.append("' ");
			    }
		    
		    if (!strZone.equals("")) {
				//PC-3678 void flag check issue //PC-4483 - Add active flag filter
		      sbQuery.append(" AND t701.C701_REGION in ( select c901_area_id from t710_sales_hierarchy where c710_void_fl is null AND c710_active_fl='Y' AND c901_zone_id in ('");
		      sbQuery.append(strZone);
		      sbQuery.append("')) ");
		    }
		    if (!strRegion.equals("")) {
		      sbQuery.append(" and t701.C701_REGION in ('");
		      sbQuery.append(strRegion);
		      sbQuery.append("') ");
		      }
		    
		    if (!strDivision.equals("")) {
			      sbQuery.append("  AND t207.C1910_DIVISION_ID IN (SELECT c906_rule_value FROM t906_rules WHERE c906_rule_grp_id ='LNLATEFEEDIVISIONS' AND c906_rule_id IN ('");
			      sbQuery.append(strDivision);
			      sbQuery.append("') ");
			      sbQuery.append(" AND c906_void_fl IS NULL) ");
		    }	
		  //to search based on Distributor name
		    if (strDist.equals("") && !strDistNm.equals("")) {
			      sbQuery.append(" and UPPER(t701.C701_DISTRIBUTOR_ID) LIKE UPPER('%");
			      sbQuery.append(strDistNm);
			      sbQuery.append("%') ");
			}
		    
		    
		    if (!strStatus.equals("")&& !strStatus.equals("0")) {
		      strStatusAlt = GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strStatus));
		      sbQuery.append("  AND t9201.C9201_status = ");
		      sbQuery.append(strStatusAlt);
		    }
		    if (!strRequestId.equals("") ){ //PMT-8847 
		    	  sbQuery.append(" AND mv.C525_Product_Request_Id ='");
		          sbQuery.append(strRequestId);
		          sbQuery.append("' ");
		    }
		      
    String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
    log.debug(" late fee charges=============== =>"+ sbQuery.toString());
    gmDBManager.close();
   return strReturn;
      }

	 /**
	   * @description save tha late fee amount while tab out from grid
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	public void saveLoanerLateFeeAmount(HashMap hmParam) {
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strAmount = GmCommonClass.parseNull((String) hmParam.get("AMOUNT"));
	    String strChargeDtlId = GmCommonClass.parseNull((String) hmParam.get("CHARGEDTLID"));
	
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_txn.gm_sav_loaner_charge_amount", 3);
	    gmDBManager.setString(1, strChargeDtlId);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.setString(3, strAmount);
	    gmDBManager.execute();
	    gmDBManager.commit();
	}
	
	 /**
	   * @description save tha late fee comment while tab out from grid
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	public void saveLoanerLateFeeComment(HashMap hmParam) {
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strNotes = GmCommonClass.parseNull((String) hmParam.get("COMMENT"));
	    String strChargeDtlId = GmCommonClass.parseNull((String) hmParam.get("CHARGEDTLID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	    gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_txn.gm_sav_loaner_charge_comment", 3);
	    gmDBManager.setString(1, strChargeDtlId);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.setString(3, strNotes);
	    
	    gmDBManager.execute();
	    gmDBManager.commit();
	}
	
	/**
	   * @description save the credit details from window opener
	   * @param hmParam
	   * @return
	   * @throws Exception,AppError
	   */
	public void saveLoanerLateFeeCreditAmount(HashMap hmParam) throws AppError, Exception{
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strCreditAmt = GmCommonClass.parseNull((String) hmParam.get("CREDITAMOUNT"));
	    String strChargeDetailId = GmCommonClass.parseNull((String) hmParam.get("CHARGEDETAILID"));
	    String strCreditNotes = GmCommonClass.parseNull((String) hmParam.get("CREDITNOTES"));
	    String strCreditDt = GmCommonClass.parseNull((String) hmParam.get("CREDITDATE"));
	    String strChargeFromDt = GmCommonClass.parseNull((String) hmParam.get("CHARGEFROMDATE"));
	    String strChargeToDt = GmCommonClass.parseNull((String) hmParam.get("CHARGETODATE"));
	    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		strDateFmt = strDateFmt.equals("") ? "MM/dd/yyyy" : strDateFmt;
		 
		Date dtChargeFromDT = GmCommonClass.getStringToDate(strChargeFromDt, strDateFmt);
	    Date dtChargeToDT = GmCommonClass.getStringToDate(strChargeToDt, strDateFmt);
	    Date dtCreditDt = GmCommonClass.getStringToDate(strCreditDt, strDateFmt);
		
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_txn.gm_sav_loaner_charge_credit_amount",7);
	    gmDBManager.setString(1, strChargeDetailId);
	    gmDBManager.setString(2, strCreditAmt);
	    gmDBManager.setString(3, strCreditNotes);
	    gmDBManager.setDate(4, dtCreditDt);
	    gmDBManager.setDate(5, dtChargeFromDT);
	    gmDBManager.setDate(6, dtChargeToDT);
	    gmDBManager.setString(7, strUserId);
	    gmDBManager.execute();
	    gmDBManager.commit();
	}
	
	
	/**
	   * @description save the status what are the rows changed
	   * @param hmParam
	   * @return
	   * @throws Exception,AppError
	   */
	public void saveLoanerLateFeeStatus(HashMap hmParam) throws AppError, Exception{
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_txn.gm_sav_loaner_charge_status",2);
	    gmDBManager.setString(1, strInputString);
	    gmDBManager.setString(2, strUserId);
	    
	    gmDBManager.execute();
	    gmDBManager.commit();
	}
	
	/**
	   * @description fetch the division 
	   * @param company id
	   * @return
	   * @throws AppError
	   */
	 public ArrayList fetchDivision(String  strCompanyId) throws AppError{
		  
		  ArrayList alDivisionList = new ArrayList();
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_rpt.gm_fch_division", 2);
		  gmDBManager.setString(1, strCompanyId);
		  gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		  gmDBManager.execute();
		  
		  alDivisionList =GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		  gmDBManager.close();
		  
	  return alDivisionList;
}
	 
	 /**
	   * @description fetch the zone values
	   * @param company id
	   * @return
	   * @throws AppError
	   */
	 public ArrayList getZone(String strCmpId) throws AppError {
		    ArrayList alResult = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_rpt.gm_fch_zone_cmp", 2);
			gmDBManager.setString(1, strCmpId);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
			gmDBManager.close();
		    return alResult;
	 }
	 /**
	   * @description fetch the sum of credit amount
	   * @param strChargeDetailId 
	   * @return
	   * @throws AppError
	   */
	 public HashMap fetchCreditAmountDetail(String strChargeDetailId) throws AppError{
		 HashMap hmParam = new HashMap();
		 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_rpt.gm_fch_credit_amount_detail", 2);
			gmDBManager.setString(1, strChargeDetailId);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			hmParam = GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)));
			gmDBManager.close();
		    return hmParam;
		 
	 }
}
