package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBackOrderReportBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  public GmBackOrderReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmBackOrderReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * loadRequestMasterReport - This method will be used to fetch the data for all REquests
   * 
   * @param hmParam - to be used later for any filters
   * @exception AppError
   */
  public RowSetDynaClass loadSetStatusReport(HashMap hmParam) throws AppError {

    RowSetDynaClass rdResult = null;

    String stRControlNum = GmCommonClass.parseNull((String) hmParam.get("CONTROLNUM"));
    String strControlNumOper = GmCommonClass.parseNull((String) hmParam.get("CTRLNUMOPER"));

    log.debug("strControlNumOper = " + strControlNumOper);

    String strCheckSelectedCon = GmCommonClass.parseNull((String) hmParam.get("INPUTCON"));
    String partNumRegexp = GmCommonClass.parseNull((String) hmParam.get("REGEXPATTERN"));
    boolean summaryVer =
        GmCommonClass.getCheckBoxValue((String) hmParam.get("SUMMARYVER")).equals("Y") ? true
            : false;
    String strCompPlantFilter = "";
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "SET_STATUS_REPORT")); // SET_STATUS_REPORT-Rule
                                                                                               // Value:=
                                                                                               // plant.
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();
    // //log.debug("Summary Flag"+GmCommonClass.parseNull((String)hmParam.get("SUMMARYVER")));
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      // //log.debug("hmParam" + hmParam);
      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(getSelectStatement(summaryVer));
      sbQuery.append(" FROM (");

      if (stRControlNum.equals("")) {
        sbQuery.append(" SELECT t520.C520_REQUEST_ID REQID, ");
        sbQuery.append(" t520.C207_SET_ID SETID,  T207.C207_SET_NM SETNAME, ");
        sbQuery.append(" '' CONID, ");
        sbQuery
            .append(" DECODE (T520.C520_STATUS_FL, 10, 'Back Order', 15, 'Back Log', 20, 'Ready To Consign') REQSTATUS, ");
        sbQuery.append(" '' CONSTATUS, ");
        sbQuery.append(" T521.C205_PART_NUMBER_ID PNUM, ");
        sbQuery.append(" T205.C205_PART_NUM_DESC PDESC, ");
        sbQuery.append(" T521.C521_QTY QTY, '' CNTRLNUM ");
        sbQuery
            .append(" FROM T520_REQUEST T520, T521_REQUEST_DETAIL T521, T207_SET_MASTER T207, T205_PART_NUMBER T205");// ,
                                                                                                                      // T504_CONSIGNMENT
                                                                                                                      // T504
                                                                                                                      // ");
        sbQuery.append(" WHERE T520.C520_VOID_FL IS NULL ");
        // sbQuery.append(" AND T520.C520_REQUEST_ID = T504.C520_REQUEST_ID (+) ");
        sbQuery.append(" AND T205.C205_PART_NUMBER_ID = T521.C205_PART_NUMBER_ID ");
        sbQuery.append(" AND T520.C520_REQUEST_ID = T521.C520_REQUEST_ID ");
        sbQuery.append(" AND T207.C207_SET_ID = T520.C207_SET_ID ");
        // Fetch records based company/plant for EDC countries
        sbQuery.append(" AND ( T520.C1900_COMPANY_ID = " + strCompPlantFilter);
        sbQuery.append(" OR T520.C5040_PLANT_ID = " + strCompPlantFilter);
        sbQuery.append(") ");
        sbQuery.append(" AND T520.C207_SET_ID IS NOT NULL ");
        sbQuery.append(" AND T520.C520_DELETE_FL IS NULL ");
        sbQuery.append(getFilterConditions(hmParam));
        if (!partNumRegexp.equals(""))      
        	 sbQuery.append(" AND REGEXP_LIKE (T521.C205_PART_NUMBER_ID, REGEXP_REPLACE('" + partNumRegexp + "','[+]','\\+'))");
        sbQuery.append(" UNION ALL ");
      }
      sbQuery.append(" SELECT t520.C520_REQUEST_ID REQID, ");
      sbQuery.append(" T520.C207_SET_ID SETID, GET_SET_NAME(T520.C207_SET_ID) SETNAME, ");
      sbQuery.append(" T504.C504_CONSIGNMENT_ID CONID, ");
      sbQuery
          .append(" DECODE (T520.C520_STATUS_FL, 10, 'Back Order', 15, 'Back Log', 20, 'Ready To Consign') REQSTATUS, ");
      sbQuery
          .append(" DECODE (T504.C504_STATUS_FL, 0, 'Initiated', 1, 'WIP', 2, 'Pending Verification') CONSTATUS, ");
      sbQuery.append(" T505.C205_PART_NUMBER_ID PNUM, ");
      sbQuery.append(" T205.C205_PART_NUM_DESC PDESC, ");
      sbQuery.append(" T505.C505_ITEM_QTY QTY, T505.C505_CONTROL_NUMBER CNTRLNUM ");
      sbQuery
          .append(" FROM T520_REQUEST T520, T504_CONSIGNMENT T504, T505_ITEM_CONSIGNMENT T505, T207_SET_MASTER T207, T205_PART_NUMBER T205 ");
      sbQuery.append(" WHERE T520.C520_REQUEST_ID = T504.C520_REQUEST_ID (+) ");
      sbQuery.append(" AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID ");
      sbQuery.append(" AND T207.C207_SET_ID = T520.C207_SET_ID ");
      // Fetch records based company/plant for EDC countries
      sbQuery.append(" AND ( T520.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbQuery.append(" OR T520.C5040_PLANT_ID = " + strCompPlantFilter);
      sbQuery.append(") ");
      sbQuery.append(" AND T520.C207_SET_ID IS NOT NULL ");
      sbQuery.append(" AND T520.C520_VOID_FL IS NULL ");
      sbQuery.append(" AND T520.C520_DELETE_FL IS NULL ");
      sbQuery.append(" AND T205.C205_PART_NUMBER_ID = T505.C205_PART_NUMBER_ID ");
      sbQuery.append(getFilterConditions(hmParam));

      if (!strCheckSelectedCon.equals("")) {
        sbQuery.append(" AND T504.C504_STATUS_FL IN (");
        sbQuery.append(" '" + GmCommonClass.getStringWithQuotes(strCheckSelectedCon) + "'");
        sbQuery.append(")");
      }



      if (!partNumRegexp.equals(""))
    	  sbQuery.append(" AND REGEXP_LIKE (T505.C205_PART_NUMBER_ID, REGEXP_REPLACE('" + partNumRegexp + "','[+]','\\+'))");
      
      if (!stRControlNum.equals("")) {
        sbQuery.append(" AND T505.C505_CONTROL_NUMBER ");
        if (strControlNumOper.equals(Integer.toString(GmSearchCriteria.NOTEQUALS))) {
          sbQuery.append(" <> '");
        } else {
          sbQuery.append(" LIKE '");
        }
        sbQuery.append(stRControlNum);
        sbQuery.append("'");
      }
      sbQuery.append(") ");
      if (!stRControlNum.equals("")) {
        sbQuery.append(" WHERE CNTRLNUM ");
        if (strControlNumOper.equals(Integer.toString(GmSearchCriteria.NOTEQUALS))) {
          sbQuery.append(" <> '");
        } else {
          sbQuery.append(" LIKE '");
        }
        sbQuery.append(stRControlNum);
        sbQuery.append("'");
      }
      sbQuery.append(getGroupByStatement(summaryVer));
      gmDBManager.setPrepareString(sbQuery.toString());
      log.debug(" Query for loadRequestMasterReport " + sbQuery.toString());
      rdResult = gmDBManager.queryDisplayTagRecord();
    }

    catch (Exception e) {
      GmLogError.log("Exception in GmRequestReportBean:loadRequestMasterReport", "Exception is:"
          + e);
      throw new AppError(e);
    }
    return rdResult;
  }

  private String getGroupByStatement(boolean summaryFlag) {
    StringBuffer sbQuery = new StringBuffer();
    if (summaryFlag) {
      sbQuery.append(" GROUP BY  SETID, SETNAME, REQSTATUS, CONSTATUS, PNUM, PDESC");
    } else {
      sbQuery
          .append(" GROUP BY REQID, SETID, SETNAME, CONID, REQSTATUS, CONSTATUS, PNUM, PDESC, QTY, CNTRLNUM");
    }


    return sbQuery.toString();
  }

  private String getSelectStatement(boolean summaryFlag) {
    StringBuffer sbQuery = new StringBuffer();
    if (summaryFlag) {
      sbQuery.append(" SELECT  SETID, SETNAME, REQSTATUS, CONSTATUS, PNUM, PDESC, SUM(QTY) QTY");
    } else {
      sbQuery
          .append(" SELECT REQID, SETID, SETNAME, CONID, REQSTATUS, CONSTATUS, PNUM, PDESC, QTY, CNTRLNUM");
    }


    return sbQuery.toString();
  }

  private String getFilterConditions(HashMap hmParam) {
    StringBuffer sbQuery = new StringBuffer();
    String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strCheckSelectedReq = GmCommonClass.parseNull((String) hmParam.get("INPUTREQ"));
    String strProjects = GmCommonClass.parseNull((String) hmParam.get("PROJECT"));
    String strConsignID = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));

    if (!strCheckSelectedReq.equals("")) {
      sbQuery.append(" AND t520.c520_status_fl IN (");
      sbQuery.append(" '" + GmCommonClass.getStringWithQuotes(strCheckSelectedReq) + "'");
      sbQuery.append(")");
    }
    if (!strConsignID.equals("")) {
      sbQuery
          .append(" AND T520.C520_REQUEST_ID IN (SELECT C520_REQUEST_ID FROM T504_CONSIGNMENT WHERE C504_CONSIGNMENT_ID ='");
      sbQuery.append(strConsignID);
      sbQuery.append("')");
      // sbQuery.append(" AND t504.C504_CONSIGNMENT_ID IN (");
      // sbQuery.append(" '" + GmCommonClass.getStringWithQuotes(strConsignID) + "'");
      // sbQuery.append(")");
    }
    if (!strReqId.equals("")) {
      sbQuery.append(" AND t520.C520_REQUEST_ID IN (");
      sbQuery.append(" '" + GmCommonClass.getStringWithQuotes(strReqId) + "'");
      sbQuery.append(")");
    }

    if (!strSetID.equals("")) {
      sbQuery.append(" AND t520.c207_set_id IN (");
      sbQuery.append(" '" + GmCommonClass.getStringWithQuotes(strSetID) + "'");
      sbQuery.append(")");
    }

    if (!strProjects.equals("0")) {
      sbQuery
          .append(" AND T207.C202_PROJECT_ID IN (SELECT C202_PROJECT_ID FROM T202_PROJECT WHERE C202_PROJECT_ID = '");
      sbQuery.append(strProjects);
      sbQuery.append("') ");
    }
    return sbQuery.toString();
  }


  /**
   * reportDSSummary - This Method is used to get crosstab details for Set Overview
   * 
   * @param hmPara
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadSetOverview(HashMap hmParam) throws AppError {

    // ////log.debug(" values to fetch sets  " + hmParam);

    HashMap hmReturn = new HashMap();
    HashMap hmLinktoStatus = new HashMap();
    HashMap hmLinktoLoaner = new HashMap();
    HashMap hmLinktoAvgNet = new HashMap();
    HashMap hmLinktoBuild = new HashMap();
    HashMap hmReturnStatus = new HashMap();
    HashMap hmLinktoOwe = new HashMap();
    HashMap hmShipOutInfo = new HashMap();
    HashMap hmSetPar = new HashMap();
    HashMap hmLinktoShipOutHistory = new HashMap();

    ArrayList alStatusDetails = new ArrayList();
    ArrayList alLoanerDetails = new ArrayList();
    ArrayList alNetDetails = new ArrayList();
    ArrayList alSetPARDetails = new ArrayList();
    ArrayList alBuildDetails = new ArrayList();
    ArrayList alOweDetails = new ArrayList();
    ArrayList alSetShipOutHistoryDetails = new ArrayList();
    ResultSet rsHeader = null;
    ResultSet rsDetails = null;
    ResultSet rsStatusHeader = null;
    ResultSet rsStatusDetails = null;
    ResultSet rsShipOutHeader = null;
    ResultSet rsShipOutDetails = null;
    GmCrossTabReport gmCrossReport = new GmCrossTabReport(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("INPUTSETS"));

    String strForeCastMonths = GmCommonClass.parseNull((String) hmParam.get("FORECASTPERIOD"));

    // If no sheet id then return null
    if (strSetId.equals("")) {
      return hmReturn;
    }

    log.debug("Set ID...." + strSetId);
    // To fetch the transaction information
    gmDBManager.setPrepareString("gm_pkg_op_backorder_rpt.gm_fch_set_overview ", 12);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(12, OracleTypes.CURSOR);

    gmDBManager.setString(1, strSetId);
    gmDBManager.setString(2, strForeCastMonths);
    gmDBManager.execute();

    rsHeader = (ResultSet) gmDBManager.getObject(3);
    rsDetails = (ResultSet) gmDBManager.getObject(4);
    rsStatusHeader = (ResultSet) gmDBManager.getObject(5);
    rsStatusDetails = (ResultSet) gmDBManager.getObject(6);
    alLoanerDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    alSetPARDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
    alBuildDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(9));
    rsShipOutHeader = (ResultSet) gmDBManager.getObject(10);
    rsShipOutDetails = (ResultSet) gmDBManager.getObject(11);
    alOweDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(12));

    hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader, rsDetails);
    hmReturnStatus = gmCrossReport.GenerateCrossTabReport(rsStatusHeader, rsStatusDetails);
    hmShipOutInfo = gmCrossReport.GenerateCrossTabReport(rsShipOutHeader, rsShipOutDetails);

    gmDBManager.close();

    alStatusDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturnStatus.get("Details"));
    alSetShipOutHistoryDetails =
        GmCommonClass.parseNullArrayList((ArrayList) hmShipOutInfo.get("Details"));

    hmLinktoStatus = getLinkInfo("Status");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alStatusDetails, hmLinktoStatus);
    hmLinktoShipOutHistory = getLinkInfo("ShipOutHistory");
    hmReturn =
        gmCrossReport.linkCrossTab(hmReturn, alSetShipOutHistoryDetails, hmLinktoShipOutHistory);
    hmSetPar = getLinkInfo("SetPar");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alSetPARDetails, hmSetPar);
    hmLinktoLoaner = getLinkInfo("Loaner");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alLoanerDetails, hmLinktoLoaner);
    hmLinktoOwe = getLinkInfo("Owe");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alOweDetails, hmLinktoOwe);
    log.debug("Owe....." + hmReturn);
    hmLinktoBuild = getLinkInfo("Build");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alBuildDetails, hmLinktoBuild);

    return hmReturn;
  }


  /**
   * getLinkInfo - This Method is used to to get the link object information
   * 
   * @return HashMap
   * @exception AppError
   **/
  private HashMap getLinkInfo(String strType) {
    HashMap hmLinkDetails = new HashMap();
    HashMap hmapValue = new HashMap();
    ArrayList alLinkList = new ArrayList();

    // First parameter holds the link object name
    hmLinkDetails.put("LINKID", "ID");

    SimpleDateFormat formatMonth = new SimpleDateFormat("MMM");
    SimpleDateFormat formatYear = new SimpleDateFormat("yy");

    if (strType.equals("Status")) {
      // Parameter to be added to link list
      /*
       * hmapValue.put("KEY","Initiated"); hmapValue.put("VALUE", "Initiated");
       * alLinkList.add(hmapValue);
       */
      hmapValue = new HashMap();
      hmapValue.put("KEY", "WIP");
      hmapValue.put("VALUE", "WIP");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "Pending Verification");
      hmapValue.put("VALUE", "Pending Verification");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "Ready To Consign");
      hmapValue.put("VALUE", "Ready To Consign");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "Pending Shipping");
      hmapValue.put("VALUE", "Pending Shipping");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "Shipped");
      hmapValue.put("VALUE", "Shipped");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "BEFORE");
    }

    else if (strType.equals("Loaner")) {
      // Parameter to be added to link list
      hmapValue.put("KEY", "INHOUSE_LOANER");
      hmapValue.put("VALUE", "Inhouse Loaner");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PRODUCT_LOANER");
      hmapValue.put("VALUE", "Product Loaner");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "BEFORE");
    }

    else if (strType.equals("AvgNet")) {
      // Parameter to be added to link list
      hmapValue = new HashMap();
      hmapValue.put("KEY", "AVG_NET");
      hmapValue.put("VALUE", "Avg Net");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "BEFORE");
    }

    else if (strType.equals("ShipOutHistory")) {
      SimpleDateFormat formatHistoryMonth = new SimpleDateFormat("MM");
      SimpleDateFormat formatHistoryYear = new SimpleDateFormat("yyyy");
      SimpleDateFormat formatHistoryMonthVal = new SimpleDateFormat("MMM");
      SimpleDateFormat formatHistoryYearVal = new SimpleDateFormat("yy");

      hmapValue = new HashMap();
      hmapValue.put("KEY", "01/2003");
      hmapValue.put("VALUE", "From 2003");
      alLinkList.add(hmapValue);

      for (int i = 3; i >= 1; i--) {
        hmapValue = new HashMap();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -i);
        hmapValue.put("KEY",
            formatHistoryMonth.format(c.getTime()) + "/" + formatHistoryYear.format(c.getTime()));
        hmapValue.put("VALUE", formatHistoryMonthVal.format(c.getTime()) + "-"
            + formatHistoryYearVal.format(c.getTime()));
        alLinkList.add(hmapValue);
      }
      hmLinkDetails.put("POSITION", "BEFORE");
    } else if (strType.equals("SetPar")) {
      hmapValue = new HashMap();
      hmapValue.put("KEY", "SET_PAR");
      hmapValue.put("VALUE", "Set PAR");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    } else if (strType.equals("Owe")) {
      // Parameter to be added to link list
      hmapValue = new HashMap();
      hmapValue.put("KEY", "OWE");
      hmapValue.put("VALUE", "Owe");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    } else if (strType.equals("Build")) {
      // Parameter to be added to link list
      hmapValue = new HashMap();
      hmapValue.put("KEY", "YET_TO_BUILD");
      hmapValue.put("VALUE", "Yet To Build");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    }

    else if (strType.equals("Part")) {
      // Parameter to be added to link list
      /*
       * hmapValue.put("KEY","ID"); hmapValue.put("VALUE", "Part Number");
       * alLinkList.add(hmapValue);
       */
      hmapValue = new HashMap();
      hmapValue.put("KEY", "PDESC");
      hmapValue.put("VALUE", "Description");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "BEFORE");
    }

    else if (strType.equals("Demand")) {
      // Parameter to be added to link list
      Calendar c = null;

      for (int i = 3; i >= 1; i--) {
        hmapValue = new HashMap();
        c = Calendar.getInstance();

        c.add(Calendar.MONTH, -i);
        hmapValue
            .put("KEY", formatMonth.format(c.getTime()) + " " + formatYear.format(c.getTime()));
        hmapValue.put("VALUE",
            formatMonth.format(c.getTime()) + " " + formatYear.format(c.getTime()));
        alLinkList.add(hmapValue);
      }

      hmLinkDetails.put("POSITION", "BEFORE");
    }

    else if (strType.equals("BackOrder")) {
      // Parameter to be added to link list
      hmapValue = new HashMap();
      hmapValue.put("KEY", "BO_QTY");
      hmapValue.put("VALUE", "BO");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    }

    else if (strType.equals("ShelfPO")) {
      // Parameter to be added to link list
      hmapValue = new HashMap();
      hmapValue.put("KEY", "SHELF");
      hmapValue.put("VALUE", "Shelf");
      alLinkList.add(hmapValue);


      hmapValue = new HashMap();
      hmapValue.put("KEY", "DHR");
      hmapValue.put("VALUE", "On DHR");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "RW_QTY");
      hmapValue.put("VALUE", "RW QTY");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "OTHER");
      hmapValue.put("VALUE", "OTHER");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "PO");
      hmapValue.put("VALUE", "On PO");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    }

    else if (strType.equals("Total")) {
      // Parameter to be added to link list
      hmapValue = new HashMap();
      hmapValue.put("KEY", "TOTALDEMAND");
      hmapValue.put("VALUE", "Total Demand");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    }

    else if (strType.equals("Avg")) {
      // Parameter to be added to link list
      hmapValue = new HashMap();
      hmapValue.put("KEY", "AVERAGE");
      hmapValue.put("VALUE", "Average");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    }

    else if (strType.equals("Trend")) {
      // Parameter to be added to link list
      Calendar c = null;

      for (int i = 0; i <= 2; i++) {
        hmapValue = new HashMap();
        c = Calendar.getInstance();

        c.add(Calendar.MONTH, i);
        hmapValue.put("KEY",
            formatMonth.format(c.getTime()) + " - " + formatYear.format(c.getTime()));
        hmapValue.put("VALUE",
            formatMonth.format(c.getTime()) + " - " + formatYear.format(c.getTime()));
        alLinkList.add(hmapValue);
      }

      hmLinkDetails.put("POSITION", "AFTER");
    } else if (strType.equals("OUSAVG")) {

      hmapValue = new HashMap();
      hmapValue.put("KEY", "US_AVERAGE");
      hmapValue.put("VALUE", "US Average");
      alLinkList.add(hmapValue);

      hmapValue = new HashMap();
      hmapValue.put("KEY", "OUS_AVERAGE");
      hmapValue.put("VALUE", "OUS Average");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    } else if (strType.equals("Current")) {
      // Parameter to be added to link list
      Calendar c = null;

      hmapValue = new HashMap();
      c = Calendar.getInstance();

      hmapValue.put("KEY", "Current");
      hmapValue.put("VALUE", "Current");
      alLinkList.add(hmapValue);

      hmLinkDetails.put("POSITION", "AFTER");
    }

    hmLinkDetails.put("LINKVALUE", alLinkList);
    return hmLinkDetails;
  }

  public RowSetDynaClass loadBODHRReoprt(String tokeni) throws AppError {
    return loadBODHRReoprt(tokeni, "");
  }

  public RowSetDynaClass loadBODHRReoprt(String tokeni, String strDHRID) throws AppError {
    log.debug("Enter" + tokeni + " : strDHRID :" + strDHRID);
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr_split.gm_fch_dhr_split_dtls", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull(tokeni));
    gmDBManager.setString(2, GmCommonClass.parseNull(strDHRID));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    // //log.debug("Exit");
    return rdResult;
  }

  /*
   * public RowSetDynaClass loadBODHRReoprt(String tokeni, String strDHRID) throws AppError{
   * log.debug("Enter"+tokeni+" : strDHRID :"+strDHRID); RowSetDynaClass rdResult = null;
   * GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
   * gmDBManager.setPrepareString("gm_pkg_op_backorder_rpt.GM_FCH_BO_DHR", 2);
   * gmDBManager.setString(1,GmCommonClass.parseNull(tokeni)); gmDBManager.registerOutParameter(2,
   * OracleTypes.CURSOR); gmDBManager.execute(); rdResult =
   * gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2)); gmDBManager.close();
   * ////log.debug("Exit"); return rdResult ; }
   */
  public ArrayList loadPartFulfillReport(HashMap hmParam) throws AppError {
    // //log.debug("Enter");
    ArrayList alResult = new ArrayList();
    ArrayList alTempResult = new ArrayList();
    ArrayList alDBResult = new ArrayList();
    String strPartNumInputString = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strSetString = GmCommonClass.parseNull((String) hmParam.get("SETSTRING"));
    String strPartString = GmCommonClass.parseNull((String) hmParam.get("PARTSTRING"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("CHECKTYPE"));
    // To fetch the transaction information
    log.debug("strSetString:" + strSetString);
    log.debug("strPartString:" + strPartString);
    log.debug("strType: " + strType);
    // strInputString="S,121.111|S,121.112|S,121.113|S,123.115|S,123.116|S,192.876|S,835.901|S,835.902|S,900.001|S,900.002|S,900.003|S,901.888|S,901.901|S,901.902|S,901.903|S,901.904|S,901.905|S,901.906|S,901.907|S,902.901|S,902.902|S,902.903|S,902.904|S,902.905|S,902.906|S,902.907|S,902.908|S,904.901|S,904.902|S,904.903|S,904.905|S,904.906|S,904.907|S,904.908|S,904.909|S,904.910|S,906.901|S,906.991|S,907.902|S,907.903|S,908.901|S,908.902|S,908.903|S,910.900|S,910.901|S,910.901BM|S,910.901DM|S,910.901TR|S,910.902|S,910.904|S,910.905|S,910.906|S,910.907|S,910.908|S,910.950|S,910.951|S,912.901|S,913.901|S,914.901|S,914.910|S,915.901|S,915.901BM|S,915.901DM|S,915.901TR|S,915.902|S,915.908|S,916.901|S,916.902|S,916.903|S,916.904|S,916.905|S,916.906|S,916.907|S,916.908|S,916.909|S,916.910|S,916.911|S,916.915|S,916.916|S,916.917|S,917.902|S,917.902BM|S,917.904|S,922.901|S,922.902|S,922.903|S,923.901|S,923.901DE|S,923.901TG|S,923.902|S,923.903|S,923.904|S,924.901|S,924.902|S,924.903|S,924.904|S,924.905|S,924.906|S,924.907|S,924.909|S,924.910|S,924.911|S,924.912|S,924.913BM|S,924.914DE|S,924.915|S,924.916|S,924.917|S,924.918|S,924.919|S,925.900|S,926.900|S,927.901|S,927.902|S,927.903|S,928.901|S,928.902|S,928.903|S,929.900DE|S,929.901|S,929.901BM|S,929.902|S,929.902TG|S,929.903|S,929.906|S,930.900DM|S,930.901|S,930.901BM|S,930.902|S,930.902TG|S,931.905|S,931.906|S,931.907|S,931.908BM|S,932.901|S,932.902|S,932.903|S,936.901|S,936.902|S,942.901|S,946.901|S,946.902|S,948.901|S,948.903|S,950.900|S,950.900BM|S,950.900DM|S,950.900TR|S,950.902|S,950.904|S,950.906|S,952.901|S,952.902|S,954.901|S,954.902|S,955.901|S,955.902|S,955.903|S,955.904|S,955.905|S,957.901|S,964.901|S,964.902|S,964.903|S,964.904|S,964.905|S,964.906|S,965.901|S,965.902|S,965.903|S,965.905|S,971.901|S,971.902|S,972.901|S,972.902|S,979.901|S,980.901|S,MER.901|S,MER.902|S,MER.903|S,MER.904|S,MER.905|S,MER.906|S,MER.907|S,MER.908|S,MER.909|S,MER.910|S,MER.911|S,MER.913|S,MER.914|S,MER.915|S,MER.916|S,MER.917|S,SET.1001|S,SET.1002|S,SET.1004|S,SET.1015|S,SET.1019|";
    if (strPartString != null && strPartString.length() > 4000) {
      /** In case if the number of part numbers exceeds 4000, the below function is called **/
      alTempResult = GmCommonClass.convertStringToList(strPartString, 4000, "|");
    } else {
      alTempResult = new ArrayList();
      alTempResult.add(strPartString);
    }

    if (alTempResult != null) {
      for (Iterator itr = alTempResult.iterator(); itr.hasNext();) {
        strPartNumInputString = (String) itr.next();
        gmDBManager.setPrepareString("gm_pkg_op_backorder_rpt.gm_fch_part_fulfill", 4);
        gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
        gmDBManager.setString(1, strSetString);
        gmDBManager.setString(2, strPartNumInputString);
        gmDBManager.setString(3, strType);
        gmDBManager.execute();
        alDBResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
        alResult.addAll(alDBResult);
      }
    }
    gmDBManager.close();
    return alResult;
  }

  /**
   * loadBackOrderReport - This method returns the datat for the backorder report
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadBackOrderReport(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("RPTOPT"));
    String strAging = GmCommonClass.parseNull((String) hmParam.get("AGTYPE"));
    String strFilters = GmCommonClass.parseNull((String) hmParam.get("STRFILTER"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strTypeVal = GmCommonClass.parseNull((String) hmParam.get("Cbo_Type_Val"));
    String strSource = GmCommonClass.parseNull((String) hmParam.get("Cbo_Source_Type"));
    String strTagId = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    log.debug("strOpt from loadBackOrderReport : " + strOpt);
    getCrossTabFilterCondition(hmParam);
    /* Adding single quotation in the set id to separate the set id with comma and quotes to include multiple set id in the where clause
    Currently for multiple set id  selection query displaying the where clause as '300.154,300.001'
    After replace function call the setid condition will 300.154','300.001
    starting and ending quotation already added in respective methods below.
    */
    strSetId = strSetId.replace(",", "','");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    if (strOpt.equals("50261")) // SALES
    {
      sbQuery.append(getSalesBackOrderSQL(strAging, strCondition));
    } else if (strOpt.equals("50262") || strOpt.equals("50263")) // SHIPSETSBACKORDER ||
                                                                 // ITEMSBACKORDER
    {
      sbQuery.append(getShipSetsOrItemsBoSQL(strOpt, strAging, strSetId, strTypeVal, strSource,
          strTagId));
    } else if (strOpt.equals("50264")) // SETS
    {
      sbQuery.append(getSetsBoSQL(strAging, strSetId));
    } else if (strOpt.equals("50265")) // LOANERS
    {
      sbQuery.append(getLoanersBoSQL(strAging, strSetId));

    } else if (strOpt.equals("50269")) // IH LOANERS
    {
      sbQuery.append(getIHLoanersBoSQL());

    } else if (strOpt.equals("PRCCHK")) {
      String strConsignId = (String) hmParam.get("CONSIGNID");
      sbQuery.append(getItemLoanersBoSQL(strAging) + " AND C412_REF_ID='" + strConsignId + "' ");
    } else if (strOpt.equals("50266")) // LOANER Items
    {
      sbQuery.append(getLoanerItemBoSQL(strAging, strFilters, strSetId, strTagId, strRefId));
    }

    log.debug("Query for BO report : " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of loadBackOrderReport

  private String getLoanerItemBoSQL(String strAgingType, String strFilters, String strSetId,
      String strTagId, String strRefId) {
    StringBuffer sbQuery = new StringBuffer();
    String strCompDateFmt=getCompDateFmt();
    sbQuery.append(" SELECT * FROM ( ");
    sbQuery.append(" select t412.C412_INHOUSE_TRANS_ID ID, C412_REF_ID MASTER_ID,");
    sbQuery
        .append(" t413.c205_part_number_id NUM ,get_partnum_desc(t413.c205_part_number_id) DSC , t413.c413_item_qty QTY");
    sbQuery.append(" ,GET_PARTNUMBER_PO_PEND_QTY (t413.C205_PART_NUMBER_ID) POPEND ");
    sbQuery.append(" ,t412.c412_created_date DT,TO_CHAR(t412.C412_CREATED_DATE,'"+strCompDateFmt.concat(" HH:MI:SS") +"') TRANSDT");
    sbQuery.append(" , 0 PRICE, 'N' LOG_FLAG");

   sbQuery
       .append(" ,decode(t504a.c504a_return_dt,null,decode(C901_CONSIGNED_TO,'50170', get_distributor_name(C504A_CONSIGNED_TO_ID),'')|| '/' || GET_REP_NAME(t504a.c703_sales_rep_id),'')REPDISTNM ");
   sbQuery
      .append(" ,decode(t504a.c504a_return_dt,null, get_account_name(t504a.c704_account_id),'')ANAME , t412.c412_type  txntype");
  
    sbQuery.append(" ,gm_pkg_op_consignment.get_set_id (t412.c412_ref_id) setid ");
    sbQuery
        .append(" FROM T412_INHOUSE_TRANSACTIONS t412 , t413_inhouse_trans_items t413, t504a_loaner_transaction t504a");
    if (!strFilters.equals("Y"))
      sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id");
    sbQuery
        .append(" and t412.c504a_loaner_transaction_id = t504a.c504a_loaner_transaction_id  (+) ");
    sbQuery.append(" and C412_STATUS_FL = 0");
    sbQuery.append(" AND t412.c412_type = 100062");
    if (!strFilters.equals("Y")) {
      sbQuery.append(" AND t504a.c901_consigned_to IN (50170)"); // 50170 Distributor
      sbQuery.append(" AND t504a.c504a_consigned_to_id = V700.D_ID ");
    }
    sbQuery.append(" AND t412.C5040_PLANT_ID =" + getCompPlantId());
    sbQuery.append(" AND t412.C5040_PLANT_ID = t504a.C5040_PLANT_ID (+) ");

    if (!strTagId.equals("")) {
      sbQuery
          .append("AND t412.C412_REF_ID in (SELECT c5010_last_updated_trans_id from t5010_tag where c5010_tag_id='"
              + strTagId + "') ");

    } else if (!strRefId.equals("")) {
      sbQuery.append(" and C412_REF_ID = '" + strRefId + "'");
    }

    if (strAgingType.equals("14")) {
      sbQuery
          .append(" AND trunc(t412.c412_created_date) BETWEEN trunc(CURRENT_DATE - 14) and trunc(CURRENT_DATE) ");
    } else if (strAgingType.equals("21")) {
      sbQuery
          .append(" AND trunc(t412.c412_created_date) BETWEEN trunc(CURRENT_DATE - 21) and trunc(CURRENT_DATE-15) ");
    } else if (strAgingType.equals("21x")) {
      sbQuery.append(" AND trunc(t412.c412_created_date) < trunc(CURRENT_DATE-21) ");
    }
    sbQuery
        .append(" and t413.C413_VOID_FL is null AND C412_VOID_FL  IS NULL order by t413.c205_part_number_id ) t412");
    if (!strSetId.equals("")) {
      sbQuery.append(" WHERE t412.setid IN (SELECT v207a.c207_actual_set_id ");
      sbQuery.append(" FROM v207a_set_consign_link v207a ");
      sbQuery.append(" WHERE v207a.C207_SET_ID IN ('" + strSetId + "')) ");
    }
    return sbQuery.toString();

  }

  /**
   * getSalesBackOrderSQL - This Method is used to get back order details
   * 
   **/
  private String getSalesBackOrderSQL(String strAgingType, String strCondition) {
    StringBuffer sbQuery = new StringBuffer();
    String strCompDateFmt=getCompDateFmt();
    String strCompanyId = getGmDataStoreVO().getCmpid();
    // To fetch the records for EDC entity countries based on plant
    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "ORDFILTER"));
    String strCompPlantFilter =
        strFilter.equals("") ? strCompanyId : getGmDataStoreVO().getPlantid();
    sbQuery.append(" SELECT	T501.C501_ORDER_ID ID, GET_ACCOUNT_NAME(T501.C704_ACCOUNT_ID) ANAME,");
    sbQuery.append(" GET_DIST_REP_NAME(T501.C703_SALES_REP_ID) REPDISTNM, ");
    sbQuery
        .append("TO_CHAR(T501.C501_CREATED_DATE,'"+strCompDateFmt.concat(" HH:MI:SS") +"') TRANSDT, T501.C501_CUSTOMER_PO PO, GET_BILL_ADD(T501.C704_ACCOUNT_ID) BILLADD, ");
    sbQuery.append(" T501.C501_TOTAL_COST + NVL(T501.C501_SHIP_COST,0) SALES, '' SHELFQTY, ");
    sbQuery.append(" T501.C501_COMMENTS COMMENTS, GET_USER_NAME(T501.C501_CREATED_BY) UNAME, ");
    sbQuery.append(" GET_PARTNUMBER_PO_PEND_QTY (T502.C205_PART_NUMBER_ID) POPEND, ");
    sbQuery
        .append(" T501.C501_SHIP_TO SHIPTO, T501.C501_SHIP_TO_ID SHIPTOID, T501.C704_ACCOUNT_ID ACCID, ");
    sbQuery
        .append(" T501.C501_PARENT_ORDER_ID MASTER_ID, T502.C205_PART_NUMBER_ID NUM, T502.C502_ITEM_QTY QTY, T502.C502_ITEM_PRICE PRICE");
    sbQuery
        .append(" ,get_partnum_desc(T502.C205_PART_NUMBER_ID) DSC, GET_LOG_FLAG(T501.C501_ORDER_ID,1233) LOG_FLAG, GET_LOG_FLAG(T501.C501_ORDER_ID,1234) INT_LOG_FLAG ");
    sbQuery.append(", get_account_curr_symb (t501.c704_account_id) acc_currency ");
    sbQuery.append(" FROM T501_ORDER T501, T502_ITEM_ORDER T502 ");
    // sbQuery.append(getAccessFilterClauseWithRepID());
    sbQuery
        .append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID  AND (T501.c901_ext_country_id is NULL ");
    sbQuery
        .append(" OR T501.c901_ext_country_id  in (select country_id from v901_country_codes)) AND T501.C901_ORDER_TYPE = ");
    sbQuery.append(2525);
    sbQuery
        .append(" AND T501.C501_DELETE_FL IS NULL AND T501.C501_VOID_FL IS NULL AND T501.C501_STATUS_FL = 0");
    sbQuery.append("  AND T502.C502_VOID_FL IS NULL ");
    // sbQuery.append(" AND V700.REP_ID = T501.C703_SALES_REP_ID ");
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    if (strAgingType.equals("14")) {
      sbQuery
          .append(" AND trunc(T501.C501_ORDER_DATE) BETWEEN trunc(CURRENT_DATE - 14) and trunc(CURRENT_DATE) ");
    } else if (strAgingType.equals("21")) {
      sbQuery
          .append(" AND trunc(T501.C501_ORDER_DATE) BETWEEN trunc(CURRENT_DATE - 21) and trunc(CURRENT_DATE-15) ");
    } else if (strAgingType.equals("21x")) {
      sbQuery.append(" AND trunc(T501.C501_ORDER_DATE) < trunc(CURRENT_DATE-21) ");
    }
    // Added CompanyId and PlantId Filter
    sbQuery.append(" AND  (T501.C1900_COMPANY_ID =" + strCompPlantFilter);
    sbQuery.append(" OR  T501.C5040_PLANT_ID =" + strCompPlantFilter);
    sbQuery.append(" ) ORDER BY T502.C205_PART_NUMBER_ID, T501.C501_ORDER_DATE ");
    return sbQuery.toString();
  }

  private String getShipSetsOrItemsBoSQL(String strOpt, String strAgingType, String strSetId,
      String strTypeVal, String strSource, String strTagId) {
    String strFilter = "";
    String strRuleGroupId = "";
    String strDistID="";
    strRuleGroupId = strOpt.equals("50263") ? "ITEMS_BO_RPT" : "ITEMS_FRM_SHPSETS"; // 50263-ITEMSBACKORDER
                                                                                    // & 50262
                                                                                    // -SHIPSETSBACKORDER
    strFilter = GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), strRuleGroupId));// ITEMS_BO_RPT
                                                                                                 // &&
                                                                                                 // ITEMS_FRM_SHPSETS
                                                                                                 // -Rule
                                                                                                 // Value
                                                                                                 // :=
                                                                                                 // plant
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();
    
    strDistID = GmCommonClass.parseNull(GmCommonClass.getRuleValue("DISTRIBUTOR","TAGST"));  // Get Distributor ID
    StringBuffer sbQuery = new StringBuffer();
    String strCompDateFmt = getCompDateFmt(); 
    if (strOpt.equals("50263")) { // Items; code added to include account item consignment
      sbQuery
          .append(" SELECT ID, MASTER_ID, ANAME, REPDISTNM, NUM, QTY, DSC , POPEND , DT, TRANSDT, LOG_FLAG, INT_LOG_FLAG");
      sbQuery.append(" FROM(");
    }

    sbQuery
        .append(" select T520.C520_REQUEST_ID ID, T520.C520_MASTER_REQUEST_ID MASTER_ID, '' ANAME, V700.D_NAME REPDISTNM,");
    sbQuery
        .append(" T521.C205_PART_NUMBER_ID NUM, T521.C521_QTY QTY, get_partnum_desc(T521.C205_PART_NUMBER_ID) DSC ");
    sbQuery.append(" ,GET_PARTNUMBER_PO_PEND_QTY (T521.C205_PART_NUMBER_ID) POPEND ");
    sbQuery
        .append(" ,T520.C520_REQUIRED_DATE DT,TO_CHAR(T520.C520_CREATED_DATE,'"+strCompDateFmt.concat(" HH:MI:SS") +"') TRANSDT, GET_LOG_FLAG(T520.C520_REQUEST_ID,1233) LOG_FLAG, GET_LOG_FLAG(T520.C520_REQUEST_ID,1234) INT_LOG_FLAG ");
    sbQuery.append(" from T520_REQUEST T520, T521_REQUEST_DETAIL T521 ");
    if (strOpt.equals("50262")){
    	sbQuery.append(getAccessFilterClause().replace(" ) V700 ", " "));
    	sbQuery.append(" UNION SELECT null ad_id, null ad_name, null region_id, null region_name,  '"+strDistID+"' d_id, 'Missing' d_name,'Missing' d_name_en, null vp_id, null vp_name, null d_active_fl, null d_compid FROM dual ");
    	sbQuery.append(" ) V700 ");
    }
    else{
    	sbQuery.append(getAccessFilterClause());
    }
    
    sbQuery.append(" where T520.C520_REQUEST_ID = T521.C520_REQUEST_ID ");
    sbQuery.append(" and T520.C520_STATUS_FL = 10 ");
    sbQuery.append(" and T520.C520_REQUEST_TO is not null ");
    sbQuery.append(" and T520.C520_VOID_FL is null ");
    sbQuery.append(" AND T520.C520_REQUEST_TO = V700.D_ID ");
    sbQuery.append(" AND ( T520.C1900_COMPANY_ID = " + strFilter);
    sbQuery.append(" OR T520.C5040_PLANT_ID = " + strFilter);
    sbQuery.append(") ");

    if (strOpt.equals("50262")) // Sets
    {
      if (!strTagId.equals("")) {
        sbQuery
            .append(" AND T520.C520_MASTER_REQUEST_ID IN (select t504.c520_request_id from t504_consignment t504,t5010_tag t5010 ");
        sbQuery
            .append(" where t5010.c5010_last_updated_trans_id = t504.c504_consignment_id  AND t5010.c5010_tag_id='"
                + strTagId + "' AND t504. C504_VOID_FL IS NULL) ");
      }

      sbQuery.append(" AND T520.C520_MASTER_REQUEST_ID IN ( ");
      sbQuery.append(" SELECT C520_REQUEST_ID FROM T520_REQUEST WHERE C520_STATUS_FL = 40 ");
      sbQuery.append(" AND C207_SET_ID IS NOT NULL ");
    } else if (strOpt.equals("50263")) // Items
    {
      sbQuery.append(" AND T520.C207_SET_ID IS NULL ");
      sbQuery.append(" and ( T520.C520_MASTER_REQUEST_ID is null ");
      sbQuery.append(" OR T520.C520_MASTER_REQUEST_ID IN ( ");
      sbQuery.append(" SELECT C520_REQUEST_ID FROM T520_REQUEST WHERE C207_SET_ID IS NULL) ");
    }
    sbQuery.append(" )");
    if (!strSetId.equals("")) {
      sbQuery.append(" AND T521.C205_PART_NUMBER_ID IN ");
      sbQuery.append(" (SELECT C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbQuery.append("  T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbQuery.append(" AND T207.C901_SET_GRP_TYPE = 1600  ");
      sbQuery.append(" AND T207.C207_SET_ID IN ('");
      sbQuery.append(strSetId);
      sbQuery.append("') )");
    }

    if (!strTypeVal.equals("0") && !strTypeVal.equals("")) {
      sbQuery.append(" AND T520.C901_REQUEST_BY_TYPE = '" + strTypeVal + "' "); // TYPE
    }
    if (!strSource.equals("0") && !strSource.equals("")) {
      sbQuery.append(" AND T520.C901_REQUEST_SOURCE = '" + strSource + "' "); // SOURCE
    }

    if (strAgingType.equals("14")) {
      sbQuery
          .append(" AND trunc(T520.c520_created_date) BETWEEN trunc(CURRENT_DATE - 14) and trunc(CURRENT_DATE) ");
    } else if (strAgingType.equals("21")) {
      sbQuery
          .append(" AND trunc(T520.c520_created_date) BETWEEN trunc(CURRENT_DATE - 21) and trunc(CURRENT_DATE-15) ");
    } else if (strAgingType.equals("21x")) {
      sbQuery.append(" AND trunc(T520.c520_created_date) < trunc(CURRENT_DATE-21) ");
    }
    if (!strOpt.equals("50263")) { // Items; code added to include account item consignment
      sbQuery.append(" order by T521.C205_PART_NUMBER_ID,T520.C520_REQUIRED_DATE ");
    } else {
      sbQuery.append(" UNION ALL ");
      sbQuery
          .append(" select T520.C520_REQUEST_ID ID, T520.C520_MASTER_REQUEST_ID MASTER_ID, GET_ACCOUNT_NAME(T520.C520_REQUEST_TO) ANAME, '' REPDISTNM,");
      sbQuery
          .append(" T521.C205_PART_NUMBER_ID NUM, T521.C521_QTY QTY, get_partnum_desc(T521.C205_PART_NUMBER_ID) DSC ");
      sbQuery.append(" ,GET_PARTNUMBER_PO_PEND_QTY (T521.C205_PART_NUMBER_ID) POPEND ");
      sbQuery
          .append(" ,T520.C520_REQUIRED_DATE DT,TO_CHAR(T520.C520_CREATED_DATE,'"+strCompDateFmt.concat(" HH:MI:SS") +"') TRANSDT , GET_LOG_FLAG(T520.C520_REQUEST_ID,1233) LOG_FLAG, GET_LOG_FLAG(T520.C520_REQUEST_ID,1234) INT_LOG_FLAG ");
      sbQuery.append(" from T520_REQUEST T520, T521_REQUEST_DETAIL T521 ");
      sbQuery.append(" where T520.C520_REQUEST_ID = T521.C520_REQUEST_ID ");
      sbQuery.append(" and T520.C520_STATUS_FL = 10 ");
      sbQuery.append(" and T520.C520_REQUEST_TO IS NOT NULL ");
      sbQuery.append(" AND T520.C520_REQUEST_FOR = 40025 ");
      sbQuery.append(" AND ( T520.C1900_COMPANY_ID = " + strFilter);
      sbQuery.append(" OR T520.C5040_PLANT_ID = " + strFilter);
      sbQuery.append(") ");
      sbQuery.append(" and T520.C520_VOID_FL IS NULL ");
      sbQuery.append(" AND T520.C207_SET_ID IS NULL ");
      sbQuery.append(" and ( T520.C520_MASTER_REQUEST_ID is null ");
      sbQuery.append(" OR T520.C520_MASTER_REQUEST_ID IN ( ");
      sbQuery.append(" SELECT C520_REQUEST_ID FROM T520_REQUEST WHERE C207_SET_ID IS NULL)) ");
      sbQuery.append(" )ORDER BY NUM, DT ");

    }

    return sbQuery.toString();
  }

  private String getSetsBoSQL(String strAgingType, String strSetId) {
    String strFilter = "";
    strFilter = GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "SETS_REPORT"));// SETS_REPORT-To
                                                                                                // fetch
                                                                                                // the
                                                                                                // details
                                                                                                // based
                                                                                                // on
                                                                                                // plant.
    // Rule Value := plant
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();
    StringBuffer sbQuery = new StringBuffer();
String strCompDateFmt=getCompDateFmt();
    sbQuery.append(" SELECT T520.C520_REQUEST_ID ID, T520.C520_MASTER_REQUEST_ID MASTER_ID");
    sbQuery.append(" , V700.D_NAME REPDISTNM, T520.C207_SET_ID NUM, 1 QTY");
    sbQuery
        .append(", GET_SET_NAME (T520.C207_SET_ID) DSC, T520.C520_REQUIRED_DATE DT, TO_CHAR(T520.C520_CREATED_DATE,'"+strCompDateFmt.concat(" HH:MI:SS") +"') TRANSDT, GET_LOG_FLAG(T520.C520_REQUEST_ID,1233) LOG_FLAG, GET_LOG_FLAG(T520.C520_REQUEST_ID,1234) INT_LOG_FLAG ");
    sbQuery.append(" FROM T520_REQUEST T520");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE T520.C520_STATUS_FL IN (10,15) ");
    sbQuery.append(" AND T520.C520_REQUEST_TO IS NOT NULL");
    sbQuery.append(" AND T520.C520_VOID_FL IS NULL");
    sbQuery.append(" AND T520.C207_SET_ID IS NOT NULL ");
    sbQuery.append(" AND T520.C520_MASTER_REQUEST_ID IS NULL ");
    sbQuery.append(" AND T520.C520_REQUEST_TO = V700.D_ID ");
    sbQuery.append(" AND ( T520.C1900_COMPANY_ID = " + strFilter);
    sbQuery.append(" OR T520.C5040_PLANT_ID = " + strFilter);
    sbQuery.append(") ");

    if (!strSetId.equals("")) {
      sbQuery.append(" AND T520.C207_SET_ID IN ");
      sbQuery.append(" (SELECT v207a.c207_actual_set_id ");
      sbQuery.append(" FROM v207a_set_consign_link v207a ");
      sbQuery.append(" WHERE v207a.C207_SET_ID IN ('" + strSetId + "'))");
    }
    if (strAgingType.equals("14")) {
      sbQuery
          .append(" AND trunc(T520.c520_created_date) BETWEEN trunc(CURRENT_DATE - 14) and trunc(CURRENT_DATE) ");
    } else if (strAgingType.equals("21")) {
      sbQuery
          .append(" AND trunc(T520.c520_created_date) BETWEEN trunc(CURRENT_DATE - 21) and trunc(CURRENT_DATE-15) ");
    } else if (strAgingType.equals("21x")) {
      sbQuery.append(" AND trunc(T520.c520_created_date) < trunc(CURRENT_DATE-21) ");
    }
    sbQuery.append(" ORDER BY T520.C207_SET_ID, T520.C520_REQUEST_DATE ");

    return sbQuery.toString();
  }

  private String getLoanersBoSQL(String strAgingType, String strSetId) {
    StringBuffer sbQuery = new StringBuffer();
    String strCompDateFmt=getCompDateFmt();
    sbQuery.append(" SELECT	 T525.C525_PRODUCT_REQUEST_ID ID, '' MASTER_ID ");
    sbQuery.append(" , V700.D_NAME REPDISTNM ");
    sbQuery.append(" , T526.C207_SET_ID NUM, GET_SET_NAME (T526.C207_SET_ID) DSC ");
    sbQuery
        .append(" , T526.C526_REQUIRED_DATE DT,TO_CHAR(T526.C526_LAST_UPDATED_DATE,'"+strCompDateFmt.concat(" HH:MI:SS") +"') TRANSDT, 1 QTY , GET_LOG_FLAG(T525.C525_PRODUCT_REQUEST_ID,1233) LOG_FLAG, GET_LOG_FLAG(T525.C525_PRODUCT_REQUEST_ID,1234) INT_LOG_FLAG ");
    sbQuery.append(" FROM T526_PRODUCT_REQUEST_DETAIL T526, T525_PRODUCT_REQUEST T525 ");
    sbQuery.append(getAccessFilterClause());
    sbQuery.append(" WHERE C901_REQUEST_TYPE = 4127 ");
    sbQuery.append(" AND T525.C525_PRODUCT_REQUEST_ID = T526.C525_PRODUCT_REQUEST_ID");
    sbQuery.append(" AND T525.C525_VOID_FL IS NULL	");
    sbQuery.append(" AND c526_status_fl = 10");// to fetch open rq
    sbQuery.append(" AND T526.c526_void_fl IS NULL");
    sbQuery.append(" AND T525.c525_status_fl != 40");// closed
    sbQuery.append(" AND T525.C525_REQUEST_FOR_ID = V700.D_ID ");
    sbQuery.append(" AND T525.C5040_PLANT_ID =" + getCompPlantId());
    if (!strSetId.equals("")) {
      sbQuery.append(" AND T526.C207_SET_ID IN ");
      sbQuery.append(" (SELECT v207a.c207_actual_set_id ");
      sbQuery.append(" FROM v207a_set_consign_link v207a ");
      sbQuery.append(" WHERE v207a.C207_SET_ID IN ('" + strSetId + "'))");
    }
    if (strAgingType.equals("14")) {
      sbQuery
          .append(" AND trunc(T525.c525_requested_date) BETWEEN trunc(CURRENT_DATE - 14) and trunc(CURRENT_DATE) ");
    } else if (strAgingType.equals("21")) {
      sbQuery
          .append(" AND trunc(T525.c525_requested_date) BETWEEN trunc(CURRENT_DATE - 21) and trunc(CURRENT_DATE-15) ");
    } else if (strAgingType.equals("21x")) {
      sbQuery.append(" AND trunc(T525.c525_requested_date) < trunc(CURRENT_DATE-21) ");
    }
    sbQuery.append(" ORDER BY  T526.C207_SET_ID ");

    return sbQuery.toString();
  }

  private String getIHLoanersBoSQL() {
    StringBuffer sbQuery = new StringBuffer();
    String strCompDateFmt=getCompDateFmt();
    sbQuery.append(" SELECT T525.C525_PRODUCT_REQUEST_ID ID  ");
    sbQuery.append(" , C525_REQUEST_FOR_ID ");
    sbQuery.append(" , '' MASTER_ID  ");
    sbQuery.append(" , DECODE( ");
    sbQuery.append(" 	t7103.c901_loan_to ");
    sbQuery.append(" 	,'19518', get_rep_name(t7103.c101_loan_to_id) ");
    sbQuery.append(" 		, get_user_name(t7103.c101_loan_to_id) ");
    sbQuery.append("  )  REPDISTNM  ");
    sbQuery.append(" , T526.C207_SET_ID NUM ");
    sbQuery.append(" , GET_SET_NAME (T526.C207_SET_ID) DSC  ");
    sbQuery.append(" , T526.C526_REQUIRED_DATE DT,TO_CHAR(T526.C526_LAST_UPDATED_DATE,'"+strCompDateFmt.concat(" HH:MI:SS") +"') TRANSDT");
    sbQuery.append(" , 1 QTY  ");
    sbQuery.append(" , GET_LOG_FLAG(T525.C525_PRODUCT_REQUEST_ID,1233) LOG_FLAG ");
    sbQuery.append(" , GET_LOG_FLAG(T525.C525_PRODUCT_REQUEST_ID,1234) INT_LOG_FLAG ");
    sbQuery.append(" FROM t525_product_request t525 ");
    sbQuery.append(" , t526_product_request_detail t526 ");
    sbQuery.append(" , t7100_case_information t7100 ");
    sbQuery.append(" , t7103_schedule_info t7103 ");
    sbQuery.append(" , t7104_case_set_information t7104 ");
    sbQuery.append(" WHERE t7100.c7100_case_info_id = t7103.c7100_case_info_id ");
    sbQuery.append(" AND t7100.c7100_case_info_id = t7104.c7100_case_info_id ");
    sbQuery
        .append(" AND t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id ");
    sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id ");
    sbQuery.append(" AND t7100.c901_type = 1006504 "); // EVENT
    sbQuery.append(" AND t7100.c901_case_status = 19524 "); // ACTIVE
    sbQuery.append(" AND c526_status_fl  = 10 "); // Open Request
    sbQuery.append(" AND t7100.c7100_void_fl IS NULL ");
    sbQuery.append(" AND t7104.c7104_void_fl IS NULL ");
    sbQuery.append(" AND t526.c526_void_fl IS NULL ");
    sbQuery.append(" AND t525.c525_void_fl IS NULL ");
    sbQuery.append(" AND t526.c207_set_id IS NOT NULL ");
    sbQuery.append(" AND T525.c525_status_fl != 40 "); // NOT CLOSED
    sbQuery.append(" ORDER BY  T526.C207_SET_ID ");

    return sbQuery.toString();

  }

  /**
   * reportDSSummary - This Method is used to get crosstab details for Set Overview
   * 
   * @param hmPara
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadTrendReport(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmFinalReturn = new HashMap();
    // try { // to throw the AppError
    // log.debug(" values to fetch Trend is  " + hmParam);
    int idiv = 1;
    HashMap hmLinktoPart = new HashMap();
    HashMap hmLinktoDemand = new HashMap();
    HashMap hmLinktoCurrent = new HashMap();
    HashMap hmLinktoBO = new HashMap();
    HashMap hmLinktoShelfPO = new HashMap();
    HashMap hmLinktoTotal = new HashMap();
    HashMap hmLinktoAvg = new HashMap();
    HashMap hmLinktoTrend = new HashMap();
    HashMap hmOUSAverage = new HashMap();

    HashMap hmReturnDemand = new HashMap();
    HashMap hmReturnCurrent = new HashMap();
    HashMap hmReturntrend = new HashMap();

    ArrayList alCurrentHeader = new ArrayList();
    ArrayList alDemandHeader = new ArrayList();
    ArrayList alForecastHeader = new ArrayList();
    ArrayList alTemp = new ArrayList();
    ArrayList alTemp1 = new ArrayList();
    ArrayList alTemp2 = new ArrayList();

    ArrayList alDemandDetails = new ArrayList();
    ArrayList alCurrentDetails = new ArrayList();
    ArrayList alBackOrder = new ArrayList();
    ArrayList alShelfPO = new ArrayList();
    ArrayList alPartDetails = new ArrayList();
    ArrayList alFcstLvlDetails = new ArrayList();
    ArrayList alTotalColumn = new ArrayList();
    ArrayList alAverageColumn = new ArrayList();
    ArrayList alOUSAverage = new ArrayList();
    ArrayList alTrend = new ArrayList();

    ResultSet rsForecastHeader = null;
    ResultSet rsForecastDetails = null;
    ResultSet rsDemandHeader = null;
    ResultSet rsDemandDetails = null;
    ResultSet rsCurrentHeader = null;
    ResultSet rsCurrentDetails = null;
    ResultSet rsFcstLvlDetails = null;
    ResultSet rsFcstLvlHeader = null;

    GmCrossTabReport gmCrossReport = new GmCrossTabReport();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strSalesGrpType = GmCommonClass.parseNull((String) hmParam.get("SALESGRPTYPE"));
    String strTrendcal = GmCommonClass.parseNull((String) hmParam.get("INPUTTRENDCAL"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));

    if (!strPartNum.equals("")) {
      String strLastcharacter = strPartNum.substring(strPartNum.length() - 1);
      strPartNum = strPartNum.replaceAll("\\*", "/*");
      if (strLastcharacter.equals(",") && strSalesGrpType.equals("50271")) {
        strPartNum = strPartNum.substring(0, strPartNum.length() - 1);
      }
      strPartNum = ("^").concat(strPartNum);
      strPartNum = strPartNum.replaceAll(",", "|^");
    }
    long headerTime = System.currentTimeMillis();

    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETNUMSEARCH"));

    String strGroupId = GmCommonClass.parseNull((String) hmParam.get("SALESGRPID"));

    String strTrendType = GmCommonClass.parseNull((String) hmParam.get("TRENDTYPE"));

    String strTrendDur = GmCommonClass.parseNull((String) hmParam.get("TRENDDURATION"));
    String strTrendPer = GmCommonClass.parseNull((String) hmParam.get("TRENDPERIOD"));


    // To fetch the transaction information
    gmDBManager.setPrepareString("gm_pkg_op_backorder_rpt.gm_fch_trend_summary ", 17);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(12, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(13, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(14, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(15, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(16, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(17, OracleTypes.CURSOR);

    gmDBManager.setString(1, strTrendcal);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setString(3, strGroupId);
    gmDBManager.setString(4, strSalesGrpType);
    gmDBManager.setString(5, strTrendType);
    gmDBManager.setString(6, strSetID);
    gmDBManager.execute();

    log.debug(" Header End Time" + System.currentTimeMillis() + "Header  Time Taken : "
        + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();

    rsForecastHeader = (ResultSet) gmDBManager.getObject(7);
    rsForecastDetails = (ResultSet) gmDBManager.getObject(8);
    rsDemandHeader = (ResultSet) gmDBManager.getObject(9);
    rsDemandDetails = (ResultSet) gmDBManager.getObject(10);
    rsCurrentHeader = (ResultSet) gmDBManager.getObject(11);
    rsCurrentDetails = (ResultSet) gmDBManager.getObject(12);

    log.debug(" >>>>>>>>>> 1 >Header End Time" + System.currentTimeMillis()
        + "Header  Time Taken : " + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();

    alBackOrder = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(13));
    log.debug(" >>>>>>>>>>2>Header End Time" + System.currentTimeMillis() + "Header  Time Taken : "
        + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();
    alShelfPO = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(14));
    log.debug(" >>>>>>>>>>2.1>Header End Time" + System.currentTimeMillis()
        + "Header  Time Taken : " + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();
    alPartDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(15));
    log.debug(" >>>>>>>>>>2.2>Header End Time" + System.currentTimeMillis()
        + "Header  Time Taken : " + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();
    rsFcstLvlHeader = (ResultSet) gmDBManager.getObject(16);
    log.debug(" >>>>>>>>>>3>Header End Time" + System.currentTimeMillis() + "Header  Time Taken : "
        + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();
    rsFcstLvlDetails = (ResultSet) gmDBManager.getObject(17);

    log.debug(" >>>>>>>>>>4>Header End Time" + System.currentTimeMillis() + "Header  Time Taken : "
        + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();

    gmCrossReport.setAcceptExtraColumn(true);
    gmCrossReport.setExtraColumnParams(4, "");
    gmCrossReport.setExtraColumnParams(5, "PART_QTY");

    hmReturn = gmCrossReport.GenerateCrossTabReport(rsForecastHeader, rsForecastDetails);
    log.debug(" >>>>>>>>>>???>Header End Time" + System.currentTimeMillis()
        + "Header  Time Taken : " + (System.currentTimeMillis() - headerTime));
    headerTime = System.currentTimeMillis();

    alTemp = (ArrayList) ((ArrayList) hmReturn.get("Header")).clone();
    alForecastHeader = (ArrayList) alTemp.clone();

    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();

    gmCrossReport.setAcceptExtraColumn(false);
    hmReturnDemand = gmCrossReport.GenerateCrossTabReport(rsDemandHeader, rsDemandDetails);
    alTemp1 = (ArrayList) ((ArrayList) hmReturnDemand.get("Header")).clone();
    alDemandHeader = (ArrayList) alTemp1.clone();

    hmReturnCurrent = gmCrossReport.GenerateCrossTabReport(rsCurrentHeader, rsCurrentDetails);
    alTemp2 = (ArrayList) ((ArrayList) hmReturnCurrent.get("Header")).clone();
    alCurrentHeader = (ArrayList) alTemp2.clone();
    alCurrentDetails = (ArrayList) hmReturnCurrent.get("Details");

    gmCrossReport.setAcceptExtraColumn(true);
    gmCrossReport.setExtraColumnParams(4, "");
    gmCrossReport.setExtraColumnParams(5, "US_QTY");
    gmCrossReport.setExtraColumnParams(6, "OUS_QTY");
    // gmCrossReport.setExtraColumnParams(7,"WW_LVL_VALUE");
    // gmCrossReport.setExtraColumnParams(8,"OUS_LVL_VALUE");

    HashMap hmForecastByLvl =
        gmCrossReport.GenerateCrossTabReport(rsFcstLvlHeader, rsFcstLvlDetails);
    gmCrossReport.setAcceptExtraColumn(false);
    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();
    if (alCurrentDetails.size() == 0) {
      HashMap hmemp = new HashMap();
      hmemp.put("ID", "");
      alCurrentDetails.add(GmCommonClass.parseNullHashMap(hmemp));
    }
    if (alBackOrder.size() == 0) {
      HashMap hmemp = new HashMap();
      hmemp.put("ID", "");
      hmemp.put("BO_QTY", "0");
      alBackOrder.add(GmCommonClass.parseNullHashMap(hmemp));
    }

    if (strTrendDur.equals("50281")) { // Duration 30 days

      idiv = 3;

    }
    if (strTrendDur.equals("50282")) { // Duration 15 days

      idiv = 6;

    }
    if (strTrendDur.equals("50283")) { // Duration 7 days

      idiv = 12;

    }

    if (strTrendType.equals("50276")) // Demand
    {
      alTotalColumn = addTotalColumn((ArrayList) hmReturnDemand.get("Details"));
      alAverageColumn = addAverageColumn((ArrayList) hmReturnDemand.get("Details"), "Total", idiv);
    } else if (strTrendType.equals("50277")) // Forecast
    {

      alTotalColumn = addTotalColumn((ArrayList) hmReturn.get("Details"));
      alAverageColumn = addAverageColumn((ArrayList) hmReturn.get("Details"), "Total", idiv);
    } else if (strTrendType.equals("50278")) // Current
    {
      alTotalColumn = addTotalColumn(alCurrentDetails);
      alAverageColumn = addAverageColumn((ArrayList) hmReturnCurrent.get("Details"), "Total", idiv);
    }
    gmDBManager.close();
    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();

    hmLinktoDemand = getLinkInfo("Demand");
    alDemandDetails = (ArrayList) hmReturnDemand.get("Details");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alDemandDetails, hmLinktoDemand);


    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();

    hmLinktoBO = getLinkInfo("BackOrder");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alBackOrder, hmLinktoBO);

    // //log.debug(" hmLinktoBO is  " + hmLinktoBO);
    // //log.debug(" alBackOrder is  " + alBackOrder);
    hmLinktoCurrent = getLinkInfo("Current");
    alCurrentDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturnCurrent.get("Details"));
    // ////log.debug(" hmLinktoCurrent is  " + hmLinktoCurrent);
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alCurrentDetails, hmLinktoCurrent);
    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();

    hmLinktoTotal = getLinkInfo("Total");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alTotalColumn, hmLinktoTotal);


    hmLinktoAvg = getLinkInfo("Avg");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alAverageColumn, hmLinktoAvg);
    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();

    hmLinktoShelfPO = getLinkInfo("ShelfPO");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alShelfPO, hmLinktoShelfPO);
    hmLinktoPart = getLinkInfo("Part");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alPartDetails, hmLinktoPart);
    hmLinktoTrend = getLinkInfo("Trend");

    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();
    hmReturn = calculateTrend(hmReturn, hmForecastByLvl, strTrendDur, strTrendPer, idiv);
    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();
    alOUSAverage = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("AVERAGE"));
    hmOUSAverage = getLinkInfo("OUSAVG");
    hmReturn = gmCrossReport.linkCrossTab(hmReturn, alOUSAverage, hmOUSAverage);
    // log.debug(" Header End Time"+System.currentTimeMillis()
    // +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
    headerTime = System.currentTimeMillis();
    hmFinalReturn.put("Main", hmReturn);
    hmFinalReturn.put("Pdetail", alPartDetails);
    hmFinalReturn.put("ForecastHeader", alForecastHeader);
    hmFinalReturn.put("DemandHeader", alDemandHeader);
    hmFinalReturn.put("CurrentHeader", alCurrentHeader);
    // } catch (Exception exp) {
    // exp.printStackTrace();
    // }
    return hmFinalReturn;
  }

  private HashMap calculateTrend(HashMap hmResult, HashMap hmForecastByLvl, String strTrendDur,
      String strTrendPer, int idivisor) {
    // //log.debug("Alresult:"+ alResult);

    double dAverage = 0;
    double dShelf = 0;
    double dDHR = 0;
    double dblRWQty = 0;
    int iStartPoint = 1;
    int iEndPoint = 1;
    double dTrend = 0;
    Calendar c = null;
    int idiv = 1;
    int iTrendPer = Integer.parseInt(strTrendPer);
    String strTemp = "";
    int iperiod = 0;

    ArrayList alReturn = new ArrayList();
    ArrayList alHeader = new ArrayList();
    ArrayList alResult = new ArrayList();

    ArrayList alFcstHeader = new ArrayList();
    ArrayList alFcstDetails = new ArrayList();
    HashMap hmTemp = new HashMap();
    String strTempHdr = "";
    double dousQty = 0;
    double dusQty = 0;
    double dOUSAvg = 0;
    double dUSAvg = 0;
    double dWWAvg = 0;
    double dWWCalcAvg = 0;
    double dOUSTrnd = 0;
    HashMap hmAverage = new HashMap();
    ArrayList alAverage = new ArrayList();
    DecimalFormat df = new DecimalFormat("#.##");

    ArrayList alTempHeader = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmRow = null;
    alHeader = (ArrayList) hmResult.get("Header");
    alResult = (ArrayList) hmResult.get("Details");

    alFcstHeader = GmCommonClass.parseNullArrayList((ArrayList) hmForecastByLvl.get("Header"));
    alFcstDetails = GmCommonClass.parseNullArrayList((ArrayList) hmForecastByLvl.get("Details"));
    int forcastdtlsize = alFcstDetails.size();
    int forcasthdrsize = alFcstHeader.size();

    Iterator itrAlResult = alResult.iterator();
    if (strTrendDur.equals("50281")) { // Duration 30 days
      iEndPoint = 30;
      idiv = 1;
      iperiod = 30;
    }
    if (strTrendDur.equals("50282")) { // Duration 15 days
      iEndPoint = 15;
      idiv = 2;
      iperiod = 15;
    }
    if (strTrendDur.equals("50283")) { // Duration 7 days
      iEndPoint = 7;
      idiv = 4;
      iperiod = 7;
    }

    HashMap hmForecastMap = new HashMap();
    for (int a = 0; a < forcastdtlsize; a++) {
      hmTemp = GmCommonClass.parseNullHashMap((HashMap) alFcstDetails.get(a));
      hmForecastMap.putAll(hmTemp);
    }
    hmTemp = new HashMap();

    // for duration select , the trend header should like "1 - 30","31 - 60", "61 - 90" when 30 days
    // selected
    // should like "1 - 15","16 - 30", "31 - 45" when 15 days selected, should like
    // "1 - 7","8 - 15", "16 - 21" when 7 days selected
    for (int i = 0; i < iTrendPer; i++) {

      strTemp = iStartPoint + " - " + iEndPoint;
      alHeader.add(strTemp);
      alTempHeader.add(strTemp); // this temp trend header used for trend calculation value
      iStartPoint = iStartPoint + iperiod;
      iEndPoint = iEndPoint + iperiod;
    }

    while (itrAlResult.hasNext()) {
      hmRow = (HashMap) itrAlResult.next();
      dWWAvg = Double.parseDouble(GmCommonClass.parseZero((String) hmRow.get("Average")));
      dShelf = Double.parseDouble(GmCommonClass.parseZero((String) hmRow.get("Shelf")));
      dDHR = Double.parseDouble(GmCommonClass.parseZero((String) hmRow.get("On DHR")));
      dShelf = dShelf + dDHR;
      dblRWQty = Double.parseDouble(GmCommonClass.parseZero((String) hmRow.get("RW QTY")));
      String strValue = GmCommonClass.parseNull((String) hmRow.get("ID")); // log.debug("strValue is ........................"
                                                                           // +strValue);
      if (strValue.equals("")) {
        itrAlResult.remove();
        // continue;
      }

      dousQty =
          Double.parseDouble(GmCommonClass.parseZero((String) hmForecastMap.get(strValue
              + "OUS_QTY")));
      dusQty =
          Double.parseDouble(GmCommonClass.parseZero((String) hmForecastMap
              .get(strValue + "US_QTY")));
      dOUSAvg = Math.ceil(dousQty / idivisor);
      dUSAvg = Math.ceil(dusQty / idivisor);

      // log.debug(strValue+" OUS Qty"+dousQty+" Ous Avg"+dOUSAvg);
      // log.debug(strValue+" US Qty"+dusQty+" us Avg"+dUSAvg);

      hmAverage = new HashMap();
      if (!GmCommonClass.parseNull((String) hmRow.get("ID")).equals("")) {
        hmAverage.put("ID", hmRow.get("ID"));
        hmAverage.put("US_AVERAGE", df.format(dUSAvg));
        hmAverage.put("OUS_AVERAGE", df.format(dOUSAvg));
        alAverage.add(hmAverage);
      }

      // calculate trend
      for (int i = 0; i < iTrendPer; i++) {
        dOUSTrnd = dblRWQty - dOUSAvg;
        dblRWQty = dblRWQty - dOUSAvg;

        if (dblRWQty < 0)
          dblRWQty = 0;

        dWWCalcAvg = dUSAvg;
        // When there is a deficit quantity, we need to add that to the US Average to calculate the
        // trend.
        if (dOUSTrnd < 0) {
          dOUSTrnd = dOUSTrnd * -1;
          dWWCalcAvg = dUSAvg + dOUSTrnd;
        }
        // When there is a deficient in RW then we should add the US need + OUS Deficient and reduce
        // it from Shelf.
        // When the OUS Avg is 0, then use the ww average rather than using the US Average.
        if (dblRWQty == 0 && dOUSAvg == 0) {
          dWWCalcAvg = dWWAvg;
        }

        dTrend = dShelf - dWWCalcAvg;
        dShelf = dShelf - dWWCalcAvg;
        // log.debug("dShelf: "+dShelf+ "dAverage: " + dAverage + " dShelf - dAverage: "+ (dShelf -
        // dAverage));
        hmRow.put(alTempHeader.get(i), "" + dTrend);
      }


    }
    // Final Value to be passed to the report
    hmReturn.put("Header", alHeader);
    hmReturn.put("Details", alResult);
    hmReturn.put("Footer", hmResult.get("Footer"));
    hmReturn.put("AVERAGE", alAverage);
    return hmReturn;
  }


  private ArrayList addAverageColumn(ArrayList alResult, String strColumnName, int iDivisor) {
    Iterator itrAlResult = alResult.iterator();
    double dResult = 0;
    DecimalFormat df = new DecimalFormat("#.##");
    HashMap hmResult = null;
    ArrayList alReturn = new ArrayList();
    while (itrAlResult.hasNext()) {
      hmResult = new HashMap();
      HashMap hmRow = (HashMap) itrAlResult.next();
      dResult = Double.parseDouble(GmCommonClass.parseZero((String) hmRow.get(strColumnName)));
      dResult = Math.ceil(dResult / iDivisor);
      hmResult.put("ID", hmRow.get("ID"));
      hmResult.put("AVERAGE", df.format(dResult));
      alReturn.add(hmResult);
    }

    return alReturn;
  }

  private ArrayList addTotalColumn(ArrayList alResult) {
    Iterator itrAlResult = alResult.iterator();
    HashMap hmResult = null;
    ArrayList alReturn = new ArrayList();

    while (itrAlResult.hasNext()) {
      hmResult = new HashMap();
      HashMap hmRow = (HashMap) itrAlResult.next();
      hmResult.put("ID", hmRow.get("ID"));
      hmResult.put("TOTALDEMAND", GmCommonClass.parseZero((String) hmRow.get("Total")));
      alReturn.add(hmResult);
    }

    return alReturn;
  }

  public RowSetDynaClass loadBackOrderByPartReport(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));

    gmDBManager.setPrepareString("gm_pkg_op_backorder_rpt.gm_fch_part_backorder", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strProjectId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    log.debug("rdResult" + rdResult);
    gmDBManager.close();

    return rdResult;
  }// End of loadBackOrderByPartReport

  public String getItemLoanersBoSQL(String strAgingType) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" select t412.C412_INHOUSE_TRANS_ID ID, C412_REF_ID MASTER_ID, t413.c205_part_number_id NUM ,get_partnum_desc(t413.c205_part_number_id) DSC , t413.c413_item_qty QTY, t412.C412_VERIFIED_DATE BODATE ");
    sbQuery.append(" from T412_INHOUSE_TRANSACTIONS t412 , t413_inhouse_trans_items t413 ");
    sbQuery.append(" WHERE t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id ");
    sbQuery.append(" and C412_STATUS_FL = 0 ");
    sbQuery.append(" and t413.C413_VOID_FL is null ");
    sbQuery.append(" and c412_void_fl  is null");
    if (strAgingType.equals("14")) {
      sbQuery
          .append(" AND trunc(t412.c412_created_date) BETWEEN trunc(CURRENT_DATE - 14) and trunc(CURRENT_DATE) ");
    } else if (strAgingType.equals("21")) {
      sbQuery
          .append(" AND trunc(t412.c412_created_date) BETWEEN trunc(CURRENT_DATE - 21) and trunc(CURRENT_DATE-15) ");
    } else if (strAgingType.equals("21x")) {
      sbQuery.append(" AND trunc(t412.c412_created_date) < trunc(CURRENT_DATE-21) ");
    }
    return sbQuery.toString();
  }

  /**
   * loadBackOrderEmail This method is to fetch the Back Order details to the Sales Back Order Email
   * screen
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   * @author arajan
   */
  public ArrayList loadBackOrderEmail(HashMap hmParam) {

    String strDistType = GmCommonClass.parseZero((String) hmParam.get("DISTTYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("STRFROMDATE"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("STRTODATE"));
    String strEmailType = GmCommonClass.parseZero((String) hmParam.get("EMAILTYPE"));
    String strAccCurrId = GmCommonClass.parseZero((String) hmParam.get("ACCCURRID"));
    String strDateFmt = getCompDateFmt();
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "ORDFILTER"));
    String strCompPlantFilter =
        strFilter.equals("") ? strCompanyId : getGmDataStoreVO().getPlantid();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery
        .append(" SELECT T501.C703_SALES_REP_ID REPID,GET_REP_NAME(T501.C703_SALES_REP_ID) SALES_REP_NAME,T501.C501_ORDER_ID ID ");
    sbQuery
        .append(" , GET_ACCOUNT_NAME (T501.C704_ACCOUNT_ID) ANAME, GET_DIST_REP_NAME (T501.C703_SALES_REP_ID) REPDISTNM ");
    sbQuery.append(" , T501.C501_ORDER_DATE ");

    sbQuery.append(" DT, T501.C501_TOTAL_COST + NVL (T501.C501_SHIP_COST, 0) SALES ");
    sbQuery
        .append(" , GET_PARTNUMBER_PO_PEND_QTY (T502.C205_PART_NUMBER_ID) POPEND , T501.C704_ACCOUNT_ID ACCID, T501.C501_PARENT_ORDER_ID MASTER_ID ");
    sbQuery
        .append(" , T502.C205_PART_NUMBER_ID NUM, T502.C502_ITEM_QTY QTY, T502.C502_ITEM_PRICE PRICE, GET_PARTNUM_DESC (T502.C205_PART_NUMBER_ID) DSC ");
    sbQuery
        .append(" , T501A.C501A_ATTRIBUTE_VALUE email_date, T501A.C501_ORDER_ID attrId, T501A.C501A_HISTORY_FL HISTFL, GM_PKG_CM_CONTACT.GET_CONTACT_VALUE (T703.C101_PARTY_ID, 4000716) EMAIL");
    sbQuery
        .append(" FROM T501_ORDER T501, T502_ITEM_ORDER T502,T703_SALES_REP t703, T501A_ORDER_ATTRIBUTE t501a, t704_account t704 ");
    sbQuery.append(" WHERE T501.C501_ORDER_ID = T502.C501_ORDER_ID ");
    sbQuery
        .append(" AND (T501.c901_ext_country_id IS NULL OR T501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)) ");
    sbQuery.append(" AND T501.C901_ORDER_TYPE  = 2525 ");
    sbQuery.append(" AND T501.C501_DELETE_FL  IS NULL ");
    sbQuery.append(" AND T501.C501_STATUS_FL   = 0 ");
    sbQuery.append(" AND T502.C502_VOID_FL IS NULL ");
    sbQuery.append(" AND T703.C703_SALES_REP_ID = T501.C703_SALES_REP_ID ");
    sbQuery.append(" AND t501.C704_ACCOUNT_ID = t704.C704_ACCOUNT_ID ");
    sbQuery.append(" AND T501A.C501_ORDER_ID(+) = T501.C501_ORDER_ID ");
    sbQuery.append(" AND T501A.C901_ATTRIBUTE_TYPE(+) = 4000764 "); // Sales BackOrder Email Time
    if (!strDistType.equals("0")) {
      sbQuery.append(" AND T501.C703_SALES_REP_ID = '");
      sbQuery.append(strDistType);
      sbQuery.append(" '");
    }

    if (!strAccCurrId.equals("") && !strAccCurrId.equals("0")) {
      sbQuery.append(" AND t704.C901_CURRENCY = '");
      sbQuery.append(strAccCurrId);
      sbQuery.append("'");
    }

    if (!strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND TRUNC (T501.C501_ORDER_DATE) BETWEEN TRUNC (TO_DATE('");
      sbQuery.append(strFromDt);
      sbQuery.append("' ,' ");
      sbQuery.append(strDateFmt);
      sbQuery.append("')) AND TRUNC (TO_DATE('");
      sbQuery.append(strToDt);
      sbQuery.append("','");
      sbQuery.append(strDateFmt);
      sbQuery.append("')) ");
    }
    if (strEmailType.equals("104761")) {
      sbQuery.append(" AND T501A.C501A_ATTRIBUTE_VALUE IS NOT NULL ");
    } else if (strEmailType.equals("104760")) {
      sbQuery.append(" AND T501A.C501A_ATTRIBUTE_VALUE IS NULL ");
    }
    sbQuery.append(" AND T501.C501_VOID_FL    IS NULL ");
    sbQuery.append(" AND T703.C703_VOID_FL IS NULL ");
    sbQuery.append(" AND T501A.C501A_VOID_FL(+) IS NULL ");
    // Added CompanyId and PlantId Filter
    sbQuery.append(" AND  (T501.C1900_COMPANY_ID =" + strCompPlantFilter);
    sbQuery.append(" OR  T501.C5040_PLANT_ID =" + strCompPlantFilter);
    sbQuery
        .append(" ) ORDER BY T501.C703_SALES_REP_ID,T502.C205_PART_NUMBER_ID, T501.C501_ORDER_DATE ");

    log.debug("sbQuery.toString()>>>>>>" + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alReturn;
  }

  /**
   * sendSalesBackOrderMail This method will Approve the Loaner Extension Request.
   * 
   * @param HashMap
   * @return void
   * @exception AppError
   * @author arajan
   */
  public void sendBackOrderEmail(HashMap hmParam) throws AppError, ParseException {
    GmBackOrderTransBean gmBackOrderTransBean = new GmBackOrderTransBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAccCurrSymb = GmCommonClass.parseNull((String) hmParam.get("ACCCURRSYMB"));
    String strOrderString = "";
    String strEmailString = "";
    String strEmailValues = "";
    String[] strRepStringArr = strInputString.split("\\|");
    String[] strRepId = null;
    String strRepString = "";
    String strBOrderString = "";

    // Getting email details from rules table
    String strSubject =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("SUBJECT", "SALESBAKORDEMAIL"));
    String strContent =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CONTENT", "SALESBAKORDEMAIL"));
    String strCCEmail =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CCEMAIL", "SALESBAKORDEMAIL"));
    String strEmailHeader =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("HEADER", "SALESBAKORDEMAIL"));
    String strCurrSymbol =
        strAccCurrSymb.equals("") ? GmCommonClass.parseNull(GmCommonClass.getRuleValue(
            "CURRSYMBOL", "CURRSYMBOL")) : strAccCurrSymb;
    String strToEmail = "";
    int intRepIdLen = strRepStringArr.length;

    for (int i = 0; i < intRepIdLen; i++) {
      strRepString = strRepStringArr[i] + "|";
      strRepId = strRepString.split("\\^"); // Get the rep id from string

      strBOrderString =
          strRepString.substring(strRepString.indexOf("^") + 1, strRepString.length() - 1);// Get
                                                                                           // the
                                                                                           // backorder
                                                                                           // string
      hmParam.put("REPID", strRepId[0]);
      hmParam.put("BOSTRING", strBOrderString);

      hmReturn = fetchBOEmailDtls(hmParam);// Fetching details for email

      hmReturn.put("SUBJECT", strSubject);
      hmReturn.put("CONTENT", strContent);
      hmReturn.put("CCMAILID", strCCEmail);
      hmReturn.put("HEADERNAME", strEmailHeader);
      hmReturn.put("CURRSYMBOL", strCurrSymbol);

      sendSalesBOEmail(hmReturn);// Call the procedure to send the mail

      gmBackOrderTransBean.saveSalesBOEmail(hmParam); // To save the date and time of mail
    }

  } // End of sendSalesBackOrderMail

  /**
   * fetchBOEmailDtls This method will fetch the details for the mail
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError
   * @author arajan
   */
  public HashMap fetchBOEmailDtls(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strBOrderString = GmCommonClass.parseNull((String) hmParam.get("BOSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    ArrayList alBODtls = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strToEmail = "";


    gmDBManager.setPrepareString("gm_pkg_op_backorder_rpt.gm_fch_backorder_email", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strRepId);
    gmDBManager.setString(2, strBOrderString);
    gmDBManager.execute();

    alBODtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3)); // Back order
                                                                                  // details
    strToEmail = GmCommonClass.parseNull(gmDBManager.getString(4));// To email id
    gmDBManager.close();

    hmReturn.put("BODTLS", alBODtls);
    hmReturn.put("TOEMAILID", strToEmail);

    return hmReturn;
  }

  /**
   * sendSalesBOEmail This method is to send the mail
   * 
   * @param HashMap
   * @return void
   * @exception AppError
   * @author arajan
   */
  public void sendSalesBOEmail(HashMap hmParam) throws AppError {
    GmEmailProperties emailProps = new GmEmailProperties();
    ArrayList alBODtls = new ArrayList();
    String strToEmail = GmCommonClass.parseNull((String) hmParam.get("TOEMAILID"));
    String strCc = GmCommonClass.parseNull((String) hmParam.get("CCMAILID"));
    String strFrom = "notification@globusmedical.com";
    String strSubject = GmCommonClass.parseNull((String) hmParam.get("SUBJECT"));
    String strHeader = GmCommonClass.parseNull((String) hmParam.get("HEADERNAME"));
    alBODtls = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("BODTLS"));

    emailProps.setMimeType("text/html;charset=utf-8");
    emailProps.setRecipients(strToEmail);
    emailProps.setCc(strCc);
    emailProps.setSender(strFrom);
    emailProps.setSubject(strSubject);
    emailProps.setEmailHeaderName(strHeader);

    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName("/GmBackOrderEmail.jasper");
    jasperMail.setAdditionalParams(hmParam);
    jasperMail.setReportData(alBODtls);
    jasperMail.setEmailProperties(emailProps);
    HashMap hmReturn = jasperMail.sendMail();
  }

  /**
   * This method is used to get the Tag Id Based on Transaction Id
   * 
   * @param String strRefId - CN ID
   * @return strTagId
   * @throws AppError the app error
   */
  public String getTagIdFromTxnId(String strRefId) throws AppError {
    String strTagId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_tagid_from_txn_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strRefId);
    gmDBManager.execute();
    strTagId = gmDBManager.getString(1);
    gmDBManager.close();
    return strTagId;
  }


}
