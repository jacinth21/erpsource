package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmSetPrioritizationBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  public GmSetPrioritizationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * @param gmDataStoreVO
   * @return
   */
  public GmSetPrioritizationBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }

  /**
   * fetchConsignmentDetails and prioritydetails - This method will
   * 
   * @param Consignmentid
   * @param HashMap hmParam
   * @return HashMap
   */
  public HashMap fetchConsignmentDetails(HashMap hmParam) {

    HashMap hmDetails = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("Gm_pkg_op_loaner_priority.gm_fch_priority_details", 2);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID")));
    gmDBManager.execute();
    hmDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmDetails;

  } // End of fetchConsignmentDetails

  /**
   * updateconsignment details and prioritydetails - This method will
   * 
   * @param Consignmentid
   * @param UserId
   * @param HashMap hmParam
   */
  public void saveConsignmentDetails(HashMap hmParam) {

    GmCommonBean gmCommonBean = new GmCommonBean();
    HashMap hmDetails = new HashMap();
    String strLog = "";
    String strConsignmentId = "";
    String strUserId = "";
    String strChangePriority = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    String strCompanyPlant = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());
    strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    strChangePriority = GmCommonClass.parseNull((String) hmParam.get("CHANGEPRIORITY"));
    gmDBManager.setPrepareString("Gm_pkg_op_loaner_priority.gm_sav_priority", 5);
    /*
     * register set input parameters
     */
    gmDBManager.setString(1, strConsignmentId);
    gmDBManager.setString(2, strChangePriority);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strCompanyId);
    gmDBManager.setString(5, strCompanyPlant);
    gmDBManager.execute();
    // 100126: Change Priority Log
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strConsignmentId, strLog, strUserId, "100126");// which user
                                                                                       // edit the
                                                                                       // change
                                                                                       // priority
                                                                                       // screen and
                                                                                       // given the
                                                                                       // comment
    }
    gmDBManager.commit();

  } // End of saveConsignmentDetails

  /**
   * fetchSetPriority To fetch the priority details of the set
   * 
   * @param strConsignId
   * @return String
   */
  public String fetchSetPriority(String strConsignId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPriorityStr = "";
    gmDBManager.setPrepareString("Gm_pkg_op_loaner_priority.gm_fch_set_priority", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.execute();
    strPriorityStr = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strPriorityStr;
  }

}
