/**
 * 
 */
package com.globus.operations.logistics.beans;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author pvigneshwaran
 *
 */
public class GmBOItemReqReleaseBean extends GmBean {
	  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

	  
	  public GmBOItemReqReleaseBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmBOItemReqReleaseBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	  
	  /**fetchBOItemReqDetails : This method used to load BO item req details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchBOItemReqDetails (HashMap hmParam) throws AppError{
		 
		   String strAccountName = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTNAME"));
		   String strDistName = GmCommonClass.parseNull((String) hmParam.get("DISTNAME"));
		   String strSalesRepName = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));
		   String strPartNos = GmCommonClass.parseNull((String) hmParam.get("PARTNOS"));
		   String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQID"));
		   String strBoPriority = GmCommonClass.parseNull((String) hmParam.get("BOPRIORITY")); 
		   String strDateFmt = getGmDataStoreVO().getCmpdfmt();
   
		   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());          
		   StringBuffer sbQuery = new StringBuffer();  
					  
		   sbQuery.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT('partnumber' value v521a.c205_part_number_id,'fgqty' value v521a.inshelf,'bopriority' value  v521a.bopriority, ");
		   sbQuery.append(" 'boqty' value  v521a.boqty,'boreqid' value id,'parentreqid' value master_id,'requesteddt' value dt,'reqtype' value reqtype,'requestornm' value nvl(aname, repdistnm), ");
		   sbQuery.append(" 'qtytorelease' value qty, 'qty' value qty, 'accountid' value accntid,'hboreqid' value id)ORDER by v521a.bopriority asc, num,dt returning CLOB)FROM(SELECT ");
		   sbQuery.append(" t520.c520_request_id id,t520.c520_master_request_id  master_id,'Field sales' reqtype,'' aname,v700.d_name repdistnm,t521.c205_part_number_id num, ");
		   sbQuery.append(" t521.c521_qty qty,TO_CHAR(t520.c520_required_date,'"+strDateFmt+"') dt,t520.c520_request_to accntid  ");
		   sbQuery.append(" FROM t520_request  t520,t521_request_detail   t521,");
		   sbQuery.append(" (SELECT DISTINCT ad_id,ad_name,region_id,region_name,");
		   sbQuery.append(" d_id,d_name,d_name_en,vp_id,vp_name,v700.d_active_fl,v700.d_compid ");
		   sbQuery.append(" FROM v700_territory_mapping_detail v700 ");
		   sbQuery.append(" WHERE v700.rep_compid NOT IN (SELECT ");
		   sbQuery.append(" c906_rule_id FROM t906_rules WHERE ");
		   sbQuery.append(" c906_rule_grp_id = 'addnl_comp_in_sales' ");
		   sbQuery.append(" AND c906_void_fl IS NULL)) v700 ");
		   sbQuery.append(" WHERE t520.c520_request_id = t521.c520_request_id ");
		   sbQuery.append(" AND t520.c520_status_fl = 10 ");
		   sbQuery.append(" AND t520.c520_request_to IS NOT NULL ");
		   sbQuery.append(" AND t520.c520_void_fl IS NULL ");
		   sbQuery.append(" AND t520.c520_request_to = v700.d_id ");
		   sbQuery.append(" AND t520.c5040_plant_id = '" + getCompPlantId() + "'");
		   sbQuery.append(" AND t520.c207_set_id IS NULL ");
		   sbQuery.append(" AND ( t520.c520_master_request_id IS NULL ");
		   sbQuery.append(" OR t520.c520_master_request_id IN ( ");
		   sbQuery.append(" SELECT c520_request_id ");
		   sbQuery.append(" FROM t520_request ");
		   sbQuery.append(" WHERE c207_set_id IS NULL)) ");
		   sbQuery.append(" UNION ALL SELECT ");
		   sbQuery.append(" t520.c520_request_id          id, ");
		   sbQuery.append(" t520.c520_master_request_id   master_id, ");
		   sbQuery.append(" 'Account'                     reqtype, ");
		   sbQuery.append(" get_account_name(t520.c520_request_to) aname, ");
		   sbQuery.append(" ''  repdistnm, ");
		   sbQuery.append(" t521.c205_part_number_id      num, ");
		   sbQuery.append(" t521.c521_qty                 qty, ");
		   sbQuery.append(" TO_CHAR(t520.c520_required_date,'"+strDateFmt+"')   dt,t520.c520_request_to accntid ");
		   sbQuery.append(" FROM t520_request          t520,t521_request_detail   t521 ");
		   sbQuery.append(" WHERE t520.c520_request_id = t521.c520_request_id ");
		   sbQuery.append(" AND t520.c520_status_fl = 10 ");
		   sbQuery.append(" AND t520.c520_request_to IS NOT NULL ");
		   sbQuery.append(" AND t520.c520_request_for = 40025 ");
		   sbQuery.append(" AND t520.c5040_plant_id = '" + getCompPlantId() + "'");
		   sbQuery.append(" AND t520.c520_void_fl IS NULL ");
		   sbQuery.append(" AND t520.c207_set_id IS NULL ");
		   sbQuery.append(" AND (t520.c520_master_request_id IS NULL ");
		   sbQuery.append(" OR t520.c520_master_request_id IN ( ");
		   sbQuery.append(" SELECT c520_request_id ");
		   sbQuery.append(" FROM t520_request ");
		   sbQuery.append(" WHERE c207_set_id IS NULL)) ");
		   sbQuery.append(" )req,v521_bo_fulfill_list v521a ");
		   sbQuery.append(" WHERE req.num = v521a.c205_part_number_id ");
		    if(!strAccountName.equals(""))	{
	        	 sbQuery
	 	        .append("  AND accntid = '" + strAccountName +"'");	
	        }
//	         if(!strSalesRepName.equals(""))	{
//	        	 sbQuery
//	 	        .append("  AND T501.C703_SALES_REP_ID = '" + strSalesRepName +"'");	
//	        }
	         if(!strReqId.equals(""))	{
	        	 sbQuery
	 	        .append("  AND (id = '" + strReqId +"' OR master_id = '" + strReqId +"')");	
	        }
	         if(!strPartNos.equals(""))	{
	        	 sbQuery
	 	        .append(" AND req.num IN (" + strPartNos +")" );	
	        }
	         if(!strBoPriority.equals(""))	{
	        	 sbQuery
	 	        .append("  AND v521a.bopriority IN (" + strBoPriority +")" );	
	        }
		    String strReturn =  gmDBManager.queryJSONRecord(sbQuery.toString());
		    log.debug("sbQuery.toString():"+sbQuery.toString());
		    if(strReturn == null) {
		    	strReturn = "";
		    }
		 	return strReturn;
		 	}	

	  /**saveBOItemReqDetails : This method use to process back order release
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String saveBOItemReqDetails(HashMap hmParam) throws AppError {
		   
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    GmBOItemReqReleaseBean gmBOItemReqReleaseBean = new GmBOItemReqReleaseBean(getGmDataStoreVO());
		    String strErrorMsg = "";
		    String strResult = "";
		    String strPrepareString = null;
		    String strReqId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
		    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		    String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO")); 
		    String strBOReleaseQty = GmCommonClass.parseNull((String) hmParam.get("BORELEASEQTY")); 
		    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		    HashMap hmReturn = new HashMap(); 
		    gmDBManager.setPrepareString("gm_pkg_ls_bo_item_req_txn.gm_sav_release_bo_item_req", 6);

		    /*
		     * register out parameter and set input parameters
		     */
		    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
		    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);

		    gmDBManager.setString(1, strReqId);
		    gmDBManager.setString(2, strPartNum);
		    gmDBManager.setString(3, strBOReleaseQty);
		    gmDBManager.setString(4, strUserId);
		    gmDBManager.execute();

		    strErrorMsg = GmCommonClass.parseNull(gmDBManager.getString(5));
		    log.debug("strErrorMsg:"+strErrorMsg);
		    String strNewRequestId = GmCommonClass.parseNull(gmDBManager.getString(6));
		    log.debug("strNewRequestId:"+strNewRequestId);
		    if (!strErrorMsg.equals("")){
		    	gmDBManager.close();
		    	strResult = "{"+"\"error\":\"'" + strErrorMsg +"'\""
		    			   +"}"; 
		    }else if(!strNewRequestId.equals("")) {
		    	gmDBManager.commit();
		    	strResult = "{"+ "  \"data\" : \"' success '\""
	    			   + "}"; 
		    }
		    return strResult;
		  } // End of processBackOrderRelease

}
