package com.globus.operations.logistics.beans;

import java.sql.ResultSet; 
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
/**
* GmLotTrackNonTissueRptBean : Contains the methods used for the Lot Track Report for Non Tissue
* author : Agilan Singaravel
*/
public class GmLotTrackNonTissueRptBean extends GmBean {

	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /**
	  *  Default Constructor
	  */
	public GmLotTrackNonTissueRptBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	 /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	public GmLotTrackNonTissueRptBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		
	}
	/**
	 * fetchLotTrackNonTissueDetails - used to fetch Lot Track report for Non Tissue
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchLotTrackNonTissueDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
	    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
	    String strWareHouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSETYPE"));
	    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATEFROM"));
	    String strToDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATETO"));
	    String strLocationId = GmCommonClass.parseNull((String) hmParam.get("STRLOCATIONID"));
	    String strLotTrackTxnRptJSONString = "";
	    StringBuffer strQuery = new StringBuffer();
	
		strQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT('PARTNO' VALUE PARTNO, 'PARTDESC' VALUE PARTDESC,'CNTROLNO' VALUE CNTROLNO, ");
	    strQuery.append(" 'QTY' VALUE QTY, 'EXPIRYDATE' VALUE EXPIRYDATE, ");
	    strQuery.append(" 'LOCATION' VALUE LOCATION, 'WAREHOUSETYPEID' VALUE WAREHOUSETYPEID, ");
	    strQuery.append(" 'WAREHOUSETYPE' VALUE WAREHOUSETYPE, 'COMPANYNM' VALUE COMPANYNM, ");
		strQuery.append(" 'PLANTNM' VALUE PLANTNM, 'CNTLINVID' VALUE CNTLINVID, ");
		strQuery.append(" 'CMPID' VALUE CMPID,'PLTID' value PLTID) ");
		strQuery.append("  ORDER BY CMPID,PLTID,WAREHOUSETYPEID,CNTROLNO RETURNING CLOB) ");
	    strQuery.append("  FROM ");
	    	    
	    if(strWareHouseType.equals("106221")){ //Loaner Inventory
	    	getLoanerInvQuery(strQuery,hmParam);
	    }else if((!strWareHouseType.equals("106221")) && (!strWareHouseType.equals("0"))){   //Loaner Inventory
	    	getInvQuery(strQuery,hmParam);
	    }else{	   
	    	getInvQuery(strQuery,hmParam);
	    	strQuery.append("  UNION ");	    
	    	getLoanerInvQuery(strQuery,hmParam);
	    }
	       
		strQuery.append(" )WHERE rownum <= 2000");
	    log.debug(" Lot Track Report (Sterile) - fetchLotTrackNonTissueDetails => "+ strQuery.toString());
	    String strReturn = gmDBManager.queryJSONRecord(strQuery.toString());
	   return strReturn;
	  }
	/**
	 * getLoanerInvQuery - used to fetch Lot Track Transaction log data based on loaner inventory warehouse type
	 * @param StringBuffer strQuery, HashMap hmParam
	 * @return strQuery
	 * @throws AppError
	 */	
	public StringBuffer getLoanerInvQuery(StringBuffer strQuery, HashMap hmParam) throws AppError{
		
		String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strWareHouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSETYPE"));
		String strFromDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATEFROM"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATETO"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
	    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
	    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
	    String strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANTID"));
	    String strExpRange = GmCommonClass.parseNull((String) hmParam.get("EXPDATERANGE"));
	    String strFieldSales = GmCommonClass.parseNull((String) hmParam.get("STRFIELDSALES"));
	    String strSetId = GmCommonClass.parseNull((String) hmParam.get("STRSETID"));
	  //strLocation,strExpired added for PC-2786  - Add Field Sales Filter in Lot Inventory Report
	    String strLocation = GmCommonClass.parseNull((String) hmParam.get("STRLOCATION"));
	    String strExpired = GmCommonClass.parseNull((String) hmParam.get("STREXPIRED")); 
		if(strWareHouseType.equals("106221")){  //Loaner Inventory
			strQuery.append(" ( ");
		}
		
		strQuery.append("  SELECT distinct t5072.c205_part_number_id PARTNO, REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC, ");
	    strQuery.append("  t5072.c5072_control_number CNTROLNO, t5072.c5072_curr_qty QTY, c5072_set_part_lot_qty_id  CNTLINVID, ");
	    strQuery.append("  to_char(t2550.c2550_expiry_date,'"+strDateFmt+"') EXPIRYDATE, ");
		strQuery.append("  nvl(t701.c701_distributor_name, get_dist_rep_name(c504_ship_to_id)) LOCATION, ");
		strQuery.append("  106221 WAREHOUSETYPEID, ");
		strQuery.append("  t901.c901_code_nm WAREHOUSETYPE, ");
		strQuery.append("  t1900.c1900_company_name COMPANYNM, ");
		strQuery.append("  t5040.c5040_plant_name PLANTNM, ");
		strQuery.append("  t504.c1900_company_id CMPID,t504.c5040_plant_id PLTID ");
	    strQuery.append("  FROM T5072_SET_PART_LOT_QTY t5072,  T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550 ");
		strQuery.append("  ,T504_CONSIGNMENT t504, T5070_SET_LOT_MASTER t5070, ");
		strQuery.append("  T901_CODE_LOOKUP t901,T1900_COMPANY t1900, T5040_PLANT_MASTER t5040, ");
		strQuery.append("  t701_distributor t701, t703_sales_rep t703, v205_sterile_parts v205 ");
	    strQuery.append("  WHERE t2550.c205_part_number_id = t205.c205_part_number_id ");
	    strQuery.append("  AND  t2550.c2550_control_number = t5072.c5072_control_number ");
		strQuery.append("  AND t5072.c205_part_number_id = v205.c205_part_number_id ");
	    strQuery.append("  AND t205.c205_part_number_id = t5072.c205_part_number_id  ");
	    strQuery.append("  AND t5072.c5072_last_updated_txn_id = t504.c504_consignment_id  ");
	    strQuery.append("  AND t5070.c901_txn_id = t504.c504_consignment_id  ");
		strQuery.append("  AND t5072.c5070_set_lot_master_id = t5070.c5070_set_lot_master_id ");
		strQuery.append("  AND t901.c901_code_id = '106221' ");
		strQuery.append("  AND t504.c701_distributor_id = t701.c701_distributor_id (+) ");
		strQuery.append("  AND t504.c504_ship_to_id = t703.c703_sales_rep_id (+) ");
		strQuery.append("  AND t504.c1900_company_id = t1900.c1900_company_id ");
		strQuery.append("  AND t504.c5040_plant_id = t5040.c5040_plant_id ");

	    if(!strLotNumber.equals("")){
		    strQuery.append(" AND UPPER(t5072.c5072_control_number)  = " );
		    strQuery.append(" UPPER('" );
		    strQuery.append(strLotNumber);
		    strQuery.append("')");
	    }
	    if (strSearch.equals("0") && (!strPartNum.equals(""))) {  	
	    	strQuery.append(" AND t5072.c205_part_number_id like upper('%"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIT") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5072.c205_part_number_id like upper('"+strPartNum+"')  ");
	    }else if(strSearch.equals("LIKEPRE") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5072.c205_part_number_id like upper('"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIKESUF") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5072.c205_part_number_id like upper('%"+strPartNum+"')  ");
	    }
	    if(!strFromDate.equals("") && !strToDate.equals("")){   //If Expiry Date (From & To) is selected in the filter
	        strQuery.append(" AND t2550.c2550_expiry_date BETWEEN TO_DATE('" + strFromDate +"','"+strDateFmt+ "') AND TO_DATE('" + strToDate +"','"+strDateFmt+"')");
	    }
	    if(strExpRange.equals("expired")){
	    	 strQuery.append(" AND t2550.c2550_expiry_date < sysdate ");
	    }if(strExpRange.equals("others")){
	    	strQuery.append(" AND t2550.c2550_expiry_date > sysdate+91 ");
	    }
	    if(!strCompanyId.equals("") && !strCompanyId.equals("0")){
	    	strQuery.append(" AND t504.c1900_company_id = '"+strCompanyId+"' ");
	    }
	    if(!strPlantId.equals("") && !strPlantId.equals("0")){
	    	strQuery.append(" AND t504.c5040_plant_id = '"+strPlantId+"' ");
	    }
	    if(!strFieldSales.equals("") && !strFieldSales.equals("0")){
	    	strQuery.append(" AND t701.c701_distributor_name = '"+strFieldSales+"' ");
	    }
	    if(!strSetId.equals("") && !strSetId.equals("0")){
			strQuery.append(" AND nvl(t504.C207_SET_ID,-999) = NVL('"+strSetId+"',-999) ");
		} 
	  //strLocation,strExpired added for PC-2786  - Add Field Sales Filter in Lot Inventory Report 
	    if(!strLocation.equals("")) {
	    	strLocation = strLocation.replaceAll("&", "' || chr(38) || '");
	    	strQuery.append("AND upper(nvl(t701.c701_distributor_name, get_dist_rep_name(c504_ship_to_id))) LIKE upper('%"+strLocation+"%') ");
	    }
	    if(strExpired.equals("Y")) {                                        
	    	 strQuery.append(" AND t2550.c2550_expiry_date < CURRENT_DATE ");
	    }
		strQuery.append(" AND t5072.c5072_void_fl IS NULL AND c504_void_fl IS NULL AND c701_void_fl IS NULL");
		strQuery.append(" AND c5070_void_fl IS NULL AND c5040_void_fl IS NULL AND c901_void_fl IS NULL AND C1900_VOID_FL IS NULL ");

	    return strQuery;
	}
	/**
	 * getInvQuery - used to fetch Lot Track Transaction log data based on inventory warehouse type not equal to loaner and also choose one
	 * @param StringBuffer strQuery, HashMap hmParam
	 * @return strQuery
	 * @throws AppError
	 */	
	public StringBuffer getInvQuery(StringBuffer strQuery, HashMap hmParam) throws AppError{
		
		String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strWareHouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSETYPE"));
		String strFromDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATEFROM"));
		String strToDate = GmCommonClass.parseNull((String) hmParam.get("EXPIRYDATETO"));		
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
	    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
	    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
	    String strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANTID"));
	    String strExpRange = GmCommonClass.parseNull((String) hmParam.get("EXPDATERANGE"));
	    String strFieldSales = GmCommonClass.parseNull((String) hmParam.get("STRFIELDSALES"));
	    String strSetId = GmCommonClass.parseNull((String) hmParam.get("STRSETID"));
	    //strLocation, strExpired added for PC-2786 - Add Field Sales Filter in Lot Inventory Report
	    String strLocation = GmCommonClass.parseNull((String) hmParam.get("STRLOCATION"));
	    String strExpired = GmCommonClass.parseNull((String) hmParam.get("STREXPIRED"));
	    
		strQuery.append(" (SELECT distinct t5080.c205_part_number_id PARTNO, ");
		strQuery.append(" REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') PARTDESC, ");
		strQuery.append(" t5080.c2550_control_number CNTROLNO, ");
		strQuery.append(" t5080.c5080_qty QTY, t5080.c5080_control_number_inv_id CNTLINVID, ");
		strQuery.append(" to_char(t2550.c2550_expiry_date,'"+strDateFmt+"') EXPIRYDATE, ");
		strQuery.append(" t5080.c5080_location_name LOCATION, ");
		strQuery.append(" t5080.c901_warehouse_type WAREHOUSETYPEID, ");
		strQuery.append(" t901.c901_code_nm WAREHOUSETYPE, ");
		strQuery.append(" t1900.c1900_company_name COMPANYNM, ");
		strQuery.append(" t5040.c5040_plant_name PLANTNM, ");
		strQuery.append(" t5080.c1900_company_id CMPID,t5080.c5040_plant_id PLTID ");
		strQuery.append(" FROM T5080_LOT_TRACK_INV t5080, T205_PART_NUMBER t205, T2550_PART_CONTROL_NUMBER t2550, ");
		strQuery.append(" T901_CODE_LOOKUP t901,T1900_COMPANY t1900, T5040_PLANT_MASTER t5040, v205_sterile_parts v205 ");
		
		if(!strSetId.equals("") && !strSetId.equals("0")){
			strQuery.append(" ,T5081_LOT_TRACK_INV_LOG t5081 ");
		}
		
		strQuery.append(" WHERE t5080.c205_part_number_id = t205.c205_part_number_id ");
		strQuery.append(" AND t5080.c2550_control_number = t2550.c2550_control_number(+) ");
		strQuery.append(" AND t205.c205_part_number_id  = t2550.c205_part_number_id(+) ");
		strQuery.append(" AND t5080.c901_warehouse_type = t901.c901_code_id ");
		strQuery.append(" AND t5080.c1900_company_id = t1900.c1900_company_id ");
		strQuery.append(" AND t5080.c5040_plant_id = t5040.c5040_plant_id ");
		strQuery.append(" AND t5080.c205_part_number_id = v205.c205_part_number_id ");
		if(!strSetId.equals("") && !strSetId.equals("0")){
			strQuery.append(" AND t5080.c5080_control_number_inv_id = t5081.c5080_control_number_inv_id "); 
			strQuery.append(" AND nvl(t5081.C207_SET_ID,-999) = NVL('"+strSetId+"',-999) ");
			strQuery.append(" AND t5081.C5081_void_fl is null "); 
		}  	    
		if(!strLotNumber.equals("")){
			strQuery.append(" AND UPPER(t5080.c2550_control_number)  = " );
			strQuery.append(" UPPER('" );
			strQuery.append(strLotNumber);
			strQuery.append("')");
		}
		if (strSearch.equals("0") && (!strPartNum.equals(""))) {  	
	    	strQuery.append(" AND t5080.c205_part_number_id like upper('%"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIT") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5080.c205_part_number_id like upper('"+strPartNum+"')  ");
	    }else if(strSearch.equals("LIKEPRE") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5080.c205_part_number_id like upper('"+strPartNum+"%')  ");
	    }else if(strSearch.equals("LIKESUF") && (!strPartNum.equals(""))){
	    	strQuery.append(" AND t5080.c205_part_number_id like upper('%"+strPartNum+"')  ");
	    } 
		if(!strWareHouseType.equals("0")){
		    strQuery.append(" AND t5080.c901_warehouse_type = ");
		    strQuery.append(strWareHouseType);
		}
		if(!strFromDate.equals("") && !strToDate.equals("")){   //If Expiry Date (From & To) is selected in the filter
		    strQuery.append(" AND t2550.c2550_expiry_date BETWEEN TO_DATE('" + strFromDate +"','"+strDateFmt+ "') AND TO_DATE('" + strToDate +"','"+strDateFmt+"')");
		}
		if(strExpRange.equals("expired")){
	    	 strQuery.append(" AND t2550.c2550_expiry_date < sysdate ");
	    }if(strExpRange.equals("others")){
	    	strQuery.append(" AND t2550.c2550_expiry_date > sysdate+91 ");
	    }
	    if(!strCompanyId.equals("") && !strCompanyId.equals("0")){
	    	strQuery.append(" AND t5080.c1900_company_id = '"+strCompanyId+"' ");
	    }
	    if(!strPlantId.equals("") && !strPlantId.equals("0")){
	    	strQuery.append(" AND t5080.c5040_plant_id = '"+strPlantId+"' ");
	    }
	    if(!strFieldSales.equals("") && !strFieldSales.equals("0")){
	    	strQuery.append(" AND t5080.c5080_location_name = '"+strFieldSales+"' ");
	    }
	    //strLocation, strExpired added for PC-2786 - Add Field Sales Filter in Lot Inventory Report
	    if(!strLocation.equals("")) {
	    	strLocation = strLocation.replaceAll("&", "' || chr(38) || '");
	    	strQuery.append(" AND upper(t5080.c5080_location_name) like upper('%"+strLocation+"%')  ");
	    }
	    if(strExpired.equals("Y")){
	    	 strQuery.append(" AND t2550.c2550_expiry_date < CURRENT_DATE ");
	    }
		strQuery.append(" AND c5040_void_fl IS NULL AND c901_void_fl IS NULL AND C1900_VOID_FL IS NULL AND C5080_VOID_FL IS NULL ");
	    return strQuery;
	}
	
	/**
	   * This method is used to fetch Lot Track Details
	   * @param String control number Inv Id
	   * @return
	   * @throws AppError
	   */
	  public ArrayList fetchLotTrackDetails(String strCntrlNumInvId) {
		    ArrayList alList = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fch_lot_track_details", 2);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.setString(1, strCntrlNumInvId);			
			gmDBManager.execute();
			
			alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
			log.debug("alList==>"+alList);
			gmDBManager.close();
			return alList;
		}
	  
	  /**
	   * This method is used to save Lot Track Details
	   * @return
	   * @throws AppError
	   */
	  public void saveLotTrackDetails(HashMap hmParam) {
		    ArrayList alList = new ArrayList();
		    String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBER"));
		    String strLotNumber = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMBER"));
		    String strQuantity = GmCommonClass.parseNull((String) hmParam.get("QUANTITY"));
		    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		    String strPlantId = GmCommonClass.parseNull((String) hmParam.get("PLANTID"));
		    String strWarehouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSETYPE"));
		    String strLocation = GmCommonClass.parseNull((String) hmParam.get("STRLOCATIONID"));
		    String strCntrlInvId = GmCommonClass.parseNull((String) hmParam.get("STRCNTRLNUMINVID"));
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    
			gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_upd_lot_track_inventory", 8);
			gmDBManager.setString(1, strCntrlInvId);
			gmDBManager.setString(2, strPartNumber);
			gmDBManager.setString(3, strLotNumber);
			gmDBManager.setString(4, strQuantity);
			gmDBManager.setString(5, strWarehouseType);
			gmDBManager.setString(6, strLocation);
			gmDBManager.setString(7, strCompanyId);
			gmDBManager.setString(8, strPlantId);
			gmDBManager.execute();
			gmDBManager.commit();
		}
	  
	  /**
	   * getDistributorList - This method will fetch Distributor lists
	   * 
	   * @param String strUsername
	   * @param String strPassword
	   * @return HashMap
	   * @exception AppError
	   */
	  public ArrayList getDistributorList(HashMap hmParam) throws AppError {
		  log.debug("hmParam getDistributorList lotrac==>"+hmParam);
	    String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
	    String strDistTyp = GmCommonClass.parseNull((String) hmParam.get("DISTTYP"));
	    String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
	    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
	    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));
	    String strExclDistTyp = GmCommonClass.parseNull((String) hmParam.get("EXCL_DISTTYP"));
	    String strScreenfrm = GmCommonClass.parseNull((String) hmParam.get("SCREEN"));
	    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
	    ArrayList alReturn = new ArrayList();
	    try {

	      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	      StringBuffer sbQuery = new StringBuffer();
	      sbQuery
	          .append(" SELECT  C701_DISTRIBUTOR_ID ID,DECODE('"
	              + getComplangid()
	              + "', '103097',NVL(C701_DISTRIBUTOR_NAME_EN, C701_DISTRIBUTOR_NAME), C701_DISTRIBUTOR_NAME) NAME , 'D' || C701_DISTRIBUTOR_ID DID, NVL(t710.c901_division_id,'100823') DDIV , LOWER(C701_DISTRIBUTOR_NAME) distname");
	      sbQuery.append(" FROM T701_DISTRIBUTOR ");
	      // Instead of function we are getting the division id from master table
	      sbQuery.append(" ,(SELECT DISTINCT t710_tmp.c901_area_id, t710_tmp.c901_division_id ");
	      sbQuery.append(" FROM t710_sales_hierarchy t710_tmp ");
	      sbQuery.append(" WHERE t710_tmp.c710_active_fl = 'Y' ");
	      sbQuery.append(" AND t710_tmp.c710_void_fl IS NULL) t710 ");
	      if(strScreenfrm.equals("4000338") || strScreenfrm.equals("ReloadRpt")){   //Field Sales
	    	  sbQuery.append(" WHERE (C701_VOID_FL  IS NULL OR C701_DISTRIBUTOR_ID = get_rule_value('DISTRIBUTOR','TAGST')) AND (c901_ext_country_id is NULL ");
	      }else{
	    	  sbQuery.append(" WHERE C701_VOID_FL IS NULL AND (c901_ext_country_id is NULL ");
	      }     
	      sbQuery.append(" OR c901_ext_country_id  in (select country_id from v901_country_codes))");
	      if (strFilter.equals("Active")) {
	        sbQuery.append(" AND ");
	        sbQuery.append(" C701_ACTIVE_FL = 'Y'");
	      }
	      if (!strExclDistTyp.equals("")) {
	        sbQuery.append(" AND c901_distributor_type NOT IN ( ");
	        sbQuery.append(strExclDistTyp);
	        sbQuery.append(" ) ");
	      }

	      if (!strDistTyp.equals("")) {
	        sbQuery.append(" AND c901_distributor_type = ");
	        sbQuery.append(strDistTyp);
	      }
	      if (!strShipTyp.equals("")) {
	        sbQuery.append(" AND C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120," + strRepID
	            + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='" + strRepID
	            + "'))) ");

	      }
	      sbQuery.append(" AND c701_region = t710.c901_area_id (+) ");
	      // Filtering Distributor based on all the companies associated with the plant.
	      if (strDistFilter.equals("COMPANY-PLANT")) {
	        sbQuery
	            .append(" AND ( C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = '"
	                + getCompPlantId() + "'  AND C5041_VOID_FL IS NULL )");
	        sbQuery.append(" OR C1900_COMPANY_ID IS NULL )  ");
	        sbQuery
	            .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
	        sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
	            + getCompId() + "') ");

	      }else {
	        sbQuery
	            .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
	        sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
	            + getCompId() + "') ");
	      }
	      sbQuery.append(" ORDER BY distname ");
	      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
	      log.debug(" Query to get the Distributor List is " + sbQuery.toString() + "size of AList "
	          + alReturn.size());

	    } catch (Exception e) {
	      GmLogError.log("Exception in GmCustomerBean:getDistributorList", "Exception is:" + e);
	    }
	    return alReturn;
	  } // End of getDistributorList
	  
	  /**
	   * This method is used to fetch Account lists
	   * @return
	   * @throws AppError
	   */
	  public ArrayList reportAccount(String StrAcctType) {
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    log.debug("StrAcctType========== "+StrAcctType);

		    String strBeanMsg = "";
		    String strMsg = "";
		    String strPrepareString = null;

		    ArrayList alReturn = new ArrayList();
		    ResultSet rs = null;

		    gmDBManager.setPrepareString("gm_pkg_op_lot_detail_report.gm_fetch_account_list", 3);
		    /*
		     * register out parameter and set input parameters
		     */
		    gmDBManager.setString(3, StrAcctType);
		    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

		    gmDBManager.execute();
		    strBeanMsg = gmDBManager.getString(1);
		    rs = (ResultSet) gmDBManager.getObject(2);
		    alReturn = gmDBManager.returnArrayList(rs);
		    gmDBManager.close();
		    log.debug("alReturn---------------- "+alReturn);
		    return alReturn;
		}
}

