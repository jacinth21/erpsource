package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonLogInterface;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import org.apache.commons.beanutils.RowSetDynaClass;

public class GmLoanerRequestReportBean extends GmBean implements GmCommonLogInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

  public GmLoanerRequestReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmLoanerRequestReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * reportLoanerOpenRequests - This Method is used fetch data for viewing Pending Requests for
   * Product Loaners/InHouse Loaners
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportLoanerOpenRequests(HashMap hmParam) throws AppError {
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strSetName = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strDtType = GmCommonClass.parseNull((String) hmParam.get("DTTYPE"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPrdReqId = GmCommonClass.parseNull((String) hmParam.get("PRDREQID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strEventId = GmCommonClass.parseNull((String) hmParam.get("EVENTID"));
    String strEmpId = GmCommonClass.parseNull((String) hmParam.get("EMPID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParam.get("TXNTYPE"));
    String strScreenType = GmCommonClass.parseNull((String) hmParam.get("SCREENTYPE"));
    String strInputParam = GmCommonClass.parseNull((String) hmParam.get("INPUTPARAM"));
    String strHInputParam = GmCommonClass.parseNull((String) hmParam.get("HINPUTPARAM"));
    String strStatusParam = GmCommonClass.parseNull((String) hmParam.get("TSTATUSPARAM"));
    String strSelectedStatus = GmCommonClass.parseNull((String) hmParam.get("SELECTEDSTATUS"));
    String strHSelectedStatus = GmCommonClass.parseNull((String) hmParam.get("HSELECTEDSTATUS"));
    String strApplDateFmt = getCompDateFmt();
    String strTimeDateFmt = strApplDateFmt + " HH:MI AM";
    String strFilter = "";

    strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_PROCESS_REQ"));// Rule
    if (strScreenType.equals("LoanerStRpt")) {
      strFilter =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_OVERALLBY_ST"));// LOANER_OVERALLBY_ST-Loaner
                                                                                                  // ("F"
                                                                                                  // icon
                                                                                                  // in
                                                                                                  // overallbystatus
                                                                                                  // screen
                                                                                                  // fetching
                                                                                                  // based
                                                                                                  // on
                                                                                                  // plant)
    } // LOANER_PROCESS_REQ:=plant
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    StringBuffer sbQuery = new StringBuffer();
    StringBuffer sbMergeQuery = new StringBuffer();

    String strType = "4127";
    strType = (strTxnType.equals("InhouseLoanerRequests")) ? "4119" : strType;
    String strPartNum = "";
    strDistId = (strDistId.equals("0")) ? "" : strDistId;
    strDtType = (strDtType.equals("0")) ? "" : strDtType;
    strStatus = (strStatus.equals("0")) ? "" : strStatus;
    strEventId = (strEventId.equals("0")) ? "" : strEventId;
    strEmpId = (strEmpId.equals("0")) ? "" : strEmpId;
    String strSubdtType = "";
    ArrayList alResult;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" SELECT extdata.*,T7103.C7103_Name EVTNM, get_user_name (T7104.C7104_Created_By) EMPNM ");
      sbQuery.append(" , NVL(TO_CHAR (t7104.C7104_Created_date,'" + strTimeDateFmt
          + "'),extdata.cdate) requested_date");
      sbQuery.append(" , TO_CHAR (T7103.C7103_Start_Date, '" + strApplDateFmt + "') STRTDATE ");
      sbQuery.append(" , TO_CHAR (T7103.C7103_End_Date, '" + strApplDateFmt + "') ENDDATE ");
      sbQuery.append(" FROM ( ");
    }
    sbQuery
        .append(" SELECT t526.c525_product_request_id PDTREQID, gm_pkg_op_consignment.get_hold_flg(t504alt.c504_consignment_id) holdfl, TO_CHAR (t525.c525_requested_date, '"
            + strApplDateFmt + "') reqsdt ");
    sbQuery
        .append(" , get_distributor_name (t525.c525_request_for_id) dname, get_code_name (t525.c901_ship_to) shiptonm");
    sbQuery
        .append(" , get_user_name (t525.c525_created_by) cuser, TO_CHAR (t525.c525_created_date, '"
            + strTimeDateFmt + "') cdate");
    sbQuery.append(" , t526.c526_qty_requested qty, TO_CHAR (t526.c526_required_date, '"
        + strApplDateFmt + "') reqrdt");
    sbQuery.append(" , TO_CHAR (t525.c525_surgery_date, '" + strApplDateFmt + "')  surgdt");
    sbQuery.append(" , t525.c525_request_for_id reqforid, t525.c901_ship_to shipto");
    sbQuery
        .append(" , t526.c526_product_request_detail_id reqdetailid, NVL(t526.c207_set_id,t526.C205_Part_Number_Id) setid");
    sbQuery
        .append(" , DECODE(t526.c207_set_id,NULL,get_partnum_desc (T526.c205_part_number_id),get_set_name (t526.c207_set_id)) setnm, t907.c907_ship_to_id shiptoid");
    sbQuery.append(" , t907.c901_delivery_carrier shipcarr, t907.c901_delivery_mode shipmode");
    sbQuery.append(" , t907.c106_address_id shipaddid");
    sbQuery.append(" , get_loaner_request_status(t526.c526_product_request_detail_id) stsfl");
    sbQuery
        .append(" , DECODE(t526.c526_status_fl, '20', gm_pkg_op_loaner.get_loaner_status (t504acl.c504a_status_fl), '') cn_status");
    sbQuery.append(" , t504alt.c504_consignment_id consignid");
    sbQuery.append(" , t504acl.c504a_etch_id etchid");
    sbQuery.append(" , t526.c526_status_fl stsid");
    sbQuery.append(" , t526.c526_loaner_escalation_fl ascfl");
    sbQuery
        .append(" ,get_loaner_log_flag(t526.c526_product_request_detail_id,26240693) ESCL_LOG_FLAG ");
    sbQuery.append(" , t525.c525_created_date crdate");
    sbQuery
        .append(" FROM t526_product_request_detail t526, t525_product_request t525, t907_shipping_info t907");
    sbQuery.append(", t504a_consignment_loaner t504acl ");
    sbQuery
        .append(", (SELECT  t504a.c504_consignment_id, c526_product_request_detail_id from t504a_consignment_loaner t504a, ");
    sbQuery.append(" (SELECT  t504alt.c504_consignment_id, t526.c526_product_request_detail_id ");
    sbQuery
        .append(" FROM t504a_loaner_transaction t504alt,t526_product_request_detail t526,t525_product_request t525 ");
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" ,t7104_case_set_information t7104 , t7103_schedule_info t7103 , t7100_case_information t7100");
    }
    sbQuery
        .append(" WHERE t504alt.c526_product_request_detail_id IS NOT NULL AND t504alt.c504a_void_fl IS NULL AND t526.c526_product_request_detail_id = t504alt.c526_product_request_detail_id ");
    sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" AND t504alt.c526_product_request_detail_id = t7104.c526_product_request_detail_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7103.c7100_case_info_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7104.c7100_case_info_id ");
    }
    if (!strDtType.equals("")) {
      if (strDtType.equals("108702")) {
        strSubdtType = " t525.C525_SURGERY_DATE ";
      } else if (strDtType.equals("108703")) {
        strSubdtType = " t526.C526_REQUIRED_DATE ";
      } else if (strDtType.equals("108704")) {
        strSubdtType = " t525.C525_REQUESTED_DATE ";
      } else if (strDtType.equals("ESD")) {
        strSubdtType = " T7103.C7103_Start_Date ";
      } else if (strDtType.equals("EED")) {
        strSubdtType = " T7103.C7103_End_Date ";
      }
      sbQuery.append(" AND " + strSubdtType + " >= TO_DATE ('");
      sbQuery.append(strFromDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");
      sbQuery.append(" AND " + strSubdtType + " <= TO_DATE ('");
      sbQuery.append(strToDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");

    } else {
      sbQuery.append(" AND t526.c526_required_date >= TO_DATE ('");
      sbQuery.append(strFromDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");
      sbQuery.append(" AND t526.c526_required_date <= TO_DATE ('");
      sbQuery.append(strToDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");
    }
    sbQuery
        .append(" GROUP BY t504alt.c504_consignment_id, t526.c526_product_request_detail_id) t504alt where t504a.c504_consignment_id = t504alt.c504_consignment_id AND t504a.c504a_status_fl != 60 AND t504a.c504a_void_fl IS NULL) t504alt ");
    sbQuery.append(" WHERE c901_request_type = ");
    sbQuery.append(strType);
    sbQuery.append(" AND t907.C901_SOURCE = 50185");
    if (strInputParam.equals("ToPutAway") || strHInputParam.equals("ToPutAway")) {
      sbQuery.append(" AND t504acl.c504A_status_fl = 58 ");
    }
    if (strInputParam.equals("ToPick") || strHInputParam.equals("ToPick")) {
      sbQuery.append(" AND t504acl.c504A_status_fl = 7 ");
    }
    if (strInputParam.equals("PendingShipping") || strHInputParam.equals("PendingShipping")) {
      sbQuery.append(" AND t504acl.c504A_status_fl = 10 ");
    }
    if ((strInputParam.equals("sameday") && strSelectedStatus.equals("Open"))
        || (strHInputParam.equals("sameday") && strHSelectedStatus.equals("Open"))) {
      sbQuery.append(" AND t907.c901_Delivery_mode = 5038 AND t526.c526_status_fl = 10 ");
    }
    if ((strInputParam.equals("sameday") && strSelectedStatus.equals("ToPick"))
        || (strHInputParam.equals("sameday") && strHSelectedStatus.equals("ToPick"))) {
      sbQuery.append(" AND t907.c901_Delivery_mode = 5038 AND t504acl.c504A_status_fl = 7  ");
    }
    if ((strInputParam.equals("sameday") && strSelectedStatus.equals("ToShip"))
        || (strHInputParam.equals("sameday") && strHSelectedStatus.equals("ToShip"))) {
      sbQuery.append(" AND t907.c901_Delivery_mode = 5038 AND t504acl.c504A_status_fl = 10 ");
    }
    sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id");
    sbQuery.append(" AND t525.c525_product_request_id = t907.c907_ref_id(+)");
    sbQuery.append(" AND t525.c525_void_fl IS NULL");
    sbQuery.append(" AND t526.c526_void_fl IS NULL");
    sbQuery
        .append(" AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504alt.C526_PRODUCT_REQUEST_DETAIL_ID(+)");
    sbQuery.append(" AND t504alt.c504_consignment_id = t504acl.c504_consignment_id(+) ");
    sbQuery.append(" AND t907.c907_void_fl(+) IS NULL");
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    if (!strSetId.equals("")) {
      sbQuery.append(" AND  c207_set_id = '");
      sbQuery.append(strSetId);
      sbQuery.append("'");
    }
    if (!strStatus.equals("")) {
      sbQuery.append(" AND  t526.c526_status_fl in ( ");
      sbQuery.append(strStatus);
      sbQuery.append(" )");
    }
    if (!strSetName.equals("")) {
      sbQuery.append(" and upper(get_set_name (t526.c207_set_id )) like upper('%" + strSetName
          + "%')");
    }

    if (!strDistId.equals("")) {
      sbQuery.append(" AND  t525.c525_request_for_id = '");
      sbQuery.append(strDistId);
      sbQuery.append("'");
    }
    if (!strPrdReqId.equals("")) {
      sbQuery.append(" AND  t526.c525_product_request_id=");
      sbQuery.append(strPrdReqId);
    }
    if (!strDtType.equals("")) {
      String strdt = "";
      if (strDtType.equals("108702")) {
        strdt = " t525.C525_SURGERY_DATE ";
      } else if (strDtType.equals("108703")) {
        strdt = " t526.C526_REQUIRED_DATE ";
      } else if (strDtType.equals("108704")) {
        strdt = " t525.C525_REQUESTED_DATE ";
      } else if (strDtType.equals("ESD")) {
        strdt = " T7103.C7103_Start_Date ";
      } else if (strDtType.equals("EED")) {
        strdt = " T7103.C7103_End_Date ";
      }
      sbMergeQuery.append(" and");
      sbMergeQuery.append(strdt);
      sbMergeQuery.append(" >= TO_DATE('");
      sbMergeQuery.append(strFromDate);
      sbMergeQuery.append("', '" + strApplDateFmt + "' ) ");
      sbMergeQuery.append(" and");
      sbMergeQuery.append(strdt);
      sbMergeQuery.append("  <= TO_DATE('");
      sbMergeQuery.append(strToDate);
      sbMergeQuery.append("', '" + strApplDateFmt + "')");

      if ((!strDtType.equals("ESD") && !strDtType.equals("EED"))) {
        sbQuery.append(sbMergeQuery);
      }
    }
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" ) extdata, t7104_case_set_information t7104 , t7103_schedule_info t7103 , t7100_case_information t7100");
      sbQuery.append(" WHERE extdata.reqdetailid = t7104.c526_product_request_detail_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7103.c7100_case_info_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7104.c7100_case_info_id ");
      sbQuery.append(" AND t7104.c7104_void_fl IS NULL ");
      sbQuery.append(" AND t7100.c7100_void_fl IS NULL ");
      sbQuery.append(" AND t7100.c901_case_status != 19526 "); // exclude rescheduled requests.
      sbQuery.append(" AND t7100.c901_type = 1006504 "); // 1006504 - event

      if (!strEventId.equals("")) {
        sbQuery.append(" AND t7100.c7100_case_info_id = '" + strEventId + "' ");
      }
      if (!strEmpId.equals("")) {
        sbQuery.append(" AND t7104.c7104_created_by = '" + strEmpId + "' ");
      }

      if ((strDtType.equals("ESD") || strDtType.equals("EED"))) {
        sbQuery.append(sbMergeQuery);
      }
    }
    sbQuery.append(" order by crdate");

    log.debug("LoanerOpenRequests Query" + sbQuery.toString());

    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alResult;

  }// End of reportLoanerOpenRequests

  /**
   * updateLoanerEscalation - This Method is used to update escalation flag detail
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void updateLoanerEscalation(HashMap hmParam) throws AppError {

    ArrayList alRequest = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strReqDetID = "";
    String strEscFl = "";
    String strLog = "";
    String strUserId = "";
    // String strReqStatus = "";
    strReqDetID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    strEscFl = GmCommonClass.parseNull((String) hmParam.get("HESCFL"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("HTYPE"));
    strLog = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    // strReqStatus = GmCommonClass.parseNull((String) hmParam.get("REQSTATUS"));

    gmDBManager.setPrepareString("gm_pkg_op_loaner_request.gm_upd_loaner_esclfl", 5);
    gmDBManager.setString(1, strReqDetID);
    gmDBManager.setString(2, strEscFl);
    gmDBManager.setString(3, strLog);
    gmDBManager.setString(4, strType);
    // gmDBManager.setString(5, strReqStatus);
    gmDBManager.setString(5, strUserId);
    gmDBManager.execute();

    gmDBManager.commit();
  }

  /**
   * getXmlGridData Xml content generation for In-House Loaner Report &Report - Loaner Requests grid
   * 
   * @author
   * @param ArrayList and HashMap
   * @return String
   * @exception AppError
   */

  public String getXmlGridData(ArrayList alValues, HashMap hmParam) {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alResult", alValues);
    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest", strSessLocale));
    templateUtil.setTemplateSubDir("operations/logistics/templates");
    templateUtil.setTemplateName("GmLoanerRequestsReport.vm");
    return templateUtil.generateOutput();
  }


  /**
   * getCallLog - This method is used to fetch escalation log details
   * 
   * @param String
   * @return RowSetDynaClass
   * @exception AppError
   */
  public RowSetDynaClass getCallLog(String strID, String strType, String strJNDIConnection)
      throws AppError {
    GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
    RowSetDynaClass rdLogDetails = null;

    gmDBManager.setPrepareString("gm_pkg_op_loaner_request_rpt.gm_fch_loaner_log_dtls", 3);
    gmDBManager.setString(1, strID);
    gmDBManager.setString(2, strType);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdLogDetails = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return rdLogDetails;

  } // End of getCallLog

}
