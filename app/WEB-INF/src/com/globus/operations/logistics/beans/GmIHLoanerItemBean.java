package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmIHLoanerItemBean extends GmCommonBean {
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance();
	
	/**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
	public GmIHLoanerItemBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
	}
	public GmIHLoanerItemBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	/**
	 * fetchPendingRtnIHLoanerItem  This method will fetch In-House Loaner Item Details.
	 * @param 		hmParam
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	 
  
	public ArrayList fetchPendingRtnIHLoanerItem(HashMap hmParam) throws AppError {		
		ArrayList alReturn 		  = new ArrayList();
		String strEventName 	  = GmCommonClass.parseNull((String) hmParam.get("EVENTNAME"));
		String strLoanToName 	  = GmCommonClass.parseNull((String) hmParam.get("EVENTLOANTONAME"));
		String strPurpose 		  = GmCommonClass.parseNull((String) hmParam.get("EVENTPURPOSE"));
		String strTransactionID   = GmCommonClass.parseNull((String) hmParam.get("TRANSACTIONID"));
		String strpartNum 		  = GmCommonClass.parseNull((String) hmParam.get("PARTNUMER"));
		String strLoanType 		  = GmCommonClass.parseNull((String) hmParam.get("LOANTYPE"));
		String strLoanName 		  = GmCommonClass.parseNull((String) hmParam.get("LOANTONAME"));
		String strDateFmt 		  = GmCommonClass.parseNull((String)hmParam.get("APPLNDATEFMT"));
		String strStartDt 		  = GmCommonClass.parseNull((String) hmParam.get("EVENTSTARTDATE"));
		String strEndDt 		  = GmCommonClass.parseNull((String) hmParam.get("EVENTENDDATE"));	
		String strDtType 		  = GmCommonClass.parseNull((String) hmParam.get("DTTYPE"));
		
		try {
			GmDBManager gmDBManager     = new GmDBManager();
			StringBuffer sbQuery 		= new StringBuffer();
			StringBuffer sbMergeQuery   = new StringBuffer();
			sbQuery.append(" SELECT DISTINCT T504.C504_Consignment_Id CONSIGNID, event_dtl.eventName EVENTNAME, get_user_name (event_dtl.loantoid) LOANEDTONAME");
			sbQuery.append(" ,get_code_name(event_dtl.eventpurpose) PURPOSE, Get_Code_Name (T504.C504_Type) TYPE, T504.C504_Ship_Date LOANEDON");
			sbQuery.append(" ,T504.C504_Return_Date EXPRTNDT, NVL (TRUNC (Sysdate) - T504.C504_Ship_Date, 0) DAYSELAPSED, get_work_days_count (TO_CHAR (TRUNC (T504.C504_Return_Date),'mm/dd/yyyy'), TO_CHAR (TRUNC (SYSDATE), 'mm/dd/yyyy')) DAYSOVERDUE");
			sbQuery.append(" FROM T504_Consignment T504, T506_Returns t506, (");
			  sbQuery.append(" SELECT event_req_dtl.c525_product_request_id,  t7103.c7103_name eventName,t7103.c901_purpose eventpurpose ,");
			  sbQuery.append(" t7103.c101_loan_to_id loantoid ,t7103.c901_loan_to_org loanto, t7103.c7103_start_date eventstartdt , t7103.c7103_end_date eventenddt");
			  sbQuery.append(" FROM t7103_schedule_info t7103, (");
			     sbQuery.append(" SELECT DISTINCT t7104.c7100_case_info_id, t525.c525_product_request_id");
			     sbQuery.append(" FROM t7104_case_set_information t7104, t526_product_request_detail t526, t525_product_request t525, t7100_case_information t7100");
			     sbQuery.append(" WHERE t7104.c526_product_request_detail_id = t526.c526_product_request_detail_id");
			     sbQuery.append(" AND t525.c525_product_request_id         = t526.c525_product_request_id");
			     sbQuery.append(" AND t7100.c7100_case_info_id             = t7104.c7100_case_info_id");
			     sbQuery.append(" AND t7100.c901_type                      = 1006504");
			     sbQuery.append(" AND t7100.c7100_void_fl                 IS NULL");
			     sbQuery.append(" AND t7104.c7104_void_fl                 IS NULL");
			     sbQuery.append(" AND t525.c525_void_fl                   IS NULL) event_req_dtl");
			  sbQuery.append(" WHERE event_req_dtl.c7100_case_info_id = t7103.c7100_case_info_id");
			  if(!strEventName.equals("0")){
				  sbQuery.append(" AND  event_req_dtl.c7100_case_info_id  = '");
				  sbQuery.append(strEventName);
				  sbQuery.append("'");
			  }
			  sbQuery.append(") event_dtl");	
			if(!strpartNum.equals("")){
				sbQuery.append(", t505_item_consignment t505");
			}			
			sbQuery.append(" WHERE T504.c504_Consignment_Id = T506.C506_ref_Id ");
			if(strLoanType.equals("100354")){
				sbQuery.append(" AND T504.C504_type = 9110");
			}else if(strLoanType.equals("100355")){
				sbQuery.append(" AND T504.C504_type = ''");
			}else{
				sbQuery.append(" AND T504.C504_type IN (9110)"); /* In Future, need to add the Product Loaner Item Type also*/
			}
			if(!strTransactionID.equals("")){
				sbQuery.append(" AND T504.c504_Consignment_Id = '");
				sbQuery.append(strTransactionID);
				sbQuery.append("'");						
			}
			sbQuery.append(" AND T506.C506_STATUS_FL      = 0");
			sbQuery.append(" AND t504.c504_void_fl       IS NULL");
			sbQuery.append(" AND T506.c506_void_fl       IS NULL");
			sbQuery.append(" AND T504.C504_ref_Id         = event_dtl.C525_Product_Request_Id");
			if(!strpartNum.equals("")){
				sbQuery.append(" AND T504.C504_Consignment_Id = T505.C504_Consignment_Id ");
				sbQuery.append(" AND T505.c205_part_number_id = '");
				sbQuery.append(strpartNum);
				sbQuery.append("'");
			}
			if(!strPurpose.equals("0")){
				  sbQuery.append(" AND  event_dtl.eventpurpose = '");
				  sbQuery.append(strPurpose);
				  sbQuery.append("'");
			}
			if(!strLoanToName.equals("0")){
				  sbQuery.append(" AND  event_dtl.loanto = '");
				  sbQuery.append(strLoanToName);
				  sbQuery.append("'");
				  if(!strLoanName.equals("0")){
					  sbQuery.append(" AND  event_dtl.loantoid = '");
					  sbQuery.append(strLoanName);
					  sbQuery.append("'");
				  }
			  }
			if (!strDtType.equals("0")) {
				String strdt = "";
				if (strDtType.equals("ESD")) {
					strdt = " event_dtl.eventstartdt ";
				}else if (strDtType.equals("EED")) {
					strdt = " event_dtl.eventenddt ";
				}
				sbMergeQuery.append(" AND");
				sbMergeQuery.append(strdt);
				sbMergeQuery.append(" >= TO_DATE('");
				sbMergeQuery.append(strStartDt);
				sbMergeQuery.append("','");
				sbMergeQuery.append(strDateFmt);
				sbMergeQuery.append("') AND");
				sbMergeQuery.append(strdt);
				sbMergeQuery.append("  <= TO_DATE('");
				sbMergeQuery.append(strEndDt);
				sbMergeQuery.append("','");
				sbMergeQuery.append(strDateFmt);
				sbMergeQuery.append("')");
				
				if((strDtType.equals("ESD") || strDtType.equals("EED")))
				{
					sbQuery.append(sbMergeQuery);
				}
			}			
			sbQuery.append(" ORDER BY CONSIGNID");
			
			log.debug("fetchPendingRtnIHLoanerItem * * * * * :" + sbQuery.toString());
			alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		} catch (Exception e) {
			GmLogError.log("Exception in GmLogisticsBean:loadItemConsignReport", "Exception is:" + e);
		}
		return alReturn;		
	}
	
    /**
     * fetchChildRequestDetail - This Method is used to get child request detail
     * @param String strRequestId,IHLN  
     * @return ArrayList
     * @exception AppError
   **/
     public ArrayList fetchChildRequestDetail(String strReqId, String strTxnId,String strType) throws AppError
     {
       
       ArrayList alResult =  new ArrayList();
       GmDBManager gmDBManager = new GmDBManager (); 
         
       // To fetch the transaction information
       gmDBManager.setPrepareString("gm_pkg_op_ihloaner_item_rpt.gm_fch_child_request",4);
       gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
       gmDBManager.setString(1,strReqId);
       gmDBManager.setString(2,strTxnId);
       gmDBManager.setString(3,strType);
       gmDBManager.execute();
       
       alResult = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
       
       gmDBManager.close();

       return alResult;
     }
     
     /**
      * fetchChildRequestDetail - This Method is used to get child request detail
      * @param String strRequestId,IHLN  
      * @return ArrayList
      * @exception AppError
    **/
      public HashMap fetchInhouseLoanerItemDetails(HashMap hmParam) throws AppError
      {
        HashMap hmReturn = new HashMap();
        HashMap hmTemp = new HashMap();
        HashMap hmResult = new HashMap();
        HashMap hmCONSIGNMENT = new HashMap();
        
        ArrayList alSetLoad = new ArrayList();
        ArrayList alChildReq = new ArrayList();
        
        GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean();
        
        String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
        String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQID"));
        String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
        
        hmTemp = gmICTSummaryBean.fetchConsignmentDetails(strConsignId,"null");
		hmCONSIGNMENT = GmCommonClass.parseNullHashMap((HashMap) hmTemp.get("HMCONSIGNDETAILS"));
		hmResult = GmCommonClass.parseNullHashMap((HashMap) hmTemp.get("HMCONSIGNCHILDDETAILS"));
		alSetLoad =  GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("SETLOAD"));
		alChildReq = GmCommonClass.parseNullArrayList(fetchChildRequestDetail(strReqId, strConsignId,strType));
		hmReturn.putAll(hmCONSIGNMENT);
		hmReturn.put("CTYPE",GmCommonClass.parseNull((String) hmCONSIGNMENT.get("TYPE")));
		hmReturn.put("SETLOAD",alSetLoad);
		hmReturn.put("SUBCHILDREQUEST", alChildReq);
		log.debug("=============fetchInhouseLoanerItemDetails========");

        return hmReturn;
      }
      public String fetchAssociatedIHLN(String strTxnId) throws AppError{
	  	String strResult = "";
	  	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  	gmDBManager.setFunctionString("gm_pkg_op_ihloaner_item_rpt.get_associated_ihln", 1);
  		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
  		gmDBManager.setString(2, strTxnId);
  		gmDBManager.execute();
  		strResult = GmCommonClass.parseNull(gmDBManager.getString(1));
  		gmDBManager.close();      
  		log.debug("strREsult ::: " + strResult);
  		return strResult;
      }
      
      
      
      
}
