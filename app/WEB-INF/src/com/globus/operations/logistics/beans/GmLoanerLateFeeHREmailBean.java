package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

public class GmLoanerLateFeeHREmailBean extends GmBean {
	 Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

	public GmLoanerLateFeeHREmailBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
	}
	
	/**
	   * @description To initialize Queue and consumer Job class
	   * @param hmValues
	   * @return
	   */
	public void initiateJMSHREmails(HashMap hmValues) {
		log.debug("initiateJMSHREmails");
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		HashMap hmParam=new HashMap();
		String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOANER_LATE_FEE_EMAIL_QUEUE"));
		hmParam.put("QUEUE_NAME", strQueueName);
		log.debug("strQueueName = "+strQueueName);
		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("LOANER_LATE_FEE_HR_EMAIL_CONSUMER_CLASS"));
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		log.debug("strConsumerClass ="+strConsumerClass);
		hmParam.put("HMVALUES", hmValues);
		gmConsumerUtil.sendMessage(hmParam);
		
	}
	
	/**
	   * @description To fetch Email summary of Direct Rep Late Fee and send to HR as Excel
	   * @param strChargeDtlIds
	   * @return
	   * @throws Exception
	   */
	public void sendDirectRepLateFeeHREmail(String strChargeDtlIds) throws Exception {
		log.debug("sendDirectRepLateFeeHREmail = " + strChargeDtlIds);
		int strRepCategory = 70101; // 70101 -Direct rep,
		int strChargeStatus = 20; // 20 -submitted
		ArrayList alSummary = new ArrayList();
		
		alSummary = fetchChargeHREmailSummary(strChargeDtlIds, strRepCategory, strChargeStatus);
		log.debug("sendDirectRepLaceFeeHREmail alSummary size =" + alSummary.size());
		if (alSummary.size() > 0) {
			String strDeductionDate = getDeductionDate(0, strRepCategory, strChargeStatus);
			log.debug("Deduction date" + strDeductionDate);
//			Generate Excel
			GmLoanerLateFeeHREmailExcelBean gmLoanerLateFeeHREmailExcelBean = new GmLoanerLateFeeHREmailExcelBean(getGmDataStoreVO());
			String strExcelPath = gmLoanerLateFeeHREmailExcelBean.saveDirectRepLateFeeHRExcel(alSummary);
			log.debug("Excel Generated ="+strExcelPath);
			
			GmEmailProperties emailProps = new GmEmailProperties();
			GmJasperMail jasperMail = new GmJasperMail();
			String TEMPLATE_NAME = "GmLoanerLateFeeSubmittedHREmailDirectRep";
			String strFrom = "";
			String strTo = "";
			String strCc = "";
			String strMimeType = "";
			String strSubject = "";
			String strBody = "";
			HashMap hmParam = new HashMap();
			String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
			GmResourceBundleBean gmResourceBundleBean = GmCommonClass
					.getResourceBundleBean("properties.Email." + System.getProperty("ENV_TYPE"), strCompanyLocale);
			strCc = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.CC);
			strFrom = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM);
			strTo = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.TO);
			strMimeType = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
			strSubject = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT));

			strSubject = GmCommonClass.replaceAll(strSubject, "#<DEDUCTION_DATE>", strDeductionDate);
			strBody = "This email includes Late Fee Charges for the " + strDeductionDate;
			hmParam.put("BODY", strBody);
			log.debug("strFrom===sendDirectrepLateFeeSubmittedEmail======== " + strFrom);
			log.debug("strTo===sendDirectrepLateFeeSubmittedEmail======= " + strTo);
			log.debug("strMimeType===sendDirectrepLateFeeSubmittedEmail====== " + strMimeType);
			log.debug("strCc==sendDirectrepLateFeeSubmittedEmail========= " + strCc);
			log.debug("strSubject====sendDirectrepLateFeeSubmittedEmail========== " + strSubject);
			HashMap hmReturn = new HashMap();
			emailProps.setMimeType(strMimeType);
			emailProps.setSender(strFrom);
			emailProps.setRecipients(strTo);
			emailProps.setCc(strCc);
			emailProps.setSubject(strSubject);
			emailProps.setAttachment(strExcelPath);

			jasperMail.setJasperReportName("/GmLoanerLateFeeSubmittedHREmailDirectRep.jasper");
			jasperMail.setAdditionalParams(hmParam);
			jasperMail.setReportData(null);
			jasperMail.setEmailProperties(emailProps);
			hmReturn =jasperMail.sendMail();
			log.debug("mail return object"+hmReturn);


		}

	}
	
	/**
	   * @description To get deduction dat
	   * @param strRepCategory, strChargeStatus
	   * @return strDeductionDate
	   */
	private String getDeductionDate(int strSalesRep, int strRepCategory, int strChargeStatus) {
		log.debug("getDeductionDate");
	    String strDeductionDate = "";
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setFunctionString("gm_pkg_op_loaner_charges_rpt.get_deduction_date", 3);
	    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	    gmDBManager.setInt(2, strSalesRep);
	    gmDBManager.setInt(3, strRepCategory);
	    gmDBManager.setInt(4, strChargeStatus);
	    gmDBManager.execute();
	    strDeductionDate = GmCommonClass.parseNull(gmDBManager.getString(1));
	    gmDBManager.close();
	    return strDeductionDate;
	}
	
	/**
	   * @description To fetch HR email Summary
	   * @param strChargeDtlIds, strRepCategory, strChargeStatus
	   * @return alList
	   */
	public ArrayList fetchChargeHREmailSummary(String strChargeDtlIds, int strRepCategory, int strChargeStatus) {
		log.debug("fetchChargeHREmailSummary");
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alList = new ArrayList();
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_email.gm_fch_charge_hr_email_summary", 4);
	    gmDBManager.setString(1, strChargeDtlIds);
	    gmDBManager.setInt(2, strRepCategory);
	    gmDBManager.setInt(3, strChargeStatus);
	    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList  = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
	    gmDBManager.close();
	    log.debug("fetchChargeHREmailSummary allist ---> "+alList.size());
		return alList;
	}
	
	/**
	   * @description To fetch Email summary and Charge Details of Dist Rep Late Fee and send to HR as Excel
	   * @param strChargeDtlIds
	   * @return
	   * @throws Exception
	   */
	public void sendDistRepLateFeeHREmail(String strChargeDtlIds) throws Exception {
		int strRepCategory = 70100; // Distrubutor rep
		int strChargeStatus = 20; // Submitted
		ArrayList alSummary = new ArrayList();
		ArrayList alDetail = new ArrayList();
//		 fetching summary data 
		alSummary = fetchChargeHREmailSummary(strChargeDtlIds, strRepCategory, strChargeStatus); // 70100 -Dist rep, 20 -submitted
		log.debug("sendDistRepLateFeeHREmail alsummary size = "+alSummary.size());																

//	 	fetching detail
		alDetail = fetchChargeHREmailDetail(strChargeDtlIds, strRepCategory, strChargeStatus); // 70100 -Dist rep, 2 -submitted
		log.debug("sendDistRepLateFeeHREmail alDetail size = "+alDetail.size());																				

		if (alSummary.size() > 0 || alDetail.size() > 0) {

//			Get deduction date  
			String strDeductionDate = getDeductionDate(0, strRepCategory, strChargeStatus);

//		   	Generate Excel
			GmLoanerLateFeeHREmailExcelBean gmLoanerLateFeeHREmailExcelBean = new GmLoanerLateFeeHREmailExcelBean(
					getGmDataStoreVO());
			String strExcelPath = gmLoanerLateFeeHREmailExcelBean.saveDistRepLateFeeHRExcel(alSummary, alDetail);
			log.debug("sendDistRepLateFeeHREmail --> Excel generated "+strExcelPath);
			GmEmailProperties emailProps = new GmEmailProperties();
			GmJasperMail jasperMail = new GmJasperMail();
			String TEMPLATE_NAME = "GmLoanerLateFeeSubmittedHREmailDistributedRep";

			String strFrom = "";
			String strTo = "";
			String strCc = "";
			String strMimeType = "";
			String strSubject = "";
			String strBody = "";
			HashMap hmParam = new HashMap();
			String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
			GmResourceBundleBean gmResourceBundleBean = GmCommonClass
					.getResourceBundleBean("properties.Email." + System.getProperty("ENV_TYPE"), strCompanyLocale);

			strCc = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.CC);
			strFrom = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM);
			strTo = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.TO);
			strMimeType = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
			strSubject = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT));

			strSubject = GmCommonClass.replaceAll(strSubject, "#<DEDUCTION_DATE>", strDeductionDate);

			strBody = "This email includes Late Fee Charges for the " + strDeductionDate;
			hmParam.put("BODY", strBody);
			log.debug("strFrom=======sendDistRepLateFeeHREmail======= " + strFrom);
			log.debug("strTo=======sendDistRepLateFeeHREmail======== " + strTo);
			log.debug("strMimeType=======sendDistRepLateFeeHREmail======= " + strMimeType);
			log.debug("strCc=======sendDistRepLateFeeHREmail======= " + strCc);
			log.debug("strSubject=======sendDistRepLateFeeHREmail====== " + strSubject);

			emailProps.setMimeType(strMimeType);
			emailProps.setSender(strFrom);
			emailProps.setRecipients(strTo);
			emailProps.setCc(strCc);
			emailProps.setSubject(strSubject);
			emailProps.setAttachment(strExcelPath);

			jasperMail.setJasperReportName("/GmLoanerLateFeeSubmittedHREmailDistributedRep.jasper");
			jasperMail.setAdditionalParams(hmParam);
			jasperMail.setReportData(null);
			jasperMail.setEmailProperties(emailProps);
			jasperMail.sendMail();

		}

	}
	
	/**
	   * @description To fetch charge details 
	   * @param strChargeDtlIds, strRepCategory, strRepCategory
	   * @return alList
	   * @throws Exception
	   **/
	private ArrayList fetchChargeHREmailDetail(String strChargeDtlIds, int strRepCategory, int strChargeStatus) {
		log.debug("fetchChargeHREmailDetail");
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alList = new ArrayList();
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_charges_email.gm_fch_charge_hr_email_detail", 4);
	    gmDBManager.setString(1, strChargeDtlIds);
	    gmDBManager.setInt(2, strRepCategory);
	    gmDBManager.setInt(3, strChargeStatus);
	    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList  = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
	    gmDBManager.close();
		return alList;
	}
	
	/**
	   * @description To fetch Email summary and Charge Details of Direct Rep Credit Late Fee and send to HR as Excel
	   * @param strChargeDtlIds
	   * @return
	   * @throws Exception
	   **/
	public void sendDirectRepCreditHREmail(String strChargeDtlIds) throws Exception {

		int strRepCategory = 70101;
		int strChargeStatus = 25;
		ArrayList alSummary = new ArrayList();
		ArrayList alDetail = new ArrayList();
//		get summary data
		alSummary = fetchChargeHREmailSummary(strChargeDtlIds, strRepCategory, strChargeStatus); // 70101 -Direct rep, 25-Credit
		log.debug("sendDirectRepCreditHREmail --> alsumary size = "+alSummary.size());
//		get detail data
		alDetail = fetchChargeHREmailDetail(strChargeDtlIds, strRepCategory, strChargeStatus); // 70101 -Direct rep, 25-Credit
		log.debug("sendDirectRepCreditHREmail --> alDetail size = "+alDetail.size());
		if (alSummary.size() > 0 || alDetail.size() > 0) {
//			Get deduction date 
			String strDeductionDate = getDeductionDate(0, strRepCategory, strChargeStatus);

//			Generate Excel
			GmLoanerLateFeeHREmailExcelBean gmLoanerLateFeeHREmailExcelBean = new GmLoanerLateFeeHREmailExcelBean(
					getGmDataStoreVO());
			String strExcelPath = gmLoanerLateFeeHREmailExcelBean.saveDirectRepCreditHRExcel(alSummary, alDetail);
			log.debug("sendDirectRepCreditHREmail --> Excel Generated --->"+strExcelPath);
			GmEmailProperties emailProps = new GmEmailProperties();
			GmJasperMail jasperMail = new GmJasperMail();
			String TEMPLATE_NAME = "GmLoanerCreditHREmailDirectRep";

			String strFrom = "";
			String strTo = "";
			String strCc = "";
			String strMimeType = "";
			String strSubject = "";
			String strBody = "";
			HashMap hmParam = new HashMap();
			String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
			GmResourceBundleBean gmResourceBundleBean = GmCommonClass
					.getResourceBundleBean("properties.Email." + System.getProperty("ENV_TYPE"), strCompanyLocale);

			strCc = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.CC);
			strFrom = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM);
			strTo = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.TO);
			strMimeType = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
			strSubject = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT));

			strSubject = GmCommonClass.replaceAll(strSubject, "#<DEDUCTION_DATE>", strDeductionDate);

			strBody = "This email includes Late Fee Charges Credits for the " + strDeductionDate;
			hmParam.put("BODY", strBody);
			log.debug("strFrom=======sendDirectRepCreditHREmail======== " + strFrom);
			log.debug("strTo=======sendDirectRepCreditHREmail======== " + strTo);
			log.debug("strMimeType=======sendDirectRepCreditHREmail======= " + strMimeType);
			log.debug("strCc=======sendDirectRepCreditHREmail====== " + strCc);
			log.debug("strSubject=======sendDirectRepCreditHREmail======= " + strSubject);

			emailProps.setMimeType(strMimeType);
			emailProps.setSender(strFrom);
			emailProps.setRecipients(strTo);
			emailProps.setCc(strCc);
			emailProps.setSubject(strSubject);
			emailProps.setAttachment(strExcelPath);

			jasperMail.setJasperReportName("/GmLoanerCreditHREmailDirectRep.jasper");
			jasperMail.setAdditionalParams(hmParam);
			jasperMail.setReportData(null);
			jasperMail.setEmailProperties(emailProps);
			jasperMail.sendMail();

		}

	}
	
	/**
	   * @description To fetch Email summary and Charge Details of Dist Rep Credit Late Fee and send to HR as Excel
	   * @param strChargeDtlIds
	   * @return
	   * @throws Exception
	   **/
	public void sendDisttRepCreditHREmail(String strChargeDtlIds) throws Exception {
		int strRepCategory = 70100;
		int strChargeStatus = 25;
		ArrayList alSummary = new ArrayList();
		ArrayList alDetail = new ArrayList();
		
			
		
//		get summary data
		alSummary = fetchChargeHREmailSummary(strChargeDtlIds, strRepCategory, strChargeStatus); // 70100 -Distributor, 25
		log.debug("sendDisttRepCreditHREmail alsummary size ="+alSummary.size());																		// -Credit

//				2) get detail data
		alDetail = fetchChargeHREmailDetail(strChargeDtlIds, strRepCategory, strChargeStatus); // 70100 -Distributor, 25
		log.debug("sendDisttRepCreditHREmail aldetail size ="+alDetail.size());// -Credit
		
		if (alSummary.size() > 0 || alDetail.size() > 0) {
//			Get deduction date 
			String strDeductionDate = getDeductionDate(0, strRepCategory, strChargeStatus);
//			Generate Excel
			GmLoanerLateFeeHREmailExcelBean gmLoanerLateFeeHREmailExcelBean = new GmLoanerLateFeeHREmailExcelBean(
					getGmDataStoreVO());
			String strExcelPath = gmLoanerLateFeeHREmailExcelBean.saveDistRepCreditHRExcel(alSummary, alDetail);
			log.debug("Excel Generated --> "+ strExcelPath);
			GmEmailProperties emailProps = new GmEmailProperties();
			GmJasperMail jasperMail = new GmJasperMail();
			String TEMPLATE_NAME = "GmLoanerCreditHREmailDistributedRep";

			String strFrom = "";
			String strTo = "";
			String strCc = "";
			String strMimeType = "";
			String strSubject = "";
			String strBody = "";
			HashMap hmParam = new HashMap();
			String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
			GmResourceBundleBean gmResourceBundleBean = GmCommonClass
					.getResourceBundleBean("properties.Email." + System.getProperty("ENV_TYPE"), strCompanyLocale);

			strCc = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.CC);
			strFrom = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM);
			strTo = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.TO);
			strMimeType = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
			strSubject = GmCommonClass
					.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT));

			strSubject = GmCommonClass.replaceAll(strSubject, "#<DEDUCTION_DATE>", strDeductionDate);

			strBody = "This email includes Late Fee Charges Credits for the " + strDeductionDate;
			hmParam.put("BODY", strBody);
			log.debug("strFrom=======sendDisttRepCreditHREmail======= " + strFrom);
			log.debug("strTo=======sendDisttRepCreditHREmail======= " + strTo);
			log.debug("strMimeType=======sendDisttRepCreditHREmail======= " + strMimeType);
			log.debug("strCc=======sendDisttRepCreditHREmail======= " + strCc);
			log.debug("strSubject=======sendDisttRepCreditHREmail===== " + strSubject);

			emailProps.setMimeType(strMimeType);
			emailProps.setSender(strFrom);
			emailProps.setRecipients(strTo);
			emailProps.setCc(strCc);
			emailProps.setSubject(strSubject);
			emailProps.setAttachment(strExcelPath);

			jasperMail.setJasperReportName("/GmLoanerCreditHREmailDistributedRep.jasper");
			jasperMail.setAdditionalParams(hmParam);
			jasperMail.setReportData(null);
			jasperMail.setEmailProperties(emailProps);
			jasperMail.sendMail();

		}

	}

}
