package com.globus.operations.logistics.beans;

import java.io.File;  
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRepEmailProperties;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.crypt.AESEncryptDecrypt;
import com.globus.common.db.GmDBManager;
import com.globus.common.jira.beans.GmJIRAHelperBean;
import com.globus.common.util.jobs.GmJob;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmTicketBean extends GmBean {

  public GmTicketBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmTicketBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

 Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  /*
   * Description: Main method that accepts all the requests that need to be processed, loop the
   * requests and call createMissingPartTickets
   * 
   * Author: VPrasath
   */
  public void sendMissingPartsEmailOnRequest(String strReqids, String strUserId,
      String strUserName, GmJasperReport gmJasperReport) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(getGmDataStoreVO());

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    String[] arrReqids = strReqids.split(",");
    String strRepName = "";
    HashMap hmReturn = new HashMap();

    HashMap hmParam = new HashMap();
    hmParam.put("USERID", strUserId);
    hmParam.put("USERNAME", strUserName);
    hmParam.put("JASPER", gmJasperReport);
    log.debug("strReqids==> "+strReqids);
    log.debug("arrReqids==> "+arrReqids);
    String strMsg =
        "Please help us in resolving this issue with the missing parts by the Expected Return Date. Thank you in advance for your help.";

    for (int i = 0; i < arrReqids.length; i++) {
      try {
        log.debug("req id is " + arrReqids[i]);
        hmParam.put("REQID", null);
        hmParam.put("REQDETID", arrReqids[i]);
        hmParam.put("MSG", strMsg);

        hmReturn = sendMissingPartsEmail(gmDBManager, 1, hmParam);
        strRepName = (String) hmReturn.get("REPNAME");
        if (!strRepName.equals("")) {
          gmCommonBean.saveLog(gmDBManager, arrReqids[i],
              "Missing parts email sent to " + hmReturn.get("REPNAME"), strUserId, "1247");
          gmRequestTransBean.saveEmailCounter(gmDBManager, arrReqids[i], strUserId);
        }
        gmDBManager.commit();
      } catch (Exception e) {
        gmDBManager.close();
        e.printStackTrace();
        log.error("Exception in sendRequestRecievedMail : " + e.getMessage());
        hmReturn.put(GmJob.EXCEPTION, e);
      }
      Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
      if (ex != null) {
        try {
          GmEmailProperties emailProps = new GmEmailProperties();
          emailProps.setRecipients(gmResourceBundleBean
              .getProperty("GmMissingParts.ExceptionMailTo"));
          emailProps.setSender(gmResourceBundleBean.getProperty("GmCommon.ExceptionMailFrom"));
          emailProps.setSubject(gmResourceBundleBean
              .getProperty("GmMissingParts.ExceptionMailSubject") + " " + arrReqids[i]);
          emailProps.setMessage("Exception : " + GmCommonClass.getExceptionStackTrace(ex, "\n")
              + "\nOriginal Mail : " + hmReturn.get("ORIGINALMAIL"));
          emailProps.setMimeType("text/html");

          GmCommonClass.sendMail(emailProps);
        } catch (Exception e) {
          log.error("Exception in sending mail to Rep for new request and also notification to IT"
              + e.getMessage());
        }
      }
    }

  }

  /*
   * Description: This method creates a ticket in JIRA for each request detail,creates a PDF file by
   * calling createPDFFile, fetch the email propertiesby calling fetchMissingEmailProperties and
   * send an email by calling sendMissingPartsEmail
   * 
   * Author: VPrasath
   */
  private void createTicket(GmDBManager gmDBManager, String strRepName, String strUserName,
      String strUserId, String strRequestID, ArrayList alTicketDetails, HashMap hmRepDetails,
      int emailno, String strMsg) throws AppError {
    String strTicketID = "";
    String strRepLoginName = "";
    String strAssRepLoginName = "";
    String strRequestDetailId = "";

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    GmJIRAHelperBean gmJIRAHelperBean = new GmJIRAHelperBean();
    HashMap hmParam = new HashMap();
    GmEmailProperties emailProps = null;
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());

    // Check if ticket already exists for the request
    HashMap hMap = (HashMap) alTicketDetails.get(0);
    
    strRequestID = GmCommonClass.parseNull((String) hMap.get("REQ_ID"));
  	 //PC-918 - To Create unique Jira CSTKT Ticket
    strRequestDetailId = GmCommonClass.parseNull((String) hMap.get("REQ_DET_ID")); 
    log.debug("strRequestDetailId==> "+strRequestDetailId);
    strTicketID = gmJIRAHelperBean.checkTicketExists(strRequestDetailId, "10056", gmDBManager);
    log.debug("strTicketID==> "+strTicketID);
    if (strTicketID.equals("")) {
      // getting rep login name to create jira ticket
      strRepLoginName = GmCommonClass.parseNull((String) hmRepDetails.get("REPLOGINNAME"));
      strAssRepLoginName = GmCommonClass.parseNull((String) hmRepDetails.get("ASSREPLOGINNAME"));
      strRepName =
          strAssRepLoginName.equals("") ? strRepLoginName.equals("") ? strRepName : strRepLoginName
              : strAssRepLoginName;

      // 1. Create Ticket at the Request Level
      strTicketID =
          gmJIRAHelperBean.createJIRAMissingTicket(strRepName, strUserName, alTicketDetails); // gmJIRAHelperBean.createJIRAMissingTicket
                                                                                              // need
                                                                                              // to
                                                                                              // be
                                                                                              // changed

      // 2. Update the Ticket ID for all the Request Detail
      hmParam.put("TICKETID", strTicketID);
      hmParam.put("REFTYPE", "10056");
      hmParam.put("USERID", strUserId);
      hmParam.put("REFID", strRequestDetailId);
      gmJIRAHelperBean.saveTicket(gmDBManager, hmParam);


      // 3. Generate Email at the Request Level
      HashMap hmParams = new HashMap();
      hmRepDetails.put("TICKETID", strTicketID);
      hmRepDetails.put("USERNM", strUserName);
      emailProps = fetchMissingEmailProperties(hmRepDetails, emailno);

      hmParams.put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
      hmParams
          .put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusTicketEmail"));
      hmParams.put("FMESSAGE", strMsg);
      try {

        GmJasperReport gmJasperReport = (GmJasperReport) hmRepDetails.get("JASPER");
        String strAttachmentFileName =
            createPDFFile(gmJasperReport, hmParams, alTicketDetails, strTicketID);
        String strMessageDesc = gmJIRAHelperBean.getMissingTicketDescription(alTicketDetails);
        emailProps.setMessage(strMessageDesc);
        emailProps.setAttachment(strAttachmentFileName);
        GmCommonClass.sendMail(emailProps);
        gmTxnSplitBean.removeFile(strAttachmentFileName);
      } catch (Exception ex) {
        throw new AppError(ex);
      }

    } else {
      if (alTicketDetails.size() > 0) {
        gmJIRAHelperBean.updateReconDetailsInMT(strTicketID, new ArrayList(), alTicketDetails,
            strMsg);
      }
    }

    /*
     * 
     * 
     * 
     * log.debug("strTicketID Created Already: " + strTicketID); log.debug("alTicketDetails: " +
     * alTicketDetails);
     * 
     * if (emailno == 1 ) { strTicketID = gmJIRAHelperBean.createJIRAMissingTicket( strRepName,
     * strUserName, alTicketDetails ); hmParam.put("TICKETID", strTicketID); hmParam.put("REFID",
     * strRequestDetailsID); hmParam.put("REFTYPE", "10056"); hmParam.put("USERID", strUserId);
     * gmJIRAHelperBean.saveTicket(gmDBManager, hmParam);
     * 
     * HashMap hmMeta = (HashMap)alTicketDetails.get(0); HashMap hmParams = new HashMap();
     * 
     * log.debug("hmMeta: " + hmMeta); String strSet =
     * GmCommonClass.parseNull((String)hmMeta.get("SETDET"));
     * 
     * if(emailno == 1) { //String strTicketID = gmJIRAHelperBean.checkTicketExists(strReqDetid,
     * "10056"); hmRepDetails.put("TICKETID", strTicketID); hmRepDetails.put("USERNM", strUserName);
     * emailProps = fetchMissingEmailProperties(hmRepDetails,emailno);
     * 
     * hmParams.put("GMCSPHONE", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSPhone"));
     * hmParams.put("GMCSEMAIL", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSEmail"));
     * hmParams.put("FMESSAGE", strMsg);
     * 
     * try { GmJasperReport gmJasperReport = (GmJasperReport)hmRepDetails.get("JASPER"); String
     * strAttachmentFileName = createPDFFile(gmJasperReport, hmParams, alTicketDetails,
     * strRequestDetailsID); String strMessageDesc =
     * gmJIRAHelperBean.getMissingTicketDescription(strSet,alTicketDetails);
     * emailProps.setMessage(strMessageDesc); emailProps.setAttachment(strAttachmentFileName);
     * GmCommonClass.sendMail(emailProps); gmTxnSplitBean.removeFile(strAttachmentFileName); }
     * catch(Exception ex) { ex.printStackTrace(); } } } else if(!strTicketID.equals("")) {
     * log.debug("alTicketDetails: " + alTicketDetails); if (alTicketDetails.size() > 0) {
     * gmJIRAHelperBean.updateReconDetailsInMT(strTicketID, new ArrayList(), alTicketDetails,
     * strMsg); } }
     */



  }

  /*
   * Description: Accepts one request at a time, loop the request for all the request details and
   * call fetchMissingPartDetails and createTicket
   * 
   * Author: VPrasath
   */

  private void createMissingPartTickets(GmDBManager gmDBManager, String strRepName,
      String strUserName, String strUserId, ArrayList alDetails, HashMap hmRepDetails, int emailno,
      String msg) throws AppError {
    Iterator itrDetails = alDetails.iterator();
    ArrayList alDetailCopy = new ArrayList(alDetails);
    ArrayList alTicketDetails = new ArrayList();
    String strRequestDetailsID = "";

    boolean bsize = false;

    if (alDetails.size() == 1) {
      bsize = true;
    }

    log.debug("alDetails : " + alDetails);

    createTicket(gmDBManager, strRepName, strUserName, strUserId, strRequestDetailsID, alDetails,
        hmRepDetails, emailno, msg);

  }

  /*
   * Description: To Fetch the missing parts for the given Requests
   * 
   * Author: VPrasath
   */
  public HashMap fetchMissingPartDetails(GmDBManager gmDBManager, int emailno, HashMap hmMap)
      throws AppError {
    String strReqid = GmCommonClass.parseNull((String) hmMap.get("REQID"));
    String strReqDetid = GmCommonClass.parseNull((String) hmMap.get("REQDETID"));
    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_missing_parts", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strReqid);
    gmDBManager.setString(2, strReqDetid);
    gmDBManager.execute();

    ArrayList alMeta = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    ArrayList alDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));

    hmReturn.put("META", alMeta);
    hmReturn.put("DETAILS", alDetails);

    return hmReturn;
  }

  /*
   * Description: fetch the Recipients, CC, Subject, and other email properties for that particular
   * request detail
   * 
   * Author: VPrasath
   */

  public GmEmailProperties fetchMissingEmailProperties(HashMap hmMap, int emailno) throws AppError {

    log.debug("hmMap: " + hmMap);
    String strAssrepID = "";
    String strAssRepName = "";
    String strAssRepEmail = "";

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    String strRepId = GmCommonClass.parseNull((String) hmMap.get("ID"));
    String strRequestId = GmCommonClass.parseNull((String) hmMap.get("REQ_ID"));
    String strRepEmail = GmCommonClass.parseNull((String) hmMap.get("REPEMAIL"));
    String strSet = GmCommonClass.parseNull((String) hmMap.get("SETDET"));
    String strTicketID = GmCommonClass.parseNull((String) hmMap.get("TICKETID"));
    String strUserNm = GmCommonClass.parseNull((String) hmMap.get("USERNM"));

    strAssRepName = GmCommonClass.parseNull((String) hmMap.get("ASSOCREPNM"));
    strAssrepID = GmCommonClass.parseNull((String) hmMap.get("ASSOCREPID"));
    strAssRepEmail = GmCommonClass.parseNull((String) hmMap.get("ASSOCREPEMAIL"));
    strRepEmail = strAssrepID.equals("") ? strRepEmail : strAssRepEmail;

    AESEncryptDecrypt aesEncryptDecrypt = new AESEncryptDecrypt();
    strUserNm = aesEncryptDecrypt.decryptText(strUserNm);

    String strExtraTo = gmResourceBundleBean.getProperty("GmMissingParts." + GmEmailProperties.TO);
    String strFrom = gmResourceBundleBean.getProperty("GmMissingParts." + GmEmailProperties.FROM);
    strFrom = GmCommonClass.replaceAll(strFrom, "#<USERNM>", strUserNm);

    log.debug("strExtraTo: " + strExtraTo);

    strRepEmail = strRepEmail + "," + strExtraTo;

    log.debug("strRepEmail: " + strRepEmail);


    GmEmailProperties emailProps =
        new GmRepEmailProperties(strRepId, "GmMissingPartsReminderEmailJob");
    emailProps.setRecipients(strRepEmail);
    emailProps.setSender(strFrom);
    emailProps.setCc(gmResourceBundleBean.getProperty("GmMissingParts." + GmEmailProperties.CC));
    emailProps.setMimeType(gmResourceBundleBean.getProperty("GmMissingParts."
        + GmEmailProperties.MIME_TYPE));

    String strSubject =
        gmResourceBundleBean.getProperty("GmMissingParts" + emailno + "."
            + GmEmailProperties.SUBJECT);;
    strSubject = GmCommonClass.replaceAll(strSubject, "#<TICKETID>", strTicketID);
    strSubject = GmCommonClass.replaceAll(strSubject, "#<RQID>", strRequestId);
    emailProps.setSubject(strSubject);

    return emailProps;
  }

  /*
   * Description: sends email to sales reps as well as JIRA listener with the PDF attached to the
   * email
   * 
   * Author: VPrasath
   */


  public HashMap sendMissingPartsEmail(GmDBManager gmDBManager, int emailno, HashMap hmMap)
      throws AppError {
    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alDetails = new ArrayList();
    GmJIRAHelperBean gmJIRAHelperBean = new GmJIRAHelperBean();
    GmEmailProperties emailProps = null;

    HashMap hmRepDetails = new HashMap();
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());

    String strRepName = "";
    String strAssRepName = "";
    String strAssrepID = "";
    String strSet = "";
    String strReqid = GmCommonClass.parseNull((String) hmMap.get("REQID"));

    String msg = GmCommonClass.parseNull((String) hmMap.get("MSG"));
    String strUser = GmCommonClass.parseNull((String) hmMap.get("USERID"));
    String strUserName = GmCommonClass.parseNull((String) hmMap.get("USERNAME"));
    GmJasperReport gmJasperReport = (GmJasperReport) hmMap.get("JASPER");

    HashMap hmDetails = fetchMissingPartDetails(gmDBManager, emailno, hmMap);

    alDetails = (ArrayList) hmDetails.get("META");

    if (alDetails.size() != 0) {

      hmRepDetails = (HashMap) alDetails.get(0);
      strRepName = (String) hmRepDetails.get("REPNAME");
      strAssRepName = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPNM"));
      strAssrepID = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPID"));
      strRepName = strAssrepID.equals("") ? strRepName : strAssRepName;

    } else {
      hmReturn.put("REPNAME", "");
      return hmReturn;
    }

    alDetails = (ArrayList) hmDetails.get("DETAILS");

    log.debug("alDetails: " + alDetails);

    hmRepDetails.putAll((HashMap) alDetails.get(0));
    hmRepDetails.put("JASPER", gmJasperReport);
    createMissingPartTickets(gmDBManager, strRepName, strUserName, strUser, alDetails,
        hmRepDetails, emailno, msg);


    hmReturn.put("REPNAME", strRepName);

    return hmReturn;
  }

  /*
   * Description: Create Missing Parts PDF for the corresponding Request Detail
   * 
   * Author: VPrasath
   */
  private String createPDFFile(GmJasperReport gmJasperReport, HashMap hmParams,
      ArrayList alDetails, String strAttachmentName) throws AppError {
    String fileName = "";


    try {
      String strFilePath = GmCommonClass.getString("UPLOADHOME");
      fileName = strFilePath + strAttachmentName + ".pdf";
      File newFile = new File(strFilePath);
      if (!newFile.exists()) {
        newFile.mkdir();
      }
      gmJasperReport.setHmReportParameters(hmParams);
      gmJasperReport.setReportDataList(alDetails);
      gmJasperReport.exportJasperReportToPdf(fileName);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return fileName;
  }


  /*
   * Description: create JIRA Tickets when NCMR created
   * 
   * Author: Vinoth
   */

  public void createNCMREvalTicket(HashMap hmParam) throws AppError {
	  
	  GmJIRAHelperBean gmJIRAHelperBean = new GmJIRAHelperBean();
	  GmDBManager gmDBManager = new GmDBManager();
	  HashMap hmDetails = new HashMap();
	  String strEvalID = "";
	  String strDHRID = "";
	  String strTicketID = "";
	  String strProjectId = "";
	  String strUserId = "";
	  String strPartNum = "";
	  String strPartDesc = "";
	  String strProject = "";
	  String strProjectName = "";
	  String strType = "";
	  String strStatus = "";
	  String strDescription = "";
	  String strStatusId = "";
	  String strPartyName = "";
	  String strUserName = "";
	  String strPartSummary = "";
	  String strUnAssignFl = "Y";
	  String strPartCustFieldId = "";
	  String strProjCustFieldId = "";
	  String strReqCustFieldId = "";
	  
	  strEvalID = GmCommonClass.parseNull((String) hmParam.get("EVALID"));
	  strDHRID = GmCommonClass.parseNull((String) hmParam.get("DHRID"));
	  strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJID"));
	  strProjectName = GmCommonClass.parseNull((String) hmParam.get("PROJNM"));
	  strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
	  strPartDesc = GmCommonClass.parseNull((String) hmParam.get("PDESC"));
	  strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	  strUserName = GmCommonClass.parseNull((String) hmParam.get("ORIGINATOR"));
	  
	  strTicketID = gmJIRAHelperBean.checkTicketExists(strEvalID);
	  
      if(strTicketID.equals("")) {
    	  gmDBManager.setPrepareString("gm_pkg_op_ncmr.gm_fch_eval_tkt_dtls", 6);
    	  gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    	  gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    	  gmDBManager.setString(1, strEvalID);
  		  gmDBManager.setString(2, strDHRID);
  		  gmDBManager.setString(3, strProjectId);
  		  gmDBManager.setInt(4, 92044);      //92044 - Product Engineer

  		  gmDBManager.execute();
  		  String ticketFl = gmDBManager.getString(5);
//  	  ArrayList alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
  		  gmDBManager.close();
  		  if(ticketFl.equals("Y")) {
  			 // if(alList.size() == 1) {
//  				  hmDetails = (HashMap) alList.get(i);
//  				  strStatusId = GmCommonClass.parseNull((String) hmDetails.get("STATUSID"));
//  				//  strPartyName = GmCommonClass.parseNull((String) hmDetails.get("NAME"));
//  				 if(!strStatusId.equals("92055")) {  //92055 - InActive
//  					  strPartyName = GmCommonClass.parseNull((String) hmDetails.get("NAME"));
//    				  hmParam.put("PARTYNAME", strPartyName); 
//    				  hmParam.put("UNASSIGNFL", "");
//    			  }
//  			}
  			  
  			  strDescription = "Part Number: " + strPartNum + "\n" + "Part Number Description: " + strPartDesc + "\n" + "Project ID: " 
				  			    + strProjectId + "/" + strProjectName;
  			  strProject = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("PROJECT_FOR_NCMR_EVAL_ISSUES")); 
  			  strType = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("NCMR_EVAL_TICKET_TYPE")); 
  			  strStatus = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("NCMR_EVAL_TKT_INI_STATUS"));
  			  strPartSummary = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("NCMR_TICKET_SUMMARY"));
  			  strReqCustFieldId = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("REQIDN_CUSTOM_FIELD_ID"));
  			  strPartCustFieldId = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("PARTNUM_CUSTOM_FIELD_ID"));
  			  strProjCustFieldId = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("PROJECT_CUSTOM_FIELD_ID"));
  			  String strSummary = strEvalID + " " + strPartSummary + " " + strPartNum; 
 			  strSummary = GmCommonClass.parseNull(strSummary).trim(); 
		  
  			  hmParam.put("SUMMARY", strSummary);
  			  hmParam.put("DESCRIPTION", strDescription);
  			  hmParam.put("PROJECT", strProject);
  			  hmParam.put("TYPE", strType);
  			  hmParam.put("STATUS", strStatus);
  			  hmParam.put("USERNM", strUserName);
  			  hmParam.put("PARTYNAME", strPartyName);
  			  hmParam.put("UNASSIGNFL", strUnAssignFl);
  			  hmParam.put("PARTCUSTFIELDID", strPartCustFieldId);
  			  hmParam.put("PROJCUSTFIELDID", strProjCustFieldId);
  			  hmParam.put("REQCUSTFIELDID", strReqCustFieldId);
  			  log.debug("hmParam==> "+hmParam);
		  
  			  strTicketID = gmJIRAHelperBean.createJIRANCMRTicket(hmParam);
  			  log.debug("strTicketID==> "+strTicketID);
	      
  			  hmParam.put("TICKETID", strTicketID);
  			  hmParam.put("REFID", strEvalID);
  			  gmJIRAHelperBean.saveTicket(hmParam);
  			 log.debug("saveTicket==> "+hmParam);
  		  }
      }

  }
  
  /*
   * Description: Close NCMR JIRA Ticket when NCMR is closed
   * 
   * Author: Vinoth
   */

  public void closeNCMREvalTicket(HashMap hmParam) throws AppError {
	  
	  GmJIRAHelperBean gmJIRAHelperBean = new GmJIRAHelperBean();
	  String strEvalID = "";
	  String strTicketID = "";
	  String strAction = "";
	  String strMsg = "";
	  
    
	  strEvalID = GmCommonClass.parseNull((String) hmParam.get("EVALID"));
	  strTicketID = gmJIRAHelperBean.checkTicketExists(strEvalID);
	  
      if(!strTicketID.equals("")) {
    	  strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    	  
    	  if(strAction.equals("CreateNCMR")){
    		  strMsg = GmCommonClass.parseNull(GmCommonClass.getRuleValue("CREATENCMR", "NCMR_EVALUATION")) + " - "+ strEvalID;
    	  }
    	  else if(strAction.equals("Cancel")){
    		  strMsg = GmCommonClass.parseNull(GmCommonClass.getRuleValue("CANCELNCMR", "NCMR_EVALUATION")) + " - "+ strEvalID;
    	  }
    	  gmJIRAHelperBean.updateIssueComments(strTicketID,strMsg);
    	  gmJIRAHelperBean.closeIssue(strTicketID,"110520");  //110520 - NCMR Evaluation
      }

  }
}
