package com.globus.operations.logistics.beans;


/**
 * 
 */

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ssharmila
 *
 */
public class GmLoanerSetAcceptanceRptBean extends GmSalesFilterConditionBean{
	
	// Instantiating the Logger
	  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
	                                                               // Class.

	  public GmLoanerSetAcceptanceRptBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }

	  public GmLoanerSetAcceptanceRptBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	
	  public ArrayList fetchLoanerSetAcceptanceDetails(HashMap hmParam) throws AppError {
		 // HashMap hmReturn = new HashMap();
		  ArrayList alReturn = new ArrayList();
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		  String strDtFrom = GmCommonClass.parseNull((String) hmParam.get("STARTDTFROM"));
		   String strDtTo = GmCommonClass.parseNull((String) hmParam.get("STARTDTTO"));
		   String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
		   String strStatus = GmCommonClass.parseNull((String) hmParam.get("STRSTATUS"));
           gmDBManager.setPrepareString("gm_pkg_op_rfid_process.gm_fch_loaner_set_accept_rpt", 5);
            gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
            gmDBManager.setString(1, strType);
            gmDBManager.setString(2, strDtFrom);
            gmDBManager.setString(3, strDtTo);
            gmDBManager.setString(4, strStatus);
            gmDBManager.execute();
            alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
			   log.debug("fetchLoanerSetAcceptanceDetails -alReturn- " + alReturn);

		   
		  return alReturn;
	  }

	

}





