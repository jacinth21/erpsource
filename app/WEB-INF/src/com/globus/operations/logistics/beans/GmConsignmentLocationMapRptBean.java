package com.globus.operations.logistics.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * GmConsignmentLocationMapRptBean - this bean class used to fetch and return the Print Location WIP sets from T5052_LOCATION_MASTER table
 * 
 * @author RSelvaraj
 *
 */
public class GmConsignmentLocationMapRptBean extends GmBean{
	
	/**
	 * this used for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmConsignmentLocationMapRptBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	

	/** This  Constructor will validate and populate company info
	 * @param gmDataStoreVO
	 * @throws AppError
	 */

	public GmConsignmentLocationMapRptBean(GmDataStoreVO gmDataStoreVO)
		throws AppError {
		super(gmDataStoreVO);
	// TODO Auto-generated constructor stub
	}

	
	
	/**
	 * fetchConsignmentLocMapDetails - This method used to forms JSON query then fetch and return the Print location details for WIP sets in JSON format 
	 * 
	 * @param HashMap - Form values
	 * @return String
	 * @exception
	 */
	public String fetchConsignmentLocMapDetails(HashMap hmParams) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strReturn="";

		log.debug("hmParams ::: " + hmParams);
		String strWareHouse = GmCommonClass.parseZero((String) hmParams.get("WAREHOUSEID"));
		String strLocationID = GmCommonClass.parseNull((String) hmParams.get("LOCATIONID"));
		String strConsignmentID = GmCommonClass.parseNull((String) hmParams.get("CNID"));
		String strZone = GmCommonClass.parseZero((String) hmParams.get("ZONEID"));
		String strType = GmCommonClass.parseZero((String) hmParams.get("INVTYPEID"));
		String strStatus = GmCommonClass.parseZero((String) hmParams.get("STATUSID"));
		String strBuildingId = GmCommonClass.parseZero((String) hmParams.get("BUILDINGID"));
		String strPrintLoc = GmCommonClass.parseNull((String) hmParams.get("PRINTLOC"));
		String strAisle = GmCommonClass.parseNull((String) hmParams.get("AISLENUM"));
		String strShelf = GmCommonClass.parseNull((String) hmParams.get("SHELF"));
		String strPlantid = getCompPlantId();

		StringBuffer strQuery = new StringBuffer();

		strQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT('WAREHOUSENM' VALUE GM_PKG_OP_INV_WAREHOUSE.GET_WAREHOUSE_NAME('',T5052.C5051_INV_WAREHOUSE_ID), ");
		strQuery.append(" 'WID' value T5052.C5051_INV_WAREHOUSE_ID, ");
		strQuery.append(" 'LOCATION_CD' value T5052.C5052_LOCATION_CD, ");
		strQuery.append(" 'INVTYPE' value GET_CODE_NAME(T5052.C901_LOCATION_TYPE), ");
		strQuery.append(" 'STATUS' value GET_CODE_NAME(T5052.C901_STATUS), ");
		strQuery.append(" 'CONSIGNMENT_ID' value T504.C504_CONSIGNMENT_ID, ");
		strQuery.append(" 'BNM' value  T5057.C5057_BUILDING_NAME) ORDER BY t5052.C5052_LOCATION_ID RETURNING CLOB) ");

		strQuery.append("  FROM T5052_LOCATION_MASTER t5052, T504_CONSIGNMENT T504, t5057_building t5057");  
		strQuery.append("  where T5052.C5052_LOCATION_ID = T504.C5052_LOCATION_ID (+)"); 
		strQuery.append("   AND t5052.c5051_inv_warehouse_id NOT IN (SELECT c906_rule_value  FROM t906_rules  WHERE c906_rule_id = 'WHID' AND c906_rule_grp_id = 'RULEWH' AND c906_void_fl IS NULL) ");
		strQuery.append("   AND t5052.C5052_VOID_FL IS NULL  ");
		strQuery.append("   AND T504.C504_VOID_FL IS NULL  ");
		strQuery.append("   AND t5052.C5057_BUILDING_ID = t5057.C5057_BUILDING_ID(+) ");
		strQuery.append("   AND t5057.C5057_VOID_FL IS NULL  ");
		strQuery.append("   AND T5052.C901_LOCATION_TYPE IN (26240846)  "); //WIP Set
		strQuery.append("   AND T5052.C901_STATUS IN (93310,93202)  ");
		if (!strWareHouse.equals("0") && !strWareHouse.equals("")) {
			strQuery.append("   AND t5052.c5051_inv_warehouse_id =" + strWareHouse);
		}
		if (!strLocationID.equals("")) {
			strQuery
			.append("   AND REGEXP_LIKE(NVL(t5052.C5052_LOCATION_CD,t5052.C5052_LOCATION_ID) ,  CASE WHEN UPPER('"
					+ strLocationID
					+ "') IS NOT NULL THEN UPPER('"
					+ strLocationID
					+ "') ELSE NVL(t5052.C5052_LOCATION_CD,t5052.C5052_LOCATION_ID) END)");
		}

		if (!strConsignmentID.trim().equals("")) {
			String[] strArrayId ={};
			if (!strConsignmentID.equals("")) {
				strArrayId = strConsignmentID.trim().split(",");
			}
			strQuery.append(" AND (");
			int length = strArrayId.length;
			for (int i = 0; i < length; i++) {
				if (i == (length - 1)) {
					strQuery.append(" T504.C504_CONSIGNMENT_ID LIKE ");
					strQuery.append("'%" + strArrayId[i].trim() + "%'");
				} else {
					strQuery.append(" T504.C504_CONSIGNMENT_ID LIKE ");
					strQuery.append("'%" + strArrayId[i].trim() + "%' OR");
				}
			}
			strQuery.append(" ) ");
		}

		if (!strZone.equals("0") && !strZone.equals("")) {
			strQuery.append("   AND t5052.C901_ZONE =" + strZone);
		}

		if (!strAisle.equals("")) {
		 strQuery.append("   AND t5052.C5052_AISLE ='" + strAisle + "'");
	    }
		
	    if (!strShelf.equals("")) {
	    	strQuery.append("   AND t5052.C5052_SHELF ='" + strShelf + "'");
	    }
	    
		if (!strType.equals("0") && !strType.equals("")) {
			strQuery.append("   AND t5052.C901_LOCATION_TYPE =" + strType);
		}
		if (!strStatus.equals("0") && !strStatus.equals("")) {
			strQuery.append("   AND t5052.C901_STATUS =" + strStatus);
		}
		if (!strBuildingId.equals("0") && !strBuildingId.equals("")) {
			strQuery.append(" AND t5052.C5057_BUILDING_ID =" + strBuildingId);
		}
		// Added PlantId for to load records depends on PlandId
		strQuery.append(" AND  t5052.C5040_PLANT_ID =" + strPlantid);

		log.debug(" Print Location for WIP Sets Report screen - fetchConsignmentLocMapDetails =>"+ strQuery.toString());
		strReturn = GmCommonClass.parseNull(gmDBManager.queryJSONRecord(strQuery.toString()));

		return strReturn;

	}

	 

	/**
	 * printInvLocation - This method used to fetch the WIP Sets location ID and location code for jasper 
	 * 
	 * @param HashMap - Form values
	 * @return ArrayList
	 * @exception
	 */
	public ArrayList printInvLocation(HashMap hmParams) throws AppError {

		ArrayList alResult = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		log.debug("hmParams Print::: " + hmParams);
		String strWareHouse = GmCommonClass.parseZero((String) hmParams.get("WAREHOUSEID"));
		String strLocationID = GmCommonClass.parseNull((String) hmParams.get("LOCATIONID"));
		String strConsignmentID = GmCommonClass.parseNull((String) hmParams.get("CNID"));
		String strZone = GmCommonClass.parseZero((String) hmParams.get("ZONEID"));
		String strType = GmCommonClass.parseZero((String) hmParams.get("INVTYPEID"));
		String strStatus = GmCommonClass.parseZero((String) hmParams.get("STATUSID"));
		String strBuildingId = GmCommonClass.parseZero((String) hmParams.get("BUILDINGID"));
		String strPrintLoc = GmCommonClass.parseNull((String) hmParams.get("PRINTLOC"));
		String strAisle = GmCommonClass.parseNull((String) hmParams.get("AISLENUM"));
		String strShelf = GmCommonClass.parseNull((String) hmParams.get("SHELF"));
		String strPlantid = getCompPlantId();

		StringBuffer strQuery = new StringBuffer();

		strQuery.append(" SELECT DISTINCT t5052.C5052_LOCATION_ID LOCATION_ID ");
		strQuery.append(" , NVL (C5052_LOCATION_CD, t5052.C5052_LOCATION_ID) LCD");
		strQuery.append("  FROM T5052_LOCATION_MASTER t5052, T504_CONSIGNMENT T504, t5057_building t5057");  
		strQuery.append("  where T5052.C5052_LOCATION_ID = T504.C5052_LOCATION_ID (+)"); 
		strQuery.append("   AND t5052.c5051_inv_warehouse_id NOT IN (SELECT c906_rule_value  FROM t906_rules  WHERE c906_rule_id = 'WHID' AND c906_rule_grp_id = 'RULEWH' AND c906_void_fl IS NULL) ");
		strQuery.append("   AND t5052.C5052_VOID_FL IS NULL  ");
		strQuery.append("   AND T504.C504_VOID_FL IS NULL  ");
		strQuery.append("   AND t5052.C5057_BUILDING_ID = t5057.C5057_BUILDING_ID(+) ");
		strQuery.append("   AND t5057.C5057_VOID_FL IS NULL  ");
		strQuery.append("   AND T5052.C901_LOCATION_TYPE IN (26240846)  "); //WIP Set
		strQuery.append("   AND T5052.C901_STATUS IN (93310,93202)  ");
		if (!strWareHouse.equals("0") && !strWareHouse.equals("")) {
			strQuery.append("   AND t5052.c5051_inv_warehouse_id =" + strWareHouse);
		}
		if (!strLocationID.equals("")) {
			strQuery
			.append("   AND REGEXP_LIKE(NVL(t5052.C5052_LOCATION_CD,t5052.C5052_LOCATION_ID) ,  CASE WHEN UPPER('"
					+ strLocationID
					+ "') IS NOT NULL THEN UPPER('"
					+ strLocationID
					+ "') ELSE NVL(t5052.C5052_LOCATION_CD,t5052.C5052_LOCATION_ID) END)");
		}

		if (!strConsignmentID.trim().equals("")) {
			String[] strArrayId ={};
			if (!strConsignmentID.equals("")) {
				strArrayId = strConsignmentID.trim().split(",");
			}
			strQuery.append(" AND (");
			int length = strArrayId.length;
			for (int i = 0; i < length; i++) {
				if (i == (length - 1)) {
					strQuery.append(" T504.C504_CONSIGNMENT_ID LIKE ");
					strQuery.append("'%" + strArrayId[i].trim() + "%'");
				} else {
					strQuery.append(" T504.C504_CONSIGNMENT_ID LIKE ");
					strQuery.append("'%" + strArrayId[i].trim() + "%' OR");
				}
			}
			strQuery.append(" ) ");
		}

		if (!strZone.equals("0") && !strZone.equals("")) {
			strQuery.append("   AND t5052.C901_ZONE =" + strZone);
		}

		if (!strAisle.equals("")) {
		 strQuery.append("   AND t5052.C5052_AISLE ='" + strAisle + "'");
	    }
			
	    if (!strShelf.equals("")) {
	    	strQuery.append("   AND t5052.C5052_SHELF ='" + strShelf + "'");
	    }
		    
		if (!strType.equals("0") && !strType.equals("")) {
			strQuery.append("   AND t5052.C901_LOCATION_TYPE =" + strType);
		}
		if (!strStatus.equals("0") && !strStatus.equals("")) {
			strQuery.append("   AND t5052.C901_STATUS =" + strStatus);
		}
		if (!strBuildingId.equals("0") && !strBuildingId.equals("")) {
			strQuery.append(" AND t5052.C5057_BUILDING_ID =" + strBuildingId);
		}
		// Added PlantId for to load records depends on PlandId
		strQuery.append(" AND  t5052.C5040_PLANT_ID =" + strPlantid);

		strQuery.append("   ORDER BY t5052.C5052_LOCATION_ID ");

		log.debug(" Print Location for WIP Sets Report screen - printInvLocation " + strQuery.toString());

		gmDBManager.setPrepareString(strQuery.toString());
		alResult = gmDBManager.queryMultipleRecord();

		return alResult;

	}
	 
}
