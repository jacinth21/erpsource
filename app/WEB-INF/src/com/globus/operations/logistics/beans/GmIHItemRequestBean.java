package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmIHItemRequestBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmIHItemRequestBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmIHItemRequestBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * fetchIHBORequest This method will fetch the Inhouse Back Order Request
   * 
   * @param HashMap hmParam
   * @return ArrayList alReturn
   * @exception AppError Author Jignesh Shah
   */
  public ArrayList fetchIHBORequest(HashMap hmParam) throws AppError, Exception {

    ArrayList alReturn = new ArrayList();

    String strReqType = GmCommonClass.parseNull((String) hmParam.get("REQTYPE"));
    String strReqPurpose = GmCommonClass.parseNull((String) hmParam.get("PURPOSE"));
    String strReqStatus = GmCommonClass.parseNull((String) hmParam.get("HSTATUS"));

    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strTagId = GmCommonClass.parseNull((String) hmParam.get("STRTAGID"));
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery
        .append("  SELECT T412.C412_Inhouse_Trans_Id reqid, T412.C412_Type reqtypeid, Get_Code_Name (T412.C412_Type) reqtype ");
    sbQuery
        .append("   , Get_Code_Name (T412.C412_Inhouse_Purpose) Reqpurpose, T412.C412_Created_Date Reqdate ");
    sbQuery
        .append("   , DECODE (T412.C412_Status_Fl,'0', 'Open', '3', 'Reviewed','4','Closed') reqstatus ");
    sbQuery
        .append("   , GET_LOG_FLAG(T412.C412_Inhouse_Trans_Id,1288) cmtlogflag , T412.C412_Status_Fl statusfl");
    sbQuery.append("    FROM T412_Inhouse_Transactions T412 ");
    if (strReqType.equals("0")) {
      sbQuery.append("   WHERE T412.C412_Type    IN ('1006483','4127','1006572') ");
    } else if (strReqType.equals("1006483")) {
      sbQuery.append("   WHERE T412.C412_Type    IN ('1006483','1006572') ");
    } else if (strReqType.equals("4127")) {
      sbQuery.append("   WHERE T412.C412_Type    IN ('4127') ");
    }
    sbQuery.append("     AND T412.C412_Void_Fl IS NULL ");
    sbQuery.append("     AND T412.C1900_COMPANY_ID = " + getGmDataStoreVO().getCmpid());
    if (!strReqPurpose.equals("0")) {
      sbQuery.append(" AND T412.C412_Inhouse_Purpose = " + strReqPurpose);
    }
    // if(!strReqStatus.equals("")){
    if (strReqStatus.equals("")) {
      strReqStatus = "NULL";
    }
    sbQuery.append(" AND T412.C412_Status_Fl IN (" + strReqStatus + ") ");
    // }
    if (!strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND Trunc (T412.C412_Created_Date) BETWEEN TO_DATE ('" + strFromDt);
      sbQuery.append("','" + strDateFmt + "') AND TO_DATE ('" + strToDt + "', '" + strDateFmt
          + "') ");
    }
    
    if (!strTagId.equals("")) {
      sbQuery.append("AND t412.c412_ref_id IN (Select c5010_last_updated_trans_id from t5010_tag where c5010_tag_id ='"+strTagId+"')  ");
      
    }

    log.debug(" Query " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    log.debug("alReturn === inside == fetchIHBORequest = " + alReturn);
    return alReturn;
  }// End of fetchIHBORequest

  /**
   * fetchIHRequestDetails This method will fetch the HeaderInfo to Reconfigure the request
   * 
   * @param HashMap hmParam
   * @return ArrayList alReturn
   * @exception AppError Author Jignesh Shah
   */
  public ArrayList fetchIHRequestDetails(HashMap hmParam) throws AppError {

    ArrayList alRuturn = new ArrayList();
    String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));

    log.debug("strReqId= " + strReqId);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_ihloaner_item_rpt.gm_fch_ihitem_dtl", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strReqId);
    gmDBManager.execute();
    alRuturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    log.debug(" alRuturn In fetchIHRequestDetails =  " + alRuturn);
    return alRuturn;
  }// End of fetchIHRequestDetails


  /**
   * fetchReplenishmentTxn This method will fetch the ReplenishTxn details for the request
   * 
   * @param HashMap hmParam
   * @return ArrayList alReturn
   * @exception AppError Author Jignesh Shah
   */
  public ArrayList fetchReplenishmentTxn(HashMap hmParam) throws AppError {

    ArrayList alRuturn = new ArrayList();
    String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));

    log.debug("strReqId= " + strReqId);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_ihloaner_item_rpt.gm_fch_replenishment_txn", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strReqId);
    gmDBManager.execute();
    alRuturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    log.debug(" alRuturn In fetchIHRequestDetails =  " + alRuturn);
    return alRuturn;
  }// End of fetchReplenishmentTxn


  /**
   * fetchIHRequestDetails This method will fetch the HeaderInfo to Reconfigure the request
   * 
   * @param HashMap hmParam
   * @return ArrayList alReturn
   * @exception AppError Author Jignesh Shah
   */
  public HashMap saveIHItemBOReleaseDtl(HashMap hmParam) throws AppError {
    String strInHouseTxns = "";
    String strTxns = "";
    HashMap hmReturn = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean();
    String strReqID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strReqType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPEID"));
    String strFGString = GmCommonClass.parseNull((String) hmParam.get("STRFGSTRING"));
    String strBLString = GmCommonClass.parseNull((String) hmParam.get("STRBLSTRING"));
    String strIHString = GmCommonClass.parseNull((String) hmParam.get("STRIHSTRING"));
    String strVDString = GmCommonClass.parseNull((String) hmParam.get("STRVDSTRING"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_ihloaner_item_txn.gm_sav_rel_ihbo_dtl", 9);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strReqID);
    gmDBManager.setString(2, strReqType);
    gmDBManager.setString(3, strFGString);
    gmDBManager.setString(4, strBLString);
    gmDBManager.setString(5, strIHString);
    gmDBManager.setString(6, strVDString);
    gmDBManager.setString(7, strUserId);
    gmDBManager.execute();
    strInHouseTxns = gmDBManager.getString(8);
    strTxns = gmDBManager.getString(9);
    hmReturn.put("INHOUSETXN", strInHouseTxns);
    hmReturn.put("TXN", strTxns);
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strReqID, strComments, strUserId, "1288");
    }
    gmDBManager.commit();
    return hmReturn;
  }// End of saveIHItemBOReleaseDtl

  public void saveIHItemBOStatus(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean();
    String strReqID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUSFL"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_trans_recon.gm_sav_ln_status", 3);
    gmDBManager.setString(1, strReqID);
    gmDBManager.setString(2, strStatus);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();

    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strReqID, strComments, strUserId, "1288");
    }
    gmDBManager.commit();
  }

  /**
   * fetchInvQtyPartMappingByID This method will fetch the Quantity for the requested Part
   * 
   * @param HashMap hmParam
   * @return ArrayList alReturn
   * @exception AppError Author Jignesh Shah
   */
  public String fetchInvQtyPartMappingByID(String strPartNum, String strReplenishId)
      throws AppError {

    String strItemQty = "";
    String strFunctionString = "";

    if (strReplenishId.equals("102751")) {
      strFunctionString = "Get_Qty_In_Stock";
    } else if (strReplenishId.equals("102752")) {
      strFunctionString = "GET_QTY_IN_INHOUSEINV";
    } else if (strReplenishId.equals("102753")) {
      strFunctionString = "Get_Qty_In_Bulk";
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString(strFunctionString, 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.execute();
    strItemQty = gmDBManager.getString(1);
    gmDBManager.close();
    return strItemQty;
  }// End of fetchInvQtyPartMappingByID

}
