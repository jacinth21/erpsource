/**
 * 
 */
package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ppandiyan
 *
 */
public class GmLoanerDisputedBean  extends GmBean{

	 Logger log = GmLogger.getInstance(this.getClass().getName());

	    GmCommonClass gmCommon = new GmCommonClass();

	    /**
	     * Constructor will populate company info.
	     * 
	     * @param gmDataStore
	     */
	public GmLoanerDisputedBean(GmDataStoreVO gmDataStore) {
		// TODO Auto-generated constructor stub
		 super(gmDataStore);
	}
	
	 /**
     * initDisputedEmailJMS - This method will form the queue message 
     * and send through JMS to send a mail while the processing disputed Loaners
     * 
     * @param - hmParam
     * @return - Void
     * @exception - AppError
     */
    public void initDisputedEmailJMS(HashMap hmParam) throws AppError {

      HashMap hmEmailParam = new HashMap();

      log.debug( "*****************Init Disputed Mail ********************");
      GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();

      String strConsumerClass =
    		  GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("LOANER_DISPUTED_CONSUMER_CLASS"));
      
      hmEmailParam.put("HMVALUES", hmParam);
      hmEmailParam.put("CONSUMERCLASS", strConsumerClass);
      gmConsumerUtil.sendMessage(hmEmailParam);

    }
	 /**
     * sendLoanerDisputedMailProcess - This method is used to separate   
     * which mail has to send for which disputed process based on the action type
     * 
     * @param - hmParam
     * @return - Void
     * @exception - AppError
     */  
    public void sendLoanerDisputedMailProcess (HashMap hmParam) throws AppError{
        
        String strAction = GmCommonClass.parseNull((String)  hmParam.get("ACTION"));
        if(strAction.equals("DISPUT")){
        	sendLoanerDisputedEmail(hmParam);
        }else{
        	sendLoanerDisputedTransferMail (hmParam);
        }
       
    }
	 /**
     * sendLoanerDisputedEmail - This method will send the mail 
     * when loaner set is moving to Disputed 
     * 
     * @param - hmParam
     * @return - Void
     * @exception - AppError
     */   
    public void sendLoanerDisputedEmail(HashMap hmParam) throws AppError {
    	
	    GmEmailProperties emailProps = new GmEmailProperties();
		GmJasperMail jasperMail = new GmJasperMail();
	    HashMap hmReturn = new HashMap();
	    String strTemplateNm = "GmLoanerDisputed";
	    String strJasperName = "/GmLoanerDisputed.jasper";
	    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
	    
	    //fetch Disputed Loaner  details to show in email
	    hmReturn = GmCommonClass.parseNullHashMap(fetchLoanerDisputedEmailDtl(hmParam));
	    	    
    	//Email Template Resource bundle
    	GmResourceBundleBean gmResourceBundleBean =
    	     		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    	//setting email properties
    	 emailProps.setMimeType(gmResourceBundleBean.getProperty(strTemplateNm + "."
    	            + GmEmailProperties.MIME_TYPE));
    	 emailProps.setSender(gmResourceBundleBean.getProperty(strTemplateNm + "."
    	            + GmEmailProperties.FROM));
    	 emailProps.setCc(gmResourceBundleBean.getProperty(strTemplateNm + "."
    	            + GmEmailProperties.CC));
    	 emailProps.setRecipients(gmResourceBundleBean.getProperty(strTemplateNm + "."+ GmEmailProperties.TO));
    	 String strSubject = gmResourceBundleBean.getProperty(strTemplateNm +"."+ GmEmailProperties.SUBJECT);
    	 strSubject =
    	            GmCommonClass.replaceAll(strSubject, "#<CN>", GmCommonClass.parseNull((String) hmReturn.get("CONSIGNID")));
    	 emailProps.setSubject(strSubject);
    	 emailProps.setEmailHeaderName(gmResourceBundleBean.getProperty(strTemplateNm + "."+ GmEmailProperties.EMAIL_HEADER_NM));
    	 
    	//setting Jasper Mail
    	 jasperMail.setJasperReportName(strJasperName);
    	 jasperMail.setAdditionalParams(hmReturn);
    	 jasperMail.setReportData(null);
    	 jasperMail.setEmailProperties(emailProps);

    	 if (!hmReturn.isEmpty()){
    	        HashMap hmReturnDetails = jasperMail.sendMail(); //send mail
    	 }

    }

	 /**
     * fetchLoanerDisputedEmailDtl - This method is used to fetch Loaner Disputed Details 
     * 
     * @param - hmParam
     * @return - Void
     * @exception - AppError
     */ 
    public HashMap fetchLoanerDisputedEmailDtl(HashMap hmParam) {
    	
    	 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
    	 HashMap hmReturn = new HashMap();
    	 String strCancelType = GmCommonClass.parseNull((String)  hmParam.get("CANCELTYPE"));
    	 String strConsignId = GmCommonClass.parseNull((String)  hmParam.get("CONSID"));
    	//procedure call for disputed loaner email details
         gmDBManager.setPrepareString("gm_pkg_set_dispute.gm_fch_loaner_disputed_dtl", 3);
         gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
         gmDBManager.setString(1, strConsignId);
         gmDBManager.setString(2, strCancelType);
         gmDBManager.execute();
         hmReturn =  GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3)));
         gmDBManager.close();
         return hmReturn;

    }
	 /**
     * sendLoanerDisputedTransferMail - This method is used to send mail 
     *  while the processing the Returns and transfers for disputed Loaners
     * 
     * @param - hmParam
     * @return - Void
     * @exception - AppError
     */
    public void sendLoanerDisputedTransferMail(HashMap hmParam){
	   	 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
		 GmEmailProperties emailProps = new GmEmailProperties();
		 GmJasperMail jasperMail = new GmJasperMail();
	   	 HashMap hmEmailParams = new HashMap();
	   	 ArrayList alResult = new ArrayList();
	   	 
	   	 String strAction = GmCommonClass.parseNull((String)  hmParam.get("ACTION"));
	   	 String strConsignId = GmCommonClass.parseNull((String)  hmParam.get("CONSID"));
	   	 String strLoanTransId = GmCommonClass.parseNull((String)  hmParam.get("LOANTRANSID"));
	   	 String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
	   	 String strTemplateNm = "GmLoanerDisputedEmail"+ strAction;
	     String strJasperName = "/GmLoanerDisputedTransfer.jasper";
	     String strMsg ="";
	     String strFromRepNm = "";


	     //procedure call for disputed loaner email details
         gmDBManager.setPrepareString("gm_pkg_set_dispute.gm_fch_ln_disputed_trans_email_dtl", 4);
         gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
         gmDBManager.setString(1, strConsignId);
         gmDBManager.setString(2, strAction);
         gmDBManager.setString(3, strLoanTransId);      
         gmDBManager.execute();
         alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4)));
         gmDBManager.close();
         
         //logic to get the From Sales Rep Name
         if (!alResult.isEmpty()){
         strFromRepNm = GmCommonClass.parseNull((String) ((HashMap)alResult.get(0)).get("FRMREPNM"));
         }
        
         //Email Template Resource bundle
         GmResourceBundleBean gmResourceBundleBean =
        		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
         //setting email properties
         emailProps.setMimeType(gmResourceBundleBean.getProperty(strTemplateNm + "."
            + GmEmailProperties.MIME_TYPE));
         emailProps.setSender(gmResourceBundleBean.getProperty(strTemplateNm + "."
            + GmEmailProperties.FROM));
         emailProps.setCc(gmResourceBundleBean.getProperty(strTemplateNm + "."
            + GmEmailProperties.CC));
         emailProps.setRecipients(gmResourceBundleBean.getProperty(strTemplateNm + "."+ GmEmailProperties.TO));
         emailProps.setSubject(gmResourceBundleBean.getProperty(strTemplateNm +"."+ GmEmailProperties.SUBJECT));
         strMsg = gmResourceBundleBean.getProperty(strTemplateNm +"."+ GmEmailProperties.MESSAGE);

         emailProps.setEmailHeaderName(gmResourceBundleBean.getProperty(strTemplateNm + "."+ GmEmailProperties.EMAIL_HEADER_NM));
         //emailProps.setMessageFooter(gmResourceBundleBean.getProperty(strTemplateNm + "."+ GmEmailProperties.MESSAGE_FOOTER));
         hmEmailParams.put("HEADER_MSG", strMsg+" "+strFromRepNm);
         hmEmailParams
           .put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
         hmEmailParams
           .put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSEmail"));
         
         //setting Jasper Mail
         jasperMail.setJasperReportName(strJasperName);
         jasperMail.setAdditionalParams(hmEmailParams);
         jasperMail.setReportData(alResult);
         jasperMail.setEmailProperties(emailProps);

         if (!alResult.isEmpty()){
        	HashMap hmReturnDetails = jasperMail.sendMail();//send mail
         }
        
    }

}
