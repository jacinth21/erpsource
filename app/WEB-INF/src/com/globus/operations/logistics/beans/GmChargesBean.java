package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.rule.beans.GmRuleParamVO;
import com.globus.common.util.excel.GmExcelManipulation;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmChargesBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmChargesBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmChargesBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public ArrayList fetchChargesReport(HashMap hmParam) throws AppError {
    // RowSetDynaClass rdReturn = null;
    ArrayList alMaster = new ArrayList();
    ArrayList alRequest = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmMaster = new HashMap();
    HashMap hmRequest = new HashMap();
    String strMasterReqId = "";
    String strReqId = "";
    String strChargeType = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strDist = GmCommonClass.parseNull((String) hmParam.get("DIST"));
    String strRep = GmCommonClass.parseNull((String) hmParam.get("REP"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strIncident = GmCommonClass.parseNull((String) hmParam.get("INCIDENT"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("CHARGESFOR"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strShowDtl = GmCommonClass.parseNull((String) hmParam.get("SHWDTL"));
    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID")); //PMT-8847
    strDist = strDist.equals("0") ? "" : strDist;
    strRep = strRep.equals("0") ? "" : strRep;
    strStatus = strStatus.equals("0") ? "" : strStatus;
    strIncident = strIncident.equals("0") ? "" : strIncident;
    strType = strType.equals("0") ? "" : strType;
    strRequestId = strRequestId.equals("0") ? "" : strRequestId;  //PMT-8847 
    String strCompDFmt = getCompDateFmt();
    
    String strFilter = 
    		GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_CHARGES_RPT"));/*LOANER_CHARGES_RPT- To fetch the records for EDC entity countries based on plant*/
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();
    
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT  T9200.C9200_REF_ID || '-' || TO_CHAR(C9200_INCIDENT_DATE,'YYYYMM') PKEY, C9201_CHARGE_DETAILS_ID ID, GET_REP_NAME(t9201.c703_sales_rep_id) SALESREP,");
    sbQuery
        .append(" GET_DISTRIBUTOR_NAME(GET_DISTRIBUTOR_ID(C703_SALES_REP_ID)) distname, GET_DISTRIBUTOR_TYPE(GET_DISTRIBUTOR_ID(C703_SALES_REP_ID)) DIST_TYPE, ");
    sbQuery.append("  t9200.c9200_ref_id LOANERTRANSID, t901.C901_CODE_NM INCIDENTTYPE");
    sbQuery
        .append(" ,  DECODE(c901_incident_type,92072,'', DECODE (t901.C902_CODE_NM_ALT, '', t9200.C9200_INCIDENT_VALUE, get_code_name (t9200.C9200_INCIDENT_VALUE))) INCIDENTVALUE , to_char(C9201_STATUS) STFL,");
    sbQuery.append(" decode(c9201_status,10,'Open',20,'Approved',40,'Cancelled','') STFLNAME ");
    sbQuery.append(" , C9200_INCIDENT_DATE incidentdate");
    sbQuery.append(" , C9201_AMOUNT amount , c901_incident_type itype ");
    sbQuery
        .append(" ,DECODE(c901_incident_type,92072,c9200_ref_id,92069,c9200_ref_id,gm_pkg_op_loaner.get_loaner_con_id(c9200_ref_id)) CNID");
    sbQuery
        .append(" ,DECODE(c901_incident_type,92072,'',GET_SET_NAME (DECODE(c901_incident_type,92069,t9200.C9200_INCIDENT_VALUE,gm_pkg_op_consignment.get_set_id (gm_pkg_op_loaner.get_loaner_con_id (c9200_ref_id))))) setdesc");
    sbQuery
        .append(" ,t9201.c205_part_number partnum ,get_partnum_desc(t9201.c205_part_number) partdesc,t9201.c9201_qty_missing qtymiss");
    sbQuery
        .append(" ,t9201.c9201_unit_cost unitcost ,NVL(t9201.c9201_qty_missing,0) * NVL(t9201.c9201_unit_cost,0) extendamount ");
    sbQuery.append(" , '' ldate , '' erdate,''rdate, '' elpdays ");
    sbQuery.append(" ,GET_REP_NAME(t9201.c703_ass_rep_id) ASSOCREP");
    sbQuery.append(" , TO_CHAR (t9201.C9201_CREDITED_DATE , '"+strCompDFmt+"') creditdate ");
    sbQuery
        .append(" FROM t9200_incident t9200, t9201_charge_details t9201, T901_code_lookup t901 ");
    sbQuery.append(" WHERE t9200.C9200_INCIDENT_ID = t9201.c9200_incident_id ");
    sbQuery.append(" AND t9200.C901_INCIDENT_TYPE = t901.C901_CODE_ID");
    sbQuery.append(" AND t9200.C9200_VOID_FL IS NULL");
    sbQuery.append(" AND t9201.c9201_void_fl IS NULL");
    sbQuery.append(" AND ( t9200.C1900_company_id = " + getCompId());
    sbQuery.append(" OR  t9200.c1900_company_id  IN (select c1900_company_id from t5041_plant_company_mapping where C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =" +strFilter);
    sbQuery.append(") ");
    sbQuery.append(") ");
    // sbQuery.append(" and T901.C901_ACTIVE_FL is NOT NULL ");
    if (!strDist.equals("")) {
      sbQuery.append(" and t9201.C701_DISTRIBUTOR_ID = '");
      sbQuery.append(strDist);
      sbQuery.append("' ");
    }
    if (!strRep.equals("")) {
      sbQuery.append(" and t9201.C703_SALES_REP_ID =");
      sbQuery.append(strRep);

    }
    if (!strStatus.equals("")) {
      sbQuery.append(" and t9201.C9201_STATUS = ");
      sbQuery.append(strStatus);
    }
    if (!strIncident.equals("")) {
      sbQuery.append(" and t901.C901_CODE_ID=");
      sbQuery.append(strIncident);
    }
    if (!strType.equals("")) {
      sbQuery.append(" and t9200.C9200_REF_TYPE =");
      sbQuery.append(strType);
    }
    sbQuery.append(" and t9201.C9201_AMOUNT > 0 ");// Should show only if the amount is greater than
                                                   // 0
    if (!strFromDt.equals("") && !strToDt.equals("")) {
      sbQuery.append(" AND trunc(t9200.C9200_INCIDENT_DATE) >= TO_DATE('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strCompDFmt + "'");
      sbQuery.append(" ) ");
      sbQuery.append(" AND trunc(t9200.C9200_INCIDENT_DATE) <= TO_DATE('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strCompDFmt + "'");

      sbQuery.append(" ) ");
    }
    
    if (!strRequestId.equals("")) { //PMT-8847 
        sbQuery.append(" AND t9200.C9200_REF_ID ='");
        sbQuery.append(strRequestId);
        sbQuery.append("' ");
      }
    
    log.debug("fetchChargesReport Master " + sbQuery.toString());
    // rdReturn = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    alMaster = gmDBManager.queryMultipleRecords(sbQuery.toString());

    sbQuery.setLength(0);
    if (strShowDtl.equals("on")) {
      sbQuery
          .append("SELECT c9200_ref_id|| '-'|| TO_CHAR (c9200_incident_date, 'YYYYMM') pkey, get_code_name (t9200.c901_incident_type) incidenttype,");
      sbQuery
          .append("t504a.c504_consignment_id incidentvalue, c9200_ref_id cnid, get_set_name (t526.c207_set_id) setdesc, '' DIST_TYPE,");
      sbQuery
          .append("t504a.c504a_loaner_dt ldate, t504a.c504a_expected_return_dt erdate, c504a_return_dt rdate,");
      sbQuery
          .append("get_work_days_count (TO_CHAR (TRUNC (t504a.c504a_expected_return_dt),'MM/dd/yyyy'), TO_CHAR (TRUNC (NVL (");
      sbQuery
          .append("t504a.c504a_return_dt, CURRENT_DATE)),'MM/dd/yyyy')) elpdays, '' ID, '' salesrep,'' distname, '' loanertransid, '' stfl,");
      sbQuery
          .append("'' stfalname, '' incidentdate, '' amount,'' partnum, '' partdesc, '' qtymiss,'' unitcost");
      sbQuery
          .append(" FROM t9200_incident t9200, t526_product_request_detail t526, t504a_loaner_transaction t504a");
      sbQuery.append(" WHERE t9200.c901_incident_type            = '92072'");
      sbQuery.append(" AND t9200.c9200_ref_id                  = t526.c525_product_request_id");
      sbQuery
          .append(" AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id");
      //sbQuery.append(" AND t504a.c504a_return_dt              IS NOT NULL");  //removed this line for PMT-8847       
      sbQuery.append(" AND t9200.c9200_void_fl                IS NULL");
      sbQuery.append(" AND t526.c526_void_fl                  IS NULL");
      sbQuery.append(" AND t504a.c504a_void_fl                IS NULL ");       
      sbQuery.append(" AND ( t9200.C1900_company_id = " + getCompId());
      sbQuery.append(" OR  t9200.c1900_company_id  IN (select c1900_company_id from t5041_plant_company_mapping where C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =" +strFilter);
      sbQuery.append(") ");
      sbQuery.append(") ");
      sbQuery.append(" AND t9200.c1900_company_id =t526.c1900_company_id");       
      if (!strFromDt.equals("") && !strToDt.equals("")) {
        sbQuery.append(" AND t9200.C9200_INCIDENT_DATE >= TO_DATE('");
        sbQuery.append(strFromDt);
        sbQuery.append("','" + strCompDFmt + "'");
        sbQuery.append(" ) ");
        sbQuery.append(" AND t9200.C9200_INCIDENT_DATE <= TO_DATE('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strCompDFmt + "'");
        sbQuery.append(" ) ");
      }
      
      if (!strRequestId.equals("")) { //PMT-8847 
          sbQuery.append(" AND t9200.C9200_REF_ID ='");
          sbQuery.append(strRequestId);
          sbQuery.append("' ");
        }
      log.debug("fetchChargesReport request " + sbQuery.toString());
      // rdReturn = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
      alRequest = gmDBManager.queryMultipleRecords(sbQuery.toString());

      for (int i = 0; i < alMaster.size(); i++) {
        hmMaster = (HashMap) alMaster.get(i);
        strMasterReqId = GmCommonClass.parseNull((String) hmMaster.get("PKEY"));
        strChargeType = GmCommonClass.parseNull((String) hmMaster.get("ITYPE"));
        alResult.add(hmMaster);
         if (strChargeType.equals("92072")) { // Late Return Details only need to be added
          for (int j = 0; j < alRequest.size(); j++) {
            hmRequest = (HashMap) alRequest.get(j);
            strReqId = GmCommonClass.parseNull((String) hmRequest.get("PKEY"));
            if (strMasterReqId.equals(strReqId)) {
              alResult.add(hmRequest);
            }
          }
        }
      }
      return alResult;
    } else {
      return alMaster;
    }
  }

  public ArrayList fetchChargesLateFeeBySetReport(HashMap hmParam) throws AppError {
	    // RowSetDynaClass rdReturn = null;
	    ArrayList alMaster = new ArrayList();
	    ArrayList alRequest = new ArrayList();
	    ArrayList alResult = new ArrayList();
	    HashMap hmMaster = new HashMap();
	    HashMap hmRequest = new HashMap();
	    String strMasterReqId = "";
	    String strReqId = "";
	    String strChargeType = "";
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	    String strDist = GmCommonClass.parseNull((String) hmParam.get("DIST"));
	    String strRep = GmCommonClass.parseNull((String) hmParam.get("REP"));
	    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
	    String strIncident = GmCommonClass.parseNull((String) hmParam.get("INCIDENT"));
	    String strType = GmCommonClass.parseNull((String) hmParam.get("CHARGESFOR"));
	    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
	    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
	    String strShowDtl = GmCommonClass.parseNull((String) hmParam.get("SHWDTL"));
	    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID")); //PMT-8847
	    strDist = strDist.equals("0") ? "" : strDist;
	    strRep = strRep.equals("0") ? "" : strRep;
	    strStatus = strStatus.equals("0") ? "" : strStatus;
	    strIncident = strIncident.equals("0") ? "" : strIncident;
	    strType = strType.equals("0") ? "" : strType;
	    strRequestId = strRequestId.equals("0") ? "" : strRequestId;  //PMT-8847 
	    String strCompDFmt = getCompDateFmt();
	    
	    String strFilter = 
	    		GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_CHARGES_RPT"));/*LOANER_CHARGES_RPT- To fetch the records for EDC entity countries based on plant*/
	    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();
	    
	    StringBuffer sbQuery = new StringBuffer();

	    sbQuery.append(" SELECT T9200.C9200_REF_ID || '-'|| TO_CHAR(C9200_INCIDENT_DATE,'YYYYMM') PKEY,C9201_Charge_Details_Id Id,Get_Rep_Name(t504a.C703_Sales_Rep_Id) Salesrep ");
	    sbQuery.append(" ,Get_Distributor_Name(Get_Distributor_Id(t504a.C703_Sales_Rep_Id)) Distname,Get_Distributor_Type(Get_Distributor_Id(t504a.C703_Sales_Rep_Id)) Dist_Type  ");
	    sbQuery.append(" ,T526.C526_Product_Request_Detail_Id LOANERTRANSID, Get_Code_Name (T9200.C901_Incident_Type) Incidenttype ");
	    if (strShowDtl.equals("on")) {
	    sbQuery.append(" ,T504b.C504a_Etch_Id  || '/' || T504a.C504_Consignment_Id incidentvalue ");
	    }else{
	    sbQuery.append(" ,T504a.C504_Consignment_Id incidentvalue ");
	    }
	    sbQuery.append(" ,to_char(C9201_STATUS) STFL ");
	    sbQuery.append(" ,decode(c9201_status,10,'Open',20,'Approved',40,'Cancelled','') STFLNAME ");
	    sbQuery.append(" ,C9200_INCIDENT_DATE incidentdate ");
	    sbQuery.append(" ,C9201_AMOUNT amount , c901_incident_type itype ");
	    sbQuery.append(" , t526.C525_Product_Request_Id CNID ");
	    sbQuery.append(" ,Get_Set_Name (T526.C207_Set_Id) setdesc ");
	    sbQuery.append(" ,T504a.C504a_Loaner_Dt Ldate,T504a.C504a_Expected_Return_Dt Erdate,C504a_Return_Dt Rdate ");
	    sbQuery.append(" ,Get_Work_Days_Count (TO_CHAR (TRUNC (T504a.C504a_Expected_Return_Dt),'MM/dd/yyyy'), TO_CHAR (TRUNC (NVL (T504a.C504a_Return_Dt, CURRENT_DATE)),'MM/dd/yyyy')) Elpdays ");
	    sbQuery.append(" ,Get_Rep_Name(T504a.C703_Ass_Rep_Id) ASSOCREP");
	    sbQuery.append(" ,TO_CHAR (t9200.C9201_CREDITED_DATE , '"+strCompDFmt+"') creditdate ");
	    
	    sbQuery.append(" FROM (SELECT * FROM T9200_Incident T9200 , T9201_Charge_Details T9201 ");
	    sbQuery.append(" WHERE T9200.C9200_Incident_Id = T9201.C9200_Incident_Id");
	    sbQuery.append(" AND T9200.C9200_Void_Fl      IS NULL");
	    sbQuery.append(" AND T9200.C901_Incident_Type  = '92068' "); //92068:= Loaner LateFee By set
	    sbQuery.append(" AND ( t9200.C1900_company_id = " + getCompId());
	    sbQuery.append(" OR  t9200.c1900_company_id  IN (select c1900_company_id from t5041_plant_company_mapping where C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =" +strFilter);
	    sbQuery.append(") ");
	    sbQuery.append(") ");
	    sbQuery.append(" AND T9201.C9201_Amount > 0"); // Should show only if the amount is greater than 0
	    sbQuery.append(" AND t9201.c9201_void_fl IS NULL");
	    if (!strFromDt.equals("") && !strToDt.equals("")) {
		      sbQuery.append(" AND trunc(t9200.C9200_INCIDENT_DATE) >= TO_DATE('");
		      sbQuery.append(strFromDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" ) ");
		      sbQuery.append(" AND trunc(t9200.C9200_INCIDENT_DATE) <= TO_DATE('");
		      sbQuery.append(strToDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" ) ");
		    }
	    sbQuery.append(" )T9200");
	    sbQuery.append(" ,(SELECT t526.* ");
	    sbQuery.append(" FROM T526_Product_Request_Detail T526 , ");
	    sbQuery.append(" (SELECT DISTINCT T526.C525_Product_Request_Id ");
	    sbQuery.append(" FROM T9200_Incident T9200 , ");
	    sbQuery.append(" T526_Product_Request_Detail T526 ");
	    sbQuery.append(" WHERE T9200.C9200_Ref_Id     = T526.C526_Product_Request_Detail_Id ");
	    sbQuery.append(" AND T9200.C9200_Void_Fl     IS NULL ");
	    sbQuery.append(" AND T526.C526_Void_Fl IS NULL ");
	    sbQuery.append(" AND T9200.C901_Incident_Type = '92068' "); //92068:= Loaner LateFee By set
	    sbQuery.append(" AND ( t9200.C1900_company_id = " + getCompId());
	    sbQuery.append(" OR  t9200.c1900_company_id  IN (select c1900_company_id from t5041_plant_company_mapping where C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id =" +strFilter);
	    sbQuery.append(") ");
	    sbQuery.append(") ");
	    if (!strFromDt.equals("") && !strToDt.equals("")) {
		      sbQuery.append(" AND trunc(t9200.C9200_INCIDENT_DATE) >= TO_DATE('");
		      sbQuery.append(strFromDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" ) ");
		      sbQuery.append(" AND trunc(t9200.C9200_INCIDENT_DATE) <= TO_DATE('");
		      sbQuery.append(strToDt);
		      sbQuery.append("','" + strCompDFmt + "'");
		      sbQuery.append(" ) ");
		    }
	    sbQuery.append(" )Charge ");
	    sbQuery.append(" WHERE T526.C525_Product_Request_Id = CHARGE.C525_Product_Request_Id ");
	    sbQuery.append(" AND T526.C526_Void_Fl IS NULL ");
	    sbQuery.append(" )t526, ");
//	    sbQuery.append(" ,T504a_Loaner_Transaction T504a");
	    /* Below Code is To Remove the Duplicates of Zero Ellapsed day's in Loaner Charges Screen (Loaner Late Fee (BY Set)) */
	    /*if the same Incident have more than one records with the Zero Ellapsed day then Get the MAXimum value of Loaner transaction id 
	     * and load only those value in Loaner Charges screen*/
	    sbQuery.append(" (SELECT T504b.C504a_Loaner_Transaction_Id , T504b.C526_Product_Request_Detail_Id ");
	    sbQuery.append(" ,T504b.C504a_Loaner_Dt , T504b.C504a_Expected_Return_Dt ");
	    sbQuery.append(" ,T504b.C703_Sales_Rep_Id , T504b.C504a_Void_Fl ");
	    sbQuery.append(" ,T504b.C504_Consignment_Id , T504b.C703_Ass_Rep_Id , T504b.C504A_RETURN_DT ");
	    sbQuery.append(" FROM T504a_Loaner_Transaction T504b ");
	    sbQuery.append(" WHERE T504b.C504a_Loaner_Transaction_Id = ");
					    sbQuery.append(" (Select Max(T504a.C504a_Loaner_Transaction_Id) ");
					    sbQuery.append(" FROM T504a_Loaner_Transaction T504a ");
					    sbQuery.append(" WHERE T504a.C526_Product_Request_Detail_Id = T504b.C526_Product_Request_Detail_Id ");
					    sbQuery.append(" AND T504a.C504a_Void_Fl IS NULL ");
					    sbQuery.append(" GROUP BY C526_Product_Request_Detail_Id) ");
		sbQuery.append(" AND T504b.C504a_Void_Fl IS NULL) T504a ");
		sbQuery.append(" ,T504a_Consignment_Loaner T504b");
	    if (strShowDtl.equals("on")) {
	    	sbQuery.append(" WHERE T526.C526_Product_Request_Detail_Id = T9200.C9200_Ref_Id(+)");	  
	    }
	    else{
	    	sbQuery.append(" WHERE T526.C526_Product_Request_Detail_Id = T9200.C9200_Ref_Id");	  
	    }
	    sbQuery.append(" AND T526.C526_Product_Request_Detail_Id   = T504a.C526_Product_Request_Detail_Id");
	    sbQuery.append(" AND T504a.C504_Consignment_Id = T504b.C504_Consignment_Id");
	    sbQuery.append(" AND T504a.C504a_Void_Fl IS NULL ");
	    sbQuery.append(" AND T504b.C504a_Void_Fl IS NULL ");
	    if (!strDist.equals("")) {
	      sbQuery.append(" and T9200.C701_DISTRIBUTOR_ID = '");
	      sbQuery.append(strDist);
	      sbQuery.append("' ");
	    }
	    if (!strRep.equals("")) {
	      sbQuery.append(" and T9200.C703_SALES_REP_ID =");
	      sbQuery.append(strRep);

	    }
	    if (!strStatus.equals("")) {
	      sbQuery.append(" and T9200.C9201_STATUS = ");
	      sbQuery.append(strStatus);
	    }
	    if (!strRequestId.equals("") ){ //PMT-8847 
	    	  sbQuery.append(" AND T526.C525_Product_Request_Id ='");
	          sbQuery.append(strRequestId);
	          sbQuery.append("' ");
	      }
	    sbQuery.append(" ORDER BY T504a.C703_Sales_Rep_Id , T526.C525_Product_Request_Id , C9200_Incident_Date ");
	    
	    log.debug("fetchChargesLateFeeBySetReport :: " + sbQuery.toString());
	    alMaster = gmDBManager.queryMultipleRecords(sbQuery.toString());
	    
	      return alMaster;
	  }
  
  public void saveChargesStatus(String strinputString, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_charge_status", 2);
    gmDBManager.setString(1, strinputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * loadMissingChargeDtls - This method will save the Loaner Missing Charges Details *
   * 
   * @param HashMap - hmParam
   * @return RowSetDynaClass
   * @exception AppError, Exception
   */
  public RowSetDynaClass loadMissingChargeDtls(HashMap hmParam) throws AppError, Exception {
    RowSetDynaClass rdResult = null;
    String strRequestID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DISTNAME"));
    String strRepName = GmCommonClass.parseNull((String) hmParam.get("REPNAME"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strReconCommnets = GmCommonClass.parseNull((String) hmParam.get("RECONCOMMNETS"));
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strScreenType = GmCommonClass.parseNull((String) hmParam.get("SCREENTYPE"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strDistType = GmCommonClass.parseNull((String) hmParam.get("DISTTYPE"));
    strDateFmt = strDateFmt.equals("") ? "MM/dd/yyyy" : strDateFmt;

    Date dtlockedFromDT = GmCommonClass.getStringToDate(strFromDate, strDateFmt);
    Date dtlockedToDT = GmCommonClass.getStringToDate(strToDate, strDateFmt);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_msng_chrg.gm_fch_msng_chrg_dtls", 10);
    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestID);
    gmDBManager.setString(2, strDistID);
    gmDBManager.setString(3, strRepName);
    gmDBManager.setString(4, strStatus);
    gmDBManager.setString(5, strScreenType);
    gmDBManager.setString(6, strDistType);
    gmDBManager.setDate(7, dtlockedFromDT);
    gmDBManager.setDate(8, dtlockedToDT);
    gmDBManager.setString(9, strReconCommnets);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(10));
    log.debug("The values from the database is ****** " + rdResult.getRows().size());
    gmDBManager.close();
    return rdResult;
  } // End of loadMissingChargeDtls()


  public void saveMissingChargeDtls(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    log.debug("the value inside strInputString*** in bean" + strInputString);
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_msng_chrg.gm_sav_msng_chrg_dtls", 3);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strOpt);
    gmDBManager.execute();
    gmDBManager.commit();
  }
  
  /**
   * sendLoanerLateFeeEmailJMS - This method call the jms for send mail *
   * 
   * @param HashMap - hmValue
   * @return String
   * @exception AppError
   */
  public void sendLoanerLateFeeEmailJMS(HashMap hmValue) throws AppError {
	    log.debug("the value inside hmParam*** in bean" + hmValue);
	    
	    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		HashMap hmParam = new HashMap();
		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("LOANER_LATE_FEE_EMAIL_CONSUMER_CLASS"));
		String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOANER_LATE_FEE_EMAIL_QUEUE"));
		hmParam.put("QUEUE_NAME", strQueueName);
		hmParam.put("HMVALUES", hmValue);
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		gmConsumerUtil.sendMessage(hmParam);
	}

  /**
   * processLoanerLateFeeEmail - This method will fetch and send mail to the Loaner Missing Charges Details *
   * 
   * @param HashMap - hmValue
   */
	public void processLoanerLateFeeEmail(HashMap hmValue) throws Exception {

		ArrayList alEmailData = new ArrayList();
		ArrayList alTempEmailData = new ArrayList();		
		ArrayList alSortedEmailData = new ArrayList();

		String strRepId = "";
		String strRepName = "";
		String replacedTempRepNameStr = "";
		String strTempFileName = "";
		String strTempId = "";
		String strDistName = "";
		String strAssocRepName = "";
		String strReqId = "";
		String strIncidentDt = "";
		String strSetName = "";
		String strEtchId = "";
		String strSetNum = "";
		String strPartDesc = "";
		String strMissQty = "";
		String strCharge = "";
		String strTotalCharge = "";
		String strDedDt = "";
		String strExcelFilePath = GmCommonClass.getString("LATE_FEE_EXCEL_FILEPATH");
		String strMasterFile = GmCommonClass.getString("LATE_FEE_EXCEL_MASTER_FILE");
		String strInputString = GmCommonClass.parseNull((String) hmValue.get("INPUTSTRING"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmValue.get("COMPANYINFO"));
		log.debug("strInputString========== " + strInputString);
		log.debug("strInputString length ========== " + strInputString.length());
		log.debug("hmValue=====looping===== " + hmValue);
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy_HH-mm-ss");
		String strCurrentDate = formatter.format(date);
		alEmailData = GmCommonClass.parseNullArrayList(populateLoanerLateFeeDetails(strInputString));
		log.debug("alEmailData========== " + alEmailData);
		System.out.println("alEmailData========== " + alEmailData);
		int alEmailDataSize = alEmailData.size();
		log.debug("alEmailDataSize========== " + alEmailDataSize);
		int count = 0;
		HashMap hmlateFeeVal = new HashMap();
		HashMap hmTempEmailParam = new HashMap();
		for (int i = 0; i < alEmailDataSize; i++) {
			HashMap hmResult = (HashMap) alEmailData.get(i);
			log.debug("hmResult========== " + i + " " + hmResult);
			strRepId = GmCommonClass.parseNull((String) hmResult.get("REP_ID"));
			log.debug("strRepId=====1===== " + strRepId);
			strRepName = GmCommonClass.parseNull((String) hmResult.get("REP_NAME"));
			replacedTempRepNameStr = strRepName.replace('/', '_');
			strTempFileName = strExcelFilePath + "\\LoanerLateFee_" + replacedTempRepNameStr + "_" + strCurrentDate + ".xls";
			strTempId = "";
			count++;
			log.debug("count======inside==== " + count);
			if (count < alEmailDataSize) {
				HashMap hmResultVal2 = GmCommonClass.parseNullHashMap((HashMap) alEmailData.get(i + 1));
				log.debug("count======inside==== " + hmResultVal2 + " " + hmResultVal2.size());
				strTempId = GmCommonClass.parseNull((String) hmResultVal2.get("REP_ID"));
			}
			if (alTempEmailData == null || alTempEmailData.isEmpty()) {
				alTempEmailData.add(alEmailData.get(i));
				if (strRepId.equals(strTempId)) {
					continue;
				}
			} else {
				HashMap hmResultVal3 = GmCommonClass.parseNullHashMap((HashMap) alTempEmailData.get(alTempEmailData.size() - 1));
				String strTempIds = GmCommonClass.parseNull((String) hmResultVal3.get("REP_ID"));
				if (strRepId.equals(strTempIds)) {
					alTempEmailData.add(alEmailData.get(i));
					if (strRepId.equals(strTempId)) {
						continue;
					}
				}
			}

			if (alTempEmailData != null && !alTempEmailData.isEmpty()) {
				log.debug("inside else ");
				GmExcelManipulation.copyFile(strMasterFile, strTempFileName);
				generateLateFeeExcelData(strTempFileName);
				generateLateFeeExcelAlData(getSortedAlTempEmailData(alTempEmailData), strTempFileName);
				hmTempEmailParam.put("REPID", strRepId);
				hmTempEmailParam.put("EXCELFILENAME", strTempFileName);
				hmTempEmailParam.put("ALRESULT", alTempEmailData);
				sendLateFeeEmail(hmTempEmailParam);
				log.debug("alTempEmailData=====2nd===== " + alTempEmailData);
				log.debug("hmTempEmailParam=====2nd===== " + hmTempEmailParam);
				alTempEmailData.clear();
				strRepId = "";
				strRepName = "";
				strTempFileName = "";
			}

		}

	}
  
  /**
   * populateLoanerLateFeeDetails - This method fetch the loner details to send mail *
   * 
   * @param String - hmVastrInputStringlue
   * @return String
   * @exception AppError
   */
  public ArrayList populateLoanerLateFeeDetails(String  strInputString) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
	  ArrayList alResult = new ArrayList();
	  gmDBManager.setPrepareString("gm_pkg_op_msng_chrg.gm_fch_late_fee_details ", 2);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.setString(1, strInputString);
      gmDBManager.execute();
      alResult =  GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2)));
      gmDBManager.close();
      return alResult;
  }
  
  public void sendLateFeeEmail(HashMap hmParam) throws Exception {  
	  GmEmailProperties emailProps = new GmEmailProperties();
      GmJasperMail jasperMail = new GmJasperMail();
      String TEMPLATE_NAME = "GmLoanerLateFeeEmail";
      
      String strFrom = "";
      String strTo = "";
      String strCC = "";
      String strMimeType = "";
      String strSubject = "";
      String strMsgFooter = "";
      String strEmailHeader = "";
      
      HashMap hmReturnDetails = new HashMap(); 
      
	  String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
	  ArrayList alDataList = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT"));
	  String strExcelFileName = GmCommonClass.parseNull((String) hmParam.get("EXCELFILENAME"));
	  
	  String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
	  GmResourceBundleBean gmResourceBundleBean =
    		  GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
	  
	  strFrom = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM);
	  strEmailHeader = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.EMAIL_HEADER_NM);
	  strMimeType = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
	  strMsgFooter = gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_FOOTER);
	  strSubject = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT));
	  strCC = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.CC));
	  
	  String strToEmail = fetchLateFeeEmailDtls(strRepId);
	  
	  emailProps.setMimeType(strMimeType);
      emailProps.setSender(strFrom);
      emailProps.setRecipients(strToEmail);
      emailProps.setCc(strCC);
      emailProps.setSubject(strSubject);
      emailProps.setAttachment(strExcelFileName);

      jasperMail.setJasperReportName("/GmLoanerLateEmail.jasper");
      jasperMail.setAdditionalParams(hmParam);
      jasperMail.setReportData(null);
      jasperMail.setEmailProperties(emailProps);
      hmReturnDetails = jasperMail.sendMail();

  }
  
  /**
   * fetchLateFeeEmailDtls - This method fetch the loner details to send mail *
   * 
   * @param String - strRepId
   * @return String
   * @exception AppError
   */
  public String fetchLateFeeEmailDtls(String  strRepId) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
	  String strToMailId = "";
		 
	  gmDBManager.setPrepareString("gm_pkg_op_msng_chrg.gm_fch_late_fee_email", 2);
	  gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
	  gmDBManager.setString(1, strRepId);
	  gmDBManager.execute();
	  strToMailId = GmCommonClass.parseNull(gmDBManager.getString(2));
	  gmDBManager.close();
	  return strToMailId;
  }
  
	public void generateLateFeeExcelData(String strExcelFileName) throws Exception {

		//String strExcelFileName = GmCommonClass.parseNull((String) hmParam.get("EXCELFILENAME"));
		//ArrayList alList = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT"));

		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Distributor");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Rep Name");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Assoc Rep");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(3);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Request #");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(4);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Incident Date");
		
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(5);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Set Name");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(6);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Etch ID");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(7);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Part/Set#");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(8);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Part Description");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(9);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Missing Qty");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(10);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Charge Per Part");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(11);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Total Charge");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(0);
		gmExcelManipulation.setColumnNumber(12);
		gmExcelManipulation.setFontBold(true);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel("Deduction Date");
		
	}
	
	public void generateLateFeeExcelAlData(
			String strDistName,String strRepName,String strAssocRepName,String strReqId,
			String strIncidentDt,String strSetName,String strEtchId,String strSetNum,
			String strPartDesc,String strMissQty,String strCharge,String strTotalCharge,String strDedDt,String strExcelFileName) 
	
		throws Exception {
		log.debug("strDistName"+strDistName);
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.writeToExcel(strDistName);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(1);
		gmExcelManipulation.writeToExcel(strRepName);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(2);
		gmExcelManipulation.writeToExcel(strAssocRepName);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(3);
		gmExcelManipulation.writeToExcel(strReqId);
      
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(4);			
		gmExcelManipulation.writeToExcel("");
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(5);
		gmExcelManipulation.writeToExcel(strSetName);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(6);
		gmExcelManipulation.writeToExcel(strEtchId);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(7);
		gmExcelManipulation.writeToExcel(strSetNum);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(8);
		gmExcelManipulation.writeToExcel(strPartDesc);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(9);
		gmExcelManipulation.writeToExcel(strMissQty);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(10);
		gmExcelManipulation.writeToExcel(strCharge);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(11);
		gmExcelManipulation.writeToExcel(strTotalCharge);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(12);
		gmExcelManipulation.writeToExcel(strDedDt);
	}
	public void generateLateFeeExcelAlData(
			ArrayList alLateFeeList,String strExcelFileName) 
		throws Exception {
		log.debug("alLateFeeList======== "+alLateFeeList);
		log.debug("alLateFeeList=size======= "+alLateFeeList.size());
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
		gmExcelManipulation.setExcelFileName(strExcelFileName);
		
		gmExcelManipulation.setSheetNumber(0);
		gmExcelManipulation.setRowNumber(1);
		gmExcelManipulation.setColumnNumber(0);
		gmExcelManipulation.setLeftBorder(true);
		gmExcelManipulation.setRightBorder(true);
		gmExcelManipulation.setTopBorder(true);
		gmExcelManipulation.setBottomBorder(true);
		gmExcelManipulation.setBorderStyle(gmExcelManipulation.BORDER_THIN);
		gmExcelManipulation.writeToExcel(alLateFeeList);
	}
	
	 /**
	   * fetchLateFeeEmailDtls - This method sort the selected email data  *
	   * 
	   * @param ArrayList - alTempEmailData
	   * @return ArrayList
	   */
	private ArrayList getSortedAlTempEmailData(ArrayList alTempEmailData) {
		
		ArrayList sortedAlTepmEmailData = new ArrayList();
		for (int i = 0; i < alTempEmailData.size(); i++) {
			HashMap hmResultVal = (HashMap) alTempEmailData.get(i);
			LinkedHashMap hmlateFeeVal = new LinkedHashMap();
			String strDistName = GmCommonClass.parseNull((String) hmResultVal.get("DIST_NAME"));
			String strRepName = GmCommonClass.parseNull((String) hmResultVal.get("REP_NAME"));
			String strAssocRepName = GmCommonClass.parseNull((String) hmResultVal.get("ASSOCREPNM"));
			String strReqId = GmCommonClass.parseNull((String) hmResultVal.get("REQ_ID"));
			String strIncidentDate = GmCommonClass.parseNull((String) hmResultVal.get("INCIDENT_DATE"));
			String strSetName = GmCommonClass.parseNull((String) hmResultVal.get("SET_NM"));
			String strEtchId = GmCommonClass.parseNull((String) hmResultVal.get("ETCH_ID"));
			String strPartSet = GmCommonClass.parseNull((String) hmResultVal.get("PNUM"));
			String strPartDesc = GmCommonClass.parseNull((String) hmResultVal.get("PDESC"));
			String strQtyMissing = GmCommonClass.parseNull((String) hmResultVal.get("QTY_MISSING"));
			String strChargePerPart = GmCommonClass.parseNull((String) hmResultVal.get("IH_CHARGE"));
			String strTotalCost = GmCommonClass.parseNull((String) hmResultVal.get("CHARGE"));
			String strDeductionDate = GmCommonClass.parseNull((String) hmResultVal.get("DED_DATE"));
			hmlateFeeVal.put("DIST_NAME", strDistName);
			hmlateFeeVal.put("REP_NAME", strRepName);
			hmlateFeeVal.put("ASSOCREPNM", strAssocRepName);
			hmlateFeeVal.put("REQ_ID", strReqId);
			hmlateFeeVal.put("INCIDENT_DATE", strIncidentDate);
			hmlateFeeVal.put("SET_NM", strSetName);
			hmlateFeeVal.put("ETCH_ID", strEtchId);
			hmlateFeeVal.put("PNUM", strPartSet);
			hmlateFeeVal.put("PDESC", strPartDesc);
			hmlateFeeVal.put("QTY_MISSING", strQtyMissing);
			hmlateFeeVal.put("IH_CHARGE", strChargePerPart);
			hmlateFeeVal.put("CHARGE", strTotalCost);
			hmlateFeeVal.put("DED_DATE", strDeductionDate);
			sortedAlTepmEmailData.add(hmlateFeeVal);
		}		
		return sortedAlTepmEmailData;
	}
}
