package com.globus.operations.logistics.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.prodmgmnt.beans.GmPartNumSearchBean;
import com.globus.sales.beans.GmSalesConsignReportBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmInHouseSetsReportBean extends GmSalesConsignReportBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  GmCommonClass gmCommon = new GmCommonClass();

  // log4j
  public GmInHouseSetsReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmInHouseSetsReportBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }


  public HashMap getConsignDetails(String strConsignId) throws AppError {
    HashMap hmReturn = new HashMap();
    ResultSet rs = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_OP_FCH_CONSIGNDETAILS", 2);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.setString(1, strConsignId);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    hmReturn = gmDBManager.returnHashMap(rs);
    gmDBManager.close();
    return hmReturn;
  }
  
 /**
 * getSetConsignDetails - This method used to fetch the set consignment details.
 * This method/DB File used in shipping Device (erpshipping repository).
 * Please make sure the changes are done in that repository too if param added/removed for the DB call.	
 * @param strConsignId
 * @param strType
 * @return
 * @throws AppError
 */
  public ArrayList getSetConsignDetails(String strConsignId, String strType) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;

    gmDBManager.setPrepareString("GM_LS_FCH_SETCSGDETAILS", 3);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strType);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(3);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    return alReturn;

  }

  /**
   * getInHouseSetStatus - This method lists status of all InHouse Sets
   * 
   * @param String
   * @param String
   * @param String
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getInHouseSetStatus(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strCategories = GmCommonClass.parseNull((String) hmParam.get("CATEGORY"));
    String strSetIds = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    log.debug("bean strstatus " + strStatus);
    String strPriorityStr = GmCommonClass.parseNull((String) hmParam.get("PRIORITYSTR"));
    String strEtchId = GmCommonClass.parseNull((String) hmParam.get("ETCHID"));
    String strConId = GmCommonClass.parseNull((String) hmParam.get("CONID"));
    String strOverdue = GmCommonClass.parseNull((String) hmParam.get("OVERDUE"));
    String strHoldTxn = GmCommonClass.parseNull((String) hmParam.get("HOLDTXN"));
    String strSetName = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strRequestID = GmCommonClass.parseNull((String) hmParam.get("REQID"));
    String strDueDays = GmCommonClass.parseNull((String) hmParam.get("DUEDAYS"));
    String strDistIds = GmCommonClass.parseNull((String) hmParam.get("DISTIDS"));
    String strScreenType = GmCommonClass.parseNull((String) hmParam.get("SCREENTYPE"));

    String strLoanerFlag =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "MANAGELOANERFILTER"));/*
                                                                                                * MANAGELOANERFILTER
                                                                                                * -
                                                                                                * To
                                                                                                * fetch
                                                                                                * the
                                                                                                * records
                                                                                                * for
                                                                                                * EDC
                                                                                                * entity
                                                                                                * countries
                                                                                                * based
                                                                                                * on
                                                                                                * plant
                                                                                                */
    if (strScreenType.equals("LoanerStRpt")) {
      strLoanerFlag =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_OVERALLBY_ST"));// LOANER_OVERALLBY_ST-Loaner
                                                                                                  // ("F"
                                                                                                  // icon
                                                                                                  // in
                                                                                                  // overallbystatus
                                                                                                  // screen
                                                                                                  // fetching
                                                                                                  // based
                                                                                                  // on
                                                                                                  // plant)
    }
    String strLoanerFilter = strLoanerFlag.equals("") ? getCompId() : getCompPlantId();
    String strSalesRepId =  GmCommonClass.parseNull((String) hmParam.get("SALESREPID")); // added for PC-884

  //PC-4148 Remove Try Catch for Email Exception (TSK-23803)

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT t504.c504_consignment_id CONID ,t504.c207_set_id SETID ,get_set_name(t504.c207_set_id) SNAME, t504.C504_HOLD_FL holdfl ");
      sbQuery
          .append(" ,t504.c504_status_fl SFL ,get_code_name(t504.c504_inhouse_purpose) PNAME ,t504.c504_inhouse_purpose PID ");
      sbQuery.append(" ,t504a.c504a_etch_id ETCHID, t504a.c504a_status_fl LSFL, ");
      sbQuery.append(" gm_pkg_op_loaner.get_loaner_status(T504A.C504A_STATUS_FL)  LOANSFL ");
      sbQuery.append(" ,t504a.c504a_expected_return_dt EDATE, t504a.c504a_loaner_dt LDATE ");
      sbQuery.append(" ,get_cs_rep_loaner_bill_nm_add(t504.c504_consignment_id) LOANNM ");
      sbQuery
          .append(" ,decode(t504a.c504a_status_fl,20,TRUNC(CURRENT_DATE) - t504a.c504a_loaner_dt) ELPDAYS ,get_work_days_count(to_char(TRUNC(t504a.c504a_expected_return_dt),get_rule_value('DATEFMT','DATEFORMAT')) ,to_char(TRUNC(CURRENT_DATE),get_rule_value('DATEFMT','DATEFORMAT'))) OVERDUE");
      sbQuery.append(" ,GET_LOG_FLAG(t504a.c504_consignment_id,1222) CMTCNT");
      sbQuery.append(" ,GET_LN_REQ_DET_ID(t504a.c504_consignment_id) REQID");
      sbQuery
          .append(" ,GET_LOG_FLAG(GET_LN_REQ_DET_ID( t504a.c504_consignment_id) ,4000316) RECONCMTCNT");
      sbQuery.append(" , get_code_name(T504A.C504A_LOANER_PRIORITY) PRIORITY");
      sbQuery.append(" , t504.c704_account_id ACCID ");
      sbQuery.append(" FROM t504_consignment t504, t504a_consignment_loaner t504a, ");
      if (!strSalesRepId.equals("")) {
    	  sbQuery.append(" t703_sales_rep t703, ");
        }
      sbQuery
          .append(" (select t504b.c1900_company_id,t504b.c504_consignment_id, t504b.c703_sales_rep_id from t504a_loaner_transaction t504b where UPPER (t504b.c504_consignment_id) LIKE UPPER ('%"
              + strConId + "%')   AND t504b.c504a_return_dt    IS  NULL ");
      sbQuery.append(" AND t504b.c504a_void_fl      IS NULL  ) t504b ");
      // For Pending Return Status only we are going to check the T504a_loaner_transaction COmpnay
      // id

      if (!strSetName.equals("") && strRequestID.equals("")) {
        sbQuery.append(", t207_set_master t207");
      }

      sbQuery.append(" WHERE t504.c504_status_fl = '4' AND t504.c504_verify_fl = '1' ");
      sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id(+) ");
      sbQuery.append(" AND (t504.C5040_PLANT_ID     = " + strLoanerFilter
          + "  OR nvl(t504b.c1900_company_id , t504.c1900_company_id)   = " + strLoanerFilter
          + ") ");
      sbQuery.append(" AND T504.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND t504.c207_set_id IS NOT NULL ");
      // For Pending Return Status only we are going to check the T504a_loaner_transaction COmpnay
      // id

      sbQuery.append(" AND T504.C5040_PLANT_ID = T504A.C5040_PLANT_ID ");
      if (!strType.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t504.c504_type = ");
        sbQuery.append(strType);
      }

      if (!strCategories.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t504.c504_inhouse_purpose IN (");
        sbQuery.append(strCategories);
        sbQuery.append(")");
      }
      if (!strSetIds.equals("")) {
        strSetIds = strSetIds.replaceAll(",", "','");
        sbQuery.append(" AND ");
        sbQuery.append(" t504.c207_set_id IN ('");
        sbQuery.append(strSetIds);
        sbQuery.append("')");
      }
      if (!strPriorityStr.equals("")) {
        strPriorityStr = strPriorityStr.replaceAll(",", "','");
        sbQuery.append(" AND ");
        sbQuery.append(" t504a.c504a_loaner_priority IN ('");
        sbQuery.append(strPriorityStr);
        sbQuery.append("')");
      }

      if (!strSetName.equals("") && strRequestID.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t207.c207_set_id = t504.c207_set_id");
        sbQuery.append(" AND UPPER(t207.c207_set_nm) LIKE UPPER('%");
        sbQuery.append(strSetName);
        sbQuery.append("%')");
      }
      if (!strRequestID.equals("")) {
        sbQuery.append(" AND T504a.C504_Consignment_Id   IN    (");
        sbQuery.append("Select T504a.C504_Consignment_Id");
        sbQuery.append(" From T504a_Loaner_Transaction T504a, T526_Product_Request_Detail T526");
        sbQuery
            .append(" Where T526.C526_Product_Request_Detail_Id = T504a.C526_Product_Request_Detail_Id");
        sbQuery.append(" And T526.C525_Product_Request_Id = ");
        sbQuery.append("'");
        sbQuery.append(strRequestID);
        sbQuery.append("'");
        sbQuery.append(" And T526.C526_Void_Fl Is Null");
        sbQuery.append(" And T504a.C504a_Void_Fl Is Null  ) ");

      }
      if (!strStatus.equals("")) {
        strStatus = strStatus.replaceAll(",", "','");
        sbQuery.append(" AND ");
        sbQuery.append(" t504a.c504a_status_fl IN ('");
        sbQuery.append(strStatus);
        sbQuery.append("')");
      }
      if (strStatus.equals("") && strConId.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t504a.c504a_status_fl not IN ('60')");
      }
      if (!strEtchId.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" UPPER(t504a.c504a_etch_id) LIKE UPPER('%");
        sbQuery.append(strEtchId);
        sbQuery.append("%')");
      }
      if (!strConId.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" UPPER(t504.c504_consignment_id) LIKE UPPER('%");
        sbQuery.append(strConId);
        sbQuery.append("%')");
      }
      if (!strOverdue.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t504a.c504a_expected_return_dt < TRUNC(CURRENT_DATE)");
      }
      if (!strHoldTxn.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t504.c504_HOLD_FL IS NOT NULL");
      }
      if (!strDueDays.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t504a.c504a_expected_return_dt = TRUNC(CURRENT_DATE +");
        sbQuery.append(strDueDays);
        sbQuery.append(")");
      }

      if (!strDistIds.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(" t504.C701_DISTRIBUTOR_ID IN (");
        sbQuery.append(strDistIds);
        sbQuery.append(")");
      }

      sbQuery.append(" AND t504.c504_consignment_id = t504a.c504_consignment_id  ");
      
      // PC-884 - To add Sales Rep filter in Manage Loaner
      if (!strSalesRepId.equals("")) {
          sbQuery.append(" AND ");
          sbQuery.append(" t504b.c703_sales_rep_id IN (");
          sbQuery.append(strSalesRepId);
          sbQuery.append(")");
          sbQuery.append(" AND t703.c703_sales_rep_id = t504b.c703_sales_rep_id ");
          sbQuery.append(" AND t703.c703_void_fl IS NULL ");
        }
      sbQuery.append(" ORDER BY setid, t504.c504_created_date, t504.c504_inhouse_purpose DESC ");

      log.debug(" Query for getInHouseSetStatus is " + sbQuery.toString());

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of getInHouseSetStatus

  /**
   * reportCrossTabSet - This Method is used to fetch the inhouse loaner set list and display the
   * same on the screem
   * 
   * 
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportCrossTabSetDetails(String strType) throws AppError {
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    HashMap hmapFinalValue; // Final value report details
    GmCrossTabReport gmCrossReport = new GmCrossTabReport();

    // To fetch header information
    sbHeaderQuery.append(" SELECT C901_CODE_NM  FROM T901_CODE_LOOKUP ");
    sbHeaderQuery.append(" WHERE C901_CODE_GRP = 'INPRP' ");
    sbHeaderQuery.append(" AND C901_ACTIVE_FL = 1 ");

    // To fetch the transaction information
    sbDetailQuery.append(" SELECT T504.C207_SET_ID ID ");
    sbDetailQuery.append(" , GET_SET_NAME(T504.C207_SET_ID ) NAME  ");
    sbDetailQuery.append(" ,GET_CODE_NAME(T504.C504_INHOUSE_PURPOSE) P_TYPE  ");
    sbDetailQuery.append(" , COUNT(1)  COUNT  ");
    sbDetailQuery.append(" FROM T504_CONSIGNMENT  T504 ");
    sbDetailQuery.append(" , T504A_CONSIGNMENT_LOANER T504A ");
    sbDetailQuery.append(" WHERE T504.C504_TYPE = 4119 ");
    sbDetailQuery.append(" AND   T504.C504_CONSIGNMENT_ID = T504A.C504_CONSIGNMENT_ID ");
    sbDetailQuery.append(" AND   T504.C207_SET_ID  IS NOT NULL ");
    sbDetailQuery.append(" AND   T504.C504_VOID_FL IS NULL ");
    sbDetailQuery.append(" AND   T504.c1900_company_id='" + getCompId() + "'");
    // Available
    if (strType.equals("1")) {
      sbDetailQuery.append(" AND T504A.C504A_STATUS_FL = 0 "); // Available
    }// Shipped
    else if (strType.equals("2")) {
      sbDetailQuery.append(" AND T504A.C504A_STATUS_FL = 20 "); // Pending Return
    }
    sbDetailQuery.append(" GROUP BY C207_SET_ID, C504_INHOUSE_PURPOSE ");
    sbDetailQuery.append(" ORDER BY NAME ");

    hmapFinalValue =
        gmCrossReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());
    // hmapFinalValue.put("FromDate",hmapFromToDate);
    return hmapFinalValue;

  }

  /**
   * loadInHouseSetShipLists - This method will
   * 
   * @param String strConsignId
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadInHouseSetShipLists(String strConsignId) throws AppError {
    GmCommonClass gmCommon = new GmCommonClass();
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    ArrayList alResult = new ArrayList();
    try {
      alResult = gmCommon.getCodeList("LONTO");
      hmReturn.put("LOANTO", alResult);

      alResult = gmCommon.getCodeList("CONSP");
      hmReturn.put("SHIPTO", alResult);

      alResult = gmCust.getDistributorList("Active");
      hmReturn.put("DISTRIBUTORLIST", alResult);

      alResult = gmCust.getRepList("Active");
      hmReturn.put("REPLIST", alResult);

      alResult = gmSales.reportAccount();
      hmReturn.put("ACCLIST", alResult);

      alResult = gmLogon.getEmployeeList();
      hmReturn.put("EMPLIST", alResult);

      alResult = gmCommon.getCodeList("DCAR", getGmDataStoreVO());
      hmReturn.put("DELCARR", alResult);

      alResult = gmCommon.getCodeList("DMODE", getGmDataStoreVO());
      hmReturn.put("DELMODE", alResult);

    } catch (Exception e) {
      GmLogError.log("Exception in GmInHouseSetsReportBean:loadInHouseSetShipLists",
          "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadInHouseSetShipLists

  /**
   * loadSavedSetMaster - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList loadSavedSetMaster(String strConsignId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;

    gmDBManager.setPrepareString("GM_LS_FCH_LOADSETMASTER", 2);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    return alReturn;

  } // End of loadSavedSetMaster

  /**
   * viewInHouseTransDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap viewInHouseTransDetails(String strConsignId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmResult = new HashMap();
    ResultSet rs = null;

    gmDBManager.setPrepareString("GM_LS_FCH_LOADIHDETAILS", 2);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    hmResult = GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap(rs));
    gmDBManager.close();
    return hmResult;

  } // End of viewInHouseTransDetails

  /**
   * loadCartForConsign - This method will use to view the part details for consignment Added
   * get_partnumber_qty to get available qty from inventory
   * 
   * @param String strConsignId
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   * @see AppError
   */
  public HashMap loadCartForConsign(String strConsignId, String strPartNum) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    StringTokenizer strTok = new StringTokenizer(strPartNum, ",");
    log.debug(" Consignment ID " + strConsignId);
    String strCompanyID = getCompId();
    String strPartCompFiltr = "";
    GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      while (strTok.hasMoreTokens()) {
        strPartNum = strTok.nextToken();
        StringBuffer sbQuery = new StringBuffer();
        strPartCompFiltr = gmPartSearchBean.getPartCompanyFilter(strPartNum, "T205");
        sbQuery.append(" SELECT    T205.C205_PART_NUMBER_ID ID, T2052.C2052_EQUITY_PRICE PRICE, ");
        sbQuery
            .append(" T205.C205_PART_NUM_DESC PDESC , GET_QTY_IN_STOCK(T205.C205_PART_NUMBER_ID) STOCK,");
        sbQuery.append(" GET_COUNT_PARTID_CONSIGN('");
        sbQuery.append(strConsignId);
        sbQuery.append("',T205.C205_PART_NUMBER_ID) SETQTY, ");
        sbQuery
            .append(" get_partnumber_qty(T205.C205_PART_NUMBER_ID,90813) QSTOCK , get_partnumber_qty(T205.C205_PART_NUMBER_ID,90814) BSTOCK, GET_PARTNUMBER_FA_WIP_QTY(T205.C205_PART_NUMBER_ID) WIPQTY, ");
        sbQuery.append(" GET_MISSING_QTY_PARTID_CONSIGN('");
        sbQuery.append(strConsignId);
        sbQuery.append("',T205.C205_PART_NUMBER_ID) MISSINGQTY ");
        sbQuery.append(" FROM T205_PART_NUMBER T205 ,T2052_PART_PRICE_MAPPING T2052 ");
        sbQuery.append(" WHERE T205.C205_PART_NUMBER_ID = '");
        sbQuery.append(strPartNum);
        sbQuery.append("'");
        sbQuery.append(strPartCompFiltr);
        sbQuery.append(" AND T205.C205_PART_NUMBER_ID  = T2052.C205_PART_NUMBER_ID(+) ");
        sbQuery.append(" AND T2052.C1900_COMPANY_ID(+) = '");
        sbQuery.append(strCompanyID);
        sbQuery.append("'");
        sbQuery.append(" AND t2052.c2052_void_fl(+) is null ");
        log.debug(" Query for loading cart for consign = " + sbQuery.toString());
        hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
        hmResult.put(strPartNum, hmReturn);
        log.debug(" hmResult is " + hmResult);
      }

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogisticsBean:loadCartForConsign", "Exception is:" + e);
    }
    return hmResult;

  } // End of loadCartForConsign

  /**
   * viewLoanerRefillHistory - This method will
   * 
   * @param String
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList viewLoanerRefillHistory(String strRefId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;

    gmDBManager.setPrepareString("GM_LS_FCH_LOANER_REFILL_HIST", 2);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRefId);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    return alReturn;
  } // End of viewLoanerRefillHistory

  /**
   * viewLoanerUsageHistory - This Method is used fetch data for viewing Usage History
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public RowSetDynaClass viewLoanerUsageHistory(HashMap hmParam) throws AppError {
    // ArrayList alReturn = new ArrayList();
    RowSetDynaClass rsLoanerUsageHistory = null;
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
    String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTH"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
    String strTurnsMonth = GmCommonClass.parseZero((String) hmParam.get("TURNSMONTH"));
    strAccessFilter = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
    String strLNType = GmCommonClass.parseNull((String) hmParam.get("LNTYPE"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    String strFromMonth = "";
    String strToMonth = "";
    strFromMonth = strToMonth = strMonth;
    StringBuffer sbQuery = new StringBuffer();

    if (!strTurnsMonth.equals("0")) {
      HashMap hmMonth = new HashMap();
      hmMonth = GmCommonClass.getMonthDiff(Integer.parseInt(strTurnsMonth), true);
      strFromMonth =
          GmCalenderOperations.getMonForMM((String) hmMonth.get("FromMonth"))
              + hmMonth.get("FromYear");
      strToMonth =
          GmCalenderOperations.getMonForMM((String) hmMonth.get("ToMonth")) + hmMonth.get("ToYear");
    }
    log.debug(" From month " + strFromMonth + " to month " + strToMonth + " strAccessFilter "
        + strAccessFilter);
    // DBConnectionWrapper dbCon = new DBConnectionWrapper();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery
        .append("SELECT to_number(c504a_loaner_transaction_id) ltxnid, ");
    sbQuery.append(" DECODE(c901_consigned_to, 50170, get_distributor_name(c504a_consigned_to_id), NULL) distnm,");
    sbQuery.append(" DECODE(c901_consigned_to, 50172, get_user_name(c504a_consigned_to_id), NULL) empnm ");
    sbQuery.append(", t504a.c504a_loaner_dt ldate");
    sbQuery.append(", t504a.c504a_expected_return_dt erdate");
    sbQuery
        .append(", c504a_return_dt rdate, trunc(c504a_return_dt) - trunc(t504a.c504a_loaner_dt) usagedays");
    sbQuery
        .append(", get_code_name (c901_consigned_to) loanto, get_account_name (t504a.c704_account_id) aname");
    sbQuery
        .append(", get_rep_name (t504a.c703_sales_rep_id) rname, nvl(c504a_is_loaner_extended,' ') isext, c504a_is_replenished isrep");
    sbQuery.append(", to_number(c504a_parent_loaner_txn_id) partid ");
    sbQuery.append(", get_rep_name (t504a.c703_ass_rep_id) assocrepnm ");
    if (!strAction.equals("ReloadUsage")) {
      sbQuery.append(", t504b.C504A_ETCH_ID ETCHID, t504.C504_CONSIGNMENT_ID CONID ");
      sbQuery.append(", t504.C207_SET_ID || ' / ' || get_set_name(t504.C207_SET_ID) SNAME ");
    }

    sbQuery.append(" FROM t504a_loaner_transaction t504a ");
    if (!strAction.equals("ReloadUsage")) {
      sbQuery
          .append(", T504_CONSIGNMENT t504, T504A_CONSIGNMENT_LOANER t504b ,T526_PRODUCT_REQUEST_DETAIL t526 ");
      sbQuery.append(getAccessFilterClause());
      sbQuery.append(" WHERE t504a.c504a_void_fl IS NULL ");
      sbQuery.append(" AND t504a.C504_CONSIGNMENT_ID = t504.C504_CONSIGNMENT_ID ");
      sbQuery.append(" AND t504b.C504_CONSIGNMENT_ID = t504.C504_CONSIGNMENT_ID ");
      sbQuery.append(" AND t504a.C504A_CONSIGNED_TO_ID = V700.D_ID ");
      sbQuery.append(strLNType.equals("4119") ? "(+) " : " "); // in-house loaner does not have dist
                                                               // id
      sbQuery
          .append(" AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id ");
      sbQuery.append(" AND t526.c526_status_fl = 30 ");
      sbQuery.append(" AND t526.c526_void_fl IS NULL ");

    } else {
      sbQuery.append(" WHERE t504a.c504a_void_fl IS NULL ");
    }

    if (!strConsignId.equals("")) {
      sbQuery.append(" AND c504_consignment_id = '");
      sbQuery.append(strConsignId);
      sbQuery.append("' ");
    }

    if (!strDistId.equals("") && !strFromMonth.equals("")) {
      strMonth = strMonth.replaceAll("~", "");
      sbQuery.append(" AND c504a_consigned_to_id = ");
      sbQuery.append(strDistId);
      sbQuery.append(" AND trunc(t504a.c504a_loaner_dt) > ADD_MONTHS(LAST_DAY(to_date('");
      sbQuery.append(strFromMonth);
      sbQuery.append("','MonYY')),-1)");
      sbQuery.append("AND trunc(t504a.c504a_loaner_dt) <= LAST_DAY(to_date('");
      sbQuery.append(strToMonth);
      sbQuery.append("','MonYY')) ");
    }

    if (!strSetId.equals("") && !strFromMonth.equals("")) {
      sbQuery.append(" AND T504.C207_SET_ID IN ('");
      sbQuery.append(strSetId);
      sbQuery.append("')  AND trunc(t504a.c504a_loaner_dt) > ADD_MONTHS(LAST_DAY(to_date('");
      sbQuery.append(strFromMonth);
      sbQuery.append("','MonYY')),-1)");
      sbQuery.append("AND trunc(t504a.c504a_loaner_dt) <= LAST_DAY(to_date('");
      sbQuery.append(strToMonth);
      sbQuery.append("','MonYY')) ");
    }

    if (strLNType.equals("4127")) {
      sbQuery.append(" AND C901_CONSIGNED_TO = 50170  ");
      sbQuery.append(" AND T504.C504_TYPE = 4127 ");
    }
    sbQuery.append(" ORDER BY to_number(c504a_loaner_transaction_id) desc");

    log.debug("*** sbQuery = " + sbQuery.toString());
    // alReturn = dbCon.queryMultipleRecords(sbQuery.toString());
    rsLoanerUsageHistory = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    log.debug("*** Executed query");

    return rsLoanerUsageHistory;
  }// End of viewLoanerUsageHistory

  /**
   * getXmlGridData Xml content generation for In-House Loaner Report &Report - Loaner Requests grid
   * 
   * @author ksomanathan
   * @param ArrayList and HashMap
   * @return String
   * @exception AppError
   */

  public String getXmlGridData(ArrayList alValues, HashMap hmParam) {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alResult", alValues);
    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest", strSessLocale));
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName("GmLoanerRequests.vm");
    return templateUtil.generateOutput();
  }

  /**
   * reportLoanerOpenRequests - This Method is used fetch data for viewing Pending Requests for
   * Product Loaners/InHouse Loaners
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList reportLoanerOpenRequests(HashMap hmParam) throws AppError {
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strSetName = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strDtType = GmCommonClass.parseNull((String) hmParam.get("DTTYPE"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPrdReqId = GmCommonClass.parseNull((String) hmParam.get("PRDREQID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strEventId = GmCommonClass.parseNull((String) hmParam.get("EVENTID"));
    String strEmpId = GmCommonClass.parseNull((String) hmParam.get("EMPID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParam.get("TXNTYPE"));
    String strScreenType = GmCommonClass.parseNull((String) hmParam.get("SCREENTYPE"));
    String strInputParam = GmCommonClass.parseNull((String) hmParam.get("INPUTPARAM"));
    String strHInputParam = GmCommonClass.parseNull((String) hmParam.get("HINPUTPARAM"));
    String strStatusParam = GmCommonClass.parseNull((String) hmParam.get("TSTATUSPARAM"));
    String strSelectedStatus = GmCommonClass.parseNull((String) hmParam.get("SELECTEDSTATUS"));
    String strHSelectedStatus = GmCommonClass.parseNull((String) hmParam.get("HSELECTEDSTATUS"));
    String strApplDateFmt = getCompDateFmt();
    String strTimeDateFmt = strApplDateFmt + " HH:MI AM";
    String strFilter = "";

    strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_PROCESS_REQ"));// Rule
    if (strScreenType.equals("LoanerStRpt")) {
      strFilter =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_OVERALLBY_ST"));// LOANER_OVERALLBY_ST-Loaner
                                                                                                  // ("F"
                                                                                                  // icon
                                                                                                  // in
                                                                                                  // overallbystatus
                                                                                                  // screen
                                                                                                  // fetching
                                                                                                  // based
                                                                                                  // on
                                                                                                  // plant)
    } // LOANER_PROCESS_REQ:=plant
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    StringBuffer sbQuery = new StringBuffer();
    StringBuffer sbMergeQuery = new StringBuffer();

    String strType = "4127";
    strType = (strTxnType.equals("InhouseLoanerRequests")) ? "4119" : strType;
    String strPartNum = "";

    strDistId = (strDistId.equals("0")) ? "" : strDistId;
    strDtType = (strDtType.equals("0")) ? "" : strDtType;
    strStatus = (strStatus.equals("0")) ? "" : strStatus;
    strEventId = (strEventId.equals("0")) ? "" : strEventId;
    strEmpId = (strEmpId.equals("0")) ? "" : strEmpId;
    String strSubdtType = "";
    ArrayList alResult;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" SELECT extdata.*,T7103.C7103_Name EVTNM, get_user_name (T7104.C7104_Created_By) EMPNM ");
      sbQuery.append(" , NVL(TO_CHAR (t7104.C7104_Created_date,'" + strTimeDateFmt
          + "'),extdata.cdate) requested_date");
      sbQuery.append(" , TO_CHAR (T7103.C7103_Start_Date, '" + strApplDateFmt + "') STRTDATE ");
      sbQuery.append(" , TO_CHAR (T7103.C7103_End_Date, '" + strApplDateFmt + "') ENDDATE ");
      sbQuery.append(" FROM ( ");
    }
    sbQuery
        .append(" SELECT t526.c525_product_request_id PDTREQID, gm_pkg_op_consignment.get_hold_flg(t504alt.c504_consignment_id) holdfl, TO_CHAR (t525.c525_requested_date, '"
            + strApplDateFmt + "') reqsdt ");
    sbQuery
        .append(" , get_distributor_name (t525.c525_request_for_id) dname, get_code_name (t525.c901_ship_to) shiptonm");
    sbQuery
        .append(" , get_user_name (t525.c525_created_by) cuser, TO_CHAR (t525.c525_created_date, '"
            + strTimeDateFmt + "') cdate");
    sbQuery.append(" , t526.c526_qty_requested qty, TO_CHAR (t526.c526_required_date, '"
        + strApplDateFmt + "') reqrdt");
    sbQuery.append(" , TO_CHAR (t525.c525_surgery_date, '" + strApplDateFmt + "')  surgdt");
    sbQuery.append(" , t525.c525_request_for_id reqforid, t525.c901_ship_to shipto");
    sbQuery
        .append(" , t526.c526_product_request_detail_id reqdetailid, NVL(t526.c207_set_id,t526.C205_Part_Number_Id) setid");
    sbQuery
        .append(" , DECODE(t526.c207_set_id,NULL,get_partnum_desc (T526.c205_part_number_id),get_set_name (t526.c207_set_id)) setnm, t907.c907_ship_to_id shiptoid");
    sbQuery.append(" , t907.c901_delivery_carrier shipcarr, t907.c901_delivery_mode shipmode");
    sbQuery.append(" , t907.c106_address_id shipaddid");
    sbQuery.append(" , get_loaner_request_status(t526.c526_product_request_detail_id) stsfl");
    sbQuery
        .append(" , DECODE(t526.c526_status_fl, '20', gm_pkg_op_loaner.get_loaner_status (t504acl.c504a_status_fl), '') cn_status");
    sbQuery.append(" , t504alt.c504_consignment_id consignid");
    sbQuery.append(" , t504acl.c504a_etch_id etchid");
    sbQuery.append(" , t526.c526_status_fl stsid");
    sbQuery.append(" , t525.c525_created_date crdate");
    sbQuery
        .append(" FROM t526_product_request_detail t526, t525_product_request t525, t907_shipping_info t907");
    sbQuery.append(", t504a_consignment_loaner t504acl ");
    sbQuery
        .append(", (SELECT  t504a.c504_consignment_id, c526_product_request_detail_id from t504a_consignment_loaner t504a, ");
    sbQuery.append(" (SELECT  t504alt.c504_consignment_id, t526.c526_product_request_detail_id ");
    sbQuery
        .append(" FROM t504a_loaner_transaction t504alt,t526_product_request_detail t526,t525_product_request t525 ");
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" ,t7104_case_set_information t7104 , t7103_schedule_info t7103 , t7100_case_information t7100");
    }
    sbQuery
        .append(" WHERE t504alt.c526_product_request_detail_id IS NOT NULL AND t504alt.c504a_void_fl IS NULL AND t526.c526_product_request_detail_id = t504alt.c526_product_request_detail_id ");
    sbQuery.append(" AND t525.c525_product_request_id = t526.c525_product_request_id ");
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" AND t504alt.c526_product_request_detail_id = t7104.c526_product_request_detail_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7103.c7100_case_info_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7104.c7100_case_info_id ");
    }
    if (!strDtType.equals("")) {
      if (strDtType.equals("SG")) {
        strSubdtType = " t525.C525_SURGERY_DATE ";
      } else if (strDtType.equals("PS")) {
        strSubdtType = " t526.C526_REQUIRED_DATE ";
      } else if (strDtType.equals("RD")) {
        strSubdtType = " t525.C525_REQUESTED_DATE ";
      } else if (strDtType.equals("ESD")) {
        strSubdtType = " T7103.C7103_Start_Date ";
      } else if (strDtType.equals("EED")) {
        strSubdtType = " T7103.C7103_End_Date ";
      }
      sbQuery.append(" AND " + strSubdtType + " >= TO_DATE ('");
      sbQuery.append(strFromDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");
      sbQuery.append(" AND " + strSubdtType + " <= TO_DATE ('");
      sbQuery.append(strToDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");

    } else {
      sbQuery.append(" AND t526.c526_required_date >= TO_DATE ('");
      sbQuery.append(strFromDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");
      sbQuery.append(" AND t526.c526_required_date <= TO_DATE ('");
      sbQuery.append(strToDate);
      sbQuery.append("', '" + strApplDateFmt + "' ) ");
    }
    sbQuery
        .append(" GROUP BY t504alt.c504_consignment_id, t526.c526_product_request_detail_id) t504alt where t504a.c504_consignment_id = t504alt.c504_consignment_id AND t504a.c504a_status_fl != 60 AND t504a.c504a_void_fl IS NULL) t504alt ");
    sbQuery.append(" WHERE c901_request_type = ");
    sbQuery.append(strType);
    sbQuery.append(" AND t907.C901_SOURCE = 50185");
    if(strInputParam.equals("ToPutAway") || strHInputParam.equals("ToPutAway")) {
    	sbQuery.append(" AND t504acl.c504A_status_fl = 58 ");
    }
    if(strInputParam.equals("ToPick") || strHInputParam.equals("ToPick")) {
    	sbQuery.append(" AND t504acl.c504A_status_fl = 7 ");
    }
    if(strInputParam.equals("PendingShipping") || strHInputParam.equals("PendingShipping")) {
    	sbQuery.append(" AND t504acl.c504A_status_fl = 10 ");
    }
    if((strInputParam.equals("sameday") && strSelectedStatus.equals("Open")) || (strHInputParam.equals("sameday") && strHSelectedStatus.equals("Open"))) {
    	sbQuery.append(" AND t907.c901_Delivery_mode = 5038 AND t526.c526_status_fl = 10 ");
    }
    if((strInputParam.equals("sameday") && strSelectedStatus.equals("ToPick")) || (strHInputParam.equals("sameday") && strHSelectedStatus.equals("ToPick"))) {
    	sbQuery.append(" AND t907.c901_Delivery_mode = 5038 AND t504acl.c504A_status_fl = 7  ");
    }
    if((strInputParam.equals("sameday") && strSelectedStatus.equals("ToShip")) || (strHInputParam.equals("sameday") && strHSelectedStatus.equals("ToShip"))) {
    	sbQuery.append(" AND t907.c901_Delivery_mode = 5038 AND t504acl.c504A_status_fl = 10 ");
    }
    sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id");
    sbQuery.append(" AND t525.c525_product_request_id = t907.c907_ref_id(+)");
    sbQuery.append(" AND t525.c525_void_fl IS NULL");
    sbQuery.append(" AND t526.c526_void_fl IS NULL");
    sbQuery
        .append(" AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504alt.C526_PRODUCT_REQUEST_DETAIL_ID(+)");
    sbQuery.append(" AND t504alt.c504_consignment_id = t504acl.c504_consignment_id(+) ");
    sbQuery.append(" AND t907.c907_void_fl(+) IS NULL");
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    if (!strSetId.equals("")) {
      sbQuery.append(" AND  c207_set_id = '");
      sbQuery.append(strSetId);
      sbQuery.append("'");
    }
    if (!strStatus.equals("")) {
      sbQuery.append(" AND  t526.c526_status_fl in ( ");
      sbQuery.append(strStatus);
      sbQuery.append(" )");
    }
    if (!strSetName.equals("")) {
      sbQuery.append(" and upper(get_set_name (t526.c207_set_id )) like upper('%" + strSetName
          + "%')");
    }

    if (!strDistId.equals("")) {
      sbQuery.append(" AND  t525.c525_request_for_id = '");
      sbQuery.append(strDistId);
      sbQuery.append("'");
    }
    if (!strPrdReqId.equals("")) {
      sbQuery.append(" AND  t526.c525_product_request_id=");
      sbQuery.append(strPrdReqId);
    }

    if (!strDtType.equals("")) {
      String strdt = "";
      if (strDtType.equals("SG")) {
        strdt = " t525.C525_SURGERY_DATE ";
      } else if (strDtType.equals("PS")) {
        strdt = " t526.C526_REQUIRED_DATE ";
      } else if (strDtType.equals("RD")) {
        strdt = " t525.C525_REQUESTED_DATE ";
      } else if (strDtType.equals("ESD")) {
        strdt = " T7103.C7103_Start_Date ";
      } else if (strDtType.equals("EED")) {
        strdt = " T7103.C7103_End_Date ";
      }
      sbMergeQuery.append(" and");
      sbMergeQuery.append(strdt);
      sbMergeQuery.append(" >= TO_DATE('");
      sbMergeQuery.append(strFromDate);
      sbMergeQuery.append("', '" + strApplDateFmt + "' ) ");
      sbMergeQuery.append(" and");
      sbMergeQuery.append(strdt);
      sbMergeQuery.append("  <= TO_DATE('");
      sbMergeQuery.append(strToDate);
      sbMergeQuery.append("', '" + strApplDateFmt + "')");

      if ((!strDtType.equals("ESD") && !strDtType.equals("EED"))) {
        sbQuery.append(sbMergeQuery);
      }
    }
    if (strTxnType.equals("InhouseLoanerRequests")) {
      sbQuery
          .append(" ) extdata, t7104_case_set_information t7104 , t7103_schedule_info t7103 , t7100_case_information t7100");
      sbQuery.append(" WHERE extdata.reqdetailid = t7104.c526_product_request_detail_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7103.c7100_case_info_id ");
      sbQuery.append(" AND t7100.c7100_case_info_id = t7104.c7100_case_info_id ");
      sbQuery.append(" AND t7104.c7104_void_fl IS NULL ");
      sbQuery.append(" AND t7100.c7100_void_fl IS NULL ");
      sbQuery.append(" AND t7100.c901_case_status != 19526 "); // exclude rescheduled requests.
      sbQuery.append(" AND t7100.c901_type = 1006504 "); // 1006504 - event

      if (!strEventId.equals("")) {
        sbQuery.append(" AND t7100.c7100_case_info_id = '" + strEventId + "' ");
      }
      if (!strEmpId.equals("")) {
        sbQuery.append(" AND t7104.c7104_created_by = '" + strEmpId + "' ");
      }

      if ((strDtType.equals("ESD") || strDtType.equals("EED"))) {
        sbQuery.append(sbMergeQuery);
      }
    }
    sbQuery.append(" order by crdate");

    log.debug("LoanerOpenRequests Query" + sbQuery.toString());

    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alResult;

  }// End of reportLoanerOpenRequests

  /**
   * fetchLoanerIncidentDetail - This method is used to fetch loaner incident details
   * 
   * @param hmValues
   * @return ArrayList
   * @exception AppError
   */
  public HashMap fetchLoanerIncidentDetail(HashMap hmValues) throws AppError {
    GmCrossTabReport gmCrossTabReport = new GmCrossTabReport();
    HashMap hmFinalValue = new HashMap();

    ResultSet rsHeader = null;
    ResultSet rsDetails = null;

    String strRepId = GmCommonClass.parseNull((String) hmValues.get("REPID"));
    String strFromDate = GmCommonClass.parseNull((String) hmValues.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmValues.get("TODATE"));
    String strInputString = GmCommonClass.parseNull((String) hmValues.get("HINPUTSTRING"));
    String strReportBy = GmCommonClass.parseZero((String) hmValues.get("REPORTBY"));
    String strType = GmCommonClass.parseNull((String) hmValues.get("INCTYPE"));
    String strGrp = GmCommonClass.parseNull((String) hmValues.get("CODEGROUP"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_incident_detail", 9);
    log.debug("repId......" + strRepId + "...inputStr1....." + strInputString);
    log.debug("strType......" + strType + "...strToDate....." + strToDate + "....strGrp..."
        + strGrp);
    gmDBManager.setInt(1, Integer.parseInt(strRepId));
    gmDBManager.setString(2, strFromDate);
    gmDBManager.setString(3, strToDate);
    gmDBManager.setString(4, strInputString);
    gmDBManager.setInt(5, Integer.parseInt(strReportBy));
    gmDBManager.setInt(6, Integer.parseInt(strType));
    gmDBManager.setString(7, strGrp);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.execute();
    rsDetails = (ResultSet) gmDBManager.getObject(8);
    rsHeader = (ResultSet) gmDBManager.getObject(9);
    log.debug("rsDetails........" + rsDetails);
    log.debug("rsHeader........" + rsHeader);
    hmFinalValue = gmCrossTabReport.GenerateCrossTabReport(rsHeader, rsDetails);
    gmDBManager.close();
    return hmFinalValue;
  }

  /**
   * getConsignDetail - This overloaded method is used to fetch Consignment details
   * 
   * @param alReqDetail
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getConsignDetails(ArrayList alReqDetail) throws AppError {
    ArrayList alDetails = new ArrayList();

    String[] strArr = new String[alReqDetail.size()];
    int i = 0;
    Iterator itReqDet = alReqDetail.iterator();

    while (itReqDet.hasNext()) {
      HashMap hmReqDet = (HashMap) itReqDet.next();
      String strConsId = (String) hmReqDet.get("CONID");
      strArr[i] = strConsId;
      i = i + 1;
    }

    String strInput = GmCommonClass.createInputString(strArr);
    log.debug("strInput........" + strInput);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_OP_FCH_CONSIGNDETAILS", 2);
    gmDBManager.setString(1, strInput);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    log.debug("alDetails........" + alDetails);

    gmDBManager.close();
    return alDetails;
  }

  /**
   * method to return the loaner log. it details who created the loaner and when
   * 
   * @param strLoanType
   * @return
   * @throws AppError
   */
  public RowSetDynaClass loadSetLog(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    strType = strType.equals("") ? "0" : strType;
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FRMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strDateFormat = getCompDateFmt();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbString = new StringBuffer();
    String strCompPlantFilter = "";
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOAD_SET_LOG")); // LOAD_SET_LOG-Rule
                                                                                          // Value:=
                                                                                          // plant.
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

    if (!strType.equals("51083")) {
      sbString
          .append(" select t504.c504_consignment_id cnid,C207_SET_ID setid ,get_set_name(C207_SET_ID) setname , C504A_LOANER_CREATE_DATE crdate");
      sbString
          .append(" , get_user_name(C504A_LOANER_CREATE_BY) crby,  get_code_name(C504_TYPE) type");
      sbString
          .append(" from t504a_consignment_loaner t504a, T504_CONSIGNMENT t504  where  t504.C504_CONSIGNMENT_ID = t504a.C504_CONSIGNMENT_ID");
      // Fetch records based company/plant for EDC countries
      sbString.append(" AND ( T504.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbString.append(" OR T504.C5040_PLANT_ID = " + strCompPlantFilter);
      sbString.append(") ");
      sbString.append(" and C504A_VOID_FL is null");
      sbString.append(" and C504_VOID_FL is null");
      sbString.append(" and C504_TYPE  in (");
      if (strType.equals("0")) {
        sbString.append(" 4127,4119 "); // product/inhouse loaners
      } else {
        sbString.append(strType);
      }
      sbString.append(" )");
      if ((strFromDate != "") && (strToDate != "")) {
        sbString.append(" AND C504A_LOANER_CREATE_DATE BETWEEN NVL (TO_DATE ('");
        sbString.append(strFromDate);
        sbString.append("', '" + strDateFormat + "') ");
        sbString.append(" , C504A_LOANER_CREATE_DATE)	AND NVL (TO_DATE ('");
        sbString.append(strToDate);
        sbString.append("', '" + strDateFormat + "'), C504A_LOANER_CREATE_DATE	) ");
      }
      if (!strSetId.equals("")) {
        sbString.append(" and 	t504.C207_SET_ID like '%");
        sbString.append(strSetId.trim());
        sbString.append("%'");
      }
    }
    if (strType.equals("0") || strType.equals("51083")) {
      if (!strType.equals("51083")) {// build sets
        sbString.append(" union");
      }
      sbString
          .append(" select t504.c504_consignment_id cnid,C207_SET_ID setid ,get_set_name(C207_SET_ID) setname ,C504_VERIFIED_DATE crdate, get_user_name(C504_VERIFIED_BY) crby,  'Build Sets' type");
      sbString
          .append(" from t504a_consignment_loaner t504a, T504_CONSIGNMENT t504  where t504.C504_CONSIGNMENT_ID = t504a.C504_CONSIGNMENT_ID(+)");
      sbString.append(" and C504A_VOID_FL is null");
      sbString
          .append(" and C504_VERIFIED_DATE is not null and  C504_VERIFIED_BY is not null and C504_STATUS_FL = 2");
      sbString.append(" AND ( T504.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbString.append(" OR T504.C5040_PLANT_ID = " + strCompPlantFilter);
      sbString.append(") ");
      if ((strFromDate != "") && (strToDate != "")) {
        sbString.append(" AND C504_VERIFIED_DATE BETWEEN NVL (TO_DATE ('");
        sbString.append(strFromDate);
        sbString.append("', '" + strDateFormat + "') ");
        sbString.append(" , C504_VERIFIED_DATE)	AND NVL (TO_DATE ('");
        sbString.append(strToDate);
        sbString.append("', '" + strDateFormat + "'), C504_VERIFIED_DATE	) ");
      }
      if (!strSetId.equals("")) {
        sbString.append(" and 	t504.C207_SET_ID like '%");
        sbString.append(strSetId.trim());
        sbString.append("%'");
      }
    }

    sbString.append(" ORDER BY crdate DESC");
    sbString.append(" ");
    log.debug("query........... " + sbString.toString());
    rdResult = gmDBManager.QueryDisplayTagRecordset(sbString.toString());
    gmDBManager.close();
    return rdResult;
  }

  /**
   * function that calls a procedure to fetch all the open inhouse trans
   * 
   * @param strConsignId
   * @return
   * @throws AppError
   */
  public ArrayList fetchOpenInHouseTrans(String strConsignId, String strhScrnFrom) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_inhouse_master.gm_fch_open_inhouse_trans", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strhScrnFrom);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alReturn;
  }

  /**
   * function that calls a procedure to fetch all back order parts
   * 
   * @param strConsignId
   * @return
   * @throws AppError
   */
  public ArrayList fetchLoanerBOParts(String strConsignId) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_inventory.gm_fch_loaner_bo_parts", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.execute();

    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    log.debug("alReturn===>" + alReturn.size());
    return alReturn;
  }

  /**
   * Method to fetch the Case Summary reports
   * 
   * @param hmParam
   * @return ArrayList
   */

  // code merge from OUS side
  public ArrayList loadCaseSummaryInfo(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    // Get values from HashMap
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strAccID = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strStsID = GmCommonClass.parseNull((String) hmParam.get("STSID"));
    String strDtType = GmCommonClass.parseNull((String) hmParam.get("DTTYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strApplnDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));

    // Check for unselected values ie: [Choose One] in combo boxes
    strDistID = (strDistID.equals("0")) ? "" : strDistID;
    strDtType = (strDtType.equals("0")) ? "" : strDtType;
    strAccID = (strAccID.equals("0")) ? "" : strAccID;
    strStsID = (strStsID.equals("0")) ? "" : strStsID;


    sbQuery.append("SELECT t525.c525_product_request_id CASEID,");
    sbQuery.append("GET_DISTRIBUTOR_NAME (t525.c525_request_for_id) DISTNAME,");
    sbQuery.append("t525.c525_request_for_id DISTID,");
    sbQuery.append("GET_ACCOUNT_NAME(t525.c704_ACCOUNT_ID) ACCNAME,");
    sbQuery.append("GET_REP_NAME(t525.C703_SALES_REP_ID) REPNAME,");
    sbQuery.append("GET_REP_NAME(t525.c703_ass_rep_id) ASSOCREPNM,");
    sbQuery.append("TO_CHAR(t525.c525_SURGERY_DATE, '");
    sbQuery.append(strApplnDateFmt);
    sbQuery.append("') SURGERYDATE,");
    sbQuery.append("TO_CHAR(t525.c525_REQUESTED_DATE, '");
    sbQuery.append(strApplnDateFmt);
    sbQuery.append("') REQUIREDDATE, ");
    sbQuery.append("DECODE (t525.c525_status_fl, '10', 'Open' , '40', 'Closed')STATUS ");
    sbQuery.append("FROM t525_product_request t525 ");
    sbQuery.append("WHERE t525.c525_void_fl IS NULL ");
    sbQuery.append("AND t525.c525_status_fl IS NOT NULL ");

    if (!strDistID.equals("")) {
      sbQuery.append("AND t525.c525_request_for_id = '");
      sbQuery.append(strDistID);
      sbQuery.append("' ");
    }

    if (!strAccID.equals("")) {
      sbQuery.append("AND t525.c704_account_id = '");
      sbQuery.append(strAccID);
      sbQuery.append("' ");
    }

    if (!strStsID.equals("")) {
      sbQuery.append("AND t525.c525_status_fl = '");
      sbQuery.append(strStsID);
      sbQuery.append("' ");
    }


    /*
     * This code for getting surgery or request date based report. if(!strDtType.equals("")){ if
     * (strDtType.equals("SG")) { sbQuery.append("AND t525.c525_surgery_date "); } else if
     * (strDtType.equals("RD")) { sbQuery.append("AND t525.c525_requested_date "); }
     */
    sbQuery.append("AND t525.c525_surgery_date ");
    sbQuery.append("BETWEEN TO_DATE('");
    sbQuery.append(strFromDt);
    sbQuery.append("', '");
    sbQuery.append(strApplnDateFmt);
    sbQuery.append("') ");
    sbQuery.append("AND TO_DATE('");
    sbQuery.append(strToDt);
    sbQuery.append("', '");
    sbQuery.append(strApplnDateFmt);
    sbQuery.append("') ");


    sbQuery.append("ORDER BY t525.c525_surgery_date, ACCNAME");

    log.debug("loadCaseSummaryInfo Query: " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alReturn;
  }


  /**
   * Function that calls a procedure to fetch all the loaner part details
   * 
   * @ArrayList alReturn
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList getSetConsignDetails(String strConsignId, String strType, String strAction)
      throws AppError {
    ArrayList alReturn = new ArrayList();
    try {
      String strGroupfl = "";
      if (strAction.equals("EditLoad") || strAction.equals("ViewSet")
          || strAction.equals("ReloadHist") || strAction.equals("REPROCESS")
          || strAction.equals("ProcessReturnSet") || strAction.equalsIgnoreCase("NO")) {
        // When Show Cnum in Loaner Stock sheet is set to NO, we should not display cnum in LSS.
        strGroupfl = "Y";
      }
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      gmDBManager.setPrepareString("GM_LS_FCH_SETCSGDETAILS_OUS", 4);
      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmDBManager.setString(1, strConsignId);
      gmDBManager.setString(2, strType);
      gmDBManager.setString(4, strGroupfl);
      gmDBManager.execute();
      alReturn = gmDBManager.returnArrayList(((ResultSet) gmDBManager.getObject(3)));
      gmDBManager.close();
    } catch (AppError e) {
      if (e.getMessage().indexOf("01722") != -1) {
        throw new AppError(AppError.APPLICATION, "1055");
      } else {
        throw new AppError(e);
      }
    }

    return alReturn;
  }

  /**
   * function to fetch set consignment details
   * 
   * @ArrayList alReturn
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList getSetConsignDetails(String strConsignId) throws AppError {
    ArrayList alReturn = new ArrayList();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      gmDBManager.setPrepareString("gm_ls_fch_lnsetdetail", 2);

      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.setString(1, strConsignId);
      gmDBManager.execute();
      alReturn = gmDBManager.returnArrayList(((ResultSet) gmDBManager.getObject(2)));
      gmDBManager.close();
    } catch (AppError e) {
      if (e.getMessage().indexOf("01722") != -1) {
        throw new AppError(AppError.APPLICATION, "1055");
      } else {
        throw new AppError(e);
      }
    }
    return alReturn;
  }
  /**
   * getConsignDetails - used to get ConsignId and ProductRequestId
   * @param strConsignId, strProductRequestId
   * @return
   * @throws AppError
   */
  public HashMap getConsignDetails(String strConsignId, String strProductRequestId) throws AppError {
	    HashMap hmReturn = new HashMap();
	    ResultSet rs = null;

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_consignment.gm_op_it_fch_consigndetails", 3);
        gmDBManager.setString(1, strConsignId);
	    gmDBManager.setString(2, strProductRequestId);
	    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    rs = (ResultSet) gmDBManager.getObject(3);
	    hmReturn = gmDBManager.returnHashMap(rs);
	    gmDBManager.close();
	    return hmReturn;
	    
	  }

}// End of GmInHouseSetsReportBean

