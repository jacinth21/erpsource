package com.globus.operations.logistics.displaytags;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.logistics.forms.GmChargesApprovalForm;
import com.globus.operations.logistics.forms.GmMissingChargesDtlForm;

public class DTMissingChargesDetailsWrapper extends TableDecorator{
	GmMissingChargesDtlForm gmMissingChargesDtlForm;
	ArrayList alCommnetsList = new ArrayList();
	List alStatus ;

	HashMap hmKeyValueList = new HashMap();
    private int intCount = 0;
    private int chkCnt = 0;
    private String formatVal;
    private DecimalFormat moneyFormat;
	Iterator alReconCommnents;
	Iterator alStatusList;
	String strOptKey = "";
	String strOptValue = "";
    public StringBuffer  strValue = new StringBuffer();
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.;    
    public void init(PageContext context, Object decorated, TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		String strFormName = GmCommonClass.parseNull((String)getPageContext().getRequest().getParameter("FORMNAME"));
		strFormName = strFormName.equals("") ? "frmGmMissingChargesDtl" : strFormName;
		log.debug("FORMNAME: " + strFormName);
		if(strFormName.equals("frmGmMissingChargesDtl")) {
			gmMissingChargesDtlForm = (GmMissingChargesDtlForm) getPageContext().getAttribute(strFormName, PageContext.REQUEST_SCOPE);
		}
		else
		{
			gmMissingChargesDtlForm = (GmChargesApprovalForm) getPageContext().getAttribute(strFormName, PageContext.REQUEST_SCOPE);
		}
		
		if(gmMissingChargesDtlForm != null){
			alCommnetsList = GmCommonClass.parseNullArrayList(gmMissingChargesDtlForm.getAlCommnetsList());	
			alStatus = GmCommonClass.parseNullArrayList(gmMissingChargesDtlForm.getAlStatus());
		}
		String  strCurrSign	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessCurrSymbol"));
		this.moneyFormat = new DecimalFormat(strCurrSign+"#,###,###.00"); //$NON-NLS-1$ // removed the hardcoded value of $ as currency type is shown in the header
	}
    public String getCHECKBOX()
    {
    	StringBuffer  strValue = new StringBuffer();
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();               
    	String strChargeId   = GmCommonClass.parseNull(String.valueOf(db.get("CHARGE_ID")));
    	String strCharge   = GmCommonClass.parseNull(String.valueOf(db.get("CHARGE")));
    	String strTransaID = GmCommonClass.parseNull(String.valueOf(db.get("TRANS_ID")));
    	String strSetID = GmCommonClass.parseNull(String.valueOf(db.get("SET_ID")));
    	String strDistId = GmCommonClass.parseNull(String.valueOf(db.get("DIST_ID")));
    	String strRepID = GmCommonClass.parseNull(String.valueOf(db.get("REP_ID")));
    	String strAssocRepID = GmCommonClass.parseNull(String.valueOf(db.get("ASSOCREPID")));
    	String strPartNum = GmCommonClass.parseNull(String.valueOf(db.get("P_S_NUM")));
    	String strMissQty = GmCommonClass.parseNull(String.valueOf(db.get("MISSING_QTY")));
    	String strListPrice = GmCommonClass.parseNull(String.valueOf(db.get("LIST_PRICE")));
    	String strPartDescription = GmCommonClass.parseNull(String.valueOf(db.get("PDESC")));
    	String strChargeQty = GmCommonClass.parseNull(String.valueOf(db.get("CHARGE_QTY")));
    	String strStatusFl = GmCommonClass.parseNull(String.valueOf(db.get("STATUS_FL")));
    	String strDisabled = "";
    	
    	if(strStatusFl.equals("25"))
    	{
    		strDisabled = "disabled";
    	}
    	
    	if(strListPrice.equals("null")){
    		strListPrice = "";
    	}    	
    	if(strDistId.equals("")){
    		strDistId = "";    		
    	}
    	if(strRepID.equals("")){
    		strRepID = "";    		
    	}
    	if(strAssocRepID.equals("null")){
    		strAssocRepID = "";    		
    	}
    	if(strMissQty.equals("null")){
    		strMissQty = "";
    	}
    	if(strPartNum.equals("null") || strPartDescription.equals("null") ){
    		strPartNum = "";
    	}
    	if(strCharge.equals("null")){
    		strCharge = "";
    	}
    	if(strTransaID.equals("null")){
    		strTransaID = "";
    	}
    	if(strSetID.equals("null")){
    		strSetID = "";
    	}
    	if(strPartDescription.equals("null")){
    		strPartDescription = "";
    	}  	
    	if(strChargeId.equals("null")){
    		strChargeId = "0";
    	}
    	/*if(strRepID.equals(strDistId))
    	{
    		strRepID = "";    		
    	}*/
    	if(strChargeQty.equals("null")){
    		strChargeQty = "0";
    	}
    	
    	
    	
    	strValue.setLength(0);
    	strValue.append("<input type='checkbox'" + strDisabled + " onClick='fnSetEditCharge("+intCount+","+strCharge+")' name='chk_rowwise"); 
    	 strValue.append(String.valueOf(intCount));
    	 strValue.append("' > ");
    	 strValue.append("<input type = 'hidden' name = rowcharge");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strCharge);
         strValue.append("'> ");
         strValue.append("<input type = 'hidden' name = transaid");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strTransaID);
         strValue.append("'> ");
         strValue.append("<input type = 'hidden' name = setid");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strSetID);
         strValue.append("'> ");
         strValue.append("<input type = 'hidden' name = distid");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strDistId);
         strValue.append("'> ");
         strValue.append("<input type = 'hidden' name = repid");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strRepID);
         strValue.append("'> ");
         
         strValue.append("<input type = 'hidden' name = assocrepid");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strAssocRepID);
         strValue.append("'> ");
         
         
         strValue.append("<input type = 'hidden' name = partnum");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strPartNum);
         strValue.append("'> ");
         strValue.append("<input type = 'hidden' name = missQty");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strMissQty);
         strValue.append("'> ");
         strValue.append("<input type = 'hidden' name = listprice");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strListPrice);
         strValue.append("'> ");       
         strValue.append("<input type = 'hidden' name = chargeid");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strChargeId);
         strValue.append("'> ");
         strValue.append("<input type = 'hidden' name = chargeQty");
         strValue.append(String.valueOf(intCount) );
         strValue.append(" value='");
         strValue.append(strChargeQty);
         strValue.append("'> ");
       	return strValue.toString();               
    }  
    public String getTKT_ID(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strTicket = GmCommonClass.parseNull((String)db.get("TKT_ID"));
    	String strTicketUrl = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("TICKET_URL"));
    	strTicketUrl = strTicketUrl + strTicket;
    	if(!strTicket.equals("")){
    		strValue.append("<a href=javascript:fnTicket('"+strTicketUrl+"')");
    		strValue.append(">"+strTicket+" </a>");	   
    	} 
       	return strValue.toString();  
      
    }
    public String getTKT_IDEX(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strTicket = GmCommonClass.parseNull((String)db.get("TKT_ID"));
    	String strTicketUrl = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("TICKET_URL"));
    	strTicketUrl = strTicketUrl + strTicket;
    	if(!strTicket.equals("")){
    		strValue.append(strTicket);
    	} 
       	return strValue.toString();  
      
    }
    public String getREQ_ID(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    
    	String strRequestID = GmCommonClass.parseNull(String.valueOf(db.get("REQ_ID")));   
    	if(!strRequestID.equals("")){
    		 strValue.append("<input type = 'hidden' name = requestID");
             strValue.append(String.valueOf(intCount) );
             strValue.append(" value='");
             strValue.append(strRequestID);
             strValue.append("'> ");
    		strValue.append("<a href=javascript:fnPrintDetails('"+strRequestID+"')");
    		strValue.append(">"+strRequestID+" </a>");	   
    	} 	
       	return strValue.toString();  
    }    
    
    public String getREQ_IDEX(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    
    	String strRequestID = GmCommonClass.parseNull(String.valueOf(db.get("REQ_ID")));   
    	if(!strRequestID.equals("")){    		
    		strValue.append(strRequestID);	   
    	}else{
    		strValue.append("");
    	}
       	return strValue.toString();  
    }    
    
    public String getLIST_PRICE(){  
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    
    	//String strListPrice = GmCommonClass.parseNull(String.valueOf(db.get("LIST_PRICE"))); 
    	if(db.get("LIST_PRICE")  == null){
    		return "";	
    	}else{
    		return this.moneyFormat.format(db.get("LIST_PRICE") );
    	}
    }
    public String getCHARGE(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strCharge = GmCommonClass.parseNull(String.valueOf(db.get("CHARGE")));
    	String strDisplayCharge = "";
    	strValue.append("<input type = 'hidden' name = charge");
    	strValue.append(String.valueOf(intCount) );
        strValue.append(" value='");
        strValue.append(strCharge);
        strValue.append("'> ");
        strValue.append(strCharge); 
       	return strValue.toString();  
    }
    public String getCHARGEEX(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strCharge = GmCommonClass.parseNull(String.valueOf(db.get("CHARGE")));
    	strValue.append(strCharge);    	 
       	return strValue.toString();  
    }
    public String getDED_DATE(){
    	 Calendar mycalender = Calendar.getInstance();
    	String  strApplJSDateFmt	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		String  strApplDateFmt		=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	int currMonth = GmCalenderOperations.getCurrentMonth();
    	String strSysDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
    	int day = mycalender.get(Calendar.DATE);
    	String strMidDayOfMonth = currMonth + "/" + "15" + "/" + GmCalenderOperations.getCurrentYear();
    	String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth);
		String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth-1);
    	String strDistType = GmCommonClass.parseNull(String.valueOf(db.get("DIST_TYPE")));
    	String strDeductionDate = GmCommonClass.parseNull(String.valueOf(db.get("DED_DATE")));    	
    	String strDueDate = "";
     	
     	String strMidDayofNextMonth ="";
    	mycalender.add(Calendar.MONTH, 1);
    	if(mycalender.get(mycalender.MONTH) < 9){
    		strMidDayofNextMonth = "0" + (mycalender.get(mycalender.MONTH)+1) + "/15/" +mycalender.get(mycalender.YEAR);
    	}else{
    		strMidDayofNextMonth =  (mycalender.get(mycalender.MONTH)+1) + "/15/" +mycalender.get(mycalender.YEAR);
    	}
  	
    	if(strDeductionDate.equals("") || strDeductionDate.equals("null")){
    		if(strDistType.equals("70100")){
    			//strDueDate = strMonthLastDay;
    			strDueDate = strMidDayofNextMonth;
    		}else if(strDistType.equals("70101")){    		
    			/*  if(GmCalenderOperations.isGreaterThan(strSysDate, strMidDayOfMonth, strApplDateFmt))
    			  {
    				  strMidDayOfMonth = (currMonth + 1) + "/" + "15" + "/" + GmCalenderOperations.getCurrentYear(); 
    			  }*/
    				//strDueDate = strMonthLastDay; 
    				strDueDate = strMidDayofNextMonth;
    		}
    	}else{
    		strDueDate = strDeductionDate;
    	}	
		
		strValue.append("<input type=\"text\" maxlength=\"10\" id=\"deductiondt" + intCount+"\" name=\"deductiondt" + intCount+"\" value=\""+strDueDate+"\"");		
		strValue.append(" size='9' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strValue.append("/>&nbsp;<img id='Img_Date' style='cursor:hand'");	
		strValue.append("onclick=\"javascript:showSingleCalendar('divDueDt"+intCount+"','deductiondt"+intCount+"','"+strApplJSDateFmt+"');\" title='Click to open Calendar'");
		strValue.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
		strValue.append("<div id=\"divDueDt"+intCount+"\" style=\"position: absolute; z-index: 10;\"></div>");	
		strValue.append("<input type=\"hidden\" value=\""+strDueDate+"\" name=\"deductiondate\" id=");
		strValue.append(intCount);
		strValue.append("> &nbsp; ");
       	return strValue.toString();     
    }
    public String getDED_DATE_EXCEL(){
    	Calendar mycalender = Calendar.getInstance();
     	String  strApplJSDateFmt	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
 		String  strApplDateFmt		=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
     	StringBuffer  strValue = new StringBuffer();    	
     	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
     	int currMonth = GmCalenderOperations.getCurrentMonth();
     	String strSysDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
     	int day = mycalender.get(Calendar.DATE);
     	
     	log.debug("currMonth: " + currMonth);
     	
 		String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth-1);
 		
 		log.debug("strMonthLastDay: " + strMonthLastDay);
 		
     	String strDistType = GmCommonClass.parseNull(String.valueOf(db.get("DIST_TYPE")));
     	
     	String strMidDayofNextMonth ="";
    	mycalender.add(Calendar.MONTH, 1);
    	if(mycalender.get(mycalender.MONTH) < 9){
    		strMidDayofNextMonth = "0" + (mycalender.get(mycalender.MONTH)+1) + "/15/" +mycalender.get(mycalender.YEAR);
    	}else{
    		strMidDayofNextMonth =  (mycalender.get(mycalender.MONTH)+1) + "/15/" +mycalender.get(mycalender.YEAR);
    	}
    	
     	String strDeductionDate = GmCommonClass.parseNull(String.valueOf(db.get("DED_DATE")));    	
     	String strDueDate = "";
     	if(strDeductionDate.equals("") || strDeductionDate.equals("null")){
     		if(strDistType.equals("70100")){
     			//strDueDate = strMonthLastDay;
     			strDueDate = strMidDayofNextMonth;
     		}else if(strDistType.equals("70101")){     			
     			//strDueDate = strMonthLastDay;
     			strDueDate = strMidDayofNextMonth;
     		}
     	}else{
     		strDueDate = strDeductionDate;
     	} 		
    	return strDueDate;     
    }
    
    public String getSTATUS_FL(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strStatusFl = GmCommonClass.parseNull(String.valueOf(db.get("STATUS_FL")));
    	Calendar mycalender = Calendar.getInstance();
    	String  strApplJSDateFmt	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		String  strApplDateFmt		=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
    	int currMonth = GmCalenderOperations.getCurrentMonth();
    	String strSysDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
    	int day = mycalender.get(Calendar.DATE);
    	String strMidDayOfMonth = currMonth + "/" + "15" + "/" + GmCalenderOperations.getCurrentYear();
    	String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth);
		String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth-1);
    	String strDistType = GmCommonClass.parseNull(String.valueOf(db.get("DIST_TYPE")));
    	String strCreditSavedDate = "";    	
    	String strCrdDate = "";
    	if(strCreditSavedDate.equals("") || strCreditSavedDate.equals("null")){
    		if(strDistType.equals("70100")){
    			strCrdDate = strMonthLastDay;
    		}else if(strDistType.equals("70101")){    		
    			  if(GmCalenderOperations.isGreaterThan(strSysDate, strMidDayOfMonth, strApplDateFmt))
    			  {
    				  strMidDayOfMonth = (currMonth + 1) + "/" + "15" + "/" + GmCalenderOperations.getCurrentYear(); 
    			  }
    			strCrdDate = strMidDayOfMonth;    		
    		}
    	}else{
    		strCrdDate = strCreditSavedDate;
    	}	
    	
    //	strCrdDate= strCrdDate.replaceAll("/",":");
    	
    	String strMessgae = "";
    	if(!strStatusFl.equals("25")){
    		alStatusList = alStatus.iterator();				
			strValue.append(" <select name='cbo_status");
			strValue.append(String.valueOf(intCount));				
			strValue.append("' onChange=fnLoadCreditAmount("+intCount+",this,'"+strCrdDate+"');> <option value = '0'>[Choose One] </option>");	
			while(alStatusList.hasNext()){
				hmKeyValueList = (HashMap) alStatusList.next();
				strOptKey = (String)hmKeyValueList.get("CODEID");
				strOptValue = (String)hmKeyValueList.get("CODENM");
				if(strOptValue.equals("Approved") || strOptValue.equals("Credit"))
				{
					if (strStatusFl.equals("20") && (strOptKey.equals("50861"))){
						strOptKey += " selected"; 
					}else if(strStatusFl.equals("25") && (strOptKey.equals("50864"))){	
						strOptKey += " selected"; 
					}else if(strStatusFl.equals("30") && (strOptKey.equals("50865"))){	
						strOptKey += " selected"; 
					}else if(strStatusFl.equals("10") && (strOptKey.equals("50860"))){	
						strOptKey += " selected"; 
					}else if(strStatusFl.equals("40") && (strOptKey.equals("50862"))){	
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");
				}	
			}				
			strValue.append(" </select>");	
    	}else{
    		strMessgae = "Credited";
    		strValue.append(strMessgae);	
    	}
    	
       	return strValue.toString();      
    }
    public String getCEREDIT_AMOUNT(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    
    	String strEditCharge = GmCommonClass.parseNull(String.valueOf(db.get("EDIT_CHRG")));    	
    	String strStatusFl = GmCommonClass.parseNull(String.valueOf(db.get("STATUS_FL")));
    	String strCgarge = GmCommonClass.parseNull(String.valueOf(db.get("CHARGE")));
    	if(strStatusFl.equals("25")){
    		 strValue.append("");
    	}else{
    		strValue.append("<input type=\"text\" maxlength=\"7\" disabled='true' name=\"crediCharge" + intCount+"\" value=''");		
    		strValue.append(" size='7' styleClass=\"InputArea\" title='Only editable for Credited Status' onBlur=\"changeBgColor(this,'#ffffff');\"");    	
    		strValue.append("<input type = 'hidden' name = preeditcharge");
    		strValue.append(String.valueOf(intCount) );
    		strValue.append(" value='");
    		strValue.append(strCgarge);
    		strValue.append("'>");
    	}
    	return strValue.toString();    
    }
    public String getCEREDIT_AMOUNTEX(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();   
    	String strEditCharge = GmCommonClass.parseNull(String.valueOf(db.get("CHARGE")));
    	strValue.append(strEditCharge);
    	return strValue.toString();    
    }
    
    public String getCREDIT_DATE(){
    	String  strApplJSDateFmt	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
 		String  strApplDateFmt		=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strCreditedDate = GmCommonClass.parseNull(String.valueOf(db.get("CRD_DATE")));  
    	String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUS_FL")));
    	
    	
    	if(strStatus.equals("25")){
    		strValue.append(strCreditedDate);
    	}else{
    		strCreditedDate = "";
    		strValue.append("<input type=\"text\" maxlength=\"10\" id=\"crediteddt" + intCount+"\" name=\"crediteddt" + intCount+"\" value=\""+strCreditedDate+"\"");		
    		strValue.append(" size='9' styleClass=\"InputArea\"  onBlur=\"changeBgColor(this,'#ffffff');\"");
    		strValue.append("/>&nbsp;<img id='Img_Date' style='cursor:hand'");	
    		strValue.append("onclick=\"javascript:showSingleCalendar('divCreditDt"+intCount+"','crediteddt"+intCount+"','"+strApplJSDateFmt+"');\" title='Click to open Calendar'");
    		strValue.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
    		strValue.append("<div id=\"divCreditDt"+intCount+"\" style=\"position: absolute; z-index: 10;\"></div>");	
    		strValue.append("<input type=\"hidden\" value=\""+strCreditedDate+"\" name=\"creditdate\" id=");
    		strValue.append(intCount);
    		strValue.append("> &nbsp; ");	
    	} 
		return strValue.toString();  
		
    }
    public String getCREDIT_DATEEX(){
    	String  strApplJSDateFmt	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
 		String  strApplDateFmt		=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strCreditedDate = GmCommonClass.parseNull(String.valueOf(db.get("CRD_DATE")));  
    	strValue.append(strCreditedDate);    	
		return strValue.toString();  		
    }
    public String getRECONCI_COMMENTS(){
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();
    	StringBuffer  strValue = new StringBuffer();
    	String strReconCommnets = GmCommonClass.parseNull(String.valueOf(db.get("RECON_COMMENT")));
    	String strReconComntID  = GmCommonClass.parseNull(String.valueOf(db.get("RECON_COMMENTS")));
    	if(strReconCommnets.equals("null")){
    		strReconCommnets = "";
    	}
    	if(!strReconCommnets.equals("")){  
    		strValue.append(strReconCommnets);       		 
    	}else{
    		strValue.append("");    		  
    	}
    	strValue.append("<input type = 'hidden' name = reconcilecommnets");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" value='");
        strValue.append(strReconComntID);
        strValue.append("'> ");
    	if(this.isLastRow()){
    	   		 strValue.append("<input type = 'hidden' name = 'totalcount'");
    	            strValue.append(" value='");
    	            strValue.append(String.valueOf(intCount) );
    	            strValue.append("'> ");
    	  }
    	return strValue.toString();     	      	
    }
    public String getRECONCI_COMMENTSEX(){
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();
    	StringBuffer  strValue = new StringBuffer();
    	String strReconCommnets = GmCommonClass.parseNull(String.valueOf(db.get("RECON_COMMENT")));
    	if(strReconCommnets.equals("null")){
    		strReconCommnets = "";
    	}
    	if(!strReconCommnets.equals("")){  
    		strValue.append(strReconCommnets);       		 
    	}else{
    		strValue.append("");    		  
    	}
    	return strValue.toString();
    }
        public String getEDIT_CHRG(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strEditCharge = GmCommonClass.parseNull(String.valueOf(db.get("EDIT_CHRG")));
    	strValue.append("<input type = 'hidden' name = preeditcharge");
        strValue.append(String.valueOf(intCount) );
        strValue.append(" value='");
        strValue.append(strEditCharge);
        strValue.append("'> ");
    	if(!strEditCharge.equals("null")){
    		strValue.append("<input type=\"text\" maxlength=\"7\" name=\"editCharge" + intCount+"\" value=\""+strEditCharge+"\"");		
    		strValue.append(" size='7' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");    	
    	}else{
    		strValue.append("<input type=\"text\" maxlength=\"7\" name=\"editCharge" + intCount+"\" value=\"\"");		
    		strValue.append(" size='7' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
    	}         	
       	return strValue.toString();     
    }   
    public String getEDIT_CHRGEX(){
        	StringBuffer  strValue = new StringBuffer();    	
        	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
        	String strEditCharge = GmCommonClass.parseNull(String.valueOf(db.get("EDIT_CHRG")));
        	strValue.append(strEditCharge);
        	return strValue.toString();     
        }   
    public String getRECON_COMMENTS(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strReconCommnets = GmCommonClass.parseNull(String.valueOf(db.get("RECON_COMMENTS")));
    	if(!strReconCommnets.equals("")){    		
    		alReconCommnents = alCommnetsList.iterator();	
			
			strValue.append(" <select name='cbo_reconcommnets");
			strValue.append(String.valueOf(intCount));	
			
			strValue.append("'> <option value = '0'>[Choose One] </option>");	
			while(alReconCommnents.hasNext()){
				hmKeyValueList = (HashMap) alReconCommnents.next();
				strOptKey = (String)hmKeyValueList.get("CODEID");
				strOptValue = (String)hmKeyValueList.get("CODENM");
				if (strReconCommnets.equals(strOptKey)){
					strOptKey += " selected"; 
				}
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");				
			}				
			strValue.append(" </select>");
			
    	}    	
       	return strValue.toString();     
    }   
    
    public String getRECON_COMMENT(){
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strReconCommnets = GmCommonClass.parseNull(String.valueOf(db.get("RECON_COMMENT")));
    	if(!strReconCommnets.equals("")){  
    		return strReconCommnets;  
    	}else{
    		return ""; 
    	}          
    }   
    
    public String getRECONCMTCNT(){
    	StringBuffer  strValue = new StringBuffer();    	
    	DynaBean db =    (DynaBean) this.getCurrentRowObject();    	
    	String strReconCount = GmCommonClass.parseNull(String.valueOf(db.get("RECONCMTCNT")));
    	String strRequestID = GmCommonClass.parseNull(String.valueOf(db.get("REQ_DTL_ID")));    
    	intCount++;
    	if(strReconCount.equals("Y")){  		
    		  return "<img id='imgEditA' style='cursor:hand' title='click to view commnets' src='/images/phone-icon_ans.gif'" +  
              " width='20' height='17'" + 
                  "onClick=\"javascript:fnOpenLog('" + strRequestID + "','"+4000316+"') \" />"; 
          }else{
              return "<img id='imgEditA' style='cursor:hand' title='click to enter commnets' src='/images/phone_icon.jpg'" +  
              " width='20' height='17' " + 
                  "onClick=\"javascript:fnOpenLog('" + strRequestID + "','"+4000316+"') \" />";
          }
    	 
    }    
}