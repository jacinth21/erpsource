package com.globus.operations.logistics.actions;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.operations.logistics.beans.GmLotTrackNonTissueRptBean;
import com.globus.operations.logistics.forms.GmLotTrackNonTissueRptForm;

/**
 * GmLotTrackNonTissueRptAction : Contains the methods used for the Lot Track
 * Report Non Tissue screen author : Agilan Singaravel
 */
public class GmLotTrackNonTissueRptAction extends GmDispatchAction {

	/** This used to get the instance of this class for enabling logging purpose */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * loadLotTrackNonTissueDetails - used to fetch lot track report for non
	 * tissue
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLotTrackNonTissueDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmLotTrackNonTissueRptForm gmLotTrackNonTissueRptForm = (GmLotTrackNonTissueRptForm) form;
		gmLotTrackNonTissueRptForm.loadSessionParameters(request);
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
		
		HashMap hmAccess = new HashMap();
		HashMap hmParam = GmCommonClass
				.getHashMapFromForm(gmLotTrackNonTissueRptForm);
		loadExpiryDate(gmLotTrackNonTissueRptForm, hmParam);

		// Get the warehouse and likes dropdown list
		loadDropDownValues(gmLotTrackNonTissueRptForm);
		String strPartyId=GmCommonClass.parseNull(gmLotTrackNonTissueRptForm.getSessPartyId());
		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "EDIT_LOT_DATA"));  
		String strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		log.debug("strAccessFlag==>" + strAccessFlag);
		gmLotTrackNonTissueRptForm.setStrAccess(strAccessFlag);
		return mapping.findForward("gmLotTrackNonTissueRpt");
	}

	/**
	 * fetchLotTrackNonTissueDetails - used to fetch lot track report for non
	 * tissue
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotTrackNonTissueDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmLotTrackNonTissueRptForm gmLotTrackNonTissueRptForm = (GmLotTrackNonTissueRptForm) form;
		gmLotTrackNonTissueRptForm.loadSessionParameters(request);
		GmLotTrackNonTissueRptBean gmLotTrackNonTissueRptBean = new GmLotTrackNonTissueRptBean(
				getGmDataStoreVO());

		String strOpt = "";
		String strJSONGridData = "";
		String strPartNumFormat = "";
		ArrayList alWareHouse = new ArrayList();
		ArrayList alPartSearch = new ArrayList();
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotTrackNonTissueRptForm);
		strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		// Get the warehouse and likes dropdown list
		loadDropDownValues(gmLotTrackNonTissueRptForm);

		if (strOpt.equals("Reload")) {// Should fetch the details only if the
										// Load button is pressed
			strJSONGridData = GmCommonClass
					.parseNull(gmLotTrackNonTissueRptBean
							.fetchLotTrackNonTissueDetails(hmParam));
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJSONGridData.equals("")) {
				gmLotTrackNonTissueRptForm.setGridXmlData(strJSONGridData);
				pw.write(strJSONGridData);
			}
			pw.flush();
			return null;

		}
		return mapping.findForward("gmLotTrackNonTissueRpt");
	}

	/**
	 * This method is used to load dropdown values for warehouse type and part
	 * search likes
	 * 
	 * @param gmLotTrackNonTissueRptForm
	 */
	private void loadDropDownValues(
			GmLotTrackNonTissueRptForm gmLotTrackNonTissueRptForm)
			throws AppError {

		ArrayList alWareHouse = new ArrayList();
		ArrayList alPartSearch = new ArrayList();

		alWareHouse = GmCommonClass.parseNullArrayList(GmCommonClass
				.getCodeList("PTTXY"));
		alPartSearch = GmCommonClass.parseNullArrayList(GmCommonClass
				.getCodeList("LIKES"));
		gmLotTrackNonTissueRptForm.setAlWarehouseType(alWareHouse);
		gmLotTrackNonTissueRptForm.setAlPartSearch(alPartSearch);
	}

	/**
	 * This method is used to load expiry date from and to values
	 * 
	 * @param gmLotTrackNonTissueRptForm
	 *            ,hmParam
	 */
	public void loadExpiryDate(
			GmLotTrackNonTissueRptForm gmLotTrackNonTissueRptForm,
			HashMap hmParam) throws AppError {

		String strDateRange = GmCommonClass.parseNull((String) hmParam
				.get("EXPDATERANGE"));
		String strDateFmt = GmCommonClass.parseNull((String) hmParam
				.get("APPLNDATEFMT"));
		String strToDate = "";
		String strFrmDate = "";

		Date today = new Date();
		DateFormat dateFormat = new SimpleDateFormat(strDateFmt);
		Calendar cal = Calendar.getInstance();
		if (strDateRange.equals("30Days")) {
			cal.add(Calendar.DAY_OF_MONTH, 30);
			strToDate = dateFormat.format(cal.getTime());
			strFrmDate = dateFormat.format(today);
		}
		if (strDateRange.equals("60Days")) {
			cal.add(Calendar.DAY_OF_MONTH, 31);
			strFrmDate = dateFormat.format(cal.getTime());
			cal.add(Calendar.DAY_OF_MONTH, 60);
			strToDate = dateFormat.format(cal.getTime());
		}
		if (strDateRange.equals("90Days")) {
			cal.add(Calendar.DAY_OF_MONTH, 61);
			strFrmDate = dateFormat.format(cal.getTime());
			cal.add(Calendar.DAY_OF_MONTH, 90);
			strToDate = dateFormat.format(cal.getTime());
		}
		gmLotTrackNonTissueRptForm.setExpiryDateFrom(strFrmDate);
		gmLotTrackNonTissueRptForm.setExpiryDateTo(strToDate);
	}

	/**
	 * addOrEditLotTrackDetails - used to Add/Edit the lot track details
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward addOrEditLotTrackDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		log.debug("addOrEditLotTrackDetails==> ");
		instantiate(request, response);
		GmLotTrackNonTissueRptForm gmLotTrackNonTissueRptForm = (GmLotTrackNonTissueRptForm) form;
		gmLotTrackNonTissueRptForm.loadSessionParameters(request);
		GmLotTrackNonTissueRptBean gmLotTrackNonTissueRptBean = new GmLotTrackNonTissueRptBean(
				getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
		GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

		ArrayList alWareHouse = new ArrayList();
		ArrayList alLotTrackDetail = new ArrayList();
		ArrayList alCompanyList = new ArrayList();
		ArrayList alPlantList = new ArrayList();
		ArrayList alFieldSales = new ArrayList();
		ArrayList alAccounts = new ArrayList();
		HashMap hmParam = new HashMap();
		String strSaveLot = "";
		String strOpt = "";
		String strCntrlNumInvId = "";
		
		String strDefalultCompany = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		String strDefalultPlant = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());
		
		alCompanyList = GmCommonClass.parseNullArrayList(gmCommonBean.getCompanyList());
		alPlantList = GmCommonClass.parseNullArrayList(gmCommonBean.getPlantList());	    
		gmLotTrackNonTissueRptForm.setAlCompanyList(alCompanyList);
		gmLotTrackNonTissueRptForm.setAlPlantList(alPlantList);
		
		String strCompanyID = GmCommonClass.parseNull(gmLotTrackNonTissueRptForm.getCompanyNm());
		String strPlantID = GmCommonClass.parseNull(gmLotTrackNonTissueRptForm.getPlantNm());
		String strXMLString = "";
		String StrAcctType = "";
		ArrayList alCompmanyPlantList = new ArrayList();
		HashMap hmResult = new HashMap();
		String strDefaultPlant = "";
		GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
		
		String strPartyId=GmCommonClass.parseNull(gmLotTrackNonTissueRptForm.getSessPartyId());
		hmParam = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "EDIT_LOT_DATA"));  
		log.debug("hmParam==>" + hmParam);	
		String strAccessFlag = GmCommonClass.parseNull((String) hmParam.get("UPDFL"));
		gmLotTrackNonTissueRptForm.setStrAccess(strAccessFlag);
		strCompanyID = strCompanyID.equals("") ? strDefalultCompany	: strCompanyID;
		strPlantID = strPlantID.equals("") ? strDefalultPlant : strPlantID;
		hmResult = gmCommonBean.fetchCompanyPlantList(strCompanyID);
		alCompmanyPlantList = (ArrayList) hmResult.get("PLANTLIST");
		strDefaultPlant = (String) hmResult.get("DEFAULTPLANT");
		hmParam = GmCommonClass.getHashMapFromForm(gmLotTrackNonTissueRptForm);
		log.debug("hmParam==>" + hmParam);
		strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		log.debug("strOpt==>" + strOpt);
		// to fetch plants 
		if (strOpt.equals("fetchCompanyPlants")) {
			strDefaultPlant = (String) hmResult.get("DEFAULTPLANT");
			strXMLString = GmCommonClass.parseNull(gmXMLParserUtility.createXMLString(alCompmanyPlantList));
			strXMLString =
					"<response>" + strXMLString + "<defplant>" + strDefaultPlant + "</defplant></response>";
			log.debug("The strXMLString **** " + strXMLString);
			response.setContentType("text/xml");
			PrintWriter pw = response.getWriter();
			if (alCompmanyPlantList.size() > 0) {
				pw.write(strXMLString);
			}
			pw.flush();
			return null;
		}
		gmLotTrackNonTissueRptForm.setAlPlantList(alCompmanyPlantList);
		if (!strCompanyID.equals("") && !strPlantID.equals("")) {
			gmLotTrackNonTissueRptForm.setCompanyNm(strCompanyID);
			gmLotTrackNonTissueRptForm.setPlantNm(strPlantID);
		} else {
			gmLotTrackNonTissueRptForm.setPlantNm(strDefalultPlant);
			gmLotTrackNonTissueRptForm.setCompanyNm(strDefalultCompany);
		}
		// Get the warehouse and likes dropdown list
		loadDropDownValues(gmLotTrackNonTissueRptForm);
		alWareHouse = GmCommonClass.parseNullArrayList(GmCommonClass
				.getCodeList("PTTXY"));
		hmParam.put("FILTER", "Active");
		
		gmLotTrackNonTissueRptForm.setAlWarehouseType(alWareHouse);
		alFieldSales = GmCommonClass.parseNullArrayList(gmLotTrackNonTissueRptBean.getDistributorList(hmParam));
		alAccounts = GmCommonClass.parseNullArrayList(gmLotTrackNonTissueRptBean.reportAccount(StrAcctType));
		gmLotTrackNonTissueRptForm.setAlAccounts(alAccounts);
		gmLotTrackNonTissueRptForm.setAlFieldSales(alFieldSales);
		strCntrlNumInvId = GmCommonClass.parseNull((String) hmParam.get("STRCNTRLNUMINVID"));
		if (strOpt.equals("fetch")) {
			alLotTrackDetail = 
					GmCommonClass.parseNullArrayList(gmLotTrackNonTissueRptBean.fetchLotTrackDetails(strCntrlNumInvId));
			log.debug("alLotTrackDetail==>" + alLotTrackDetail);
			gmLotTrackNonTissueRptForm.setAlLotTrackDetail(alLotTrackDetail);
			if (alLotTrackDetail.size() > 0) {
				HashMap hmReturn = new HashMap();
				hmReturn = (HashMap) alLotTrackDetail.get(0);
				request.setAttribute("HMRETURN", hmReturn);
			}

		} else if (strOpt.equals("save")) {
			hmParam = GmCommonClass
					.getHashMapFromForm(gmLotTrackNonTissueRptForm);
			gmLotTrackNonTissueRptBean.saveLotTrackDetails(hmParam);
		}
		return mapping.findForward("gmAddorEditLotTrackDetail");
	}
}
