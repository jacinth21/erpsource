/**
 * FileName : GmLoanerIncidentReportAction.java Description : Author : rshah Date : Apr 2, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.operations.logistics.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.forms.GmIncidentReportForm;

/**
 * @author rshah
 * 
 */
public class GmIncidentReportAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmIncidentReportForm gmIncidentReportForm = (GmIncidentReportForm) form;
    gmIncidentReportForm.loadSessionParameters(request);
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmInHouseSetsReportBean gmInHouseSetsReportBean =
        new GmInHouseSetsReportBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    Logger log = GmLogger.getInstance(this.getClass().getName());
    HttpSession session = request.getSession(false);
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    String hinputStringIncident = "";
    String strOpt = gmIncidentReportForm.getStrOpt();
    String strType = gmIncidentReportForm.getIncType();
    String strApplnDateFmt =
    		getGmDataStoreVO().getCmpdfmt();
    String strTodaysDate = gmCal.getCurrentDate(strApplnDateFmt);
    String strMonthBeginDate = "";
    log.debug("strOpt.............." + strOpt + "....strType..." + strType);
    int currMonth = 0;
    if (!strOpt.equals("load")) {
      currMonth = gmCal.getCurrentMonth() - 1;
      strMonthBeginDate =
          GmCommonClass.parseNull(gmCal.getAnyDateOfMonth(currMonth, 1, strApplnDateFmt));

      gmIncidentReportForm.setFromDate(strMonthBeginDate);
      gmIncidentReportForm.setToDate(strTodaysDate);
    }

    hmParam = GmCommonClass.getHashMapFromForm(gmIncidentReportForm);
    hinputStringIncident =
        GmCommonClass.createInputString(gmIncidentReportForm.getSelectedIncidents());

    if (strOpt.equals("load")) {
      hmParam.put("HINPUTSTRING", hinputStringIncident);
      hmParam.put("CODEGROUP", "ADDCH");
      hmReturn = gmInHouseSetsReportBean.fetchLoanerIncidentDetail(hmParam);
      request.setAttribute("hmReturn", hmReturn);
    }
    gmIncidentReportForm.setAlRepList(gmCust.getDistributorList("Active"));
    gmIncidentReportForm.setAlIncidentList(GmCommonClass.getAllCodeList("ADDCH"));
    gmIncidentReportForm.setAlRptByList(GmCommonClass.getCodeList("RPTBY"));
    return mapping.findForward("success");
  }

}
