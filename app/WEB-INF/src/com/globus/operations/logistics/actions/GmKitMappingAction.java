package com.globus.operations.logistics.actions;
import org.apache.log4j.Logger;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.actions.GmAction;
import com.globus.common.actions.GmDispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.operations.logistics.forms.GmKitMappingForm;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.corporate.beans.GmDivisionBean;
public class GmKitMappingAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward loadKitMapping(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmKitMappingForm gmKitMappingForm = (GmKitMappingForm) form;
		    gmKitMappingForm.loadSessionParameters(request);
		    GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO()); 
		    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
		   
		    String strSessCompanyLocale =
		            GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
		    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmKitMappingForm);
		    String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
			String strTagID =  GmCommonClass.parseNull(request.getParameter("TagId"));
			String strSetID =  GmCommonClass.parseNull(request.getParameter("setId"));
			String strKitId = GmCommonClass.parseNull(gmKitMappingForm.getKitId());
			hmParam.put("SETID",strSetID);
			hmParam.put("TAGID",strTagID);
	    	gmKitMappingForm.setSetId(strSetID);
	    	gmKitMappingForm.setTagId(strTagID);
		    HashMap hmValues = new HashMap();
		    String strSetName = "";
		    String strKitNm ="";
		    if(strOpt.equals("fetchContainer")){
		    	
		    	String strKitName = GmCommonClass.parseNull(gmKitMappingForm.getKitId());
		    	ArrayList alList = new ArrayList();
		    	alList=fetchKitNameDetails(gmKitMappingForm,strKitName);
		    	gmKitMappingForm.setAlList(alList);
		    	request.setAttribute("ALLIST",alList);
		    }
		    if(strOpt.equals("SearchKitName")){
		    	strKitNm = GmCommonClass.parseNull(request.getParameter("KitNm"));
		    	strKitNm = gmKitMappingBean.fetchKitName(strKitNm);
				gmKitMappingForm.setImage(strKitNm);
				response.setContentType("text/xml");
				 if ( !strKitNm.equals("")) {
			          PrintWriter pw = response.getWriter();
			          pw.write(strKitNm);
			          pw.flush();
			        }
			        return null;
		    }
		    if(strOpt.equals("searchSetDetails")){
		    	
		    	 if(!strSetID.equals("")){
		    		 gmKitMappingForm.setSetId(strSetID);
						hmValues = gmKitMappingBean.fetchSetDetails(strSetID);
						 gmKitMappingForm =
						          (GmKitMappingForm) GmCommonClass.getFormFromHashMap(gmKitMappingForm, hmValues);
						strSetName=GmCommonClass.parseNull((String) hmValues.get("SETDESC"));
						gmKitMappingForm.setSetNm(strSetName);
						request.setAttribute("SetId",strSetID);
						String strSetStatus=GmCommonClass.parseNull((String) hmValues.get("SETSTATUS"));
						String strInputString = strSetID + "^" + strSetName +"^" + strSetStatus;
						 response.setContentType("text/xml");
						 if ( !strSetID.equals("")) {
					          PrintWriter pw = response.getWriter();
					          pw.write(strInputString);
					          pw.flush();
					        }
					       return null;
					}
		    	 gmKitMappingForm.setSetId(strSetID);
		    }
		    if(strOpt.equals("searchTagNumber")){
		    	strSetID = GmCommonClass.parseNull(request.getParameter("setId"));
		    	strTagID = GmCommonClass.parseNull(request.getParameter("TagId"));
		    	strKitId = GmCommonClass.parseNull(request.getParameter("KitId"));
		    	hmParam.put("TAGID",strTagID);
		    	HashMap hmVal = gmKitMappingBean.fetchTagDetails(strTagID,strSetID,strKitId);
		    	String tagNum = GmCommonClass.parseNull((String)hmVal.get("TagNumber"));
		    	String setNum = GmCommonClass.parseNull((String)hmVal.get("setID"));
		    	String tagNumber = GmCommonClass.parseNull((String)hmVal.get("TagNum"));
		    	String strInputString =  tagNum + "^" + setNum + "^" + tagNumber;
		    	response.setContentType("text/xml");
				 if ( !strInputString.equals("")) {
			 		  PrintWriter pw = response.getWriter();
			          pw.write(strInputString);
			          pw.flush();
			        }
			        return null;
		    }
		    if(strOpt.equals("searchTagNum")){
		    	strSetID = GmCommonClass.parseNull(gmKitMappingForm.getSetId());
		    	strKitId = GmCommonClass.parseNull(request.getParameter("kitId"));
		    	hmParam.put("TAGID",strTagID);
		    	String strTagImg = gmKitMappingBean.fetchTagDetail(strTagID,strSetID,strKitId);
		    	response.setContentType("text/xml");
				 if ( !strTagImg.equals("")) {
			 		  PrintWriter pw = response.getWriter();
			          pw.write(strTagImg);
			          pw.flush();
			        }
			        return null;
		    }
		   
			if(strOpt.equals("save")){
				ArrayList alList = new ArrayList();
				String strSetId = GmCommonClass.parseNull(request.getParameter("setId"));
				String strTagId = GmCommonClass.parseNull(request.getParameter("tagId"));
				hmParam.put("SETID",strSetId);
				hmParam.put("TAGID",strTagId);
				hmValues = saveKitMapping(gmKitMappingForm,hmParam);
				String strKidID = GmCommonClass.parseNull((String)hmValues.get("KITID"));
				String strKidNm = GmCommonClass.parseNull((String)hmValues.get("KITNM"));
				strTagId = GmCommonClass.parseNull((String)hmValues.get("TAGID"));
				synKitNameList(strKidID,strKidID,strKitNm,"synKitNameList",GmCommonClass.parseNull(request.getParameter("companyInfo")));
				gmKitMappingForm.setAlLogReasons(gmCommonBean.getLog(strKidID, "26240460"));
				alList=fetchKitNameDetails(gmKitMappingForm,strKidID);
		    	gmKitMappingForm.setAlList(alList);
		    	gmKitMappingForm.setTagId(strTagId);
		    	request.setAttribute("ALLIST",alList);
			}
			
			if(strOpt.equals("RemoveSet")){	
				String strSetId = GmCommonClass.parseNull(request.getParameter("setId"));
				String strkitId = GmCommonClass.parseNull(request.getParameter("kitId"));
				String strTagId = GmCommonClass.parseNull(request.getParameter("tagId"));
				String strCount = removeKitMapping(strSetId,strkitId,strTagId);
				response.setContentType("text/xml");
				 if ( !strCount.equals("")) {
			 		  PrintWriter pw = response.getWriter();
			          pw.write(strCount);
			          pw.flush();
			        }
			        return null;
			}
		    return mapping.findForward("default");
		  }
   /**
	   * fetchKitNameDetails - To fetch the kit Name details
	   * 
	   * @param Form, String
	   * @return 
	   * @throws 
	*/
	private ArrayList fetchKitNameDetails(GmKitMappingForm gmKitMappingForm, String strKitName) {
			GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO()); 
		    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
		    ArrayList alList = new ArrayList();
		    	if (!strKitName.equals("")) {
		    		alList = gmKitMappingBean.fetchKitMapDtls(strKitName);
		    	}
		  
		    String strKitId = GmCommonClass.parseNull(gmKitMappingForm.getKitId());
		    gmKitMappingForm.setAlLogReasons(gmCommonBean.getLog(strKitId, "26240460"));
		    return alList;
		  }
    /**
		   * saveKitMapping - To save the kit mapping details
		   * 
		   * @param HashMap,Form
		   * @return HashMap
		   * @throws Exception
     */
		public HashMap saveKitMapping(GmKitMappingForm gmKitMappingForm, HashMap hmParam) throws Exception {

			GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
			HashMap hmValues = gmKitMappingBean.saveKitMapping(hmParam);
			String strKidID = GmCommonClass.parseNull((String)hmValues.get("KITID"));
			String strKidNm = GmCommonClass.parseNull((String)hmValues.get("KITNM"));
			gmKitMappingForm.setKitId(strKidID);
		  return 	 hmValues;
		}
		
	 /**
			   * removeKitMapping - To save the kit mapping details
			   * 
			   * @param String,String
			   * @return 
			   * @throws Exception
	*/
			public String removeKitMapping(String strSetid,String strKitid,String strTagid) throws Exception {
				GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
				String strCount = gmKitMappingBean.removeKitMapping(strSetid,strKitid,strTagid);
				return strCount;
			}	
	/**
		* synKitNameList - To save the kit name details to fetch from catch
		* @param String
		* @return 
		* @throws Exception
	*/
public void synKitNameList(String strkitId,String strKitnm,String strKitname,String strMethod ,String strCompanyInfo) throws Exception {
	
    GmAutoCompleteTransBean gmAutoCompleteTransBean = new GmAutoCompleteTransBean(getGmDataStoreVO());
    HashMap hmJmsParam = new HashMap();
    hmJmsParam.put("ID", strkitId);
    hmJmsParam.put("OLD_NM", strKitnm);
    hmJmsParam.put("NEW_NM", strKitname);
    hmJmsParam.put("METHOD", strMethod);
    hmJmsParam.put("companyInfo", strCompanyInfo);
    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
  }
}
