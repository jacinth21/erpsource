package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.logistics.forms.GmSetLogReportForm;

public class GmSetLogReportAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    // TODO Auto-generated method stub
    instantiate(request, response);
    GmSetLogReportForm gmSetLogReportForm = (GmSetLogReportForm) form;
    GmInHouseSetsReportBean gmInHouseSetsReportBean =
        new GmInHouseSetsReportBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    List alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    Logger log = GmLogger.getInstance(this.getClass().getName());
    HttpSession session = request.getSession(false);
    String strAction =
        (gmSetLogReportForm.getHaction().equals("") ? "" : gmSetLogReportForm.getHaction());
    String strApplnDateFmt =getGmDataStoreVO().getCmpdfmt();
        //GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    log.debug("strAction " + strAction);
    if (strAction.equals("")) {
    	GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strToDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
      String strFromDate = GmCalenderOperations.addMonths(-1, strApplnDateFmt);
      gmSetLogReportForm.setToDate(strToDate);
      gmSetLogReportForm.setFrmDate(strFromDate);
    }
    alReturn = GmCommonClass.getCodeList("SETTYP");
    gmSetLogReportForm.setAlType(alReturn);
    hmParam = GmCommonClass.getHashMapFromForm(gmSetLogReportForm);
    RowSetDynaClass rdResult = gmInHouseSetsReportBean.loadSetLog(hmParam);
    alResult = rdResult.getRows();
    gmSetLogReportForm.setLresult(alResult);
    return mapping.findForward("gmSetLogReport");
  }
}
