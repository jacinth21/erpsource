package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.logistics.beans.GmSetPropertyBean;
import com.globus.operations.logistics.forms.GmSetWeightForm;

import com.globus.common.beans.GmLogger; 

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger; 


public class GmSetWeightAction extends GmDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    public ActionForward setWeightReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) throws AppError ,Exception
    {   
    	
    	GmSetWeightForm gmSetWeightForm = (GmSetWeightForm) form;
    	gmSetWeightForm.loadSessionParameters(request);		
    	GmCommonClass gmCommonClass = new GmCommonClass();
		
    	HashMap hmParam = new HashMap();		
		hmParam = GmCommonClass.getHashMapFromForm(gmSetWeightForm);       
		
		GmSetPropertyBean gmSetPropertyBean = new GmSetPropertyBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		
		ArrayList alSetType = new ArrayList();
		RowSetDynaClass rdSetWeightReport = null;
        
        String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));      
        String strPartyId = GmCommonClass.parseNull((String) gmSetWeightForm.getSessPartyId());
        String strUpdateFl = "";
        HashMap hmAccess = new HashMap();
        hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "SET_WEIGHT")));
        strUpdateFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
        
        alSetType = GmCommonClass.parseNullArrayList((ArrayList) GmCommonClass.getCodeList("SETTY"));
        gmSetWeightForm.setAlSetType(alSetType);
        
        if(strOpt.equals("reload")){        	
        	rdSetWeightReport = gmSetPropertyBean.fetchSetWeightDtls(hmParam);
             gmSetWeightForm.setReturnReport(rdSetWeightReport.getRows());        
        }
        
        if(strOpt.equals("save")){
        	gmSetPropertyBean.saveSetWeight(hmParam);        	 
			String strSetID = GmCommonClass.parseNull((String)hmParam.get("SETID"));
			String strSetName = GmCommonClass.parseNull((String)hmParam.get("SETNM"));
			String strSetType = GmCommonClass.parseNull((String)hmParam.get("SETTYPE"));
			String strRedirectURL = "/gmSetWeight.do?method=setWeightReport&strOpt=reload&setId="+strSetID+"&setNm="+strSetName+"&setType="+strSetType;
			ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
			return actionRedirect;
        }
        gmSetWeightForm.setUpdatefl(strUpdateFl);
        return mapping.findForward("gmSetWeight");
    }   
}