package com.globus.operations.logistics.actions;

import javax.servlet.http.HttpServletRequest;    
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.util.GmTemplateUtil;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.actions.GmAction; 
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.logistics.beans.GmLoanerDashBoardBean;
import com.globus.operations.logistics.forms.GmLoanerDashBoardForm;

public class GmLoanerDashBoardAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**
	 * @description Load Loaner Dashboard
	 * @param actionMapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	  public ActionForward loadLoanerDash(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
	
		instantiate(request, response);
		
		ArrayList alReturn = new ArrayList();
		HashMap hmParam = new HashMap();
		HashMap hmResult = new HashMap();
		
		GmLoanerDashBoardForm gmLoanerDashBoardForm = (GmLoanerDashBoardForm) form;
		gmLoanerDashBoardForm.loadSessionParameters(request);
		GmLoanerDashBoardBean gmLoanerDashBoardBean = new GmLoanerDashBoardBean(getGmDataStoreVO());
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLoanerDashBoardForm));
		
		hmResult = GmCommonClass.parseNullHashMap(gmLoanerDashBoardBean.loadIndividualDash());
		log.debug("hmResult"+hmResult);
		request.setAttribute("HMRESULT", hmResult);
		
		return mapping.findForward("gmLoanerDashBoard");
	}
}
