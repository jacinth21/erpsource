package com.globus.operations.logistics.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.logistics.beans.GmLotDetailReportBean;
import com.globus.operations.logistics.forms.GmLotDetailReportForm;

/**
* GmLotDetailReportAction : Contains the methods used for the Lot Track Detail Report screen
* author : Agilan Singaravel
*/
public class GmLotDetailReportAction extends GmDispatchAction {
	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * loadLotDetailsReport - used to load lot track detail report screen when left link clicked
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLotDetailsReport(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		loadDropDownValues(gmLotDetailReportForm);
		return mapping.findForward("gmLotDetailRpt");
	}
	/**
	 * fetchLotDetailProdInfo - used to fetch product information based on part number or lot number
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotDetailProdInfo(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotDetailReportForm);
		
		strJSONGridData = GmCommonClass.parseNull(gmLotDetailReportBean.fetchLotDetailProdInfo(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}	
	/**
	 * fetchLotDetailReceivingInfo - used to fetch Receiving information based on part number or lot number
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotDetailReceivingInfo(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotDetailReportForm);
		
		strJSONGridData = GmCommonClass.parseNull(gmLotDetailReportBean.fetchLotDetailReceivingInfo(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}
	/**
	 * fetchLotDetailInventoryInfo - used to fetch inventory inhouse information based on part number or lot number
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotDetailInventoryInfo(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotDetailReportForm);
		
		strJSONGridData = GmCommonClass.parseNull(gmLotDetailReportBean.fetchLotDetailInventoryInfo(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}	
	/**
	 * fetchLotDetailFieldSalesInfo - used to fetch inventory field sales information based on part number or lot number
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotDetailFieldSalesInfo(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotDetailReportForm);
		
		strJSONGridData = GmCommonClass.parseNull(gmLotDetailReportBean.fetchLotDetailFieldSalesInfo(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}	
	/**
	 * fetchLotDetailConsignmentInfo - used to fetch inventory consignment information based on part number or lot number
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotDetailConsignmentInfo(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotDetailReportForm);
		
		strJSONGridData = GmCommonClass.parseNull(gmLotDetailReportBean.fetchLotDetailConsignmentInfo(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}
	/**
	 * fetchLotDetailLoanerInfo - used to fetch loaner information based on part number or lot number
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotDetailLoanerInfo(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotDetailReportForm);
		
		strJSONGridData = GmCommonClass.parseNull(gmLotDetailReportBean.fetchLotDetailLoanerInfo(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}
	
	/**
	 * loadLotTransHistory - used to load lot track transaction history screen 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLotTransHistory(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		loadDropDownValues(gmLotDetailReportForm);
		return mapping.findForward("gmLotTransHistory");
	}
	/**
	 * fetchLotTransHistory - used to fetch transaction history for field sales, consignment and loaner
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotTransHistory(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotDetailReportForm gmLotDetailReportForm = (GmLotDetailReportForm) form;
		gmLotDetailReportForm.loadSessionParameters(request);
		GmLotDetailReportBean gmLotDetailReportBean = new GmLotDetailReportBean(getGmDataStoreVO());
		
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmLotDetailReportForm);
	
		strJSONGridData = GmCommonClass.parseNull(gmLotDetailReportBean.fetchLotTransHistory(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			gmLotDetailReportForm.setGridDataTxnHisInfo(strJSONGridData);
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}

	/**
	 * This method is used to load dropdown values for warehouse type and part search likes
	 * 
	 * @param gmLotTrackTxnLogRptForm
	 */	
	private void loadDropDownValues(GmLotDetailReportForm gmLotDetailReportForm) throws AppError {

		ArrayList alPartSearch = new ArrayList();

		alPartSearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
		gmLotDetailReportForm.setAlPartSearch(alPartSearch);
	}
}
