package com.globus.operations.logistics.actions;
/**
 * FileName    : GmSetMappingAction.java 
 * Author      : Velu
 * Date        : Aug 2010 
 ** Copyright   : Globus Medical Inc
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import com.globus.operations.logistics.forms.GmLnCnSWAPLogForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.common.util.GmTemplateUtil;

public class GmLnCnSWAPLogAction extends GmAction {
	 public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
	        try {
	        	
	        	GmLnCnSWAPLogForm gmLnCnSWAPLogForm = (GmLnCnSWAPLogForm)form;
	        	gmLnCnSWAPLogForm.loadSessionParameters(request);
		        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
		        ArrayList alResult = null;
		        HashMap hResult = null;
		        HashMap hmParam = new HashMap();
		        String strGridXmlData = "";
		        RowSetDynaClass rowSetDynaClass = null;
		        
		        //getting to Consignment Details
				String strLoanerCnId=request.getParameter("loanerCnId");
				log.debug("loanerCnId::::::::"+strLoanerCnId);
				GmLogisticsBean gmLogisticsBean = new GmLogisticsBean();
				 
				rowSetDynaClass = gmLogisticsBean.fetchLNLogDetails(strLoanerCnId);
				//log.debug(rowSetDynaClass.getRows().size());
				alResult=(ArrayList)rowSetDynaClass.getRows();
				strGridXmlData = gmLogisticsBean.getXmlGridData(alResult);
				log.debug("strGridXmlData "+strGridXmlData);
				gmLnCnSWAPLogForm.setGridXmlData(strGridXmlData);
				
	        }catch(Exception exp){
	            exp.printStackTrace();
	            throw new AppError(exp);
	        }
	        return mapping.findForward("GmLnCnSWAPLog");
	    }
}

