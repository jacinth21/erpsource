package com.globus.operations.logistics.actions;

import java.io.IOException; 
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.inventory.beans.GmUserBuildingMappingBean;
import com.globus.operations.inventory.forms.GmInvLocationForm;
import com.globus.operations.logistics.beans.GmConsignmentLocationMapRptBean;
import com.globus.operations.logistics.forms.GmConsignmentLocationMapRptForm;

/**
 * @author rselvaraj
 *This class used to view WIP sets Print location details
 */
public class GmConsignmentLocationMapRptAction extends GmDispatchAction {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/** loadConsignmentLocMapDetails - this method used load the Location WIP set mapping Report screen 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 * @throws IOException
	 */

	public ActionForward loadConsignmentLocMapDetails(ActionMapping actionMapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) 
			throws AppError, IOException, Exception{

		instantiate(request,response);
		GmConsignmentLocationMapRptForm gmConsignmentLocationMapRptForm = (GmConsignmentLocationMapRptForm) form;
		gmConsignmentLocationMapRptForm.loadSessionParameters(request);
		//To load dropdown values
		loadDropdownValues(gmConsignmentLocationMapRptForm);

		return actionMapping.findForward("gmConsignmentLocationReport");	
	}

	
	/**
	 * loadDropdownValues: this method used load dropdown values for WIP set mapping Report screen
	 * 
	 * @exception AppError
	 */
	public void loadDropdownValues(GmConsignmentLocationMapRptForm gmConLocMapRptForm)throws AppError{

		// Bean Initalization
		GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
		GmUserBuildingMappingBean gmUserBuildingMappingBean = new GmUserBuildingMappingBean(getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
		HashMap hmAllDrpdwnValue = new HashMap();
		HashMap hmParams = new HashMap();
		String strWHRType = "";
		String strDeptId = "";
		String strCodeGrp = "LOCWT";
		String strPartyId = "";
		String strAllDrpdwnVal="";
		strDeptId = GmCommonClass.parseNull(gmConLocMapRptForm.getDeptId());
		strPartyId = GmCommonClass.parseNull(gmConLocMapRptForm.getSessPartyId());
		hmParams.put("TXNTYPE", strWHRType);
		hmParams.put("CODEGRP", strCodeGrp);
		hmParams.put("DEPTID", strDeptId);
		//if the Party id is mapped to the below security event then show all the ZONE and Type available in the Add New Location Screen
		hmAllDrpdwnValue =
				GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
						"ADD_LOC_ALL_DEPT"));
		strAllDrpdwnVal = GmCommonClass.parseNull((String) hmAllDrpdwnValue.get("UPDFL"));
		//if the strAllDrpdwnVal IS NOT EQUALS Y then need to show the dropdown based on the department.
		//if the strAllDrpdwnVal IS EQUALS Y then need to show the dropdown values available in the code group.
		if (!strAllDrpdwnVal.equalsIgnoreCase("Y")){
			log.debug("strAllDrpdwnVal 1 ==" + strAllDrpdwnVal);
			gmConLocMapRptForm.setAlZones(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOCZT",strDeptId)));
			gmConLocMapRptForm.setAlInvLocTypes(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOCTP",strDeptId)));
		}else{
			log.debug("strAllDrpdwnVal 2 ==" + strAllDrpdwnVal);
			gmConLocMapRptForm.setAlZones(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOCZT")));
			gmConLocMapRptForm.setAlInvLocTypes(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOCTP")));
		}
		gmConLocMapRptForm.setAlWareHouse(GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouseDtl(hmParams)));
		gmConLocMapRptForm.setAlBuilding(GmCommonClass.parseNullArrayList(gmUserBuildingMappingBean.fetchBuildingDetails())); 
		gmConLocMapRptForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOCSS",getGmDataStoreVO())));
	}

	
	/** fetchConsignmentLocMapDetails - this method used to fetch and return the Print location for WIP sets in JSON format  
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 * @throws IOException
	 */
	public ActionForward fetchConsignmentLocMapDetails(ActionMapping actionMapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) 
			throws AppError, IOException, Exception{

		instantiate(request,response);
		GmConsignmentLocationMapRptForm gmConsignmentLocationMapRptForm = (GmConsignmentLocationMapRptForm) form;
		gmConsignmentLocationMapRptForm.loadSessionParameters(request);
		GmConsignmentLocationMapRptBean gmConsignmentLocationMapRptBean = new GmConsignmentLocationMapRptBean(getGmDataStoreVO());

		HashMap hmParam =new HashMap();
		String strJSONWIPSetDtls="";

		//Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmConsignmentLocationMapRptForm);

		strJSONWIPSetDtls = gmConsignmentLocationMapRptBean.fetchConsignmentLocMapDetails(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONWIPSetDtls.equals("")) {
			pw.write(strJSONWIPSetDtls);
		}
		pw.flush();
		return null;
	}

	
	/** printInvLocation - this method used to fetch the locations for WIP sets  
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 * @throws IOException
	 */
	public ActionForward printInvLocation(ActionMapping actionMapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmConsignmentLocationMapRptForm gmConsignmentLocationMapRptForm = (GmConsignmentLocationMapRptForm) form;
		GmConsignmentLocationMapRptBean gmConsignmentLocationMapRptBean = new GmConsignmentLocationMapRptBean(getGmDataStoreVO());
		gmConsignmentLocationMapRptForm.loadSessionParameters(request);

		HashMap hmParams = new HashMap();
		ArrayList alInvLocList = new ArrayList();

		GmCommonClass gmCommonClass = new GmCommonClass();
		hmParams = GmCommonClass.getHashMapFromForm(gmConsignmentLocationMapRptForm);
		hmParams.put("PRINTLOC", "PRINTLOC");
		alInvLocList =
				GmCommonClass.parseNullArrayList(gmConsignmentLocationMapRptBean.printInvLocation(hmParams));
		gmConsignmentLocationMapRptForm.setAlInvLocList(alInvLocList);
		log.debug("alInvLocList=>"+alInvLocList);

		return actionMapping.findForward("Print");
	}

}
