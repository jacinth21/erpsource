package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmAutoCompletePartyListRptBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.logistics.beans.GmIHLoanerItemBean;
import com.globus.operations.logistics.forms.GmIHLoanerItemForm;
import com.globus.sales.event.beans.GmEventReportBean;

public class GmIHLoanerItemAction extends GmDispatchAction {
  HashMap hmValues = new HashMap();

  GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
  protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                                         // Logger Class.;

  public ActionForward loadPendingRtnIHReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
    
    GmIHLoanerItemForm gmIHLoanerItemForm = (GmIHLoanerItemForm) form;
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmIHLoanerItemBean gmIHLoanerItemBean = new GmIHLoanerItemBean(getGmDataStoreVO());
    GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    ArrayList alEventName = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alEmpList = new ArrayList();
    ArrayList alRepList = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmIHLoanerItemForm);
    alReturn = GmCommonClass.getCodeList("LONTP");
    gmIHLoanerItemForm.setAlType(alReturn);
    alEventName = GmCommonClass.parseNullArrayList(gmEventRptBean.fetchEventNameList());
    gmIHLoanerItemForm.setAlEventName(alEventName);
    gmIHLoanerItemForm.setAlPurEvent(GmCommonClass.parseNullArrayList(gmAutoCompleteReportBean
        .getCodeList("EVEPUR",null)));
    gmIHLoanerItemForm.setAlLoanTo(GmCommonClass.parseNullArrayList(gmAutoCompleteReportBean
        .getCodeList("EVELON",null)));
    int currMonth = 0;
    String strOpt = "";
    String strFromDT = "";
    String strToDT = "";
    String strApplDateFmt = "";
    String strxmlGridData = "";
    String strDtType = GmCommonClass.parseNull(request.getParameter("DTTYPE"));
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strFromDT = GmCommonClass.parseNull((String) hmParam.get("EVENTSTARTDATE"));
    strToDT = GmCommonClass.parseNull((String) hmParam.get("EVENTENDDATE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    currMonth = gmCal.getCurrentMonth() - 1;

    if (strFromDT.equals("")) {
      strFromDT = gmCal.getAnyDateOfMonth(currMonth, 1, strApplDateFmt);
    }
    if (strToDT.equals("")) {
      strToDT = GmCommonClass.parseNull(gmCal.getCurrentDate(strApplDateFmt));
    }
    if (strOpt.equals("Load")) {
      if (strDtType.equals("") && gmIHLoanerItemForm.getEventStartDate().equals("")
          && gmIHLoanerItemForm.getEventEndDate().equals("")) {
        gmIHLoanerItemForm.setEventStartDate(strFromDT);
        gmIHLoanerItemForm.setEventEndDate(strToDT);
        strDtType = "ESD";
        gmIHLoanerItemForm.setDtType(strDtType);
      }
    }
    if (strOpt.equals("Reload")) {
      alResult = gmIHLoanerItemBean.fetchPendingRtnIHLoanerItem(hmParam);
      hmReturn.put("INHOUSELOANERITEMS", alResult);
      hmReturn.put("APPLNDATEFMT", strApplDateFmt);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      strxmlGridData = generateOutPut(hmReturn);
      gmIHLoanerItemForm.setXmlGridData(strxmlGridData);
    }
    GmAutoCompletePartyListRptBean gmAutoCompletePartyListRptBean= new GmAutoCompletePartyListRptBean();
    String strFlag = "Y";
    ArrayList alOUSList = new ArrayList();
    HashMap hmData = new HashMap();
    hmData.put("USERTYPE","300");
    hmData.put("USERSTATUS","311");
    alRepList = gmAutoCompletePartyListRptBean.getUSRepList(strFlag);
    alEmpList = gmAutoCompletePartyListRptBean.getEmployeeList(hmData);
    alOUSList = gmAutoCompletePartyListRptBean.getOUSSalesList(strFlag);
    //alEmpList = gmLogonBean.getEmployeeList();
    gmIHLoanerItemForm.setAlOUSList(alOUSList);
    gmIHLoanerItemForm.setAlEmpList(alEmpList);

    //alRepList = gmCust.getRepList();
    gmIHLoanerItemForm.setAlRepList(alRepList);

    return mapping.findForward("gmPendingRtnIHItemRpt");
  }

  /**
   * generateOutPut xml content generation for In-House Loaner Item Details
   * 
   * @param HashMap
   * @return String
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessLocal = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("operations/logistics/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.Shipping.GmShippingReport", strSessLocal));
    templateUtil.setTemplateName("GmPendingRtnIHItemRpt.vm");
    return templateUtil.generateOutput();
  }
}
