package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.dashboard.beans.GmCustDashBoardBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.logistics.beans.GmInHouseSetsTransBean;
import com.globus.operations.logistics.beans.GmTicketBean;
import com.globus.operations.logistics.forms.GmLoanerReconciliationForm;
import com.globus.common.beans.GmCalenderOperations;

public class GmLoanerReconciliationAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      GmLoanerReconciliationForm gmLoanerReconciliationForm = (GmLoanerReconciliationForm) form;
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmInHouseSetsTransBean gmInHouseSetsTransBean =
          new GmInHouseSetsTransBean(getGmDataStoreVO());
      GmTicketBean gmTicketBean = new GmTicketBean(getGmDataStoreVO());
      GmCustDashBoardBean gmCustDashBoardBean = new GmCustDashBoardBean(getGmDataStoreVO());
      GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      ArrayList alReturn = new ArrayList();
      ArrayList alResult = new ArrayList();
      HashMap hmParam = new HashMap();
      HashMap hmDept = new HashMap();
      HashMap hmSalesFilters = new HashMap();
      gmLoanerReconciliationForm.loadSessionParameters(request);
      String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strTodaysDate = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strApplDateFmt));
      String strOpt =
          (gmLoanerReconciliationForm.getStrOpt().equals("") ? "Load" : gmLoanerReconciliationForm
              .getStrOpt());
      HttpSession session = request.getSession(false);
      /* set filter values */
      alReturn = GmCommonClass.getCodeList("LNRCST");
      gmLoanerReconciliationForm.setAlStatus(alReturn);
      alReturn = GmCommonClass.getCodeList("LNRTYP");
      gmLoanerReconciliationForm.setAlType(alReturn);
      alReturn = GmCommonClass.getCodeList("LNRCON");
      gmLoanerReconciliationForm.setAlQtyType(alReturn);
      String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
      gmLoanerReconciliationForm.setDeptId(strDeptId);
      hmDept.put("STRDEPT", strDeptId);
      String strTicketUrl = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("TICKET_URL"));
      hmDept.put("TICKETURL", strTicketUrl);
      String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
      hmDept.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      // String strAction = gmLoanerReconciliationForm.getHaction();
      String strRequestId = gmLoanerReconciliationForm.getRequestId();
      hmDept.put("REQUESTID", strRequestId);
      hmDept.put("DATEFORMAT", strApplDateFmt);
      // access filter
      String strAccessCondition = getAccessFilter(request, response);
      // The following Code is added for avoiding the Duplicate Field Sales Names
      HashMap hmAccessValues = new HashMap();
      hmAccessValues.put("CONDITION", strAccessCondition);
      hmAccessValues.put("ACTIVEDISTFL", "Y");
      hmAccessValues.put("STATUS", "Active");
      hmAccessValues.put("FLTR_DIST_BY_COMP","YES");//Filter Distributor Based on company
      hmSalesFilters = gmCommonBean.getSalesFilterLists(hmAccessValues);
      alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
      gmLoanerReconciliationForm.setAlDist(alReturn);
      alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
      gmLoanerReconciliationForm.setAlRepList(alReturn);
      /* set filter values */
      if (strOpt.equals("Load")) {
        // set default params
        if (gmLoanerReconciliationForm.getDtType().equals("")) {
          gmLoanerReconciliationForm.setDtType("RT");
        }
        if (gmLoanerReconciliationForm.getFromDt().equals("")
            && gmLoanerReconciliationForm.getToDt().equals("")) {
          gmLoanerReconciliationForm.setFromDt(strTodaysDate);
          gmLoanerReconciliationForm.setToDt(strTodaysDate);
        }
      }
      if (strOpt.equals("Email")) {
        gmLoanerReconciliationForm.getHreqids();
        String strCipherUserName = (String) request.getSession().getAttribute("strLogonName");
        GmJasperReport gmJasperReport = new GmJasperReport();
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        gmJasperReport.setJasperReportName("/GmEmailMissingParts.jasper");
        gmTicketBean.sendMissingPartsEmailOnRequest(gmLoanerReconciliationForm.getHreqids(),
            gmLoanerReconciliationForm.getUserId(), strCipherUserName, gmJasperReport);
      }
      hmParam = GmCommonClass.getHashMapFromForm(gmLoanerReconciliationForm);
      hmParam.put("AccessFilter", strAccessCondition);
      alResult = gmCustDashBoardBean.loadLoanerReconDashboard(hmParam);

      if (gmLoanerReconciliationForm.getStatus().equals("50821")) {// non-usage
        gmSearchCriteria.addSearchCriteria("INHID", "-", GmSearchCriteria.EQUALS);
        gmSearchCriteria.query(alResult);
      }
      gmLoanerReconciliationForm.setGridData(generateOutPut(alResult, hmDept));

      // log.debug("Loaner recon rpt o/p size is " + rdResult.getRows().size());
      // log.debug("Loaner recon rpt o/p size is " + alResult.size());
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }

  private String generateOutPut(ArrayList alResult, HashMap hmDept) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale = (String)hmDept.get("STRSESSCOMPANYLOCALE");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmDept", hmDept);
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName("GmLoanerReconTransReport.vm");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.common.dashboard.GmLoanerReconciliation",
            strSessCompanyLocale));
    return templateUtil.generateOutput();
  }
}
