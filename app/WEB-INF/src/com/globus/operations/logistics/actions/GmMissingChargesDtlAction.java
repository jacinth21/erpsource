package com.globus.operations.logistics.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.logistics.beans.GmChargesBean;
import com.globus.operations.logistics.forms.GmMissingChargesDtlForm;

public class GmMissingChargesDtlAction extends GmAction {
  HashMap hmValues = new HashMap();

  protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                                         // Logger Class.;


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    GmMissingChargesDtlForm gmMissingChargesDtlForm = (GmMissingChargesDtlForm) form;
    gmMissingChargesDtlForm.loadSessionParameters(request);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    ArrayList alDistList = new ArrayList();
    ArrayList alRepList = new ArrayList();
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strForward = "";
    String strOpt = "";
    String strFromDate = "";
    String strToDate = "";
    String strChkTicket = "";
    String strChkRepname = "";
    String strChkListPrice = "";
    String strChkConsignment = "";
    String strChkLastUpdBy = "";
    String strChkLastUpdDt = "";
    String strScreenType = "";
    String strRedirectURL = "";
    String strChargesFor = "";
    String strRequestID = "";
    String strFrom = "";
    String strTo = "";
    String strChkAssocRepname = "";
    
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmMissingChargesDtlForm);
    gDistTypeFromRequest(request, hmParam);
    GmChargesBean gmChargesBean = new GmChargesBean(getGmDataStoreVO());
    RowSetDynaClass rdDemandSheetReport = null;
    
    // The following code added for Getting the Distributor, Sales Rep and Employee List based on the PLANT too if the Selected Company is EDC
    String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid()); 
    String strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "MISSING_PART_HEADER")) ;
	  
     HashMap hmFilters = new HashMap();
     hmFilters.put("FILTER", "Active");
     if(strCompanyPlant.equals("Plant")){
	 hmFilters.put("DISTFILTER", "COMPANY-PLANT");
	 hmFilters.put("REPFILTER", "COMPANY-PLANT");
     }
     
    alRepList = gmCustomerBean.getRepList(hmFilters);
    alDistList = gmCustomerBean.getDistributorList(hmFilters);
    ArrayList alReconCommnetsList =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RECOMM"));
    gmMissingChargesDtlForm.setAlDistList(alDistList);
    gmMissingChargesDtlForm.setAlRepList(alRepList);
    gmMissingChargesDtlForm.setAlCommnetsList(alReconCommnetsList);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    strOpt = GmCommonClass.parseNull(gmMissingChargesDtlForm.getStrOpt());
    strScreenType = GmCommonClass.parseNull(gmMissingChargesDtlForm.getScreenType());
    if (strOpt.equals("load")) {
      strFromDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("From Date", "MISCHRGDT"));
      strToDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("To Date", "MISCHRGDT"));
      GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
      gmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      int intFromdate = Integer.parseInt(strFromDate);
      int intTodate = Integer.parseInt(strToDate);
      int intPrevMonth = gmCalenderOperations.getCurrentMonth() - 2;
      int intCurrMonth = gmCalenderOperations.getCurrentMonth() - 1;
      strFrom = gmCalenderOperations.getAnyDateOfMonth(intPrevMonth, intFromdate, strApplnDateFmt);
      strTo = gmCalenderOperations.getAnyDateOfMonth(intCurrMonth, intTodate, strApplnDateFmt);
      gmMissingChargesDtlForm.setFromDate(strFrom);
      gmMissingChargesDtlForm.setToDate(strTo);
    }
    GmSearchCriteria gmsearchCriteria = new GmSearchCriteria();
    ArrayList alReturn = GmCommonClass.getCodeList("CHGST");
    gmsearchCriteria.addSearchCriteria("CODENMALT", "50", GmSearchCriteria.NOTEQUALS);
    alReturn = (ArrayList) gmsearchCriteria.query(alReturn);
    gmMissingChargesDtlForm.setAlStatus(alReturn);
    HashMap hmSaveAccess = new HashMap();
    String strPartyId = "";
    String strsaveBtnAccess = "";
    strPartyId = gmMissingChargesDtlForm.getSessPartyId();
    hmSaveAccess =
        GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
            "SAVECHARGES")));
    strsaveBtnAccess = GmCommonClass.parseNull((String) hmSaveAccess.get("UPDFL"));
    request.setAttribute("SAVEUPDFL", strsaveBtnAccess);

    HashMap hmSaveChargeAccess = new HashMap();
    String strChargeBtnAccess = "";
    hmSaveChargeAccess =
        GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
            "CHARGEANDWRITEOFF")));
    strChargeBtnAccess = GmCommonClass.parseNull((String) hmSaveChargeAccess.get("UPDFL"));
    request.setAttribute("CHARGEUPDFL", strChargeBtnAccess);

    HashMap hmSaveCreditAccess = new HashMap();
    String strCreditBtnAccess = "";
    hmSaveCreditAccess =
        GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
            "SAVECREDIT")));
    strCreditBtnAccess = GmCommonClass.parseNull((String) hmSaveCreditAccess.get("UPDFL"));
    request.setAttribute("CREDITUPDFL", strCreditBtnAccess);

    if (strOpt.equals("Save") || strOpt.equals("Charge")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmMissingChargesDtlForm);
      gmChargesBean.saveMissingChargeDtls(hmParam);
      strForward = "gmLoadMissingChargesDetails";
    }

    if (strForward.equals("")) {
      strForward = "gmMissingChargesDetails";
    }

    if (strOpt.equals("LoadReport")) {
      rdDemandSheetReport = gmChargesBean.loadMissingChargeDtls(hmParam);
      strChargesFor = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChargesFor());
      strFromDate = GmCommonClass.parseNull(gmMissingChargesDtlForm.getFromDate());
      strToDate = GmCommonClass.parseNull(gmMissingChargesDtlForm.getToDate());
      strChkTicket = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_ticket());
      strChkRepname = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_repname());
      strChkAssocRepname = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_assocrepname());
      strChkListPrice = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_listprice());
      strChkConsignment = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_consignment());
      strChkLastUpdBy = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_lastupdby());
      strChkLastUpdDt = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_lastupddt());
      strRequestID = GmCommonClass.parseNull(gmMissingChargesDtlForm.getRequestID());
      gmMissingChargesDtlForm.setLdResult(rdDemandSheetReport.getRows());
      gmMissingChargesDtlForm.setFromDate(strFromDate);
      gmMissingChargesDtlForm.setToDate(strToDate);
    }
    if(strOpt.equals("sendLoanerLateFeeEmail")) {
        gmMissingChargesDtlForm.loadSessionParameters(request);
        String strInputString = GmCommonClass.parseNull(gmMissingChargesDtlForm.getStrInputString());
        String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
        String strJsonString = "";
        
        HashMap hmParams = new HashMap();
        hmParams.put("INPUTSTRING", strInputString);
        hmParams.put("COMPANYINFO", strCompanyInfo);
        
        gmChargesBean.sendLoanerLateFeeEmailJMS(hmParams);
        
        response.setContentType("text/json");
        PrintWriter pw = response.getWriter();
        if (!strJsonString.equals("")) {
        pw.write(strJsonString);
        }
        pw.flush();
      return null;   
    }

    request.setAttribute("CHK_TICKET", strChkTicket);
    request.setAttribute("CHK_REPNAME", strChkRepname);
    request.setAttribute("CHK_LSTPRICE", strChkListPrice);
    request.setAttribute("CHK_CONSIGNMENT", strChkConsignment);
    request.setAttribute("CHK_LSTUPDBY", strChkLastUpdBy);
    request.setAttribute("CHK_LSTUPDDT", strChkLastUpdDt);
    request.setAttribute("CHK_ASSOCREPNAME", strChkAssocRepname);

    return mapping.findForward(strForward);

  }

  private void gDistTypeFromRequest(HttpServletRequest request, HashMap hmParam) {
    String strDist = GmCommonClass.parseNull(request.getParameter("shwDist"));
    String shwDirRep = GmCommonClass.parseNull(request.getParameter("shwDirRep"));
    String strDistType = "";
    if (strDist.equals("true")) {
      strDistType += "1,70100|";
    }
    if (shwDirRep.equals("true")) {
      strDistType += "2,70101|";
    }
    hmParam.put("DISTTYPE", strDistType);
  }
  

}
