package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.logistics.beans.GmBackOrderReportBean;
import com.globus.operations.logistics.forms.GmBackOrderReportForm;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.actions.GmSalesDispatchAction;
import com.globus.sales.beans.GmSalesReportBean;

public class GmBackOrderReportAction extends GmDispatchAction {


  HashMap hmValues = new HashMap();
  protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                                         // Logger Class.;

  public ActionForward loadSetStatusReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      // log.debug("***************Step 3");
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmBackOrderReportForm gmBackOrderReportForm = (GmBackOrderReportForm) form;
      gmBackOrderReportForm.loadSessionParameters(request);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      HashMap hmParam = new HashMap();
      HashMap hmParams = new HashMap();
      RowSetDynaClass rdResult = null;
      String strOpt = gmBackOrderReportForm.getStrOpt();
      hmParam = GmCommonClass.getHashMapFromForm(gmBackOrderReportForm);
      String hconcheck = gmBackOrderReportForm.getHconstatusNotcheck();
      String hreqcheck = gmBackOrderReportForm.getHreqstatusNotcheck();
      if (hconcheck.equals("true"))
        gmBackOrderReportForm.setCheckSelectedCon(new String[] {});
      if (hreqcheck.equals("true"))
        gmBackOrderReportForm.setCheckSelectedReq(new String[] {});
      String strSelectedCon =
          GmCommonClass.createInputString(gmBackOrderReportForm.getCheckSelectedCon());
      String strSelectedReq =
          GmCommonClass.createInputString(gmBackOrderReportForm.getCheckSelectedReq());
      String strCtrlNumOperator = gmBackOrderReportForm.getOperator();
      hmParam.put("CTRLNUMOPER", strCtrlNumOperator);
      hmParam.put("INPUTCON", strSelectedCon);
      hmParam.put("INPUTREQ", strSelectedReq);
      String strPartNum = GmCommonClass.parseNull(gmBackOrderReportForm.getPartNum());
      String strSearch = GmCommonClass.parseNull(gmBackOrderReportForm.getSearch());
      hmParams.clear();
      hmParams.put("PARTNUM", strPartNum);
      hmParams.put("SEARCH", strSearch);
      String strPartNumFormat = GmCommonClass.createRegExpString(hmParams);
      hmParam.put("REGEXPATTERN", strPartNumFormat);
      rdResult = gmBackOrderReportBean.loadSetStatusReport(hmParam);
      List alResult = rdResult.getRows();
      gmBackOrderReportForm.setLdtResult(alResult);
      gmBackOrderReportForm.setAlConStatus(GmCommonClass.getCodeList("GSTAS"));// consignment status
      gmBackOrderReportForm.setAlReqStatus(GmCommonClass.getCodeList("RSTAS"));// request status
      gmBackOrderReportForm.setAlCtrlNumOpers(GmCommonClass.getCodeList("OPERS", "20515")); // #3241
      ArrayList alProject = new ArrayList();
      HashMap hmProj = new HashMap();
      hmProj.put("COLUMN", "ID");
      hmProj.put("STATUSID", "20301"); // Filter all approved projects
      alProject = gmProj.reportProject(hmProj);
      gmBackOrderReportForm.setAlSearch(GmCommonClass.getCodeList("LIKES"));// part like search
      gmBackOrderReportForm.setAlProject(alProject); // project list
      // gmLogisticsReportForm.setAlSetStatusReport(alReport);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }

  public ActionForward loadSetOverview(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strExcel = GmCommonClass.parseNull(request.getParameter("hExcel"));
    String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    try {
      instantiate(request, response);
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
      GmBackOrderReportForm gmBackOrderReportForm = (GmBackOrderReportForm) form;
      gmBackOrderReportForm.loadSessionParameters(request);
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      HashMap hmParam = new HashMap();
      HashMap hmResult = new HashMap();
      hmParam = GmCommonClass.getHashMapFromForm(gmBackOrderReportForm);
      String strSelectedSets = gmBackOrderReportForm.gethTxnId();
      hmParam.put("INPUTSETS", strSelectedSets);
      request.setAttribute("hAction", strAction);
      hmResult = gmBackOrderReportBean.loadSetOverview(hmParam);
      gmBackOrderReportForm.setHmCrossTabReport(hmResult);
      gmBackOrderReportForm.setAlSets(gmProj.loadSetMap("1601")); // set list
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    if (strExcel.equalsIgnoreCase("Excel")) {
      response.setContentType("application/vnd.ms-excel");
      response.setHeader("Content-disposition", "attachment;filename=Set_Overview.xls");
      return mapping.findForward("excell");
    }
    return mapping.findForward("setoverview");
  }

  public ActionForward loadBODHRReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      String strInputString = "";
      RowSetDynaClass rdResult = null;
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
      GmBackOrderReportForm gmBackOrderReportForm = (GmBackOrderReportForm) form;
      gmBackOrderReportForm.loadSessionParameters(request);
      HashMap hmTemp = new HashMap();
      hmTemp.put("COLUMN", "ID");
      hmTemp.put("STATUSID", "20301"); // Filter all approved projects
      gmBackOrderReportForm.setAlProject(gmProj.reportProject(hmTemp)); // set project list

      strInputString =
          GmCommonClass.createInputStringWithType(gmBackOrderReportForm.getCheckSelectedProjects(),
              "PROJECT");
      strInputString +=
          GmCommonClass.createInputStringWithType(gmBackOrderReportForm.getPartNum().split(","),
              "PART");

      hmTemp.put("INPUTSTRING", strInputString);

      if (!strInputString.equals("")) {
        rdResult = gmBackOrderReportBean.loadBODHRReoprt(strInputString);
        gmBackOrderReportForm.setLdhrPartResult(rdResult.getRows());
      }

      if (gmBackOrderReportForm.getFulfillBoFlag().equals("true")) {
        GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
        gmSearchCriteria.setSearchObject(GmSearchCriteria.DYNABEAN);
        // log.debug("testing");
        gmSearchCriteria.addSearchCriteria("SALES_BO_QTY", "0", GmSearchCriteria.GREATERTHAN);
        gmSearchCriteria.query(rdResult.getRows());
        // log.debug("The value:"+gmSearchCriteria.query(rdResult.getRows()));
        // log.debug("test1");
      }

    }

    catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

  public ActionForward loadPartFulfillReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      log.debug("***************inside here.. ");
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      ArrayList alResult = new ArrayList();
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
      GmBackOrderReportForm gmBackOrderReportForm = (GmBackOrderReportForm) form;
      gmBackOrderReportForm.loadSessionParameters(request);
      gmBackOrderReportForm.setAlSets(gmProj.loadSetMap("1601")); // set list
      gmBackOrderReportForm.setAlType(GmCommonClass.getCodeList("PFRLR")); // set type

      HashMap hmParam = new HashMap();
      hmParam = GmCommonClass.getHashMapFromForm(gmBackOrderReportForm);

      String strSetString = gmBackOrderReportForm.gethSetId();
      String strPartString =
          GmCommonClass.createInputStringWithType(gmBackOrderReportForm.getPartNum().split(","),
              "PART");
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      hmParam.put("SETSTRING", strSetString);
      hmParam.put("PARTSTRING", strPartString);
      if (!GmCommonClass.parseNull(strSetString).equals("")
          || !GmCommonClass.parseNull(strPartString).equals("")) {
        alResult = gmBackOrderReportBean.loadPartFulfillReport(hmParam);

        if (gmBackOrderReportForm.getFulfillQtyFlag().equals("true")) {
          GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
          gmSearchCriteria.addSearchCriteria("FULFILL_QTY", "0", GmSearchCriteria.GREATERTHAN);
          gmSearchCriteria.query(alResult);
        }

        gmBackOrderReportForm.setLdtResult(alResult);

        GmTemplateUtil templateUtil = new GmTemplateUtil();
        templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.operations.logistics.GmPartFulfillReport", strSessCompanyLocale));
        templateUtil.setDataList("alResult", alResult);
        templateUtil.setTemplateName("GmPartFulFillReport.vm");
        templateUtil.setTemplateSubDir("operations/logistics/templates");

        String strXmlString = templateUtil.generateOutput();
        log.debug(" XML STRING :   " + strXmlString);
        strXmlString = GmCommonClass.replaceForXML(strXmlString);
        request.setAttribute("strXml", strXmlString);
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);

    }
    return mapping.findForward("fulfill");
  }


  public ActionForward loadTrendReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
      GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
      GmSalesReportBean gmSales = new GmSalesReportBean(getGmDataStoreVO());
      GmBackOrderReportForm gmBackOrderReportForm = (GmBackOrderReportForm) form;
      gmBackOrderReportForm.loadSessionParameters(request);

      String strOpt = gmBackOrderReportForm.getStrOpt();
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      HashMap hmParam = new HashMap();

      HashMap hmResult = new HashMap();
      String strSalesGrpType = GmCommonClass.parseNull(request.getParameter("salesGrpType"));
      if (strOpt.equals("reload")) {

        hmParam = GmCommonClass.getHashMapFromForm(gmBackOrderReportForm);
        strSalesGrpType =
            strSalesGrpType.equals("50271") ? strSalesGrpType : GmCommonClass
                .parseNull((String) hmParam.get("SALESGRPTYPE"));
        hmParam.put("SALESGRPTYPE", strSalesGrpType);
        String strSelectedTrendcal =
            GmCommonClass.createInputStringWithType(gmBackOrderReportForm.getCheckSelectedTrend(),
                "TREND");
        hmParam.put("INPUTTRENDCAL", strSelectedTrendcal);
        hmResult = gmBackOrderReportBean.loadTrendReport(hmParam);
        HashMap hmMain = (HashMap) hmResult.get("Main");

        // log.debug(" Details  is  "+ hmMain.get("Details"));

        ArrayList alHeaders = (ArrayList) hmMain.get("Header");
        Iterator itrHeader = alHeaders.iterator();
        String strColumnName = "";
        String strQueryColumnName = "";

        if (gmBackOrderReportForm.getBackorderCheck().equals("on")) {
          ArrayList alDetails = (ArrayList) hmMain.get("Details");

          while (itrHeader.hasNext()) {
            strColumnName = (String) itrHeader.next();

            if (strColumnName.indexOf("-") != -1) {
              strQueryColumnName = strColumnName;
            }
          }
          gmSearchCriteria.addSearchCriteria(strQueryColumnName, "0", GmSearchCriteria.LESSTHAN);
          // log.debug("alDetails: "+alDetails);
          Iterator alDetailsItr = alDetails.iterator();
          while (alDetailsItr.hasNext()) {
            HashMap hmMap = (HashMap) alDetailsItr.next();
            String strValue = GmCommonClass.parseNull((String) hmMap.get("31 - 45"));
            if (strValue.equals("null")) {
              // log.debug("hmMap: "+ hmMap);
            }
            // //log.debug("ID: "+hmMap.get("ID")+ "Value: " + hmMap.get("31 - 45") );
          }
          gmSearchCriteria.query(alDetails);
        }


      }
      gmBackOrderReportForm.setHmCrossTabReport(hmResult);

      gmBackOrderReportForm.setAlTrendStatus(GmCommonClass.getCodeList("DMDTP"));// Trend status,
                                                                                 // calculate for
      gmBackOrderReportForm.setAlTrendType(GmCommonClass.getCodeList("TRETP"));// Trend Type

      ArrayList alProject = new ArrayList();
      alProject = gmSales.loadProjectGroup();

      gmBackOrderReportForm.setAlSalesGrpType(GmCommonClass.getCodeList("SGTYP"));// search type
      gmBackOrderReportForm.setAlTrendDuration(GmCommonClass.getCodeList("TREDR"));
      gmBackOrderReportForm.setAlSalesGrpId(alProject); // project list

      String strExcel = GmCommonClass.parseNull(request.getParameter("hExcel"));

      // Trend Report excel Download.
      if (strExcel.equalsIgnoreCase("Excel")) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=TrendExcelReport.xls");
        return mapping.findForward("ExcelTrendReport");
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("trend");
  }

  /**
   * loadBackOrderReport This method used for creating/saving the Incident Information.
   * 
   * @exception Exception
   */
  public ActionForward loadBackOrderReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    HashMap hmCurrency = new HashMap();
    instantiate(request, response);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    GmBackOrderReportForm gmBackOrderReportForm = (GmBackOrderReportForm) form;
    gmBackOrderReportForm.loadSessionParameters(request);
    GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCommonClass gmCommonClass = new GmCommonClass();
    GmBackOrderReportBean gmBackOrderReportBean = new GmBackOrderReportBean(getGmDataStoreVO());
    GmCalenderOperations gmCal = new GmCalenderOperations();
    gmCal.setTimeZone(getGmDataStoreVO().getCmptzone());
    String strTodaysDate = gmCal.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strAccCurrId = gmBackOrderReportForm.getAccCurrId();
    ArrayList alDistTypes = GmCommonClass.parseNullArrayList(gmCustomerBean.getRepList("")); // Get
                                                                                             // Rep
                                                                                             // list
                                                                                             // to
                                                                                             // dropdown
    gmBackOrderReportForm.setAlDistributorType(alDistTypes);
    ArrayList alEmailType = gmCommonClass.getCodeList("BOMAIL"); // Get Email status to dropdown
    gmBackOrderReportForm.setAlEmail(alEmailType);

    // when loading from left link account currency id is set to company defualt txn currencyid
    if (strAccCurrId.equals("")) {
      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      strAccCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
      gmBackOrderReportForm.setAccCurrId(strAccCurrId);
    }
    gmBackOrderReportForm.setAccCurrSymb(GmCommonClass.getCodeNameFromCodeId(strAccCurrId));

    gmBackOrderReportForm.setAlCompCurr(gmCompanyBean.fetchCompCurrMap());


    HashMap hmParam = new HashMap();
    ArrayList alBODetails = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmBackOrderReportForm);
    String strOpt = GmCommonClass.parseNull(gmBackOrderReportForm.getStrOpt());

    if (strOpt.equals("save")) {
      gmBackOrderReportBean.sendBackOrderEmail(hmParam);
      strOpt = "load";
    }

    if (strOpt.equals("load")) {
      alBODetails = gmBackOrderReportBean.loadBackOrderEmail(hmParam);
      gmBackOrderReportForm.setAlBODetails(alBODetails);
      gmBackOrderReportForm.setStrOpt(strOpt);
    }
    if (gmBackOrderReportForm.getStrFromDate().equals("")
        && gmBackOrderReportForm.getStrToDate().equals("") && !strOpt.equals("save")
        && !strOpt.equals("load")) {
      gmBackOrderReportForm.setStrFromDate(strTodaysDate);
      gmBackOrderReportForm.setStrToDate(strTodaysDate);
    }
    if (gmBackOrderReportForm.getEmailType().equals("")) {
      gmBackOrderReportForm.setEmailType("104760");
    }
    return mapping.findForward("gmSalesBackOrderMail");
  }

  /**
   * generateOutPut xml content generation for Set Cost Report grid
   * 
   * @param ArrayList
   * @return String
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName("GmSalesBackOrderEmail.vm");
    return templateUtil.generateOutput();
  }

}
