package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.logistics.beans.GmLoanerRequestReportBean;
import com.globus.operations.logistics.forms.GmLoanerRequestReportForm;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmLoanerRequestReportAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * loadLoanerRequest- load loaner request details
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadLoanerRequest(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLoanerRequestReportForm gmLoanerRequestReportForm = (GmLoanerRequestReportForm) form;
    gmLoanerRequestReportForm.loadSessionParameters(request);
    GmLoanerRequestReportBean gmLoanerRequestReportBean =
        new GmLoanerRequestReportBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    String strCompanyId = getGmDataStoreVO().getCmpid();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerRequestReportForm);

    String strEsclAccessFl = "";
    String strEsclRemAccessFl = "";
    String strXmlString = "";
    boolean showGrid=true;

    ArrayList alReport = new ArrayList();
    int alSize = 0;

    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strEscAccFl = GmCommonClass.parseNull((String) hmParam.get("hEscFl"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("HSTATUS"));

    String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strShowEsc =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_ESCALATION"));

    // if(strOpt.equals("Load")){
    strEsclAccessFl = gmEventSetupBean.getEventAccess(strUserId, "EVENT", "ESCALATION_ACCESS");
    strEsclRemAccessFl = gmEventSetupBean.getEventAccess(strUserId, "EVENT", "ESC_REMOVE_ACCESS");
    gmLoanerRequestReportForm.setStrEsclAccessFl(strEsclAccessFl);
    gmLoanerRequestReportForm.setStrEsclRemAccessFl(strEsclRemAccessFl);
    // }

    hmParam = loadDropdownValues(form);
    String strDtType = GmCommonClass.parseNull((String) hmParam.get("DTTYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    if (strDtType.equals("") && strFromDt.equals("") && strToDt.equals("")) {
      strFromDt = strTodaysDate;
      strToDt = strTodaysDate;
      strDtType = "108703";
    }
    if (strStatus.equals("")) {
      strStatus = "10,20";
    }
      hmParam.put("DTTYPE", strDtType);
      hmParam.put("FROMDT", strFromDt);
      hmParam.put("FROMDT", strFromDt);
      hmParam.put("TODT", strToDt);
      hmParam.put("STATUS", strStatus);
      alReport = gmLoanerRequestReportBean.reportLoanerOpenRequests(hmParam);
      alSize = alReport.size();
         
      gmLoanerRequestReportForm.setAlLoanerRequest(alReport);
      gmLoanerRequestReportForm.setAlSize(alSize);

      hmParam.put("ESCLFL", strEsclAccessFl);
      hmParam.put("SHOWESCL", strShowEsc);
      hmParam.put("REMESCFL", strEsclRemAccessFl);

      strXmlString = gmLoanerRequestReportBean.getXmlGridData(alReport, hmParam);
      if(strXmlString!=null && strXmlString.indexOf("cell")==-1){
        showGrid = false;            
      }
      gmLoanerRequestReportForm.setGridXmlData(strXmlString);
      gmLoanerRequestReportForm.setShowGrid(showGrid);
      gmLoanerRequestReportForm.sethStatus(strStatus);
      gmLoanerRequestReportForm.setDtType(strDtType);
      gmLoanerRequestReportForm.setFromDt(strFromDt);
      gmLoanerRequestReportForm.setToDt(strToDt);
    return mapping.findForward("gmLoanerRequests");
  }

  /**
   * updateLoanerEscalation- update escalation flag details
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward updateLoanerEscalation(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLoanerRequestReportForm gmLoanerRequestReportForm = (GmLoanerRequestReportForm) form;
    GmLoanerRequestReportBean gmLoanerRequestReportBean =
        new GmLoanerRequestReportBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();

    gmLoanerRequestReportForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerRequestReportForm);

    gmLoanerRequestReportBean.updateLoanerEscalation(hmParam);
    // gmLoanerRequestReportBean.saveLogDetails(hmParam);
    log.debug("hmParam======================== " + hmParam);

    return mapping.findForward("gmLoanerRequests");
  }

  /**
   * loadDropdownValues- fetch dropdown details
   * 
   * @param mapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public HashMap loadDropdownValues (ActionForm form) throws AppError, Exception {

    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmLoanerRequestReportForm gmLoanerRequestReportForm = (GmLoanerRequestReportForm) form;
    
    String strCompPlantFilter = "";
    String strCompanyID = "";

    ArrayList alDist = new ArrayList();
    ArrayList alSet = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alDtType = new ArrayList();
    ArrayList alReqType = new ArrayList();
    HashMap hmFilter = new HashMap();
    HashMap hmParam = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerRequestReportForm);
    
    strCompanyID = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyID, "LOAN_PROCESS_HEADER"));

    hmFilter.put("FILTER", "Active");
    if (strCompPlantFilter.equals("Plant")) {
      hmFilter.put("DISTFILTER", "COMPANY-PLANT");
    }
    alDist = gmCust.getDistributorList(hmFilter);

    alSet = gmProjectBean.loadSetMap("1600");
    alStatus = GmCommonClass.getCodeList("RQDTST");
    alDtType = GmCommonClass.getCodeList("LNDTTY");
    alReqType = GmCommonClass.getCodeList("LRQACT");
    gmLoanerRequestReportForm.setAlDistributor(alDist);
    gmLoanerRequestReportForm.setAlType(alDtType);
    gmLoanerRequestReportForm.setAlReqType(alReqType);
    gmLoanerRequestReportForm.setAlStatus(alStatus);

    return hmParam;
  }
}
