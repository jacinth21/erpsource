package com.globus.operations.logistics.actions;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;





import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.logistics.beans.GmChargesBean;
import com.globus.operations.logistics.beans.GmLoanerLateFeeBean;
import com.globus.operations.logistics.beans.GmLoanerLateFeeHREmailBean;
import com.globus.operations.logistics.forms.GmChargesApprovalForm;
import com.globus.operations.logistics.forms.GmLoanerLateFeeApprovalForm;
import com.globus.sales.Loaners.beans.GmLoanerExtBean;


public class GmLoanerLateFeeApprovalAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
	
	/**
	 * @description Load the access and datas for the screen
	 * @param actionMapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	  public ActionForward loadLateFeeDashboard(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
		 
		 instantiate(request, response);
		    GmLoanerLateFeeBean gmLoanerLateFeeBean= new GmLoanerLateFeeBean(getGmDataStoreVO());
		    GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm = (GmLoanerLateFeeApprovalForm) form;
		    GmLoanerExtBean gmLoanerExtBean = new GmLoanerExtBean(getGmDataStoreVO());
		    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
		    gmLoanerLateFeeApprovalForm.loadSessionParameters(request);
		    
		    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmLoanerLateFeeApprovalForm);

		    ArrayList alDivisionList = new ArrayList();
		    ArrayList alWaiveReason = new ArrayList();
		    ArrayList alReturn = new ArrayList();
			HashMap hmAccess = new HashMap();
			ArrayList alStatusList = new ArrayList();
			ArrayList alReasonList = new ArrayList();
			HashMap hmLoop = new HashMap();
			HashMap hmLoopReason = new HashMap();
		    alReturn = GmCommonClass.getCodeList("FEE_ST");
		    alWaiveReason = GmCommonClass.getCodeList("WIVRSN");
		    gmLoanerLateFeeApprovalForm.setAlStatus(alReturn);
		    gmLoanerLateFeeApprovalForm.setAlWaiveReason(alWaiveReason);
		    String strStatus = "";
		    String strApplnDateFmt = "";
		    String strFromDate = "";
		    String strToDate = "";
		    String strFrom = "";
		    String strTo = "";
		    String strstatuscodeid = "";
		    String strStatusflg = "";
		    String strPartyId = "";
		    int Fromdate;
		    int Todate;
		    int prevMonth;
		    int currMonth;
		    int intDiv = 0;
		    int intReason = 0;
		    String strPOHtml = "";
		    String strReasonHtml = "";
		    String strCompId = "";
		    
		    // now fetch the status dropdown values for the displaytag rows
		    
//		    //for setting default date in jsp
//		      strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
//		      strFromDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("From Date", "MISCHRGDT"));
//		      strToDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("To Date", "MISCHRGDT"));
//		      GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
//		      Fromdate = Integer.parseInt(strFromDate);
//		      Todate = Integer.parseInt(strToDate);
//		      prevMonth = gmCalenderOperations.getCurrentMonth() - 2;
//		      currMonth = gmCalenderOperations.getCurrentMonth() - 1;
//		      strFrom = gmCalenderOperations.getAnyDateOfMonth(prevMonth, Fromdate, strApplnDateFmt);
//		      strTo = gmCalenderOperations.getAnyDateOfMonth(currMonth, Todate, strApplnDateFmt);
//		      gmLoanerLateFeeApprovalForm.setFromDate(strFrom);
//		      gmLoanerLateFeeApprovalForm.setToDate(strTo);
		    
		  //to get the partyid SESSPARTYID
			alReturn = GmCommonClass.getCodeList("FEE_ST");
			strPartyId = gmLoanerLateFeeApprovalForm.getSessPartyId();
			fetchLoanerLateSecurityEvents(strPartyId,gmLoanerLateFeeApprovalForm);
			//To form the status dropdown array formation as string and set that value to form variable
				alStatusList = gmLoanerLateFeeApprovalForm.getAlStatus();
				alReasonList = gmLoanerLateFeeApprovalForm.getAlWaiveReason();
				intDiv = alStatusList.size();
				hmLoop = new HashMap();
				if (intDiv > 0) {
					for (int i = 0; i < intDiv; i++) {
						hmLoop = (HashMap) alStatusList.get(i);
						strPOHtml += "statusArr[" + i + "] = new Array(\""
								+ hmLoop.get("CODEID") + "\",\""
								+ hmLoop.get("CODENM") + "\");";
					}
				}
				
				intReason = alReasonList.size();
				hmLoopReason = new HashMap();
				if (intReason > 0) {
					for (int i = 0; i < intReason; i++) {
						hmLoopReason = (HashMap) alReasonList.get(i);
						strReasonHtml += "reasonArr[" + i + "] = new Array(\""
								+ hmLoopReason.get("CODEID") + "\",\""
								+ hmLoopReason.get("CODENM") + "\");";
					}
				}
				gmLoanerLateFeeApprovalForm.setStrPOHtml(GmCommonClass.parseNull(strPOHtml));
				gmLoanerLateFeeApprovalForm.setStrReasonHtml(GmCommonClass.parseNull(strReasonHtml));
				
				strCompId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
				alDivisionList = GmCommonClass.parseNullArrayList(gmLoanerLateFeeBean.fetchDivision(strCompId));
				gmLoanerLateFeeApprovalForm.setAlZone(gmLoanerLateFeeBean.getZone(strCompId));
				gmLoanerLateFeeApprovalForm.setAlRegions(gmLoanerExtBean.getRegion(strCompId));
				gmLoanerLateFeeApprovalForm.setAlDivision(alDivisionList);
				

		 return mapping.findForward("gmLateFeeCharges");
	 }
	  
	  /**
	   * @description fetch loaner late fee charge details based on the filters
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward FetchLateFeeCharges(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		    GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm = (GmLoanerLateFeeApprovalForm) form;
		    gmLoanerLateFeeApprovalForm.loadSessionParameters(request);
		    GmLoanerLateFeeBean gmLoanerLateFeeBean= new GmLoanerLateFeeBean(getGmDataStoreVO());
		  //Initialize
			HashMap hmParam = new HashMap(); 
			HashMap hmAccess = new HashMap();
			ArrayList alReturn = new ArrayList();
			String strJsonString = "";
			hmParam = GmCommonClass.getHashMapFromForm(gmLoanerLateFeeApprovalForm);
			
			strJsonString = GmCommonClass.parseNull(gmLoanerLateFeeBean.fetchChargesLateFeeBySetReport(hmParam));
			//to get the partyid SESSPARTYID
			alReturn = GmCommonClass.getCodeList("FEE_ST");
	    	response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonString.equals("")) {
				pw.write(strJsonString);
			}
			pw.flush();
		    return null;
	  }
	  
	  /**
	   * @description save tha late fee amount while tab out from grid
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward saveLoanerLateFeeAmount(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		  try {
		    GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm = (GmLoanerLateFeeApprovalForm) form;
		    gmLoanerLateFeeApprovalForm.loadSessionParameters(request);
		    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmLoanerLateFeeApprovalForm);
		    GmLoanerLateFeeBean gmLoanerLateFeeBean= new GmLoanerLateFeeBean(getGmDataStoreVO());
		    gmLoanerLateFeeBean.saveLoanerLateFeeAmount(hmParam);
			response.setContentType("text/xml");
			response.setStatus(200);
			PrintWriter pw = response.getWriter();
			pw.flush();
			}catch(Exception e){
			PrintWriter pWriter = response.getWriter();
			response.setStatus(300);
			response.setContentType("text/xml");
			pWriter.write(e.getMessage());
			pWriter.flush();
			}
	    	return null;
	  }
	  
	  /**
	   * @description save tha late fee comment while tab out from grid
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward saveLoanerLateFeeComment(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		  try {
		    GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm = (GmLoanerLateFeeApprovalForm) form;
		    gmLoanerLateFeeApprovalForm.loadSessionParameters(request);
		    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmLoanerLateFeeApprovalForm);
		    GmLoanerLateFeeBean gmLoanerLateFeeBean= new GmLoanerLateFeeBean(getGmDataStoreVO());
			gmLoanerLateFeeBean.saveLoanerLateFeeComment(hmParam);
			response.setContentType("text/xml");
			response.setStatus(200);
			PrintWriter pw = response.getWriter();
			pw.flush();
			}catch(Exception e){
			PrintWriter pWriter = response.getWriter();
			response.setStatus(300);
			response.setContentType("text/xml");
			pWriter.write(e.getMessage());
			pWriter.flush();
			}
	    	return null;
	  }

	  
	  /**
	   * @description fetch row details while click on the credit button from grid
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward fetchCreditAmount(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		    HashMap hmResParam = new HashMap();
		    GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm = (GmLoanerLateFeeApprovalForm) form;
		    gmLoanerLateFeeApprovalForm.loadSessionParameters(request);
		    GmLoanerLateFeeBean gmLoanerLateFeeBean = new GmLoanerLateFeeBean(getGmDataStoreVO());
		    hmResParam = gmLoanerLateFeeBean.fetchCreditAmountDetail(gmLoanerLateFeeApprovalForm.getChargeDetailId());
		    
			gmLoanerLateFeeApprovalForm.setCreditAmount((String)hmResParam.get("CREDIT"));
			gmLoanerLateFeeApprovalForm.setAmount((String)hmResParam.get("AMOUNT"));
			gmLoanerLateFeeApprovalForm.setStrAvailAmount((String)hmResParam.get("AVAILAMOUNT"));
		  //Initialize
			HashMap hmParam = new HashMap();   
			hmParam = GmCommonClass.getHashMapFromForm(gmLoanerLateFeeApprovalForm);
			
			return mapping.findForward("gmCreditCharges");
	  }
	  
	  /**
	   * @description save the credit details from window opener
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward saveLoanerLateFeeCreditAmount(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		  try {
		    GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm = (GmLoanerLateFeeApprovalForm) form;
		    gmLoanerLateFeeApprovalForm.loadSessionParameters(request);
		    GmLoanerLateFeeBean gmLoanerLateFeeBean= new GmLoanerLateFeeBean(getGmDataStoreVO());
		    GmLoanerLateFeeApprovalAction gmLoanerLateFeeApprovalAction =new GmLoanerLateFeeApprovalAction();
		  //Initialize
			HashMap hmParam = new HashMap();   
			hmParam = GmCommonClass.getHashMapFromForm(gmLoanerLateFeeApprovalForm);
			gmLoanerLateFeeBean.saveLoanerLateFeeCreditAmount(hmParam);
			gmLoanerLateFeeApprovalAction.loadLateFeeDashboard(mapping,form,request,response);
			String strJsonString = "";
	    	response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonString.equals("")) {
				pw.write(strJsonString);
			}
			pw.flush();
		  }catch(Exception e){
			  PrintWriter pWriter = response.getWriter();
				response.setStatus(300);
				response.setContentType("text/xml");
				pWriter.write(e.getMessage());
				pWriter.flush();
				}
	    	return null;
	  }
	  
	  /**
	   * @description save the status what are the rows changed
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward saveLoanerLateFeeStatus(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		  instantiate(request, response);
		    GmLoanerLateFeeApprovalForm gmLateFeeChargesForm = (GmLoanerLateFeeApprovalForm) form;
		    gmLateFeeChargesForm.loadSessionParameters(request);
		    GmLoanerLateFeeBean gmLoanerLateFeeBean= new GmLoanerLateFeeBean(getGmDataStoreVO());
		  //Initialize
			HashMap hmParam = new HashMap();   
			hmParam = GmCommonClass.getHashMapFromForm(gmLateFeeChargesForm);
			String strInputString = GmCommonClass.parseNull(request.getParameter("inputString"));
		  		    
		    hmParam.put("INPUTSTRING", strInputString);
		   
			gmLoanerLateFeeBean.saveLoanerLateFeeStatus(hmParam);
			
			String strJsonString = "";
	    	response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonString.equals("")) {
				pw.write(strJsonString);
			}
			pw.flush();

	    	return null;
	  }
	  
	  /**
	   * @description fetch the security event
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   * @author ASophia
	   */
	  public void fetchLoanerLateSecurityEvents(String strPartyId, GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm) throws Exception {
		    GmAccessControlBean gmAccessControlBean= new GmAccessControlBean(getGmDataStoreVO());
		    
		    //GmLoanerLateFeeApprovalForm gmLoanerLateFeeApprovalForm = (GmLoanerLateFeeApprovalForm) form;
			HashMap hmAccessAmt = new HashMap(); 
			HashMap hmAccessStatus = new HashMap(); 
			HashMap hmAccessHold = new HashMap(); 
			HashMap hmAccessNote = new HashMap(); 
			HashMap hmAccessCredit = new HashMap(); 
			String strEditLateFeeAmtAccess="";
			String strEditLateFeeStatusAccess = "";
			String strHoldLateFeeAccess = "";
			String strNoteLateFeeAccess = "";
			String strCreditLateFeeAccess ="";
			
			hmAccessAmt =GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "EDIT_LATE_FEE_AMOUNT")));
			hmAccessStatus =GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "EDIT_LATE_FEE_STATUS")));
			hmAccessHold =GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "HOLD_LATE_FEE_STATUS")));
			hmAccessNote =GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "NOTE_LATE_FEE")));
			hmAccessCredit =GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "CREDIT_LATE_FEE")));
			
			strEditLateFeeAmtAccess = GmCommonClass.parseNull((String) hmAccessAmt.get("UPDFL"));	
		    strEditLateFeeStatusAccess =  GmCommonClass.parseNull((String)hmAccessStatus.get("UPDFL"));
			strHoldLateFeeAccess =  GmCommonClass.parseNull((String)hmAccessHold.get("UPDFL"));
			strNoteLateFeeAccess =  GmCommonClass.parseNull((String)hmAccessNote.get("UPDFL"));
			strCreditLateFeeAccess =  GmCommonClass.parseNull((String)hmAccessCredit.get("UPDFL"));
			
			gmLoanerLateFeeApprovalForm.setAmounteventaccfl(strEditLateFeeAmtAccess);
			gmLoanerLateFeeApprovalForm.setCrediteventaccfl(strCreditLateFeeAccess);
			gmLoanerLateFeeApprovalForm.setHoldeventaccfl(strHoldLateFeeAccess);
			gmLoanerLateFeeApprovalForm.setNoteseventaccfl(strNoteLateFeeAccess);
			gmLoanerLateFeeApprovalForm.setStatuseventaccfl(strEditLateFeeStatusAccess);

			
	  }
	  
	  /**
	   * @description to send HR emails when the status is either submitted or credit
	   * @param actionMapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward sendHREmails(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws Exception {
		  log.debug("sendHREmails Action");
		  String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		  String strChargeDtlIds = GmCommonClass.parseNull(request.getParameter("chargeDetailInputString"));
		  HashMap hmParams = new HashMap();
          hmParams.put("COMPANYINFO", strCompanyInfo);
          hmParams.put("INPUTSTRING",strChargeDtlIds);
		  GmLoanerLateFeeHREmailBean gmLoanerLateFeeHREmailBean = new GmLoanerLateFeeHREmailBean(getGmDataStoreVO());
		  gmLoanerLateFeeHREmailBean.initiateJMSHREmails(hmParams);
		  return null;
	  }
	  
}

