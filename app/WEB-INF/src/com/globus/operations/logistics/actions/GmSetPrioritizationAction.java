package com.globus.operations.logistics.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.logistics.beans.GmSetPrioritizationBean;
import com.globus.operations.logistics.forms.GmSetPrioritizationForm;

public class GmSetPrioritizationAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                               // Logger Class.;

  /*
   * fetchChangePriority(): this method is used to fetch the changepriority details values
   */

  public ActionForward fetchChangePriority(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmSetPrioritizationForm gmSetPrioritizationForm = (GmSetPrioritizationForm) form;
    gmSetPrioritizationForm.loadSessionParameters(request);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmSetPrioritizationBean gmSetPrioritizationBean =
        new GmSetPrioritizationBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alPriority = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    String strAccess = "";
    String strConsignmentId = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmSetPrioritizationForm);
    alPriority = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LNPRTY"));
    gmSetPrioritizationForm.setAlSetPriority(alPriority);
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "LN_PRIORITY_EVENT"));
    strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if (!strAccess.equals("Y")) {
      throw new AppError("", "20685");
    }
    hmReturn = gmSetPrioritizationBean.fetchConsignmentDetails(hmParam);
    GmCommonClass.getFormFromHashMap(gmSetPrioritizationForm, hmReturn);
    strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));
    alLogReasons = gmCommonBean.getLog(strConsignmentId, "100126");// Displaying comments
    gmSetPrioritizationForm.setAlLogReasons(alLogReasons);
    return mapping.findForward("GmChangePriority");

  }

  /*
   * saveChangePriority(): this method is used to save the changepriority details values
   */
  public ActionForward saveChangePriority(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmSetPrioritizationForm gmSetPrioritizationForm = (GmSetPrioritizationForm) form;
    gmSetPrioritizationForm.loadSessionParameters(request);
    GmSetPrioritizationBean gmSetPrioritizationBean =
        new GmSetPrioritizationBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmSetPrioritizationForm);
    gmSetPrioritizationBean.saveConsignmentDetails(hmParam);
    return fetchChangePriority(mapping, form, request, response);
  }

  /**
   * fetchSetPriority: This method is used to fetch the priority details of Loaner set
   * 
   * @exception AppError
   */
  public ActionForward fetchSetPriority(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmSetPrioritizationForm gmSetPrioritizationForm = (GmSetPrioritizationForm) form;
    GmSetPrioritizationBean gmSetPrioritizationBean =
        new GmSetPrioritizationBean(getGmDataStoreVO());
    String strConsignId = GmCommonClass.parseNull(gmSetPrioritizationForm.getConsignmentId());
    String strPriorityStr = "";

    strPriorityStr =
        GmCommonClass.parseNull(gmSetPrioritizationBean.fetchSetPriority(strConsignId));

    response.setContentType("text/plain");
    PrintWriter pw = response.getWriter();
    pw.write(strPriorityStr);
    pw.flush();
    return null;
  }

}
