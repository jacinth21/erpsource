/**
 * 
 */
package com.globus.operations.logistics.actions;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.cyclecount.beans.GmCycleCountLockTransBean;
import com.globus.accounts.cyclecount.forms.GmCycleCountApprovalForm;
import com.globus.common.actions.GmDispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.io.PrintWriter;
import java.util.*;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.logistics.forms.GmBOItemReqReleaseForm;
import com.globus.operations.logistics.beans.GmBOItemReqReleaseBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;

/**
 * @author pvigneshwaran
 * 
 */
public class GmBOItemReqReleaseAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**loadBOItemReqDetails : This Method will call when click left link
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadBOItemReqDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmBOItemReqReleaseForm gmBOItemReqReleaseForm = (GmBOItemReqReleaseForm) form;
		gmBOItemReqReleaseForm.loadSessionParameters(request);
		GmBOItemReqReleaseBean gmBOItemReqReleaseBean = new GmBOItemReqReleaseBean(getGmDataStoreVO());
		String strInitialCheck = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getInitialFullchk());
		log.debug("strInitialCheck?" + strInitialCheck);
		gmBOItemReqReleaseForm.setInitialFullchk(strInitialCheck);
		return mapping.findForward("gmBOItemReqRelease");
	}

	/**fetchBOItemReqDetails : This method load Back order item Req Details when click Load Button
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchBOItemReqDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		HashMap hmParam = new HashMap();
		HashMap hmTemp = new HashMap();
		ArrayList alTempPartNos = new ArrayList();
		ArrayList alData = new ArrayList();
		HashMap hmDetails =  new HashMap();
		HashMap hmCurrency = new HashMap();
		String strPartNumFormat = "";
		String strTempBOPriority = "";
		ArrayList alReturn =  new ArrayList();
		ArrayList alCurrency =  new ArrayList();
		ArrayList alCurrReturn;
		GmBOItemReqReleaseForm gmBOItemReqReleaseForm = (GmBOItemReqReleaseForm) form;
		gmBOItemReqReleaseForm.loadSessionParameters(request);
		GmBOItemReqReleaseBean gmBOItemReqReleaseBean = new GmBOItemReqReleaseBean(
				getGmDataStoreVO());
		String StrAccName = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getStrAccountName());
		String strSalesRepName = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getStrSalesRep());
		String strPartNos = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getStrPartNos());
		String strReqId = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getStrReqId()); 
		String strBoPriority = "";
		String strAccCurrSymb = "";
		String strCheckFull = "";
		String strCheckPartial = "";
		String strCheckZero = "";
		String strJsonString = "";
		
		String strInitialCheck = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getInitialFullchk()); 
		
		log.debug("strInitialCheck?"+strInitialCheck);
		if(!strPartNos.equals("")){
			strPartNumFormat = strPartNos;
	        strPartNumFormat = "'".concat(strPartNumFormat).concat(",");
	        strPartNumFormat = strPartNumFormat.replaceAll(",", "','");
	        strPartNumFormat = strPartNumFormat.substring(0, strPartNumFormat.length() - 2);
		}
					
		 strCheckFull = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getChkFullQty());
		 strCheckPartial = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getChkPartialQty());
		 strCheckZero = GmCommonClass.parseNull(gmBOItemReqReleaseForm.getChkFGQty());
		if(strInitialCheck.equals("yes") || (strCheckFull!= null && !strCheckFull.equals("")) ){
			strBoPriority = strBoPriority.concat("'1'").concat(",");
		}
		if(strCheckPartial!= null && !strCheckPartial.equals("")){
			strBoPriority = strBoPriority.concat("'2'").concat(",");
		}
		if(strCheckZero!= null && !strCheckZero.equals("")){
			strBoPriority = strBoPriority.concat("'3'").concat(",");
		}
		
		log.debug("strBoPriority "+strBoPriority);
		if(strBoPriority.endsWith(",")){
			strBoPriority = strBoPriority.substring(0, strBoPriority.lastIndexOf(","));	
		}
        
		hmDetails.put("ACCOUNTNAME",StrAccName);
		hmDetails.put("SALESREP",strSalesRepName);
		hmDetails.put("PARTNOS",strPartNumFormat);
		hmDetails.put("REQID",strReqId);
		hmDetails.put("BOPRIORITY",strBoPriority);
		String strSessCompanyLocale = GmCommonClass.parseNull((String) session
				.getAttribute("strSessCompanyLocale"));
		strJsonString = gmBOItemReqReleaseBean.fetchBOItemReqDetails(hmDetails);
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
	}
	
	/**
	   * saveBOItemReqDetails - This method is used to release the back order
	   * 
	   * @param actionMapping
	   * @param form
	   * @param request
	   * @param response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward saveBOItemReqDetails(ActionMapping actionMapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {
	    instantiate(request, response);
	    log.debug("releaseBackOrderDetails");
	    GmBOItemReqReleaseForm gmBOItemReqReleaseForm = (GmBOItemReqReleaseForm) form;
	    gmBOItemReqReleaseForm.loadSessionParameters(request);
	    GmBOItemReqReleaseBean gmBOItemReqReleaseBean = new GmBOItemReqReleaseBean(
				getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    HashMap hmReturn = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmBOItemReqReleaseForm);
	    
	    String strReqId = GmCommonClass.parseNull(request.getParameter("hReqId"));
        String strBOReleaseQty = GmCommonClass.parseNull(request.getParameter("releaseQty"));
        String strPartnum = GmCommonClass.parseNull(request.getParameter("pnum"));
        String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
        String strUserId = (String) session.getAttribute("strSessUserId");
        
        hmParam.put("TXNID", strReqId);
        hmParam.put("PNUM", strPartnum); 
        hmParam.put("USERID", strUserId);
        hmParam.put("BORELEASEQTY", strBOReleaseQty);
        hmParam.put("COMPANYINFO", strCompanyInfo); 
        String strMsg = gmBOItemReqReleaseBean.saveBOItemReqDetails(hmParam); 
        if (!strMsg.equals("")){
        	response.setContentType("text/plain");
        	PrintWriter pw = response.getWriter();
		    pw.write(strMsg);
		    pw.flush();
        }
        return null;
	  }
}
