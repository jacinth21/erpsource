package com.globus.operations.logistics.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.actions.GmDispatchAction;
import com.globus.operations.logistics.beans.GmMultiLoanerExtnBean;
import com.globus.operations.logistics.forms.GmMultiLoanerExtnForm;
/**
 * GmMultiLoanerExtnAction - multi loaner extension Action class
 * @author rajan
 *
 */
public class GmMultiLoanerExtnAction extends GmDispatchAction {
	/**
     *  Code to Initialize the Logger Class.
     */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**
	 * loadDropdownValues - this is used load initial screen details
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	  public ActionForward loadDropdownValues(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	          HttpServletResponse response) throws Exception {
		       instantiate(request, response);
		       GmMultiLoanerExtnForm gmMultiLoanerExtnForm = (GmMultiLoanerExtnForm) form;
		       gmMultiLoanerExtnForm.loadSessionParameters(request);
		       
		       GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
		       
			   String strRepName = GmCommonClass.parseNull((String)request.getParameter("hSalesRepName"));
			   String strSalesRepId = GmCommonClass.parseNull((String)request.getParameter("Cbo_RepId")); 
			   
			   String strDistName = GmCommonClass.parseNull((String)request.getParameter("hDistName"));
			   String strDistId = GmCommonClass.parseNull((String)request.getParameter("Cbo_DistId"));
			   String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
			   request.setAttribute("REPNAME", strRepName);
			   request.setAttribute("REPID", strSalesRepId);
			   
			   request.setAttribute("DISTNAME", strDistName);
			   request.setAttribute("DISTID", strDistId);
			   
			   // Extention should be allowed above current date, Validation in jsp
		       gmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
		       String strCurrDate = gmCalenderOperations.getCurrentDate(strApplDateFmt);
		       request.setAttribute("CurrDate", strCurrDate);   
		          
	
	    return mapping.findForward("gmMultiLoanerExtn");
	}
	  
	  /**
	   * fetchLoanerList  - this is used to fetch the multi loaner extension details
	   * @param mapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	 public ActionForward fetchLoanerList(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	          HttpServletResponse response) throws Exception {
		      log.debug("==GmMultiLoanerExtnAction ==fetchLoanerList");
	          instantiate(request, response);
		      GmMultiLoanerExtnForm gmMultiLoanerExtnForm = (GmMultiLoanerExtnForm) form;
		      GmMultiLoanerExtnBean gmMultiLoanerExtnBean = new GmMultiLoanerExtnBean(getGmDataStoreVO());
		      gmMultiLoanerExtnForm.loadSessionParameters(request);
			  String strJSONGridData = "";
			  HashMap hmParam = new HashMap();
			  hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmMultiLoanerExtnForm));
		      strJSONGridData = GmCommonClass.parseNull(gmMultiLoanerExtnBean.fetchLoanerList(hmParam));
			  response.setContentType("text/json");
			  PrintWriter pw = response.getWriter();
			    if (!strJSONGridData.equals("")) {
			    	gmMultiLoanerExtnForm.setGridXmlData(strJSONGridData);
						pw.write(strJSONGridData);
				  }
				pw.flush();
	   
		   return null;
	 }
	 
	 /**
	  * saveLoanerExtDtl   - this is used to save the multi loaner extension details
	  * @param mapping, form, request, response
	  * @return
	  * @throws Exception
	  */
	 public ActionForward saveLoanerExtDtl(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	          HttpServletResponse response) throws Exception {
		      log.debug("==GmMultiLoanerExtnAction ==saveLoanerExtDtl");
	          instantiate(request, response);
	          try{
		          GmMultiLoanerExtnForm gmMultiLoanerExtnForm = (GmMultiLoanerExtnForm) form;
		          GmMultiLoanerExtnBean gmMultiLoanerExtnBean = new GmMultiLoanerExtnBean(getGmDataStoreVO());
		          gmMultiLoanerExtnForm.loadSessionParameters(request);
				  String strResult = "";
			      HashMap hmParam = new HashMap();
			      hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmMultiLoanerExtnForm));
			      strResult = gmMultiLoanerExtnBean.saveLoanerExtDtl(hmParam);
			      response.setContentType("text/xml");
			      response.setStatus(200);
		          PrintWriter pw = response.getWriter();
		          pw.write(strResult);
		      	  pw.flush();	
	          }catch(Exception e){
	              PrintWriter pWriter = response.getWriter();
	              response.setStatus(300);
	              response.setContentType("text/xml");
	              pWriter.write(e.getMessage());
	              pWriter.flush();
	         }	
	          return null;
	 }
}
