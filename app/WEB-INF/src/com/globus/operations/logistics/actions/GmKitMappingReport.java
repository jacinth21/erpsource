package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.logistics.forms.GmKitMappingReportForm;
import com.globus.operations.logistics.beans.GmKitMappingBean;

public class GmKitMappingReport extends GmDispatchAction{
	
	  Logger log = GmLogger.getInstance(this.getClass().getName());

	public ActionForward loadKitMapReports(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		    instantiate(request, response);
			GmKitMappingReportForm gmKitMappingForm = (GmKitMappingReportForm) form;
			gmKitMappingForm.loadSessionParameters(request);
			GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO()); 
			HttpSession session = request.getSession(false);
			ArrayList alReturn = new ArrayList();
			ArrayList aldata = new ArrayList();
			HashMap hmParam =new HashMap();
			String strOpt = gmKitMappingForm.getStrOpt();
			String strGridXmlData="";
			String strkitStatus = "";
			String strSessCompanyLocale = "";
			String strFrdPage = "GmKitMapping";
			hmParam =  GmCommonClass.getHashMapFromForm(gmKitMappingForm);
			 if (strOpt.equals("load")) {
				    strkitStatus = GmCommonClass.parseNull(request.getParameter("kitStatus"));
				    hmParam.put("KITSTATUS",strkitStatus);
				 	alReturn = GmCommonClass.parseNullArrayList(gmKitMappingBean.fetchReport(hmParam));
				 	String strKitName = GmCommonClass.parseNull(request.getParameter("kitId"));
				 	gmKitMappingForm.setKitId(strKitName);
					String strApplnDateFmt = GmCommonClass.parseNull((String)hmParam.get("APPLNDATEFMT"));
		            hmParam.put("REPORT", alReturn);
		            hmParam.put("SESSDATEFMT", strApplnDateFmt);
		            hmParam.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
		            strGridXmlData = generateOutPut(hmParam);
		            gmKitMappingForm.setGridXmlData(strGridXmlData);
		            request.setAttribute("XMLGRIDDATA",strGridXmlData);
		            request.setAttribute("ALRETURN",alReturn);
		            request.setAttribute("selectedValue",strkitStatus);
		     }
			 return mapping.findForward(strFrdPage);
} 
 /**
	* generateOutPut xml content generation for Kit Mapping Report Details
	* 
	* @param HashMap
	* @return String
 */
	private String generateOutPut(HashMap hmParam) throws Exception {

		 	GmTemplateUtil templateUtil = new GmTemplateUtil();
			String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
			templateUtil.setDataList("alResult", (ArrayList) hmParam.get("REPORT"));
			templateUtil.setDataMap("hmParam", hmParam);
			templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.operations.logistics.GmKitMappingReport", strSessCompanyLocale));
			templateUtil.setTemplateName("GmKitMapping.vm");
			templateUtil.setTemplateSubDir("operations/logistics/templates");
			
			return templateUtil.generateOutput();
   }
}
	
	
