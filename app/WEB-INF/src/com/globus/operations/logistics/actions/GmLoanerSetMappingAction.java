package com.globus.operations.logistics.actions;

/**
 * FileName : GmSetMappingAction.java Author : Velu Date : Aug 2010 Copyright : Globus Medical Inc
 */
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.logistics.forms.GmSetMappingForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmLoanerSetMappingAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmSetMappingForm gmSetMappingForm = (GmSetMappingForm) form;
      gmSetMappingForm.loadSessionParameters(request);
      GmLoanerBean gmLoanerBean = new GmLoanerBean(getGmDataStoreVO());

      ArrayList alResult = null;
      HashMap hParam = null;
      ArrayList alResultGrp = new ArrayList();
      ArrayList alLoanerSetIDResult = new ArrayList();
      ArrayList alConsignSetIDResult = new ArrayList();
      GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
      HashMap hResult = null;
      String strLoanerSetID = "";
      String strConsignSetID = "";
      HashMap hmParam = new HashMap();
      HashMap hmValues = new HashMap();

      hmParam = GmCommonClass.getHashMapFromForm(gmSetMappingForm);
      String strOpt = gmSetMappingForm.getStrOpt();
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));
      hmValues.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      // Loaner Set ID and Consign Set ID save Details
      if (strOpt.equals("save") || strOpt.equals("edit")) {
        gmLoanerBean.saveSetMappingDetails(hmParam);
        strLoanerSetID = GmCommonClass.parseNull((String) hmParam.get("LOANERSETID"));
        hResult = gmLoanerBean.fetchEditMappingDetails(strLoanerSetID);
        gmSetMappingForm.setLoanersetid((String) hResult.get("loanersetid"));
        strOpt = "editload";
      }

      // For displaying all the records in the report
      if (strOpt.equals("report")) {
        alResult = gmLoanerBean.fetchSetMappingDetails(hmParam);

        hmValues.put("LRESULT", alResult);
        hmValues.put("TEMPLATE", "GmSetMapping.vm");
        hmValues.put("TEMPLATEPATH", "/operations/logistics/templates");

        System.out.println("alResult::" + alResult.size());
        gmSetMappingForm.setGridXmlData(getXmlGridData(hmValues));
      }

      // For displaying the edited record in report
      if (strOpt.equals("editload")) {

        // To populate values of the LoanerSetID drop down
        hmValues.put("TYPE", "1601");
        hmValues.put("SETTYPE", "4074");
        alLoanerSetIDResult = gmProjectBean.loadSetMap(hmValues);
        gmSetMappingForm.setAlresultLSetId(alLoanerSetIDResult);

        // To populate values of the LoanerSetID drop down
        hmValues.put("SETTYPE", "4070");
        alConsignSetIDResult = gmProjectBean.loadSetMap(hmValues);
        gmSetMappingForm.setAlresultCSetId(alConsignSetIDResult);

        // To edit the value of the LoanerSetID drop down & ConsignSetID drop down
        strLoanerSetID = GmCommonClass.parseNull((String) hmParam.get("LOANERSETID"));
        hResult = gmLoanerBean.fetchEditMappingDetails(strLoanerSetID);
        gmSetMappingForm.setLoanersetid((String) hResult.get("loanersetid"));

        // For displaying all the records in the report
        gmSetMappingForm.setLoanersetid(strLoanerSetID);
        hmParam = GmCommonClass.getHashMapFromForm(gmSetMappingForm);
        alResultGrp = gmLoanerBean.fetchSetMappingDetails(hmParam);

        hmValues.put("LRESULT", alResultGrp);
        hmValues.put("TEMPLATE", "GmSetMapping.vm");
        hmValues.put("TEMPLATEPATH", "/operations/logistics/templates");

        gmSetMappingForm.setGridXmlData(getXmlGridData(hmValues));
        strOpt = "edit";

        return mapping.findForward("GmLoanerSwapSetup");
      }

      // Page on load
      if (strOpt.equals("load")) {

        // To populate values of the LoanerSetID drop down
        hmValues.put("TYPE", "1601");
        hmValues.put("SETTYPE", "4074");
        alLoanerSetIDResult = gmProjectBean.loadSetMap(hmValues);
        gmSetMappingForm.setAlresultLSetId(alLoanerSetIDResult);

        // To populate values of the LoanerSetID drop down
        hmValues.put("SETTYPE", "4070");
        alConsignSetIDResult = gmProjectBean.loadSetMap(hmValues);
        gmSetMappingForm.setAlresultCSetId(alConsignSetIDResult);

        return mapping.findForward("GmLoanerSwapSetup");
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("GmLoanerSwapReport");
  }

  private String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strTemplate = (String) hmParam.get("TEMPLATE");
    String strTemplatePath = (String) hmParam.get("TEMPLATEPATH");
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.forecast.GmSetmapping", strSessCompanyLocale));
    alResult = (ArrayList) hmParam.get("LRESULT");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hReturn", hmReturn);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    return templateUtil.generateOutput();
  }
}
