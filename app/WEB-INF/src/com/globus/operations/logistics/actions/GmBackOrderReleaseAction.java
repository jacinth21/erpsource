/**
 * 
 */
package com.globus.operations.logistics.actions;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.cyclecount.beans.GmCycleCountLockTransBean;
import com.globus.accounts.cyclecount.forms.GmCycleCountApprovalForm;
import com.globus.common.actions.GmDispatchAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.io.PrintWriter;
import java.util.*;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.logistics.forms.GmBackOrderReleaseForm;
import com.globus.operations.logistics.beans.GmBackOrderReleaseBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;

/**
 * @author sswetha
 * 
 */
public class GmBackOrderReleaseAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public ActionForward loadBackOrderDetails(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		HashMap hmParam = new HashMap();
		HashMap hmTemp = new HashMap();
		ArrayList alTempPartNos = new ArrayList();
		ArrayList alData = new ArrayList();
		HashMap hmDetails =  new HashMap();
		HashMap hmCurrency = new HashMap();
		String strPartNumFormat = "";
		String strTempBOPriority = "";
		ArrayList alReturn =  new ArrayList();
		ArrayList alCurrency =  new ArrayList();
		ArrayList alCurrReturn;
		GmBackOrderReleaseForm gmBackOrderReleaseForm = (GmBackOrderReleaseForm) form;
		gmBackOrderReleaseForm.loadSessionParameters(request);
		GmBackOrderReleaseBean gmBackOrderReleaseBean = new GmBackOrderReleaseBean(
				getGmDataStoreVO());
		String StrAccName = GmCommonClass.parseNull(gmBackOrderReleaseForm.getStrAccountName());
		String strSalesRepName = GmCommonClass.parseNull(gmBackOrderReleaseForm.getStrSalesRep());
		String strPartNos = GmCommonClass.parseNull(gmBackOrderReleaseForm.getStrPartNos());
		String strOrderId = GmCommonClass.parseNull(gmBackOrderReleaseForm.getStrOrderId()); 
		String strBoPriority = "";
		String strAccCurrSymb = "";
		String strCheckFull = "";
		String strCheckPartial = "";
		String strCheckZero = "";
		
		String strInitialCheck = GmCommonClass.parseNull(gmBackOrderReleaseForm.getInitialFullchk()); 
		
		log.debug("strInitialCheck?"+strInitialCheck);
		if(!strPartNos.equals("")){
			strPartNumFormat = strPartNos;
	        strPartNumFormat = "'".concat(strPartNumFormat).concat(",");
	        strPartNumFormat = strPartNumFormat.replaceAll(",", "','");
	        strPartNumFormat = strPartNumFormat.substring(0, strPartNumFormat.length() - 2);
		}
					
		 strCheckFull = GmCommonClass.parseNull(gmBackOrderReleaseForm.getChkFullQty());
		 strCheckPartial = GmCommonClass.parseNull(gmBackOrderReleaseForm.getChkPartialQty());
		 strCheckZero = GmCommonClass.parseNull(gmBackOrderReleaseForm.getChkFGQty());
		
		if(strInitialCheck.equals("yes") || (strCheckFull!= null && !strCheckFull.equals("")) ){
			strBoPriority = strBoPriority.concat("'1'").concat(",");
		}
		if(strCheckPartial!= null && !strCheckPartial.equals("")){
			strBoPriority = strBoPriority.concat("'2'").concat(",");
		}
		if(strCheckZero!= null && !strCheckZero.equals("")){
			strBoPriority = strBoPriority.concat("'3'").concat(",");
		}
		
		log.debug("strBoPriority "+strBoPriority);
		if(strBoPriority.endsWith(",")){
			strBoPriority = strBoPriority.substring(0, strBoPriority.lastIndexOf(","));	
		}
        
		hmDetails.put("ACCOUNTNAME",StrAccName);
		hmDetails.put("SALESREP",strSalesRepName);
		hmDetails.put("PARTNOS",strPartNumFormat);
		hmDetails.put("ORDERID",strOrderId);
		hmDetails.put("BOPRIORITY",strBoPriority);
		String strSessCompanyLocale = GmCommonClass.parseNull((String) session
				.getAttribute("strSessCompanyLocale"));
		alData = gmBackOrderReleaseBean.fetchBackOrderDetails(hmDetails);
		
		if(alData.size() > 0){ 
			for(int i=0; i<alData.size();i++){
				hmTemp = (HashMap)alData.get(i);
				alTempPartNos.add(GmCommonClass.parseNull((String)hmTemp.get("NUM"))); //To get the part numbers to load partfulfill report
			}
		} 
		if(alTempPartNos.size() > 0){
			 Set<String> setPartNos = new LinkedHashSet<String>(alTempPartNos);  
			 alReturn = new ArrayList<String>(setPartNos);
		}
	
		hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
	    String strCompanyCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
	    String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strCompanyCurrId);
		
		log.debug("Currency symbol is "+strAccCurrName);
		hmParam.put("TEMPLATENAME", "GmBackOrderRelease.vm");
		hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
		hmParam.put("SESSCURRSYMBOL",strAccCurrName);
		String strGridXmlData = generateOutPut(alData, hmParam);
		gmBackOrderReleaseForm.setGridXmlData(strGridXmlData);
		gmBackOrderReleaseForm.setInitialFullchk(strInitialCheck);
		gmBackOrderReleaseForm.setAlPartNos(alReturn);//set the part nos
		return mapping.findForward("gmBackOrderRelease"); 
	}
	
	/**
	   * releaseBackOrderDetails - This method is used to release the back order
	   * 
	   * @param actionMapping
	   * @param form
	   * @param request
	   * @param response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward releaseBackOrderDetails(ActionMapping actionMapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {
	    instantiate(request, response);
	    log.debug("releaseBackOrderDetails");
	    GmBackOrderReleaseForm gmBackOrderReleaseForm = (GmBackOrderReleaseForm) form;
		gmBackOrderReleaseForm.loadSessionParameters(request);
		// GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
		GmBackOrderReleaseBean gmBackOrderReleaseBean = new GmBackOrderReleaseBean(
				getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    HashMap hmReturn = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmBackOrderReleaseForm);
	    
	    String strOrderId = GmCommonClass.parseNull(request.getParameter("hOrderId"));
        String strBOReleaseQty = GmCommonClass.parseNull(request.getParameter("releaseQty"));
        String strPartnum = GmCommonClass.parseNull(request.getParameter("pnum"));
        String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
        String strUserId = (String) session.getAttribute("strSessUserId");

        log.debug("strOrderId " + strOrderId);
        log.debug("strBOReleaseQty " + strBOReleaseQty);

        hmParam.put("TXNID", strOrderId);
        hmParam.put("PNUM", strPartnum); 
        hmParam.put("USERID", strUserId);
        hmParam.put("BORELEASEQTY", strBOReleaseQty);
        hmParam.put("COMPANYINFO", strCompanyInfo); 
        hmReturn = gmBackOrderReleaseBean.processBackOrderRelease(hmParam); 
        log.debug("hmReturn " + hmReturn);
        String strMsg = GmCommonClass.parseNull((String) hmReturn.get("MSG"));
        log.debug("strMsg " + strMsg);
        if (!strMsg.equals("")){
        	response.setContentType("text/plain");
        	PrintWriter pw = response.getWriter();
		    pw.write(strMsg);
		    pw.flush();
		    return null;
        }
        return null;
	  }

	  
	/**
	 * description grid data used DHTMLX for vm file
	 * 
	 * @author swetha
	 * @param hmParam
	 * @return templateUtil
	 * @throws AppError
	 */
	public String generateOutPut(ArrayList alresult, HashMap hmParam)
			throws AppError {
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam
				.get("STRSESSCOMPANYLOCALE"));
		String strTempName = GmCommonClass.parseNull((String) hmParam
				.get("TEMPLATENAME"));
		templateUtil.setDataMap("hmParam", hmParam);
		templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.operations.logistics.GmBackOrderRelease", strSessCompanyLocale));
		templateUtil.setDataList("alList", alresult);
		templateUtil.setTemplateSubDir("operations/logistics/templates");
		templateUtil.setTemplateName(strTempName);
		return templateUtil.generateOutput();
	}

}
