package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmLoanerReconBean;
import com.globus.operations.logistics.beans.GmTicketBean;
import com.globus.operations.logistics.forms.GmLoanerReconciliationForm;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmAutoCompletePartyListRptBean;
public class GmLoanerReconReportAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLoanerReconciliationForm gmLoanerReconciliationForm = (GmLoanerReconciliationForm) form;
    GmLoanerReconBean gmLoanerReconBean = new GmLoanerReconBean(getGmDataStoreVO());
    GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmTicketBean gmTicketBean = new GmTicketBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alUSRepList = new ArrayList();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmDept = new HashMap();
    ArrayList alEmpList = new ArrayList();
    ArrayList alRepList = new ArrayList();
    int currMonth = 0;
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    gmLoanerReconciliationForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerReconciliationForm);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strTicketUrl = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("TICKET_URL"));
    String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strTodayDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strTransType = GmCommonClass.parseNull((String) hmParam.get("LOANERTYPE"));
    
    String strDistId = gmLoanerReconciliationForm.getDist();
    String strRepId = gmLoanerReconciliationForm.getRep();
    
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    String firstDayofMonth = "";

    firstDayofMonth = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
log.debug("GmLoanerReconReportAction  strOpt "+strOpt);
    if (firstDayofMonth.equals("")) {
      firstDayofMonth =
          GmCommonClass.parseNull(GmCalenderOperations.getAnyDateOfMonth(currMonth, 1,
              strApplDateFmt));
    }
    if (strTodayDt.equals("")) {
      strTodayDt = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strApplDateFmt));
    }
    String strAccessCondition = GmCommonClass.parseNull(getAccessFilter(request, response));
    // The following Code is added for avoiding the Duplicate Field Sales Names
    HashMap hmAccessValues = new HashMap();
    hmAccessValues.put("CONDITION", strAccessCondition);
    hmAccessValues.put("ACTIVEDISTFL", "Y");
    hmAccessValues.put("STATUS", "Active");
    hmAccessValues.put("FLTR_DIST_BY_COMP", "YES");// Filter Distributor Based on company

    hmAccessValues.put("SCREENNAME", "LOANERRECON");
    // The following code added for Getting the Distributor, Sales Rep and Employee List based on
    // the PLANT too if the Selected Company is EDC
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    String strCompanyPlant =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "LOAN_RECON_HEADER"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    if (strCompanyPlant.equals("Plant")) {
      hmAccessValues.put("DISTFILTER", "COMPANY-PLANT");
      hmAccessValues.put("REPFILTER", "COMPANY-PLANT");
    }

    hmSalesFilters = gmCommonBean.getSalesFilterLists(hmAccessValues);
    
   /* alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
    gmLoanerReconciliationForm.setAlDist(alReturn);
    alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
    gmLoanerReconciliationForm.setAlRepList(alReturn);*/
    
    String strRequestId = gmLoanerReconciliationForm.getRequestId();
    alReturn = gmAutoCompleteReportBean.getCodeList("LRTYP", null);
    gmLoanerReconciliationForm.setAlStatus(alReturn);

    alReturn = gmAutoCompleteReportBean.getCodeList("LNRTYP", null);
    gmLoanerReconciliationForm.setAlType(alReturn);
    /*
     * The below Method call for getting the Rep List from GmCustomerBean is commenting due to
     * already we are getting from the Sales filters . so avoiding the multiple assignments to Sales
     * Rep List.
     */
    GmAutoCompletePartyListRptBean gmAutoCompletePartyListRptBean =
        new GmAutoCompletePartyListRptBean();
    String strFlag = "Y";
    ArrayList alOUSList = new ArrayList();
    HashMap hmData = new HashMap();
    hmData.put("USERTYPE", "300");
    hmData.put("USERSTATUS", "311");
    alUSRepList = gmAutoCompletePartyListRptBean.getUSRepList(strFlag);
    alEmpList = gmAutoCompletePartyListRptBean.getEmployeeList(hmData);
    alOUSList = gmAutoCompletePartyListRptBean.getOUSSalesList(strFlag);
    // alRepList = gmCust.getRepList();
    gmLoanerReconciliationForm.setAlOUSList(alOUSList);
    gmLoanerReconciliationForm.setAlUSRepList(alUSRepList);
    // alEmpList = gmLogon.getEmployeeList();
    gmLoanerReconciliationForm.setAlEmployeeList(alEmpList);

    gmLoanerReconciliationForm.setAlEventLoanToList(GmCommonClass
        .parseNullArrayList(gmAutoCompleteReportBean.getCodeList("EVELON", null)));

    gmLoanerReconciliationForm.setDeptId(strDeptId);
    hmDept.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmDept.put("STRDEPT", strDeptId);
    hmDept.put("TICKETURL", strTicketUrl);
    hmDept.put("REQUESTID", strRequestId);
    hmDept.put("TRANTYPE", strTransType);
    hmDept.put("DATEFORMAT", strApplDateFmt);

    GmCommonClass.getFormFromHashMap(gmLoanerReconciliationForm, hmParam);
    hmParam.put("DIST", strDistId);
    hmParam.put("REP", strRepId);
    
    request.setAttribute("DIST", strDistId);
    request.setAttribute("REP", strRepId);
    if (strOpt.equals("onLoad")) {
      if (gmLoanerReconciliationForm.getFromDt().equals("")
          && gmLoanerReconciliationForm.getToDt().equals("")) {
        gmLoanerReconciliationForm.setFromDt(firstDayofMonth);
        gmLoanerReconciliationForm.setToDt(strTodayDt);
      }
      gmLoanerReconciliationForm.setLoanerType("4127");
    }
    if (strOpt.equals("Email")) {
      gmLoanerReconciliationForm.getHreqids();
      String strCipherUserName = (String) request.getSession().getAttribute("strLogonName");
      GmJasperReport gmJasperReport = new GmJasperReport();
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setJasperReportName("/GmEmailMissingParts.jasper");
      gmTicketBean.sendMissingPartsEmailOnRequest(gmLoanerReconciliationForm.getHreqids(),
          gmLoanerReconciliationForm.getUserId(), strCipherUserName, gmJasperReport);

      gmLoanerReconciliationForm.setStrOpt("Load");
      strOpt = "Load";
    }
    if (strOpt.equals("Load")) {
      String strFrmdate = "";
      String strToDate = "";
      hmParam.put("AccessFilter", strAccessCondition);
      if (gmLoanerReconciliationForm.getFromDt().equals("")
          && gmLoanerReconciliationForm.getToDt().equals("")) {
        strFrmdate = "";
        strToDate = "";
      } else {
        strFrmdate = firstDayofMonth;
        strToDate = strTodayDt;
      }
      strDistId = GmCommonClass.parseNull((String) hmParam.get("DIST"));
      strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
      alResult = gmLoanerReconBean.loadReconReport(hmParam);
      gmLoanerReconciliationForm.setFromDt(strFrmdate);
      gmLoanerReconciliationForm.setToDt(strToDate);

    }

    gmLoanerReconciliationForm.setGridData(generateOutPut(alResult, hmDept));
    return mapping.findForward("ReconcileLoaner");
  }

  private String generateOutPut(ArrayList alResult, HashMap hmDept) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale = (String) hmDept.get("STRSESSCOMPANYLOCALE");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmDept", hmDept);
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setTemplateName("GmLoanerReconcileReport.vm");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.GmReconcileLoanerReport", strSessCompanyLocale));
    return templateUtil.generateOutput();
  }
}
