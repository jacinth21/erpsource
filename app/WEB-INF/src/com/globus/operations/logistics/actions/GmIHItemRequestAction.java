package com.globus.operations.logistics.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.itemcontrol.beans.GmItemControlRptBean;
import com.globus.operations.logistics.beans.GmIHItemRequestBean;
import com.globus.operations.logistics.forms.GmIHItemRequestForm;

public class GmIHItemRequestAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());
    instantiate(request, response);
    String strForward = "gmIHItemRequestRpt";
    GmIHItemRequestForm gmIHItemRequestForm = (GmIHItemRequestForm) form;
    gmIHItemRequestForm.loadSessionParameters(request);
    GmIHItemRequestBean gmIHItemRequestBean = new GmIHItemRequestBean(getGmDataStoreVO());
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    HttpSession session = request.getSession(false);
    HashMap hmParam = new HashMap();
    HashMap hmHeaderInfo = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alReqtype = new ArrayList();
    ArrayList alPurpose = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alChooseAction = new ArrayList();
    ArrayList alPartNumberDetails = new ArrayList();
    ArrayList alReplenishDetails = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    String strTemplateName = "";
    String strRedirectURL = "";
    String strRequestID = "";
    String strRequestType = "";
    String strStatus = "";
    String strStatusFl = "";
    String strInhouseTxn = "";
    String strTxn = "";
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    String strFromDt = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
    String strToDt = GmCalenderOperations.addDays(30, strApplnDateFmt);
    String strOpt = gmIHItemRequestForm.getStrOpt();
    String strhStatus = GmCommonClass.parseNull(gmIHItemRequestForm.gethStatus());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    if (strOpt.equals("")) {
      gmIHItemRequestForm.setFromDt(strFromDt);
      gmIHItemRequestForm.setToDt(strToDt);
    }

    alReqtype = GmCommonClass.getCodeList("RQFOIH");
    gmIHItemRequestForm.setAlReqtype(alReqtype);

    alPurpose = GmCommonClass.getCodeList("RQIHPU");
    gmIHItemRequestForm.setAlPurpose(alPurpose);

    alStatus = GmCommonClass.getCodeList("RQIHST");
    gmIHItemRequestForm.setAlStatus(alStatus);

    if (strhStatus.equals("") && strOpt.equals("")) {
      gmIHItemRequestForm.sethStatus("0,3");
    }
    alChooseAction = GmCommonClass.getCodeList("RQIHCA");
    gmIHItemRequestForm.setAlChooseAction(alChooseAction);

    hmParam = GmCommonClass.getHashMapFromForm(gmIHItemRequestForm);
    hmParam.put("DATEFORMAT", strApplnDateFmt);
    if (strOpt.equals("Load")) {
      String strFromDate = "";
      String strToDate = "";
      String strReqFromDt = gmIHItemRequestForm.getFromDt();
      String strReqToDt = gmIHItemRequestForm.getToDt();

      if (strReqFromDt.equals("") && strReqToDt.equals("")) {
        strFromDate = "";
        strToDate = "";
      } else {
        strFromDate = strReqFromDt;
        strToDate = strReqToDt;
      }
      gmIHItemRequestForm.setFromDt(strFromDate);
      gmIHItemRequestForm.setToDt(strToDate);

      ArrayList alResult = gmIHItemRequestBean.fetchIHBORequest(hmParam);
      hmParam.put("ALIHBODATA", alResult);
      hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmParam.put("VMFILEPATH", "properties.labels.operations.logistics.GmIHItemRequestRpt");
      request.setAttribute("hmParam", hmParam);
      strTemplateName = "GmIHItemRequest.vm";
      gmIHItemRequestForm.setXmlGridData(getXmlGridData(hmParam, strTemplateName));
    } else if (strOpt.equals("Save")) {
      hmReturn = gmIHItemRequestBean.saveIHItemBOReleaseDtl(hmParam);
      strInhouseTxn = GmCommonClass.parseNull((String) hmReturn.get("INHOUSETXN"));
      strTxn = GmCommonClass.parseNull((String) hmReturn.get("TXN"));
      strRequestID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
      strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPEID"));
      /* Below redirect for avoid duplicate insert due to refresh after save. */
      strRedirectURL =
          "/gmIHItemRequest.do?strOpt=ReconfigReq&requestID=" + strRequestID + "&requestTypeID="
              + strRequestType + "&inHouseTxns=" + strInhouseTxn + "&txns=" + strTxn;

      return actionRedirect(strRedirectURL, request);
    } else if (strOpt.equals("ReconfigReq")) {
      ArrayList alReplenish = new ArrayList();
      strInhouseTxn = GmCommonClass.parseNull((String) hmParam.get("INHOUSETXNS"));
      strTxn = GmCommonClass.parseNull((String) hmParam.get("TXNS"));
      strRequestID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
      strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPEID"));
      hmHeaderInfo =
          GmCommonClass.parseNullHashMap(gmItemControlRptBean.fetchTransHeaderInfo(strRequestID,
              strRequestType));
      GmCommonClass.getFormFromHashMap(gmIHItemRequestForm, hmHeaderInfo);
      gmIHItemRequestForm.setRequestID(GmCommonClass.parseNull((String) hmHeaderInfo.get("TXNID")));
      gmIHItemRequestForm
          .setReqtype(GmCommonClass.parseNull((String) hmHeaderInfo.get("TXNTYPENM")));
      strStatusFl = GmCommonClass.parseNull((String) hmHeaderInfo.get("STATUSFL"));
      gmIHItemRequestForm.sethStatus(strStatusFl);
      strStatus = GmCommonClass.parseNull((String) hmHeaderInfo.get("STATUS"));
      gmIHItemRequestForm.setStatus(strStatus);
      alReplenish = GmCommonClass.getCodeList("IHRQRF");
      gmIHItemRequestForm.setAlReplenish(alReplenish);
      hmParam.put("ALREPLENISHLIST", alReplenish);
      alPartNumberDetails =
          GmCommonClass.parseNullArrayList(gmIHItemRequestBean.fetchIHRequestDetails(hmParam));
      gmIHItemRequestForm.setAlPartNmDtl(alPartNumberDetails);
      alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strRequestID, "1288"));
      gmIHItemRequestForm.setAlLogReasons(alLogReasons);
      alReplenishDetails =
          GmCommonClass.parseNullArrayList(gmIHItemRequestBean.fetchReplenishmentTxn(hmParam));
      hmParam.put("ALIHREQDTLDATA", alReplenishDetails);
      hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmParam.put("VMFILEPATH", "properties.labels.operations.logistics.GmIHRequestDtlInclude");
      request.setAttribute("hmParam", hmParam);
      strTemplateName = "GmIHRequestDtl.vm";
      gmIHItemRequestForm.setXmlGridData(getXmlGridData(hmParam, strTemplateName));
      gmIHItemRequestForm.setInHouseTxns(strInhouseTxn);
      gmIHItemRequestForm.setTxns(strTxn);
      strForward = "gmIHItemReconfigReq";
    } else if (strOpt.equals("InvPartQty")) {
      ArrayList alInvPartQty = new ArrayList();
      String strReplenishId = request.getParameter("replenish");
      String strPartNum = request.getParameter("partNum");
      // alInvPartQty =
      // GmCommonClass.parseNullArrayList(gmIHItemRequestBean.fetchInvQtyPartMappingByID(hmParam,strPartNum,strReplenishId));
      String strInvQty = gmIHItemRequestBean.fetchInvQtyPartMappingByID(strPartNum, strReplenishId);
      String strInputString = strInvQty;
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (!strInputString.equals(""))
        pw.write(strInputString);
      pw.flush();
      pw.close();
      return null;
    } else if (strOpt.equals("Review")) {
      hmParam.put("STATUSFL", "3");
      gmIHItemRequestBean.saveIHItemBOStatus(hmParam);
      strRequestID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
      strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPEID"));
      /* Below redirect for avoid duplicate insert due to refresh after save. */
      strRedirectURL =
          "/gmIHItemRequest.do?strOpt=ReconfigReq&requestID=" + strRequestID + "&requestTypeID="
              + strRequestType;

      return actionRedirect(strRedirectURL, request);
    }
    return mapping.findForward(strForward);
  }

  private String getXmlGridData(HashMap hmParam, String strTemplateName) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALIHBODATA"));
    ArrayList alReqdtl =
        GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALIHREQDTLDATA"));
    ArrayList alReplenishDropDown =
        GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALREPLENISHLIST"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataList("alReqdtl", alReqdtl);

    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setDropDownMaster("alReplenish", alReplenishDropDown);
    templateUtil.setTemplateSubDir("operations/logistics/templates");
    templateUtil.setTemplateName(strTemplateName);
    return templateUtil.generateOutput();
  }

}
