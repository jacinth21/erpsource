package com.globus.operations.logistics.actions;

import javax.servlet.http.HttpServletRequest;    
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.common.actions.GmAction; 
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.logistics.forms.GmMultiProcConsignForm;
import com.globus.operations.logistics.beans.GmMultiProcConsignBean;

public class GmMultiProcConsignAction extends GmDispatchAction {
    
	/**
     *  Code to Initialize the Logger Class.
     */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**
	 * loadDropdownValues - this is used load page
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	  public ActionForward loadDropdownValues(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	          HttpServletResponse response) throws Exception {
		       instantiate(request, response);
			   GmMultiProcConsignForm gmMultiProcConsignForm = (GmMultiProcConsignForm) form;
			   gmMultiProcConsignForm.loadSessionParameters(request);
			   GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
			   ArrayList alDistList = new ArrayList();
			   String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
			   String strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "ITEMINITFILTER")) ;
			   HashMap hmFilters = new HashMap();
			   	    hmFilters.put("FILTER", "Active");
			   	    hmFilters.put("USERCODE", "300");
			   	    hmFilters.put("DEPT", "");
			   	    hmFilters.put("STATUS", "311");    
			   	    if(strCompanyPlant.equals("Plant")){
			   		    hmFilters.put("DISTFILTER", "COMPANY-PLANT");
			   		    hmFilters.put("REPFILTER", "COMPANY-PLANT");
			   		    hmFilters.put("EMPFILTER", "COMPANY-PLANT");
			   	    }	

			        alDistList = GmCommonClass.parseNullArrayList(gmCustomerBean.getDistributorList(hmFilters));
			  gmMultiProcConsignForm.setAlRequestTo(alDistList);
			  gmMultiProcConsignForm.setAlRequestTo(alDistList);
	
	    return mapping.findForward("gmMultiProcConsign");
	}
	  
	  /**
		 * loadMultiSetDetails - this is used load page
		 * @param mapping, form, request, response
		 * @return
		 * @throws Exception
		 */
	  public ActionForward loadMultiSetDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		       HttpServletResponse response) throws Exception {
		        log.debug("==GmMultiProcConsignAction ==loadMultiSetDetails");
		        instantiate(request, response);
				GmMultiProcConsignForm gmMultiProcConsignForm = (GmMultiProcConsignForm) form;
				GmMultiProcConsignBean gmMultiProcConsignBean = new GmMultiProcConsignBean(getGmDataStoreVO());
				gmMultiProcConsignForm.loadSessionParameters(request);
				String strJSONGridData = "";
			    HashMap hmParam = new HashMap();
			    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmMultiProcConsignForm));
		        strJSONGridData = GmCommonClass.parseNull(gmMultiProcConsignBean.loadMultiSetDetails(hmParam));
			    response.setContentType("text/json");
			    PrintWriter pw = response.getWriter();
			    if (!strJSONGridData.equals("")) {
						gmMultiProcConsignForm.setGridXmlData(strJSONGridData);
						pw.write(strJSONGridData);
				  }
				pw.flush();
			return null;
		}
		  
	  /**
	   * fetchRequestDetails - This is used to fetch request details
	   * @param mapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	  public ActionForward fetchRequestDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	           HttpServletResponse response) throws Exception {
		        log.debug("==GmMultiProcConsignAction ==fetchRequestDetails");
		        instantiate(request, response);
			    GmMultiProcConsignForm gmMultiProcConsignForm = (GmMultiProcConsignForm) form;
			    GmMultiProcConsignBean gmMultiProcConsignBean = new GmMultiProcConsignBean(getGmDataStoreVO());
			    gmMultiProcConsignForm.loadSessionParameters(request);
			    HashMap hmParam = new HashMap();
			    String strJSONGridData = "";
			    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmMultiProcConsignForm));
			    strJSONGridData = gmMultiProcConsignBean.fetchRequestDetails(hmParam);
			    response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				if (!strJSONGridData.equals("")) {
					gmMultiProcConsignForm.setGridXmlData(strJSONGridData);
					pw.write(strJSONGridData);
				}
				pw.flush();
				return null;
		}
	  
	  /**
	   * processMultiConsignSets  - This is used to save the multiple sets
	   * @param mapping, form, request, response
	   * @return
	   * @throws Exception
	   */
	 public ActionForward processMultiConsignSets (ActionMapping mapping, ActionForm form,  HttpServletRequest request,
			 HttpServletResponse response) throws Exception {
		      log.debug("==GmMultiProcConsignAction ==processMultiConsignSets");
		      instantiate(request, response);
		      GmMultiProcConsignForm gmMultiProcConsignForm = (GmMultiProcConsignForm) form;
		      GmMultiProcConsignBean gmMultiProcConsignBean = new GmMultiProcConsignBean(getGmDataStoreVO());
		      gmMultiProcConsignForm.loadSessionParameters(request);
		      HashMap hmParam = new HashMap();
		      String strJSONGridData = "";
		      hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmMultiProcConsignForm));
		      strJSONGridData = gmMultiProcConsignBean.processMultiConsignSets(hmParam);
    		  response.setContentType("text/json");
    		  PrintWriter pw = response.getWriter();
	    		if (!strJSONGridData.equals("")) {
	    			pw.write(strJSONGridData);
	    		}
    		pw.flush();
    	return null;
	  }
		  
}
