package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.logistics.beans.GmChargesBean;
import com.globus.operations.logistics.forms.GmChargesApprovalForm;

public class GmChargesApprovalAction extends GmAction {
  HashMap hmValues = new HashMap();

  protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                                         // Logger Class.;

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmChargesApprovalForm gmChargesApprovalForm = (GmChargesApprovalForm) form;
    gmChargesApprovalForm.loadSessionParameters(request);

    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmChargesBean gmChargesBean = new GmChargesBean(getGmDataStoreVO());
    GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
    String strScreenType = GmCommonClass.parseNull(gmChargesApprovalForm.getScreenType());

    ArrayList alReturn = new ArrayList();
    ArrayList alResult = new ArrayList();
    RowSetDynaClass rdReturn = null;

    log.debug("inside GmChargesApprovalAction");
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    
    // The following code added for Getting the Distributor, Sales Rep and Employee List based on the PLANT too if the Selected Company is EDC
	 String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
	 String strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "LOAN_CHARGES_HEADER")) ;//LOAN_CHARGES_HEADER- To fetch the records for EDC entity countries based on plant
	  
	 HashMap hmFilters = new HashMap();
	    hmFilters.put("FILTER", "Active");
	    if(strCompanyPlant.equals("Plant")){
		    hmFilters.put("DISTFILTER", "COMPANY-PLANT");
		    hmFilters.put("REPFILTER", "COMPANY-PLANT");
	    }	
    alReturn = gmCustomerBean.getRepList(hmFilters);
    gmChargesApprovalForm.setAlRepList(alReturn);
    alReturn = gmCustomerBean.getDistributorList(hmFilters);
    gmChargesApprovalForm.setAlDistList(alReturn);
    alReturn = GmCommonClass.getAllCodeList("ADDCH");
    gmChargesApprovalForm.setAlIncidentList(alReturn);
    alReturn = GmCommonClass.getCodeList("CHGTY");
    gmChargesApprovalForm.setAlType(alReturn);
    String strDeptId = gmChargesApprovalForm.getDeptId();
    String strOpt = gmChargesApprovalForm.getStrOpt();
    String strChargeType = GmCommonClass.parseNull(gmChargesApprovalForm.getChargesFor());
    String strFromDate = "";
    String strToDate = "";
    String strFrom = "";
    String strTo = "";
    /*
     * The follwing code is commnets for the task PMT-2145 for the From date as previous month 16th
     * day and TO date as Current Month 15th day int intCurrentDt =
     * GmCalenderOperations.getCurrentMonth()-2; if(gmChargesApprovalForm.getToDate()==null ||
     * gmChargesApprovalForm.getToDate().equals("") )
     * gmChargesApprovalForm.setToDate(GmCalenderOperations.getLastDayOfMonth(intCurrentDt));
     * if(gmChargesApprovalForm.getFromDate()==null ||
     * gmChargesApprovalForm.getFromDate().equals(""))
     * gmChargesApprovalForm.setFromDate(GmCalenderOperations.getFirstDayOfMonth(intCurrentDt));
     */
    GmSearchCriteria gmsearchCriteria = new GmSearchCriteria();
    alReturn = GmCommonClass.getCodeList("CHGST");
    gmsearchCriteria.addSearchCriteria("CODENMALT", "50", GmSearchCriteria.NOTEQUALS);
    alReturn = (ArrayList) gmsearchCriteria.query(alReturn);
    gmChargesApprovalForm.setAlStatus(alReturn);
    // now fetch the status dropdown values for the displaytag rows
    List alStlist = GmCommonClass.getCodeList("CHGST");
    String strStatus = gmChargesApprovalForm.getStatus();
    String strChargesFor = gmChargesApprovalForm.getChargesFor();
    if (strStatus.equals("10")) {// open
      gmSearchCriteria.addSearchCriteria("CODENMALT", "10", GmSearchCriteria.GREATERTHAN);
      alStlist = gmSearchCriteria.query(alStlist);
    } else if (strStatus.equals("20")) { // approved
      gmSearchCriteria.addSearchCriteria("CODENMALT", "20", GmSearchCriteria.GREATERTHAN);
      alStlist = gmSearchCriteria.query(alStlist);
    } else if (strStatus.equals("40")) {// cancelled
      gmSearchCriteria.addSearchCriteria("CODENMALT", "40", GmSearchCriteria.GREATERTHAN);
      alStlist = gmSearchCriteria.query(alStlist);
      // alStlist = null;
    }
    if (!strChargesFor.equals("50890")) {
      gmSearchCriteria.addSearchCriteria("CODENMALT", "50", GmSearchCriteria.NOTEQUALS);
      alStlist = gmSearchCriteria.query(alStlist);
    }
    gmChargesApprovalForm.setAlStatustListRpt(alStlist);
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
    if (strFromDate.equals("") && strOpt.equals("")) {
      strFromDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("From Date", "MISCHRGDT"));
      strToDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("To Date", "MISCHRGDT"));
      int intFromdate = Integer.parseInt(strFromDate);
      int intTodate = Integer.parseInt(strToDate);
      int intPrevMonth = gmCalenderOperations.getCurrentMonth() - 2;
      int intCurrMonth = gmCalenderOperations.getCurrentMonth() - 1;
      strFrom = gmCalenderOperations.getAnyDateOfMonth(intPrevMonth, intFromdate, strApplnDateFmt);
      strTo = gmCalenderOperations.getAnyDateOfMonth(intCurrMonth, intTodate, strApplnDateFmt);
      gmChargesApprovalForm.setFromDate(strFrom);
      gmChargesApprovalForm.setToDate(strTo);
    }
    log.debug("strChargeType ??"+strChargeType );

    if (strScreenType.equals("LoanerCharges") && strChargeType.equals("50891")) {
      return missingCharges(mapping, gmChargesApprovalForm, request, response);
    } else {
      HashMap hmParam = new HashMap();
      hmParam = GmCommonClass.getHashMapFromForm(gmChargesApprovalForm);
      if (strOpt.equals("update")) {
        gmChargesBean.saveChargesStatus(gmChargesApprovalForm.getUpdateString(),
            gmChargesApprovalForm.getUserId());
        strOpt = "load";
      }
      if (strDeptId.equals("2000")) {// Accounts
        hmParam.put("STATUS", "20");
        gmChargesApprovalForm.setStatus("20");
      }
      if (strOpt.equals("load") && strChargeType.equals("92068") ) { //Loaner Late Fee charge by Set
    	  // Call the below method to load the Loaner Late Fee charge by Set
         alResult = gmChargesBean.fetchChargesLateFeeBySetReport(hmParam);
         gmChargesApprovalForm.setAlReturn(alResult);
      }else if (strOpt.equals("load")){
    	 alResult = gmChargesBean.fetchChargesReport(hmParam);
         gmChargesApprovalForm.setAlReturn(alResult);
      }
      return mapping.findForward("gmApproveChargesDetails");
    }
  }

  public ActionForward missingCharges(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmChargesApprovalForm gmMissingChargesDtlForm = (GmChargesApprovalForm) form;
    gmMissingChargesDtlForm.loadSessionParameters(request);

    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

    ArrayList alDistList = new ArrayList();
    ArrayList alRepList = new ArrayList();
    String strForward = "";
    String strOpt = "";
    String strFromDate = "";
    String strToDate = "";
    String strChkTicket = "";
    String strChkRepname = "";
    String strChkListPrice = "";
    String strChkConsignment = "";
    String strChkLastUpdBy = "";
    String strChkLastUpdDt = "";
    String strScreenType = "";
    String strRedirectURL = "";
    String strChargesFor = "";
    String strRequestID = "";
    String strFrom = "";
    String strTo = "";
    String strChkAssocRepname = "";
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmMissingChargesDtlForm);
    getDistTypeFromRequest(request, hmParam);
    GmChargesBean gmChargesBean = new GmChargesBean(getGmDataStoreVO());
    RowSetDynaClass rdDemandSheetReport = null;
    alRepList = gmCustomerBean.getRepList("Active");
    alDistList = gmCustomerBean.getDistributorList("Active");
    ArrayList alReconCommnetsList =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RECOMM"));
    gmMissingChargesDtlForm.setAlDistList(alDistList);
    gmMissingChargesDtlForm.setAlRepList(alRepList);
    gmMissingChargesDtlForm.setAlCommnetsList(alReconCommnetsList);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    strOpt = GmCommonClass.parseNull(gmMissingChargesDtlForm.getStrOpt());
    if (strOpt.equals("load")) {
      strFromDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("From Date", "MISCHRGDT"));
      strToDate = GmCommonClass.parseNull(GmCommonClass.getRuleValue("To Date", "MISCHRGDT"));
      GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
      int Fromdate = Integer.parseInt(strFromDate);
      int Todate = Integer.parseInt(strToDate);
      int prevMonth = gmCalenderOperations.getCurrentMonth() - 2;
      int currMonth = gmCalenderOperations.getCurrentMonth() - 1;
      strFrom = gmCalenderOperations.getAnyDateOfMonth(prevMonth, Fromdate, strApplnDateFmt);
      strTo = gmCalenderOperations.getAnyDateOfMonth(currMonth, Todate, strApplnDateFmt);
      gmMissingChargesDtlForm.setFromDate(strFrom);
      gmMissingChargesDtlForm.setToDate(strTo);
    }
    GmSearchCriteria gmsearchCriteria = new GmSearchCriteria();
    ArrayList alReturn = GmCommonClass.getCodeList("CHGST");
    gmsearchCriteria.addSearchCriteria("CODENMALT", "50", GmSearchCriteria.NOTEQUALS);
    alReturn = (ArrayList) gmsearchCriteria.query(alReturn);
    gmMissingChargesDtlForm.setAlStatus(alReturn);
    HashMap hmSaveAccess = new HashMap();
    String strPartyId = "";
    String strsaveBtnAccess = "";
    strPartyId = gmMissingChargesDtlForm.getSessPartyId();
    hmSaveAccess =
        GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
            "SAVECHARGES")));
    strsaveBtnAccess = GmCommonClass.parseNull((String) hmSaveAccess.get("UPDFL"));
    request.setAttribute("SAVEUPDFL", strsaveBtnAccess);
    HashMap hmSaveChargeAccess = new HashMap();
    String strChargeBtnAccess = "";
    hmSaveChargeAccess =
        GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
            "CHARGEANDWRITEOFF")));
    strChargeBtnAccess = GmCommonClass.parseNull((String) hmSaveChargeAccess.get("UPDFL"));
    request.setAttribute("CHARGEUPDFL", strChargeBtnAccess);

    HashMap hmSaveCreditAccess = new HashMap();
    String strCreditBtnAccess = "";
    hmSaveCreditAccess =
        GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId,
            "SAVECREDIT")));
    strCreditBtnAccess = GmCommonClass.parseNull((String) hmSaveCreditAccess.get("UPDFL"));
    request.setAttribute("CREDITUPDFL", strCreditBtnAccess);
    if (strOpt.equals("Credit")) {
      log.debug("Inside Credit");
      hmParam = GmCommonClass.getHashMapFromForm(gmMissingChargesDtlForm);
      if (!gmMissingChargesDtlForm.getInputString().equals("")) {
        log.debug("Input String: " + gmMissingChargesDtlForm.getInputString());
        gmChargesBean.saveMissingChargeDtls(hmParam);
      }
      strForward = "gmLoadApproveChargesDetails";
    }
    if (strForward.equals("")) {
      strForward = "gmApproveChargesDetails";
    }

    if (strOpt.equals("LoadReport")) {
      rdDemandSheetReport = gmChargesBean.loadMissingChargeDtls(hmParam);
      strChargesFor = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChargesFor());
      strFromDate = GmCommonClass.parseNull(gmMissingChargesDtlForm.getFromDate());
      strToDate = GmCommonClass.parseNull(gmMissingChargesDtlForm.getToDate());
      strChkTicket = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_ticket());
      strChkRepname = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_repname());
      strChkListPrice = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_listprice());
      strChkConsignment = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_consignment());
      strChkLastUpdBy = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_lastupdby());
      strChkLastUpdDt = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_lastupddt());
      strRequestID = GmCommonClass.parseNull(gmMissingChargesDtlForm.getRequestID());
      strChkAssocRepname = GmCommonClass.parseNull(gmMissingChargesDtlForm.getChk_assocrepname());
      gmMissingChargesDtlForm.setLdResult(rdDemandSheetReport.getRows());
      gmMissingChargesDtlForm.setFromDate(strFromDate);
      gmMissingChargesDtlForm.setToDate(strToDate);
      gmMissingChargesDtlForm.setInputString("");
    }
    request.setAttribute("CHK_TICKET", strChkTicket);
    request.setAttribute("CHK_REPNAME", strChkRepname);
    request.setAttribute("CHK_LSTPRICE", strChkListPrice);
    request.setAttribute("CHK_CONSIGNMENT", strChkConsignment);
    request.setAttribute("CHK_LSTUPDBY", strChkLastUpdBy);
    request.setAttribute("CHK_LSTUPDDT", strChkLastUpdDt);
    request.setAttribute("CHK_ASSOCREPNAME", strChkAssocRepname);
    return mapping.findForward(strForward);
  }

  private void getDistTypeFromRequest(HttpServletRequest request, HashMap hmParam) {
    String strDist = GmCommonClass.parseNull(request.getParameter("shwDist"));
    String shwDirRep = GmCommonClass.parseNull(request.getParameter("shwDirRep"));
    String strDistType = "";
    if (strDist.equals("true")) {
      strDistType += "1,70100|";
    }

    if (shwDirRep.equals("true")) {
      strDistType += "2,70101|";
    }
    request.setAttribute("shwDist", strDist);
    request.setAttribute("shwDirRep", shwDirRep);
    hmParam.put("DISTTYPE", strDistType);
  }
}
