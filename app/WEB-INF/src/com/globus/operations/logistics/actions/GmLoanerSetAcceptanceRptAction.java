package com.globus.operations.logistics.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.logistics.beans.GmLoanerSetAcceptanceRptBean;
import com.globus.operations.logistics.forms.GmBackOrderReleaseForm;
import com.globus.operations.logistics.forms.GmLoanerSetAcceptanceRptForm;

/**
 * @author ssharmila
 *
 */
public class GmLoanerSetAcceptanceRptAction extends GmDispatchAction{
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	public ActionForward loadLoanerSetAcceptanceDetails(ActionMapping mapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		    instantiate(request, response);
		    log.debug("loadLoanerSetAcceptanceDetails");
		    GmLoanerSetAcceptanceRptForm gmLoanerSetAcceptanceRptForm = (GmLoanerSetAcceptanceRptForm) actionForm;
		    gmLoanerSetAcceptanceRptForm.loadSessionParameters(request);
		    String strSessCompanyLocale =
		            GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
		    GmLoanerSetAcceptanceRptBean gmLoanerSetAcceptanceRptBean = new GmLoanerSetAcceptanceRptBean();
		    HashMap hmParam = new HashMap();
		    HashMap hmPendingAcpDetail = new HashMap();
		    ArrayList alPendingAcpDetail = new ArrayList();
		    ArrayList alType= new ArrayList();
		    ArrayList alStatus = new ArrayList();
		    String strOpt = "";
		    String strStatus = "";
		    String strType = "";
		    String strFrmDt="";
		    String strToDt="";
		    try
		    {
		    	 
		    		strOpt = gmLoanerSetAcceptanceRptForm.getStrOpt();
		    		strStatus= gmLoanerSetAcceptanceRptForm.getStrStatus();
		    		strType= gmLoanerSetAcceptanceRptForm.getStrType();
		    	    alType = GmCommonClass.getCodeList("LNRTYP");
			        alStatus = GmCommonClass.getCodeList("YES/NO");
					gmLoanerSetAcceptanceRptForm.setAlStatus(alStatus);
					gmLoanerSetAcceptanceRptForm.setAlType(alType);
				    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerSetAcceptanceRptForm);
				    gmLoanerSetAcceptanceRptForm.setStrStatus(strStatus);
					gmLoanerSetAcceptanceRptForm.setStrType(strType);
				   String strAppleDateFmt = getGmDataStoreVO().getCmpdfmt();
				   String strSessTodaysDate = GmCalenderOperations.getCurrentDate(strAppleDateFmt);
				   gmLoanerSetAcceptanceRptForm.setStartDtFrom(strSessTodaysDate);
				   gmLoanerSetAcceptanceRptForm.setStartDtTo(strSessTodaysDate);
				    if(strOpt.equals("reload"))
				    {
				    	   alPendingAcpDetail = gmLoanerSetAcceptanceRptBean.fetchLoanerSetAcceptanceDetails (hmParam);
						    hmParam.put("ALSETDTLS", alPendingAcpDetail);
							hmParam.put("TEMPLATENAME", "GmLoanerSetAcceptanceRpt.vm");
							hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
							String strGridXmlData = generateOutPut(alPendingAcpDetail, hmParam);
							gmLoanerSetAcceptanceRptForm.setGridXmlData(strGridXmlData);
							strFrmDt=(String)hmParam.get("STARTDTFROM");
							strToDt=(String)hmParam.get("STARTDTTO");
							gmLoanerSetAcceptanceRptForm.setStartDtFrom(strFrmDt);
							gmLoanerSetAcceptanceRptForm.setStartDtTo(strToDt);
							
				    }
				   
				    return mapping.findForward("GmLoanerSetAcceptanceRpt");
		    }catch (Exception exp)
		    {
		    	exp.printStackTrace();
		        throw new AppError(exp);	
		    }	 

	}
	
	public String generateOutPut(ArrayList alresult, HashMap hmParam)
			throws AppError {
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam
				.get("STRSESSCOMPANYLOCALE"));
		String strTempName = GmCommonClass.parseNull((String) hmParam
				.get("TEMPLATENAME"));
		templateUtil.setDataMap("hmParam", hmParam);
		templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.operations.logistics.GmLoanerSetAcceptanceRpt", strSessCompanyLocale));
		templateUtil.setDataList("alResult", alresult);
		templateUtil.setTemplateSubDir("operations/logistics/templates");
		templateUtil.setTemplateName(strTempName);
		return templateUtil.generateOutput();
	}

	
	
}
