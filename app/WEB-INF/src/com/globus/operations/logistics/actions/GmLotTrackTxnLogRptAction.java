package com.globus.operations.logistics.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.operations.logistics.beans.GmLotTrackTxnLogRptBean;
import com.globus.operations.logistics.forms.GmLotTrackTxnLogRptForm;

/**
* GmLotTrackTxnLogRptAction : Contains the methods used for the Lot Track Transaction Log Report screen
* author : Agilan Singaravel
*/
public class GmLotTrackTxnLogRptAction extends GmDispatchAction{
	
	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward loadLotTrackTxnLogDetails(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmLotTrackTxnLogRptForm gmLotTrackTxnLogRptForm = (GmLotTrackTxnLogRptForm) form;
		gmLotTrackTxnLogRptForm.loadSessionParameters(request);
		GmLotTrackTxnLogRptBean gmLotTrackTxnLogRptBean = new GmLotTrackTxnLogRptBean(getGmDataStoreVO());
		// Get the warehouse and likes dropdown list
		loadDropDownValues(gmLotTrackTxnLogRptForm);
		
		return mapping.findForward("gmLotTrackTxnRpt");
	}
	
	/**
	 * fetchLotTrackTxnLogDetails - used to fetch lot transaction log data
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotTrackTxnLogDetails(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		instantiate(request, response);
		GmLotTrackTxnLogRptForm gmLotTrackTxnLogRptForm = (GmLotTrackTxnLogRptForm) form;
		gmLotTrackTxnLogRptForm.loadSessionParameters(request);
		GmLotTrackTxnLogRptBean gmLotTrackTxnLogRptBean = new GmLotTrackTxnLogRptBean(getGmDataStoreVO());

		String strOpt = "";
		String strJSONGridData = "";
		String strPartNum = "";
		String strPartNumFormat = "";
		HashMap hmParam = new HashMap();

		hmParam = GmCommonClass.getHashMapFromForm(gmLotTrackTxnLogRptForm);
		strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		// Get the warehouse and likes dropdown list
		loadDropDownValues(gmLotTrackTxnLogRptForm);
		
		strJSONGridData = GmCommonClass.parseNull(gmLotTrackTxnLogRptBean.fetchLotTrackTxnLogDetails(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			gmLotTrackTxnLogRptForm.setGridXmlData(strJSONGridData);
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;
	}

	/**
	 * This method is used to load dropdown values for warehouse type and part search likes
	 * 
	 * @param gmLotTrackTxnLogRptForm
	 */	
	private void loadDropDownValues(GmLotTrackTxnLogRptForm gmLotTrackTxnLogRptForm) throws AppError {

		ArrayList alWareHouse = new ArrayList();
		ArrayList alPartSearch = new ArrayList();

		alWareHouse = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTTXY"));
		alPartSearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
		gmLotTrackTxnLogRptForm.setAlWarehouseType(alWareHouse);
		gmLotTrackTxnLogRptForm.setAlPartSearch(alPartSearch);
	}	  	
}
