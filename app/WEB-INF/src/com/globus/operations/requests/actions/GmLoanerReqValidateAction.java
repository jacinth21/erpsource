package com.globus.operations.requests.actions;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.forms.GmLoanerReqValidateForm;

public class GmLoanerReqValidateAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

			Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
			log.debug("Enter into GmLoanerReqValidateAction....");
			
			GmLoanerReqValidateForm gmLoanerReqValidateForm = (GmLoanerReqValidateForm) form;
			gmLoanerReqValidateForm.loadSessionParameters(request);
			
			GmRequestReportBean gmRequestReportBean = new GmRequestReportBean();
			
			String strOpt = gmLoanerReqValidateForm.getStrOpt();

			if (strOpt.equals("InvPartQty")) {
			 
				String strReqFlag = gmRequestReportBean.fetchReqfl(request.getParameter("requestId"));
				log.debug("strReqFlag == "+strReqFlag);
				response.setContentType("text/plain");
				PrintWriter pw = response.getWriter();

				if (strReqFlag.equals("N")){
					pw.write("Fail");
				}else{
					pw.write(strReqFlag);
				}
				pw.flush();
			}
		return null;//gmSetProcessConsign
	}
}
