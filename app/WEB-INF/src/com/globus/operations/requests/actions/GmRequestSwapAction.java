package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.operations.requests.forms.GmRequestSwapForm;

public class GmRequestSwapAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmRequestSwapForm gmRequestSwapForm = (GmRequestSwapForm) form;
      gmRequestSwapForm.loadSessionParameters(request);

      GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strOpt = gmRequestSwapForm.getStrOpt();
      String strRequestFrom = gmRequestSwapForm.getRequestFrom();
      String strRequestTo = gmRequestSwapForm.getRequestTo();
      String strSwapFlag = gmRequestSwapForm.getSwapflag();
      String strUserId = gmRequestSwapForm.getUserId();

      RowSetDynaClass rdResult = null;
      RowSetDynaClass rdToResult = null;
      HashMap hmParam = new HashMap();

      ArrayList alLogReasons = new ArrayList();

      HashMap hmReturn = new HashMap();

      hmParam = GmCommonClass.getHashMapFromForm(gmRequestSwapForm);
      
      hmParam.put("SKIP_RQ_VOID_VALIDATION", "N");
      
      if (strOpt.equals("save")) {
        gmRequestTransBean.saveSwapRequest(hmParam);
        strOpt = "reload";
        gmRequestSwapForm.setSwapflag("Y");
        //PC-2319: After swap to exclude the RQ validation
        
		// After the Request Swap is done, system will void the Source
		// RQ. Since the show the Source RQ info in the screen and its
		// voided, we are skipping the RQ validation in this instance
        
        hmParam.put("SKIP_RQ_VOID_VALIDATION", "Y");
      }

      if (strOpt.equals("reload")) {

        hmReturn = gmRequestTransBean.fetchReqdetail(hmParam);
        rdResult = (RowSetDynaClass) hmReturn.get("REQUESTSFROM");
        rdToResult = (RowSetDynaClass) hmReturn.get("REQUESTSTO");


        gmRequestSwapForm.setReturnList(rdResult.getRows());
        gmRequestSwapForm.setChildList(rdToResult.getRows());
        // gmLockTagForm.setMaStatus(GmCommonClass.parseNull((String) hmReturn.get("MASTATUS")));
      }

      // alLogReasons = gmCommonBean.getLog(strRequestFrom,"1235");
      // gmRequestSwapForm.setAlLogReasons(alLogReasons);

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }
}
