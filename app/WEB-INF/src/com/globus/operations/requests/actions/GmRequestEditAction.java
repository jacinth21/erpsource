package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.operations.requests.forms.GmRequestEditForm;

public class GmRequestEditAction extends GmRequestAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    String strActForward = "";

    try {
      GmRequestEditForm gmRequestEditForm = (GmRequestEditForm) form;
      gmRequestEditForm.loadSessionParameters(request);
      GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
      GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
      // Code to get the Party Access Condition.
      HttpSession session = request.getSession(false);
      HashMap hmPartyParam = new HashMap();
      HashMap hmAccess = new HashMap();
      String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
      hmPartyParam.put("PartyId", session.getAttribute("strSessPartyId"));
      hmPartyParam.put("FunctionId", "1015");// Corresponds to Demand Sheet Access Function Name
      String strApplnDateFmt =
          GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
      HashMap hmPartyAccess = (new GmAccessFilter()).fetchPartyAccessPermission(hmPartyParam);

      String strUpdateFlg = GmCommonClass.parseNull((String) hmPartyAccess.get("UPDFL"));
      String strVoidFlg = GmCommonClass.parseNull((String) hmPartyAccess.get("VOIDFL"));
      String strReadFlg = GmCommonClass.parseNull((String) hmPartyAccess.get("READFL"));

      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "UPD_PRSCONG_OUS_ACC"));
      String strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("UPD_PRSCONG_OUS_REPLE_ACC", strUpdateAccess);
      // If the party is not having update access for the the function, the will have limited access
      // in Demand Sheet
      // summary and in Monthly demand sheet.
      String strLimitedAccess = "";
      if (strUpdateFlg != "" && !strUpdateFlg.equalsIgnoreCase("Y")) {
        strLimitedAccess = "true";
        request.setAttribute("DMNDSHT_LIMITED_ACC", strLimitedAccess);
      }



      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      String strOpt = gmRequestEditForm.getStrOpt();
      String strRequestId = gmRequestEditForm.getRequestId();

      ArrayList alRequestDetaillist = new ArrayList();

      HashMap hmParam = new HashMap();
      HashMap hmReturnheader = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmAsscHReturn = new HashMap();
      ArrayList alAsscDlist = new ArrayList();

      ArrayList alDistList = new ArrayList();
      ArrayList alEmpList = new ArrayList();
      ArrayList alAccList = new ArrayList();
      // Loading the Request Attribute Values
      ArrayList alAttribute = new ArrayList();
      alAttribute = GmCommonClass.getCodeList("CNATTB");
      gmRequestEditForm.setAlAttribute(alAttribute);
      // Fetch the Customer PO value by passing the Customer PO type 1006420
      String strAttbVal =
          GmCommonClass.parseNull(gmRequestReportBean
              .fetchRequestAttribute(strRequestId, "1006420"));
      gmRequestEditForm.setCustPo(strAttbVal);
      String strRequestPurpose = GmCommonClass.parseNull(gmRequestEditForm.getStrRequestPurpose());
      if (!strUpdateAccess.equals("Y") && strRequestPurpose.equals("4000097")) { // OUS SALES
                                                                                 // REPLENISHMENT
        throw new AppError("", "20594");
      }

      if (strOpt.equals("save")) {
        hmParam = GmCommonClass.getHashMapFromForm(gmRequestEditForm);
        hmParam.putAll(loadShipReqParams(request));
        // loadShipReqParams(request, hmParam);
        log.debug(" values inside hmParam " + hmParam);
        gmRequestTransBean.saveRequestEditInfo(hmParam);
        String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
        GmJasperReport gmJasperReport = new GmJasperReport();
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        gmTxnSplitBean.sendVendorEmail(strReqId, strApplnDateFmt, request.getSession()
            .getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION")),
            gmJasperReport);
        gmRequestEditForm.setStrOpt("fetch");
      }
      alRequestDetaillist = gmRequestReportBean.fetchRequestInfoForEdit(strRequestId);
      setFormHeader(gmRequestEditForm);

      hmReturn = gmRequestReportBean.fetchRequestAsscTxn(strRequestId);
      hmAsscHReturn = (HashMap) hmReturn.get("HMASSCHEADER");
      alAsscDlist = (ArrayList) hmReturn.get("ALASSCDETAIL");

      log.debug("alAsscDlist is " + alAsscDlist);
      log.debug("alRequestDetaillist is " + alRequestDetaillist);
      String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
	 String	 strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "ITEMINITFILTER")) ;
      HashMap hmFilters = new HashMap();
	    hmFilters.put("FILTER", "Active");
	    hmFilters.put("USERCODE", "300");
	    hmFilters.put("DEPT", "");
	    hmFilters.put("STATUS", "311");    
	    if(strCompanyPlant.equals("Plant")){
		    hmFilters.put("DISTFILTER", "COMPANY-PLANT");
		    hmFilters.put("REPFILTER", "COMPANY-PLANT");
		    hmFilters.put("EMPFILTER", "COMPANY-PLANT");
	    }	

      alDistList = GmCommonClass.parseNullArrayList(gmCust.getDistributorList(hmFilters));
      alEmpList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmFilters));

      String strRequestFor = gmRequestEditForm.getReqFor();

      if (strRequestFor.equals("40021") || strRequestFor.equals("102930")) // 40021 - Consignment
                                                                           // 102930 - ICT
      {
        gmRequestEditForm.setAlRequestTo(alDistList);

      }
      if (strRequestFor.equals("40022")) // 40022 - InHouse
      {
        gmRequestEditForm.setAlRequestTo(alEmpList);
      }
      if (strRequestFor.equals("40025")) // 40022 - InHouse
      {
        alAccList = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
        gmRequestEditForm.setAlRequestTo(alAccList);
      }
      log.debug("strRequestFor is " + strRequestFor);

      String strStatus = gmRequestEditForm.getRequestStatus();
      gmRequestEditForm.setStatus(strStatus);

      ArrayList alShipTo = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONSP"));
      gmRequestEditForm.setAlRequestShipTo(alShipTo);

      ArrayList alRepList = GmCommonClass.parseNullArrayList(gmCust.getRepList(hmFilters));
      ArrayList alAccountList = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
      String strShipTo = gmRequestEditForm.getRequestShipTo();
      // 4020 - Distorbutor
      gmRequestEditForm.setAlShipDistributor(alDistList);

      // 4021 - Sales Rep
      gmRequestEditForm.setAlShipSalesRep(alRepList);


      // 4022 - Hospital --todo to get hospital list
      gmRequestEditForm.setAlShipHospital(alAccountList);

      // 4023 - Employee
      gmRequestEditForm.setAlShipEmployee(alEmpList);


      log.debug("shipto is........... " + strShipTo);
      gmRequestEditForm.setAlAsscDlist(alAsscDlist);

      gmRequestEditForm.setChildrequestId(GmCommonClass.parseNull((String) hmAsscHReturn
          .get("REQID")));
      gmRequestEditForm.setStatus(GmCommonClass.parseNull((String) hmAsscHReturn.get("STATUS")));

      gmRequestEditForm.setAlRequestDetaillist(alRequestDetaillist);

    } catch (Exception exp) {

      exp.printStackTrace();
      throw new AppError(exp);
    }
    strActForward = GmCommonClass.parseNull(request.getParameter("FORWARD"));
    strActForward = strActForward.equals("") ? "success" : strActForward;
    return mapping.findForward(strActForward);
  }
}
