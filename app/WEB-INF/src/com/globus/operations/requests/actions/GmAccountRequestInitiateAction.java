package com.globus.operations.requests.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.requests.beans.GmAccountRequestTransBean;
import com.globus.operations.requests.forms.GmAccountRequestInitiateForm;

public class GmAccountRequestInitiateAction extends GmAccountRequestAction {

  HashMap hmValues = new HashMap();

  GmCustomerBean gmCustomerBean = new GmCustomerBean();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      GmAccountRequestInitiateForm gmRequestInitiateForm = (GmAccountRequestInitiateForm) form;
      gmRequestInitiateForm.loadSessionParameters(request);
      GmAccountRequestTransBean gmRequestTransBean =
          new GmAccountRequestTransBean(getGmDataStoreVO());
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmRuleEngine gmRuleEngine = new GmRuleEngine(getGmDataStoreVO());
      String strMaterialRequestId = "";
      ArrayList alResult = new ArrayList();
      String strOpt = gmRequestInitiateForm.getStrOpt();
      HashMap hmPutGroupId = new HashMap();
      HashMap hmruleShipVal = new HashMap();
      GmCommonBean gmcommon = new GmCommonBean(getGmDataStoreVO());
      HashMap hmParam = GmCommonClass.getHashMapFromForm(gmRequestInitiateForm);
      String strConsignmentType = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTTYPE"));
      /*
       * alResult = gmCust.getRepList("Active"); gmRequestInitiateForm.setAlRepList(alResult);
       * 
       * alResult = gmSales.reportAccount(); gmRequestInitiateForm.setAlAccountList(alResult);
       */
      String strRepId = GmCommonClass.parseNull(request.getParameter("Cbo_LoanToRep"));
      String strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_LoanToAcc"));
      String strAssocRepId = GmCommonClass.parseNull(request.getParameter("Cbo_LoanToASSORep"));
      hmParam.put("REPID", strRepId);
      hmParam.put("ACCOUNTID", strAccId);
      hmParam.put("ASSOCREPID", strAssocRepId);

      gmRequestInitiateForm.setDistList(GmCommonClass.getRuleValue("DISTLIST", "ICTSAMPLE"));
      // code merge from OUS side , this is for surgery details
      ArrayList alAttrib = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LNATB"));
      gmRequestInitiateForm.setAlAttrib(alAttrib);
      // this is for ship to, carrier and mode
      hmPutGroupId.put("RULEID", "SHIP_CARR_MODE_VAL");
      hmruleShipVal = gmcommon.fetchTransactionRules(hmPutGroupId,getGmDataStoreVO());// ship carrier and mode default
                                                                   // value get from rule
      gmRequestInitiateForm.setHmReturn(hmruleShipVal);
      if (strOpt.equals("save")) {
        ArrayList alReturn = new ArrayList();
        alReturn =
            GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("CONSEQUENCES"));
        hmParam.putAll(loadShipReqParams(request));
        hmParam.putAll(loadAttribValue(request));
        String strSurDate = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDATE"));
        String strApplDateFmt = gmRequestInitiateForm.getApplnDateFmt();
        Date dtSurDate = GmCommonClass.getStringToDate(strSurDate, strApplDateFmt);
        hmParam.put("REQUIREDDATE", dtSurDate);
        hmParam.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        GmJasperReport gmJasperReport = new GmJasperReport();
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        hmParam.put("JASPEROBJ", gmJasperReport);
        log.debug("hmParam ::: " + hmParam);
        strMaterialRequestId = gmRequestTransBean.saveProductRequest(hmParam, alReturn);
      }
      if(strOpt.equals("fetchLoanerRefDetails")){ //PMT-32450 - To Validate Loaner Request ID
    	  String strLoanRefIDMSg ="";
    	  hmParam = GmCommonClass.getHashMapFromForm(gmRequestInitiateForm);
    	  strLoanRefIDMSg = gmRequestTransBean.fetchLoanerRefDetails(hmParam);
    	   PrintWriter pw = response.getWriter();
    	   if (!strLoanRefIDMSg.equals("")) {
			response.setContentType("text/xml");
			pw.write(strLoanRefIDMSg);	
    	   }
	    	pw.flush();
			return null;
      }
      gmRequestInitiateForm.setHfromPage("PRODUCTREQUEST");
      setArrayListValues(gmRequestInitiateForm, request);
      if (strConsignmentType.equals("4127") && !strMaterialRequestId.equals("")) {
        return actionRedirect("/gmOprLoanerReqEdit.do?strOpt=load&requestId=" + strMaterialRequestId, request);

        // request.setAttribute("PrdReqId", strMaterialRequestId);
        // return mapping.findForward("successrpt");
        // 102930 - Inter Company Transfer , 40021 - Consignment and 40022 - In-House Consignment
      } else if ((strConsignmentType.equals("102930") || strConsignmentType.equals("40021")
          || strConsignmentType.equals("40025") || strConsignmentType.equals("40022"))
          && !strMaterialRequestId.equals("")) {
        return actionRedirect("/gmRequestMaster.do?strOpt=GIMS&requestId=" + strMaterialRequestId,request);
      }
    } catch (AppError appError) {
      request.setAttribute("hType", appError.getType());
      throw appError;
    } catch (Exception exp) {

      exp.printStackTrace();
      throw new AppError(exp);
    }


    return mapping.findForward("success");
  }
}
