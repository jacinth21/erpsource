/**
 * 
 */
package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.beans.GmStockInitiateBean;
import com.globus.operations.requests.forms.GmRequestMasterForm;
import com.globus.operations.requests.forms.GmStockInitiateForm;

/**
 * @author ppandiyan
 *
 */
public class GmStockInitiateAction extends GmRequestAction{
  Logger log = GmLogger.getInstance(this.getClass().getName());
  /**
   * below execute method is used to create, save and reconfigure the Stock Transfer Initiate Request 
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return findforward mapping
   */
  public ActionForward execute(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {
            instantiate(request, response);
            GmStockInitiateForm gmStockInitiateForm = (GmStockInitiateForm) form;
            gmStockInitiateForm.loadSessionParameters(request);
            HashMap hmParam = new HashMap();
            hmParam = GmCommonClass.getHashMapFromForm(gmStockInitiateForm);
           
            
            GmStockInitiateBean gmStockInitiateBean = new GmStockInitiateBean(getGmDataStoreVO());
            GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
            GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
            String strRedirectPath = "";
            String strForward = "gmStockInitiate";
            String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
            String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
            String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
            String strActForward = GmCommonClass.parseNull(request.getParameter("RE_FORWARD"));
            ArrayList alFulfillCompany = new ArrayList();
            ArrayList alPlantList = new ArrayList();
            String strOpt = GmCommonClass.parseNull((String)gmStockInitiateForm.getStrOpt());

            GmCommonBean gmcommon = new GmCommonBean(getGmDataStoreVO());
       
            String strRequestType = GmCommonClass
                    .parseNull((String) hmParam.get("REQUESTTYPE"));
            
            
            //to get the fulfill company
            alFulfillCompany = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchAssocCompMap());
            gmStockInitiateForm.setAlFulfillCompany(alFulfillCompany);
            
            //to get the plant list associated to the company
            alPlantList = GmCommonClass.parseNullArrayList(gmCommonBean.loadPlantCompanyMapping());
            gmStockInitiateForm.setAlAssocPlantList(alPlantList);
            //to create a request
            if (strOpt.equals("save")) {
                ArrayList alReturn = new ArrayList();
                alReturn = GmCommonClass.parseNullArrayList((ArrayList) request
                        .getAttribute("CONSEQUENCES"));
                hmParam.putAll(loadShipReqParams(request));
           
                String strSurDate = GmCommonClass.parseNull((String) hmParam
                        .get("REQUIREDDATE"));
                String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
                Date dtSurDate = GmCommonClass.getStringToDate(strSurDate,
                        strApplDateFmt);
                hmParam.put("REQUIREDDATE", dtSurDate);

                strRequestId = gmStockInitiateBean.saveStockRequest(
                        hmParam, alReturn);
                if( strRequestType.equals("26240420") && !strRequestId.equals("")) { //for summary print

                      strRedirectPath = "/gmRequestMaster.do?strOpt=GIMS&reqFor="+strRequestType+"&requestId="
                             + strRequestId;
                     return actionRedirect(strRedirectPath, request);
                  }

            }
            
         
            request.setAttribute("screentyp", strActForward);
            
            if (strOpt.equals("saveReconfig")) { //to save reconfigure request
            
              gmStockInitiateBean.saveReconfigureRequest(strRequestId, strInputString, strUserId);
            
                 strRedirectPath = "/gmRequestDetail.do?requestId="
                    + strRequestId;
            return actionRedirect(strRedirectPath, request);
            }
           
    return mapping.findForward(strForward);
    
  }
}
