package com.globus.operations.requests.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmShippingAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.forms.GmAccountRequestInitiateForm;
import com.globus.operations.requests.forms.GmRequestForm;

public class GmAccountRequestAction extends GmShippingAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.
  HashMap hmData = new HashMap();


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    String strActForward = "";
    try {
      GmRequestForm gmRequestForm = (GmRequestForm) form;
      gmRequestForm.loadSessionParameters(request);
      Logger log = GmLogger.getInstance(this.getClass().getName());
      String strOpt = gmRequestForm.getStrOpt();

      log.debug(" opt " + strOpt);
      setFormHeader(gmRequestForm);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    strActForward = GmCommonClass.parseNull(request.getParameter("RE_RE_FORWARD"));
    strActForward = strActForward.equals("") ? "success" : strActForward;
    return mapping.findForward(strActForward);
  }



  public void setFormHeader(GmRequestForm gmRequestForm) throws AppError {
    GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
    String strRequestId = gmRequestForm.getRequestId();
    if (strRequestId.equals("")) {
      String strConID = gmRequestForm.getConsignID();
      strRequestId = GmCommonClass.parseNull(gmRequestReportBean.fetchRequestId(strConID));
      gmRequestForm.setRequestId(strRequestId);
    }
    if (strRequestId != null && !strRequestId.equals("")) {
      hmData = gmRequestReportBean.fetchRequestHeader(strRequestId);
    }
    gmRequestForm = (GmRequestForm) GmCommonClass.getFormFromHashMap(gmRequestForm, hmData);
  }

  public void setArrayListValues(GmAccountRequestInitiateForm gmRequestInitiateForm,
      HttpServletRequest request) throws AppError {
    // ArrayList alDistributorList = gmCustomerBean.getDistributorList("Active");
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    String strTodaysDate;
    String strApplDate = "";
    strTodaysDate = (String) request.getSession().getAttribute("strSessTodaysDate");
    strApplDate = (String) request.getSession().getAttribute("strSessApplDateFmt");
    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);

    // Add 5 days to the todays date if the dept code is 2014
    Calendar now = Calendar.getInstance();
    Calendar calendar;
    SimpleDateFormat formatter = new SimpleDateFormat(strApplDate);
    calendar = (Calendar) now.clone();
    calendar.add(Calendar.DAY_OF_YEAR, +7);
    String strExpRetnDate = formatter.format(calendar.getTime());
    // log.debug("the today's date +5 days  is "+strExpRetnDate );

    if (gmRequestInitiateForm.getDeptId().equals("2014")) {
      gmRequestInitiateForm.setRequiredDate(strExpRetnDate);
    }
    // Add 5 days to the todays date if the dept code is 2014

    String strFromPage = gmRequestInitiateForm.getHfromPage();

    gmRequestInitiateForm.setDepartmentName(GmCommonClass.getCodeAltName(gmRequestInitiateForm
        .getDeptId()));
    /*
     * Get only Consignment and InHouse values in the list
     */
    List ltTemp = new ArrayList();
    ltTemp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DMDTP"));
    ltTemp = ltTemp.subList(3, 4);
    ArrayList alConsignmentType = new ArrayList(ltTemp);
    ArrayList alContyList = new ArrayList();
    /*
     * if(strFromPage.equals("PRODUCTREQUEST")){ if(strCountryCode.equals("en")){
     * alConsignmentType.add
     * (GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONTY")).get(3)); // adding
     * Product Loaner }else{ alContyList =
     * GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONTY","102924")); // 102924 -
     * Master Request Type contains Product Loaner and Inter Company Transfer
     * alConsignmentType.addAll(alContyList); } }
     */

    /*
     * Add GlobusMed In-House to the Distributor List
     */
    ArrayList alDistributorList =
        GmCommonClass.parseNullArrayList(gmCustomerBean.getDistributorList("Active"));
    HashMap hmTemp = new HashMap();
    hmTemp.put("ID", "01");
    hmTemp.put("NAME", "Globus Med In-House");
    alDistributorList.add(0, hmTemp);

    /*
     * Use only 3 values from ShipTo List
     */
    ArrayList alShipTo = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONSP"));

    // alShipTo.remove(2); // remove Hospital
    // alShipTo.remove(2); // remove N/A
    String strhaction = gmRequestInitiateForm.getHaction();

    HashMap hmPur = new HashMap();

    ltTemp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COPUR"));
    ltTemp = ltTemp.subList(0, 1);
    ArrayList alConsignmentPurpose = new ArrayList(ltTemp);
    ArrayList alInHousePurpose =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONPU"));
    ArrayList alRepList = GmCommonClass.parseNullArrayList(gmCustomerBean.getRepList("Active"));
    ArrayList alAccountList = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
    ArrayList alEmployeeList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList("300", ""));

    hmPur.put("INHOUSEPUR", alInHousePurpose);
    hmPur.put("CONSIGNPUR", alConsignmentPurpose);
    gmRequestInitiateForm.setHmPurposes(hmPur);

    gmRequestInitiateForm.setAlConsignmentType(alConsignmentType);
    gmRequestInitiateForm.setAlShipTo(alShipTo);
    // 40022 In-house consignment
    if (strhaction.equals("40022")) {
      gmRequestInitiateForm.setAlInHousePurpose(alInHousePurpose);
    } else {
      gmRequestInitiateForm.setAlInHousePurpose(alConsignmentPurpose);
    }
    ArrayList alAttribute = new ArrayList();
    alAttribute = GmCommonClass.getCodeList("CNATTB");
    gmRequestInitiateForm.setAlAttribute(alAttribute);
    gmRequestInitiateForm.setAlDistributorList(alDistributorList);
    gmRequestInitiateForm.setAlRepList(alRepList);
    gmRequestInitiateForm.setAlAccountList(alAccountList);
    gmRequestInitiateForm.setAlEmployeeList(alEmployeeList);
  }
}
