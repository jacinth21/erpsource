package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.operations.requests.beans.GmMaterialReqRptBean;
import com.globus.operations.requests.forms.GmRequestInitiateForm;

public class GmRequestInitiateAction extends GmRequestAction {

	HashMap hmValues = new HashMap();
	GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

	// Initialize
	// the
	// Logger
	// Class.

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		instantiate(request, response);
		try {
			GmRequestInitiateForm gmRequestInitiateForm = (GmRequestInitiateForm) form;
			gmRequestInitiateForm.loadSessionParameters(request);
			GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(
					getGmDataStoreVO());
			GmMaterialReqRptBean gmMasterialReqRptBean = new GmMaterialReqRptBean(getGmDataStoreVO());
			GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
			GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
			GmRuleEngine gmRuleEngine = new GmRuleEngine(getGmDataStoreVO());
			String strMaterialRequestId = "";
			String strActForward = GmCommonClass.parseNull(request.getParameter("RE_FORWARD"));
			String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
			ArrayList alResult = new ArrayList();
			String strOpt = gmRequestInitiateForm.getStrOpt();
			HashMap hmPutGroupId = new HashMap();
			HashMap hmruleShipVal = new HashMap();
			GmCommonBean gmcommon = new GmCommonBean(getGmDataStoreVO());
		      
			log.debug(" opt " + strOpt + " inputstribng "
					+ gmRequestInitiateForm.getHinputString());
			
			log.debug(" strActForward " + strActForward);
			HashMap hmParam = GmCommonClass
					.getHashMapFromForm(gmRequestInitiateForm);
			String strConsignmentType = GmCommonClass
					.parseNull((String) hmParam.get("CONSIGNMENTTYPE"));
			log.debug(" strConsignmentType " + strConsignmentType);

			alResult = gmCust.getRepList("Active");
			gmRequestInitiateForm.setAlRepList(alResult);

			alResult = gmSales.reportAccount();
			gmRequestInitiateForm.setAlAccountList(alResult);

			String strRepId = GmCommonClass.parseNull(request
					.getParameter("Cbo_LoanToRep"));
			String strAccId = GmCommonClass.parseNull(request
					.getParameter("Cbo_LoanToAcc"));
			String strAssocRepId = GmCommonClass.parseNull(request
					.getParameter("Cbo_LoanToASSORep"));

			hmParam.put("REPID", strRepId);
			hmParam.put("ACCOUNTID", strAccId);
			hmParam.put("ASSOCREPID", strAssocRepId);

			gmRequestInitiateForm.setDistList(GmCommonClass
					.parseNull(GmCommonClass.getRuleValue("DISTLIST",
							"ICTSAMPLE")));
			// code merge from OUS side , this is for surgery details
			ArrayList alAttrib = GmCommonClass.parseNullArrayList(GmCommonClass
					.getCodeList("LNATB"));
			gmRequestInitiateForm.setAlAttrib(alAttrib);
			// this is for ship to, carrier and mode
			hmPutGroupId.put("RULEID", "SHIP_CARR_MODE_VAL");
			hmruleShipVal = gmcommon.fetchTransactionRules(hmPutGroupId,
					getGmDataStoreVO());// ship carrier and mode default
			// value get from rule
			gmRequestInitiateForm.setHmReturn(hmruleShipVal);
			if (strOpt.equals("save")) {
				ArrayList alReturn = new ArrayList();
				alReturn = GmCommonClass.parseNullArrayList((ArrayList) request
						.getAttribute("CONSEQUENCES"));
				hmParam.putAll(loadShipReqParams(request));
				hmParam.putAll(loadAttribValue(request));
				hmParam.putAll(loadTissueShipReqParams(request));
				String strSurDate = GmCommonClass.parseNull((String) hmParam
						.get("REQUIREDDATE"));
				String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
				Date dtSurDate = GmCommonClass.getStringToDate(strSurDate,
						strApplDateFmt);
				hmParam.put("REQUIREDDATE", dtSurDate);
				hmParam.put(
						"SUBREPORT_DIR",
						request.getSession()
								.getServletContext()
								.getRealPath(
										GmCommonClass
												.getString("GMJASPERLOCATION")));
				GmJasperReport gmJasperReport = new GmJasperReport();
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				hmParam.put("JASPEROBJ", gmJasperReport);
				hmParam.put("COMPANYINFO", strCompanyInfo);
				//PMT-45058 Ability to link Loaner Request to Item consignment transaction
				String strLoanerRequestID = GmCommonClass.parseNull((String)gmRequestInitiateForm.getLoanerReqID());
			    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
				strMaterialRequestId = gmRequestTransBean.saveProductRequest(
						hmParam, alReturn);
				if(strLoanerRequestID != ""){
				 gmMasterialReqRptBean.saveItemLoanerRequest(strLoanerRequestID,strMaterialRequestId,strUserId);//PMT-45058
				}
			}

			gmRequestInitiateForm.setHfromPage("PRODUCTREQUEST");
			setArrayListValues(gmRequestInitiateForm, request);
			request.setAttribute("screentyp", strActForward);
			if (strConsignmentType.equals("4127")
					&& !strMaterialRequestId.equals("")) {
				// ActionForward actionForward =
				// mapping.findForward("editsuccess");
				// ActionForward itemForward = new ActionForward(actionForward);
				// itemForward.setPath("/gmOprLoanerReqEdit.do?strOpt=load&requestId="
				// + strMaterialRequestId);
				// itemForward.setRedirect(true);
				String strRedirectPath = "/gmOprLoanerReqEdit.do?strOpt=load&requestId="
						+ strMaterialRequestId;
				return actionRedirect(strRedirectPath, request);

				// request.setAttribute("PrdReqId", strMaterialRequestId);
				// return mapping.findForward("successrpt");
				// 102930 - Inter Company Transfer , 40021 - Consignment and
				// 40022 - In-House Consignment
			} else if ((strConsignmentType.equals("102930")
					|| strConsignmentType.equals("40021") || strConsignmentType
						.equals("40022")) && !strMaterialRequestId.equals("")) {
				/*
				 * ActionForward itemForward = new ActionForward();
				 * itemForward.setPath
				 * ("/gmRequestMaster.do?strOpt=GIMS&requestId=" +
				 * strMaterialRequestId); itemForward.setRedirect(true); return
				 * itemForward;
				 */
				String strRedirectPath = "/gmRequestMaster.do?strOpt=GIMS&requestId="
						+ strMaterialRequestId;
				return actionRedirect(strRedirectPath, request);
			}
		} catch (AppError appError) {
			request.setAttribute("hType", appError.getType());
			throw appError;
		} catch (Exception exp) {

			exp.printStackTrace();
			throw new AppError(exp);
		}

		return mapping.findForward("success");
	}
}
