package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.forms.GmSetBuildProcessForm;

public class GmSetBuildProcessAction extends GmRequestAction{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
    	instantiate(request, response);
    	ArrayList alConsignmentDetails = new ArrayList();
    	String strForward = "success";
    	String strOpt = "";
    	GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
    	GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    	HttpSession session = request.getSession(false);
    	String strOption=request.getParameter("strOption")==null?"":request.getParameter("strOption");
    	//String msg = "";
    	try {
        	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
        	GmSetBuildProcessForm gmSetBuildProcessForm = (GmSetBuildProcessForm)form;
	        gmSetBuildProcessForm.loadSessionParameters(request);
	        GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(getGmDataStoreVO());
	        GmCommonClass gmCommonClass = new GmCommonClass();
        	ActionMessages verifyMessages = new ActionMessages();
        	String strApplnDateFmt = gmSetBuildProcessForm.getApplnDateFmt();
        	String strUserId = 	GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
        	ArrayList alTypeList  = new ArrayList();
        	//Request Comes from RuleEngine once fetch the rules for setbuildproess
        	//Removing all the parts from Consequence if rule created only for SET and keep the Rule Id with part number as SET
        	if(strOption.equals("rule"))
         	{
         		ArrayList alConsequences=GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSEQUENCES"));
         		
         		if(alConsequences.size()>0) 
         		{
         			HashMap hmRuleID=GmCommonClass.parseNullHashMap(gmRequestReportBean.findRuleID(GmCommonClass.parseNull((String)request.getAttribute("SETID"))));
         			 
         			if(hmRuleID.size()>0)
         			{
         				HashMap hmAvailableRule,hmSetID=new HashMap();
         				int j=0;
         				Iterator itrTmpList = alConsequences.iterator();
         				
         				while(itrTmpList.hasNext())
         				{
         					hmAvailableRule = GmCommonClass.parseNullHashMap((HashMap)itrTmpList.next());   
         			    	String strRuleId = GmCommonClass.parseNull((String)hmAvailableRule.get("ID"));
         			    	if(strRuleId.equals(hmRuleID.get("RULEID")))
         			    	{        			    		
         			    		if(j==0)
         			    		{	
         			    			//adding only one rule for SET
         			    			hmAvailableRule.put("PNUM","SET");
         			    			hmSetID=new HashMap(hmAvailableRule);
         			    			j++;
         			    		}
         			    		//removing the rules
         			    			itrTmpList.remove();
         			         }
         			    }
         				if(hmSetID.size()>0)
         				{
         				alConsequences.add(hmSetID);
         				}
         			}
         		}
         		//setting the form information
         		setFormHeader(gmSetBuildProcessForm);
         		//setting the rule info
         		request.setAttribute("CONSEQUENCES",alConsequences);
         		request.setAttribute("OVERRIDE_ACCESS", request.getAttribute("OVERRIDE_ACCESS"));
         		request.setAttribute("SHOWLINK",request.getAttribute("SHOWLINK"));
         		return mapping.findForward(strForward);
         		
         	}
        	
        	
        	
	        strOpt = GmCommonClass.parseNull(gmSetBuildProcessForm.getStrOpt()); 
	        String strRequestId = GmCommonClass.parseNull(gmSetBuildProcessForm.getRequestId()); 
	        String strConsignmentId = GmCommonClass.parseNull(gmSetBuildProcessForm.getConsignID()); 
	        String strRefConsignmentId = GmCommonClass.parseNull(gmSetBuildProcessForm.getReloadConsignmentId()); 
	        log.debug("strOpt===>"+strOpt);
	        if(strOpt.equals("VMAIL")){
	        	GmJasperReport gmJasperReport = new GmJasperReport();
	        	gmJasperReport.setRequest(request);
	        	gmJasperReport.setResponse(response);
	        	gmTxnSplitBean.sendVendorEmail(strRequestId,strApplnDateFmt,request.getSession().getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION")),gmJasperReport);
	        }
	        HashMap hmParam = GmCommonClass.getHashMapFromForm(gmSetBuildProcessForm);
	        
	        List rdBackOrderDetails = new ArrayList();
	        int daysDiff = 0;
	        int daysDiffRule = 0;
	        Double consignmentStatusFlag = 0.0;
	        GmAccessControlBean gmAccessControlBean=new GmAccessControlBean();
	        String strPartyId=GmCommonClass.parseNull(gmSetBuildProcessForm.getUserId());
	        ArrayList alUserList= GmCommonClass.parseNullArrayList(gmAccessControlBean.getPartyList("RLBCKSET",strPartyId));
	        String strAccess="N";
	       	if(alUserList.size()>0){
	        	strAccess="Y";
	       	}
	       	
	        //gmSetBuildProcessForm.setAccessFl(strAccess);
	       	gmSetBuildProcessForm.setRollBackAccessFl(strAccess);
	        if (strConsignmentId.equals("") || !strRequestId.equals(""))
	        {
	        	strConsignmentId = GmCommonClass.parseNull(gmRequestReportBean.fetchConsignmentIdFromMR(strRequestId));
	        	gmSetBuildProcessForm.setConsignID(strConsignmentId);
	        }
        
	        if(strOpt.equals("save")){
	        	//I am commenting below lines since setter method is not there in form and also the method is not returning anything
	        	//msg = gmLogisticsBean.saveSetMaster(hmParam);
	        	//gmSetBuildProcessForm.setMessage(msg);
	        	gmLogisticsBean.saveSetMaster(hmParam);
	        }
	        
	        setFormHeader(gmSetBuildProcessForm);
	        strRequestId = gmSetBuildProcessForm.getRequestId();

	        // When submit with selectSet Build Process and OUS Sales Replenishment need access.
	        String strRequestPurpose = GmCommonClass.parseNull(gmSetBuildProcessForm.getStrRequestPurpose());
	        HashMap hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,"OUS_SAL_REP_STBL_ACC"));
            String strUpdateAccess = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
            String strReadAccess = GmCommonClass.parseNull((String)hmAccess.get("READFL"));
            String strVoidAccess = GmCommonClass.parseNull((String)hmAccess.get("VOIDFL"));
            log.debug("strRequestPurpose::"+strRequestPurpose+":::"+strUpdateAccess+"::"+strUserId);
            alTypeList = gmCommonClass.getCodeList("CONTY","102926",getGmDataStoreVO()); // 102926 - Master Process Loaner
            gmSetBuildProcessForm.setAlTypeList(alTypeList);           
	        if (strRequestPurpose.equals("4000097") && !strUpdateAccess.equals("Y")){
        		throw new AppError("","20595");
	        }
	        
	        if(!strRequestId.equals("")){
	        	gmRequestReportBean.validateRequest(strRequestId, "SETBUILDPROCESS");
	        }
	       
	        
	        	alConsignmentDetails = GmCommonClass.parseNullArrayList(gmRequestReportBean.fetchConsignmentDetail(strConsignmentId, strRefConsignmentId));
		        rdBackOrderDetails = gmLogisticsBean.fetchBackOrderReport(strRequestId);
		        String strCounter = String.valueOf(alConsignmentDetails.size());
		        gmSetBuildProcessForm.setRdBackOrderDetails(rdBackOrderDetails);
		        gmSetBuildProcessForm.setAlConsignmentDetail(alConsignmentDetails);
		        request.setAttribute("HCOUNTER", strCounter);

		      if(!gmSetBuildProcessForm.getRequiredDate().equals("")){
		        daysDiff = Integer.parseInt(gmSetBuildProcessForm.getDaysDiff());
		        daysDiffRule = Integer.parseInt(gmSetBuildProcessForm.getDaysDiffRule());   
		 
		        if(daysDiffRule != 0 && daysDiff <= daysDiffRule){
			        ActionMessage requireDateCntDown = new ActionMessage("message.setbuild.countdown");
			        if(daysDiff < 0)
			        requireDateCntDown = new ActionMessage("message.setbuild.countdown.overdue");
			        if(daysDiff == 0)
			        requireDateCntDown = new ActionMessage("message.setbuild.countdown.today");
		        	verifyMessages.add("requireDateCntDown",requireDateCntDown);
		        	saveMessages(request,verifyMessages);
		        }
		      }
		        if(gmSetBuildProcessForm.getVerifyFlag().equals("1")){
		 
		        	ActionMessage verifiedMsg = new ActionMessage("message.setbuild.verified");
		        	verifyMessages.add("verifyMessage",verifiedMsg);
		        	//verifyMessage.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.setbuild.verified","This consignment has been verified and taken out of Inventory. You must NOT make any more changes to the contents of this set. If you do need to make changes, please contact the Logistics Manager/System Admin."));
		        	saveMessages(request,verifyMessages);
		        }
		        
		        consignmentStatusFlag = Double.parseDouble(GmCommonClass.parseZero(gmSetBuildProcessForm.getStatusflag()));
		        if(consignmentStatusFlag <=1){
		        	gmSetBuildProcessForm.setDisplayFlag("1");
		        }
	       }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
       
        if(alConsignmentDetails.size() > 0)
        {
        	//log.debug("size..."+alConsignmentDetails.size());
        	request.setAttribute("HEADERDATA", hmData);
        	request.setAttribute("ALDATA",alConsignmentDetails);
        	request.setAttribute("RE_FORWARD","gmSetBuildTxn");
            request.setAttribute("RE_TXN","SETCONSWRAPPER");
            request.setAttribute("txnStatus","PROCESS");
            strForward = "gmRuleEngine";            
        }
        return mapping.findForward(strForward);
    }


}
