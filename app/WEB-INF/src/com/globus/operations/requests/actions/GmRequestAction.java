package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmShippingAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.forms.GmRequestForm;
import com.globus.operations.requests.forms.GmRequestInitiateForm;

public class GmRequestAction extends GmShippingAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.
	HashMap hmData = new HashMap();

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String strActForward = "";
		try {
			instantiate(request, response);
			GmRequestForm gmRequestForm = (GmRequestForm) form;
			gmRequestForm.loadSessionParameters(request);
			Logger log = GmLogger.getInstance(this.getClass().getName());
			String strOpt = gmRequestForm.getStrOpt();

			setFormHeader(gmRequestForm);
		} catch (Exception exp) {
			exp.printStackTrace();
			throw new AppError(exp);
		}
		strActForward = GmCommonClass.parseNull(request
				.getParameter("RE_RE_FORWARD"));
		strActForward = strActForward.equals("") ? "success" : strActForward;
		return mapping.findForward(strActForward);
	}

	public void setFormHeader(GmRequestForm gmRequestForm) throws AppError {
		GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(
				getGmDataStoreVO());
		String strRequestId = gmRequestForm.getRequestId();
		

		if (strRequestId.equals("")) {
			String strConID = gmRequestForm.getConsignID();
			strRequestId = GmCommonClass.parseNull(gmRequestReportBean
					.fetchRequestId(strConID));
			gmRequestForm.setRequestId(strRequestId);
			log.debug("GmRequestAction strMRID  " + strRequestId);
		}
		if (strRequestId != null && !strRequestId.equals("")) {
			hmData = gmRequestReportBean.fetchRequestHeader(strRequestId);
		}
	
		gmRequestForm = (GmRequestForm) GmCommonClass.getFormFromHashMap(
				gmRequestForm, hmData);
	}

	public void setArrayListValues(GmRequestInitiateForm gmRequestInitiateForm,
			HttpServletRequest request) throws AppError {
		// ArrayList alDistributorList =
		// gmCustomerBean.getDistributorList("Active");
		GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
		GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
		GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		String strTodaysDate;
		String strApplDate = "";
		strTodaysDate = (String) request.getSession().getAttribute(
				"strSessTodaysDate");
		strApplDate = getGmDataStoreVO().getCmpdfmt();// (String)request.getSession().getAttribute("strSessApplDateFmt");
		String strCompanyLocale = GmCommonClass
				.getCompanyLocale(getGmDataStoreVO().getCmpid());
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass
				.getResourceBundleBean("properties.Company", strCompanyLocale);
		String strCountryCode = gmResourceBundleBean.getProperty("COUNTRYCODE");
		// Add 5 days to the todays date if the dept code is 2014
		GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
		String strExpRetnDate = GmCalenderOperations.addDays(7, strApplDate);
		// log.debug("the today's date +5 days  is "+strExpRetnDate );

		if (gmRequestInitiateForm.getDeptId().equals("2014")) {
			gmRequestInitiateForm.setRequiredDate(strExpRetnDate);
		}
		// Add 5 days to the todays date if the dept code is 2014

		String strFromPage = gmRequestInitiateForm.getHfromPage();

		gmRequestInitiateForm.setDepartmentName(GmCommonClass
				.getCodeAltName(gmRequestInitiateForm.getDeptId()));
		log.debug("GmRequestAction Dept id  is " + gmRequestInitiateForm.getDeptId());
		/*
		 * Get only Consignment and InHouse values in the list
		 */
		List ltTemp = new ArrayList();
		ltTemp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(
				"DMDTP", getGmDataStoreVO()));
		String strConsignmentType = gmRequestInitiateForm.getConsignmentType();
		
		if(!strConsignmentType.equals("26240420")){
		  ltTemp = ltTemp.subList(1, 3);
		}
		
		ArrayList alConsignmentType = new ArrayList(ltTemp);
		ArrayList alContyList = new ArrayList();
		if (strFromPage.equals("PRODUCTREQUEST")) {
			if (strCountryCode.equals("en")) {
				alConsignmentType.add(GmCommonClass.parseNullArrayList(
						gmAutoCompleteReportBean.getCodeList("CONTY", getGmDataStoreVO()))
						.get(3)); // adding Product
			} else {
				alContyList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONTY", "102924", getGmDataStoreVO())); // 102924
				alConsignmentType.addAll(alContyList);
			}
		}

		/*
		 * Add GlobusMed In-House to the Distributor List
		 */
		// The following code added for Getting the Distributor, Sales Rep and Employee List based on the PLANT too if the Selected Company is EDC
		 String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
		 String	 strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "LOANERINITFILTER")) ;
		 strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "ITEMINITFILTER")) ;
		  
		 HashMap hmFilters = new HashMap();
		    hmFilters.put("FILTER", "Active");
		    hmFilters.put("USERCODE", "300");
		    hmFilters.put("DEPT", "");
		    hmFilters.put("STATUS", "311");    
		    if(strCompanyPlant.equals("Plant")){
			    hmFilters.put("DISTFILTER", "COMPANY-PLANT");
			    hmFilters.put("REPFILTER", "COMPANY-PLANT");
			    hmFilters.put("EMPFILTER", "COMPANY-PLANT");
		    }	
		
		ArrayList alDistributorList = GmCommonClass
				.parseNullArrayList(gmCustomerBean.getDistributorList(hmFilters));
		HashMap hmTemp = new HashMap();
		hmTemp.put("ID", "01");
		hmTemp.put("NAME", "Globus Med In-House");
		alDistributorList.add(0, hmTemp);

		/*
		 * Use only 3 values from ShipTo List
		 */
		ArrayList alShipTo = GmCommonClass.parseNullArrayList(GmCommonClass
				.getCodeList("CONSP", getGmDataStoreVO()));

		// alShipTo.remove(2); // remove Hospital
		// alShipTo.remove(2); // remove N/A
		String strhaction = gmRequestInitiateForm.getHaction();

		HashMap hmPur = new HashMap();

		ArrayList alConsignmentPurpose = GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("COPUR",
						getGmDataStoreVO()));

		ArrayList alInHousePurpose = GmCommonClass
				.parseNullArrayList(gmAutoCompleteReportBean.getCodeList("CONPU",
						getGmDataStoreVO()));
		ArrayList alRepList = GmCommonClass.parseNullArrayList(gmCustomerBean
				.getRepList(hmFilters));
		ArrayList alAccountList = GmCommonClass.parseNullArrayList(gmSales
				.reportAccount());
		ArrayList alEmployeeList = GmCommonClass.parseNullArrayList(gmLogon
				.getEmployeeList(hmFilters));

		hmPur.put("INHOUSEPUR", alInHousePurpose);
		hmPur.put("CONSIGNPUR", alConsignmentPurpose);
		gmRequestInitiateForm.setHmPurposes(hmPur);

		gmRequestInitiateForm.setAlConsignmentType(alConsignmentType);
		gmRequestInitiateForm.setAlShipTo(alShipTo);
		// 40022 In-house consignment
		if (strhaction.equals("40022")) {
			gmRequestInitiateForm.setAlInHousePurpose(alInHousePurpose);
		} else {
			gmRequestInitiateForm.setAlInHousePurpose(alConsignmentPurpose);
		}
		ArrayList alAttribute = new ArrayList();
		alAttribute = GmCommonClass.getCodeList("CNATTB", getGmDataStoreVO());
		gmRequestInitiateForm.setAlAttribute(alAttribute);
		gmRequestInitiateForm.setAlDistributorList(alDistributorList);
		gmRequestInitiateForm.setAlRepList(alRepList);
		gmRequestInitiateForm.setAlAccountList(alAccountList);
		gmRequestInitiateForm.setAlEmployeeList(alEmployeeList);

	}

}
