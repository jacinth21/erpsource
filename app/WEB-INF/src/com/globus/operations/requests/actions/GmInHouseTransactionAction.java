package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.requests.beans.GmInHouseTransactionBean;
import com.globus.operations.requests.forms.GmInHouseTransactionForm;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * This Class created for to create all inhouse transactions 
 * 
 * Finished Goods - Inhouse(FGIN), Quarantine - inhouse(QNIN), Repackaging - Inhouse(PNIN)
 * Bulk - Inhouse(BLIN),  IH Inventory - Inhouse(IHIN), Restricted Warehouse -Inhouse(RWIN)
 * 
 * @author tramasamy
 *
 */
public class GmInHouseTransactionAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * loadInHouseTxnInfo - This Method used to load the Inhouse transaction screen
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadInHouseTxnInfo(ActionMapping actionMapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		instantiate(request, response);

		GmInHouseTransactionForm gmInHouseTransactionForm = (GmInHouseTransactionForm) actionForm;

		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmInHouseTransactionForm);
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		GmInHouseTransactionBean gmInHouseTransactionBean = new GmInHouseTransactionBean(getGmDataStoreVO());
		String strReportType = gmInHouseTransactionForm.getIHTransType();
		gmInHouseTransactionForm.setIHTransType(strReportType);
		// IHTransType
		gmInHouseTransactionForm.loadSessionParameters(request);
		ArrayList alConsignmentType = new ArrayList();
		ArrayList alContyList = new ArrayList();
		ArrayList alPartList = new ArrayList();
		HashMap hmTemplateParam = new HashMap();

		alConsignmentType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DMDTP", getGmDataStoreVO()));
		alContyList = GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("CONTY", "102405", getGmDataStoreVO())); // 102405
		alConsignmentType.addAll(alContyList);
		gmInHouseTransactionForm.setAlRequestFor(alConsignmentType);
		ArrayList alInHousePurpose = GmCommonClass
				.parseNullArrayList(gmAutoCompleteReportBean.getCodeList("CONPU", getGmDataStoreVO()));
		gmInHouseTransactionForm.setAlInHousePurpose(alInHousePurpose);

		String strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
		// Get XML Grid Data
		hmTemplateParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
		hmTemplateParam.put("VMFILEPATH", "properties.labels.operations.inventory.GmInvLocationEdit");
		hmTemplateParam.put("TEMPLATE", "GmInHouseTransactionSetup.vm");
		hmTemplateParam.put("TEMPLATEPATH", "operations/templates");
		hmTemplateParam.put("ALPARTLIST", alPartList);
		// XML String
		ArrayList alParam = new ArrayList();
		String strXmlString = gmInHouseTransactionBean.getXmlGridData(alParam);
		gmInHouseTransactionForm.setGridXmlData(strXmlString);

		return actionMapping.findForward("gmInHouseTransactionSetup");
	}

	/**saveInHouseTxn - This method used to create the new inhouse transaction 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveInHouseTxn(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		log.debug("GmInHouseTransactionAction  saveInHouseTxn");

		GmInHouseTransactionForm gmInHouseTransactionForm = (GmInHouseTransactionForm) actionForm;

		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		GmInHouseTransactionBean gmInHouseTransactionBean = new GmInHouseTransactionBean(getGmDataStoreVO());
		GmCommonBean gmcommon = new GmCommonBean(getGmDataStoreVO());
		
		String strReportType = gmInHouseTransactionForm.getIHTransType();
		
		gmInHouseTransactionForm.setIHTransType(strReportType);
		gmInHouseTransactionForm.loadSessionParameters(request);
		
		HashMap hmParam = new HashMap();

		hmParam = GmCommonClass.getHashMapFromForm(gmInHouseTransactionForm);

		String strMaterialRequestId = "";
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

		String strActForward = GmCommonClass.parseNull(request.getParameter("RE_FORWARD"));
		String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		ArrayList alResult = new ArrayList();
		String strOpt = gmInHouseTransactionForm.getStrOpt();
		HashMap hmPutGroupId = new HashMap();
		HashMap hmruleShipVal = new HashMap();

		log.debug(" hmParam== " + hmParam);

		String strConsignmentType = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTTYPE"));
		log.debug(" strConsignmentType " + strConsignmentType);
		hmParam.put("USERID", strUserId);
		strMaterialRequestId = gmInHouseTransactionBean.saveInHouserequest(hmParam);

		String strRedirectPath = "/gmInHouseTransactionSetup.do?method=loadInHouseTxnPickSlip&requestId="
				+ strMaterialRequestId;
		return actionRedirect(strRedirectPath, request);

	}

	/** loadInHouseTxnPickSlip - This method used to load the Summary screen
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadInHouseTxnPickSlip(ActionMapping actionMapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		instantiate(request, response);

		GmInHouseTransactionForm gmInHouseTransactionForm = (GmInHouseTransactionForm) actionForm;
		gmInHouseTransactionForm.loadSessionParameters(request);
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmInHouseTransactionForm);

		String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));

		String strRedirectPath = "/gmRequestMaster.do?strOpt=GIMS&requestId=" + strRequestId;
		return actionRedirect(strRedirectPath, request);
	}
}
