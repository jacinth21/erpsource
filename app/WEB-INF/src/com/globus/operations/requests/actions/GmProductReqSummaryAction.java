package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.forms.GmProductReqSummaryForm;

public class GmProductReqSummaryAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      GmProductReqSummaryForm gmProductReqSummaryForm = (GmProductReqSummaryForm) form;
      gmProductReqSummaryForm.loadSessionParameters(request);
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
      HashMap hmSalesFilters = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alResult = new ArrayList();
      HashMap hmValue = new HashMap();
      HttpSession session = request.getSession(false);
      String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
      hmValue.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
      String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
      hmValue.put("STRDEPT", strDeptId);
      String strJIRASearchURL = GmCommonClass.parseNull(GmCommonClass.getJiraConfig("SEARCH_URL"));
      hmValue.put("SEARCHURL", strJIRASearchURL);
      String strOpt = gmProductReqSummaryForm.getStrOpt();
      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      int currMonth = 0;
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strTodaysDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
      currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String strMonthBeginDate =
          GmCommonClass.parseNull(GmCalenderOperations.getAnyDateOfMonth(currMonth, 1,
              strApplnDateFmt));
      // access
      String strAccessCondition = getAccessFilter(request, response);
      // The following Code is added for avoiding the Duplicate Field Sales Names
      HashMap hmAccessValues = new HashMap();
      hmAccessValues.put("CONDITION", strAccessCondition);
      hmAccessValues.put("ACTIVEDISTFL", "Y");
      hmAccessValues.put("STATUS", "Active");
      hmAccessValues.put("FLTR_DIST_BY_COMP", "YES");// Filter Distributor Based on company
      // The following code added for Getting the Distributor, Sales Rep and Employee List based on the PLANT too if the Selected Company is EDC
      String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid()); 
      String strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "RQT_SUMMARY_HEADER")) ;
      hmAccessValues.put("SCREENNAME", "REQUESTSUMMARY");
      if(strCompanyPlant.equals("Plant")){
  	  hmAccessValues.put("DISTFILTER", "COMPANY-PLANT");
  	  hmAccessValues.put("REPFILTER", "COMPANY-PLANT");
      }
      hmSalesFilters = gmCommonBean.getSalesFilterLists(hmAccessValues);
      alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
      gmProductReqSummaryForm.setAlDist(alReturn);
      alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("REP"));
      gmProductReqSummaryForm.setAlRepList(alReturn);
      alReturn = GmCommonClass.getCodeList("LNRQSM");
      gmProductReqSummaryForm.setAlStatus(alReturn);
      if (gmProductReqSummaryForm.getToDt().equals("")) {
        gmProductReqSummaryForm.setToDt(strTodaysDate);
      }
      if (gmProductReqSummaryForm.getFromDt().equals("")) {
        gmProductReqSummaryForm.setFromDt(strMonthBeginDate);
      }
      if (strOpt.equals("load")) {
        hmParam = GmCommonClass.getHashMapFromForm(gmProductReqSummaryForm);
        hmParam.put("AccessFilter", strAccessCondition);
        log.debug("hmParam :: " + hmParam);

        alResult = gmRequestReportBean.loadProductSummaryReport(hmParam);
        log.debug("alResult :: " + alResult.size());
        gmProductReqSummaryForm.setGridData(generateOutPut(alResult, hmValue));
      }

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

  private String generateOutPut(ArrayList alResult, HashMap hmParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale=(String)hmParam.get("STRSESSCOMPANYLOCALE");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setTemplateName("GmProductReqSummary.vm");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.operations.requests.GmProductReqSummary",
            strSessCompanyLocale));
    return templateUtil.generateOutput();
  }
}
