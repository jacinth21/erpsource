package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.requests.forms.GmSetInitiateForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmSetInitiateAction extends GmRequestAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		instantiate(request, response);

		GmSetInitiateForm gmSetInitiateForm = (GmSetInitiateForm) form;
		gmSetInitiateForm.loadSessionParameters(request);

		ArrayList alSetList = new ArrayList();
		List rdSetListReport = new ArrayList();
		HashMap hmReturn = new HashMap();
		HashMap hmPutGroupId = new HashMap();
		HashMap hmruleShipVal = new HashMap();

		GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
		GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(
				getGmDataStoreVO());
		GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(
				getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmRuleEngine gmRuleEngine = new GmRuleEngine(getGmDataStoreVO());
		// GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean();
		String strOpt = gmSetInitiateForm.getStrOpt();
		String strSetId = gmSetInitiateForm.getSetId();
		String strSetNm = gmSetInitiateForm.getSearchsetId();
		String strBillToId = gmSetInitiateForm.getDistributorId();
		String strBillToNm = gmSetInitiateForm.getSearchdistId();
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmSetInitiateForm);
		HashMap hmTemp = new HashMap();
		String strForward = "success";
		String strNameId = "";
		String strShipToId = "";
		ArrayList alReturn = new ArrayList();
		// this is for ship to, carrier and mode
		hmPutGroupId.put("RULEID", "SHIP_CARR_MODE_VAL");
		hmruleShipVal = gmCommonBean.fetchTransactionRules(hmPutGroupId,
				getGmDataStoreVO());// ship carrier and mode
		// default value get from rule
		gmSetInitiateForm.setHmReturn(hmruleShipVal);
		if (strOpt.equals("initiate")) {
			hmParam.putAll(loadShipReqParams(request));
			// loadShipReqParams(request, hmParam);
			hmReturn = gmLogisticsBean.initiateSetBuild(hmParam);
			String strMaterialRequestId = (String) hmReturn
					.get("MATERIALREQUESTID");
			String strConsignmentId = (String) hmReturn.get("CONSIGNID");

			/*
			 * Get the forward for sucess. Create a new URL with the requestId
			 * and consignID (these are form variable in gmSetBuildProcess *
			 * Redirect to this new URL
			 */
			// String strApplnDateFmt = gmSetInitiateForm.getApplnDateFmt();
			// gmTxnSplitBean.sendVendorEmail(strMaterialRequestId,strApplnDateFmt,request.getSession().getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION")),request,response);

			alReturn = GmCommonClass.parseNullArrayList((ArrayList) request
					.getAttribute("CONSEQUENCES"));
			if (alReturn.size() > 0) {
				gmRuleEngine
						.sendRFSEmail(strMaterialRequestId, alReturn, "SET");
			}

			String strPath = "/gmSetBuildProcess.do?requestId="
					+ strMaterialRequestId + "&consignID=" + strConsignmentId
					+ "&strOpt=VMAIL";
			return actionRedirect(strPath, request);
		} else {
			if (!strSetId.equals("")) {
				hmTemp = new HashMap();
				hmTemp.put("SETID", strSetId);
				hmTemp.put("MODE", "SET");
				hmTemp.put("FROMPAGE", "SETINITIATE");
				hmTemp.put("SETNM", strSetNm);
				rdSetListReport = gmProjectBean.fetchSetMaping(hmTemp);
				gmSetInitiateForm.setRdSetListReport(rdSetListReport);

				request.setAttribute("SETID", strSetId);
				request.setAttribute("SETNM", strSetNm);

			}

			//alSetList = gmProjectBean.loadSetMap("1601");
			gmSetInitiateForm.setAlSetList(alSetList);
			setArrayListValues(gmSetInitiateForm, request);
			gmSetInitiateForm.setAlSheetList(gmDemandSetupBean
					.loadDemandSheetList(hmTemp));
			gmSetInitiateForm.setAlRequestByType(GmCommonClass
					.getCodeList("RQBYT"));
			gmSetInitiateForm.setDistList(GmCommonClass.parseNull(GmCommonClass
					.getRuleValue("DISTLIST", "ICTSAMPLE")));
			gmSetInitiateForm.setSearchdistributorId(strBillToNm);
			if (rdSetListReport.size() > 0) {
				request.setAttribute("ALDATA", rdSetListReport);
				request.setAttribute("RE_FORWARD", "gmSetInit");
				request.setAttribute("RE_TXN", "SETINITWRAPPER");
				request.setAttribute("txnStatus", "PROCESS");
				strForward = "gmRuleEngine";
			}

			return mapping.findForward(strForward);
		}
	}

}
