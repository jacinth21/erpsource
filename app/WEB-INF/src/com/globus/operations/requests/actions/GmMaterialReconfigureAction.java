package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.beans.GmRequestTransBean;
import com.globus.operations.requests.forms.GmMaterialReconfigureForm;

public class GmMaterialReconfigureAction extends GmRequestAction {


  HashMap hmValues = null;
  ArrayList alReport = null;

  String strAction;



  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmRequestTransBean gmRequestTransBean = new GmRequestTransBean(getGmDataStoreVO());
    String strAction;
    HashMap hmtemp = null, hmHeader = null;
    String boreqid;
    try {
      String strRequestPurpose = "";
      String strFunctionId = "";
      HttpSession session = request.getSession(false);
      GmMaterialReconfigureForm gmMaterialReconfigureForm = (GmMaterialReconfigureForm) form;
      setFormHeader(gmMaterialReconfigureForm);
      gmMaterialReconfigureForm.loadSessionParameters(request);
      GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
      String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
      String strOpt = gmMaterialReconfigureForm.getStrOpt();
      String action = gmMaterialReconfigureForm.getHaction();
      // When select Reconfigure request, if we have OUS Sales Replenishment purpose then only
      // mapped user allowed.
      strRequestPurpose = GmCommonClass.parseNull(gmMaterialReconfigureForm.getStrRequestPurpose());
      HashMap hmAccess = new HashMap();
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "OUS_SAL_REPL_EDT_ACC"));
      String strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      String strReadAccess = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      String strVoidAccess = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
      if (strRequestPurpose.equals("4000097") && !strUpdateAccess.equals("Y")
          && !strReadAccess.equals("Y") && !strVoidAccess.equals("Y")) {
        throw new AppError("", "20593");
      }
      // submit
      if (action.equals("Submit")) {
        String inpString = gmMaterialReconfigureForm.getHinputstring();
        gmRequestTransBean.reconfig_RequestToConsign(gmMaterialReconfigureForm.getRequestId(),
            inpString, gmMaterialReconfigureForm.getUserId(),strCompanyInfo);
      }
      if (gmMaterialReconfigureForm.getRequestId() == null) {
        gmMaterialReconfigureForm.setRequestId("");
      }

      // Get data
      if (!gmMaterialReconfigureForm.getRequestId().equals("")) {
        hmtemp = gmRequestReportBean.fetchRequestDetail(gmMaterialReconfigureForm.getRequestId());
        alReport = (ArrayList) hmtemp.get("BODETAIL");

        log.debug(" alReport " + alReport);
        hmHeader = (HashMap) hmtemp.get("BOHEADER");

        gmMaterialReconfigureForm.setAlDetails(alReport);

        gmMaterialReconfigureForm.setBoreqId((String) hmHeader.get("BOREQID"));
        gmMaterialReconfigureForm.setBostatus((String) hmHeader.get("BOSTATUS"));
        gmMaterialReconfigureForm.setBocreateddate((String) hmHeader.get("BOCREATEDDATE"));
        gmMaterialReconfigureForm.setBocreatedby((String) hmHeader.get("BOCREATEDBY"));

      }

      String stReqFor = GmCommonClass.parseNull(gmMaterialReconfigureForm.getReqFor());

      if(stReqFor.equals("26240420")){// getting only the void dropdown for Stock Transfer
         gmMaterialReconfigureForm.setAlAction(GmCommonClass.getCodeList("RCSTRQ",getGmDataStoreVO()));
      }
      else{
        gmMaterialReconfigureForm.setAlAction(GmCommonClass.getCodeList("RCFRQ"));
      }
     
      log.debug(" alReport " + GmCommonClass.getCodeList("RCFRQ"));


      return mapping.findForward("success");

    } catch (AppError e) {
      e.printStackTrace();
      log.debug(" Exception e " + e.getMessage());


      request.setAttribute("hType", e.getType());
      throw e;
      // session.setAttribute("hAppErrorMessage",e.getMessage());
      // gmServlet.gotoStrutsPage("/common/GmError.jsp",request,response);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }


  }


}
