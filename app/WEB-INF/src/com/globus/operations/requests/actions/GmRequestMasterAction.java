package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.operations.requests.beans.GmStockInitiateBean;
import com.globus.operations.requests.forms.GmRequestMasterForm;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmAccessControlBean;

public class GmRequestMasterAction extends GmAction {

  HashMap hmValues = new HashMap();
  GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    String strForward = "success";
    try {

      GmRequestMasterForm gmRequestMasterForm = (GmRequestMasterForm) form;
      gmRequestMasterForm.loadSessionParameters(request);
      GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
      GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
      GmStockInitiateBean gmStockInitiateBean = new GmStockInitiateBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());
      GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());

      HashMap hmParam = new HashMap();
      HashMap hmTemp = new HashMap();
      HashMap hmAccess = new HashMap();     
      // ArrayList alReport = new ArrayList();

      RowSetDynaClass resultSet = null;

      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      List alResult = new ArrayList();
      ArrayList alEmpList = new ArrayList();
      ArrayList alConsignmentType = new ArrayList();
      ArrayList alContyList = new ArrayList();
      ArrayList alReport = null;
      HashMap hmData = new HashMap();
      HashMap hmCompanyAddress = new HashMap();
      String strDivisionId = "";

      GmJasperReport gmJasperReport = new GmJasperReport();
      GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(getGmDataStoreVO());

      String strRequestFor = gmRequestMasterForm.getRequestFor();

      String strOpt = gmRequestMasterForm.getStrOpt();
      String strUserId = "";
      String strUpdateAccess = "";
 
      String strRequestId = gmRequestMasterForm.getRequestId();
      String strCntryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
      if (strOpt.equals("")) {
        String depId = gmRequestMasterForm.getDeptId();
        if (depId.equals("2014")) {
          gmRequestMasterForm.setRequestStatusoper("90191");

        } else {
          gmRequestMasterForm.setRequestStatusoper("90192");

        }

      }

      hmParam = GmCommonClass.getHashMapFromForm(gmRequestMasterForm);
      strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

      hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,"SET_BUILD_ROLLBACK"));
      strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));  
      request.setAttribute("UPDFL", strUpdateAccess);  

      if (strOpt.equals("reload")) {
        hmParam = GmCommonClass.getHashMapFromForm(gmRequestMasterForm);
        resultSet = gmRequestReportBean.loadRequestMasterReport(hmParam);
        // log.debug(" alReport " + resultSet);
        alResult = resultSet.getRows();
        gmRequestMasterForm.setAlRequestReport(alResult);
      }
     

      if (strOpt.equals("print") || strOpt.equals("GIMS")) {
        List rdRequestInfoDetails = gmRequestReportBean.fetchRequestInfoForEdit(strRequestId);
        hmData = gmRequestReportBean.fetchRequestHeader(strRequestId);
        gmRequestMasterForm =
            (GmRequestMasterForm) GmCommonClass.getFormFromHashMap(gmRequestMasterForm, hmData);
        List rdSubDetails = gmLogisticsBean.fetchChildRequest(strRequestId);
        String strPagebreak = GmCommonClass.parseNull(request.getParameter("strPgBrk"));
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        
        String strReqtype = GmCommonClass.parseNull((String) hmData.get("REQFOR"));


        hmData.put("SUBDETAILSOURCE1", rdSubDetails);
        hmData.put("REQUESTINFODETAILS", rdRequestInfoDetails);
        hmData.put("SUBREPORT_DIR", gmJasperReport.getJasperRealLocation());

        hmData.put("REQUESTID", strRequestId);
        strDivisionId = GmCommonClass.parseNull((String) hmData.get("DIVISION_ID"));
        strDivisionId = strDivisionId.equals("") ? "2000" : strDivisionId;
        hmCompanyAddress =
            GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), strDivisionId);

        String strCompanyId =
            GmCommonClass.parseNull((String) request.getAttribute("strSessCompId"));
        strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
        String strCountryCode = GmCommonClass.parseNull(GmCommonClass.getString("COUNTRYCODE"));
        String strCompanyName = "";
        String strCompanyAddress = "";
        String strCompanyPhone = "";
        String strShowPhoneNo = "";
        String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
        GmResourceBundleBean gmResourceBundleBean =
            GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
        strShowPhoneNo =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("REQUEST.SHOW_PHONE_NO"));
        // to get the dynamic company LOGO on Request Screen after Item
        // Initiation.
        String strRuleImage =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("ITEMREQUESTDETAIL", "LOGO"));
        strRuleImage = strRuleImage.replace("/", "");
        // to get the dynamic company address details
        // COuntry Code condition added for BUG-3687 By HReddi
        if (!strDivisionId.equals("")) {
          strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
          strCompanyAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
          if (strShowPhoneNo.equals("YES")) {
            strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
          }
        } else {
          strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYNAME"));
          strCompanyAddress =
              GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYADDRESS"));
          strCompanyPhone =
              GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYPHONE"));
        }

        hmData.put("GMCOMPANYNAME", strCompanyName);
        hmData.put("RULEIMAGE", strRuleImage);
        hmData.put("GMCOMPANYADDRESS", strCompanyAddress + " / " + strCompanyPhone);

        ArrayList alLogReasons = new ArrayList();
        GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
        alLogReasons = gmCommonBean.getLog(strRequestId, "1235"); // 1235
        // -Request
        // Master
        // Comments
        gmRequestMasterForm.setAlLogReasons(alLogReasons);
                   
         
        
        HashMap hmConsignDetails = new HashMap();
        String strConsignId =
            GmCommonClass.parseNull(gmRequestReportBean.fetchConsignmentIdFromMR(strRequestId));
        // reverting the changes of BUG-4161 for PMT-7461
        hmConsignDetails = gmCust.loadConsignDetails(strConsignId, strUserId);
        hmData.putAll(hmConsignDetails);
        if(strReqtype.equals("26240420")){//26240420 -- Stock Transfer 
          HashMap hmReqShipDtls = new HashMap();
          // to fetch the shipping details of request
          hmReqShipDtls = gmStockInitiateBean.fetchRequestShipDetails(strRequestId);
          hmData.putAll(hmReqShipDtls);
          alReport = GmCommonClass.parseNullArrayList(alLogReasons);
        }
        String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");

        hmData.put("LOGOIMAGE",
            strImagePath + GmCommonClass.parseNull((String) hmCompanyAddress.get("LOGO")) + ".gif");
        hmData.put("SESSIONDTFMT", getGmDataStoreVO().getCmpdfmt());
        hmData.put("COMPANYDFMT", getGmDataStoreVO().getCmpdfmt());
        if (strOpt.equals("print")) {
          hmData.put("LASTPAGE", "Y");
          gmJasperReport.setJasperReportName("/GmMRPrint.jasper");
          // When strPagebreak set as no, this is to print the JASPER
          // report without any page break.
          if (strPagebreak.equals("no")) {
            hmData.put("IS_IGNORE_PAGINATION", Boolean.TRUE);
          }
          gmJasperReport.setHmReportParameters(hmData);
          gmJasperReport.setReportDataList(alReport);
          gmJasperReport.createJasperHtmlReport();// bug-3801 code
          // fixed
          return null;
        } else {// GIMS
          hmData.put("LASTPAGE", "N");

            hmData.put("SHIP_DTLS", hmConsignDetails);

          strForward = "GmRequestSummary";
        }
        gmRequestMasterForm.setHmData(hmData);
      }

      if (strOpt.equals("part")) {
        gmRequestMasterForm.setSetOrPart("50637");
      }

      gmRequestMasterForm
          .setAlRequestSource(GmCommonClass.getCodeList("RQSRC", getGmDataStoreVO()));

      gmRequestMasterForm
          .setAlRequestStatus(GmCommonClass.getCodeList("RQSTS", getGmDataStoreVO()));

      gmRequestMasterForm.setAlRequestStatus1(GmCommonClass
          .getCodeList("CPRSN", getGmDataStoreVO()));

      if (strCntryCode.equals("en")) {
        gmRequestMasterForm.setAlRequestFor(GmCommonClass.getCodeList("DMDTP", getGmDataStoreVO()));
      } else {
        alConsignmentType =
            GmCommonClass
                .parseNullArrayList(GmCommonClass.getCodeList("DMDTP", getGmDataStoreVO()));
        alContyList =
            GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONTY", "102405",
                getGmDataStoreVO())); // 102405
        // Master
        // Request
        // For
        alConsignmentType.addAll(alContyList);
        gmRequestMasterForm.setAlRequestFor(alConsignmentType);
      }

      alResult = GmCommonClass.parseNullArrayList(gmCust.getDistributorList("Active"));

      alEmpList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList());

      ArrayList alRepList = GmCommonClass.parseNullArrayList(gmCust.getRepList("Active"));

      if (strRequestFor.equals("40020")) // 40020 - Sales
      {
        gmRequestMasterForm.setAlRequestTo(alRepList);
      }
      
  // Adding the new condition to fetch the Request to Account details if the Request for is chosen as "Account Consignment"
  // PMT-24836 Author:gpalani
      
      if (strRequestFor.equals("40025")) // 40025 - BBA Account Consignment
      {
    	alResult= GmCommonClass.parseNullArrayList(gmSalesBean.reportAccount());
        gmRequestMasterForm.setAlRequestTo(alResult);
      }

      if (strRequestFor.equals("40021") || strRequestFor.equals("102930")) // 40021
      // -
      // Consignment
      // and
      // 102930
      // -
      // ICT
      {
        gmRequestMasterForm.setAlRequestTo(alResult);
      }
      if (strRequestFor.equals("40022")) // 40022 - InHouse
      {
        gmRequestMasterForm.setAlRequestTo(alEmpList);
        // log.debug("alEmpList is " + alEmpList);
      }

      gmRequestMasterForm.setAlSetPart(GmCommonClass.getCodeList("STTYP", getGmDataStoreVO()));
      gmRequestMasterForm.setAlMainOrChild(GmCommonClass.getCodeList("REQMC", getGmDataStoreVO()));
      // Get the Team dropdown values from GMDemandSetupBean

      // gmRequestMasterForm.setAlTeamList(gmDemandSetupBean.loadHierarchyList());
      // Below code is added fro PMT-2448.
      gmRequestMasterForm.setAlTeamList(gmRequestReportBean.loadTTPLevelListTeam());
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward(strForward);
  }
}
