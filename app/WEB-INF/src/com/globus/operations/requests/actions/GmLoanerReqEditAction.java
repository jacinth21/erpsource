/**
 * FileName : GmLoanerReqEditAction.java Description : Author : rshah Date : Mar 23, 2009 Copyright
 * : Globus Medical Inc
 */
package com.globus.operations.requests.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.operations.requests.beans.GmLoanerReqEditBean;
import com.globus.operations.requests.forms.GmLoanerReqEditForm;
import com.globus.sales.event.beans.GmEventReportBean;
import com.globus.sales.event.beans.GmEventSetupBean;

/**
 * @author rshah
 * 
 */
public class GmLoanerReqEditAction extends GmAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmLoanerReqEditForm gmLoanerReqEditForm = (GmLoanerReqEditForm) form;
    gmLoanerReqEditForm.loadSessionParameters(request);
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmEventReportBean gmEventReportBean = new GmEventReportBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmHeaderValues = new HashMap();
    HashMap hmReq = new HashMap();
    // Code Added for MNTTASK-8116
    ArrayList alReqList = new ArrayList();
    ArrayList alShippingDetails = new ArrayList();
    ArrayList alVoidRequestDetails = new ArrayList();
    HashMap hmReturnReport = new HashMap();
    ArrayList alMasterReport = new ArrayList();
    java.util.Date dtRequestDate = null;
    java.util.Date dtSurgeryDate = null;
    String strRequestDate = "";
    String strSurgeryDate = "";
    String strSurgeryDt = "";
    String strDataFlag = "Y";
    String strVoidDataFlag = "Y";
    String strFindForword = "success";
    String strCompanyName = "";
    String strCompanyAddress = "";
    String strCompanyPhone = "";
    String strCompanyId = "";
    String strCountryCode = "";
    String strDivisionId = "";
    String strEsclAccessFl = "";
    String strEsclRemAccessFl = "";
    ArrayList alReqSlipList = new ArrayList();

    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmLoanerReqEditBean gmLoanerReqEditBean = new GmLoanerReqEditBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strOpt = gmLoanerReqEditForm.getStrOpt();
    String strRequestId = gmLoanerReqEditForm.getRequestId();
    String strCaseInfoID = GmCommonClass.parseNull(gmEventSetupBean.fetchCaseInfoID(strRequestId));
    String strOrderBy = gmLoanerReqEditForm.getOrderBy();
    String strTxnType = GmCommonClass.parseNull(gmLoanerReqEditForm.getTxnType());
    String strUserId = GmCommonClass.parseNull(gmLoanerReqEditForm.getUserId());
    String strRequestTxnType =
        GmCommonClass.parseNull(gmEventReportBean.fetchRequestType(strRequestId));
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strEtchidInputStr = GmCommonClass.parseNull(gmLoanerReqEditForm.getEtchidInputStr());
    String strVoidInputStr = GmCommonClass.parseNull(gmLoanerReqEditForm.getVoidInputStr());
    ArrayList alLogReasons = new ArrayList();
    
    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerReqEditForm);
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strShowEsc =  GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_ESCALATION"));
    strEsclAccessFl = gmEventSetupBean.getEventAccess(strUserId, "EVENT", "ESCALATION_ACCESS"); //PC-137
    strEsclRemAccessFl = gmEventSetupBean.getEventAccess(strUserId, "EVENT", "ESC_REMOVE_ACCESS");
    gmLoanerReqEditForm.setStrEsclAccessFl(strEsclAccessFl);
	gmLoanerReqEditForm.setStrEsclRemAccessFl(strEsclRemAccessFl);
    
	hmParam.put("ESCLFL", strEsclAccessFl);
    hmParam.put("SHOWESCL", strShowEsc);
    hmParam.put("REMESCFL", strEsclRemAccessFl);
    
    
    strTxnType = (strTxnType.equals("") ? strRequestTxnType : strTxnType);
    hmParam.put("TXNTYPE", strTxnType);
    if (strOpt.equals("save")) {
      strSurgeryDt = GmCommonClass.parseNull((String) hmParam.get("SURGERYDT"));
      dtSurgeryDate = GmCommonClass.getStringToDate(strSurgeryDt, strApplnDateFmt);
      hmParam.put("SURGERYDT", dtSurgeryDate);
      hmParam.put("CASEINFOID", strCaseInfoID);
      gmLoanerReqEditBean.saveLoanerRequestDetail(hmParam);
      if (!strEtchidInputStr.equals("") && strTxnType.equals("4119")) {
        gmEventReportBean.sendEventSetRejectionEmail(strEtchidInputStr, "OVERRIDE", "VILNRQ");
      }
      log.debug("strVoidInputStr" + strVoidInputStr);
      if (!strVoidInputStr.equals("") && strTxnType.equals("4119")) {
        gmEventReportBean.sendEventSetRejectionEmail(strVoidInputStr, "CANCEL", "VDIHLN");
      }
      strFindForword = "success";
    }
    if (!strRequestId.equals("")) {

      gmLoanerReqEditBean.validateProdRequestID(strRequestId, strTxnType);
      gmLoanerReqEditForm.setRequestTxnType(strRequestTxnType);
      log.debug("strRequestType::" + strRequestTxnType);
      if (strRequestTxnType.equals("4127")) {
        hmHeaderValues = gmLoanerReqEditBean.fetchLoanerRequestHeader(hmParam);
      } else if (strRequestTxnType.equals("4119")) {
        hmHeaderValues = GmCommonClass.parseNullHashMap(fetchEventInfo(strRequestId, strUserId));
        gmLoanerReqEditForm =
            (GmLoanerReqEditForm) GmCommonClass.getFormFromHashMap(gmLoanerReqEditForm,
                hmHeaderValues);
      }

      alVoidRequestDetails = gmLoanerReqEditBean.fetchLoanerVoidedRequest(hmParam);
      alShippingDetails = gmLoanerReqEditBean.fetchLoanerShippingInfo(hmParam);
      hmValues = gmLoanerReqEditBean.fetchLoanerRequestDetail(hmParam);
      // merge code from OUS side, attribute detail
      ArrayList alAttrib =
          GmCommonClass.parseNullArrayList(gmLoanerReqEditBean.fetchRequestAttribDet(strRequestId));
      gmLoanerReqEditForm.setAlAttrib(alAttrib);

      hmReq = GmCommonClass.parseNullHashMap((HashMap) hmHeaderValues.get("HMREQ"));
      // Checking the weather the entered Request ID is having the Data or not.if not we are hiding
      // the all the fields in JSP using the following Flag value.
      if (!hmReq.isEmpty() || !hmHeaderValues.isEmpty()) {
        strDataFlag = "N";
      }
      if (alVoidRequestDetails.size() >= 1) {
        strVoidDataFlag = "N";
      }
      strRequestDate = GmCommonClass.parseNull((String) hmReq.get("REQUESTEDDT"));
      strSurgeryDate = GmCommonClass.parseNull((String) hmReq.get("SURGERYDT"));
      hmReq.put("REQUESTEDDT", strRequestDate);
      hmReq.put("SURGERYDT", strSurgeryDate);
      gmLoanerReqEditForm.setHmRequest(hmReq);
      alReqList = (ArrayList) hmValues.get("ALREQUESTDETAIL");
      alReqSlipList = (ArrayList) hmValues.get("ALREQSLIP");
      gmLoanerReqEditForm =
          (GmLoanerReqEditForm) GmCommonClass.getFormFromHashMap(gmLoanerReqEditForm, hmReq);
      strFindForword = "success";
    }
    // Code Modified for PMT-3244[Showing country based company address, name and Date formats in
    // Print Loaner Jasper report]
    if (strOpt.equals("print") || strOpt.equals("InhousePrint")) {
      strCompanyId = GmCommonClass.parseNull((String) request.getAttribute("strSessCompId"));
      strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
      strDivisionId = strCompanyId.equals("100801") ? "2001" : "2000";

      HashMap hmCompanyAddress =
          GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), strDivisionId);

      strCountryCode = GmCommonClass.parseNull((String) hmCompanyAddress.get("COUNTRYCODE"));

      if (strCountryCode.equals("en")) {

        strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
        strCompanyAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
        strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
      } else {
        strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYNAME"));
        strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYPHONE"));
	        if (strCountryCode.equals("jp")) {
		        strCompanyAddress =
		            GmCommonClass.parseNull((String) hmCompanyAddress.get("3FLR_ADDRESS"));
		    }else{
		    	strCompanyAddress = 
	        	GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYADDRESS"));
	        }
      }

      if (strOpt.equals("print")) {
        hmReturnReport = gmLoanerReqEditBean.fetchLoanerRequestHeader(hmParam);
        alShippingDetails = gmLoanerReqEditBean.fetchLoanerShippingInfoPrint(hmParam);
        alLogReasons = gmCommonBean.getLog(strRequestId, "1247");
        hmReturnReport.put("ACCNMLBL", "Account");
        hmReturnReport.put("REQDTLBL", "Requested Date");
        hmReturnReport.put("SURGERYDTLBL", "Surgery Date");
        hmReturnReport.put("REPNMLBL", "Rep Name");
        hmReturnReport.put("HEADERLBL", "Loaner Request");
        hmReturnReport.put("SESSIONDTFMT", getGmDataStoreVO().getCmpdfmt());
        hmReturnReport.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
                + "\\");
        hmReturnReport.put("ALLOGREASONS", alLogReasons);
        hmReturnReport.put("SIZE", alShippingDetails.size());
        strFindForword = "gmloanerreqprint";
      } else if (strOpt.equals("InhousePrint")) {
        hmReturnReport = gmLoanerReqEditBean.fetchLoanerRequestHeader(hmParam);
        hmHeaderValues = GmCommonClass.parseNullHashMap(fetchEventInfo(strRequestId, strUserId));
        String strEventStart =
            GmCommonClass.parseNull((String) hmHeaderValues.get("EVENTSTARTDATE"));
        String strEndStart = GmCommonClass.parseNull((String) hmHeaderValues.get("EVENTENDDATE"));
        alShippingDetails = gmLoanerReqEditBean.fetchLoanerShippingInfoPrint(hmParam);
        alLogReasons = gmCommonBean.getLog(strCaseInfoID, "1287");
        hmReturnReport.put("ACCOUNTNM",
            GmCommonClass.parseNull((String) hmHeaderValues.get("EVENTNAME")));
        hmReturnReport.put("REQUESTEDDT", strEventStart);
        hmReturnReport.put("SURGERYDT", strEndStart);
        hmReturnReport.put("ACCNMLBL", "Event Name");
        hmReturnReport.put("REQDTLBL", "Event Start Date");
        hmReturnReport.put("SURGERYDTLBL", "Event End Date");
        hmReturnReport.put("REPNMLBL", "Requested Date");
        hmReturnReport.put("SESSIONDTFMT", getGmDataStoreVO().getCmpdfmt());
        hmReturnReport.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
                + "\\");
        hmReturnReport.put("ALLOGREASONS", alLogReasons);
        hmReturnReport.put("SIZE", alShippingDetails.size());
        String strReqDate = GmCommonClass.parseNull((String) hmHeaderValues.get("REQUESTEDDT"));
        hmReturnReport.put("SALESREPNM", strReqDate);
        hmReturnReport.put("HEADERLBL", "In-house Loaner Request");
        strFindForword = "gmloanerreqprint";
      }

      hmReturnReport.put("GMCOMPANYNAME", strCompanyName);
      if (strCountryCode.equals("en")) {
        hmReturnReport.put("GMCOMPANYADDRESS", strCompanyAddress + " / " + strCompanyPhone);
      } else {
        hmReturnReport.put("GMCOMPANYADDRESS", strCompanyAddress + " / Phone :" + strCompanyPhone);
      }
      hmReturnReport.put("LOGO",
          GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));
    }
    if (strTxnType.equals("4127")) {
      gmLoanerReqEditForm.setAlVoidReasons(GmCommonClass.getCodeList("VDLON"));
    } else if (strTxnType.equals("4119")) {
      gmLoanerReqEditForm.setAlVoidReasons(GmCommonClass.getCodeList("VDIHLN"));
    }
    // gmLoanerReqEditForm.setAlVoidReasons(GmCommonClass.getCodeList("VDLON"));
    gmLoanerReqEditForm.setAlReqList(GmCommonClass.parseNullArrayList(alReqList));
    gmLoanerReqEditForm.setAlReqSlipList(GmCommonClass.parseNullArrayList(alReqSlipList));
    gmLoanerReqEditForm.setAlShippingDetails(alShippingDetails);
    gmLoanerReqEditForm.setHmReportHeaderDetails(hmReturnReport);
    gmLoanerReqEditForm.setAlVoidRequestDetails(alVoidRequestDetails);
    if (strRequestTxnType.equals("4127")) {
      alLogReasons = gmCommonBean.getLog(strRequestId, "1247");
    } else if (strRequestTxnType.equals("4119")) {
      alLogReasons = gmCommonBean.getLog(strCaseInfoID, "1287");
    }
    gmLoanerReqEditForm.setAlLogReasons(alLogReasons);
    gmLoanerReqEditForm.setDataFlag(strDataFlag);
    gmLoanerReqEditForm.setVoidDataFlag(strVoidDataFlag);
    gmLoanerReqEditForm.setTxnType(strTxnType);
    // merge code from OUS side for Delivery/Collection PicSLip
    if (strOpt.equals("PicSlip")) {
      String strhMode = GmCommonClass.parseNull(request.getParameter("strMode"));
      gmLoanerReqEditForm.setRequestId(strRequestId);
      gmLoanerReqEditForm.setStrMode(strhMode);
      return mapping.findForward("PicSlipSuccess");
    }

    return mapping.findForward(strFindForword);
  }

  private HashMap fetchEventInfo(String strRequestId, String strUserId) throws AppError {
    HashMap hmJasperHeaderValues = new HashMap();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    String strCaseInfoID = GmCommonClass.parseNull(gmEventSetupBean.fetchCaseInfoID(strRequestId));
    hmJasperHeaderValues = gmEventSetupBean.fetchEventBookingInfo(strCaseInfoID, strUserId);
    return hmJasperHeaderValues;
  }

}
