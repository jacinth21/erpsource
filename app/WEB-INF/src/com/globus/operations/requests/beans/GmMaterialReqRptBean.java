package com.globus.operations.requests.beans;


import java.util.HashMap;  





import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ASophia
 *
 */
public class GmMaterialReqRptBean extends GmBean{
	
Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmMaterialReqRptBean(GmDataStoreVO gmDataStoreVO)
			throws Exception {
		// TODO Auto-generated constructor stub
		
		super(gmDataStoreVO);
	}
	
	/**
	 * saveItemLoanerRequest : This method is used to save the request id 
	 * */
	
	public void saveItemLoanerRequest(String strLoanerRequestID,  String strMaterialRequestId,  String strUserId) throws AppError{
		    String strLoanerReqId="";
			    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
		 	    gmDBManager.setPrepareString("gm_pkg_op_process_prod_request.gm_sav_master_loaner_item_consign", 3);
				gmDBManager.setString(1, strMaterialRequestId);
				gmDBManager.setString(2, strLoanerRequestID);
				gmDBManager.setString(3, strUserId);
				gmDBManager.execute();
				gmDBManager.commit();
	}

}
