package com.globus.operations.requests.beans;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRepEmailProperties;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.rule.beans.GmRuleEngine;
import com.globus.common.util.jobs.GmJob;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.operations.returns.beans.GmReturnsBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAccountRequestTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAccountRequestTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // log4j

  /**
   * loadReqInitiateLists - This method loads the initial lists meant for the Request Initiate
   * Module
   * 
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadReqInitiateLists() throws AppError {
    GmCommonClass gmCommon = new GmCommonClass();
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    ArrayList alResult = new ArrayList();

    try {
      alResult = gmCust.getDistributorList("Active");
      hmReturn.put("DISTLIST", alResult);
      alResult = gmCommon.getCodeList("RQFOR");
      hmReturn.put("REQFOR", alResult);
      alResult = gmCommon.getCodeList("RQTYP");
      hmReturn.put("REQTYPE", alResult);
      alResult = gmCommon.getCodeList("RQLOC");
      hmReturn.put("REQLOC", alResult);
      alResult = gmCommon.getCodeList("RQSHP");
      hmReturn.put("REQSHIP", alResult);
      alResult = gmCommon.getCodeList("RQRSN");
      hmReturn.put("REQRSN", alResult);
      alResult = gmCommon.getCodeList("RQPTY");
      hmReturn.put("REQPTY", alResult);

    } catch (Exception e) {
      GmLogError.log("Exception in GmRequestTransBean:loadReqInitiateLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadItemOrderLists

  /**
   * saveRequestEditInfo - This method will Required date and Request To
   * 
   * @param hmParam - parameters to be saved / updated
   * @exception AppError
   */
  public void saveRequestEditInfo(HashMap hmParam) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    log.debug(" inside save.... " + hmParam);

    String strReqDate = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDATE"));
    String strReqTo = GmCommonClass.parseNull((String) hmParam.get("REQTO"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strShipToID = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));
    String strInpStr = GmCommonClass.parseNull((String) hmParam.get("HINPSTR"));
    String strPlannedShipDate = GmCommonClass.parseNull((String) hmParam.get("PLANSHIPDATE"));
    String strDateFmt = getCompDateFmt();// GmCommonClass.parseNull((String)
                                         // hmParam.get("APPLNDATEFMT"));
    Date dtPlanShipDt = GmCommonClass.getStringToDate(strPlannedShipDate, strDateFmt);
    log.debug("strReqTo " + strReqTo);
    gmDBManager.setPrepareString("gm_pkg_op_request_master.gm_sav_request_edit_info", 8);

    gmDBManager.setString(1, strReqDate);
    gmDBManager.setString(2, strReqTo);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strReqId);
    gmDBManager.setString(5, strShipTo);
    gmDBManager.setString(6, strShipToID);
    gmDBManager.setString(7, strInpStr);
    gmDBManager.setDate(8, dtPlanShipDt);


    gmDBManager.execute();
    log.debug("saving shipping params now");
    // saving shipping params
    hmParam.put("REFID", strReqId);
    hmParam.put("SOURCE", "50184"); // request
    gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
    gmTxnSplitBean.checkAndSplitConsignamnet(gmDBManager, strReqId, strUserId);
    gmDBManager.commit();
  }

  /**
   * save reconfig - request to consign
   * 
   * @param requestID
   * @param strInputString
   * @param strUserId
   * @throws AppError
   */

  /**
   * saveProductRequest - Saves Product Request
   * 
   * @param hmParam - parameters to be saved / updated
   * @exception AppError
   */
  public String saveProductRequest(HashMap hmParam, ArrayList alReturn) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    GmCustomerBean custBean = new GmCustomerBean(getGmDataStoreVO());
    GmRuleEngine gmRuleEngine = new GmRuleEngine(getGmDataStoreVO());
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    GmReturnsBean gmReturnBean = new GmReturnsBean(getGmDataStoreVO());
    String strMaterialRequestId = "";
    String strConsignId = "";
    String strShipToId = "";
    String strRequestSource = "50618";
    String strRequestByType = "50625";
    StringBuffer sbMessage = new StringBuffer();
    String strEmailShipToId = "";
    String strAdd = "";
    log.debug(" values to save in GMaccountRequestTransBean.java *** " + hmParam);
    String strRAFlag = GmCommonClass.parseNull((String) hmParam.get("STRRAFLAG"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("SESSDATEFMT"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strConsignmentType = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTTYPE"));
    String strInHousePurpose = GmCommonClass.parseNull((String) hmParam.get("INHOUSEPURPOSE"));
    String strDistributorId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strEmployeeId = GmCommonClass.parseNull((String) hmParam.get("EMPLOYEEID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDepartmentId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    Date dtRequiredDate = (Date) hmParam.get("REQUIREDDATE");
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strShipCarrier = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strAddId = GmCommonClass.parseNull((String) hmParam.get("ADDRESSID"));
    String strEmpShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));
    String strPlannedShipDate = GmCommonClass.parseNull((String) hmParam.get("PLANNEDDATE"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("HREFNAME"));
    String strApplDateFmt = getCompDateFmt();// GmCommonClass.parseNull((String)
                                             // hmParam.get("APPLNDATEFMT"));
    Date dtPlanShipDT = GmCommonClass.getStringToDate(strPlannedShipDate, strApplDateFmt);
    String strCountryCode = GmCommonClass.parseNull((String)GmCommonClass.countryCode);
    String strInpStr = "";
    String strAssocRepId = GmCommonClass.parseNull((String) hmParam.get("ASSOCREPID"));
    String strRequestTo = strDistributorId;
    String strLoanReqID = GmCommonClass.parseNull((String) hmParam.get("LOANERREQID")); //PMT-32450 - Add Loaner Req Id For Account Item Initaiate Screen
    // check if user from US or OUS
    if (strCountryCode.equals("en")) {
      strInpStr = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    } else {
      strInpStr = GmCommonClass.parseNull((String) hmParam.get("ATTINPSTR"));
    }
    // commented the below line as if shipto is 0 then its getting reset to
    // employee.
    strShipTo = strShipTo.equals("0") ? "50625" : strShipTo;
    strDistributorId =
        strDistributorId.equals("01") ? "" : strDistributorId.equals("0") ? "" : strDistributorId;
    if (strConsignmentType.equals("40025")) {
      strAccountId = strDistributorId;
      strEmployeeId = strEmpShipToId;
    }
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));

    /*
     * Adding SHIPMODE and SHIPCARRIER values to the strInputString to save in t504_consignmnet
     * table instead of NULL values and code modified for the GH task PMT-3243 [BUG-3723]
     */
    if (!strConsignmentType.equals("4127")) { // product loaner
      strInputString = strInputString + "~" + strShipCarrier + "^" + strShipMode + "|";
    }
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    String strReqComments = GmCommonClass.parseNull((String) hmParam.get("HADDINFO"));

    if (strDepartmentId.equals("2014")) {
      strRequestSource = "50617";
    } else {
      strRequestSource = "50618";
    }

    /*
     * 4121 - Sales Rep 4123, 50625 - Employee
     */
    if (strConsignmentType.equals("4127")) { // product loaner
      strRequestByType = "50626";
    } else if (strShipTo.equals("4121")) {
      strRequestByType = "50626";
    } else if (strShipTo.equals("4123")) {
      strShipToId = "50625";
    }

    strShipToId = strEmployeeId;
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_op_account_request.gm_sav_initiate_request", 25);
    log.debug("strAccountId " + strAccountId);
    log.debug("strDistributorId " + strDistributorId);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(22, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(23, OracleTypes.VARCHAR);

    gmDBManager.setString(1, "");
    gmDBManager.setDate(2, dtRequiredDate);
    // gmDBManager.setString(2,"");
    gmDBManager.setString(3, strRequestSource);
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, strSetId);
    gmDBManager.setInt(6, Integer.parseInt(strConsignmentType));
    gmDBManager.setString(7, strDistributorId);
    gmDBManager.setInt(8, Integer.parseInt(strRequestByType));
    gmDBManager.setString(9, strUserId);
    gmDBManager.setString(10, strShipTo);
    gmDBManager.setString(11, strShipToId);
    gmDBManager.setString(12, "");
    gmDBManager.setInt(13, 15); // Status 15 is backlog
    gmDBManager.setString(14, strUserId);
    gmDBManager.setString(15, strInputString);
    gmDBManager.setString(16, strInHousePurpose);
    gmDBManager.setString(17, strRepId);
    gmDBManager.setString(18, strAccountId);
    gmDBManager.setString(19, strInpStr);
    gmDBManager.setDate(20, dtPlanShipDT);
    gmDBManager.setString(21, strAssocRepId);
    gmDBManager.setString(24, strReqComments);
    gmDBManager.setString(25, strLoanReqID);


    gmDBManager.execute();
    strMaterialRequestId = GmCommonClass.parseNull(gmDBManager.getString(22));
    log.debug("strMaterialRequestId " + strMaterialRequestId);
    strConsignId = GmCommonClass.parseNull(gmDBManager.getString(23));

    strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
    log.debug("strConsignmentType " + strConsignmentType);

    if (strMaterialRequestId != null) {
      if (!strShipTo.equals("") && !strShipTo.equals("0")) {
        log.debug("saving shipping record");
        if (strConsignmentType.equals("4127")) { // product loaner
          hmParam.put("REFID", strMaterialRequestId);
          hmParam.put("SOURCE", "50185"); // Product loaner
        } else {
          hmParam.put("REFID", strMaterialRequestId);
          hmParam.put("SOURCE", "50184"); // request
        }
        gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
      }
      // calling the procedure to allocate the requests created
      allocateProductRequests(gmDBManager, hmParam);
    }
    
    if (strRAFlag.equals("Y")) {
        gmReturnBean.initiateReturnItemConsign(gmDBManager, strMaterialRequestId, strRAFlag, strUserId);
      }

    if (!strLog.equals("") && strConsignmentType.equals("4127")) { // product
      // loaner
      gmCommonBean.saveLog(gmDBManager, strMaterialRequestId, strLog, strUserId, "1247");
    } else if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strMaterialRequestId, strLog, strUserId, "1235");
    }
    if (!strConsignmentType.equals("4127")) {
      gmTxnSplitBean.checkAndSplitConsignamnet(gmDBManager, strMaterialRequestId, strUserId);
    }
    gmDBManager.commit();

    log.debug("Consignment Id is " + strConsignId);
    // log.debug("Material Request Id is " + strMaterialRequestId);

    hmReturn.put("MATERIALREQUESTID", strMaterialRequestId);
    hmReturn.put("CONSIGNID", strConsignId);
    if (!strConsignmentType.equals("4127")) {
      String strApplnDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
      String strJasPerLoaction = GmCommonClass.parseNull((String) hmParam.get("SUBREPORT_DIR"));
      GmJasperReport gmJasperReport = (GmJasperReport) hmParam.get("JASPEROBJ");
      gmTxnSplitBean.sendVendorEmail(strMaterialRequestId, strApplnDateFmt, strJasPerLoaction,
          gmJasperReport);
    }
    if (strMaterialRequestId != null) {
      String strMsg = AppError.rbApp.getString("REQIDSUCCESS");
      strMsg = GmCommonClass.replaceAll(strMsg, "#<REQID>", strMaterialRequestId);
      strMsg = GmCommonClass.replaceAll(strMsg, "#<CNID>", GmCommonClass.parseNull(strConsignId));
      sbMessage.append(strMsg);
      // sbMessage.append("Transaction MR created : ");
      // sbMessage.append(strMaterialRequestId);
      if (alReturn.size() > 0) {
        gmRuleEngine.sendRFSEmail(strMaterialRequestId, alReturn, strType);
      }
    }
    if (strConsignId != null) {
      String strMsg = AppError.rbApp.getString("CNIDSUCCESS");
      strMsg = GmCommonClass.replaceAll(strMsg, "#<CNID>", strConsignId);
      // sbMessage.append(" with CN : ");
      // sbMessage.append(strConsignId);
      sbMessage.append(strMsg);
    }
    log.debug(" Success message " + sbMessage.toString());
    if (!strConsignmentType.equals("4127") && !strConsignmentType.equals("102930")
        && !strConsignmentType.equals("40021") && !strConsignmentType.equals("40025")
        && !strConsignmentType.equals("40022")) { // 102930 - Inter Company Transfer , 4127 -
                                                  // Product Loaner , 40021 - Consignment , 40022 -
                                                  // In-House Consignment
      throw new AppError(sbMessage.toString(), "", 'S');
    }
    // log.debug("strShipTo:" + strShipTo
    // +"  strShipToId:"+strShipToId+"  strDistributorId: "+strDistributorId);
    if (strShipTo.equals("4120")) {
      strEmailShipToId = strDistributorId;
    } else if (strShipTo.equals("4122")) {
      strEmailShipToId = strEmpShipToId;
    } else if (strShipTo.equals("4123")) {
      strEmailShipToId = strEmpShipToId;
    } else if (strShipTo.equals("4121")) {
      strEmailShipToId = strAddId;
    } else if (strShipTo.equals("4000642")) {
      strEmailShipToId = strEmpShipToId;
    }

    log.debug("strShipTo:" + strShipTo + "  strShipToId:" + strShipToId + "  strEmailShipToId: "
        + strEmailShipToId + "strEmpShipToId: " + strEmpShipToId);
    if (!strConsignmentType.equals("40025")) {
      if (!strShipTo.equals("4124")) {
        strAdd = GmCommonClass.parseNull(custBean.getAddress(strShipTo, strEmailShipToId));
      }

      // log.debug(" strAdd: " + strAdd);
      sendRequestRecievedMail(strMaterialRequestId, strShipMode, strAdd, strApplDateFmt);
    }
    return strMaterialRequestId;
  }

  public void sendRequestRecievedMail(String strRequestId, String strShippingMode,
      String strAddress, String strApplDateFmt) {
    log.debug("strRequestId = " + strRequestId);
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strrepID = "";
    String strRepName = "";
    String strRepEmail = "";
    String strAssrepID = "";
    String strAssRepName = "";
    String strAssRepEmail = "";
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    
    try {
      SimpleDateFormat df = new SimpleDateFormat(strApplDateFmt);
      ArrayList alRequestDetails = new ArrayList();
      HashMap hmRepDetails = new HashMap();
      HashMap hmParams = new HashMap();


      gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_fch_request_details", 2);

      gmDBManager.setString(1, strRequestId);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.execute();

      alRequestDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

      gmDBManager.close();

      hmParams.put("UNAVAILABLE_FL", "N");

      if (alRequestDetails != null && alRequestDetails.size() > 0) {
        hmRepDetails = (HashMap) alRequestDetails.get(0);
        strRepName = (String) hmRepDetails.get("REP_NAME");
        strrepID = (String) hmRepDetails.get("REP_ID");
        strRepEmail = (String) hmRepDetails.get("EMAIL");
        /*
         * The associate rep code has been added for PMT-4031. The functionality is to send an email
         * to Associate rep instead of Rep. And Sales rep should be in CC. The same code changes has
         * been added to below files as well. GmLoanerSetDueEmailJob.java (Due job),
         * GmLoanerBean.java (Transfer), GmTicketBean (missing charge)
         */

        strAssRepName = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPNM"));
        strAssrepID = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPID"));
        strAssRepEmail = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPEMAIL"));

        if (!strAssrepID.equals("")) {
          strRepName = strAssRepName;
          strRepEmail = strAssRepEmail;

        }

        for (Iterator iter = alRequestDetails.iterator(); iter.hasNext();) {
          HashMap hmTemp = (HashMap) iter.next();
          String strStatus = (String) hmTemp.get("STATUS");
          if (strStatus != null && strStatus.equals("Unavailable")) {
            hmParams.put("UNAVAILABLE_FL", "Y");
            break;
          }
        }
      }

      GmEmailProperties emailProps = new GmRepEmailProperties(strrepID, "GmLoanerRequestReceived");
      emailProps.setRecipients(strRepEmail);
      emailProps.setSender(gmResourceBundleBean.getProperty("GmRecievedRequest."
          + GmEmailProperties.FROM));
      emailProps.setCc(gmResourceBundleBean
          .getProperty("GmRecievedRequest." + GmEmailProperties.CC));
      emailProps.setMimeType(gmResourceBundleBean.getProperty("GmRecievedRequest."
          + GmEmailProperties.MIME_TYPE));

      String strSubject =
          gmResourceBundleBean.getProperty("GmRecievedRequest." + GmEmailProperties.SUBJECT);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", strRepName);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<RQDATE>", df.format(new Date()));
      emailProps.setSubject(strSubject);

      hmParams.put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
      hmParams.put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSEmail"));
      hmParams.put("SHIPMODE", GmCommonClass.getCodeNameFromCodeId(strShippingMode));
      hmParams.put("SHIPADD", strAddress);

      GmJasperMail jasperMail = new GmJasperMail();
      jasperMail.setJasperReportName("/GmEmailReq.jasper");
      jasperMail.setAdditionalParams(hmParams);
      jasperMail.setReportData(alRequestDetails);
      jasperMail.setEmailProperties(emailProps);

      hmReturn = jasperMail.sendMail();

    } catch (Exception e) {
      log.error("Exception in sendRequestRecievedMail : " + e.getMessage());
      hmReturn.put(GmJob.EXCEPTION, e);
    }
    Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
    if (ex != null) {
      try {
        GmEmailProperties emailProps = new GmEmailProperties();
        emailProps.setRecipients(gmResourceBundleBean
            .getProperty("GmRecievedRequest.ExceptionMailTo"));
        emailProps.setSender("notification@globusmedical.com");
        emailProps.setSubject(gmResourceBundleBean
            .getProperty("GmRecievedRequest.ExceptionMailSubject") + " " + strRequestId);
        emailProps.setMessage("Exception : " + GmCommonClass.getExceptionStackTrace(ex, "\n")
            + "\nOriginal Mail : " + hmReturn.get("ORIGINALMAIL"));
        emailProps.setMimeType("text/html");

        GmCommonClass.sendMail(emailProps);
      } catch (Exception e) {
        log.error("Exception in sending mail to Rep for new request and also notification to IT"
            + e.getMessage());
      }
    }
  }

  public void reconfig_RequestToConsign(String requestID, String strInputString, String strUserId)
      throws AppError {
    String strNewMRID = " ", strNewConID = "", strMessage = "", strInHouseTxnID = "", strAdditionalMessage =
        "";
    HashMap hm = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_sav_reconfig_part", 6);
    gmDBManager.setString(1, requestID);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.execute();
    strNewMRID = GmCommonClass.parseNull(gmDBManager.getString(4));
    strNewConID = GmCommonClass.parseNull(gmDBManager.getString(5));
    strInHouseTxnID = GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.commit();

    if (strNewMRID.equals("") && strNewConID.equals("")) {
      return;
    }
    // log.debug("strNewMRID: "+ strNewMRID +" and strNewConID:" +
    // strNewConID + " at line 227." );
    // wip
    if (strNewMRID.equals("")) {
      if (!strInHouseTxnID.equals("")) {
        strAdditionalMessage =
            ", <br> and an Inhouse Transaction Created with ID: " + strInHouseTxnID;
      }
      strMessage =
          "Transaction CN: ".concat(strNewConID).concat(" updated ").concat(strAdditionalMessage);
    } else {
      // built set
      strMessage =
          "Transaction CN: ".concat(strNewConID).concat(" created with  MR: " + strNewMRID);
    }
    log.debug(" Success message " + strMessage);
    throw new AppError(strMessage, "", 'S');
  }

  public HashMap fetchReqdetail(HashMap hmParam) throws AppError {

    RowSetDynaClass rdResult = null;
    RowSetDynaClass rdToResult = null;
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strRequestFrom = GmCommonClass.parseNull((String) hmParam.get("REQUESTFROM"));
    String strRequestTo = GmCommonClass.parseNull((String) hmParam.get("REQUESTTO"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_op_process_request.GM_FCH_REQ_TO_SWAP", 4);
    // gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.setString(1, strRequestFrom);
    gmDBManager.setString(2, strRequestTo);
    gmDBManager.execute();

    // String strMAstatus = gmDBManager.getString(3);
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    rdToResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));

    gmDBManager.close();
    hmReturn.put("REQUESTSTO", rdToResult);
    hmReturn.put("REQUESTSFROM", rdResult);

    return hmReturn;

  }

  /**
   * saveDemandSheet - This method will be save / update the demand sheet data
   * 
   * @param hmParam - parameters to be saved / updated
   * @exception AppError
   */
  public void saveSwapRequest(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strRequestFrom = GmCommonClass.parseNull((String) hmParam.get("REQUESTFROM"));
    String strRequestTo = GmCommonClass.parseNull((String) hmParam.get("REQUESTTO"));
    String strUserId = (String) hmParam.get("USERID");
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    gmDBManager.setPrepareString("gm_pkg_op_process_request.gm_sav_swap_req", 3);

    gmDBManager.setString(1, strRequestFrom);
    gmDBManager.setString(2, strRequestTo);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();

    // String strMAstatus = gmDBManager.getString(3);

    if (!strLogReason.equals("")) {
      strLogReason =
          "REQUEST SWAP: From " + strRequestFrom + " To " + strRequestTo + " " + strLogReason;
      gmCommonBean.saveLog(gmDBManager, strRequestFrom, strLogReason, strUserId, "1235"); // 1235
      // -
      // Log
      // Code
      // Number
      // for
      // Swap
      // Request
      gmCommonBean.saveLog(gmDBManager, strRequestTo, strLogReason, strUserId, "1235");
    }
    gmDBManager.commit();

  }

  public void allocateProductRequests(GmDBManager gmDBManager, HashMap hmParam) throws AppError {

    String strProdReqId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strConsignmentType = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug("calling to allocate " + strProdReqId);
    gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_sav_allocate_requests", 3);

    gmDBManager.setString(1, strProdReqId);
    gmDBManager.setString(2, strConsignmentType);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();

  }

  /***************************************************************************
   * saveEmailCounter - this method updates the C525_EMAIL_COUNT column in t525 tbl when a missing
   * parts email is sent to the rep.
   * 
   * @param strProdReqId
   * @param strUserId
   * @throws AppError
   */
  public void saveEmailCounter(GmDBManager gmDBManager, String strProdReqId, String strUserId)
      throws AppError {

    log.debug("calling to update counter " + strProdReqId);
    gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_sav_email_counter", 2);

    gmDBManager.setString(1, strProdReqId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();

  }

  /**
   * moveReqToCommonPool - This method will be move request to common pool
   * 
   * @param String - parameters to be saved / updated
   * @exception AppError
   */
  public void moveReqToCommonPool(String strReqId, String strDmdShtMonthId, String strUserId)
      throws AppError {
    log.debug(" strReqId = " + strReqId + " strDmdShtMonthId = " + strDmdShtMonthId);
    /* JNDI1 ->US database */
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmDBManager gmDBManagerDM = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    gmDBManager.setPrepareString("gm_pkg_op_request_master.gm_move_req_to_compool", 2);

    gmDBManager.setString(1, strReqId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();


    /* JNDI2 ->Order Planning database */
    gmDBManagerDM.setPrepareString("gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_request", 5);

    gmDBManagerDM.setString(1, strReqId);
    gmDBManagerDM.setString(2, "common_pool");
    gmDBManagerDM.setString(3, strUserId);
    gmDBManagerDM.setString(4, "");
    gmDBManagerDM.setString(5, strDmdShtMonthId);
    gmDBManagerDM.execute();

    gmDBManager.commit();
    gmDBManagerDM.commit();
  }
  /** PMT-32450 - Referencing Loaner Request in Item Consignment
   * fetchLoanerRefDetails - This method will be to validate the Loaner Request Id for Account item Initiate and  request Edit
   *  
   * @param String 
   * @exception AppError
   */
  public String fetchLoanerRefDetails(HashMap hmParam)throws AppError {
	   
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strLoanerReqId = GmCommonClass.parseNull((String) hmParam.get("LOANERREQID"));
	    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
	    String strRequestFor = GmCommonClass.parseNull((String) hmParam.get("REQFOR"));
	    log.debug("hmParam============ "+hmParam);
	    String strLonerReqIdErrMsg = "";
	    gmDBManager.setPrepareString("gm_pkg_op_account_request.gm_Validate_loaner_ref_details", 4);
	    gmDBManager.setString(1, strLoanerReqId);
	    gmDBManager.setString(2, strAccountId);
	    gmDBManager.setString(3, strRequestFor);
	    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	    gmDBManager.execute();
	    strLonerReqIdErrMsg = GmCommonClass.parseNull(gmDBManager.getString(4));
	    gmDBManager.close();
	   return strLonerReqIdErrMsg;
	  }
}// End of GmRequestTransBean
