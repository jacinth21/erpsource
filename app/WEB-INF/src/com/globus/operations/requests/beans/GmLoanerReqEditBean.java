/**
 * FileName    : GmLoanerReqEditBean.java 
 * Description :
 * Author      : rshah
 * Date        : Mar 23, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.requests.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

/**
 * @author rshah
 *
 */
public class GmLoanerReqEditBean extends GmBean{
	
	  public GmLoanerReqEditBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmLoanerReqEditBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	// Instantiating the Logger
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmCommonBean gmCommonBean = new GmCommonBean();
	/**
     * fetchLoanerRequestHeader - This method will be fetch loaner request Header details For Jasper Call
     * @param hmParam 
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchLoanerRequestHeader(HashMap hmParam) throws AppError
    {
        HashMap hmHeaderValues = new HashMap();    
        HashMap hmValues = new HashMap();
        RowSetDynaClass rdCurrConv = null;
        String strRequestId = GmCommonClass.parseNull((String)hmParam.get("REQUESTID"));
        String strUserID = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strStrOpt = GmCommonClass.parseNull((String)hmParam.get("STROPT"));
        String strTxnType  = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));
        log.debug("strTxnType::"+strTxnType);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_fch_loaner_request_header",4);
		gmDBManager.setString(1, GmCommonClass.parseNull(strRequestId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strUserID));
		gmDBManager.setString(3, GmCommonClass.parseNull(strTxnType));
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);	
		gmDBManager.execute();		
		hmHeaderValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
		hmValues.put("HMREQ", hmHeaderValues);
		gmDBManager.close();	
		/* Here we are adding the Resulted hash map to the Another Hash map to Show the Report Parameters in Edit Loaner Request Screen.
		 * But same Resulted Hash Map needs to use to set the HmReport Parameters to the Jasper Call. so Hash Map inside the HashMap object is not returning the values to the Jasper Report.*/
		if(strStrOpt.equals("print")||strStrOpt.equals("InhousePrint")){
			return hmHeaderValues;
		}else{
			return hmValues;
		}
    }   
    
	/**
     * fetchLoanerRequestDetail - This method will be fetch loaner request details
     * @param strRequestId 
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchLoanerRequestDetail(HashMap hmParam) throws AppError
    {
        HashMap hmValues = new HashMap();
        HashMap hmReq = new HashMap();
        ArrayList alList = new ArrayList();
        ArrayList alSlipList = new ArrayList();
        String strRequestId = GmCommonClass.parseNull((String)hmParam.get("REQUESTID"));
        String strTxnType  = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));
        RowSetDynaClass rdCurrConv = null;
        RowSetDynaClass rdSlip = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_fch_loaner_request_detail", 4);
		gmDBManager.setString(1, GmCommonClass.parseNull(strRequestId));		
		gmDBManager.setString(2, GmCommonClass.parseNull(strTxnType));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		//add one more output for  Delivery/Collection PicSLip for OUS
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.execute();
    rdCurrConv = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
		alList=(ArrayList)rdCurrConv.getRows();		
    rdSlip = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
		alSlipList=(ArrayList)rdSlip.getRows();
		hmValues.put("ALREQUESTDETAIL", alList);
		hmValues.put("ALREQSLIP", alSlipList);
		gmDBManager.close();
		
		
		return hmValues;	
    }
	
    /**
     * fetchLoanerVoidedRequest - This method will be fetch loaner request voided records 
     * @param hmParam 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchLoanerVoidedRequest(HashMap hmParam) throws AppError
    {
        ArrayList alVoidRequestList = new ArrayList();        
        RowSetDynaClass rdCurrConv = null;
        String strRequestId = GmCommonClass.parseNull((String)hmParam.get("REQUESTID"));
        String strTxnType  = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));        
        log.debug("strTxnType::"+strTxnType);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_fch_loaner_voided_request", 3);
		gmDBManager.setString(1, GmCommonClass.parseNull(strRequestId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strTxnType));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);		
		gmDBManager.execute();		
    rdCurrConv = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
		alVoidRequestList =(ArrayList)rdCurrConv.getRows();		
		gmDBManager.close();		
		return alVoidRequestList;	
    }
    
    /**
     * fetchLoanerShippingInfo - This method will be fetch Shipping Detaiils for a Loaner Request
     * @param HmParam 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchLoanerShippingInfo(HashMap hmParam) throws AppError
    {        
        ArrayList alShipDetailList 	= new ArrayList();
        ArrayList alShipList 		= new ArrayList();
        ArrayList alShipDetails     = new ArrayList();
        String strRequestId 		= GmCommonClass.parseNull((String)hmParam.get("REQUESTID"));
        String strStrOpt  			= GmCommonClass.parseNull((String)hmParam.get("STROPT"));
        String strTxnType    = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));
        String strPrintFl = "";
        log.debug("strTxnType::"+strTxnType);
        if(strStrOpt.equals("print")){
        	strPrintFl = "Y";
		}else{		
			strPrintFl = "N";
		}
        RowSetDynaClass rdCurrConv = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_fch_loaner_shipping_details", 4);
		gmDBManager.setString(1, GmCommonClass.parseNull(strRequestId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strPrintFl));
		gmDBManager.setString(3, GmCommonClass.parseNull(strTxnType));
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.execute();	
		/* Checking the Condition as For Showing the Report in JSP using the Display Tag we are returning the result into the Row Set Dyna Clsss
		 * IF for Shoing the same report to the Jasper Report(Paper Work) we Need ArrayList to set the Vales. so that we are checking the Condition as "Print" or "not".*/
		if(strStrOpt.equals("print")){
			alShipList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));	
			alShipDetails =  alShipList;
		}else{		
      rdCurrConv = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
			alShipDetailList =(ArrayList)rdCurrConv.getRows();
			alShipDetails =  alShipDetailList;
		}
		gmDBManager.close();		
		return alShipDetails;
    }    
    /**
     * fetchLoanerShippingInfoPrint - This method will be fetch Shipping Detaiils for a Loaner Request
     * @param HmParam 
     * @return ArrayList
     * @exception AppError
     */
    public ArrayList fetchLoanerShippingInfoPrint(HashMap hmParam) throws AppError
    {        
        ArrayList alShipDetailList 	= new ArrayList();
        ArrayList alShipList 		= new ArrayList();
        ArrayList alShipDetails     = new ArrayList();
        String strRequestId 		= GmCommonClass.parseNull((String)hmParam.get("REQUESTID"));
        String strStrOpt  			= GmCommonClass.parseNull((String)hmParam.get("STROPT"));
        String strTxnType    = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));
        String strPrintFl = "";
        
        if(strStrOpt.equals("print")){
        	strPrintFl = "Y";
		}else{		
			strPrintFl = "N";
		}
        RowSetDynaClass rdCurrConv = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_fch_loaner_shipping_prntdtl", 4);
		gmDBManager.setString(1, GmCommonClass.parseNull(strRequestId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strPrintFl));
		gmDBManager.setString(3, GmCommonClass.parseNull(strTxnType));
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.execute();	
		/* Checking the Condition as For Showing the Report in JSP using the Display Tag we are returning the result into the Row Set Dyna Clsss
		 * IF for Shoing the same report to the Jasper Report(Paper Work) we Need ArrayList to set the Vales. so that we are checking the Condition as "Print" or "not".*/
		if(strStrOpt.equals("print")){
			alShipList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));	
			alShipDetails =  alShipList;
		}else{		
      rdCurrConv = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
			alShipDetailList =(ArrayList)rdCurrConv.getRows();
			alShipDetails =  alShipDetailList;
		}
		gmDBManager.close();		
		return alShipDetails;
    }    
    /**
     * saveLoanerRequestDetail  - This method saves the loaner Request details 
     * @param hmParam - parameters to be saved
     * @exception AppError
     * @return void
     */
	public void saveLoanerRequestDetail(HashMap hmParam) throws AppError
	{
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
        String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
        log.debug("input str....."+strInputString);
        String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
        Date dtSurgeryDt = (Date)hmParam.get("SURGERYDT");
        String strSesnUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        log.debug("REQUESTID ....."+strRequestId);
        String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
        String strTxnType    = GmCommonClass.parseNull((String) hmParam.get("TXNTYPE"));
        String strCaseInfoId = GmCommonClass.parseNull((String) hmParam.get("CASEINFOID"));
        gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_sav_loanerrequest_detail",4);        
        gmDBManager.setString(1,strRequestId); 
        if(dtSurgeryDt == null){
            gmDBManager.setDate(2,null);
        }else{
        	gmDBManager.setDate(2,new java.sql.Date(dtSurgeryDt.getTime()));	
        }
        gmDBManager.setString(3,strInputString);
        gmDBManager.setString(4,strSesnUserId);
        gmDBManager.execute();   
        if (!strLogReason.equals("")) {
        	if(strTxnType.equals("4127")){
        		gmCommonBean.saveLog(gmDBManager, strRequestId, strLogReason, strSesnUserId, "1247");
        	}else if(strTxnType.equals("4119")){
        		gmCommonBean.saveLog(gmDBManager, strCaseInfoId, strLogReason, strSesnUserId, "1287");
        	}
		}
        gmDBManager.commit();		
	}
	 /**
     * fetchRequestAttribDet  - This method fetch sugery details 
     * @param String strReqId
     * @exception AppError
     * @return void
     */
	public ArrayList fetchRequestAttribDet(String strReqId) throws AppError {

		ArrayList  alResult=null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_fch_attribute_req_det", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strReqId);
	 	gmDBManager.execute();  

	 	alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)); 
	 	log.debug(" alResult :::: "+alResult );
		gmDBManager.close();
		return alResult;
	}
  /**
   * validateProdRequestID - This method is used to validate the request id raised based on the company id.
   * 
   * @param String strReqId,String strRequestTxnType
   * @exception AppError
   * @return void
   */
  public void validateProdRequestID(String strRequestID,String strRequestTxnType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_validate_prod_request", 2);
    gmDBManager.setString(1, strRequestID);
    gmDBManager.setString(2, strRequestTxnType);
    gmDBManager.execute();
    gmDBManager.close();
  }
}
