package com.globus.operations.requests.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

public class GmInHouseTransactionBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public GmInHouseTransactionBean(GmDataStoreVO gmDataStoreVO) {
		// TODO Auto-generated constructor stub

		super(gmDataStoreVO);
	}

	/**
	 * getXmlGridData - This Method used to generate XML data
	 * @param alParam 
	 * 
	 * @return
	 * @throws AppError
	 */
	public String getXmlGridData(ArrayList alParam) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		//ArrayList alParam = new ArrayList();
		HashMap hmTemplateParam = new HashMap();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmTemplateParam.get("STRSESSCOMPANYLOCALE"));
		try {
			templateUtil.setDataList("alResult", alParam);
			templateUtil.setDataMap("hmLcnParam", hmTemplateParam);
			templateUtil.setTemplateName("GmInHouseTransactionSetup.vm");
			templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
					"properties.labels.custservice.ProcessRequest.GmInhouseTransaction", strSessCompanyLocale));
			templateUtil.setTemplateSubDir("operations/templates");
		} catch (Exception e) {
			log.debug("Exception in GmInHouseTransactionBean");
		}

		return templateUtil.generateOutput();
	}

	/**saveInHouserequest - This method Used to Save the Inhouse Transaction
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String saveInHouserequest(HashMap hmParam) throws AppError {

		String strMaterialRequestId = "";
		String strConsignId = "";
		String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company",
				strCompanyLocale);
		  GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
		log.debug(" values to save " + hmParam);
		String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("SESSDATEFMT"));
		// String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		String strConsignmentType = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTTYPE"));
		String strInHousePurpose = GmCommonClass.parseNull((String) hmParam.get("INHOUSEPURPOSE"));
		String strInHouseType = GmCommonClass.parseNull((String) hmParam.get("IHTRANSTYPE"));
		String strEmployeeId = GmCommonClass.parseNull((String) hmParam.get("EMPID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strDepartmentId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
		String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strReqComments = GmCommonClass.parseNull((String) hmParam.get("HADDINFO"));
		
		HashMap hmStorageBuildInfo = new HashMap();
		
		String strShipTo ="4123"; //4123 - Employee
		String strCarrier ="4000603"; //4000603 -	Other
		String strMode="5030"; //5030 - Hand Delivered
		String strSource ="50618"; //50618 - Customer Service
		
		hmParam.put("SHIPTO", strShipTo);
		hmParam.put("SHIPCARRIER", strCarrier);
		hmParam.put("SHIPMODE", strMode);
		hmParam.put("SHIPTOID", strEmployeeId);
		hmParam.put("SOURCE", strSource);
		
		//~5001^5004 - for save shipping Details 
		strInputString += "~" + strCarrier +"^"+strMode +"|";
		log.debug(" values to save strInputString " + strInputString);
		
		gmDBManager.setPrepareString("gm_pkg_op_process_inhouse_txn.gm_sav_inhouse_request", 11);
		gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(11, OracleTypes.VARCHAR);

		gmDBManager.setString(1, strInHouseType);
		gmDBManager.setString(2, "");
		gmDBManager.setString(3, strInHousePurpose);
		gmDBManager.setString(4, strInputString);
		gmDBManager.setInt(5, 15); // Status 15 is backlog
		gmDBManager.setString(6, strUserId);
		gmDBManager.setString(7, strShipTo);
		gmDBManager.setString(8, strSource);
		gmDBManager.setString(9, strEmployeeId);
		gmDBManager.execute();
		strMaterialRequestId = GmCommonClass.parseNull(gmDBManager.getString(10));
		log.debug("strMaterialRequestId " + strMaterialRequestId);
		strConsignId = GmCommonClass.parseNull(gmDBManager.getString(11));
        
		log.debug("strLog " + strLog);
		if (!strLog.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strMaterialRequestId, strLog, strUserId, "1235");
		}
		
		  if (strMaterialRequestId != null) {
		      if (!strShipTo.equals("") && !strShipTo.equals("0")) {
		        log.debug("saving shipping record");
		          hmParam.put("REFID", strMaterialRequestId);
		          hmParam.put("SOURCE", "50184"); // request
		       
		        gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
		      }
		    
		    }
		  
		// JMS call for Storing Building Info PMT-33511
		log.debug("Storing building information saveProductRequest ");
		hmStorageBuildInfo.put("TXNID", strConsignId);
		hmStorageBuildInfo.put("TXNTYPE", "CONSIGNMENT");
		
		updateShippingStatus(gmDBManager,strConsignId,strUserId);
		gmDBManager.commit();
		gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);
		
		return strMaterialRequestId;
	}

	/**updateShippingStatus - This method used to update shipping table active flag as shipped
	 * @param gmDBManager
	 * @param strConsignId
	 * @param strUserId
	 */
	public void updateShippingStatus(GmDBManager gmDBManager, String strConsignId, String strUserId) {
		
		gmDBManager.setPrepareString("gm_pkg_op_process_inhouse_txn.gm_update_shipping_active_fl", 2);

		gmDBManager.setString(1, strConsignId);
		gmDBManager.setString(2, strUserId);
	    gmDBManager.execute();
	}

	
}
