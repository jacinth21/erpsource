package com.globus.operations.requests.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmRequestReportBean extends GmSalesFilterConditionBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

  public GmRequestReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmRequestReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * loadRequestInfoForEdit - This Method is used to get Request detail information
   * 
   * @param String strRequestId
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchRequestInfoForEdit(String strRequestId) throws AppError {

    ArrayList alRequest = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // To fetch the transaction information
    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_summary_csg_req_detail", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.execute();

    alRequest = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    gmDBManager.close();

    return alRequest;
  }

  /**
   * loadPartDetail - This Method is used to get Associated Transaction info
   * 
   * @param String strRequestId
   * @return Hashmap
   * @exception AppError
   **/
  public HashMap fetchRequestAsscTxn(String strRequestId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmAsscHeader = new HashMap();

    log.debug("strRequestId  is  " + strRequestId);
    ArrayList alAsscDetail = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_request_assc_txn", 3);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setString(1, strRequestId);

    gmDBManager.execute();

    hmAsscHeader =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    alAsscDetail =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));

    gmDBManager.close();

    hmReturn.put("HMASSCHEADER", hmAsscHeader);
    hmReturn.put("ALASSCDETAIL", alAsscDetail);


    return hmReturn;
  }

  /**
   * Get request header
   * 
   * @param strREQId
   * @return
   * @throws AppError
   */
  public HashMap fetchRequestHeader(String strREQId) throws AppError {

    HashMap hmReqHeader;


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_request_header", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strREQId);

    gmDBManager.execute();

    hmReqHeader = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmReqHeader;

  }

  /**
   * get request details
   * 
   * @param strREQId
   * @return
   * @throws AppError
   */
  public HashMap fetchRequestDetail(String strREQId) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alDetails;
    String strMessage = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_pending_request_detail", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strREQId);

    gmDBManager.execute();

    hmReturn.put("BOHEADER", gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)));
    alDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    hmReturn.put("BODETAIL", alDetails);

    gmDBManager.close();

    if (alDetails.size() == 0) {
      // strMessage = "No backorder request for " + strREQId;
      strMessage = "All parts have been removed and " + strREQId + " has been voided";
      throw new AppError(strMessage, "", 'E');
    }

    return hmReturn;

  }


  /**
   * get request id
   * 
   * @param strConsignmentId
   * @return
   * @throws AppError
   */
  public String fetchRequestId(String strConsignmentId) throws AppError {
    String strMaterialRequestId = "";
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmValue = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // To fetch the transaction information
    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_mrid_from_csnid", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strConsignmentId);
    gmDBManager.execute();
    strMaterialRequestId = gmDBManager.getString(2);
    gmDBManager.close();

    /*
     * sbQuery.append( " SELECT   C520_REQUEST_ID  MRID " ); sbQuery.append(
     * " FROM T504_CONSIGNMENT "); sbQuery.append( " WHERE C504_CONSIGNMENT_ID  ='");
     * sbQuery.append(strConsignmentId); sbQuery.append( "'");
     * 
     * log.debug(" Query to fetch Request Id from CN is " + sbQuery.toString());
     * 
     * 
     * hmValue = gmDBManager.querySingleRecord(sbQuery.toString()); strMaterialRequestId =
     * GmCommonClass.parseNull((String)hmValue.get("MRID"));
     */

    return strMaterialRequestId;
  }

  public String fetchOUSRequestId(HashMap hmParam) throws AppError {
    String strMaterialRequestId = "";
    HashMap hmValue = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    String strConsignmentId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));

    gmDBManager.setPrepareString("gm_pkg_oppr_request_summary.gm_fch_mrid_from_csnid", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strConsignmentId);
    gmDBManager.execute();
    strMaterialRequestId = gmDBManager.getString(2);
    gmDBManager.close();

    return strMaterialRequestId;
  }


  /**
   * fetchConsignmentDetail - This Method is used to get Consignment details for set building
   * 
   * @param String strRequestId
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchConsignmentDetail(String strConsignmentId, String strRefConsignmentId)
      throws AppError {

    ArrayList alResult;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // To fetch the transaction information
    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_summary_csg_detail", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignmentId);
    gmDBManager.setString(2, strRefConsignmentId);
    gmDBManager.execute();

    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    gmDBManager.close();

    return alResult;
  }

  /*
   * fetchConsignmentIdFromMR - fetches consignment id from MR id
   * 
   * @param String strRequestId
   * 
   * @return ArrayList
   * 
   * @exception AppError
   */
  public String fetchConsignmentIdFromMR(String strRequestId) throws AppError {
    String strConsignmentId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // To fetch the transaction information
    gmDBManager.setPrepareString("gm_pkg_op_request_summary.gm_fch_CsgId_From_MRId", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.execute();

    strConsignmentId = gmDBManager.getString(2);

    gmDBManager.close();
    log.debug(" Consignment Id from MR is " + strConsignmentId);

    return strConsignmentId;
  }

  /**
   * loadRequestMasterReport - This method will be used to fetch the data for all REquests
   * 
   * @param hmParam - to be used later for any filters
   * @exception AppError
   */
  public RowSetDynaClass loadRequestMasterReport(HashMap hmParam) throws AppError {
    ArrayList alRequestMaster = new ArrayList();
    RowSetDynaClass resultSet = null;

    String strReqId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strReqTxnID = GmCommonClass.parseZero((String) hmParam.get("REQUESTTXNID"));
    String strReqDate = GmCommonClass.parseNull((String) hmParam.get("REQUESTDATE"));
    String strReqUDate = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDATE"));
    String strReqSource = GmCommonClass.parseZero((String) hmParam.get("REQUESTSOURCE"));
    String strStatus = GmCommonClass.parseZero((String) hmParam.get("REQUESTSTATUS"));
    String strStatusval = GmCommonClass.parseNull((String) hmParam.get("REQUESTSTATUSVAL"));
    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strReqFor = GmCommonClass.parseZero((String) hmParam.get("REQUESTFOR"));
    String strReqTo = GmCommonClass.parseZero((String) hmParam.get("REQUESTTO"));
    String strSetOrPart = GmCommonClass.parseNull((String) hmParam.get("SETORPART"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));

    String strReqDateTo = GmCommonClass.parseNull((String) hmParam.get("REQUESTDATETO"));
    String strReqUDateTo = GmCommonClass.parseNull((String) hmParam.get("REQUIREDDATETO"));
    String strMasterReqID = GmCommonClass.parseNull((String) hmParam.get("MASTERREQID"));
    String strMainOrChild = GmCommonClass.parseNull((String) hmParam.get("MAINORCHILD"));
    String strConsignID = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));

    String strConStatusFlag = GmCommonClass.parseNull((String) hmParam.get("CONSTATUSFLAG"));

    String strShipFromDate = GmCommonClass.parseNull((String) hmParam.get("HSHIPFROMDATE"));
    String strShipToDate = GmCommonClass.parseNull((String) hmParam.get("HSHIPTODATE"));

    String StrDepid = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strTeamID = GmCommonClass.parseZero((String) hmParam.get("TEAMID"));// checking the
                                                                               // condtion only for
                                                                               // zero in the query
    // condtion only for
    String strDtFmt = getCompDateFmt();// GmCommonClass.parseNull((String)
    String strTagID = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
    String strReprocessID = GmCommonClass.parseNull((String) hmParam.get("REPROCESSID"));
    String strCompPlantFilter = "";
    String strRuleGroupId = "";

    strRuleGroupId = strSetOrPart.equals("50637") ? "ITEM_REPORT" : "SET_DASH_REPORT"; // 50637-By
                                                                                       // Item &
                                                                                       // 50636 -By
                                                                                       // Set
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), strRuleGroupId));
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT t520.C520_REQUEST_ID REQID, get_log_flag (t520.C520_REQUEST_ID, 1235) clog, t520.C520_REQUEST_DATE REQDT, ");
      sbQuery.append(" t520.C207_SET_ID SETID, get_set_name(t520.C207_SET_ID) NAME, ");
      sbQuery.append(" NVL(t504.C504_CONSIGNMENT_ID,'-' ) CONID, ");
      sbQuery.append(" get_consignment_part_attribute(t504.C504_CONSIGNMENT_ID,92340)TAG, ");
      sbQuery.append(" t520.C520_REQUIRED_DATE REQUDT, ");
      sbQuery.append(" get_code_name(t520.C901_REQUEST_SOURCE) REQSRC, ");
      sbQuery.append(" t520.C901_REQUEST_SOURCE REQSRCCID, ");
      sbQuery.append(" get_code_name(t520.c520_request_for) REQFOR, t520.c520_request_for REQUESTFORID,");
      sbQuery
          .append(" gm_pkg_op_request_summary.get_request_status(t520.C520_STATUS_FL) RQSTATUS, ");
      sbQuery.append(" t504.c504_status_fl CNSTFL, ");
      sbQuery
          .append(" decode(t504.c504_status_fl,'0','Initiated','1','WIP','1.10','Completed Set','1.20','Pending Putaway','2','Built Set','2.20','Pending Pick' ");
      sbQuery
          .append(" ,'3','Pending Shipping','3.30','Packing In Progress','3.60','Ready For Pickup','4',decode(c504_type, '26240144', 'Returned Stock', 'Closed'),NULL) CNSTATUS, ");
      // 40021: Consignment; 40022: In-House Consignment; 102930: Inter Company Transfer; 40025:
      // Account Consignment
      sbQuery
          .append(" DECODE(t520.c520_request_for, 40021, get_distributor_name (t520.c520_request_to), ");
      sbQuery
          .append(" 40022, get_user_name (t520.c520_request_to),102930, get_distributor_name (t520.c520_request_to),40025,GET_ACCOUNT_NAME(t520.c520_request_to)) REQTO, ");
      sbQuery.append(" DECODE(t520.C520_STATUS_FL, '15', ");
      sbQuery
          .append(" gm_pkg_op_request_summary.get_consg_crit_cnt(t504.C504_CONSIGNMENT_ID),NULL) crtqty, ");
      sbQuery.append("DECODE(t520.C520_STATUS_FL, '15', ");
      sbQuery
          .append(" gm_pkg_op_request_summary.get_consg_comp_cnt (t504.C504_CONSIGNMENT_ID),NULL) cmpqty,");
      sbQuery.append("t4020.C4020_DEMAND_NM sheetnm, ");
      sbQuery.append("t520.C520_STATUS_FL STFLG, NVL(t520.c901_purpose,0) purpose");
      //PMT-36376 - 7-Add Priority in 'Process Request Set Screen'-Add Priority Column in Process Request Screen
      sbQuery.append(",t520.C5403_REQUEST_PRIORITY PRIORITYID "); 
      sbQuery.append(",GM_PKG_OP_INV_SCAN.GET_LOCATION_CODE(T504.C5052_LOCATION_ID) SETWIPLOCATION "); 
      sbQuery.append(" From t520_request t520, t504_consignment t504,t4020_demand_master t4020");
      if (!strTagID.equals("")) {
        sbQuery.append(", t5010_tag t5010 ");
      }
      sbQuery.append(" WHERE t520.C520_VOID_FL IS NULL ");
      sbQuery.append(" AND T504.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND t520.C520_REQUEST_ID = t504.C520_REQUEST_ID (+) ");      
      sbQuery.append("AND t520.C520_REQUEST_TXN_ID =  t4020.C4020_DEMAND_MASTER_ID (+)");
      sbQuery.append("AND t520.C5040_PLANT_ID = t504.C5040_PLANT_ID (+)");
      sbQuery.append(" AND ( t520.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbQuery.append(" OR t520.C5040_PLANT_ID = " + strCompPlantFilter);
      sbQuery.append(") ");
      if (strConStatusFlag.equals("Initiated")) {
        sbQuery.append(" AND t504.C504_STATUS_FL = 0 ");

      } else if (strConStatusFlag.equals("WIP")) {
        // In Set Overview Report screen, under the WIP status, following consignment status should
        // come: 0-Initiated, 1-WIP, 2.20-Pending Pick with request status Back Log
        sbQuery.append(" AND t504.C504_STATUS_FL IN (0,1,2.20) AND t504.C504_VERIFY_FL = 0");
      }

      else if (strConStatusFlag.equals("PendVer")) {
        // In Set Overview Report screen, under the Pending Verification status, following
        // consignment status should come: 1.10-Completed, 2- Built Set with request status Back Log
        sbQuery.append(" AND t504.C504_STATUS_FL IN (2,1.10) AND t504.C504_VERIFY_FL =0 ");
      }

      else if (strConStatusFlag.equals("Shipped")) {
        sbQuery.append(" AND t504.C504_STATUS_FL = 4 ");

      }

      if (!strTagID.equals("")) {
        sbQuery.append(" AND t5010.c5010_last_updated_trans_id = t504.c504_consignment_id ");
        sbQuery.append(" AND t5010.c5010_tag_id = '" + strTagID + "' ");
        sbQuery.append(" AND t5010.c5010_void_fl IS NULL ");
      }

      if (!strReqId.equals("")) {
        sbQuery.append(" AND t520.C520_REQUEST_ID = '");
        sbQuery.append(strReqId);
        sbQuery.append("'");
      }

      // REQUEST DATE CONDITION
      if (!strReqDate.equals("")) {
        sbQuery.append(" AND t520.C520_REQUEST_DATE >= ");
        sbQuery.append("TO_DATE ('");
        sbQuery.append(strReqDate);
        sbQuery.append("', '" + strDtFmt + "')");
      }
      if (!strReqDateTo.equals("")) {
        sbQuery.append(" AND t520.C520_REQUEST_DATE <= ");
        sbQuery.append("TO_DATE ('");
        sbQuery.append(strReqDateTo);
        sbQuery.append("', '" + strDtFmt + "')");
      }

      // REQUIRED DATE CONDITION
      if (!strReqUDate.equals("")) {
        sbQuery.append(" AND t520.C520_REQUIRED_DATE >= ");
        sbQuery.append("TO_DATE ('");
        sbQuery.append(strReqUDate);
        sbQuery.append("', '" + strDtFmt + "')");
      }
      if (!strReqUDate.equals("")) {
        sbQuery.append(" AND t520.C520_REQUIRED_DATE <= ");
        sbQuery.append("TO_DATE ('");
        sbQuery.append(strReqUDateTo);
        sbQuery.append("', '" + strDtFmt + "')");
      }

      if (!strReqSource.equals("0")) {
        sbQuery.append(" AND t520.C901_REQUEST_SOURCE = '");
        sbQuery.append(strReqSource);
        sbQuery.append("'");
      }

      if (!strStatus.equals("0")) {

        if (strStatusval.equals("") || strStatusval.equals("0")) {
          strStatusval = "=";
        }

        sbQuery.append(" AND t520.C520_STATUS_FL  " + strStatusval);
        sbQuery.append(" GET_CODE_NAME_ALT('");
        sbQuery.append(strStatus);
        sbQuery.append("')");
      }



      if (!strSetID.equals("")) {
        sbQuery.append(" AND t520.C207_SET_ID = '");
        sbQuery.append(strSetID);
        sbQuery.append("'");
      }

      if (!strPartNum.equals("")) {
        sbQuery
            .append(" AND  t520.C520_REQUEST_ID  IN (SELECT a.C520_REQUEST_ID From t520_request a , T521_REQUEST_DETAIL b ");
        sbQuery.append("WHERE a.C520_REQUEST_ID = b.C520_REQUEST_ID AND b.C205_PART_NUMBER_ID IN");
        sbQuery.append("('");
        sbQuery.append(GmCommonClass.getStringWithQuotes(strPartNum));
        sbQuery.append("'))");
      }

      if (!strReqFor.equals("0")) {
        sbQuery.append(" AND t520.C520_REQUEST_FOR = '");
        sbQuery.append(strReqFor);
        sbQuery.append("'");
      }

      if (!strReqTo.equals("0")) {
        sbQuery.append(" AND t520.C520_REQUEST_TO = '");
        sbQuery.append(strReqTo);
        sbQuery.append("'");
      }

      if (!strReqTxnID.equals("0")) {
        sbQuery.append(" AND t520.C520_REQUEST_TXN_ID = '");
        sbQuery.append(strReqTxnID);
        sbQuery.append("'");
      }

      if (!strTeamID.equals("0")) {

        sbQuery.append("AND t520.C520_REQUEST_TXN_ID in (");
        sbQuery.append(" SELECT t520.c520_request_txn_id ");
        sbQuery
            .append(" FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020, t520_request t520 ");
        sbQuery.append(" WHERE c901_level_value IN ( ");
        sbQuery.append(" SELECT DISTINCT v700.region_id FROM v700_territory_mapping_detail v700 ");
        sbQuery.append(" WHERE (v700.divid = '");
        sbQuery.append(strTeamID);
        sbQuery.append("' OR v700.region_id ='");
        sbQuery.append(strTeamID);
        sbQuery.append("')  UNION");
        sbQuery.append(" SELECT DISTINCT v700.region_id FROM v700_territory_mapping_detail v700 ");
        sbQuery.append(" WHERE (v700.gp_id = '");
        sbQuery.append(strTeamID);
        sbQuery.append("')  UNION");
        sbQuery.append(" SELECT DISTINCT v700.compid FROM v700_territory_mapping_detail v700 ");
        sbQuery.append(" WHERE (v700.compid = '");
        sbQuery.append(strTeamID);
        sbQuery.append("')  UNION");
        sbQuery.append(" SELECT DISTINCT v700.divid FROM v700_territory_mapping_detail v700 ");
        sbQuery.append(" WHERE (v700.divid = '");
        sbQuery.append(strTeamID);
        sbQuery
            .append("') ) AND t4015.c4015_global_sheet_map_id = t4020.c4015_global_sheet_map_id");
        sbQuery.append(" AND t4020.c4020_demand_master_id    = t520.c520_request_txn_id ");
        sbQuery.append(" AND t4020.C4020_VOID_FL IS NULL ");
        sbQuery.append(" AND t520.C520_VOID_FL  IS NULL )");
      }

      if (!strConsignID.equals("")) {
        sbQuery.append(" AND t504.C504_CONSIGNMENT_ID = '");
        sbQuery.append(strConsignID);
        sbQuery.append("'");
      }
      
      /* reprocess*/
      
      if (!strReprocessID.equals("")) {
          sbQuery.append(" AND t504.C504_REPROCESS_ID like'%");
          sbQuery.append(strReprocessID);
          sbQuery.append("' ");
        }

      if (!strMasterReqID.equals("")) {
        sbQuery.append(" AND t520.C520_MASTER_REQUEST_ID = '");
        sbQuery.append(strMasterReqID);
        sbQuery.append("'");
      }

      if (strMainOrChild.equals("50638")) // MAIN REQUEST
      {
        sbQuery.append(" AND t520.C520_MASTER_REQUEST_ID IS NULL ");

      }

      if (strMainOrChild.equals("50639")) // CHILD REQUEST
      {
        sbQuery.append(" AND t520.C520_MASTER_REQUEST_ID IS NOT NULL ");
      }

      if (strSetOrPart.equals("50636")) {
        sbQuery.append(" AND t520.C207_SET_ID IS NOT NULL");
      }

      if (strSetOrPart.equals("50637")) {
        sbQuery.append(" AND t520.C207_SET_ID IS NULL ");
      }


      if (!strShipFromDate.equals("") && !strShipToDate.equals("")) {

        sbQuery.append(" AND t504.c504_ship_date between ");
        sbQuery.append(" to_date('");
        sbQuery.append(strShipFromDate);
        sbQuery.append("', '" + strDtFmt + "') AND to_date('");
        sbQuery.append(strShipToDate);
        sbQuery.append("', '" + strDtFmt + "') ");
      }
      log.debug("Query for loadRequestMasterReport: " + sbQuery.toString());


      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    }

    catch (Exception e) {
      GmLogError.log("Exception in GmRequestReportBean:loadRequestMasterReport", "Exception is:"
          + e);
      throw new AppError(e);
    }
    return resultSet;
  }



  public HashMap findRuleID(String strSetID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    HashMap hmResult = new HashMap();
    String strRuleID = "";

    sbQuery
        .append("select case when count(*) = 1 then c9700_rule_id else 0 end ruleid  from t9702_rule_condition where  ");
    sbQuery
        .append("c9700_rule_id in (select c9700_rule_id from t9702_rule_condition where c9702_condition_value='");
    sbQuery.append(strSetID);
    sbQuery.append("') group by c9700_rule_id");
    try {
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmRequestReportBean:findRuleID", "Exception is:" + e);
      throw new AppError(e);
    }
    return hmResult;

  }

  /**
   * loadTTPLevelListTeam - This method will be used to fetch the Team Drpdwn values for in process
   * request.
   * 
   * @param
   * @return ArrayList - will be used as drpdown values
   * @exception AppError
   */
  public ArrayList loadTTPLevelListTeam() throws AppError {
    ArrayList alTTPLevelList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C906_RULE_ID ID , get_code_name(C906_RULE_ID) NAME  ");
    sbQuery.append(" FROM t906_rules ");
    sbQuery.append(" WHERE C906_VOID_FL IS NULL ");
    sbQuery.append(" AND c906_rule_grp_id = 'TEAMLVL'");
    sbQuery.append(" ORDER BY c906_rule_seq_id ");
    log.debug("Query inside loadTTPLevelListTeam is " + sbQuery.toString());
    alTTPLevelList =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    log.debug("alTTPLevelList " + alTTPLevelList);
    return alTTPLevelList;
  }


  public void validateRequest(String strREQId, String strAction) throws AppError {
    HashMap hmHeader = fetchRequestHeader(strREQId);
    String strFlag = GmCommonClass.parseNull((String) hmHeader.get("REQUESTSTATUSFLAG"));
    int intErrorCode = 0;



    if (strAction.equals("ConsignmentReconfig")) {
      log.debug("strFlag: " + strFlag);
      if (strFlag.equals("10")) // back ordered
      {
        intErrorCode = 20700;
        // 20078 Request marked as Back Order, No Parts available to Reconfigure CN.
      } else if (strFlag.equals("40")) // Completed
      {
        intErrorCode = 20701;
        // 20085 Request already processed, can't reconfigure.
      } else if (strFlag.equals("30")) // Ready to Ship
      {
        intErrorCode = 20702;
        // 20086 Request is about to ship, can't reconfigure.
      }
    }

    if (strAction.equals("ShipOut")) {
      if (strFlag.equals("40")) // Completed
      {
        intErrorCode = 20703;
        // 20085 Request already processed, can't can't process again.
      }
    }

    if (strAction.equals("SETBUILDPROCESS")) {
      String strSetId = GmCommonClass.parseNull((String) hmHeader.get("SETID"));
      if (strSetId.equals("")) // back ordered
      {
        intErrorCode = 20704;
        // 20089 Request marked as Back Order, No Parts available to Reconfigure CN.
      }
    }
    log.debug("intErrorCode-->" + intErrorCode);
    if (intErrorCode != 0) {
      throw new AppError(new SQLException("Error Occured", null, intErrorCode));
    }
  }

  public ArrayList loadProductSummaryReport(HashMap hmParam) throws AppError {

    log.debug("hmParam: " + hmParam);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    initializeParameters(hmParam);
    String strDist = GmCommonClass.parseNull((String) hmParam.get("DIST"));
    String strRep = GmCommonClass.parseNull((String) hmParam.get("REP"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("STRDEPT"));

    String strstatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    log.debug("strstatus: " + strstatus);

    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    log.debug("strFromDate: " + strFromDate);

    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    log.debug("strToDate: " + strToDate);

    strDist = (strDist.equals("0")) ? "" : strDist;
    strRep = (strRep.equals("0")) ? "" : strRep;

    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "REQUEST_SUMMARY"));/*
                                                                                             * REQUEST_SUMMARY
                                                                                             * - To
                                                                                             * fetch
                                                                                             * the
                                                                                             * records
                                                                                             * for
                                                                                             * EDC
                                                                                             * entity
                                                                                             * countries
                                                                                             * based
                                                                                             * on
                                                                                             * plant
                                                                                             */
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    log.debug("strDeptId: " + strDeptId);
    log.debug("hmParam: " + hmParam);

    gmDBManager.setPrepareString("gm_pkg_op_product_requests.gm_find_requests", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.INTEGER);
    gmDBManager.setString(1, strDist);
    gmDBManager.setString(2, strRep);
    gmDBManager.setString(3, strstatus);
    gmDBManager.setString(4, strFromDate);
    gmDBManager.setString(5, strToDate);
    gmDBManager.execute();
    int iResult = gmDBManager.getInt(6);
    gmDBManager.commit();

    log.debug("iResult: " + iResult);

    log.debug("strRep: " + strRep);

    sbQuery.append(" SELECT * FROM ");
    sbQuery
        .append(" (SELECT t525.c525_product_request_id pr_id, get_distributor_name (t525.c525_request_for_id) dname,");
    sbQuery
        .append(" t525.c525_request_for_id did,get_rep_name (t525.c703_sales_rep_id) repname,t525.c703_sales_rep_id repid,");
    sbQuery
        .append(" get_account_name (t525.c704_account_id) acc_name,t525.c704_account_id acc_id, NVL(requested,0) requested, NVL(shipped,0) shipped, NVL(returned,0) returned, NVL(MISSING,0) MISSING, NVL(reconciled,0) reconciled, NVL(unreconciled,0) unreconciled, NVL(unrecon_parts,0) unrecon_parts, NVL(charges,0) charges, NVL(tickets,0) tickets");
    sbQuery.append(" FROM t525_product_request t525 ");

    if (strDeptId.equals("S")) {
      sbQuery.append(getAccessFilterClauseWithRepID());
    }

    // Request Details

    sbQuery.append(" ,(SELECT   c525_product_request_id, SUM (requested) requested,");
    sbQuery.append(" SUM (shipped) shipped, SUM (returned) returned ");
    sbQuery.append(" FROM (SELECT t525.c525_product_request_id,");
    sbQuery
        .append(" t504.c504_consignment_id cn_id, 1 requested, DECODE (t526.c526_status_fl, 30, 1, 0) shipped,DECODE (t504a.c504a_return_dt, '', 0, 1) returned");
    sbQuery
        .append(" FROM t525_product_request t525,t526_product_request_detail t526,t504a_loaner_transaction t504a,t504_consignment t504");
    sbQuery.append(" WHERE t525.c525_product_request_id = t526.c525_product_request_id");
    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id(+)");
    sbQuery.append(" AND t504.c504_consignment_id(+) = t504a.c504_consignment_id");
    sbQuery.append(" AND t525.c525_void_fl IS NULL");
    sbQuery.append(" AND t526.c526_void_fl IS NULL");
    sbQuery.append(" AND t504a.c504a_void_fl(+) IS NULL");
    sbQuery.append(" AND t504.c504_void_fl(+) IS NULL");
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    sbQuery
        .append(" AND t525.c525_product_request_id IN (SELECT MY_TEMP_TXN_ID FROM my_temp_list))");
    sbQuery.append(" GROUP BY c525_product_request_id) req_det");

    // Missing & Reconilication Details

    sbQuery
        .append(",( SELECT c525_product_request_id, SUM(MISSING) MISSING, SUM(reconciled) reconciled, SUM(unreconciled) unreconciled, SUM(UNRECON_PARTS) unrecon_parts ");
    sbQuery.append(" FROM ");
    sbQuery
        .append(" (SELECT   t525.c525_product_request_id, t526.c526_product_request_detail_id,  ");
    sbQuery.append(" SUM (DECODE (t413.c413_status_fl, ");
    sbQuery.append(" 'Q', NVL (t413.c413_item_qty, 0), ");
    sbQuery.append(" 0 )) MISSING, ");
    sbQuery.append(" SUM (DECODE (c413_status_fl, ");
    sbQuery.append(" 'Q', 0, ");
    sbQuery.append(" NVL (c413_item_qty, 0) ");
    sbQuery.append(" ) ");
    sbQuery.append(" ) reconciled, ");
    sbQuery.append(" (SUM ");
    sbQuery.append(" (DECODE (t413.c413_status_fl, ");
    sbQuery.append(" 'Q', NVL ");
    sbQuery.append(" (t413.c413_item_qty, ");
    sbQuery.append(" 0 ");
    sbQuery.append(" ), ");
    sbQuery.append(" 0 ");
    sbQuery.append(" ) ");
    sbQuery.append(" ) ");
    sbQuery.append(" - SUM (DECODE (c413_status_fl, ");
    sbQuery.append(" 'Q', 0, ");
    sbQuery.append(" NVL (t413.c413_item_qty, ");
    sbQuery.append(" 0 ");
    sbQuery.append(" ) ");
    sbQuery.append(" ) ");
    sbQuery.append(" )) UNRECON_PARTS, ");
    sbQuery.append(" DECODE(SUM ");
    sbQuery.append(" (DECODE (t413.c413_status_fl, ");
    sbQuery.append(" 'Q', NVL (t413.c413_item_qty, 0), ");
    sbQuery.append(" 0)) ");
    sbQuery.append(" - SUM (DECODE (c413_status_fl, ");
    sbQuery.append(" 'Q', 0, ");
    sbQuery.append(" NVL (t413.c413_item_qty, 0))),0,0,1) unreconciled ");
    sbQuery.append(" FROM t525_product_request t525, ");
    sbQuery.append(" t526_product_request_detail t526, ");
    sbQuery.append(" t504a_loaner_transaction t504a, ");
    sbQuery.append(" t504_consignment t504, ");
    sbQuery.append(" t412_inhouse_transactions t412, ");
    sbQuery.append(" t413_inhouse_trans_items t413 ");
    sbQuery.append(" WHERE t525.c525_product_request_id = ");
    sbQuery.append(" t526.c525_product_request_id ");
    sbQuery
        .append(" AND t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id(+) ");
    sbQuery.append(" AND t504.c504_consignment_id(+) = ");
    sbQuery.append(" t504a.c504_consignment_id ");
    sbQuery.append(" AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id(+) ");
    sbQuery.append(" AND t412.c412_inhouse_trans_id = t413.c412_inhouse_trans_id(+) ");
    sbQuery.append(" AND t412.c412_void_fl(+) IS NULL ");
    sbQuery.append(" AND t525.c525_void_fl IS NULL ");
    sbQuery.append(" AND t526.c526_void_fl IS NULL ");
    sbQuery.append(" AND t504a.c504a_void_fl(+) IS NULL ");
    sbQuery.append(" AND t504.c504_void_fl(+) IS NULL ");
    sbQuery.append(" AND t412.c412_type(+) = 50159 ");
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    sbQuery.append(" AND t525.c525_product_request_id IN ( ");
    sbQuery.append(" SELECT my_temp_txn_id ");
    sbQuery.append(" FROM my_temp_list) ");
    sbQuery.append(" GROUP BY t525.c525_product_request_id, t526.c526_product_request_detail_id )");
    sbQuery.append(" GROUP BY c525_product_request_id) miss_recon ");

    // Ticket Details

    sbQuery.append(",(SELECT   t526.c525_product_request_id,COUNT (gm_pkg_cm_loaner_jobs.get_jira_ticket_id(t526.c526_product_request_detail_id, t526.c525_product_request_id)) tickets");
    sbQuery.append(" FROM t526_product_request_detail t526");    
    sbQuery.append(" AND t526.c526_void_fl IS NULL");
    sbQuery
        .append(" AND t526.c525_product_request_id IN (SELECT MY_TEMP_TXN_ID FROM my_temp_list)");
    sbQuery.append(" GROUP BY t526.c525_product_request_id) tkt_det");

    // Charge Details

    sbQuery
        .append(",(SELECT   t526.c525_product_request_id,SUM (NVL (t9201.c9201_amount, 0)) charges");
    sbQuery
        .append(" FROM t9200_incident t9200,t9201_charge_details t9201,t504a_loaner_transaction t504a,t526_product_request_detail t526,t504_consignment t504");
    sbQuery
        .append(" WHERE t526.c526_product_request_detail_id = t504a.c526_product_request_detail_id(+)");
    sbQuery.append(" AND t504.c504_consignment_id(+) = t504a.c504_consignment_id");
    sbQuery.append(" AND t504a.c504a_loaner_transaction_id = t9200.c9200_ref_id(+)");
    sbQuery.append(" AND t9200.c901_incident_type(+) = 92072");
    sbQuery.append(" AND t9200.c9200_ref_type(+) = 4127");
    sbQuery.append(" AND t9200.c9200_incident_id = t9201.c9200_incident_id(+)");
    sbQuery.append(" AND t9201.c9201_void_fl IS NULL");
    sbQuery.append(" AND t9200.c9200_void_fl IS NULL");
    sbQuery.append(" AND t9201.c9201_status(+) != 40");
    sbQuery
        .append(" AND t526.c525_product_request_id IN (SELECT MY_TEMP_TXN_ID FROM my_temp_list)");
    sbQuery.append(" GROUP BY t526.c525_product_request_id) chrg_det");
    sbQuery.append(" WHERE t525.c525_product_request_id = req_det.c525_product_request_id(+)");
    sbQuery.append(" AND t525.c525_product_request_id = miss_recon.c525_product_request_id(+)");
    sbQuery.append(" AND t525.c525_product_request_id = tkt_det.c525_product_request_id(+)");
    sbQuery.append(" AND t525.c525_product_request_id = chrg_det.c525_product_request_id(+)");
    sbQuery
        .append(" AND t525.c525_product_request_id IN (SELECT MY_TEMP_TXN_ID FROM my_temp_list)");

    if (strDeptId.equals("S")) {
      sbQuery.append(" AND t525.c703_sales_rep_id = V700.rep_id");
    }
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    // sbQuery.append(" AND t525.c704_account_id = V700.ac_id(+)");
    sbQuery.append(" ) t525");


    log.debug("query for summary " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());



    return alReturn;
  }

  /**
   * @author rjayakumar
   * @param RequestId
   * @return String
   * @throws AppError
   */
  public String fetchRequestAttribute(String strRequestId, String strType) throws AppError {
    String strAttbValue = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_op_request_master.get_request_attribute", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strRequestId);
    gmDBManager.setString(3, strType);
    gmDBManager.execute();
    strAttbValue = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strAttbValue;
  }

  /**
   * fetchReqfl() method used to get Request flag.
   * 
   * @author Jignesh Shah
   * @param strReqId
   * @return String
   * @throws AppError
   */
  public String fetchReqfl(String strReqId) throws AppError {
    log.debug("Enter into fetchReqfl() - strReqId ====> " + strReqId);
    String strReqfl = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_op_fch_ln_req_fl", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strReqId);
    gmDBManager.execute();
    strReqfl = gmDBManager.getString(2);
    gmDBManager.close();
    return strReqfl;
  }// End of fetchReqfl()

  // removed the function loadSetShipDetails() [the changes of BUG-4161] for PMT-7461

}// End of GmRequestReportBean
