/**
 * 
 */
package com.globus.operations.requests.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ppandiyan
 *
 */
public class GmStockInitiateBean extends GmBean {
  
  public GmStockInitiateBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());

  
  public String saveStockRequest(HashMap hmParam, ArrayList alReturn) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

    String strMaterialRequestId = "";

    String strRequestSource = "50618";
    String strRequestByType = "50625";
    StringBuffer sbMessage = new StringBuffer();

    log.debug(" values to save " + hmParam);

    String strRequestType = GmCommonClass.parseNull((String) hmParam.get("REQUESTTYPE"));

    String strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDepartmentId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    Date dtRequiredDate = (Date) hmParam.get("REQUIREDDATE");
    String strShipMode = GmCommonClass.parseNull((String) hmParam.get("SHIPMODE"));
    String strShipCarrier = GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER"));
    String strShipToId = GmCommonClass.parseNull((String) hmParam.get("SHIPTOID"));

    strShipTo = strShipTo.equals("0") ? "50625" : strShipTo;

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));

      strInputString = strInputString + "~" + strShipCarrier + "^" + strShipMode + "|";

    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    String strReqComments = GmCommonClass.parseNull((String) hmParam.get("HADDINFO"));

    if (strDepartmentId.equals("2014")) {
      strRequestSource = "50617";
    } else {
      strRequestSource = "50618";
    }

    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_op_process_prod_request.gm_sav_initiate_request", 24);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(22, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(23, OracleTypes.VARCHAR);

    gmDBManager.setString(1, "");
    gmDBManager.setDate(2, dtRequiredDate);
    gmDBManager.setString(3, strRequestSource);
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, "");
    gmDBManager.setInt(6, Integer.parseInt(strRequestType));
    gmDBManager.setString(7, "");
    gmDBManager.setInt(8, Integer.parseInt(strRequestByType));
    gmDBManager.setString(9, strUserId);
    gmDBManager.setString(10, strShipTo);
    gmDBManager.setString(11, strShipToId);
    gmDBManager.setString(12, "");
    gmDBManager.setInt(13, 15); // Status 15 is backlog as per the existing flow
    gmDBManager.setString(14, strUserId);
    gmDBManager.setString(15, strInputString);
    gmDBManager.setString(16, "");
    gmDBManager.setString(17, "");
    gmDBManager.setString(18, "");
    gmDBManager.setString(19, "");
    gmDBManager.setDate(20, null);
    gmDBManager.setString(21, "");
    gmDBManager.setString(24, strReqComments);

    gmDBManager.execute();
    strMaterialRequestId = GmCommonClass.parseNull(gmDBManager.getString(22));
    log.debug("strMaterialRequestId " + strMaterialRequestId);


    strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));

    if (strMaterialRequestId != "") {
      if (!strShipTo.equals("") && !strShipTo.equals("0")) {
        log.debug("saving shipping record");

          hmParam.put("REFID", strMaterialRequestId);
          hmParam.put("SOURCE", "26240435"); // request

        gmShippingTransBean.saveShipDetails(gmDBManager, hmParam);
      }
      hmParam.put("MATERIALREQID", strMaterialRequestId);
      if (!strMaterialRequestId.equals("")) {      // to save the fulfill company for the request
        saveAssociatedFulfillCompany(gmDBManager, hmParam);
      }

    }

 if (!strLog.equals("")) {
     gmCommonBean.saveLog(gmDBManager, strMaterialRequestId, strLog, strUserId, "1235");
    }
    if (!strRequestType.equals("4127")) {
      gmTxnSplitBean.checkAndSplitConsignamnet(gmDBManager, strMaterialRequestId, strUserId);
    }

    gmDBManager.commit();


    return strMaterialRequestId;
  }
  
  
  /**
   * saveAssociatedFulfillCompany - This method will be save corresponding fulfill company
   * 
   * @param hmParam,gmDBManager - parameters to be saved
   * @exception AppError
   */
  public void saveAssociatedFulfillCompany(GmDBManager gmDBManager, HashMap hmParam)
      throws AppError {

    log.debug("calling new fulfil method" +hmParam);
    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("MATERIALREQID"));
    String strFulfillCompId = GmCommonClass.parseNull((String) hmParam.get("FULFILLCOMPID"));
    Date dtRequiredDate = (Date) hmParam.get("REQUIREDDATE");
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    

    gmDBManager.setPrepareString("gm_pkg_op_request_fulfill.gm_sav_request_fulfill_company", 4);

    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strFulfillCompId);
    gmDBManager.setDate(3, dtRequiredDate);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();

  }
  /**
   * saveReconfigureRequest - This method will  save the reconfigure request
   * 
   * @param string  - parameters to be saved
   * @exception AppError
   */
  public void saveReconfigureRequest(String requestID, String strInputString, String strUserId)
      throws AppError {
    String strMessage = "";
    String  strReqVoidfl = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_stock_transfer.gm_sav_reconfig_request", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.setString(1, requestID);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    strReqVoidfl = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.commit();
    
    if (strReqVoidfl.equals("Y")) { // to throw error if all the parts are voided
      strMessage = "All parts have been removed and " + requestID + " has been voided";
      throw new AppError(strMessage, "", 'E');
    }

  }
  
  /**
   * fetchRequestShipDetails -- to fetch the ship details of the request
   * 
   * @param strRequestId
   * @return
   * @throws AppError
   */
  public HashMap fetchRequestShipDetails(String strRequestId) throws AppError {

    HashMap hmShipDtls = new HashMap();


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    gmDBManager.setPrepareString("gm_pkg_op_stock_transfer.gm_fch_request_ship_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);

    gmDBManager.execute();

    hmShipDtls = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmShipDtls;
}
}
