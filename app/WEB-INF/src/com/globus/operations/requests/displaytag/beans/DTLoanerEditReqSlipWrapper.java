/**
 * FileName    : DTLoanerRequest.java 
 * Description :
 * Author      : rshah
 * Date        : Mar 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.requests.displaytag.beans;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.requests.forms.GmLoanerReqEditForm;

/**
 * @author rshah
 *
 */
public class DTLoanerEditReqSlipWrapper extends TableDecorator {
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	Logger log = GmLogger.getInstance(this.getClass().getName());
	private HashMap hmParam = new HashMap();
	String strRes = "";
	private DynaBean db = null;
	private GmLoanerReqEditForm gmLoanerReqEditForm;
    private String strOpt;
    private int intSNo = 0;
     
	/**
	* Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
	*/
	public DTLoanerEditReqSlipWrapper()
	{
		super();		
	}
	
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmLoanerReqEditForm = (GmLoanerReqEditForm) getPageContext().getAttribute("frmLoanerReqEdit", PageContext.REQUEST_SCOPE);
		if(gmLoanerReqEditForm != null){
			strOpt = gmLoanerReqEditForm.getStrOpt();
		}	
	}
	
	public String getVOIDFL() {
		db =  	(DynaBean) this.getCurrentRowObject();  
		String strID = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		
		
		StringBuffer strValue = new StringBuffer();
		String strDisable = "disabled";
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		
		if(!strStatus.equals("30") && !strStatus.equals("40"))
		{
			strDisable = "";
		}
		strValue.append("<table><tr><td>");
		strValue.append("<input type=\"checkbox\" "+strDisable+" onClick=\"fnEnableDiv(this,'"+strID+"')\"; value=\"\" name=\"Chk_voidfl\" id=");
		strValue.append(strID);
		strValue.append(">");
		strValue.append("</td><td><DIV style=display:none id = voidrsn");
		strValue.append(strID);
		strValue.append("></DIV>");		
		
		strValue.append("</td></tr></table>");
		strRes = strValue.toString();		
		return strRes;
	}
	public String getDATE()
	{
		String  strApplJSDateFmt=GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		String  strApplDateFmt=GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
		String  strSetID ="";
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		strSetID = GmCommonClass.parseNull(String.valueOf(db.get("SETID")));
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		
		StringBuffer strBufVal = new StringBuffer();
		String strNo = GmCommonClass.parseNull(String.valueOf(db.get("NO")));
		String strId = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		java.sql.Timestamp dtShipDate = null;
		SimpleDateFormat sdfDestination;
		String strShipDate = "";
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}

		
		if(db.get("REQUIREDDT")!=null){
			dtShipDate= (java.sql.Timestamp)(db.get("REQUIREDDT"));
			sdfDestination = new SimpleDateFormat(strApplDateFmt);// the date into another format30.   
			strShipDate = sdfDestination.format(dtShipDate);
		}

		log.debug("The Convert the Date is "+strShipDate);
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strDisable = "disabled";

		if(!strStatus.equals("30") && !strStatus.equals("40"))
		{
			strDisable = "";
		}
		
		strBufVal.append("<input type=\"text\" maxlength=\"10\" "+strDisable+" name=\"shipDt" + strNo+"\" value=\""+strShipDate+"\"");		
		strBufVal.append(" size='9' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strBufVal.append("/>&nbsp;<img id='Img_Date' "+strDisable+" style='cursor:hand'");	
		strBufVal.append("onclick=\"javascript:showSingleCalendar('divShipDt"+strNo+"','shipDt"+strNo+"','"+strApplJSDateFmt+"');\" title='Click to open Calendar'");
		strBufVal.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
		strBufVal.append("<div id=\"divShipDt"+strNo+"\" style=\"position: absolute; z-index: 10;\"></div>");
		strBufVal.append("<input type=\"hidden\" value=\""+strId+"\" name=\"shippedDate\" id=");
        strBufVal.append(strNo);
        strBufVal.append("> &nbsp; ");
        strBufVal.append("<input type=\"hidden\" value=\""+strStatus+"\" name=\"shipStatus\" id=");
        strBufVal.append(strNo);
        strBufVal.append("> &nbsp; ");
        strBufVal.append("<input type=\"hidden\" value=\""+strSetID+"\" name=\"setID"+strNo+"\" id=");
        strBufVal.append(strNo);
        strBufVal.append("> &nbsp; ");
        
        strRes = strBufVal.toString(); 
        return strRes;
  }

	public String getETCHID(){
		StringBuffer strBufVal = new StringBuffer();
		String strNo = GmCommonClass.parseNull(String.valueOf(db.get("NO")));
		String strEtchId = GmCommonClass.parseNull((String)db.get("ETCHID"));
		String strStatus = GmCommonClass.parseNull((String)db.get("STATUS"));
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		
		if(strStatus.equalsIgnoreCase("Allocated") || strStatus.equalsIgnoreCase("Open")){
			strBufVal.append("<input type=\"text\" maxlength=\"20\" name=\"etchId" + strNo+"\" value=\""+strEtchId+"\"");		
			strBufVal.append(" size='20' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
			strBufVal.append("> &nbsp; ");
			strBufVal.append("<input type=\"hidden\" name=\"oldEtchId" + strNo+"\" value=\""+strEtchId+"\"");		
			strBufVal.append("> &nbsp; ");
		}else{
			strBufVal.append(strEtchId);
		}
		strRes = strBufVal.toString(); 
        return strRes;
	}
		
	public String getLOGIMG()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		String strSeq = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		String strFlag = GmCommonClass.parseNull(String.valueOf(db.get("LOG")));
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append(strSeq);
		strBufVal.append("&nbsp;&nbsp;<img style='cursor:hand' src='");
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		
		if(strFlag.equals("N"))
        {
			strBufVal.append(strImagePath+"/phone_icon.jpg'");
        }
        else
        {            
            strBufVal.append(strImagePath+"/phone-icon_ans.gif'");
        }
   		strBufVal.append(" title='Click to add comments' width='17' height='12'" ); 
	   	strBufVal.append(" onClick =\"fnOpenLog('"+strSeq+"','1248');\" </img>");
	   	strRes = strBufVal.toString(); 
	   	return strRes;		
	}
	public String getEDITREQ()  
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		
		
		String strReqId = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		String strCnId = GmCommonClass.parseNull(String.valueOf(db.get("REQCNID")));
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		String strId = (!strCnId.equals("null"))?strCnId:strReqId;
		StringBuffer strBufVal = new StringBuffer();
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strDisable = "disabled";
		String strTitle = "Cannot Edit Shipping Details.";
		
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		
		if(strStatus.equals("20") && !strShipInfoCnt.equals("0") ) // Allocated Status and shipping record exists, Can edit Shipping records.
		{
			strDisable = "";
			strTitle = "Click to Edit Shipping Details";
		}
		
		strBufVal.append("<img style='cursor:hand' "+strDisable+"  src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='"+strTitle+"' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =\"fnEdtId('"+strId+"');\" </img> ");
   		strRes = strBufVal.toString(); 
		
		//return  "<a href= javascript:fnViewInv('" + strPO + "','" + strId + "')>" + "Y" + "</a>" ; ,'" +strSessNm+ "','" + 2 + "','" + 2 + "','" + 2 + "','" + 2 + "' ----  "','"+strSessNm+"','"+strSessStartDt+"','"+strSessEndDt+"','"+strSessOwner+"','"+strSessStatus+
 		return strRes;
	}
	
	public String getBARCODE()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		String strReqCN = GmCommonClass.parseNull(String.valueOf(db.get("REQCNID")));
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("&nbsp;&nbsp;<img src='/GmCommonBarCodeServlet?ID=");
		strBufVal.append(strReqCN);
	   	strBufVal.append("&type=2d' title='Bar Code' width='40' height='40'" ); 
	   	log.debug("strBufVal======="+strBufVal);
	   	strRes = strBufVal.toString(); 
	   	return strRes;
	}
	
	public String getNO()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		int intNo = ((BigDecimal)db.get("NO")).intValue();
		String strNo = "";
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			intSNo += 1;			
			strNo =""; 
		}
		else {
			intNo = intNo - intSNo;
			strNo = intNo+"";
		}
		
		return strNo;
	}
	public String getSETID()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		strRes = GmCommonClass.parseNull(String.valueOf(db.get("SETID")));
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		else 
	   	return strRes;
	}
	public String getSETNM()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		strRes = GmCommonClass.parseNull(String.valueOf(db.get("SETNM")));
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		else 
	   	return strRes;
	}
	public String getQTY()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		strRes = GmCommonClass.parseNull(String.valueOf(db.get("QTY")));
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		else 
	   	return strRes;
	}
	public String getETCHID1()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		strRes = GmCommonClass.parseNull(String.valueOf(db.get("ETCHID1")));
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		else 
	   	return strRes;
	}
	public String getREFID()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		
		strRes = GmCommonClass.parseNull(String.valueOf(db.get("REFID")));
		
		// When there is no shipping record created ,the info should not be displayed in the Delivery/Collection Note.
		String strShipInfoCnt = GmCommonClass.parseZero((String)db.get("SHIP_INFO_CNT"));
		
		if (strShipInfoCnt.equals("0") && strOpt.equalsIgnoreCase("PicSlip")){
			return "";
		}
		else 
	   	return strRes;
	}
}
