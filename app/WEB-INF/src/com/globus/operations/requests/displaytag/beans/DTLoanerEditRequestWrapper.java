/**
 * FileName    : DTLoanerRequest.java 
 * Description :
 * Author      : rshah
 * Date        : Mar 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.requests.displaytag.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.requests.forms.GmLoanerReqEditForm;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author rshah
 *
 */
public class DTLoanerEditRequestWrapper extends TableDecorator {
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	Logger log = GmLogger.getInstance(this.getClass().getName());
	private HashMap hmParam = new HashMap();
	String strRes = "";
	private DynaBean db = null;
	int count = 0,totCount = 0;
	GmLoanerReqEditForm gmLoanerReqEditForm;
	ArrayList alShippingDetails = new ArrayList();
	 private boolean bnSetDelFl = false;
	 private boolean bnPrintFl = false;
	/**
	* Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
	*/
	 public void init(PageContext context, Object decorated, TableModel tableModel) {
			// TODO Auto-generated method stub
			super.init(context, decorated, tableModel);
			gmLoanerReqEditForm = (GmLoanerReqEditForm) getPageContext().getAttribute("frmLoanerReqEdit", PageContext.REQUEST_SCOPE);			
			alShippingDetails = gmLoanerReqEditForm.getAlShippingDetails();			
		}
	public DTLoanerEditRequestWrapper()
	{
		super();		
	}
	
	public String getVOIDFL() {
		db =  	(DynaBean) this.getCurrentRowObject();  
		String strID = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strPartNum = GmCommonClass.parseNull(String.valueOf(db.get("PNUM")));;
		strPartNum = (strPartNum.equals("null"))?"":strPartNum;
		String strSetID = GmCommonClass.parseNull(String.valueOf(db.get("SET_ID")));;
		strSetID = (strSetID.equals("null"))?"":strSetID;
		StringBuffer strValue = new StringBuffer();
		String strDisable = "disabled";
		if(!strStatus.equals("30") && !strStatus.equals("40"))
		{
			strDisable = "";
		}
		/*if request for part and allocated then disable void option.because, we have backend validation as well.*/
		if(!strPartNum.equals("") && strStatus.equals("20")){
			strDisable = "disabled";
		}
		strValue.append("<table><tr><td>");
		strValue.append("<input type=\"checkbox\" "+strDisable+" onClick=\"fnEnableDiv(this,'"+strID+"')\"; value=\"\" name=\"Chk_voidfl\" id=");
		strValue.append(strID);
		strValue.append(">");
		strValue.append("</td><td><DIV style=display:none id = voidrsn");
		strValue.append(strID);
		strValue.append("></DIV>");		
		
		strValue.append("</td></tr></table>");
		if(strStatus.equals("10")){
		if(!bnSetDelFl){
			strValue.append("<input type=\"hidden\" value=\"Y\"  name=\"hOverride\" id=\"hOverride\" >");
			bnSetDelFl = true;
		}
		}
		/*Disable print button if the request does not have a set associated.*/
		if(!strSetID.equals("")){
			if(!bnPrintFl){
				strValue.append("<input type=\"hidden\" value=\"Y\"  name=\"hPrintFl\" name=\"hPrintFl\" >");
				bnPrintFl = true;
			}
		}
		strRes = strValue.toString();		
		return strRes;
	}
	public String getDATE()
	{
		GmDataStoreVO gmDataStoreVO =
		        (GmDataStoreVO) getPageContext().getRequest().getAttribute("gmDataStoreVO");
		    gmDataStoreVO =
		        ((gmDataStoreVO == null) || ((gmDataStoreVO.getCmpdfmt()).equals(""))) ? GmCommonClass
		            .getDefaultGmDataStoreVO() : gmDataStoreVO;
		String  strApplJSDateFmt=GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		String  strApplDateFmt=gmDataStoreVO.getCmpdfmt();
		db =  	(DynaBean) this.getCurrentRowObject(); 
		String strShipDate = "";
		StringBuffer strBufVal = new StringBuffer();
		String strNo = GmCommonClass.parseNull(String.valueOf(db.get("NO")));
		String strId = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		String strConsignID = GmCommonClass.parseNull(String.valueOf(db.get("CONSGID")));
		if((db.get("REQUIREDDT") != null))
		{
			java.sql.Timestamp dtShipDate= (java.sql.Timestamp)((db.get("REQUIREDDT")));
			SimpleDateFormat sdfDestination = new SimpleDateFormat(strApplDateFmt);// the date into another format30.      
			strShipDate = sdfDestination.format(dtShipDate);		
		}
		log.debug("The Convert the Date is "+strShipDate);
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strDisable = "disabled";

		if(!strStatus.equals("30") && !strStatus.equals("40"))
		{
			strDisable = "";
		}
		
		strBufVal.append("<input type=\"text\" maxlength=\"10\" "+strDisable+" id=\"shipDt" + strNo+"\" name=\"shipDt" + strNo+"\" value=\""+strShipDate+"\"");		
		strBufVal.append(" size='9' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strBufVal.append("/>&nbsp;<img id='Img_Date' "+strDisable+" style='cursor:hand'");	
		strBufVal.append("onclick=\"javascript:showSingleCalendar('divShipDt"+strNo+"','shipDt"+strNo+"','"+strApplJSDateFmt+"');\" title='Click to open Calendar'");
		strBufVal.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
		strBufVal.append("<div id=\"divShipDt"+strNo+"\" style=\"position: absolute; z-index: 10;\"></div>");
		strBufVal.append("<input type=\"hidden\" value=\""+strId+"\" name=\"shippedDate\" id=");
        strBufVal.append(strNo);
        strBufVal.append("> &nbsp; ");
        strBufVal.append("<input type=\"hidden\" value=\""+strStatus+"\" name=\"shipStatus\" id=");
        strBufVal.append(strNo);
        strBufVal.append(">");
        strBufVal.append("<input type=\"hidden\" value=\""+strShipDate+"\" name=\"oldShippedDate" + strNo+"\" >");
        strBufVal.append("<input type=\"hidden\" value=\""+strConsignID+"\" name=\"hIHLN" + strNo+"\" >");
        strBufVal.append("<input type=\"hidden\" value=\""+strStatus+"\" name=\"shipStatusId"+strNo+"\" id=");
        strBufVal.append(strNo);
        strBufVal.append("> &nbsp; ");
        
        strRes = strBufVal.toString(); 
        return strRes;
  }
		
	public String getLOGIMG()
	{
		db =  	(DynaBean) this.getCurrentRowObject();  
		String strSeq = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		String strFlag = GmCommonClass.parseNull(String.valueOf(db.get("LOG")));
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append(strSeq);
		strBufVal.append("&nbsp;&nbsp;<img style='cursor:hand' src='");
		if(strFlag.equals("N"))
        {
			strBufVal.append(strImagePath+"/phone_icon.jpg'");
        }
        else
        {            
            strBufVal.append(strImagePath+"/phone-icon_ans.gif'");
        }
   		strBufVal.append(" title='Click to add comments' width='17' height='12'" ); 
	   	strBufVal.append(" onClick =\"fnOpenLog('"+strSeq+"','1248');\" </img>");
	   	strRes = strBufVal.toString(); 
	   	return strRes;		
	}
	
	public String getEDITREQ()  
	{
		db =  	(DynaBean) this.getCurrentRowObject();  

		String strReqId = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		String strCnId = GmCommonClass.parseNull(String.valueOf(db.get("REQCNID")));
		String strConsignID = GmCommonClass.parseNull(String.valueOf(db.get("CONSGID")));
		strConsignID = (strConsignID.equals("null"))?"":strConsignID;
		String strId = (!strCnId.equals("N"))?strCnId:strReqId;
		StringBuffer strBufVal = new StringBuffer();
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strSource= GmCommonClass.parseNull(String.valueOf(db.get("SOURCE")));
		String strDisable = "disabled";
		totCount++;
		if(!strStatus.equals("30") && !strStatus.equals("40"))
		{
			strDisable = "";
		}
		if(!strStatus.equals("40")){
			count++;
		}
		int shipDetialSize = alShippingDetails.size();
		// Checking the total records count with Shipping Arraysize to set the Record Count Flag to set the Loaner Request Check Flag value to The Check Box 
		if(count >= 1 && totCount == shipDetialSize){
		strBufVal.append("<input type=\"hidden\" name=\"recordCount\" id=\"RecordFlag\" value=\"Y\" />");
		}else if(count < 1 && totCount == shipDetialSize){
		strBufVal.append("<input type=\"hidden\" name=\"recordCount\" id=\"RecordFlag\" value=\"N\" />");
		}
		// Checking the Status for Applying the Planned Ship date is same to all Set IDs in a Loaner Request based on the following Hidden Variable.
		if(strStatus.equals("40")){
			strBufVal.append("<input type=\"hidden\" name=\"SetStatus\" id=\"SetStatus\" value=\"Y\" />");
		}
		
		strBufVal.append("<img style='cursor:hand' "+strDisable+"  src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit Shipping Details' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =\"fnEdtId('"+strId+"','"+strSource+"');\" </img> "+strConsignID);	   
   		strRes = strBufVal.toString(); 
		
		//return  "<a href= javascript:fnViewInv('" + strPO + "','" + strId + "')>" + "Y" + "</a>" ; ,'" +strSessNm+ "','" + 2 + "','" + 2 + "','" + 2 + "','" + 2 + "' ----  "','"+strSessNm+"','"+strSessStartDt+"','"+strSessEndDt+"','"+strSessOwner+"','"+strSessStatus+
 		return strRes;
	}
	public String getETCHID(){
		StringBuffer strBufVal = new StringBuffer();
		String strNo = GmCommonClass.parseNull(String.valueOf(db.get("NO")));
		String strEtchId = GmCommonClass.parseNull((String)db.get("ETCHID"));
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strPartNum = GmCommonClass.parseNull(String.valueOf(db.get("PNUM")));
		strPartNum = (strPartNum.equals("null"))?"":strPartNum;
		strBufVal.append("<input type=\"hidden\" maxlength=\"20\" name=\"partnum" + strNo+"\" id=\"partnum" + strNo+"\" value=\""+strPartNum+"\"/>");		
		strBufVal.append("<input type=\"hidden\" maxlength=\"20\" name=\"statusId" + strNo+"\" id=\"statusId" + strNo+"\" value=\""+strStatus+"\"/>");
		
		
		
		if(strStatus.equals("10")){
			strBufVal.append("<input type=\"text\" maxlength=\"20\" name=\"etchId" + strNo+"\" value=\""+strEtchId+"\"");		
			strBufVal.append(" size='10' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\" style=\"visibility: hidden;\"");
			strBufVal.append(">  &nbsp; ");
		}else{
			strBufVal.append(strEtchId);
		}
		strRes = strBufVal.toString(); 
        return strRes;
	}
		
	public String getREQUIREDDATE()
	{
		GmDataStoreVO gmDataStoreVO =
		        (GmDataStoreVO) getPageContext().getRequest().getAttribute("gmDataStoreVO");
		    gmDataStoreVO =
		        ((gmDataStoreVO == null) || ((gmDataStoreVO.getCmpdfmt()).equals(""))) ? GmCommonClass
		            .getDefaultGmDataStoreVO() : gmDataStoreVO;
		String strShipDate = "";
		String  strApplDateFmt = gmDataStoreVO.getCmpdfmt();
		db =  	(DynaBean) this.getCurrentRowObject();  
		StringBuffer strBufVal = new StringBuffer();
		if((db.get("REQUIREDDT") != null))
		{
			java.sql.Timestamp dtShipDate = (java.sql.Timestamp)(db.get("REQUIREDDT"));
			SimpleDateFormat sdfDestination = new SimpleDateFormat(strApplDateFmt);// the date into another format30.      
			strShipDate =  sdfDestination.format(dtShipDate);	
		}
		strBufVal.append(strShipDate);	        
        strRes = strBufVal.toString(); 
        return strRes;
  }
	public String getSHIPDATE()
	{
		GmDataStoreVO gmDataStoreVO =
		        (GmDataStoreVO) getPageContext().getRequest().getAttribute("gmDataStoreVO");
		    gmDataStoreVO =
		        ((gmDataStoreVO == null) || ((gmDataStoreVO.getCmpdfmt()).equals(""))) ? GmCommonClass
		            .getDefaultGmDataStoreVO() : gmDataStoreVO;
		String strShipDate =  "";
		String  strApplDateFmt = gmDataStoreVO.getCmpdfmt();
		db =  	(DynaBean) this.getCurrentRowObject();  
		StringBuffer strBufVal = new StringBuffer();
		
		if((db.get("SHIPDATE") != null)){
		java.sql.Timestamp dtShipDate= (java.sql.Timestamp)(db.get("SHIPDATE"));
		SimpleDateFormat sdfDestination = new SimpleDateFormat(strApplDateFmt);// the date into another format30.      
		strShipDate = sdfDestination.format(dtShipDate);	
		}
		strBufVal.append(strShipDate);	        
        strRes = strBufVal.toString(); 
        return strRes;
  }	
	
	/* PC-137 To display flag icon in edit loaner screen
	 * author:shiny
	 */
	
	public String getESCLLOGFLAG() {
		db =  	(DynaBean) this.getCurrentRowObject();  
		String strFlag = GmCommonClass.parseNull(String.valueOf(db.get("ESCLLOGFLAG")));;
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strReqId = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("&nbsp;&nbsp;<img id='" + strReqId + "' style='cursor:hand' src='");
		String strDisable = "disabled";
		if(!strStatus.equals("10")) { //10-Open
			strBufVal.append("&nbsp;&nbsp;<img id='" + strReqId + "' "+strDisable+" style='cursor:hand' src='");	
			strBufVal.append(" title='Disabled' width='8' height='12'" ); 
		}
		if(strFlag.equals("Y") && strStatus.equals("10")){
			strBufVal.append("/img/escalation_flag_red.jpg'");	
			strBufVal.append("onclick=\"javascript:fnRemoveEscalation(\'" + strReqId + "\');\"");			
		}else{
			strBufVal.append("/img/escalation_flag_grey.jpg'"); 
			strBufVal.append("onclick=\"javascript:fnUpdateEscalation(\'" + strReqId + "\');\"");
		}
		strBufVal.append("> ");		
     	strRes = strBufVal.toString();
	   	return strRes;		
	}  
	
	
	/* PC-137 To display phone icon in edit loaner screen
	 * author:shiny
	 */
	public String getASCFL() 	{
		db =  	(DynaBean) this.getCurrentRowObject();  	
		String strFlag = GmCommonClass.parseNull((String)db.get("ASCFL"));
		String strStatus = GmCommonClass.parseNull(String.valueOf(db.get("STATUSID")));
		String strReqId = GmCommonClass.parseNull(String.valueOf(db.get("REQDTLID")));
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("&nbsp;&nbsp;<img style='cursor:hand' src='");
		String strDisable = "disabled";
		if(!strStatus.equals("10")) {
			strBufVal.append("&nbsp;&nbsp;<img id='" + strReqId + "' "+strDisable+" style='cursor:hand' src='");	
			strBufVal.append(" title='Disabled' width='8' height='12'" ); 
		}
		if(strFlag.equals("N") && strStatus.equals("10")) {//10-Open       
			strBufVal.append(strImagePath+"/phone_icon.jpg'");
			strBufVal.append(" title='Click to add comments' width='17' height='12'" ); 
	   		strBufVal.append(" onClick =\"fnReqOpenLog(''," + strReqId + ");\" > </img>");
        }
        else{            
        	strBufVal.append(strImagePath+"/phone-icon_ans.gif'");
        	strBufVal.append(" title='Click to add comments' width='17' height='12'" ); 
       		strBufVal.append(" onClick =\"fnReqOpenLog(" + strReqId + ");\" > </img>");
        } 	
	   	strRes = strBufVal.toString(); 
	   	return strRes;		
	}		
}
