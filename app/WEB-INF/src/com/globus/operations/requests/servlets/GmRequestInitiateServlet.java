package com.globus.operations.requests.servlets;

import com.globus.common.beans.*;
import com.globus.common.servlets.*;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.requests.beans.GmRequestTransBean;


import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

	public class GmRequestInitiateServlet extends GmServlet
	{

		public void init(ServletConfig config) throws ServletException
		{
			super.init(config);

		}

	  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
		{
			HttpSession session = request.getSession(false);
			String strComnPath = GmCommonClass.getString("GMCOMMON");
			String strRequestPath = GmCommonClass.getString("GMREQUESTS");
			String strDispatchTo = strRequestPath.concat("/GmRequestInitiate.jsp");
		
			HashMap hmReturn = new HashMap();
			HashMap hmParam = new HashMap();

			GmRequestTransBean gmReq = new GmRequestTransBean();
			try
			{
				checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				instantiate(request,response);
				
				if (strAction.equals("Load"))
				{
					hmReturn = gmReq.loadReqInitiateLists();
				}
				else if (strAction.equals("SaveRequest"))
				{
					
				}
				else if(strAction.equals("PrintRequest"))
				{

				}

				request.setAttribute("hAction",strAction);
				request.setAttribute("hmReturn",hmReturn);
				
				dispatch(strDispatchTo,request,response);
			}
			catch (Exception e)
			{
					session.setAttribute("hAppErrorMessage",e.getMessage());
					strDispatchTo = strComnPath + "/GmError.jsp";
					gotoPage(strDispatchTo,request,response);
			}// End of catch
		}//End of service method
	}// End of GmRequestInitiateServlet

