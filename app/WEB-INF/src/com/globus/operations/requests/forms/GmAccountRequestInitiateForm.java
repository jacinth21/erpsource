package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;




public class GmAccountRequestInitiateForm extends GmLogForm{
	
	private String 	consignmentType = "40025"; //consignment
	private String 	inHousePurpose = "";
	private String 	distributorId = "";
	private String 	repId = "";
	private String 	shipTo = "";
	private String  accountId = "";
	private String  employeeId = "";
	private String  departmentName = "";
	private String  requiredDate = "";
	private String  hinputString = "";
	private String  hfromPage = "";
	private String planShipDate = "";
	
	private String shipToID = "";
	private String sheetID = "";
	private String requestBy = "";
	private String requestByType = "";
	private String distList = ""; // for ICT Sample
	private String plannedDate = "";
	private String haddInfo = "";
	private String strRAFlag = "";
	
	private HashMap hmReturn = new HashMap();

	private String  hrefName= ""; 

	private String hinputstr = "";
	
	private ArrayList alAttrib=new ArrayList();
	
	private String loanerReqID = "";
	private String reqFor = "";//PMT-45058
	/**
	 * @return the reqFor
	 */
	public String getReqFor() {
		return reqFor;
	}
	/**
	 * @param reqFor the reqFor to set
	 */
	public void setReqFor(String reqFor) {
		this.reqFor = reqFor;
	}
	/**
	 * @return the strRAFlag
	 */
	public String getStrRAFlag() {
		return strRAFlag;
	}
	public void setStrRAFlag(String strRAFlag) {
		this.strRAFlag = strRAFlag;
	}
	
	public String getHinputstr() {
		return hinputstr;
	}
	public void setHinputstr(String hinputstr) {
		this.hinputstr = hinputstr;
	}
	/**
	 * @return the hrefName
	 */
	public String getHrefName() {
		return hrefName;
	}
	/**
	 * @param hrefName the hrefName to set
	 */
	public void setHrefName(String hrefName) {
		this.hrefName = hrefName;
	}
	 
	public String getPlannedDate() {
		return plannedDate;
	}
	public void setPlannedDate(String plannedDate) {
		this.plannedDate = plannedDate;
	}
	private ArrayList alSheetList = new ArrayList();
	private ArrayList alRequestBy = new ArrayList();
	private ArrayList alRequestByType = new ArrayList();
	
	private ArrayList alConsignmentType = new ArrayList();
	private ArrayList alInHousePurpose = new ArrayList();
	private ArrayList alDistributorList = new ArrayList();
	private ArrayList alRepList = new ArrayList();
	private ArrayList alShipTo = new ArrayList();
	private ArrayList alAccountList = new ArrayList();
	private ArrayList alEmployeeList = new ArrayList();
	
	private ArrayList alAttribute  = new ArrayList();
	
	public ArrayList getAlAttribute() {
		return alAttribute;
	}
	public void setAlAttribute(ArrayList alAttribute) {
		this.alAttribute = alAttribute;
	}

	private HashMap hmPurposes = new HashMap();
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the alAccountList
	 */
	public ArrayList getAlAccountList() {
		return alAccountList;
	}
	/**
	 * @param alAccountList the alAccountList to set
	 */
	public void setAlAccountList(ArrayList alAccountList) {
		this.alAccountList = alAccountList;
	}
	
	/**
	 * @return the alDistributorList
	 */
	public ArrayList getAlDistributorList() {
		return alDistributorList;
	}
	/**
	 * @param alDistributorList the alDistributorList to set
	 */
	public void setAlDistributorList(ArrayList alDistributorList) {
		this.alDistributorList = alDistributorList;
	}
	/**
	 * @return the alEmployeeList
	 */
	public ArrayList getAlEmployeeList() {
		return alEmployeeList;
	}
	/**
	 * @param alEmployeeList the alEmployeeList to set
	 */
	public void setAlEmployeeList(ArrayList alEmployeeList) {
		this.alEmployeeList = alEmployeeList;
	}
	/**
	 * @return the alInHousePurpose
	 */
	public ArrayList getAlInHousePurpose() {
		return alInHousePurpose;
	}
	/**
	 * @param alInHousePurpose the alInHousePurpose to set
	 */
	public void setAlInHousePurpose(ArrayList alInHousePurpose) {
		this.alInHousePurpose = alInHousePurpose;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the alShipTo
	 */
	public ArrayList getAlShipTo() {
		return alShipTo;
	}
	/**
	 * @param alShipTo the alShipTo to set
	 */
	public void setAlShipTo(ArrayList alShipTo) {
		this.alShipTo = alShipTo;
	}
	/**
	 * @return the consignmentType
	 */
	public String getConsignmentType() {
		return consignmentType;
	}
	/**
	 * @param consignmentType the consignmentType to set
	 */
	public void setConsignmentType(String consignmentType) {
		this.consignmentType = consignmentType;
	}
	/**
	 * @return the distributorId
	 */
	public String getDistributorId() {
		return distributorId;
	}
	/**
	 * @param distributorId the distributorId to set
	 */
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}
	/**
	 * @return the employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}
	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	/**
	 * @return the inHousePurpose
	 */
	public String getInHousePurpose() {
		return inHousePurpose;
	}
	/**
	 * @param inHousePurpose the inHousePurpose to set
	 */
	public void setInHousePurpose(String inHousePurpose) {
		this.inHousePurpose = inHousePurpose;
	}
	/**
	 * @return the repId
	 */
	public String getRepId() {
		return repId;
	}
	/**
	 * @param repId the repId to set
	 */
	public void setRepId(String repId) {
		this.repId = repId;
	}
	/**
	 * @return the shipTo
	 */
	public String getShipTo() {
		return shipTo;
	}
	/**
	 * @param shipTo the shipTo to set
	 */
	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}
	/**
	 * @return the alConsignmentType
	 */
	public ArrayList getAlConsignmentType() {
		return alConsignmentType;
	}
	/**
	 * @param alConsignmentType the alConsignmentType to set
	 */
	public void setAlConsignmentType(ArrayList alConsignmentType) {
		this.alConsignmentType = alConsignmentType;
	}
	/**
	 * @return the departmentName
	 */
	public String getDepartmentName() {
		return departmentName;
	}
	/**
	 * @param departmentName the departmentName to set
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	/**
	 * @return the requiredDate
	 */
	public String getRequiredDate() {
		return requiredDate;
	}
	/**
	 * @param requiredDate the requiredDate to set
	 */
	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}
	/**
	 * @return the hinputString
	 */
	public String getHinputString() {
		return hinputString;
	}
	/**
	 * @param hinputString the hinputString to set
	 */
	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}
	/**
	 * @return the hfromPage
	 */
	public String getHfromPage() {
		return hfromPage;
	}
	/**
	 * @param hfromPage the hfromPage to set
	 */
	public void setHfromPage(String hfromPage) {
		this.hfromPage = hfromPage;
	}
	public String getSheetID() {
		return sheetID;
	}
	public void setSheetID(String sheetID) {
		this.sheetID = sheetID;
	}
	public ArrayList getAlSheetList() {
		return alSheetList;
	}
	public void setAlSheetList(ArrayList alSheetList) {
		this.alSheetList = alSheetList;
	}
	public String getRequestBy() {
		return requestBy;
	}
	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}
	public String getRequestByType() {
		return requestByType;
	}
	public void setRequestByType(String requestByType) {
		this.requestByType = requestByType;
	}
	public ArrayList getAlRequestBy() {
		return alRequestBy;
	}
	public void setAlRequestBy(ArrayList alRequestBy) {
		this.alRequestBy = alRequestBy;
	}
	public ArrayList getAlRequestByType() {
		return alRequestByType;
	}
	public void setAlRequestByType(ArrayList alRequestByType) {
		this.alRequestByType = alRequestByType;
	}
	public String getShipToID() {
		return shipToID;
	}
	public void setShipToID(String shipToID) {
		this.shipToID = shipToID;
	}
	/**
	 * @return the hmPurposes
	 */
	public HashMap getHmPurposes() {
		return hmPurposes;
	}
	/**
	 * @param hmPurposes the hmPurposes to set
	 */
	public void setHmPurposes(HashMap hmPurposes) {
		this.hmPurposes = hmPurposes;
	}
	/**
	 * @return the distList
	 */
	public String getDistList() {
		return distList;
	}
	/**
	 * @param distList the distList to set
	 */
	public void setDistList(String distList) {
		this.distList = distList;
	}
	public String getPlanShipDate() {
		return planShipDate;
	}
	public void setPlanShipDate(String planShipDate) {
		this.planShipDate = planShipDate;
	}
	/**
	 * @return the haddInfo
	 */
	public String getHaddInfo() {
		return haddInfo;
	}
	/**
	 * @param haddInfo the haddInfo to set
	 */
	public void setHaddInfo(String haddInfo) {
		this.haddInfo = haddInfo;
	}
	/**
	 * @return the alAttrib
	 */
	public ArrayList getAlAttrib() {
		return alAttrib;
	}
	/**
	 * @param alAttrib the alAttrib to set
	 */
	public void setAlAttrib(ArrayList alAttrib) {
		this.alAttrib = alAttrib;
	}
	/**
	 * @return the hmReturn
	 */
	public HashMap getHmReturn() {
		return hmReturn;
	}
	/**
	 * @param hmReturn the hmReturn to set
	 */
	public void setHmReturn(HashMap hmReturn) {
		this.hmReturn = hmReturn;
	}
	/**
	 * @return the loanerReqID
	 */
	public String getLoanerReqID() {
		return loanerReqID;
	}
	/**
	 * @param loanerReqID the loanerReqID to set
	 */
	public void setLoanerReqID(String loanerReqID) {
		this.loanerReqID = loanerReqID;
	}
	 
	
	
}
