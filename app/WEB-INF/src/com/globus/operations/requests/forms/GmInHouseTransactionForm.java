/**
 * 
 */
package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

/**
 * @author tramasamy
 *
 */
public class GmInHouseTransactionForm extends GmCommonForm{
	
	  private String IHTransType = "";
	  private String empId ="";
	  private String empName ="";
	  private String gridXmlData ="";
	  private String  consignmentType ="";
	  private ArrayList alRequestFor = new ArrayList();
	  private ArrayList alInHousePurpose = new ArrayList(); 
	  private ArrayList alLogReasons = new ArrayList();
	  private String hinputString = "";
	  private String requestId = "";
	  private HashMap hmData = new HashMap(); 
	  private String inHousePurpose ="";
	  private String txt_LogReason ="";
	  
	/**
	 * @return the iHTransType
	 */
	public String getIHTransType() {
		return IHTransType;
	}
	

	/**
	 * @param iHTransType the iHTransType to set
	 */
	public void setIHTransType(String iHTransType) {
		IHTransType = iHTransType;
	}
	/**
	 * @return the empId
	 */
	public String getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(String empId) {
		this.empId = empId;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the alRequestFor
	 */
	public ArrayList getAlRequestFor() {
		return alRequestFor;
	}

	/**
	 * @param alRequestFor the alRequestFor to set
	 */
	public void setAlRequestFor(ArrayList alRequestFor) {
		this.alRequestFor = alRequestFor;
	}

	/**
	 * @return the alInHousePurpose
	 */
	public ArrayList getAlInHousePurpose() {
		return alInHousePurpose;
	}

	/**
	 * @param alInHousePurpose the alInHousePurpose to set
	 */
	public void setAlInHousePurpose(ArrayList alInHousePurpose) {
		this.alInHousePurpose = alInHousePurpose;
	}


	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}


	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}


	/**
	 * @return the alLogReasons
	 */
	public ArrayList getAlLogReasons() {
		return alLogReasons;
	}


	/**
	 * @param alLogReasons the alLogReasons to set
	 */
	public void setAlLogReasons(ArrayList alLogReasons) {
		this.alLogReasons = alLogReasons;
	}


	/**
	 * @return the hinputString
	 */
	public String getHinputString() {
		return hinputString;
	}


	/**
	 * @param hinputString the hinputString to set
	 */
	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}


	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}


	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}


	/**
	 * @return the hmData
	 */
	public HashMap getHmData() {
		return hmData;
	}


	/**
	 * @param hmData the hmData to set
	 */
	public void setHmData(HashMap hmData) {
		this.hmData = hmData;
	}


	/**
	 * @return the consignmentType
	 */
	public String getConsignmentType() {
		return consignmentType;
	}


	/**
	 * @param consignmentType the consignmentType to set
	 */
	public void setConsignmentType(String consignmentType) {
		this.consignmentType = consignmentType;
	}


	/**
	 * @return the inHousePurpose
	 */
	public String getInHousePurpose() {
		return inHousePurpose;
	}


	/**
	 * @param inHousePurpose the inHousePurpose to set
	 */
	public void setInHousePurpose(String inHousePurpose) {
		this.inHousePurpose = inHousePurpose;
	}


	public String getTxt_LogReason() {
		return txt_LogReason;
	}


	public void setTxt_LogReason(String txt_LogReason) {
		this.txt_LogReason = txt_LogReason;
	}




}
