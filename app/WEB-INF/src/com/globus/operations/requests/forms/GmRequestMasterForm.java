package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;

public class GmRequestMasterForm extends GmRequestForm{
	
	private String requestId = "";
	private String requestType = "";
 
    private String partNum = "";
	private String setOrPart = "50636"; //by set
	private String mainOrChild = "50638"; //main reqest
	private String masterReqID = "";  
	private String requestStatus = "50646";  
	private String requestStatusoper ="";
	private String requestStatusval ="";
		
	private String requestDateTo = "";
	private String requiredDateTo = "";
	
	private String hId = "";
	private String reqFor = "";
	private String reqTo = "";
	private String conStatusFlag = "";
	private String depId = "";
	
	private String hshipFromDate = "";
	private String hshipToDate = "";
	private String teamId  = "";//Team Name
	
	private String tagId = "";
	private String reprocessId = "";
	private String hRequestFor = "";
	
	/**
   * @return the hRequestFor
   */
  public String gethRequestFor() {
    return hRequestFor;
  }
  /**
   * @param hRequestFor the hRequestFor to set
   */
  public void sethRequestFor(String hRequestFor) {
    this.hRequestFor = hRequestFor;
  }
  public String getReprocessId() {
		return reprocessId;
	}
	public void setReprocessId(String reprocessId) {
		this.reprocessId = reprocessId;
	}
	/**
	 * @return the teamId
	 */
	public String getTeamId() {
		return teamId;
	}
	/**
	 * @param teamId the teamId to set
	 */
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}
	
	
	private ArrayList alRequestType = new ArrayList();
	private List alRequestReport = new ArrayList();
	private RowSetDynaClass rdRequestMaster = null ;
	
	private ArrayList alRequestSource = new ArrayList();
	private ArrayList alRequestStatus = new ArrayList();
	private List alRequestTo = new ArrayList();
 	private ArrayList alRequestTxnID = new ArrayList();	
	private ArrayList alSetPart = new ArrayList();
	private ArrayList alRequestStatus1 = new ArrayList();
	
	
	private ArrayList alMainOrChild = new ArrayList();
	private List rdBackOrderDetails = new ArrayList();
	private ArrayList alTeamList  = new ArrayList();//Team ArrylistName
	
	private ArrayList alLogReasons = new ArrayList();	
	private HashMap hmData = new HashMap();
		
	/**
	 * @return the alTeamList
	 */
	public ArrayList getAlTeamList() {
		return alTeamList;
	}
	/**
	 * @param alTeamList the alTeamList to set
	 */
	public void setAlTeamList(ArrayList alTeamList) {
		this.alTeamList = alTeamList;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/**
	 * @return the requestType
	 */
	public String getRequestType() {
		return requestType;
	}
	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the setOrPart
	 */
	public String getSetOrPart() {
		return setOrPart;
	}
	/**
	 * @param setOrPart the setOrPart to set
	 */
	public void setSetOrPart(String setOrPart) {
		this.setOrPart = setOrPart;
	}
	/**
	 * @return the mainOrChild
	 */
	public String getMainOrChild() {
		return mainOrChild;
	}
	/**
	 * @param mainOrChild the mainOrChild to set
	 */
	public void setMainOrChild(String mainOrChild) {
		this.mainOrChild = mainOrChild;
	}
	/**
	 * @return the masterReqID
	 */
	public String getMasterReqID() {
		return masterReqID;
	}
	/**
	 * @param masterReqID the masterReqID to set
	 */
	public void setMasterReqID(String masterReqID) {
		this.masterReqID = masterReqID;
	}
	/**
	 * @return the requestDateTo
	 */
	public String getRequestDateTo() {
		return requestDateTo;
	}
	/**
	 * @param requestDateTo the requestDateTo to set
	 */
	public void setRequestDateTo(String requestDateTo) {
		this.requestDateTo = requestDateTo;
	}
	/**
	 * @return the requiredDateTo
	 */
	public String getRequiredDateTo() {
		return requiredDateTo;
	}
	/**
	 * @param requiredDateTo the requiredDateTo to set
	 */
	public void setRequiredDateTo(String requiredDateTo) {
		this.requiredDateTo = requiredDateTo;
	}
	/**
	 * @return the alRequestType
	 */
	public ArrayList getAlRequestType() {
		return alRequestType;
	}
	/**
	 * @param alRequestType the alRequestType to set
	 */
	public void setAlRequestType(ArrayList alRequestType) {
		this.alRequestType = alRequestType;
	}
	/**
	 * @return the alRequestReport
	 */
	public List getAlRequestReport() {
		return alRequestReport;
	}
	/**
	 * @param alRequestReport the alRequestReport to set
	 */
	public void setAlRequestReport(List alRequestReport) {
		this.alRequestReport = alRequestReport;
	}
	/**
	 * @return the alRequestSource
	 */
	public ArrayList getAlRequestSource() {
		return alRequestSource;
	}
	/**
	 * @param alRequestSource the alRequestSource to set
	 */
	public void setAlRequestSource(ArrayList alRequestSource) {
		this.alRequestSource = alRequestSource;
	}
	/**
	 * @return the alRequestStatus
	 */
	public ArrayList getAlRequestStatus() {
		return alRequestStatus;
	}
	/**
	 * @param alRequestStatus the alRequestStatus to set
	 */
	public void setAlRequestStatus(ArrayList alRequestStatus) {
		this.alRequestStatus = alRequestStatus;
	}
	
	/**
	 * @return the alRequestTxnID
	 */
	public ArrayList getAlRequestTxnID() {
		return alRequestTxnID;
	}
	/**
	 * @param alRequestTxnID the alRequestTxnID to set
	 */
	public void setAlRequestTxnID(ArrayList alRequestTxnID) {
		this.alRequestTxnID = alRequestTxnID;
	}
	/**
	 * @return the alSetPart
	 */
	public ArrayList getAlSetPart() {
		return alSetPart;
	}
	/**
	 * @param alSetPart the alSetPart to set
	 */
	public void setAlSetPart(ArrayList alSetPart) {
		this.alSetPart = alSetPart;
	}
	/**
	 * @return the alMainOrChild
	 */
	public ArrayList getAlMainOrChild() {
		return alMainOrChild;
	}
	/**
	 * @param alMainOrChild the alMainOrChild to set
	 */
	public void setAlMainOrChild(ArrayList alMainOrChild) {
		this.alMainOrChild = alMainOrChild;
	}
	/**
	 * @return the rdBackOrderDetails
	 */
	public List getRdBackOrderDetails() {
		return rdBackOrderDetails;
	}
	/**
	 * @param rdBackOrderDetails the rdBackOrderDetails to set
	 */
	public void setRdBackOrderDetails(List rdBackOrderDetails) {
		this.rdBackOrderDetails = rdBackOrderDetails;
	}
	public String getHId() {
		return hId;
	}
	public void setHId(String id) {
		hId = id;
	}
	public String getReqFor() {
		return reqFor;
	}
	public void setReqFor(String reqFor) {
		this.reqFor = reqFor;
	}
	public String getReqTo() {
		return reqTo;
	}
	public void setReqTo(String reqTo) {
		this.reqTo = reqTo;
	}
	public String getConStatusFlag() {
		return conStatusFlag;
	}
	public void setConStatusFlag(String conStatusFlag) {
		this.conStatusFlag = conStatusFlag;
	}
	public String getHshipToDate() {
		return hshipToDate;
	}
	public void setHshipToDate(String hshipToDate) {
		this.hshipToDate = hshipToDate;
	}
	public String getHshipFromDate() {
		return hshipFromDate;
	}
	public void setHshipFromDate(String hshipFromDate) {
		this.hshipFromDate = hshipFromDate;
	}
	/**
	 * @return the alRequestStatus1
	 */
	public ArrayList getAlRequestStatus1() {
		return alRequestStatus1;
	}
	/**
	 * @param alRequestStatus1 the alRequestStatus1 to set
	 */
	public void setAlRequestStatus1(ArrayList alRequestStatus1) {
		this.alRequestStatus1 = alRequestStatus1;
	}
	/**
	 * @return the requestStatus
	 */
	public String getRequestStatus() {
		return requestStatus;
	}
	/**
	 * @param requestStatus the requestStatus to set
	 */
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	/**
	 * @return the requestStatusoper
	 */
	public String getRequestStatusoper() {
		return requestStatusoper;
	}
	/**
	 * @param requestStatusoper the requestStatusoper to set
	 */
	public void setRequestStatusoper(String requestStatusoper) {
		this.requestStatusoper = requestStatusoper;
	}
	/**
	 * @return the requestStatusval
	 */
	public String getRequestStatusval() {
		return requestStatusval;
	}
	/**
	 * @param requestStatusval the requestStatusval to set
	 */
	public void setRequestStatusval(String requestStatusval) {
		this.requestStatusval = requestStatusval;
	}
	/**
	 * @return the depId
	 */
	public String getDepId() {
		return depId;
	}
	/**
	 * @param depId the depId to set
	 */
	public void setDepId(String depId) {
		this.depId = depId;
	}
	/**
	 * @return the rdRequestMaster
	 */
	public RowSetDynaClass getRdRequestMaster() {
		return rdRequestMaster;
	}
	/**
	 * @param rdRequestMaster the rdRequestMaster to set
	 */
	public void setRdRequestMaster(RowSetDynaClass rdRequestMaster) {
		this.rdRequestMaster = rdRequestMaster;
	}
	/**
	 * @return the alRequestTo
	 */
	public List getAlRequestTo() {
		return alRequestTo;
	}
	/**
	 * @param alRequestTo the alRequestTo to set
	 */
	public void setAlRequestTo(List alRequestTo) {
		this.alRequestTo = alRequestTo;
	}
	/**
	 * @return the tagId
	 */
	public String getTagId() {
		return tagId;
	}
	/**
	 * @param tagId the tagId to set
	 */
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	/**
	 * @return the alLogReasons
	 */
	public ArrayList getAlLogReasons() {
		return alLogReasons;
	}
	/**
	 * @param alLogReasons the alLogReasons to set
	 */
	public void setAlLogReasons(ArrayList alLogReasons) {
		this.alLogReasons = alLogReasons;
	}
	/**
	 * @return the hmData
	 */
	public HashMap getHmData() {
		return hmData;
	}
	/**
	 * @param hmData the hmData to set
	 */
	public void setHmData(HashMap hmData) {
		this.hmData = hmData;
	}
			}