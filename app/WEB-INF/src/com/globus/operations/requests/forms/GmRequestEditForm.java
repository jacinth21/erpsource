package com.globus.operations.requests.forms;

import java.util.ArrayList;

 
public class GmRequestEditForm extends GmRequestForm {
	
	private String requestId = "";
	private String childrequestId = "";
	private String status = "";
	private String requestFor = "";
	
	private String reqFor = "";
	private String reqTo = "";
	
	private String custPo = ""; 
	private String hinpStr = "";
	
	private ArrayList alAsscDlist = new ArrayList();
	private ArrayList alRequestDetaillist = new ArrayList();
	
	private ArrayList alRequestTo = new ArrayList();
	private ArrayList alAttribute = new ArrayList();

	private String loanerReqID="";
	public ArrayList getAlAttribute() {
		return alAttribute;
	}
	public void setAlAttribute(ArrayList alAttribute) {
		this.alAttribute = alAttribute;
	}
	
	public String getCustPo() {
		return custPo;
	}
	public void setCustPo(String custPo) {
		this.custPo = custPo;
	}
	
	public String getHinpStr() {
		return hinpStr;
	}
	public void setHinpStr(String hinpStr) {
		this.hinpStr = hinpStr;
	}
	
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public ArrayList getAlAsscDlist() {
		return alAsscDlist;
	}
	public void setAlAsscDlist(ArrayList alAsscDlist) {
		this.alAsscDlist = alAsscDlist;
	}
	public ArrayList getAlRequestDetaillist() {
		return alRequestDetaillist;
	}
	public void setAlRequestDetaillist(ArrayList alRequestDetaillist) {
		this.alRequestDetaillist = alRequestDetaillist;
	}
	public String getChildrequestId() {
		return childrequestId;
	}
	public void setChildrequestId(String childrequestId) {
		this.childrequestId = childrequestId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList getAlRequestTo() {
		return alRequestTo;
	}
	public void setAlRequestTo(ArrayList alRequestTo) {
		this.alRequestTo = alRequestTo;
	}
	public String getRequestFor() {
		return requestFor;
	}
	public void setRequestFor(String requestFor) {
		this.requestFor = requestFor;
	}
	public String getReqFor() {
		return reqFor;
	}
	public void setReqFor(String reqFor) {
		this.reqFor = reqFor;
	}
	public String getReqTo() {
		return reqTo;
	}
	public void setReqTo(String reqTo) {
		this.reqTo = reqTo;
	}
	/**
	 * @return the loanerReqID
	 */
	public String getLoanerReqID() {
		return loanerReqID;
	}
	/**
	 * @param loanerReqID the loanerReqID to set
	 */
	public void setLoanerReqID(String loanerReqID) {
		this.loanerReqID = loanerReqID;
	}


}
