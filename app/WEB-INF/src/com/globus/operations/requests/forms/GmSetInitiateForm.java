package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmSetInitiateForm extends GmRequestInitiateForm {
	
	private String setId = "";
	private String setNm = "";
	private String searchsetId = "";
	private String distributorId = "";
	private String searchdistributorId ="";
	private String distId ="";
	private String searchdistId ="";
	
	private ArrayList alSetList = new ArrayList();
	private List rdSetListReport = new ArrayList();
	private HashMap hmReturn = new HashMap();
	
	/**
	 * @return the alSetList
	 */
	public ArrayList getAlSetList() {
		return alSetList;
	}
	/**
	 * @param alSetList the alSetList to set
	 */
	public void setAlSetList(ArrayList alSetList) {
		this.alSetList = alSetList;
	}
	/**
	 * @return the rdSetListReport
	 */
	public List getRdSetListReport() {
		return rdSetListReport;
	}
	/**
	 * @param rdSetListReport the rdSetListReport to set
	 */
	public void setRdSetListReport(List rdSetListReport) {
		this.rdSetListReport = rdSetListReport;
	}
	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}
	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}
	/**
	 * @return the hmReturn
	 */
	public HashMap getHmReturn() {
		return hmReturn;
	}
	/**
	 * @param hmReturn the hmReturn to set
	 */
	public void setHmReturn(HashMap hmReturn) {
		this.hmReturn = hmReturn;
	}
	public String getSetNm() {
		return setNm;
	}
	public void setSetNm(String setNm) {
		this.setNm = setNm;
	}
	public String getSearchsetId() {
		return searchsetId;
	}
	public void setSearchsetId(String searchsetId) {
		this.searchsetId = searchsetId;
	}
	public String getDistributorId() {
		return distributorId;
	}
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}
	public String getSearchdistributorId() {
		return searchdistributorId;
	}
	public void setSearchdistributorId(String searchdistributorId) {
		this.searchdistributorId = searchdistributorId;
	}
	public String getDistId() {
		return distId;
	}
	public void setDistId(String distId) {
		this.distId = distId;
	}
	public String getSearchdistId() {
		return searchdistId;
	}
	public void setSearchdistId(String searchdistId) {
		this.searchdistId = searchdistId;
	}
	
	
}
