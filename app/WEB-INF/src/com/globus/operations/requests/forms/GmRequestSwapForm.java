package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmRequestSwapForm extends GmCancelForm{
	
	private List returnList = new ArrayList();
	private List childList = new ArrayList();

	private String requestFrom = "GM-RQ-";
	private String requestTo = "GM-RQ-";
	private String swapflag = "";
	
	public String getSwapflag() {
		return swapflag;
	}
	public void setSwapflag(String swapflag) {
		this.swapflag = swapflag;
	}
	public List getReturnList() {
		return returnList;
	}
	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}
	public List getChildList() {
		return childList;
	}
	public void setChildList(List childList) {
		this.childList = childList;
	}
	public String getRequestFrom() {
		return requestFrom;
	}
	public void setRequestFrom(String requestFrom) {
		this.requestFrom = requestFrom;
	}
	public String getRequestTo() {
		return requestTo;
	}
	public void setRequestTo(String requestTo) {
		this.requestTo = requestTo;
	}

}
