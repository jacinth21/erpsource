package com.globus.operations.requests.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmRequestForm extends GmCommonForm {

  private String requestId = "";
  private String requestFor = "";
  private String reqFor = "";
  private String requestType = "";
  private String requestReason = "";
  private String requestPriority = "";
  private String requestLocation = "";
  private String requestShipTo = "";
  private String requestShipToID = "";
  private String planShipDate = "";

  private String shipTo = "";
  private String shipToID = "";

  private String requestDate = "";
  private String requiredDate = "";
  private String requestSource = "";
  private String requestTxnID = "";
  private String requestTxnName = "";

  private String setID = "";
  private String setName = "";
  private String requestTo = "";
  private String requestStatus = "";


  private String cnsetID = "";
  private String cnsetName = "";

  private String consignID = "";
  private String iniBy = "";
  private String iniDate = "";
  private String lastUpdNm = "";
  private String lastUpdDt = "";
  private String consignFlag = "";
  private String statusflag = "";
  private String requestStatusFlag = "";
  private String historyfl = "";
  private String strRequestPurpose = "";

  private ArrayList alRequestFor = new ArrayList();
  private ArrayList alRequestType = new ArrayList();
  private ArrayList alRequestReason = new ArrayList();
  private ArrayList alRequestPriority = new ArrayList();
  private ArrayList alRequestLocation = new ArrayList();
  private ArrayList alRequestShipTo = new ArrayList();

  private ArrayList alRequestShipToID = new ArrayList();

  private ArrayList alShipDistributor = new ArrayList();
  private ArrayList alShipSalesRep = new ArrayList();
  private ArrayList alShipHospital = new ArrayList();

  /**
   * @return the strRequestPurpose
   */
  public String getStrRequestPurpose() {
    return strRequestPurpose;
  }

  /**
   * @param strRequestPurpose the strRequestPurpose to set
   */
  public void setStrRequestPurpose(String strRequestPurpose) {
    this.strRequestPurpose = strRequestPurpose;
  }

  private ArrayList alShipEmployee = new ArrayList();

  /**
   * @return the requestId
   */
  public String getRequestId() {
    return requestId;
  }

  /**
   * @param requestId the requestId to set
   */
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  /**
   * @return the requestFor
   */
  public String getRequestFor() {
    return requestFor;
  }

  /**
   * @param requestFor the requestFor to set
   */
  public void setRequestFor(String requestFor) {
    this.requestFor = requestFor;
  }

  /**
   * @return the requestType
   */
  public String getRequestType() {
    return requestType;
  }

  /**
   * @param requestType the requestType to set
   */
  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }

  /**
   * @return the requestReason
   */
  public String getRequestReason() {
    return requestReason;
  }

  /**
   * @param requestReason the requestReason to set
   */
  public void setRequestReason(String requestReason) {
    this.requestReason = requestReason;
  }

  /**
   * @return the requestPriority
   */
  public String getRequestPriority() {
    return requestPriority;
  }

  /**
   * @param requestPriority the requestPriority to set
   */
  public void setRequestPriority(String requestPriority) {
    this.requestPriority = requestPriority;
  }

  /**
   * @return the requestLocation
   */
  public String getRequestLocation() {
    return requestLocation;
  }

  /**
   * @param requestLocation the requestLocation to set
   */
  public void setRequestLocation(String requestLocation) {
    this.requestLocation = requestLocation;
  }

  /**
   * @return the requestShipTo
   */
  public String getRequestShipTo() {
    return requestShipTo;
  }

  /**
   * @param requestShipTo the requestShipTo to set
   */
  public void setRequestShipTo(String requestShipTo) {
    this.requestShipTo = requestShipTo;
  }

  /**
   * @return the alRequestFor
   */
  public ArrayList getAlRequestFor() {
    return alRequestFor;
  }

  /**
   * @param alRequestFor the alRequestFor to set
   */
  public void setAlRequestFor(ArrayList alRequestFor) {
    this.alRequestFor = alRequestFor;
  }

  /**
   * @return the alRequestType
   */
  public ArrayList getAlRequestType() {
    return alRequestType;
  }

  /**
   * @param alRequestType the alRequestType to set
   */
  public void setAlRequestType(ArrayList alRequestType) {
    this.alRequestType = alRequestType;
  }

  /**
   * @return the alRequestReason
   */
  public ArrayList getAlRequestReason() {
    return alRequestReason;
  }

  /**
   * @param alRequestReason the alRequestReason to set
   */
  public void setAlRequestReason(ArrayList alRequestReason) {
    this.alRequestReason = alRequestReason;
  }

  /**
   * @return the alRequestPriority
   */
  public ArrayList getAlRequestPriority() {
    return alRequestPriority;
  }

  /**
   * @param alRequestPriority the alRequestPriority to set
   */
  public void setAlRequestPriority(ArrayList alRequestPriority) {
    this.alRequestPriority = alRequestPriority;
  }

  /**
   * @return the alRequestLocation
   */
  public ArrayList getAlRequestLocation() {
    return alRequestLocation;
  }

  /**
   * @param alRequestLocation the alRequestLocation to set
   */
  public void setAlRequestLocation(ArrayList alRequestLocation) {
    this.alRequestLocation = alRequestLocation;
  }

  /**
   * @return the alRequestShipTo
   */
  public ArrayList getAlRequestShipTo() {
    return alRequestShipTo;
  }

  /**
   * @param alRequestShipTo the alRequestShipTo to set
   */
  public void setAlRequestShipTo(ArrayList alRequestShipTo) {
    this.alRequestShipTo = alRequestShipTo;
  }

  public String getRequestDate() {
    return requestDate;
  }

  public void setRequestDate(String requestDate) {
    this.requestDate = requestDate;
  }

  public String getRequiredDate() {
    return requiredDate;
  }

  public void setRequiredDate(String requiredDate) {
    this.requiredDate = requiredDate;
  }

  public String getRequestSource() {
    return requestSource;
  }

  public void setRequestSource(String requestSource) {
    this.requestSource = requestSource;
  }

  public String getRequestTxnID() {
    return requestTxnID;
  }

  public void setRequestTxnID(String requestTxnID) {
    this.requestTxnID = requestTxnID;
  }

  public String getSetID() {
    return setID;
  }

  public void setSetID(String setID) {
    this.setID = setID;
  }

  public String getSetName() {
    return setName;
  }

  public void setSetName(String setName) {
    this.setName = setName;
  }

  public String getRequestTo() {
    return requestTo;
  }

  public void setRequestTo(String requestTo) {
    this.requestTo = requestTo;
  }

  public String getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(String requestStatus) {
    this.requestStatus = requestStatus;
  }

  public String getIniDate() {
    return iniDate;
  }

  public String getLastUpdNm() {
    return lastUpdNm;
  }

  public String getLastUpdDt() {
    return lastUpdDt;
  }

  public String getConsignFlag() {
    return consignFlag;
  }

  public void setIniDate(String iniDate) {
    this.iniDate = iniDate;
  }

  public void setLastUpdNm(String lastUpdNm) {
    this.lastUpdNm = lastUpdNm;
  }

  public void setLastUpdDt(String lastUpdDt) {
    this.lastUpdDt = lastUpdDt;
  }

  public void setConsignFlag(String consignFlag) {
    this.consignFlag = consignFlag;
  }

  public String getIniBy() {
    return iniBy;
  }

  public void setIniBy(String iniBy) {
    this.iniBy = iniBy;
  }

  public String getConsignID() {
    return consignID;
  }

  public void setConsignID(String consignID) {
    this.consignID = consignID;
  }

  /**
   * @return the statusflag
   */
  public String getStatusflag() {
    return statusflag;
  }

  /**
   * @param statusflag the statusflag to set
   */
  public void setStatusflag(String statusflag) {
    this.statusflag = statusflag;
  }

  /**
   * @return the requestStatusFlag
   */
  public String getRequestStatusFlag() {
    return requestStatusFlag;
  }

  /**
   * @param requestStatusFlag the requestStatusFlag to set
   */
  public void setRequestStatusFlag(String requestStatusFlag) {
    this.requestStatusFlag = requestStatusFlag;
  }

  public String getCnsetID() {
    return cnsetID;
  }

  public void setCnsetID(String cnsetID) {
    this.cnsetID = cnsetID;
  }

  public String getCnsetName() {
    return cnsetName;
  }

  public void setCnsetName(String cnsetName) {
    this.cnsetName = cnsetName;
  }

  public String getRequestShipToID() {
    return requestShipToID;
  }

  public void setRequestShipToID(String requestShipToID) {
    this.requestShipToID = requestShipToID;
  }

  public ArrayList getAlRequestShipToID() {
    return alRequestShipToID;
  }

  public void setAlRequestShipToID(ArrayList alRequestShipToID) {
    this.alRequestShipToID = alRequestShipToID;
  }

  public ArrayList getAlShipDistributor() {
    return alShipDistributor;
  }

  public void setAlShipDistributor(ArrayList alShipDistributor) {
    this.alShipDistributor = alShipDistributor;
  }

  public ArrayList getAlShipSalesRep() {
    return alShipSalesRep;
  }

  public void setAlShipSalesRep(ArrayList alShipSalesRep) {
    this.alShipSalesRep = alShipSalesRep;
  }

  public ArrayList getAlShipHospital() {
    return alShipHospital;
  }

  public void setAlShipHospital(ArrayList alShipHospital) {
    this.alShipHospital = alShipHospital;
  }

  public ArrayList getAlShipEmployee() {
    return alShipEmployee;
  }

  public void setAlShipEmployee(ArrayList alShipEmployee) {
    this.alShipEmployee = alShipEmployee;
  }

  public String getShipTo() {
    return shipTo;
  }

  public void setShipTo(String shipTo) {
    this.shipTo = shipTo;
  }

  public String getShipToID() {
    return shipToID;
  }

  public void setShipToID(String shipToID) {
    this.shipToID = shipToID;
  }

  public String getHistoryfl() {
    return historyfl;
  }

  public void setHistoryfl(String historyfl) {
    this.historyfl = historyfl;
  }

  public String getPlanShipDate() {
    return planShipDate;
  }

  public void setPlanShipDate(String planShipDate) {
    this.planShipDate = planShipDate;
  }

  /**
   * @return the requestTxnName
   */
  public String getRequestTxnName() {
    return requestTxnName;
  }

  /**
   * @param requestTxnName the requestTxnName to set
   */
  public void setRequestTxnName(String requestTxnName) {
    this.requestTxnName = requestTxnName;
  }
  /**
   * @return the reqFor
   */
  public String getReqFor() {
    return reqFor;
  }

  /**
   * @param reqFor the reqFor to set
   */
  public void setReqFor(String reqFor) {
    this.reqFor = reqFor;
  }


}
