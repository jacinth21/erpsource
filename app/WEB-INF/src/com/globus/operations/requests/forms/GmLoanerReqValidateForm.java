package com.globus.operations.requests.forms;

import com.globus.common.forms.GmCommonForm;

public class GmLoanerReqValidateForm extends GmCommonForm{
	
	private String requestId = "";

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	
}