package com.globus.operations.requests.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmMaterialReconfigureForm extends GmRequestForm{
	
	ArrayList alDetails = new ArrayList() ;
	ArrayList alAction ;
	
	int hcnt = 0;
	
	
	
	String hinputstring = "" ;
	
	
	String boreqId = "" ;
	String bostatus = "";
	String bocreateddate = "";
	String bocreatedby = "" ;
	
	
	public ArrayList getAlDetails() {
		return alDetails;
	}

	public void setAlDetails(ArrayList alDetails) {
		this.alDetails = alDetails;
	}

	public ArrayList getAlAction() {
		return alAction;
	}

	public void setAlAction(ArrayList alAction) {
		this.alAction = alAction;
	}

	public int getHcnt() {
		return hcnt;
	}

	public void setHcnt(int hcnt) {
		this.hcnt = hcnt;
	}

	public String getHinputstring() {
		return hinputstring;
	}

	public void setHinputstring(String hinputstring) {
		this.hinputstring = hinputstring;
	}

	public String getBoreqId() {
		return boreqId;
	}

	public void setBoreqId(String boreqId) {
		this.boreqId = boreqId;
	}

	public String getBostatus() {
		return bostatus;
	}

	public void setBostatus(String bostatus) {
		this.bostatus = bostatus;
	}

	public String getBocreateddate() {
		return bocreateddate;
	}

	public void setBocreateddate(String bocreateddate) {
		this.bocreateddate = bocreateddate;
	}

	public String getBocreatedby() {
		return bocreatedby;
	}

	public void setBocreatedby(String bocreatedby) {
		this.bocreatedby = bocreatedby;
	}

	
	

}
