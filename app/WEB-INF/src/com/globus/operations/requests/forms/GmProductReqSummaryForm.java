package com.globus.operations.requests.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmProductReqSummaryForm extends GmCommonForm{
	
	private String dist="";
	private String rep="";
	private String status="0";
	private String fromDt="";
	private String toDt="";
	private String gridData="";
	private ArrayList alDist = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alRepList = new ArrayList();
	/**
	 * @return the dist
	 */
	public String getDist() {
		return dist;
	}
	/**
	 * @param dist the dist to set
	 */
	public void setDist(String dist) {
		this.dist = dist;
	}
	/**
	 * @return the rep
	 */
	public String getRep() {
		return rep;
	}
	/**
	 * @param rep the rep to set
	 */
	public void setRep(String rep) {
		this.rep = rep;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the fromDt
	 */
	public String getFromDt() {
		return fromDt;
	}
	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}
	/**
	 * @return the toDt
	 */
	public String getToDt() {
		return toDt;
	}
	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(String toDt) {
		this.toDt = toDt;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the alDist
	 */
	public ArrayList getAlDist() {
		return alDist;
	}
	/**
	 * @param alDist the alDist to set
	 */
	public void setAlDist(ArrayList alDist) {
		this.alDist = alDist;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}

}
