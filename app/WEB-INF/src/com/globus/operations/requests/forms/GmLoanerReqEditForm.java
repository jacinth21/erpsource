/**
 * FileName    : GmLoanerReqEditForm.java 
 * Description :
 * Author      : rshah
 * Date        : Mar 23, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

/**
 * @author rshah
 *
 */
public class GmLoanerReqEditForm extends GmLogForm{
	private String requestId = "";
	private String requestedDt = "";
	private String requestForId = "";
	private String requestForNm = "";
	private String salesRepId = "";
	private String salesRepNm = "";
	private String accountId = "";
	private String accountNm = "";
	private String surgeryDt = "";
	private String hinputString = "";
	//private String shipId = "";
	private String addressid = "";
	private String shipCarrier = "";
	private String shipTo="";
	private String names="";
	private String shipMode="";
	private String orderBy = "";
	private String dataFlag = "";
	private String voidDataFlag = "";	
	private String txnType = "";
	private String requestTxnType = "";
	
	private String eventName = "";
	private String eventStartDate = "";
	private String eventEndDate = "";
	private String loanToType = "";
	private String loanToName = "";
	private String etchidInputStr = "";
	private String voidInputStr = "";
	private String assocrepnm = "";
	
	private String strMode = "";
	private String strEsclAccessFl = "";
	private String strEsclRemAccessFl = "";
		
	private ArrayList alAttrib=new ArrayList();
	
	public String getVoidInputStr() {
		return voidInputStr;
	}
	public void setVoidInputStr(String voidInputStr) {
		this.voidInputStr = voidInputStr;
	}
	public String getEtchidInputStr() {
		return etchidInputStr;
	}
	public void setEtchidInputStr(String etchidInputStr) {
		this.etchidInputStr = etchidInputStr;
	}
	/**
	 * @return the voidDataFlag
	 */
	public String getVoidDataFlag() {
		return voidDataFlag;
	}
	/**
	 * @param voidDataFlag the voidDataFlag to set
	 */
	public void setVoidDataFlag(String voidDataFlag) {
		this.voidDataFlag = voidDataFlag;
	}
	/**
	 * @return the dataFlag
	 */
	public String getDataFlag() {
		return dataFlag;
	}
	/**
	 * @param dataFlag the dataFlag to set
	 */
	public void setDataFlag(String dataFlag) {
		this.dataFlag = dataFlag;
	}
	private ArrayList alReqList = new ArrayList();
	private ArrayList alVoidReasons = new ArrayList();
	private ArrayList alShippingDetails = new ArrayList();
	private ArrayList alVoidRequestDetails = new ArrayList();
	private HashMap hmReportHeaderDetails = new HashMap();
	private HashMap hmRequest  = new HashMap();
	
	private ArrayList alReqSlipList = new ArrayList();
	
	
	
	/**
	 * @return the alReqSlipList
	 */
	public ArrayList getAlReqSlipList() {
		return alReqSlipList;
	}
	/**
	 * @param alReqSlipList the alReqSlipList to set
	 */
	public void setAlReqSlipList(ArrayList alReqSlipList) {
		this.alReqSlipList = alReqSlipList;
	}
	/**
	 * @return the alVoidRequestDetails
	 */
	public ArrayList getAlVoidRequestDetails() {
		return alVoidRequestDetails;
	}
	/**
	 * @param alVoidRequestDetails the alVoidRequestDetails to set
	 */
	public void setAlVoidRequestDetails(ArrayList alVoidRequestDetails) {
		this.alVoidRequestDetails = alVoidRequestDetails;
	}
	/**
	 * @return the hmReportHeaderDetails
	 */
	public HashMap getHmReportHeaderDetails() {
		return hmReportHeaderDetails;
	}
	/**
	 * @param hmReportHeaderDetails the hmReportHeaderDetails to set
	 */
	public void setHmReportHeaderDetails(HashMap hmReportHeaderDetails) {
		this.hmReportHeaderDetails = hmReportHeaderDetails;
	}
	/**
	 * @return the alShippingDetails
	 */
	public ArrayList getAlShippingDetails() {
		return alShippingDetails;
	}
	/**
	 * @param alShippingDetails the alShippingDetails to set
	 */
	public void setAlShippingDetails(ArrayList alShippingDetails) {
		this.alShippingDetails = alShippingDetails;
	}
	/**
	 * @return the hmRequest
	 */
	public HashMap getHmRequest() {
		return hmRequest;
	}
	/**
	 * @param hmRequest the hmRequest to set
	 */
	public void setHmRequest(HashMap hmRequest) {
		this.hmRequest = hmRequest;
	}
	
	
	/**
	 * @return the orderBy
	 */
	public String getOrderBy() {
		return orderBy;
	}
	/**
	 * @param orderBy the orderBy to set
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/**
	 * @return the requestedDt
	 */
	public String getRequestedDt() {
		return requestedDt;
	}
	/**
	 * @param requestedDt the requestedDt to set
	 */
	public void setRequestedDt(String requestedDt) {
		this.requestedDt = requestedDt;
	}
	/**
	 * @return the requestForId
	 */
	public String getRequestForId() {
		return requestForId;
	}
	/**
	 * @param requestForId the requestForId to set
	 */
	public void setRequestForId(String requestForId) {
		this.requestForId = requestForId;
	}
	/**
	 * @return the salesRepId
	 */
	public String getSalesRepId() {
		return salesRepId;
	}
	/**
	 * @param salesRepId the salesRepId to set
	 */
	public void setSalesRepId(String salesRepId) {
		this.salesRepId = salesRepId;
	}
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @return the surgeryDt
	 */
	public String getSurgeryDt() {
		return surgeryDt;
	}
	/**
	 * @param surgeryDt the surgeryDt to set
	 */
	public void setSurgeryDt(String surgeryDt) {
		this.surgeryDt = surgeryDt;
	}
	/**
	 * @return the hinputString
	 */
	public String getHinputString() {
		return hinputString;
	}
	/**
	 * @param hinputString the hinputString to set
	 */
	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}
	/**
	 * @return the alReqList
	 */
	public ArrayList getAlReqList() {
		return alReqList;
	}
	/**
	 * @param alReqList the alReqList to set
	 */
	public void setAlReqList(ArrayList alReqList) {
		this.alReqList = alReqList;
	}
	/**
	 * @return the requestForNm
	 */
	public String getRequestForNm() {
		return requestForNm;
	}
	/**
	 * @param requestForNm the requestForNm to set
	 */
	public void setRequestForNm(String requestForNm) {
		this.requestForNm = requestForNm;
	}
	/**
	 * @return the salesRepNm
	 */
	public String getSalesRepNm() {
		return salesRepNm;
	}
	/**
	 * @param salesRepNm the salesRepNm to set
	 */
	public void setSalesRepNm(String salesRepNm) {
		this.salesRepNm = salesRepNm;
	}
	/**
	 * @return the accountNm
	 */
	public String getAccountNm() {
		return accountNm;
	}
	/**
	 * @param accountNm the accountNm to set
	 */
	public void setAccountNm(String accountNm) {
		this.accountNm = accountNm;
	}
	/**
	 * @return the alVoidReasons
	 */
	public ArrayList getAlVoidReasons() {
		return alVoidReasons;
	}
	/**
	 * @param alVoidReasons the alVoidReasons to set
	 */
	public void setAlVoidReasons(ArrayList alVoidReasons) {
		this.alVoidReasons = alVoidReasons;
	}
	
	public String getAddressid() {
		return addressid;
	}
	public void setAddressid(String addressId) {
		this.addressid = addressId;
	}
	public String getShipCarrier() {
		return shipCarrier;
	}
	public void setShipCarrier(String shipCarrier) {
		this.shipCarrier = shipCarrier;
	}
	public String getShipTo() {
		return shipTo;
	}
	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}
	public String getNames() {
		return names;
	}
	public void setNames(String names) {
		this.names = names;
	}
	public String getShipMode() {
		return shipMode;
	}
	public void setShipMode(String shipMode) {
		this.shipMode = shipMode;
	}
	/**
	 * @return the txnType
	 */
	public String getTxnType() {
		return txnType;
	}
	/**
	 * @param txnType the txnType to set
	 */
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	/**
	 * @return the eventName
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * @param eventName the eventName to set
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * @return the eventStartDate
	 */
	public String getEventStartDate() {
		return eventStartDate;
	}
	/**
	 * @param eventStartDate the eventStartDate to set
	 */
	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}
	/**
	 * @return the eventEndDate
	 */
	public String getEventEndDate() {
		return eventEndDate;
	}
	/**
	 * @param eventEndDate the eventEndDate to set
	 */
	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}
	/**
	 * @return the loanToType
	 */
	public String getLoanToType() {
		return loanToType;
	}
	/**
	 * @param loanToType the loanToType to set
	 */
	public void setLoanToType(String loanToType) {
		this.loanToType = loanToType;
	}
	/**
	 * @return the loanToName
	 */
	public String getLoanToName() {
		return loanToName;
	}
	/**
	 * @param loanToName the loanToName to set
	 */
	public void setLoanToName(String loanToName) {
		this.loanToName = loanToName;
	}
	/**
	 * @return the requestTxnType
	 */
	public String getRequestTxnType() {
		return requestTxnType;
	}
	/**
	 * @param requestTxnType the requestTxnType to set
	 */
	public void setRequestTxnType(String requestTxnType) {
		this.requestTxnType = requestTxnType;
	}
	/**
	 * @return the strMode
	 */
	public String getStrMode() {
		return strMode;
	}
	/**
	 * @param strMode the strMode to set
	 */
	public void setStrMode(String strMode) {
		this.strMode = strMode;
	}
	/**
	 * @return the alAttrib
	 */
	public ArrayList getAlAttrib() {
		return alAttrib;
	}
	/**
	 * @param alAttrib the alAttrib to set
	 */
	public void setAlAttrib(ArrayList alAttrib) {
		this.alAttrib = alAttrib;
	}
	
	public String getAssocrepnm() {
		return assocrepnm;
	}
	public void setAssocrepnm(String assocrepnm) {
		this.assocrepnm = assocrepnm;
	}
	
	/**
	 * @return the strEsclAccessFl
	 */
	public String getStrEsclAccessFl() {
		return strEsclAccessFl;
	}
	/**
	 * @param strEsclAccessFl the strEsclAccessFl to set
	 */
	public void setStrEsclAccessFl(String strEsclAccessFl) {
		this.strEsclAccessFl = strEsclAccessFl;
	}
	/**
	 * @return the strEsclRemAccessFl
	 */
	public String getStrEsclRemAccessFl() {
		return strEsclRemAccessFl;
	}
	/**
	 * @param strEsclRemAccessFl the strEsclRemAccessFl to set
	 */
	public void setStrEsclRemAccessFl(String strEsclRemAccessFl) {
		this.strEsclRemAccessFl = strEsclRemAccessFl;
	}
		
}
