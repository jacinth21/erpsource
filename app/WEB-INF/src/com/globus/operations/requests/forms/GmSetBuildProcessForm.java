package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.List;

public class GmSetBuildProcessForm extends GmRequestForm{
	private String chkComplete = "";
	private String reloadConsignmentId = "";
	private String rollBackAccessFl="";
	private String cbo_Type = "";
	/**
	 * @return the rollBackAccessFl
	 */
	public String getRollBackAccessFl() {
		return rollBackAccessFl;
	}
	/**
	 * @param rollBackAccessFl the rollBackAccessFl to set
	 */
	public void setRollBackAccessFl(String rollBackAccessFl) {
		this.rollBackAccessFl = rollBackAccessFl;
	}	
	/**
	 * @return the cbo_Type
	 */
	public String getCbo_Type() {
		return cbo_Type;
	}
	/**
	 * @param cboType the cbo_Type to set
	 */
	public void setCbo_Type(String cboType) {
		cbo_Type = cboType;
	}

	private String hcounter = "";
	private String hinputstring = "";
	private String completeFlag = "";
	private String verifyFlag = "";
	private String daysDiff = "0";
	private String daysDiffRule = "0";
	private String displayFlag = "";
	ArrayList alReloadConsignment = new ArrayList();
	ArrayList alConsignmentDetail = new ArrayList();
	ArrayList alTypeList = new ArrayList();
	List rdBackOrderDetails = new ArrayList(); 
	
	
	/**
	 * @return the alReloadConsignment
	 */
	public ArrayList getAlReloadConsignment() {
		return alReloadConsignment;
	}
	/**
	 * @param alReloadConsignment the alReloadConsignment to set
	 */
	public void setAlReloadConsignment(ArrayList alReloadConsignment) {
		this.alReloadConsignment = alReloadConsignment;
	}
	/**
	 * @return the chkComplete
	 */
	public String getChkComplete() {
		return chkComplete;
	}
	/**
	 * @param chkComplete the chkComplete to set
	 */
	public void setChkComplete(String chkComplete) {
		this.chkComplete = chkComplete;
	}
	/**
	 * @return the alConsignmentDetail
	 */
	public ArrayList getAlConsignmentDetail() {
		return alConsignmentDetail;
	}
	/**
	 * @param alConsignmentDetail the alConsignmentDetail to set
	 */
	public void setAlConsignmentDetail(ArrayList alConsignmentDetail) {
		this.alConsignmentDetail = alConsignmentDetail;
	}
	/**
	 * @return the reloadConsignmentId
	 */
	public String getReloadConsignmentId() {
		return reloadConsignmentId;
	}
	/**
	 * @param reloadConsignmentId the reloadConsignmentId to set
	 */
	public void setReloadConsignmentId(String reloadConsignmentId) {
		this.reloadConsignmentId = reloadConsignmentId;
	}
	/**
	 * @return the hcounter
	 */
	public String getHcounter() {
		return hcounter;
	}
	/**
	 * @param hcounter the hcounter to set
	 */
	public void setHcounter(String hcounter) {
		this.hcounter = hcounter;
	}
	/**
	 * @return the hinputstring
	 */
	public String getHinputstring() {
		return hinputstring;
	}
	/**
	 * @param hinputstring the hinputstring to set
	 */
	public void setHinputstring(String hinputstring) {
		this.hinputstring = hinputstring;
	}
	/**
	 * @return the completeFlag
	 */
	public String getCompleteFlag() {
		return completeFlag;
	}
	/**
	 * @param completeFlag the completeFlag to set
	 */
	public void setCompleteFlag(String completeFlag) {
		this.completeFlag = completeFlag;
	}
	/**
	 * @return the verifyFlag
	 */
	public String getVerifyFlag() {
		return verifyFlag;
	}
	/**
	 * @param verifyFlag the verifyFlag to set
	 */
	public void setVerifyFlag(String verifyFlag) {
		this.verifyFlag = verifyFlag;
	}
	/**
	 * @return the rdBackOrderDetails
	 */
	public List getRdBackOrderDetails() {
		return rdBackOrderDetails;
	}
	/**
	 * @param rdBackOrderDetails the rdBackOrderDetails to set
	 */
	public void setRdBackOrderDetails(List rdBackOrderDetails) {
		this.rdBackOrderDetails = rdBackOrderDetails;
	}
	/**
	 * @return the daysDiff
	 */
	public String getDaysDiff() {
		return daysDiff;
	}
	/**
	 * @param daysDiff the daysDiff to set
	 */
	public void setDaysDiff(String daysDiff) {
		this.daysDiff = daysDiff;
	}
	/**
	 * @return the daysDiffRule
	 */
	public String getDaysDiffRule() {
		return daysDiffRule;
	}
	/**
	 * @param daysDiffRule the daysDiffRule to set
	 */
	public void setDaysDiffRule(String daysDiffRule) {
		this.daysDiffRule = daysDiffRule;
	}
	/**
	 * @return the displayFlag
	 */
	public String getDisplayFlag() {
		return displayFlag;
	}
	/**
	 * @param displayFlag the displayFlag to set
	 */
	public void setDisplayFlag(String displayFlag) {
		this.displayFlag = displayFlag;
	}
	/**
	 * @return the alTypeList
	 */
	public ArrayList getAlTypeList() {
		return alTypeList;
	}
	/**
	 * @param alTypeList the alTypeList to set
	 */
	public void setAlTypeList(ArrayList alTypeList) {
		this.alTypeList = alTypeList;
	}
	
}
