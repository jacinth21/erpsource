package com.globus.operations.requests.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmStockInitiateForm extends GmRequestMasterForm {

  
  private String requestType = "26240420"; // consignment
  private String requestTypeName ="Stock Transfer";
  private String requestId = "";
  private String shipTo = "";
  private String requiredDate = "";
  private String hinputStr = "";

  private String hinputstring = "";
  private String hfromPage = "";
  private String shipToID = "";
  private String requestBy = "";
  private String requestByType = "";
  private String haddInfo = "";
  private String fulfillCompId = "";
  // extending GmRequestMasterForm so couldnot extend GmLogForm
  private String txt_LogReason = "";
  private ArrayList alLogReasons = new ArrayList();

  private ArrayList alFulfillCompany = new ArrayList();
  private ArrayList alAssocPlantList = new ArrayList();
  
  


  /**
   * @return the alAssocPlantList
   */
  public ArrayList getAlAssocPlantList() {
    return alAssocPlantList;
  }

  /**
   * @param alAssocPlantList the alAssocPlantList to set
   */
  public void setAlAssocPlantList(ArrayList alAssocPlantList) {
    this.alAssocPlantList = alAssocPlantList;
  }

  /**
   * @return the hinputstring
   */
  public String getHinputstring() {
    return hinputstring;
  }

  /**
   * @param hinputstring the hinputstring to set
   */
  public void setHinputstring(String hinputstring) {
    this.hinputstring = hinputstring;
  }

  /**
   * @return the requestType
   */
  public String getRequestType() {
    return requestType;
  }

  /**
   * @param requestType the requestType to set
   */
  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }

  /**
   * @return the shipTo
   */
  public String getShipTo() {
    return shipTo;
  }

  /**
   * @param shipTo the shipTo to set
   */
  public void setShipTo(String shipTo) {
    this.shipTo = shipTo;
  }


  /**
   * @return the requiredDate
   */
  public String getRequiredDate() {
    return requiredDate;
  }

  /**
   * @param requiredDate the requiredDate to set
   */
  public void setRequiredDate(String requiredDate) {
    this.requiredDate = requiredDate;
  }



  /**
   * @return the hfromPage
   */
  public String getHfromPage() {
    return hfromPage;
  }

  /**
   * @param hfromPage the hfromPage to set
   */
  public void setHfromPage(String hfromPage) {
    this.hfromPage = hfromPage;
  }


  public String getRequestBy() {
    return requestBy;
  }

  public void setRequestBy(String requestBy) {
    this.requestBy = requestBy;
  }

  public String getRequestByType() {
    return requestByType;
  }

  public void setRequestByType(String requestByType) {
    this.requestByType = requestByType;
  }


  public String getShipToID() {
    return shipToID;
  }

  public void setShipToID(String shipToID) {
    this.shipToID = shipToID;
  }


  /**
   * @return the haddInfo
   */
  public String getHaddInfo() {
    return haddInfo;
  }

  /**
   * @param haddInfo the haddInfo to set
   */
  public void setHaddInfo(String haddInfo) {
    this.haddInfo = haddInfo;
  }

  
  /**
   * @return the alFulfillCompany
   */
  public ArrayList getAlFulfillCompany() {
    return alFulfillCompany;
  }

  /**
   * @param alFulfillCompany the alFulfillCompany to set
   */
  public void setAlFulfillCompany(ArrayList alFulfillCompany) {
    this.alFulfillCompany = alFulfillCompany;
  }

  /**
   * @return the fulfillCompId
   */
  public String getFulfillCompId() {
    return fulfillCompId;
  }

  /**
   * @param fulfillCompId the fulfillCompId to set
   */
  public void setFulfillCompId(String fulfillCompId) {
    this.fulfillCompId = fulfillCompId;
  }

  /**
   * @return the requestTypeName
   */
  public String getRequestTypeName() {
    return requestTypeName;
  }

  /**
   * @param requestTypeName the requestTypeName to set
   */
  public void setRequestTypeName(String requestTypeName) {
    this.requestTypeName = requestTypeName;
  }
  
  /**
   * @return the requestId
   */
  public String getRequestId() {
    return requestId;
  }

  /**
   * @param requestId the requestId to set
   */
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
  
  /**
   * @return the hinputStr
   */
  public String getHinputStr() {
    return hinputStr;
  }

  /**
   * @param hinputStr the hinputStr to set
   */
  public void setHinputStr(String hinputStr) {
    this.hinputStr = hinputStr;
  }

  /**
   * @return Returns the alLogReasons.
   */
  public ArrayList getAlLogReasons()
  {
      return alLogReasons;
  }
  /**
   * @param alLogReasons The alLogReasons to set.
   */
  public void setAlLogReasons(ArrayList alLogReasons)
  {
      this.alLogReasons = alLogReasons;
  }
  /**
   * @return Returns the txt_LogReason.
   */
  public String getTxt_LogReason()
  {
      return txt_LogReason;
  }
  /**
   * @param txt_LogReason The txt_LogReason to set.
   */
  public void setTxt_LogReason(String txt_LogReason)
  {
      this.txt_LogReason = txt_LogReason;
  }
}
