package com.globus.operations.receiving.thbreceiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInventoryLookUpDetailsBean extends GmBean {
	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmInventoryLookUpDetailsBean(GmDataStoreVO gmDataStoreVO)
			throws Exception {
		super(gmDataStoreVO);
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchInventoryLookUpDetails - Depending on the strFromLink value, Backorder/PTRD details 
	 * will be fetched
	 **/

	public String fetchInventoryLookUpDetails(HashMap hmParam) throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strPrivatePart = "";
		String stranalysisid = "";
		String strFromLink = "";
		String strJSONArrayList = "";
		String strJSONAnalysisArr = "";
		strPrivatePart = GmCommonClass.parseNull((String) hmParam.get("STRPRIVATEPART"));
		stranalysisid = GmCommonClass.parseNull((String) hmParam.get("STRANALYSISID"));
		strFromLink = GmCommonClass.parseNull((String) hmParam.get("STRFROMLINK"));
		//103932: PTRD, 101260: Acknowledgement order
		String strType = strFromLink.equals("PTRD") ? "103932": "101260";
		log.info("Back Order Link Type"+strType);
		log.info("strPrivatePart"+strPrivatePart);
		log.info("stranalysisid"+stranalysisid);
		gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_fch_load_back_order_dtls", 4); 
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strPrivatePart);
		gmDBManager.setString(2, stranalysisid);
		gmDBManager.setString(3, strType);
		gmDBManager.execute();
		strJSONArrayList = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();
		//JSON String Object To JSON String array
		strJSONAnalysisArr = "[" + strJSONArrayList+ "]";
    	return strJSONAnalysisArr;
	}
}