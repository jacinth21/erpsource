package com.globus.operations.receiving.thbreceiving.actions;

import org.apache.log4j.Logger;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.thbreceiving.beans.GmDonorLoadDtlsReportBean;
import com.globus.operations.receiving.thbreceiving.beans.GmDonorScheduleBean;
import com.globus.operations.receiving.thbreceiving.forms.GmDonorScheduleForm;

 /**
 * GmDonorScheduleAction : Contains the methods used for the THB Donor stage Scheduling screen
 * author : ssharmila
 */

public class GmDonorScheduleAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadTHBDonorScheduling : This method is used to load the THB Donor stage Scheduling screen
   */
  public ActionForward loadTHBDonorScheduling(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDonorScheduleForm gmDonorScheduleForm = (GmDonorScheduleForm) actionForm;
    gmDonorScheduleForm.loadSessionParameters(request);
    GmDonorScheduleBean gmDonorScheduleBean = new GmDonorScheduleBean(getGmDataStoreVO());
    GmDonorLoadDtlsReportBean gmDonorLoadDtlsReportBean =
        new GmDonorLoadDtlsReportBean(getGmDataStoreVO());
    
    ArrayList alActionList = new ArrayList();
    alActionList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("BBARS",getGmDataStoreVO()));
    gmDonorScheduleForm.setAlActionList(alActionList);
    return actionMapping.findForward("gmTHBDonorSchedule");
  }

  /**
   * fetchTHBDonorSchedulingDtls : This method is used to fetch the THB Donor stage Scheduling
   * screen
   */
  public ActionForward fetchTHBDonorSchedulingDtls(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    instantiate(request, response);
    GmDonorScheduleForm gmDonorScheduleForm = (GmDonorScheduleForm) actionForm;
    gmDonorScheduleForm.loadSessionParameters(request);
    GmDonorScheduleBean gmDonorScheduleBean = new GmDonorScheduleBean(getGmDataStoreVO());
    GmDonorLoadDtlsReportBean gmDonorLoadDtlsReportBean =
        new GmDonorLoadDtlsReportBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    String strJSONGridData = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmDonorScheduleForm);
    log.debug("fetchTHBDonorSchedulingDtls>hmParam> " + hmParam);
    strJSONGridData = GmCommonClass.parseNull(gmDonorScheduleBean.fetchDonorLoadSchDtsl(hmParam));
    gmDonorScheduleForm.setStrScheduleGridData(strJSONGridData);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strJSONGridData);
      pw.flush();
      return null;
  }

  /**
   * fchDonorStageDate : This method is used to fetch the THB Donor Date stage in Refresh screen
   */
  public ActionForward fchDonorStageDate(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDonorScheduleForm gmDonorScheduleForm = (GmDonorScheduleForm) actionForm;
    gmDonorScheduleForm.loadSessionParameters(request);
    GmDonorScheduleBean gmDonorScheduleBean = new GmDonorScheduleBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alLoadDtls = new ArrayList();
    String strxmlGridData = "";
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmDonorScheduleForm);
    log.debug("fchDonorStageDate>> " + hmParam);
    alLoadDtls = GmCommonClass.parseNullArrayList(gmDonorScheduleBean.fchDonorStageDate(hmParam));
    hmReturn.put("alReturn", alLoadDtls);
    hmReturn.put("DATEFMT", strApplDateFmt);
    hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmReturn.put("STRAPPLDATEFMT ", strApplDateFmt);
    strxmlGridData = generateOutPut(hmReturn, "GmTHBDonorSchedulingDt.vm");
    if (!alLoadDtls.equals("")) {
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strxmlGridData);
      pw.flush();
      return null;
    }
    return actionMapping.findForward("gmTHBDonorSchedule");
  }

  /**
   * Generate output method : calls the vmFile
   */
  private String generateOutPut(HashMap hmReturn, String vmFile) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmReturn.get("STRSESSCOMPANYLOCALE"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmReturn.get("STRAPPLDATEFMT"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setTemplateSubDir("operations/receiving/thbreceiving//templates");
    templateUtil.setTemplateName("GmTHBDonorSchedulingDt.vm");
    return templateUtil.generateOutput();
  }

  /**
   * SavDonorStageDate : This method is used to save the date entered in the screen
   */
  public ActionForward SavDonorStageDate(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try{
    GmDonorScheduleForm gmTHBDonorSchedulingForm = (GmDonorScheduleForm) actionForm;
    gmTHBDonorSchedulingForm.loadSessionParameters(request);
    GmDonorScheduleBean gmTHBDonorSchedulingBean = new GmDonorScheduleBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    String strStageeDt = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmTHBDonorSchedulingForm);
    log.debug("SavDonorStageDate>hmParam> " + hmParam);
    gmTHBDonorSchedulingBean.saveDonorStageDate(hmParam);
    PrintWriter pw = response.getWriter();
	pw.write(strStageeDt);
	pw.flush();	
    }catch(Exception e){
      PrintWriter pWriter = response.getWriter();
      response.setStatus(300);
      response.setContentType("text/xml");
      pWriter.write(e.getMessage());
      pWriter.flush();
   }	
    return null;
  }

  public ActionForward loadTHBDonorPO(ActionMapping actionMapping, ActionForm actionForm,  
	      HttpServletRequest request, HttpServletResponse response) throws Exception { 
	    instantiate(request, response);
	    
	    GmDonorScheduleForm gmDonorScheduleForm = (GmDonorScheduleForm) actionForm;
	    gmDonorScheduleForm.loadSessionParameters(request);
	    String strDonorNum=GmCommonClass.parseNull((String)gmDonorScheduleForm.getStrDonorNum());
	    String strLoadNum=GmCommonClass.parseNull((String)gmDonorScheduleForm.getStrLoadNum());
	    
	    gmDonorScheduleForm.setStrDonorNum(strDonorNum);
	    gmDonorScheduleForm.setStrLoadNum(strLoadNum);	    
	    
    return actionMapping.findForward("gmTHBDonorSchedulePO");

  }
  
  public ActionForward createRS(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
      try{
	    GmDonorScheduleForm gmDonorScheduleForm = (GmDonorScheduleForm) actionForm;
	    gmDonorScheduleForm.loadSessionParameters(request);
	    GmDonorScheduleBean gmDonorScheduleBean = new GmDonorScheduleBean(getGmDataStoreVO());
	    String strDonorNum=GmCommonClass.parseNull((String)gmDonorScheduleForm.getStrDonorNum());
	    String strLoadNum=GmCommonClass.parseNull((String)gmDonorScheduleForm.getStrLoadNum());
	    String strPoNum=GmCommonClass.parseNull((String)gmDonorScheduleForm.getStrPoNum());
	    gmDonorScheduleForm.setStrDonorNum(strDonorNum);
	    gmDonorScheduleForm.setStrLoadNum(strLoadNum);
	    gmDonorScheduleForm.setStrPoNum(strPoNum);
	    String plantid = GmCommonClass.parseNull((String)getGmDataStoreVO().getPlantid());
	    String cmpid = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmDonorScheduleForm);
	    String userid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    hmParam.put("STRDONORNUM", strDonorNum);
	    hmParam.put("STRLOADNUM", strLoadNum);
	    hmParam.put("STRPONUM", strPoNum);
	    hmParam.put("PLANTID", plantid);
	    hmParam.put("CMPID", cmpid);
	    hmParam.put("USERID", userid);
	    hmParam.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
	    gmDonorScheduleBean.initiateTHBDonorRSJMS(hmParam);
	    log.debug("hmParam> " + hmParam);
	    }catch(Exception e){
	     }	
	      return null; 
  	}
}
