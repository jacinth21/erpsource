package com.globus.operations.receiving.thbreceiving.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.jms.consumers.beans.GmTHBLockGenBean;
import com.globus.operations.receiving.thbreceiving.beans.GmTHBPendAnalysisDashBean;
import com.globus.operations.receiving.thbreceiving.forms.GmTHBPendAnalysisDashForm;
import com.globus.sales.event.beans.GmEventSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ssharmila
 * 
 */

public class GmTHBPendAnalysisDashAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadTHBPendAnalysisDashBoard : This method is used to fetch the information to THB Load Pending
   * Analysis Dashboard screen
   */

  public ActionForward loadTHBPendAnalysisDashBoard(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    instantiate(request, response);
    GmTHBPendAnalysisDashForm gmTHBPendAnalysisDashForm = (GmTHBPendAnalysisDashForm) actionForm;
    gmTHBPendAnalysisDashForm.loadSessionParameters(request);
    GmTHBPendAnalysisDashBean gmTHBPendAnalysisDashBean =
        new GmTHBPendAnalysisDashBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    String strTHBPendAnalJsonString = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmTHBPendAnalysisDashForm);
    String strLockGenerateAccess = "";
    strLockGenerateAccess =
        gmEventSetupBean.getEventAccess(getGmDataStoreVO().getPartyid(), "EVENT",
            "LOCK_GENERATE_ACCESS");
    gmTHBPendAnalysisDashForm.setStrLockGenerateAccess(strLockGenerateAccess);
    strTHBPendAnalJsonString =
        GmCommonClass.parseNull(gmTHBPendAnalysisDashBean.loadTHBPendAnalysisDashBoard(hmParam));
    gmTHBPendAnalysisDashForm.setStrJSONGridData(strTHBPendAnalJsonString);
    return actionMapping.findForward("gmTHBPendDash");
  }

  /**
   * loadLockAndGenerate : This method is used to Lock the Details in inventory for Analysis
   */

  public ActionForward loadLockAndGenerate(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTHBPendAnalysisDashForm gmTHBPendAnalysisDashForm = (GmTHBPendAnalysisDashForm) actionForm;
    gmTHBPendAnalysisDashForm.loadSessionParameters(request);
    GmTHBPendAnalysisDashBean gmTHBPendAnalysisDashBean =
        new GmTHBPendAnalysisDashBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmTHBPendAnalysisDashForm);
    String strLockGenerateAccess = "";
    strLockGenerateAccess =
        gmEventSetupBean.getEventAccess(getGmDataStoreVO().getPartyid(), "EVENT",
            "LOCK_GENERATE_ACCESS");
    gmTHBPendAnalysisDashForm.setStrLockGenerateAccess(strLockGenerateAccess);
    return actionMapping.findForward("gmTHBLockGen");
  }

  /**
   * saveLockAndGenerate : This method is used to save the Lock Details in inventory for Analysis
   */

  public ActionForward saveLockAndGenerate(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTHBPendAnalysisDashForm gmTHBPendAnalysisDashForm = (GmTHBPendAnalysisDashForm) actionForm;
    gmTHBPendAnalysisDashForm.loadSessionParameters(request);
    GmTHBPendAnalysisDashBean gmTHBPendAnalysisDashBean =
        new GmTHBPendAnalysisDashBean(getGmDataStoreVO());
    GmTHBLockGenBean gmTHBLockGenBean = new GmTHBLockGenBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmDataStoreVO tmpGmDataStoreVO = null;
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    hmParam = GmCommonClass.getHashMapFromForm(gmTHBPendAnalysisDashForm);
    String strScreen = GmCommonClass.parseNull((String) hmParam.get("STRSCREEN"));
    hmParam.put("COMPANYINFO", strCompanyInfo);
    String strLockGenerateAccess = "";
    strLockGenerateAccess =
        gmEventSetupBean.getEventAccess(getGmDataStoreVO().getPartyid(), "EVENT",
            "LOCK_GENERATE_ACCESS");
    gmTHBPendAnalysisDashForm.setStrLockGenerateAccess(strLockGenerateAccess);
    gmTHBPendAnalysisDashBean.updLockStatus(gmTHBPendAnalysisDashForm.getStrLoadNumStr(),
        gmTHBPendAnalysisDashForm.getPartyId(), "108220");
    gmTHBPendAnalysisDashForm.setStrLoadStatus("108220");// to indicate the process inprogress
    if (strScreen.equals("")) {
      // gmTHBPendAnalysisDashBean.saveLockAndGenerate(hmParam);
      gmTHBLockGenBean.saveLockAndGenerate(hmParam);
    } else // to update processing date from List Report screen
    {
      gmTHBPendAnalysisDashBean.updLockAndGenerateDate(hmParam);
    }
    return actionMapping.findForward("gmTHBLockGen");
  }
}
