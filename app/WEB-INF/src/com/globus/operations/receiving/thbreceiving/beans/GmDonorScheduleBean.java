package com.globus.operations.receiving.thbreceiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;


public class GmDonorScheduleBean extends GmBean {

  public GmDonorScheduleBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveDonorStageDate - This Method Used to save the donor stage date Details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public void saveDonorStageDate(HashMap hmParam) throws AppError, Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLoadNum = GmCommonClass.parseNull((String) hmParam.get("STRLOADNUM"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("STRDONORNUM"));
    String strStageDt = GmCommonClass.parseNull((String) hmParam.get("STRSTAGEDT"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    Date dtStageDt = (Date) GmCommonClass.getStringToDate(strStageDt, strApplDateFmt);
    gmDBManager.setPrepareString("gm_pkg_op_donor_inv_allocation.gm_sav_donor_scheduling_stage", 4);
    gmDBManager.setString(1, strLoadNum);
    gmDBManager.setString(2, strDonorNum);
    gmDBManager.setDate(3, dtStageDt);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }
  
  
  
  /**
   * saveDonorStageDate - This Method Used to save the donor stage date Details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public void updateStageDate(HashMap hmParam) throws AppError, Exception {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strTxn_id = GmCommonClass.parseNull((String) hmParam.get("STRTXN"));
    String txn_type = GmCommonClass.parseNull((String) hmParam.get("STRTXNTYPE"));
    String strStageDt = GmCommonClass.parseNull((String) hmParam.get("STRTXNDATE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    Date dtStageDt = (Date) GmCommonClass.getStringToDate(strStageDt, strApplDateFmt);
    gmDBManager.setPrepareString("gm_pkg_op_donor_inv_allocation.gm_sav_stage_date", 4);
    gmDBManager.setString(1, strTxn_id);
    gmDBManager.setString(2, txn_type);
    gmDBManager.setDate(3, dtStageDt);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchDonorLoadSchDtsl - This Method Used to load the donor load Details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public String fetchDonorLoadSchDtsl(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLoadNum = GmCommonClass.parseNull((String) hmParam.get("STRLOADNUM"));
    String strDonorDtlsRptJSONString = "";
    gmDBManager.setPrepareString("gm_pkg_op_thb_detail_rpt.gm_fch_load_schedue_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
    gmDBManager.setString(1, strLoadNum);
    gmDBManager.execute();
    strDonorDtlsRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strDonorDtlsRptJSONString;
  }

  /**
   * fchDonorStageDate - This Method Used to load the donor Stage Date Details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public ArrayList fchDonorStageDate(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLoadNum = GmCommonClass.parseNull((String) hmParam.get("STRLOADNUM"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("STRDONORNUM"));
    ArrayList alDonorDtlsRpt = new ArrayList();
    String strDonorDtlsRptJSONString = "";
    gmDBManager.setPrepareString("gm_pkg_op_thb_detail_rpt.gm_fch_donor_stage_date", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLoadNum);
    gmDBManager.setString(2, strDonorNum);
    gmDBManager.execute();
    alDonorDtlsRpt = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alDonorDtlsRpt;
  }
  
  //Create new Bean method to initiate the JMS process 
  public void initiateTHBDonorRSJMS(HashMap hmParam) throws AppError {       

	  	String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("companyInfo"));
	  	GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
        String strTHBQueue = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_THB_LOCK_GENERATE_QUEUE"));
        String strBBAConsumerClass =   GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("BBA_RS_CREATION_CONSUMER_CLASS"));
        hmParam.put("COMPANY_ID", getCompId());
  		hmParam.put("HMHOLDVALUES", hmParam);
  		hmParam.put("companyInfo", strCompanyInfo);
        hmParam.put("CONSUMERCLASS", strBBAConsumerClass);
        hmParam.put("QUEUE_NAME", strTHBQueue);
  	    log.debug("Message Sent Batch #:"+hmParam);
  	      
  		gmConsumerUtil.sendMessage(hmParam);
  }
  public void createRS(HashMap hmParam) throws AppError, Exception {

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
        String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("STRDONORNUM"));
	    String strLoadNum = GmCommonClass.parseNull((String) hmParam.get("STRLOADNUM"));
	    String userid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strPoNum = GmCommonClass.parseNull((String) hmParam.get("STRPONUM"));
	    String cmpid = GmCommonClass.parseNull((String) hmParam.get("CMPID"));
	    String plantid = GmCommonClass.parseNull((String) hmParam.get("PLANTID"));
	    gmDBManager.setPrepareString("gm_pkg_op_donor_sav_bulk_rs.gm_sav_rs_creation_main", 6);
	    gmDBManager.setString(1, strDonorNum);
	    gmDBManager.setString(2, strLoadNum);
	    gmDBManager.setString(3, userid);
	    gmDBManager.setString(4, strPoNum);
	    gmDBManager.setString(5, cmpid);
	    gmDBManager.setString(6, plantid);
	    gmDBManager.execute();
	    gmDBManager.commit();
	    
	  }

}
