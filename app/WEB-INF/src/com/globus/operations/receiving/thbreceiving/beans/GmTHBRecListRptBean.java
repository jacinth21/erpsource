package com.globus.operations.receiving.thbreceiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmTHBRecListRptBean extends GmBean {

	public GmTHBRecListRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchTHBRecListRpt : Method used to fetch THB List Details
	 * 
	 * @return alReturn
	 * @throws AppError
	 */
	public ArrayList fetchTHBRecListRpt(HashMap hmParam) throws AppError {
		String strThbLoadNum = GmCommonClass.parseNull((String) hmParam
				.get("STRTHBLOADNUM"));
		String strBoxNum = GmCommonClass.parseNull((String) hmParam
				.get("STRBOXNUM"));
		String strLoadStatus = GmCommonClass.parseNull((String) hmParam
				.get("STRLOADSTATUS"));
		String strFromDt = GmCommonClass.parseNull((String) hmParam
				.get("STRFROMDATE"));
		String strToDt = GmCommonClass.parseNull((String) hmParam
				.get("STRTODATE"));
		String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());

		strLoadStatus = strLoadStatus.equals("0") ? "" : strLoadStatus;

		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		StringBuffer sbQuery = new StringBuffer();
		/* Query for Date Filter */
		sbQuery.append(" SELECT T2560.C2560_THB_LOAD_ID LOAD_ID, ");
		sbQuery.append(" T2560.C2560_THB_LOAD_NUM LOAD_NUM,T2560.C2560_THB_BOX_NUM BOX_NUM,T2560.C2560_LOAD_RECEIVED_DATE RECEIVED_DT, ");
		sbQuery.append(" GET_USER_NAME(T2560.C2560_LOAD_RECEIVED_BY) RECEIVED_BY,GET_CODE_NAME(T2560.C901_LOAD_STATUS) LOAD_STATUS , T2560.C901_LOAD_STATUS LOAD_STATUS_ID");
		sbQuery.append(" FROM T2560_THB_LOAD T2560 ");
		sbQuery.append(" WHERE T2560.C2560_VOID_FL IS NULL ");

		if (!strThbLoadNum.equals("")) {
			sbQuery.append(" AND T2560.C2560_THB_LOAD_NUM = " + strThbLoadNum);
		}
		if (!strBoxNum.equals("")) {
			sbQuery.append(" AND T2560.C2560_THB_BOX_NUM = '");
			sbQuery.append(strBoxNum);
			sbQuery.append("' ");
		}
		if (!strLoadStatus.equals("")) {
			sbQuery.append(" AND T2560.C901_LOAD_STATUS = " + strLoadStatus);
		}
		if (!strFromDt.equals("") && !strToDt.equals("")) {

			sbQuery.append(" AND To_DATE(T2560.C2560_LOAD_RECEIVED_DATE) >= To_DATE('"
					+ strFromDt + "', '" + strCompDateFmt + "')");
			sbQuery.append(" AND To_DATE(T2560.C2560_LOAD_RECEIVED_DATE) <= To_DATE('"
					+ strToDt + "', '" + strCompDateFmt + "')");
		}
		sbQuery.append(" ORDER BY T2560.C2560_THB_LOAD_NUM,T2560.C2560_THB_BOX_NUM ");
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug("fetchTHBRecListRpt sbQuery" + sbQuery.toString());
		return alReturn;

	}

	/**
	 * fetchTHBListReportDetail:Method used to fetch THB List Report Details
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchTHBListReportDetail(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		ArrayList alReturn = new ArrayList();

		String strLoadNum = GmCommonClass.parseNull((String) hmParam
				.get("STRTHBLOADNUM"));
		String strLoadId = GmCommonClass.parseNull((String) hmParam
				.get("STRLOADID"));
		gmDBManager.setPrepareString(
				"gm_pkg_op_thb_load_report.gm_fch_thb_load_detail", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strLoadNum);
		gmDBManager.setString(2, strLoadId);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager
				.getObject(3));
		gmDBManager.close();
		return alReturn;

	}

	/**
	 * 
	 * generateOutput - Array list converted to XML string using (VM)
	 * 
	 * @param alGridData
	 * @param hmDtls
	 * @return String
	 * @throws AppError
	 */
	public String generateOutput(ArrayList alGridData, HashMap hmDtls)
			throws AppError {
		GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
		String strTemplateName = GmCommonClass.parseNull((String) hmDtls
				.get("TEMPLATE_NM"));
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmDtls
				.get("STRSESSCOMPANYLOCALE"));
		String strLabelProperties = GmCommonClass.parseNull((String) hmDtls
				.get("LABEL_PROPERTIES_NM"));
		gmTemplateUtil.setDataList("alGridData", alGridData);
		gmTemplateUtil.setDataMap("hmApplnParam", hmDtls);
		gmTemplateUtil.setTemplateName(strTemplateName);
		gmTemplateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
				strLabelProperties, strSessCompanyLocale));
		gmTemplateUtil
				.setTemplateSubDir("operations/receiving/thbreceiving/templates");

		return gmTemplateUtil.generateOutput();

	}
}
