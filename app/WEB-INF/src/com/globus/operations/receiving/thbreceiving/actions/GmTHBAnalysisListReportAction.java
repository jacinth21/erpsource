package com.globus.operations.receiving.thbreceiving.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.receiving.thbreceiving.beans.GmTHBAnalysisListReportBean;
import com.globus.operations.receiving.thbreceiving.forms.GmTHBAnalysisListReportForm;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmTHBAnalysisListReportAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * loadListReportDetails : This method is used to load the 
	 * THB Analysis List report screen
	 */

	public ActionForward loadListReportDetails(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmTHBAnalysisListReportForm gmTHBAnalysisListReportForm = (GmTHBAnalysisListReportForm) actionForm;
		GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		gmTHBAnalysisListReportForm.loadSessionParameters(request);
		ArrayList alStatList = new ArrayList();
		ArrayList alActionList = new ArrayList();
		String strScheduleAccess = "";
		strScheduleAccess = gmEventSetupBean.getEventAccess(getGmDataStoreVO()
				.getPartyid(), "EVENT", "SCHEDULE_ACCESS");//fetch Authorized users to perform the actions listed in the dropdown
		gmTHBAnalysisListReportForm.setStrScheduleAccess(strScheduleAccess);
		alStatList = gmAutoCompleteReportBean.getCodeList("THBANA", getGmDataStoreVO()); // to fetch the THB Status List 
		alActionList = gmAutoCompleteReportBean.getCodeList("THBACT", getGmDataStoreVO());// to fetch the details for the action dropdown List 
		gmTHBAnalysisListReportForm.setAlStatList(alStatList);
		gmTHBAnalysisListReportForm.setAlActionList(alActionList);
		return actionMapping.findForward("gmTHBAnalysisListRpt");

	}

	/**
	 * fetchListReportDetails : This method is used to fetch the information to
	 * THB Analysis List report screen
	 */

	public ActionForward fetchListReportDetails(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmTHBAnalysisListReportForm gmTHBAnalysisListReportForm = (GmTHBAnalysisListReportForm) actionForm;
		GmTHBAnalysisListReportBean gmTHBAnalysisListReportBean = new GmTHBAnalysisListReportBean(
				getGmDataStoreVO());
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
		gmTHBAnalysisListReportForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmTHBAnalysisListReportForm);
		String strScheduleAccess = "";
		strScheduleAccess = gmEventSetupBean.getEventAccess(getGmDataStoreVO()
				.getPartyid(), "EVENT", "SCHEDULE_ACCESS");//fetch Authorized users to perform the actions listed in the dropdown
		gmTHBAnalysisListReportForm.setStrScheduleAccess(strScheduleAccess);
		log.debug("fetchListReportDetails hmParam>> "+hmParam);
		strJSONGridData = gmTHBAnalysisListReportBean
				.fetchListReportDetails(hmParam);
		gmTHBAnalysisListReportForm.setStrJSONGridData(strJSONGridData);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONGridData.equals("")) {
			pw.write(strJSONGridData);
		}
		pw.flush();
		return null;

	}
}