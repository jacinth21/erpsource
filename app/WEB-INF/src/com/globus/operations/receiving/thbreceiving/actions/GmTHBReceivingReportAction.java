/**
 * Description : This file is used for perform fetch and save related actions of THB Receiving Dashboard
 */
package com.globus.operations.receiving.thbreceiving.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.globus.common.actions.GmAction;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping; 
import org.apache.struts.action.ActionForm;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.thbreceiving.forms.GmTHBReceivingReportForm;
import com.globus.operations.receiving.thbreceiving.beans.GmTHBReceivingDashboardBean;


public class GmTHBReceivingReportAction extends GmDispatchAction {
Logger log = GmLogger.getInstance(this.getClass().getName());

/**
 * fetchTHBReceivingDashBoard : This method is used to fetch the info to the screen
 */

  public ActionForward fetchTHBReceivingDashBoard(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTHBReceivingReportForm gmTHBReceivingReportForm = (GmTHBReceivingReportForm) actionForm;
    gmTHBReceivingReportForm.loadSessionParameters(request);
    GmTHBReceivingDashboardBean gmTHBReceivingDashboardBean = new GmTHBReceivingDashboardBean(getGmDataStoreVO());
    
    ArrayList alLoadDtls = new ArrayList();   
    HashMap hmReturn = new HashMap();

    String strOpt = "";
    String strxmlGridData = "";  
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strSessCompanyLocale = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    alLoadDtls = GmCommonClass.parseNullArrayList(gmTHBReceivingDashboardBean.fetchTHBReceivingDashBoard());
    hmReturn.put("alReturn", alLoadDtls);
    hmReturn.put("DATEFMT", strApplDateFmt);
    hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmReturn.put("STRAPPLDATEFMT ", strApplDateFmt);
   
    strxmlGridData = generateOutPut(hmReturn, "GmTHBReceivingDashBoard.vm");
    gmTHBReceivingReportForm.setStrXmlGridData(strxmlGridData);
    return actionMapping.findForward("gmTHBReceivingReport");
}
  
  /**
   * Generate output method :  calls the vmFile
   */
   private String generateOutPut(HashMap hmReturn, String vmFile) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale = GmCommonClass.parseNull((String) hmReturn.get("STRSESSCOMPANYLOCALE"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmReturn.get("STRAPPLDATEFMT"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setTemplateSubDir("operations/receiving/thbreceiving//templates");
    templateUtil.setTemplateName("GmTHBReceivingDashBoard.vm");
    return templateUtil.generateOutput();
  }
}
