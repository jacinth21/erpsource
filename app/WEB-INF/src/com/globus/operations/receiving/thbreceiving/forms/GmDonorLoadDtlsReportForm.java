package com.globus.operations.receiving.thbreceiving.forms;

import com.globus.common.forms.GmCommonForm;

public class GmDonorLoadDtlsReportForm extends GmCommonForm {

	private String gridXmlData = "";
	private String loadNum = "";
	private String donorNum = "";
	private String hLoadNumStr = "";
	private String hDonorNumStr = "";
	private String strJSONGridData = "";
	private String strOpt = "";
    private String strFromLink = "";
	private String hFromLink = "";
	private String stageDateFrom = "";
	private String stageDateTo = "";
    private String status = "";

  /**
   * @return the strFromLink
   */
  public String getStrFromLink() {
    return strFromLink;
  }

  /**
   * @param strFromLink the strFromLink to set
   */
  public void setStrFromLink(String strFromLink) {
    this.strFromLink = strFromLink;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

	/**
	 * @return the loadNum
	 */
	public String getLoadNum() {
		return loadNum;
	}

  /**
   * @param loadNum the loadNum to set
   */
  public void setLoadNum(String loadNum) {
    this.loadNum = loadNum;
  }

	/**
	 * @return the donorNum
	 */
	public String getDonorNum() {
		return donorNum;
	}

  /**
   * @param donorNum the donorNum to set
   */
  public void setDonorNum(String donorNum) {
    this.donorNum = donorNum;
  }

	/**
	 * @return the hLoadNumStr
	 */
	public String gethLoadNumStr() {
		return hLoadNumStr;
	}

  /**
   * @param hLoadNumStr the hLoadNumStr to set
   */
  public void sethLoadNumStr(String hLoadNumStr) {
    this.hLoadNumStr = hLoadNumStr;
  }

	/**
	 * @return the hDonorNumStr
	 */
	public String gethDonorNumStr() {
		return hDonorNumStr;
	}

  /**
   * @param hDonorNumStr the hDonorNumStr to set
   */
  public void sethDonorNumStr(String hDonorNumStr) {
    this.hDonorNumStr = hDonorNumStr;
  }

	public String getStrJSONGridData() {
		return strJSONGridData;
	}

	public void setStrJSONGridData(String strJSONGridData) {
		this.strJSONGridData = strJSONGridData;
	}

	/**
	 * @return the strOpt
	 */
	public String getStrOpt() {
		return strOpt;
	}

	/**
	 * @param strOpt the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}

	public String gethFromLink() {
		return hFromLink;
	}

	public void sethFromLink(String hFromLink) {
		this.hFromLink = hFromLink;
	}

	public String getStageDateFrom() {
		return stageDateFrom;
	}

	public void setStageDateFrom(String stageDateFrom) {
		this.stageDateFrom = stageDateFrom;
	}

	public String getStageDateTo() {
		return stageDateTo;
	}

	public void setStageDateTo(String stageDateTo) {
		this.stageDateTo = stageDateTo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
