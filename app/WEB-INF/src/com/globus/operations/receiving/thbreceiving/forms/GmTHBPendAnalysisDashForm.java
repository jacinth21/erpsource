package com.globus.operations.receiving.thbreceiving.forms;

import com.globus.common.forms.GmCommonForm;

/**
 * @author ssharmila
 * 
 */

public class GmTHBPendAnalysisDashForm extends GmCommonForm{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String strJSONGridData = "";
	private String strLockGenerateAccess = "";
	private String strLoadNumStr = "";
	private String strLoadIdStr = "";
	private String fromDate = "";
	private String toDate = "";
	private String hLoadNumStr = "";
	private String strAnalysisId = "";
	private String strLoadStatus = "";
	private String strScreen = "";
	/**
	 * @return the strJSONGridData
	 */
	public String getStrJSONGridData() {
		return strJSONGridData;
	}
	/**
	 * @param strJSONGridData the strJSONGridData to set
	 */
	public void setStrJSONGridData(String strJSONGridData) {
		this.strJSONGridData = strJSONGridData;
	}
	/**
	 * @return the strLockGenerateAccess
	 */
	public String getStrLockGenerateAccess() {
		return strLockGenerateAccess;
	}
	/**
	 * @param strLockGenerateAccess the strLockGenerateAccess to set
	 */
	public void setStrLockGenerateAccess(String strLockGenerateAccess) {
		this.strLockGenerateAccess = strLockGenerateAccess;
	}
	/**
	 * @return the strLoadNumStr
	 */
	public String getStrLoadNumStr() {
		return strLoadNumStr;
	}
	/**
	 * @param strLoadNumStr the strLoadNumStr to set
	 */
	public void setStrLoadNumStr(String strLoadNumStr) {
		this.strLoadNumStr = strLoadNumStr;
	}
	/**
	 * @return the strLoadIdStr
	 */
	public String getStrLoadIdStr() {
		return strLoadIdStr;
	}
	/**
	 * @param strLoadIdStr the strLoadIdStr to set
	 */
	public void setStrLoadIdStr(String strLoadIdStr) {
		this.strLoadIdStr = strLoadIdStr;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the hLoadNumStr
	 */
	public String gethLoadNumStr() {
		return hLoadNumStr;
	}
	/**
	 * @param hLoadNumStr the hLoadNumStr to set
	 */
	public void sethLoadNumStr(String hLoadNumStr) {
		this.hLoadNumStr = hLoadNumStr;
	}
	/**
	 * @return the strAnalysisId
	 */
	public String getStrAnalysisId() {
		return strAnalysisId;
	}
	/**
	 * @param strAnalysisId the strAnalysisId to set
	 */
	public void setStrAnalysisId(String strAnalysisId) {
		this.strAnalysisId = strAnalysisId;
	}
	public String getStrLoadStatus() {
		return strLoadStatus;
	}
	public void setStrLoadStatus(String strLoadStatus) {
		this.strLoadStatus = strLoadStatus;
	}
	/**
	 * @return the strScreen
	 */
	public String getStrScreen() {
		return strScreen;
	}
	/**
	 * @param strScreen the strScreen to set
	 */
	public void setStrScreen(String strScreen) {
		this.strScreen = strScreen;
	}
		
}
