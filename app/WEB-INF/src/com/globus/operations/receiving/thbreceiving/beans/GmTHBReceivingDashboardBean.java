package com.globus.operations.receiving.thbreceiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmTHBReceivingDashboardBean extends GmBean {
/**
 * Constructor will populate company info. 
 * @param gmDataStore
 */
  public GmTHBReceivingDashboardBean(GmDataStoreVO gmDataStoreVO) throws Exception {
    super(gmDataStoreVO);
  }
  Logger log = GmLogger.getInstance(this.getClass().getName());
  
  /**
   * fetchTHBReceivingDashBoard - This method will fetch the THB Receiving DashBoard
   * @exception AppError
   **/
  
  public ArrayList fetchTHBReceivingDashBoard() throws AppError  {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_thb_load_report.gm_fch_thb_load_report", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;

  }

}
