package com.globus.operations.receiving.thbreceiving.forms;



import com.globus.common.forms.GmCommonForm;


/**
 * @author ASophia
 *
 */
public class GmBBAStagingDashForm extends GmCommonForm {
private String strXmlGridData = "";
private String hFrom = "";

/**
 * @return the hFrom
 */
public String gethFrom() {
	return hFrom;
}

/**
 * @param hFrom the hFrom to set
 */
public void sethFrom(String hFrom) {
	this.hFrom = hFrom;
}

/**
 * @return the strXmlGridData
 */
public String getStrXmlGridData() {
	return strXmlGridData;
}

/**
 * @param strXmlGridData the strXmlGridData to set
 */
public void setStrXmlGridData(String strXmlGridData) {
	this.strXmlGridData = strXmlGridData;
}


}
