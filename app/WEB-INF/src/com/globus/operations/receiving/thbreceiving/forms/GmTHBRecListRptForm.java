package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;
import java.util.Date;
import com.globus.common.forms.GmCommonForm;

 /**
 * PMT-30847 -THB Receiving List Report Screen
 * Author -Shiny
 */

public class GmTHBRecListRptForm extends GmCommonForm{

	private String strThbLoadNum = "" ;
	private String strBoxNum = "";
	private String strLoadStatus = "";
	private String strFromDate = "" ;
	private String strToDate = "";
	private String strXmlGridData = "" ;
	private String strLoadId = "" ;
	
	private ArrayList alLoadStatusType = new ArrayList();
	/**
	 * @return the AlLoadStatus
	 */
	public ArrayList getAlLoadStatusType() {
		return alLoadStatusType;
	}
	public void setAlLoadStatusType(ArrayList alLoadStatusType) {
		this.alLoadStatusType = alLoadStatusType;
	}
	/**
	 * @return the GridData
	 */
	public String getStrXmlGridData() {
		return strXmlGridData;
	}
	public void setStrXmlGridData(String strXmlGridData) {
		this.strXmlGridData = strXmlGridData;
	}
	/**
	 * @return the ThbLoadNum
	 */
	public String getStrThbLoadNum() {
		return strThbLoadNum;
	}
	public void setStrThbLoadNum(String strThbLoadNum) {
		this.strThbLoadNum = strThbLoadNum;
	}
	/**
	 * @return the BoxNum
	 */
	public String getStrBoxNum() {
		return strBoxNum;
	}
	public void setStrBoxNum(String strBoxNum) {
		this.strBoxNum = strBoxNum;
	}
	/**
	 * @return the LoadStatus
	 */
	public String getStrLoadStatus() {
		return strLoadStatus;
	}
	public void setStrLoadStatus(String strLoadStatus) {
		this.strLoadStatus = strLoadStatus;
	}
	/**
	 * @return the FromDate
	 */
	public String getStrFromDate() {
		return strFromDate;
	}
	public void setStrFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}
	/**
	 * @return the ToDate
	 */
	public String getStrToDate() {
		return strToDate;
	}
	public void setStrToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	/**
	 * @return the strLoadId
	 */
	public String getStrLoadId() {
		return strLoadId;
	}
	/**
	 * @param strLoadId the strLoadId to set
	 */
	public void setStrLoadId(String strLoadId) {
		this.strLoadId = strLoadId;
	}
}
