package com.globus.operations.receiving.thbreceiving.beans;


import java.util.HashMap;



import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
/**
 * @author ASophia
 * */

public class GmPOBulkReceiveBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmPOBulkReceiveBean(GmDataStoreVO gmDataStoreVO)
			throws Exception {
		// TODO Auto-generated constructor stub
		
		super(gmDataStoreVO);
	}
	/**
	 * fetchStagingDash : This method is used to fetch the details to Stage Dashboard
	 * @param actionMapping
     * @param actionForm
	 * @param request
     * @param response
	 * @return
	 * @throws Exception
	 * */
	
	public String fetchStagingDash(HashMap hmParam) throws AppError{
		String strJSONDashData = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_bba_staging_dash.gm_fch_staging_dash", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CLOB);
		gmDBManager.execute();
		strJSONDashData = GmCommonClass.parseNull(gmDBManager.getString(1));
		gmDBManager.close();
		return strJSONDashData;
	}
}

