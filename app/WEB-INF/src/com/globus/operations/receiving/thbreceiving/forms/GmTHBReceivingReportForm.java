package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmTHBReceivingReportForm extends GmCommonForm {

	private String strXmlGridData="";

	public String getStrXmlGridData() {
		return strXmlGridData;
	}

	public void setStrXmlGridData(String strXmlGridData) {
		this.strXmlGridData = strXmlGridData;
	}
}