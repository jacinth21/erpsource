package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmTHBAnalysisListReportForm extends GmCommonForm{
	/**
	 * @author ssharmila
	 * 
	 */
	private String analysisId = "";
	private String loadId = "";
	private String status = "";
	private String procStartDt = "";
	private String procEndDt = "";
	private String strJSONGridData = "";
	private String hAnalysisIdStr = "";
	private String hLoadIdStr = "";
	private String hStatusStr = "";
	private String hLoadId = "";
	private String strScheduleAccess = "";
	private String hAnalysisId = "";
	ArrayList alStatList = new ArrayList();
	ArrayList alActionList = new ArrayList();
	/**
	 * @return the analysisId
	 */
	public String getAnalysisId() {
		return analysisId;
	}
	/**
	 * @param analysisId the analysisId to set
	 */
	public void setAnalysisId(String analysisId) {
		this.analysisId = analysisId;
	}
	/**
	 * @return the loadId
	 */
	public String getLoadId() {
		return loadId;
	}
	/**
	 * @param loadId the loadId to set
	 */
	public void setLoadId(String loadId) {
		this.loadId = loadId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the procStartDt
	 */
	public String getProcStartDt() {
		return procStartDt;
	}
	/**
	 * @param procStartDt the procStartDt to set
	 */
	public void setProcStartDt(String procStartDt) {
		this.procStartDt = procStartDt;
	}
	/**
	 * @return the procEndDt
	 */
	public String getProcEndDt() {
		return procEndDt;
	}
	/**
	 * @param procEndDt the procEndDt to set
	 */
	public void setProcEndDt(String procEndDt) {
		this.procEndDt = procEndDt;
	}
	/**
	 * @return the strJSONGridData
	 */
	public String getStrJSONGridData() {
		return strJSONGridData;
	}
	/**
	 * @param strJSONGridData the strJSONGridData to set
	 */
	public void setStrJSONGridData(String strJSONGridData) {
		this.strJSONGridData = strJSONGridData;
	}
	/**
	 * @return the hAnalysisIdStr
	 */
	public String gethAnalysisIdStr() {
		return hAnalysisIdStr;
	}
	/**
	 * @param hAnalysisIdStr the hAnalysisIdStr to set
	 */
	public void sethAnalysisIdStr(String hAnalysisIdStr) {
		this.hAnalysisIdStr = hAnalysisIdStr;
	}
	/**
	 * @return the hLoadIdStr
	 */
	public String gethLoadIdStr() {
		return hLoadIdStr;
	}
	/**
	 * @param hLoadIdStr the hLoadIdStr to set
	 */
	public void sethLoadIdStr(String hLoadIdStr) {
		this.hLoadIdStr = hLoadIdStr;
	}
	/**
	 * @return the hStatusStr
	 */
	public String gethStatusStr() {
		return hStatusStr;
	}
	/**
	 * @param hStatusStr the hStatusStr to set
	 */
	public void sethStatusStr(String hStatusStr) {
		this.hStatusStr = hStatusStr;
	}
	/**
	 * @return the hLoadId
	 */
	public String gethLoadId() {
		return hLoadId;
	}
	/**
	 * @param hLoadId the hLoadId to set
	 */
	public void sethLoadId(String hLoadId) {
		this.hLoadId = hLoadId;
	}
	/**
	 * @return the strScheduleAccess
	 */
	public String getStrScheduleAccess() {
		return strScheduleAccess;
	}
	/**
	 * @param strScheduleAccess the strScheduleAccess to set
	 */
	public void setStrScheduleAccess(String strScheduleAccess) {
		this.strScheduleAccess = strScheduleAccess;
	}
	/**
	 * @return the hAnalysisId
	 */
	public String gethAnalysisId() {
		return hAnalysisId;
	}
	/**
	 * @param hAnalysisId the hAnalysisId to set
	 */
	public void sethAnalysisId(String hAnalysisId) {
		this.hAnalysisId = hAnalysisId;
	}
	/**
	 * @return the alStatList
	 */
	public ArrayList getAlStatList() {
		return alStatList;
	}
	/**
	 * @param alStatList the alStatList to set
	 */
	public void setAlStatList(ArrayList alStatList) {
		this.alStatList = alStatList;
	}
	/**
	 * @return the alActionList
	 */
	public ArrayList getAlActionList() {
		return alActionList;
	}
	/**
	 * @param alActionList the alActionList to set
	 */
	public void setAlActionList(ArrayList alActionList) {
		this.alActionList = alActionList;
	}

	

}
