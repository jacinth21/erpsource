package com.globus.operations.receiving.thbreceiving.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.thbreceiving.beans.GmTHBReceivingTransBean;
import com.globus.operations.receiving.thbreceiving.forms.GmTHBReceivingTransForm;

/**
 * @author ssharmila
 * 
 */
public class GmTHBReceivingTransAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchTHBHeadLoadDetails :This method load the Jsp Page
	 * 
	 * @exception AppError
	 *                ,Exception
	 */
	public ActionForward fetchTHBHeadLoadDetails(ActionMapping actionMapping,
			ActionForm Form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmTHBReceivingTransForm gmTHBReceivingTransForm = (GmTHBReceivingTransForm) Form;
		gmTHBReceivingTransForm.loadSessionParameters(request);
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmHeaderValues = new HashMap();
		ArrayList alTHBHead = new ArrayList();
		hmParam = GmCommonClass.getHashMapFromForm(gmTHBReceivingTransForm);
		return actionMapping.findForward("gmTHBLoad");
	}

	/**
	 * fetchTHBLoadDetails :This method load the respective Header values for
	 * the Load number
	 * 
	 * @exception AppError
	 *                ,Exception
	 */
	public ActionForward fetchTHBLoadDetails(ActionMapping actionMapping,
			ActionForm Form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmTHBReceivingTransForm gmTHBReceivingTransForm = (GmTHBReceivingTransForm) Form;
		gmTHBReceivingTransForm.loadSessionParameters(request);
		GmTHBReceivingTransBean gmTHBReceivingTransBean = new GmTHBReceivingTransBean();
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmHeaderValues = new HashMap();
		ArrayList alReturn = new ArrayList();
		ArrayList alBoxDtls = new ArrayList();
		hmParam = GmCommonClass.getHashMapFromForm(gmTHBReceivingTransForm);
		hmReturn = gmTHBReceivingTransBean.fetchTHBHeadDetails(hmParam);
		hmHeaderValues = GmCommonClass.parseNullHashMap((HashMap) hmReturn
				.get("HEADERDATA"));
		alBoxDtls = GmCommonClass.parseNullArrayList((ArrayList) hmReturn
				.get("ALTHBDTL"));
		GmCommonClass.getFormFromHashMap(gmTHBReceivingTransForm,
				hmHeaderValues);
		gmTHBReceivingTransForm.setAlTHBHead(alBoxDtls);
		return actionMapping.findForward("gmTHBLoad");

	}

	/**
	 * fetchTHBLoadDetailsByBoxNum :This method load the Box Details and THB
	 * details
	 * 
	 * @exception AppError
	 *                ,Exception
	 */
	public ActionForward fetchTHBLoadDetailsByBoxNum(
			ActionMapping actionMapping, ActionForm Form,
			HttpServletRequest request, HttpServletResponse response)
			throws AppError, Exception {
		instantiate(request, response);
		GmTHBReceivingTransForm gmTHBReceivingTransForm = (GmTHBReceivingTransForm) Form;
		gmTHBReceivingTransForm.loadSessionParameters(request);
		GmTHBReceivingTransBean gmTHBReceivingTransBean = new GmTHBReceivingTransBean();
		HashMap hmBoxReturn = new HashMap();
		HashMap hmHeaderValues = new HashMap();
		HashMap hmParam = GmCommonClass
				.getHashMapFromForm(gmTHBReceivingTransForm);
		String strSessCompanyLocale = GmCommonClass.parseNull((String) session
				.getAttribute("strSessCompanyLocale"));
		String strXmlGridData = "";
		hmBoxReturn = gmTHBReceivingTransBean.fetchTHBLoadDetailsByBoxNum(hmParam);
		HashMap hmTotValues = GmCommonClass.parseNullHashMap((HashMap) hmBoxReturn
				.get("HMTOTVALUES"));
		String strTotAllo = GmCommonClass.parseNull((String) hmTotValues.get("STRTOTALLO"));
		String strScanAllo = GmCommonClass.parseNull((String) hmTotValues.get("STRSCANALLO"));
		String strStatus = GmCommonClass.parseNull((String) hmBoxReturn.get("STATUS"));
		gmTHBReceivingTransForm.setStrStatus(strStatus);
		GmCommonClass.getFormFromHashMap(gmTHBReceivingTransForm, hmTotValues);
		hmBoxReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
		strXmlGridData = generateOutPut(hmBoxReturn, "GmTHBReceivingTrans.vm");
		gmTHBReceivingTransForm.setStrGridXmlData(strXmlGridData);
		response.setContentType("text/xml");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter pw = response.getWriter();
		pw.write(strTotAllo+"||"+strScanAllo+"||"+strStatus+"||"+strXmlGridData);
		pw.flush();
		return null;
	}

	/**
	 * fetchTHBLoadDetailsfrmDash :This method load the Page with all the
	 * details fetched into it.
	 * 
	 * @exception AppError
	 *                ,Exception
	 */
	public ActionForward fetchTHBLoadDetailsfrmDash(
			ActionMapping actionMapping, ActionForm Form,
			HttpServletRequest request, HttpServletResponse response)
			throws AppError, Exception {

		instantiate(request, response);
		GmTHBReceivingTransForm gmTHBReceivingTransForm = (GmTHBReceivingTransForm) Form;
		gmTHBReceivingTransForm.loadSessionParameters(request);
		GmTHBReceivingTransBean gmTHBReceivingTransBean = new GmTHBReceivingTransBean();
		HashMap hmReturn = new HashMap();
		ArrayList alBoxDtls = new ArrayList();
		String strfrom = "";String strStatus = "";
		HashMap hmParam = GmCommonClass
				.getHashMapFromForm(gmTHBReceivingTransForm);
		String strSessCompanyLocale = GmCommonClass.parseNull((String) session
				.getAttribute("strSessCompanyLocale"));
		String strXmlGridData = "";
		strfrom  = GmCommonClass.parseNull((String)hmParam.get("STRFROM"));
		strfrom = strfrom.equals("") ? "dash" : strfrom;
		hmReturn = gmTHBReceivingTransBean.fetchTHBLoadDetailsfrmDash(hmParam);
		alBoxDtls = GmCommonClass.parseNullArrayList((ArrayList) hmReturn
				.get("BOXLIST"));
		strStatus = GmCommonClass.parseNull((String) hmReturn.get("STATUS"));
		gmTHBReceivingTransForm.setStrStatus(strStatus);
		gmTHBReceivingTransForm.setAlTHBHead(alBoxDtls);
		GmCommonClass.getFormFromHashMap(gmTHBReceivingTransForm, GmCommonClass
				.parseNullHashMap((HashMap) hmReturn.get("HMTOTVALUES")));
		GmCommonClass.getFormFromHashMap(gmTHBReceivingTransForm, GmCommonClass
				.parseNullHashMap((HashMap) hmReturn.get("HEADERDATA")));
		hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
		strXmlGridData = generateOutPut(hmReturn, "GmTHBReceivingTrans.vm");
		gmTHBReceivingTransForm.setStrGridXmlData(strXmlGridData);
		gmTHBReceivingTransForm.setStrFrom(strfrom);
		return actionMapping.findForward("gmTHBLoad");
	}

	/**
	 * saveTHBScannedGrafts :This method is used to update and save the scanned
	 * grafts
	 * 
	 * @exception AppError
	 *                ,Exception
	 */
	public ActionForward saveTHBScannedGrafts(ActionMapping actionMapping,
			ActionForm Form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmTHBReceivingTransForm gmTHBReceivingTransForm = (GmTHBReceivingTransForm) Form;
		gmTHBReceivingTransForm.loadSessionParameters(request);
		GmTHBReceivingTransBean gmTHBReceivingTransBean = new GmTHBReceivingTransBean();
		String strTotAllo ="";
		String strScanAllo = "";
		HashMap hmTotValues = new HashMap();
		HashMap hmGridValue = new HashMap();
		ArrayList alBoxDtls = new ArrayList();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) session
				.getAttribute("strSessCompanyLocale"));
		String strXmlGridData = "";
		HashMap hmParam = GmCommonClass
				.getHashMapFromForm(gmTHBReceivingTransForm);
		hmTotValues = gmTHBReceivingTransBean.saveTHBScannedGrafts(hmParam);
		hmGridValue = GmCommonClass.parseNullHashMap((HashMap) hmTotValues.get("HMTOTVALUES"));
		strTotAllo = GmCommonClass.parseNull((String) hmGridValue.get("STRTOTALLO"));
		strScanAllo = GmCommonClass.parseNull((String) hmGridValue.get("STRSCANALLO"));
		PrintWriter pw = response.getWriter();
		pw.write(strTotAllo+"||"+strScanAllo);
		pw.flush();
		return null;
	
	}

	/**
	 * UpdTHBScannedGrafts :This method is used to update status in the Master
	 * Table.
	 * 
	 * @exception AppError
	 *                ,Exception
	 */
	public ActionForward updTHBScannedGrafts(ActionMapping actionMapping,
			ActionForm Form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmTHBReceivingTransForm gmTHBReceivingTransForm = (GmTHBReceivingTransForm) Form;
		gmTHBReceivingTransForm.loadSessionParameters(request);
		GmTHBReceivingTransBean gmTHBReceivingTransBean = new GmTHBReceivingTransBean();
		HashMap hmReturn = new HashMap();
		HashMap hmParam = GmCommonClass
				.getHashMapFromForm(gmTHBReceivingTransForm);
		gmTHBReceivingTransBean.updateTHBScannedGrafts(hmParam);
		return actionMapping.findForward("gmTHBScanLoad");
	}

	/**
	 * Generate out put.
	 * 
	 * @param hmReturn
	 *            ,vmFile
	 * @return the string
	 * @throws AppError
	 *             the app error
	 */
	private String generateOutPut(HashMap hmReturn, String vmFile)
			throws AppError {
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) hmReturn
				.get("STRSESSCOMPANYLOCALE"));
		templateUtil.setDataMap("hmReturn", hmReturn);
		templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
				"properties.labels.operations.receiving.thbreceiving.GmTHBReceivingTrans",
				strSessCompanyLocale));
		templateUtil.setTemplateSubDir("operations/receiving/thbreceiving/templates");
		templateUtil.setTemplateName(vmFile);
		return templateUtil.generateOutput();
	}
	/**
	 * rollbackTHBBox - This method used to update the load box status as Initiated
	 * @param actionMapping
	 * @param Form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward rollbackTHBBox(ActionMapping actionMapping, ActionForm Form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmTHBReceivingTransForm gmTHBReceivingTransForm = (GmTHBReceivingTransForm) Form;
		gmTHBReceivingTransForm.loadSessionParameters(request);
		GmTHBReceivingTransBean gmTHBReceivingTransBean = new GmTHBReceivingTransBean();
		HashMap hmParam = new HashMap();
		String strJsonResponse = "";
		hmParam = GmCommonClass.getHashMapFromForm(gmTHBReceivingTransForm);
		gmTHBReceivingTransBean.rollbackTHBBox(hmParam);
		return null;
	}
}
