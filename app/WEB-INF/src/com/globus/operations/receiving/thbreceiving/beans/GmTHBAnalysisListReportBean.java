package com.globus.operations.receiving.thbreceiving.beans;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmTHBAnalysisListReportBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public GmTHBAnalysisListReportBean(GmDataStoreVO gmDataStoreVO)
			throws Exception {
		// TODO Auto-generated constructor stub
		super(gmDataStoreVO);
	}
	/**
	 * fetchListReportDetails : This method is used to fetch the information to
	 * THB Analysis List report screen
	 */
	public String fetchListReportDetails(HashMap hmParam) throws AppError,Exception {
		String strJSONListData = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		String strAnalId = GmCommonClass.parseNull((String) hmParam
				.get("ANALYSISID"));
		String strLoadId = GmCommonClass.parseNull((String) hmParam
				.get("LOADID"));
		String strStatus = GmCommonClass.parseNull((String) hmParam
				.get("STATUS"));
		strStatus = strStatus.equals("0")?"":strStatus;
		String strProcStartDt = GmCommonClass.parseNull((String) hmParam
				.get("PROCSTARTDT"));
		String strProcEndDt = GmCommonClass.parseNull((String) hmParam
				.get("PROCENDDT"));
		String strApplDateFmt = getGmDataStoreVO().getCmpdfmt(); 	
		Date  dtProcStartDt = GmCommonClass.getStringToDate(strProcStartDt, strApplDateFmt);
		Date  dtProcEndDt   = GmCommonClass.getStringToDate(strProcEndDt, strApplDateFmt);
		gmDBManager.setPrepareString(
				"gm_pkg_op_thb_analysis_rpt.gm_fch_analysis_list_rpt", 6);
		gmDBManager.setString(1, strAnalId);
		gmDBManager.setString(2, strLoadId);
		gmDBManager.setString(3, strStatus);
		gmDBManager.setDate(4, dtProcStartDt);
		gmDBManager.setDate(5, dtProcEndDt);
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.execute();
		strJSONListData = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();
		return strJSONListData;
	}
	
}
