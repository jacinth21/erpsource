package com.globus.operations.receiving.thbreceiving.actions;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import com.globus.operations.receiving.thbreceiving.beans.GmPOBulkReceiveBean;
import com.globus.operations.receiving.thbreceiving.forms.GmBBAStagingDashForm;

/**
 * @author ASophia
 *
 */
public class GmReceiveShipDashAction extends GmDispatchAction
{

	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	 * fetchStagingDashboard : This method is used to fetch the information  to Stage DashBoard
	 *
	 * @param actionMapping
     * @param actionForm
	 * @param request
     * @param response
	 * @return
	 * @throws Exception
	 * */
	
	
	public ActionForward fetchStagingDashboard(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)throws Exception{
		instantiate(request,response);
		GmBBAStagingDashForm gmBBAStagingDashForm = (GmBBAStagingDashForm) actionForm;
		gmBBAStagingDashForm.loadSessionParameters(request);
		GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		String strJsonString = "";
		hmParam = GmCommonClass.getHashMapFromForm(gmBBAStagingDashForm);
		strJsonString = GmCommonClass.parseNull(gmPOBulkReceiveBean.fetchStagingDash(hmParam));
		gmBBAStagingDashForm.setStrXmlGridData(strJsonString);
		return actionMapping.findForward("gmBBAStagingDashBoard");
	}		
}
