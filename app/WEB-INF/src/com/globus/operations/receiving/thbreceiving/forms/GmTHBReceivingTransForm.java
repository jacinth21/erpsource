package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

public class GmTHBReceivingTransForm extends GmCommonForm{

	private String strLoadNo= "" ;
	private String strBoxNo = "";
	private String strSyncBy = "";
	private String strSyncDt ;
	private String strGridXmlData = "";
	private String strCtrlNo = "";
	private String strTotAllo = "";
	private String strScanAllo = "";
	private String strLoadId = "";
	private String strLoadDtlId = "";
	ArrayList alTHBHead = new ArrayList();
	private String strFrom = "";
	private String strStatus = "";
	
	
	

	public String getStrStatus() {
		return strStatus;
	}
	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}
	public String getStrFrom() {
		return strFrom;
	}
	public void setStrFrom(String strFrom) {
		this.strFrom = strFrom;
	}
	/**
	 * @return the strLoadDtlId
	 */
	public String getStrLoadDtlId() {
		return strLoadDtlId;
	}
	/**
	 * @param strLoadDtlId the strLoadDtlId to set
	 */
	public void setStrLoadDtlId(String strLoadDtlId) {
		this.strLoadDtlId = strLoadDtlId;
	}
	/**
	 * @return the strLoadId
	 */
	public String getStrLoadId() {
		return strLoadId;
	}
	/**
	 * @param strLoadId the strLoadId to set
	 */
	public void setStrLoadId(String strLoadId) {
		this.strLoadId = strLoadId;
	}
	/**
	 * @return the strBoxNo
	 */
	public String getStrBoxNo() {
		return strBoxNo;
	}
	/**
	 * @param strBoxNo the strBoxNo to set
	 */
	public void setStrBoxNo(String strBoxNo) {
		this.strBoxNo = strBoxNo;
	}
	/**
	 * @return the strSyncBy
	 */
	public String getStrSyncBy() {
		return strSyncBy;
	}
	/**
	 * @param strSyncBy the strSyncBy to set
	 */
	public void setStrSyncBy(String strSyncBy) {
		this.strSyncBy = strSyncBy;
	}
	
	/**
	 * @return the strGridXmlData
	 */
	public String getStrGridXmlData() {
		return strGridXmlData;
	}
	/**
	 * @param strGridXmlData the strGridXmlData to set
	 */
	public void setStrGridXmlData(String strGridXmlData) {
		this.strGridXmlData = strGridXmlData;
	}
	/**
	 * @return the alTHBHead
	 */
	public ArrayList getAlTHBHead() {
		return alTHBHead;
	}
	/**
	 * @param alTHBHead the alTHBHead to set
	 */
	public void setAlTHBHead(ArrayList alTHBHead) {
		this.alTHBHead = alTHBHead;
	}

	
	/**
	 * @return the strLoadNo
	 */
	public String getStrLoadNo() {
		return strLoadNo;
	}
	/**
	 * @param strLoadNo the strLoadNo to set
	 */
	public void setStrLoadNo(String strLoadNo) {
		this.strLoadNo = strLoadNo;
	}
	/**
	 * @return the strSyncDt
	 */
	public String getStrSyncDt() {
		return strSyncDt;
	}
	/**
	 * @param strSyncDt the strSyncDt to set
	 */
	public void setStrSyncDt(String strSyncDt) {
		this.strSyncDt = strSyncDt;
	}
	/**
	 * @return the strCtrlNo
	 */
	public String getStrCtrlNo() {
		return strCtrlNo;
	}
	/**
	 * @param strCtrlNo the strCtrlNo to set
	 */
	public void setStrCtrlNo(String strCtrlNo) {
		this.strCtrlNo = strCtrlNo;
	}
	/**
	 * @return the strTotAllo
	 */
	public String getStrTotAllo() {
		return strTotAllo;
	}
	/**
	 * @param strTotAllo the strTotAllo to set
	 */
	public void setStrTotAllo(String strTotAllo) {
		this.strTotAllo = strTotAllo;
	}
	/**
	 * @return the strScanAllo
	 */
	public String getStrScanAllo() {
		return strScanAllo;
	}
	/**
	 * @param strScanAllo the strScanAllo to set
	 */
	public void setStrScanAllo(String strScanAllo) {
		this.strScanAllo = strScanAllo;
	}
	

}
