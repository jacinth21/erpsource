package com.globus.operations.receiving.thbreceiving.beans;

import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmTHBAnalysisDetailsBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public GmTHBAnalysisDetailsBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * fetchTHBAnalysisDetails - this Method to fetch the THb Analysis Details
	 * based on analysis Id
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 * @throws Exception 
	 */
	public String fetchTHBAnalysisDetails(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strJSONAnalysisDtls = "";
		String strJSONAnalysisDtlsArr ="";
		String strAnalysisId = GmCommonClass.parseNull((String) hmParam.get("ANALYSISID"));
		gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_fch_analysis_details",2);
		gmDBManager.setString(1, strAnalysisId); 
		gmDBManager.registerOutParameter(2,OracleTypes.CLOB);
		gmDBManager.execute(); 
		strJSONAnalysisDtls =GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		//convert JSON String Object To JSON String array
		strJSONAnalysisDtlsArr = "[" + strJSONAnalysisDtls+ "]";
		return strJSONAnalysisDtlsArr;
	}
	/**
	 * saveAnalysisStatus - Updates the Status 
	 * @param hmParam
	 * author Shiny
	 * @return
	 * @throws AppError
	 * @throws Exception 
	 */
	public void saveAnalysisStatus(HashMap hmParam) throws AppError {
		
	    HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strAnalysisId = GmCommonClass.parseNull((String) hmParam.get("ANALYSISID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_sav_analysis_status",2);
		gmDBManager.setString(1, strAnalysisId); 
		gmDBManager.setInt(2, Integer.parseInt(strUserId)); 
		gmDBManager.execute(); 
		gmDBManager.commit();
	}
	
	/**fetchAnalysisCompletion  - this function used to enable/ disable the complete analysis button based On return JSON Pending Qty 
	 * If JSON QTY Greater/Lesser  Than 0 button should not be displayed 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchAnalysisCompletion(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strAnalysisId = GmCommonClass.parseNull((String) hmParam.get("ANALYSISID"));
		String strQtyJsonString ="";
		gmDBManager.setPrepareString("gm_pkg_op_thb_detail_rpt.gm_fch_analysis_completion_status",2);
		gmDBManager.setString(1, strAnalysisId); 
		gmDBManager.registerOutParameter(2,OracleTypes.CLOB);
		gmDBManager.execute(); 
		strQtyJsonString =GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strQtyJsonString;
	}
	
}
