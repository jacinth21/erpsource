package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;
import com.globus.common.forms.GmCommonForm;

public class GmDonorScheduleForm extends GmCommonForm {

  private String strScheduleGridData = "";
  private String strScheduleDtGridData = "";
  private String strStageDt = "";
  private String strLoadNum = "";
  private String strDonorNum = "";
  private String strAnalyId = "";
  private String strPoNum = "";
  ArrayList alActionList = new ArrayList();


  /**
   * @return the strScheduleGridData
   */
  public String getStrScheduleGridData() {
    return strScheduleGridData;
  }

  /**
   * @param strScheduleGridData the strScheduleGridData to set
   */
  public void setStrScheduleGridData(String strScheduleGridData) {
    this.strScheduleGridData = strScheduleGridData;
  }

  /**
   * @return the strScheduleDtGridData
   */
  public String getStrScheduleDtGridData() {
    return strScheduleDtGridData;
  }

  /**
   * @param strScheduleDtGridData the strScheduleDtGridData to set
   */
  public void setStrScheduleDtGridData(String strScheduleDtGridData) {
    this.strScheduleDtGridData = strScheduleDtGridData;
  }

  /**
   * @return the strStageDt
   */
  public String getStrStageDt() {
    return strStageDt;
  }

  /**
   * @param strStageDt the strStageDt to set
   */
  public void setStrStageDt(String strStageDt) {
    this.strStageDt = strStageDt;
  }

  /**
   * @return the strLoadNum
   */
  public String getStrLoadNum() {
    return strLoadNum;
  }

  /**
   * @param strLoadNum the strLoadNum to set
   */
  public void setStrLoadNum(String strLoadNum) {
    this.strLoadNum = strLoadNum;
  }

  /**
   * @return the strDonorNum
   */
  public String getStrDonorNum() {
    return strDonorNum;
  }

  /**
   * @param strDonorNum the strDonorNum to set
   */
  public void setStrDonorNum(String strDonorNum) {
    this.strDonorNum = strDonorNum;
  }

  /**
   * @return the strAnalyId
   */
  public String getStrAnalyId() {
    return strAnalyId;
  }

  /**
   * @param strAnalyId the strAnalyId to set
   */
  public void setStrAnalyId(String strAnalyId) {
    this.strAnalyId = strAnalyId;
  }

  /**
   * @return the strPoNum
   */  
	public String getStrPoNum() {
		return strPoNum;
	}
	
	/**
	   * @param strPoNum the strPoNum to set
	*/
	public void setStrPoNum(String strPoNum) {
		this.strPoNum = strPoNum;
	}
	
	  /**
	   * @return the alActionList
	   */
	public ArrayList getAlActionList() {
		return alActionList;
	}
	
	/**
	   * @param alActionList the alActionList to set
	   */
	public void setAlActionList(ArrayList alActionList) {
		this.alActionList = alActionList;
	}

}
