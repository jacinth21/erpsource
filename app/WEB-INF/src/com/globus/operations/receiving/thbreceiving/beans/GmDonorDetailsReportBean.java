package com.globus.operations.receiving.thbreceiving.beans;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author tramasamy
 * 
 */
public class GmDonorDetailsReportBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public GmDonorDetailsReportBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * fetchDonorDtls - this method used to Fetch the donor details based on
	 * load number , donor number, processed date , base part
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchDonorDtls(HashMap hmParam) throws AppError,Exception {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		String strLoadNum = GmCommonClass.parseNull((String) hmParam
				.get("LOADNUM"));
		String strDonorNum = GmCommonClass.parseNull((String) hmParam
				.get("DONORNUM"));
		String strProcessedDt = GmCommonClass.parseNull((String) hmParam
				.get("PROCESSEDDATE"));
		String strBasePart = GmCommonClass.parseNull((String) hmParam
				.get("BASEPART"));
		String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
		Date  dtProcessedDt = (Date)GmCommonClass.getStringToDate(strProcessedDt, strApplDateFmt);
		String strDonorDtlsJSONString = "";
		log.debug("{{hmParam}}"+hmParam);
		gmDBManager.setPrepareString(
		 "gm_pkg_op_thb_analysis_rpt.gm_fch_donor_load_details", 5);
		 gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
		 gmDBManager.setString(1, strLoadNum);
	     gmDBManager.setString(2, strDonorNum);
	     gmDBManager.setString(3, strBasePart);
	     gmDBManager.setDate(4, dtProcessedDt);
		 gmDBManager.execute();
		 strDonorDtlsJSONString = GmCommonClass.parseNull(gmDBManager.getString(5));
		 log.debug("{{strDonorDtlsJSONString}}"+strDonorDtlsJSONString);
		  gmDBManager.close();
		return strDonorDtlsJSONString;
	}
}
