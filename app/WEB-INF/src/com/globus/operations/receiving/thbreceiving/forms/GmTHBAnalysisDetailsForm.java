package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmTHBAnalysisDetailsForm extends GmCommonForm {

	private String analysisId = "";
	private String basePart = "";
	private String privatePart = "";
	private String columnFilter = "";

	private String hBasePartStr = "";
	private String hPrivatePartStr = "";
	private String hColumnFilterStr = "";
	private String strJSONGridData = "";

	private ArrayList alComparisonFilter = new ArrayList();
	private ArrayList alPartQty = new ArrayList();
	private String comparType = "";
	private String partType = "";
	private String qty = "";
	private String pendingQty = "";
	private String status ="";
	private String completeBtnAccess ="";

	/**
	 * @return the analysisId
	 */
	public String getAnalysisId() {
		return analysisId;
	}

	/**
	 * @param analysisId
	 *            the analysisId to set
	 */
	public void setAnalysisId(String analysisId) {
		this.analysisId = analysisId;
	}

	/**
	 * @return the basePart
	 */
	public String getBasePart() {
		return basePart;
	}

	/**
	 * @param basePart
	 *            the basePart to set
	 */
	public void setBasePart(String basePart) {
		this.basePart = basePart;
	}

	/**
	 * @return the privatePart
	 */
	public String getPrivatePart() {
		return privatePart;
	}

	/**
	 * @param privatePart
	 *            the privatePart to set
	 */
	public void setPrivatePart(String privatePart) {
		this.privatePart = privatePart;
	}

	/**
	 * @return the columnFilter
	 */
	public String getColumnFilter() {
		return columnFilter;
	}

	/**
	 * @param columnFilter
	 *            the columnFilter to set
	 */
	public void setColumnFilter(String columnFilter) {
		this.columnFilter = columnFilter;
	}

	/**
	 * @return the hBasePartStr
	 */
	public String gethBasePartStr() {
		return hBasePartStr;
	}

	/**
	 * @param hBasePartStr
	 *            the hBasePartStr to set
	 */
	public void sethBasePartStr(String hBasePartStr) {
		this.hBasePartStr = hBasePartStr;
	}

	/**
	 * @return the hPrivatePartStr
	 */
	public String gethPrivatePartStr() {
		return hPrivatePartStr;
	}

	/**
	 * @param hPrivatePartStr
	 *            the hPrivatePartStr to set
	 */
	public void sethPrivatePartStr(String hPrivatePartStr) {
		this.hPrivatePartStr = hPrivatePartStr;
	}

	/**
	 * @return the hColumnFilterStr
	 */
	public String gethColumnFilterStr() {
		return hColumnFilterStr;
	}

	/**
	 * @param hColumnFilterStr
	 *            the hColumnFilterStr to set
	 */
	public void sethColumnFilterStr(String hColumnFilterStr) {
		this.hColumnFilterStr = hColumnFilterStr;
	}

	/**
	 * @return the strJSONGridData
	 */
	public String getStrJSONGridData() {
		return strJSONGridData;
	}

	/**
	 * @param strJSONGridData
	 *            the strJSONGridData to set
	 */
	public void setStrJSONGridData(String strJSONGridData) {
		this.strJSONGridData = strJSONGridData;
	}

	/**
	 * @return the alComparisonFilter
	 */
	public ArrayList getAlComparisonFilter() {
		return alComparisonFilter;
	}

	/**
	 * @param alComparisonFilter
	 *            the alComparisonFilter to set
	 */
	public void setAlComparisonFilter(ArrayList alComparisonFilter) {
		this.alComparisonFilter = alComparisonFilter;
	}

	/**
	 * @return the alPartQty
	 */
	public ArrayList getAlPartQty() {
		return alPartQty;
	}

	/**
	 * @param alPartQty
	 *            the alPartQty to set
	 */
	public void setAlPartQty(ArrayList alPartQty) {
		this.alPartQty = alPartQty;
	}

	/**
	 * @return the comparType
	 */
	public String getComparType() {
		return comparType;
	}

	/**
	 * @param comparType
	 *            the comparType to set
	 */
	public void setComparType(String comparType) {
		this.comparType = comparType;
	}

	/**
	 * @return the partType
	 */
	public String getPartType() {
		return partType;
	}

	/**
	 * @param partType
	 *            the partType to set
	 */
	public void setPartType(String partType) {
		this.partType = partType;
	}

	/**
	 * @return the qty
	 */
	public String getQty() {
		return qty;
	}

	/**
	 * @param qty
	 *            the qty to set
	 */
	public void setQty(String qty) {
		this.qty = qty;
	}

	/**
	 * @return the pendingQty
	 */
	public String getPendingQty() {
		return pendingQty;
	}

	/**
	 * @param pendingQty the pendingQty to set
	 */
	public void setPendingQty(String pendingQty) {
		this.pendingQty = pendingQty;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the completeBtnAccess
	 */
	public String getCompleteBtnAccess() {
		return completeBtnAccess;
	}

	/**
	 * @param completeBtnAccess the completeBtnAccess to set
	 */
	public void setCompleteBtnAccess(String completeBtnAccess) {
		this.completeBtnAccess = completeBtnAccess;
	}

	
}
