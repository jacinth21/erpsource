package com.globus.operations.receiving.thbreceiving.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.actions.GmAction;
import com.globus.operations.receiving.thbreceiving.beans.GmInventoryLookUpDetailsBean;
import com.globus.operations.receiving.thbreceiving.forms.GmInventoryLookUpDetailsForm;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.util.Map;

public class GmInventoryLookUpDetailsAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchInventoryLookUpDetails : This method is used to fetch the info to Backorder/PTRD details
	 * the screen
	 */

	public ActionForward fetchInventoryLookUpDetails(
			ActionMapping actionMapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		instantiate(request, response);
		GmInventoryLookUpDetailsForm gmInventoryLookUpDetailsForm = (GmInventoryLookUpDetailsForm) actionForm;
		gmInventoryLookUpDetailsForm.loadSessionParameters(request);
		String strfetchJsonString = "";
		String strJSONGridData = "";
		HashMap hmParam = new HashMap();
		GmInventoryLookUpDetailsBean gmInventoryLookUpDetailsBean = new GmInventoryLookUpDetailsBean(getGmDataStoreVO());
		hmParam = GmCommonClass.getHashMapFromForm(gmInventoryLookUpDetailsForm);
		log.debug("fetchInventoryLookUpDetails-action  "+hmParam);
		strfetchJsonString = GmCommonClass.parseNull(gmInventoryLookUpDetailsBean.fetchInventoryLookUpDetails(hmParam));
		gmInventoryLookUpDetailsForm.setStrJSONGridData(strfetchJsonString);
		  //JSON data from bean
		 strJSONGridData =  GmCommonClass.parseNull(gmInventoryLookUpDetailsBean.fetchInventoryLookUpDetails(hmParam));
		 gmInventoryLookUpDetailsForm.setStrJSONGridData(strJSONGridData);
		 return actionMapping.findForward("gmInventoryLookUpDetails");
	}
}