package com.globus.operations.receiving.thbreceiving.beans;

import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ssharmila
 * 
 */
public class GmTHBPendAnalysisDashBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());



  public GmTHBPendAnalysisDashBean(GmDataStoreVO gmDataStoreVO) throws Exception {
    // TODO Auto-generated constructor stub
    super(gmDataStoreVO);
  }

  /**
   * loadTHBPendAnalysisDashBoard : This method is used to fetch the information to THB Load Pending
   * Analysis Dashboard screen
   */
  public String loadTHBPendAnalysisDashBoard(HashMap hmParam) throws AppError {
    String strJSONDashData = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_fch_pend_analysis_dash", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CLOB);
    gmDBManager.execute();
    strJSONDashData = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strJSONDashData;
  }

  /**
   * saveLockAndGenerate : This method is used to save Lock the Details in inventory for Analysis
   */

  public void saveLockAndGenerate(HashMap hmParam) throws AppError, Exception {
    log.debug("saveLockAndGenerate>>hmParam" + hmParam);
    HashMap hmReturn = new HashMap();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO());
    String strloadnumstr = GmCommonClass.parseNull((String) hmParam.get("STRLOADNUMSTR"));
    String fromdate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String todate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String struserid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strScreen = GmCommonClass.parseNull((String) hmParam.get("STRSCREEN"));
    String strAnalysisid = GmCommonClass.parseNull((String) hmParam.get("STRANALYSISID"));
    String strCompInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
    if (!strCompInfo.equals("")) {
      gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompInfo);
    }
    String strCompanyId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
    String strPlantId = GmCommonClass.parseNull(gmDataStoreVO.getPlantid());
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    Date dtfromdate = GmCommonClass.getStringToDate(fromdate, strApplDateFmt);
    Date dttodate = GmCommonClass.getStringToDate(todate, strApplDateFmt);

    gmDBManager.setPrepareString("gm_pkg_op_thb_lock_bck_ord_txn.gm_thb_load_lock_master_main", 6);
    gmDBManager.setString(1, strloadnumstr);
    gmDBManager.setDate(2, dtfromdate);
    gmDBManager.setDate(3, dttodate);
    gmDBManager.setInt(4, Integer.parseInt(struserid));
    gmDBManager.setInt(5, Integer.parseInt(strCompanyId));
    gmDBManager.setInt(6, Integer.parseInt(strPlantId));

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * This procedure is used to update the Status to indicate the process in progress
   */


  public void updLockStatus(String strloadnumstr, String struserid, String strStatus)
      throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_upd_lock_status", 3);
    gmDBManager.setString(1, strloadnumstr);
    gmDBManager.setString(2, struserid);
    gmDBManager.setString(3, strStatus);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * updLockAndGenerateDate : This method is used to Update Locked Processing dates from List report
   * screen
   */

  public void updLockAndGenerateDate(HashMap hmParam) throws AppError, Exception {
    log.debug("updLockAndGenerateDate>>hmParam" + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strAnalysisid = GmCommonClass.parseNull((String) hmParam.get("STRANALYSISID"));
    String fromdate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String todate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String struserid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    Date dtfromdate = GmCommonClass.getStringToDate(fromdate, strApplDateFmt);
    Date dttodate = GmCommonClass.getStringToDate(todate, strApplDateFmt);

    gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_update_lock_date", 4);
    gmDBManager.setString(1, strAnalysisid);
    gmDBManager.setDate(2, dtfromdate);
    gmDBManager.setDate(3, dttodate);
    gmDBManager.setInt(4, Integer.parseInt(struserid));

    gmDBManager.execute();
    gmDBManager.commit();
  }
}
