package com.globus.operations.receiving.thbreceiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ssharmila
 * 
 */
public class GmTHBReceivingTransBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmCommonClass gmCommon = new GmCommonClass();

	public GmTHBReceivingTransBean() throws AppError {
		super(GmCommonClass.getDefaultGmDataStoreVO());
		// TODO Auto-generated constructor stub
	}

	  /**
	   * fetchTHBHeadDetails - This Method is written to fetch the Header 
	   * values for Load Number
	   * 
	   * @author ssharmila
	   * 
	   * */
	public HashMap fetchTHBHeadDetails(HashMap hmParam) throws AppError {

		HashMap hmReturn = new HashMap();
		HashMap hmHeaderValues = new HashMap();
		ArrayList alTBHHeader = new ArrayList();
		ArrayList alTBHDetail = new ArrayList();
		String intLoadId = GmCommonClass.parseNull((String) hmParam
				.get("STRLOADNO"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString(
				"gm_pkg_op_thb_load_report.gm_fch_thb_head_details", 3);
		gmDBManager.setString(1, intLoadId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmHeaderValues = gmDBManager.returnHashMap((ResultSet) gmDBManager
				.getObject(2));
		alTBHDetail = gmDBManager.returnArrayList((ResultSet) gmDBManager
				.getObject(3));
		hmReturn.put("HEADERDATA", hmHeaderValues);
		hmReturn.put("ALTHBDTL", alTBHDetail);
		gmDBManager.close();
		return hmReturn;

	}

	  /**
	   * fetchTHBLoadDetailsByBoxNum - This Method is written to load the Box 
	   * Details and THB Details
	   * 
	   * @author ssharmila
	   * 
	   * */
	public HashMap fetchTHBLoadDetailsByBoxNum(HashMap hmParam) throws AppError {

		HashMap hmReturn = new HashMap();
		ArrayList alReturn = new ArrayList();
		HashMap hmTotValues = new HashMap();
		String strStatus = "";
		String strLoadno = GmCommonClass.parseNull((String) hmParam
				.get("STRLOADNO"));
		String strLoadId = GmCommonClass.parseNull((String) hmParam
				.get("STRLOADID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString(
				"gm_pkg_op_thb_load_report.gm_fch_thb_load_box_details", 4);
		gmDBManager.setString(1, strLoadId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager
				.getObject(2));
		hmTotValues = gmDBManager.returnHashMap((ResultSet) gmDBManager
				.getObject(3));
		strStatus = gmDBManager.getString(4);
		hmReturn.put("STATUS", strStatus);
		hmReturn.put("LOTDETAILS", alReturn);
		hmReturn.put("HMTOTVALUES", hmTotValues);
		gmDBManager.close();
		return hmReturn;

	}

	/**
	   * fetchTHBLoadDetailsfrmDash - This Method is written to load the Page with all the
	   * details fetched into it.
	   * 
	   * @author ssharmila
	   * 
	   * */
	public HashMap fetchTHBLoadDetailsfrmDash(HashMap hmParam) throws AppError {
		HashMap hmReturn = new HashMap();
		ArrayList alReturn = new ArrayList();
		ArrayList alBoxList = new ArrayList();
		HashMap hmTotValues = new HashMap();
		HashMap hmHeaderValues = new HashMap();
		String strStatus = "";
		String strLoadId = GmCommonClass.parseNull((String) hmParam
				.get("STRLOADID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString(
				"gm_pkg_op_thb_load_report.gm_fch_thb_details_frm_dash", 6);
		gmDBManager.setString(1, strLoadId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
		gmDBManager.execute();
		hmHeaderValues = gmDBManager.returnHashMap((ResultSet) gmDBManager
				.getObject(2));
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager
				.getObject(3));
		hmTotValues = gmDBManager.returnHashMap((ResultSet) gmDBManager
				.getObject(4));
		alBoxList = gmDBManager.returnArrayList((ResultSet) gmDBManager
				.getObject(5));
		strStatus = gmDBManager.getString(6);
		hmReturn.put("STATUS", strStatus);
		hmReturn.put("HEADERDATA", hmHeaderValues);
		hmReturn.put("LOTDETAILS", alReturn);
		hmReturn.put("HMTOTVALUES", hmTotValues);
		hmReturn.put("BOXLIST", alBoxList);
		gmDBManager.close();
		return hmReturn;

	}
	/**
	   * saveTHBScannedGrafts - This Method is written to update and save the scanned
	   * grafts
	   * 
	   * @author ssharmila
	   * 
	   * */
	public HashMap saveTHBScannedGrafts(HashMap hmParam) throws AppError {
		HashMap hmReturn = new HashMap();
		ArrayList alReturn = new ArrayList();
		HashMap hmTotValues = new HashMap();
		HashMap hmHeaderValues = new HashMap();
		String strLoaddtlId = GmCommonClass.parseNull((String) hmParam.get("STRLOADDTLID"));
		String strCtrlNo = GmCommonClass.parseNull((String) hmParam.get("STRCTRLNO"));
		String strLoadId = GmCommonClass.parseNull((String) hmParam.get("STRLOADID"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString(
				"gm_pkg_op_thb_load_trans.gm_sav_scan_grafts", 6);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.setString(1, strLoaddtlId);
		gmDBManager.setString(2, strLoadId);
		gmDBManager.setString(3, strCtrlNo);
		gmDBManager.setString(4, strUserID);
		gmDBManager.execute();
		hmTotValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(5));
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		hmReturn.put("HMTOTVALUES", hmTotValues);
		hmReturn.put("LOTDETAILS", alReturn);
		gmDBManager.commit();
		return hmReturn;

	}

	/**
	   * updateTHBScannedGrafts - This Method is written to update status in the Master
	   * Table.
	   * 
	   * @author ssharmila
	   * 
	   * */
	public void updateTHBScannedGrafts(HashMap hmParam) throws AppError {
		HashMap hmReturn = new HashMap();
		ArrayList alReturn = new ArrayList();
		HashMap hmTotValues = new HashMap();
		HashMap hmHeaderValues = new HashMap();
		String strLoadId = GmCommonClass.parseNull((String) hmParam
				.get("STRLOADID"));
		String strUserID = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString(
				"gm_pkg_op_thb_load_trans.gm_upd_scan_grafts", 2);
		gmDBManager.setString(1, strLoadId);
		gmDBManager.setString(2, strUserID);
		gmDBManager.execute();
		gmDBManager.commit();

	}

	/**
	 * rollbackTHBBox - This method used to update the load box status as Initiated
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void rollbackTHBBox(HashMap hmParam) throws AppError {
		String strJsonData = "";
		String strLoadId = GmCommonClass.parseNull((String) hmParam.get("STRLOADID"));
		String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_thb_load_trans.gm_rollback_thb_load_box", 2);
		gmDBManager.setString(1, strLoadId);
		gmDBManager.setString(2, strUserID);
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
