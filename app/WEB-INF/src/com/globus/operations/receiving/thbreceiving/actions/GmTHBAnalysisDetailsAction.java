package com.globus.operations.receiving.thbreceiving.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdk.nashorn.internal.parser.JSONParser;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.operations.receiving.thbreceiving.beans.GmTHBAnalysisDetailsBean;
import com.globus.operations.receiving.thbreceiving.forms.GmTHBAnalysisDetailsForm;
import com.globus.sales.event.beans.GmEventSetupBean;

/**
 * @author tramasamy
 * 
 */
public class GmTHBAnalysisDetailsAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchTHBAnalysisDetails - this Method used to Load the THB Analysis Details Report Screen based
   * on Analysis Id details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward fetchTHBAnalysisDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTHBAnalysisDetailsForm gmTHBAnalysisDetailsForm = (GmTHBAnalysisDetailsForm) actionForm;
    gmTHBAnalysisDetailsForm.loadSessionParameters(request);
    GmTHBAnalysisDetailsBean gmTHBAnalysisDetailsBean =
        new GmTHBAnalysisDetailsBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();

    ArrayList alComparisonFilter = new ArrayList();
    ArrayList alPartQty = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmMap = new HashMap();
    String strJSONGridData = "";
    String strPendingQtyJSON = "";
    String strPendingQty = "";
    String strCompleteBtnAccess = "";
    String strUserId = "";
    // get Code group value
    alComparisonFilter = GmCommonClass.getCodeList("CPRSN");
    alPartQty = GmCommonClass.getCodeList("THBFLR");
    // get value from form
    hmParam = GmCommonClass.getHashMapFromForm(gmTHBAnalysisDetailsForm);
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    gmTHBAnalysisDetailsForm.setAlComparisonFilter(alComparisonFilter); // set code group value to
                                                                        // column filter drop down
    gmTHBAnalysisDetailsForm.setAlPartQty(alPartQty);// set code group value to column filter drop
                                                     // down
    strCompleteBtnAccess =
        gmEventSetupBean.getEventAccess(strUserId, "EVENT", "COMPLETE_BTN_ACCESS");
    gmTHBAnalysisDetailsForm.setCompleteBtnAccess(strCompleteBtnAccess);
    // used to enable/ disable the complete analysis button based On return JSON Pending Qty
    strPendingQtyJSON =
        GmCommonClass.parseNull(gmTHBAnalysisDetailsBean.fetchAnalysisCompletion(hmParam));
    hmMap = (HashMap) gmWSUtil.parseJsonToMap(strPendingQtyJSON);
    strPendingQty = GmCommonClass.parseNull((String) hmMap.get("Pending_Allocated_Qty"));
    gmTHBAnalysisDetailsForm.setPendingQty(strPendingQty);

    // get JSON data from bean
    strJSONGridData =
        GmCommonClass.parseNull(gmTHBAnalysisDetailsBean.fetchTHBAnalysisDetails(hmParam));
    gmTHBAnalysisDetailsForm.setStrJSONGridData(strJSONGridData);
    return actionMapping.findForward("gmTHBAnalysisDetails");

  }

  /**
   * saveAnalysisStatus - Updates the analysis status based on Analysis Id
   * 
   * @param actionMapping author Shiny
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward saveAnalysisStatus(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTHBAnalysisDetailsForm gmTHBAnalysisDetailsForm = (GmTHBAnalysisDetailsForm) actionForm;
    gmTHBAnalysisDetailsForm.loadSessionParameters(request);
    GmTHBAnalysisDetailsBean gmTHBAnalysisDetailsBean =
        new GmTHBAnalysisDetailsBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmTHBAnalysisDetailsForm);
    gmTHBAnalysisDetailsBean.saveAnalysisStatus(hmParam);
    return null;
  }
}
