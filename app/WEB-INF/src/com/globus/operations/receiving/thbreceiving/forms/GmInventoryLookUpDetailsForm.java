package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Shiny
 * 
 */

public class GmInventoryLookUpDetailsForm extends GmCommonForm {

	private String strFromLink = "";
	private String strQty = "";
	private String strJSONGridData = "";
	private String strBase = "";
	private String strPrivatePart = "";
	private String strPartDesc = "";
	private String strAccountName = "";
	private String strRequiredDate = "";
	private String Qty = "";
	private String RefId = "";
	private String RefType = "";
	private String CreatedBy = "";
	private String CreatedDate = "";
	private String stranalysisid = "";

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return CreatedBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return CreatedDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}

	/**
	 * @return the strFromLink
	 */
	public String getStrFromLink() {
		return strFromLink;
	}

	/**
	 * @param strFromLink
	 *            the strFromLink to set
	 */
	public void setStrFromLink(String strFromLink) {
		this.strFromLink = strFromLink;
	}

	/**
	 * @return the strQty
	 */
	public String getStrQty() {
		return strQty;
	}

	/**
	 * @param strQty
	 *            the strQty to set
	 */
	public void setStrQty(String strQty) {
		this.strQty = strQty;
	}

	/**
	 * @return the strJSONGridData
	 */
	public String getStrJSONGridData() {
		return strJSONGridData;
	}

	/**
	 * @param strJSONGridData
	 *            the strJSONGridData to set
	 */
	public void setStrJSONGridData(String strJSONGridData) {
		this.strJSONGridData = strJSONGridData;
	}

	/**
	 * @return the strBase
	 */
	public String getStrBase() {
		return strBase;
	}

	/**
	 * @param strBase
	 *            the strBase to set
	 */
	public void setStrBase(String strBase) {
		this.strBase = strBase;
	}

	/**
	 * @return the strPrivatePart
	 */
	public String getStrPrivatePart() {
		return strPrivatePart;
	}

	/**
	 * @param strPrivatePart
	 *            the strPrivatePart to set
	 */
	public void setStrPrivatePart(String strPrivatePart) {
		this.strPrivatePart = strPrivatePart;
	}

	/**
	 * @return the strPartDesc
	 */
	public String getStrPartDesc() {
		return strPartDesc;
	}

	/**
	 * @param strPartDesc
	 *            the strPartDesc to set
	 */
	public void setStrPartDesc(String strPartDesc) {
		this.strPartDesc = strPartDesc;
	}

	/**
	 * @return the strAccountName
	 */
	public String getStrAccountName() {
		return strAccountName;
	}

	/**
	 * @param strAccountName
	 *            the strAccountName to set
	 */
	public void setStrAccountName(String strAccountName) {
		this.strAccountName = strAccountName;
	}

	/**
	 * @return the strRequiredDate
	 */
	public String getStrRequiredDate() {
		return strRequiredDate;
	}

	/**
	 * @param strRequiredDate
	 *            the strRequiredDate to set
	 */
	public void setStrRequiredDate(String strRequiredDate) {
		this.strRequiredDate = strRequiredDate;
	}

	/**
	 * @return the qty
	 */
	public String getQty() {
		return Qty;
	}

	/**
	 * @param qty
	 *            the qty to set
	 */
	public void setQty(String qty) {
		Qty = qty;
	}

	/**
	 * @return the refId
	 */
	public String getRefId() {
		return RefId;
	}

	/**
	 * @param refId
	 *            the refId to set
	 */
	public void setRefId(String refId) {
		RefId = refId;
	}

	/**
	 * @return the refType
	 */
	public String getRefType() {
		return RefType;
	}

	/**
	 * @param refType
	 *            the refType to set
	 */
	public void setRefType(String refType) {
		RefType = refType;
	}

	/**
	 * @return the stranalysisid
	 */
	public String getStranalysisid() {
		return stranalysisid;
	}

	/**
	 * @param stranalysisid the stranalysisid to set
	 */
	public void setStranalysisid(String stranalysisid) {
		this.stranalysisid = stranalysisid;
	}

}
