package com.globus.operations.receiving.thbreceiving.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmDonorDetailsReportForm extends GmCommonForm{

		private String gridXmlData = "";
		private String loadNum = "";
		private String donorNum = "";
		private String basePart ="";
		private String processedDate ="";
		
		private String hLoadNumStr ="";
		private String hDonorNumStr ="";
		private String hBasePartStr ="";
	    private String strJSONGridData = "";
	    private String strOpt = "";
	    private String strFromLink = "";
	    
	    //Added for PMT-32606 THB Rad Run Re-designate and Inventory Allocate
	    private String privatePart="";
	    private String analysisId="";
		private String itemDetailsId ="";
	    private String locationType ="";
	    private String redesinateAs ="";
	    private String  scheduleAccessFl ="";
	    private String  dropDownType ="";
		private ArrayList alLocations = new ArrayList();
		private ArrayList alRedesignate = new ArrayList();
		/**
		 * @return the gridXmlData
		 */
		public String getGridXmlData() {
			return gridXmlData;
		}
		/**
		 * @param gridXmlData the gridXmlData to set
		 */
		public void setGridXmlData(String gridXmlData) {
			this.gridXmlData = gridXmlData;
		}
		/**
		 * @return the loadNum
		 */
		public String getLoadNum() {
			return loadNum;
		}
		/**
		 * @param loadNum the loadNum to set
		 */
		public void setLoadNum(String loadNum) {
			this.loadNum = loadNum;
		}
		/**
		 * @return the donorNum
		 */
		public String getDonorNum() {
			return donorNum;
		}
		/**
		 * @param donorNum the donorNum to set
		 */
		public void setDonorNum(String donorNum) {
			this.donorNum = donorNum;
		}
		/**
		 * @return the basePart
		 */
		public String getBasePart() {
			return basePart;
		}
		/**
		 * @param basePart the basePart to set
		 */
		public void setBasePart(String basePart) {
			this.basePart = basePart;
		}
		/**
		 * @return the processedDate
		 */
		public String getProcessedDate() {
			return processedDate;
		}
		/**
		 * @param processedDate the processedDate to set
		 */
		public void setProcessedDate(String processedDate) {
			this.processedDate = processedDate;
		}
		/**
		 * @return the hLoadNumStr
		 */
		public String gethLoadNumStr() {
			return hLoadNumStr;
		}
		/**
		 * @param hLoadNumStr the hLoadNumStr to set
		 */
		public void sethLoadNumStr(String hLoadNumStr) {
			this.hLoadNumStr = hLoadNumStr;
		}
		/**
		 * @return the hDonorNumStr
		 */
		public String gethDonorNumStr() {
			return hDonorNumStr;
		}
		/**
		 * @param hDonorNumStr the hDonorNumStr to set
		 */
		public void sethDonorNumStr(String hDonorNumStr) {
			this.hDonorNumStr = hDonorNumStr;
		}
		/**
		 * @return the hBasePartStr
		 */
		public String gethBasePartStr() {
			return hBasePartStr;
		}
		/**
		 * @param hBasePartStr the hBasePartStr to set
		 */
		public void sethBasePartStr(String hBasePartStr) {
			this.hBasePartStr = hBasePartStr;
		}
		/**
		 * @return the strJSONGridData
		 */
		public String getStrJSONGridData() {
			return strJSONGridData;
		}
		/**
		 * @param strJSONGridData the strJSONGridData to set
		 */
		public void setStrJSONGridData(String strJSONGridData) {
			this.strJSONGridData = strJSONGridData;
		}
		/**
		 * @return the strOpt
		 */
		public String getStrOpt() {
			return strOpt;
		}
		/**
		 * @param strOpt the strOpt to set
		 */
		public void setStrOpt(String strOpt) {
			this.strOpt = strOpt;
		}
		/**
		 * @return the strFromLink
		 */
		public String getStrFromLink() {
			return strFromLink;
		}
		/**
		 * @param strFromLink the strFromLink to set
		 */
		public void setStrFromLink(String strFromLink) {
			this.strFromLink = strFromLink;
		}
		/**
		 * @return the itemDetailsId
		 */
		public String getItemDetailsId() {
			return itemDetailsId;
		}
		/**
		 * @param itemDetailsId the itemDetailsId to set
		 */
		public void setItemDetailsId(String itemDetailsId) {
			this.itemDetailsId = itemDetailsId;
		}
		/**
		 * @return the locationType
		 */
		public String getLocationType() {
			return locationType;
		}
		/**
		 * @param locationType the locationType to set
		 */
		public void setLocationType(String locationType) {
			this.locationType = locationType;
		}
		/**
		 * @return the redesinateAs
		 */
		public String getRedesinateAs() {
			return redesinateAs;
		}
		/**
		 * @param redesinateAs the redesinateAs to set
		 */
		public void setRedesinateAs(String redesinateAs) {
			this.redesinateAs = redesinateAs;
		}
		/**
		 * @return the alLocations
		 */
		public ArrayList getAlLocations() {
			return alLocations;
		}
		/**
		 * @param alLocations the alLocations to set
		 */
		public void setAlLocations(ArrayList alLocations) {
			this.alLocations = alLocations;
		}
		/**
		 * @return the alRedesignate
		 */
		public ArrayList getAlRedesignate() {
			return alRedesignate;
		}
		/**
		 * @param alRedesignate the alRedesignate to set
		 */
		public void setAlRedesignate(ArrayList alRedesignate) {
			this.alRedesignate = alRedesignate;
		}
		/**
		 * @return the scheduleAccessFl
		 */
		public String getScheduleAccessFl() {
			return scheduleAccessFl;
		}
		/**
		 * @param scheduleAccessFl the scheduleAccessFl to set
		 */
		public void setScheduleAccessFl(String scheduleAccessFl) {
			this.scheduleAccessFl = scheduleAccessFl;
		}
		/**
		 * @return the privatePart
		 */
		public String getPrivatePart() {
			return privatePart;
		}
		/**
		 * @param privatePart the privatePart to set
		 */
		public void setPrivatePart(String privatePart) {
			this.privatePart = privatePart;
		}
		/**
		 * @return the analysisId
		 */
		public String getAnalysisId() {
			return analysisId;
		}
		/**
		 * @param analysisId the analysisId to set
		 */
		public void setAnalysisId(String analysisId) {
			this.analysisId = analysisId;
		}
		/**
		 * @return the dropDownType
		 */
		public String getDropDownType() {
			return dropDownType;
		}
		/**
		 * @param dropDownType the dropDownType to set
		 */
		public void setDropDownType(String dropDownType) {
			this.dropDownType = dropDownType;
		}

		
	   
}
