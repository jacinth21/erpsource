package com.globus.operations.receiving.thbreceiving.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.receiving.thbreceiving.beans.GmDonorDetailsReportBean;
import com.globus.operations.receiving.thbreceiving.forms.GmDonorDetailsReportForm;


/**
 * @author tramasamy
 *
 */
public class GmDonorDetailsReportAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/** loadDonorDtlsRpt - This method used to load the GmRadRunReport.jsp
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadRadRunRpt(ActionMapping actionMapping,ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmDonorDetailsReportForm gmDonorDetailsReportForm = (GmDonorDetailsReportForm) actionForm;
		gmDonorDetailsReportForm.loadSessionParameters(request);
		return actionMapping.findForward("gmDonorDetailsRpt");
	}
	/**fetchDonorDtlsReport - This method used to Fetch Donor Details to load the Report
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchDonorDtlsReport(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmDonorDetailsReportForm gmDonorDetailsReportForm = (GmDonorDetailsReportForm) actionForm;
		gmDonorDetailsReportForm.loadSessionParameters(request);
		GmDonorDetailsReportBean gmDonorDetailsReportBean = new GmDonorDetailsReportBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		String strDonorDtlsJsonString = "";
		hmParam = GmCommonClass.getHashMapFromForm(gmDonorDetailsReportForm);
		strDonorDtlsJsonString = GmCommonClass
				.parseNull(gmDonorDetailsReportBean
						.fetchDonorDtls(hmParam));
		gmDonorDetailsReportForm.setStrJSONGridData(strDonorDtlsJsonString);
        return actionMapping.findForward("gmDonorDetailsRpt");
		/*response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strDonorDtlsJsonString.equals("")) {
			pw.write(strDonorDtlsJsonString);
		}
		pw.flush();
		return null;*/
		

	}
}
