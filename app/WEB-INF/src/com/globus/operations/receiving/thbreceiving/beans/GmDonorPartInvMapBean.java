package com.globus.operations.receiving.thbreceiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author tramasamy
 * 
 *         PMT-32606 - THB Rad Run Re-designate and Inventory Allocate
 */
public class GmDonorPartInvMapBean extends GmCommonBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmDonorPartInvMapBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * fetchDonorDetailsReport - This method used to Fetch RadRunQty Details
   * 
   * @author shiny
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public String fetchDonorDetailsReport(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBasePart = GmCommonClass.parseNull((String) hmParam.get("BASEPART"));
    String strPrivatePart = GmCommonClass.parseNull((String) hmParam.get("PRIVATEPART"));
    String strAnalysisId = GmCommonClass.parseNull((String) hmParam.get("ANALYSISID"));
    String strDonorDetailsReports = "";
    gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_fch_rad_run_details", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
    gmDBManager.setString(1, strAnalysisId);
    gmDBManager.setString(2, strBasePart);
    gmDBManager.setString(3, strPrivatePart);
    gmDBManager.execute();
    strDonorDetailsReports = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.close();
    return strDonorDetailsReports;
  }

  /**
   * savPartInvMap - this method used to save the Ware house type based on item details id
   * 
   * @param hmParam
   * @throws AppError
   */
  public void savPartInvMap(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strItemId = GmCommonClass.parseNull((String) hmParam.get("ITEMDETAILSID"));
    String strLocationId = GmCommonClass.parseNull((String) hmParam.get("LOCATIONTYPE"));
    if (strLocationId.equals("0")) {
      strLocationId = "";
    }
    gmDBManager.setPrepareString("gm_pkg_op_donor_inv_allocation.gm_sav_inventory_allocation", 3);
    gmDBManager.setString(1, strItemId);
    gmDBManager.setString(2, strLocationId);
    gmDBManager.setInt(3, Integer.parseInt(strUserId));
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchPartnumDtls - This method used to fetch the value for redesignated As Drop Down
   * 
   * @return alReturn
   * @throws AppError
   */
  public ArrayList fetchPartnumDtls(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    String strBasePart = GmCommonClass.parseNull((String) hmParam.get("BASEPART"));
    gmDBManager.setPrepareString("gm_pkg_op_thb_detail_rpt.gm_fch_private_part_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strBasePart);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * savOrderedPartMap - this method used to update the redesignated Part number based on the Item
   * Details Id
   * 
   * @param hmParam
   * @throws AppError
   */
  public void savOrderedPartMap(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strItemId = GmCommonClass.parseNull((String) hmParam.get("ITEMDETAILSID"));
    String strRedesignateAs = GmCommonClass.parseNull((String) hmParam.get("REDESINATEAS"));
    if (strRedesignateAs.equals("0")) {
      strRedesignateAs = "";
    }
    gmDBManager.setPrepareString("gm_pkg_op_donor_inv_allocation.gm_sav_part_allocation", 3);
    gmDBManager.setString(1, strItemId);
    gmDBManager.setString(2, strRedesignateAs);
    gmDBManager.setInt(3, Integer.parseInt(strUserId));
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveRadRunAlloccation - This Method Used for to update allocation qty
   * 
   * @param hmParam
   * @throws AppError
   */
  public void saveRadRunAllocationQty(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLoadNum = GmCommonClass.parseNull((String) hmParam.get("LOADNUM"));
    String strBasePart = GmCommonClass.parseNull((String) hmParam.get("BASEPART"));
    String strAnalysisId = GmCommonClass.parseNull((String) hmParam.get("ANALYSISID"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("DONORNUM"));
    String strPrivatePart = GmCommonClass.parseNull((String) hmParam.get("PRIVATEPART"));

    gmDBManager.setPrepareString("gm_pkg_op_donor_inv_allocation.gm_sav_rad_run_allocation", 5);
    gmDBManager.setString(1, strDonorNum);
    gmDBManager.setString(2, strBasePart);
    gmDBManager.setString(3, strAnalysisId);
    gmDBManager.setString(4, strPrivatePart);
    gmDBManager.setInt(5, Integer.parseInt(strUserId));
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * updateAnalyisStatus - this method used to update status as In Progress
   * 
   * @param hmParam
   * @throws AppError
   */
  public void updateAnalyisStatus(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strAnalysisId = GmCommonClass.parseNull((String) hmParam.get("ANALYSISID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_upd_analysis_status", 2);
    gmDBManager.setString(1, strAnalysisId);
    gmDBManager.setInt(2, Integer.parseInt(strUserId));
    gmDBManager.execute();
    gmDBManager.commit();

  }

}
