package com.globus.operations.receiving.thbreceiving.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.thbreceiving.beans.GmTHBRecListRptBean;
import com.globus.operations.receiving.thbreceiving.forms.GmTHBRecListRptForm;

public class GmTHBRecListRptAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchTHBRecListRpt : This method is used to load the details on the
	 * screen
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchTHBRecListRpt(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		GmTHBRecListRptForm gmTHBRecListRptForm = (GmTHBRecListRptForm) actionForm;
		gmTHBRecListRptForm.loadSessionParameters(request);
		GmTHBRecListRptBean gmTHBRecListRptBean = new GmTHBRecListRptBean(getGmDataStoreVO());

		ArrayList alLoadDtls = new ArrayList();
		HashMap hmReturn = new HashMap();
		HashMap hmParam = new HashMap();

		String strxmlGridData = "";
		String strOpt = "";
		String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
		String strSessCompanyLocale = GmCommonClass.parseNull((String) request
				.getSession().getAttribute("strSessCompanyLocale"));
		hmParam = GmCommonClass.getHashMapFromForm(gmTHBRecListRptForm);
		strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		gmTHBRecListRptForm.setAlLoadStatusType(GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("THBLDL",getGmDataStoreVO())));

		if (strOpt.equals("Load")) {
			alLoadDtls = GmCommonClass.parseNullArrayList(gmTHBRecListRptBean.fetchTHBRecListRpt(hmParam));
			hmReturn.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
			hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
			hmReturn.put("TEMPLATE_NM", "GmTHBRecListRpt.vm");
			hmReturn.put("LABEL_PROPERTIES_NM","properties.labels.operations.receiving.thbreceiving.GmTHBRecListReportSearch");
			// based on array list to get the Grid xml string
			strxmlGridData = gmTHBRecListRptBean.generateOutput(alLoadDtls,hmReturn);
			gmTHBRecListRptForm.setStrXmlGridData(strxmlGridData);
		}
		return actionMapping.findForward("gmTHBRecListRpt");
	}

	/**
	 * fetchTHBListReportDetail:Fetch THB List Report Details
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchTHBListReportDetail(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		GmTHBRecListRptForm gmTHBRecListRptForm = (GmTHBRecListRptForm) actionForm;
		gmTHBRecListRptForm.loadSessionParameters(request);
		GmTHBRecListRptBean gmTHBRecListRptBean = new GmTHBRecListRptBean(
				getGmDataStoreVO());
		GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();

		ArrayList alTHBListReportDetails = new ArrayList();
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmApplnParam = null;

		hmParam = GmCommonClass.getHashMapFromForm(gmTHBRecListRptForm);
		alTHBListReportDetails = gmTHBRecListRptBean.fetchTHBListReportDetail(hmParam);
		hmApplnParam = new HashMap();
		hmApplnParam.put("TEMPLATE_NM", "GmTHBReceivingListReportdtls.vm");
		hmApplnParam.put("LABEL_PROPERTIES_NM",
						"properties.labels.operations.receiving.thbreceiving.GmTHBReceivingListRpt");

		// based on array list to get the Grid xml string
		String xmlData = gmTHBRecListRptBean.generateOutput(
				alTHBListReportDetails, hmApplnParam);
		gmTHBRecListRptForm.setStrXmlGridData(xmlData);
		return actionMapping.findForward("gmTHBReceivingListReport");
	}
}
