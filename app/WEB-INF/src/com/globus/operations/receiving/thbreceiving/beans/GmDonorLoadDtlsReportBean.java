package com.globus.operations.receiving.thbreceiving.beans;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

public class GmDonorLoadDtlsReportBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmDonorLoadDtlsReportBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * fetchDonorLoadDtsl - This Method Used to load the donor load Details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */

  public String fetchDonorLoadDtsl(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLoadNum = GmCommonClass.parseNull((String) hmParam.get("LOADNUM"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("DONORNUM"));
    String strDonorDtlsRptJSONString = "";
    gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_fch_load_details", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
    gmDBManager.setString(1, strLoadNum);
    gmDBManager.setString(2, strDonorNum);
    gmDBManager.execute();
    strDonorDtlsRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    return strDonorDtlsRptJSONString;
  }


  /**
   * fetchDonorLoadDtsl - This Method Used to load the donor load Details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */

  public String fetchDonorLoadStageDtsl(HashMap hmParam) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLoadNum = GmCommonClass.parseNull((String) hmParam.get("LOADNUM"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("DONORNUM"));
    String strStgDtFrom = GmCommonClass.parseNull((String) hmParam.get("STAGEDATEFROM"));
    String strStgDtTo = GmCommonClass.parseNull((String) hmParam.get("STAGEDATETO"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    Date dtStgDtFrom = GmCommonClass.getStringToDate(strStgDtFrom, strApplDateFmt);;
    Date dtStgDtTo = GmCommonClass.getStringToDate(strStgDtTo, strApplDateFmt);;
    String strDonorDtlsRptJSONString = "";
    gmDBManager.setPrepareString("gm_pkg_op_thb_analysis_rpt.gm_fch_load_stage_details", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
    gmDBManager.setString(1, strLoadNum);
    gmDBManager.setString(2, strDonorNum);
    gmDBManager.setDate(3, dtStgDtFrom);
    gmDBManager.setDate(4, dtStgDtTo);
    gmDBManager.execute();
    strDonorDtlsRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.close();
    return strDonorDtlsRptJSONString;
  }
}
