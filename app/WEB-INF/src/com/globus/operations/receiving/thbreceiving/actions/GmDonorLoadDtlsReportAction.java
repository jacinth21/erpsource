package com.globus.operations.receiving.thbreceiving.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.receiving.thbreceiving.beans.GmDonorLoadDtlsReportBean;
import com.globus.operations.receiving.thbreceiving.forms.GmDonorLoadDtlsReportForm;

public class GmDonorLoadDtlsReportAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * loadDonorLoadDtls - This method used to load the
	 * GmDonorLoadDtlsReport.jsp Page
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadDonorLoadDtls(ActionMapping actionMapping,  
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		String strFrdPage = "";
		String strDHRStatus = "";
		strDHRStatus = GmCommonClass.parseNull(request.getParameter("Cbo_Status"));
		request.setAttribute("hStatus", strDHRStatus);
		 
		GmDonorLoadDtlsReportForm gmDonorLoadDtlsRptForm = (GmDonorLoadDtlsReportForm) actionForm;
		gmDonorLoadDtlsRptForm.loadSessionParameters(request);
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmDonorLoadDtlsRptForm);
		String strMode = GmCommonClass.parseNull(gmDonorLoadDtlsRptForm.gethFromLink());
		if(strMode.equals("donorSchedule")){ // if strmode equals to donorSchedule redirect to donor schedule report page
			strFrdPage = "gmDonorScheduleReport";
		}else{
			strFrdPage = "gmDonorLoadDtlsRpt";  // else redirect to donor report page
		}
		return actionMapping.findForward(strFrdPage);
	}

	/**
	 * fetchDonorLoadDtls - This method used to Fetch Donor Load Details
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchDonorLoadDtsl(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmDonorLoadDtlsReportForm gmDonorLoadDtlsRptForm = (GmDonorLoadDtlsReportForm) actionForm;
		gmDonorLoadDtlsRptForm.loadSessionParameters(request);
		GmDonorLoadDtlsReportBean gmDonorLoadDtlsReportBean = new GmDonorLoadDtlsReportBean(
				getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		String strDonorDtlsRptJsonString = "";
		String strFrdPage = "";
		hmParam = GmCommonClass.getHashMapFromForm(gmDonorLoadDtlsRptForm);
		
		String strMode = GmCommonClass.parseNull(gmDonorLoadDtlsRptForm.gethFromLink());
		if(strMode.equals("donorSchedule")){
			strDonorDtlsRptJsonString = GmCommonClass
					.parseNull(gmDonorLoadDtlsReportBean
							.fetchDonorLoadStageDtsl(hmParam));
			gmDonorLoadDtlsRptForm.setStrJSONGridData(strDonorDtlsRptJsonString);
			strFrdPage = "gmDonorScheduleReport";
		}else{
			strDonorDtlsRptJsonString = GmCommonClass
					.parseNull(gmDonorLoadDtlsReportBean
							.fetchDonorLoadDtsl(hmParam));
			gmDonorLoadDtlsRptForm.setStrJSONGridData(strDonorDtlsRptJsonString);
			strFrdPage = "gmDonorLoadDtlsRpt";
		}
		return actionMapping.findForward(strFrdPage);
		
		//return actionMapping.findForward("gmDonorLoadDtlsRpt");
		/*response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strDonorDtlsRptJsonString.equals("")) {
			pw.write(strDonorDtlsRptJsonString);
		}
		pw.flush();
		return null;*/
	}
}