package com.globus.operations.receiving.thbreceiving.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import com.globus.operations.receiving.thbreceiving.beans.GmDonorPartInvMapBean;
import com.globus.operations.receiving.thbreceiving.forms.GmDonorDetailsReportForm;
import com.globus.sales.event.beans.GmEventSetupBean;

/**
 * @author tramasamy PMT-32606 - THB Rad Run Re-designate and Inventory Allocate
 * 
 */
public class GmDonorPartInventoryMapAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchDonorDetailsReport - This method used to load the THB Donor Allocation Screen
   * 
   * @author shiny
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward fetchDonorAllocationDetails(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    instantiate(request, response);
    GmDonorDetailsReportForm gmDonorDetailsReportForm = (GmDonorDetailsReportForm) actionForm;
    gmDonorDetailsReportForm.loadSessionParameters(request);
    GmDonorPartInvMapBean gmDonorPartInvMapBean = new GmDonorPartInvMapBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alLocations = new ArrayList();
    ArrayList alPartDtls = new ArrayList();
    String strDonorDetailsReports = "";
    String strScheduleAccess = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorDetailsReportForm);
    alPartDtls = GmCommonClass.parseNullArrayList(gmDonorPartInvMapBean.fetchPartnumDtls(hmParam));
    //RSIHTY code group is used since this the code group used in listing the drop down value in the DHR Pending process screen. The drop down value selected in 
    //this screen will be displayed in DHR pending process screen. Hence both the code group has to be same.
    alLocations = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RSIHTY"));
    gmDonorDetailsReportForm.setAlLocations(alLocations);// set value for
    // Inventory
    // allocation
    // drop down
    gmDonorDetailsReportForm.setAlRedesignate(alPartDtls);// set value for
    // redesignated
    // part drop
    // down

    strScheduleAccess =
        gmEventSetupBean.getEventAccess(getGmDataStoreVO().getPartyid(), "EVENT",
            "SCHEDULE_BTN_ACCESS");
    gmDonorDetailsReportForm.setScheduleAccessFl(strScheduleAccess);// set
    // access
    // for
    // Schedule
    // button
    gmDonorPartInvMapBean.updateAnalyisStatus(hmParam);// to update the
    // status as In
    // Progress
    strDonorDetailsReports =
        GmCommonClass.parseNull(gmDonorPartInvMapBean.fetchDonorDetailsReport(hmParam));
    gmDonorDetailsReportForm.setStrJSONGridData(strDonorDetailsReports);
    gmDonorDetailsReportForm.setStrFromLink("radRunQty");
    return actionMapping.findForward("gmDonorDetailsRpt");
  }

  /**
   * This Method Used to Save the ware house type and redesignated part
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward saveInvAllocationForPart(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmDonorDetailsReportForm gmDonorDetailsReportForm = (GmDonorDetailsReportForm) actionForm;
    gmDonorDetailsReportForm.loadSessionParameters(request);
    GmDonorPartInvMapBean gmDonorPartInvMapBean = new GmDonorPartInvMapBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    String strInv = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorDetailsReportForm);
    strInv = GmCommonClass.parseNull(gmDonorDetailsReportForm.getDropDownType());
    if (strInv.equals("InvType")) {
      gmDonorPartInvMapBean.savPartInvMap(hmParam); // To save the ware
      // house type
    } else {
      gmDonorPartInvMapBean.savOrderedPartMap(hmParam); // to save the
      // redesignated
      // part
    }
    return null;
  }

  /**
   * saveRadRunAllocation - This method Used to update the allocation Qty
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward saveRadRunAllocation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmDonorDetailsReportForm gmDonorDetailsReportForm = (GmDonorDetailsReportForm) actionForm;
    gmDonorDetailsReportForm.loadSessionParameters(request);
    GmDonorPartInvMapBean gmDonorPartInvMapBean = new GmDonorPartInvMapBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorDetailsReportForm);
    gmDonorPartInvMapBean.saveRadRunAllocationQty(hmParam);
    return null;
  }

}
