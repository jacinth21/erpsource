package com.globus.operations.receiving.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmReceivingParamIncForm extends GmCommonForm {
	
	private String strOpt = "";
	private String xmlStringData = "";
	private String attrtype = "";
	private String labelprintfl = "";
	private String form42fl = "";
	private String qcverifiedfl = "";
	private String rsid = "";
	private String strBtnDisabled = "";
	private String preStagingfl = "";
	private String stagingfl = "";
	private String inputstr = "";
	private String strDHRStatusId ="";

	/**
	 * @return the inputstr
	 */
	public String getInputstr() {
		return inputstr;
	}

	/**
	 * @param inputstr the inputstr to set
	 */
	public void setInputstr(String inputstr) {
		this.inputstr = inputstr;
	}

	/**
	 * @return the preStagingfl
	 */
	public String getPreStagingfl() {
		return preStagingfl;
	}

	/**
	 * @param preStagingfl the preStagingfl to set
	 */
	public void setPreStagingfl(String preStagingfl) {
		this.preStagingfl = preStagingfl;
	}

	/**
	 * @return the stagingfl
	 */
	public String getStagingfl() {
		return stagingfl;
	}

	/**
	 * @param stagingfl the stagingfl to set
	 */
	public void setStagingfl(String stagingfl) {
		this.stagingfl = stagingfl;
	}

	/**
	 * @return the strOpt
	 */
	public String getStrOpt() {
		return strOpt;
	}

	/**
	 * @param strOpt the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}

	/**
	 * @return the xmlStringData
	 */
	public String getXmlStringData() {
		return xmlStringData;
	}

	/**
	 * @param xmlStringData the xmlStringData to set
	 */
	public void setXmlStringData(String xmlStringData) {
		this.xmlStringData = xmlStringData;
	}

	/**
	 * @return the attrtype
	 */
	public String getAttrtype() {
		return attrtype;
	}

	/**
	 * @param attrtype the attrtype to set
	 */
	public void setAttrtype(String attrtype) {
		this.attrtype = attrtype;
	}

	/**
	 * @return the labelprintfl
	 */
	public String getLabelprintfl() {
		return labelprintfl;
	}

	/**
	 * @param labelprintfl the labelprintfl to set
	 */
	public void setLabelprintfl(String labelprintfl) {
		this.labelprintfl = labelprintfl;
	}

	/**
	 * @return the form42fl
	 */
	public String getForm42fl() {
		return form42fl;
	}

	/**
	 * @param form42fl the form42fl to set
	 */
	public void setForm42fl(String form42fl) {
		this.form42fl = form42fl;
	}

	/**
	 * @return the qcverifiedfl
	 */
	public String getQcverifiedfl() {
		return qcverifiedfl;
	}

	/**
	 * @param qcverifiedfl the qcverifiedfl to set
	 */
	public void setQcverifiedfl(String qcverifiedfl) {
		this.qcverifiedfl = qcverifiedfl;
	}

	/**
	 * @return the rsid
	 */
	public String getRsid() {
		return rsid;
	}

	/**
	 * @param rsid the rsid to set
	 */
	public void setRsid(String rsid) {
		this.rsid = rsid;
	}

	/**
	 * @return the strBtnDisabled
	 */
	public String getStrBtnDisabled() {
		return strBtnDisabled;
	}

	/**
	 * @param strBtnDisabled the strBtnDisabled to set
	 */
	public void setStrBtnDisabled(String strBtnDisabled) {
		this.strBtnDisabled = strBtnDisabled;
	}
	
	
	/**
	 * @return the strDHRStatusId
	 */
	public String getStrDHRStatusId() {
		return strDHRStatusId;
	}

	/**
	 * @param strDHRStatusId the strDHRStatusId to set
	 */
	public void setStrDHRStatusId(String strDHRStatusId) {
		this.strDHRStatusId = strDHRStatusId;
	}
	
	

	
	
	
	
	
	
}