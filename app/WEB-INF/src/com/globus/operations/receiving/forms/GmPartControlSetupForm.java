package com.globus.operations.receiving.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCancelForm;


public class GmPartControlSetupForm extends GmLogForm{
 
	
	private String partNM = "";
	private String controlNM = "";
	private String vendorId = "";
	private String vendorName = "";
	private String donorNM = "";
	private String donorID = "";
	private String expDate = "";
	private String manfDate = "";
	private String revLevel = "";
	private String udi = "";
	private String customSize = "";
	private String packSlip = "";	
	private ArrayList alLogReasons = new ArrayList();
	private ArrayList alVendors = new ArrayList();
	
	private ArrayList alDonors = new ArrayList();
	private String partCNId = "";
	private String prodClass = "";
	private String prodType = "";
	private String julianRule = "";
	
	private String expiryAccess = "";
	private String cnumfmt="";
	private String hScreenType = "";
	private String hAccess = "";
	private String udiLabel = "";
	
	private String udiFormat = "";
	private String diNum = "";
	private String show_Txtbox = "";
	private String udiNo = "";
	private String partNum = "";
	private String disableText = "";	
	private String successMsg = "";	
	private String UDI_LOG_ENTRY="";
	private String lotOverrideTxnids = "";
	
	public String getCnumfmt() {
		return cnumfmt;
	}
	public void setCnumfmt(String cnumfmt) {
		this.cnumfmt = cnumfmt;
	}
	/**
	 * @return the julianRule
	 */
	public String getJulianRule() {
		return julianRule;
	}
	/**
	 * @param julianRule the julianRule to set
	 */
	public void setJulianRule(String julianRule) {
		this.julianRule = julianRule;
	}
	/**
	 * @return the prodType
	 */
	public String getProdType() {
		return prodType;
	}
	/**
	 * @param prodType the prodType to set
	 */
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	/**
	 * @return the prodClass
	 */
	public String getProdClass() {
		return prodClass;
	}
	/**
	 * @param prodClass the prodClass to set
	 */
	public void setProdClass(String prodClass) {
		this.prodClass = prodClass;
	}
	/**
	 * @return the alDonors
	 */
	public ArrayList getAlDonors() {
		return alDonors;
	}
	/**
	 * @param alDonors the alDonors to set
	 */
	public void setAlDonors(ArrayList alDonors) {
		this.alDonors = alDonors;
	}
	/**
	 * @return the partCNId
	 */
	public String getPartCNId() {
		return partCNId;
	}
	/**
	 * @param partCNId the partCNId to set
	 */
	public void setPartCNId(String partCNId) {
		this.partCNId = partCNId;
	}
	/**
	 * @return the alVendors
	 */
	public ArrayList getAlVendors() {
		return alVendors;
	}
	/**
	 * @param alVendors the alVendors to set
	 */
	public void setAlVendors(ArrayList alVendors) {
		this.alVendors = alVendors;
	}
	private String strMsg = "";
	/**
	 * @return the strMsg
	 */
	public String getStrMsg() {
		return strMsg;
	}
	/**
	 * @param strMsg the strMsg to set
	 */
	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}
	/**
	 * @return the alLogReasons
	 */
	public ArrayList getAlLogReasons() {
		return alLogReasons;
	}
	/**
	 * @param alLogReasons the alLogReasons to set
	 */
	public void setAlLogReasons(ArrayList alLogReasons) {
		this.alLogReasons = alLogReasons;
	}
	/**
	 * @return the partNM
	 */
	public String getPartNM() {
		return partNM;
	}
	/**
	 * @param partNM the partNM to set
	 */
	public void setPartNM(String partNM) {
		this.partNM = partNM;
	}
	/**
	 * @return the controlNM
	 */
	public String getControlNM() {
		return controlNM;
	}
	/**
	 * @param controlNM the controlNM to set
	 */
	public void setControlNM(String controlNM) {
		this.controlNM = controlNM;
	}
	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}
	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	/**
	 * @return the donorNM
	 */
	public String getDonorNM() {
		return donorNM;
	}
	/**
	 * @param donorNM the donorNM to set
	 */
	public void setDonorNM(String donorNM) {
		this.donorNM = donorNM;
	}
	/**
	 * @return the expDate
	 */
	public String getExpDate() {
		return expDate;
	}
	/**
	 * @param expDate the expDate to set
	 */
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	/**
	 * @return the manfDate
	 */
	public String getManfDate() {
		return manfDate;
	}
	/**
	 * @param manfDate the manfDate to set
	 */
	public void setManfDate(String manfDate) {
		this.manfDate = manfDate;
	}
	/**
	 * @return the revLevel
	 */
	public String getRevLevel() {
		return revLevel;
	}
	/**
	 * @param revLevel the revLevel to set
	 */
	public void setRevLevel(String revLevel) {
		this.revLevel = revLevel;
	}
	/**
	 * @return the customSize
	 */
	public String getCustomSize() {
		return customSize;
	}
	/**
	 * @param customSize the customSize to set
	 */
	public void setCustomSize(String customSize) {
		this.customSize = customSize;
	}
	/**
	 * @return the packSlip
	 */
	public String getPackSlip() {
		return packSlip;
	}
	/**
	 * @param packSlip the packSlip to set
	 */
	public void setPackSlip(String packSlip) {
		this.packSlip = packSlip;
	}
	/**
	 * @return the expiryAccess
	 */
	public String getExpiryAccess() {
		return expiryAccess;
	}
	/**
	 * @param expiryAccess the expiryAccess to set
	 */
	public void setExpiryAccess(String expiryAccess) {
		this.expiryAccess = expiryAccess;
	}	
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @return the donorID
	 */
	public String getDonorID() {
		return donorID;
	}
	/**
	 * @param donorID the donorID to set
	 */
	public void setDonorID(String donorID) {
		this.donorID = donorID;
	}
	/**
	 * @return the udi
	 */
	public String getUdi() {
		return udi;
	}
	/**
	 * @param udi the udi to set
	 */
	public void setUdi(String udi) {
		this.udi = udi;
	}
	/**
	 * @return the hScreenType
	 */
	public String gethScreenType() {
		return hScreenType;
	}
	/**
	 * @param hScreenType the hScreenType to set
	 */
	public void sethScreenType(String hScreenType) {
		this.hScreenType = hScreenType;
	}
	/**
	 * @return the hAccess
	 */
	public String gethAccess() {
		return hAccess;
	}
	/**
	 * @param hAccess the hAccess to set
	 */
	public void sethAccess(String hAccess) {
		this.hAccess = hAccess;
	}
	/**
	 * @return the udiLabel
	 */
	public String getUdiLabel() {
		return udiLabel;
	}
	/**
	 * @param udiLabel the udiLabel to set
	 */
	public void setUdiLabel(String udiLabel) {
		this.udiLabel = udiLabel;
	}
	
	/**
	 * @return the udiFormat
	 */
	public String getUdiFormat() {
		return udiFormat;
	}
	/**
	 * @param udiFormat the udiFormat to set
	 */
	public void setUdiFormat(String udiFormat) {
		this.udiFormat = udiFormat;
	}
	/**
	 * @return the diNum
	 */
	public String getDiNum() {
		return diNum;
	}
	/**
	 * @param diNum the diNum to set
	 */
	public void setDiNum(String diNum) {
		this.diNum = diNum;
	}
	/**
	 * @return the show_Txtbox
	 */
	public String getShow_Txtbox() {
		return show_Txtbox;
	}
	/**
	 * @param show_Txtbox the show_Txtbox to set
	 */
	public void setShow_Txtbox(String show_Txtbox) {
		this.show_Txtbox = show_Txtbox;
	}
	/**
	 * @return the udiNo
	 */
	public String getUdiNo() {
		return udiNo;
	}
	/**
	 * @param udiNo the udiNo to set
	 */
	public void setUdiNo(String udiNo) {
		this.udiNo = udiNo;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the disableText
	 */
	public String getDisableText() {
		return disableText;
	}
	/**
	 * @param disableText the disableText to set
	 */
	public void setDisableText(String disableText) {
		this.disableText = disableText;
	}
	/**
	 * @return the successMsg
	 */
	public String getSuccessMsg() {
		return successMsg;
	}
	/**
	 * @param successMsg the successMsg to set
	 */
	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}
	/**
	 * @return the uDI_LOG_ENTRY
	 */
	public String getUDI_LOG_ENTRY() {
		return UDI_LOG_ENTRY;
	}
	/**
	 * @param uDI_LOG_ENTRY the uDI_LOG_ENTRY to set
	 */
	public void setUDI_LOG_ENTRY(String uDI_LOG_ENTRY) {
		UDI_LOG_ENTRY = uDI_LOG_ENTRY;
	}
	/**
	 * @return the lotOverrideTxnids
	 */
	public String getLotOverrideTxnids() {
		return lotOverrideTxnids;
	}
	/**
	 * @param lotOverrideTxnids the lotOverrideTxnids to set
	 */
	public void setLotOverrideTxnids(String lotOverrideTxnids) {
		this.lotOverrideTxnids = lotOverrideTxnids;
	}

}