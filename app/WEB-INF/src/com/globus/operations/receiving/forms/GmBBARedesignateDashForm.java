/**
 * 
 */
package com.globus.operations.receiving.forms;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import java.util.Date;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

/**
 * @author Anilkumar
 *
 */
public class GmBBARedesignateDashForm extends GmLogForm {
	Logger log = GmLogger.getInstance(this.getClass().getName());			
	private String gridXmlData ="";
	private String hFrom="";
	private String strTxn="";
	private String strTxnType="";
	private String strTxnDate="";

	
	               
	
	/**
	 * @return the StrTxnDate
	 */
	
	public String getStrTxnDate() {
		return strTxnDate;
	}
	
	/**
	 * Set the StrTxnDate
	 */

	public void setStrTxnDate(String strTxnDate) {
		this.strTxnDate = strTxnDate;
	}

	/**
	 * @return the txnId
	 */
	
	public String getStrTxn() {
		return strTxn;
	}
	
	/**
	 * Set the txnId
	 */
	public void setStrTxn(String strTxn) {
		this.strTxn = strTxn;
	}
	
	/**
	 * @return the txn type
	 */
	public String getStrTxnType() {
		return strTxnType;
	}
	
	/**
	 * Set the Txn Type the txntype
	 */
	public void setStrTxnType(String strTxnType) {
		this.strTxnType = strTxnType;
	}
	
	/**
	 * @return the hFrom
	 */
	public String gethFrom() {
		return hFrom;
	}
	/**
	 * @param hFrom the hFrom to set
	 */
	public void sethFrom(String hFrom) {
		this.hFrom = hFrom;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	

}
