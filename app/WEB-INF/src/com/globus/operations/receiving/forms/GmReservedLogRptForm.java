package com.globus.operations.receiving.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmReservedLogRptForm extends GmCommonForm{

	private String xmlGridData ="";
	private String partNum ="";
	private String lotNum ="";
	private String transId ="";
	private String inventoryType ="";
	private String fromDate ="";         
	private String toDate ="";

	private ArrayList alInVenTypes = new ArrayList();

	public String getXmlGridData() {
		return xmlGridData;
	}

	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}

	public String getPartNum() {
		return partNum;
	}

	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	public String getLotNum() {
		return lotNum;
	}
	
	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getInventoryType() {
		return inventoryType;
	}

	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public ArrayList getAlInVenTypes() {
		return alInVenTypes;
	}

	public void setAlInVenTypes(ArrayList alInVenTypes) {
		this.alInVenTypes = alInVenTypes;
	}
	
}
