/**
 * 
 */
package com.globus.operations.receiving.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Anilkumar
 *
 */
public class GmReceiveShipDashForm extends GmCommonForm {
	
	private String strXmlGridData="";

	public String getStrXmlGridData() {
		return strXmlGridData;
	}

	public void setStrXmlGridData(String strXmlGridData) {
		this.strXmlGridData = strXmlGridData;
	}
	
}
