package com.globus.operations.receiving.forms;
import org.apache.struts.upload.FormFile;

import com.globus.common.forms.GmLogForm;
/**
 * @author pvigneshwaran
 *
 */
public class GmLotOverrideBulkActionForm extends GmLogForm {
	
	private FormFile file;
	private String strRefID = "";
	private String refType = "";
	private String message = "";
	/**
	 * @return the file
	 */
	public FormFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(FormFile file) {
		this.file = file;
	}

	/**
	 * @return the strRefID
	 */
	public String getStrRefID() {
		return strRefID;
	}

	/**
	 * @param strRefID the strRefID to set
	 */
	public void setStrRefID(String strRefID) {
		this.strRefID = strRefID;
	}

	/**
	 * @return the refType
	 */
	public String getRefType() {
		return refType;
	}

	/**
	 * @param refType the refType to set
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
