/**
 * 
 */
package com.globus.operations.receiving.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

/**
 * @author arajan
 * 
 */
public class GmPOBulkReceiveForm extends GmCommonForm {

  private String poNumber = "";
  private String vname = "";
  private Date poDate = null;
  private String poTypeNm = "";
  private String cname = "";
  private String receivedDt = "";
  private String packSlipId = "";
  private String donorNum = "";
  private String hShelfLife = "";
  private String hProcessClient = "";
  private String vid = "";
  private String haction = "";
  private String stgDt = "";

  /**
 * @return the stgDt
 */
public String getStgDt() {
	return stgDt;
}

/**
 * @param stgDt the stgDt to set
 */
public void setStgDt(String stgDt) {
	this.stgDt = stgDt;
}

// Enter Lot Information section
  private String partNum = "";
  private String partDesc = "";
  private String size = "";
  private String prodType = "";
  private String lotCode = "";
  private String mfgDate = "";
  private String expDate = "";
  private String customSize = "";
  private String gridXmlData = "";
  private String hMfgDate = "";
  private String hPartNum = "";
  private String hProdType = "";
  private String hsize = "";

  // second screen
  private String rsNumber = "";
  private String status = "";
  private String hInputString = "";
  private String hLotString = "";
  private String rejType = "";
  private String rejReason = "";
  private String hcurrentTab = "";
  private String statusId = "";
  private String strSbmtBtnDisabled = "";
  private String happrString = "";
  private String hrejString = "";

  private String accessfl = "";
  private String strMode = "";
  private String strApprRej = "";
  private String strDisabled = "";
  private String strMsg = "";
  private String chkSplitRS = "";
  private String rejRSId = "";
  private String showChkFl = "";

  // third screen
  private String hdhrProcessAction = "";
  private String strJSFunciton = "";
  private String strbuttonName = "";
  private String strRejectFl = "";
  private String selectedTab = "";
  private String strAllChecked = "";
  private String scanLotCode = "";
  private String hFGString = "";
  private String hPNString = "";
  private String hQNString = "";
  private String hInvSplitCnt = "";

  private ArrayList alrejType = new ArrayList();
  private ArrayList alLocations = new ArrayList();


  public String getSelectedTab() {
    return selectedTab;
  }

  public void setSelectedTab(String selectedTab) {
    this.selectedTab = selectedTab;
  }

  public String getAccessfl() {
    return accessfl;
  }

  public void setAccessfl(String accessfl) {
    this.accessfl = accessfl;
  }

  /**
   * @return the hInputString
   */
  public String gethInputString() {
    return hInputString;
  }

  /**
   * @param hInputString the hInputString to set
   */
  public void sethInputString(String hInputString) {
    this.hInputString = hInputString;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the rsNumber
   */
  public String getRsNumber() {
    return rsNumber;
  }

  /**
   * @param rsNumber the rsNumber to set
   */
  public void setRsNumber(String rsNumber) {
    this.rsNumber = rsNumber;
  }

  /**
   * @return the poNumber
   */
  public String getPoNumber() {
    return poNumber;
  }

  /**
   * @param poNumber the poNumber to set
   */
  public void setPoNumber(String poNumber) {
    this.poNumber = poNumber;
  }

  /**
   * @return the poDate
   */
  public Date getPoDate() {
    return poDate;
  }

  /**
   * @param poDate the poDate to set
   */
  public void setPoDate(Date poDate) {
    this.poDate = poDate;
  }

  /**
   * @return the poTypeNm
   */
  public String getPoTypeNm() {
    return poTypeNm;
  }

  /**
   * @param poTypeNm the poTypeNm to set
   */
  public void setPoTypeNm(String poTypeNm) {
    this.poTypeNm = poTypeNm;
  }

  /**
   * @return the vname
   */
  public String getVname() {
    return vname;
  }

  /**
   * @param vname the vname to set
   */
  public void setVname(String vname) {
    this.vname = vname;
  }

  /**
   * @return the cname
   */
  public String getCname() {
    return cname;
  }

  /**
   * @param cname the cname to set
   */
  public void setCname(String cname) {
    this.cname = cname;
  }

  /**
   * @return the receivedDt
   */
  public String getReceivedDt() {
    return receivedDt;
  }

  /**
   * @param receivedDt the receivedDt to set
   */
  public void setReceivedDt(String receivedDt) {
    this.receivedDt = receivedDt;
  }

  /**
   * @return the packSlipId
   */
  public String getPackSlipId() {
    return packSlipId;
  }

  /**
   * @param packSlipId the packSlipId to set
   */
  public void setPackSlipId(String packSlipId) {
    this.packSlipId = packSlipId;
  }

  /**
   * @return the donorNum
   */
  public String getDonorNum() {
    return donorNum;
  }

  /**
   * @param donorNum the donorNum to set
   */
  public void setDonorNum(String donorNum) {
    this.donorNum = donorNum;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the size
   */
  public String getSize() {
    return size;
  }

  /**
   * @param size the size to set
   */
  public void setSize(String size) {
    this.size = size;
  }

  /**
   * @return the prodType
   */
  public String getProdType() {
    return prodType;
  }

  /**
   * @param prodType the prodType to set
   */
  public void setProdType(String prodType) {
    this.prodType = prodType;
  }

  /**
   * @return the lotCode
   */
  public String getLotCode() {
    return lotCode;
  }

  /**
   * @param lotCode the lotCode to set
   */
  public void setLotCode(String lotCode) {
    this.lotCode = lotCode;
  }

  /**
   * @return the mfgDate
   */
  public String getMfgDate() {
    return mfgDate;
  }

  /**
   * @param mfgDate the mfgDate to set
   */
  public void setMfgDate(String mfgDate) {
    this.mfgDate = mfgDate;
  }

  /**
   * @return the expDate
   */
  public String getExpDate() {
    return expDate;
  }

  /**
   * @param expDate the expDate to set
   */
  public void setExpDate(String expDate) {
    this.expDate = expDate;
  }

  /**
   * @return the customSize
   */
  public String getCustomSize() {
    return customSize;
  }

  /**
   * @param customSize the customSize to set
   */
  public void setCustomSize(String customSize) {
    this.customSize = customSize;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the hShelfLife
   */
  public String gethShelfLife() {
    return hShelfLife;
  }

  /**
   * @param hShelfLife the hShelfLife to set
   */
  public void sethShelfLife(String hShelfLife) {
    this.hShelfLife = hShelfLife;
  }

  /**
   * @return the hProcessClient
   */
  public String gethProcessClient() {
    return hProcessClient;
  }

  /**
   * @param hProcessClient the hProcessClient to set
   */
  public void sethProcessClient(String hProcessClient) {
    this.hProcessClient = hProcessClient;
  }

  /**
   * @return the partDesc
   */
  public String getPartDesc() {
    return partDesc;
  }

  /**
   * @param partDesc the partDesc to set
   */
  public void setPartDesc(String partDesc) {
    this.partDesc = partDesc;
  }

  /**
   * @return the vid
   */
  public String getVid() {
    return vid;
  }

  /**
   * @param vid the vid to set
   */
  public void setVid(String vid) {
    this.vid = vid;
  }

  /**
   * @return the hMfgDate
   */
  public String gethMfgDate() {
    return hMfgDate;
  }

  /**
   * @param hMfgDate the hMfgDate to set
   */
  public void sethMfgDate(String hMfgDate) {
    this.hMfgDate = hMfgDate;
  }

  /**
   * @return the hPartNum
   */
  public String gethPartNum() {
    return hPartNum;
  }

  /**
   * @param hPartNum the hPartNum to set
   */
  public void sethPartNum(String hPartNum) {
    this.hPartNum = hPartNum;
  }

  /**
   * @return the hProdType
   */
  public String gethProdType() {
    return hProdType;
  }

  /**
   * @param hProdType the hProdType to set
   */
  public void sethProdType(String hProdType) {
    this.hProdType = hProdType;
  }

  /**
   * @return the hsize
   */
  public String getHsize() {
    return hsize;
  }

  /**
   * @param hsize the hsize to set
   */
  public void setHsize(String hsize) {
    this.hsize = hsize;
  }

  /**
   * @return the rejType
   */
  public String getRejType() {
    return rejType;
  }

  /**
   * @param rejType the rejType to set
   */
  public void setRejType(String rejType) {
    this.rejType = rejType;
  }

  /**
   * @return the alrejType
   */
  public ArrayList getAlrejType() {
    return alrejType;
  }

  /**
   * @param alrejType the alrejType to set
   */
  public void setAlrejType(ArrayList alrejType) {
    this.alrejType = alrejType;
  }

  /**
   * @return the rejReason
   */
  public String getRejReason() {
    return rejReason;
  }

  /**
   * @param rejReason the rejReason to set
   */
  public void setRejReason(String rejReason) {
    this.rejReason = rejReason;
  }

  /**
   * @return the haction
   */
  public String getHaction() {
    return haction;
  }

  /**
   * @param haction the haction to set
   */
  public void setHaction(String haction) {
    this.haction = haction;
  }

  /**
   * @return the hcurrentTab
   */
  public String getHcurrentTab() {
    return hcurrentTab;
  }

  /**
   * @param hcurrentTab the hcurrentTab to set
   */
  public void setHcurrentTab(String hcurrentTab) {
    this.hcurrentTab = hcurrentTab;
  }

  /**
   * @return the statusId
   */
  public String getStatusId() {
    return statusId;
  }

  /**
   * @param statusId the statusId to set
   */
  public void setStatusId(String statusId) {
    this.statusId = statusId;
  }

  /**
   * @return the strMode
   */
  public String getStrMode() {
    return strMode;
  }

  /**
   * @param strMode the strMode to set
   */
  public void setStrMode(String strMode) {
    this.strMode = strMode;
  }

  /**
   * @return the strApprRej
   */
  public String getStrApprRej() {
    return strApprRej;
  }

  /**
   * @param strApprRej the strApprRej to set
   */
  public void setStrApprRej(String strApprRej) {
    this.strApprRej = strApprRej;
  }

  /**
   * @return the strDisabled
   */
  public String getStrDisabled() {
    return strDisabled;
  }

  /**
   * @param strDisabled the strDisabled to set
   */
  public void setStrDisabled(String strDisabled) {
    this.strDisabled = strDisabled;
  }

  /**
   * @return the alLocations
   */
  public ArrayList getAlLocations() {
    return alLocations;
  }

  /**
   * @param alLocations the alLocations to set
   */
  public void setAlLocations(ArrayList alLocations) {
    this.alLocations = alLocations;
  }

  /**
   * @return the strMsg
   */
  public String getStrMsg() {
    return strMsg;
  }

  /**
   * @param strMsg the strMsg to set
   */
  public void setStrMsg(String strMsg) {
    this.strMsg = strMsg;
  }

  /**
   * @return the hdhrProcessAction
   */
  public String getHdhrProcessAction() {
    return hdhrProcessAction;
  }

  /**
   * @param hdhrProcessAction the hdhrProcessAction to set
   */
  public void setHdhrProcessAction(String hdhrProcessAction) {
    this.hdhrProcessAction = hdhrProcessAction;
  }

  /**
   * @return the strJSFunciton
   */
  public String getStrJSFunciton() {
    return strJSFunciton;
  }

  /**
   * @param strJSFunciton the strJSFunciton to set
   */
  public void setStrJSFunciton(String strJSFunciton) {
    this.strJSFunciton = strJSFunciton;
  }

  /**
   * @return the strbuttonName
   */
  public String getStrbuttonName() {
    return strbuttonName;
  }

  /**
   * @param strbuttonName the strbuttonName to set
   */
  public void setStrbuttonName(String strbuttonName) {
    this.strbuttonName = strbuttonName;
  }

  /**
   * @return the strRejectFl
   */
  public String getStrRejectFl() {
    return strRejectFl;
  }

  /**
   * @param strRejectFl the strRejectFl to set
   */
  public void setStrRejectFl(String strRejectFl) {
    this.strRejectFl = strRejectFl;
  }

  /**
   * @return the strAllChecked
   */
  public String getStrAllChecked() {
    return strAllChecked;
  }

  /**
   * @param strAllChecked the strAllChecked to set
   */
  public void setStrAllChecked(String strAllChecked) {
    this.strAllChecked = strAllChecked;
  }

  /**
   * @return the strSbmtBtnDisabled
   */
  public String getStrSbmtBtnDisabled() {
    return strSbmtBtnDisabled;
  }

  /**
   * @param strSbmtBtnDisabled the strSbmtBtnDisabled to set
   */
  public void setStrSbmtBtnDisabled(String strSbmtBtnDisabled) {
    this.strSbmtBtnDisabled = strSbmtBtnDisabled;
  }

  /**
   * @return the hLotString
   */
  public String gethLotString() {
    return hLotString;
  }

  /**
   * @param hLotString the hLotString to set
   */
  public void sethLotString(String hLotString) {
    this.hLotString = hLotString;
  }

  /**
   * @return the scanLotCode
   */
  public String getScanLotCode() {
    return scanLotCode;
  }

  /**
   * @param scanLotCode the scanLotCode to set
   */
  public void setScanLotCode(String scanLotCode) {
    this.scanLotCode = scanLotCode;
  }

  /**
   * @return the chkSplitRS
   */
  public String getChkSplitRS() {
    return chkSplitRS;
  }

  /**
   * @param chkSplitRS the chkSplitRS to set
   */
  public void setChkSplitRS(String chkSplitRS) {
    this.chkSplitRS = chkSplitRS;
  }

  /**
   * @return the happrString
   */
  public String getHapprString() {
    return happrString;
  }

  /**
   * @param happrString the happrString to set
   */
  public void setHapprString(String happrString) {
    this.happrString = happrString;
  }

  /**
   * @return the hrejString
   */
  public String getHrejString() {
    return hrejString;
  }

  /**
   * @param hrejString the hrejString to set
   */
  public void setHrejString(String hrejString) {
    this.hrejString = hrejString;
  }

  /**
   * @return the rejRSId
   */
  public String getRejRSId() {
    return rejRSId;
  }

  /**
   * @param rejRSId the rejRSId to set
   */
  public void setRejRSId(String rejRSId) {
    this.rejRSId = rejRSId;
  }

  /**
   * @return the hFGString
   */
  public String gethFGString() {
    return hFGString;
  }

  /**
   * @param hFGString the hFGString to set
   */
  public void sethFGString(String hFGString) {
    this.hFGString = hFGString;
  }

  /**
   * @return the hPNString
   */
  public String gethPNString() {
    return hPNString;
  }

  /**
   * @param hPNString the hPNString to set
   */
  public void sethPNString(String hPNString) {
    this.hPNString = hPNString;
  }

  /**
   * @return the hQNString
   */
  public String gethQNString() {
    return hQNString;
  }

  /**
   * @param hQNString the hQNString to set
   */
  public void sethQNString(String hQNString) {
    this.hQNString = hQNString;
  }

  /**
   * @return the hInvSplitCnt
   */
  public String gethInvSplitCnt() {
    return hInvSplitCnt;
  }

  /**
   * @param hInvSplitCnt the hInvSplitCnt to set
   */
  public void sethInvSplitCnt(String hInvSplitCnt) {
    this.hInvSplitCnt = hInvSplitCnt;
  }

  /**
   * @return the showChkFl
   */
  public String getShowChkFl() {
    return showChkFl;
  }

  /**
   * @param showChkFl the showChkFl to set
   */
  public void setShowChkFl(String showChkFl) {
    this.showChkFl = showChkFl;
  }

}
