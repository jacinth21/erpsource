/**
 * 
 */
package com.globus.operations.receiving.forms;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Anilkumar
 *
 */
public class GmReceiveShipRptForm extends GmCommonForm {
	
	private String xmlGridData="";
	private String recShipID="";
	private String fromDate="";
	private String toDate="";
	
	public String getXmlGridData() {
		return xmlGridData;
	}
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	public String getRecShipID() {
		return recShipID;
	}
	public void setRecShipID(String recShipID) {
		this.recShipID = recShipID;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

}
