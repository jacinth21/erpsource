/**
 * @author arajan
 */
package com.globus.operations.receiving.beans;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Date;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.util.GmWSUtil;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPOBulkReceiveBean extends GmBean {


  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

  GmCommonClass gmCommon = new GmCommonClass();

  public GmPOBulkReceiveBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPOBulkReceiveBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchPO - This method is used to fetch the Purchase Order details
   * 
   * @param String strPONum
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchPO(String strPONum) throws AppError {
    GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

  //  hmReturn = gmPurchaseBean.viewPO(strPONum, "", "");
   // Commented the common method viewPO to avoid calling wodetails and UDI related calls for this functionality
    hmReturn = gmPurchaseBean.viewPODetails(strPONum);
    hmReturn = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("PODETAILS"));

    if (hmReturn.size() <= 0) {
      throw new AppError("", "20698");
    }

    return hmReturn;
  }

  /**
   * fetchPartInfo - This method is used to fetch the part details
   * 
   * @param String strPartNumber
   * @return HashMap
   * @exception AppError
   **/

  public HashMap fetchPartInfo(String strPartNumber) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_rpt.gm_fch_part_info", 2);
    gmDBManager.setString(1, strPartNumber);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchExpiryDate - This method is used to get the expiry date
   * 
   * @param String strMfgDate, strShelfLife
   * @return HashMap
   * @exception AppError
   * @throws ParseException
   **/

  public String fetchExpiryDate(String strMfgDate, String strShelfLife) throws AppError,
      ParseException {
    String strExpDate = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
    Date dtMfgDate = GmCommonClass.getStringToDate(strMfgDate, strCompDateFmt);

    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_rpt.get_pnum_expdt", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setDate(2, dtMfgDate);
    gmDBManager.setString(3, strShelfLife);
    gmDBManager.execute();
    strExpDate = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strExpDate;
  }


  /**
   * fetchBatchShipment - This method is used to fetch the batch shipment details
   * 
   * @param String strRSNumber
   * @return HashMap
   * @exception AppError
   **/

  public HashMap fetchBatchShipment(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alRSHeader = new ArrayList();
    ArrayList alRSDtls = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRSNumber = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
    log.debug("strAction in Bean::" + strAction);
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_rpt.gm_fch_bulk_dhr", 4);

    gmDBManager.setString(1, strRSNumber);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(4, strAction);
    gmDBManager.execute();
    alRSHeader = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    alRSDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    hmReturn.put("RSHEADER", alRSHeader); // header details

    hmReturn.put("RSDTLS", alRSDtls); // grid details
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * saveBulkDHR to create the batch DHR and add the batch details
   * 
   * @author
   * @param HashMap
   * @return String
 * @throws ParseException 
   */
  public String saveBulkDHR(HashMap hmParam) throws AppError, Exception {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRSID = saveBulkDHR(gmDBManager, hmParam);
    gmDBManager.commit();
    return strRSID;
  }

  /**
   * saveBulkDHR overloaded method to create the batch DHR and add the batch details to call from
   * wrapper bean, which is doing commit from wrapper
   * 
   * @author
   * @param HashMap
   * @return String
 * @throws ParseException 
   */
  public String saveBulkDHR(GmDBManager gmDBManager, HashMap hmParam) throws AppError, Exception{
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strPONumber = GmCommonClass.parseNull((String) hmParam.get("PONUMBER"));
    String strPOReceivedDt = GmCommonClass.parseNull((String) hmParam.get("RECEIVEDDT"));


    String strPackSlipId = GmCommonClass.parseNull((String) hmParam.get("PACKSLIPID"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("DONORNUM"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    String strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    String strRejRSFl = GmCommonClass.parseNull((String) hmParam.get("REJRSFL"));
    
    //PMT-36506-Converting String to Date
	String strStageDate                = GmCommonClass.parseNull((String) hmParam.get("STGDT")); 
	String strDateFormat      =  getGmDataStoreVO().getCmpdfmt();
	Date dtStagedate          = GmCommonClass.getStringToDate(strStageDate,strDateFormat);
    Date dtPOReceivedDt = GmCommonClass.getStringToDate(strPOReceivedDt,strDateFormat );
   
    if (strAction.equals("SAVE_BULK_DHR")) {
      gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_txn.gm_pkg_op_rec_bulk_shipment", 10);
      gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
      gmDBManager.setString(1, strRSId);
      gmDBManager.setString(2, strPONumber);
      gmDBManager.setDate(3, dtPOReceivedDt);
      gmDBManager.setString(4, strPackSlipId);
      gmDBManager.setString(5, strDonorNum);
      gmDBManager.setString(6, strInputString);
      gmDBManager.setString(7, strUserId);
      gmDBManager.setString(8, strAction);
      gmDBManager.setString(9, strRejRSFl);
      gmDBManager.setDate(10, dtStagedate);
    } else {
      gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_txn.gm_op_edit_bulk_dhr_detail", 3);
      gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
      gmDBManager.setString(1, strRSId);
      gmDBManager.setString(2, strInputString);
      gmDBManager.setString(3, strUserId);
    }

    gmDBManager.execute();
    String strRSID = GmCommonClass.parseNull(gmDBManager.getString(1));

    return strRSID;
  }

  /**
   * saveBulkDHRInsp to create the batch DHR and add the batch details
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveBulkDHRInsp(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRSID = saveBulkDHRInsp(gmDBManager, hmParam);
    gmDBManager.commit();

    return strRSID;
  }

  /**
   * saveBulkDHRInsp overloaded method to create the batch DHR and add the batch details to call
   * from wrapper bean, which is doing commit from wrapper
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveBulkDHRInsp(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    String strRejtype = GmCommonClass.parseNull((String) hmParam.get("REJTYPE"));
    String strRejReason = GmCommonClass.parseNull((String) hmParam.get("REJREASON"));

    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_appr.gm_op_sav_bulk_dhr_appr", 5);
    gmDBManager.setString(1, strRSId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strRejtype);
    gmDBManager.setString(4, strRejReason);
    gmDBManager.setString(5, strUserId);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strRSID = GmCommonClass.parseNull(gmDBManager.getString(1));

    return strRSID;
  }

  /**
   * saveBulkDHRProcessVerify: To save the batch DHR Processing information
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveBulkDHRProcessVerify(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRSID = saveBulkDHRProcessVerify(gmDBManager, hmParam);
    gmDBManager.commit();

    return strRSID;
  }

  /**
   * saveBulkDHRProcessVerify: To save the batch DHR Processing information
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveBulkDHRProcessVerify(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    GmBulkShipmentSplitWrapperBean gmBulkShipmentSplitWrapperBeann =
        new GmBulkShipmentSplitWrapperBean(getGmDataStoreVO());
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("HDHRPROCESSACTION"));

    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_process.gm_op_sav_bulk_dhr_process", 4);
    gmDBManager.setString(1, strRSId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strAction);
    gmDBManager.setString(4, strUserId);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strRSID = GmCommonClass.parseNull(gmDBManager.getString(1));

    // If the QC check box is checked, release the inhouse txn for verification
    String strQCVerifyFl = fetchAttribValue(gmDBManager, hmParam);

    if (strQCVerifyFl.equals("Y")) {// QC Verification of Entries
      // Prepare the string for inhouse txn based on the rule value to release the inhouse txn for
      // verification
      String strInputStr = gmBulkShipmentSplitWrapperBeann.fetchReleaseTxnStr(gmDBManager, hmParam);
      // IF string is available, then release the inhouse txn for verification
      if (!strInputStr.equals("")) {
        hmParam.put("HINPUTSTRING", strInputStr);
        hmParam.put("SKIPSHIPMENT", "Y");
        saveBulkDHRVerifyRelease(gmDBManager, hmParam);
      }
    }

    return strRSID;
  }

  /**
   * saveBulkDHRVerifyRelease: To save the batch DHR verify information
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveBulkDHRVerifyRelease(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRSID = saveBulkDHRVerifyRelease(gmDBManager, hmParam);
    gmDBManager.commit();
    return strRSID;
  }

  /**
   * saveBulkDHRVerifyRelease: To save the batch DHR verify information
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveBulkDHRVerifyRelease(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("HDHRPROCESSACTION"));
    String strSkipShpmtFl = GmCommonClass.parseNull((String) hmParam.get("SKIPSHIPMENT"));

    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_verify.gm_op_sav_bulk_dhr_verify", 5);
    gmDBManager.setString(1, strRSId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strAction);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strSkipShpmtFl);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strRSID = GmCommonClass.parseNull(gmDBManager.getString(1));

    return strRSID;
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String generateOutPut(HashMap hmParam) throws AppError {
    String strTemplateNm = "";
    String strIsApprRej = "";
    String strDhrStatus = "";
    ArrayList alGridData = new ArrayList();
    ArrayList alApprRej = new ArrayList();

    strTemplateNm = GmCommonClass.parseNull((String) hmParam.get("TEMPLATENAME"));
    strIsApprRej = GmCommonClass.parseNull((String) hmParam.get("ISAPPR"));
    strDhrStatus = GmCommonClass.parseNull((String) hmParam.get("DHRSTATUS"));
    alGridData = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRSDTLS"));
    alApprRej = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALAPPRREJ"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    HashMap hmParamv = new HashMap();
    hmParamv.put("ALAPPRREJ", alApprRej);
    hmParamv.put("ISAPPR", strIsApprRej);
    hmParamv.put("DHRSTATUS", strDhrStatus);
    hmParamv.put("RELCHKACCESS", GmCommonClass.parseNull((String) hmParam.get("RELCHKACCESS")));
    templateUtil.setDataMap("hmParam", hmParamv);
    templateUtil.setDataList("alResult", alGridData);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/receiving/templates");
    templateUtil.setTemplateName(strTemplateNm);
    return templateUtil.generateOutput();
  }

  /**
   * Fetch receive ship rpt.
   * 
   * @param hmParam the hm param
   * @return the array list
   * @throws AppError the app error
   */
  public ArrayList fetchReceiveShipRpt(HashMap hmParam) throws AppError {
    ArrayList alReturn = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    String strReport = GmCommonClass.parseNull((String) hmParam.get("REPORT"));
    String strRsID = GmCommonClass.parseNull((String) hmParam.get("RECSHIPID"));
    String strfromdate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strtodate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strCompnayId = GmCommonClass.parseNull(getCompId());

    sbQuery.append(" SELECT t4081.c4081_dhr_bulk_id dhr_bulk_id ,");
    sbQuery.append(" t4081.c4081_packing_slip pack_slip ,");
    sbQuery
        .append(" t2540.c2540_donor_number donor_number , get_vendor_name(t2540.c301_vendor_id) vendnm,");
    sbQuery.append(" v4081a.form_42 form_42 ,");
    sbQuery.append(" v4081a.labels_printed labels_printed ,");
    sbQuery.append(" v4081a.qc_verified qc_verified ,");
    sbQuery.append(" gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status(t4081.c4081_dhr_bulk_id) status ,");
    sbQuery
        .append(" gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status_name(t4081.c4081_dhr_bulk_id) status_name ,");
    sbQuery.append(" t4081.c4081_received_date received_date ,");
    sbQuery.append(" get_user_name(t4081.c4081_last_updated_by) updated_by ,");
    sbQuery.append(" t4081.c4081_last_updated_date updated_dt ,");
    sbQuery
        .append(" part_count ,  qty_received ,  Qty_Rejected, get_log_flag (t4081.c4081_dhr_bulk_id, 4000697) bkdhrlog");
    sbQuery.append(" FROM t4081_dhr_bulk_receive t4081 ,");
    sbQuery.append(" v4081a_dhr_bulk_attribute v4081a ,");
    sbQuery.append(" t2540_donor_master t2540 ,");
    sbQuery.append(" ( SELECT c4081_dhr_bulk_id ,");
    sbQuery.append(" COUNT(t408.c205_part_number_id) part_count ,");
    sbQuery.append(" SUM(t408.c408_qty_received) qty_received ,");
    sbQuery.append(" SUM(t408.c408_qty_rejected) qty_rejected");
    sbQuery.append(" FROM t4082_bulk_dhr_mapping t4082 ,    t408_dhr t408");
    sbQuery.append(" WHERE t408.c408_dhr_id   = t4082.c408_dhr_id");
    if (!strReport.equals("")) {
      sbQuery.append(" AND t408.c408_status_fl != 4");
    }
    sbQuery.append(" AND t4082.c4082_void_fl IS NULL");
    sbQuery.append(" AND t408.c408_void_fl   IS NULL  AND t408.c1900_company_id = " + strCompnayId
        + " ");
    sbQuery.append(" GROUP BY c4081_dhr_bulk_id ) dhr");
    sbQuery.append(" WHERE t4081.c4081_dhr_bulk_id  = v4081a.c4081_dhr_bulk_id (+)");
    sbQuery.append(" AND t4081.c2540_donor_id       = t2540.c2540_donor_id(+)");
    sbQuery.append(" AND t4081.c4081_dhr_bulk_id    = dhr.c4081_dhr_bulk_id");
    sbQuery.append(" AND t4081.c1900_company_id =  " + strCompnayId + " ");
    if (!strRsID.equals("")) {
      sbQuery.append(" AND t4081.c4081_dhr_bulk_id ='" + strRsID + "'");
    }
    if (!strfromdate.equals("")) {
      sbQuery.append(" AND t4081.c4081_received_date >= To_DATE('" + strfromdate + "', '"
          + getCompDateFmt() + "')");
    }
    if (!strtodate.equals("")) {
      sbQuery.append(" AND t4081.c4081_received_date <= to_date('" + strtodate + "', '"
          + getCompDateFmt() + "')");
    }

    log.debug("fetchReceiveShipRpt query is   " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    if (!strRsID.equals("") && alReturn.size() <= 0) {
      throw new AppError("", "20699");
    }

    return alReturn;
  }

  /**
   * Fetch Bulk Dhr Attribute. The Below method is used to fetch the Recently saved DHR parameter.
   * 
   * @param hmParam the hm param
   * @return the array list
   * @throws AppError the app error
   */
  public ArrayList fetchBulkDhrAttribute(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strDhrBulkId = GmCommonClass.parseNull((String) hmParam.get("RSID"));
    String struserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_rpt.gm_fch_bulk_dhr_attr", 3);
    gmDBManager.setString(1, strDhrBulkId);
    gmDBManager.setString(2, struserId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * saveBulkDhrAttribute The Below method is used to save the data for DHR Attribute .
   * 
   * @param hmParam the hm param
   * @return the array list
   * @throws AppError the app error
   */
  public void saveBulkDhrAttribute(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveBulkDhrAttribute(gmDBManager, hmParam);
    gmDBManager.commit();
  }

  /**
   * saveBulkDhrAttribute The Below method is used to save the data for DHR Attribute .
   * 
   * @param hmParam the hm param
   * @return the array list
   * @throws AppError the app error
   */
  public void saveBulkDhrAttribute(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strinputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strAttrtype = GmCommonClass.parseNull((String) hmParam.get("ATTRTYPE"));
    String strrsid = GmCommonClass.parseNull((String) hmParam.get("RSID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_txn.gm_sav_bulk_dhr_attr", 4);
    gmDBManager.setString(1, strrsid);
    gmDBManager.setString(2, strAttrtype);
    gmDBManager.setString(3, strinputStr);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
  }

  /**
   * fetchDonorNumCnt - This method is used to get the count of donor number
   * 
   * @param String strDonorNum, strVendorId
   * @return String
   * @exception AppError
   * @throws ParseException
   **/

  public String fetchDonorNumCnt(String strDonorNum, String strVendorId) throws AppError,
      ParseException {
    String strDonorNumCnt = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_rpt.get_donor_num_cnt", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.INTEGER);
    gmDBManager.setString(2, strDonorNum);
    gmDBManager.setString(3, strVendorId);
    gmDBManager.execute();
    strDonorNumCnt = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strDonorNumCnt;
  }

  /**
   * fetchDhrBulkAtrrVal - This method is used to get the attribute values of DHR Bulk id
   * 
   * @param String strRsNum
   * @return String
   * @exception AppError
   **/
  public String fetchDhrBulkAtrrVal(String strRsNum) throws AppError {
    String strAllChecked = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_attr_checked", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strRsNum);
    gmDBManager.execute();
    strAllChecked = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strAllChecked;
  }

  /**
   * getBulkDhrStatus - This method is used to get the DHR status
   * 
   * @param String strRsNum
   * @return String
   * @exception AppError
   **/
  public String getBulkDhrStatus(String strRsNum) throws AppError {
    String strDhrStatus = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_rpt.get_bulk_dhr_status", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strRsNum);
    gmDBManager.execute();
    strDhrStatus = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strDhrStatus;
  }

  /**
   * fetchLotNumberCnt - This method is used to get the count of Lot number to verify for Duplicate
   * Lots
   * 
   * @param String strLotNumber
   * @return String
   * @exception AppError
   * @throws ParseException
   **/

  public String fetchLotNumberCnt(String strLotNumber) throws AppError, ParseException {
    String strDonorNumCnt = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_rpt.get_lot_num_cnt", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strLotNumber);
    gmDBManager.execute();
    strDonorNumCnt = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strDonorNumCnt;
  }

  /**
   * fetchinvalidLots This method will results the Duplicate Lots among the lotnumberString
   * 
   * @param String hmParams
   * @return String
   * @exception AppError
   */
  public String fetchDuplicateLots(HashMap hmParams) throws AppError {
    String strinvalidLots = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_rpt.gm_fch_duplicate_lots", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("LOTNUMBERSTRING")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("RSID")));
    gmDBManager.execute();
    strinvalidLots = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();
    return strinvalidLots;
  } // End of fetchinvalidLots

  /**
   * fetchAttribValue This method is to get the attribute value from T4081A_DHR_BULK_ATTRIBUTE table
   * 
   * @param GmDBManager hmParams
   * @return String
   * @exception AppError
   */
  public String fetchAttribValue(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    String strQCVerificationFl = "";
    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_rpt.get_qc_verification_fl", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("RSNUMBER")));
    gmDBManager.execute();
    strQCVerificationFl = GmCommonClass.parseNull(gmDBManager.getString(1));
    return strQCVerificationFl;
  }

}
