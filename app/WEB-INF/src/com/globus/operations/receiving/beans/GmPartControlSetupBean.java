package com.globus.operations.receiving.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPartControlSetupBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  public GmPartControlSetupBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmPartControlSetupBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }


  /**
   * loadPartControlInfo - This method will fetch the Control Number details for pa part.
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/

  public HashMap loadPartControlInfo(String strPartCNId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_op_partcontrol_rpt.gm_fch_part_control_info", 2);
    gmDBManager.setString(1, strPartCNId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * savePartControlInfo - This method will save Control Number, Vendor, DOnor , Expairy and
   * Manfacture date details.
   * 
   * @param HashMap hmParam
   * @return String
   * @exception AppError
   **/

  public String savePartControlInfo(HashMap hmParam) throws AppError {
    String strPartCnId = GmCommonClass.parseNull((String) hmParam.get("PARTCNID"));
    String strPartNM = GmCommonClass.parseNull((String) hmParam.get("PARTNM"));
    String srtrControlNM = GmCommonClass.parseNull((String) hmParam.get("CONTROLNM"));
    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    String strDonorId = GmCommonClass.parseNull((String) hmParam.get("DONORNM"));
    String strExpDt = GmCommonClass.parseNull((String) hmParam.get("EXPDATE"));
    String strManfDate = GmCommonClass.parseNull((String) hmParam.get("MANFDATE"));
    String strRevLevel = GmCommonClass.parseNull((String) hmParam.get("REVLEVEL"));
    String strCustSize = GmCommonClass.parseNull((String) hmParam.get("CUSTOMSIZE"));
    String strPackSlip = GmCommonClass.parseNull((String) hmParam.get("PACKSLIP"));
    strDonorId = strDonorId.equals("0") ? "" : strDonorId;

    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUDI = GmCommonClass.parseNull((String) hmParam.get("UDINO"));
    String strUdiLogEntry = GmCommonClass.parseNull((String) hmParam.get("UDI_LOG_ENTRY"));
    GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_partcontrol_txn.gm_save_part_control_info", 12);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.setString(1, strPartCnId);
    gmDBManager.setString(2, strPartNM);
    gmDBManager.setString(3, srtrControlNM);
    gmDBManager.setString(4, strCustSize);
    gmDBManager.setString(5, strVendorId);
    gmDBManager.setString(6, strManfDate);
    gmDBManager.setString(7, strDonorId);
    gmDBManager.setString(8, strExpDt);
    gmDBManager.setString(9, strUserId);
    gmDBManager.setString(10, strPackSlip);
    gmDBManager.setString(11, strRevLevel);
    gmDBManager.setString(12, strUDI);

    gmDBManager.execute();
    strPartCnId = GmCommonClass.parseNull(gmDBManager.getString(1));
    if (!strLog.equals("")) {
      gmCom.saveLog(gmDBManager, strPartCnId, strLog, strUserId, "1295");
    }
    if (!strUdiLogEntry.equals(""))/*When will edit  the udi value that time saved to log table*/
      {
        gmCom.saveLog(gmDBManager, strPartCnId, strUdiLogEntry, strUserId, "106780");
      }
    gmDBManager.commit();
    return strPartCnId;
  }

  /**
   * fetchPartNumberInfo - To fetch part No details.
   * 
   * @param String
   * @return String
   * @throws AppError
   */
  public String fetchPartNumberInfo(String strPartNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_op_partcontrol_rpt.gm_fch_part_no", 2);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strPartNum = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strPartNum;
  }
  
  /**
   * fetchPartCtrlNumId - To fetch the part control number id for the lot and part combination.
   * 
   * @param hashmap
   * @return String
   * @throws AppError
   */
  public String fetchPartCtrlNumId(HashMap hmParam) throws AppError {
	    String strPartNM = GmCommonClass.parseNull((String) hmParam.get("PARTNM"));
	    String srtrControlNM = GmCommonClass.parseNull((String) hmParam.get("CONTROLNM"));
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_partcontrol_rpt.get_part_ctrlnum_id", 3);
	    gmDBManager.setString(1, strPartNM);
	    gmDBManager.setString(2, srtrControlNM);
	    gmDBManager.registerOutParameter(3,java.sql.Types.CHAR);
	    gmDBManager.execute();
	    String strPartCnId = GmCommonClass.parseNull(gmDBManager.getString(3));
	    gmDBManager.close();
	    return strPartCnId;
	  }
  /**
   * saveLotOverrideByTxn - To fetch the validate transaction id(s) and save part details to t2500 table
   * 
   * @param hashmap
   * @return String
   * @throws AppError
   */
  public String saveLotOverrideByTxn(HashMap hmParam) throws AppError {
	  
	    String strLotOverrideTxnIds = GmCommonClass.parseNull((String) hmParam.get("LOTOVERRIDETXNIDS"));
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANTID"));
	    String strTxnIds = "";
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_partcontrol_txn.gm_sav_bulk_lot_override_by_txn", 4);	    
	    gmDBManager.setString(1, strLotOverrideTxnIds);
	    gmDBManager.setString(2, strCompanyId);
	    gmDBManager.setString(3, strUserId);
	    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);	    

	    gmDBManager.execute();
	    strTxnIds = GmCommonClass.parseNull(gmDBManager.getString(4));
	    gmDBManager.commit();
	    
	    return strTxnIds;
	  }
}
