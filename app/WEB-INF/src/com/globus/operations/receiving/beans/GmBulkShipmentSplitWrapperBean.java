/**
 * This file is to check whether the shipment/DHR has to be split or not and perform the operation
 * based on that
 * 
 * @author arajan
 */
package com.globus.operations.receiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmBulkShipmentSplitWrapperBean extends GmBean {


  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

  GmCommonClass gmCommon = new GmCommonClass();

  public GmBulkShipmentSplitWrapperBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmBulkShipmentSplitWrapperBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveBulkDHRInspSplit - To save the shipment details based on the split checkbox while doing
   * Inspection
   * 
   * @param HashMap hmParam
   * @return String
 * @throws Exception 
   **/
  public String saveBulkDHRInspSplit(HashMap hmParam) throws Exception {
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());

    String strSplitChkFl = GmCommonClass.parseNull((String) hmParam.get("CHKSPLITRS"));
    String strApprString = GmCommonClass.parseNull((String) hmParam.get("HAPPRSTRING"));
    String strRejString = GmCommonClass.parseNull((String) hmParam.get("HREJSTRING"));
    String strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDHRId = "";
    String strRejRSId = "";
    HashMap hmPODtls = new HashMap();
    HashMap hmDHRList = new HashMap();

    ArrayList alDHRList = new ArrayList();
    // If split check box is checked, then need to split the shipment into two
    if (strSplitChkFl.equals("on")) {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      hmParam.put("HINPUTSTRING", strApprString);
      alDHRList = fetchDHRFromShipment(gmDBManager, strRSId);

      /*
       * For approved string do the following 1. Void all the DHR's which were created for the
       * shipment 2. Save the approved sting in the temp table 3. Create new DHR ids for the
       * approved string 4. Call method to void the old DHR-RS mapping 5. Update the approved string
       * with new DHR id 6. Call the method to create the batch DHR and add the batch details
       */

      // Void all the DHR id in the RS
      for (int i = 0; i < alDHRList.size(); i++) {
        hmDHRList = GmCommonClass.parseNullHashMap((HashMap) alDHRList.get(i));
        strDHRId = GmCommonClass.parseNull((String) hmDHRList.get("DHRID"));
        updateDHRforRollback(gmDBManager, strDHRId, strUserId);
        updateDHRforRS(gmDBManager, strDHRId, hmParam);
      }

      hmPODtls = fetchPOId(gmDBManager, strRSId);
      saveTempDHRString(gmDBManager, hmParam, strApprString);

      // To create DHR transaction
      saveBulkDHRDtls(gmDBManager, hmParam, (String) hmPODtls.get("VENDORID"),
          (String) hmPODtls.get("DONORID"));

      strApprString = updateInputStr(gmDBManager, hmParam, strApprString, strRSId);

      hmParam.put("HINPUTSTRING", strApprString);
      strRSId = gmPOBulkReceiveBean.saveBulkDHRInsp(gmDBManager, hmParam);

      /*
       * For rejected string do the following 1. Create new shipment for the rejected parts with
       * prefix RSRJ 2. Update the rejected string with new DHR id 3. Call the method to create the
       * batch DHR and add the batch details
       */

      hmParam.put("HINPUTSTRING", strRejString);
      hmParam.put("HACTION", "SAVE_BULK_DHR");
      hmParam.put("RSNUMBER", "");
      hmParam.put("REJRSFL", "Y");
      strRejRSId = gmPOBulkReceiveBean.saveBulkDHR(gmDBManager, hmParam);
      hmParam.put("RSNUMBER", strRejRSId);

      strRejString = updateInputStr(gmDBManager, hmParam, strRejString, strRejRSId);

      hmParam.put("HINPUTSTRING", strRejString);
      strRejRSId = gmPOBulkReceiveBean.saveBulkDHRInsp(gmDBManager, hmParam);

      saveLinkedRS(gmDBManager, strRSId, strRejRSId, hmParam);

      gmDBManager.commit();
    } else {
      // If not splitting, then call the existing method to save the approve/reject details
      strRSId = gmPOBulkReceiveBean.saveBulkDHRInsp(hmParam);
    }

    return strRejRSId;
  }

  /**
   * saveBulkDHRProcessVerifySplit - To Process the shipment based on the split
   * 
   * @param HashMap hmParam
   * @return String
   * @exception AppError
   **/
  public String saveBulkDHRProcessVerifySplit(HashMap hmParam) throws AppError {
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    ArrayList alDHRList = new ArrayList();
    HashMap hmDHRList = new HashMap();
    HashMap hmPODtls = new HashMap();
    String strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strFGString = GmCommonClass.parseNull((String) hmParam.get("HFGSTRING"));
    String strQNString = GmCommonClass.parseNull((String) hmParam.get("HQNSTRING"));
    String strPNString = GmCommonClass.parseNull((String) hmParam.get("HPNSTRING"));
    String strInputString = "";
    int intSplitCnt =
        Integer.parseInt((GmCommonClass.parseZero((String) hmParam.get("HINVSPLITCNT"))));
    String strDHRId = "";

    // if the user select more than one inventory in the screen, then only need to split the DHR.
    // Otherwise dont split
    if (intSplitCnt <= 1) {
      strRSId = gmPOBulkReceiveBean.saveBulkDHRProcessVerify(hmParam);
    } else {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      alDHRList = fetchDHRFromShipment(gmDBManager, strRSId);
      // Void all the DHR id in the RS
      for (int i = 0; i < alDHRList.size(); i++) {
        hmDHRList = GmCommonClass.parseNullHashMap((HashMap) alDHRList.get(i));
        strDHRId = GmCommonClass.parseNull((String) hmDHRList.get("DHRID"));
        updateDHRforRollback(gmDBManager, strDHRId, strUserId);
        updateDHRforRS(gmDBManager, strDHRId, hmParam);
      }

      hmPODtls = fetchPOId(gmDBManager, strRSId);

      /*
       * For each string FG,QN,PN. Do the below to create new DHR 1. Save the values in String to
       * temp table 2. Create new DHR 3. Update the string with new DHR details
       */

      // For FG string
      if (!strFGString.equals("")) {
        strFGString = splitInhouseTxnDHR(gmDBManager, hmParam, hmPODtls, strFGString, strRSId);
        strInputString = strInputString + strFGString;
      }

      // For QN String
      if (!strQNString.equals("")) {
        strQNString = splitInhouseTxnDHR(gmDBManager, hmParam, hmPODtls, strQNString, strRSId);
        strInputString = strInputString + strQNString;
      }

      // For PN String
      if (!strPNString.equals("")) {
        strPNString = splitInhouseTxnDHR(gmDBManager, hmParam, hmPODtls, strPNString, strRSId);
        strInputString = strInputString + strPNString;
      }
      hmParam.put("HINPUTSTRING", strInputString);
      strRSId = gmPOBulkReceiveBean.saveBulkDHRProcessVerify(gmDBManager, hmParam);
      gmDBManager.commit();
    }

    return strRSId;
  }

  /**
   * saveReleaseBulkDHRInhouseTxn - To create new DHR for each transaction 'QC Verification of
   * Entries' checkbox
   * 
   * @param gmDBManager, hmParam, hmPODtls, strTxnString,strRSId
   * @return String
   * @exception AppError
   **/
  public String splitInhouseTxnDHR(GmDBManager gmDBManager, HashMap hmParam, HashMap hmPODtls,
      String strTxnString, String strRSId) throws AppError {

    saveTempDHRString(gmDBManager, hmParam, strTxnString);
    updateTempDHRStatus(gmDBManager, hmParam);

    // To create DHR transaction for FG String
    saveBulkDHRDtls(gmDBManager, hmParam, (String) hmPODtls.get("VENDORID"),
        (String) hmPODtls.get("DONORID"));

    strTxnString = updateInputStr(gmDBManager, hmParam, strTxnString, strRSId);
    saveDHRLotStatus(gmDBManager, hmParam);
    return strTxnString;
  }

  /**
   * saveReleaseBulkDHRInhouseTxn - To release the inhouse txn to inhouse dash while checking the
   * 'QC Verification of Entries' checkbox
   * 
   * @param HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void saveReleaseBulkDHRInhouseTxn(HashMap hmParam) throws AppError {
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strParamStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    hmParam.put("RSNUMBER", GmCommonClass.parseNull((String) hmParam.get("RSID")));
    gmPOBulkReceiveBean.saveBulkDhrAttribute(gmDBManager, hmParam);// Saving the Attribute Values.

    if (strParamStr.indexOf("104520") >= 0) {// QC Verification of Entries
      // Prepare the string for inhouse txn based on the rule value to release the inhouse txn for
      // verification
      String strInputStr = fetchReleaseTxnStr(gmDBManager, hmParam);
      // IF string is available, then release the inhouse txn for verification
      if (!strInputStr.equals("")) {
        hmParam.put("HINPUTSTRING", strInputStr);
        hmParam.put("SKIPSHIPMENT", "Y");
        gmPOBulkReceiveBean.saveBulkDHRVerifyRelease(gmDBManager, hmParam);
      }
    }
    gmDBManager.commit();
  }

  /**
   * fetchDHRFromShipment - To get all the DHR id's from existing shipment
   * 
   * @param GmDBManager gmDBManager, String strRSId
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchDHRFromShipment(GmDBManager gmDBManager, String strRSId) throws AppError {
    ArrayList alReturn = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_split_txn.gm_fch_rs_dhr_list", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRSId);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    return alReturn;
  }

  /**
   * fetchPOId - To get all the details from PO
   * 
   * @param GmDBManager gmDBManager, String strRSId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchPOId(GmDBManager gmDBManager, String strRSId) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_split_txn.gm_fch_rs_txn_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRSId);
    gmDBManager.execute();
    HashMap hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    return hmResult;
  }

  /**
   * updateDHRforRollback - To rollback the DHR
   * 
   * @param GmDBManager gmDBManager, String strDHRId, String strUserName
   * @return
   * @exception AppError
   **/
  public void updateDHRforRollback(GmDBManager gmDBManager, String strDHRId, String strUserName)
      throws AppError {

    gmDBManager.setPrepareString("GM_UPDATE_DHR_ROLLBACK", 5);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDHRId);
    gmDBManager.setString(2, "UpdateVoid");
    gmDBManager.setString(3, strUserName);
    gmDBManager.execute();
  }

  /**
   * saveTempDHRString - To save the input string in temp table TEMP_BULK_DHR_TABLE
   * 
   * @param GmDBManager gmDBManager, HashMap hmPODtls, String strApprString
   * @return
   * @exception AppError
   **/
  public void saveTempDHRString(GmDBManager gmDBManager, HashMap hmPODtls, String strApprString)
      throws AppError {
    String strPOId = GmCommonClass.parseNull((String) hmPODtls.get("PONUMBER"));
    // Save the input string in temp table TEMP_BULK_DHR_TABLE
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_txn.gm_sav_dhr_input_str_tmp", 2);
    gmDBManager.setString(1, strPOId);
    gmDBManager.setString(2, strApprString);
    gmDBManager.execute();
  }

  /**
   * updateTempDHRStatus - To save the lot status in temp table to update the t408a table later
   * 
   * @param GmDBManager gmDBManager, HashMap hmParam
   * @return
   * @exception AppError
   **/
  public void updateTempDHRStatus(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_split_txn.gm_upd_dhr_status_temp", 1);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("RSNUMBER")));
    gmDBManager.execute();
  }

  /**
   * saveBulkDHRDtls - To Create DHR's for the received shipment
   * 
   * @param GmDBManager gmDBManager, HashMap hmPODtls
   * @param String strVendorId, String strDonorId
   * @return
   * @exception AppError
   **/
  public void saveBulkDHRDtls(GmDBManager gmDBManager, HashMap hmPODtls, String strVendorId,
      String strDonorId) throws AppError {
    // Create DHR's for the received shipment
    String strUserId = GmCommonClass.parseNull((String) hmPODtls.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_txn.gm_op_sav_bulk_dhr_detail", 7);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmPODtls.get("RSNUMBER")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmPODtls.get("PONUMBER")));
    gmDBManager.setString(3, GmCommonClass.parseNull(strVendorId));
    gmDBManager.setInt(4, Integer.parseInt(GmCommonClass.parseNull(strDonorId)));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmPODtls.get("PACKSLIPID")));
    gmDBManager.setString(6, "");
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmPODtls.get("USERID")));
    gmDBManager.execute();
  }

  /**
   * updateInputStr - To update the input string with new DHR details
   * 
   * @param GmDBManager gmDBManager, HashMap hmParam
   * @param String strInpStr, String strRSId
   * @return String
   * @exception AppError
   **/
  public String updateInputStr(GmDBManager gmDBManager, HashMap hmParam, String strInpStr,
      String strRSId) throws AppError {
    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_split_txn.gm_upd_dhr_input_str_tmp", 3);
    gmDBManager.registerOutParameter(1, OracleTypes.CLOB);
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("PONUMBER")));
    gmDBManager.setString(3, strInpStr);
    gmDBManager.setString(4, strRSId);
    gmDBManager.execute();
    String strInputString = GmCommonClass.parseNull(gmDBManager.getString(1));
    return strInputString;
  }

  /**
   * saveLinkedRS - To update the comment with linked RS in the parent RS
   * 
   * @param GmDBManager gmDBManager, HashMap hmParam, String strRSId, String strRejRSId
   * @return
   * @exception AppError
   **/
  public void saveLinkedRS(GmDBManager gmDBManager, String strRSId, String strRejRSId,
      HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_split_txn.gm_op_sav_linked_rs_detail", 3);
    gmDBManager.setString(1, strRSId);
    gmDBManager.setString(2, strRejRSId);
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.execute();
  }

  /**
   * updateDHRforRS - To void the Old DHR-RS mapping, while voiding the DHR
   * 
   * @param GmDBManager gmDBManager, HashMap hmParam
   * @param String strInpStr, String strRSId
   * @return String
   * @exception AppError
   **/
  public void updateDHRforRS(GmDBManager gmDBManager, String strDHRId, HashMap hmParam)
      throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_split_txn.gm_op_sav_void_rsdhr", 3);
    gmDBManager.setString(1, strDHRId);
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("RSNUMBER")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.execute();
  }

  /**
   * fetchReleaseTxnStr - Prepare the input string of the txns which needs to be released
   * 
   * @param GmDBManager gmDBManager, HashMap hmParam
   * @return String
   * @exception AppError
   **/
  public String fetchReleaseTxnStr(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_split_txn.gm_fch_dhr_split_txn_str", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("RSNUMBER")));
    gmDBManager.execute();
    String strInputString = GmCommonClass.parseNull(gmDBManager.getString(2));
    return strInputString;
  }

  /**
   * saveDHRLotStatus - Save the lot status in t408a table, since it is not updated for new dhr
   * 
   * @param GmDBManager gmDBManager, HashMap hmParam
   * @return String
   * @exception AppError
   **/
  public void saveDHRLotStatus(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_split_txn.gm_upd_dhr_lot_sts", 1);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("RSNUMBER")));
    gmDBManager.execute();
  }
}
