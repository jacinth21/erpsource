/**
 * @author Anilkumar
 */
package com.globus.operations.receiving.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmReservedLogRptBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmReservedLogRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmReservedLogRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public ArrayList reportReservedDetails(HashMap hmParam) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager();
    StringBuffer sbQuery = new StringBuffer();

    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strLotNum = GmCommonClass.parseNull((String) hmParam.get("LOTNUM"));
    String strTransId = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    String strInvType = GmCommonClass.parseNull((String) hmParam.get("INVENTORYTYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strTodate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));

    sbQuery.append(" SELECT TO_CHAR(T5061.C5061_CREATED_DATE,'" + getGmDataStoreVO().getCmpdfmt()
        + "' || ' HH24:MI:SS') TRANS_DT,");
    sbQuery.append(" T5061.C205_PART_NUMBER_ID PNUM,");
    sbQuery.append(" T205.C205_PART_NUM_DESC PDESC,");
    sbQuery.append(" T5061.C5061_ORIG_QTY ORG_QTY,");
    sbQuery.append(" T5061.C5061_TXN_QTY TRANS_QTY,");
    sbQuery.append(" T5061.C5061_NEW_QTY NEW_QTY,");
    sbQuery.append(" T5061.C5060_CONTROL_NUMBER LOT_NUM,");
    sbQuery.append(" GET_CODE_NAME(T5061.C901_WAREHOUSE_TYPE) INVTYPE_NM,");
    sbQuery.append(" GET_CODE_NAME(T5061.C901_LAST_TRANSACTION_TYPE) TTTYPE_NM,");
    sbQuery.append(" T5061.C901_LAST_TRANSACTION_TYPE TTYPE_ID, ");
    sbQuery.append(" T5061.C5061_LAST_UPDATE_TRANS_ID TRANS_ID,");
    sbQuery.append(" GET_USER_NAME(T5061.C5061_CREATED_BY) POSTD_BY");
    sbQuery.append(" FROM T5061_CONTROL_NUMBER_INV_LOG T5061,");
    sbQuery.append(" T205_PART_NUMBER T205");
    sbQuery.append(" WHERE T5061.C205_PART_NUMBER_ID   = T205.C205_PART_NUMBER_ID");
    if (!strPartNum.equals("")) {
      strPartNum = GmCommonClass.getStringWithQuotes(strPartNum);

      sbQuery.append(" AND T5061.C205_PART_NUMBER_ID IN ('");
      sbQuery.append(strPartNum);
      sbQuery.append("')");
    }
    if (!strLotNum.equals("")) {
      sbQuery.append(" AND T5061.C5060_CONTROL_NUMBER ='");
      sbQuery.append(strLotNum);
      sbQuery.append("'");
    }
    if (!strTransId.equals("")) {
      sbQuery.append(" AND T5061.C5061_LAST_UPDATE_TRANS_ID ='");
      sbQuery.append(strTransId);
      sbQuery.append("'");
    }
    if (!strFromDt.equals("")) {
      sbQuery.append(" AND TRUNC(T5061.C5061_CREATED_DATE) >= TO_DATE('");
      sbQuery.append(strFromDt);
      sbQuery.append("', GET_RULE_VALUE('DATEFMT','DATEFORMAT'))");
    }
    if (!strTodate.equals("")) {
      sbQuery.append(" AND TRUNC(T5061.C5061_CREATED_DATE) <= TO_DATE('");
      sbQuery.append(strTodate);
      sbQuery.append("', GET_RULE_VALUE('DATEFMT','DATEFORMAT'))");
    }
    if (!strInvType.equals("") && !strInvType.equals("0")) {
      sbQuery.append(" AND T5061.C901_WAREHOUSE_TYPE ='");
      sbQuery.append(strInvType);
      sbQuery.append("'");
    }
    sbQuery.append(" AND T5061.C1900_COMPANY_ID = '" + getGmDataStoreVO().getCmpid() + "'");
    sbQuery.append(" AND T5061.C5040_PLANT_ID = '" + getGmDataStoreVO().getPlantid() + "'");
    sbQuery.append(" ORDER BY T5061.C205_PART_NUMBER_ID,");
    sbQuery.append(" T5061.C5061_LAST_UPDATE_TRANS_ID");
    log.debug("reportReservedDetails =======> " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }
}
