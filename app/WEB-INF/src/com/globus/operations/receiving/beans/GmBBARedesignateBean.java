package com.globus.operations.receiving.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBBARedesignateBean extends GmBean {

  public GmBBARedesignateBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadAccoutDetails - This Method is written for fetch Re-designation parts
   * 
   * @author Dsandeep
   * 
   * */

  public ArrayList gmfetchRedesignateDash() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_partlabeling_rpt.gm_fch_resdesignate_ptrds", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    log.debug("alResult--" + alReturn);
    gmDBManager.close();
    return alReturn;

  }

}
