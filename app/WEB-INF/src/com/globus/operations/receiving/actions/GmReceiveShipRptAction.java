/**
 * 
 */
package com.globus.operations.receiving.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.beans.GmPOBulkReceiveBean;
import com.globus.operations.receiving.forms.GmReceiveShipRptForm;

/**
 * @author Anilkumar
 * 
 */
public class GmReceiveShipRptAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmReceiveShipRptForm gmReceiveShipRptForm = (GmReceiveShipRptForm) actionForm;
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strAction = "";
    String strxmlGridData = "";
    String strApplDateFmt = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    gmReceiveShipRptForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);

    strApplDateFmt = GmCommonClass.parseNull(gmReceiveShipRptForm.getApplnDateFmt());
    strAction = GmCommonClass.parseNull(gmReceiveShipRptForm.getHaction());
    hmParam = GmCommonClass.getHashMapFromForm(gmReceiveShipRptForm);

    if (strAction.equals("load")) {
      alReturn = GmCommonClass.parseNullArrayList(gmPOBulkReceiveBean.fetchReceiveShipRpt(hmParam));
      hmReturn.put("alReturn", alReturn);
      hmReturn.put("DATEFMT", strApplDateFmt);
      hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);

      strxmlGridData = generateOutPut(hmReturn, "GmReceiveShipRpt.vm");
      gmReceiveShipRptForm.setXmlGridData(strxmlGridData);
    }
    return mapping.findForward("GmReceiveShipRpt");
  }

  /**
   * Generate out put.
   * 
   * @param hmReturn,vmFile
   * @return the string
   * @throws AppError the app error
   */
  private String generateOutPut(HashMap hmReturn, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmReturn.get("STRSESSCOMPANYLOCALE"));

    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.receiving.GmReceiveShipmentRpt", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/receiving/templates");
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }

}
