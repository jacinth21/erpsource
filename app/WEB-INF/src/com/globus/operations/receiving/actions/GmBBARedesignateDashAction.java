package com.globus.operations.receiving.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.beans.GmBBARedesignateBean;
import com.globus.operations.receiving.forms.GmBBARedesignateDashForm;
import com.globus.operations.receiving.thbreceiving.beans.GmDonorScheduleBean;
import com.globus.operations.receiving.thbreceiving.forms.GmBBAStagingDashForm;
public class GmBBARedesignateDashAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /*
   * RedesignedDashBBA(): this method is used to fetch the Re-designation values
   */
  public ActionForward RedesignedDashBBA(ActionMapping mapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmBBARedesignateDashForm gmBBAform = new GmBBARedesignateDashForm();
    GmBBARedesignateBean gmBBABean = new GmBBARedesignateBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    alReturn = GmCommonClass.parseNullArrayList(gmBBABean.gmfetchRedesignateDash());
    hmReturn.put("alReturn", alReturn);
    String strxmlGridData = generateOutPut(hmReturn, "GmBBARedesignateDash.vm");
    request.setAttribute("XMLGRIDDATA", strxmlGridData);
    return mapping.findForward("gmBBADash");
  }
  
  /**
   * SavDonorStageDate : This method is used to save the date entered in the screen
   */
  public ActionForward SavStageDate(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try{
    
    GmBBARedesignateDashForm gmBBARedesignateDashForm = (GmBBARedesignateDashForm) actionForm;
    gmBBARedesignateDashForm.loadSessionParameters(request);
    GmDonorScheduleBean gmTHBDonorSchedulingBean = new GmDonorScheduleBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmBBARedesignateDashForm);
    log.debug("SavDonorStageDate>hmParam> " + hmParam);
    gmTHBDonorSchedulingBean.updateStageDate(hmParam);

    PrintWriter pw = response.getWriter();
	pw.flush();	
    }catch(Exception e){
      PrintWriter pWriter = response.getWriter();
      response.setStatus(300);
      response.setContentType("text/xml");
      pWriter.write(e.getMessage());
      pWriter.flush();
   }	
    return null;
  }
  private String generateOutPut(HashMap hmReturn, String strFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmReturn.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.receiving.GmReceiveShipDash", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/receiving/templates");
    templateUtil.setTemplateName(strFile);
    return templateUtil.generateOutput();
  }
}
