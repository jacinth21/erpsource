/**
 * 
 */
package com.globus.operations.receiving.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.beans.GmPOBulkReceiveBean;
import com.globus.operations.receiving.forms.GmReceiveShipDashForm;

/**
 * @author Anilkumar
 * 
 */
public class GmReceiveShipDashAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmReceiveShipDashForm gmReceiveShipDashForm = (GmReceiveShipDashForm) actionForm;
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strAction = "";
    String strxmlGridData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    gmReceiveShipDashForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);

    String strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
    strAction = GmCommonClass.parseNull(gmReceiveShipDashForm.getHaction());

    if (strAction.equals("load")) {
      hmParam.put("REPORT", "RECDASHBD");
      alReturn = GmCommonClass.parseNullArrayList(gmPOBulkReceiveBean.fetchReceiveShipRpt(hmParam));
      hmReturn.put("alReturn", alReturn);
      hmReturn.put("DATEFMT", strApplDateFmt);
      hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);

      strxmlGridData = generateOutPut(hmReturn, "GmReceiveShipDash.vm");
      gmReceiveShipDashForm.setStrXmlGridData(strxmlGridData);
    }
    return mapping.findForward("gmReceiveShip");
  }

  /**
   * Generate out put.
   * 
   * @param hmReturn,vmFile
   * @return the string
   * @throws AppError the app error
   */
  private String generateOutPut(HashMap hmReturn, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmReturn.get("STRSESSCOMPANYLOCALE"));

    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.receiving.GmReceiveShipDash", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/receiving/templates");
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }

}
