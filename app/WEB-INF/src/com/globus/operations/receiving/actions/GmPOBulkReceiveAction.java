/**
 * 
 */
package com.globus.operations.receiving.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.operations.beans.GmDHRBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.receiving.beans.GmBulkShipmentSplitWrapperBean;
import com.globus.operations.receiving.beans.GmPOBulkReceiveBean;
import com.globus.operations.receiving.forms.GmPOBulkReceiveForm;

/**
 * @author arajan
 * 
 */
public class GmPOBulkReceiveAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadPO: This method is used to load the Purchase Order details
   * 
   * @exception AppError
   */
  public ActionForward loadPO(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmGridDtls = new HashMap();
    String strOpt = "";
    String strPONum = "";
    String strXmlGridData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmPOBulkReceiveForm);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strAppliDtFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
    if (strOpt.equals("loadPO")) {
      gmPOBulkReceiveForm.setReceivedDt(GmCalenderOperations.getCurrentDate(strAppliDtFmt));// By
                                                                                            // default
                                                                                            // the
                                                                                            // Expiry
                                                                                            // date
                                                                                            // should
                                                                                            // be
                                                                                            // the
                                                                                            // current
                                                                                            // date
      strPONum = GmCommonClass.parseNull((String) hmParam.get("PONUMBER"));
      // validatePOPlant := To validate the PO based on the plant
      gmPurchaseBean.validatePOPlant(strPONum);
      hmReturn = gmPOBulkReceiveBean.fetchPO(strPONum);
      GmCommonClass.getFormFromHashMap(gmPOBulkReceiveForm, hmReturn);

      hmGridDtls.put("TEMPLATENAME", "GmScanedCtrlNum.vm");
      hmGridDtls.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmGridDtls.put("VMFILEPATH", "properties.labels.operations.receiving.GmScanedCtrlNum");
      strXmlGridData = gmPOBulkReceiveBean.generateOutPut(hmGridDtls);
      gmPOBulkReceiveForm.setGridXmlData(strXmlGridData);
    }

    return actionMapping.findForward("GmPOBulkReceive");
  }

  /**
   * loadPartInfo: This method is used to fetch the part details
   * 
   * @exception AppError
   */
  public ActionForward loadPartInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    String strXMLString = "";

    String strPartNum = GmCommonClass.parseNull(gmPOBulkReceiveForm.getPartNum());

    hmReturn = gmPOBulkReceiveBean.fetchPartInfo(strPartNum);

    strXMLString = gmXMLParserUtility.createXMLString(hmReturn);

    log.debug("The strXMLString for the Part " + strPartNum + " is *** " + strXMLString);
    response.setContentType("text/xml");
    response.setCharacterEncoding("UTF-8");
    response.setHeader("Cache-Control", "no-cache");

    PrintWriter pw = response.getWriter();
    pw.write(strXMLString);
    pw.flush();
    return null;
  }


  /**
   * fetchExpiryDate: This method is used to fetch the expiry date from manufacturing date
   * 
   * @exception AppError
   */
  public ActionForward fetchExpiryDate(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());

    String strExpDate = "";
    String strMfgDate = GmCommonClass.parseNull(gmPOBulkReceiveForm.getMfgDate());
    String strShelfLife = GmCommonClass.parseNull(gmPOBulkReceiveForm.gethShelfLife());

    strExpDate = gmPOBulkReceiveBean.fetchExpiryDate(strMfgDate, strShelfLife);

    response.setContentType("text/xml");

    PrintWriter pw = response.getWriter();
    pw.write(strExpDate);
    pw.flush();
    return null;
  }

  /**
   * loadBatchshipment: This method is used to load the Bulk receive shipment details
   * 
   * @exception AppError
   */
  public ActionForward loadBatchshipment(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmDHRBean gmDHRBean = new GmDHRBean();

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmRSHeader = new HashMap();
    HashMap hmGridDtls = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmLocation = new HashMap();
    ArrayList alRSHeader = new ArrayList();
    ArrayList alRSDtls = new ArrayList();
    ArrayList alApprRej = new ArrayList();
    ArrayList alrejType = new ArrayList();
    ArrayList alLocations = new ArrayList();
    HashMap hmRecord = new HashMap();
    String strCodeId = "";
    String strOpt = "";
    String strRSNum = "";
    String strXmlGridData = "";
    String strPONumber = "";
    String strStatus = "";
    String strTemplateName = "";
    String strBtnDisabled = "false";
    String strDispatchTo = "GmRSProcess";
    String strIsApprRej = "";
    String strRejfl = "";
    String strMsg = "";
    String strMode = GmCommonClass.parseNull(gmPOBulkReceiveForm.getStrMode());
    String strbuttonName = "";
    String strJSFunciton = "";
    String strAllChecked = "";
    String strSbmtBtnUpdFl = "";
    String strSbmtBtnDisabled = "false";
    String strProcessBtnUpdFl = "";
    String strVerifyBtnUpdFl = "";
    String strSpmtBtnUpdFl = "";
    String strRelChkAccess = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    int intStatus = 0;
    if (strMode.equals("FETCHCONTAINER")) { // To fetch the container JSP
      strDispatchTo = "GmRSBulkReceive";
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmPOBulkReceiveForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strMsg = GmCommonClass.parseNull((String) hmParam.get("STRMSG"));

    // Get the user access to process the DHR
    String strPartyId = (String) request.getSession().getAttribute("strSessPartyId");
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "BULK_DHR_ACCESS"));
    String strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPOBulkReceiveForm.setAccessfl(strUpdFl);
    // Access for Submit button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "BULK_RECEIVE_SUBMIT"));
    strSbmtBtnUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    // Access for Release for Processing button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "BULK_RECEIVE_PROCESS"));
    strProcessBtnUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    // Access for release for verification button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "BULK_RECEIVE_VERIFY"));
    strVerifyBtnUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    // Access for release shipment button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "BULK_RECEIVE_SHPMNT"));
    strSpmtBtnUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    // Access for Master Check box in Release shipement screen at Pending Verification status
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "BULK_RECEIVE_CHK_ACS"));
    strRelChkAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if (strOpt.equals("loadRS")) {

      alrejType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("NCRTP")); // Rejection
                                                                                        // type
      gmPOBulkReceiveForm.setAlrejType(alrejType);

      strRSNum = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
      hmReturn = gmPOBulkReceiveBean.fetchBatchShipment(hmParam);
      alRSHeader = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RSHEADER"));// header
                                                                                          // details
      alRSDtls = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RSDTLS")); // grid
                                                                                       // details

      if (alRSDtls.size() > 0) {
        // To check whether the DHR is processed or not
        hmRecord = GmCommonClass.parseNullHashMap((HashMap) alRSDtls.get(0));
        strCodeId = GmCommonClass.parseNull((String) hmRecord.get("CODEID"));// To check whether the
                                                                             // DHR is processed or
                                                                             // not
      }

      strIsApprRej = strCodeId.equals("") ? "false" : "true";// To check whether the DHR is
                                                             // processed or not

      if (alRSHeader.size() > 0)
        hmRSHeader = (HashMap) alRSHeader.get(0);

      strStatus = GmCommonClass.parseNull((String) hmRSHeader.get("STATUSID"));
      Float fStatus = strStatus.equals("") ? 0 : Float.valueOf(strStatus).floatValue();
      intStatus = (int) (fStatus * 10);
      strPONumber = GmCommonClass.parseNull((String) hmRSHeader.get("PO_ID"));
      hmRSHeader.put("PONUMBER", strPONumber);

      GmCommonClass.getFormFromHashMap(gmPOBulkReceiveForm, hmRSHeader);

      alApprRej = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DHRAR")); // approve
                                                                                        // or reject

      // For fetching the locations to the dropdown
      hmLocation.put("DHRID", "");
      hmLocation.put("LOCGRP", "RSLCTY");
      alLocations = GmCommonClass.parseNullArrayList(gmDHRBean.fetchSplitLocations(hmLocation));

      if (strUpdFl.equals("Y")) { // Call vm file based on the status
        switch (intStatus) {
          case 10: {
            strTemplateName = "GmRSDHRPendingInspection.vm";
            strbuttonName = "Release for Processing";
            strJSFunciton = "fnReleaseForProcess(this.form)";
            strBtnDisabled = strProcessBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
            strSbmtBtnDisabled = strSbmtBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
            strAllChecked = gmPOBulkReceiveBean.fetchDhrBulkAtrrVal(strRSNum);
            log.debug("strAllChecked case 10"+strAllChecked);

            hmGridDtls.put("VMFILEPATH",
                "properties.labels.operations.receiving.GmRSDHRPendingInspection");
            if (!strAllChecked.equals("true") || strStatus.equals("4")) {
                strBtnDisabled = "true";
              } else {
                strBtnDisabled = strSpmtBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
              }
            break;
          }

          case 15: { // If part is rejected, then should not show Processing screen and the button
                     // should be disabled
            strTemplateName = "GmRSDHRPendingInspection.vm";
            strbuttonName = "Release for Processing";
            strJSFunciton = "fnReleaseForProcess(this.form)";
            strBtnDisabled = "true";
            strSbmtBtnDisabled = "true";
            hmGridDtls.put("VMFILEPATH",
                "properties.labels.operations.receiving.GmRSDHRPendingInspection");
            break;
          }

          case 20: {
            strTemplateName = "GmRSDHRPendingProcess.vm";
            strbuttonName = "Release for Verification";
            strJSFunciton = "fnReleaseForVerification(this.form)";
            strBtnDisabled = strVerifyBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
            alApprRej = alLocations;
            hmGridDtls.put("VMFILEPATH",
                "properties.labels.operations.receiving.GmRSDHRPendingInspection");
            break;
          }

          case 30:
          case 40: {
            strTemplateName = "GmRSDHRVerify.vm";
            strbuttonName = "Release Shipment";
            strJSFunciton = "fnReleaseShipment(this.form)";
            strAllChecked = gmPOBulkReceiveBean.fetchDhrBulkAtrrVal(strRSNum);
            hmGridDtls.put("VMFILEPATH", "properties.labels.operations.receiving.GmRSDHRVerify");
            // if DHRs already verified, should always disable button. button should be enabled only
            // if all the parameters are checked
            if (!strAllChecked.equals("true") || strStatus.equals("4")) {
              strBtnDisabled = "true";
            } else {
              strBtnDisabled = strSpmtBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
            }
            break;
          }
          default: {
            strTemplateName = "GmRSDHRPendingInspection.vm";
            strbuttonName = "Release for Processing";
            strJSFunciton = "fnReleaseForProcess(this.form)";
            strBtnDisabled = strProcessBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
            strSbmtBtnDisabled = strSbmtBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
            hmGridDtls.put("VMFILEPATH",
                "properties.labels.operations.receiving.GmRSDHRPendingInspection");
          }
        }
      } else {
        strTemplateName = "GmSavedScanCtrlNum.vm";
        strSbmtBtnDisabled = strSbmtBtnUpdFl.equalsIgnoreCase("Y") ? "false" : "true";
        hmGridDtls.put("VMFILEPATH", "properties.labels.operations.receiving.GmSavedScanCtrlNum");
      }

      hmGridDtls.put("TEMPLATENAME", strTemplateName);
      hmGridDtls.put("ALRSDTLS", alRSDtls);
      hmGridDtls.put("ALAPPRREJ", alApprRej);
      hmGridDtls.put("ISAPPR", strIsApprRej);
      hmGridDtls.put("DHRSTATUS", strStatus);
      hmGridDtls.put("RELCHKACCESS", strRelChkAccess);
      hmGridDtls.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);

      strXmlGridData = gmPOBulkReceiveBean.generateOutPut(hmGridDtls);

      gmPOBulkReceiveForm.setStatusId(strStatus);
      gmPOBulkReceiveForm.setStrApprRej(strCodeId);
      gmPOBulkReceiveForm.setStrDisabled(strBtnDisabled);
      gmPOBulkReceiveForm.setStrSbmtBtnDisabled(strSbmtBtnDisabled);
      gmPOBulkReceiveForm.setAlLocations(alLocations);
      gmPOBulkReceiveForm.setStrMsg(strMsg);
      gmPOBulkReceiveForm.setStrbuttonName(strbuttonName);
      gmPOBulkReceiveForm.setStrJSFunciton(strJSFunciton);
      gmPOBulkReceiveForm.setStrRejectFl(strRejfl);
      gmPOBulkReceiveForm.setStrAllChecked(strAllChecked);
      gmPOBulkReceiveForm.setGridXmlData(strXmlGridData);
    }
    return actionMapping.findForward(strDispatchTo);
  }



  /**
   * saveBulkDHR: This method is used to save the Bulk DHR
   * 
   * @exception AppError
   */
  public ActionForward saveBulkDHR(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strRSId = GmCommonClass.parseNull(gmPOBulkReceiveForm.getRsNumber());
    String strMsg = "";
    if (!strRSId.equals("")) {
      strMsg = "Records Saved Successfully";
    }

    hmParam = GmCommonClass.getHashMapFromForm(gmPOBulkReceiveForm);

    strRSId = gmPOBulkReceiveBean.saveBulkDHR(hmParam);

    // redirect to fetch method to fetch the saved details to screen
    strRedirectURL =
        "gmPOBulkReceive.do?method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="
            + strRSId + "&strMsg=" + strMsg;
    return actionRedirect(strRedirectURL, request);
  }


  /**
   * saveBulkDHRInsp: This method is used to save the Bulk DHR Inspection
   * 
   * @exception AppError
   */
  public ActionForward saveBulkDHRInsp(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmBulkShipmentSplitWrapperBean gmBulkShipmentSplitWrapperBean =
        new GmBulkShipmentSplitWrapperBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strRSId = "";
    String strRejRSId = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmPOBulkReceiveForm);
    strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    // strRSId = gmPOBulkReceiveBean.saveBulkDHRInsp(hmParam);
    strRejRSId = gmBulkShipmentSplitWrapperBean.saveBulkDHRInspSplit(hmParam);
    // redirect to fetch method to fetch the saved details to screen
    strRedirectURL =
        "gmPOBulkReceive.do?method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="
            + strRSId;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * printRecShipment: This method is used to Print the Details
   * 
   * @exception AppError
   */
  public ActionForward printRecShipment(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();

    ArrayList alRSHeader = new ArrayList();
    ArrayList alRSDtls = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmPOBulkReceiveForm);
    String strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    hmParam.put("ACTION", "Print");
    hmReturn = gmPOBulkReceiveBean.fetchBatchShipment(hmParam);

    alRSHeader = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RSHEADER"));
    alRSDtls = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RSDTLS"));
    request.setAttribute("HMRETURN", hmReturn);
    request.setAttribute("ALRSHEADER", alRSHeader);
    request.setAttribute("ALRSDTLS", alRSDtls);


    return actionMapping.findForward("GmPrintPOBulk");
  }

  /**
   * saveBulkDHRProcessVerify: This method is used to save the Bulk DHR Processing details
   * 
   * @exception AppError
   */
  public ActionForward saveBulkDHRProcessVerify(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmBulkShipmentSplitWrapperBean gmBulkShipmentSplitWrapperBean =
        new GmBulkShipmentSplitWrapperBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strRSId = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmPOBulkReceiveForm);

    // strRSId = gmPOBulkReceiveBean.saveBulkDHRProcessVerify(hmParam);
    strRSId = gmBulkShipmentSplitWrapperBean.saveBulkDHRProcessVerifySplit(hmParam);
    strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    // redirect to fetch method to fetch the saved details to screen
    strRedirectURL =
        "gmPOBulkReceive.do?method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="
            + strRSId;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * validateDonorNum: This method is used to check whether the donor number is already there or not
   * 
   * @exception AppError
   */
  public ActionForward validateDonorNum(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    String strDonorNumCnt = "";

    String strDonorNum = GmCommonClass.parseNull(gmPOBulkReceiveForm.getDonorNum());
    String strVendorId = GmCommonClass.parseNull(gmPOBulkReceiveForm.getVid());

    strDonorNumCnt = gmPOBulkReceiveBean.fetchDonorNumCnt(strDonorNum, strVendorId);
    response.setContentType("text/xml");

    PrintWriter pw = response.getWriter();
    pw.write(strDonorNumCnt);
    pw.flush();
    return null;
  }

  /**
   * saveBulkDHRVerifyRelease: This method is used to save the Bulk DHR Verify
   * 
   * @exception AppError
   */
  public ActionForward saveBulkDHRVerifyRelease(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strRSId = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmPOBulkReceiveForm);

    strRSId = gmPOBulkReceiveBean.saveBulkDHRVerifyRelease(hmParam);
    strRSId = GmCommonClass.parseNull((String) hmParam.get("RSNUMBER"));
    // redirect to fetch method to fetch the saved details to screen
    strRedirectURL =
        "gmPOBulkReceive.do?method=loadBatchshipment&strOpt=loadRS&strMode=FETCHCONTAINER&rsNumber="
            + strRSId;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * validateDonorNum: This method is used to check whether the donor number is already there or not
   * 
   * @exception AppError
   */
  public ActionForward validateLotNumber(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPOBulkReceiveForm gmPOBulkReceiveForm = (GmPOBulkReceiveForm) actionForm;
    gmPOBulkReceiveForm.loadSessionParameters(request);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    String strLotNumberCnt = "";
    String strinvalidLots = "";

    String strLotNumber = GmCommonClass.parseNull(gmPOBulkReceiveForm.getLotCode());
    String strLotString = GmCommonClass.parseNull(gmPOBulkReceiveForm.gethLotString());
    String strOption = GmCommonClass.parseNull(gmPOBulkReceiveForm.getStrOpt());
    String strRsID = GmCommonClass.parseNull(gmPOBulkReceiveForm.getRsNumber());

    HashMap hmParams = new HashMap();
    hmParams.put("LOTNUMBERSTRING", strLotString);
    hmParams.put("RSID", strRsID);

    if (strOption.equals("LOT")) {
      strLotNumberCnt = gmPOBulkReceiveBean.fetchLotNumberCnt(strLotNumber);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strLotNumberCnt);
      pw.flush();
    } else if (strOption.equals("LOTNUMBERS")) {
      strinvalidLots = gmPOBulkReceiveBean.fetchDuplicateLots(hmParams);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strinvalidLots.replaceAll(",", ", "));
      pw.flush();
    }
    return null;
  }
}
