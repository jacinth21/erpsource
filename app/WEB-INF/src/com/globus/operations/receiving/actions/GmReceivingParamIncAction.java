package com.globus.operations.receiving.actions;


import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.beans.GmBulkShipmentSplitWrapperBean;
import com.globus.operations.receiving.beans.GmPOBulkReceiveBean;
import com.globus.operations.receiving.forms.GmReceivingParamIncForm;

public class GmReceivingParamIncAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    GmReceivingParamIncForm gmReceivingParamIncForm = (GmReceivingParamIncForm) actionForm;
    GmCommonClass gmCommonClass = new GmCommonClass();
    gmReceivingParamIncForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmReceivingParamIncForm);
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmBulkShipmentSplitWrapperBean gmBulkShipmentSplitWrapperBean =
        new GmBulkShipmentSplitWrapperBean(getGmDataStoreVO());
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    HashMap hmAccess = new HashMap();
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strForm42Accessflag = "";
    String strLabelPrintAccessflag = "";
    String strQCVerifiedAccessflag = "";
    String strPreStagingAccessflag = "";
    String strStagingAccessflag = "";
    String strReadAccessfl = "";
    String strUpdAccessfl = "";
    String strVoidAccessfl = "";
    String strBulkDhrStatus = "";
    String strBtnDisabled = "false";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "FORM42_CHECK"));

    strReadAccessfl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
    strUpdAccessfl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strVoidAccessfl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

    if (strReadAccessfl.equals("") && strUpdAccessfl.equals("") && strVoidAccessfl.equals("")) {
      strForm42Accessflag = "N";
    }
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "LBL_PRINTED_CHECK"));
    strReadAccessfl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
    strUpdAccessfl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strVoidAccessfl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

    if (strReadAccessfl.equals("") && strUpdAccessfl.equals("") && strVoidAccessfl.equals("")) {
      strLabelPrintAccessflag = "N";
    }
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "QC_VERIFIED_CHECK"));
    strReadAccessfl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
    strUpdAccessfl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strVoidAccessfl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

    if (strReadAccessfl.equals("") && strUpdAccessfl.equals("") && strVoidAccessfl.equals("")) {
      strQCVerifiedAccessflag = "N";
    }
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "PRE_STAGING_ACCESS"));
    strReadAccessfl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
    strUpdAccessfl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strVoidAccessfl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

    if (strReadAccessfl.equals("") && strUpdAccessfl.equals("") && strVoidAccessfl.equals("")) {
        strPreStagingAccessflag = "N";
    }
        
     hmAccess =
           GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "STAGING_ACCESS"));
      strReadAccessfl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
      strUpdAccessfl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      strVoidAccessfl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

     if (strReadAccessfl.equals("") && strUpdAccessfl.equals("") && strVoidAccessfl.equals("")) {
         strStagingAccessflag = "N";
      }
    gmReceivingParamIncForm.setForm42fl(strForm42Accessflag);
    gmReceivingParamIncForm.setQcverifiedfl(strQCVerifiedAccessflag);
    gmReceivingParamIncForm.setLabelprintfl(strLabelPrintAccessflag);
    gmReceivingParamIncForm.setPreStagingfl(strPreStagingAccessflag);
    gmReceivingParamIncForm.setStagingfl(strStagingAccessflag);
    if (strOpt.equals("save")) {
      String strinputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
      String strAttrtype = GmCommonClass.parseNull((String) hmParam.get("ATTRTYPE"));
      String strrsid = GmCommonClass.parseNull((String) hmParam.get("RSID"));

      gmBulkShipmentSplitWrapperBean.saveReleaseBulkDHRInhouseTxn(hmParam);
      // gmPOBulkReceiveBean.saveBulkDhrAttribute(hmParam);// Saving the Attribute Values.

      gmReceivingParamIncForm.setStrOpt("load");
      strOpt = "load";
      gmReceivingParamIncForm.setRsid(strrsid);

    }
    if (strOpt.equals("load")) {
      ArrayList alreturn = new ArrayList();
      ArrayList alAttribute = new ArrayList();
      HashMap hmReturn = new HashMap();
      String strRsId = GmCommonClass.parseNull((String) hmParam.get("RSID"));

      strBulkDhrStatus = gmPOBulkReceiveBean.getBulkDhrStatus(strRsId);
      strBtnDisabled = strBulkDhrStatus.equals("4") ? "true" : "false"; // should disable the submit
                                                                        // button if the DHR is
                                                                        // verified

      String strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
      alreturn = GmCommonClass.parseNullArrayList(fetchBulkDhrAttrDtls(hmParam));
      hmReturn.put("RECEIVEATTRIBUTEDATA", alreturn);
      hmReturn.put("SESSDATEFMT", strApplDateFmt);
      hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      // hmReturn.put("APPLNDATEFMT",strApplDateFmt);
      String strxmlGridData = generateOutPut(hmReturn);
      gmReceivingParamIncForm.setStrBtnDisabled(strBtnDisabled);
      // Setting DHR Status in the bean
      gmReceivingParamIncForm.setStrDHRStatusId(strBulkDhrStatus);
      log.debug("strBulkDhrStatus*****"+strBulkDhrStatus);
      gmReceivingParamIncForm.setXmlStringData(strxmlGridData);
    }

    return actionMapping.findForward("GmReceivingParamInclude");


  }

  /*
   * The Below method is used to fetch the Parameter for Respective DHR Bulk ID.
   */
  private ArrayList fetchBulkDhrAttrDtls(HashMap hmParam) {
    GmPOBulkReceiveBean gmPOBulkReceiveBean = new GmPOBulkReceiveBean();

    HashMap hmAcct = new HashMap();
    ArrayList alReceivingParamInc = new ArrayList();
    HashMap hmAssocRepDetail = new HashMap();
    alReceivingParamInc = gmPOBulkReceiveBean.fetchBulkDhrAttribute(hmParam);
    return alReceivingParamInc;

  }

  private String generateOutPut(HashMap hmParam) throws AppError { // Processing HashMap into XML
                                                                   // data

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("operations/receiving/templates");

    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.receiving.GmReceivingParamInclude", strSessCompanyLocale));
    templateUtil.setTemplateName("GmReceivingParamInc.vm");
    return templateUtil.generateOutput();
  }

}
