/**
 * 
 */
package com.globus.operations.receiving.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.receiving.beans.GmPartControlSetupBean;
import com.globus.operations.receiving.forms.GmPartControlSetupForm;

/**
 * @author KSomanathan
 * 
 */
public class GmPartControlSetupAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  String strEUpdateAccess = "";
  String strEReadAccess = "";
  String strEVoidAccess = "";

  /**
   * loadPartControlInfo: This method is used to load the part control details
   * 
   * @exception AppError
   */
  public ActionForward loadPartControlInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmPartControlSetupForm gmPartControlSetupForm = (GmPartControlSetupForm) actionForm;
    gmPartControlSetupForm.loadSessionParameters(request);
    GmPartControlSetupBean gmPartControlSetupBean = new GmPartControlSetupBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alVendors = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    String strPartCNId = "";
    String julianRule = "";
    String strEditExpairyAccess = "";
    String strUserId = "";
    String strCnumFmt = "";
    String strSecEventName = "CNTRL_NUM_OVRD_SUBMT";
    String strSesPartyId = "";

    String strScreenType = GmCommonClass.parseNull(gmPartControlSetupForm.gethScreenType());
    strCnumFmt = GmCommonClass.parseNull(gmPartControlSetupForm.getCnumfmt());
    hmParam = GmCommonClass.getHashMapFromForm(gmPartControlSetupForm);
    strPartCNId = GmCommonClass.parseNull((String) hmParam.get("PARTCNID"));
    /* Able to Edit the expiry date in Lot number override screen by clicking the "D" Icon in Lot Tracking report screen
     * users who have access in the "EXP_DATE_CHNG_ACCESS" security group will have access to edit Lot#/Rev/Donor number in lot nu,ber override screen
     * */
    if (strScreenType.equals("popup")){
        String strdisableText = GmCommonClass.parseNull((String) hmParam.get("DISABLETEXT"));
    	strPartCNId = gmPartControlSetupBean.fetchPartCtrlNumId(hmParam);
    	strSecEventName = "EXP_DATE_CHNG_ACCESS";
        gmPartControlSetupForm.setPartCNId(strPartCNId);
        gmPartControlSetupForm.setDisableText(strdisableText); 
        gmPartControlSetupForm.sethScreenType(strScreenType);
    }
    hmReturn = gmPartControlSetupBean.loadPartControlInfo(strPartCNId);
    gmPartControlSetupForm =
        (GmPartControlSetupForm) GmCommonClass.getFormFromHashMap(gmPartControlSetupForm, hmReturn);
    julianRule =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("JULIAN_DATE", "LOTVALIDATION"));
    strUserId = GmCommonClass.parseNull(gmPartControlSetupForm.getUserId());
    HashMap hmInput = new HashMap();
    hmInput.put("CHECKACTIVEFL", "Y");

   
    
    // CNTRL_NUM_OVRD_SUBMT - Users in this security group will have the ability to enter Lot Number
    // information
    strSesPartyId = GmCommonClass.parseNull(gmPartControlSetupForm.getSessPartyId());
    HashMap hmCntrlNumberAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
            strSecEventName));
    String strCntrlNoSubmitFl = GmCommonClass.parseNull((String) hmCntrlNumberAccess.get("UPDFL"));
    request.setAttribute("SUBMITCNTRLFL", strCntrlNoSubmitFl);
    alVendors =
        GmCommonClass.parseNullArrayList((ArrayList) gmVendor.getVendorList(hmInput).get(
            "VENDORLIST"));
    
    gmPartControlSetupForm.setAlVendors(alVendors);
    gmPartControlSetupForm.setJulianRule(julianRule);
    gmPartControlSetupForm.setPartCNId(GmCommonClass.parseNull((String) hmReturn.get("PARTCNID")));
    alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strPartCNId, "92092"));
    gmPartControlSetupForm.setAlLogReasons(alLogReasons);
    gmPartControlSetupForm.setCnumfmt(strCnumFmt);
    gmPartControlSetupForm.sethScreenType(strScreenType);
    return actionMapping.findForward("gmPartControl");
  }

  /**
   * savePartControlInfo: This method is used to Save/Update the Control Number Infor in
   * t2550_part_control_number table
   * 
   * @exception AppError
   */
  public ActionForward savePartControlInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmPartControlSetupForm gmPartControlSetupForm = (GmPartControlSetupForm) actionForm;
    gmPartControlSetupForm.loadSessionParameters(request);
    GmPartControlSetupBean gmPartControlSetupBean = new GmPartControlSetupBean(getGmDataStoreVO());
    GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alVendors = new ArrayList();
    ArrayList alDonors = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    // String strExpiryAccFl = "";
    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmPartControlSetupForm));
    String strCnumFmt = GmCommonClass.parseNull(gmPartControlSetupForm.getCnumfmt());
    String strhAccess = GmCommonClass.parseNull(gmPartControlSetupForm.gethAccess());
    String strPartNum = GmCommonClass.parseNull(gmPartControlSetupForm.getPartNM());
    String strPartCNId = gmPartControlSetupBean.savePartControlInfo(hmParam);

    String strSecEventName = "CNTRL_NUM_OVRD_SUBMT";
    // CNTRL_NUM_OVRD_SUBMT - Users in this security group will have the ability to enter Lot Number
    // information
    String strSesPartyId = GmCommonClass.parseNull(gmPartControlSetupForm.getSessPartyId());
    HashMap hmCntrlNumberAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
            strSecEventName));
    String strCntrlNoSubmitFl = GmCommonClass.parseNull((String) hmCntrlNumberAccess.get("UPDFL"));
    request.setAttribute("SUBMITCNTRLFL", strCntrlNoSubmitFl);

    hmReturn = gmPartControlSetupBean.loadPartControlInfo(strPartCNId);
    gmPartControlSetupForm =
        (GmPartControlSetupForm) GmCommonClass.getFormFromHashMap(gmPartControlSetupForm, hmReturn);
    HashMap hmInput = new HashMap();
    hmInput.put("CHECKACTIVEFL", "Y");
    alVendors =
        GmCommonClass.parseNullArrayList((ArrayList) gmVendor.getVendorList(hmInput).get(
            "VENDORLIST"));
    gmPartControlSetupForm.setAlVendors(alVendors);
    alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strPartCNId, "1295"));
    gmPartControlSetupForm.setAlLogReasons(alLogReasons);

    gmPartControlSetupForm.setCnumfmt(strCnumFmt);
    gmPartControlSetupForm.sethAccess(strhAccess);
    if (!strPartCNId.equals("")){
    	String strsuccessMsg = "Y";
    	gmPartControlSetupForm.setSuccessMsg(strsuccessMsg);
    }
    return actionMapping.findForward("gmPartControl");
  }


  /**
   * fetchPartNumber: This method is used to check whether the Part number
   * 
   * @exception AppError
   */
  public ActionForward fetchPartNumber(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmPartControlSetupForm gmPartControlSetupForm = (GmPartControlSetupForm) actionForm;
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    gmPartControlSetupForm.loadSessionParameters(request);
    GmPartControlSetupBean gmPartControlSetupBean = new GmPartControlSetupBean(getGmDataStoreVO());

    // GmPartNumBean gmPartNumBean = new GmPartNumBean();
    String strPartNumber = "";
    String strPartNM = GmCommonClass.parseNull(gmPartControlSetupForm.getPartNM());
    strPartNumber = gmPartControlSetupBean.fetchPartNumberInfo(strPartNM);
    response.setContentType("text/xml");
    response.setCharacterEncoding("UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter pw = response.getWriter();
    pw.write(strPartNumber);
    pw.flush();
    return null;
  }
  /**
   * PMT-51152: LOT number override by Transaction
   * This method called from Lot Override by Transaction Screen
   * 
   * loadLotOverrideByTxn: This method is used to load the lot override by transaction screen
   * 
   * @exception AppError
   */
  public ActionForward loadLotOverrideByTxn(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmPartControlSetupForm gmPartControlSetupForm = (GmPartControlSetupForm) actionForm;
    
    return actionMapping.findForward("gmLotNumOverrideByTxn");
  }
  /**
   * saveLotOverrideByTxn: This method is used to check validation and
   * 					   save the lot details on t2550 table
   * 
   * @exception AppError
   */
  public ActionForward saveLotOverrideByTxn(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmPartControlSetupForm gmPartControlSetupForm = (GmPartControlSetupForm) actionForm;
    gmPartControlSetupForm.loadSessionParameters(request);
    GmPartControlSetupBean gmPartControlSetupBean = new GmPartControlSetupBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strErrorMsg = "";
    String strCompanyId = "";
      
    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmPartControlSetupForm));
    strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    hmParam.put("COMPANTID", strCompanyId);
    
    strErrorMsg = gmPartControlSetupBean.saveLotOverrideByTxn(hmParam);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(strErrorMsg);
    pw.flush();
    return null;

  }
  
}
