package com.globus.operations.receiving.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.beans.GmReservedLogRptBean;
import com.globus.operations.receiving.forms.GmReservedLogRptForm;

public class GmReservedLogRptAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    instantiate(request, response);

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alInventoryTypes = new ArrayList();
    String strOpt = "";
    String strXmlGridData = "";
    String strApplDateFmt = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmReservedLogRptForm gmReservedLogRptForm = (GmReservedLogRptForm) actionForm;
    GmReservedLogRptBean gmReservedLogRptBean = new GmReservedLogRptBean(getGmDataStoreVO());
    gmReservedLogRptForm.loadSessionParameters(request);
    HttpSession session = request.getSession(false);

    strApplDateFmt = GmCommonClass.parseNull(gmReservedLogRptForm.getApplnDateFmt());
    strOpt = gmReservedLogRptForm.getStrOpt();
    hmParam = GmCommonClass.getHashMapFromForm(gmReservedLogRptForm);

    alInventoryTypes = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTTXY"));
    gmReservedLogRptForm.setAlInVenTypes(alInventoryTypes);

    if (strOpt.equals("load")) {
      alReturn =
          GmCommonClass.parseNullArrayList(gmReservedLogRptBean.reportReservedDetails(hmParam));
    }
    hmReturn.put("alReturn", alReturn);
    hmReturn.put("DATEFMT", strApplDateFmt);
    hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    strXmlGridData = generateOutPut(hmReturn, "GmReservedLogRpt.vm");
    gmReservedLogRptForm.setXmlGridData(strXmlGridData);

    return mapping.findForward("gmReservedLog");
  }

  /**
   * Generate out put.
   * 
   * @param hmReturn,vmFile
   * @return the string
   * @throws AppError the app error
   */
  private String generateOutPut(HashMap hmReturn, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmReturn.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.receiving.GmReservedLogRpt", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/receiving/templates");
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }
}
