package com.globus.operations.receiving.actions;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javazoom.upload.UploadException;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonApplicationBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;

import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.receiving.forms.GmLotOverrideBulkActionForm;
import com.globus.jms.consumers.beans.GmLotBulkExtBean;



/**
 * @author pvigneshwaran
 *
 */
public class GmLotOverrideBulkAction extends GmDispatchAction {

	  private static String strComnPath = GmCommonClass.getString("GMCOMMON");
	  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
	  /**Lot Upload:This method load and save files
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadLotUpload(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		    instantiate(request, response);
		    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
		    GmLotOverrideBulkActionForm gmLotOverrideBulkActionForm = (GmLotOverrideBulkActionForm) actionForm;
		    GmLotBulkExtBean gmLotBulkExtBean = new GmLotBulkExtBean(getGmDataStoreVO());
		    gmLotOverrideBulkActionForm.loadSessionParameters(request);
		    String strUploadHome = GmCommonClass.parseNull(GmCommonApplicationBean.getLotExpiryConfig("GMLOTOVERRIDEBULKUPLOAD"));
		    log.debug("strUploadHome"+strUploadHome);
		   
		    ArrayList alExcelData = new ArrayList();
		    HashMap hmParam=new HashMap();
		  
		    String strOpt = GmCommonClass.parseNull(gmLotOverrideBulkActionForm.getStrOpt());
		    String strRefID = GmCommonClass.parseNull(gmLotOverrideBulkActionForm.getStrRefID());
		    String strRefType = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOTEXT","LOTEXT"));
		    String strUserID = GmCommonClass.parseNull(gmLotOverrideBulkActionForm.getUserId());
		    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
		    hmParam = GmCommonClass.getHashMapFromForm(gmLotOverrideBulkActionForm);
		   
		    String strMessage="";
		    log.debug("Before calling upload file");
		  
		   if(strOpt.equals("Upload")){
		    	
		    	 FormFile strFile = gmLotOverrideBulkActionForm.getFile	();
		    	 int intFileSize = strFile.getFileSize();
		    	 String strFileName = strFile.getFileName();
		    	 String fileExt = strFileName.substring(strFileName.lastIndexOf("."));
		      	
		    	 if (!strFileName.equals("")) {
		             try {
		            	 log.debug("strFile"+strFile);
		            	 log.debug("strUploadHome"+strUploadHome);

		               uploadFile(strFile, strUploadHome); 
		            } catch (Exception e) {
		             throw new AppError("", "20675");
		            }//end try
		             strFileName = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
		             gmCommonUploadBean.saveUploadUniqueInfo(strRefID, strRefType , strFileName, strUserID);
		             strMessage=GmCommonClass.parseNull(GmCommonApplicationBean.getLotExpiryConfig("GMLOTMSG"));
		             gmLotOverrideBulkActionForm.setMessage(strMessage);
		    	 	}//end if
		    	 hmParam.put("MESSAGE", strMessage);
		    	 hmParam.put("COMPANY_INFO",strCompanyInfo);
		    	 log.debug("Before calling JMS to update the expiry date");
		    	 gmLotBulkExtBean.UpdateLotExpiry(strFileName,strUploadHome,strFile,hmParam);
		    	 
		    }//end strOpt If
		    GmCommonClass.getFormFromHashMap(gmLotOverrideBulkActionForm, hmParam);
		    return actionMapping.findForward("GmLotOverrideBulk");
	  }
	  
	  
	  /**uploadFile:This method load and save files
		 * @param formFile
		 * @param strUploadHome
		 * @throws UploadException,IOException
		 */
		    public void uploadFile(FormFile formFile, String strUploadHome) throws UploadException,
		      IOException {
		    // Upload the file on server
		    FileOutputStream outputStream = null;
		    log.debug(" new file"+strUploadHome);

		    File file = new File(strUploadHome);
		    log.debug("File creation process started");

		    if (!file.exists()) {
		    	log.debug("File exists process");
		      file.mkdir();
		    }
		    log.debug("Before replace"+strUploadHome);
		    String strUploadPath = strUploadHome + formFile.getFileName();
		    log.debug("strUploadPath "+strUploadPath);

		    try {
			      log.debug("strUploadPath"+strUploadPath);

		      outputStream = new FileOutputStream(new File(strUploadPath));
			    
		      outputStream.write(formFile.getFileData());
		      log.debug("After writting file");
		    } finally {
		      if (outputStream != null) {
		        outputStream.close();
		      }
		    }
		  }//end of file Upload
	  
}
