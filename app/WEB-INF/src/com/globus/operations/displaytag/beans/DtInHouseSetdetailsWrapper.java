package com.globus.operations.displaytag.beans;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.displaytag.decorator.TableDecorator;

public class DtInHouseSetdetailsWrapper extends TableDecorator {

	HashMap hmResult = new HashMap ();
	String strReturn="";
	int i=0;
	public String getPNUM(){
		hmResult = (HashMap) this.getCurrentRowObject();
		int intSetListQty=Integer.parseInt((String)hmResult.get("SETLISTQTY"));
		int intQty=Integer.parseInt((String)hmResult.get("QTY"));
		if(intSetListQty < intQty || intSetListQty > intQty){
			if(i==0){
			strReturn="<font color=\"red\">" + (String)hmResult.get("PNUM") + "</font><input type=\"hidden\" name=\"hDisp\" value=\"disp\">";
			i++;
			}else{
				strReturn="<font color=\"red\">" + (String)hmResult.get("PNUM") + "</font>";
			}
		}else{
			strReturn="<font color=\"black\">" + (String)hmResult.get("PNUM") + "</font>";
		}
		return strReturn;
	}
	public String getPDESC(){
		hmResult = (HashMap) this.getCurrentRowObject();
		int intSetListQty=Integer.parseInt((String)hmResult.get("SETLISTQTY"));
		int intQty=Integer.parseInt((String)hmResult.get("QTY"));
		if(intSetListQty < intQty || intSetListQty > intQty){
			strReturn="<font color=\"red\">" + (String)hmResult.get("PDESC") + "</font>";
		}else{
			strReturn="<font color=\"black\">" + (String)hmResult.get("PDESC") + "</font>";
		}
		return strReturn;
	}
	public String getSETLISTQTY(){
		hmResult = (HashMap) this.getCurrentRowObject();
		int intSetListQty=Integer.parseInt((String)hmResult.get("SETLISTQTY"));
		int intQty=Integer.parseInt((String)hmResult.get("QTY"));
		if(intSetListQty < intQty || intSetListQty > intQty){
			strReturn="<font color=\"red\">" + (String)hmResult.get("SETLISTQTY") + "</font>";
		}else{
			strReturn="<font color=\"black\">" + (String)hmResult.get("SETLISTQTY") + "</font>";
		}
		return strReturn;
	}
	public String getQTY(){
		hmResult = (HashMap) this.getCurrentRowObject();
		int intSetListQty=Integer.parseInt((String)hmResult.get("SETLISTQTY"));
		int intQty=Integer.parseInt((String)hmResult.get("QTY"));
		if(intSetListQty < intQty || intSetListQty > intQty){
			strReturn="<font color=\"red\">" + (String)hmResult.get("QTY") + "</font>";
		}else{
			strReturn="<font color=\"black\">" + (String)hmResult.get("QTY") + "</font>";
		}
		return strReturn;
	}
}