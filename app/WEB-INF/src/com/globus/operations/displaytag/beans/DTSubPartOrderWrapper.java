package com.globus.operations.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTSubPartOrderWrapper extends TableDecorator {
	private DynaBean db;
	private String strStatus = "";
	private String strPartNumber;
	
	public StringBuffer strValue = new StringBuffer();
	private int intCount = 0;

	public DTSubPartOrderWrapper() {
		super();
	}

	public String getSTATUS() {
		db = (DynaBean) this.getCurrentRowObject();
		strStatus = GmCommonClass.parseNull((String) db.get("STATUS"));
		strPartNumber   = GmCommonClass.parseNull((String)db.get("PNUM"));
		String strIsChecked = "checked";
		if (strStatus.equals("N")) {
			strIsChecked = "";
		}
		strValue.setLength(0);

		strValue.append("<input class=RightText type='checkbox' ");
		strValue.append(strIsChecked);
		strValue.append(" name='Chk_sub");
		strValue.append(String.valueOf(intCount));
		strValue.append("' value='");
		strValue.append(strStatus);

		strValue.append("' >");
		strValue.append("<input type=hidden  name=pnum");
        strValue.append(String.valueOf(intCount) );
        strValue.append("  value='");
        strValue.append(strPartNumber);
        strValue.append("' >");
		intCount++;
		return strValue.toString();
	}

}