package com.globus.operations.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTPartMappingWrapper extends TableDecorator {
	
	private String strParNumber; 
    private int intLevel = 0;
    
    private String strSpace = "";
    private DynaBean db ;
	public DTPartMappingWrapper() {
		super();
	}

	public String getPNUM() {
		String strTab = "";
        db =    (DynaBean) this.getCurrentRowObject();
        
        strParNumber = String.valueOf(db.get("PNUM"));
        intLevel = Integer.parseInt(String.valueOf(db.get("LVL")));
        if (intLevel >1)  strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
         for (int i = 1; i < intLevel; i++)
         { 
        	 	strTab = strTab + strSpace;
         }
         strSpace="";
         strParNumber = strTab +strParNumber;
       
        
        return strParNumber;
	}
	
	public String getTOORDER() {
        db =    (DynaBean) this.getCurrentRowObject();        
        String strToOrder = String.valueOf(db.get("TOORDER"));        
        if(!strToOrder.equals("N"))		 strToOrder = "<b>Y</b>";                
        return strToOrder;
	}
}