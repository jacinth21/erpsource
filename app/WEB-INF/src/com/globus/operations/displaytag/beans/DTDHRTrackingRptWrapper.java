package com.globus.operations.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;


public class DTDHRTrackingRptWrapper extends TableDecorator {
  Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);

  /**
   * Returns the string for DHR id field icon.
   * 
   * @return String
   */

  public String getID() {
    StringBuffer strValue = new StringBuffer();
    DynaBean db = (DynaBean) this.getCurrentRowObject();

    String strID = GmCommonClass.parseNull((String) db.get("ID"));
    String strVendorId = GmCommonClass.parseNull(String.valueOf(db.get("VID")));
    String strProdType = GmCommonClass.parseNull(String.valueOf(db.get("PRODTYPE")));
    String strDonorNum = GmCommonClass.parseNull(String.valueOf(db.get("DONORNUMBER")));
    String strSerialNumFl = GmCommonClass.parseNull(String.valueOf(db.get("SERIALNUMNEEDFL")));
    strValue.setLength(0);
    strValue.append(strID);
    if ((strProdType.equals("100845") && !strDonorNum.equals("") && !strDonorNum.equals("null"))
        || strSerialNumFl.equals("Y")) {
      strValue
          .append("&nbsp;<img align='top' id='imgEdit' style='cursor: hand' src='/images/d.gif' title='Donor'  width='14' height='14' onClick=javascript:fnCallDonor('"
              + strID + "','" + strVendorId + "') >");
    }

    return strValue.toString();
  }

}
