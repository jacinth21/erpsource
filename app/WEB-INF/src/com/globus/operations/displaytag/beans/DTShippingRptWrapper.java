/**
 * Description : This DT Wrapper is used for GmShippingReport.jsp
 * Copyright : Globus Medical Inc
 */
package com.globus.operations.displaytag.beans;



import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;


/**
 * @author bvidyasankar
 *
 */

public class DTShippingRptWrapper extends TableDecorator{

	private DynaBean db;
	public StringBuffer strValue = new StringBuffer();
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	private int intCount = 0;
	public DTShippingRptWrapper()
    {
        super();
    }
    /**
     * Returns the html string to be displayed in displaytag as per logic defined
     * @return String
     */
	
	public String getTRACK(){//Decoration for column Pending Shipping/Track#
		db = (DynaBean) this.getCurrentRowObject();
		strValue.setLength(0);
	//	log.debug("Printing db values..." + db);
		String strTrackID = "";
		String strSourceID = "";
		String strStatus = "";
		String strCarrierID = "";
		String strRefID = "";
		Object obj = null;
		
		obj = db.get("TRACK");
		if(obj != null){
			strTrackID = db.get("TRACK").toString();
		}
		
		obj = db.get("SOURCEID");
		if(obj != null){
			strSourceID = db.get("SOURCEID").toString();
		}
		
		obj = db.get("STATUS");
		if(obj != null){
			strStatus = db.get("STATUS").toString();
		}
		
		obj = db.get("CARRIERID");
		if(obj != null) {
			strCarrierID = db.get("CARRIERID").toString();
		}
		
		obj = db.get("REFID");
		if(obj != null) {
			strRefID = db.get("REFID").toString();
		}
		
	//	log.debug("1a@....DTShippingRptWrapper...accessed db values...SourceId.." + strSourceID + "...Status..." + strStatus);
		/*
		if(strStatus.equals("30")){
			strValue.append("<a href=/gmOPEditShipDetails.do?refId="+strRefID+"&source="+strSourceID+">");
			strValue.append(strTrackID);
			strValue.append("</a>");
		}
		*/
		if(!strTrackID.equalsIgnoreCase("Y")){
			strValue.append("<a href=javascript:fnOpenTrack('");//Calling fnOpenTrack() in .js file
			strValue.append(strTrackID.replace(' ','~'));
			strValue.append("','");
			strValue.append(strCarrierID);
			strValue.append("')>");
			strValue.append(strTrackID);
			strValue.append("</a>");
		}
	//	log.debug("2@....DTShippingRptWrapper...strValue..." + strValue.toString());
		return strValue.toString();
	}
	
	/**
     * Returns the html string to be displayed in displaytag as per logic defined
     * @return String
     */
	public String getREFID(){//Decoration for column RefID
		db = (DynaBean) this.getCurrentRowObject();
		strValue.setLength(0);
		
		String strShipId = "";
		String strRefID = "";
		String strSourceID = "";
		String strStatus = "";
		String strEtchId = "";
		String strReqId = "";
		
		Object obj = null;
		
		obj = db.get("SOURCEID");
		if(obj != null){
			strSourceID = db.get("SOURCEID").toString();
		}
		
		obj = db.get("REFID");
		if(obj != null){
			strRefID = db.get("REFID").toString();
		}
		
		obj = db.get("STATUS");
		if(obj != null){
			strStatus = db.get("STATUS").toString();
		}
		
		obj = db.get("SID");
		if(obj != null){
			strShipId = db.get("SID").toString();
		}
		obj = db.get("ETCHID");
		if(obj != null){
			strEtchId = db.get("ETCHID").toString();
		}
		
		//strValue.append("<a href=/gmOPEditShipDetails.do?refId="+strRefID+"&source="+strSourceID+">");
		strValue.append("<a href=/gmOPEditShipDetails.do?shipId="+strShipId+"&strOpt=report>");
		strValue.append("<img width='39' height='19' title='Click here to edit details' style='cursor:hand' src='/images/edit.jpg'/> ");
		strValue.append("</a>&nbsp;");

		strValue.append("<a href=javascript:fnPrintSlip('");//Calling fnPrintSlip() in .js file 
		strValue.append(strRefID);
		strValue.append("','");
		strValue.append(strSourceID);
		strValue.append("','");
		strValue.append(strStatus);
		strValue.append("')><img src=/images/product_icon.gif height='13' width='12' alt='Print Slip'></a>&nbsp;");
		if (strSourceID.equals("50182")) { // loaners
			strReqId = GmCommonClass.parseNull((String)db.get("REQID"));
			
			if(!strReqId.trim().equals("")){//&& (strStatus.equals("30") || strStatus.equals("40"))
				strValue.append("<input class=RightText type='checkbox' ");
				strValue.append(" name='Chk_sub");
				strValue.append(String.valueOf(intCount));
				strValue.append("' value='");
				strValue.append(strRefID);
				strValue.append("' checked>");
	
				intCount++;
			}
			
			strValue.append(strEtchId);
			
		} 
		else{
			strValue.append(strRefID);	
		}		
		return strValue.toString();
	}
	
	
}

