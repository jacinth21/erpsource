package com.globus.operations.displaytag.beans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Date;
import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;
import com.globus.operations.logistics.forms.GmChargesApprovalForm;
import com.globus.valueobject.common.GmDataStoreVO;

import java.text.DecimalFormat;
public class DTChargesApprovalWrapper extends TableDecorator {
	//private DynaBean db;
	private HashMap hmResult;
	private int intCount = 0;
	private int countId = 0;
	private GmChargesApprovalForm gmCharges;
	private List alStatusList;
	private Iterator alListIter;
	private String strOptKey = "";
	private String strOptValue = "";
	private String strDeptId =""; 
	//private DecimalFormat moneyFormat;
	public StringBuffer strValue = new StringBuffer();
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmCharges = (GmChargesApprovalForm) getPageContext().getAttribute("frmChargesApproval", PageContext.REQUEST_SCOPE);
		// alStatusList = gmCharges.getAlStatustList();
		alStatusList = gmCharges.getAlStatustListRpt();
		strDeptId = gmCharges.getDeptId();
		 //this.moneyFormat = new DecimalFormat("$#,###,###.00"); //$NON-NLS-1$
	}

	public String getSTFL() {
		StringBuffer strValue = new StringBuffer();
		HashMap hmStatusList = new HashMap();
		hmResult = (HashMap) this.getCurrentRowObject();
		String strStatus = GmCommonClass.parseNull((String)hmResult.get("STFL").toString());
		String strCountryCode =  GmCommonClass.parseNull((String)GmCommonClass.countryCode);
		String strMidDayOfMonth = "";
		String strDateFormat = "MM/dd/yyyy";
		Date DTMidDayMonth = null;
		
		GmDataStoreVO gmDataStoreVO =
		        (GmDataStoreVO) getPageContext().getRequest().getAttribute("gmDataStoreVO");
		    gmDataStoreVO =
		        ((gmDataStoreVO == null) || ((gmDataStoreVO.getCmpdfmt()).equals(""))) ? GmCommonClass
		            .getDefaultGmDataStoreVO() : gmDataStoreVO;
		//String  strApplJSDateFmt=GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		String  strApplDateFmt = gmDataStoreVO.getCmpdfmt();
	    
	    //System.out.println(sdfDestination.format(date));	
		
		//String strStatusName = db.get("STFLNAME").toString();
		if (strDeptId.equals("2000") ){ //accounting
			if (strStatus.equals("10")){
				return "Open";				
			}
			else if (strStatus.equals("20")){
				return "Approved";
			}
			else if (strStatus.equals("40")){
				return "Cancelled";
			}
			else
				return "";

		}
		
		Calendar mycalender = Calendar.getInstance();
    	String  strApplJSDateFmt	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		//String  strApplDateFmt		=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
    	int currMonth = GmCalenderOperations.getCurrentMonth();
    	String strSysDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
    	int day = mycalender.get(Calendar.DATE);
    	
    	if(strCountryCode.equals("en")){
    		strMidDayOfMonth = currMonth + "/" + "15" + "/" + GmCalenderOperations.getCurrentYear();
    	}else{
    		strMidDayOfMonth = "15" + "/" + currMonth + "/" + GmCalenderOperations.getCurrentYear();
    	}
    		
    	String strMonthFirstDay = GmCalenderOperations.getFirstDayOfMonth(currMonth,strApplDateFmt);
		String strMonthLastDay = GmCalenderOperations.getLastDayOfMonth(currMonth-1,strApplDateFmt);
    	String strDistType = GmCommonClass.parseNull((String)hmResult.get("DIST_TYPE").toString());
    	String strCreditSavedDate = "";    	
    	String strCrdDate = "";
    	if(strCreditSavedDate.equals("") || strCreditSavedDate.equals("null")){
    		if(strDistType.equals("70100")){
    			strCrdDate = strMonthLastDay;
    		}else if(strDistType.equals("70101")){
    			if(strCountryCode.equals("en")){
    				 strDateFormat = "MM/dd/yyyy";
    			}else{
    				 strDateFormat = "dd/MM/yyyy";
    			}
    			
    			if(!GmCalenderOperations.isGreaterThan(strSysDate, strMidDayOfMonth, strApplDateFmt) && !strSysDate.equals(strMidDayOfMonth)){
    				if(strCountryCode.equals("en")){
    					strMidDayOfMonth = (currMonth) + "/" + "15" + "/" + GmCalenderOperations.getCurrentYear();
    				}else{
    		    		strMidDayOfMonth = "15" + "/" + (currMonth) + "/" + GmCalenderOperations.getCurrentYear();
    		    	}
  			    }else if((strSysDate.equals(strMidDayOfMonth) || GmCalenderOperations.isGreaterThan(strSysDate, strMidDayOfMonth, strApplDateFmt)) && !strSysDate.equals(strMonthLastDay)){
    			    strMidDayOfMonth  = strMonthLastDay;
    			}else if(strSysDate.equals(strMonthLastDay)){    				
    				mycalender.add(Calendar.MONTH, 1);
    		    	if(mycalender.get(mycalender.MONTH) < 9){
    		    		strMidDayOfMonth = strCountryCode.equals("en")? "0" + (mycalender.get(mycalender.MONTH)+1) + "/15/" +mycalender.get(mycalender.YEAR): "/15/" +"0" + (mycalender.get(mycalender.MONTH)+1) +mycalender.get(mycalender.YEAR);
    		    	}else{
    		    		strMidDayOfMonth =  strCountryCode.equals("en")? (mycalender.get(mycalender.MONTH)+1) + "/15/" +mycalender.get(mycalender.YEAR): "/15/" + (mycalender.get(mycalender.MONTH)+1) +mycalender.get(mycalender.YEAR);
    		    	}
    			}
    			/*if(GmCalenderOperations.isGreaterThan(strSysDate, strMidDayOfMonth, strApplDateFmt))
    			  {
    				  strMidDayOfMonth = (currMonth + 1) + "/" + "15" + "/" + GmCalenderOperations.getCurrentYear(); 
    			  }*/
    			try{
    			 DTMidDayMonth = GmCommonClass.getStringToDate(strMidDayOfMonth,strDateFormat);
    			strCrdDate =GmCommonClass.getStringFromDate(DTMidDayMonth,strApplDateFmt) ;  
    			}
    			catch(Exception e){
    				
    			}
    		}
    	}else{
    		strCrdDate = strCreditSavedDate;
    	}	
		if(!strStatus.equals("")){
		alListIter = alStatusList.iterator();
		
		strValue.append(" <input type='hidden' name='hCrdDate"+intCount+"' value='"+strCrdDate+"'>"); 
		strValue.append("<select name='cbo_row_stat");
		strValue.append(String.valueOf(intCount));
		strValue.append("' onChange=fnLoadCreditAmounts("+intCount+",this,'"+strCrdDate+"');> ");
		strValue.append(" <option value='0'>[Choose One] </option>");
		while (alListIter.hasNext()) {
			hmStatusList = (HashMap) alListIter.next();
			strOptKey = (String) hmStatusList.get("CODENMALT");
			strOptValue = (String) hmStatusList.get("CODENM");
			if (strStatus.equals(strOptKey)) {
				strOptKey += " selected";
			}
			strValue.append(" <option value= " + strOptKey + " > " + strOptValue + "</option>");
		}

		strValue.append(" </select>");
		}
		return strValue.toString();
	}

	/*public String getLOANERTRANSID() {
		db = (DynaBean) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		String Id = db.get("ID").toString();
		String cnId = db.get("CNID").toString();
		strValue.append("<input type=hidden name = 'ID");
		strValue.append(String.valueOf(countId));
		strValue.append("' ");
		strValue.append(" value='");
		strValue.append(Id);
		strValue.append("'> ");
		strValue.append(cnId);
		countId++;
		return strValue.toString();
	}
	*/
	/*public String getCNID() {
		hmResult = (HashMap) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		String Id = GmCommonClass.parseNull((String)hmResult.get("ID").toString());
		String cnId =GmCommonClass.parseNull((String)hmResult.get("CNID"));
		if(!Id.equals("")){
		strValue.append("<input type=hidden name = 'ID");
		strValue.append(String.valueOf(countId));
		strValue.append("' ");
		strValue.append(" value='");
		strValue.append(Id);
		strValue.append("'> ");
		countId++;
		}
		strValue.append(cnId);
		return strValue.toString();
	}*/
  public String getINCIDENTVALUE(){
	  hmResult = (HashMap) this.getCurrentRowObject();
	  String strIncidentVal= GmCommonClass.parseNull((String)hmResult.get("INCIDENTVALUE"));
	  String strIncidentType =  GmCommonClass.parseNull((String)hmResult.get("INCIDENTTYPE"));
	  String Id = GmCommonClass.parseNull((String)hmResult.get("ID"));
	  if(strIncidentType.equals("Late Return")){
		  if(Id.equals("")){
		  strIncidentVal = "<a href=javascript:fnRefillHistory('"+strIncidentVal+"');>"+strIncidentVal+"</a>";
		  }else{
			  strIncidentVal="";
		  }
	  }else{
		  strIncidentVal = strIncidentVal.replaceAll(",",", ");
	  }
	  return strIncidentVal;
  }
  public String getINCIDENTVALUEEX(){
	  hmResult = (HashMap) this.getCurrentRowObject();
	  String strIncidentVal= GmCommonClass.parseNull((String)hmResult.get("INCIDENTVALUE"));
	  String strIncidentType =  GmCommonClass.parseNull((String)hmResult.get("INCIDENTTYPE"));
	  String Id = GmCommonClass.parseNull((String)hmResult.get("ID"));
	  if(strIncidentType.equals("Late Return")){
		  if(Id.equals("")){
			  strIncidentVal = strIncidentVal;
		  }else{
			  strIncidentVal="";
		  }
	  }else{
		  strIncidentVal = strIncidentVal.replaceAll(",",", ");
	  }
	  return strIncidentVal;
  }
  public Integer getELPDAYS(){
	  hmResult = (HashMap) this.getCurrentRowObject();
	  String strElpDays = GmCommonClass.parseNull((String)hmResult.get("ELPDAYS"));
	  Integer intElpDays = (strElpDays.equals("") || Integer.parseInt(strElpDays) < 0 )?0:Integer.parseInt(strElpDays);
	  return 	intElpDays; 
  }
  public Object getUNITCOST(){
	  hmResult = (HashMap) this.getCurrentRowObject();
	  String Id = GmCommonClass.parseNull((String)hmResult.get("ID"));
	  String strUnitCost = GmCommonClass.parseNull((String)hmResult.get("UNITCOST"));
	  if(!Id.equals("")&&!strUnitCost.equals("")){
	  double dbUnitCost = Double.parseDouble((String)hmResult.get("UNITCOST"));
	  dbUnitCost = Double.valueOf(String.valueOf(dbUnitCost));
	  return dbUnitCost;
	  }else{
		  return "";
	  }
  }
  public Object getEXTENDAMOUNT(){
	  hmResult = (HashMap) this.getCurrentRowObject();
	  String Id = GmCommonClass.parseNull((String)hmResult.get("ID"));
	  if(!Id.equals("")){
	  double dbExtAmount = Double.parseDouble((String)hmResult.get("EXTENDAMOUNT"));
	  dbExtAmount = Double.valueOf(String.valueOf(dbExtAmount));
	  return dbExtAmount;//this.moneyFormat.format(dbExtAmount);
	  }else{
		  return "";
	  }
  }
  public Object  getAMOUNT(){
	  hmResult = (HashMap) this.getCurrentRowObject();
	  String Id = GmCommonClass.parseNull((String)hmResult.get("ID"));
	  if(!Id.equals("")){
	  double dbAmount = Double.parseDouble((String)hmResult.get("AMOUNT"));
	  dbAmount = Double.valueOf(String.valueOf(dbAmount));
	  return dbAmount;//this.moneyFormat.format(dbAmount);
	  }else{
		  return "";
	  }
  }
  public String getINCIDENTTYPE(){
	  hmResult = (HashMap) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		String Id = GmCommonClass.parseNull((String)hmResult.get("ID").toString());
		String strIncidentType =GmCommonClass.parseNull((String)hmResult.get("INCIDENTTYPE"));
		if(!Id.equals("")){
		strValue.append("<input type=hidden name = 'ID");
		strValue.append(String.valueOf(intCount));
		strValue.append("' ");
		strValue.append(" value='");
		strValue.append(Id);
		strValue.append("'> ");		
		}
		strValue.append(strIncidentType);
		return strValue.toString();
  }
  public String getINCIDENTTYPEEX(){
	  hmResult = (HashMap) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		String Id = GmCommonClass.parseNull((String)hmResult.get("ID").toString());
		String strIncidentType =GmCommonClass.parseNull((String)hmResult.get("INCIDENTTYPE"));		
		strValue.append(strIncidentType);
		return strValue.toString();
  }
  public String getCEREDIT_AMOUNT(){
	  StringBuffer strValue = new StringBuffer();
	  double dbAmount = Double.parseDouble(GmCommonClass.parseZero((String)hmResult.get("AMOUNT")));
	  dbAmount = Double.valueOf(String.valueOf(dbAmount));
	  String strStatus = GmCommonClass.parseNull((String)hmResult.get("STFL").toString());	 
	    strValue.append("<input type=\"text\" maxlength=\"7\" disabled='true' name=\"ccharge" + intCount+"\" value=''");		
		strValue.append(" size='7' styleClass=\"InputArea\" title='Only editable for Credited Status' onBlur=\"changeBgColor(this,'#ffffff');\"");    	
		strValue.append("<input type = 'hidden' name = preeditcharge");
		strValue.append(String.valueOf(intCount) );
		strValue.append(" value='");
		strValue.append(dbAmount);
		strValue.append("'>");		
	  return strValue.toString();
  }
  public String getCREDIT_DATE(){
  		String  strApplJSDateFmt	=	GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
  		StringBuffer  strValue = new StringBuffer();
  		// Instead of incident date, need to show the date which is saved for credit date    	
  		String strCreditedDate = GmCommonClass.parseNull((String.valueOf((hmResult.get("CREDITDATE")))));
  		String strStatus = GmCommonClass.parseNull((String.valueOf(hmResult.get("STFL").toString())));
  		if(strStatus.equals("25")){
  			strValue.append(strCreditedDate);
  			strValue.append("&nbsp; <input type=\"hidden\" value=\""+strCreditedDate+"\"  name=\"crediteddt"+intCount+"\"  />&nbsp;");
  		}else{
	  		strCreditedDate = "";
	  		strValue.append("<input type=\"text\" maxlength=\"10\" id=\"crediteddt" + intCount+"\" name=\"crediteddt" + intCount+"\" value=\""+strCreditedDate+"\"");		
	  		strValue.append(" size='9' styleClass=\"InputArea\"  onBlur=\"changeBgColor(this,'#ffffff');\"");
	  		strValue.append("/>&nbsp;<img id='Img_Date' style='cursor:hand'");	
	  		strValue.append("onclick=\"javascript:showSingleCalendar('divCreditDt"+intCount+"','crediteddt"+intCount+"','"+strApplJSDateFmt+"');\" title='Click to open Calendar'");
	  		strValue.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
	  		strValue.append("<div id=\"divCreditDt"+intCount+"\" style=\"position: absolute; z-index: 10;\"></div>");	
	  		strValue.append("<input type=\"hidden\" value=\""+strCreditedDate+"\" name=\"creditdate\" id=");
	  		strValue.append(intCount);
	  		strValue.append("> &nbsp; ");	
  		} 
  		intCount++;
		return strValue.toString();  		
  }
}
