package com.globus.operations.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTItemQuarantineWrapper extends TableDecorator {

	private DynaBean db;
	private String strTxnId;
	private Number strType;	// Changed this from String to Number : Issue 3241
	private String strConType="";
	public DTItemQuarantineWrapper() {
		super();
	}

	public String getCONID() {
		db = (DynaBean) this.getCurrentRowObject();
		strTxnId = (String) db.get("CONID");
		strType = (Number)db.get("TID");
		strConType = (String)db.get("CON_TYPE");
		return "<a href=javascript:fnPrintVer('" + strTxnId + "','" + strType + "','" + strConType + "')> " + strTxnId + "</a>";
	}
}
