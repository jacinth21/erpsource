package com.globus.operations.displaytag.beans;
import java.text.DecimalFormat;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

public class DTSubPartDetailWrapper extends TableDecorator{

	
	 	private String strParNumber; 
	    private int intLevel = 0;
	    
	    private String strSpace = "";
	    private DynaBean db ;
	    
	    public StringBuffer  strValue = new StringBuffer();
	   
	    public String getPARTNUM()
	    {
	    	String strTab = "";
	        db =    (DynaBean) this.getCurrentRowObject();
	        
	        strParNumber = String.valueOf(db.get("PARTNUM"));
	        intLevel = Integer.parseInt(String.valueOf(db.get("LEVEL")));
	        if (intLevel >1)  strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	         for (int i = 1; i < intLevel; i++)
	         { 
	        	 	strTab = strTab + strSpace;
	         }
	         strSpace="";
	        strParNumber = strTab +strParNumber;
	          
	      //   System.out.println("intLevel is ............ : "+intLevel);        
	     //    System.out.println("strParNumber is ............ : "+strParNumber);  
	        
	        return strParNumber;
	        //return "<input type = text name = parvalue size = 10 value="+strPar+">";
	        
	    }
}
