package com.globus.operations.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import javax.servlet.jsp.PageContext;

public class DTLEHistoryViewWrapper extends TableDecorator {

	private String strISEXT = "";
	GmLogger log = GmLogger.getInstance();
	//private HashMap hmRow = new HashMap();
	String strType="";
	String strConsignId="";
	private DynaBean db;
	
	public DTLEHistoryViewWrapper() {
		super();
	}
	public void init(PageContext context, Object decorated, TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		strType = GmCommonClass.parseNull((String) getPageContext().getAttribute("TYPE", PageContext.REQUEST_SCOPE));
	}

	public String getISEXT() {
		db = (DynaBean) this.getCurrentRowObject();
		strISEXT = String.valueOf(db.get("ISEXT"));

		if(!strType.equals("ReloadUsage"))
			strConsignId = String.valueOf(db.get("CONID"));
		return "<a href=javascript:fnViewSummary('" + strISEXT + "','"+strConsignId+"')> <font color='red'>" + strISEXT + " </font></a>";
	}

}
