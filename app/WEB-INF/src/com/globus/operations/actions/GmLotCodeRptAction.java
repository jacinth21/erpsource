/**
 * 
 */
package com.globus.operations.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmLotCodeRptBean;
import com.globus.operations.forms.GmLotCodeRptForm;

/**
 * @author arajan
 * 
 */
public class GmLotCodeRptAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadLotCodeProdInfo: For fetching the product information to screen
   * 
   * @exception AppError
   */
  public ActionForward loadLotCodeProdInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotCodeRptForm gmLotCodeRptForm = (GmLotCodeRptForm) actionForm;
    gmLotCodeRptForm.loadSessionParameters(request);
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmLotCodeRptForm);
    String strLotNum = GmCommonClass.parseNull(gmLotCodeRptForm.getLotNum());
    String strOpt = GmCommonClass.parseNull(gmLotCodeRptForm.getStrOpt());
    String strScreen = GmCommonClass.parseNull(request.getParameter("hscreen"));
    request.setAttribute("hscreen", strScreen);
    String strXMLString = "";
    if (strOpt.equalsIgnoreCase("Load")) {
      gmLotCodeRptForm.setStrOpt("Load");
      hmReturn = gmLotCodeRptBean.fetchLotCodeProdInfo(strLotNum);
      strXMLString = gmXMLParserUtility.createXMLString(hmReturn);
      response.setContentType("text/xml");
      response.setCharacterEncoding("UTF-8");
      response.setHeader("Cache-Control", "no-cache");

      PrintWriter pw = response.getWriter();
      pw.write(strXMLString);
      pw.flush();
      return null;
    } else {
      return actionMapping.findForward("gmLotCodeReport");
    }
  }

  /**
   * loadLotCodeDonorInfo: For fetching the donor details to screen
   * 
   * @exception AppError
   */
  public ActionForward loadLotCodeDonorInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotCodeRptForm gmLotCodeRptForm = (GmLotCodeRptForm) actionForm;
    gmLotCodeRptForm.loadSessionParameters(request);
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    String strXMLString = "";
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmLotCodeRptForm);
    String strLotNum = GmCommonClass.parseNull(gmLotCodeRptForm.getLotNum());
    HashMap hmReturn = new HashMap();
    hmReturn = gmLotCodeRptBean.fetchLotCodeDonorInfo(strLotNum);
    strXMLString = gmXMLParserUtility.createXMLString(hmReturn);
    response.setContentType("text/xml");
    response.setCharacterEncoding("UTF-8");
    response.setHeader("Cache-Control", "no-cache");

    PrintWriter pw = response.getWriter();
    pw.write(strXMLString);
    pw.flush();
    return null;
  }

  /**
   * loadLotCodeInfo: For fetching the lot information to screen
   * 
   * @exception AppError
   */
  public ActionForward loadLotCodeInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotCodeRptForm gmLotCodeRptForm = (GmLotCodeRptForm) actionForm;
    gmLotCodeRptForm.loadSessionParameters(request);
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    String strXMLString = "";
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmLotCodeRptForm);
    String strLotNum = GmCommonClass.parseNull(gmLotCodeRptForm.getLotNum());
    HashMap hmReturn = new HashMap();
    hmReturn = gmLotCodeRptBean.fetchLotCodeInfo(strLotNum);
    strXMLString = gmXMLParserUtility.createXMLString(hmReturn);
    response.setContentType("text/xml");
    response.setCharacterEncoding("UTF-8");
    response.setHeader("Cache-Control", "no-cache");

    PrintWriter pw = response.getWriter();
    pw.write(strXMLString);
    pw.flush();
    return null;
  }

  /**
   * fetchPartRedesignation: For fetching the recieving information to screen
   * 
   * @exception AppError
   */
  public ActionForward loadLotCodeRecInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotCodeRptForm gmLotCodeRptForm = (GmLotCodeRptForm) actionForm;
    gmLotCodeRptForm.loadSessionParameters(request);
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    String strXmlGridData = "";
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    String strScreen = GmCommonClass.parseNull(request.getParameter("hscreen"));
    request.setAttribute("hscreen", strScreen);
    log.debug("strScreen>>>" + strScreen);
    hmParam = GmCommonClass.getHashMapFromForm(gmLotCodeRptForm);
    String strLotNum = GmCommonClass.parseNull(gmLotCodeRptForm.getLotNum());
    ArrayList alResult = new ArrayList();
    alResult = gmLotCodeRptBean.fetchLotCodeRecInfo(strLotNum);

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmResult.put("COMPANYLOCALE", strSessCompanyLocale);
    hmResult.put("ALRESULT", alResult);
    hmResult.put("hscreen", strScreen);
    hmResult.put("TEMPLETENAME", "GmLotCodeReceInfo.vm");
    strXmlGridData = generateOutPut(hmResult);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(strXmlGridData);
    pw.flush();
    return null;
  }

  /**
   * loadLotCodeOpentxn: For fetching the open transaction details
   * 
   * @exception AppError
   */
  public ActionForward loadLotCodeOpentxn(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotCodeRptForm gmLotCodeRptForm = (GmLotCodeRptForm) actionForm;
    gmLotCodeRptForm.loadSessionParameters(request);
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    String strXmlGridData = "";
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmLotCodeRptForm);
    String strLotNum = GmCommonClass.parseNull(gmLotCodeRptForm.getLotNum());
    ArrayList alResult = new ArrayList();
    alResult = gmLotCodeRptBean.fetchLotCodeOpentxn(strLotNum);

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmResult.put("COMPANYLOCALE", strSessCompanyLocale);
    hmResult.put("ALRESULT", alResult);
    hmResult.put("TEMPLETENAME", "GmLotCodeOpenTxn.vm");
    strXmlGridData = generateOutPut(hmResult);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(strXmlGridData);
    pw.flush();
    return null;
  }

  /**
   * loadLotCodeInvInfo: For fetching the inventory information
   * 
   * @exception AppError
   */
  public ActionForward loadLotCodeInvInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotCodeRptForm gmLotCodeRptForm = (GmLotCodeRptForm) actionForm;
    gmLotCodeRptForm.loadSessionParameters(request);
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    String strXmlGridData = "";
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmLotCodeRptForm);
    String strLotNum = GmCommonClass.parseNull(gmLotCodeRptForm.getLotNum());
    ArrayList alResult = new ArrayList();
    alResult = gmLotCodeRptBean.fetchLotCodeInvInfo(strLotNum);

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmResult.put("COMPANYLOCALE", strSessCompanyLocale);
    hmResult.put("ALRESULT", alResult);
    hmResult.put("TEMPLETENAME", "GmLotCodeInventoryInfo.vm");
    strXmlGridData = generateOutPut(hmResult);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(strXmlGridData);
    pw.flush();
    return null;
  }

  /**
   * loadLotCodeTxnHistory: For fetching the transaction history
   * 
   * @exception AppError
   */
  public ActionForward loadLotCodeTxnHistory(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmLotCodeRptForm gmLotCodeRptForm = (GmLotCodeRptForm) actionForm;
    gmLotCodeRptForm.loadSessionParameters(request);
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    String strXmlGridData = "";
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmLotCodeRptForm);
    String strLotNum = GmCommonClass.parseNull(gmLotCodeRptForm.getLotNum());
    ArrayList alResult = new ArrayList();
    alResult = gmLotCodeRptBean.fetchLotCodeTxnHistory(strLotNum);

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmResult.put("COMPANYLOCALE", strSessCompanyLocale);
    hmResult.put("ALRESULT", alResult);
    hmResult.put("TEMPLETENAME", "GmLotCodeTxnHistory.vm");
    strXmlGridData = generateOutPut(hmResult);

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(strXmlGridData);
    pw.flush();
    return null;
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strTempName = GmCommonClass.parseNull((String) hmParam.get("TEMPLETENAME"));
    ArrayList alList = new ArrayList();
    alList = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alList", alList);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmLotCodeReport",
        GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"))));
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setTemplateName(strTempName);
    return templateUtil.generateOutput();
  }
}

