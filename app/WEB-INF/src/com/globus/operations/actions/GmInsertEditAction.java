package com.globus.operations.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmInsertEditBean;
import com.globus.operations.forms.GmInsertProcessForm;

public class GmInsertEditAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveInsertDetail
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward saveInsertDetail(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Intitalization
    GmInsertProcessForm gmInsertForm = (GmInsertProcessForm) actionForm;
    // Bean Initalization
    gmInsertForm.loadSessionParameters(request);
    GmInsertEditBean gmInsertEditBean = new GmInsertEditBean(getGmDataStoreVO());
    GmCommonClass gmCommonClass = new GmCommonClass();

    HashMap hmParams = new HashMap();
    hmParams = GmCommonClass.getHashMapFromForm(gmInsertForm);
    log.debug(" hmParams ?"+hmParams);
    gmInsertEditBean.saveInsertDetail(hmParams);

    return actionMapping.findForward("FetchByOrderID");
  }
  
  /**
   * fetchInsertDetail
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchInsertDetail(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	  instantiate(request, response);
	  GmInsertProcessForm gmInsertForm = (GmInsertProcessForm) actionForm;
	  gmInsertForm.loadSessionParameters(request);
	 
    GmInsertEditBean gmInsertEditBean = new GmInsertEditBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonClass gmCommonClass = new GmCommonClass();
    ArrayList alInsertDetails = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmTemplateParam = new HashMap();
    HashMap hmAccess = new HashMap();
    String strStatusFl = "";
    String strUserId = "";
    String strAccessFl = "";
    String strSessCompanyLocale =
            GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strTxnID =
            (request.getParameter("txnID")) == null ? gmInsertForm.getTxnID() : request
                .getParameter("txnID");
    strUserId = GmCommonClass.parseNull(gmInsertForm.getUserId());
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "ADD_INSERT_ACCESS"));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    
    hmReturn =  GmCommonClass.parseNullHashMap(gmInsertEditBean.fetchInsertDetail(strTxnID));
    alInsertDetails = (ArrayList) hmReturn.get("ALINSERTDETAILS");
    strStatusFl = (String) hmReturn.get("STATUSFL");
    gmInsertForm.setAlInsertDetail(alInsertDetails);
    gmInsertForm.setStatusFlag(strStatusFl);
    gmInsertForm.setAccessFlag(strAccessFl);
    
 // Get XML Grid Data
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmTemplateParam.put("VMFILEPATH", "properties.labels.operations.GmInsertEdit");
    hmTemplateParam.put("TEMPLATE", "gmInsertEdit.vm");
    hmTemplateParam.put("TEMPLATEPATH","operations/inventory/templates");
    hmTemplateParam.put("ALRESULT", alInsertDetails);
   
    // XML String
    String strXmlString = getXmlGridData(hmTemplateParam);
    gmInsertForm.setGridXmlData(strXmlString);
  
    return actionMapping.findForward("GmInsertEditDtl");

  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParam) throws AppError {
    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE"));
    String strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATEPATH"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    String strSesscompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
    		strSesscompanyLocale));
    templateUtil.setTemplateName(strTemplateName);
    
    return templateUtil.generateOutput();
  }

}
