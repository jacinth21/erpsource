package com.globus.operations.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmPartConversionBean;
import com.globus.operations.forms.GmPartConversionForm;
import com.globus.operations.forms.GmPartConversionSummaryForm;

public class GmPartConversionSummaryAction extends GmDispatchAction {

  public ActionForward reportPCSummary(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmPartConversionSummaryForm gmPartConversionSummaryForm = (GmPartConversionSummaryForm) form;
    gmPartConversionSummaryForm.loadSessionParameters(request);
    GmPartConversionBean gmPartConversionBean = new GmPartConversionBean(getGmDataStoreVO());
    List alResult;
    HashMap hmParam = new HashMap();
    String strOpt;
    String strFromDate = "";
    String strToDate = "";
    strFromDate = GmCommonClass.parseNull(gmPartConversionSummaryForm.getFromDate());
    strToDate = GmCommonClass.parseNull(gmPartConversionSummaryForm.getToDate());
    try {

      gmPartConversionSummaryForm.setAlType(gmPartConversionBean.fetchInventoryTypeList());

      strOpt = gmPartConversionSummaryForm.getStrOpt();

      if (strOpt.equals("Report")) {
        hmParam = GmCommonClass.getHashMapFromForm(gmPartConversionSummaryForm);
        alResult = gmPartConversionBean.loadPCSummary(hmParam).getRows();
        gmPartConversionSummaryForm.setLdtResult(alResult);
      }
      gmPartConversionSummaryForm.setFromDate(strFromDate);
      gmPartConversionSummaryForm.setToDate(strToDate);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }

  public ActionForward reportPCDetail(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.
    GmPartConversionBean gmPartConversionBean = new GmPartConversionBean();
    GmPartConversionForm gmPartConversionForm = (GmPartConversionForm) form;
    gmPartConversionForm.loadSessionParameters(request);
    Map hmPCData;
    RowSetDynaClass rdPCDetail;
    Map hmPCHeader;

    gmPartConversionForm.loadSessionParameters(request);
    String strPcID = gmPartConversionForm.getPcID();
    String strOpt = gmPartConversionForm.getStrOpt();
    log.debug("PCID clicked is  : " + strPcID);

    try {
      if (strOpt != "FromMenu") {
        hmPCData = gmPartConversionBean.fetchPCDetail(strPcID);
        hmPCHeader = (HashMap) hmPCData.get("HMPCHEADER");
        GmCommonClass.getFormFromHashMap(gmPartConversionForm, (HashMap) hmPCHeader);

        rdPCDetail = (RowSetDynaClass) hmPCData.get("RDPCDETAIL");
        List alResult = rdPCDetail.getRows();
        gmPartConversionForm.setLdtResult(alResult);
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }
}
