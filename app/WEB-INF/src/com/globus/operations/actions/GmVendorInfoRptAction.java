package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.beans.GmVendorReportBean;
import com.globus.operations.forms.GmVendorInfoRptForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmVendorInfoRptAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmVendorInfoRptForm gmVendorInfoRptForm = (GmVendorInfoRptForm) form;
    GmVendorReportBean gmVendorReportBean = new GmVendorReportBean(getGmDataStoreVO());
    GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
    
    ArrayList alResult = new ArrayList();
    String strOpt = gmVendorInfoRptForm.getStrOpt();
    HashMap hmParam = new HashMap();
    String strApplnDateFmt = "";
    String strApplJSDateFmt = "";
    String strSessCompanyLocale = "";
    
    if (strOpt.equals("Load")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmVendorInfoRptForm);
      strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
      strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      alResult = GmCommonClass.parseNullArrayList(gmVendorReportBean.loadVendorInfoReport(hmParam));
      gmSearchCriteria = getSearchCriteria(gmVendorInfoRptForm);
      gmSearchCriteria.query(alResult);
      gmVendorInfoRptForm.setAlResult(alResult);
      
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmParam.put("ALRESULT", alResult);
      hmParam.put("TEMPNAME", "GmVendorInfoRpt.vm");
      hmParam.put("TEMPPATH", "operations/templates");
      hmParam.put("RESOURCEBNDLPATH", "properties.labels.operations.purchasing.GmVendorInfoReport");
      hmParam.put("SESSDATEFMT", strApplnDateFmt);
      
      String strXmlGridData = getXmlGridData(hmParam);
      gmVendorInfoRptForm.setStrXmlGridData(strXmlGridData);
    }

    // Load Vendor List drop down
    HashMap hmTemp = gmVendorBean.getVendorList(new HashMap());
    gmVendorInfoRptForm.setAlVendorList((ArrayList) hmTemp.get("VENDORLIST"));

    // Load Location drop down
    gmVendorInfoRptForm.setAlLocationList(GmCommonClass.getCodeList("STATE", getGmDataStoreVO()));

    // Load Month History List drop down
    gmVendorInfoRptForm.setAlMonthList(GmCommonClass.getCodeList("MONHS"));

    // Load Project List drop down
    gmVendorInfoRptForm.setAlProjectList(gmProjectBean.reportProject(new HashMap()));

    // Load PO Type drop down
    gmVendorInfoRptForm.setAlPOTypeList(GmCommonClass.getCodeList("POTYP"));

    // Load OPerators drop downs
    gmVendorInfoRptForm.setAlOperatorsList(GmCommonClass.getCodeList("OPERS"));


    return mapping.findForward("success");
  }

  private GmSearchCriteria getSearchCriteria(GmVendorInfoRptForm gmVendorInfoRptForm) {
    GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();

    if (!gmVendorInfoRptForm.getCurImplant().equals("")
        && !gmVendorInfoRptForm.getCurImplantOpr().equals("0")) {
      gmSearchCriteria.addSearchCriteria("IMP_COUNT", gmVendorInfoRptForm.getCurImplant(),
          gmVendorInfoRptForm.getCurImplantOpr());
    }

    if (!gmVendorInfoRptForm.getCurIns().equals("")
        && !gmVendorInfoRptForm.getCurInsOpr().equals("0")) {
      gmSearchCriteria.addSearchCriteria("INS_COUNT", gmVendorInfoRptForm.getCurIns(),
          gmVendorInfoRptForm.getCurInsOpr());
    }

    if (!gmVendorInfoRptForm.getCoreImplant().equals("")
        && !gmVendorInfoRptForm.getCoreImplantOpr().equals("0")) {
      gmSearchCriteria.addSearchCriteria("CORE_IMP_COUNT", gmVendorInfoRptForm.getCoreImplant(),
          gmVendorInfoRptForm.getCoreImplantOpr());
    }

    if (!gmVendorInfoRptForm.getCoreIns().equals("")
        && !gmVendorInfoRptForm.getCoreInsOpr().equals("0")) {
      gmSearchCriteria.addSearchCriteria("CORE_INS_COUNT", gmVendorInfoRptForm.getCoreIns(),
          gmVendorInfoRptForm.getCoreInsOpr());
    }

    if (!gmVendorInfoRptForm.getNcoreImplant().equals("")
        && !gmVendorInfoRptForm.getNcoreImplantOpr().equals("0")) {
      gmSearchCriteria.addSearchCriteria("NON_CORE_IMP_COUNT",
          gmVendorInfoRptForm.getNcoreImplant(), gmVendorInfoRptForm.getNcoreImplantOpr());
    }

    if (!gmVendorInfoRptForm.getNcoreIns().equals("")
        && !gmVendorInfoRptForm.getNcoreInsOpr().equals("0")) {
      gmSearchCriteria.addSearchCriteria("NON_CORE_INS_COUNT", gmVendorInfoRptForm.getNcoreIns(),
          gmVendorInfoRptForm.getNcoreInsOpr());
    }

    return gmSearchCriteria;
  }
  
  /**
   * @param hmParam
   * @return
   * @throws AppError
   */
  //display tag is changed into dhtmlx grid - PMT-32374
  public String getXmlGridData(HashMap hmParam) throws AppError {
      GmTemplateUtil templateUtil = new GmTemplateUtil();
      ArrayList alResult = new ArrayList();
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
      String strTempletPath = GmCommonClass.parseNull((String) hmParam.get("TEMPPATH"));
      String strTempletName = GmCommonClass.parseNull((String) hmParam.get("TEMPNAME"));
      alResult = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("ALRESULT"));
      String strResourceBdlePath = GmCommonClass.parseNull((String) hmParam.get("RESOURCEBNDLPATH"));
      templateUtil.setDataList("alResult", alResult);
      templateUtil.setDataMap("hmApplnParam", hmParam);
      templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strResourceBdlePath, strSessCompanyLocale));
      templateUtil.setTemplateSubDir(strTempletPath);
      templateUtil.setTemplateName(strTempletName);
      return templateUtil.generateOutput();
    }
}
