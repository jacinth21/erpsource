package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.forms.GmControlValidationForm;

public class GmControlValidationAction extends GmAction {

	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			 HttpServletRequest request, HttpServletResponse response)
	     throws Exception {

		  Logger log = GmLogger.getInstance(this.getClass().getName());
	        String strPartcnum="";
	        String strPartNumber="";
	        String strControlNumber="";
            String strMsg = "";
	        String strDispatchTo = "gmcontrolValidation";

	        GmControlValidationForm  gmControlValidationForm= (GmControlValidationForm)form;
	        gmControlValidationForm.loadSessionParameters(request);
	        HttpSession session=request.getSession(false);
	        GmOperationsBean gmOper = new GmOperationsBean();
	        ArrayList alReturn= new ArrayList();
	        HashMap hmReturn = new HashMap();
	        String strOpt = GmCommonClass.parseNull(gmControlValidationForm.getStrOpt()); 
	        strPartcnum = GmCommonClass.parseNull(gmControlValidationForm.getPartcnum ());
	        StringTokenizer  partCnum=new StringTokenizer(strPartcnum,"^");
	        while(partCnum.hasMoreTokens()){
	        	strPartNumber  = partCnum.nextToken();
	        	strControlNumber  = partCnum.nextToken();
	        }
	        if(!strPartNumber.equals("")&&!strControlNumber.equals("")){
            log.debug("strPartNumber==>"+strPartNumber+"strControlNumber==>"+strControlNumber);
            hmReturn= gmOper.checkPartControlNumber(strPartNumber, strControlNumber );
	        alReturn.add(hmReturn);
	         log.debug("alReturn==>"+alReturn);
	         gmControlValidationForm.setAlResult(alReturn);
	        }
	        return mapping.findForward(strDispatchTo);

	}

}
