package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.forms.GmVendorPOReportForm;
import com.globus.operations.purchasing.beans.GmVendorPOReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;

/**
 * GmVendorPOReportAction - Class is to access Vendor PO reports methods
 */
public class GmVendorPOReportAction extends GmDispatchAction
{
	/**
	 * Log variable Initialize
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
 
    /**
     * loadVendorPOReport method is to load PO Vendor Screen
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward loadVendorPOReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
    	  instantiate(request, response);
    	  HttpSession session = request.getSession(false);
    	     
    	  String strGridXmlData = "";
    	  String strFromDt = "";
    	  String strToDt = "";
    	  String strPOType = "";
    	  String strUserId = "";
    	  String strMonthBeginDate = "";
    	  String strTodaysDate = "";
    	  HashMap hmReturn = new HashMap();
    	  HashMap hmResult = new HashMap();
    	  HashMap hmParam = new HashMap();
    	  ArrayList alPODetails = new ArrayList();
    	  
    	  GmVendorPOReportForm gmVendorPOReportForm = (GmVendorPOReportForm) form;
    	  hmParam = GmCommonClass.parseNullHashMap((HashMap) GmCommonClass.getHashMapFromForm(gmVendorPOReportForm));
    	  strTodaysDate = GmCommonClass.parseNull((String) GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt())); // (String) // session.getAttribute("strSessTodaysDate");
    	  strMonthBeginDate = GmCommonClass.parseNull((String) GmCalenderOperations.getFirstDayOfMonth(GmCalenderOperations.getCurrentMonth() - 1, 
    			  getGmDataStoreVO().getCmpdfmt()));// strTodaysDate.replaceAll("/\\d\\d/", "/01/");

	      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
	      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
          
          strFromDt = GmCommonClass.parseNull((String) hmParam.get("FRMDT"));
          strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
          strPOType = GmCommonClass.parseNull((String) hmParam.get("POTYPE"));
          strFromDt = strFromDt.equals("") ? strMonthBeginDate :  strFromDt;
          hmParam.put("FRMDT",strFromDt);
          strToDt = strToDt.equals("") ? strTodaysDate :  strToDt;
          hmParam.put("TODT",strToDt);
          strPOType = strPOType.equals("") ||  strPOType.equals("0") ? "3100" :  strPOType;
          hmParam.put("POTYPE",strPOType);
          
          strUserId = GmCommonClass.parseNull((String) hmParam.get("EMPID"));
          hmParam.put("USERID",strUserId);          
          log.debug("hmParam>>getPOListCall>>"+hmParam);                  
          gmVendorPOReportForm.setAlPODetails(GmCommonClass.parseNullArrayList((ArrayList) alPODetails));
        
          gmVendorPOReportForm = (GmVendorPOReportForm) GmCommonClass.getFormFromHashMap(gmVendorPOReportForm, hmParam); 
          
          loadDropdownValues(gmVendorPOReportForm); // To Load Dropdown Values for the filters
	      return mapping.findForward("gmvendorrpt");
    }
    
    
    
    /**
     * fetchVendorPOReport method is to load Vendor PO reports
     * @param mapping, form, request, response
     * @return
     * @throws Exception
     */
    public ActionForward fetchVendorPOReport(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
    	  instantiate(request, response);
    	  HttpSession session = request.getSession(false);
    	     
    	  String strGridXmlData = "";
    	  String strFromDt = "";
    	  String strToDt = "";
    	  String strPOType = "";
    	  String strUserId = "";
    	  String strMonthBeginDate = "";
    	  String strTodaysDate = "";
    	  String strHoldFL = "";
    	  HashMap hmReturn = new HashMap();
    	  HashMap hmResult = new HashMap();
    	  HashMap hmParam = new HashMap();
    	  
    	  GmVendorPOReportForm gmVendorPOReportForm = (GmVendorPOReportForm) form;
    	  hmParam = GmCommonClass.parseNullHashMap((HashMap) GmCommonClass.getHashMapFromForm(gmVendorPOReportForm));
    	  strTodaysDate = GmCommonClass.parseNull((String) GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt())); // (String) // session.getAttribute("strSessTodaysDate");
    	  strMonthBeginDate = GmCommonClass.parseNull((String) GmCalenderOperations.getFirstDayOfMonth(GmCalenderOperations.getCurrentMonth() - 1, 
    			  getGmDataStoreVO().getCmpdfmt()));// strTodaysDate.replaceAll("/\\d\\d/", "/01/");

	      GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
	      GmVendorPOReportBean gmVendorPOReportBean = new GmVendorPOReportBean(getGmDataStoreVO());
	      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
          
          strFromDt = GmCommonClass.parseNull((String) hmParam.get("FRMDT"));
          strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
          strPOType = GmCommonClass.parseNull((String) hmParam.get("POTYPE"));
          strFromDt = strFromDt.equals("") ? strMonthBeginDate :  strFromDt;
          hmParam.put("FRMDT",strFromDt);
          strToDt = strToDt.equals("") ? strTodaysDate :  strToDt;
          hmParam.put("TODT",strToDt);
          strPOType = strPOType.equals("0") ? "" :  strPOType;
          hmParam.put("POTYPE",strPOType);
          
          strUserId = GmCommonClass.parseNull((String) hmParam.get("EMPID"));
          hmParam.put("USERID",strUserId);
          log.debug("hmParam>>getPOListCall>>"+hmParam);
          hmParam.put("CHECKVPACCESSFL", "Y");
  		  hmResult = GmCommonClass.parseNullHashMap((HashMap) gmVendorPOReportBean.getVendorPOList(hmParam));
  		  hmParam.remove("CHECKVPACCESSFL");          
          gmVendorPOReportForm.setAlPODetails(GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("POLIST")));
          
          hmReturn.put("REPORT", hmResult.get("POLIST"));          
          hmReturn.put("gmDataStoreVO", getGmDataStoreVO());
          strGridXmlData = GmCommonClass.parseNull((String) gmVendorPOReportBean.getVDXmlGridData(hmReturn));
          gmVendorPOReportForm = (GmVendorPOReportForm) GmCommonClass.getFormFromHashMap(gmVendorPOReportForm, hmParam); 
          gmVendorPOReportForm.setStrGridXmlData(strGridXmlData);
          
          // Fetch Hold Access Flag 
  		  strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
          strHoldFL = GmCommonClass.parseNull((String) fetchHoldAccessFl(strUserId));
          gmVendorPOReportForm.setStrHoldFL(strHoldFL);
          
          loadDropdownValues(gmVendorPOReportForm); // To Load Dropdown Values for the filters
	      return mapping.findForward("gmvendorrpt");
    }
    
    /**
     * loadDropdownValues - Method is to generate drop down values and set to gmVendorPOReportForm
     * @param gmVendorPOReportForm
     */
    public void loadDropdownValues(GmVendorPOReportForm gmVendorPOReportForm){
    	HashMap hmResult = new HashMap();
    	HashMap hmTemp = new HashMap();
    	GmCommonClass gmCommon = new GmCommonClass();
    	GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    	GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
	    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
	      
    	hmTemp.put("COLUMN", "ID");
	    hmTemp.put("STATUSID", "20301"); // Filter all approved projects
	    gmVendorPOReportForm.setAlProject(GmCommonClass.parseNullArrayList(gmProj.reportProject(hmTemp)));
	      
	    hmTemp.clear();
	    hmTemp.put("CHECKACTIVEFL", "N"); // check for active flag status in vendor list	  
	    hmTemp.put("CHECKVPACCESSFL", "Y"); // check for Vendor portal Access Flag in vendor list	
	    hmResult = GmCommonClass.parseNullHashMap((HashMap) gmVendor.getVendorList(hmTemp));
	      
        gmVendorPOReportForm.setAlVendorList(GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("VENDORLIST")));
        gmVendorPOReportForm.setAlUsers(GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList("300", "W")));
        gmVendorPOReportForm.setAlPOAction(GmCommonClass.parseNullArrayList(gmCommon.getCodeList("VPOAVT")));
        gmVendorPOReportForm.setAlTypes(GmCommonClass.parseNullArrayList(gmCommon.getCodeList("POTYP")));
        gmVendorPOReportForm.setAlStatus(GmCommonClass.parseNullArrayList(gmCommon.getCodeList("VPRSTS")));    	
    }
    
	/**
	 * publishMultiplePO-Method is to Publish Multiple PO Reports
	 * @param mapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward publishMultiplePO(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		instantiate(request, response);
		HttpSession session = request.getSession(false);

		GmVendorPOReportForm gmVendorPOReportForm = (GmVendorPOReportForm) form;
		String strPOId = "";
		String strPOType = "";
		String strUserId = "";
		String strGridXmlData = "";
		String strCompanyInfo = "";
		String strHoldFL = "";
		HashMap hmResult = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmJMSParam = new HashMap();
		hmParam = GmCommonClass.parseNullHashMap((HashMap) GmCommonClass.getHashMapFromForm(gmVendorPOReportForm));
		GmPurchaseBean gmPurc = new GmPurchaseBean(getGmDataStoreVO());
		GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());
		GmVendorPOReportBean gmVendorPOReportBean = new GmVendorPOReportBean(
				getGmDataStoreVO());
		strPOId = GmCommonClass.parseNull((String) hmParam.get("HPOSTRING"));
		strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        strPOType = GmCommonClass.parseNull((String) hmParam.get("POTYPE"));
        
		// Code For Sending Email
		strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));		
		hmJMSParam.put("REFID", strPOId);
		hmJMSParam.put("REFTYPE", "108440"); //purchase order type
		hmJMSParam.put("REFACTION", "108400"); //update Purchase Order Action
		hmJMSParam.put("MAILSTATUS", "0"); //to send instant mail
		hmJMSParam.put("USERID", strUserId);
		hmJMSParam.put("COMPANYINFO", strCompanyInfo);
		log.debug("hmJMSParam>>>>>"+hmJMSParam);
		hmResult = GmCommonClass.parseNullHashMap((HashMap) gmVendorPOReportBean.publishBlkPO(strPOId, "Y", strUserId));
		gmInitiateVendorJMS.initVendorEmailJMS(hmJMSParam);    // Po jms call
		
		strPOType = strPOType.equals("0") ? "" :  strPOType;
        hmParam.put("POTYPE",strPOType);
		strUserId = GmCommonClass.parseNull((String) hmParam.get("EMPID"));
		hmParam.put("USERID", strUserId);
		log.debug("hmParam>>publishBlkPOReport>>>>getPOListCall>>" + hmParam);
		hmParam.put("CHECKVPACCESSFL", "Y");
		hmResult = GmCommonClass.parseNullHashMap((HashMap) gmVendorPOReportBean.getVendorPOList(hmParam));
		hmParam.remove("CHECKVPACCESSFL");
		gmVendorPOReportForm.setAlPODetails(GmCommonClass
				.parseNullArrayList((ArrayList) hmResult.get("POLIST")));

		hmReturn.put("REPORT", hmResult.get("POLIST"));
		hmReturn.put("gmDataStoreVO", getGmDataStoreVO());
		strGridXmlData = GmCommonClass.parseNull((String) gmVendorPOReportBean.getVDXmlGridData(hmReturn));
		gmVendorPOReportForm = (GmVendorPOReportForm) GmCommonClass
				.getFormFromHashMap(gmVendorPOReportForm, hmParam);
		gmVendorPOReportForm.setStrGridXmlData(strGridXmlData);
		
		// Fetch Hold Access Flag 
		strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        strHoldFL = GmCommonClass.parseNull((String) fetchHoldAccessFl(strUserId));
        gmVendorPOReportForm.setStrHoldFL(strHoldFL);
        
		loadDropdownValues(gmVendorPOReportForm); // To Load Dropdown Values for the filters
		return mapping.findForward("gmvendorrpt");
	}
	
	
	/**
     * fetchHoldAccessFl - Method is to fetch hold access fl for the given user id
     * @param strUserID
     */
    public String fetchHoldAccessFl(String strUserID){
    	log.debug("strUserID>>>>>"+strUserID);
    	GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    	HashMap hmAccess = new HashMap();
    	hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID, "PO_HOLD_GRP"));   
    	String strHoldFL = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
    	
    	log.debug("strHoldFL>>>>>"+strHoldFL);
    	return strHoldFL;
    }
    
}
