package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmPartConversionBean;
import com.globus.operations.forms.GmPartConversionForm;

public class GmPartConversionAction extends GmAction{

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
        	GmPartConversionBean gmPartConversionBean = new GmPartConversionBean();
	        GmPartConversionForm gmPartConversionForm = (GmPartConversionForm)form;
	        gmPartConversionForm.loadSessionParameters(request);
	
	        String strOpt = gmPartConversionForm.getStrOpt();
	        String strPartNum = gmPartConversionForm.getPartNumber();
	        String strInvType = gmPartConversionForm.getFromInvType();
	        
	        HashMap hmParam = GmCommonClass.getHashMapFromForm(gmPartConversionForm);
	        HashMap hmValues = new HashMap();
	        ArrayList alPartDetails = new ArrayList();
	        ArrayList alInvType = new ArrayList();
	        
	        log.debug("values in param" +hmParam);
      
	       if (strOpt.equals("save")) {
	    	   String strPCId = gmPartConversionBean.savePartConversionDetails(hmParam);
	           log.debug(" PC Id generated after save " +strPCId);
	           gmPartConversionForm.setStrOpt("");
	           gmPartConversionForm.setPcID(strPCId);
	           return mapping.findForward("savesuccess");
	        }
	       
	        if(!strPartNum.equals("")){
	        	hmValues = gmPartConversionBean.loadFromPartDetails(strPartNum,strInvType);
	        	gmPartConversionForm = (GmPartConversionForm)GmCommonClass.getFormFromHashMap(gmPartConversionForm,hmValues);
	        	alPartDetails = gmPartConversionBean.loadToPartDetails(strPartNum,strInvType);
	        	gmPartConversionForm.setAlPartDetails(alPartDetails);
	        	gmPartConversionForm.setCounterValue(String.valueOf(alPartDetails.size()));
	        }
	        alInvType = gmPartConversionBean.fetchInventoryTypeList();
        	log.debug(" alInvType values " + alInvType);
        	gmPartConversionForm.setAlInvType(alInvType);
        }
	       
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        
        return mapping.findForward("success");
    }


}
