package com.globus.operations.actions;

import java.io.IOException; 
import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.operations.beans.GmChangeLotBean;
import com.globus.operations.forms.GmChangeLotForm;

/**
 * @author vramanathan
 *
 */
public class GmChangeLotAction extends GmDispatchAction {

	/** 
	 * Code to Initialize the logger Class
	 */
Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/** changeLotNumber - to Load the Change Lot# screen
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return to JSP
	 * @throws Exception
	 */
	public ActionForward changeLotNumber(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		 // call the instantiate method
		    instantiate(request, response);
		    GmChangeLotForm gmChangeLotForm = (GmChangeLotForm)form;
		    gmChangeLotForm.loadSessionParameters(request);
		    GmChangeLotBean gmChangeLotBean =new GmChangeLotBean(getGmDataStoreVO());
			return actionMapping.findForward("gmchangelot");
	 }
	
	/** saveChangeLotNumber - to save the lot number for the transactions
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return forward to saveChangeLotNumber
	 * @throws Exception
	 */
	public ActionForward saveChangeLotNumber(ActionMapping mapping, ActionForm form, 
		      HttpServletRequest request, HttpServletResponse response) throws Exception {
		    instantiate(request, response);
		    GmChangeLotForm gmChangeLotForm = (GmChangeLotForm)form;
		    gmChangeLotForm.loadSessionParameters(request);
		    GmChangeLotBean gmChangeLotBean =new GmChangeLotBean(getGmDataStoreVO());
			HttpSession session = request.getSession(false);
			HashMap hmParam = new HashMap();
			String strSessCompanyLocale = "";
			String strChangeLotString ="";
			hmParam =  GmCommonClass.getHashMapFromForm(gmChangeLotForm);
			strChangeLotString = GmCommonClass.parseNull(gmChangeLotBean.saveChangeLotNumber(hmParam));  
	        response.setContentType("text/json");
		    PrintWriter pw = response.getWriter();
		    if (!strChangeLotString.equals("")) {
	             pw.write(strChangeLotString);
		     }
	             pw.flush();
		     return null;
	}

}
