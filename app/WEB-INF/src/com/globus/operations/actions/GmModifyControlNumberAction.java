/**
 * FileName : GmModifyControlNumberAction.java Description : Author : sthadeshwar Date : Apr 20,
 * 2009 Copyright : Globus Medical Inc
 */
package com.globus.operations.actions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.forms.GmModifyControlNumberForm;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.requests.beans.GmLoanerReqEditBean;

/**
 * @author sthadeshwar
 * 
 */
public class GmModifyControlNumberAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmModifyControlNumberForm gmModifyControlNumber = (GmModifyControlNumberForm) form;
    GmInHouseSetsReportBean gmInHouseSetsReportBean =
        new GmInHouseSetsReportBean(getGmDataStoreVO());
    GmOperationsBean gmOperationsBean = new GmOperationsBean();
    GmLoanerBean gmLoanerBean = new GmLoanerBean();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
    GmLoanerReqEditBean gmLoanerReqEditBean = new GmLoanerReqEditBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    String strStatus = "";
    String strType = "";
    String strURL = "";
    String strCboOpt = "";
    String strAction = "";
    String strServlet = "";
    String strContype = "";
    String strTBLNM = "";
    String strExcludeTrans = "";
    // gmModifyControlNumber.setAlSource(GmCommonClass.getCodeList("SOURCE"));

    String strRefId = GmCommonClass.parseNull(gmModifyControlNumber.getRefId());
    //Getting CN id from Back Order Close button to return to Process Transaction
    if (strRefId.equals("")) {
      strRefId = GmCommonClass.parseNull(request.getParameter("REFID"));
      gmModifyControlNumber.setRefId(strRefId);
    }
    log.debug("strRefId==>" + strRefId);
    if (!strRefId.trim().equals("")) {
      hmParam = gmCommonBean.fetchTransDetails(strRefId);
      log.debug("hmParam==>" + hmParam);
      strStatus = GmCommonClass.parseNull((String) hmParam.get("SFL"));
      strType = GmCommonClass.parseNull((String) hmParam.get("CTYPE"));
      strContype = GmCommonClass.parseNull((String) hmParam.get("CON_TYPE"));
      strTBLNM = GmCommonClass.parseNull((String) hmParam.get("TBLNM"));
      String strTxnId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
      String strRaCount = GmCommonClass.parseNull((String) hmParam.get("RACOUNT"));
      Date dtReprocessDate = (java.util.Date) hmParam.get("REPROCESSDATE");
      String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
      String strReprocessDate =
          GmCommonClass.getStringFromDate(dtReprocessDate, getGmDataStoreVO().getCmpdfmt());
      // To check whether we need to process a transaction or not
      strExcludeTrans =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strType, "EXCLUDETRANS"));

      // If the value is 'Y', no need to control/verify the transaction
      if (strExcludeTrans.equalsIgnoreCase("Y")) {
        throw new AppError(new SQLException("Transaction does not require to be processed", null,
            20635));
      }

      if (!strTxnId.equals("")) {
        strRefId = strTxnId;
      }
      if (strTBLNM.equals("ORDER")) {
        log.debug("its an order");
        strAction = "EditControl";
        strURL =
            "/gmItemControl.do?" + "txnid=" + strRefId
                + "&mode=CONTROL&haction=EditControl&hConsignId=" + strRefId
                + "&txntype=50261&loctype=93343";
      } else if (strType.equals("4127") || strTBLNM.equals("LOANER")) {
        // The following code added for Validating the Pending Request should not processed another
        // company/plant from Process Transaction screen.
        gmLoanerReqEditBean.validateProdRequestID(strRefId, strTBLNM);
        if (strStatus.equals("20") || strStatus.equals("22")|| strStatus.equals("24") ||strStatus.equals("21")) { // pending return //22 Missed 21 Disputed
          strAction = "EditLoad";
          strCboOpt = "RETURN";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hOpt=" + strType;
        } else if (strStatus.equals("25")) {// pending Check
          strAction = "EditLoad";
          strCboOpt = "PROCESSRETURN";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hOpt=" + strType + "&hScreenFrom=PROCESS_TXN_SCN";
        } else if (strStatus.equals("30") || strStatus.equals("40")) {
          strAction = "BOPROCESS";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hType=" + strType + "&hScreenFrom=PROCESS_TXN_SCN";
        } else if (strStatus.equals("50")) {// pending Process
          strAction = "REPROCESS";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hType=" + strType;;
        } else if (strStatus.equals("55")) {// pending Picture
          strAction = "PICTURE";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hType=" + strType;;
        } else {
          strAction = "ViewSet";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hType=" + strType;;
        }
      } else if (strType.equals("4119") || strTBLNM.equals("LOANER")) {
        if (strStatus.equals("20") || strStatus.equals("23") || strStatus.equals("22") || strStatus.equals("24")) { // 20
                                                                                          // pending
                                                                                          // return
                                                                                          // , 23
                                                                                          // Deployed,
                                                                                          // 22
                                                                                          // Missed
          strAction = "EditLoad";
          strCboOpt = "RETURN";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hOpt=" + strType;
        } else if (strStatus.equals("25")) {// pending Check
          strAction = "EditLoad";
          strCboOpt = "PROCESSRETURN";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hOpt=" + strType + "&hScreenFrom=PROCESS_TXN_SCN";
        } else if (strStatus.equals("30") || strStatus.equals("40")) {
          strAction = "BOPROCESS";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hType=" + strType + "&hScreenFrom=PROCESS_TXN_SCN";
        } else if (strStatus.equals("50")) {// pending Process
          strAction = "REPROCESS";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hType=" + strType;
        } else {
          strAction = "ViewSet";
          strServlet = "/GmInHouseSetServlet?";
          strURL =
              strServlet + "hAction=" + strAction + "&hConsignId=" + strRefId + "&Cbo_Action="
                  + strCboOpt + "&hType=" + strType;
        }
      } else {
        /*
         * if(strType.equals("4112")){ strURL =
         * "/gmOPEditShipDetails.do?refId="+strRefId+"&strOpt=modify&source=50181"; }else
         */
        if (strContype.equals("C") || strType.equals("100406")) {
          if (strStatus.equals("2")) {
            strURL =
                "/GmConsignItemServlet?" + "&hId=" + strRefId
                    + "&hMode=CONTROL&hFrom=ItemDashboard&hAction=EditControl&hConsignId="
                    + strRefId + "&hType=" + strType;
          } else if (strStatus.equals("3")) {
            strURL =
                "GmReprocessMaterialServlet?" + "&hId=" + strRefId
                    + "&hFrom=LoadItemShip&hAction=LoadItemShip&hConsignId=" + strRefId + "&hType="
                    + strType + "&hOpt=" + strType;
          }
          else if(strType.equals("40057")){
        	  strURL = "/GmConsignItemServlet?" + "&hId=" + strRefId
              + "&hMode=CONTROL&hFrom=ItemDashboard&hAction=EditControl&hConsignId="
              + strRefId + "&hType=" + strType; 
          }
        } else if (strContype.equals("I")) {
          if (strStatus.equals("2")) {
            strURL =
                "GmSetAddRemPartsServlet?" + "&hId=" + strRefId
                    + "&hMode=CONTROL&hFrom=ItemDashboard&hAction=EditControl&hConsignId="
                    + strRefId + "&hType=" + strType;
          } else {
            strURL =
                "GmSetAddRemPartsServlet?" + "&hId=" + strRefId
                    + "&hFrom=LoadItemShip&hAction=LoadItemShip&hConsignId=" + strRefId + "&hType="
                    + strType + "&hOpt=" + strType;
          }
        } else if (strContype.equals("T")) {
          if (strStatus.equals("2")) {
            strURL =
                "/gmItemControl.do?" + "&txnid=" + strRefId
                    + "&mode=CONTROL&haction=EditControl&hConsignId=" + strRefId + "&txntype="
                    + strType + "&loctype=93343";
          } else if (strType.equals("9110") || strType.equals("50154")) {// 9110-IHLN, There is no
                                                                         // direct verify process
                                                                         // for IHLN. The
                                                                         // verification process
                                                                         // will called when IHLN is
                                                                         // shipped out.
            // 50154-FGLE, No need to pull the record of FGLE for verification
            strStatus = "4";
          } else {
            if ((strType.equals("4110") || strType.equals("4112")) && strStatus.equals("3")) {
              throw new AppError("", "20693");
            } else if ((strType.equals("400085") || strType.equals("400086"))
                && strStatus.equals("3")) {
              throw new AppError("", "20948");
            } else {
              strURL =
                  "/gmItemVerify.do?" + "&txnid=" + strRefId
                      + "&mode=VERIFY&haction=EditVerify&hConsignId=" + strRefId + "&txntype="
                      + strType + "&loctype=93345";
            }
          }
        } else if (strContype.equals("R")) {// RAID from T506_returns table
          String strTxnSts =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strType, "RA-PROCESS"));// get rule
                                                                                         // value
                                                                                         // for sets

          if (strTxnSts.equals("SETS") && strStatus.equals("0") && strSetId.equals("")) { // Consignment
                                                                                          // Sets &
                                                                                          // ICT
            // Returns - Sets // Open Status
            strURL =
                "/GmReturnSummaryServlet?hAction=report&ProcessTransFl=YES&RaCount=" + strRaCount
                    + "&hRAId=" + strRefId;
          } else {
            if (strStatus.equals("0")) {// Open Status
              strURL = "/GmReturnReceiveServlet?hAction=Reload&hMode=Process&hRAId=" + strRefId;
            } else if (strStatus.equals("1")) {// Pending Credit
              strURL = "/GmReturnCreditServlet?hAction=LoadCredit&hMode=Process&hRAId=" + strRefId;
            } else if (strStatus.equals("2")) {// Pending Reprocess
              strURL =
                  "/GmReturnProcessServlet?hAction=Reload&ReprocessDate=" + strReprocessDate
                      + "&hId=" + strRefId;
            }
          }
        }
      }
      log.debug("strURL ==> " + strURL);
      strURL = GmFramework.appendCompanyInfo(strURL, request);
      gmModifyControlNumber.setUrl(strURL);
      gmModifyControlNumber.setRefId(gmModifyControlNumber.getRefId());
      if ((strStatus.equals("4")) && (!strType.equals("40057"))) {
    	
       throw new AppError("", "20694");
      }
      if (strType.equals("100062")) {
        throw new AppError("", "20695");
      }
    }
    return mapping.findForward("success");
  }
}
