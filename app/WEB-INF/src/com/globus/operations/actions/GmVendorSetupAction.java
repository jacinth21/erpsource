package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.forms.GmVendorSetupForm;
import com.globus.common.beans.GmAccessControlBean;

public class GmVendorSetupAction extends Action
{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        GmVendorSetupForm gmVendForm = (GmVendorSetupForm)form;
        GmVendorBean gmVendorBean = new GmVendorBean();
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        String strVendCatMapId = "";
        String strSetupAccessflag="";
        String strOpt = gmVendForm.getStrOpt();
        String strUserId = gmVendForm.getUserId();
        String strVendorId = gmVendForm.getVendorId();
        
        HashMap hmTemp = new HashMap();
        HashMap hmParam = new HashMap ();
        HashMap hmValues = new HashMap();
        HashMap hmVendListTemp = new HashMap();
        HashMap hmAccess = new HashMap();
        hmVendListTemp.put("CHECKACTIVEFL","N"); // check for active flag status in vendor list

        
        ArrayList alVendorList = new ArrayList();
        ArrayList alVendorType = new ArrayList();
        ArrayList alVendorCategory = new ArrayList();
        
        gmVendForm.loadSessionParameters(request);
        log.debug(" values inside gmcForm form " + gmVendForm.getUserId() + " values in vendor form --> " + gmVendForm.toString());
        log.debug(" stropt is " + strOpt);
        hmParam = GmCommonClass.getHashMapFromForm(gmVendForm);
        strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "VENDOR_SETUP_ACS"));  	
        strSetupAccessflag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
        gmVendForm.setStrAccessfl(strSetupAccessflag);
        if (strOpt.equals("save"))
        {
            hmParam = GmCommonClass.getHashMapFromForm(gmVendForm);
            hmParam.put("USERID",strUserId);
            log.debug(" values inside hmParam " + hmParam);
            gmVendorBean.saveVendorMapping (hmParam);
        }
        else if (strOpt.equals("edit"))
        {
            strVendCatMapId = gmVendForm.getHvendCatMapId();
            hmValues = gmVendorBean.fetchEditVendorMapInfo(strVendCatMapId);
            log.debug(" values fetched for edit " +hmValues);
            gmVendForm = (GmVendorSetupForm)GmCommonClass.getFormFromHashMap(gmVendForm,hmValues);
        }
        hmTemp = gmVendorBean.getVendorList(hmVendListTemp);
        
        alVendorList = (ArrayList)hmTemp.get("VENDORLIST");
        alVendorType = GmCommonClass.getCodeList("VEND");
        alVendorCategory = GmCommonClass.getCodeList("PFLY");
        
        gmVendForm.setAlVendorList(alVendorList);
        gmVendForm.setAlVendorType(alVendorType);
        gmVendForm.setAlVendorCategory(alVendorCategory);
        
        if (!strVendorId.equals("")){
            log.debug(" strvendoris ins " +strVendorId);
        RowSetDynaClass rdlist = gmVendorBean.fetchVendorMapInfo(strVendorId);
        request.setAttribute("RDVENDMAPLIST",rdlist);
        }
        
        return mapping.findForward("success");
    }

}
