package com.globus.operations.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmDonorLotRptBean;
import com.globus.operations.forms.GmDonorLotRptForm;

public class GmDonorLotRptAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadDonorLotInfo: For fetching the Lot Details based on the Donor Number o screen
   * 
   * @exception AppError
   * @throws IOException
   */
  public ActionForward loadDonorLotInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorLotRptForm gmDonorLotRptForm = (GmDonorLotRptForm) actionForm;
    gmDonorLotRptForm.loadSessionParameters(request);
    GmDonorLotRptBean gmDonorLotRptBean = new GmDonorLotRptBean(getGmDataStoreVO());
    String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
    strOpt = strOpt.length() == 0 ? GmCommonClass.parseNull(gmDonorLotRptForm.getStrOpt()) : strOpt;
    String strScreen = GmCommonClass.parseNull(request.getParameter("hscreen"));
    log.debug("strScreen==" + strScreen);
    request.setAttribute("hscreen", strScreen);
    String strXmlGridData = new String();
    String strISReport = "";
    int arraySize;
    HashMap hmParam = new HashMap();
    HashMap hmLotDetails = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alLotData = new ArrayList();
    gmDonorLotRptForm.setStrOpt(strOpt);
    if (strOpt.equals("fetchLots")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmDonorLotRptForm);
      hmLotDetails = gmDonorLotRptBean.fetchLotCodeProdInfo(hmParam);
      alLotData = GmCommonClass.parseNullArrayList((ArrayList) hmLotDetails.get("LOTDETAILS"));
      hmReturn.put("DONORLOTDATA", alLotData);
      hmReturn.put("hscreen", strScreen);
      strXmlGridData = generateOutPut(hmReturn);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strXmlGridData);
      pw.flush();
      return null;
    } else {
      return actionMapping.findForward("gmDonorLotReport");
    }
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alList = new ArrayList();

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    log.debug("strSessCompanyLocale==" + strSessCompanyLocale);

    alList = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("DONORLOTDATA"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alList", alList);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmDonorLotReport", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setTemplateName("GmDonorLotReport.vm");
    return templateUtil.generateOutput();
  }
}
