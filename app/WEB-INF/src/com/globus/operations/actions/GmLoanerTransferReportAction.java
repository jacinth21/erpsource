package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.forms.GmLoanerTransferForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmLoanerTransferReportAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    // TODO Auto-generated method stub
    instantiate(request, response);
    String strOpt = "";
    String strAccessFl = "";
    String strUserId = "";
    HashMap hmAccess = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alDistList = new ArrayList();
    ArrayList alSetList = new ArrayList();
    ArrayList alStatus = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();

    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    // Gettign company JS date format
    String strApplJSDateFmt =
        GmCommonClass.getCodeAltName(GmCommonClass.getCodeID(strApplnDateFmt, "DATFMT"));
    
    Logger log = GmLogger.getInstance(this.getClass().getName());

    GmLoanerTransferForm gmLoanerTransferForm = (GmLoanerTransferForm) form;
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmLoanerBean gmLoanerBean = new GmLoanerBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    gmLoanerTransferForm.loadSessionParameters(request);
    strUserId = gmLoanerTransferForm.getUserId();
    alDistList = gmCustomerBean.getDistributorList("Active");
    gmLoanerTransferForm.setAlFromDistributor(alDistList);
    gmLoanerTransferForm.setAlToDistributor(alDistList);
    alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOTRP"));
    gmLoanerTransferForm.setAlStatus(alStatus);

    // To populate values of the SetID drop down
    hmValues.put("TYPE", "1601");
    alSetList = gmProjectBean.loadSetMap(hmValues);
    gmLoanerTransferForm.setAlSetId(alSetList);

    strOpt = GmCommonClass.parseNull(gmLoanerTransferForm.getStrOpt());
    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLoanerTransferForm));
    log.debug("inside action " + strOpt);
    if (strOpt.equals("edit")) {
      gmLoanerBean.editLoanerTransferDate(hmParam);
      strOpt = "load";
    }
    if (strOpt.equals("load")) {
      // Getting the template headers from property files
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));
      String strRBPath = "properties.labels.operations.GmLoanerTransferReport";
      hmParam.put("COMPANYLOCALE", strSessCompanyLocale);
      hmParam.put("RBPATH", strRBPath);
      hmParam.put("SESSJSDATEFMT", strApplJSDateFmt);
      alResult = GmCommonClass.parseNullArrayList(gmLoanerBean.fetchLoanerTransferLog(hmParam));
      String strGridData = generateOutPut(alResult, hmParam);
      gmLoanerTransferForm.setGridXmlData(strGridData);
    }
    // PMT-712 adding security event to the report
    hmAccess = gmAccessControlBean.getAccessPermissions(strUserId, "LOANER_TRANS_RPT_ACC");
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if (!strAccessFl.equals("Y")) {
      gmLoanerTransferForm.setAccessFl("disabled");
    }
    return mapping.findForward("GmLoanerTransferReport");
  }

  /**
   * getXmlGridData - This method will be used to generate Output for grid data.
   * 
   * @return String - will be used by dhtmlx grid
   * @exception AppError
   */
  private String generateOutPut(ArrayList alParam, HashMap hmParams) throws Exception {
    String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParams.get("COMPANYLOCALE"));
    String strRBPath = GmCommonClass.parseNull((String) hmParams.get("RBPATH"));

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmParam", hmParams);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strRBPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName("GmLoanerTransferReport.vm");
    templateUtil.setTemplateSubDir("operations/templates");

    return templateUtil.generateOutput();
  }

}
