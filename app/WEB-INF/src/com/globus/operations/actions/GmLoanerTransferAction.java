package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.forms.GmLoanerTransferForm;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.sales.beans.GmSalesMappingBean;

public class GmLoanerTransferAction extends GmAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    // to
    // Initialize
    // the
    // Logger
    // Class.
    HttpSession session = request.getSession(false);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLogonBean gmLogon =  new GmLogonBean(getGmDataStoreVO());
    GmLoanerTransferForm gmLoanerTransferForm = (GmLoanerTransferForm) form;
    gmLoanerTransferForm.loadSessionParameters(request);
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    // Gettign company JS date format
    String strApplJSDateFmt =
        GmCommonClass.getCodeAltName(GmCommonClass.getCodeID(strApplnDateFmt, "DATFMT"));
    GmLoanerBean gmLoanerBean = new GmLoanerBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmInHouseSetsReportBean gmInHouseSetsReportBean =
        new GmInHouseSetsReportBean(getGmDataStoreVO());
    String strOpt = gmLoanerTransferForm.getStrOpt();
    String strAction = gmLoanerTransferForm.getHaction();
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    String strTodayDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
    log.debug("StrOpt:::::" + strOpt);

    ArrayList alDistList = new ArrayList();
    ArrayList alReqList = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alEmpList = new ArrayList();
    ArrayList alReps = new ArrayList();
    ArrayList alAccount = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();
    ArrayList alTrnsType = new ArrayList();
    ArrayList alLReqList = new ArrayList();
    ArrayList alReqDtl = new ArrayList();
    ArrayList alAssocReps = new ArrayList();
    String strDistId = "";
    String strFromtId = "";
    String strLoanerType = "";
    String strAccountId = "";
 // The following code added for Getting the Distributor, Sales Rep and Employee List based on the PLANT too if the Selected Company is EDC
    String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid()); 
    String strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "LOANERTRANSFERFILTER")) ;
    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale")); 
     HashMap hmFilters = new HashMap();
     hmFilters.put("FILTER", "Active");
     hmFilters.put("USERCODE", "300");
     hmFilters.put("DEPT", "");
     hmFilters.put("STATUS", "");    
     if(strCompanyPlant.equals("Plant")){
	 hmFilters.put("DISTFILTER", "COMPANY-PLANT");
	 hmFilters.put("REPFILTER", "COMPANY-PLANT");
	 hmFilters.put("EMPFILTER", "COMPANY-PLANT");
     } 

    // code added for loading from and to distributor drop down
    alDistList = GmCommonClass.parseNullArrayList(gmCustomerBean.getDistributorList(hmFilters));
    // to employee list
    alEmpList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmFilters));
    // to get sales reps and accounts
    alReps =  GmCommonClass.parseNullArrayList(gmCustomerBean.getRepList(hmFilters));
    gmLoanerTransferForm.setAlReps(alReps);
    alAssocReps = gmSalesMappingBean.getAssocRepList(strAccountId);
    gmLoanerTransferForm.setAlAssocReps(alAssocReps);

    alAccount = gmSales.reportAccount();
    gmLoanerTransferForm.setAlAccounts(alAccount);

    if (strAction.equals("INHOUSELOANER")) {
      gmLoanerTransferForm.setAlFromDistributor(alEmpList);
      gmLoanerTransferForm.setAlToDistributor(alEmpList);
      strLoanerType = "4119"; // In-House Loaner
    } else {
      gmLoanerTransferForm.setAlFromDistributor(alDistList);
      gmLoanerTransferForm.setAlToDistributor(alDistList);
      strLoanerType = "4127"; // Product Loaner
    }

    hmParam = GmCommonClass.getHashMapFromForm(gmLoanerTransferForm);
    hmParam.put("TYPE", strLoanerType);
    request.setAttribute("strType", strLoanerType);

    if (strOpt.equals("save")) {
      try {
        String strMessage = gmLoanerBean.saveLoanerTransfer(hmParam);
        // throw new AppError(strMessage, "", 'S');
      } catch (AppError appError) {
        request.setAttribute("hType", appError.getType());
        throw appError;
      } catch (Exception exp) {
        exp.printStackTrace();
        throw new AppError(exp);
      }
    }

    if (strOpt.equals("load")) {
      strDistId = GmCommonClass.parseNull((String) hmParam.get("TODISTRIBUTOR"));
      strFromtId = GmCommonClass.parseNull((String) hmParam.get("FROMDISTRIBUTOR"));

      if (strDistId != null) {
        alTrnsType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LREQTR"));
        alReqDtl = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LREQDL"));
        alReqList = gmLoanerBean.fetchOpenLoanerRequests(strDistId, strLoanerType);
        alLReqList = gmLoanerBean.fetchLoanerRequests(strDistId, strLoanerType);

        log.debug("alLReqList : " + alLReqList);
        hmParam.put("TYPE", strLoanerType);
        hmParam.put("STATUS", "20");
        hmParam.put("DISTIDS", strFromtId);
        if (strLoanerType.equals("4119")) { // 4119 Inhouse Loaners
          alResult = gmLoanerBean.fetchInHouseLoanerSetStatus(hmParam);
        } else {
          hmParam.put("STATUS", "20,21");//20 - Pending Return and Disputed
          alResult = gmInHouseSetsReportBean.getInHouseSetStatus(hmParam);
        }

        request.setAttribute("alResult", alResult);
        request.setAttribute("alReqList", alReqList);
        request.setAttribute("alLNReqList", alLReqList);
        request.setAttribute("alReqDtl", alReqDtl);
        request.setAttribute("alReps", alReps);
        request.setAttribute("alAccount", alAccount);
        request.setAttribute("alAssocReps", alAssocReps);
        hmValues.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
        hmValues.put("LRESULT", alResult);
        hmValues.put("LREQLIST", alReqList);
        hmValues.put("LTRNSTYP", alTrnsType);
        hmValues.put("TYPE", strLoanerType);
        hmValues.put("TEMPLATE", "GmLoanerTransfer.vm");
        hmValues.put("TEMPLATEPATH", "/operations/templates");
        hmValues.put("CURRDATE", strTodayDate);
        hmValues.put("SESSDATEFMT", strApplnDateFmt);
        hmValues.put("SESSJSDATEFMT", strApplJSDateFmt);
        String XmlGridData = getXmlGridData(hmValues);
        
        gmLoanerTransferForm.setGridXmlData(XmlGridData);
      }
    }
    return mapping.findForward("GmLoanerTransfer");
  }

  private String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alResult = new ArrayList();
    ArrayList alReqList = new ArrayList();
    ArrayList alTrnsType = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strTemplate = (String) hmParam.get("TEMPLATE");
    String strSessCompanyLocale = (String) hmParam.get("STRSESSCOMPANYLOCALE");
    String strTemplatePath = (String) hmParam.get("TEMPLATEPATH");
    alResult = (ArrayList) hmParam.get("LRESULT");
    alReqList = (ArrayList) hmParam.get("LREQLIST");
    alTrnsType = (ArrayList) hmParam.get("LTRNSTYP");
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataList("alReqList", alReqList);
    templateUtil.setDropDownMaster("alTrnsType", alTrnsType);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.operations.GmLoanerTransfer",
            strSessCompanyLocale));
    return templateUtil.generateOutput();
  }
}
