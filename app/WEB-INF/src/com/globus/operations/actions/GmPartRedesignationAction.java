/**
 * 
 */
package com.globus.operations.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmPartRedesignationBean;
import com.globus.operations.forms.GmPartRedesignationForm;

/**
 * @author arajan
 * 
 */
public class GmPartRedesignationAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchPartRedesignation: For fetching the details to screen
   * 
   * @exception AppError
   */
  public ActionForward fetchPartRedesignation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    GmPartRedesignationForm gmPartRedesignationForm = (GmPartRedesignationForm) actionForm;
    gmPartRedesignationForm.loadSessionParameters(request);
    instantiate(request, response);
    GmPartRedesignationBean gmPartRedesignationBean =
        new GmPartRedesignationBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
    strOpt =
        strOpt.length() == 0 ? GmCommonClass.parseNull(gmPartRedesignationForm.getStrOpt())
            : strOpt;
    String strXmlGridData = new String();
    String strPartNum = new String();
    String strTransId = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    HashMap hmParam = new HashMap();
    HashMap hmPartReDesignDtls = new HashMap();
    // ArrayList alPartsList = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alCartDetails = new ArrayList();
    ArrayList alFrmPart = new ArrayList();
    HashMap hmFromPart = null;
    HashMap hmCartData = null;

    if (strOpt.equals("Load")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmPartRedesignationForm);
      hmPartReDesignDtls = gmPartRedesignationBean.getPartRedesignationDtls(hmParam); // 18753
      // alPartsList = GmCommonClass.parseNullArrayList((ArrayList)
      // hmPartReDesignDtls.get("PARTLIST"));
      alCartDetails =
          GmCommonClass.parseNullArrayList((ArrayList) hmPartReDesignDtls.get("CTRLDTLS"));
      strPartNum = GmCommonClass.parseNull(gmPartRedesignationForm.getPartId());
      // gmPartRedesignationForm.setAlPartsList(alPartsList);
      hmPartReDesignDtls.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      strXmlGridData = generateOutPut(hmPartReDesignDtls);
      gmPartRedesignationForm.setGridXmlData(strXmlGridData);
      request.setAttribute("CARTDATA", alCartDetails);
      strTransId = GmCommonClass.parseNull(gmPartRedesignationForm.getStrTxnIds());
      alLogReasons = gmCommonBean.getLog(strTransId, "103931");
      gmPartRedesignationForm.setAlLogReasons(alLogReasons);
    }
    return actionMapping.findForward("InitiatePartRedesignation");
  }

  /**
   * savePartRedesignation: For saving the details of screen
   * 
   * @exception AppError
   */
  public ActionForward savePartRedesignation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    GmPartRedesignationForm gmPartRedesignationForm = (GmPartRedesignationForm) actionForm;
    gmPartRedesignationForm.loadSessionParameters(request);
    instantiate(request, response);
    GmPartRedesignationBean gmPartRedesignationBean = new GmPartRedesignationBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    String strTransId = "";
    String strMessage = "";
    String strUserId = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmPartRedesignationForm);
    strTransId = gmPartRedesignationBean.savePartRedesignationDtls(hmParam);
    if (!strTransId.equals("")) {
      gmPartRedesignationForm.setStrTxnIds(strTransId);
      gmPartRedesignationBean.saveDonorNumPTRD(strTransId,strUserId);
      
    }
    String strPartNum = GmCommonClass.parseNull(gmPartRedesignationForm.getPartId());
    String strDonorNum = GmCommonClass.parseNull(gmPartRedesignationForm.getDonorId());
    String strPartNumStr = GmCommonClass.parseNull(gmPartRedesignationForm.gethScannedPartStr());
    String strLotNumStr = GmCommonClass.parseNull(gmPartRedesignationForm.gethScannedLotStr());
    String strDonorNumStr = GmCommonClass.parseNull(gmPartRedesignationForm.gethScannedDonorStr());
    alLogReasons = gmCommonBean.getLog(strTransId, "103931");
    gmPartRedesignationForm.setAlLogReasons(alLogReasons);

    // redirect to fetch method to fetch the saved details to screen
    String strRedirectURL =
        "gmPartredesignation.do?method=fetchPartRedesignation&strOpt=Load&partId=" + strPartNum
            + "&donorId=" + strDonorNum + "&strTxnIds=" + strTransId + "&hScannedDonorStr="
            + strDonorNumStr + "&hScannedPartStr=" + strPartNumStr + "&hScannedLotStr="
            + strLotNumStr;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * generatePicSlip: Method to Print the Jasper PIC Slip for PTRD Transaction
   * 
   * @exception AppError
   */
  public ActionForward generatePicSlip(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    GmPartRedesignationForm gmPartRedesignationForm = (GmPartRedesignationForm) actionForm;
    gmPartRedesignationForm.loadSessionParameters(request);
    instantiate(request, response);
    GmPartRedesignationBean gmPartRedesignationBean =
        new GmPartRedesignationBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strUserId = GmCommonClass.parseNull(gmPartRedesignationForm.getUserId());
    String strUserName = GmCommonClass.parseNull(gmCommonBean.getUserName(strUserId));
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmHeaderdata = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartRedesignationForm);
    String strTransID = GmCommonClass.parseNull(gmPartRedesignationForm.getStrTxnIds());
    hmParam.put("TRANSID", strTransID);
    hmResult = GmCommonClass.parseNullHashMap(gmPartRedesignationBean.getTransDetails(hmParam));
    hmHeaderdata = GmCommonClass.parseNullHashMap((HashMap) hmResult.get("HEADERDATA"));
    hmHeaderdata.put("LOGINNAME", strUserName);
    request.setAttribute("HEADERDATA", hmHeaderdata);
    request.setAttribute("CARTDETAILS", hmResult.get("CARTDETAILS"));
    return actionMapping.findForward("gmPartRedegPicSlip");
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmPartRedesignation", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPartRedesignation.vm");
    return templateUtil.generateOutput();
  }

  /**
   * validateToPart: Method to validate 'To Part' number
   * 
   * @exception AppError
   */
  public ActionForward validateToPart(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartRedesignationBean gmPartRedesignationBean =
        new GmPartRedesignationBean(getGmDataStoreVO());
    String strFromPart = GmCommonClass.parseNull((String) request.getParameter("fromPart"));
    String strToPart = GmCommonClass.parseNull((String) request.getParameter("toPart"));
    String strValidFl = gmPartRedesignationBean.validateToPart(strFromPart, strToPart);
    response.setContentType("text/json");
    PrintWriter pw = response.getWriter();
    pw.write(strValidFl);
    pw.flush();
    return null;

  }
}
