/**
 * 
 */
package com.globus.operations.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmPartLabelingBean;
import com.globus.operations.forms.GmPartLabelingForm;
import com.globus.operations.itemcontrol.beans.GmItemControlRptBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;

/**
 * @author arajan
 * 
 */
public class GmPartLabelingAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchPartLabelDetail: For fetching the header and details to the Part Label screen
   * 
   * @exception AppError
   */
  public ActionForward fetchPartLabelDetail(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartLabelingForm gmPartLabelingForm = (GmPartLabelingForm) actionForm;
    gmPartLabelingForm.loadSessionParameters(request);
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmGridDtls = new HashMap();
    HashMap hmPlAccess = new HashMap();
    HashMap hmQCAccess = new HashMap();
    HashMap hmPSAccess = new HashMap();
    HashMap hmStageAccess = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alTemp = new ArrayList();
    ArrayList alApprRej = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    String strTransId = "";
    String strXmlGridData = "";
    String strAccess = "";
    String strStatuflg = "";
    HashMap hmTmp = new HashMap();
    String strPartyId = gmPartLabelingForm.getSessPartyId();
    hmPlAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "LBL_PRINTED_CHECK")); // Event id(LBL_PRINTED_CHECK) for Print Label
    hmQCAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "Label_QC")); // Event id(Label_QC) for QC label
    //PMT-33719- To get the access for Pre Staging and Staging
   hmPSAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                "PRE_STAGING_ACCESS")); // Event id(Pre_Staging_Access) - Pre Staging label
    hmStageAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                "STAGING_ACCESS")); // Event id(Staging_Access) -Staging label
    String strPlAccess = GmCommonClass.parseNull((String) hmPlAccess.get("UPDFL"));
    String strQCAccess = GmCommonClass.parseNull((String) hmQCAccess.get("UPDFL"));
    String strPStagingAccess = GmCommonClass.parseNull((String) hmPSAccess.get("UPDFL"));
    String strStagingAccess = GmCommonClass.parseNull((String) hmStageAccess.get("UPDFL"));
    gmPartLabelingForm.setAccessFlg(strPlAccess);
    gmPartLabelingForm.setQCAccessFlg(strQCAccess);
    gmPartLabelingForm.setPreStagingFlag(strPStagingAccess);
    gmPartLabelingForm.setStagingFlag(strStagingAccess);
    strStatuflg = GmCommonClass.parseNull(request.getParameter("statuflg"));
    strStatuflg =
        strStatuflg.equals("") ? GmCommonClass.parseNull((String) request.getAttribute("statuflg"))
            : strStatuflg;
    // To get the access for Pending Inspection screen
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_I"));
    strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

    // To get the access for Pending Processing Button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_P"));
    gmPartLabelingForm.setStrProcessAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

    // To get the access for Pending Verification Button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_V"));
    gmPartLabelingForm.setStrVerifyAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

    String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    strAction =
        strAction.equals("") ? GmCommonClass.parseNull((String) request.getAttribute("hAction"))
            : strAction;
    if (!strAccess.equals("Y") && strAction.equals("PI")) {
      throw new AppError("", "20685");
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmPartLabelingForm);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    strTransId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    hmReturn = GmCommonClass.parseNullHashMap(gmItemControlRptBean.fetchItenControlMain(hmParam));
    hmTemp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("hmHeaderInfo"));
    hmTmp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HMLABLEFLAGS"));
    gmPartLabelingForm =
        (GmPartLabelingForm) GmCommonClass.getFormFromHashMap(gmPartLabelingForm, hmTemp);
    gmPartLabelingForm =
        (GmPartLabelingForm) GmCommonClass.getFormFromHashMap(gmPartLabelingForm, hmTmp);
    alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alDetailInfo"));
    gmPartLabelingForm.setHaction(strAction);
    gmPartLabelingForm.setLabelprestage( GmCommonClass.parseNull((String) hmTmp.get("LBL_PRE_STAGE")));
    gmPartLabelingForm.setLabelstage(GmCommonClass.parseNull((String) hmTmp.get("LBL_STAGE")));
    HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
    gmPartLabelingForm.setHmTransRules(hmTransRules);
    alApprRej = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DHRAR"));
    // 103931: Part Re-Desig Log
    alLogReasons = gmCommonBean.getLog(strTransId, "103931");
    gmPartLabelingForm.setAlLogReasons(alLogReasons);
    hmGridDtls.put("STATUSFLG", strStatuflg);
    hmGridDtls.put("ALAPPRREJ", alApprRej);
    hmGridDtls.put("ALRESULT", alTemp);
    hmGridDtls.put("DHRSTATUS", GmCommonClass.parseNull((String) hmTemp.get("STATUSFL")));
    hmGridDtls.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    strXmlGridData = generateOutPut(hmGridDtls);
    gmPartLabelingForm.setGridXmlData(strXmlGridData);

    return actionMapping.findForward("GmPartLabeling");
  }

  /**
   * savePartApprRejDetails: To save/reject the part numbers in PTRD transaction
   * 
   * @exception AppError
   */
  public ActionForward savePartApprRejDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartLabelingForm gmPartLabelingForm = (GmPartLabelingForm) actionForm;
    gmPartLabelingForm.loadSessionParameters(request);
    GmPartLabelingBean gmPartLableingBean = new GmPartLabelingBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmPartLabelingForm);
    String strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    // Get the corresponding method from the bean based on the approve/reject parts
    Method method = gmPartLableingBean.getClass().getMethod(strAction, HashMap.class);
    String strPNQNTxnId = (String) method.invoke(gmPartLableingBean, hmParam);

    if (strPNQNTxnId.indexOf('|') != -1 && !strPNQNTxnId.equals("")) {
      strPNQNTxnId = strPNQNTxnId.substring(0, strPNQNTxnId.length() - 2);
      throw new AppError("The PTRD Transaction is voided and new transaction created "
          + strPNQNTxnId, "", 'S');
    }
    gmPartLabelingForm.setPnqnTxnId(strPNQNTxnId);
    gmPartLabelingForm.setAction("1");
    return fetchPartLabelDetail(actionMapping, actionForm, request, response);
  }

  /**
   * saveComments():This method is used to save the comments in Pending Process status
   * 
   * @exception AppError
   */
  public ActionForward saveComments(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmPartLabelingForm gmPartLabelingForm = (GmPartLabelingForm) actionForm;
    gmPartLabelingForm.loadSessionParameters(request);
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmPartLabelingForm);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strComments = GmCommonClass.parseNull(gmPartLabelingForm.getTxt_LogReason());
    String strUserId = GmCommonClass.parseNull(gmPartLabelingForm.getUserId());
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(strTxnId, strComments, strUserId, "103931"); // 103931-->Part Re-Desig
                                                                        // Log
    }
    request.setAttribute("statuflg", "2");
    request.setAttribute("hAction", "PP");
    return fetchPartLabelDetail(actionMapping, actionForm, request, response);

  }

  /**
   * saveChkBox() this method is used to save the attribute value of Part label and QC verification
   * 
   * @exception AppError
   */
  public ActionForward saveChkBox(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartLabelingForm gmPartLabelingForm = (GmPartLabelingForm) actionForm;
    gmPartLabelingForm.loadSessionParameters(request);
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();

    String strPartyId = gmPartLabelingForm.getSessPartyId();
    String strTxnId = GmCommonClass.parseNull(request.getParameter("Txt_txn_id"));
    String strTxnAttValuew = GmCommonClass.parseNull(request.getParameter("Txt_AttribValue"));
    String strAttType = GmCommonClass.parseNull(request.getParameter("Txt_AttribType"));
    hmParam.put("REFID", strTxnId);
    hmParam.put("ATTRIBVALUE", strTxnAttValuew);
    hmParam.put("ATTRIBTYPE", strAttType);
    hmParam.put("USERID", strPartyId);
    GmPartLabelingBean gmPartLabelingBean = new GmPartLabelingBean();
    String strResult = gmPartLabelingBean.savePartLabelParameter(hmParam);
    response.setContentType("text/json");
    PrintWriter pw = response.getWriter();
    pw.write(strResult);
    pw.flush();
    pw.close();
    return actionMapping.findForward("GmPartLabeling");
  }

  /**
   * savPrintlabelProcess() this method is used to save the part details
   * 
   * @exception AppError
   */

  public ActionForward savPrintlabelProcess(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartLabelingForm gmPartLabelingForm = (GmPartLabelingForm) actionForm;
    gmPartLabelingForm.loadSessionParameters(request);
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmGridDtls = new HashMap();
    HashMap hmPlAccess = new HashMap();
    HashMap hmQCAccess = new HashMap();
    HashMap hmPSAccess = new HashMap();
    HashMap hmStageAccess = new HashMap();
    HashMap hmTmp = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alTemp = new ArrayList();
    ArrayList alApprRej = new ArrayList();
    String strXmlGridData = "";
    String strPartyId = GmCommonClass.parseNull((String) gmPartLabelingForm.getSessPartyId());
    String strTxnId = GmCommonClass.parseNull(request.getParameter("Txt_txn_id"));
    String strStatuflg = GmCommonClass.parseNull(request.getParameter("statuflg"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmPlAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "LBL_PRINTED_CHECK"));// Event id(LBL_PRINTED_CHECK) for Print Label
    hmQCAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "Label_QC"));// Event id(Label_QC) for QC Label
    hmPSAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PRE_STAGING_ACCESS")); // Event id(Pre_Staging_Access) - Pre Staging label
    hmStageAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "STAGING_ACCESS")); // Event id(Staging_Access) -Staging label
    String strPlAccess = GmCommonClass.parseNull((String) hmPlAccess.get("UPDFL"));
    String strQCAccess = GmCommonClass.parseNull((String) hmQCAccess.get("UPDFL"));
    String strPStagingAccess = GmCommonClass.parseNull((String) hmQCAccess.get("UPDFL"));
    String strStagingAccess = GmCommonClass.parseNull((String) hmQCAccess.get("UPDFL"));
    gmPartLabelingForm.setAccessFlg(strPlAccess);
    gmPartLabelingForm.setQCAccessFlg(strQCAccess);
    gmPartLabelingForm.setPreStagingFlag(strPStagingAccess);
    gmPartLabelingForm.setStagingFlag(strStagingAccess);
    hmParam.put("REFID", strTxnId);
    hmParam.put("USERID", strPartyId);
    hmParam.put("TXT_LOGREASON", GmCommonClass.parseNull(gmPartLabelingForm.getTxt_LogReason()));
    GmPartLabelingBean gmPartLabelingBean = new GmPartLabelingBean();
    gmPartLabelingBean.savePrintlabelProcess(hmParam);
    String strAction = "PP";
    hmParam = GmCommonClass.getHashMapFromForm(gmPartLabelingForm);
    hmReturn = GmCommonClass.parseNullHashMap(gmItemControlRptBean.fetchItenControlMain(hmParam));
    hmTemp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("hmHeaderInfo"));
    hmTmp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HMLABLEFLAGS"));
    // To get the access for Pending Inspection screen
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_I"));
    String strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

    // To get the access for Pending Processing Button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_P"));
    gmPartLabelingForm.setStrProcessAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

    // To get the access for Pending Verification Button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_V"));
    gmPartLabelingForm.setStrVerifyAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

    gmPartLabelingForm =
        (GmPartLabelingForm) GmCommonClass.getFormFromHashMap(gmPartLabelingForm, hmTemp);
    gmPartLabelingForm =
        (GmPartLabelingForm) GmCommonClass.getFormFromHashMap(gmPartLabelingForm, hmTmp);
    alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alDetailInfo"));
    gmPartLabelingForm.setLabelprestage( GmCommonClass.parseNull((String) hmTmp.get("LBL_PRE_STAGE")));
    gmPartLabelingForm.setLabelstage(GmCommonClass.parseNull((String) hmTmp.get("LBL_STAGE")));
    gmPartLabelingForm.setAldetailinfo(alTemp);
    gmPartLabelingForm.setHaction(strAction);
    HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
    gmPartLabelingForm.setHmTransRules(hmTransRules);
    // 103931: Part Re-Desig Log
    alLogReasons = gmCommonBean.getLog(strTxnId, "103931");
    gmPartLabelingForm.setAlLogReasons(alLogReasons);
    alApprRej = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DHRAR"));
    hmGridDtls.put("STATUSFLG", strStatuflg);
    hmGridDtls.put("ALAPPRREJ", alApprRej);
    hmGridDtls.put("ALRESULT", alTemp);
    hmGridDtls.put("DHRSTATUS", GmCommonClass.parseNull((String) hmParam.get("ACTION")));
    hmGridDtls.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    strXmlGridData = generateOutPut(hmGridDtls);
    gmPartLabelingForm.setGridXmlData(strXmlGridData);
    gmPartLabelingForm.setPreStagingFlag(strPStagingAccess);
    gmPartLabelingForm.setStagingFlag(strStagingAccess);
    hmParam = GmCommonClass.getHashMapFromForm(gmPartLabelingForm);
    return actionMapping.findForward("GmPartLabeling");
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String generateOutPut(HashMap hmParam) throws AppError {

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alResult",
        GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT")));
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmPartLabel", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setTemplateName("GmPartLabeling.vm");
    return templateUtil.generateOutput();
  }

  /**
   * savPrintlabelProcess() this method is used to save the part details
   * 
   * @exception AppError
   */
  public ActionForward verifyPrintLableProcess(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartLabelingForm gmPartLabelingForm = (GmPartLabelingForm) actionForm;
    gmPartLabelingForm.loadSessionParameters(request);
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    HashMap hmGridDtls = new HashMap();
    ArrayList alApprRej = new ArrayList();
    String strXmlGridData = "";
    HashMap hmParam = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmPlAccess = new HashMap();
    HashMap hmQCAccess = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alTemp = new ArrayList();
    HashMap hmTmp = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    String strPartyId = gmPartLabelingForm.getSessPartyId();
    String strTxnId = GmCommonClass.parseNull(request.getParameter("Txt_txn_id"));
    String strTxnTypes = GmCommonClass.parseNull(request.getParameter("txntypenm"));
    String strInput = GmCommonClass.parseNull(request.getParameter("strInput"));
    String strStatuflg = GmCommonClass.parseNull(request.getParameter("statusflg"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmPlAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "LBL_PRINTED_CHECK"));
    hmQCAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "Label_QC"));
    String strPlAccess = GmCommonClass.parseNull((String) hmPlAccess.get("UPDFL"));
    String strQCAccess = GmCommonClass.parseNull((String) hmQCAccess.get("UPDFL"));
    gmPartLabelingForm.setAccessFlg(strPlAccess);
    gmPartLabelingForm.setQCAccessFlg(strQCAccess);
    String strComments = GmCommonClass.parseNull(gmPartLabelingForm.getTxt_LogReason());
    String strUserId = GmCommonClass.parseNull(gmPartLabelingForm.getUserId());
    hmParam.put("REFID", strTxnId);
    hmParam.put("USERID", strPartyId);
    String strAction = "PV";
    hmParams.put("TXNID", strTxnId);
    hmParams.put("TXNTYPE", "103932");
    hmParams.put("RELVERFL", "");
    hmParams.put("INPUTSTRING", strInput);
    hmParams.put("USERID", strPartyId);
    hmParams.put("LOCTYPE", "93345");
    gmItemControlTxnBean.saveItemVerifyMain(hmParams);
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(strTxnId, strComments, strUserId, "103931"); // 103931-->Part Re-Desig
                                                                        // Log
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmPartLabelingForm);
    hmReturn = GmCommonClass.parseNullHashMap(gmItemControlRptBean.fetchItenControlMain(hmParam));
    hmTemp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("hmHeaderInfo"));
    hmTmp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HMLABLEFLAGS"));
    // To get the access for Pending Inspection screen
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_I"));
    String strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

    // To get the access for Pending Processing Button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_P"));
    gmPartLabelingForm.setStrProcessAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

    // To get the access for Pending Verification Button
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PTRD_V"));
    gmPartLabelingForm.setStrVerifyAccess(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

    gmPartLabelingForm =
        (GmPartLabelingForm) GmCommonClass.getFormFromHashMap(gmPartLabelingForm, hmTemp);
    gmPartLabelingForm =
        (GmPartLabelingForm) GmCommonClass.getFormFromHashMap(gmPartLabelingForm, hmTmp);
    alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alDetailInfo"));
    gmPartLabelingForm.setAldetailinfo(alTemp);
    gmPartLabelingForm.setHaction(strAction);
    HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
    gmPartLabelingForm.setHmTransRules(hmTransRules);
    // 103931: Part Re-Desig Log
    alLogReasons = gmCommonBean.getLog(strTxnId, "103931");
    gmPartLabelingForm.setAlLogReasons(alLogReasons);
    alApprRej = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DHRAR"));
    hmGridDtls.put("STATUSFLG", hmTmp.get("STATUS_FL"));
    hmGridDtls.put("ALAPPRREJ", alApprRej);
    hmGridDtls.put("ALRESULT", alTemp);
    hmGridDtls.put("DHRSTATUS", GmCommonClass.parseNull((String) hmParam.get("ACTION")));
    hmGridDtls.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    strXmlGridData = generateOutPut(hmGridDtls);
    gmPartLabelingForm.setGridXmlData(strXmlGridData);
    return actionMapping.findForward("GmPartLabeling");
  }
}
