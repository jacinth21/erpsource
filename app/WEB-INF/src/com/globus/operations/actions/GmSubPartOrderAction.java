//Source file: C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmLockTagAction.java

package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.forms.GmSubPartOrderForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmSubPartOrderAction extends GmAction {
	// private String strOpt = LOAD / RELOAD / SAVE;

	/**
	 * @return org.apache.struts.action.ActionForward
	 * @roseuid 48D1292D0247
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {

			GmSubPartOrderForm gmSubPartOrderForm = (GmSubPartOrderForm) form;
			gmSubPartOrderForm.loadSessionParameters(request);
			 
			GmProjectBean gmProj = new GmProjectBean();
			GmPurchaseBean gmPurchaseBean = new GmPurchaseBean();
			Logger log = GmLogger.getInstance(this.getClass().getName());

			String strOpt = gmSubPartOrderForm.getStrOpt();
			RowSetDynaClass rdSubResult = null;
			 
			HashMap hmParam = new HashMap();
			HashMap hmReturn = new HashMap();
			 
			ArrayList alProject = new ArrayList();
 
			log.debug(" Testing. ..... ");
			hmParam = GmCommonClass.getHashMapFromForm(gmSubPartOrderForm); 

			if (strOpt.equals("save")) {
				gmPurchaseBean.saveSubpartOrder(hmParam);
				}
			if (!strOpt.equals("")) {
					 
				hmReturn = gmPurchaseBean.loadSubpart(hmParam);
				rdSubResult = (RowSetDynaClass) hmReturn.get("SUBPARTORDER");
				log.debug(" hmReturn. ..... " + hmReturn);
				log.debug(" rdSubResult.getRows(). ..... "+ rdSubResult.getRows().size());
				gmSubPartOrderForm.setReturnList(rdSubResult.getRows());	 
			} 
			HashMap hmProj = new HashMap();
			hmProj.put("COLUMN", "ID");
			hmProj.put("STATUSID", "20301"); // Filter all approved projects
			alProject = gmProj.reportProject(hmProj); 
			gmSubPartOrderForm.setProjectList(alProject);
			 

		} catch (Exception exp) {
			exp.printStackTrace();
			throw new AppError(exp);
		}

		return mapping.findForward("success");
	}

}
