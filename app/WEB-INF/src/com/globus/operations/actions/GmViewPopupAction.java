/**
 * 
 */
package com.globus.operations.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.operations.beans.GmLotCodeRptBean;
import com.globus.operations.forms.GmLotCodeRptForm;

public class GmViewPopupAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * dhrViewPopup: To popup the DHR view	
	 * @exception 	AppError
	 */
	public ActionForward dhrViewPopup(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
		HashMap hmResult = new HashMap();
		String strDHRId = GmCommonClass.parseNull((String)request.getParameter("hDHRId"));
		String strRedirectURL = "";
		String strVendorId = "";
		String strWorkOrderId = "";
		
		hmResult = gmLotCodeRptBean.fetchDHRDetails(strDHRId);
		strVendorId = GmCommonClass.parseNull((String)hmResult.get("VENDORID"));
		strWorkOrderId = GmCommonClass.parseNull((String)hmResult.get("WOID"));
		
		//Redirect to the servlet of DHR view
		strRedirectURL = "/GmPOReceiveServlet?hAction=ViewDHRPrint&hDHRId="+strDHRId+"&hVenId="+strVendorId+"&hWOId="+strWorkOrderId;
		return actionRedirect(strRedirectURL, request);
	}	
}
