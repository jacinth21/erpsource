/**
 * Description : This file is used for performing the fetch and save related actions of Donor
 * Details
 * 
 * @author arajan
 */
package com.globus.operations.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmDonorInfoBean;
import com.globus.operations.forms.GmDonorInfoForm;

public class GmDonorInfoAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * fetchDonorInfo: This method is used to fetch the donor details to the screen
   * 
   * @exception AppError
   */
  public ActionForward fetchDonorInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoForm gmDonorInfoForm = (GmDonorInfoForm) actionForm;
    gmDonorInfoForm.loadSessionParameters(request);
    GmDonorInfoBean gmDonorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());

    ArrayList alGender = new ArrayList();
    ArrayList alInternationalUse = new ArrayList();
    ArrayList alCtrlDtls = new ArrayList();
    ArrayList alDonorDtls = new ArrayList();
    ArrayList alResearch = new ArrayList();
    ArrayList alApprRej = new ArrayList();
    HashMap hmResult = new HashMap();
    HashMap hmDonorDtls = new HashMap();
    HashMap hmCtrlsDtls = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmParamv = new HashMap();
    String strRsBulkIdCnt = "";
    String strBtnDisable = "false";
    String strMsg = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorInfoForm);
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("HMODE"));
    String strScreenType = GmCommonClass.parseNull((String) hmParam.get("SCREENTYPE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    String strNcmrStatus = GmCommonClass.parseNull((String) hmParam.get("NCMRSTATUS"));
    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));
    strMsg = GmCommonClass.parseNull((String) hmParam.get("STRMSG"));

    // Getting the dropdown values for Sex and International Use
    alGender = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DONSEX"));
    alInternationalUse = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ITLUSE"));
    alResearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DONRES"));
    gmDonorInfoForm.setAlGender(alGender);
    gmDonorInfoForm.setAlInternationalUse(alInternationalUse);
    gmDonorInfoForm.setAlResearch(alResearch);

    // Fetch the donor details for the sections "Donor Details" and "Donor Parameters"
    hmResult = gmDonorInfoBean.fetchDonorInfo(strDHRId);
    alDonorDtls = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("DONORDTLS"));
    alCtrlDtls = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("CTRLDTLS"));

    /*
     * strRsBulkIdCnt = gmDonorInfoBean.getRSBulkIdCnt(strDHRId);
     * gmDonorInfoForm.setStrRsBulkIdCnt(strRsBulkIdCnt);
     */

    if (strScreenType.equals("NcmrUpdate")) { // If the dhr is a part of bulk shipment then need to
                                              // display the screen having status and control number
      alApprRej = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DHRAR")); // approve
                                                                                        // or reject
      alCtrlDtls = GmCommonClass.parseNullArrayList(gmDonorInfoBean.fetchLotNumStatus(strDHRId));
      strBtnDisable = (strNcmrStatus.equals("0") || strNcmrStatus.equals("1")) ? "false" : "true";
      gmDonorInfoForm.setNcmrStatus(strNcmrStatus);

      hmParamv.put("APPRREJ", alApprRej);
      hmParamv.put("TEMPLATE", "GmSavedLotStatus.vm");
      hmParamv.put("VMFILEPATH", "properties.labels.operations.inventory.GmSavedLotStatus");
    } else {
      hmParamv.put("VMFILEPATH", "properties.labels.operations.inventory.GmSavedControlNum");
      hmParamv.put("TEMPLATE", "GmSavedControlNum.vm");
    }

    if (alDonorDtls.size() > 0)
      hmDonorDtls = (HashMap) alDonorDtls.get(0);

    GmCommonClass.getFormFromHashMap(gmDonorInfoForm, hmDonorDtls);
    gmDonorInfoForm.sethDhrId(strDHRId);
    gmDonorInfoForm.sethMode(strMode);
    gmDonorInfoForm.setAlCtrlDtls(alCtrlDtls);

    // Set the array list of control number details and vm file detials to a hashmap to generate the
    // grid after saving the informations
    hmParamv.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParamv.put("CLIST", alCtrlDtls);
    hmParamv.put("TEMPLATEPATH", "operations/inventory/templates");

    String strGridXmlData = gmDonorInfoBean.getXmlGridData(hmParamv);
    gmDonorInfoForm.setStrGridData(strGridXmlData);
    gmDonorInfoForm.setScreenType(strScreenType);
    gmDonorInfoForm.setStrMsg(strMsg);
    gmDonorInfoForm.setStrBtnDisable(strBtnDisable);
    gmDonorInfoForm.sethVendorId(strVendorId);
    return actionMapping.findForward("DonorInfo");
  }

  /**
   * saveDonorInfo: This method is used to save the donor details
   * 
   * @exception AppError
   */
  public ActionForward saveDonorInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoForm gmDonorInfoForm = (GmDonorInfoForm) actionForm;
    gmDonorInfoForm.loadSessionParameters(request);
    GmDonorInfoBean gmDonorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());

    ArrayList alCtrlDtls = new ArrayList();
    ArrayList alDonorDtls = new ArrayList();
    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strDHRId = "";
    String strDHRStatus = "";
    String strMode = "";
    String strVendorId = "";
    String strMsg = "";
    strMsg = "Records Saved Successfully";
    Integer intDHRStatus;

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorInfoForm);

    strDHRId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    strDHRStatus = GmCommonClass.parseNull((String) hmParam.get("DHRSTATUS"));
    strVendorId = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));
    intDHRStatus = Integer.parseInt(strDHRStatus);

    // 3: Pending Verification, Once the DHR is processed, user should not be able to change the
    // control number
    strMode = intDHRStatus < 3 ? "Edit" : "Report";

    // Call the save procedure to save the donor information
    gmDonorInfoBean.saveDonorInfo(hmParam);

    // redirect to fetch method to fetch the saved details to screen
    strRedirectURL =
        "/gmDonorInfo.do?method=fetchDonorInfo&hDhrId=" + strDHRId + "&hMode=" + strMode
            + "&hVendorId=" + strVendorId + "&strMsg=" + strMsg;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * fetchDHRdetail: This method is used to fetch the donor details when the screen is loading from
   * "Update - Device History Record" screen
   * 
   * @exception AppError
   */
  public ActionForward fetchDHRdetail(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoForm gmDonorInfoForm = (GmDonorInfoForm) actionForm;
    gmDonorInfoForm.loadSessionParameters(request);
    GmDonorInfoBean gmDonorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());

    ArrayList alGender = new ArrayList();
    ArrayList alInternationalUse = new ArrayList();
    ArrayList alCtrlDtls = new ArrayList();
    ArrayList alDonorDtls = new ArrayList();
    ArrayList alLocations = new ArrayList();
    ArrayList alDHRSplitdtl = new ArrayList();
    ArrayList alResearch = new ArrayList();
    HashMap hmResult = new HashMap();
    HashMap hmDonorDtls = new HashMap();
    HashMap hmCtrlsDtls = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmParamv = new HashMap();
    String strLoc = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorInfoForm);
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("HMODE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    // Getting the dropdown values for Sex and International Use
    alGender = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DONSEX"));
    alInternationalUse = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ITLUSE"));
    alResearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DONRES"));
    gmDonorInfoForm.setAlGender(alGender);
    gmDonorInfoForm.setAlInternationalUse(alInternationalUse);
    gmDonorInfoForm.setAlResearch(alResearch);

    // Fetch the donor details for the sections "Donor Details" and "Donor Parameters"
    hmResult = gmDonorInfoBean.fetchDonorInfo(strDHRId);
    alDonorDtls = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("DONORDTLS"));
    alCtrlDtls = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("CTRLDTLS"));

    if (alDonorDtls.size() > 0)
      hmDonorDtls = (HashMap) alDonorDtls.get(0);

    // Get different code groups to be passed to the query from the DHR id
    if (strDHRId.indexOf("MWO") > 0) {
      strLoc = "MWLC";
    } else if (strDHRId.indexOf("SHR") > 0) {
      strLoc = "SHLC";
    } else {
      strLoc = "DHRLC";
    }
    hmParam.put("DHRID", strDHRId);
    hmParam.put("LOCGRP", strLoc);

    // Fetch the values of transaction types to the screen
    alLocations = gmDonorInfoBean.fetchDHRTxnTypeList(hmParam);
    // Fetch DHR Split control number details
    alDHRSplitdtl = gmDonorInfoBean.fetchDHRSplitDtls(hmParam);

    GmCommonClass.getFormFromHashMap(gmDonorInfoForm, hmDonorDtls);
    gmDonorInfoForm.sethDhrId(strDHRId);
    gmDonorInfoForm.sethMode(strMode);
    gmDonorInfoForm.setAlCtrlDtls(alCtrlDtls);

    hmParamv.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
    hmParamv.put("SCREEN", "DHRDetail");// To identify the screen in vm file to show/hide details
    hmParamv.put("MODE", strMode);
    hmParamv.put("CLIST", alCtrlDtls);
    hmParamv.put("TYPE", alLocations);
    hmParamv.put("SPLITDTL", alDHRSplitdtl);
    hmParamv.put("VMFILEPATH", "properties.labels.operations.inventory.GmSavedControlNum");
    hmParamv.put("TEMPLATE", "GmSavedControlNum.vm");
    hmParamv.put("TEMPLATEPATH", "operations/inventory/templates");
    String strGridXmlData = gmDonorInfoBean.getXmlGridData(hmParamv);
    gmDonorInfoForm.setStrGridData(strGridXmlData);
    gmDonorInfoForm.setAlTxnType(alLocations);
    gmDonorInfoForm.setAlDHRSplitdtl(alDHRSplitdtl);
    gmDonorInfoForm.setScreenType("DHRDetail");

    return actionMapping.findForward("DonorInfo");
  }

  /**
   * saveDHRSplit: This method is used to save the donor details when the screen is loading from
   * "Update - Device History Record" screen
   * 
   * @exception AppError
   */
  public ActionForward saveDHRSplit(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoForm gmDonorInfoForm = (GmDonorInfoForm) actionForm;
    gmDonorInfoForm.loadSessionParameters(request);
    GmDonorInfoBean gmDornorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());

    ArrayList alCtrlDtls = new ArrayList();
    ArrayList alDonorDtls = new ArrayList();
    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strVendorId = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorInfoForm);
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    strVendorId = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));
    // save the donor details
    gmDornorInfoBean.saveDHRSplit(hmParam);
    // Call the save procedure to save the donor information
    gmDornorInfoBean.saveDonorMaster(hmParam);
    strRedirectURL =
        "/gmDonorInfo.do?method=fetchDHRdetail&hDhrId=" + strDHRId
            + "&hMode=DHRSplitReport&hVendorId=" + strVendorId;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * fetchDonorParamDtls: To fetch the donor parameter details
   * 
   * @exception AppError
   */
  public ActionForward fetchDonorParamDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoForm gmDonorInfoForm = (GmDonorInfoForm) actionForm;
    gmDonorInfoForm.loadSessionParameters(request);
    GmDonorInfoBean gmDornorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());

    ArrayList alGender = new ArrayList();
    ArrayList alInternationalUse = new ArrayList();
    ArrayList alResearch = new ArrayList();
    HashMap hmResult = new HashMap();

    String strScreenType = GmCommonClass.parseNull(request.getParameter("screenType"));
    String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
    String strDonorNum = GmCommonClass.parseNull(request.getParameter("hDonorNum"));
    String strVendorId = GmCommonClass.parseNull(request.getParameter("hVendorId"));
    String strsubmitScreenType = GmCommonClass.parseNull(request.getParameter("submitAccess"));
    // Getting the dropdown values for Sex and International Use
    alGender = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DONSEX"));
    alInternationalUse = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ITLUSE"));
    alResearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DONRES"));
    gmDonorInfoForm.setAlGender(alGender);
    gmDonorInfoForm.setAlInternationalUse(alInternationalUse);
    gmDonorInfoForm.setAlResearch(alResearch);
    gmDonorInfoForm.setSubmitAccess(strsubmitScreenType);
    hmResult = gmDornorInfoBean.fetchDonorParamDtls(strDonorNum, strVendorId);

    GmCommonClass.getFormFromHashMap(actionForm, hmResult);

    gmDonorInfoForm.sethMode(strScreenType);
    gmDonorInfoForm.setDhrstatus("");
    gmDonorInfoForm.sethDonorNum(strDonorNum);
    gmDonorInfoForm.sethVendorId(strVendorId);
    request.setAttribute("FORMNAME", strFormName);
    return actionMapping.findForward("DonorParam");
  }

  /**
   * saveDonorParamDtls: This method is used to save the donor parameter details
   * 
   * @exception AppError
   */
  public ActionForward saveDonorParamDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoForm gmDonorInfoForm = (GmDonorInfoForm) actionForm;
    gmDonorInfoForm.loadSessionParameters(request);
    GmDonorInfoBean gmDornorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strDonorNum = "";
    String strVendorId = "";
    String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorInfoForm);
    strDonorNum = GmCommonClass.parseNull((String) hmParam.get("HDONORNUM"));
    strVendorId = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));

    hmParam.put("DONORNO", strDonorNum);
    gmDornorInfoBean.saveDonorMaster(hmParam);
    request.setAttribute("FORMNAME", strFormName);
    strRedirectURL =
        "/gmDonorInfo.do?method=fetchDonorParamDtls&hDonorNum=" + strDonorNum + "&FORMNAME="
            + strFormName + "&hVendorId=" + strVendorId + "&screenType=recShip";
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * saveLotCodeStatus: This method is used to save the lot code and status
   * 
   * @exception AppError
   */
  public ActionForward saveLotCodeStatus(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoForm gmDonorInfoForm = (GmDonorInfoForm) actionForm;
    gmDonorInfoForm.loadSessionParameters(request);
    GmDonorInfoBean gmDonorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strRedirectURL = "";
    String strDHRId = "";
    String strVendorId = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmDonorInfoForm);
    String strNcmrStatus = GmCommonClass.parseNull((String) hmParam.get("NCMRSTATUS"));
    strDHRId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    strVendorId = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));
    String strMode = GmCommonClass.parseNull((String) hmParam.get("HMODE"));

    // Below save procedure is not needed since the Donor Parameter section in NCMR screen is report
    // gmDonorInfoBean.saveDonorMaster(hmParam); // To save the donor parameters

    // Call the save procedure to save the information
    gmDonorInfoBean.saveLotCodeStatus(hmParam);

    // redirect to fetch method to fetch the saved details to screen
    strRedirectURL =
        "/gmDonorInfo.do?method=fetchDonorInfo&hDhrId=" + strDHRId
            + "&screenType=NcmrUpdate&ncmrStatus=" + strNcmrStatus + "&hVendorId=" + strVendorId
            + "&hMode=" + strMode;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * saveLotCodeStatus: This method is used to save the lot code and status
   * 
   * @exception AppError
   * @throws IOException
   */
  public ActionForward validateCtrlNum(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmDonorInfoBean gmDonorInfoBean = new GmDonorInfoBean(getGmDataStoreVO());
    String strScanCode = GmCommonClass.parseNull(request.getParameter("scanCode"));
    String strDhrId = GmCommonClass.parseNull(request.getParameter("hDHRId"));
    String strDupCtlNums = "";
    strDupCtlNums = GmCommonClass.parseNull(gmDonorInfoBean.validateCtrlNum(strScanCode, strDhrId));
    log.debug("strDupCtlNums>>>>>>>>" + strDupCtlNums);
    response.setContentType("text/plain");

    PrintWriter pw = response.getWriter();
    pw.write(strDupCtlNums);
    pw.flush();
    return null;
  }
}
