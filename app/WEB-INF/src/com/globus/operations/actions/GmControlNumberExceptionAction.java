package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmDHRBean;
import com.globus.operations.forms.GmControlNumberExceptionForm;

public class GmControlNumberExceptionAction extends GmAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    // TODO Auto-generated method stub

    Logger log = GmLogger.getInstance(this.getClass().getName());
    log.debug("GmControlNumberExceptionAction");
    String strOpt = "";
    String strFrdPage = "GmControlNumberExceptionSetup";
    String strCtrlAsd_ID = "";
    ArrayList alLogReasons = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alPartyList = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    instantiate(request, response);
    GmControlNumberExceptionForm gmControlNumberExceptionForm = (GmControlNumberExceptionForm) form;
    gmControlNumberExceptionForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDHRBean gmDHRBean = new GmDHRBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    ActionMessages amMessages = new ActionMessages();

    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass
            .getHashMapFromForm(gmControlNumberExceptionForm));
    strOpt = GmCommonClass.parseNull(gmControlNumberExceptionForm.getStrOpt());
    String strPartNumber = GmCommonClass.parseNull(request.getParameter("partNumber"));
    String strleftLink = GmCommonClass.parseNull(request.getParameter("leftLink"));
    String strPartyId = gmControlNumberExceptionForm.getSessPartyId();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    alPartyList = gmAccessControlBean.getPartyList("CNOVERRIDE", strPartyId);   
    // limited access if setup page is called
    if (alPartyList.size() == 0
        && (strOpt.equals("") || strOpt.equals("edit") || strOpt.equals("save"))) {
      throw new AppError("", "20690");
    }
    strleftLink = strleftLink.equals("") ? "true" : strleftLink;
    // String strRefId = GmCommonClass.parseNull(request.getParameter("refid"));
    // hmParam.put("REFID", strRefId);
    gmControlNumberExceptionForm.setPartNM(strPartNumber);
    gmControlNumberExceptionForm.setLeftLink(strleftLink);
    // log.debug("=====strRefId======="+strRefId);
    log.debug("=====pnum=======" + strPartNumber);
    log.debug("=====strOpt=======" + strOpt);
    if (strOpt.equals("save")) {
      strCtrlAsd_ID = GmCommonClass.parseNull(gmDHRBean.saveControlNumberDetails(hmParam));
      gmControlNumberExceptionForm.setCtrlAsd_ID(strCtrlAsd_ID);
      alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strCtrlAsd_ID, "92092"));
      gmControlNumberExceptionForm.setAlLogReasons(alLogReasons);
      GmCommonClass.getFormFromHashMap(gmControlNumberExceptionForm, hmParam);
      gmControlNumberExceptionForm.setCtrlAsd_ID(strCtrlAsd_ID);
      ActionMessage success = new ActionMessage("message.success");
      amMessages.add("success", success);
      saveMessages(request, amMessages);
      strOpt = "edit";
    } else if (strOpt.equals("edit")) {
      hmResult = GmCommonClass.parseNullHashMap(gmDHRBean.fetchControlNumberDetails(hmParam));
      log.debug("HM Result : " + hmResult);
      // String
      // strLock=(gmDHRBean.checkLockFlag(hmParam).trim().toLowerCase().equals("y"))?"disabled":"";
      // log.debug("Lock : "+strLock);
      gmControlNumberExceptionForm.setPartNM(GmCommonClass.parseNull((String) hmResult
          .get("PARTNM")));
      gmControlNumberExceptionForm.setControlNM(GmCommonClass.parseNull((String) hmResult
          .get("CONTROLNM")));
      if (gmControlNumberExceptionForm.getPartNM().equals(""))
        gmControlNumberExceptionForm.setPartNM(strPartNumber);
      strCtrlAsd_ID = gmControlNumberExceptionForm.getCtrlAsd_ID();
      // request.setAttribute("lockfl", strLock);
      // if(strLock.trim().equals("disabled"))
      // gmControlNumberExceptionForm.setStrMsg("Note: You can't VOID this Control Number, because it was Locked.");
      alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strCtrlAsd_ID, "92092"));
      gmControlNumberExceptionForm.setAlLogReasons(alLogReasons);
    } else if (strOpt.equals("reload")) {
      alResult = gmDHRBean.fetchControlNumberList(hmParam);
      String strXmlData = generateOutPut(alResult, strSessCompanyLocale);
      gmControlNumberExceptionForm.setStrXmlData(strXmlData);
      strFrdPage = "GmControlNumberExceptionReport";
    } else if (strOpt.equals("report")) {
      strFrdPage = "GmControlNumberExceptionReport";
    }
    else if(strOpt.equals("loadExpDtOverride"))
	{
    	hmParam.put("TXNID","");
    	strCtrlAsd_ID = GmCommonClass.parseNull(gmDHRBean.saveCnumSkipExpiryDate(hmParam));        	
        gmControlNumberExceptionForm.setCtrlAsd_ID(strCtrlAsd_ID);
        alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strCtrlAsd_ID, "92092"));
        gmControlNumberExceptionForm.setAlLogReasons(alLogReasons);        
	 	strOpt="saveExpDtOverride";    	
    	
	}else if(strOpt.equals("saveExpDtOverride"))
    {    		
    	 	strCtrlAsd_ID = GmCommonClass.parseNull(gmDHRBean.saveCnumSkipExpiryDate(hmParam));        	
            gmControlNumberExceptionForm.setCtrlAsd_ID(strCtrlAsd_ID);
            alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strCtrlAsd_ID, "92092"));
            gmControlNumberExceptionForm.setAlLogReasons(alLogReasons);
            GmCommonClass.getFormFromHashMap(gmControlNumberExceptionForm, hmParam);
            ActionMessage success = new ActionMessage("message.success");
            amMessages.add("success", success);
            saveMessages(request, amMessages);
    }      
    gmControlNumberExceptionForm.setStrOpt(strOpt);
    return mapping.findForward(strFrdPage);
  }

  private String generateOutPut(ArrayList alGridData, String strSessCompanyLocale) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmControlNumber", strSessCompanyLocale));
    templateUtil.setTemplateName("GmControlNumber.vm");
    return templateUtil.generateOutput();
  }
}
