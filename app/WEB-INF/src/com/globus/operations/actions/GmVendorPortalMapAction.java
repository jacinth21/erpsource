package com.globus.operations.actions;

import org.apache.log4j.Logger;

import java.util.ArrayList;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmVendorPortalMapBean;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;

import com.globus.operations.forms.GmVendorPortalMapForm;

public class GmVendorPortalMapAction extends GmDispatchAction{
	
	/**
	 * get the instance of this class for enabling logging purpose
	 */
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	 
	 /**
	  * fetchVendorPortalMap - To fetch the Vendor Portal Map details
	  * @param actionMapping
	  * @param form, request, response
	  * @return
	  * @throws Exception
	  */
	public ActionForward fetchVendorPortalMap(ActionMapping actionMapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
				instantiate(request, response);
				GmVendorPortalMapForm gmVendorPortalMapForm = (GmVendorPortalMapForm) form;
				gmVendorPortalMapForm.loadSessionParameters(request);
				GmVendorPortalMapBean gmVendorPortalMapBean= new GmVendorPortalMapBean(getGmDataStoreVO());
				String strfromPartyid = "";
				ArrayList alReturn = new ArrayList();
				HashMap hmReturn = new HashMap();
			    String strSessCompanyLocale =
			            GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
				strfromPartyid = GmCommonClass.parseNull(gmVendorPortalMapForm.getFromVendorId());
				alReturn = GmCommonClass.parseNullArrayList(gmVendorPortalMapBean.fetchVendorPortalMap(strfromPartyid));
				hmReturn.put("VendorPortalMapValue", alReturn);
				
				gmVendorPortalMapForm.setAlVendorMapList(alReturn);
				// to generate grid data
				hmReturn.put("ALRESULT", alReturn);
				hmReturn.put("VMFILE", "GmVendorPortalMapping.vm");
				hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
			    String strGridData = generateOutPut(hmReturn);
			    gmVendorPortalMapForm.setGridData(strGridData);
				
				return actionMapping.findForward("success");
	}


	/** voidVendorPortalMap - To remove the Vendor Portal Map details
	 * @param actionMapping
	 * @param form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward voidVendorPortalMap(ActionMapping actionMapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response) throws Exception {
				instantiate(request, response);
				GmVendorPortalMapForm gmVendorPortalMapForm = (GmVendorPortalMapForm) form;
				gmVendorPortalMapForm.loadSessionParameters(request);
			
				HashMap hmParam = new HashMap();
				hmParam = GmCommonClass.getHashMapFromForm(gmVendorPortalMapForm);
				GmVendorPortalMapBean gmVendorPortalMapBean = new GmVendorPortalMapBean(getGmDataStoreVO());
				gmVendorPortalMapBean.voidVendorPortalMap(hmParam);
				
				return actionMapping.findForward("success");
	}
	
	/**
	 * saveVendorPortalMap - To save the Vendor Portal Map details
	 * @param actionMapping
	 * @param form, request, response
	 * @return
	 * @throws Exception
	 */
	public ActionForward  saveVendorPortalMap(ActionMapping actionMapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws Exception {
				
				instantiate(request, response);
				GmVendorPortalMapForm gmVendorPortalMapForm = (GmVendorPortalMapForm) form;
				gmVendorPortalMapForm.loadSessionParameters(request);
				GmVendorPortalMapBean gmVendorPortalMapBean = new GmVendorPortalMapBean(getGmDataStoreVO());
				HashMap hmParam = new HashMap();
				hmParam = GmCommonClass.getHashMapFromForm(gmVendorPortalMapForm);
				gmVendorPortalMapBean.saveVendorPortalMap(hmParam);
				
				return actionMapping.findForward("success");
	}
	

	  /**
	   * generateOutPut This method is used to generate data which need to show in grid.
	   * 
	   * @param hmParam HashMap
	   * @exception AppError
	   */
	private String generateOutPut(HashMap hmParam) throws AppError{
	    GmTemplateUtil templateUtil = new GmTemplateUtil();
	    String vmFile = GmCommonClass.parseNull((String) hmParam.get("VMFILE"));
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	    templateUtil.setDataMap("hmParam", hmParam);
	    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	        "properties.labels.operations.GmVendorPortalMapping", strSessCompanyLocale));
	    templateUtil.setTemplateSubDir("operations/templates");
	    templateUtil.setTemplateName(vmFile);
	    return templateUtil.generateOutput();
	}
}
