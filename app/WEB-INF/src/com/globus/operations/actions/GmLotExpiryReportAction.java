package com.globus.operations.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmLotExpiryReportBean;
import com.globus.operations.forms.GmLotExpiryReportForm;
/**
* GmLotExpiryReportAction : Contains the methods used for the Lot Expiry Report screen
* author : Agilan Singaravel
*/
public class GmLotExpiryReportAction extends GmDispatchAction {
	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * loadLotExpiryDashboard - used to load lot track expiry report screen for sterile parts
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLotExpiryDashboard(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ArrayList alPartSearch = new ArrayList();
		// call the instantiate method
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());
		alPartSearch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LIKES"));
		gmLotExpiryReportForm.setAlPartSearch(alPartSearch);
		return actionMapping.findForward("gmLotExpiryReport");
	}
	/**
	 * fetchLotExpByRange - used to fetch lot track expiry report details based on expired days
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotExpByRange(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// call the instantiate method
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		HashMap hmResult = new HashMap();
		String strReturnJson = "";
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLotExpiryReportForm));
		String strOpt = GmCommonClass.parseNull(gmLotExpiryReportForm.getStrOpt());		
		
		if(strOpt.equals("loadSterile")){
			strReturnJson = gmLotExpiryReportBean.fetchLotExpByRange(hmParam);
		}else{
			strReturnJson = gmLotExpiryReportBean.fetchTissueLotExpByRange(hmParam);
		}

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strReturnJson.equals("")) {
			pw.write(strReturnJson);
		}
		pw.flush();
		return null;
	}
	/**
	 * fetchLotExpGraph - used to fetch lot track expiry report chart data based on expired days
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchLotExpGraph(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// call the instantiate method
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		HashMap hmResult = new HashMap();
		String strReturnJson = "";
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLotExpiryReportForm));
		String strOpt = GmCommonClass.parseNull(gmLotExpiryReportForm.getStrOpt());		
		
		if(strOpt.equals("loadSterile")){
			strReturnJson = gmLotExpiryReportBean.fetchLotExpGraph(hmParam);
		}else{
			strReturnJson = gmLotExpiryReportBean.fetchTissueLotExpGraph(hmParam);
		}
	
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strReturnJson.equals("")) {
			pw.write(strReturnJson);
		}
		pw.flush();
		return null;
	}
	/**
	 * loadExpiryLotDetails - used to load lot track expiry report popup screen
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */	
	public ActionForward loadExpiryLotDetails(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());

		return actionMapping.findForward("gmLotExpiryPopUp");
	}

	/**
	 * fetchInventoryLotDetails - used to fetch lot track popup screen Inventory details based on expired days like 
	 *                            0 to 30 days, 301 to 60 days, 61 to 90 days and others
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */	
	public ActionForward fetchInventoryLotDetails(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strReturnJson = "";
		String strOpt = GmCommonClass.parseNull(gmLotExpiryReportForm.getStrOpt());	
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLotExpiryReportForm));		

		if(strOpt.equals("loadSterile")){
			strReturnJson = gmLotExpiryReportBean.fetchInventoryLotDetails(hmParam);
		}else{
			strReturnJson = gmLotExpiryReportBean.fetchTissueLotPopupDetails(hmParam);
		}
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strReturnJson.equals("")) {
			pw.write(strReturnJson);
		}
		pw.flush();
		return null;
	}
	/**
	 * fetchFieldSalesLotDetails - used to fetch lot track popup screen Field Sales details based on expired days like 
	 *                            0 to 30 days, 301 to 60 days, 61 to 90 days and others
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */	
	public ActionForward fetchFieldSalesLotDetails(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strReturnJson = "";
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLotExpiryReportForm));
		String strOpt = GmCommonClass.parseNull(gmLotExpiryReportForm.getStrOpt());	
	
		if(strOpt.equals("loadSterile")){
			strReturnJson = gmLotExpiryReportBean.fetchFieldSalesLotDetails(hmParam);
		}else{
			strReturnJson = gmLotExpiryReportBean.fetchTissueLotPopupDetails(hmParam);
		}
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strReturnJson.equals("")) {
			pw.write(strReturnJson);
		}
		pw.flush();
		return null;
	}
	/**
	 * fetchConsignmentSetLotDetails - used to fetch lot track popup screen Consignment set details based on expired days like 
	 *                            0 to 30 days, 301 to 60 days, 61 to 90 days and others
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */		
	public ActionForward fetchConsignmentSetLotDetails(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strReturnJson = "";
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLotExpiryReportForm));
		String strOpt = GmCommonClass.parseNull(gmLotExpiryReportForm.getStrOpt());;	
		
		if(strOpt.equals("loadSterile")){
			strReturnJson = gmLotExpiryReportBean.fetchConsignmentSetLotDetails(hmParam);
		}else{
			strReturnJson = gmLotExpiryReportBean.fetchTissueLotPopupDetails(hmParam);
		}
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strReturnJson.equals("")) {
			pw.write(strReturnJson);
		}
		pw.flush();
		return null;
	}
	/**
	 * fetchLoanerSetLotDetails - used to fetch lot track popup screen Loaner set details based on expired days like 
	 *                            0 to 30 days, 301 to 60 days, 61 to 90 days and others
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */	
	public ActionForward fetchLoanerSetLotDetails(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		instantiate(request, response);
		GmLotExpiryReportForm gmLotExpiryReportForm = (GmLotExpiryReportForm) form;
		gmLotExpiryReportForm.loadSessionParameters(request);
		GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strReturnJson = "";
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmLotExpiryReportForm));	
		String strOpt = GmCommonClass.parseNull(gmLotExpiryReportForm.getStrOpt());;	
		
		if(strOpt.equals("loadSterile")){
			strReturnJson = gmLotExpiryReportBean.fetchLoanerSetLotDetails(hmParam);
		}else{
			strReturnJson = gmLotExpiryReportBean.fetchTissueLoanerSetLotDetails(hmParam);
		}
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strReturnJson.equals("")) {
			pw.write(strReturnJson);
		}
		pw.flush();
		return null;
	}	
}
