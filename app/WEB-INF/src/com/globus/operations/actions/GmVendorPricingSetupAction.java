package com.globus.operations.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.forms.GmVendorPricingSetupForm;
import com.globus.prodmgmnt.beans.GmProjectBean;


/**
 * @author Anilkumar The Class GmVendorPricingSetupAction.
 * @extends Action class
 */
public class GmVendorPricingSetupAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmVendorPricingSetupForm gmVendPriceForm = (GmVendorPricingSetupForm) form;
    gmVendPriceForm.loadSessionParameters(request);
    HashMap hmTemplate = new HashMap();
    String strOpt = "";
    String strXmlStringData = "";
    String strAction = "";
    String strForward = "";
    strAction = gmVendPriceForm.getHaction();
    strOpt = gmVendPriceForm.getStrOpt();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    if (strAction.equals("load")) {
      // below metod is used to fetch vendor Pricing details
      hmTemplate = loadVendorPricingDetails(gmVendPriceForm);
      if (strOpt.equals("fetch")) {
    	String  maxCountvalue = GmCommonClass.parseNull(GmCommonClass.getRuleValue("VENDOR_PRICING", "ADD_DEFAULT_ROW")); // this variable is used to get the default row size
    	int maxCount=Integer.parseInt(maxCountvalue);
    	gmVendPriceForm.setMaxCount(maxCount);
        hmTemplate.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
        strXmlStringData = generateXMLString(hmTemplate);
        gmVendPriceForm.setXmlString(strXmlStringData);
      }
    }
    if (strAction.equals("save")) {
      // below metod is used to save vendor Pricing details
      String strErrorMsg = GmCommonClass.parseNull(saveVenodrPricingDetails(gmVendPriceForm));
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (!strErrorMsg.equals("")) {
        response.setStatus(300); // Explicit Error Status to identify the error status in JS.
        pw.write(strErrorMsg);
      } else {
        pw.write("Data Saved Successfully.");
      }
      pw.flush();
      pw.close();
      return null;
    }
    return mapping.findForward("gmVendorPricingSetup");
  }

  /**
   * saveVenodrPricingDetails - To save the Vendor pricing Details.
   * 
   * @param GmVendorPricingSetupForm gmVendPriceForm
   * @return ActionRedirect
   * @throws AppError
   */
  private String saveVenodrPricingDetails(GmVendorPricingSetupForm gmVendPriceForm) throws AppError {

    GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
    HashMap hmFormValues = new HashMap();

    hmFormValues = GmCommonClass.getHashMapFromForm(gmVendPriceForm);
    // below metod is used to save vendor Pricing details
    String strErrorMsg = gmVendorBean.saveVendorPricingDetails(hmFormValues);
    return strErrorMsg;

  }// End of saveVenodrPricingDetails

  /**
   * Load vendor pricing details.
   * 
   * @param gmVendPriceForm the gm vend price form
   * @return the action forward
   * @throws AppError the app error
   */
  private HashMap loadVendorPricingDetails(GmVendorPricingSetupForm gmVendPriceForm)
      throws AppError {

    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();

    HashMap hmParam = new HashMap();
    HashMap hmVendList = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmTempalate = new HashMap();
    HashMap hmFormValues = new HashMap();

    ArrayList alProjList = new ArrayList();
    ArrayList alUOMList = new ArrayList();
    ArrayList alPartsList = new ArrayList();
    ArrayList alReturn = new ArrayList();
    ArrayList alVendorList = new ArrayList();

    String strSubmitAccess = "VENDPRC_BLK_UPD";
    String strUserId = "";
    String strProjectId = "";
    String strPartNum = "";
    String strPartNumFormat = "";
    String strSearch = "";
    String strPartyId = "";
    String strVendorCostFl = "";
    String strVendorCostType = "106560";// Default cost type as vendor price

    String strOpt = GmCommonClass.parseNull(gmVendPriceForm.getStrOpt());
    strUserId = GmCommonClass.parseNull(gmVendPriceForm.getUserId());
    strProjectId = GmCommonClass.parseNull(gmVendPriceForm.getProjectId());
    strPartNum = GmCommonClass.parseNull(gmVendPriceForm.getPartNumber());
    strSearch = GmCommonClass.parseNull(gmVendPriceForm.getSearch());
    strPartyId =  GmCommonClass.parseNull(gmVendPriceForm.getSessPartyId());
    hmFormValues = GmCommonClass.getHashMapFromForm(gmVendPriceForm);

    hmParam.put("COLUMN", "ID");
    String strProjectCompanyId= GmCommonClass.parseNull(gmVendPriceForm.getCompanyId());
    hmParam.put("COMPANY_ID_PROJECT_LIST", strProjectCompanyId);
    // alProjList = GmCommonClass.parseNullArrayList(gmProject.reportProject(hmParam));
    gmVendPriceForm.setAlProjectList(gmProject.reportProject(hmParam));
    
 // Access flag for standard cost vendor type PMT-51082
    hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
                "STD_COST_VENDOR_ACC"));
    strVendorCostFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    
  //standard cost vendor type access value is Y, Then setting type as Standard cost
    if(strVendorCostFl.equals("Y")){
  	  strVendorCostType = "106561"; // cost type as Standard cost
    }
    
    //PMT-51082 Setting cost type flag value as Y ,to fetch vendor list
    hmParam.put("COST_TYPE_FL", "Y");
    hmParam.put("COST_TYPE", strVendorCostType);
    
    //PMT-51082 Setting cost type flag value as Y ,to fetch vendor pricing report
    hmFormValues.put("COST_TYPE", strVendorCostType);
    String strCompanyId=GmCommonClass.parseNull(gmVendPriceForm.getCompanyId());
    hmParam.put("COMPANY_ID", strCompanyId);
    hmVendList = GmCommonClass.parseNullHashMap(gmVendorBean.getVendorList(hmParam));
    alVendorList = GmCommonClass.parseNullArrayList((ArrayList) hmVendList.get("VENDORLIST"));
    gmVendPriceForm.setAlAllVendorList(alVendorList);

    if (strOpt.equals("fetch")) {
      // getting submit access base on the user loggedin.
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              strSubmitAccess));
      gmVendPriceForm.setSubmitAccFl(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

      alUOMList = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("UOM"));
      gmVendPriceForm.setAlUOMList(alUOMList);

      hmTempalate.put("UOMLIST", alUOMList);
      hmTempalate.put("VENDORLIST", alVendorList);

      // below method is used to get the partnumlist based on the project id selected
      alPartsList =
          GmCommonClass.parseNullArrayList(gmProject.reportPartNumByProject(strProjectId));
      gmVendPriceForm.setAlPartsList(alPartsList);
      hmTempalate.put("PARTNUMS", alPartsList);

      // forming partnumber string based on the search criteria given
      // strPartNum = GmCommonClass.createRegExpString(hmFormValues);
      strPartNumFormat = strPartNum;
      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }
      hmFormValues.put("PARTNUMBER", strPartNumFormat);

      // we getting vendor pricing details by calling the below method.
      alReturn = gmVendorBean.fetchVendorPricingDetails(hmFormValues);
      hmTempalate.put("REPORT", alReturn);
    }

    return hmTempalate;
  }


  /**
   * Generate xml string.
   * 
   * @param hmParam the hm param
   * @return the string
   * @throws AppError the app error
   */
  private String generateXMLString(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.purchasing.GmVendorPricingSetup",
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setTemplateName("GmVendorPricingSetup.vm");
    return templateUtil.generateOutput();
  }// End of generateXMLString
}
