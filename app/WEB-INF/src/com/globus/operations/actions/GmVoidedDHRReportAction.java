package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmDHRBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.forms.GmVoidedDHRReportForm;

public class GmVoidedDHRReportAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    log.debug("Enter into GmVoidedDHRReportAction in Action");

    instantiate(request, response);
    GmVoidedDHRReportForm gmVoidedDHRReportForm = (GmVoidedDHRReportForm) actionForm;
    GmDHRBean gmDHRBean = new GmDHRBean(getGmDataStoreVO());
    GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());

    ArrayList alVendorList = new ArrayList();
    ArrayList alReport = new ArrayList();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();

    String strOpt = "";

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    gmVoidedDHRReportForm.loadSessionParameters(request);

    strOpt = GmCommonClass.parseNull(gmVoidedDHRReportForm.getStrOpt());


    // Get employee list
    hmTemp.put("CHECKACTIVEFL", "");
    hmResult = gmVendorBean.getVendorList(hmTemp);

    alVendorList = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("VENDORLIST"));
    gmVoidedDHRReportForm.setAlVendorName(alVendorList);

    hmParam = GmCommonClass.getHashMapFromForm(gmVoidedDHRReportForm);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    hmParam.put("APPLNDATEFMT", strApplDateFmt);
    if (strOpt.equals("Reload")) {
      log.debug("enter into report");
      alReport = GmCommonClass.parseNullArrayList(gmDHRBean.fetchVoidedDHRReprot(hmParam));
      hmReturn.put("VOIDEDDHRDATA", alReport);
      hmReturn.put("APPLNDATEFMT", strApplDateFmt);
      hmReturn.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);

      String strxmlGridData = generateOutPut(hmReturn);
      gmVoidedDHRReportForm.setXmlStringData(strxmlGridData);
    }


    return actionMapping.findForward("gmDHRReport");

  }

  private String generateOutPut(HashMap hmParam) throws AppError { // Processing HashMap into XML
                                                                   // data
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("operations/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmVoidedDHRReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmVoidedDHRReport.vm");
    return templateUtil.generateOutput();
  }
}
