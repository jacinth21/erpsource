//Source file: C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmLockTagAction.java

package com.globus.operations.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.forms.GmSubPartOrderForm;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPartMappingAction extends GmAction {
	// private String strOpt = LOAD / RELOAD / SAVE;

	/**
	 * @return org.apache.struts.action.ActionForward
	 * @roseuid 48D1292D0247
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {

			GmSubPartOrderForm gmSubPartOrderForm = (GmSubPartOrderForm) form;
			gmSubPartOrderForm.loadSessionParameters(request);

			GmProjectBean gmProj = new GmProjectBean();
			GmPurchaseBean gmPurchaseBean = new GmPurchaseBean();
			GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
			Logger log = GmLogger.getInstance(this.getClass().getName());

			String strOpt = gmSubPartOrderForm.getStrOpt();
			String strUserId = gmSubPartOrderForm.getUserId();

			RowSetDynaClass rdPartResult = null;

			HashMap hmParam = new HashMap();

			ArrayList alProject = new ArrayList();
			

			if (strOpt.equals("reload")) {
				
				
				String hsubcheck = gmSubPartOrderForm.getHsubpartNotcheck();
	            String hparentcheck = gmSubPartOrderForm.getHparentpartNotcheck();
	            
	            if(hsubcheck.equals("true")) gmSubPartOrderForm.setDisplaySubParts("");
	            if(hparentcheck.equals("true")) gmSubPartOrderForm.setDisplayParentParts("");
	            
	            hmParam = GmCommonClass.getHashMapFromForm(gmSubPartOrderForm);
				log.debug(" hmParam. ..... " + hmParam);
				rdPartResult = gmPurchaseBean.loadpartMapping(hmParam);

				log.debug(" rdPartResult.getRows().size() ..... " + rdPartResult.getRows().size());
				gmSubPartOrderForm.setReturnList(rdPartResult.getRows());

				if (gmSubPartOrderForm.getDisplayOrderParts().equals("on")) {
					gmSearchCriteria.addSearchCriteria("TOORDER", "N", GmSearchCriteria.NOTEQUALS);
				} else if (!(gmSubPartOrderForm.getDisplaySubParts().equals("on")&&gmSubPartOrderForm.getDisplayParentParts().equals("on"))){
					if  (gmSubPartOrderForm.getDisplaySubParts().equals("on")){

						gmSearchCriteria.addSearchCriteria("PART_TYPE", "SP", GmSearchCriteria.EQUALS);
					}
					if (gmSubPartOrderForm.getDisplayParentParts().equals("on")) {

						gmSearchCriteria.addSearchCriteria("PART_TYPE", "PP", GmSearchCriteria.EQUALS);
					}
				}
				gmSearchCriteria.query(rdPartResult.getRows());
			}
			HashMap hmProj = new HashMap();
			hmProj.put("COLUMN", "ID");
			hmProj.put("STATUSID", "20301"); // Filter all approved projects
			alProject = gmProj.reportProject(hmProj);
			gmSubPartOrderForm.setProjectList(alProject);

		} catch (Exception exp) {
			exp.printStackTrace();
			throw new AppError(exp);
		}

		return mapping.findForward("success");
	}

}
