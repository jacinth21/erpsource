package com.globus.operations.returns.beans;
import com.globus.common.beans.AppError;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

public abstract class GmBBAReturnSummaryPicSlip implements GmBBAProcessReturnsProcessInterface {
	/**
	 * This method returns the object dynamically. 
	 * @param strClassName
	 * @return
	 * @throws Exception
	 */
	
	public static GmBBAProcessReturnsProcessInterface getInstance(String strClassName) throws AppError	{	
		GmBBAProcessReturnsProcessInterface clientInterface = null;
		try {
			Class packSlipClient = Class.forName(strClassName);
			clientInterface = (GmBBAProcessReturnsProcessInterface) packSlipClient
					.newInstance();
		} catch (Exception ex) {
			throw new AppError(ex);
		}
		return clientInterface;	  
	}
}
