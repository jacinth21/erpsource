package com.globus.operations.returns.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmUSProcessReturnsBean implements GmBBAProcessReturnsProcessInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public HashMap loadCreditReport(String strRAID, String strAction) throws AppError {
    // TODO Auto-generated method stub
    log.debug("Enter");
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();


    GmDBManager gmDBManager = new GmDBManager();

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT A.C506_RMA_ID RAID, A.C506_REF_ID REFID, NVL(A.C701_DISTRIBUTOR_ID,A.C704_ACCOUNT_ID) DID ");
    sbQuery.append(" , A.C506_STATUS_FL STATUS ");
    sbQuery.append(" , get_cs_ship_email(4121, A.C703_SALES_REP_ID) EMAILADD");
    sbQuery.append(" ,A.C506_CREDIT_DATE CREDITEMAILDATE ");
    sbQuery
        .append(" , DECODE(A.C701_DISTRIBUTOR_ID, NULL, GET_ACCOUNT_NAME(A.C704_ACCOUNT_ID), GET_DISTRIBUTOR_NAME(A.C701_DISTRIBUTOR_ID)) DNAME ");
    sbQuery.append(" , A.C506_STATUS_FL STATUS, ");
    sbQuery.append(" GET_USER_NAME(A.C506_LAST_UPDATED_BY) CREDITBY, ");
    sbQuery.append(" A.C506_COMMENTS COMMENTS,A.C506_CREATED_DATE CDATE, ");
    sbQuery.append(" A.C506_AC_CREDIT_DT CREDITDATE, ");
    sbQuery.append(" GET_USER_NAME(A.C506_CREATED_BY) PER, GET_CODE_NAME(A.C506_TYPE) TYPE, ");
    sbQuery.append(" A.C506_CREDIT_MEMO_ID MEMOID, T501.C503_INVOICE_ID CREDITINVID , ");
    sbQuery
        .append(" GET_CODE_NAME(A.C506_REASON) REASON, A.C207_SET_ID SETID, B.C503_CUSTOMER_PO PO,");
    sbQuery
        .append(" DECODE(A.C506_TYPE,3308,GET_CONSIGN_BILL_ADD(A.C506_REF_ID), GET_CREDIT_BILL_ADD (A.C506_RMA_ID)) ACCADD, ");
    sbQuery.append(" A.C506_LAST_UPDATED_DATE UDATE,");
    sbQuery.append(" B.C503_INVOICE_ID INVID, ");
    sbQuery.append(" T501.C501_COMMENTS ORDER_COMMENTS, ");
    sbQuery.append(" C506_EXPECTED_DATE EDATE ");
    sbQuery.append(" ,T501.C501_SHIP_COST SCOST ");
    sbQuery.append(" ,A.C506_CREATED_DATE RETDATE ");
    sbQuery
        .append(" ,A.C501_ORDER_ID ORDER_ID, T501.C901_ORDER_TYPE ORDTYPE, A.C506_TYPE RETTYPE  ");
    sbQuery
        .append(" ,decode(GET_DISTRIBUTOR_INTER_PARTY_ID(A.C701_DISTRIBUTOR_ID),NULL,'USD',get_rule_value (50218, get_distributor_inter_party_id (A.c701_distributor_id))) NEWCURRENCY ");
    sbQuery.append(" ,GET_DISTRIBUTOR_INTER_PARTY_ID(A.C701_DISTRIBUTOR_ID) PARTYID ");
    sbQuery
        .append(" ,nvl(get_rule_value (A.c701_distributor_id, 'CC-LIST-RETURNS'),get_rule_value ('DEFAULTSHIPEMAIL', 'EMAIL')) CCLIST , get_user_emailid (A.C506_CREATED_BY) EMAIL_ID ");
    sbQuery
        .append(" ,get_ac_company_id(T501.c704_account_id) COMPANYID ,get_rep_name (A.c703_sales_rep_id) REPNM ");
    // to fetch child RA list for passing RA - PMT-234
    sbQuery
        .append(" , (SELECT  RTRIM(XMLAGG(XMLELEMENT(E,c506_rma_id || ',')).EXTRACT('//text()').GETCLOBVAL(),',')  ");
    sbQuery.append(" FROM T506_RETURNS WHERE c506_parent_rma_id = '");
    sbQuery.append(strRAID);
    sbQuery
        .append("' AND c506_void_fl IS NULL) CHILDRAID , get_rule_value('DOMAINDB','DOMAINPACKSLIP') RULEDOMAIN , get_rule_value('RETURNSUMMARY','LOGO') IMAGELOGO, get_rule_value ('RAPICSLIP', 'RETURNSUMMARY')  BBARASUMMARYSHOW ");
    sbQuery.append(", get_account_curr_symb (A.C704_ACCOUNT_ID) ACCOUNT_CURR_NM ");
    sbQuery.append(" FROM T506_RETURNS A, T503_INVOICE B, T501_ORDER T501 ");
    sbQuery.append(" WHERE A.C506_RMA_ID = '");
    sbQuery.append(strRAID);
    sbQuery.append("' AND A.C503_INVOICE_ID = B.C503_INVOICE_ID(+)");
    sbQuery.append(" AND A.C506_RMA_ID = T501.C506_RMA_ID(+)");
    sbQuery.append(" AND B.C503_VOID_FL IS NULL ");
    sbQuery.append(" AND A.C506_VOID_FL IS NULL ");
    sbQuery.append(" AND (B.c901_ext_country_id is NULL ");
    sbQuery.append(" OR B.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND (T501.c901_ext_country_id is NULL ");
    sbQuery.append(" OR T501.c901_ext_country_id  in (select country_id from v901_country_codes))");

    log.debug(" Query for RADETAILS is " + sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

    hmReturn.put("RADETAILS", hmResult);

    // Since its a return QTY is divided by -ive value
    /*
     * sbQuery.setLength(0); sbQuery.append(
     * " SELECT -1 * SUM(C507_ITEM_QTY) QTY, C507_ITEM_PRICE PRICE, C507_STATUS_FL SFL, ");
     * sbQuery.append(" C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC ");
     * sbQuery.append(" FROM T507_RETURNS_ITEM "); sbQuery.append(" WHERE C506_RMA_ID = '");
     * sbQuery.append(strRAID); sbQuery.append("' AND C507_STATUS_FL IN ('C', 'W', 'R') ");
     */
    /*
     * sbQuery.setLength(0); sbQuery.append(" SELECT -1 * SUM(C507_ITEM_QTY) QTY ");
     * sbQuery.append(", -1 * SUM(C507_ITEM_QTY) IQTY");
     * sbQuery.append(", nvl(C507_ITEM_PRICE,0) PRICE "); sbQuery.append(", '-' CNUM "); sbQuery
     * .append(
     * " , DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C' , 'Return', 'W', 'Write-off','M','Missing') SFL "
     * ); sbQuery.append(" , C205_PART_NUMBER_ID PNUM ");
     * sbQuery.append(" , GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC "); sbQuery.append(" FROM ");
     * sbQuery.append(" T507_RETURNS_ITEM "); sbQuery.append(" WHERE "); sbQuery .append(
     * " C506_RMA_ID IN ( SELECT  C506_RMA_ID FROM T506_returns T506 WHERE T506.C506_RMA_ID = '");
     * sbQuery.append(strRAID); sbQuery.append("') AND C507_STATUS_FL IN ('C', 'W', 'R','M') ");
     * sbQuery .append(
     * " GROUP BY C507_ITEM_PRICE,C205_PART_NUMBER_ID,C507_STATUS_FL,C507_ITEM_PRICE  ORDER BY C205_PART_NUMBER_ID "
     * );
     */
    // Removed for Credit Memo changes
    /*
     * if (strAction.equals("PrintVersion")) { sbQuery.append(" AND C507_STATUS_FL = 'C'"); }
     */

    /*
     * sbQuery.append(" GROUP BY C507_ITEM_PRICE,C205_PART_NUMBER_ID,C507_STATUS_FL ");
     * sbQuery.append(" ORDER BY C205_PART_NUMBER_ID");
     */

    log.debug(" Query for RAITEMDETAILS is " + sbQuery.toString());
    // alReturn = dbCon.queryMultipleRecords(sbQuery.toString());

    // PMT-6740 - Query moved to prc to fetch RA item detail either from t507 or t502
    alReturn = loadRAItemDetail(strRAID);
    hmReturn.put("RAITEMDETAILS", alReturn);

    sbQuery.setLength(0);
    sbQuery.append(" SELECT -1 * SUM(C507_ITEM_QTY) QTY ");
    sbQuery.append(", -1 * SUM(C507_ITEM_QTY) IQTY");
    sbQuery.append(", nvl(C507_ITEM_PRICE,0) PRICE ");
    sbQuery.append(", '-' CNUM ");
    sbQuery
        .append(" , DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C' , 'Return', 'W', 'Write-off','M','Missing') SFL ");
    sbQuery.append(" , t507.C205_PART_NUMBER_ID PNUM ");
    sbQuery.append(" , GET_PARTNUM_DESC(t507.C205_PART_NUMBER_ID) PDESC ");
    sbQuery.append(" FROM ");
    sbQuery.append(" T507_RETURNS_ITEM t507 , t205_part_number t205");
    sbQuery.append(" WHERE ");
    sbQuery
        .append(" C506_RMA_ID IN ( SELECT  C506_RMA_ID FROM T506_returns T506 WHERE T506.C506_RMA_ID = '");
    sbQuery.append(strRAID);
    sbQuery.append("') AND C507_STATUS_FL IN ('C', 'W', 'R','M') ");
    sbQuery.append(" AND t507.c205_part_number_id  = t205.c205_part_number_id ");
    sbQuery
        .append(" AND t205.c202_project_id NOT IN (SELECT c906_rule_id FROM t906_rules WHERE c906_rule_grp_id = 'EXVND' AND c906_void_fl IS NULL )");
    sbQuery
        .append(" GROUP BY C507_ITEM_PRICE,t507.C205_PART_NUMBER_ID,C507_STATUS_FL,C507_ITEM_PRICE  ORDER BY t507.C205_PART_NUMBER_ID ");

    log.debug(" Query for RAINITMAILDECIDE is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("RAINITMAILDECIDE", alReturn);

    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT t506.c506_rma_id craid, t506a.c506a_attribute_value projid, GET_PROJECT_NAME (t506a.c506a_attribute_value) projnm ");
    sbQuery.append(" FROM t506_returns t506 , t506a_returns_attribute t506a ");
    sbQuery.append(" WHERE  t506.c506_rma_id = t506a.c506_rma_id ");
    sbQuery.append(" AND t506.c506_parent_rma_id = '" + strRAID + "'");
    sbQuery.append(" AND t506.c506_void_fl IS NULL ");
    sbQuery.append(" AND t506a.c506a_void_fl IS NULL ");
    log.debug(" Query for CHILDRA IDS is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    hmReturn.put("CHILDRADETAILS", alReturn);

    log.debug("Exit" + "RAITEMDETAILS :" + alReturn.size() + "RADETAILS :" + hmResult.size());
    return hmReturn;
  }

  /**
   * Load Ra item detail.
   * 
   * @param strRAID the strRaid
   * @return the ArrayList
   */
  public ArrayList loadRAItemDetail(String strRAID) throws AppError {
    ArrayList alRAItemDtl = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_return.gm_fch_credit_report", 2);
    gmDBManager.setString(1, strRAID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();

    alRAItemDtl =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    log.debug("RAITEMDETAILS" + alRAItemDtl);
    return alRAItemDtl;
  }
}
