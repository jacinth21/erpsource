/**
 * 
 */
package com.globus.operations.returns.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmReturnsBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing


  public GmReturnsBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmReturnsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * initiate return for item consignment -
   * 
   * 
   * @exception AppError
   */
  public void initiateReturnItemConsign(GmDBManager gmDBManager, String strRequestId,
      String strRaFlag, String strUserId) {

    gmDBManager.setPrepareString("gm_pkg_op_return_txn.gm_sav_return_fl_for_req", 3);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strRaFlag);
    gmDBManager.setString(3, strUserId);

    gmDBManager.execute();
  }

  public String fetchCNPackslipComment(String strConsignId) {
    String strComments = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_return_txn.gm_fch_CN_packslip_RAcomment", 2);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);

    gmDBManager.execute();
    strComments = GmCommonClass.parseNull(gmDBManager.getString(2));

    return strComments;
  }

  /**
   * @Description: Method used to fetch RA id by consignment id from t520_request table
   * @Param : String strConsignId
   * @Return : String
   * @Author : Mahavishnu
   **/
  public String fetchRAIdByConsignId(String strConsignId) {
    String strRAId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_return_rpt.gm_fch_RA_id_by_CN", 2);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);

    gmDBManager.execute();
    strRAId = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strRAId;
  }

  /**
   * @Description: Method used to Generate RA initiate mail, RA Flag is Y
   * @Param : String strRefId, String strSource
   * @Return : void
   * @Author : Mahavishnu
   **/
  public void sendRAEmail(String strRefId, String strSource, String userId) {

    // checking transaction is "50181"-consignment
    if (strSource.equals("50181")) {
      String strRAId = fetchRAIdByConsignId(strRefId);
      log.debug("strRAId----" + strRAId);
      // below method to send RA email notification by passing RA ID
      sendRAEmail(strRAId);
    }

  }


  /**
   * @Description: Method used to Generate RA initiate mail by passing RA ID
   * @Param : String strRAId
   * @Return : void
   * @Author : Mahavishnu
   **/
  public void sendRAEmail(String strRAId) {
    // If RA ID presents for transaction we are sending RA Initialize email notification for user
    if (!strRAId.equals("")) {
      GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());

      GmResourceBundleBean rbPaperworkBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      GmResourceBundleBean gmResourceBundleBean =
    		  GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

      String strClassName =
          GmCommonClass.parseNull(rbPaperworkBundleBean.getProperty("RA.FETCH_CLASS"));

      GmBBAProcessReturnsProcessInterface returnSummaryClient =
          GmBBAReturnSummaryPicSlip.getInstance(strClassName);

      HashMap hmRADetail = returnSummaryClient.loadCreditReport(strRAId, "PRINT");

      String strToEmailId =
          gmResourceBundleBean.getProperty("GmRAInitialEmail" + "." + GmEmailProperties.TO);
      ArrayList alRAMailDecide =
          GmCommonClass.parseNullArrayList((ArrayList) hmRADetail.get("RAINITMAILDECIDE"));

      if (alRAMailDecide.size() > 0 && strToEmailId != null) {
        hmRADetail.put("EMAILFILENM", "GmRAInitialEmail");
        hmRADetail.put("JASPERNAME", "/GmRAStatusEmail.jasper");
        hmRADetail.put("ISCREDIT", "NOTCREDIT");
        hmRADetail.put("PDFL", "TRUE");
        gmProcessReturnsBean.sendRAStatusEmail(hmRADetail);
      }
    }
  }

  /**
   * validateCopyTransactions - To validate copy transaction type,verify flag,void flag
   * 
   * 
   * @param strSourceTransId, strTgtTransType
   * @author Mahavishnu
   * @exception AppError
   */
  public String validateCopyTransactions(String strSourceTransId, String strTgtTransType)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_op_fch_trans_detail.gm_validate_trans_type", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strSourceTransId);
    gmDBManager.setString(2, strTgtTransType);
    gmDBManager.execute();
    String strErrorMessage = GmCommonClass.parseNull(gmDBManager.getString(3));
    log.debug("strErrorMessage=====" + strErrorMessage);
    gmDBManager.commit();
    return strErrorMessage;
  }

  /**
   * loadPartInfoFromTransId - To loads a transaction deatails
   * 
   * 
   * @param strSrcTransIDs,strTgtTransType
   * @author Mahavishnu
   * @exception AppError
   */
  public HashMap loadPartInfoFromTransId(String strSrcTransIDs, String strTgtTransType)
      throws AppError {
    ArrayList alList = new ArrayList();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    StringTokenizer strTok = new StringTokenizer(strSrcTransIDs, ",");
    String strCompanyID = getCompId();
    String strTransParts = "";
    String strSrcIds = "";


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    while (strTok.hasMoreTokens()) {
      strSrcTransIDs = strTok.nextToken();
      StringBuffer sbQuery = new StringBuffer();

      sbQuery
          .append("SELECT T205.C205_PART_NUMBER_ID ID ,    T2052.C2052_EQUITY_PRICE PRICE,      T205.C205_PART_NUM_DESC PDESC ,");

      // --checks transaction type 4116 - Quarantine To Shelf and 400083- Quarantine to Repack and 400085- Quarantine to Retruns
      if (strTgtTransType.equals("4116") || strTgtTransType.equals("400083") || strTgtTransType.equals("400085")) {

        sbQuery
            .append(" GET_QTY_IN_QUARANTINE(T205.C205_PART_NUMBER_ID) QSTOCK, 0  REPACK_STOCK,   ");

      } else if (strTgtTransType.equals("400063")) {
        // --checks transaction type 400063 - Repack to Shelf

        sbQuery.append(" 0 QSTOCK,GET_QTY_IN_PACKAGING(T205.C205_PART_NUMBER_ID) REPACK_STOCK, ");

      }else if (strTgtTransType.equals("400086")) {
          // --checks transaction type 400086 -  Shelf to Returns

          sbQuery.append("GET_QTY_IN_STOCK(T205.C205_PART_NUMBER_ID) STOCK, ");

        }
      // -- Needed available quarantine qty and package qty, so other inhouse quantity manually
      // mentioned as 0
      sbQuery.append(" 0 BLK_STOCK,  0 BSTOCK,  0 RTN_STOCK, ");

      sbQuery
          .append(" t505.c505_control_number CTN_NUM, t505.c504_consignment_id TRANSID,t505.C505_ITEM_QTY QTY  ");

      sbQuery
          .append("    FROM T205_PART_NUMBER T205 , T2052_PART_PRICE_MAPPING T2052,   t505_item_consignment t505 ");

      sbQuery
          .append(" WHERE T205.C205_PART_NUMBER_ID = t505.c205_part_number_id  AND t505.c504_consignment_id = '"
              + strSrcTransIDs + "'");

      sbQuery.append(" AND T205.C205_PART_NUMBER_ID   = T2052.C205_PART_NUMBER_ID(+) ");

      sbQuery.append(" AND t505.c505_void_fl IS NULL ");

      sbQuery.append(" AND T2052.C1900_COMPANY_ID(+) = " + strCompanyID
          + "    AND t2052.c2052_void_fl(+)   IS NULL ");

      sbQuery.append(" UNION ");

      sbQuery
          .append(" SELECT T205.C205_PART_NUMBER_ID ID ,  T2052.C2052_EQUITY_PRICE PRICE,  T205.C205_PART_NUM_DESC PDESC ,");

      // --checks transaction type 4116 - Quarantine To Shelf and 400083- Quarantine to Repack and 400085- Quarantine to Retruns
      if (strTgtTransType.equals("4116") || strTgtTransType.equals("400083") || strTgtTransType.equals("400085")) {

        sbQuery
            .append(" GET_QTY_IN_QUARANTINE(T205.C205_PART_NUMBER_ID) QSTOCK, 0  REPACK_STOCK,   ");

      } else if (strTgtTransType.equals("400063")) {
        // --checks transaction type 4116 - Repack to Shelf
        sbQuery.append(" 0 QSTOCK,GET_QTY_IN_PACKAGING(T205.C205_PART_NUMBER_ID) REPACK_STOCK, ");

      }
      else if (strTgtTransType.equals("400086")) {
          // --checks transaction type 400086 -  Shelf to Returns

          sbQuery.append("GET_QTY_IN_STOCK(T205.C205_PART_NUMBER_ID) STOCK, ");

        }
      // -- Needed available quarantine qty and package qty, so other inhouse quantity manually
      // mentioned as 0
      sbQuery.append(" 0 BLK_STOCK,  0 BSTOCK, 0 RTN_STOCK, ");

      sbQuery
          .append(" t413.c413_control_number   CTN_NUM ,t413.c412_inhouse_trans_id TRANSID,t413.C413_ITEM_QTY QTY  ");

      sbQuery
          .append(" FROM T205_PART_NUMBER T205 ,T2052_PART_PRICE_MAPPING T2052,t413_inhouse_trans_items t413 ");

      sbQuery
          .append(" WHERE T205.C205_PART_NUMBER_ID = t413.c205_part_number_id AND t413.c412_inhouse_trans_id ='"
              + strSrcTransIDs + "'");

      sbQuery.append("AND T205.C205_PART_NUMBER_ID   = T2052.C205_PART_NUMBER_ID(+)");

      sbQuery.append("AND t413.C413_VOID_FL IS NULL ");

      sbQuery.append("AND T2052.C1900_COMPANY_ID(+) =  " + strCompanyID
          + "   AND t2052.c2052_void_fl(+)   IS NULL");

      log.debug(" Query for Loading transaction cart " + sbQuery.toString());

      alList =
          GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

      // -- we are getting multiple rows as result. In jsp we evaluate values by hashmap, so we add
      // arraylist values to hash map by key values.
      strSrcIds += strSrcTransIDs + ",";
      if (alList != null) {// --checks alList is not null
        for (int i = 0; i < alList.size(); i++) {

          hmTemp = (HashMap) alList.get(i);

          // -- Formation of key - parnumber-transactionid-controlnumber-qty.
          // -- to avoid repeating partnumber key in hashmap values
          hmResult.put(hmTemp.get("ID") + "-" + strSrcTransIDs + "-" + hmTemp.get("CTN_NUM") + "-"
              + hmTemp.get("QTY"), alList.get(i));

          // -- we are fetching records by hashmap keys, we formed key in
          // parnumber-transactionid-controlnumber format
          // -- to fetch hashmap values from jsp by keys - we formed hashmap keys as string and put
          // in hashmap to get values in jsp.
          strTransParts +=
              (String) hmTemp.get("ID") + "-" + strSrcTransIDs + "-" + hmTemp.get("CTN_NUM") + "-"
                  + hmTemp.get("QTY") + ",";



        }
      }

    }
    gmDBManager.close();
    hmResult.put("TRANSPARTNUMS", strTransParts.substring(0, strTransParts.length() - 1));
    hmResult.put("TRANSIDS", strSrcIds.substring(0, strSrcIds.length() - 1));

    return hmResult;
  } // End of loadPartInfoFromTransId

  /**
   * saveItemConsignByTrans - To save copied transaction to t414_transaction_log table
   * 
   * 
   * @param strConsignId,strSrcTransIDs,strUserId
   * @author Mahavishnu
   * @exception AppError
   */
  public void saveItemConsignByTrans(String strConsignId, String strSrcTransIDs, String strUserId)
      throws AppError {

    strSrcTransIDs = strSrcTransIDs.concat(",");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_op_fch_trans_detail.gm_save_trans_details", 3);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strSrcTransIDs);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
    gmDBManager.close();


  }

}
