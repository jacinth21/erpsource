package com.globus.operations.returns.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmReturnReportBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmReturnReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmReturnReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  // Instantiating the Logger
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger

  /**
   * loadReturnSummary - obvious
   * 
   * @param HashMap hmParam
   * @return List
   * @exception AppError
   **/

  public List loadReturnSummary(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    List lResult = null;
    String strDistId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
    String strType = GmCommonClass.parseZero((String) hmParam.get("TYPE"));
    String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
    String strRAID = GmCommonClass.parseNull((String) hmParam.get("RAID"));
    String strParentRAID = GmCommonClass.parseNull((String) hmParam.get("PARENTRAID"));
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strElapse = GmCommonClass.parseNull((String) hmParam.get("ELAPSE"));
    String strComparison =
        (String) hmParam.get("ELAPSECOMP") == null ? "90190" : (String) hmParam.get("ELAPSECOMP");
    String strReason = GmCommonClass.parseNull((String) hmParam.get("REASON"));
    String strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
    String strFrmDate = GmCommonClass.parseNull((String) hmParam.get("FRMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strRepId = GmCommonClass.parseZero((String) hmParam.get("REPID"));
    String strAccId = GmCommonClass.parseZero((String) hmParam.get("ACCOUNTID"));
    String strDateField = "";
    String strCompDFmt = getCompDateFmt();

    HashMap hmTempComp = new HashMap();
    hmTempComp.put("90190", ">");
    hmTempComp.put("90191", "<");
    hmTempComp.put("90192", "=");
    hmTempComp.put("90193", ">=");
    hmTempComp.put("90194", "<=");

    HashMap hmTempStatus = new HashMap();
    hmTempStatus.put("50520", "0");
    hmTempStatus.put("50521", "1");
    hmTempStatus.put("50522", "2");

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C506_RMA_ID RAID, C506_COMMENTS COMMENTS, C506_CREATED_DATE CDATE, ");
    sbQuery
        .append(" GET_USER_NAME(C506_CREATED_BY) PER, GET_CODE_NAME(C506_TYPE) TYPE, C506_TYPE TYPEID, ");
    sbQuery.append(" GET_CODE_NAME(C506_REASON) REASON, C506_EXPECTED_DATE EDATE,");
    sbQuery.append(" DECODE(C701_DISTRIBUTOR_ID, NULL,C704_ACCOUNT_ID,C701_DISTRIBUTOR_ID) DID, ");
    sbQuery
        .append(" DECODE(C701_DISTRIBUTOR_ID,NULL,GET_ACCOUNT_NAME(C704_ACCOUNT_ID), GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID)) DNAME, ");
    sbQuery.append(" GET_REP_NAME(C703_SALES_REP_ID) rname, ");
    sbQuery
        .append(" GET_USER_NAME(GM_PKG_OP_IHLOANER_ITEM_TXN.GET_IH_LOANTOID(C506_REF_ID))  LOANTONAME, ");
    sbQuery.append(" GM_PKG_OP_IHLOANER_ITEM_RPT.GET_IHLN_EVENT_NAME(C506_REF_ID)  EVENTNAME, ");
    sbQuery.append(" C704_ACCOUNT_ID ACCID, GET_ACCOUNT_NAME(C704_ACCOUNT_ID) ANAME, ");
    sbQuery.append(" C501_ORDER_ID ORDID, C501_REPROCESS_DATE RPDATE,");
    sbQuery
        .append(" DECODE(C506_STATUS_FL,0,'-',1,'Y') STATUS_FL, NVL(GET_TOTAL_RETURN_AMT(C506_RMA_ID),0) RAMT, ");
    sbQuery.append(" C506_RETURN_DATE RDATE, GET_SET_NAME(C207_SET_ID) SNAME ");
    sbQuery
        .append(" ,C501_ORDER_ID PARENTOID, DECODE(C506_STATUS_FL,2,0,TRUNC(CURRENT_DATE) - C506_EXPECTED_DATE) ELPDAYS  ");
    sbQuery
        .append(" , GET_LOG_FLAG (C506_RMA_ID , 1216  )  LOG , '' PLINE, C506_PARENT_RMA_ID PARENTRMAID  ");
    sbQuery
        .append(" , DECODE(C506_LAST_UPDATED_DATE,NULL,C506_CREATED_DATE,C506_LAST_UPDATED_DATE) LUDATE ");
    sbQuery.append(" FROM T506_RETURNS ");
    sbQuery.append(" WHERE C506_VOID_FL IS NULL ");
    sbQuery.append(" AND C5040_PLANT_ID = " + getCompPlantId());

    if (!strDistId.equals("0") && !strDistId.equals("01")) {
      sbQuery.append(" AND C701_DISTRIBUTOR_ID = '");
      sbQuery.append(strDistId);
      sbQuery.append("' ");
    } else if (strDistId.equals("01")) {
      sbQuery.append(" AND C704_ACCOUNT_ID = '01' ");
      sbQuery.append(" AND C101_USER_ID IS NOT NULL ");
    }

    if (!strAccId.equals("0")) {
      sbQuery.append(" AND C704_ACCOUNT_ID = '");
      sbQuery.append(strAccId);
      sbQuery.append("' ");
    }

    if (!strType.equals("0")) {
      sbQuery.append(" AND C506_TYPE = ");
      sbQuery.append(strType);
    }

    if (!strStatus.equals("0")) {
      sbQuery.append(" AND C506_STATUS_FL = ");
      sbQuery.append(hmTempStatus.get(strStatus));
    }

    if (!strRAID.equals("")) {
      if (strRAID.startsWith("G") || strRAID.indexOf("-") != -1) {
        sbQuery.append(" AND C506_RMA_ID = '");
        sbQuery.append(strRAID);
        sbQuery.append("' ");
      } else {
        sbQuery.append(" AND (C506_RMA_ID = 'GM-RA-");
        sbQuery.append(strRAID);
        sbQuery.append("' ");
        sbQuery.append(" OR C506_RMA_ID = 'GM-CRA-");
        sbQuery.append(strRAID);
        sbQuery.append("') ");
      }
    }

    if (!strParentRAID.equals("")) {
      if (strParentRAID.startsWith("G") || strRAID.indexOf("-") != -1) {
        sbQuery.append(" AND C506_PARENT_RMA_ID = '");
        sbQuery.append(strParentRAID);
        sbQuery.append("' ");
      } else {
        sbQuery.append(" AND C506_PARENT_RMA_ID = 'GM-RA-");
        sbQuery.append(strParentRAID);
        sbQuery.append("' ");
      }
    }
    if (!strRefID.equals("")) {
    	strRefID=	Character.isDigit(strRefID.charAt(0))?"-"+strRefID:strRefID;
    	
    	sbQuery.append(" AND C506_REF_ID LIKE '%");
        sbQuery.append(strRefID);
        sbQuery.append("' ");
      }

    if (!strElapse.equals("")) {
      sbQuery.append(" AND DECODE(C506_STATUS_FL,2,0,TRUNC(CURRENT_DATE) - C506_EXPECTED_DATE) ");
      sbQuery.append(hmTempComp.get(strComparison));
      sbQuery.append(strElapse);
    }

    if (!strReason.equals("0")) {
      sbQuery.append(" AND C506_REASON='");
      sbQuery.append(strReason);
      sbQuery.append("' ");
    }

    if (!strDateType.equals("0")) {
      if (strDateType.equals("IN")) {
        strDateField = "C506_CREATED_DATE";
      } else if (strDateType.equals("RT")) {
        strDateField = "C506_RETURN_DATE";
      } else if (strDateType.equals("CR")) {
        strDateField = "C506_CREDIT_DATE";
      } else if (strDateType.equals("RP")) {
        strDateField = "C501_REPROCESS_DATE";
      }

      sbQuery.append(" AND trunc( ");
      sbQuery.append(strDateField);
      sbQuery.append(") >= to_date('");
      sbQuery.append(strFrmDate);
      sbQuery.append("','" + strCompDFmt + "'");
      sbQuery.append(" ) ");
      sbQuery.append(" AND trunc( ");
      sbQuery.append(strDateField);
      sbQuery.append(") <= to_date('");
      sbQuery.append(strToDate);
      sbQuery.append("','" + strCompDFmt + "'");
      sbQuery.append(" ) ");
    }
    if (!strSetId.equals("")) {
      sbQuery.append(" AND C207_SET_ID = '");
      sbQuery.append(strSetId);
      sbQuery.append("' ");
    }

    if (!strPartNum.equals("")) {
      sbQuery.append(" AND C506_RMA_ID IN (");
      sbQuery.append(" SELECT C506_RMA_ID FROM T507_RETURNS_ITEM ");
      sbQuery.append(" WHERE C205_PART_NUMBER_ID = '");
      sbQuery.append(strPartNum);
      sbQuery.append("') ");
    }
    if (!strRepId.equals("0")) {
      sbQuery.append(" AND C703_SALES_REP_ID = ");
      sbQuery.append(strRepId);
    }

    sbQuery.append(" ORDER BY C506_CREATED_DATE DESC ");

    log.debug(" Query value is  " + sbQuery.toString());

    lResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return lResult;
  } // End of loadReturnSummary

  public String getXmlGridData(ArrayList alParam, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.returns.GmReturnSummaryList", strSessCompanyLocale));
    templateUtil.setTemplateName("GmReturnSummary.vm");
    templateUtil.setTemplateSubDir("/operations/returns/templates");
    return templateUtil.generateOutput();
  }

  /**
   * viewReturnSetDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadReturnHeaderInfo(String strRAId) throws AppError {
    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT T506.C506_RMA_ID RAID, T506.C506_PARENT_RMA_ID PRAID, T506.C506_COMMENTS COMMENTS, T506.C207_SET_ID SETID, GET_SET_NAME(T506.C207_SET_ID) SNAME, ");
    sbQuery
        .append(" T506.C701_DISTRIBUTOR_ID DISTID, GET_DISTRIBUTOR_NAME(T506.C701_DISTRIBUTOR_ID) DNAME, GET_ACCOUNT_NAME(T506.C704_ACCOUNT_ID) ANAME, ");
    sbQuery
        .append(" T506.C704_ACCOUNT_ID ACCID ,GET_CODE_NAME(T506.C506_TYPE) TYPE, GET_CODE_NAME(T506.C506_REASON) REASON, ");
    sbQuery
        .append(" T506.C506_RETURN_DATE RET_DATE,T506.C506_RETURN_DATE RETDATE, T506.C506_STATUS_FL SFL, GET_LOG_FLAG (T506.C506_RMA_ID , 1216)  LOG, GET_LOG_FLAG (T506.C506_PARENT_RMA_ID , 1216) PLOG,");
    sbQuery.append(" T506.C506_CREATED_DATE CDATE, get_user_name(T506.C506_CREATED_BY) CUSER, ");
    sbQuery
        .append(" T506.C506_EXPECTED_DATE EDATE,CURRENT_DATE SYSDT, T506.C501_ORDER_ID ORDID, T506.C506_CREDIT_DATE CRDATE ");
    sbQuery.append(" ,T506.C506_TYPE TYPEID, T506.C506_REASON REASONID, T506.C506_REF_ID refid ");
    sbQuery.append(" ,T506.C506_CREDIT_DATE CREDITEMAILDATE ");
    sbQuery
        .append(" , GET_CODE_NAME(GM_PKG_OP_IHLOANER_ITEM_TXN.GET_IH_LOANTO(T506.C506_REF_ID)) LOANTO, GET_USER_NAME(GM_PKG_OP_IHLOANER_ITEM_TXN.GET_IH_LOANTOID(T506.C506_REF_ID)) LOANTOID ");
    sbQuery
        .append(" ,NVL(GET_CS_SHIP_EMAIL (4121, DECODE(NVL(T501.C501_ORDER_ID,'-9999'),'-9999',T506.C703_SALES_REP_ID,T501.C703_SALES_REP_ID)), get_rule_value ('DEFAULTSHIPEMAIL', 'EMAIL')) EMAILADD ");
    sbQuery
        .append(" ,GET_RULE_VALUE (GET_DISTRIBUTOR_ID(DECODE(NVL(T501.C501_ORDER_ID,'-9999'),'-9999',T506.C703_SALES_REP_ID,T501.C703_SALES_REP_ID)), 'CC-LIST-RETURNS') CCLIST");
    sbQuery
        .append(" ,get_rep_name(DECODE(NVL(T501.C501_ORDER_ID,'-9999'),'-9999',T506.C703_SALES_REP_ID,T501.C703_SALES_REP_ID)) repname");
    sbQuery
        .append(" ,T506.c506_ASSOCIATED_RMA_id ASSOCIATED_RMA_id,gm_pkg_op_return.get_associated_rep_name(T506.c506_ASSOCIATED_RMA_id) associated_rep_name,gm_pkg_op_return.get_associated_rep_email(T506.c506_ASSOCIATED_RMA_id) associated_rep_email");
    sbQuery.append(" , GM_PKG_OP_RETURN.get_returned_parts(T506.C506_RMA_ID) rapartnums"); // get
                                                                                           // the
                                                                                           // part
                                                                                           // numbers
                                                                                           // from
                                                                                           // the
                                                                                           // RA
    sbQuery
        .append(",  GET_ACCOUNT_ATTRB_VALUE (t506.c704_account_id, 101191) valid_add_fl, get_rule_value('TAX_DATE', 'TAX_INVOICE') tax_start_date ");
    sbQuery
        .append(", gm_pkg_ac_sales_tax.get_tax_country_fl(t506.c704_account_id) tax_country_fl ");
    sbQuery.append(", t501.c503_invoice_id  invid, t507.errcnt errcnt,get_rule_value('" + getCompPlantId() + "','SPLIT_RA') SPLITFL"); 
    sbQuery.append(" FROM T506_RETURNS T506, T501_ORDER T501");
  
    sbQuery.append(" , (SELECT COUNT(1) errcnt FROM T507_RETURNS_ITEM  WHERE c506_rma_id = '");
    sbQuery.append(strRAId);
    sbQuery
        .append("' AND c507_status_fl <> 'Q' AND c507_error_dtls IS NOT NULL AND c507_control_number IS NOT NULL) t507");
    sbQuery.append(" WHERE T501.C501_ORDER_ID (+) = T506.C501_ORDER_ID AND T506.C506_RMA_ID = '");
    sbQuery.append(strRAId);
    sbQuery.append("'");
    sbQuery.append(" AND T501.C501_VOID_FL IS NULL AND T506.C506_VOID_FL IS NULL ");
    if(getCompPlantId().equals("3003")) {
    	 sbQuery.append(" AND T506.C5040_PLANT_ID in ( SELECT c906_rule_value FROM t906_rules WHERE c906_rule_id = '"+getCompPlantId()+"'  AND c906_rule_grp_id = 'RTN_CRDT' AND C1900_COMPANY_ID IS NULL AND C906_VOID_FL IS NULL) ");
    	
    } else {
    	sbQuery.append(" AND T506.C5040_PLANT_ID = '" + getCompPlantId() + "'"); //nedc
    }
    log.debug(" Query to Fetch Details based on RAID in Customer Bean is " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
    
    log.debug("Exit");
    return hmReturn;
  } // End of loadReturnHeaderInfo



  /**
   * loadReturnsDetails - This method will return values for an RMA
   * 
   * @param String strRAId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadReturnsAccpDetails(String strRAId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    HashMap hmTemp = new HashMap();
    String strSetId = "";



    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT NVL(C207_SET_ID,0) SETID ");
    sbQuery.append(" FROM T506_RETURNS ");
    sbQuery.append(" WHERE C506_RMA_ID = '");
    sbQuery.append(strRAId);
    sbQuery.append("' AND C506_VOID_FL IS NULL ");
    log.debug("Query to check Set ID is " + sbQuery.toString());

    hmTemp = gmDBManager.querySingleRecord(sbQuery.toString());
    strSetId = (String) hmTemp.get("SETID");
    hmReturn.put("SETID", strSetId);

    return hmReturn;
  } // End of loadReturnsDetails


  /**
   * loadReturnsDetails - obvious
   * 
   * @param String strRAID
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadReturnsDetails(String strRAId) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT C506_RMA_ID RAID, C506_COMMENTS COMMENTS, GET_SET_NAME(C207_SET_ID) SNAME, ");
    sbQuery
        .append(" GET_DISTRIBUTOR_NAME(C701_DISTRIBUTOR_ID) DNAME, GET_ACCOUNT_NAME(C704_ACCOUNT_ID) ANAME, ");
    sbQuery.append(" GET_CODE_NAME(C506_TYPE) TYPE, GET_CODE_NAME(C506_REASON) REASON, ");
    sbQuery.append(" C506_RETURN_DATE RETDATE, C506_STATUS_FL SFL, ");
    sbQuery.append(" C506_CREATED_DATE CDATE, get_user_name(C506_CREATED_BY) CUSER, ");
    sbQuery.append(" C506_EXPECTED_DATE EDATE, C501_ORDER_ID ORDID, C506_CREDIT_DATE CRDATE ");
    sbQuery
        .append(" ,C506_TYPE TYPEID, C506_REASON REASONID , GET_LOG_FLAG (C506_RMA_ID , 1216)  LOG ");
    sbQuery.append(" FROM T506_RETURNS ");
    sbQuery.append(" WHERE C506_RMA_ID = '");
    sbQuery.append(strRAId);
    sbQuery.append("'");
    sbQuery.append(" AND C506_VOID_FL IS NULL ");
    sbQuery.append(" AND C5040_PLANT_ID = '" + getCompPlantId() + "'");
    log.debug(" Query to loadReturnsDetails is " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    if (hmReturn.isEmpty()) {
      throw new AppError("", "20677");
    }

    return hmReturn;
  } // End of loadReturnsDetails


  /**
   * loadReturnDetail - obvious
   * 
   * @param HashMap hmParam
   * @return RowSetDynaClass
   * @exception AppError
   **/
  public HashMap loadReturnsDetails(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rsHeaderResultSet = null;
    RowSetDynaClass rsFooterResultSet = null;

    String strRAID = GmCommonClass.parseNull((String) hmParam.get("RAID"));
    String strType = GmCommonClass.parseZero((String) hmParam.get("TYPE"));
    String strProdFamily = GmCommonClass.parseZero((String) hmParam.get("PRODFAMILY"));
    String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMFORMATTED"));

    HashMap hmTempStatus = new HashMap();
    hmTempStatus.put("50520", "0");
    hmTempStatus.put("50521", "1");
    hmTempStatus.put("50522", "2");

    StringBuffer sbHeaderQuery = new StringBuffer();

    sbHeaderQuery.append("SELECT T205.C205_PART_NUMBER_ID PNUM, T205.C205_PART_NUM_DESC PDESC ");
    sbHeaderQuery.append(",SUM(DECODE(C507_STATUS_FL,'Q',  0, C507_ITEM_QTY) )  QTY_PCD ");
    sbHeaderQuery.append(",SUM(DECODE(C507_STATUS_FL,'C',   C507_ITEM_QTY ,0))  QTY_RTN ");
    sbHeaderQuery
        .append(",NVL(SUM(DECODE(C507_STATUS_FL,'R',  DECODE(TRIM(T507.C507_CONTROL_NUMBER),NULL,0 ,C507_ITEM_QTY) )),0) QTY_PCRD ");
    sbHeaderQuery
        .append(",NVL(SUM(DECODE(C507_STATUS_FL,'R',  DECODE(TRIM(T507.C507_CONTROL_NUMBER),NULL,C507_ITEM_QTY,0 ) )),0) QTY_PRTN ");
    sbHeaderQuery.append(",SUM(DECODE(C507_STATUS_FL,'W',   C507_ITEM_QTY ,0)) QTY_WRF ");
    sbHeaderQuery.append(",NVL(T205.C205_EQUITY_PRICE,0) EPRICE ");
    sbHeaderQuery
        .append(",NVL(SUM(DECODE(C507_STATUS_FL,'R',  DECODE(TRIM(T507.C507_CONTROL_NUMBER),NULL,C507_ITEM_QTY,0 ) )) * T205.C205_EQUITY_PRICE,0) PENDRETVAL ");
    sbHeaderQuery.append(", GET_OP_FCH_RA_EXCESS_QTY('");
    sbHeaderQuery.append(strRAID);
    sbHeaderQuery.append("',T205.C205_PART_NUMBER_ID) EXCESS ");
    sbHeaderQuery
        .append(" FROM T506_RETURNS T506, T507_RETURNS_ITEM T507 , T205_PART_NUMBER T205,t2023_part_company_mapping t2023 ");
    sbHeaderQuery.append(" WHERE C506_VOID_FL IS NULL  AND (T506.C506_RMA_ID = '");
    sbHeaderQuery.append(strRAID);
    sbHeaderQuery.append("' OR T506.C506_PARENT_RMA_ID = '");
    sbHeaderQuery.append(strRAID);
    sbHeaderQuery.append("') AND T506.C506_RMA_ID = T507.C506_RMA_ID  ");
    sbHeaderQuery.append(" AND T205.C205_PART_NUMBER_ID = T507.C205_PART_NUMBER_ID  ");
    sbHeaderQuery.append(" AND T205.C205_PART_NUMBER_ID = t2023.C205_PART_NUMBER_ID  ");
    sbHeaderQuery.append(" AND t2023.c1900_company_id = '" + getCompId() + "'  ");
    sbHeaderQuery.append(" AND T506.C5040_PLANT_ID = '" + getCompPlantId() + "' ");
    log.debug(" type is " + hmTempStatus.get(strType));

    if (!strType.equals("0")) {
      sbHeaderQuery.append(" AND T506.C506_STATUS_FL = ");
      sbHeaderQuery.append(hmTempStatus.get(strType));
    }

    if (!strProdFamily.equals("0")) {
      sbHeaderQuery.append(" AND T205.C205_PRODUCT_FAMILY = ");
      sbHeaderQuery.append(strProdFamily);
    }

    if (!strProjectId.equals("0")) {
      sbHeaderQuery.append(" AND T205.C202_PROJECT_ID ='");
      sbHeaderQuery.append(strProjectId);
      sbHeaderQuery.append("' ");
    }

    if (!strPartNum.equals("")) {
      sbHeaderQuery.append(" AND T205.C205_PART_NUMBER_ID IN('");
      sbHeaderQuery.append(strPartNum);
      sbHeaderQuery.append("') ");
    }

    sbHeaderQuery
        .append(" GROUP BY  T205.C205_PART_NUMBER_ID, T205.C205_PART_NUM_DESC, T205.C205_EQUITY_PRICE ");
    log.debug(" Header Query loadReturnDetail is " + sbHeaderQuery.toString());
    rsHeaderResultSet = gmDBManager.QueryDisplayTagRecordset(sbHeaderQuery.toString());

    StringBuffer sbFooterQuery = new StringBuffer();
    sbFooterQuery
        .append(" SELECT T506.C506_RMA_ID RID, T506.C207_SET_ID SID, GET_SET_NAME(T506.C207_SET_ID) SNM ");
    sbFooterQuery
        .append(" , DECODE (T506.C506_STATUS_FL, 0, 'INITIATED', 1, 'PENDING CREDIT','CREDITED') STATUS ");
    sbFooterQuery
        .append(" , GET_USER_NAME(T506.C506_CREATED_BY) INIT_BY, C506_CREATED_DATE INIT_DT ");
    sbFooterQuery.append(" FROM T506_RETURNS T506 ");
    sbFooterQuery.append(" WHERE C506_VOID_FL IS NULL ");
    sbFooterQuery.append(" AND T506.C506_PARENT_RMA_ID = '");
    sbFooterQuery.append(strRAID);
    sbFooterQuery.append("' ");

    log.debug(" Footer Query loadReturnDetail is " + sbFooterQuery.toString());
    rsFooterResultSet = gmDBManager.QueryDisplayTagRecordset(sbFooterQuery.toString());

    hmReturn.put("HEADERRESULT", rsHeaderResultSet);
    hmReturn.put("FOOTERRESULT", rsFooterResultSet);

    return hmReturn;
  } // End of loadReturnDetail
}
