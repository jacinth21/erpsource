package com.globus.operations.returns.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmExcessReturnsBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  GmCommonClass gmCommon = new GmCommonClass();

  public GmExcessReturnsBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmExcessReturnsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public RowSetDynaClass loadExcessReturnSummary(HashMap hmParam) {
    RowSetDynaClass rdResultSet = null;

    String strDistributorId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
    String strStatus =
        GmCommonClass.parseNull((String) hmParam.get("STATUS")).equals("") ? "90177"
            : (String) hmParam.get("STATUS");
    String strRAID = GmCommonClass.parseNull((String) hmParam.get("RAID"));
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));

    HashMap hmTempStatus = new HashMap();
    hmTempStatus.put("90175", "0");
    hmTempStatus.put("90176", "1");
    hmTempStatus.put("90177", "2");

  //PC-4148 Remove Try Catch for Email Exception (TSK-23802)
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT T504A.C504_CONSIGNMENT_ID CONSIGNID , GET_DISTRIBUTOR_NAME(T504.C701_DISTRIBUTOR_ID) DISTNAME ");
      sbQuery.append(" , T504A.C506_RMA_ID RMAID, TO_CHAR(T506.C506_RETURN_DATE,'"
          + getCompDateFmt() + "') RETURNDATE ");
      sbQuery.append(" , GET_USER_NAME(T504A.C504A_CREATED_BY) CREDITEDBY ");
      sbQuery.append(" , GET_CODE_NAME(T504A.C901_REASON) REASON ");
      sbQuery.append(" , T921.C920_TRANSFER_ID TID ");
      sbQuery
          .append(" , GET_LOG_FLAG (T504A.C506_RMA_ID , 1216 )  RLOG, GET_LOG_FLAG (T504A.C504_CONSIGNMENT_ID , 1221)  CLOG ");
      sbQuery
          .append(" FROM T504A_CONSIGNMENT_EXCESS T504A, T504_CONSIGNMENT T504, T506_RETURNS T506 , T921_TRANSFER_DETAIL T921 ");
      sbQuery.append(" WHERE T504.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND T504A.C504_CONSIGNMENT_ID = T921.C921_REF_ID(+)");

      if (!strDistributorId.equals("0")) {
        sbQuery.append(" AND T504.C701_DISTRIBUTOR_ID ='");
        sbQuery.append(strDistributorId);
        sbQuery.append("' ");
      }

      if (!strStatus.equals("90177")) // 90177 is ALL
      {
    	sbQuery.append(" AND T504A.C504A_STATUS_FL = '");
        sbQuery.append(hmTempStatus.get(strStatus));
        sbQuery.append("'");
      }

      if (!strRAID.equals("")) {
        sbQuery.append(" AND T504A.C506_RMA_ID ='");
        sbQuery.append(strRAID);
        sbQuery.append("' ");
      }

      if (!strConsignId.equals("")) {
        sbQuery.append(" AND T504A.C504_CONSIGNMENT_ID ='");
        sbQuery.append(strConsignId);
        sbQuery.append("' ");
      }

      sbQuery.append(" AND T504A.C504A_VOID_FL IS NULL ");
      sbQuery.append(" AND T504.C504_CONSIGNMENT_ID = T504A.C504_CONSIGNMENT_ID ");
      // Added CompanyId for Consignment Reconciliation SUmmary
      sbQuery.append(" AND  T504.C1900_COMPANY_ID =" + getCompId());
      sbQuery.append(" AND  T506.C1900_COMPANY_ID =" + getCompId());
      sbQuery.append(" AND T504A.C506_RMA_ID = T506.C506_RMA_ID ");
      sbQuery.append(" ORDER BY T506.C506_RETURN_DATE desc ");

      log.debug(" Query for Excess Return Summary is " + sbQuery.toString());

      rdResultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return rdResultSet;
  }

  public HashMap loadExcessReturnDetails(String strConsignId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ResultSet rs = null;
    RowSetDynaClass rdExcessReturnDetails = null;
    HashMap hmHeaderInfo = new HashMap();
    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("gm_pkg_op_excess_return.gm_op_fch_excess_return_detail", 3);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    rdExcessReturnDetails = gmDBManager.returnRowSetDyna(rs);
    rs = (ResultSet) gmDBManager.getObject(3);
    hmHeaderInfo = gmDBManager.returnHashMap(rs);
    gmDBManager.close();
    hmReturn.put("RSEXCESSRETURNDETAILS", rdExcessReturnDetails);
    hmReturn.put("HMHEADERINFO", hmHeaderInfo);

    return hmReturn;
  }

  public void saveExcessReturnDetails(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ResultSet rs = null;
    String strMessage = " Transaction Successful ";

    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNID"));
    String strRAID = GmCommonClass.parseNull((String) hmParam.get("RAID"));
    String strReason = GmCommonClass.parseNull((String) hmParam.get("REASON"));
    String strCloseDate = GmCommonClass.parseNull((String) hmParam.get("CLOSEDATE"));
    String strTransferType = GmCommonClass.parseNull((String) hmParam.get("TRANSFERTYPE"));
    String strTransferMode = GmCommonClass.parseNull((String) hmParam.get("TRANSFERMODE")); // Transfer
                                                                                            // Mode
                                                                                            // for
                                                                                            // Excess
                                                                                            // Return
                                                                                            // Transfer
    String strTransferFrom = GmCommonClass.parseNull((String) hmParam.get("TRANSFERFROM"));
    String strTransferTo = GmCommonClass.parseNull((String) hmParam.get("TRANSFERTO"));
    String strTransferReason = GmCommonClass.parseNull((String) hmParam.get("TRANSFERREASON"));
    String strDate = GmCommonClass.parseNull((String) hmParam.get("DATE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strPartInputString = GmCommonClass.parseNull((String) hmParam.get("PARTINPUTSTRING"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOG"));



    gmDBManager.setPrepareString("gm_pkg_op_excess_return.gm_op_sav_excess_return_detail", 14);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strConsignId);
    gmDBManager.setString(2, strRAID);
    gmDBManager.setString(3, strReason);
    gmDBManager.setString(4, strCloseDate);
    gmDBManager.setString(5, strTransferType);
    gmDBManager.setString(6, strTransferMode);
    gmDBManager.setString(7, strTransferFrom);
    gmDBManager.setString(8, strTransferTo);
    gmDBManager.setString(9, strTransferReason);
    gmDBManager.setString(10, strDate);
    gmDBManager.setString(11, strUserId);
    gmDBManager.setString(12, ""); // Set Id is sent for the sake of save transfer proc. Since it is
                                   // not
    // used, it is NULL
    gmDBManager.setString(13, strInputString);
    gmDBManager.setString(14, strPartInputString);

    gmDBManager.execute();
    // Code to save the log information (comments) from the screen
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strConsignId, strLogReason, strUserId, "1221");// 1221 is
                                                                                       // the
      // code for
      // Consignment in
      // the log table.
      // GPLOG
    }

    gmDBManager.commit();

    throw new AppError(strMessage, "", 'S');


  }

  public void MissingTagReconciliation(HashMap hmParam) throws AppError {

    String inputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_return.gm_tag_reconciliation", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull(inputString));
    gmDBManager.setString(2, GmCommonClass.parseNull(strUserId));
    gmDBManager.execute();
    String errMsg = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.commit();
    log.debug("MissingTagReconciliation::errMsg::" + errMsg);

    if (!errMsg.equals("")) {
      throw new AppError(errMsg, "", 'S');
    }
  }

  public RowSetDynaClass loadMissingTagReturns(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResultSet = null;

    String strDistributorId = GmCommonClass.parseZero((String) hmParam.get("DISTID"));
    String strRAID = GmCommonClass.parseNull((String) hmParam.get("RAID"));
    String strExcessId = GmCommonClass.parseNull((String) hmParam.get("EXCESSID"));
    String partNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String ctrlNum = GmCommonClass.parseNull((String) hmParam.get("CTRLNUM"));
    String setID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strStatusId = GmCommonClass.parseZero((String) hmParam.get("STATUSID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT t506.c506_rma_id RAID, t506.c207_set_id SETID, t507.c205_part_number_id PNUM, get_partnum_desc(t507.c205_part_number_id) PDESC");
    sbQuery
        .append(" , get_code_name(t506.c506_type) TYPE,t506.c506_type TYPE_ID, t504a.c504_consignment_id EXCESS_ID ");
    sbQuery
        .append(" , GET_DISTRIBUTOR_NAME(t506.C701_DISTRIBUTOR_ID) DIST_NAME,t507.C507_CONTROL_NUMBER CTRL_NUM ");
    sbQuery
        .append(" , nvl(t507.C901_MISSING_TAG_REASON,0) REASON, t506.C701_DISTRIBUTOR_ID DISTID, t507.c507_item_qty QTY,t506.c506_created_date RDATE");
    sbQuery
        .append(" , get_user_name(t507.c507_reconciled_by) LASTUPNAME, GET_LOG_FLAG (t506.c506_rma_id , 1270 )  RLOG ,get_code_name(t507.C901_MISSING_TAG_REASON) MISSING_REASON");
    sbQuery
        .append(" FROM t506_returns t506,t507_returns_item t507,(select C506_RMA_ID,min(C504_CONSIGNMENT_ID) C504_CONSIGNMENT_ID from t504a_consignment_excess where c504a_void_fl IS NULL group by C506_RMA_ID) t504a ");
    sbQuery.append(" WHERE t506.C506_VOID_FL is null  ");
    sbQuery.append(" AND t507.c506_rma_id = t506.c506_rma_id ");
    sbQuery.append(" AND t507.c507_missing_fl ='Y' ");
    sbQuery.append(" AND t507.C507_STATUS_FL ='C' ");
    sbQuery.append(" AND t504a.c506_rma_id(+) = t506.c506_rma_id ");
    sbQuery.append(" AND t506.c1900_company_id ='" + getGmDataStoreVO().getCmpid() + "'");
    if (strStatusId.equals("4580")) { // Reconciled
      sbQuery.append(" AND t507.C901_MISSING_TAG_REASON IS NOT NULL ");
    } else if (strStatusId.equals("4581")) { // UnReconciled
      sbQuery.append(" AND t507.C901_MISSING_TAG_REASON IS NULL ");
    }

    if (!strDistributorId.equals("0")) {
      sbQuery.append(" AND T506.C701_DISTRIBUTOR_ID ='");
      sbQuery.append(strDistributorId);
      sbQuery.append("' ");
    }


    if (!strRAID.equals("")) {
      sbQuery.append(" AND t506.c506_rma_id ='");
      sbQuery.append(strRAID);
      sbQuery.append("' ");
    }

    if (!strExcessId.equals("")) {
      sbQuery.append(" AND T504A.C504_CONSIGNMENT_ID ='");
      sbQuery.append(strExcessId);
      sbQuery.append("' ");
    }

    if (!partNum.equals("")) {
      sbQuery.append(" AND t507.c205_part_number_id ='");
      sbQuery.append(partNum);
      sbQuery.append("' ");
    }

    if (!ctrlNum.equals("")) {
      sbQuery.append(" AND t507.C507_CONTROL_NUMBER ='");
      sbQuery.append(ctrlNum);
      sbQuery.append("' ");
    }

    if (!ctrlNum.equals("")) {
      sbQuery.append(" AND t507.C507_CONTROL_NUMBER ='");
      sbQuery.append(ctrlNum);
      sbQuery.append("' ");
    }
    if (!setID.equals("")) {
      sbQuery.append(" AND t506.c207_set_id ='");
      sbQuery.append(setID);
      sbQuery.append("' ");
    }
    sbQuery.append("  order by t507.c506_rma_id ");
    // sbQuery.append(" AND T504A.C506_RMA_ID = T506.C506_RMA_ID ");

    log.debug(" Query for Excess Return Summary is " + sbQuery.toString());

    rdResultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    gmDBManager.close();
    return rdResultSet;
  }

  public ArrayList loadAssociatedRAReport(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strInitRA = GmCommonClass.parseNull((String) hmParam.get("INITRA"));
    String strInitDist = GmCommonClass.parseZero((String) hmParam.get("INITDIST"));
    String strCreditRA = GmCommonClass.parseNull((String) hmParam.get("CREDITRA"));
    String strCreditDist = GmCommonClass.parseZero((String) hmParam.get("CREDITDIST"));
    log.debug("start");
    log.debug(strInitRA);
    log.debug(strInitDist);
    log.debug(strCreditRA);
    log.debug(strCreditDist);
    log.debug("End");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_return.gm_fch_associatedRA", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInitRA);
    gmDBManager.setString(2, strInitDist);
    gmDBManager.setString(3, strCreditRA);
    gmDBManager.setString(4, strCreditDist);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();
    return alReturn;
  }
  
  /**
   * getDistributorList - This method will return the Distributor list
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getDistributorList(HashMap hmParam) throws AppError {

    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));

    ArrayList alDistributorList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery
        .append(" SELECT T701.C701_DISTRIBUTOR_ID CODEID, DECODE('"
    		 + getComplangid()
    		 +"', '103097',NVL(T701.C701_DISTRIBUTOR_NAME_EN, T701.C701_DISTRIBUTOR_NAME), T701.C701_DISTRIBUTOR_NAME) CODENM,");
    sbQuery.append(" 'D'||T701.C701_DISTRIBUTOR_ID DID, DECODE( '"
    + getComplangid()
    +"', '103097',NVL(T701.C701_DISTRIBUTOR_NAME_EN, T701.C701_DISTRIBUTOR_NAME), T701.C701_DISTRIBUTOR_NAME) NAME ");
    sbQuery.append(" FROM T701_DISTRIBUTOR T701  ");
    sbQuery.append(" WHERE (T701.C901_EXT_COUNTRY_ID IS NULL ");
    sbQuery.append(" OR T701.C901_EXT_COUNTRY_ID IN (SELECT COUNTRY_ID FROM V901_COUNTRY_CODES))");
    // Filtering Distributor based on all the companies associated with the plant.
    if (strDistFilter.equals("COMPANY-PLANT")) {
      sbQuery
          .append(" AND ( T701.C1900_COMPANY_ID IN ( SELECT T5041.C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = '"
              + getCompPlantId() + "'  AND C5041_VOID_FL IS NULL )");
      sbQuery.append(" OR T701.C1900_COMPANY_ID IS NULL )  ");
      sbQuery
          .append("  AND T701.C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
      sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
          + getCompId() + "') ");

    } else {
      sbQuery.append("  AND ( T701.C1900_COMPANY_ID IS NULL OR T701.C1900_COMPANY_ID = '"
          + getCompId() + "') ");
      sbQuery
          .append("  AND T701.C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
      sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
          + getCompId() + "') ");
    }
    sbQuery.append(" AND (T701.C701_VOID_FL IS NULL OR T701.C701_DISTRIBUTOR_ID = GET_RULE_VALUE('DISTRIBUTOR','TAGST')) ORDER BY T701.C701_DISTRIBUTOR_NAME ");

    log.debug("Query to fetch distributor list " + sbQuery.toString());

    alDistributorList = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alDistributorList;
}
}
