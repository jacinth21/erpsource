package com.globus.operations.returns.beans;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * @author HReddi
 * Interface created for write method to print the Return Summary for US and BBA.	 *
 */	
public interface GmBBAProcessReturnsProcessInterface {
	public HashMap loadCreditReport (String strRAID, String strAction);	
}
