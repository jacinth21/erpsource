/**
 * 
 */
package com.globus.operations.returns.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmOrderUsageBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * @author vprasath
 */
public class GmProcessReturnsBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.;

  // this will be removed all place changed with GmDataStoreVO constructor

  public GmProcessReturnsBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmProcessReturnsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * loadCLROpenTxn - Fetch the open tansactions of a distributor, before the return is initiated.
   * 
   * @param strDistID
   * @return
   * @throws AppError
   */
  public HashMap loadCLROpenTxn(String strDistID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    ResultSet rs = null;
    // log.debug("HMPARAM:"+hmParam);

    HashMap hmOpenTransactions = new HashMap();
    ArrayList alReturn = new ArrayList();
    boolean isTxnAvl = false;

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_FCH_DISTOPENTRANS", 4);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDistID);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    alReturn = gmDBManager.returnArrayList(rs);
    isTxnAvl = alReturn.size() > 0 ? true : false;
    hmOpenTransactions.put("OPENCONS", alReturn);

    rs = (ResultSet) gmDBManager.getObject(3);
    alReturn = gmDBManager.returnArrayList(rs);
    isTxnAvl = alReturn.size() > 0 || isTxnAvl ? true : false;
    hmOpenTransactions.put("OPENRET", alReturn);

    rs = (ResultSet) gmDBManager.getObject(4);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    isTxnAvl = alReturn.size() > 0 || isTxnAvl ? true : false;
    hmOpenTransactions.put("OPENTRANS", alReturn);

    if (isTxnAvl) {
      hmOpenTransactions.put("ISOPENTXNAVL", "true");
    }

    log.debug(" Open Transactions: " + hmOpenTransactions);

    return hmOpenTransactions;
  }

  /**
   * saveCLRInitiate - Initiate a return for the Distributor closure
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public HashMap saveCLRInitiate(HashMap hmParam, String strUserID) throws AppError {
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strEdate = GmCommonClass.parseNull((String) hmParam.get("EDATE"));
    String strReason = GmCommonClass.parseNull((String) hmParam.get("REASON"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOGREASON"));
    String strSessCompanyLocale= GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.returns.GmCLRInitiate", strSessCompanyLocale);
    String strMessage =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RA_INITIATED_SUCCESSFULLY"));
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strPrepareQuery = null;
    String strRAID = "";

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_DISTCLOSURE", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strDistID);
    gmDBManager.setString(2, strEdate);
    gmDBManager.setString(3, strReason);
    gmDBManager.setString(4, strUserID);

    gmDBManager.execute();

    strRAID = gmDBManager.getString(5);

    if (strLogReason != null && !strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strRAID, strLogReason, strUserID, "1216");
    }

    gmDBManager.commit();

    hmReturn.put("RAID", strRAID);
    hmReturn.put("MESSAGE", strMessage);

    return hmReturn;
  }

  /**
   * saveInitiate - Initiate a return for the Item Returns
   * 
   * @param hmParam
   * @return
   * @throws AppError
   * @throws SQLException
   */
  public HashMap saveInitiate(HashMap hmParam, String strUserID) throws AppError {

    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strEdate = GmCommonClass.parseNull((String) hmParam.get("EXPDATE"));
    String strReason = GmCommonClass.parseNull((String) hmParam.get("REASON"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strAccID = GmCommonClass.parseNull((String) hmParam.get("EMPNAME"));
    String strPartQty = GmCommonClass.parseNull((String) hmParam.get("PARTQTY"));
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
    String strSetNo = GmCommonClass.parseNull((String) hmParam.get("SETNO"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOGREASON"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));

    strAccID = strType.equals("3304") ? strAccountId : strAccID; // 3304: Account Item consignment
    strDistID = strDistID.equals("0") ? null : strDistID;

    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strPrepareQuery = null;
    String strRAID = "";

    // log.debug("Dist ID: "+strDistID+" strEdate: "+strEdate+" strReason: "+strReason+"strUserID: "+strUserID);

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_RETURN", 10);

    gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strDistID);
    gmDBManager.setString(2, strEdate);
    gmDBManager.setString(3, strReason);
    gmDBManager.setString(4, strUserID);
    gmDBManager.setString(5, strType);
    gmDBManager.setString(6, strAccID);
    gmDBManager.setString(7, strPartQty);
    gmDBManager.setString(8, strPartID);
    gmDBManager.setString(9, strSetNo);

    gmDBManager.execute();

    strRAID = gmDBManager.getString(10);

    if (strLogReason != null && !strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strRAID, strLogReason, strUserID, "1216");
    }
    gmTxnSplitBean.checkAndSplitRA(gmDBManager, strRAID, strUserId);
    gmDBManager.commit();
    hmReturn.put("RAID", strRAID);
    hmReturn.put("MESSAGE", "RA initiated successfully");

    return hmReturn;
  }

  /**
   * loadCLRPndReturn - Fetch the Pending parts of a distributor to return
   * 
   * @param strDistID
   * @return
   * @throws AppError
   */
  public HashMap loadCLRPndReturn(String strDistID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ResultSet rs = null;

    RowSetDynaClass rPendingReturn = null;
    ArrayList alRules = new ArrayList();
    ArrayList alRA = new ArrayList();
    HashMap hmRA = new HashMap();

    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_FCH_DISTOPENCONS", 4);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDistID);

    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(2);
    rPendingReturn = gmDBManager.returnRowSetDyna(rs);
    hmReturn.put("PENTXNS", rPendingReturn);
    rs = (ResultSet) gmDBManager.getObject(3);
    alRules = gmDBManager.returnArrayList(rs);
    hmReturn.put("RULES", alRules);
    rs = (ResultSet) gmDBManager.getObject(4);
    alRA = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    if (alRA.size() > 0) {
      hmRA = (HashMap) (alRA.get(0));
      hmReturn.put("RADETAILS", hmRA);
    }


    return hmReturn;
  }

  /**
   * loadOUSCreditReport - This method will GOP OUS return credit details.
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   */

  public HashMap loadOUSCreditReport(HashMap hmParam) throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    HashMap hmRADetails = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alItemDetails = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    String strRAId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    log.debug("strRAId ::: " + strRAId);

    gmDBManager.setPrepareString("gm_pkg_oppr_request_summary.gm_fch_ous_ra_dtls", 4);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRAId);

    gmDBManager.execute();
    hmRADetails =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    hmReturn.put("RADETAILS", hmRADetails);
    alItemDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    hmReturn.put("RAITEMDETAILS", alItemDetails);
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(4)));
    hmReturn.put("RAINITMAILDECIDE", alReturn);

    gmDBManager.close();
    log.debug("hmReturn ::: " + hmReturn);
    return hmReturn;
  } // End of loadOUSCreditReport.

  /**
   * loadCreditReport - This method will return credit invoice (Part Return)
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadCreditReport(String strRAID, String strAction) throws AppError {
    log.debug("Enter");
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT A.C506_RMA_ID RAID, NVL(A.C701_DISTRIBUTOR_ID,A.C704_ACCOUNT_ID) DID ");
      sbQuery.append(" , A.C506_STATUS_FL STATUS ");
      sbQuery.append(" , get_cs_ship_email(4121, A.C703_SALES_REP_ID) EMAILADD");
      sbQuery.append(" , A.C506_CREDIT_DATE CREDITEMAILDATE ");
      sbQuery
          .append(" , DECODE(A.C701_DISTRIBUTOR_ID, NULL, GET_ACCOUNT_NAME(A.C704_ACCOUNT_ID), GET_DISTRIBUTOR_NAME(A.C701_DISTRIBUTOR_ID)) DNAME ");
      sbQuery.append(" , A.C506_STATUS_FL STATUS, ");
      sbQuery.append(" GET_USER_NAME(A.C506_LAST_UPDATED_BY) CREDITBY, ");
      sbQuery.append(" A.C506_COMMENTS COMMENTS, A.C506_CREATED_DATE CDATE, ");
      sbQuery.append(" A.C506_AC_CREDIT_DT CREDITDATE, ");
      sbQuery.append(" GET_USER_NAME(A.C506_CREATED_BY) PER, GET_CODE_NAME(A.C506_TYPE) TYPE, ");
      sbQuery.append(" A.C506_CREDIT_MEMO_ID MEMOID, T501.C503_INVOICE_ID CREDITINVID , ");
      sbQuery
          .append(" GET_CODE_NAME(A.C506_REASON) REASON, A.C207_SET_ID SETID, B.C503_CUSTOMER_PO PO,");
      sbQuery
          .append(" DECODE(A.C506_TYPE,3308,GET_CONSIGN_BILL_ADD(A.C506_REF_ID), GET_CREDIT_BILL_ADD (A.C506_RMA_ID)) ACCADD, ");
      sbQuery.append(" A.C506_LAST_UPDATED_DATE UDATE,");
      sbQuery.append(" B.C503_INVOICE_ID INVID, ");
      sbQuery.append(" T501.C501_COMMENTS ORDER_COMMENTS, ");
      sbQuery.append(" C506_EXPECTED_DATE EDATE ");
      sbQuery.append(" ,T501.C501_SHIP_COST SCOST ");
      sbQuery.append(" , A.C506_CREATED_DATE RETDATE ");

      sbQuery.append(" ,A.C506_CREATED_DATE RET_DATE ");
      sbQuery.append(" , A.C506_CREATED_DATE RET_DATE ");
      sbQuery
          .append(" ,A.C501_ORDER_ID ORDER_ID, T501.C901_ORDER_TYPE ORDTYPE, A.C506_TYPE RETTYPE  ");
      sbQuery
          .append(" ,decode(GET_DISTRIBUTOR_INTER_PARTY_ID(A.C701_DISTRIBUTOR_ID),NULL,'USD',get_rule_value (50218, get_distributor_inter_party_id (A.c701_distributor_id))) NEWCURRENCY ");
      sbQuery.append(" ,GET_DISTRIBUTOR_INTER_PARTY_ID(A.C701_DISTRIBUTOR_ID) PARTYID ");
      sbQuery
          .append(" ,nvl(get_rule_value (A.c701_distributor_id, 'CC-LIST-RETURNS'),get_rule_value ('DEFAULTSHIPEMAIL', 'EMAIL')) CCLIST , get_user_emailid (A.C506_CREATED_BY) EMAIL_ID ");
      sbQuery
          .append(" ,get_ac_company_id(T501.c704_account_id) COMPANYID ,get_rep_name (A.c703_sales_rep_id) REPNM ");
      // to fetch child RA list for passing RA - PMT-234
      sbQuery
          .append(" , (SELECT  RTRIM(XMLAGG(XMLELEMENT(E,c506_rma_id || ',')).EXTRACT('//text()').GETCLOBVAL(),',')  ");
      sbQuery.append(" FROM T506_RETURNS WHERE c506_parent_rma_id = '");
      sbQuery.append(strRAID);
      sbQuery.append("' AND c506_void_fl IS NULL) CHILDRAID ");
      sbQuery.append(", get_account_curr_symb (A.C704_ACCOUNT_ID) ACCOUNT_CURR_NM ");
      sbQuery.append(" FROM T506_RETURNS A, T503_INVOICE B, T501_ORDER T501 ");
      sbQuery.append(" WHERE A.C506_RMA_ID = '");
      sbQuery.append(strRAID);
      sbQuery.append("' AND A.C503_INVOICE_ID = B.C503_INVOICE_ID(+)");
      sbQuery.append(" AND A.C506_RMA_ID = T501.C506_RMA_ID(+)");
      sbQuery.append(" AND B.C503_VOID_FL IS NULL ");
      sbQuery.append(" AND A.C506_VOID_FL IS NULL ");
      sbQuery.append(" AND (B.c901_ext_country_id is NULL ");
      sbQuery.append(" OR B.c901_ext_country_id  in (select country_id from v901_country_codes))");
      sbQuery.append(" AND (T501.c901_ext_country_id is NULL ");
      sbQuery
          .append(" OR T501.c901_ext_country_id  in (select country_id from v901_country_codes))");

      log.debug(" Query for RADETAILS is " + sbQuery.toString());
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

      hmReturn.put("RADETAILS", hmResult);

      // Since its a return QTY is divided by -ive value
      /*
       * sbQuery.setLength(0); sbQuery.append(
       * " SELECT -1 * SUM(C507_ITEM_QTY) QTY, C507_ITEM_PRICE PRICE, C507_STATUS_FL SFL, ");
       * sbQuery.append(" C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC ");
       * sbQuery.append(" FROM T507_RETURNS_ITEM "); sbQuery.append(" WHERE C506_RMA_ID = '");
       * sbQuery.append(strRAID); sbQuery.append("' AND C507_STATUS_FL IN ('C', 'W', 'R') ");
       */

      sbQuery.setLength(0);
      sbQuery.append(" SELECT -1 * SUM(C507_ITEM_QTY) QTY ");
      sbQuery.append(", -1 * SUM(C507_ITEM_QTY) IQTY");
      sbQuery.append(", nvl(C507_ITEM_PRICE,0) PRICE ");
      sbQuery.append(", '-' CNUM ");
      sbQuery
          .append(" , DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C' , 'Return', 'W', 'Write-off','M','Missing') SFL ");
      sbQuery.append(" , C205_PART_NUMBER_ID PNUM ");
      sbQuery
          .append(" , get_partdesc_by_company(C205_PART_NUMBER_ID) PDESC, c507_unit_price UPRICE , c507_unit_price_adj_value UADJVAL, c901_unit_price_adj_code UADJCODE ");
      sbQuery.append(" FROM ");
      sbQuery.append(" T507_RETURNS_ITEM ");
      sbQuery.append(" WHERE ");
      sbQuery
          .append(" C506_RMA_ID IN ( SELECT  C506_RMA_ID FROM T506_returns T506 WHERE T506.C506_RMA_ID = '");
      sbQuery.append(strRAID);
      sbQuery.append("') AND C507_STATUS_FL IN ('C', 'W', 'R','M') ");
      sbQuery
          .append(" GROUP BY C507_ITEM_PRICE,C205_PART_NUMBER_ID,C507_STATUS_FL,C507_ITEM_PRICE, c507_unit_price , c507_unit_price_adj_value , c901_unit_price_adj_code  ORDER BY C205_PART_NUMBER_ID ");


      // Removed for Credit Memo changes
      /*
       * if (strAction.equals("PrintVersion")) { sbQuery.append(" AND C507_STATUS_FL = 'C'"); }
       */

      /*
       * sbQuery.append(" GROUP BY C507_ITEM_PRICE,C205_PART_NUMBER_ID,C507_STATUS_FL ");
       * sbQuery.append(" ORDER BY C205_PART_NUMBER_ID");
       */

      log.debug(" Query for RAITEMDETAILS is " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("RAITEMDETAILS", alReturn);

      sbQuery.setLength(0);
      sbQuery.append(" SELECT -1 * SUM(C507_ITEM_QTY) QTY ");
      sbQuery.append(", -1 * SUM(C507_ITEM_QTY) IQTY");
      sbQuery.append(", nvl(C507_ITEM_PRICE,0) PRICE ");
      sbQuery.append(", '-' CNUM ");
      sbQuery
          .append(" , DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C' , 'Return', 'W', 'Write-off','M','Missing') SFL ");
      sbQuery.append(" , t507.C205_PART_NUMBER_ID PNUM ");
      sbQuery
          .append(" , get_partdesc_by_company(t507.C205_PART_NUMBER_ID) PDESC, t507.c507_unit_price UPRICE , t507.c507_unit_price_adj_value UADJVAL, t507.c901_unit_price_adj_code UADJCODE ");
      sbQuery.append(" FROM ");
      sbQuery.append(" T507_RETURNS_ITEM t507 , t205_part_number t205");
      sbQuery.append(" WHERE ");
      sbQuery
          .append(" C506_RMA_ID IN ( SELECT  C506_RMA_ID FROM T506_returns T506 WHERE T506.C506_RMA_ID = '");
      sbQuery.append(strRAID);
      sbQuery.append("') AND C507_STATUS_FL IN ('C', 'W', 'R','M') ");
      sbQuery.append(" AND t507.c205_part_number_id  = t205.c205_part_number_id ");
      sbQuery
          .append(" AND t205.c202_project_id NOT IN (SELECT c906_rule_id FROM t906_rules WHERE c906_rule_grp_id = 'EXVND' AND c906_void_fl IS NULL )");
      sbQuery
          .append(" GROUP BY C507_ITEM_PRICE,t507.C205_PART_NUMBER_ID,C507_STATUS_FL,C507_ITEM_PRICE, c507_unit_price , c507_unit_price_adj_value,  c901_unit_price_adj_code   ORDER BY t507.C205_PART_NUMBER_ID ");

      log.debug(" Query for RAINITMAILDECIDE is " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("RAINITMAILDECIDE", alReturn);

      sbQuery.setLength(0);
      sbQuery
          .append(" SELECT t506.c506_rma_id craid, t506a.c506a_attribute_value projid, GET_PROJECT_NAME (t506a.c506a_attribute_value) projnm ");
      sbQuery.append(" FROM t506_returns t506 , t506a_returns_attribute t506a ");
      sbQuery.append(" WHERE  t506.c506_rma_id = t506a.c506_rma_id ");
      sbQuery.append(" AND t506.c506_parent_rma_id = '" + strRAID + "'");
      sbQuery.append(" AND t506.c506_void_fl IS NULL ");
      sbQuery.append(" AND t506a.c506a_void_fl IS NULL ");
      log.debug(" Query for CHILDRA IDS is " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("CHILDRADETAILS", alReturn);

    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmSalesCreditBean:loadCreditReport", "Exception is:" + e);
    }
    log.debug("Exit" + "RAITEMDETAILS :" + alReturn.size() + "RADETAILS :" + hmResult.size());
    return hmReturn;
  } // End of loadCreditReport

  /**
   * loadCreditAsstRAReport - This method will return credit invoice (Part Return)
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadCreditAsstRAReport(String strRAID, String strAction) throws AppError {
    log.debug("Enter");
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alReturn = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT A.C506_RMA_ID RAID, NVL(A.C701_DISTRIBUTOR_ID,A.C704_ACCOUNT_ID) DID ");
      sbQuery.append(" , A.C506_STATUS_FL STATUS ");
      sbQuery.append(" , get_cs_ship_email(4121, A.C703_SALES_REP_ID) EMAILADD");
      sbQuery.append(" , A.C506_CREDIT_DATE CREDITEMAILDATE ");
      sbQuery
          .append(" , DECODE(A.C701_DISTRIBUTOR_ID, NULL, GET_ACCOUNT_NAME(A.C704_ACCOUNT_ID), GET_DISTRIBUTOR_NAME(A.C701_DISTRIBUTOR_ID)) DNAME ");
      sbQuery.append(" , A.C506_STATUS_FL STATUS, ");
      sbQuery.append(" GET_USER_NAME(A.C506_LAST_UPDATED_BY) CREDITBY, ");
      sbQuery.append(" A.C506_COMMENTS COMMENTS, A.C506_CREATED_DATE CDATE, ");
      sbQuery.append(" A.C506_AC_CREDIT_DT CREDITDATE, ");
      sbQuery.append(" GET_USER_NAME(A.C506_CREATED_BY) PER, GET_CODE_NAME(A.C506_TYPE) TYPE, ");
      sbQuery.append(" A.C506_CREDIT_MEMO_ID MEMOID, T501.C503_INVOICE_ID CREDITINVID , ");
      sbQuery
          .append(" GET_CODE_NAME(A.C506_REASON) REASON, A.C207_SET_ID SETID, B.C503_CUSTOMER_PO PO,");
      sbQuery.append(" GET_CREDIT_ASST_BILL_ADD(A.C506_RMA_ID) ACCADD, ");
      sbQuery.append(" A.C506_LAST_UPDATED_DATE UDATE,");
      sbQuery.append(" B.C503_INVOICE_ID INVID, ");
      sbQuery.append(" T501.C501_COMMENTS ORDER_COMMENTS, ");
      sbQuery.append(" C506_EXPECTED_DATE EDATE ");
      sbQuery.append(" ,T501.C501_SHIP_COST SCOST ");
      sbQuery.append(" ,A.C506_CREATED_DATE RETDATE ");
      sbQuery
          .append(" ,A.C501_ORDER_ID ORDER_ID, T501.C901_ORDER_TYPE ORDTYPE, A.C506_TYPE RETTYPE  ");
      sbQuery
          .append(" ,decode(GET_DISTRIBUTOR_INTER_PARTY_ID(A.C701_DISTRIBUTOR_ID),NULL,'USD',get_rule_value (50218, get_distributor_inter_party_id (A.c701_distributor_id))) NEWCURRENCY ");
      sbQuery.append(" ,GET_DISTRIBUTOR_INTER_PARTY_ID(A.C701_DISTRIBUTOR_ID) PARTYID ");
      sbQuery
          .append(" ,nvl(get_rule_value (A.c701_distributor_id, 'CC-LIST-RETURNS'),get_rule_value ('DEFAULTSHIPEMAIL', 'EMAIL')) CCLIST");
      sbQuery.append(" FROM T506_RETURNS A, T503_INVOICE B, T501_ORDER T501 ");
      sbQuery.append(" WHERE A.C506_RMA_ID = '");
      sbQuery.append(strRAID);
      sbQuery.append("' AND A.C503_INVOICE_ID = B.C503_INVOICE_ID(+)");
      sbQuery.append(" AND A.C506_RMA_ID = T501.C506_RMA_ID(+)");
      sbQuery.append(" AND B.C503_VOID_FL IS NULL ");
      sbQuery.append(" AND (B.c901_ext_country_id is NULL ");
      sbQuery.append(" OR B.c901_ext_country_id  in (select country_id from v901_country_codes))");
      sbQuery.append(" AND (T501.c901_ext_country_id is NULL ");
      sbQuery
          .append(" OR T501.c901_ext_country_id  in (select country_id from v901_country_codes))");
      // sbQuery.append(" AND A.C506_VOID_FL IS NULL ");

      log.debug(" Query for RADETAILS is " + sbQuery.toString());
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

      hmReturn.put("RADETAILS", hmResult);

      // Since its a return QTY is divided by -ive value
      /*
       * sbQuery.setLength(0); sbQuery.append(
       * " SELECT -1 * SUM(C507_ITEM_QTY) QTY, C507_ITEM_PRICE PRICE, C507_STATUS_FL SFL, ");
       * sbQuery.append(" C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(C205_PART_NUMBER_ID) PDESC ");
       * sbQuery.append(" FROM T507_RETURNS_ITEM "); sbQuery.append(" WHERE C506_RMA_ID = '");
       * sbQuery.append(strRAID); sbQuery.append("' AND C507_STATUS_FL IN ('C', 'W', 'R') ");
       */

      sbQuery.setLength(0);
      sbQuery.append(" SELECT -1 * SUM(C507_ITEM_QTY) QTY ");
      sbQuery.append(", -1 * SUM(C507_ITEM_QTY) IQTY");
      sbQuery.append(", nvl(C507_ITEM_PRICE,0) PRICE ");
      sbQuery.append(", '-' CNUM ");
      sbQuery
          .append(" , DECODE (C507_STATUS_FL, 'R', 'Pending Return', 'C' , 'Return', 'W', 'Write-off','M','Missing') SFL ");
      sbQuery.append(" , C205_PART_NUMBER_ID PNUM ");
      sbQuery
          .append(" , get_partdesc_by_company(C205_PART_NUMBER_ID) PDESC, c507_unit_price UPRICE , c507_unit_price_adj_value UADJVAL, c901_unit_price_adj_code UADJCODE ");
      sbQuery.append(" FROM ");
      sbQuery.append(" T507_RETURNS_ITEM ");
      sbQuery.append(" WHERE ");
      sbQuery
          .append(" C506_RMA_ID IN ( SELECT  C506_RMA_ID FROM T506_returns T506 WHERE T506.C506_RMA_ID = '");
      sbQuery.append(strRAID);
      sbQuery.append("' OR T506.C506_PARENT_RMA_ID = '");
      sbQuery.append(strRAID);
      sbQuery.append("') AND C507_STATUS_FL IN ('C', 'W', 'R','M') ");
      sbQuery
          .append(" GROUP BY C507_ITEM_PRICE,C205_PART_NUMBER_ID,C507_STATUS_FL,C507_ITEM_PRICE, c507_unit_price ,c507_unit_price_adj_value, c901_unit_price_adj_code ORDER BY C205_PART_NUMBER_ID ");


      // Removed for Credit Memo changes
      /*
       * if (strAction.equals("PrintVersion")) { sbQuery.append(" AND C507_STATUS_FL = 'C'"); }
       */

      /*
       * sbQuery.append(" GROUP BY C507_ITEM_PRICE,C205_PART_NUMBER_ID,C507_STATUS_FL ");
       * sbQuery.append(" ORDER BY C205_PART_NUMBER_ID");
       */

      log.debug(" Query for RAITEMDETAILS is " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      hmReturn.put("RAITEMDETAILS", alReturn);

    } catch (Exception e) {
      e.printStackTrace();
      GmLogError.log("Exception in GmSalesCreditBean:loadCreditReport", "Exception is:" + e);
    }
    log.debug("Exit" + "RAITEMDETAILS :" + alReturn.size() + "RADETAILS :" + hmResult.size());
    return hmReturn;
  } // End of loadCreditAsstRAReport

  /**
   * saveReturnsDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveReturnsDetails(HashMap hmParam, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";

    String strRAId = (String) hmParam.get("RAID");
    String strInputString = (String) hmParam.get("INPUTSTRING");
    String strRetDate = (String) hmParam.get("RETDATE");
    String strFlag = (String) hmParam.get("RACOMPLETE");
    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_RETURN_ACCP", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strRAId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strRetDate);
    gmDBManager.setString(4, strFlag);
    gmDBManager.setString(5, strUserId);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(6);
    gmDBManager.commit();

    hmReturn.put("MESSAGE", strMsg);
    log.debug("Exit" + strMsg);
    return hmReturn;
  } // end of saveReturnsDetails

  /**
   * saveReturnReconfDetails - This method will
   * 
   * @param String strUsername
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveReturnReconfDetails(HashMap hmParam, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturnDetails = new HashMap();
    hmReturnDetails = saveReturnReconfDetails(gmDBManager, hmParam, strUserId);
    gmDBManager.commit();
    return hmReturnDetails;
  }

  /**
   * saveReturnsDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap saveReturnReconfDetails(GmDBManager gmDBManager, HashMap hmParam, String strUserId)
      throws AppError {
    // PMT-4534 -- DbConnection Wrapper is changed to GmDBManager class
    String strMsg = "";
    String strPrepareString = null;

    String strRAID =
        (hmParam.get("RAID") == null || ((String) hmParam.get("RAID")).equals("")) ? ""
            : (String) hmParam.get("RAID");
    String strParentRAId = (String) hmParam.get("PARENTRAID");
    String strInputString = (String) hmParam.get("INPUTSTRING");
    String strRetDate = (String) hmParam.get("RETDATE");
    String strFlag = (String) hmParam.get("RACOMPLETE");
    String strSetID = (String) hmParam.get("SETID");
    strSetID = strSetID.equals("0") ? "" : strSetID;
    HashMap hmReturn = new HashMap();

    // PMT-4534 -- DbConnection Wrapper is changed to GmDBManager class
    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_RECONF_ACCP", 8);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strParentRAId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strRetDate);
    gmDBManager.setString(4, strFlag);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strRAID);
    gmDBManager.setString(7, strSetID);
    gmDBManager.execute();
    strRAID = gmDBManager.getString(8);
    strMsg = "RA SAVED SUCCESSFULLY";
    hmReturn.put("CHILDRAID", strRAID);
    hmReturn.put("MESSAGE", strMsg);
    return hmReturn;
  } // end of saveReturnReconfDetails


  /**
   * loadReturnsDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadDisplayDetails() throws AppError {
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCustomerBean gmCustBean = new GmCustomerBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    ArrayList alReturnType = new ArrayList();
    ArrayList alReturnReasons = new ArrayList();
    ArrayList alDistributors = new ArrayList();
    ArrayList alSets = new ArrayList();
    ArrayList alUserSet = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alAccountList = new ArrayList();

    // try {
    alReturnType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RETTY"));
    alReturnReasons = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RETRE"));
    hmReturn.put("SCREEN", "ReloadRpt");
    alDistributors = GmCommonClass.parseNullArrayList(gmCustBean.getDistributorList(hmReturn));
    alSets = GmCommonClass.parseNullArrayList(gmProject.loadSetMap("1601"));
    alUserSet = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList("300", ""));
    alRepList = GmCommonClass.parseNullArrayList(gmCustBean.getRepList("Active"));
    alAccountList = GmCommonClass.parseNullArrayList(gmSales.reportAccount());

    hmReturn.put("RETTYPES", alReturnType);
    hmReturn.put("RETREASONS", alReturnReasons);
    hmReturn.put("DISTLIST", alDistributors);
    hmReturn.put("SETLIST", alSets);
    hmReturn.put("EMPNAME", alUserSet);
    hmReturn.put("ALREPLIST", alRepList);
    hmReturn.put("ALACCOUNTLIST", alAccountList);

    /*
     * } catch (Exception e) { GmLogError.log("Exception in GmCustomerBean:loadReturnsDetails",
     * "Exception is:" + e); }
     */
    return hmReturn;
  } // End of loadDisplayDetails

  public ArrayList loadReturnReConfigureInfo(HashMap hmParam) throws AppError {
    log.debug("Enter loadReturnReConfigureInfo : " + hmParam);

    String strDistID = (String) hmParam.get("DISTID");
    String strRAID =
        hmParam.get("CHILDRAID") == null ? (String) hmParam.get("PARENTRAID") : (String) hmParam
            .get("CHILDRAID");
    String strConsignID = (String) hmParam.get("CONSGID");
    String strSetID = (String) hmParam.get("SETID");
    String strPartNums = (String) hmParam.get("PARTNUMS");

    ResultSet rs = null;
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_FCH_RECONF_ACCP", 6);
    /*
     * register out parameter and set input parameters
     */

    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDistID);
    gmDBManager.setString(2, strRAID);
    gmDBManager.setString(3, strConsignID);
    gmDBManager.setString(4, strPartNums);
    gmDBManager.setString(5, strSetID);

    log.debug("Before Executing");

    gmDBManager.execute();

    log.debug("After Executing");

    rs = (ResultSet) gmDBManager.getObject(6);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    // log.debug("Reconf Parts:"+alReturn);

    return alReturn;
  }

  public RowSetDynaClass loadReturnsDetails(HashMap hmParam) throws AppError {
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strSetNums = GmCommonClass.parseNull((String) hmParam.get("SETNUMS"));
    String strSetQtys = GmCommonClass.parseNull((String) hmParam.get("SETQTY"));
    String strPartNums = GmCommonClass.parseNull((String) hmParam.get("PARTNUMS"));
    String strEmpName = GmCommonClass.parseNull((String) hmParam.get("EMPNAME"));
    String strReqType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));

    strDistID = strReqType.equals("3304") ? strAccountID : strDistID; // 3304: Account Consignment -
                                                                      // Items
    /*
     * DBConnectionWrapper dbCon = null; dbCon = new DBConnectionWrapper();
     */

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    RowSetDynaClass rPendingReturn = null;
    ArrayList alReturn = new ArrayList();

    // try {
    // conn = dbCon.getConnection();
    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_FCH_RETURN_INFO", 7);
    // csmt = conn.prepareCall(strPrepareString);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);

    gmDBManager.setString(1, strDistID);
    gmDBManager.setString(2, strSetNums);
    gmDBManager.setString(3, strSetQtys);
    gmDBManager.setString(4, strPartNums);
    gmDBManager.setString(5, strEmpName);
    gmDBManager.setString(6, strReqType);
    gmDBManager.execute();
    // rs = (ResultSet) csmt.getObject(7);
    rPendingReturn = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));
    log.debug("Size : " + rPendingReturn.getRows().size());
    gmDBManager.close();

    /*
     * } catch (Exception e) { e.printStackTrace();
     * GmLogError.log("Exception in GmProcessReturnsBean:loadReturnsDetails", "Exception is:" + e);
     * throw new AppError(e); } finally { try { if (csmt != null) { csmt.close(); }// Enf of if
     * (csmt != null) if (conn != null) { conn.close(); closing the Connections } } catch (Exception
     * e) { throw new AppError(e); }// End of catch finally { csmt = null; conn = null; gmDBManager
     * = null; } }
     */
    return rPendingReturn;
  }// End of loadReturnsDetails

  /**
   * loadReturnsDetailInfo - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList loadReturnsDetailInfo(HashMap hmParam) throws AppError {
    log.debug("Enter");
    String strRAId = (String) hmParam.get("RAID");
    String strConsignID = (String) hmParam.get("CONSGID");
    String strPartNums = (String) hmParam.get("PARTNUMS");
    String strDistID = (String) hmParam.get("DISTID");

    // strPartNums = GmCommonClass.getStringWithQuotes(strPartNums);

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ResultSet rs = null;
    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_FCH_RETURN_ACCP", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.setString(1, strRAId);
    gmDBManager.setString(2, strConsignID);
    gmDBManager.setString(3, strPartNums);

    gmDBManager.execute();

    rs = (ResultSet) gmDBManager.getObject(4);
    alReturn = gmDBManager.returnArrayList(rs);

    gmDBManager.close();
    // hmReturn.put("SETLOAD",alReturn);

    log.debug("Exit");
    return alReturn;
  } // End of loadReturnsSavedSetMaster

  /**
   * loadReturnsConsignForCredit - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadReturnsConsignForCredit(String strRAId, String strFromScr) throws AppError {
    /*
     * DBConnectionWrapper dbCon = null; dbCon = new DBConnectionWrapper(); CallableStatement csmt =
     * null; Connection conn = null; String strPrepareString = null; ArrayList alReturn = new
     * ArrayList(); ResultSet rs = null;
     */
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    ArrayList alMailReport = new ArrayList();
    ArrayList alAssociatedReport = new ArrayList();


    log.debug("Enter");

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_FCH_RETURN_FORCREDIT ", 4);

    gmDBManager.setString(1, GmCommonClass.parseNull(strRAId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strFromScr));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    alMailReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    alAssociatedReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));

    hmReturn.put("RACRITEMDETAILS", alMailReport);
    hmReturn.put("ASSRACRITEMDETAILS", alAssociatedReport);

    gmDBManager.close();

    log.debug("Exit");

    return hmReturn;
  } // End of loadReturnsConsignForCredit

  /**
   * saveReturnsDetails - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String saveReturnsConsignForCredit(String strRAID, Date dtCreditDate, int intStatus,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33510
    HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33510


    String strMsg = "RA Credited Successfully";

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_RETURN_FORCREDIT", 3);

    gmDBManager.setString(1, strRAID);
    gmDBManager.setDate(2, new java.sql.Date(dtCreditDate.getTime()));
    gmDBManager.setString(3, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();

    log.debug("Exit" + strMsg);
   
  //JMS call for Storing Building Info PMT-33510 
    log.debug("JMS call for Storing building Information for Returns...");
    hmStorageBuildInfo.put("TXNID", strRAID); //RMA ID
    hmStorageBuildInfo.put("TXNTYPE", "RETURN"); // Transaction type is Return
  //Method call to split the transaction(orders) using RMA ID and update building id based on the parts available in the building
    gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo);

    return strMsg;
  } // end of saveReturnsDetails


  public ArrayList loadReprocessDetails(String strRAIDs, String strSetID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPrepareQuery = null;
    ArrayList alResult = new ArrayList();
    ResultSet rs = null;

    gmDBManager.setPrepareString("GM_PKG_OP_REPROCESS.GM_OP_RPT_REPROCESS", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // register out parameter
    gmDBManager.setString(1, strRAIDs);
    gmDBManager.setString(2, strSetID);
    gmDBManager.execute();
    rs = (ResultSet) gmDBManager.getObject(3);
    alResult = gmDBManager.returnArrayList(rs);
    gmDBManager.close();


    return alResult;
  }

  public HashMap saveReturnReprocess(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    log.debug("hmParam = " + hmParam);
    String strReturnIds = "";
    String strMsg = "";
    String strRAID = GmCommonClass.parseNull((String) hmParam.get("RAID"));
    String strRAIDS = GmCommonClass.parseNull((String) hmParam.get("RAIDS"));
    String strCNSTR = GmCommonClass.parseNull((String) hmParam.get("CNSTR"));
    String strQNSTR = GmCommonClass.parseNull((String) hmParam.get("QNSTR"));
    String strPNSTR = GmCommonClass.parseNull((String) hmParam.get("PNSTR"));
    String strIASTR = GmCommonClass.parseNull((String) hmParam.get("IASTR"));
    String strIHSTR = GmCommonClass.parseNull((String) hmParam.get("IHSTR"));
    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strRequestID = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    ///Add New column for sterileStr
    String QNSterileStr = GmCommonClass.parseNull((String) hmParam.get("QNSterileStr")); 
    String strFGSTR = GmCommonClass.parseNull((String) hmParam.get("FGSTR"));//PC-4788 - get FG string value
    
    gmDBManager.setPrepareString("GM_PKG_OP_REPROCESS.GM_SAVE_REPROCESS_RETURNS", 15);
    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(13, java.sql.Types.CHAR);
    gmDBManager.setString(1, strRAID);
    gmDBManager.setString(2, strRAIDS);
    gmDBManager.setString(3, strCNSTR);
    gmDBManager.setString(4, strQNSTR);
    gmDBManager.setString(5, strPNSTR);
    gmDBManager.setString(6, strSetID);
    gmDBManager.setString(7, strUserID);
    gmDBManager.setString(8, strRequestID);
    gmDBManager.setString(9, strComments);
    gmDBManager.setString(10, strIHSTR);
    gmDBManager.setString(11, strIASTR);
    /*Set QNSterilestr to Data Base*/
    gmDBManager.setString(14, QNSterileStr);
    gmDBManager.setString(15, strFGSTR); //PC-4788 - Add a new parameter for Return to FG Transaction
    gmDBManager.execute();
    strReturnIds = gmDBManager.getString(12);
    strMsg = gmDBManager.getString(13);
    log.debug("strReturnIds = " + strReturnIds);
    hmReturn.put("RETURNIDS", strReturnIds);
    hmReturn.put("ORAMSG", strMsg);
    if (!strQNSTR.equals("") && !strComments.equals("")) {
      String arrReturnIDs[] = strReturnIds.split("\\,");
      for (int i = 0; i < arrReturnIDs.length; i++) {
        if (arrReturnIDs[i].startsWith("GM-QN")) {
          gmCommonBean.saveLog(gmDBManager, arrReturnIDs[i], strComments, strUserID, "4113");// 4113
                                                                                             // Quarantine
          break;
        }
      }
    }
    gmDBManager.commit();
    return hmReturn;
  }

  /**
   * saveOrderReturns - This method will be used to initate order credit info
   * 
   * @return HashMap
   * @exception AppError
   **/
  public String saveOrderReturns(HashMap hmParam) throws AppError {
    // PMT-4534 -- DbConnection Wrapper is changed to GmDBManager class
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    String strRAId = "";
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strReason = GmCommonClass.parseNull((String) hmParam.get("REASON"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("ID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    Date dtExpDate = (Date) hmParam.get("EDATE");
    String strDateFmt = getCompDateFmt();// GmCommonClass.parseNull((String)
                                         // hmParam.get("DATEFORMAT"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strParentOrderId = GmCommonClass.parseNull((String) hmParam.get("PARENT_ORDERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strUsername = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strEmpName = GmCommonClass.parseZero((String) hmParam.get("EMPNAME"));
    String strOrderType = GmCommonClass.parseZero((String) hmParam.get("ORDERTYPE"));
    String strUsedLotStr = GmCommonClass.parseNull((String) hmParam.get("USEDLOT_STR"));
    String strConsigment = "";
    String strLoaner = "";
    String strLnorcn = "";
    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
    String strRuleOrdType =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDERTYPE", "ORDERRETURN"));
    hmParam.put("RAID", "");
    hmParam.put("RETDATE",
        GmCommonClass.getStringFromDate((Date) hmParam.get("SYSTEMDATE"), strDateFmt));
    hmParam.put("RACOMPLETE", "");
    hmParam.put("SETID", "0");
    boolean blLoanerSts = false;
    // PMT-4534 -- DbConnection Wrapper is changed to GmDBManager class

    if (strInputString.indexOf("~") != -1) {
      String[] strData = strInputString.split("~");
      if (strData.length > 0) {
        // get the consignment and loaner input string
        strConsigment = GmCommonClass.parseNull(strData[0]);
        strLoaner = GmCommonClass.parseNull(strData.length > 1 ? strData[1] : "");
        if (strConsigment.equals("") && !strLoaner.equals("")) {
          strLnorcn = strLoaner;
          strLoaner = "";
          blLoanerSts = true;
        } else {
          strLnorcn = strConsigment;
        }

        // for creating consignment or loaner RA
        if (!strLnorcn.equals("")) {
          // PMT-4534 -- DbConnection Wrapper is changed to GmDBManager class
          gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_ORDER_RETURN", 11);
          gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);
          gmDBManager.setInt(1, Integer.parseInt(strType));
          gmDBManager.setInt(2, Integer.parseInt(strReason));
          gmDBManager.setString(3, strDistId);
          gmDBManager.setString(4, strComments);
          gmDBManager.setDate(5, new java.sql.Date(dtExpDate.getTime()));
          gmDBManager.setString(6, strOrderId);
          gmDBManager.setInt(7, Integer.parseInt(strUsername));
          gmDBManager.setString(8, strParentOrderId);
          gmDBManager.setInt(9, Integer.parseInt(strEmpName));
          gmDBManager.setString(10, strLnorcn);
          gmDBManager.execute();
          strRAId = gmDBManager.getString(11);
          /*
           * Here blLoanerSts is replaced with this condition due to During Returns initiation for
           * Bill Only -Sales Consignment it should be initiated like Bill Only -Loaner. so
           * blLoanerSts this condition will applicable only for Loaner Parts. So we are getting the
           * Order type from Rule and verifying the order type for Bill Only LOnaer [2530] and Bill
           * Only - Sales Consignment
           */
          if ((strRuleOrdType.indexOf("|" + strOrderType + "|") != -1)) {
            strLnorcn = strLnorcn.replace(", ,", ",");
            hmParam.put("LOANERRAID", strRAId);
            hmParam.put("LOANERINPUTSTR", strLnorcn);
          }
        }
        // for creating loaner child RA
        if (!strLoaner.equals("")) {
          strLoaner = strLoaner.replace(", ,", ",");
          hmParam.put("PARENTRAID", strRAId);
          hmParam.put("INPUTSTRING", strLoaner);
          hmReturn = saveReturnReconfDetails(gmDBManager, hmParam, strUsername);
          hmParam.put("LOANERRAID", hmReturn.get("CHILDRAID"));
          hmParam.put("LOANERINPUTSTR", strLoaner);
          blLoanerSts = true;
        }
        // to credit RA automatically and clear the re process, 3317: Loaner to consign swap
        if (((strRuleOrdType.indexOf("|" + strOrderType + "|") != -1) && (!strReason.equals("3317")))
            || !strLoaner.equals("")) {// PMT-4534 -- restricted to Loaner to
          // COnsign Swap
          saveLoanerOrderReturns(gmDBManager, hmParam);
        }
      }
      if (!strUsedLotStr.equals("")) {
        GmOrderUsageBean gmOrderUsageBean = new GmOrderUsageBean(getGmDataStoreVO());
        gmOrderUsageBean
            .returnUsedLot(strOrderId, strRAId, strUsedLotStr, strUsername, gmDBManager);
      }
      gmDBManager.commit();
    }
    return strRAId;
  } // end of saveOrderReturns

  /**
   * saveLoanerOrderReturns - This method will be used to credit RA automatically and clear the
   * reprocess
   * 
   * @return HashMap
   * @exception AppError
   **/
  public void saveLoanerOrderReturns(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    // PMT-4534 -- DbConnection Wrapper is changed to GmDBManager class
    String strRAId = GmCommonClass.parseNull((String) hmParam.get("LOANERRAID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strRetDt = GmCommonClass.parseNull((String) hmParam.get("RETDATE"));
    String strDateFmt = getCompDateFmt();// GmCommonClass.parseNull((String)
                                         // hmParam.get("DATEFORMAT"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("LOANERINPUTSTR"));
    String strUsername = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    // for creating consignment or loaner RA
    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_SAV_ORDER_LOANER_RETURN", 7);
    gmDBManager.setString(1, strRAId);
    gmDBManager.setString(2, strInputString);
    gmDBManager.setString(3, strRetDt);
    gmDBManager.setString(4, "1");
    gmDBManager.setString(5, strComments);
    gmDBManager.setString(6, strOrderId);
    gmDBManager.setString(7, strUsername);
    gmDBManager.execute();
  } // end of saveLoanerOrderReturns

  public ArrayList fetchRequestsForSet(String strSetID) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_reprocess.gm_op_fch_request_for_set", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strSetID);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return (ArrayList) rdResult.getRows();
  }

  public void sendRAStatusEmail(HashMap hmHashMap) throws AppError {
    // *************** send email when initiate/credit
    HashMap hmReturn = new HashMap();
    HashMap hmReturnDetails = (HashMap) hmHashMap.get("RADETAILS");
    String strSubject = "";
    String strHeaderMsg = "";
    String strCc = "";
    String associated_rep_email = "";
    String strAssociatedRA = "";
    String strAssociatedRep = "";
    String strPDEmail = "";
    String strRAId = GmCommonClass.parseNull((String) hmReturnDetails.get("RAID"));
    String strPDApproval = GmCommonClass.parseNull((String) hmHashMap.get("PDFL"));
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    String strHeaderName = "";
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    log.debug("Resource bundle bean in process trans bean===========>" + gmResourceBundleBean);
    String strDistributorEmailAdd =
        GmCommonClass.parseNull((String) hmReturnDetails.get("EMAILADD"));
    if (strPDApproval.equals("TRUE")) {// Only From Item Request Screen.
      strPDEmail = GmCommonClass.parseNull((String) hmReturnDetails.get("EMAIL_ID"));
      strDistributorEmailAdd = strDistributorEmailAdd + "," + strPDEmail;
    }
    String strName = GmCommonClass.parseNull((String) hmReturnDetails.get("REPNAME"));
    strName =
        strName.equals("") ? GmCommonClass.parseNull((String) hmReturnDetails.get("DNAME"))
            : strName;
    strDistributorEmailAdd =
        strDistributorEmailAdd + ","
            + GmCommonClass.parseNull((String) hmReturnDetails.get("CCLIST"));
    log.debug("strDistributorEmailAdd: RA email ========" + strDistributorEmailAdd);
    if (!strDistributorEmailAdd.equals("")) {
      GmJasperMail jasperMail = new GmJasperMail();
      GmEmailProperties emailProps = new GmEmailProperties();
      String strEmailPropName = GmCommonClass.parseNull((String) hmHashMap.get("EMAILFILENM"));
      String strJasperName = GmCommonClass.parseNull((String) hmHashMap.get("JASPERNAME"));
      // The Below code is added for PMT-5988.To get Customer Service Team mail id from E-mail
      // Template Properties.
      String strToEmailId =

      gmResourceBundleBean.getProperty(strEmailPropName + "." + GmEmailProperties.TO);
      strHeaderMsg =

      gmResourceBundleBean.getProperty(strEmailPropName + "." + GmEmailProperties.MESSAGE);
      strSubject =

      gmResourceBundleBean.getProperty(strEmailPropName + "." + GmEmailProperties.SUBJECT);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<RAID>", strRAId);
      strCc = gmResourceBundleBean.getProperty(strEmailPropName + "." + GmEmailProperties.CC);
      strHeaderName = gmResourceBundleBean.getProperty(strEmailPropName + "." + GmEmailProperties.EMAIL_HEADER_NM);
      associated_rep_email =
          GmCommonClass.parseNull((String) hmReturnDetails.get("ASSOCIATED_REP_EMAIL"));
      if (!associated_rep_email.equals("")) {
        strCc = strCc.concat(",").concat(associated_rep_email);
      }

      associated_rep_email =
          GmCommonClass.parseNull((String) hmReturnDetails.get("ASSOCIATED_REP_EMAIL"));
      associated_rep_email =
          GmCommonClass.parseNull((String) hmReturnDetails.get("ASSOCIATED_REP_EMAIL"));
      emailProps.setCc(strCc);
      emailProps.setEmailHeaderName(strHeaderName);
      // The below code is added for PMT-5988 to get set customer service team mail ID.
      if (strPDApproval.equals("TRUE")) {
        strDistributorEmailAdd = strDistributorEmailAdd + ',' + strToEmailId;
      }
      emailProps.setRecipients(strDistributorEmailAdd);
      emailProps.setMimeType(gmResourceBundleBean.getProperty(strEmailPropName + "."
          + GmEmailProperties.MIME_TYPE));
      emailProps.setSender(gmResourceBundleBean.getProperty(strEmailPropName + "."
          + GmEmailProperties.FROM));
      emailProps.setSubject(strSubject);
      strAssociatedRA =
          GmCommonClass.parseNull((String) hmReturnDetails.get("ASSOCIATED_REP_EMAIL"));

      strAssociatedRA = GmCommonClass.parseNull((String) hmReturnDetails.get("ASSOCIATED_RMA_ID"));
      if (!strAssociatedRA.equals("")) {
        strAssociatedRep =
            GmCommonClass.parseNull((String) hmReturnDetails.get("ASSOCIATED_REP_NAME"));

        strHeaderMsg = "The following parts were returned by ";
        strHeaderMsg = strHeaderMsg.concat(strAssociatedRep);
        strHeaderMsg =
            strHeaderMsg
                .concat(". Those parts were originally consigned to you and will be credited to you, ");
        strHeaderMsg = strHeaderMsg.concat(strName + ".");
        log.debug("strHeaderMsg" + strHeaderMsg);
      }
      hmReturnDetails.put("EMAILHEADER", strHeaderMsg);
      hmReturnDetails.put("SUBRAITEMDETAILLIST", hmHashMap.get("RAITEMDETAILS"));
      hmReturnDetails.put("SUBRATAG", hmHashMap.get("ASSRACRITEMDETAILS"));
      log.debug("SUBRACREDITITEMLIST=======" + hmHashMap.get("RACRITEMDETAILS"));
      hmReturnDetails.put("SUBRACREDITITEMLIST", hmHashMap.get("RACRITEMDETAILS"));
      hmReturnDetails.put("ISCEDIT", hmHashMap.get("ISCREDIT"));
      hmReturnDetails.put("BARPRINT", hmHashMap.get("BARPRINT"));
      log.debug("location = " + hmHashMap.get("SUBREPORT_DIR"));
      hmReturnDetails.put("SUBREPORT_DIR", hmHashMap.get("SUBREPORT_DIR"));
      hmReturnDetails.put("APPLNDATEFMT", strApplnDateFmt);

      jasperMail.setJasperReportName(strJasperName);
      jasperMail.setAdditionalParams(hmReturnDetails);
      jasperMail.setReportData(null);
      jasperMail.setEmailProperties(emailProps);
      hmReturn = jasperMail.sendMail();
      log.debug("SUBRACREDITITEMLIST hmReturn hmReturn hmReturn=======" + hmReturn);

    }
  }

  /**
   * This method is newly added for PMT-5988. saveInitiate - Initiate a return for the Item Returns
   * This method will be called from device ,whenever there is an Initiate return for Item request.
   * 
   * @param Hash Map hmParam
   * @return
   * @throws AppError
   * @throws SQLException
   */
  public HashMap saveInitiate(GmDBManager gmDBManager, HashMap hmParam, String strUserID)
      throws AppError {

    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strEdate = GmCommonClass.parseNull((String) hmParam.get("EXPDATE"));
    String strReason = GmCommonClass.parseNull((String) hmParam.get("REASON"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strAccID = GmCommonClass.parseNull((String) hmParam.get("EMPNAME"));
    String strPartQty = GmCommonClass.parseNull((String) hmParam.get("PARTQTY"));
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
    String strSetNo = GmCommonClass.parseNull((String) hmParam.get("SETNO"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOGREASON"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPrepareQuery = null;
    String strRAID = "";
    HashMap hmReturn = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    strReason = "3313";

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_RETURN", 10);
    gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strDistID);
    gmDBManager.setString(2, strEdate);
    gmDBManager.setString(3, strReason);
    gmDBManager.setString(4, strUserID);
    gmDBManager.setString(5, strType);
    gmDBManager.setString(6, strAccID);
    gmDBManager.setString(7, strPartQty);
    gmDBManager.setString(8, strPartID);
    gmDBManager.setString(9, strSetNo);
    gmDBManager.execute();
    strRAID = gmDBManager.getString(10);

    if (strLogReason != null && !strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strRAID, strLogReason, strUserID, "1216");
    }
    gmTxnSplitBean.checkAndSplitRA(gmDBManager, strRAID, strUserId);

    hmReturn.put("RAID", strRAID);
    hmReturn.put("MESSAGE", "RA initiated successfully");

    return hmReturn;
  }

  /*
   * saveItemLotNum - Method to call the procedure to validate the lot numbers and to save it
   * 
   * @param HashMap
   * 
   * @return String
   * 
   * @exception AppError
   */
  public String saveReturnLotNum(HashMap hmParams, String strUserID) throws AppError {

    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("RAID"));
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strErrFl = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.gm_sav_return_lot_num", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, "101723");// Control Number Attribute
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, strUserID);
    gmDBManager.setString(6, "93343");// pick
    gmDBManager.execute();
    strErrFl = GmCommonClass.parseNull(gmDBManager.getString(7));

    gmDBManager.commit();
    return strErrFl;
  }

  /*
   * saveReturnReconfStatus - Method to call the procedure to validate the lot numbers
   * 
   * @param HashMap
   * 
   * @return String
   * 
   * @exception AppError
   */
  public void saveReturnReconfStatus(HashMap hmParams, String strUserID) throws AppError {

    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("RAID"));
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strErrFl = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.gm_sav_return_reconf_lot_num", 6);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, "101723");// Control Number Attribute
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, strUserID);
    gmDBManager.setString(6, "93343");// pick
    gmDBManager.execute();

    gmDBManager.commit();
  }
  
  /**
   * This method is newly added for PC-3847
   * Include Requestor to receive RA Initiated notification. saveInitiate - Initiate a return for the Item Returns with sales rep id
   * This method will be called from device ,whenever there is an Initiate return for Item request.
   * 
   * @param Hash Map hmParam
   * @return
   * @throws AppError
   * @throws SQLException
   */
  public HashMap saveInitiateWithRepId(GmDBManager gmDBManager, HashMap hmParam, String strUserID)
      throws AppError {

    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    String strDistID = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strEdate = GmCommonClass.parseNull((String) hmParam.get("EXPDATE"));
    String strReason = GmCommonClass.parseNull((String) hmParam.get("REASON"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strPartQty = GmCommonClass.parseNull((String) hmParam.get("PARTQTY"));
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
    String strSetNo = GmCommonClass.parseNull((String) hmParam.get("SETNO"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("LOGREASON"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strPrepareQuery = null;
    String strRAID = "";
    HashMap hmReturn = new HashMap();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    strReason = "3313";

    gmDBManager.setPrepareString("GM_PKG_OP_RETURN.GM_OP_SAV_RETURN", 10);
    gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strDistID);
    gmDBManager.setString(2, strEdate);
    gmDBManager.setString(3, strReason);
    gmDBManager.setString(4, strUserID);
    gmDBManager.setString(5, strType);
    gmDBManager.setString(6, strRepId);
    gmDBManager.setString(7, strPartQty);
    gmDBManager.setString(8, strPartID);
    gmDBManager.setString(9, strSetNo);
    gmDBManager.execute();
    strRAID = gmDBManager.getString(10);
    log.debug("strRAID: "+strRAID);
    if (strLogReason != null && !strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strRAID, strLogReason, strUserID, "1216");
    }
    gmTxnSplitBean.checkAndSplitRA(gmDBManager, strRAID, strUserId);

    hmReturn.put("RAID", strRAID);
    hmReturn.put("MESSAGE", "RA initiated successfully");

    return hmReturn;
  }


}
