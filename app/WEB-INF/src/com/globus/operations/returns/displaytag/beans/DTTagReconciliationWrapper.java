package com.globus.operations.returns.displaytag.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import javax.servlet.jsp.PageContext;
import org.displaytag.model.TableModel;
import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.operations.returns.forms.GmTagReconciliationForm;

public class DTTagReconciliationWrapper extends TableDecorator {
	private DynaBean db;
	private String strDistributor = "";
	private GmTagReconciliationForm gmTagReconciliationForm;
	private List alMTagReason;
	private Iterator alListIter;
	public StringBuffer strValue = new StringBuffer();
	private String partNum = "";
	private int intCount = 0;
	private String strRAID = "";
	private String strTYPE = "";
	private String strTYPEId="";
	private String strOptKey = "";
	private String strOptValue = "";
	private String strSetID = "";
	private String strDistID = "";
	private String strQty = "";
	private String strFlag = "";
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public DTTagReconciliationWrapper() {
		super();
	}
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmTagReconciliationForm = (GmTagReconciliationForm) getPageContext().getAttribute("frmTagReconciliation", PageContext.REQUEST_SCOPE);
		 alMTagReason = gmTagReconciliationForm.getAlMTagRList();
	}
	public String getRAID() {
		db = (DynaBean) this.getCurrentRowObject();
		strRAID = GmCommonClass.parseNull((String) db.get("RAID"));
		strFlag = GmCommonClass.parseNull((String) db.get("RLOG"));
		strValue.setLength(0);
		
		strValue.append("<img style='cursor:hand' src='");
		if(strFlag.equals("N"))
        {
			strValue.append(strImagePath+"/phone_icon.jpg'");
        }
        else
        {            
        	strValue.append(strImagePath+"/phone-icon_ans.gif'");
        }
		strValue.append(" title='Click to add comments' width='17' height='12'" ); 
		strValue.append(" onClick =\"javascript:fnOpenRALog('"+strRAID+"','1270');\" </img>");

		strValue.append(" <input type='hidden' name='hraid"+intCount+"' value='");
		strValue.append(strRAID+"'>");
		strValue.append("<a class=RightText href= javascript:fnViewReturns('"+strRAID+"');>"+strRAID+"</a>");		
		return strValue.toString();
	}
	public String getTYPE() {
		db = (DynaBean) this.getCurrentRowObject();
		strTYPE = GmCommonClass.parseNull((String) db.get("TYPE"));
		
		strValue.setLength(0);

		strValue.append(" <input type='hidden' name='htype"+intCount+"' value='");
		strValue.append(strTYPE+"'>");
		strValue.append(strTYPE);
		
		
		
		
		return strValue.toString();
	}
	
	public String getEXCESS_ID() {
		db = (DynaBean) this.getCurrentRowObject();
		String strExcess = GmCommonClass.parseNull((String) db.get("EXCESS_ID"));
		
		strValue.setLength(0);

		strValue.append(" <input type='hidden' name='hexcessid"+intCount+"' value='");
		strValue.append(strExcess+"'>");
		strValue.append("<a class=RightText href= javascript:fnPrintConsignVer('"+strExcess+"');>"+strExcess+"</a>");	
		return strValue.toString();
	}	
	public String getPNUM() {
		db = (DynaBean) this.getCurrentRowObject();
		String strPNUM = GmCommonClass.parseNull((String) db.get("PNUM"));
		
		strValue.setLength(0);

		strValue.append(" <input type='hidden' name='hpnum"+intCount+"' value='");
		strValue.append(strPNUM+"'>");
		strValue.append(strPNUM);
		return strValue.toString();
	}	
	public String getCTRL_NUM() {
		db = (DynaBean) this.getCurrentRowObject();
		String strCTRL_NUM = GmCommonClass.parseNull((String) db.get("CTRL_NUM"));
		
		strValue.setLength(0);

		strValue.append(" <input type='hidden' name='hctrlnum"+intCount+"' value='");
		strValue.append(strCTRL_NUM+"'>");
		strValue.append(strCTRL_NUM);
		return strValue.toString();
	}
	public String getTAGID() {
		db = (DynaBean) this.getCurrentRowObject();
		strTYPEId = GmCommonClass.parseNull((String) db.get("TYPE_ID").toString());
		strValue.setLength(0);

		strValue.append(" <input type='hidden' name='htypeId"+intCount+"' value='");
		strValue.append(strTYPEId+"'>");

		strValue.append(" <input class=RightText type='text' ");
		strValue.append(" name='tagID");
		strValue.append(intCount);
		strValue.append("' value='' size='10' >");
		//intCount++;
		return strValue.toString();
	}
	public String getREASON(){
		StringBuffer strValue = new StringBuffer();
		HashMap hmReasonList = new HashMap();
		strValue.setLength(0);
		db = (DynaBean) this.getCurrentRowObject();
		strSetID = "";
		strDistID = "";
		strQty = "";
		if(db.get("SETID")!=null){
			strSetID = GmCommonClass.parseNull((String) db.get("SETID").toString());
		}
		
		strValue.append(" <input type='hidden' name='hsetId"+intCount+"' value='");
		strValue.append(strSetID+"'>");
		
		if(db.get("DISTID")!=null){
			strDistID = GmCommonClass.parseNull((String) db.get("DISTID").toString());
		}
		strValue.append(" <input type='hidden' name='hdistId"+intCount+"' value='");
		strValue.append(strDistID+"'>");
		
		if(db.get("QTY")!=null){
			strQty = GmCommonClass.parseNull((String) db.get("QTY").toString());
		}
		
		strValue.append(" <input type='hidden' name='hQty"+intCount+"' value='");
		strValue.append(strQty+"'>");
		
		String strReason ="";
		if(db.get("REASON")!=null){
			strReason =(String)db.get("REASON").toString();//GmCommonClass.parseNull(db.get("REASON").toString())
		}
		alListIter = gmTagReconciliationForm.getAlMTagRList().iterator();
		strValue.append("<select name='cbo_reason");
		strValue.append(String.valueOf(intCount));
		strValue.append("'> ");
		strValue.append(" <option value='0'>[Choose One] </option>");
		while (alListIter.hasNext()) {
			hmReasonList = (HashMap) alListIter.next();
			strOptKey = (String) hmReasonList.get("CODEID");
			strOptValue = (String) hmReasonList.get("CODENM");
			if (strReason.equals(strOptKey)) {
				strOptKey += " selected";
			}
			strValue.append(" <option value= " + strOptKey + " > " + strOptValue + "</option>");
		}
		strValue.append(" </select>");
		intCount++;
		return strValue.toString();
	}
}