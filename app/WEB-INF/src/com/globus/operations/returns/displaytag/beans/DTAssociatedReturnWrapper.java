package com.globus.operations.returns.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTAssociatedReturnWrapper extends TableDecorator {
	private HashMap hmParam = new HashMap();
	private String strDistributor = "";
	private String strInitRA = "";
	private String strCreditRA = "";
	public DTAssociatedReturnWrapper() {
		super();
	}
	public String getINITRA() {
		hmParam = (HashMap) this.getCurrentRowObject();
		strInitRA = GmCommonClass.parseNull((String) hmParam.get("INITRA"));
				
		StringBuffer sbHtml = new StringBuffer();
		sbHtml.append("<a href= javascript:fnPrintReturnDetails('");
		sbHtml.append(strInitRA);
		sbHtml.append("');> <img src=/images/product_icon.gif alt='Print Version'> </a>");
		sbHtml.append(strInitRA);        
		return sbHtml.toString();	
	}
	
	public String getCREDITRA() {
		hmParam = (HashMap) this.getCurrentRowObject();
		strCreditRA = GmCommonClass.parseNull((String) hmParam.get("CREDITRA"));
				
		StringBuffer sbHtml = new StringBuffer();
		sbHtml.append("<a href= javascript:fnPrintReturnDetails('");
		sbHtml.append(strCreditRA);
		sbHtml.append("');> <img src=/images/product_icon.gif alt='Print Version'> </a>");
		        sbHtml.append(strCreditRA);        
		return sbHtml.toString();	
	}
	

}