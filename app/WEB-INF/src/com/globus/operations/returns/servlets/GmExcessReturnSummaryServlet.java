package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.returns.beans.GmExcessReturnsBean;

public class GmExcessReturnSummaryServlet extends GmReturnsServlet {
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmExcessReturnsBean gmExcessReturnsBean = new GmExcessReturnsBean(getGmDataStoreVO());
    RowSetDynaClass rdExcessReturnSummary;
    HashMap hmParam = new HashMap();
    String strDistributorId = "";
    String strStatus = "";
    String strRAID = "";
    String strConsignId = "";



    try {
      ArrayList alDistributor = GmCommonClass.parseNullArrayList(gmExcessReturnsBean.getDistributorList(hmParam));
      ArrayList alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("OPCLO"));

      strDistributorId = GmCommonClass.parseNull(request.getParameter("Cbo_DistId"));
      strStatus =
          GmCommonClass.parseNull(request.getParameter("Cbo_Status")).equals("") ? "90175"
              : request.getParameter("Cbo_Status"); // 90175 is Open Status
      strRAID = GmCommonClass.parseNull(request.getParameter("Txt_RAID"));
      strConsignId = GmCommonClass.parseNull(request.getParameter("Txt_ConsignId"));

      hmParam.put("DISTID", strDistributorId);
      hmParam.put("STATUS", strStatus);
      hmParam.put("RAID", strRAID);
      hmParam.put("CONSIGNID", strConsignId);

      log.debug(" valus in param " + hmParam);

      rdExcessReturnSummary = gmExcessReturnsBean.loadExcessReturnSummary(hmParam);

      request.setAttribute("RDEXCESSRETURNSUMMARY", rdExcessReturnSummary);
      request.setAttribute("HMPARAM", hmParam);
      request.setAttribute("ALDISTLIST", alDistributor);
      request.setAttribute("ALSTATUS", alStatus);

      // Return Reconciliation Excel download report
      String strExcel = GmCommonClass.parseNull(request.getParameter("hExcel"));
      if (strExcel.equals("Excel")) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition",
            "attachment;filename=GmExcessReturnSummaryServlet.xls");
      }

      String strDispatchTo = strReturnsPath.concat("/GmExcessReturnSummary.jsp");
      dispatch(strDispatchTo, request, response);
    }

    catch (Exception exp) {
      session.setAttribute("hAppErrorMessage", exp.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }
  }
}
