/**
 * 
 */
package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmSalesCreditBean;
import com.globus.operations.returns.beans.GmBBAProcessReturnsProcessInterface;
import com.globus.operations.returns.beans.GmBBAReturnSummaryPicSlip;
import com.globus.operations.returns.beans.GmProcessReturnsBean;

/**
 * @author vprasath
 * 
 */
public class GmPrintCreditServlet extends GmReturnsServlet {

  /**
     * 
     */
  public GmPrintCreditServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    log.debug("Enter GmInitClosureServlet");
    instantiate(request, response);
    GmSalesCreditBean gmSalesCredit = new GmSalesCreditBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    HashMap hmCompCurrency =
        GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    String strCompCurrencySymbol =
        GmCommonClass.parseNull((String) hmCompCurrency.get("CMPCURRSMB"));
    String strCompCurrencyFormat =
        GmCommonClass.parseNull((String) hmCompCurrency.get("CMPCURRFMT"));

    // Local Variables Used
    String strRAID = GmCommonClass.parseNull(request.getParameter("hRAID"));
    String strDispatchTo =
        strReturnsPath.concat(GmCommonClass.parseNull(gmResourceBundleBean
            .getProperty("CREDIT_SUMMARY.DISPATCHTO")));
    String strOusReturnSummary =
        GmCommonClass.parseNull(gmResourceBundleBean
            .getProperty("CREDIT_SUMMARY.OUS_RETURNSUMMARY"));
    String strOrderID =
        request.getParameter("hOrderId") == null ? "" : request.getParameter("hOrderId");
    String strAsstFlag = GmCommonClass.parseNull(request.getParameter("hAsstFlag")); // For
                                                                                     // Associated
                                                                                     // Return
                                                                                     // Report
    String strHideBtn = GmCommonClass.parseNull(request.getParameter("hideBtn"));
    String strCountryId = GmCommonClass.parseNull(request.getParameter("countryid"));
    String strNativeCurrency = "USD";
    log.debug("strHideBtn" + strHideBtn);
    // Return Data Stores
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    // Beans Initialization
    GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
    try {
      if (!strOrderID.equals("")) {
        strRAID = gmSalesCredit.getRAID(strOrderID);
      }
      if (strAction.equals("PRINT")) {
        if (strAsstFlag.equals("true")) {
          hmReturn = gmProcessReturnsBean.loadCreditAsstRAReport(strRAID, strAction);
        } else {
          if (strOusReturnSummary.equalsIgnoreCase("YES")) {
            // Country ID not equal to null and not equal to US, it is only for OUS.
            hmParam.put("TXNID", strRAID);
            hmParam.put("ACTION", strAction);
            hmReturn = gmProcessReturnsBean.loadCreditReport(strRAID, strAction);
            // For ous loading native currency from rules.
            strNativeCurrency = strCompCurrencySymbol;
            // GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCountryId, "CNTYCURR"));
            // strNativeCurrency = strNativeCurrency.equals("")? "USD":strNativeCurrency;
            request.setAttribute("CURRENCY", strNativeCurrency);
            // For ous dispatching to New JSP.
            strDispatchTo = strReturnsPath.concat("/GmOUSCreditMemoPrint.jsp");
          } else {
            /*
             * The following code is added for showing the Domain based Return Summary. For BBA, we
             * are calling the GmBBAProcessReturnsBean.java and for US, we are using the
             * GmUSProcessReturnsBean.java to get the RA details.
             */
            String strClassName =
                GmCommonClass.parseNull(gmResourceBundleBean
                    .getProperty("CREDIT_SUMMARY.RAPICSLIP_CLASS"));

            GmBBAProcessReturnsProcessInterface returnSummaryClient =
                GmBBAReturnSummaryPicSlip.getInstance(strClassName);
            hmReturn = returnSummaryClient.loadCreditReport(strRAID, strAction);
            ArrayList AlLogReasons = GmCommonClass.parseNullArrayList((ArrayList)gmCommonBean.getLog(strRAID, "26240618"));
            request.setAttribute("LOGREASON", AlLogReasons);
          }
        }

      } else if (strAction.equals("VIEW")) {
        hmReturn = gmProcessReturnsBean.loadCreditReport(strRAID, "ViewReturn");

        strDispatchTo = strReturnsPath.concat("/GmReturnView.jsp");
      }
      hmReturn.put("COMPANYLOCALE", strCompanyLocale);
      hmReturn.put("COMPCURRENCYSYMBOL", strCompCurrencySymbol);
      hmReturn.put("COMPCURRENCYFORMAT", strCompCurrencyFormat);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hideBtn", strHideBtn);
      dispatch(strDispatchTo, request, response);
    } catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }
  }
}
