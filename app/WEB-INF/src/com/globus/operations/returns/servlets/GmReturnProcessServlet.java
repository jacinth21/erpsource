package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmCacheManager;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.operations.returns.beans.GmReturnReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmReturnProcessServlet extends GmReturnsServlet {
  String strOperPath = GmCommonClass.getString("GMOPERATIONS");


  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // local varaibles used
    String strHID = GmCommonClass.parseNull(request.getParameter("hId"));
    String strReprocessDate = GmCommonClass.parseNull(request.getParameter("ReprocessDate"));
    String strRAIDs = GmCommonClass.parseNull(request.getParameter("hRAIDs"));
    String strSetID = GmCommonClass.parseNull(request.getParameter("Cbo_SetID"));
    String strCNStr =GmCommonClass.parseNull(request.getParameter("hCNStr"));
    String strQNStr = GmCommonClass.parseNull(request.getParameter("hQNStr"));
    /*Add Sterile String by getting QNSterileStr from JSP FILE*/
    String QNSterileStr = GmCommonClass.parseNull(request.getParameter("hQNSterileStr"));
    String strPNStr =GmCommonClass.parseNull(request.getParameter("hPNStr"));
    String strIAStr = GmCommonClass.parseNull(request.getParameter("hIAStr"));
    String strIHStr = GmCommonClass.parseNull(request.getParameter("hIHStr"));
    String strFGStr = GmCommonClass.parseNull(request.getParameter("hFGStr"));
    String strRequestID = GmCommonClass.parseNull(request.getParameter("hRequestID"));
    String strDispatchTo = strOperPath.concat("/GmReturnsReprocess.jsp");
    HashMap hmParam = new HashMap();
    try {
      log.debug("Enter");
      instantiate(request, response);

      // Data Store
      HashMap hmReturn = new HashMap();
      ArrayList alReturnDetails = new ArrayList();
      HashMap hmResult = new HashMap();
      HashMap hmHeader = new HashMap();
      ArrayList alRequests = new ArrayList();
      // strAction = new String();


      // Bean Initialization
      GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
      GmReturnReportBean gmReturnReportBean = new GmReturnReportBean(getGmDataStoreVO());
      GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());

      strHID = GmCommonClass.parseNull(request.getParameter("hId"));
      if (!strReprocessDate.equals("")) {
        throw new AppError(new SQLException("Error Occured", null, 20943));
      }
      hmHeader = gmReturnReportBean.loadReturnHeaderInfo(strHID);
    log.debug("hmHeader----->>"+hmHeader);
      if (strSetID.equals("")
          && !GmCommonClass.parseNull((String) hmHeader.get("SETID")).equals("")) {
        strSetID = (String) hmHeader.get("SETID");
      }

      if (strRAIDs.equals("") && !strHID.equals("")) {
        strRAIDs = strHID;
      }

      hmReturn.put("RADETAILS", hmHeader);

      alRequests = gmProcessReturnsBean.fetchRequestsForSet(strSetID);
      request.setAttribute("REQUESTS", alRequests);
      log.debug(" strHID " + strHID + " Set Id " + strSetID);
      String strComments = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));
      hmParam.put("RAID", strHID);
      hmParam.put("RAIDS", strRAIDs);
      hmParam.put("CNSTR", strCNStr);
      hmParam.put("QNSTR", strQNStr);
      /*put QnSterileStr to hm param*/
      hmParam.put("QNSterileStr", QNSterileStr);
      hmParam.put("PNSTR", strPNStr);
      hmParam.put("IASTR", strIAStr);
      hmParam.put("IHSTR", strIHStr);
      hmParam.put("SETID", strSetID);
      hmParam.put("USERID", strUserId);
      hmParam.put("REQUESTID", strRequestID);
      hmParam.put("COMMENTS", strComments);
      hmParam.put("FGSTR", strFGStr);

      if (strAction.equals("Save")) {
        hmResult = gmProcessReturnsBean.saveReturnReprocess(hmParam);
        throw new AppError(
            "The Following Reprocess IDs were generated as a result of this transaction: "
                + hmResult.get("RETURNIDS"), "", 'S');

      }
      if ((!strHID.equals("") && !strSetID.equals("") && strAction.equals("Reload"))) {
        log.debug("Set ID:" + strSetID);
        alReturnDetails = gmProcessReturnsBean.loadReprocessDetails(strHID, strSetID);
      
        hmReturn.put("RAITEMDETAILS", alReturnDetails);
      } else if ((!strRAIDs.equals("") && !strSetID.equals("") && strAction.equals("AddRA"))) {
        log.debug("Set ID:" + strSetID);
        alReturnDetails = gmProcessReturnsBean.loadReprocessDetails(strRAIDs, strSetID);
      
        hmReturn.put("RAITEMDETAILS", alReturnDetails);
      }
      log.debug("Action " + strAction + "...alReturnDetails.size()..." + alReturnDetails.size()
          + "..strDispatchTo = " + strDispatchTo);
      if (alReturnDetails.size() > 0 && (strAction.equals("Reload") || strAction.equals("AddRA"))) // for
                                                                                                   // rule
                                                                                                   // to
                                                                                                   // execute.
      {
        request.setAttribute("RE_FORWARD", "gmReturnsReProcess");
        request.setAttribute("RE_TXN", "RETURNSWRAPPER");
        request.setAttribute("txnStatus", "PROCESS");
        strDispatchTo = "/gmRuleEngine.do";
      }
      String strReturnQtyFl = GmCommonClass.parseNull((String)GmCommonClass.getRuleValueByCompany("RET_DEF_QTY_FL", "RET_DEF_QTY",getGmDataStoreVO().getCmpid()));
      log.debug("strReturnQtyFl=="+strReturnQtyFl);
      hmReturn.put("RETURNDEFQTYFL", strReturnQtyFl);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("SETS", gmProjectBean.loadSetMap("1601"));
      dispatch(strDispatchTo, request, response);
      log.debug("Exit");
    } catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      dispatch(strComnErrorPath, request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }// End of catch
  }
}
