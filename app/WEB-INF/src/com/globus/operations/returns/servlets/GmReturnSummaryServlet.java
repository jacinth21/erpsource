package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.returns.beans.GmReturnReportBean;

public class GmReturnSummaryServlet extends GmReturnsServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmReturnReportBean gmReturnReportBean = new GmReturnReportBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    ArrayList alDistList;
    ArrayList alType = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alComparison = new ArrayList();
    ArrayList alReason = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alAccountList = new ArrayList();
    ArrayList alChangeList = new ArrayList();
    RowSetDynaClass rsResultSet;
    List lResult = null;

    String strQAAccess = "";
    String strRecReadAccess = "";
    String strDistId = GmCommonClass.parseZero(request.getParameter("Cbo_DistId"));
    String strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));
    String strStatus = GmCommonClass.parseNull(request.getParameter("Cbo_Status"));
    String strRAID = GmCommonClass.parseNull(request.getParameter("Txt_RAID"));
    String strParentRAID = GmCommonClass.parseNull(request.getParameter("Txt_ParentRAID"));
    String strRefID = GmCommonClass.parseNull(request.getParameter("Txt_RefID"));
    String strElapse = GmCommonClass.parseNull(request.getParameter("Txt_Elapse"));
    String strComparison = GmCommonClass.parseNull(request.getParameter("Cbo_Comparison"));
    String strReason = GmCommonClass.parseNull(request.getParameter("Cbo_Reason"));
    String strDateType = GmCommonClass.parseNull(request.getParameter("Cbo_DateType"));
    String strFrmDate = GmCommonClass.parseNull(request.getParameter("Txt_FrmDate"));
    String strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
    String strSetId = GmCommonClass.parseNull(request.getParameter("Txt_SetID"));
    String strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
    String strRepId = GmCommonClass.parseNull(request.getParameter("Cbo_RepName"));
    String strAcctId = GmCommonClass.parseNull(request.getParameter("Cbo_AcctId"));
    String strUserId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    String strPrcessFl = GmCommonClass.parseZero(request.getParameter("ProcessTransFl"));
    String strPrcessID = GmCommonClass.parseZero(request.getParameter("hRAId"));
    String strRaCount = GmCommonClass.parseZero(request.getParameter("RaCount"));
    HashMap hmParam = new HashMap();
    HashMap hmQAAccess = new HashMap();
    HashMap hmRRAccess = new HashMap();

    try {
      alDistList = new ArrayList(gmCommonBean.getDistributorList());
      alType = GmCommonClass.getCodeList("RETTY");
      alStatus = GmCommonClass.getCodeList("RETST");
      alComparison = GmCommonClass.getCodeList("CPRSN"); // Comparison < , > , =
      alReason = GmCommonClass.getCodeList("RETRE");
      alRepList = gmCustomerBean.getRepList();
      alAccountList = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
      // For Choose Action Dropdown to convert RA reason to QA Evaluvation.
      alChangeList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CRARSN"));
      // Access for changing RA reason to QA evaluvation to the Quality users.
      hmQAAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "CONVERT_RA_ACCESS"));
      // Read only Access for Receive Returns
      hmRRAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "REC_RET_READ_ACCESS"));
      // Based on this Access flag Quality users can Access the Choose Action Dropdown.
      strQAAccess = GmCommonClass.parseNull((String) hmQAAccess.get("UPDFL"));
      strRecReadAccess = GmCommonClass.parseNull((String) hmRRAccess.get("UPDFL"));

      hmParam.put("DISTID", strDistId);
      hmParam.put("TYPE", strType);
      hmParam.put("STATUS", strStatus);
      hmParam.put("RAID", strRAID);
      hmParam.put("PARENTRAID", strParentRAID);
      hmParam.put("REFID", strRefID);
      hmParam.put("ELAPSE", strElapse);
      hmParam.put("ELAPSECOMP", strComparison);
      hmParam.put("REASON", strReason);
      hmParam.put("DATETYPE", strDateType);
      hmParam.put("FRMDATE", strFrmDate);
      hmParam.put("TODATE", strToDate);
      hmParam.put("SETID", strSetId);
      hmParam.put("PARTNUM", strPartNum);
      hmParam.put("REPID", strRepId);
      hmParam.put("ACCOUNTID", strAcctId);
      hmParam.put("QAACCESS", strQAAccess);
      hmParam.put("RRACCESS", strRecReadAccess);
      hmParam.put("DATEFORMAT", strApplnDateFmt);
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

      log.debug("hmParam is " + hmParam);

      // created velu Aug 17
      if (strAction.equals("Report")) {
        lResult = gmReturnReportBean.loadReturnSummary(hmParam);
        request.setAttribute("rsResultSet",
            gmReturnReportBean.getXmlGridData((ArrayList) lResult, hmParam));
      }
      // end code
      request.setAttribute("STRRACOUNT", strRaCount);
      request.setAttribute("STRPRCESSFL", strPrcessFl);
      request.setAttribute("STRPRCESSID", strPrcessID);
      request.setAttribute("ALDISTLIST", alDistList);
      request.setAttribute("ALTYPE", alType);
      request.setAttribute("ALSTATUS", alStatus);
      request.setAttribute("ALCOMPARISON", alComparison);
      request.setAttribute("ALREASON", alReason);
      request.setAttribute("ALREPLIST", alRepList);
      request.setAttribute("HMPARAM", hmParam);
      request.setAttribute("ALACCOUNTLIST", alAccountList);
      request.setAttribute("ALCHANGELIST", alChangeList);

      String strDispatchTo = strReturnsPath.concat("/GmReturnSummaryList.jsp");
      dispatch(strDispatchTo, request, response);
    }

    catch (Exception exp) {
      session.setAttribute("hAppErrorMessage", exp.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }
  }

}
