/**
 * 
 */
package com.globus.operations.returns.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;

/**
 * @author vprasath
 * 
 */
public class GmReturnsServlet extends GmServlet {
  protected String strReturnsPath = GmCommonClass.getString("GMRETURNS");

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  protected String getInputString(HttpServletRequest request) {
    String strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
    int intCount = Integer.parseInt(strCount);

    String strInputString = "";
    String strPartNum = "";
    String strQty = "";
    String strControl = "";
    String strOrgControl = "";
    // String strOrgControl = "";
    String strItemType = "";
    String tagFlag = "";
    String strUnitPrice = "";
    String strUnitPriceAdj = "";
    String strAdjCode = "";
    double intQty = 0;


    for (int i = 0; i <= intCount; i++) {
      strPartNum =
          request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum" + i);

      if (!strPartNum.equals("")) {
        tagFlag = request.getParameter("strTagFl" + i);
        strQty = GmCommonClass.parseNull(request.getParameter("Txt_Qty" + i));
        strQty = strQty.equals("") ? "0" : strQty;
        strControl = GmCommonClass.parseNull(request.getParameter("Txt_CNum" + i));
        strOrgControl = GmCommonClass.parseNull(request.getParameter("hControl" + i));
        strControl = strControl.equals("") ? strOrgControl : strControl;
        strControl = strControl.equals("") ? " " : strControl;
        String strPrice = GmCommonClass.parseNull(request.getParameter("hItemPrice" + i));
        strItemType = GmCommonClass.parseNull(request.getParameter("hItemType" + i));
        strUnitPrice = GmCommonClass.parseNull(request.getParameter("hUnitPrice" + i));
        strUnitPriceAdj = GmCommonClass.parseNull(request.getParameter("hUnitPriceAdj" + i));
        strAdjCode = GmCommonClass.parseNull(request.getParameter("hAdjCode" + i));
        intQty = Double.parseDouble(strQty);
        if (tagFlag.equals("Y") & intQty > 1) {
          strQty = "1";
          for (int j = 1; j <= intQty; j++) {
            strInputString =
                strInputString + strPartNum + "," + strQty + "," + strControl + "," + " " + ","
                    + strItemType + "," + strPrice + "," + strUnitPrice+ "," + strUnitPriceAdj+ "," + strAdjCode+ "|";
          }

        } else {
          strInputString =
              strInputString + strPartNum + "," + strQty + "," + strControl + "," + " " + ","
                  + strItemType + "," + strPrice + "," + strUnitPrice+ "," + strUnitPriceAdj+ "," + strAdjCode+ "|";
        }
      }
    }
    return strInputString;
  }
}
