/**
 * 
 */
package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmSalesCreditBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.operations.returns.beans.GmReturnReportBean;

/**
 * @author vprasath
 * 
 */
public class GmReturnCreditServlet extends GmReturnsServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    log.debug("Enter GmReturnCreditServlet");

    HttpSession session = request.getSession(false);
    String strLogisticsPath = GmCommonClass.getString("GMLOGISTICS");
    String strDispatchTo = strLogisticsPath.concat("/GmReturnForCredit.jsp");


    checkSession(response, session);
    instantiate(request, response);

    // Local Variables Used
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strRAID = GmCommonClass.parseNull(request.getParameter("hRAId"));
    Date dtCreditDate = getDateFromStr(request.getParameter("Txt_CreditDate"));
    String strStatus = GmCommonClass.parseNull(request.getParameter("Chk_RAComplete"));
    String strUserComments = GmCommonClass.parseNull(request.getParameter("Txt_Comments"));
    String strType = GmCommonClass.parseNull((request.getParameter("CTYPE")));
    String strCreditString = GmCommonClass.parseNull(request.getParameter("hCreditString"));
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    // Need to check if the source from accouting or sales
    String strFrom = GmCommonClass.parseNull(request.getParameter("hFrom"));
    String strUserId = (String) session.getAttribute("strSessUserId");
    int intStatus = strStatus.equals("") ? 1 : Integer.parseInt(strStatus);
    String strAcctId = GmCommonClass.parseNull(request.getParameter("strAcctID"));
    String strRefId = GmCommonClass.parseNull(request.getParameter("REFID"));
    GmSalesCreditBean gmSalesCredit = new GmSalesCreditBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean();
    // Return Data Stores
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strMsg = "";
    HashMap hmResult = new HashMap();

    // Beans Initialization
    GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
    GmReturnReportBean gmReturnReportBean = new GmReturnReportBean(getGmDataStoreVO());

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    // Locale
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));
    
    HashMap hmAccess = new HashMap();
    String strReadFl = "";
    String strUpdFl = "";
    String strVoidFl = "";
    String strInvoiceId = "";
    try {
    	System.out.println("strFrom"+strFrom);
    	System.out.println("strAction"+strAction);
    	System.out.println("strCreditString"+strCreditString);

      if (strFrom.equals("AccountsReturnsDashboard") && strAction.equals("SaveCredit")) {
        strInvoiceId =
            gmSalesCredit.saveCreditforReturns(strRAID, dtCreditDate, strUserComments, strUserId,
                strCreditString);
        
    
        
        // Tax
        GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean(getGmDataStoreVO());
        String strErrorPO = gmSalesTaxTransBean.updateSalesTax(strInvoiceId, strUserId);
        log.debug(" strErrorPO --> " + strErrorPO);
        if (!strErrorPO.equals("")) {
          // Roll back the RA to pending returns
          gmSalesTaxTransBean.rollBackReturns(strRAID, strUserId);
          throw new AppError("", "20667");
        }
        String strGenerateXmlFl = gmResourceBundleBean.getProperty("INVOICE.GENERATE_XML");
        // if Company is creating the XML file
        if (strGenerateXmlFl.equals("Y") && !strInvoiceId.equals("")) {
          GmInvoiceXmlInterface gmInvoiceXmlInterface =
              (GmInvoiceXmlInterface) GmCommonClass.getSpringBeanClass("xml/Invoice_Xml_Beans.xml",
                  getGmDataStoreVO().getCmpid());

          gmInvoiceXmlInterface.generateInvocieXML(strInvoiceId, strUserId);
        }
        hmReturn = gmProcessReturnsBean.loadCreditReport(strRAID, "ViewReturn");

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", "PrintMemo");
        strDispatchTo = strReturnsPath.concat("/GmReturnView.jsp");

      } else if (strAction.equals("SaveCredit")) {
        hmResult = gmProcessReturnsBean.loadReturnsConsignForCredit(strRAID, strFrom);
        
        alReturn = (ArrayList) hmResult.get("RACRITEMDETAILS");


        strMsg =
            gmProcessReturnsBean.saveReturnsConsignForCredit(strRAID, dtCreditDate, intStatus,
                strUserId);
        // JMS call to update the Acct Net Qty by Lot
        // PMT- 14376
        // 3304: Account Consignment Item Return
        System.out.println("Return Type"+strType);
       
        	        
        HashMap hmAcctParam = new HashMap();
        //Ref ID -> Pass the Account ID
        //Trans ID -> Pass the Transaction ID
       // hmAcctParam.put("REFID", strAcctId);
        hmAcctParam.put("TRANS_ID", strRAID);
        hmAcctParam.put("USER_ID", strUserId);
        hmAcctParam.put("TRANS_TYPE", "3304");
        hmAcctParam.put("WAREHOUSE_TYPE", "5");
        hmAcctParam.put("COMPANY_INFO",strCompanyInfo);
        gmInvLocationBean.updateLoanerStockSheetInfo(hmAcctParam);
        
               
        request.setAttribute("MESSAGE", strMsg);
        HashMap hmMap = gmReturnReportBean.loadReturnHeaderInfo(strRAID);
        hmMap.put("APPLNDATEFMT", strApplnDateFmt);

        hmReturn.put("RADETAILS", hmMap);
        hmReturn.put("RACRITEMDETAILS", alReturn);
        hmReturn.put("ASSRACRITEMDETAILS", hmResult.get("ASSRACRITEMDETAILS"));
        hmReturn.put("EMAILFILENM", "GmRACreditEmail");
        hmReturn.put("JASPERNAME", "/GmRAStatusEmail.jasper");
        hmReturn.put("ISCREDIT", "isCredit");
        hmReturn.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        gmProcessReturnsBean.sendRAStatusEmail(hmReturn);
        // //To Update redis key to show add set field (PMT-35098)
        GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
        log.debug("DISTID---->>"+hmMap.get("DISTID"));
        gmKitMappingBean.removeSetKey(GmCommonClass.parseNull((String) hmMap.get("DISTID")));
        throw new AppError("RETURN " + strRAID + " CREDITED SUCCESSFULLY", "", 'S');
      }

      if (!strAction.equals("SaveCredit")) {
        if (strFrom.equals("AccountsReturnsDashboard")) {
          hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  "PEND_AC_CRD_ACCESS"));
          strReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
          strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          strVoidFl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
          if (!strReadFl.equals("Y") || !strUpdFl.equals("Y")) {
            throw new AppError("", "20685");
          }
        }
        hmResult = gmProcessReturnsBean.loadReturnsConsignForCredit(strRAID, strFrom);
        alReturn = (ArrayList) hmResult.get("RACRITEMDETAILS");
        hmReturn.put("RADETAILS", gmReturnReportBean.loadReturnHeaderInfo(strRAID));
        
        

        request.setAttribute("alReturn", alReturn);
      }

      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hFrom", strFrom);

      dispatch(strDispatchTo, request, response);

    } catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      dispatch(strComnErrorPath, request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }

  }
}
