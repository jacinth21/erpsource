/**
 * 
 */
package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmTxnSplitBean;
import com.globus.operations.returns.beans.GmBBAProcessReturnsProcessInterface;
import com.globus.operations.returns.beans.GmBBAReturnSummaryPicSlip;
import com.globus.operations.returns.beans.GmProcessReturnsBean;

/**
 * @author vprasath
 * 
 */
public class GmProcessConsRtnServlet extends GmReturnsServlet {

  private final static long serialVersionUID = 123456L;

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String strDispatchTo = strReturnsPath.concat("/GmProcessReturns.jsp");
    try {
      log.debug("Enter");
      instantiate(request, response);
      GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
      GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      HashMap hmRADetails = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alRAItems = new ArrayList();
      ArrayList alRAMailDecide = new ArrayList();
      String strRAID = "";
      String strEdate;
      ArrayList alLog = new ArrayList();
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      // Locale
      GmResourceBundleBean rbPaperworkBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strUserId = (String) session.getAttribute("strSessUserId");
      if (strAction.equals("Reload")) {
        log.debug("Reload Enter");
        request.setAttribute("PARTDETAILS",
            gmProcessReturnsBean.loadReturnsDetails(parseRequest(request)));
        log.debug("Reload Exit");
      } else if (strAction.equals("Initiate")) {
        log.debug("Enter Initiate");
        hmParam = parseRequest(request);
        hmParam.put("USERID", strUserId);
        hmRADetails = gmProcessReturnsBean.saveInitiate(hmParam, strUserId);

        strRAID = GmCommonClass.parseNull((String) hmRADetails.get("RAID"));
        /*
         * The following code is added for showing the Domain based Return Summary. For BBA, we are
         * calling the GmBBAProcessReturnsBean.java and for US, we are using the
         * GmUSProcessReturnsBean.java to get the RA details.
         */
        String strClassName =
            GmCommonClass.parseNull(rbPaperworkBundleBean.getProperty("RA.FETCH_CLASS"));
        String strShowBBAPicSlip =
            GmCommonClass.parseNull(rbPaperworkBundleBean.getProperty("RA.RETURNSUMMARY"));
        GmBBAProcessReturnsProcessInterface returnSummaryClient =
            GmBBAReturnSummaryPicSlip.getInstance(strClassName);
        hmReturn = returnSummaryClient.loadCreditReport(strRAID, "PRINT");
        // hmReturn = gmProcessReturnsBean.loadCreditReport(strRAID, "PRINT");
        hmReturn.put("COMPCURRENCYSYMBOL",
            GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol")));
        hmReturn.put("COMPANYLOCALE", strCompanyLocale);
        request.setAttribute("hmReturn", hmReturn);
        alRAItems = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RAITEMDETAILS"));
        alRAMailDecide =
            GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("RAINITMAILDECIDE"));
        if (alRAMailDecide.size() > 0) {
          hmReturn.put("EMAILFILENM", "GmRAInitialEmail");
          hmReturn.put("JASPERNAME", "/GmRAStatusEmail.jasper");
          hmReturn.put("ISCREDIT", "NOTCREDIT");
          hmReturn.put(
              "SUBREPORT_DIR",
              request.getSession().getServletContext()
                  .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
          gmProcessReturnsBean.sendRAStatusEmail(hmReturn);
        }
        sendVendorEmail(request, response, strApplnDateFmt, strRAID);
        strDispatchTo =
            (strShowBBAPicSlip.equals("Y")) ? strReturnsPath.concat("/GmBBACreditMemoPrint.jsp")
                : strReturnsPath.concat("/GmCreditMemoPrint.jsp");
        log.debug("Exit Initiate ***** " + strDispatchTo);
      }

      GmCommonBean gmCommonBean = new GmCommonBean();
      hmReturn = gmProcessReturnsBean.loadDisplayDetails();

      request.setAttribute("hmLoad", hmReturn);

      log.debug("Exit");
      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }// End of catch
  }// End of service method

  private void sendVendorEmail(HttpServletRequest request, HttpServletResponse response,
      String strApplnDateFmt, String strRAId) throws AppError {
    GmTxnSplitBean gmTxnSplitBean = new GmTxnSplitBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    String strVendorMail = "";
    String strRAID = "";
    String strRepEmail = "";
    HashMap hmEmailProps = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTxnSplit = new HashMap();
    hmTxnSplit = gmTxnSplitBean.fetchSplitRADetails(strRAId);
    Set keySet = hmTxnSplit.keySet();
    for (Iterator keys = keySet.iterator(); keys.hasNext();) {
      String key = (String) keys.next();
      if (!key.contains("Details")) {
        alResult = (ArrayList) hmTxnSplit.get(key);
        hmResult = (HashMap) hmTxnSplit.get(key + "Details");
        hmResult.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        hmResult.put("APPLNDATEFMT", strApplnDateFmt);
        strVendorMail = GmCommonClass.parseNull(GmCommonClass.getRuleValue(key, "VENDOREMAIL"));
        strRepEmail = GmCommonClass.parseNull((String) hmResult.get("EMAILADD"));
        strVendorMail = strRepEmail + "^^" + strVendorMail;
        strRAID = GmCommonClass.parseNull((String) hmResult.get("RAID"));
        hmEmailProps =
            gmTxnSplitBean.fetchEmailProperties("GmRAInitialEmail", strVendorMail, strRAID,
                "#<RAID>");
        GmJasperReport gmJasperReport = new GmJasperReport();
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        String strFileName =
            gmTxnSplitBean.createPDFFile(strRAID, gmJasperReport, new HashMap(hmResult),
                new ArrayList(alResult), "/GmVendorDeliverRAForm.jasper");
        GmEmailProperties gmEmailProps = (GmEmailProperties) hmEmailProps.get("EMAILPROPS");
        gmEmailProps.setAttachment(strFileName);
        gmTxnSplitBean.sendMailToVendor(alResult, hmResult, gmEmailProps,
            "/GmVendorDeliverRAForm.jasper");
        gmTxnSplitBean.removeFile(strFileName);
      }
    }
  }
}// end of servelet
