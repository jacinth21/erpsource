/**
 * 
 */
package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.operations.returns.beans.GmReturnReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

/**
 * @author vprasath
 * 
 */
public class GmReturnReconfRecServlet extends GmReturnsServlet {
  String strDispatchTo = strReturnsPath.concat("/GmReturnAccept.jsp");

  /**
     * 
     */
  public GmReturnReconfRecServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    log.debug("Enter GmReturnReconfRecServlet");
    instantiate(request, response);

    // Local Variables Used
    GmReturnReportBean gmReturnReportBean = new GmReturnReportBean(getGmDataStoreVO());
    GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
    String strParentRAId = GmCommonClass.parseNull(request.getParameter("hParentRAId"));
    String strInputString = GmCommonClass.parseNull(request.getParameter("hInputString"));
    String strReturnType = GmCommonClass.parseNull(request.getParameter("hReturnType"));
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());

    String strStatus = "";
    String strOrderID = "";
    String strAcctID = "";
    // Return Data Stores
    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    HashMap hmReturnDetails = new HashMap();
    HashMap hmHeader = new HashMap();
    HashMap hmParam = new HashMap();

    String strDistID = "";

    try {
      instantiate(request, response);
      hmParam = parseRequest(request);
      if (!strStatus.equals("0") && hmReturnDetails.size() == 0) {
        if (strParentRAId.indexOf("CRA") != -1) {
          throw new AppError(new SQLException("Error Occured", null, 20706));
        }
      }
      if (strAction.equals("SaveReconf")) {
        log.debug("Enter Save");
        if (strReturnType.equals("3302") || strReturnType.equals("3300")
            || strReturnType.equals("3306") || strReturnType.equals("3307")
            || strReturnType.equals("3304")) { // Consignment- Items
                                               // , sales - Items,
                                               // Consignment -
                                               // Distributor
                                               // Closure and ICT
                                               // Returns
                                               // Respectively.
          hmParam.put("INPUTSTRING", strInputString);
        } else {
          hmParam.put("INPUTSTRING", getInputString(request));
        }


        hmReturnDetails = gmProcessReturnsBean.saveReturnReconfDetails(hmParam, strUserId);
        request.setAttribute("hmReturnDetails", hmReturnDetails);
        hmParam.put("RAID", hmReturnDetails.get("CHILDRAID"));
        gmProcessReturnsBean.saveReturnReconfStatus(hmParam, strUserId);
        strAction = "Save";
        alResult = gmProcessReturnsBean.loadReturnsDetailInfo(hmParam);
        log.debug("Exit Save");
        // throw new AppError("RETURN SAVED SUCCESSFULLY","", 'S');
      }

      if (strAction.equals("ReloadReconf")) {
        alResult = gmProcessReturnsBean.loadReturnReConfigureInfo(hmParam);
      }

      hmReturn = gmReturnReportBean.loadReturnsAccpDetails(strParentRAId);
      hmHeader = gmReturnReportBean.loadReturnHeaderInfo(strParentRAId);
      strStatus = GmCommonClass.parseNull((String) hmHeader.get("SFL"));
      strOrderID = GmCommonClass.parseNull((String) hmHeader.get("ORDID"));
      strDistID = GmCommonClass.parseNull((String) hmHeader.get("DNAME"));
      strAcctID = GmCommonClass.parseNull((String) hmHeader.get("ACCID"));

      if (!strOrderID.equals("") || (strDistID.equals("") && strAcctID.equals(""))) {
        throw new AppError(new SQLException("Error Occured", null, 20707));
      }
      if (!strStatus.equals("0") && hmReturnDetails.size() == 0) {
        throw new AppError(new SQLException("Error Occured", null, 20705));
      }
      hmReturn.put("RADETAILS", hmHeader);
      hmReturn.put("RAITEMDETAILS", alResult);
      request.setAttribute("SETS", gmProjectBean.loadSetMap("1601"));
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      dispatch(strDispatchTo, request, response);
    } catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      gotoPage(strComnErrorPath, request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }
  }

}
