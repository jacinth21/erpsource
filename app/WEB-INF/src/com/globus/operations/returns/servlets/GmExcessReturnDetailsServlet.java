package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.returns.beans.GmExcessReturnsBean;

public class GmExcessReturnDetailsServlet extends GmReturnsServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    GmExcessReturnsBean gmExcessReturnsBean = new GmExcessReturnsBean(getGmDataStoreVO());
    RowSetDynaClass rdExcessReturnDetails = null;
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmHeaderInfo = new HashMap();
    ArrayList alReturn = new ArrayList();
    String strConsignId = "";
    String strRAID = "";
    String strReason = "";
    // to get the current date
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    String strCloseDate = strTodaysDate;
    String strDispatchTo = "";

    try {
      ArrayList alReason = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CSGER"));

      strConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
      strRAID = GmCommonClass.parseNull(request.getParameter("hRAID"));
      strReason = GmCommonClass.parseNull(request.getParameter("Cbo_Reason"));
      strCloseDate =
          request.getParameter("Txt_CloseDate") == null ? strTodaysDate : request
              .getParameter("Txt_CloseDate");
      String strInputString = GmCommonClass.parseNull(request.getParameter("hInputStr"));
      String strPartInputString = GmCommonClass.parseNull(request.getParameter("hPartInputStr"));
      // Remove the trailing , if present
      log.debug(" strPartInputString " + strPartInputString);
      int len = strPartInputString.length();
      log.debug(" len " + len);
      if (len > 0) {
        strPartInputString =
            String.valueOf(strPartInputString.charAt(len - 1)).equals(",") ? strPartInputString
                .substring(0, len - 1) : strPartInputString;
      }
      log.debug(" after strPartInputString " + strPartInputString);

      hmParam.put("CONSIGNID", strConsignId);
      hmParam.put("RAID", strRAID);
      hmParam.put("REASON", strReason);
      hmParam.put("CLOSEDATE", strCloseDate);
      hmParam.put("INPUTSTRING", strInputString);
      hmParam.put("PARTINPUTSTRING", strPartInputString);

      log.debug(" Consign Id " + strConsignId + " Reason is " + strReason);

      hmResult = gmExcessReturnsBean.loadExcessReturnDetails(strConsignId);
      rdExcessReturnDetails = (RowSetDynaClass) hmResult.get("RSEXCESSRETURNDETAILS");
      hmHeaderInfo = (HashMap) hmResult.get("HMHEADERINFO");

      log.debug(" Values inside Result " + hmResult);

      // For Transfer
      if (strReason.equals("50530")) {
        ArrayList alDistributorList =
            GmCommonClass.parseNullArrayList(gmExcessReturnsBean.getDistributorList(hmParam));
        ArrayList alEmployeeList = GmCommonClass.parseNullArrayList(gmLogonBean.getEmployeeList());
        ArrayList alTsfReasonDtoD =
            GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TSFRN"));
        ArrayList alTsfReasonItoD =
            GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TFRNI"));
        ArrayList alTransferType =
            GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TSFTP"));

        String strTransferType = GmCommonClass.parseNull(request.getParameter("Cbo_TransferType"));
        String strTransferFrom = GmCommonClass.parseNull(request.getParameter("hTransferFrom"));
        String strTransferTo = GmCommonClass.parseNull((String) hmHeaderInfo.get("DISTID"));
        String strTransferReason =
            GmCommonClass.parseNull(request.getParameter("Cbo_TransferReason"));
        String strDate =
            request.getParameter("Txt_Date") == null ? strTodaysDate : request
                .getParameter("Txt_Date");

        hmParam.put("TRANSFERTYPE", strTransferType);
        hmParam.put("TRANSFERFROM", strTransferFrom);
        hmParam.put("TRANSFERTO", strTransferTo);
        hmParam.put("TRANSFERREASON", strTransferReason);
        hmParam.put("TRANSFERMODE", "90353"); // Transfer Mode for Excess Return Transfer
        hmParam.put("DATE", strDate);

        log.debug(" Param values " + hmParam);

        request.setAttribute("ALTSFTP", alTransferType);
        request.setAttribute("ALTSFRNDtoD", alTsfReasonDtoD);
        request.setAttribute("ALTSFRNItoD", alTsfReasonItoD);
        request.setAttribute("ALDISTLIST", alDistributorList);
        request.setAttribute("ALEMPLIST", alEmployeeList);
        request.setAttribute("DISABLED", "disabled");
      }

      if (strAction.equals("Save")) {
        String strUserId = (String) session.getAttribute("strSessUserId");
        String strLogReason = GmCommonClass.parseNull(request.getParameter("Txt_LogReason"));

        hmParam.put("USERID", strUserId);
        hmParam.put("LOG", strLogReason);

        log.debug(" param values saved " + hmParam);
        gmExcessReturnsBean.saveExcessReturnDetails(hmParam);
        log.debug(" saved ");
      }

      // displaying comments
      alReturn = gmCommonBean.getLog(strConsignId, "1221");
      request.setAttribute("hmLog", alReturn);

      request.setAttribute("RDEXCESSRETURNDETAILS", rdExcessReturnDetails);
      request.setAttribute("HMHEADERINFO", hmHeaderInfo);
      request.setAttribute("HMPARAM", hmParam);
      request.setAttribute("ALREASON", alReason);

      strDispatchTo = strReturnsPath.concat("/GmExcessReturnDetails.jsp");
      dispatch(strDispatchTo, request, response);
    }

    catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      strDispatchTo = strComnPath + "/GmError.jsp";
      dispatch(strDispatchTo, request, response);
    }

    catch (Exception exp) {
      session.setAttribute("hAppErrorMessage", exp.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }
  }


}
