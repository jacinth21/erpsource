/*****************************************************************************
 * File : GmReturnOrderServlet Desc : User to process order return (code moved from
 * GmProcessReturnsServlets)
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.operations.returns.beans.GmProcessReturnsBean;



public class GmReturnOrderServlet extends GmReturnsServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strDispatchTo = "";

    try {
      instantiate(request, response);

      GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      String strRAId = "";
      String strDateFmt =
          GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
      hmParam.put("TYPE", GmCommonClass.parseNull(request.getParameter("Cbo_Type")));
      hmParam.put("REASON", GmCommonClass.parseNull(request.getParameter("Cbo_Reason")));
      hmParam.put("ID", GmCommonClass.parseNull(request.getParameter("Cbo_Id")));
      hmParam.put("COMMENTS", GmCommonClass.parseNull(request.getParameter("Txt_Comments")));
      hmParam.put("EDATE",
          getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_ExpDate"))));
      hmParam.put("ORDERID", GmCommonClass.parseNull(request.getParameter("hOrderId")));
      hmParam.put("ORDERTYPE", GmCommonClass.parseNull(request.getParameter("hOrderType")));
      hmParam
          .put("PARENT_ORDERID", GmCommonClass.parseNull(request.getParameter("Txt_ParOrderID")));
      hmParam.put("INPUTSTR", GmCommonClass.parseNull(request.getParameter("hInputStr")));
      hmParam.put("USERID", strUserId);
      hmParam.put("EMPNAME", "");
      hmParam.put("DATEFORMAT", strDateFmt);
      hmParam.put("SYSTEMDATE", GmCommonClass.getCurrentDate(strDateFmt));
      hmParam.put("USEDLOT_STR", GmCommonClass.parseNull(request.getParameter("hUsedLotStr")));
      // Calls to save the return info
      strRAId = gmProcessReturnsBean.saveOrderReturns(hmParam);

      hmReturn = gmProcessReturnsBean.loadCreditReport(strRAId, "PRINT");

      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", "ReturnDetails");

      strDispatchTo = strReturnsPath.concat("/GmReturnView.jsp");
      dispatch(strDispatchTo, request, response);


    }// End of try
    catch (Exception e) {
      GmLogError.log("Exception in initiateItemReturns:- User " + strUserNm,
          "Exception is:" + e.getStackTrace());
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmProcessReturnsServlet
