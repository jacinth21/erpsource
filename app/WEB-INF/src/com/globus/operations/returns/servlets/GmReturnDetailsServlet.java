package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.returns.beans.GmReturnReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmReturnDetailsServlet extends GmReturnsServlet {
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);

    GmCommonBean gmCommonBean = new GmCommonBean();
    GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
    GmReturnReportBean gmReturnReportBean = new GmReturnReportBean(getGmDataStoreVO());

    ArrayList alType = new ArrayList();
    ArrayList alProdFamily = new ArrayList();
    ArrayList alProject = new ArrayList();
    HashMap hmRADetails = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmParam = new HashMap();
    RowSetDynaClass rsHeaderResultSet = null;
    RowSetDynaClass rsFooterResultSet = null;

        String strRAID = GmCommonClass.parseNull(request.getParameter("hRAId"));
        String strType = GmCommonClass.parseZero(request.getParameter("Cbo_StatusType"));
        String strProdFamily = GmCommonClass.parseZero(request.getParameter("Cbo_ProdFamily"));
        String strProjectId = GmCommonClass.parseZero(request.getParameter("Cbo_Project"));
        String strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
        String strPartNumFormatted = GmCommonClass.getStringWithQuotes(strPartNum);

        log.debug(" StrType " + strType + " strRAID " + strRAID);
        
        String strColumn = "";
        strColumn = request.getParameter("hColumn") == null ? "ID":strColumn;
        hmTemp.put("COLUMN",strColumn);
        hmTemp.put("STATUSID","20302"); // 20302 is for launched projects
        
        try {
            alType = GmCommonClass.getCodeList("RETST");
            alProdFamily = GmCommonClass.getCodeList("PFLY");
            alProject = gmProject.reportProject(hmTemp);
            
            hmParam.put("RAID",strRAID);            
            hmParam.put("TYPE",strType);
            hmParam.put("PRODFAMILY",strProdFamily);
            hmParam.put("PROJECTID",strProjectId);
            hmParam.put("PARTNUM",strPartNum);
            hmParam.put("PARTNUMFORMATTED",strPartNumFormatted);
            
            log.debug("Action is " + strAction);
            log.debug("hmParam is " + hmParam);
            
            hmRADetails = gmReturnReportBean.loadReturnHeaderInfo(strRAID);
            
            if (strAction.equals("Report")) {
                hmTemp = gmReturnReportBean.loadReturnsDetails(hmParam);
                rsHeaderResultSet = (RowSetDynaClass)hmTemp.get("HEADERRESULT"); 
                rsFooterResultSet = (RowSetDynaClass)hmTemp.get("FOOTERRESULT");
                request.setAttribute("rsHeaderResultSet",rsHeaderResultSet);
                request.setAttribute("rsFooterResultSet",rsFooterResultSet);
            }
            
            request.setAttribute("ALTYPE",alType);
            request.setAttribute("ALPRODFAMILY",alProdFamily);
            request.setAttribute("ALPROJECT",alProject);
            request.setAttribute("HMPARAM",hmParam);
            request.setAttribute("HMRADETAILS",hmRADetails);
            
            String strDispatchTo = strReturnsPath.concat("/GmReturnDetailReport.jsp");
            dispatch(strDispatchTo,request,response);
        }
        
        catch(Exception exp){
            session.setAttribute("hAppErrorMessage",exp.getMessage());
            gotoPage(strComnErrorPath,request,response);
        }
    }
}
