/**
 * 
 */
package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.operations.returns.beans.GmReturnReportBean;

/**
 * @author vprasath
 */
public class GmReturnReceiveServlet extends GmReturnsServlet {
  String strDispatchTo = strReturnsPath.concat("/GmReturnAccept.jsp");

  /**
     * 
     */
  public GmReturnReceiveServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    log.debug("Enter GmInitClosureServlet");
    instantiate(request, response);

    // Local Variables Used
    GmReturnReportBean gmReturnReportBean = new GmReturnReportBean(getGmDataStoreVO());
    GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    String strRAId = request.getParameter("hRAId");
    String strInputString = GmCommonClass.parseNull(request.getParameter("hInputString"));
    String strReturnType = GmCommonClass.parseNull(request.getParameter("hReturnType"));
    String strTxnSts = GmCommonClass.parseNull(request.getParameter("txnStatus"));
    String strStatus = "";
    String strOrdID = "";
    String strValidationFl = "";
    String strCheckRule = "";
    String strRuleEntryFl = "N";
    strCheckRule = GmCommonClass.parseNull((String) request.getAttribute("chkRule"));
    strCheckRule =
        strCheckRule.equals("") ? GmCommonClass.parseNull(request.getParameter("chkRule"))
            : strCheckRule;
    // Return Data Stores

    HashMap hmReturnDetails = new HashMap();
    HashMap hmHeader = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();

    try {
      instantiate(request, response);
      hmParam = parseRequest(request);

      if (strAction.equals("Save")) {
        if (strReturnType.equals("3302") || strReturnType.equals("3300")
            || strReturnType.equals("3306") || strReturnType.equals("3307")
            || strReturnType.equals("3304")) { // Consignment- Items , sales - Items, Consignment -
                                               // Distributor Closure and ICT Returns Respectively.
          hmParam.put("INPUTSTRING", strInputString);
        } else {
          hmParam.put("INPUTSTRING", getInputString(request));
        }
        log.debug("Enter Save " + hmParam);

        strValidationFl = gmProcessReturnsBean.saveReturnLotNum(hmParam, strUserId);

        if (strValidationFl.equalsIgnoreCase("N") || strValidationFl.equals("")) {
          if (strCheckRule.equalsIgnoreCase("Yes")) {
            strRuleEntryFl = "Y";

            request.setAttribute("RE_TXN", "RECEIVERETURNSWRAPPER");
            request.setAttribute("txnStatus", strTxnSts);
            request.setAttribute("RE_FORWARD", "gmReturnReceiveServlet");
            request.setAttribute("chkRule", "No");
            dispatch("/gmRuleEngine.do", request, response);
            return;
          }
          hmReturnDetails = gmProcessReturnsBean.saveReturnsDetails(hmParam, strUserId);
        }
        request.setAttribute("hmReturnDetails", hmReturnDetails);
        log.debug("Exit Save");
        // throw new AppError("RETURN "+ strRAId +" SAVED SUCCESSFULLY","", 'S');
      }
      hmReturn = gmReturnReportBean.loadReturnsAccpDetails(strRAId);
      hmHeader = gmReturnReportBean.loadReturnHeaderInfo(strRAId);
      strStatus = GmCommonClass.parseNull((String) hmHeader.get("SFL"));
      strOrdID = GmCommonClass.parseNull((String) hmHeader.get("ORDID"));

      if (!strStatus.equals("0") && hmReturnDetails.size() == 0) {
        throw new AppError(new SQLException("Error Occured", null, 20705));
      }
      hmReturn.put("RADETAILS", hmHeader);
      hmReturn.put("RAITEMDETAILS", gmProcessReturnsBean.loadReturnsDetailInfo(hmParam));
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      request.setAttribute("ORDERID", strOrdID);

      // log.debug("request.getParameter('hMode') : "+request.getParameter("hMode"));
      request.setAttribute("RE_FORWARD", "gmReturnAccept");
      request.setAttribute("RE_TXN", "RECEIVERETURNSWRAPPER");
      request.setAttribute("txnStatus", "PROCESS");
      strDispatchTo = "/gmRuleEngine.do";
      dispatch(strDispatchTo, request, response);
    } catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      dispatch(strComnErrorPath, request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }
  }
}
