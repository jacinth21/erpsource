/**
 * 
 */
package com.globus.operations.returns.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.returns.beans.GmProcessReturnsBean;

/**
 * @author vprasath
 * 
 */
public class GmInitClosureServlet extends GmReturnsServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    log.debug("Enter GmInitClosureServlet");
    instantiate(request, response);

    // Local Variables Used
    String strDistID = GmCommonClass.parseNull(request.getParameter("Cbo_DistId"));

    String strDispatchTo = strReturnsPath.concat("/GmCLRValidate.jsp");

    // Return Data Stores
    HashMap hmOpenTxns = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmRADetails = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alDistList = new ArrayList();
    ArrayList alReasonList = new ArrayList();
    ArrayList alLog = new ArrayList();
    String strRAID = "";
    String strEdate = "";

    // Beans Initialization
    GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    
    // log.debug(" action ");

    try {

      if (strAction.equals("VALIDATE")) {
        hmOpenTxns = gmProcessReturnsBean.loadCLROpenTxn(strDistID);

      } else if (strAction.equals("INITIATE")) {
        hmParam = parseRequest(request);
        strEdate = GmCommonClass.parseNull((String) hmParam.get("EDATE"));
        String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
        
        strEdate = strEdate.equals("") ? GmCalenderOperations.addDays(4) : strEdate;
        hmParam.put("EDATE", strEdate);
        hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
       
        hmRADetails = gmProcessReturnsBean.saveCLRInitiate(hmParam, strUserId);
        String strMessage = GmCommonClass.parseNull((String) hmRADetails.get("MESSAGE"));
        
      }

      if (hmOpenTxns.get("ISOPENTXNAVL") != null) {
        request.setAttribute("OPENTXNS", hmOpenTxns);
      } else if (!strAction.equals("Load") && hmOpenTxns.get("ISOPENTXNAVL") == null) {
        hmParam = parseRequest(request);
        alReasonList = GmCommonClass.getCodeList("INIRA");
        request.setAttribute("REASONLIST", alReasonList);

        hmReturn = gmProcessReturnsBean.loadCLRPndReturn(strDistID);

        request.setAttribute("PENTXNS", hmReturn.get("PENTXNS"));
        request.setAttribute("RULES", hmReturn.get("RULES"));

        // If already initiated there will a RA ID for the distributor.
        strRAID = GmCommonClass.parseNull((String) hmRADetails.get("RAID"));
        
        // if(strRAID)
        if (strRAID.equals("")) {
          hmRADetails = (HashMap) hmReturn.get("RADETAILS");
          strRAID =
              hmRADetails != null ? GmCommonClass.parseNull((String) hmRADetails.get("RAID"))
                  : null;
          if (strRAID != null) {
            throw new AppError("Return Already Intiated for this Distributor Closure. RA ID is "
                + strRAID, "", 'S');
          }
        }


        log.debug("strRAID  : " + strRAID);
        alLog = gmCommonBean.getLog(strRAID);
        request.setAttribute("hmLog", alLog);
        strDispatchTo = strReturnsPath.concat("/GmCLRInitiate.jsp");
      }

      request.setAttribute("PARAM", hmParam);
      alDistList = gmCommonBean.getDistributorList();
      // log.debug("size of alDistListr " + alDistList);
      request.setAttribute("RADETAILS", hmRADetails);
      request.setAttribute("DISTLIST", alDistList);
      log.debug("Exit GmInitClosureServlet");
      dispatch(strDispatchTo, request, response);
    } catch (AppError e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      dispatch(strComnErrorPath, request, response);
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strComnErrorPath, request, response);
    }
  }
}
