package com.globus.operations.returns.forms;

import java.util.ArrayList;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCancelForm;


public class GmAssociatedReturnForm extends GmCancelForm {
	
	
	private String initRA = ""; 
	private String initDist = "";
	private String creditRA= ""; 
	private String creditDist=""; 
	private ArrayList alInitDistList = null;
	private ArrayList alCreditDistList  = null;
	private ArrayList alDetail = new ArrayList();
		/**
	 * @return the alDetail
	 */
	public ArrayList getAlDetail() {
		return alDetail;
	}
	/**
	 * @param alDetail the alDetail to set
	 */
	public void setAlDetail(ArrayList alDetail) {
		this.alDetail = alDetail;
	}
		/**
	 * @return the initRA
	 */
	public String getInitRA() {
		return initRA;
	}
	/**
	 * @param initRA the initRA to set
	 */
	public void setInitRA(String initRA) {
		this.initRA = initRA;
	}
	/**
	 * @return the initDist
	 */
	public String getInitDist() {
		return initDist;
	}
	/**
	 * @param initDist the initDist to set
	 */
	public void setInitDist(String initDist) {
		this.initDist = initDist;
	}
	/**
	 * @return the creditRA
	 */
	public String getCreditRA() {
		return creditRA;
	}
	/**
	 * @param creditRA the creditRA to set
	 */
	public void setCreditRA(String creditRA) {
		this.creditRA = creditRA;
	}
	/**
	 * @return the creditDist
	 */
	public String getCreditDist() {
		return creditDist;
	}
	/**
	 * @param creditDist the creditDist to set
	 */
	public void setCreditDist(String creditDist) {
		this.creditDist = creditDist;
	}
	
	/**
	 * @return the alInitDistList
	 */
	public ArrayList getAlInitDistList() {
		return alInitDistList;
	}
	/**
	 * @param alInitDistList the alInitDistList to set
	 */
	public void setAlInitDistList(ArrayList alInitDistList) {
		this.alInitDistList = alInitDistList;
	}
	/**
	 * @return the alCreditDistList
	 */
	public ArrayList getAlCreditDistList() {
		return alCreditDistList;
	}
	/**
	 * @param alCreditDistList the alCreditDistList to set
	 */
	public void setAlCreditDistList(ArrayList alCreditDistList) {
		this.alCreditDistList = alCreditDistList;
	}	
	
	}
