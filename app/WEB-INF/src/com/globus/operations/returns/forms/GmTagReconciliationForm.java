package com.globus.operations.returns.forms;

import java.util.ArrayList;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCancelForm;


public class GmTagReconciliationForm extends GmCancelForm {
	
	
	private ArrayList alDistList = new ArrayList();
	private RowSetDynaClass rdReport= null;
	private String raID = "";
	private String setID = "";
	private String excessID = "";
	private String distID = "";
	private String partNum = "";
	private String ctrlNum = "";
	private String tagID = "";
	private String strOpt="";
	private String inputString = "";
	private String untaggedSets = "off";
	private String statusID = "";
	
	
	private ArrayList alStatus = new ArrayList();
	
	public String getUntaggedSets() {
		return untaggedSets;
	}
	public void setUntaggedSets(String untaggedSets) {
		this.untaggedSets = untaggedSets;
	}
	private ArrayList alMTagRList = new ArrayList(); //missing tag reason list
	public ArrayList getAlMTagRList() {
		return alMTagRList;
	}
	public void setAlMTagRList(ArrayList alMTagRList) {
		this.alMTagRList = alMTagRList;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @return the raID
	 */
	public String getRaID() {
		return raID;
	}
	/**
	 * @param raID the raID to set
	 */
	public void setRaID(String raID) {
		this.raID = raID;
	}

	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}
	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}
	/**
	 * @return the excessID
	 */
	public String getExcessID() {
		return excessID;
	}
	/**
	 * @param excessID the excessID to set
	 */
	public void setExcessID(String excessID) {
		this.excessID = excessID;
	}

	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the ctrlNum
	 */
	public String getCtrlNum() {
		return ctrlNum;
	}
	/**
	 * @param ctrlNum the ctrlNum to set
	 */
	public void setCtrlNum(String ctrlNum) {
		this.ctrlNum = ctrlNum;
	}
	/**
	 * @return the tagID
	 */
	public String getTagID() {
		return tagID;
	}
	/**
	 * @param tagID the tagID to set
	 */
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}
	/**
	 * @return the alDistList
	 */
	public ArrayList getAlDistList() {
		return alDistList;
	}
	/**
	 * @param alDistList the alDistList to set
	 */
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}
	/**
	 * @return the rdReport
	 */
	public RowSetDynaClass getRdReport() {
		return rdReport;
	}
	/**
	 * @param rdReport the rdReport to set
	 */
	public void setRdReport(RowSetDynaClass rdReport) {
		this.rdReport = rdReport;
	}
	/**
	 * @return the distID
	 */
	public String getDistID() {
		return distID;
	}
	/**
	 * @param distID the distID to set
	 */
	public void setDistID(String distID) {
		this.distID = distID;
	}
	/**
	 * @return the statusID
	 */
	public String getStatusID() {
		return statusID;
	}
	/**
	 * @param statusID the statusID to set
	 */
	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	} 
	
}
