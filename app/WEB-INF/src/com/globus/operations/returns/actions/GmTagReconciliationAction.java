package com.globus.operations.returns.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.returns.beans.GmExcessReturnsBean;
import com.globus.operations.returns.forms.GmTagReconciliationForm;

public class GmTagReconciliationAction extends GmAction {


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code
      GmTagReconciliationForm gmTagReconciliationForm = (GmTagReconciliationForm) form;
      HashMap hmParam = new HashMap();
      String inputString = "";
      String strOpt = "";
      String strStatusId = "";
      String strPartInputStr = "";
      String strDisplayNm = "Missing Tag Rec. Screen";

      gmTagReconciliationForm.loadSessionParameters(request);

      strStatusId = GmCommonClass.parseNull(gmTagReconciliationForm.getStatusID());
      String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      String strURL = "/gmTagReconciliation.do?companyInfo=" + strComapnyInfo;

      if (strStatusId.equals("")) {
        gmTagReconciliationForm.setStatusID("4581"); // to set default - Un reconciled
      }
      hmParam = GmCommonClass.getHashMapFromForm(gmTagReconciliationForm);
      GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

      GmExcessReturnsBean gmExcessReturnsBean = new GmExcessReturnsBean(getGmDataStoreVO());
      ArrayList alDistList = new ArrayList();
      alDistList = gmCustomerBean.getDistributorList("Active");
      RowSetDynaClass rdReport = null;
      strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
      if (strOpt.equals("Save")) {
        request.setAttribute("hRedirectURL", strURL);
        request.setAttribute("hDisplayNm", strDisplayNm);
        inputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
        gmExcessReturnsBean.MissingTagReconciliation(hmParam);
      }
      gmTagReconciliationForm.setAlMTagRList(GmCommonClass.parseNullArrayList(GmCommonClass
          .getCodeList("MTAGRE")));
      gmTagReconciliationForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
          .getCodeList("MIRCST")));
      rdReport = gmExcessReturnsBean.loadMissingTagReturns(hmParam);
      log.debug("gmTagReconciliationForm.getAlMTagRList():"
          + gmTagReconciliationForm.getAlMTagRList());
      gmTagReconciliationForm.setRdReport(rdReport);
      gmTagReconciliationForm.setAlDistList(alDistList);
    } catch (AppError appError) {
      request.setAttribute("hType", appError.getType());
      throw appError;
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("GmTagReconciliation");
  }

}
