package com.globus.operations.returns.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.returns.beans.GmExcessReturnsBean;
import com.globus.operations.returns.forms.GmAssociatedReturnForm;

public class GmAssociatedReturnAction extends GmAction {


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    GmAssociatedReturnForm gmAssociatedReturnForm = (GmAssociatedReturnForm) form;
    HashMap hmParam = new HashMap();
    ArrayList alDetail = new ArrayList();
    String strOpt = "";
    gmAssociatedReturnForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmAssociatedReturnForm);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmExcessReturnsBean gmExcessReturnsBean = new GmExcessReturnsBean(getGmDataStoreVO());
    ArrayList alDistList = new ArrayList();
    alDistList = gmCustomerBean.getDistributorList("Active");
    RowSetDynaClass rdReport = null;
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    log.debug("strOpt ============" + strOpt);
    if (strOpt.equals("load")) {
      alDetail =
          GmCommonClass.parseNullArrayList(gmExcessReturnsBean.loadAssociatedRAReport(hmParam));
      gmAssociatedReturnForm.setAlDetail(alDetail);
    }

    gmAssociatedReturnForm.setAlInitDistList(alDistList);
    gmAssociatedReturnForm.setAlCreditDistList(alDistList);
    return mapping.findForward("GmAssociatedReturn");
  }

}
