package com.globus.operations.forecast.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmLogger; 

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger; 

public class GmForecastTransBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    /**
     * loadTTPLinkData - This method will load the TTP / Sheet link data
     * @param hmParam - hmParam
     * @return HashMap
     * @exception AppError
     */
    public HashMap loadTTPLinkData (HashMap hmParam)throws AppError
    {
        String strTTPId = GmCommonClass.parseNull((String)hmParam.get("TTPID")); 
        String strMonth = GmCommonClass.parseNull((String)hmParam.get("MONTHID"));
        String strYear = GmCommonClass.parseNull((String)hmParam.get("YEARID"));
        String strMonYear = strMonth.concat("/").concat(strYear);
        
        HashMap hmReturn = new HashMap();
        ArrayList alSelected = new ArrayList();
        ArrayList alUnSelected = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager ();        
        
        // To fetch header information
        gmDBManager.setPrepareString("gm_pkg_op_ttp.gm_fc_fch_ttp_dsmonthly",3);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        gmDBManager.setString(1,strTTPId);
        gmDBManager.setString(2,strMonYear);
        gmDBManager.execute();
        alSelected = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
        
        // To fetch the transaction information
        gmDBManager.setPrepareString("gm_pkg_op_ttp.gm_fc_fch_ttp_approvedds",3);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        gmDBManager.setString(1,strTTPId);
        gmDBManager.setString(2,strMonYear);
        gmDBManager.execute();
        alUnSelected = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
        gmDBManager.close();
        hmReturn.put("SELECTED",alSelected);
        hmReturn.put("UNSELECTED",alUnSelected);
        
        return hmReturn;
    }
    
    /**
     * saveTTPLinkData - This method will save the TTP / Sheet grouping
     * @param hmParam - hmParam
     * @return void
     * @exception AppError
     */
    public void saveTTPLinkData (HashMap hmParam)throws AppError
    {
        log.debug(" values inside hmParam " + hmParam);
        String[] strPnumOrd=null;
        String strVisitCnt = "";
        String strTTPDtlId = "";
        String strTTPId = GmCommonClass.parseNull((String)hmParam.get("TTPID")); 
        String strDemandSheets = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETIDS"));
        String strForecastMonth = GmCommonClass.parseNull((String)hmParam.get("TTPFORECASTMONTHS"));
        String strInventoryId = GmCommonClass.parseNull((String)hmParam.get("INVENTORYID"));
        String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strOrdQty = GmCommonClass.parseNull((String)hmParam.get("ORDSTR"));
        GmDBManager gmDBManager = new GmDBManager ();        
        strPnumOrd = GmCommonClass.getStringTillLength("|",4000, strOrdQty);
        for(int k =0;k< strPnumOrd.length; k++)
        {
        	if (strPnumOrd[k] == null)
        		break;
        	log.debug("string..  "+k+"....."+strPnumOrd[k]);
	        // To fetch header information
	        gmDBManager.setPrepareString("gm_pkg_op_ttp.gm_fc_sav_ttp_dsmonthly",8);
	        gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
	        gmDBManager.setString(1,strTTPId);
	        gmDBManager.setString(2,strDemandSheets);
	        gmDBManager.setString(3,strForecastMonth);
	        gmDBManager.setString(4,strInventoryId);
	        gmDBManager.setString(5,strUserId);
	        gmDBManager.setString(6,strPnumOrd[k]);
	        gmDBManager.setString(7,Integer.toString(k));
	        gmDBManager.setString(8,strTTPDtlId);	
	        gmDBManager.execute();
	        
	        strTTPDtlId = (String) gmDBManager.getString(8);
        }
        
        gmDBManager.commit();
    }
    
    /**
     * loadInventoryIDList - This method will load the inventory list
     * @param hmParam - hmParam
     * @return void
     * @exception AppError
     */
    public ArrayList loadInventoryIDList (String strInvLockType)throws AppError
    {
        ArrayList alInventoryList = new ArrayList();
        GmDBManager gmDBManager = new GmDBManager ();
        
        gmDBManager.setPrepareString("Gm_pkg_op_ttp.gm_fc_fch_inventoryidlist",2);
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        gmDBManager.setString(1,strInvLockType);
        gmDBManager.execute();
        
        alInventoryList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
        gmDBManager.close();
        return alInventoryList;
    }
    
    /**
     * getForecastMonthRule - returns the default forecast months from rule table
     */
    public String getForecastMonthRule ()throws AppError
    {
        String strForecastMonth = "4";
        String strRuleId = "FORECASTMONTHS";
        String strRuleGroupId = "ORDERPLANNING";
        GmDBManager gmDBManager = new GmDBManager ();
        StringBuffer sbQuery = new StringBuffer();
        
        sbQuery.append(" SELECT GET_RULE_VALUE('");
        sbQuery.append(strRuleId);
        sbQuery.append("','");
        sbQuery.append(strRuleGroupId);
        sbQuery.append("') FORECASTMONTH FROM DUAL ");
        
        log.debug("Query for Forecast month rulevalue " + sbQuery.toString());
        strForecastMonth = (String)gmDBManager.querySingleRecord(sbQuery.toString()).get("FORECASTMONTH");
        gmDBManager.close();
        return strForecastMonth;
    
    }
    /**
	 * fetchPPQDetails
	 * 
	 * @param
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchPPQDetails(HashMap hmParam) throws AppError {
		HashMap hmReturn = new HashMap();
		RowSetDynaClass rdPartDetails = null;
		GmDBManager gmDBManager = new GmDBManager();
		String strPartNums = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBERS"));
		String strInventoryLockId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYLOCKID"));
		gmDBManager.setPrepareString("gm_pkg_op_ttp_summary.gm_fc_fch_ppqdetails", 4);
		gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

		gmDBManager.setString(1, strPartNums);
		gmDBManager.setInt(2, Integer.parseInt(strInventoryLockId));
		// log.debug("strPartNums:"+ strPartNums + "--strInventoryLockId : "+strInventoryLockId);

		gmDBManager.execute();
		rdPartDetails = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
		hmReturn.put("PDESC", gmDBManager.getString(3));
		hmReturn.put("PARENTPARTDETAILS", rdPartDetails.getRows());
		gmDBManager.close();
		return hmReturn;
	}
    
}
