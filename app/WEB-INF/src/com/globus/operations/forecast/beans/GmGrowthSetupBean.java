package com.globus.operations.forecast.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmGrowthSetupBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	public HashMap saveGrowthDetails(HashMap hmParam) throws AppError {
		log.debug("Enter");
		HashMap hmReturn = new HashMap();

		String strDemandSheetId = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETID"));
		String strReferenceType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strReferenceID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
		String strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strGrowthId = "";
		String strMsg = "";
		strReferenceID = (strReferenceType.equals("20295") && strReferenceID.equals("0")) ? strPnum : strReferenceID;
		String strTemp = "";
		ArrayList outList = null;
		log.debug("hmParam :"+hmParam);

		GmDBManager gmDBManager = new GmDBManager();
		GmCommonBean gmCommonBean = new GmCommonBean();
		try {
			if(strInputString!=null && strInputString.length()>4000){
				
				outList = GmCommonClass.convertStringToList(strInputString, 4000, "$");
			}else{
				outList = new ArrayList();
				outList.add(strInputString);
			}
			if(outList!=null){
				for(Iterator itr=outList.iterator();itr.hasNext();){
					strTemp = (String)itr.next();
					
					gmDBManager.setPrepareString("GM_PKG_PD_GROWTH.GM_PD_SAV_GROWTH_DETAILS", 6);
					gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
					gmDBManager.setString(1, strDemandSheetId);
					gmDBManager.setString(2, strReferenceType);
					gmDBManager.setString(3, strReferenceID);
					gmDBManager.setString(4, strTemp);
					gmDBManager.setString(5, strUserId);
					gmDBManager.execute();
					strGrowthId = gmDBManager.getString(6);
					log.debug("strGrowthId :"+strGrowthId);
					if (!strLogReason.equals("")) {
						gmCommonBean.saveLog(gmDBManager, strGrowthId, strLogReason, strUserId, "1215"); // 1215
																											// -
																											// Log
																											// Code
						// Number for Growth
					}	
				}
			}
			gmDBManager.commit();
		} catch (Exception ex) {
			log.debug("Message : " + ex.getMessage());
			String strException = ex.getMessage();

			if (strException.indexOf("20042") > 0) {
				strGrowthId = strException.substring(10, 21);
				strGrowthId = strGrowthId.substring(0, strGrowthId.indexOf("O") - 1).trim();
				strMsg = "Growth already exist for given parameters, Please use override screen to update";
			} else if (strException.indexOf("20029") > 0) {
				strMsg = "Part number does not exist for the selected demand sheet";
			} else {
				throw new AppError(ex);
			}
		}
		log.debug("Exit");
		hmReturn.put("GROWTHID", strGrowthId);
		
		return hmReturn;
	}

	public ArrayList fetchGrowthReference(String strDemandSheetId, String strRefType) throws AppError {
		log.debug("Enter");
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_PD_GROWTH.GM_PD_FCH_GROWTH_REF", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strDemandSheetId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strRefType));
		gmDBManager.execute();
		ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		log.debug("Exit");
		return alReturn;
	}

	public HashMap fetchGrowthDetails(HashMap hmParam) throws AppError {

		String strDemandSheetId = (String) hmParam.get("DEMANDSHEETID");
		String strRefType = (String) hmParam.get("REFTYPE");
		String strRefId = (String) hmParam.get("REFID");
		String strYear = (String) hmParam.get("YEAR");
		String launchDate = (String) hmParam.get("LAUNCHDATE");
		String strFromDate = (String)hmParam.get("FROMDATE");
		String strToDate = (String)hmParam.get("TODATE");	
		String strPNum   = (String)hmParam.get("PNUM");
		if(!strPNum.equals("")){
			strPNum = "^"+strPNum.replaceAll(",", "|^");
		}
		
		log.debug("Input From Action "+hmParam);
		
		StringBuffer sbHeaderQuery = new StringBuffer();
		StringBuffer sbDetailsQuery = new StringBuffer();
		HashMap hmGrowthMap = new HashMap();
		
		GmCrossTabReport crossTabReport = new GmCrossTabReport();
		crossTabReport.setAcceptExtraColumn(true);
		
		crossTabReport.setExtraColumnParams(4, "refvalue");
		crossTabReport.setExtraColumnParams(5, "Description");
		crossTabReport.setExtraColumnParams(6, "refyear");
		crossTabReport.setExtraColumnParams(7, "refmonthtype");
		crossTabReport.setExtraColumnParams(8,"controlvalue");
		crossTabReport.setExtraColumnParams(9,"history_fl");
		crossTabReport.setExtraColumnParams(10,"comments");
		crossTabReport.setExtraColumnParams(11,"grw_details_id");
	//	crossTabReport.setExtraColumnParams(11,"row_control");
		
		if(launchDate==null || launchDate.length()==0){
			launchDate = "(SELECT TO_CHAR (SYSDATE, 'YYYYMM') FROM DUAL) ";
		}
		else{
			launchDate ="TO_CHAR (TO_DATE ('"+launchDate+"',GET_RULE_VALUE('DATEFMT','DATEFORMAT')),'YYYYMM')";
		}
		
		/*
		// For Group (In Sales) and Set (In Consignment) we need to display all the available set/Group irrespective of Growth Existance or not.
		if(strRefType.equals("20296")){
		
			sbDetailsQuery.append(" SELECT c4021_ref_id ID, t4021.c4021_ref_id NAME, '' refmonth, '' refvalue,");
			sbDetailsQuery.append(" get_set_name (t4021.c4021_ref_id) description, '' refyear,");
			sbDetailsQuery.append(" '' refmonthtype, '' controlvalue, '' history_fl, '' comments,");
			sbDetailsQuery.append(" 0 grw_details_id");
			sbDetailsQuery.append(" FROM t4021_demand_mapping t4021");
			sbDetailsQuery.append(" WHERE t4021.c4020_demand_master_id = "+strDemandSheetId+" AND t4021.c901_ref_type = '40031' UNION ALL ");
			
		}if(strRefType.equals("20297")){
			
			sbDetailsQuery.append(" SELECT c4021_ref_id ID, get_group_name (t4021.C4021_REF_ID) NAME, '' refmonth, '' refvalue,");
			sbDetailsQuery.append(" '' description, '' refyear,");
			sbDetailsQuery.append(" '' refmonthtype, '' controlvalue, '' history_fl, '' comments,");
			sbDetailsQuery.append(" 0 grw_details_id");
			sbDetailsQuery.append(" FROM t4021_demand_mapping t4021");
			sbDetailsQuery.append(" WHERE t4021.c4020_demand_master_id = "+strDemandSheetId+" AND t4021.c901_ref_type = '40030' UNION ALL");
							
			
		}*/
		sbHeaderQuery.append(" SELECT  TO_CHAR(v9001.month_value,'Mon YY') refmonth ");
		sbHeaderQuery.append(" FROM v9001_month_list v9001 ");
		sbHeaderQuery.append(" WHERE v9001.month_value BETWEEN TO_DATE('");
		sbHeaderQuery.append( strFromDate );
		sbHeaderQuery.append("','Mon YY') and LAST_DAY(TO_DATE('");
		sbHeaderQuery.append( strToDate );
		sbHeaderQuery.append("','Mon YY')) ORDER BY TO_DATE(refmonth,'Mon YY') ");		
    	
    	log.debug(" sbHeaderQuery is "+sbHeaderQuery);
    	
    	sbDetailsQuery.append(" SELECT t4030.c4030_ref_id ID, ");
    	sbDetailsQuery.append(" DECODE (t4030.c901_ref_type,'20297', get_group_name (t4030.c4030_ref_id),t4030.c4030_ref_id) Name, ");
    	/*
    	sbDetailsQuery.append(" DECODE(t4030.c901_ref_type,'20295', t4030.c4030_ref_id|| ':'||get_partnum_desc (t4030.c4030_ref_id)," );
    	sbDetailsQuery.append(" '20296',t4030.c4030_ref_id|| ':'||get_set_name (t4030.c4030_ref_id),'20297',get_group_name(t4030.c4030_ref_id),'20298',");
    	sbDetailsQuery.append(" t4030.c4030_ref_id|| ':'||get_partnum_desc (t4030.c4030_ref_id)) NAME, ");
    	*/
    	sbDetailsQuery.append(" TO_CHAR (t4031.c4031_start_date, 'Mon YY')refmonth , ");
    	sbDetailsQuery.append(" t4031.c4031_value || " );
    	sbDetailsQuery.append("DECODE (t4030.c901_ref_type,'20297',DECODE(get_code_name (t4031.c901_ref_type),'U',get_code_name (t4031.c901_ref_type),''),'' ");
    	sbDetailsQuery.append(",t4030.c901_ref_type,'20295',DECODE(get_code_name (t4031.c901_ref_type),'P',get_code_name (t4031.c901_ref_type),''),'' ");
    	sbDetailsQuery.append(",t4030.c901_ref_type,'20298',DECODE(get_code_name (t4031.c901_ref_type),'A',get_code_name (t4031.c901_ref_type),''),'' ");
    	sbDetailsQuery.append(",t4030.c901_ref_type,'20296',DECODE(get_code_name (t4031.c901_ref_type),'A',get_code_name (t4031.c901_ref_type),''),'' ");
    	sbDetailsQuery.append(") refvalue, ");  
    	
    	sbDetailsQuery.append(" DECODE (t4030.c901_ref_type,'20295', get_partnum_desc (t4030.c4030_ref_id), ");
    	sbDetailsQuery.append(" '20296', get_set_name (t4030.c4030_ref_id),'20297', '', ");
    	sbDetailsQuery.append(" '20298', get_partnum_desc (t4030.c4030_ref_id)) Description, ");
    	
    	sbDetailsQuery.append(" TO_CHAR (t4031.c4031_start_date, 'YYYY') refyear, ");
    	sbDetailsQuery.append(" get_code_name (t4031.c901_ref_type) refmonthtype, ");
    	sbDetailsQuery.append(" DECODE(SIGN (  TO_NUMBER (TO_CHAR (t4031.c4031_start_date, 'YYYYMM'))- TO_NUMBER (");
    	sbDetailsQuery.append( launchDate );
    	sbDetailsQuery.append(")),-1, 'D',0, DECODE(NVL(gm_pkg_op_sheet.get_demand_sheet_status( ");
    	sbDetailsQuery.append( strDemandSheetId );
    	sbDetailsQuery.append(" ,t4031.c4031_start_date),50550),50550, 'E','D'),'E') controlvalue ");
    	sbDetailsQuery.append(" ,t4031.C4031_HISTORY_FL history_fl,get_all_comments(t4031.C4031_GROWTH_DETAILS_ID,'1224') comments ");
    	sbDetailsQuery.append(" ,t4031.C4031_GROWTH_DETAILS_ID grw_details_id ");
    	/*    	
    	sbDetailsQuery.append(" ,DECODE(gm_pkg_op_sheet.get_launch_date(t4030.c4020_demand_master_id,t4030.c4030_ref_id,t4030.c901_ref_type)");
    	sbDetailsQuery.append(" ,NULL,'DISABLE','ENABLE') row_control ");
    	*/
    	sbDetailsQuery.append(" FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031 ,t4010_group t4010 ");
    	sbDetailsQuery.append(" WHERE t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id ");
    	sbDetailsQuery.append(" AND t4030.c4030_ref_id = (TO_CHAR (t4010.c4010_group_id(+))) ");
    	sbDetailsQuery.append(" AND t4010.c4010_void_fl is null ");
    	sbDetailsQuery.append(" AND t4030.c4020_demand_master_id =  ");
    	sbDetailsQuery.append( strDemandSheetId );
    	sbDetailsQuery.append(" AND t4030.c901_ref_type = '");
    	sbDetailsQuery.append( strRefType );
    	sbDetailsQuery.append("' AND t4030.c4030_void_fl IS NULL ");
    	if(!strPNum.equals("")){
    		sbDetailsQuery.append(" AND regexp_like(t4030.c4030_ref_id, '"+strPNum+"')");
    	}
    	sbDetailsQuery.append(" AND t4031.c4031_void_fl IS NULL ");
    	sbDetailsQuery.append(" AND c4031_start_date BETWEEN TO_DATE ('");
    	sbDetailsQuery.append( strFromDate );
    	sbDetailsQuery.append("', 'Mon YY')AND LAST_DAY(TO_DATE ('");
    	sbDetailsQuery.append( strToDate );
    	sbDetailsQuery.append("', 'Mon YY'))order by NAME-- to_date(refmonth,'Mon YY') ");
    	
    	
    	log.debug(" sbDetailsQuery is "+sbDetailsQuery);
    	hmGrowthMap = crossTabReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailsQuery.toString());
    	
		return hmGrowthMap;
	}
	public String fetchDemandType(String strDemandMasterID) throws AppError {
		log.debug("Enter");
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn = new HashMap();
		String strDemandType = "";
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT  C901_DEMAND_TYPE DEMANDSHEETTYPE");
		sbQuery.append(" FROM T4020_DEMAND_MASTER ");
		sbQuery.append(" WHERE  C4020_VOID_FL IS NULL  ");
		sbQuery.append(" AND C4020_DEMAND_MASTER_ID =  ?  ");

		gmDBManager.setPrepareString(sbQuery.toString());
		gmDBManager.setString(1, strDemandMasterID);
		hmReturn = gmDBManager.querySingleRecord();
		strDemandType = (String) hmReturn.get("DEMANDSHEETTYPE");
		log.debug("DemandType: " + strDemandType);
		log.debug("Exit");
		return strDemandType;
	}
}
