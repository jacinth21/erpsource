package com.globus.operations.forecast.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger;


public class GmTTPSetupBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    /**
     * loadTTPList - This method will be used to fetch the TTP
     * @param hmParam - to be used later for any filters
     * @return ArrayList - will be used by drop down tag
     * @exception AppError
     */
    public ArrayList loadTTPList(HashMap hmParam) throws AppError
    {
        ArrayList alTTPList = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_op_ttp.gm_fc_fch_ttplist",1);
        gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
        gmDBManager.execute();
        
        alTTPList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1)));
        gmDBManager.close();

        return alTTPList;
    }
    /**
     * fetchTTPGroupMap - This method will fetch the TTP - Demand Sheet mapping info
     * @param hmParam - strTTPId
     * @exception AppError
     */
    public HashMap fetchTTPGroupMap (String strTTPId)throws AppError
    {
        HashMap hmReturn = new HashMap();
        ArrayList alUnselected = new ArrayList();
        ArrayList alSelected = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager ();
        
        gmDBManager.setPrepareString("Gm_pkg_op_ttp.gm_fc_fch_ttp_demandsheetmap",3);
        
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        
        gmDBManager.setString(1,strTTPId);
        
        gmDBManager.execute();
        
        alUnselected = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
        alSelected = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3)); 
        gmDBManager.commit();
        
        hmReturn.put("ALSELECTED",alSelected); 
        hmReturn.put("ALUNSELECTED",alUnselected);
        return hmReturn;
    }
    
    /**
     * fetchEditTTPGroupMap - This method will fetch the TTP - Demand Sheet mapping info
     * @param hmParam - strTTPId
     * @exception AppError
     */
    public HashMap fetchEditTTPGroupMap (String strTTPId)throws AppError
    {
        HashMap hmReturn = new HashMap();
        GmDBManager gmDBManager = new GmDBManager ();
            StringBuffer sbQuery = new StringBuffer();
            sbQuery.append(" SELECT C4050_TTP_ID TTPID , C4050_TTP_NM TTPNAME  ");
            sbQuery.append(" , C4050_PRIMARY_USER TTPOWNER ");
            sbQuery.append(" FROM T4050_TTP ");
            sbQuery.append(" WHERE  C4050_VOID_FL IS NULL ");
            sbQuery.append(" AND C4050_TTP_ID =  ?  ");
            
            log.debug("Query inside fetchGroupPartInfo is " + sbQuery.toString());

            gmDBManager.setPrepareString(sbQuery.toString());
            gmDBManager.setString(1,strTTPId);
            hmReturn = gmDBManager.querySingleRecord();
            return hmReturn;
    }
    
    /**
     * saveTTPGrouping - This method will be save / update the demand sheets associated to a TTP 
     * @param hmParam - parameters to be saved / updated
     * @exception AppError
     */
    public String saveTTPGrouping (HashMap hmParam)throws AppError
    {
        String strTTPIdFromDb = "";
        GmDBManager gmDBManager = new GmDBManager ();
        
        String strTTPId = GmCommonClass.parseNull((String)hmParam.get("TTPID"));
        String strTTPName = GmCommonClass.parseNull((String)hmParam.get("TTPNAME"));
        String strPrimaryUser = (String)hmParam.get("TTPOWNER");
        String strUserId = (String)hmParam.get("USERID");
        String strInputString = (String)hmParam.get("INPUTSTRING");

        gmDBManager.setPrepareString("Gm_pkg_op_ttp.gm_fc_sav_ttpdsmap",6);
        
        gmDBManager.registerOutParameter(6,java.sql.Types.CHAR);
        
        gmDBManager.setString(1,strTTPId);
        gmDBManager.setString(2,strTTPName);
        gmDBManager.setString(3,strPrimaryUser);
        gmDBManager.setString(4,strUserId);
        gmDBManager.setString(5,strInputString);
        
        gmDBManager.execute();
        
        strTTPIdFromDb = gmDBManager.getString(6);
        gmDBManager.commit();
        return strTTPIdFromDb;
    }
    
    /**
     * lockTTPSummary - This method will be lock the TTP sheet and create Order Qty (if need be) 
     * @param hmParam - parameters to be saved / updated
     * @exception AppError
     */
    public String lockTTPSummary (HashMap hmParam)throws AppError
    {
        String strTTPIdFromDb = "";
        GmDBManager gmDBManager = new GmDBManager ();
        
        String strTTPId = GmCommonClass.parseNull((String)hmParam.get("TTPID"));
        String strMonth = GmCommonClass.parseNull((String)hmParam.get("MONTHID"));
        String strYear = GmCommonClass.parseNull((String)hmParam.get("YEARID"));
        String strUserId = (String)hmParam.get("USERID");

        gmDBManager.setPrepareString("Gm_pkg_op_ttp.gm_fc_sav_ttplock",4);
        
        gmDBManager.setString(1,strTTPId);
        gmDBManager.setString(2,strMonth);
        gmDBManager.setString(3,strYear);
        gmDBManager.setString(4,strUserId);

        gmDBManager.execute();
        
        gmDBManager.commit();
        return strTTPIdFromDb;
    }   // End of lockTTPSummary 
    
}
