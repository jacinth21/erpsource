package com.globus.operations.forecast.beans;



import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmForecastReportBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
      

    /**
      * reportDSSummary - This Method is used to get crosstab details for Demand Sheet
      * @param String strStudyListId, String strSiteId  
      * @return HashMap
      * @exception AppError
    **/
    
      
          public HashMap reportDemandSheetSummary(HashMap hmParam) throws AppError
      {        log.debug(" values to fetch ds summary " + hmParam);
      
      HashMap hmReturn = new HashMap();
      ArrayList alForecastHeader = new ArrayList();
        
      ResultSet rsHeader = null;
      ResultSet rsDetailsExpected = null;      
      GmCrossTabReport gmCrossReport = new GmCrossTabReport();
      GmDBManager gmDBManager = new GmDBManager (); 
        
      String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
      String strGroupIds = GmCommonClass.parseNull((String)hmParam.get("GRPINPUTSTRING"));
      String strPartNums = GmCommonClass.parseNull((String)hmParam.get("PARTINPUTSTRING"));
      String strOverrideFlag = GmCommonClass.getCheckBoxValue((String)hmParam.get("OVERRIDEFLAG"));
      String strForeCastMonths = GmCommonClass.parseNull((String)hmParam.get("FORECASTMONTHS"));
      String strUnitType = GmCommonClass.parseNull((String) hmParam.get("UNITRPRICE"));
      
      
      // If no sheet id then return null 
      if (strDemandSheetId.equals(""))
      {
          return hmReturn;
      }
      
      try{
      // To fetch the transaction information
      gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_dsdetails",11);
      gmDBManager.registerOutParameter(7,OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(8,OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(9,OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(10,java.sql.Types.CHAR);
      gmDBManager.registerOutParameter(11,java.sql.Types.CHAR);        
      gmDBManager.setString(1,strDemandSheetId);
      gmDBManager.setString(2,strGroupIds);
      gmDBManager.setString(3,strPartNums);
      gmDBManager.setString(4,strOverrideFlag);
      gmDBManager.setString(5,strForeCastMonths);
      gmDBManager.setString(6,strUnitType);
      
      gmDBManager.execute();        
      
      rsHeader = (ResultSet)gmDBManager.getObject(7);
      rsDetailsExpected = (ResultSet)gmDBManager.getObject(8);       
      gmCrossReport.setGroupFl(true);
      gmCrossReport.setOverRideFl(true);
      gmCrossReport.setRowOverRideFl(true);
      gmCrossReport.setFixedColumn(true);
      gmCrossReport.setFixedParams(9,"CROSSOVERF");
      gmCrossReport.setFixedParams(10,"PS_FLAG");
      
      hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);
      log.debug("hmReturn: "+ hmReturn);
      alForecastHeader = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(9));        
      hmReturn.put("FORECASTHEAD", alForecastHeader);
      hmReturn.put("DEMANDEND",  gmDBManager.getString(10));
      hmReturn.put("SHEETSTATUS",  gmDBManager.getString(11));
      
      gmDBManager.close();
      }
      
      catch(Exception exp){
        gmDBManager.close();
        throw new AppError(exp);
      }   
      return hmReturn;
      }
      

      /**
       * reportDSSummary - This Method is used to get crosstab details for Demand Sheet
       * @param String strStudyListId, String strSiteId  
       * @return HashMap
       * @exception AppError
     **/
       public HashMap reportDemandSheetGrowthSummary(String strDemandSheetMonthId) throws AppError
       {
         
         HashMap hmReturn = new HashMap();

         ResultSet rsHeader = null;
         ResultSet rsDetailsExpected = null;
         GmCrossTabReport gmCrossReport = new GmCrossTabReport();
         GmDBManager gmDBManager = new GmDBManager (); 
         String strStatus = "";  
         
         try{
         // To fetch the transaction information
         gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_dsgrowthdetail",4);
         gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
         gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
         gmDBManager.registerOutParameter(4,OracleTypes.VARCHAR);
         gmDBManager.setString(1,strDemandSheetMonthId);
         gmDBManager.execute();
         
         rsHeader = (ResultSet)gmDBManager.getObject(2);
         rsDetailsExpected = (ResultSet)gmDBManager.getObject(3);
         strStatus = gmDBManager.getString(4);
         gmCrossReport.setFixedColumn(true);
         gmCrossReport.setFixedParams(6,"GTYPE");
         
         hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);

         gmDBManager.close();
         }
       
         catch(Exception exp){
        	 gmDBManager.close();
        	 throw new AppError(exp);
     	 }   
         log.debug("Values for demand sheet summary are " + hmReturn);
         hmReturn.put("SHEETSTATUS", strStatus);
         
         return hmReturn;
       }
       
       
       /**
        * reportDSRequestDetailSummary - This Method is used to get crosstab details for Demand Sheet
        * @param String strDemandSheetMonthId, String strDemandTypeId  
        * @return ArrayList
        * @exception AppError
      **/
        public ArrayList reportDemandSheetRequestSummary(String strDemandSheetMonthId, String strDemandTypeId) throws AppError
        {
           
          RowSetDynaClass rdResult = null;
          
          GmDBManager gmDBManager = new GmDBManager ();
          
          gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_requestdetail",3);
          
          gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
                     
          gmDBManager.setString(1,strDemandSheetMonthId);
          gmDBManager.setString(2,strDemandTypeId);           
          gmDBManager.execute();
           
          
          rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(3));
          
          gmDBManager.close();
          
                  
          return (ArrayList)rdResult.getRows();
          
        }
        
        /**
         * reportDSGrowthRequestDetails - This Method is used to get crosstab details for Demand Sheet
         * @param String strDemandSheetMonthId 
         * @return ArrayList
         * @exception AppError
       **/
         public ArrayList reportDSGrowthRequestDetails(String strDemandSheetMonthId) throws AppError
         {
            
           RowSetDynaClass rdResult = null;
           
           GmDBManager gmDBManager = new GmDBManager ();
           
           gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fch_growth_request_detail",2);
           
           gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
                      
           gmDBManager.setString(1,strDemandSheetMonthId);
                      
           gmDBManager.execute();
            
           
           rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));
           
           gmDBManager.close();
           
                   
           return (ArrayList)rdResult.getRows();
           
         }
       
       /**
        * reportDemandSheetPartHistory - This Method is used to get crosstab Part Override information
        * @param String strStudyListId, String strSiteId  
        * @return HashMap
        * @exception AppError
      **/
        public ArrayList reportDemandSheetPartHistory(String strDemandSheetMonthId) throws AppError
        {
          
          ArrayList alPartList = new ArrayList();
          GmDBManager gmDBManager = new GmDBManager (); 
            
          // To fetch the transaction information
          gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_dsparthistory",2);
          gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetMonthId);
          gmDBManager.execute();
          
          alPartList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
          
          log.debug("Values for demand sheet Part summary are " + alPartList);
          gmDBManager.close();

          return alPartList;
        }

        
        /**
         * fetchDemandSheetMapping - This Method is used to get Region information
         * @param String strDemandSheetMonthId  
         * @return ArrayList
         * @exception AppError
       **/
         public ArrayList fetchDemandSheetMapping(String strDemandSheetMonthId) throws AppError
         {
           
           ArrayList alRegionList = new ArrayList();
           GmDBManager gmDBManager = new GmDBManager (); 
             
           // To fetch the transaction information
           gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_dsmapping",2);
           gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
           gmDBManager.setString(1,strDemandSheetMonthId);
           gmDBManager.execute();
           
           alRegionList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
           
           gmDBManager.close();

           return alRegionList;
         }
        
      /**
       * fetchGroupFromDS - This method will fetch the Groups associated with a DS
       * @param demandSheetId
       * @return - ArrayList
       * @exception AppError
       */
      public ArrayList fetchDemandGroupMap (String strDemandSheetMonthId,String strDemandSheetId)throws AppError
      {
          ArrayList alGroupList = new ArrayList();

          GmDBManager gmDBManager = new GmDBManager ();
          gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_grpfords",3);
          gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetMonthId);
          gmDBManager.setString(2,strDemandSheetId);
          gmDBManager.execute();
          alGroupList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
          gmDBManager.commit();
          
          return alGroupList;
      }
      
      /**
       * fetchMonthlyDemandSheetInfo - This method will fetch demand sheet data from t4040  
       * @param strDemandSheetId 
       * @return HashMap
       * @exception AppError
       */
      public HashMap fetchMonthlyDemandSheetInfo(HashMap hmParam) throws AppError
      {
          HashMap hmReturn = new HashMap();
          log.debug("HmParam : "+hmParam);
          String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETID")); 
          String strMonth = GmCommonClass.parseNull((String)hmParam.get("MONTHID"));
          String strYear = GmCommonClass.parseNull((String)hmParam.get("YEARID"));
          String strMonYear = strMonth.concat("/").concat(strYear);
          
          GmDBManager gmDBManager = new GmDBManager ();
          gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_dsmonthdetails",3);
          gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetId);
          gmDBManager.setString(2,strMonYear);
          gmDBManager.execute();
          hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(3));
          gmDBManager.commit();
              
          return hmReturn;
      }
      
      /**
       * approveDemandSheet - This method will approve the demand sheet for that month
       * @param hmParam
       * @return - void
       * @exception AppError
       */
      public HashMap fetchapproveDetail (String strDemandSheetMonthId)throws AppError
      {
          HashMap hmReturn = new HashMap();
          
          GmDBManager gmDBManager = new GmDBManager ();
          gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_dsapprovaldetail",2);
          gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
          gmDBManager.setString(1,strDemandSheetMonthId);
          //gmDBManager.setString(2,strUserId);
    
          gmDBManager.execute();

          
          hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
          gmDBManager.commit();
          
          return  hmReturn;   
      }
      
      /**
       * approveDemandSheet - This method will approve the demand sheet for that month
       * @param hmParam
       * @return - void
       * @exception AppError
       */
      public void approveDemandSheet (HashMap hmParam)throws AppError
      {
          GmCommonBean gmCommon = new GmCommonBean ();
          
          String strDemandSheetMonthId  = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID"));
          String strComments  = GmCommonClass.parseNull((String)hmParam.get("APPCOMMENTS"));
          String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
          
          log.debug("Input Param:"+strDemandSheetMonthId+", "+strComments+", "+strUserId);
          
          GmDBManager gmDBManager = new GmDBManager ();
          gmDBManager.setPrepareString("gm_pkg_op_sheet.gm_fc_sav_approveds",2);
          gmDBManager.setString(1,strDemandSheetMonthId);
          gmDBManager.setString(2,strUserId);
          gmDBManager.execute();
          
          // save approval comments
          gmCommon.saveLog(gmDBManager, strDemandSheetMonthId, strComments,strUserId, "1226" );          
          gmDBManager.commit();

      }
      
      public RowSetDynaClass fchSetSheetReport(String strSheetReportRefId,String strSheetReportMonthId) throws AppError
      {         
        RowSetDynaClass rdResult = null;        
        GmDBManager gmDBManager = new GmDBManager ();        
        gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fch_setSheet",3);       
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);                   
        gmDBManager.setString(1,strSheetReportRefId);
        gmDBManager.setString(2,strSheetReportMonthId);                   
        gmDBManager.execute();
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(3));        
        gmDBManager.close();
        return  rdResult ;
        
      }
      
  public HashMap fchMultiPartDetails (HashMap hmParam) throws AppError
  {
      log.debug(" values to fetch ds summary " + hmParam);
      
      HashMap hmReturn = new HashMap();
      ResultSet rsHeader = null;
      ResultSet rsDetailsExpected = null;      
      GmCrossTabReport gmCrossReport = new GmCrossTabReport();
      GmDBManager gmDBManager = new GmDBManager (); 
        
      String strDemandSheetId = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
      String strPnum = GmCommonClass.parseNull((String)hmParam.get("PARTDETAILS"));      
      String strForeCastMonths = GmCommonClass.parseNull((String)hmParam.get("FORECASTMONTHS"));

      try{
      // To fetch the transaction information
      gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_op_fch_multisheet_details",5);
      gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
      gmDBManager.setString(1,strDemandSheetId);
      gmDBManager.setString(2,strPnum);      
      gmDBManager.setString(3,strForeCastMonths);            
      gmDBManager.execute();              
      rsHeader = (ResultSet)gmDBManager.getObject(4);
      rsDetailsExpected = (ResultSet)gmDBManager.getObject(5);             
      gmCrossReport.setFixedColumn(true);
      gmCrossReport.setFixedParams(5,"STATUS");      
      hmReturn = gmCrossReport.GenerateCrossTabReport(rsHeader,rsDetailsExpected);
      log.debug("Report: " + hmReturn);
      gmDBManager.close();
      }      
      catch(Exception exp){
        gmDBManager.close();
        throw new AppError(exp);
      }   
      return hmReturn;
  } 
   
  public ArrayList fchOpenSheets(HashMap hmParam) throws AppError
  {         
	String strDemandSheetId =  GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID")); 
    RowSetDynaClass rdResult = null;        
    GmDBManager gmDBManager = new GmDBManager ();  
    log.debug(" values in fchOpenSheets " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_op_fch_multipart_opensheets",2);       
    gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
    gmDBManager.setString(1,strDemandSheetId);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));        
    gmDBManager.close();
    log.debug("Open Sheets" + (ArrayList)rdResult.getRows());
    return (ArrayList)rdResult.getRows() ;
    
  }
  
  /*  This Method is called to pull the Forecast data for all the Shared Parts into the Multi Part TTP.
   *  For the passed in demand Sheet, get the forecast information for the shared part and load it into the 
   *  t4042_demand_sheet_detail for the Multi Part Demand Sheet. 
   */
  public void pullDemandSheets(HashMap hmParam) throws AppError
  { 	         
	  GmDBManager gmDBManager = new GmDBManager ();
	  String strDemandSheetString = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETSTRING")); 
	  String strUserID = GmCommonClass.parseNull((String)hmParam.get("USERID"));
	  log.debug("strDemandSheetString: "+ strDemandSheetString+ "strUserID: "+ strUserID);
	  String strTemp ="";
	  ArrayList outList = null;
	// Get the demand sheet data and split by 100 sheet and start pulling the data.
	if(strDemandSheetString!=null && strDemandSheetString.length()>800){
		
		outList = GmCommonClass.convertStringToList(strDemandSheetString, 800, ",");
	}else{
		outList = new ArrayList();
		outList.add(strDemandSheetString);
	}
	if(outList!=null){
		for(Iterator itr=outList.iterator();itr.hasNext();){
			strTemp = (String)itr.next();
			 	gmDBManager.setPrepareString("gm_pkg_op_sheet.gm_op_sav_multipartsheet",2);       
			    gmDBManager.setString(1,strTemp);
			    gmDBManager.setString(2,strUserID);    
			    gmDBManager.execute();
			    gmDBManager.commit();
		}
		 
	}
  }
}
