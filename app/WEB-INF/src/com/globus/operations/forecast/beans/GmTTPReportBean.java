package com.globus.operations.forecast.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmFilterConditionBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmTTPReportBean extends GmFilterConditionBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchMonthlySheetReport - This method fetches the details of the sheets
	 * generated every month
	 * 
	 * @return RowSetDynaClass
	 * @exception AppError
	 */
	public RowSetDynaClass reportMonthlySheetSummary() throws AppError {
		RowSetDynaClass rdResult = null;
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("Gm_pkg_op_ttp.gm_fc_fch_monthsheet_report", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();

		return rdResult;
	}
	/**
	 * fetchMonthlySheetReport - This method fetches the details of the sheets
	 * generated every month
	 * 
	 * @return RowSetDynaClass
	 * @exception AppError
	 */
	public RowSetDynaClass reportMonthlySheetSummary(HashMap hmParams) throws AppError {
		
		StringBuffer sbQuery = new StringBuffer();
		String strSheetnmregex = (String) hmParams.get("SHEETNMREGEX");
		String strStatus = (String) hmParams.get("STATUS");
		String strSheetType =(String) hmParams.get("SHEETTYPE");
		String strMonth = (String) hmParams.get("LOADMONTH");
		String strYear = (String) hmParams.get("LOADYEAR");
		String strTtpId = (String) hmParams.get("TTPNAME");
		String strPrimaryUser = (String) hmParams.get("SHEETOWNER");
		DBConnectionWrapper dbWrapper = new DBConnectionWrapper();
		
		sbQuery.append("SELECT   t4020.c4020_demand_master_id dmid, t4020.c4020_demand_nm demnm ");
		sbQuery.append(", get_code_name (t4020.c901_demand_type) dtype, get_user_name (t4040.c4040_primary_user) cuser ");
		sbQuery.append(", TO_CHAR (c4040_load_dt, 'mm/dd/yyyy') ldate, c4040_demand_period dper ");
		sbQuery.append(", get_code_name (c901_status) dstat, c4040_forecast_period fper ");
		sbQuery.append(", get_user_name (c4040_last_updated_by) luser, t4040.c4040_demand_sheet_id dmsheetid ");
		sbQuery.append(", TO_CHAR (c4040_load_dt, 'MM') ldtmonth, TO_CHAR (c4040_load_dt, 'YYYY') ldtyear ");
		sbQuery.append(", TO_CHAR (t4040.c4040_demand_period_dt, 'mm/dd/yyyy') perioddate, TO_CHAR (c901_status) statusid ");
		sbQuery.append(" FROM t4040_demand_sheet t4040, t4020_demand_master t4020 ");
		sbQuery.append(" WHERE t4040.c4020_demand_master_id = t4020.c4020_demand_master_id ");
		sbQuery.append(" AND t4040.c4040_void_fl IS NULL ");
		sbQuery.append(" AND t4020.C4020_INACTIVE_FL IS NULL ");
		sbQuery.append(" AND t4020.c4020_void_fl IS NULL ");
		
		if(strSheetnmregex!=null && strSheetnmregex.length()>0){
			sbQuery.append(" AND UPPER(t4020.c4020_demand_nm) LIKE UPPER('" );
			sbQuery.append(strSheetnmregex);
			sbQuery.append("')");
		}
		if(strStatus!=null && !strStatus.equals("0")){
			sbQuery.append("AND c901_status = '");
			sbQuery.append(strStatus);
			sbQuery.append("'");
		}
		if(strSheetType!=null && !strSheetType.equals("0")){
			sbQuery.append("AND t4020.c901_demand_type = '" );
			sbQuery.append(strSheetType);
			sbQuery.append("'");
		}
		if(strMonth!=null && !strMonth.equals("0")){
			sbQuery.append("AND t4040.c4040_demand_period_dt = ");
			sbQuery.append("TO_DATE ( '");
			sbQuery.append(strMonth  );
			sbQuery.append("'|| '/' ||  '");
			sbQuery.append(strYear );
			sbQuery.append("', 'MM/YYYY') ");
		}
		if(strTtpId!=null && !strTtpId.equals("0") ){
			sbQuery.append("AND c4052_ttp_detail_id IN");
			sbQuery.append("(SELECT t4052.c4052_ttp_detail_id from t4052_ttp_detail t4052 WHERE");
			sbQuery.append(" t4052.c4050_ttp_id=");
			sbQuery.append(strTtpId );
			if(strMonth!=null&& !strMonth.equals("0")){
				sbQuery.append(" AND t4052.c4052_ttp_link_date=TO_DATE ('");
				sbQuery.append(strMonth  );
				sbQuery.append("'|| '/01/' ||  '");
				sbQuery.append(strYear );
				sbQuery.append("', 'MM/DD/YYYY') ");			
			}
			sbQuery.append(")" );
		}
		
		if(strPrimaryUser!=null && !strPrimaryUser.equals("0")){
			sbQuery.append(" AND t4040.c4040_primary_user = '");
			sbQuery.append(strPrimaryUser );
			sbQuery.append("'");
		}
		sbQuery.append(getListDemandSheetFilter(hmParams));
		sbQuery.append(" ORDER BY demnm ");
		
		log.debug("# # # sbQuery "+sbQuery);
		
		RowSetDynaClass rdResult = dbWrapper.QueryDisplayTagRecordset(sbQuery.toString());

		return rdResult;
	}
	public ArrayList fetchTTPNames() throws AppError {

		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_ttpname", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		ArrayList alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
        gmDBManager.close();

		return alList;
	}

	/**
	 * fetchPartDemandSheetDetails - This Method is used to get crosstab details for listing all the demand sheets associated with a part
	 * @param HashMap hmParam
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchPartDemandSheetDetails(HashMap hmParam) throws AppError {
		log.debug(" values to fetch ds summary " + hmParam);

		HashMap hmReturn = new HashMap();
		HashMap hmCrossTabReport = new HashMap();
		ResultSet rsHeader = null;
		ResultSet rsDetailsExpected = null;
		GmCrossTabReport gmCrossReport = new GmCrossTabReport();
		GmDBManager gmDBManager = new GmDBManager();

		String strDemandSheetIds = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
		String strPartDetails = "";

		// If no sheet id then return null
		if (strDemandSheetIds.equals("")) {
			return hmReturn;
		}
		try{
		
		// To fetch the transaction information
		gmDBManager.setPrepareString("gm_pkg_op_ttp_summary.gm_fc_fch_partdemandsheetlist", 5);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CHAR);

		gmDBManager.setString(1, strDemandSheetIds);
		gmDBManager.setString(2, strPartNum);
		gmDBManager.execute();

		rsHeader = (ResultSet) gmDBManager.getObject(3);
		rsDetailsExpected = (ResultSet) gmDBManager.getObject(4);
		hmCrossTabReport = gmCrossReport.GenerateCrossTabReport(rsHeader, rsDetailsExpected);
		strPartDetails = (String) gmDBManager.getObject(5);
			
		hmReturn.put("CROSSTABREPORT",hmCrossTabReport);
		hmReturn.put("PARTDETAILS",strPartDetails);

		gmDBManager.close();
		}
		
		catch(Exception exp){
			exp.printStackTrace();
			throw new AppError(exp);
		}
		return hmReturn;

	}
}
