package com.globus.operations.forecast.beans;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmFooterCalcuationBean;

public class GmDemandReportBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	/**
	 * demandSheetReport - This method will report on the demand sheet
	 * 
	 * @param String -
	 *            strDemandSheetType
	 * @return RowSetDynaClass
	 * @exception AppError
	 */
	public RowSetDynaClass demandSheetReport(String strDemandSheetType) {
		RowSetDynaClass rdResult = null;
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		try {

			StringBuffer sbQuery = new StringBuffer();
			sbQuery
					.append(" SELECT C4020_DEMAND_MASTER_ID DEMANDSHEETID , C4020_DEMAND_NM DEMANDSHEETNAME , GET_CODE_NAME(C901_DEMAND_TYPE) DEMANDSHEETTYPENAME ");
			sbQuery
					.append(" , C4020_DEMAND_PERIOD DEMANDPERIOD, C4020_FORECAST_PERIOD FORECASTPERIOD , DECODE(C4020_INCLUDE_CURRENT_FL, 'Y', 'Yes', 'No') INCLUDECURRMONTH ");
			sbQuery.append(" , DECODE(C4020_INACTIVE_FL,'Y','Active','InActive') ACTIVEFLAG ");
			sbQuery.append(" FROM T4020_DEMAND_MASTER ");
			sbQuery.append(" WHERE  C4020_VOID_FL IS NULL  ");
			sbQuery.append(" AND C901_DEMAND_TYPE IN ('");
			sbQuery.append(strDemandSheetType);
			sbQuery.append("') ");
			sbQuery.append(" ORDER BY C4020_DEMAND_NM ");

			log.debug("Query inside demandSheetReport is " + sbQuery.toString());
			rdResult = dbCon.QueryDisplayTagRecordset(sbQuery.toString());
		}

		catch (Exception e) {
			// GmLogError.log("Exception in
			// GmPartNumBean:fetchEditDemandSheetInfo","Exception is:"+e);
			e.printStackTrace();
		}
		return rdResult;
	}

	/**
	 * fetchTTPReport - This method will report on the TTP
	 * 
	 * @return RowSetDynaClass
	 * @exception AppError
	 */
	public RowSetDynaClass fetchTTPReport() throws AppError {
		RowSetDynaClass rdResult = null;
		GmDBManager gmDBManager = new GmDBManager();
		log.debug(" inside fetch TTP report ");
		gmDBManager.setPrepareString("Gm_pkg_op_ttp.gm_fc_fch_ttp_report", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();

		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();

		return rdResult;
	}
	/**
	 * fetchTTPSummarySheets - This method will fetch the TTP Summary Sheet records only.
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchTTPSummarySheets(HashMap hmParam) throws AppError {
		ArrayList alSheetName = new ArrayList();
		HashMap hmFinalValue = new HashMap();
		
		String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
		String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTHID"));
		String strYear = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
		String strMonYear = strMonth.concat("/").concat(strYear);
		String strDemandSheetIds = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));
		String strInventoryId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYID"));
		String strForecastMonth = GmCommonClass.parseNull((String) hmParam.get("TTPFORECASTMONTHS"));
		String strStatus = "";
		String strFinalDate = "";
		String strDSStatus = "";
		log.debug("strDemandSheetIds = "+strDemandSheetIds);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_ttp_summary.gm_fc_fch_ttp_summary_sheets", 9);
		gmDBManager.setString(1, strTTPId);
		gmDBManager.setString(2, strMonYear);
		gmDBManager.setString(3, strDemandSheetIds);
		gmDBManager.setString(4, strInventoryId);
		gmDBManager.setString(5, strForecastMonth);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
		gmDBManager.execute();
		alSheetName = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		strStatus = (String) gmDBManager.getObject(7);
		strFinalDate = (String) gmDBManager.getObject(8);
		strDSStatus = (String) gmDBManager.getObject(9);
		
		gmDBManager.close();
		
		hmFinalValue.put("STATUS", strStatus);
		hmFinalValue.put("SHEETNAME", alSheetName);
		hmFinalValue.put("FINALDATE", strFinalDate);
		hmFinalValue.put("DSSTATUS", strDSStatus);
		return hmFinalValue;
	}
	/**
	 * fetchTTPSummaryReport - Cross tab report for TTP Summary
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchTTPSummaryReport(HashMap hmParam) throws AppError {
		//log.debug("Parameters in Bean: " + hmParam);

		GmCrossTabReport gmCrossReport = new GmCrossTabReport();

		ResultSet rsForecastHeader = null;
		ResultSet rsForecastDetails = null;
		ResultSet rsInventoryHeader = null;
		ResultSet rsInventoryDetails = null;

		HashMap hmForecast = new HashMap();
		HashMap hmInventory = new HashMap();
		HashMap hmFinalValue = new HashMap();
		HashMap hmLinkInfo = new HashMap();

		ArrayList alInventoryDetails = new ArrayList();
		ArrayList alTotQtyRequired = new ArrayList();
		ArrayList alTotQtyOrder = new ArrayList();
		ArrayList alUnitPrice = new ArrayList();
		ArrayList alAdjustmentList = new ArrayList();
		ArrayList alSheetName = new ArrayList();
		ArrayList alPendingRequest = new ArrayList();
		ArrayList alPNeedList = new ArrayList();
		ArrayList alParentSubList = new ArrayList();
		
		String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
		String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTHID"));
		String strYear = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
		String strMonYear = strMonth.concat("/").concat(strYear);
		String strDemandSheetIds = GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS"));
		String strInventoryId = GmCommonClass.parseNull((String) hmParam.get("INVENTORYID"));
		String strForecastMonth = GmCommonClass.parseNull((String) hmParam.get("TTPFORECASTMONTHS"));
		String strStatus = "";
		String strDSStatus = "";
		String strFinalDate = "";
		String strTempInventoryId = "";
		String strTTPType = "";
		
		log.debug(" ***************** " + strTTPId + " - " + " - " + strMonYear + " - " + strDemandSheetIds);
		log.debug("Inventory ID: " + strInventoryId + " Forecast Months " + strForecastMonth);

		GmDBManager gmDBManager = new GmDBManager();
		try {
			gmDBManager.setPrepareString("gm_pkg_op_ttp_summary.gm_fc_fch_ttp_mon_summary", 18);
			gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(12, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(13, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(14, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(15, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(16, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(17, OracleTypes. CURSOR);
			gmDBManager.registerOutParameter(18, OracleTypes.VARCHAR);
			gmDBManager.setString(1, strTTPId);
			gmDBManager.setString(2, strMonYear);
			gmDBManager.setString(3, strDemandSheetIds);
			gmDBManager.setString(4, strInventoryId);
			gmDBManager.setString(5, strForecastMonth);
			
			gmDBManager.execute();
			strTempInventoryId = (String) gmDBManager.getString(4);
			rsForecastHeader = (ResultSet) gmDBManager.getObject(6);
			rsForecastDetails = (ResultSet) gmDBManager.getObject(7);
			rsInventoryHeader = (ResultSet) gmDBManager.getObject(8);
			rsInventoryDetails = (ResultSet) gmDBManager.getObject(9);
			alTotQtyRequired = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10));
			alTotQtyOrder = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(11));

			//log.debug(" total qty reqd " + alTotQtyRequired);

			alUnitPrice = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(12));
			alSheetName = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(13));
			strStatus = (String) gmDBManager.getObject(14);
			strFinalDate = (String) gmDBManager.getObject(15);
			strTTPType = GmCommonClass.parseNull((String) gmDBManager.getObject(16));
			alParentSubList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(17));
			strDSStatus = (String) gmDBManager.getObject(18);
			
			
			//
	        
	        gmCrossReport.setGroupFl(true);			 
			gmCrossReport.setFixedColumn(true);
			gmCrossReport.setFixedParams(6, "CROSSOVERFL");
			gmCrossReport.setFixedParams(7,"Description");
			
	        hmForecast = gmCrossReport.GenerateCrossTabReport(rsForecastHeader, rsForecastDetails);
	        //log.debug("hmForecast: " + hmForecast);
			gmCrossReport = new GmCrossTabReport(); // reinitializing since we
													// wont have crossfl in the
													// subsequent queries
			hmInventory = gmCrossReport.GenerateCrossTabReport(rsInventoryHeader, rsInventoryDetails);

			gmDBManager.close();
			
			
			alInventoryDetails = (ArrayList) hmInventory.get("Details");
			//log.debug("alInventoryDetails: "+alInventoryDetails);
			
			
			
			
			hmLinkInfo = getLinkInfo("Inv");
			hmFinalValue = gmCrossReport.linkCrossTab(hmForecast, alInventoryDetails, hmLinkInfo);
			
			
			
			hmLinkInfo = getLinkInfo("TotQtyReq");
			hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alTotQtyRequired, hmLinkInfo);
		
			hmLinkInfo = getLinkInfo("UnitPrice");
			hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alUnitPrice, hmLinkInfo);
			

			log.debug(" ********* alTotQtyOrder size **** " + alTotQtyOrder.size()+"   "+alTotQtyOrder);
			if (alTotQtyOrder.size() > 0) {
				hmLinkInfo = getLinkInfo("OrderQty");
			//	log.debug("Completed link *** " + hmLinkInfo);
				hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alTotQtyOrder, hmLinkInfo);

			} else {
				hmLinkInfo = getLinkInfo("OrdQty");
				//log.debug("hmFinal before order = "+(ArrayList) hmFinalValue.get("Details"));
				alAdjustmentList = adjustmentList((ArrayList) hmFinalValue.get("Details"), alTotQtyOrder);
				hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alAdjustmentList, hmLinkInfo);
			}
			/*
			 * Need All Parent need arraylist to show the column on screen.
			 * Based on the paretn part PO qty and the order qty the need for the subpart gets calculated.
			 */
			alPNeedList = parentNeedList((ArrayList) hmFinalValue.get("Details"), alParentSubList,alTotQtyOrder.size());
			hmLinkInfo = getLinkInfo("PNeed");
			hmFinalValue = gmCrossReport.linkCrossTab(hmFinalValue, alPNeedList, hmLinkInfo);
			//log.debug("hmFinal last = "+(ArrayList) hmFinalValue.get("Details"));
			
			/*
			 * After getting the final list we need to updated the implants, instrument, grafic case and total with 
			 * updated qtys. Below method is used for the same.
			 */
			adjustFinalList((ArrayList) hmFinalValue.get("Details"), alTotQtyOrder);
			
		}

		catch (Exception exp) {
			exp.printStackTrace();
			gmDBManager.close();
			throw new AppError(exp);
		}
		hmFinalValue.put("INVENTORYID", strTempInventoryId);
		hmFinalValue.put("STATUS", strStatus);
		hmFinalValue.put("SHEETNAME", alSheetName);
		hmFinalValue.put("FINALDATE", strFinalDate);
		hmFinalValue.put("TTPTYPE", strTTPType);
		hmFinalValue.put("DSSTATUS", strDSStatus);
		//log.debug("hmFinalValue: " + hmFinalValue);

		return hmFinalValue;
	}
	private ArrayList parentNeedList(ArrayList alTTPData, ArrayList alParentSubList, int iAlOrdSize) {
		Iterator itDetails = alTTPData.iterator();
		Iterator itParentSubDetails = alParentSubList.iterator();
		ArrayList alPNeed = new ArrayList();
		HashMap hmTempDetails = new HashMap();
		HashMap hmAllTTPData = new HashMap();
		HashMap hmParentSubData = new HashMap();
		HashMap hmSubPartRowObj = new HashMap();
		HashMap hmParentPartRowObj = new HashMap();
		HashMap hmSubPartRowObjTemp = new HashMap();
		
		String strSubPart ="";
		String strParentPart ="";
		String strQty ="";
		String strPO = "";
		String strOrdQty = "";
		String strInv = "";
		String strTotReq = "";
		String strTempSubPart = "";
		String strTempNeed = "";
		String strUnitCost = "";
		
		int iAlNeedSize = 0;
		while (itDetails.hasNext()){
			hmAllTTPData = (HashMap) itDetails.next();
			hmTempDetails.put((String)hmAllTTPData.get("ID"),hmAllTTPData);
		}
		//log.debug("alParentSubList size = "+alParentSubList.size());
		//log.debug("hmTempDetails = "+hmTempDetails);
		while (itParentSubDetails.hasNext()){
			int iPNeed = 0;
			int iTotReq = 0;
			int iOrdQty = 0;
			int iNeedCnt = 0;
			double dbOrdCost = 0;
			hmParentSubData = (HashMap) itParentSubDetails.next();
			strSubPart = (String)hmParentSubData.get("SUBPART");
		    strParentPart = (String)hmParentSubData.get("PARENTPART");
		    strQty = (String)hmParentSubData.get("QTY");
		    
		    hmSubPartRowObj = GmCommonClass.parseNullHashMap((HashMap) hmTempDetails.get(strSubPart));
		    hmParentPartRowObj = GmCommonClass.parseNullHashMap((HashMap)hmTempDetails.get(strParentPart));
		    //From Parent Part
		    strPO = GmCommonClass.parseZero((String)hmParentPartRowObj.get("PO"));
		    strOrdQty = GmCommonClass.parseZero((String)hmParentPartRowObj.get("Order Qty"));
		    //From Subpart Part
		    strInv = GmCommonClass.parseZero((String)hmSubPartRowObj.get("Total Inventory"));
		    strTotReq = GmCommonClass.parseZero((String)hmSubPartRowObj.get("Total Req"));
		    strUnitCost = GmCommonClass.parseZero((String)hmSubPartRowObj.get("Unit Cost $"));
		    
		    iAlNeedSize = GmCommonClass.parseNullArrayList(alPNeed).size();
		    for(int k=0; k<iAlNeedSize; k++)
		    {
		    	iNeedCnt = 0;
		    	HashMap hmPneed = (HashMap)alPNeed.get(k);
		    	strTempSubPart = (String)hmPneed.get("ID");	
		    	if(strTempSubPart.equals(strSubPart))
		    	{
		    		iNeedCnt = 1;
		    		strTempNeed = (String)hmPneed.get("PNEED");
		    		iPNeed = Integer.parseInt(strTempNeed)+ (Integer.parseInt(strPO) + Integer.parseInt(strOrdQty)) * Integer.parseInt(strQty);
		    		hmPneed.put("PNEED", Integer.toString(iPNeed));
		    		iTotReq = (Integer.parseInt(strPO) + Integer.parseInt(strOrdQty)) * Integer.parseInt(strQty) + Integer.parseInt(strTotReq);
		    		break;
		    	}
		    }
		    if(iNeedCnt == 0)
		    {
		    	HashMap hmParentNeed = new HashMap();
		    	iPNeed = (Integer.parseInt(strPO) + Integer.parseInt(strOrdQty)) * Integer.parseInt(strQty);
		    	iTotReq = iPNeed + Integer.parseInt(strTotReq);	
		    	hmSubPartRowObjTemp = GmCommonClass.parseNullHashMap((HashMap) hmTempDetails.get(strSubPart));
		    	if(!hmSubPartRowObjTemp.isEmpty()){
			    	hmParentNeed.put("ID", strSubPart);
			    	hmParentNeed.put("PNEED", Integer.toString(iPNeed));		    			    
			    	alPNeed.add(hmParentNeed);
		    	}
		    }
		    iOrdQty = iTotReq - Integer.parseInt(strInv);
		    iOrdQty = iOrdQty < 0 ? 0: iOrdQty;
		    dbOrdCost = iOrdQty * Double.parseDouble(strUnitCost);
	    	hmSubPartRowObj.put("Total Req", Integer.toString(iTotReq));
	    	if (iAlOrdSize <= 0)
		    {
	    		hmSubPartRowObj.put("Order Qty", Integer.toString(iOrdQty));
	    		hmSubPartRowObj.put("Order Cost$", String.valueOf(dbOrdCost));
		    }
		}		
		log.debug("alPNeed = "+alPNeed);				
		return alPNeed;
	}
	/**
	 * getLinkInfo - This Method is used to to get the link object information
	 * 
	 * @return HashMap
	 * @exception AppError
	 */
	private HashMap getLinkInfo(String strType) {
		HashMap hmLinkDetails = new HashMap();
		HashMap hmapValue = new HashMap();
		ArrayList alLinkList = new ArrayList();

		// First parameter holds the link object name
		hmLinkDetails.put("LINKID", "ID");

		if (strType.equals("Inv")) {
			// Parameter to be added to link list
			hmapValue.put("KEY", "In Stock ");
			hmapValue.put("VALUE", "In Stock");
			alLinkList.add(hmapValue);
			/*
			hmapValue = new HashMap();
			hmapValue.put("KEY", "PO DHR ");
			hmapValue.put("VALUE", "PO + DHR");
			alLinkList.add(hmapValue);
			*/
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", "Open PO ");
			hmapValue.put("VALUE", "PO");
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", "Open DHR ");
			hmapValue.put("VALUE", "DHR");
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", "Build Set");
			hmapValue.put("VALUE", "Build Sets");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "Total Inventory");
			hmapValue.put("VALUE", "Total Inventory");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "Quarantine Allocated (Scrap) ");
			hmapValue.put("VALUE", "Quar. Avail");
			alLinkList.add(hmapValue);
			
			hmapValue = new HashMap();
			hmapValue.put("KEY", "Quarantine Allocated (Inventory) ");
			hmapValue.put("VALUE", "Quar. Alloc");
			alLinkList.add(hmapValue);
			/*
			hmapValue = new HashMap();
			hmapValue.put("KEY", "Parent Part Qty");
			hmapValue.put("VALUE", "Parent Part Qty");
			alLinkList.add(hmapValue);
			*/
			hmLinkDetails.put("POSITION", "AFTER");
		} else if (strType.equals("TotQtyReq")) {
			// Parameter to be added to link list

			hmapValue = new HashMap();
			hmapValue.put("KEY", "TOTAL_PENDING_REQUEST");
			hmapValue.put("VALUE", "TPR");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "FORECAST_QTY");
			hmapValue.put("VALUE", "Forecast Qty");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "TOTAL_REQUIRED");
			hmapValue.put("VALUE", "Total Req");
			alLinkList.add(hmapValue);

			hmLinkDetails.put("POSITION", "AFTER");
		}

		else if (strType.equals("OrdQty")) {
			hmapValue = new HashMap();
			hmapValue.put("KEY", "ORDQTY");
			hmapValue.put("VALUE", "Order Qty");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "ORDCOST");
			hmapValue.put("VALUE", "Order Cost$");
			alLinkList.add(hmapValue);

			hmLinkDetails.put("POSITION", "AFTER");
		}

		else if (strType.equals("UnitPrice")) {
			// Parameter to be added to link list
			hmapValue.put("KEY", "UNIT_PRICE");
			hmapValue.put("VALUE", "Unit Cost $");
			alLinkList.add(hmapValue);
			hmLinkDetails.put("POSITION", "AFTER");
			hmLinkDetails.put("COLUMNNAME", "Description");
		} else if (strType.equals("OrderQty")) {
			// Parameter to be added to link list
			// Parameter to be added to link list
			hmapValue.put("KEY", "QTY");
			hmapValue.put("VALUE", "Order Qty");
			alLinkList.add(hmapValue);

			hmapValue = new HashMap();
			hmapValue.put("KEY", "TOTALCOST");
			hmapValue.put("VALUE", "Order Cost$");
			alLinkList.add(hmapValue);

			hmLinkDetails.put("POSITION", "AFTER");
			
		}
		else if(strType.equals("PNeed")) {
			hmapValue.put("KEY", "PNEED");
			hmapValue.put("VALUE", "Parent Need");
			alLinkList.add(hmapValue);
			hmLinkDetails.put("POSITION", "BEFORE");
			hmLinkDetails.put("COLUMNNAME", "TPR");
		}
		
		hmLinkDetails.put("LINKVALUE", alLinkList);

		return hmLinkDetails;
	}

	/*
	 * This method is used to calculate the order pricing based on order qty
	 * 
	 */
	private ArrayList adjustmentList(ArrayList alDetails, ArrayList alTotQtyOrder) {
		Iterator itDetails = alDetails.iterator();
		ArrayList alTemp = new ArrayList();
		HashMap hmTempDetails = new HashMap();
		String strOrdQty = "0";
		String strOrdCost = "0";
		int intGroupingCount = 1;
		int intTotalRequired = 0;
		int intTotalInventory = 0;
		int intOrdQty = 0;
		double dblOrdCost = 0.0;
		double dblUnitPrice = 0.0;
		double dblTotalL2 = 0.0;
		double dblTotalL3 = 0.0;
		int intOrdQtyL2 = 0;
		int intOrdQtyL3 = 0;

		if (alTotQtyOrder.size() == 0) {
			while (itDetails.hasNext()) {
				hmTempDetails = (HashMap) itDetails.next();
				intTotalRequired = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Total Req")));
				intTotalInventory = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Total Inventory")));
				//intTotalInventory += Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Parent Part Qty")));
				intGroupingCount = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("GROUPINGCOUNT")));

				if (intTotalInventory > intTotalRequired && intGroupingCount == 0) {
					strOrdQty = "0";
					strOrdCost = "0";
				} else {
					intOrdQty = intTotalRequired - intTotalInventory;
					dblUnitPrice = Double.parseDouble(GmCommonClass.parseZero((String) hmTempDetails.get("Unit Cost $")));
					if (intGroupingCount == 0) {
						dblOrdCost = intOrdQty * dblUnitPrice;
						dblTotalL2 = dblTotalL2 + dblOrdCost;
						intOrdQtyL2 = intOrdQtyL2 + intOrdQty;
						strOrdCost = String.valueOf(dblOrdCost);
						strOrdQty = String.valueOf(intOrdQty);
					} else if (intGroupingCount == 1) {
						// log.debug(" total 2 is " + dblTotalL2);
						dblTotalL3 = dblTotalL2 + dblTotalL3;
						intOrdQtyL3 = intOrdQtyL2 + intOrdQtyL3;
						strOrdCost = String.valueOf(dblTotalL2);
						strOrdQty = String.valueOf(intOrdQtyL2);
						dblTotalL2 = 0.0;
						intOrdQtyL2 = 0;
					} else if (intGroupingCount == 3) {
						strOrdCost = String.valueOf(dblTotalL3);
						strOrdQty = String.valueOf(intOrdQtyL3);
					}
				}
				HashMap hmTemp = new HashMap();
				hmTemp.put("ID", hmTempDetails.get("ID"));
				hmTemp.put("ORDQTY", strOrdQty);
				hmTemp.put("ORDCOST", strOrdCost);
				alTemp.add(hmTemp);
			}
		}
		return alTemp;
	}

    /**
     * executeJob - This method will execute the job for the specified demand
     * sheet
     */
    public void executeJob(String strDemandSheetId,HashMap hmParam) throws AppError {
    	GmDBManager gmDBManager = new GmDBManager();
    	gmDBManager.setPrepareString("gm_pkg_op_ld_demand.gm_op_sav_load_detail", 3);
    	gmDBManager.setString(1, strDemandSheetId);
    	gmDBManager.setInt(2,91951);  //Hard coded Ref_Type
    	gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("USERID")));
       	gmDBManager.execute();
       	gmDBManager.commit();
    }


	public ArrayList fetchTTPForecastRequest(HashMap hmParam) throws AppError {
		RowSetDynaClass rdResult = null;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_ttp_summary.gm_fc_fch_request", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETIDS")));
		gmDBManager.execute();
		rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return (ArrayList) rdResult.getRows();
	}

    public HashMap fetchTPRReport(HashMap hmParam) throws AppError
    {
   	 log.debug("Enter");
     	 HashMap hmReturn = new HashMap();
   	 RowSetDynaClass rdResult = null;    	 
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_op_ttp_summary.gm_fc_fch_tprdetails",11);
        gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
        gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
        
        String demandsheetid = GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETID"));
        
        gmDBManager.setString(1, GmCommonClass.parseNull((String)hmParam.get("PNUM")));
        
        gmDBManager.setString(2, GmCommonClass.parseNull((String)hmParam.get("INVENTORYID")));       
        gmDBManager.setString(3, GmCommonClass.parseNull((String)hmParam.get("PSTATUS")));
        gmDBManager.setString(4, GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETMONTHID"))); //demand sheet id
        gmDBManager.setString(5, GmCommonClass.parseNull((String)hmParam.get("DEMANDSHEETID"))); //demand master id
        gmDBManager.setString(6, GmCommonClass.parseNull((String)hmParam.get("SETID")));
        log.debug("hmParam is "+ hmParam);
        gmDBManager.execute();
        
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(7));
        
        hmReturn.put("TPRREPORT", rdResult.getRows());
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(8));
        hmReturn.put("TPRVOIDREPORT", rdResult.getRows());         
        hmReturn.put("PDESC",gmDBManager.getString(9));
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(10));
        hmReturn.put("TPRSALESREPORT",rdResult.getRows());
        
        rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(11));
        hmReturn.put("TPRLOANERBOREPORT",rdResult.getRows());
        
        gmDBManager.close();
        log.debug("Exit");
        return hmReturn;
    }
    
    /*
	 * This method is used to calculate the order pricing based on order qty
	 * 
	 */
	private void adjustFinalList(ArrayList alDetails, ArrayList alTotQtyOrder) {
		Iterator itDetails = alDetails.iterator();
		HashMap hmTempDetails = new HashMap();
		String strOrdQty = "0";
		String strTotReqQty = "0";
		String strOrdCost = "0";
		String strPneed = "0";
		int intGroupingCount = 1;
		int intTotalRequired = 0;
		int intOrdQty = 0;
		int intPneed = 0;
		double dblOrdCost = 0.0;
		double dblUnitPrice = 0.0;
		double dblTotalL2 = 0.0;
		double dblTotalL3 = 0.0;
		int intOrdQtyL2 = 0;
		int intOrdQtyL3 = 0;
		int intTotReqQtyL2 = 0;
		int intTotReqQtyL3 = 0;
		int intPneedL2 = 0;
		int intPneedL3 = 0;
		
		while (itDetails.hasNext()) {
			hmTempDetails = (HashMap) itDetails.next();
			intTotalRequired = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Total Req")));
			intGroupingCount = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("GROUPINGCOUNT")));
			intOrdQty = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Order Qty")));
			intPneed = Integer.parseInt(GmCommonClass.parseZero((String) hmTempDetails.get("Parent Need")));
			dblUnitPrice = Double.parseDouble(GmCommonClass.parseZero((String) hmTempDetails.get("Unit Cost $")));
			if (intGroupingCount == 0) {
				dblOrdCost = intOrdQty * dblUnitPrice;
				dblTotalL2 = dblTotalL2 + dblOrdCost;
				intOrdQtyL2 = intOrdQtyL2 + intOrdQty;
				intTotReqQtyL2 = intTotReqQtyL2 + intTotalRequired;
				intPneedL2 = intPneedL2 + intPneed;
				strOrdCost = String.valueOf(dblOrdCost);
				strOrdQty = String.valueOf(intOrdQty);					
			} else if (intGroupingCount == 1) {
				dblTotalL3 = dblTotalL2 + dblTotalL3;
				intOrdQtyL3 = intOrdQtyL2 + intOrdQtyL3;
				intTotReqQtyL3 = intTotReqQtyL2 + intTotReqQtyL3;
				intPneedL3 = intPneedL3 + intPneedL2;
				strOrdCost = String.valueOf(dblTotalL2);
				strOrdQty = String.valueOf(intOrdQtyL2);
				strTotReqQty = String.valueOf(intTotReqQtyL2);
				strPneed = String.valueOf(intPneedL2);
				dblTotalL2 = 0.0;
				intOrdQtyL2 = 0;
				intTotReqQtyL2 = 0;
				intPneedL2 = 0;
				hmTempDetails.put("Total Req",strTotReqQty);
				hmTempDetails.put("Parent Need",strPneed);
			} else if (intGroupingCount == 3) {
				strOrdCost = String.valueOf(dblTotalL3);
				strOrdQty = String.valueOf(intOrdQtyL3);
				strTotReqQty = String.valueOf(intTotReqQtyL3);
				strPneed = String.valueOf(intPneedL3);
				hmTempDetails.put("Total Req",strTotReqQty);
				hmTempDetails.put("Parent Need",strPneed);
			}
			if (alTotQtyOrder.size() == 0) {
			hmTempDetails.put("Order Qty",strOrdQty);
			hmTempDetails.put("Order Cost$", strOrdCost);
			}		
	}		
	}
}
