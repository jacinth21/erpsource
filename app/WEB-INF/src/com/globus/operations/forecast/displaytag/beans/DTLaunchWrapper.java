package com.globus.operations.forecast.displaytag.beans;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.forms.GmDemandSheetSetupForm;


public class DTLaunchWrapper extends TableDecorator
{
    private String strLaunchDt; 
    private DynaBean db ;
    private int intlDtCount = 0;
    private int intNDtCount = 0;
    private int intIdCount = 0;
    private int intSize = 0;
    private String strId="";
    private String strHistoryfl="";
    private String strIdValue="";
    private String strRefType="";
    public StringBuffer  strValue = new StringBuffer();
    private HashMap hmVal = new HashMap();
    String strCodeID;
	String strSelected;
	
	private ArrayList alDates = new ArrayList();
	GmDemandSheetSetupForm gmDemandSheetSetupForm;
  
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmDemandSheetSetupForm = (GmDemandSheetSetupForm) getPageContext().getAttribute("frmDemandSheetSetup", PageContext.REQUEST_SCOPE);

		alDates = gmDemandSheetSetupForm.getAlDates();
		intSize = alDates.size(); 

	}
	
    public String getLAUNCHDT()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        strLaunchDt = String.valueOf(db.get("LAUNCHDT"));
    	strId = String.valueOf(db.get("MAPPINGID"));
    	strHistoryfl = String.valueOf(db.get("HISTFL"));
        strValue.setLength(0);
        strValue.append("<select name = cboLaunchDate");
        strValue.append(String.valueOf(intlDtCount) );
        strValue.append("> <option value= >"); 
        strValue.append("[Choose One]</option>");                       
       
        for (int i=0;i<intSize;i++)
        {
				hmVal = (HashMap)alDates.get(i);
				strCodeID = (String)hmVal.get("MONTH");
				strSelected = strLaunchDt.equals(strCodeID)?"selected":"";
				strValue.append("<option " + strSelected + " value= " ) ;
				strValue.append( strCodeID +  ">" + hmVal.get("MMNAME") + "</option>");
		}
        strValue.append( "</select>");
        if ( strHistoryfl.equals("Y") ) {
        	strValue.append( " <img  id='imgEdit' style='cursor:hand' onclick='javascript:fnLaunchDateHistory("+strId+")'; title='Click to open Launch date history'" );  
        	strValue.append( "	src='/images/icon_History.gif' align='bottom'  height=13 width=13 />");
        }
        intlDtCount++;
        return strValue.toString();
    }
     
    public String getNEWDT()
    {
    	db =    (DynaBean) this.getCurrentRowObject();
        strValue.setLength(0);
        strValue.append("<select name = cboNewDate");
        strValue.append(String.valueOf(intNDtCount) );
        strValue.append("> <option value= >"); 
        strValue.append("[Choose One]</option>");                       
       
        for (int i=0;i<intSize;i++)
        {
				hmVal = (HashMap)alDates.get(i);
				strCodeID = (String)hmVal.get("MONTH");
				strValue.append("<option  value= " ) ;
				strValue.append( strCodeID +  ">" + hmVal.get("MMNAME") + "</option>");
		}
        strValue.append( "</select>");
        intNDtCount++;
        return strValue.toString();
        
    }
     
    public String getID()
    {
    	db = (DynaBean) this.getCurrentRowObject();
    	
       	strId = String.valueOf(db.get("ID"));
    	strRefType = String.valueOf(db.get("REF_TYPE"));  
    	String strShipFl = String.valueOf(db.get("SETSHIPPED"));
    	
    	//strId = strId.equals("null") ? "" :strId;    	
    	strIdValue = strId.equals("null") ? "-9999" :strId;
    	if (strId.equals("null") || strId.equals("-9999")){
    		strId = "" ;
    	}
    	strValue.setLength(0);
		strValue.append("<input type=hidden name=txtId");
		strValue.append(String.valueOf(intIdCount) );
		strValue.append(" value = ");
		strValue.append(strIdValue);
		strValue.append("^");
		strValue.append(strRefType);
		strValue.append(" >");
		strValue.append(strId);
		
		strValue.append("<input type=hidden name=txtShipFl");
		strValue.append(String.valueOf(intIdCount) );
		strValue.append(" value = ");
		strValue.append(strShipFl);
		strValue.append(" >");
		
    	intIdCount++;
    	
        return strValue.toString();
    }
    
}


