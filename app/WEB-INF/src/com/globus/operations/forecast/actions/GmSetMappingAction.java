/**
 * FileName    : GmSetMappingAction.java 
 * Description :
 * Author      : vprasath
 * Date        : Aug 8, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmDemandSheetSetupForm;
 
public class GmSetMappingAction extends GmAction {
	
	 public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
	        try {
	        GmDemandSheetSetupForm gmDemandSheetSetupForm = (GmDemandSheetSetupForm)form;
	        gmDemandSheetSetupForm.loadSessionParameters(request);
	        GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
	        
	        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	        String strOpt = gmDemandSheetSetupForm.getStrOpt();
	        String strDemandSheetId = gmDemandSheetSetupForm.getDemandSheetId();
	        String strInput = gmDemandSheetSetupForm.getHinputSetAction();       
	        RowSetDynaClass rdResult = null;
	        
	        log.debug(GmCommonClass.getHashMapFromForm(gmDemandSheetSetupForm));
	        log.debug (" Testing. ..... ");
	        if (strOpt.equals("save"))
	        { 
	            gmDemandSetupBean.saveSetMap(strInput); 
	        } 
	        
	        rdResult = gmDemandSetupBean.loadSetMapList(strDemandSheetId);
	        gmDemandSheetSetupForm.setLdtResult(rdResult.getRows());
	        gmDemandSheetSetupForm.setAlSetActionType((ArrayList)GmCommonClass.getCodeList("SATYP"));
	      
	        }
	        catch(Exception exp){
	            exp.printStackTrace();
	            throw new AppError(exp);
	        }
	        
	        return mapping.findForward("success");
	    }


}
