package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
 
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.operations.forecast.forms.GmDemandSheetSetupForm;

public class GmDemandSheetHistoryAction extends GmAction{

	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmDemandSheetSetupForm gmDemandSheetSetupForm = (GmDemandSheetSetupForm)form;
        gmDemandSheetSetupForm.loadSessionParameters(request);
        GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
        
        GmCommonBean gmDemandCommonBean = new GmCommonBean();
        
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
         
        String strDemandMasterId = gmDemandSheetSetupForm.getDemandMasterId();
        
        String strPartNumber = GmCommonClass.removeSpaces(gmDemandSheetSetupForm.getPartNumbers());
        String strID = gmDemandSheetSetupForm.getLogId();
        String strType = gmDemandSheetSetupForm.getLogType();
        
        HashMap hmTemp = new HashMap();
              
         
        ArrayList alList = new ArrayList();
        ArrayList alListPar = new ArrayList();
        alList = gmDemandCommonBean.getLog(strID, strType);
        request.setAttribute("ALCOMMENTS",alList);
        
      //  log.debug(GmCommonClass.getHashMapFromForm(gmDemandSheetSetupForm));
                 
        	hmTemp = gmDemandSetupBean.loadDemandsheetPar(strDemandMasterId, strPartNumber);
        	
            log.debug(" values fetched for edit " + alList);
            request.setAttribute("hmReturn",hmTemp);
            
            String strDetailId =  GmCommonClass.parseNull((String)hmTemp.get("DETAILID"));
            alListPar = gmDemandSetupBean.loadPartHistory(strDemandMasterId, strDetailId);
             
             log.debug("strDetailID is  "+ strDetailId);
             request.setAttribute("ALDEMANDPAR",alListPar);
             log.debug("size is " + alListPar.size());
                         
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        
        return mapping.findForward("success");
    }
}
