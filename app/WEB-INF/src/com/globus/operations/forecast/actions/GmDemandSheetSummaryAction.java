package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.operations.forecast.beans.GmDemandReportBean;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.beans.GmForecastReportBean;
import com.globus.operations.forecast.forms.GmDemandSheetSummaryForm;

public class GmDemandSheetSummaryAction extends GmAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
	try {
	    GmDemandSheetSummaryForm gmDemandSheetSummaryForm = (GmDemandSheetSummaryForm)form;
	    gmDemandSheetSummaryForm.loadSessionParameters(request);
	    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
	    GmCommonBean gmCommonBean = new GmCommonBean();
	    GmForecastReportBean gmForecastReportBean = new GmForecastReportBean(); 
       
 	

		    
	    // String strOpt = validateDate(gmDemandSheetSummaryForm);
	    String strOpt = gmDemandSheetSummaryForm.getStrOpt();
	    String strDemandSheetId = gmDemandSheetSummaryForm.getDemandSheetId();
	    String strDemandSheetMonthId = gmDemandSheetSummaryForm.getDemandSheetMonthId();
	    String refID = gmDemandSheetSummaryForm.getRefId();
    	String month = gmDemandSheetSummaryForm.getMonth();
    	
    	    
	    log.debug(" Testing action "+strOpt);
	    
	    String strPartInput = "";
	    HashMap hmDemandSheet = new HashMap();   
	    HashMap hmTemp = new HashMap();
	    HashMap hmParam = new HashMap();
	    HashMap hmValues = new HashMap();
	    
	    HashMap hmapValue = new HashMap();
		
	    
	    ArrayList alDemandSheetList = new ArrayList();
	    ArrayList alMonth = new ArrayList();
	    ArrayList alYear = new ArrayList();
	    ArrayList alGroupList = new ArrayList();
	    ArrayList alidmon = new ArrayList();
	    List lResult = null;
	    boolean blnApprove = false;
	    
		String strDeptId = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptId"));
		String strAccessFilter = "";

		//The filter condition should be applied for ICS Dept users alone.
		if (strDeptId.equals("ICS"))
			strAccessFilter =  this.getAccessFilter(request, response);
		
	    hmTemp.put("ACCESS_FILTER", strAccessFilter);
	 
        // Code to get the Party Access Condition.
        HttpSession session = request.getSession(false);
        HashMap hmPartyParam = new HashMap();
		hmPartyParam.put("PartyId", (String)session.getAttribute("strSessPartyId"));
		hmPartyParam.put("FunctionId","1015");//Corresponds to Demand Sheet Access Function Name

		HashMap hmPartyAccess = (new GmAccessFilter()).fetchPartyAccessPermission(hmPartyParam);
		
		String strUpdateFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("UPDFL"));
		String strVoidFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("VOIDFL"));
		String strReadFlg = GmCommonClass.parseNull((String)hmPartyAccess.get("READFL"));
		
		// If the party is not having update access for the the function, the will have limited access in Demand Sheet 
		// summary and in Monthly demand sheet.
		String strLimitedAccess  = "";
		if(strUpdateFlg!="" && !strUpdateFlg.equalsIgnoreCase("Y")){
			strLimitedAccess="true";
			request.setAttribute("DMNDSHT_LIMITED_ACC", strLimitedAccess);
		}
		
	   if (strOpt.equals("fetchPPP"))
	    {
	    	GmDemandReportBean gmDemandReportBean = new GmDemandReportBean();
	    	HashMap hmParams = GmCommonClass.getHashMapFromForm(gmDemandSheetSummaryForm);             
                        
       	 	HashMap hmReturn =  gmDemandReportBean.fetchTPRReport(hmParams);   
       	    gmDemandSheetSummaryForm.setAlTPRReport((ArrayList)hmReturn.get("TPRREPORT"));
       	    gmDemandSheetSummaryForm.setAlTPRVoidReport((ArrayList)hmReturn.get("TPRVOIDREPORT"));
	    	return mapping.findForward("fetchppp");	          
	    }
	    
	    
	    if(strOpt.equals("fetch")){
	    	gmDemandSheetSummaryForm.setUnitrprice("Unit");
	    	gmDemandSheetSummaryForm.setForecastMonths("4");
	    	gmDemandSheetSummaryForm.setPartNumbers("");
	    }
	    hmParam = GmCommonClass.getHashMapFromForm(gmDemandSheetSummaryForm);
	    
	    if (strOpt.equals("approvefetch"))
	    {
	        hmValues = gmForecastReportBean.fetchapproveDetail(strDemandSheetMonthId);
	        gmDemandSheetSummaryForm = (GmDemandSheetSummaryForm)GmCommonClass.getFormFromHashMap(gmDemandSheetSummaryForm,hmValues);
	        return mapping.findForward("approvefetch");
	    }
	    
	    if (strOpt.equals("partdrilldown"))
	    {
	        lResult = gmDemandSetupBean.fetchPartDrillDown(hmParam).getRows();
	        gmDemandSheetSummaryForm.setLdtResult(lResult);
	        return mapping.findForward("partdrilldown");
	    }
	    
	    if (strOpt.equals("approve"))
	    {
	    	try{
	    	blnApprove = true;
	        gmForecastReportBean.approveDemandSheet(hmParam);
	    	}
	    	catch(Exception ex)
	    	{
	    		request.setAttribute("ErrorMessage", ex.getMessage());
	    	}
	        return mapping.findForward("approve");
	    }
	    
	    if (strOpt.equals("allpartdrilldown"))
	    {
	    	hmValues = gmDemandSetupBean.loadPartQtyDetails(hmParam);
	    	gmDemandSheetSummaryForm.setHmDemandSheetDetail(hmValues);
	    	gmDemandSheetSummaryForm.setHmSubPartDetail((HashMap)hmValues.get("SUBPART"));
	    	gmDemandSheetSummaryForm.setHmParentPartDetail((HashMap)hmValues.get("PARENTPART"));
	    	gmDemandSheetSummaryForm.setHmPartDetail((HashMap)hmValues.get("PARTDETAILS"));
	        return mapping.findForward("allpartdrilldown");
	    }
	    //************* Method to fetch Demand Sheet information ********* 
	    
	    hmDemandSheet = gmForecastReportBean.fetchMonthlyDemandSheetInfo(hmParam);
	    gmDemandSheetSummaryForm = (GmDemandSheetSummaryForm)GmCommonClass.getFormFromHashMap(gmDemandSheetSummaryForm,hmDemandSheet);
	        
	
	    //  To fecth sheet information (on load condition)
	    //  To fetch the sheet information and the tab from TTP Monthly Summary Screen
	    if (strOpt.equals("fetch") || strOpt.equals("ttpfetch"))
	    {	    	
	        hmParam.put("DEMANDSHEETMONTHID",gmDemandSheetSummaryForm.getDemandSheetMonthId());
	    }
	    
	    // Reload condition 
	    if (strOpt.equals("report"))
	    {
	        String strSelectedGroup = gmDemandSheetSummaryForm.getCheckedGroupsString(); //Form.getCheckedGroupId());
	        strPartInput = gmDemandSheetSummaryForm.getPartNumbers();
	        if (!strPartInput.equals("") ){
	            strPartInput = strPartInput.replaceAll(",","|");
	        }
	
	        hmParam.put("GRPINPUTSTRING",strSelectedGroup);
	        hmParam.put("PARTINPUTSTRING",strPartInput);
	    }
	    
	    // fetch sheet detail information 
	    hmValues = gmForecastReportBean.reportDemandSheetSummary(hmParam);
	    gmDemandSheetSummaryForm.setHmDemandSheetDetail(hmValues);
	    
	    if (strOpt.equals("fetchSheet"))
	    {
	        log.debug("fetch Sheet Information **** redirect ");
	        return mapping.findForward("sheetfetch");
	    }
	    
	    
	    // To fetch group information 
	    strDemandSheetMonthId = GmCommonClass.parseNull((String)hmDemandSheet.get("DEMANDSHEETMONTHID"));
	    alGroupList = GmCommonClass.parseNullArrayList(gmForecastReportBean.fetchDemandGroupMap(strDemandSheetMonthId,strDemandSheetId));
	    gmDemandSheetSummaryForm.setAlGroupList(alGroupList);
	
	    
	    alMonth = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()); 
	    alYear =  GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());
	    
	
	    gmDemandSheetSummaryForm.setAlMonth(alMonth);
	    gmDemandSheetSummaryForm.setAlYear(alYear);
	
	    if(strOpt.equals("filterfetch"))
	    	return mapping.findForward("filterfetch"); //on fetch
	    if(strOpt.equals("ttpfetch"))
	    	return mapping.findForward("ttpfetch"); //on fetch
	   
	    if(blnApprove)
	    	return mapping.findForward("success"); //on fetch
	    
	    if (strOpt.equals("report") )
	    	return mapping.findForward("sheetfetch"); //on report and approve
	    
	    if (strOpt.equals("setsheetreport"))
	    {
	        RowSetDynaClass rdResult = null;
	    	rdResult = gmForecastReportBean.fchSetSheetReport(refID,month);
	    	List alResult = rdResult.getRows();
	    	gmDemandSheetSummaryForm.setLdtResult(alResult);
	    	return mapping.findForward("setsheetreport");	          
	    }
	    
	    
		return mapping.findForward("success"); //on fetch
	    
	    }
	    catch(Exception exp)
	    {
	        log.debug("Inside Catch");
	        exp.printStackTrace();	       
	        throw new AppError(exp);
	    }

	}

}
