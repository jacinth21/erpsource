package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmForecastTransBean;
import com.globus.operations.forecast.beans.GmTTPSetupBean;
import com.globus.operations.forecast.forms.GmTTPMonthlyForm;

public class GmTTPMonthlyAction extends GmAction
{
   
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmTTPMonthlyForm gmTTPMonthlyForm = (GmTTPMonthlyForm)form;
        gmTTPMonthlyForm.loadSessionParameters(request);
        GmForecastTransBean gmForecastTransBean = new GmForecastTransBean();
        GmTTPSetupBean gmTTPSetupBean = new GmTTPSetupBean();
        GmCommonBean gmCommonBean = new GmCommonBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
        
        HashMap hmTemp = new HashMap();
        HashMap hmParam = new HashMap();
        HashMap hmValues = new HashMap();
        
        if(gmTTPMonthlyForm.getTtpForecastMonths().equals("")){
        	log.debug (" NULL inside ");
        	gmTTPMonthlyForm.setTtpForecastMonths(gmForecastTransBean.getForecastMonthRule());
        }
        hmParam = GmCommonClass.getHashMapFromForm(gmTTPMonthlyForm);
        String strOpt = gmTTPMonthlyForm.getStrOpt();
        log.debug(" Action is " + strOpt  + " Forecast months  " + gmTTPMonthlyForm.getTtpForecastMonths());
        String strInvLockType = "20430";
        if (strOpt.equals("edit"))
        {
            hmValues = gmForecastTransBean.loadTTPLinkData(hmParam);
            log.debug(" values fetched for edit " +hmValues);
            gmTTPMonthlyForm = (GmTTPMonthlyForm)GmCommonClass.getFormFromHashMap(gmTTPMonthlyForm,hmValues);
            gmTTPMonthlyForm.setAlSelectedSheet(GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("SELECTED")));
            gmTTPMonthlyForm.setAlUnSelectedSheet(GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("UNSELECTED")));
        }
        
        log.debug(" values in gmPartGroupForm form --> " + GmCommonClass.getHashMapFromForm(gmTTPMonthlyForm));
        gmTTPMonthlyForm.setAlMonth(GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()));
        gmTTPMonthlyForm.setAlYear(GmCommonClass.parseNullArrayList(gmCommonBean.getYearList()));
        gmTTPMonthlyForm.setAlInventoryIdList(gmForecastTransBean.loadInventoryIDList(strInvLockType));
        
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        return mapping.findForward("success");
    }
}
