package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
 
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmDemandSheetSetupForm;

public class GmDemandSheetPARAction extends GmAction{

	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmDemandSheetSetupForm gmDemandSheetSetupForm = (GmDemandSheetSetupForm)form;
        gmDemandSheetSetupForm.loadSessionParameters(request);
        GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
        
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        String strOpt = gmDemandSheetSetupForm.getStrOpt();
        String strInput = gmDemandSheetSetupForm.getHinputStr();
        String strDemandMasterId = gmDemandSheetSetupForm.getDemandMasterId();
        String strUserId = gmDemandSheetSetupForm.getUserId();
        String strPartInput = GmCommonClass.removeSpaces(gmDemandSheetSetupForm.getPartNumbers());
        String strPopType = gmDemandSheetSetupForm.getPopType();
        String strDemandSheetId = gmDemandSheetSetupForm.getDemandSheetId();
        String strpartNumber = gmDemandSheetSetupForm.getHpartnumber();
        String strparvalue = gmDemandSheetSetupForm.getHparvalue();
        
        HashMap hmTemp = new HashMap();
              
        ArrayList alDemandSheetList = new ArrayList();
        ArrayList alList = new ArrayList();
        
        
        log.debug(GmCommonClass.getHashMapFromForm(gmDemandSheetSetupForm));
         
        log.debug("strInput is" + strInput);
       // request.setAttribute("PopType", strPopType);
        if (strOpt.equals("save"))
        {
           
        	 gmDemandSetupBean.saveDemandParDetail(strInput, strUserId);
        	  strOpt = "edit";
        	  if(strPopType.equals("pop")) 
          	{   
        		  gmDemandSetupBean.saveDemandSheetDetail(strDemandSheetId, strpartNumber, strparvalue );
           		  strOpt = "";
          	}
        }
        
        if (strOpt.equals("edit"))
        {
        	log.debug("strPopType is" + strPopType); 
        	log.debug("strDemandSheetId is " + strDemandSheetId); 
        	alList = gmDemandSetupBean.loadDemandParDetail(strDemandMasterId, strPartInput);
        	 
            log.debug(" values fetched for edit " + alList);
            request.setAttribute("RDDEMANDPAR",alList);
            
            log.debug("size is " + alList.size());
     
        }
       
        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        
        return mapping.findForward("success");
    }
}
