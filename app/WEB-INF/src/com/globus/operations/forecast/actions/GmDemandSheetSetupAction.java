package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmDemandSheetSetupForm;

public class GmDemandSheetSetupAction extends Action {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      GmDemandSheetSetupForm gmDemandSheetSetupForm = (GmDemandSheetSetupForm) form;
      gmDemandSheetSetupForm.loadSessionParameters(request);
      GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
      GmCommonBean gmCommonBean = new GmCommonBean();
      GmLogonBean gmLogonBean = new GmLogonBean();

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.


      String strOpt = gmDemandSheetSetupForm.getStrOpt();
      String strDemandSheetId = gmDemandSheetSetupForm.getDemandSheetId();


      HashMap hmParam = new HashMap();
      HashMap hmValues = new HashMap();
      ArrayList alDemandSheetList = new ArrayList();
      ArrayList alLogReasons = new ArrayList();
      ArrayList alDSOwner = new ArrayList();

      log.debug(GmCommonClass.getHashMapFromForm(gmDemandSheetSetupForm));
      log.debug(" Testing. ..... ");
      if (strOpt.equals("save")) {
        hmParam = GmCommonClass.getHashMapFromForm(gmDemandSheetSetupForm);
        log.debug(" values inside hmParam " + hmParam);
        strDemandSheetId = gmDemandSetupBean.saveDemandSheet(hmParam);

        gmDemandSheetSetupForm.setDemandSheetId(strDemandSheetId);
        // JMS call to update the values to cache server
        GmAutoCompleteTransBean gmAutoCompleteTransBean =
            new GmAutoCompleteTransBean(GmCommonClass.getDefaultGmDataStoreVO());
        HashMap hmCacheUpd = new HashMap();
        // to get the values
        String strNewDemandSheetName =
            GmCommonClass.parseNull((String) hmParam.get("DEMANDSHEETNAME"));
        String strOldDemandSheetName =
            GmCommonClass.parseNull((String) hmParam.get("HDEMANDSHEETNAME"));
        // set the values
        hmCacheUpd.put("ID", strDemandSheetId);
        hmCacheUpd.put("OLD_NM", strOldDemandSheetName);
        hmCacheUpd.put("NEW_NM", strNewDemandSheetName);
        hmCacheUpd.put("METHOD", "DemandSheet");
        hmCacheUpd.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
        gmAutoCompleteTransBean.saveDetailsToCache(hmCacheUpd);

      } else if (strOpt.equals("edit")) {
        hmValues = gmDemandSetupBean.fetchDemandSheetInfo(strDemandSheetId);
        log.debug(" values fetched for edit " + hmValues);
        gmDemandSheetSetupForm =
            (GmDemandSheetSetupForm) GmCommonClass.getFormFromHashMap(gmDemandSheetSetupForm,
                hmValues);
      }



      gmDemandSheetSetupForm.setAlHierarchyList(gmDemandSetupBean.loadHierarchyList());
      // displaying comments
      alLogReasons = gmCommonBean.getLog(strDemandSheetId, "1227");
      gmDemandSheetSetupForm.setAlLogReasons(alLogReasons);

      alDSOwner = gmLogonBean.getEmployeeList("300", "W','P");
      gmDemandSheetSetupForm.setAlDSOwner(alDSOwner);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
