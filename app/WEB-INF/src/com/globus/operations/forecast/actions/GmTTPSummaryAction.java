package com.globus.operations.forecast.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.operations.forecast.beans.GmDemandReportBean;
import com.globus.operations.forecast.beans.GmForecastTransBean;
import com.globus.operations.forecast.beans.GmTTPSetupBean;
import com.globus.operations.forecast.forms.GmTTPSummaryForm;

public class GmTTPSummaryAction extends GmAction
{

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
		GmTTPSummaryForm gmTTPSummaryForm = (GmTTPSummaryForm)form;
		gmTTPSummaryForm.loadSessionParameters(request);
		GmTTPSetupBean gmTTPSetupBean = new GmTTPSetupBean();
		GmDemandReportBean gmDemandReportBean = new GmDemandReportBean();
		GmForecastTransBean gmForecastTransBean = new GmForecastTransBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
		GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
		   
		String strOpt = gmTTPSummaryForm.getStrOpt();
		HashMap hmTemp = new HashMap();
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		
	    hmParam = GmCommonClass.getHashMapFromForm(gmTTPSummaryForm);
	    
	    if (strOpt.equals("lock"))
	    {
	    	gmTTPSetupBean.lockTTPSummary(hmParam);
	        strOpt = "report";
	    }
	    String strUnSelectedDS  = GmCommonClass.createInputString(gmTTPSummaryForm.getCheckSelectedSheet());
	    String strSelectedDS = GmCommonClass.createInputString(gmTTPSummaryForm.getCheckUnSelectedSheet());
	    String strComma = "";
	    if (strUnSelectedDS.length() > 0 && strSelectedDS.length() > 0 ){
	        strComma = ",";
	    }        
	    String strDSInputString = strUnSelectedDS.concat(strComma).concat(strSelectedDS);
	    if(!strDSInputString.equals(""))
	    {
	    	gmTTPSummaryForm.setDemandSheetIds(strDSInputString);        
	    	hmParam.put("DEMANDSHEETIDS",strDSInputString);
	    }
	    else
	    {
	    	hmParam.put("DEMANDSHEETIDS",gmTTPSummaryForm.getDemandSheetIds());
	    }
	    //...end
	    if(strOpt.equals("save"))
	    {
	    	hmParam.put("ORDSTR", gmTTPSummaryForm.getStrPnumOrd());
	    	gmForecastTransBean.saveTTPLinkData(hmParam);
	    	strOpt = "report";
	    }
	    if (strOpt.equals("report") || strOpt.equals("reportSummary") || strOpt.equals("reportDetails"))
	    {
	        if(strOpt.equals("report"))
	        {
	        	hmValues = gmDemandReportBean.fetchTTPSummarySheets(hmParam);
	        }
	        else
	        {        
		        hmValues = gmDemandReportBean.fetchTTPSummaryReport(hmParam);
		        gmTTPSummaryForm.setInventoryId((String)hmValues.get("INVENTORYID"));
		        gmTTPSummaryForm.setTtpType((String)hmValues.get("TTPTYPE"));
	        }
	        if(strOpt.equals("reportDetails")){
			        ArrayList alDetails = (ArrayList)hmValues.get("Details");
			        
			        if(!gmTTPSummaryForm.getOrderQty().equals("") && !gmTTPSummaryForm.getQtyOper().equals("0"))
			        {	                	
				        gmSearchCriteria.addSearchCriteria("Order Qty", gmTTPSummaryForm.getOrderQty(), gmTTPSummaryForm.getQtyOper());	        	      	        
			        }
			        if(!gmTTPSummaryForm.getOrderCost().equals("") && !gmTTPSummaryForm.getOrdOper().equals("0"))
			        {	        
				        gmSearchCriteria.addSearchCriteria("Order Cost$", gmTTPSummaryForm.getOrderCost(), gmTTPSummaryForm.getOrdOper());	        	      	        
			        }                
			        if (!gmTTPSummaryForm.getPnum().equals(""))
			        {
			        	gmSearchCriteria.addSearchCriteria("Name", gmTTPSummaryForm.getPnum(), GmSearchCriteria.LIKE);        	
			        }
			        if (!gmTTPSummaryForm.getOrdType().equals(""))
			        {
			        	gmSearchCriteria.addSearchCriteria("ptype", gmTTPSummaryForm.getOrdType(), GmSearchCriteria.EQUALS);        	
			        }   
			        if(gmTTPSummaryForm.getSummaryTotal().equals("true"))
			        {
			        	gmSearchCriteria.addSearchCriteria("ID", "4050,4051,4052,4053,Total", GmSearchCriteria.IN);
			        }
			        gmSearchCriteria.query(alDetails);  
	        }
	        request.setAttribute("HMVALUES",hmValues);	        
	    }     
	    if(strOpt.equals("reportFCRequest"))
	    {    	
	    	gmTTPSummaryForm.setAlForeCastReport(gmDemandReportBean.fetchTTPForecastRequest(hmParam));
	    	//log.debug("Forecast Request: "+gmTTPSummaryForm.getAlForeCastReport());
	    }
	    //log.debug(" hmParam values is " + hmParam);    
	    gmTTPSummaryForm.setAlOperators(GmCommonClass.getCodeList("OPERS"));
	    gmTTPSummaryForm.setAlOrderTypes(GmCommonClass.getCodeList("PFLY"));        
	    gmTTPSummaryForm.setAlMonth(GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()));
	    gmTTPSummaryForm.setAlYear(GmCommonClass.parseNullArrayList(gmCommonBean.getYearList()));    
	    //log.debug(" Deman sheet ids --- " + gmTTPSummaryForm.getDemandSheetIds());
	    
    
	   if(strOpt.equals("fetchFilter") || strOpt.equals("reportFCRequest") || strOpt.equals("reportSummary") || strOpt.equals("reportDetails") )
	    {
	    	return mapping.findForward(strOpt);
	    }
	    
	    return mapping.findForward("success");
}


}
