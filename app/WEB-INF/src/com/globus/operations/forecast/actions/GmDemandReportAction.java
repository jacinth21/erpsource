package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.forecast.beans.GmDemandReportBean;
import com.globus.operations.forecast.forms.GmTTPMonthlyForm;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 



public class GmDemandReportAction extends GmDispatchAction
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    public ActionForward demandSheetReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response) 
    {   
        RowSetDynaClass rdDemandSheetReport = null;
        GmDemandReportBean gmDemandReportBean = new GmDemandReportBean();
        DynaActionForm gmDemandSheetReportForm = (DynaActionForm) form;  
        log.debug(" inside demandSheetReport ");
        try
        {
        ArrayList alDemandSheetType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DMDTP"));
        String strDemandSheetType  = GmCommonClass.getStringWithQuotes(GmCommonClass.createInputString((String[])gmDemandSheetReportForm.get("chkDemandSheetType")));
        rdDemandSheetReport = gmDemandReportBean.demandSheetReport(strDemandSheetType);
        request.setAttribute("RDDSREPORT",rdDemandSheetReport);
        gmDemandSheetReportForm.set("alDemandSheetType",alDemandSheetType);
        }
        catch(Exception ex)
        {
        	ex.printStackTrace();
        	//throw new AppError(ex);
        }
        return mapping.findForward("success");
    }
   
    public ActionForward loadTTPReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                    HttpServletResponse response)
    {   
        RowSetDynaClass rdTTPReport = null;
        GmDemandReportBean gmDemandReportBean = new GmDemandReportBean();
        
        log.debug(" inside demandTTPReport ");
        try
        {
            rdTTPReport = gmDemandReportBean.fetchTTPReport();
            log.debug(" size of result " + rdTTPReport.getRows().size());
            request.setAttribute("results",rdTTPReport);
            
        } catch (Exception e)
        {
            e.printStackTrace();
            log.debug("Exception " + e);
         //   throw new AppError(e);
        }

        return mapping.findForward("success");
    }
    
    public ActionForward fetchTPRReport(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) 
    {    	 
         
         try
         {
        	 GmDemandReportBean gmDemandReportBean = new GmDemandReportBean();
             GmTTPMonthlyForm gmTTPMonthlyForm = (GmTTPMonthlyForm) form;         
             gmTTPMonthlyForm.loadSessionParameters(request);
             
             log.debug(" gmTTPMonthlyForm.getPnum()" + gmTTPMonthlyForm.getPnum()); 
             HashMap hmParam = GmCommonClass.getHashMapFromForm(gmTTPMonthlyForm);             
             
             
        	 HashMap hmReturn =  gmDemandReportBean.fetchTPRReport(hmParam);        	 
             gmTTPMonthlyForm.setAlTPRReport((ArrayList)hmReturn.get("TPRREPORT"));
             gmTTPMonthlyForm.setAlTPRVoidReport((ArrayList)hmReturn.get("TPRVOIDREPORT"));
             gmTTPMonthlyForm.setPdesc((String)hmReturn.get("PDESC"));
             gmTTPMonthlyForm.setAlTPRSalesReport((ArrayList)hmReturn.get("TPRSALESREPORT"));
             gmTTPMonthlyForm.setAlTPRBoLoanerReport((ArrayList)hmReturn.get("TPRLOANERBOREPORT"));
             
         } catch (Exception e)
         {
             e.printStackTrace();
             log.debug("Exception " + e);
          //   throw new AppError(e);
         }

         return mapping.findForward("tprreport");
    }    
}
