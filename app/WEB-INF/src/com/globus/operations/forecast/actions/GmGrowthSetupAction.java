package com.globus.operations.forecast.actions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.beans.GmGrowthSetupBean;
import com.globus.operations.forecast.forms.GmGrowthSetupForm;
import com.globus.common.beans.GmResourceBundleBean;

public class GmGrowthSetupAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		// to
		// Initialize
		// the		
		// Logger
		// Class.
		log.debug("Enter");
		GmGrowthSetupForm gmGrowthSetupForm = (GmGrowthSetupForm) form;
		gmGrowthSetupForm.loadSessionParameters(request);

		GmGrowthSetupBean gmGrowthSetupBean = new GmGrowthSetupBean();
		GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
		GmCommonBean gmCommonBean = new GmCommonBean();

		HashMap hmParam = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmDemandSheet = new HashMap();

		String strTodaysDate = (String) request.getSession().getAttribute("strSessTodaysDate");
		String strApplDateFmt = (String) request.getSession().getAttribute("strSessApplDateFmt");
		log.debug("strTodaysDate is " + strTodaysDate);
		
		String strDeptId = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessDeptId"));
		String strAccessFilter = "";

		//The filter condition should be applied for ICS Dept users alone.
		if (strDeptId.equals("ICS"))
			strAccessFilter =  this.getAccessFilter(request, response);
		
		hmDemandSheet.put("ACCESS_FILTER", strAccessFilter);

		String strOpt = gmGrowthSetupForm.getStrOpt();
		String strDemandSheetId = gmGrowthSetupForm.getDemandSheetId();
		String strReferenceType = gmGrowthSetupForm.getRefType();
		String strMsg = "";
		hmParam = GmCommonClass.getHashMapFromForm(gmGrowthSetupForm);
		String strPnum = (String) hmParam.get("PNUM");
		HashMap hmGrowthDetails = new HashMap();
		hmParam.put("TODAYSDATE", strTodaysDate);
		hmParam.put("APPLDATEFMT", strApplDateFmt);
		String strGrowthID = "";
		if (strOpt.equals("save")) {
			hmReturn = gmGrowthSetupBean.saveGrowthDetails(hmParam);
			strGrowthID = (String) hmReturn.get("GROWTHID");
			gmGrowthSetupForm.setGrowthId(strGrowthID);
		}

		if (strOpt.equals("reload") || strOpt.equals("save")) {
			String strSessCompanyLocale = GmCommonClass.parseNull((String)request.getSession().getAttribute("strSessCompanyLocale"));
			GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.prodmgmnt.GmGrowthSetup", strSessCompanyLocale);

			ArrayList alGrowthRef = gmGrowthSetupBean.fetchGrowthReference(strDemandSheetId, strReferenceType);
			boolean hasValue = false;

			strMsg = (strMsg.equals("") && alGrowthRef.size() == 0) ? GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REF_VALUE")) : strMsg;
			gmGrowthSetupForm.setMsg(strMsg);
			gmGrowthSetupForm.setAlRefIds(alGrowthRef);

			gmGrowthSetupForm.reset();

			if (!strPnum.equals("")) {
				Iterator itrRefIds = gmGrowthSetupForm.getAlRefIds().iterator();

				while (itrRefIds.hasNext()) {
					HashMap hmGrowth = (HashMap) itrRefIds.next();
					if (GmCommonClass.parseNull((String) hmGrowth.get("REFID")).equals(strPnum)) {
						gmGrowthSetupForm.setReferences(strPnum);
						hasValue = true;
						break;
					}
				}

				if (!hasValue) {
					gmGrowthSetupForm.setMsg(GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REF_VALUE")));
				}
			}

			log.debug("HmParam in Gm Growth Setup Action :" + hmParam);

			fetchDates(gmGrowthSetupForm, hmParam);
						
			hmGrowthDetails =  gmGrowthSetupBean.fetchGrowthDetails(hmParam);
			gmGrowthSetupForm.setGrowthDetailsMap(hmGrowthDetails);
		}

	
		// This is  to set the From,toDate during onLoad Condition.
		 Calendar cal = Calendar.getInstance();
		 SimpleDateFormat dateFormat = new SimpleDateFormat("MMM'/'yy");
		 SimpleDateFormat formatter = new SimpleDateFormat(strApplDateFmt);
		 String hFromDate = "";
		 String hToDate  = "";
		 
		String strFromDate = dateFormat.format(cal.getTime());
		
		if(gmGrowthSetupForm.getFromDate()==""){
			gmGrowthSetupForm.setFromDate(strFromDate);			
		}
		hFromDate = formatter.format(dateFormat.parse(gmGrowthSetupForm.getFromDate()));
	
		cal.add(Calendar.MONTH,11);
		String strToDate = dateFormat.format(cal.getTime());
		
		if(gmGrowthSetupForm.getToDate()==""){
			gmGrowthSetupForm.setToDate(strToDate);			
		}
		hToDate = formatter.format(dateFormat.parse(gmGrowthSetupForm.getToDate()));
		log.debug(" hFromDate is : "+hFromDate+"\n"+" hToDate :"+hToDate);
		// Date Format to be used in the calendar .
		request.setAttribute("hidden_FromDate",hFromDate);
		request.setAttribute("hidden_ToDate",hToDate);
		
		if (strDemandSheetId !=null && !strDemandSheetId.equals("")&&!strDemandSheetId.equals("0")) {
			gmGrowthSetupForm.setAlRefTypes(GmCommonClass.getCodeList("GRFTP", gmGrowthSetupBean.fetchDemandType(strDemandSheetId)));
		}
		
		
		
		
		
		log.debug("Exit");

		return mapping.findForward("success");
	}
	private void fetchDates(GmGrowthSetupForm gmGrowthSetupForm, HashMap hmParam) throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		log.debug("called fetchDates function.............................................");
		HashMap hmRuleData = new HashMap();
		HashMap hmSheetDetails = new HashMap();
		GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Date launchDate = new Date();
		String strApplDateFmt = (String) hmParam.get("APPLDATEFMT");
		int intLaunchMonth = 0;
		int intLaunchYear = 0;
		DateFormat dateFormat = new SimpleDateFormat(strApplDateFmt);
		String strTodaysDate = (String) hmParam.get("TODAYSDATE");
		log.debug("strTodaysDate is " + strTodaysDate);
		Date todaysDate = dateFormat.parse(strTodaysDate);

		// fetch project cutoff date to determine if project is old or new
		hmRuleData.put("RULEID", "CUTOFFPRJDT");
		hmRuleData.put("RULEGROUP", "PROJECT");
		String strPrjCutoffDt = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
		Date dtPrjCutoffDt = dateFormat.parse(strPrjCutoffDt);
		log.debug("dtPrjCutoffDt " + dtPrjCutoffDt);

		// fetch demandsheetid created date
		hmSheetDetails = gmDemandSetupBean.fetchDemandSheetInfo(gmGrowthSetupForm.getDemandSheetId());
		String strSheetCrDt = GmCommonClass.parseNull((String) hmSheetDetails.get("CREATEDDT"));
		Date dtSheetCrDt = dateFormat.parse(strSheetCrDt);
		log.debug("dtSheetCrDt " + dtSheetCrDt);

		if (dtSheetCrDt.compareTo(dtPrjCutoffDt) > 0) {
			gmGrowthSetupForm.setPrjStatus("NEW");
		} else
			gmGrowthSetupForm.setPrjStatus("OLD");

		// fetch LaunchDate
		launchDate = gmDemandSetupBean.fetchLaunchDate(hmParam);
		if (launchDate == null) {
			gmGrowthSetupForm.setLaunchDt("");
			launchDate = new Date();
			hmParam.put("LAUNCHDATE", "");
		} else {
			hmParam.put("LAUNCHDATE", dateFormat.format(launchDate));
			gmGrowthSetupForm.setLaunchDt(dateFormat.format(launchDate).toString());
		}

		Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(launchDate);

		intLaunchMonth = cal.get(cal.MONTH);
		intLaunchMonth += 1;
		intLaunchYear = cal.get(cal.YEAR);

		if (launchDate.compareTo(todaysDate) > 0) {
			gmGrowthSetupForm.setValidateMonth(intLaunchMonth);
			gmGrowthSetupForm.setValidateYear(intLaunchYear);
		} else {
			gmGrowthSetupForm.setValidateMonth(GmCalenderOperations.getCurrentMonth());
			gmGrowthSetupForm.setValidateYear(GmCalenderOperations.getCurrentYear());
		}
	}

	private void populateForm(GmGrowthSetupForm gmGrowthSetupForm, ArrayList alGrowthDetails) {
		Iterator iteratorGrowth = alGrowthDetails.iterator();
		while (iteratorGrowth.hasNext()) {
			HashMap hmTemp = (HashMap) iteratorGrowth.next();
			int iMonth = Integer.parseInt((String) hmTemp.get("REFMONTH"));

			switch (iMonth) {
			case 1:
				gmGrowthSetupForm.setMonth1((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth1((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setGrowthId((String) hmTemp.get("GROWTHID"));
				gmGrowthSetupForm.setControlValue1((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 2:
				gmGrowthSetupForm.setMonth2((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth2((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue2((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 3:
				gmGrowthSetupForm.setMonth3((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth3((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue3((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 4:
				gmGrowthSetupForm.setMonth4((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth4((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue4((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 5:
				gmGrowthSetupForm.setMonth5((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth5((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue5((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 6:
				gmGrowthSetupForm.setMonth6((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth6((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue6((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 7:
				gmGrowthSetupForm.setMonth7((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth7((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue7((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 8:
				gmGrowthSetupForm.setMonth8((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth8((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue8((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 9:
				gmGrowthSetupForm.setMonth9((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth9((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue9((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 10:
				gmGrowthSetupForm.setMonth10((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth10((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue10((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 11:
				gmGrowthSetupForm.setMonth11((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth11((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue11((String) hmTemp.get("CONTROLVALUE"));
				break;
			case 12:
				gmGrowthSetupForm.setMonth12((String) hmTemp.get("REFVALUE"));
				gmGrowthSetupForm.setRefMonth12((String) hmTemp.get("REFMONTHTYPE"));
				gmGrowthSetupForm.setControlValue12((String) hmTemp.get("CONTROLVALUE"));
				break;
			}

		}
	}
}
