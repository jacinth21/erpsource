package com.globus.operations.forecast.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.globus.common.beans.GmAutoCompleteTransBean;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmGroupPartMapForm;
import com.globus.prodmgmnt.beans.GmGroupServiceBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmGroupPartMapAction extends GmAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmGroupPartMapForm gmGroupPartMapForm = (GmGroupPartMapForm) form;
    gmGroupPartMapForm.loadSessionParameters(request);
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmGroupServiceBean gmGroupServiceBean = new GmGroupServiceBean(getGmDataStoreVO());
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    // to
    // Initialize
    // the
    // Logger
    // Class.
    String strDisableBtn = GmCommonClass.parseNull(gmGroupPartMapForm.getStrDisableBtn());// To hide
                                                                                          // the
                                                                                    // close
                                                                                          // button
                                                                                          // Group
                                                                                          // Part
                                                                                          // Details
                                                                                    // section
    String strForward = GmCommonClass.parseNull(gmGroupPartMapForm.getStrForward());
    String strAction = GmCommonClass.parseNull(gmGroupPartMapForm.getHaction());
    String strGroupId = GmCommonClass.parseNull(gmGroupPartMapForm.getGroupId());
    request.setAttribute("GROUPID", strGroupId);
    String strGroupName = GmCommonClass.parseNull(gmGroupPartMapForm.getGroupName());
    String strPartInput = GmCommonClass.parseNull(gmGroupPartMapForm.getPartNumbers());
    String strPartInputString = "";
    String strPartListFlag = GmCommonClass.parseNull(gmGroupPartMapForm.getStrPartListFlag());
    String strPricingByGroupFl =
        GmCommonClass.parseNull(gmGroupPartMapForm.getStrPricingByGroupFl());
    // PMT-758 - When click P icon in Group Part Pricing screen, should be display List & TW price.
    String strHPartPriceVal = GmCommonClass.parseNull(gmGroupPartMapForm.getStrHPartPriceFl());
    String strOption = "";
    String strPartyId = GmCommonClass.parseNull(gmGroupPartMapForm.getSessPartyId());
    String accessFlag = "";
    String strPublishFL = "";
    String strName = "";
    String strOldGrpNm = GmCommonClass.parseNull(gmGroupPartMapForm.gethOld_GrpId());
    //String strOldGrpNm = GmCommonClass.parseNull(request.getParameter("hOld_GrpId"));
    String strNewNm = ""; 
   
    strOption = strForward.equals("") ? "success" : strForward;// if strForward is null, assigning
                                                                               // success as strOption
    HashMap hmAccess = new HashMap();
    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmGroupPartMapForm);
    request.setAttribute("HACTION", strAction);
    /*
     * The below code is added for PMT-4918 to validate the group name.
     */
    if (strAction.equals("CHECKGROUPNAME")) {
      strName = gmGroupServiceBean.checkGroupExist(strGroupName);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (!strName.equals(""))
        pw.write(strName);
      pw.flush();
      pw.close();
      return null;
    }
    if (strAction.equals("save")) {
      // strPartInputString =
      // GmCommonClass.createInputString(gmGroupPartMapForm.getCheckPartNumbers());
      // hmParam.put("INPUTSTRING",strPartInputString);
      strPartInputString = GmCommonClass.parseNull(gmGroupPartMapForm.getInputString());
      strGroupId = gmDemandSetupBean.saveGroupPartMapping(hmParam);
      gmGroupPartMapForm.setGroupId(strGroupId);
      gmGroupPartMapForm.setPartNumbers("");
      synGroupList(strGroupId, strOldGrpNm,strGroupName,"synGroupList",GmCommonClass.parseNull(request.getParameter("companyInfo")));
    }
    if (strAction.equals("save") || strAction.equals("edit")) {
      strGroupId = gmGroupPartMapForm.getGroupId();
      HashMap hmValues = gmDemandSetupBean.fetchGroupPartInfo(strGroupId);
       request.setAttribute("HMVALUES", hmValues);
      gmGroupPartMapForm =
          (GmGroupPartMapForm) GmCommonClass.getFormFromHashMap(gmGroupPartMapForm, hmValues);
      strPublishFL = gmGroupPartMapForm.getPublishfl();
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1161");
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

      if (!accessFlag.equals("Y") && strPublishFL.equals("Y")) {
        gmGroupPartMapForm.setStrEnableGrpType("disabled");
        // MNTTASK-6106 - After Publish group, this party only should change below drop downs.
        gmGroupPartMapForm.setStrDisablePricedType("disabled");
        gmGroupPartMapForm.setStrDisableGroupName("disabled");
      }
      synGroupList(strGroupId, strOldGrpNm,strGroupName,"synGroupList",GmCommonClass.parseNull(request.getParameter("companyInfo")));
    }

    hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1160");
    accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    if (!accessFlag.equals("Y")) {
      gmGroupPartMapForm.setEnableGrpPartMapChange("disabled");
    }
   // ArrayList alGroupList = gmDemandSetupBean.loadDemandGroupList(hmParam);
   // gmGroupPartMapForm.setAlGroupList(alGroupList);
   // ArrayList alSalesGrp = new ArrayList();
   // alSalesGrp = gmProjectBean.loadSetMap("GROUPPARTMAPPING");
   //String strType = "GROUPPARTMAPPING";
    // hmTemp.put("SALESGROUP",alSalesGrp);
    
    //gmGroupPartMapForm.setAlSetID(alSalesGrp);
    ArrayList alGroupType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("GRPTYP"));
    gmGroupPartMapForm.setAlGroupType(alGroupType);
    ArrayList alPricedType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("GPMTYP"));
    gmGroupPartMapForm.setAlPricedType(alPricedType);
    if (!strGroupId.equals("0") && !strAction.equals("loadParts")) {
      String hactiveflag = gmGroupPartMapForm.getHactiveflag();
      if (hactiveflag.equals("true"))
        gmGroupPartMapForm.setActiveflag("");
      /*
       * String strInActiveFlag =
       * GmCommonClass.getCheckBoxValue(gmGroupPartMapForm.getActiveflag());
       * 
       * log.debug(" strInActiveFlag is >>>>>>>>>>>>>>>>>> " + strInActiveFlag);
       * if(strInActiveFlag.equals("")) { gmGroupPartMapForm.setActiveflag("on"); }
       */
      gmGroupPartMapForm.setGroupId(strGroupId);
      hmParam = GmCommonClass.getHashMapFromForm(gmGroupPartMapForm);
    }
    String strSubComponentFlag =
        GmCommonClass.getCheckBoxValue(gmGroupPartMapForm.getEnableSubComponent());
    strGroupId = gmGroupPartMapForm.getGroupId();
    if (!strGroupId.equals("0") || strAction.equals("loadParts")) {

      if (!strPartInput.equals("")) {

        strPartInput = strPartInput.replaceAll(",", "|^");
        strPartInput = "^".concat(strPartInput);
      }    
      hmParam.put("STRACTION", strAction);
      hmParam.put("STRGROUPID", strGroupId);
      hmParam.put("STRPARTINPUT", strPartInput);
      hmParam.put("STRSUBCOMPONENTFLAG", strSubComponentFlag);
      hmParam.put("STRPRICINGBYGROUPFL", strPricingByGroupFl);
      hmParam.put("STRPARTPRICEVAL", strHPartPriceVal);
      ArrayList alList = gmDemandSetupBean.loadPartGroupMapping(hmParam);
      request.setAttribute("RDGROUPPARTMAP", alList);
      
    }
  
    if (strPartListFlag.equals("Y"))
      strOption = "loadPartList";
    gmGroupPartMapForm.setStrDisableBtn(strDisableBtn);
  
    return mapping.findForward(strOption);
  }
  // To sync the modified/ new value with the redis cache
  public void synGroupList(String strGroupId, String strOldGrpNm, String strNewNm,String strMethod ,String strCompanyInfo) throws Exception {
	 
	    GmAutoCompleteTransBean gmAutoCompleteTransBean =
	        new GmAutoCompleteTransBean(getGmDataStoreVO());
	    HashMap hmJmsParam = new HashMap();
	    hmJmsParam.put("ID", strGroupId);
	    hmJmsParam.put("OLD_NM", strOldGrpNm);
	    hmJmsParam.put("NEW_NM", strNewNm);
	    hmJmsParam.put("METHOD", strMethod);
	    hmJmsParam.put("companyInfo", strCompanyInfo);
	    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
	  }
  
}
