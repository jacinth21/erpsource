package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmDemandMapForm;

public class GmDemandMapAction extends GmAction
{

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
        GmDemandMapForm gmDemandMapForm = (GmDemandMapForm)form;
        gmDemandMapForm.loadSessionParameters(request);
        GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        String strOpt = gmDemandMapForm.getStrOpt();
        String strDemandSheetId = gmDemandMapForm.getDemandSheetId();
        String strDemandSheetTypeId = gmDemandMapForm.getDemandSheetTypeId();
        
        if (strDemandSheetTypeId.equals("40020")) // 40020 - Sales 
        {
        	 gmDemandMapForm.setRequestPeriod("0");
            
        }
        String strRequestPeriod = gmDemandMapForm.getRequestPeriod();
        log.debug("first here is test requestperiod is " + strRequestPeriod  );       
        HashMap hmParam = new HashMap();
        HashMap hmValues = new HashMap();
        HashMap hmGSValues = new HashMap();
        HashMap hmRIValues = new HashMap();
        ArrayList alDemandSheetType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DMDTP"));
        ArrayList alUnSelectedGS = new ArrayList(); 
        ArrayList alSelectedGS = new ArrayList();
        ArrayList alUnSelectedRI = new ArrayList();
        ArrayList alSelectedRI = new ArrayList();
        
        String strGSType = "";
        String strRIType = "";
        String GROUP_DEMAND_MAP_TYPE = "40030";
        String SET_DEMAND_MAP_TYPE = "40031";
        String REGION_DEMAND_MAP_TYPE = "40032";
        String INHOUSE_DEMAND_MAP_TYPE = "40033";
        
        if (strDemandSheetTypeId.equals("40020")) // 40020 - Sales 
        {
            strGSType = GROUP_DEMAND_MAP_TYPE;
            strRIType = REGION_DEMAND_MAP_TYPE;
        }
        if (strDemandSheetTypeId.equals("40021")) // 40021 - Consignment 
        {
            strGSType = SET_DEMAND_MAP_TYPE;
            strRIType = REGION_DEMAND_MAP_TYPE;
        }
        if (strDemandSheetTypeId.equals("40022")) // 40022 - InHouse 
        {
            strGSType = SET_DEMAND_MAP_TYPE;
            strRIType = INHOUSE_DEMAND_MAP_TYPE;
        }

        log.debug(" strOpt is " + strOpt + "Demand Sheet type " +strDemandSheetTypeId );

        if (strOpt.equals("save"))
        {
            hmParam = GmCommonClass.getHashMapFromForm(gmDemandMapForm);
            String strUnSelectedGS  = GmCommonClass.createInputString(gmDemandMapForm.getCheckUnSelectedGS());
            String strSelectedGS = GmCommonClass.createInputString(gmDemandMapForm.getCheckSelectedGS());
            String strComma = "";
            
            log.debug(" The values of strUnSelectedGroup "+strUnSelectedGS);
            
            if (strUnSelectedGS.length() > 0 && strSelectedGS.length() > 0 ){
                strComma = ",";
            }
            
            String strGroupInputString = strUnSelectedGS.concat(strComma).concat(strSelectedGS);
            hmParam.put("INPUTSTRING",strGroupInputString);
            hmParam.put("MAPTYPE",strGSType);
            
            log.debug(" Group Input String is  " + strGroupInputString);
            gmDemandSetupBean.saveDemandSheetMapping (hmParam);

            String strUnSelectedRI  = GmCommonClass.createInputString(gmDemandMapForm.getCheckUnSelectedRI());
            String strSelectedRI = GmCommonClass.createInputString(gmDemandMapForm.getCheckSelectedRI());
            strComma = "";
            
            log.debug(" The values of strUnSelectedGroup "+strUnSelectedRI);
            if (strUnSelectedRI.length() > 0 && strSelectedRI.length() > 0 ){
                strComma = ",";
            }
            
            String strRIInputString = strUnSelectedRI.concat(strComma).concat(strSelectedRI);
            hmParam.put("INPUTSTRING",strRIInputString);
            hmParam.put("MAPTYPE",strRIType);
            
            log.debug(" strRIInputString is  " + strGroupInputString);
            gmDemandSetupBean.saveDemandSheetMapping (hmParam);

        }
        
        log.debug(" values in gmDemandMapForm form --> " + GmCommonClass.getHashMapFromForm(gmDemandMapForm));
        if (strDemandSheetTypeId.equals("40020")) // 40020 - Sales 
        {
        	//gmDemandMapForm.setRequestPeriod("0");
            hmGSValues = gmDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,GROUP_DEMAND_MAP_TYPE);
            hmRIValues = gmDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,REGION_DEMAND_MAP_TYPE);
        }
        else if (strDemandSheetTypeId.equals("40021")) // 40021 - Consignment 
        {
            hmGSValues = gmDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,SET_DEMAND_MAP_TYPE);
            hmRIValues = gmDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,REGION_DEMAND_MAP_TYPE);
        }
        else if (strDemandSheetTypeId.equals("40022")) // 40022 - InHouse 
        {
            hmGSValues = gmDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,SET_DEMAND_MAP_TYPE);
            hmRIValues = gmDemandSetupBean.fetchDemandGroupMap(strDemandSheetId,strDemandSheetTypeId,INHOUSE_DEMAND_MAP_TYPE);
        }
        
        alUnSelectedGS = GmCommonClass.parseNullArrayList((ArrayList)hmGSValues.get("ALUNSELECTED"));
        alSelectedGS = GmCommonClass.parseNullArrayList((ArrayList)hmGSValues.get("ALSELECTED")); 
        
        alUnSelectedRI = GmCommonClass.parseNullArrayList((ArrayList)hmRIValues.get("ALUNSELECTED"));
        alSelectedRI = GmCommonClass.parseNullArrayList((ArrayList)hmRIValues.get("ALSELECTED")); 
        
        log.debug(" value in hmRIValues " + hmRIValues);
        strRequestPeriod = GmCommonClass.parseNull((String)hmGSValues.get("REQUESTPERIOD"));
        //log.debug(" Count of values in unselected " + alUnSelectedRI.size());
        
        
        gmDemandMapForm.setAlUnSelectedGS(alUnSelectedGS);
        gmDemandMapForm.setAlSelectedGS(alSelectedGS); 
        gmDemandMapForm.setAlUnSelectedRI(alUnSelectedRI);
        gmDemandMapForm.setAlSelectedRI(alSelectedRI); 
        
        gmDemandMapForm.setAlDemandSheetType(alDemandSheetType);
        
        gmDemandMapForm.setRequestPeriod(strRequestPeriod);
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            throw new AppError(exp);
        }
        return mapping.findForward("success");
    }



}
