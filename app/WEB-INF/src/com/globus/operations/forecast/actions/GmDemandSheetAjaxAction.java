package com.globus.operations.forecast.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmForecastReportBean;
import com.globus.operations.forecast.forms.GmDemandSheetAjaxForm;

/*
 * The name has Ajax because we have to move all the ajax actions from demand sheet summary action
 */

public class GmDemandSheetAjaxAction extends GmAction{
	
	   public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
		   GmDemandSheetAjaxForm gmDemandSheetAjaxForm = (GmDemandSheetAjaxForm)form;
		   gmDemandSheetAjaxForm.loadSessionParameters(request);
		   GmForecastReportBean gmForecastReportBean = new GmForecastReportBean();
           Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	       String strOpt = gmDemandSheetAjaxForm.getStrOpt();
	       HashMap hmParam =  GmCommonClass.getHashMapFromForm(gmDemandSheetAjaxForm);
	       
	       if (strOpt.equals("pullopensheets"))
	        {	    	   
	        	gmForecastReportBean.pullDemandSheets(hmParam);
	        }
	       log.debug("Test Test");
	        if(strOpt.equals("opensheets") || strOpt.equals("pullopensheets"))
	        {	        	
	        	gmDemandSheetAjaxForm.setAlResult(gmForecastReportBean.fchOpenSheets(hmParam));	        	
	        }	        
	        else if(strOpt.equals("multisheetdetails"))
	        {
	        	gmDemandSheetAjaxForm.setHmReport(gmForecastReportBean.fchMultiPartDetails(hmParam));
	        }
	        request.setAttribute("FORMNAME", "frmDemandSheetAjax");
	        return mapping.findForward(strOpt);
	        }
}
