package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;

import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmForecastReportBean;
import com.globus.operations.forecast.forms.GmDemandSheetSummaryForm;

public class GmDemandSheetPartHistoryAction extends GmAction
{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
            
            GmDemandSheetSummaryForm gmDemandSheetSummaryForm = (GmDemandSheetSummaryForm)form;
            GmForecastReportBean gmForecastReportBean = new GmForecastReportBean();
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
            ArrayList alPartList = new ArrayList();
            
            gmDemandSheetSummaryForm.loadSessionParameters(request);
            String strDemandSheetMonthId = gmDemandSheetSummaryForm.getDemandSheetMonthId();
            
            alPartList = gmForecastReportBean.reportDemandSheetPartHistory(strDemandSheetMonthId);
            gmDemandSheetSummaryForm.setAlPartList(alPartList);
            
        }
        catch(Exception exp)
        {
            
            exp.printStackTrace();
            throw new AppError(exp);
        }
        return mapping.findForward("success");
    }

}
