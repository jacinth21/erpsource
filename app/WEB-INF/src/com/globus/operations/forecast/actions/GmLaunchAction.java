/**
 * FileName    : GmLaunchAction.java 
 * Description :
 * Author      : Brinal
 * Date        : Sept 30th, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.forecast.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.forecast.forms.GmDemandSheetSetupForm;
 
public class GmLaunchAction extends GmAction {
	
	 public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
	        try {
	        GmDemandSheetSetupForm gmDemandSheetSetupForm = (GmDemandSheetSetupForm)form;
	        gmDemandSheetSetupForm.loadSessionParameters(request);
	        GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean();
	        GmCommonBean gmCommonBean = new GmCommonBean (); 
	        RowSetDynaClass rdResult = null;
	        HashMap hmParams = new HashMap();
	        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

	        gmDemandSheetSetupForm.setAlDates(gmCommonBean.getDateList());
	        
	        hmParams = GmCommonClass.getHashMapFromForm(gmDemandSheetSetupForm);
	        
	        String strOpt = gmDemandSheetSetupForm.getStrOpt();
	        String strDemandSheetId = gmDemandSheetSetupForm.getDemandSheetId();
	        String strInputStr = gmDemandSheetSetupForm.getHinputStr();  
	        	                	        
	        log.debug (" Testing. ..... "+strInputStr );
	        if (strOpt.equals("save"))
	        { 
	            gmDemandSetupBean.saveLaunchDates(hmParams); 
	        } 
	        
	        rdResult = gmDemandSetupBean.loadSetMapListforLaunch(strDemandSheetId);
	        gmDemandSheetSetupForm.setLdtResult(rdResult.getRows());
	      
	        }
	        catch(Exception exp){
	            exp.printStackTrace();
	            throw new AppError(exp);
	        }
	        
	        return mapping.findForward("GmLaunch");
	    }


}
