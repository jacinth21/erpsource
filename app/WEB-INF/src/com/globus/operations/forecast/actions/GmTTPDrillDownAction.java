package com.globus.operations.forecast.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.ArrayList;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.forecast.beans.GmTTPReportBean;
import com.globus.operations.forecast.beans.GmForecastTransBean;
import com.globus.operations.forecast.forms.GmDemandSheetSummaryForm;
import com.globus.operations.forecast.forms.GmDrillDownForm;

public class GmTTPDrillDownAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

	// Initialize
	// the
	// Logger
	// Class.

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			GmTTPReportBean gmTTPReportBean = new GmTTPReportBean();
			GmForecastTransBean gmForecastTransBean = new GmForecastTransBean();
			GmDrillDownForm gmDrillDownForm = (GmDrillDownForm) form;
			HashMap hmReport = new HashMap();
			HashMap hmReturn = new HashMap();
			HashMap hmParams = new HashMap();
			String strOpt = "";

			strOpt = gmDrillDownForm.getStrOpt();
			log.debug("strOpt: " + strOpt);
			if (strOpt.equals("PPQDrillDown")) {
				ArrayList alPPQDetail = new ArrayList();
				String strPartNum = gmDrillDownForm.getPartNumber();
				String strInventoryLockId = gmDrillDownForm.getInventoryLockId();
				String strParentPartQty = gmDrillDownForm.getParentPartQty();
				log.debug("strPartNum : " + strPartNum + "--strInventoryLockId:" + strInventoryLockId);
				hmParams.put("PARTNUMBERS", strPartNum);
				hmParams.put("INVENTORYLOCKID", strInventoryLockId);

				hmReturn = gmForecastTransBean.fetchPPQDetails(hmParams);
				alPPQDetail = (ArrayList) hmReturn.get("PARENTPARTDETAILS");
				// log.debug("alPPQDetail:"+ alPPQDetail);

				gmDrillDownForm.setAlParentPartDetails(alPPQDetail);
				gmDrillDownForm.setPartNumber(strPartNum);
				gmDrillDownForm.setPartDescription((String) hmReturn.get("PDESC"));
				gmDrillDownForm.setParentPartQty(strParentPartQty);
				return mapping.findForward("parentPartDetails");

			} else {
				HashMap hmParam = GmCommonClass.getHashMapFromForm(gmDrillDownForm);
				log.debug(" values inside param --- " + hmParam);
				hmReport = gmTTPReportBean.fetchPartDemandSheetDetails(hmParam);
				gmDrillDownForm.setPartDetails((String) hmReport.get("PARTDETAILS"));
				gmDrillDownForm.setHmReport((HashMap) hmReport.get("CROSSTABREPORT"));
				return mapping.findForward("partSheetDetails");
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.debug("Exception " + e);
		}
		return mapping.findForward("partSheetDetails");
	}

}
