package com.globus.operations.forecast.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;

public class GmTTPGroupingForm extends GmCancelForm
{

    private String ttpId = "";
    private String ttpName = "";
    private String ttpOwner = "";
    private String hOld_ttpName = "";
    
    private String[] checkUnSelectedDS = new String [20];
    private String[] checkSelectedDS = new String [20];
    
    private ArrayList alTTPList = new ArrayList();
    private ArrayList alTTPOwner = new ArrayList();
    private ArrayList alUnSelectedDS = new ArrayList();
    private ArrayList alSelectedDS = new ArrayList();
    
    /**
     * @return Returns the alTTPList.
     */
    public ArrayList getAlTTPList()
    {
        return alTTPList;
    }
    /**
     * @param alTTPList The alTTPList to set.
     */
    public void setAlTTPList(ArrayList alTTPList)
    {
        this.alTTPList = alTTPList;
    }
    /**
     * @return Returns the alTTPOwner.
     */
    public ArrayList getAlTTPOwner()
    {
        return alTTPOwner;
    }
    /**
     * @param alTTPOwner The alTTPOwner to set.
     */
    public void setAlTTPOwner(ArrayList alTTPOwner)
    {
        this.alTTPOwner = alTTPOwner;
    }
    
    /**
     * @return Returns the checkSelectedDS.
     */
    public String[] getCheckSelectedDS()
    {
        return checkSelectedDS;
    }
    /**
     * @param checkSelectedDS The checkSelectedDS to set.
     */
    public void setCheckSelectedDS(String[] checkSelectedDS)
    {
        this.checkSelectedDS = checkSelectedDS;
    }
    /**
     * @return Returns the checkUnSelectedDS.
     */
    public String[] getCheckUnSelectedDS()
    {
        return checkUnSelectedDS;
    }
    /**
     * @param checkUnSelectedDS The checkUnSelectedDS to set.
     */
    public void setCheckUnSelectedDS(String[] checkUnSelectedDS)
    {
        this.checkUnSelectedDS = checkUnSelectedDS;
    }
    /**
     * @return Returns the ttpId.
     */
    public String getTtpId()
    {
        return ttpId;
    }
    /**
     * @param ttpId The ttpId to set.
     */
    public void setTtpId(String ttpId)
    {
        this.ttpId = ttpId;
    }
    /**
     * @return Returns the ttpName.
     */
    public String getTtpName()
    {
        return ttpName;
    }
    /**
     * @param ttpName The ttpName to set.
     */
    public void setTtpName(String ttpName)
    {
        this.ttpName = ttpName;
    }
    /**
     * @return Returns the ttpOwner.
     */
    public String getTtpOwner()
    {
        return ttpOwner;
    }
    /**
     * @param ttpOwner The ttpOwner to set.
     */
    public void setTtpOwner(String ttpOwner)
    {
        this.ttpOwner = ttpOwner;
    }
    /**
     * @return Returns the alSelectedDS.
     */
    public ArrayList getAlSelectedDS()
    {
        return alSelectedDS;
    }
    /**
     * @param alSelectedDS The alSelectedDS to set.
     */
    public void setAlSelectedDS(ArrayList alSelectedDS)
    {
        this.alSelectedDS = alSelectedDS;
    }
    /**
     * @return Returns the alUnSelectedDS.
     */
    public ArrayList getAlUnSelectedDS()
    {
        return alUnSelectedDS;
    }
    /**
     * @param alUnSelectedDS The alUnSelectedDS to set.
     */
    public void setAlUnSelectedDS(ArrayList alUnSelectedDS)
    {
        this.alUnSelectedDS = alUnSelectedDS;
    }
    /**
     * @return Returns the gethOld_ttpName.
     */
    public String gethOld_ttpName() {
		return hOld_ttpName;
	}
	/**
     * @param hOld_ttpName The hOld_ttpName to set.
     */
	public void sethOld_ttpName(String hOld_ttpName) {
		this.hOld_ttpName = hOld_ttpName;
	}
    
}
