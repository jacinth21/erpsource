/**
 * FileName    : GmDemandSheetAjaxForm.java 
 * Description :
 * Author      : vprasath
 * Date        : Aug 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.forecast.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

/**
 * @author vprasath
 *
 */
public class GmDemandSheetAjaxForm extends GmCommonForm {
	 private String demandSheetMonthId = "";
	 private String partDetails = "";
	 private String forecastMonths = "";
	 private String demandSheetString = "";
	 private String statusId = "";
	 private HashMap hmReport = new HashMap();
	 private List alResult = new ArrayList();
	 
	 
	public String getDemandSheetMonthId() {
		return demandSheetMonthId;
	}
	public void setDemandSheetMonthId(String demandSheetMonthId) {
		this.demandSheetMonthId = demandSheetMonthId;
	}
	
	public String getPartDetails() {
		return partDetails;
	}
	public void setPartDetails(String partDetails) {
		this.partDetails = partDetails;
	}
	public HashMap getHmReport() {
		return hmReport;
	}
	public void setHmReport(HashMap hmReport) {
		this.hmReport = hmReport;
	}
	public List getAlResult() {
		return alResult;
	}
	public void setAlResult(List alResult) {
		this.alResult = alResult;
	}
	public String getForecastMonths() {
		return forecastMonths;
	}
	public void setForecastMonths(String forecastMonths) {
		this.forecastMonths = forecastMonths;
	}
	public String getDemandSheetString() {
		return demandSheetString;
	}
	public void setDemandSheetString(String demandSheetString) {
		this.demandSheetString = demandSheetString;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	 
	 

}
