package com.globus.operations.forecast.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCommonForm;

public class GmDemandSheetReportForm extends GmCommonForm
{
    private String templateName = "";
    private String templateSetId = "";
    private String templateSetGrpNm = "";
    private List ldResult = new ArrayList();
    
    private ArrayList alTemplates = new ArrayList();

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	/**
	 * @return the templateSetId
	 */
	public String getTemplateSetId() {
		return templateSetId;
	}

	/**
	 * @param templateSetId the templateSetId to set
	 */
	public void setTemplateSetId(String templateSetId) {
		this.templateSetId = templateSetId;
	}

	/**
	 * @return the templateSetGrpNm
	 */
	public String getTemplateSetGrpNm() {
		return templateSetGrpNm;
	}

	/**
	 * @param templateSetGrpNm the templateSetGrpNm to set
	 */
	public void setTemplateSetGrpNm(String templateSetGrpNm) {
		this.templateSetGrpNm = templateSetGrpNm;
	}

	/**
	 * @return the alTemplates
	 */
	public ArrayList getAlTemplates() {
		return alTemplates;
	}

	/**
	 * @param alTemplates the alTemplates to set
	 */
	public void setAlTemplates(ArrayList alTemplates) {
		this.alTemplates = alTemplates;
	}

	/**
	 * @return the ldResult
	 */
	public List getLdResult() {
		return ldResult;
	}

	/**
	 * @param ldResult the ldResult to set
	 */
	public void setLdResult(List ldResult) {
		this.ldResult = ldResult;
	}
	
}
