package com.globus.operations.forecast.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

public class GmTTPSummaryForm extends GmCommonForm
{
    
    private String ttpId = "";
    private String monthId = GmCalenderOperations.getCurrentMM();
    private String yearId = String.valueOf(GmCalenderOperations.getCurrentYear());
    private String inventoryId = "";
    private String ttpForecastMonths = "";
    private String demandSheetIds = "";
    private String qtyOper = "";
    private String orderQty = "";
    private String ordOper = "";
    private String orderCost = "";    
    private String pnum="";
    private String ordType = "";
    private String summaryTotal = "";
    private String httpId ="";
    private String ttpType = "";
    private String quarAvail = "";
    private String quarAlloc = "";
    private String pdesc = "";
    private String strPnumOrd = "";
    private String ttpName = "";
    
 
	private ArrayList alTTPList = new ArrayList();
    private ArrayList alMonth = new ArrayList();
    private ArrayList alYear = new ArrayList();
    private ArrayList alOperators = new ArrayList();
    private ArrayList alOrderTypes = new ArrayList();
    
    private String[] checkUnSelectedSheet = new String [20];
    private String[] checkSelectedSheet = new String [20];
    private ArrayList alUnSelectedSheet = new ArrayList();
    private ArrayList alSelectedSheet = new ArrayList();
    private ArrayList alForeCastReport = new ArrayList();
    
    
    /**
     * @return Returns the alMonth.
     */
    public ArrayList getAlMonth()
    {
        return alMonth;
    }
    /**
     * @param alMonth The alMonth to set.
     */
    public void setAlMonth(ArrayList alMonth)
    {
        this.alMonth = alMonth;
    }
    /**
     * @return Returns the alTTPList.
     */
    public ArrayList getAlTTPList()
    {
        return alTTPList;
    }
    /**
     * @param alTTPList The alTTPList to set.
     */
    public void setAlTTPList(ArrayList alTTPList)
    {
        this.alTTPList = alTTPList;
    }
    /**
     * @return Returns the alYear.
     */
    public ArrayList getAlYear()
    {
        return alYear;
    }
    /**
     * @param alYear The alYear to set.
     */
    public void setAlYear(ArrayList alYear)
    {
        this.alYear = alYear;
    }
    /**
     * @return Returns the monthId.
     */
    public String getMonthId()
    {
        return monthId;
    }
    /**
     * @param monthId The monthId to set.
     */
    public void setMonthId(String monthId)
    {
        this.monthId = monthId;
    }
    /**
     * @return Returns the ttpId.
     */
    public String getTtpId()
    {
        return ttpId;
    }
    /**
     * @param ttpId The ttpId to set.
     */
    public void setTtpId(String ttpId)
    {
        this.ttpId = ttpId;
    }
    /**
     * @return Returns the yearId.
     */
    public String getYearId()
    {
        return yearId;
    }
    /**
     * @param yearId The yearId to set.
     */
    public void setYearId(String yearId)
    {
        this.yearId = yearId;
    }
    /**
     * @return Returns the alSelectedSheet.
     */
    public ArrayList getAlSelectedSheet()
    {
        return alSelectedSheet;
    }
    /**
     * @param alSelectedSheet The alSelectedSheet to set.
     */
    public void setAlSelectedSheet(ArrayList alSelectedSheet)
    {
        this.alSelectedSheet = alSelectedSheet;
    }
    /**
     * @return Returns the alUnSelectedSheet.
     */
    public ArrayList getAlUnSelectedSheet()
    {
        return alUnSelectedSheet;
    }
    /**
     * @param alUnSelectedSheet The alUnSelectedSheet to set.
     */
    public void setAlUnSelectedSheet(ArrayList alUnSelectedSheet)
    {
        this.alUnSelectedSheet = alUnSelectedSheet;
    }
    /**
     * @return Returns the checkSelectedSheet.
     */
    public String[] getCheckSelectedSheet()
    {
        return checkSelectedSheet;
    }
    /**
     * @param checkSelectedSheet The checkSelectedSheet to set.
     */
    public void setCheckSelectedSheet(String[] checkSelectedSheet)
    {
        this.checkSelectedSheet = checkSelectedSheet;
    }
    /**
     * @return Returns the checkUnSelectedSheet.
     */
    public String[] getCheckUnSelectedSheet()
    {
        return checkUnSelectedSheet;
    }
    /**
     * @param checkUnSelectedSheet The checkUnSelectedSheet to set.
     */
    public void setCheckUnSelectedSheet(String[] checkUnSelectedSheet)
    {
        this.checkUnSelectedSheet = checkUnSelectedSheet;
    }
    /**
     * @return Returns the inventoryId.
     */
    public String getInventoryId()
    {
        return inventoryId;
    }
    /**
     * @param inventoryId The inventoryId to set.
     */
    public void setInventoryId(String inventoryId)
    {
        this.inventoryId = inventoryId;
    }
    /**
     * @return Returns the forecastMonths.
     */
    public String getTtpForecastMonths()
    {
        return ttpForecastMonths;
    }
    /**
     * @param forecastMonths The forecastMonths to set.
     */
    public void setTtpForecastMonths(String ttpForecastMonths)
    {
        this.ttpForecastMonths = ttpForecastMonths;
    }
	public String getDemandSheetIds() {
		return demandSheetIds;
	}
	public void setDemandSheetIds(String demandSheetIds) {
		this.demandSheetIds = demandSheetIds;
	}
	public String getQtyOper() {
		return qtyOper;
	}
	public void setQtyOper(String qtyOper) {
		this.qtyOper = qtyOper;
	}
	public String getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(String orderQty) {
		this.orderQty = orderQty;
	}
	public ArrayList getAlOperators() {
		return alOperators;
	}
	public void setAlOperators(ArrayList alOperators) {
		this.alOperators = alOperators;
	}	
	public String getPnum() {
		return pnum;
	}
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	public String getOrdOper() {
		return ordOper;
	}
	public void setOrdOper(String ordOper) {
		this.ordOper = ordOper;
	}
	public String getOrderCost() {
		return orderCost;
	}
	public void setOrderCost(String orderCost) {
		this.orderCost = orderCost;
	}
	public String getOrdType() {
		return ordType;
	}
	public void setOrdType(String ordType) {
		this.ordType = ordType;
	}
	public ArrayList getAlOrderTypes() {
		return alOrderTypes;
	}
	public void setAlOrderTypes(ArrayList alOrderTypes) {
		this.alOrderTypes = alOrderTypes;
	}
	public String getSummaryTotal() {
		return summaryTotal;
	}
	public void setSummaryTotal(String summaryTotal) {
		this.summaryTotal = summaryTotal;
	}
	public String getHttpId() {
		return ttpId;
	}
	public void setHttpId(String httpId) {
		this.httpId = httpId;
	}
	public ArrayList getAlForeCastReport() {
		return alForeCastReport;
	}
	public void setAlForeCastReport(ArrayList alForeCastReport) {
		this.alForeCastReport = alForeCastReport;
	}
	public String getTtpType() {
		return ttpType;
	}
	public void setTtpType(String ttpType) {
		this.ttpType = ttpType;
	}
	public String getQuarAvail() {
		return quarAvail;
	}
	public void setQuarAvail(String quarAvail) {
		this.quarAvail = quarAvail;
	}
	public String getQuarAlloc() {
		return quarAlloc;
	}
	public void setQuarAlloc(String quarAlloc) {
		this.quarAlloc = quarAlloc;
	}
	public String getPdesc() {
		return pdesc;
	}
	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}
	/**
	 * @return the strPnumOrd
	 */
	public String getStrPnumOrd() {
		return strPnumOrd;
	}
	/**
	 * @param strPnumOrd the strPnumOrd to set
	 */
	public void setStrPnumOrd(String strPnumOrd) {
		this.strPnumOrd = strPnumOrd;
	}
	
	  
	/**
	 * @return the ttpName
	 */
	public String getTtpName() {
		return ttpName;
	}
	/**
	 * @param ttpName the ttpName to set
	 */
	public void setTtpName(String ttpName) {
		this.ttpName = ttpName;
	}
   
}
