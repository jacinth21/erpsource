package com.globus.operations.forecast.forms;

import java.util.HashMap;
import java.util.ArrayList;
import com.globus.common.forms.GmCommonForm;

public class GmDrillDownForm extends GmCommonForm {
	private String partNumber = "";
	private String demandSheetIds = "";
	private String partDetails = "";
	private String inventoryLockId = "";
	private String parentPartQty = "";
	private String partDescription = "";

	private HashMap hmReport = new HashMap();
	private ArrayList alParentPartDetails = new ArrayList();

	/**
	 * @return the partDetails
	 */
	public String getPartDetails() {
		return partDetails;
	}

	/**
	 * @param partDetails
	 *            the partDetails to set
	 */
	public void setPartDetails(String partDetails) {
		this.partDetails = partDetails;
	}

	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber
	 *            the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return the demandSheetIds
	 */
	public String getDemandSheetIds() {
		return demandSheetIds;
	}

	/**
	 * @param demandSheetIds
	 *            the demandSheetIds to set
	 */
	public void setDemandSheetIds(String demandSheetIds) {
		this.demandSheetIds = demandSheetIds;
	}

	/**
	 * @return the hmReport
	 */
	public HashMap getHmReport() {
		return hmReport;
	}

	/**
	 * @param hmReport
	 *            the hmReport to set
	 */
	public void setHmReport(HashMap hmReport) {
		this.hmReport = hmReport;
	}

	public String getInventoryLockId() {
		return inventoryLockId;
	}

	public void setInventoryLockId(String inventoryLockId) {
		this.inventoryLockId = inventoryLockId;
	}

	public String getParentPartQty() {
		return parentPartQty;
	}

	public void setParentPartQty(String parentPartQty) {
		this.parentPartQty = parentPartQty;
	}

	public ArrayList getAlParentPartDetails() {
		return alParentPartDetails;
	}

	public void setAlParentPartDetails(ArrayList alParentPartDetails) {
		this.alParentPartDetails = alParentPartDetails;
	}

	public String getPartDescription() {
		return partDescription;
	}

	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}

}
