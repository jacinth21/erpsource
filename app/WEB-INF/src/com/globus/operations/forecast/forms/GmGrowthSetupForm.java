package com.globus.operations.forecast.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmGrowthSetupForm extends GmLogForm {

	private String demandSheetId = "";
	private ArrayList alDemandSheets = new ArrayList();
	private String refType = "";
	private ArrayList alRefTypes = new ArrayList();
	private String refId = "";
	private ArrayList alRefIds = new ArrayList();
	private String year = "";
	private ArrayList alYears = new ArrayList();
	private String growthId = "";
	private String month1 = "";
	private String month2 = "";
	private String month3 = "";
	private String month4 = "";
	private String month5 = "";
	private String month6 = "";
	private String month7 = "";
	private String month8 = "";
	private String month9 = "";
	private String month10 = "";
	private String month11 = "";
	private String month12 = "";
	private String refMonth1 = "";
	private String refMonth2 = "";
	private String refMonth3 = "";
	private String refMonth4 = "";
	private String refMonth5 = "";
	private String refMonth6 = "";
	private String refMonth7 = "";
	private String refMonth8 = "";
	private String refMonth9 = "";
	private String refMonth10 = "";
	private String refMonth11 = "";
	private String refMonth12 = "";
	private String references = "";
	private String applyAllMonth = "";
	private ArrayList alRefMonths = new ArrayList();
	private String pdisablevalue = "";
	private String pnum = "";
	private String inputString = "";
	private String msg = "";
	private String overrideTypeId = "";
	private String refIdForLogReason = "";
	private String applyAll = "";

	private String prjStatus = ""; // to mark if the project is new or old
	private String launchDt = "";
	private int validateMonth;
	private int validateYear;

	private String controlValue1 = "";
	private String controlValue2 = "";
	private String controlValue3 = "";
	private String controlValue4 = "";
	private String controlValue5 = "";
	private String controlValue6 = "";
	private String controlValue7 = "";
	private String controlValue8 = "";
	private String controlValue9 = "";
	private String controlValue10 = "";
	private String controlValue11 = "";
	private String controlValue12 = "";
	private String evalutedContent = "";
	private HashMap GrowthDetailsMap = new HashMap();
	private String fromDate = "";
	private String toDate = "";
	private String applyToMarked = "";
	private String searchdemandSheetId="";

	/**
	 * @return the searchdemandSheetId
	 */
	public String getSearchdemandSheetId() {
		return searchdemandSheetId;
	}
	/**
	 * @param searchdemandSheetId the searchdemandSheetId to set
	 */
	public void setSearchdemandSheetId(String searchdemandSheetId) {
		this.searchdemandSheetId = searchdemandSheetId;
	}

	/**
	 * @return the applyToMarked
	 */
	public String getApplyToMarked() {
		return applyToMarked;
	}

	/**
	 * @param applyToMarked the applyToMarked to set
	 */
	public void setApplyToMarked(String applyToMarked) {
		this.applyToMarked = applyToMarked;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the growthDetailsMap
	 */
	public HashMap getGrowthDetailsMap() {
		return GrowthDetailsMap;
	}

	/**
	 * @param growthDetailsMap the growthDetailsMap to set
	 */
	public void setGrowthDetailsMap(HashMap growthDetailsMap) {
		GrowthDetailsMap = growthDetailsMap;
	}

	/**
	 * @return the evalutedContent
	 */
	public String getEvalutedContent() {
		return evalutedContent;
	}

	/**
	 * @param evalutedContent the evalutedContent to set
	 */
	public void setEvalutedContent(String evalutedContent) {
		this.evalutedContent = evalutedContent;
	}

	public String getApplyAll() {
		return applyAll;

	}

	public void setApplyAll(String applyAll) {
		this.applyAll = applyAll;
	}

	/**
	 * @return Returns the refIdForLogReason.
	 */
	public String getRefIdForLogReason() {
		return refIdForLogReason;
	}

	/**
	 * @param refIdForLogReason
	 *            The refIdForLogReason to set.
	 */
	public void setRefIdForLogReason(String refIdForLogReason) {
		this.refIdForLogReason = refIdForLogReason;
	}

	/**
	 * @return Returns the overrideTypeId.
	 */
	public String getOverrideTypeId() {
		return overrideTypeId;
	}

	/**
	 * @param overrideTypeId
	 *            The overrideTypeId to set.
	 */
	public void setOverrideTypeId(String overrideTypeId) {
		this.overrideTypeId = overrideTypeId;
	}

	public void reset() {
		// TODO Auto-generated method stub
		month1 = "";
		month2 = "";
		month3 = "";
		month4 = "";
		month5 = "";
		month6 = "";
		month7 = "";
		month8 = "";
		month9 = "";
		month10 = "";
		month11 = "";
		month12 = "";
		refMonth1 = "";
		refMonth2 = "";
		refMonth3 = "";
		refMonth4 = "";
		refMonth5 = "";
		refMonth6 = "";
		refMonth7 = "";
		refMonth8 = "";
		refMonth9 = "";
		refMonth10 = "";
		refMonth11 = "";
		refMonth12 = "";
		growthId = "";
		applyAllMonth = "";
	}

	/**
	 * @return Returns the msg.
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg
	 *            The msg to set.
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return Returns the inputString.
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString
	 *            The inputString to set.
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	/**
	 * @return Returns the pnum.
	 */
	public String getPnum() {
		return pnum;
	}

	/**
	 * @param pnum
	 *            The pnum to set.
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	public String toString() {
		return "[ Demand Sheet ID: " + demandSheetId + ", Reference Type: " + refType + ", Reference ID: " + refId + ",Year: " + year + " AL Demand Sheets: "
				+ alDemandSheets + ", AL Ref Types: " + alRefTypes + ", AL Ref IDs: " + alRefIds + "AL Years:" + alYears + " AL Ref Months:" + alRefMonths
				+ " ]";
	}

	/**
	 * @return Returns the alDemandSheets.
	 */
	public ArrayList getAlDemandSheets() {
		return alDemandSheets;
	}

	/**
	 * @param alDemandSheets
	 *            The alDemandSheets to set.
	 */
	public void setAlDemandSheets(ArrayList alDemandSheets) {
		this.alDemandSheets = alDemandSheets;
	}

	/**
	 * @return Returns the alRefIds.
	 */
	public ArrayList getAlRefIds() {
		return alRefIds;
	}

	/**
	 * @param alRefIds
	 *            The alRefIds to set.
	 */
	public void setAlRefIds(ArrayList alRefIds) {
		this.alRefIds = alRefIds;
	}

	/**
	 * @return Returns the alRefTypes.
	 */
	public ArrayList getAlRefTypes() {
		return alRefTypes;
	}

	/**
	 * @param alRefTypes
	 *            The alRefTypes to set.
	 */
	public void setAlRefTypes(ArrayList alRefTypes) {
		this.alRefTypes = alRefTypes;
	}

	/**
	 * @return Returns the alYears.
	 */
	public ArrayList getAlYears() {
		return alYears;
	}

	/**
	 * @param alYears
	 *            The alYears to set.
	 */
	public void setAlYears(ArrayList alYears) {
		this.alYears = alYears;
	}

	/**
	 * @return Returns the demandSheetId.
	 */
	public String getDemandSheetId() {
		return demandSheetId;
	}

	/**
	 * @param demandSheetId
	 *            The demandSheetId to set.
	 */
	public void setDemandSheetId(String demandSheetId) {
		this.demandSheetId = demandSheetId;
	}

	/**
	 * @return Returns the growthID.
	 */
	public String getGrowthId() {
		return growthId;
	}

	/**
	 * @param growthID
	 *            The growthID to set.
	 */
	public void setGrowthId(String growthId) {
		this.growthId = growthId;
	}

	/**
	 * @return Returns the refID.
	 */
	public String getRefId() {
		return refId;
	}

	/**
	 * @param refID
	 *            The refID to set.
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}

	/**
	 * @return Returns the refType.
	 */
	public String getRefType() {
		return refType;
	}

	/**
	 * @param refType
	 *            The refType to set.
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}

	/**
	 * @return Returns the year.
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year
	 *            The year to set.
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return Returns the alRefMonths.
	 */
	public ArrayList getAlRefMonths() {
		return alRefMonths;
	}

	/**
	 * @param alRefMonths
	 *            The alRefMonths to set.
	 */
	public void setAlRefMonths(ArrayList alRefMonths) {
		this.alRefMonths = alRefMonths;
	}

	/**
	 * @return Returns the month1.
	 */
	public String getMonth1() {
		return month1;
	}

	/**
	 * @param month1
	 *            The month1 to set.
	 */
	public void setMonth1(String month1) {
		this.month1 = month1;
	}

	/**
	 * @return Returns the month10.
	 */
	public String getMonth10() {
		return month10;
	}

	/**
	 * @param month10
	 *            The month10 to set.
	 */
	public void setMonth10(String month10) {
		this.month10 = month10;
	}

	/**
	 * @return Returns the month11.
	 */
	public String getMonth11() {
		return month11;
	}

	/**
	 * @param month11
	 *            The month11 to set.
	 */
	public void setMonth11(String month11) {
		this.month11 = month11;
	}

	/**
	 * @return Returns the month12.
	 */
	public String getMonth12() {
		return month12;
	}

	/**
	 * @param month12
	 *            The month12 to set.
	 */
	public void setMonth12(String month12) {
		this.month12 = month12;
	}

	/**
	 * @return Returns the month2.
	 */
	public String getMonth2() {
		return month2;
	}

	/**
	 * @param month2
	 *            The month2 to set.
	 */
	public void setMonth2(String month2) {
		this.month2 = month2;
	}

	/**
	 * @return Returns the month3.
	 */
	public String getMonth3() {
		return month3;
	}

	/**
	 * @param month3
	 *            The month3 to set.
	 */
	public void setMonth3(String month3) {
		this.month3 = month3;
	}

	/**
	 * @return Returns the month4.
	 */
	public String getMonth4() {
		return month4;
	}

	/**
	 * @param month4
	 *            The month4 to set.
	 */
	public void setMonth4(String month4) {
		this.month4 = month4;
	}

	/**
	 * @return Returns the month5.
	 */
	public String getMonth5() {
		return month5;
	}

	/**
	 * @param month5
	 *            The month5 to set.
	 */
	public void setMonth5(String month5) {
		this.month5 = month5;
	}

	/**
	 * @return Returns the month6.
	 */
	public String getMonth6() {
		return month6;
	}

	/**
	 * @param month6
	 *            The month6 to set.
	 */
	public void setMonth6(String month6) {
		this.month6 = month6;
	}

	/**
	 * @return Returns the month7.
	 */
	public String getMonth7() {
		return month7;
	}

	/**
	 * @param month7
	 *            The month7 to set.
	 */
	public void setMonth7(String month7) {
		this.month7 = month7;
	}

	/**
	 * @return Returns the month8.
	 */
	public String getMonth8() {
		return month8;
	}

	/**
	 * @param month8
	 *            The month8 to set.
	 */
	public void setMonth8(String month8) {
		this.month8 = month8;
	}

	/**
	 * @return Returns the month9.
	 */
	public String getMonth9() {
		return month9;
	}

	/**
	 * @param month9
	 *            The month9 to set.
	 */
	public void setMonth9(String month9) {
		this.month9 = month9;
	}

	/**
	 * @return Returns the refMonth1.
	 */
	public String getRefMonth1() {
		return refMonth1;
	}

	/**
	 * @param refMonth1
	 *            The refMonth1 to set.
	 */
	public void setRefMonth1(String refMonth1) {
		this.refMonth1 = refMonth1;
	}

	/**
	 * @return Returns the refMonth10.
	 */
	public String getRefMonth10() {
		return refMonth10;
	}

	/**
	 * @param refMonth10
	 *            The refMonth10 to set.
	 */
	public void setRefMonth10(String refMonth10) {
		this.refMonth10 = refMonth10;
	}

	/**
	 * @return Returns the refMonth11.
	 */
	public String getRefMonth11() {
		return refMonth11;
	}

	/**
	 * @param refMonth11
	 *            The refMonth11 to set.
	 */
	public void setRefMonth11(String refMonth11) {
		this.refMonth11 = refMonth11;
	}

	/**
	 * @return Returns the refMonth12.
	 */
	public String getRefMonth12() {
		return refMonth12;
	}

	/**
	 * @param refMonth12
	 *            The refMonth12 to set.
	 */
	public void setRefMonth12(String refMonth12) {
		this.refMonth12 = refMonth12;
	}

	/**
	 * @return Returns the refMonth2.
	 */
	public String getRefMonth2() {
		return refMonth2;
	}

	/**
	 * @param refMonth2
	 *            The refMonth2 to set.
	 */
	public void setRefMonth2(String refMonth2) {
		this.refMonth2 = refMonth2;
	}

	/**
	 * @return Returns the refMonth3.
	 */
	public String getRefMonth3() {
		return refMonth3;
	}

	/**
	 * @param refMonth3
	 *            The refMonth3 to set.
	 */
	public void setRefMonth3(String refMonth3) {
		this.refMonth3 = refMonth3;
	}

	/**
	 * @return Returns the refMonth4.
	 */
	public String getRefMonth4() {
		return refMonth4;
	}

	/**
	 * @param refMonth4
	 *            The refMonth4 to set.
	 */
	public void setRefMonth4(String refMonth4) {
		this.refMonth4 = refMonth4;
	}

	/**
	 * @return Returns the refMonth5.
	 */
	public String getRefMonth5() {
		return refMonth5;
	}

	/**
	 * @param refMonth5
	 *            The refMonth5 to set.
	 */
	public void setRefMonth5(String refMonth5) {
		this.refMonth5 = refMonth5;
	}

	/**
	 * @return Returns the refMonth6.
	 */
	public String getRefMonth6() {
		return refMonth6;
	}

	/**
	 * @param refMonth6
	 *            The refMonth6 to set.
	 */
	public void setRefMonth6(String refMonth6) {
		this.refMonth6 = refMonth6;
	}

	/**
	 * @return Returns the refMonth7.
	 */
	public String getRefMonth7() {
		return refMonth7;
	}

	/**
	 * @param refMonth7
	 *            The refMonth7 to set.
	 */
	public void setRefMonth7(String refMonth7) {
		this.refMonth7 = refMonth7;
	}

	/**
	 * @return Returns the refMonth8.
	 */
	public String getRefMonth8() {
		return refMonth8;
	}

	/**
	 * @param refMonth8
	 *            The refMonth8 to set.
	 */
	public void setRefMonth8(String refMonth8) {
		this.refMonth8 = refMonth8;
	}

	/**
	 * @return Returns the refMonth9.
	 */
	public String getRefMonth9() {
		return refMonth9;
	}

	/**
	 * @param refMonth9
	 *            The refMonth9 to set.
	 */
	public void setRefMonth9(String refMonth9) {
		this.refMonth9 = refMonth9;
	}

	/**
	 * @return Returns the monthAndYear.
	 */
	public String getReferences() {
		return references;
	}

	/**
	 * @param monthAndYear
	 *            The monthAndYear to set.
	 */
	public void setReferences(String references) {
		this.references = references;
	}

	public String getApplyAllMonth() {
		return applyAllMonth;
	}

	public void setApplyAllMonth(String applyAllMonth) {
		this.applyAllMonth = applyAllMonth;
	}

	public String getPdisablevalue() {
		return pdisablevalue;
	}

	public void setPdisablevalue(String pdisablevalue) {
		this.pdisablevalue = pdisablevalue;
	}

	public String getControlValue1() {
		return controlValue1;
	}

	public void setControlValue1(String controlValue1) {
		this.controlValue1 = controlValue1;
	}

	public String getControlValue2() {
		return controlValue2;
	}

	public void setControlValue2(String controlValue2) {
		this.controlValue2 = controlValue2;
	}

	public String getControlValue3() {
		return controlValue3;
	}

	public void setControlValue3(String controlValue3) {
		this.controlValue3 = controlValue3;
	}

	public String getControlValue4() {
		return controlValue4;
	}

	public void setControlValue4(String controlValue4) {
		this.controlValue4 = controlValue4;
	}

	public String getControlValue5() {
		return controlValue5;
	}

	public void setControlValue5(String controlValue5) {
		this.controlValue5 = controlValue5;
	}

	public String getControlValue6() {
		return controlValue6;
	}

	public void setControlValue6(String controlValue6) {
		this.controlValue6 = controlValue6;
	}

	public String getControlValue7() {
		return controlValue7;
	}

	public void setControlValue7(String controlValue7) {
		this.controlValue7 = controlValue7;
	}

	public String getControlValue8() {
		return controlValue8;
	}

	public void setControlValue8(String controlValue8) {
		this.controlValue8 = controlValue8;
	}

	public String getControlValue9() {
		return controlValue9;
	}

	public void setControlValue9(String controlValue9) {
		this.controlValue9 = controlValue9;
	}

	public String getControlValue10() {
		return controlValue10;
	}

	public void setControlValue10(String controlValue10) {
		this.controlValue10 = controlValue10;
	}

	public String getControlValue11() {
		return controlValue11;
	}

	public void setControlValue11(String controlValue11) {
		this.controlValue11 = controlValue11;
	}

	public String getControlValue12() {
		return controlValue12;
	}

	public void setControlValue12(String controlValue12) {
		this.controlValue12 = controlValue12;
	}

	/**
	 * @return the prjStatus
	 */
	public String getPrjStatus() {
		return prjStatus;
	}

	/**
	 * @param prjStatus
	 *            the prjStatus to set
	 */
	public void setPrjStatus(String prjStatus) {
		this.prjStatus = prjStatus;
	}

	/**
	 * @return the launchDt
	 */
	public String getLaunchDt() {
		return launchDt;
	}

	/**
	 * @param launchDt
	 *            the launchDt to set
	 */
	public void setLaunchDt(String launchDt) {
		this.launchDt = launchDt;
	}

	/**
	 * @return the validateMonth
	 */
	public int getValidateMonth() {
		return validateMonth;
	}

	/**
	 * @param validateMonth
	 *            the validateMonth to set
	 */
	public void setValidateMonth(int validateMonth) {
		this.validateMonth = validateMonth;
	}

	/**
	 * @return the validateYear
	 */
	public int getValidateYear() {
		return validateYear;
	}

	/**
	 * @param validateYear
	 *            the validateYear to set
	 */
	public void setValidateYear(int validateYear) {
		this.validateYear = validateYear;
	}

}
