/**
 * 
 */
package com.globus.operations.factory.beans;

import com.globus.common.beans.GmCommonClass;
import com.globus.operations.abs.beans.GmUDIBean;
import com.globus.operations.beans.GmOUSUDIBean;
import com.globus.operations.beans.GmUSUDIBean;

import org.apache.log4j.Logger;
/**
 * @author tsekaran
 *
 */
public class GmUDIFactoryBean {
	public GmUDIBean getInstance(String strAction)
	{
		if(strAction.equals("en"))
		{
			return new GmUSUDIBean();
		}
		else
		{
			return new GmOUSUDIBean();
		}

	}

}
