/**
 * 
 */
package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

/**
 * @author arajan
 * 
 */
public class GmPartRedesignationForm extends GmLogForm {

  private String partId = "";
  private String gridXmlData = "";
  private String hInputString = "";
  private String hctlNumInvString = "";
  private String hfinalComments = "";
  private String strTxnIds = "";
  private String donorId = "";
  private String controlId = "";
  private String hScannedLotStr = "";
  private String hScannedPartStr = "";
  private String hScannedDonorStr = "";
  private String stgDt = "";


  /**
 * @return the stgDt
 */
public String getStgDt() {
	return stgDt;
}

/**
 * @param stgDt the stgDt to set
 */
public void setStgDt(String stgDt) {
	this.stgDt = stgDt;
}

private ArrayList alPartsList = new ArrayList();
  private ArrayList alLogReasons = new ArrayList();

  /**
   * @return the partId
   */
  public String getPartId() {
    return partId;
  }

  /**
   * @param partId the partId to set
   */
  public void setPartId(String partId) {
    this.partId = partId;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the alPartsList
   */
  public ArrayList getAlPartsList() {
    return alPartsList;
  }

  /**
   * @param alPartsList the alPartsList to set
   */
  public void setAlPartsList(ArrayList alPartsList) {
    this.alPartsList = alPartsList;
  }

  /**
   * @return the alLogReasons
   */
  public ArrayList getAlLogReasons() {
    return alLogReasons;
  }

  /**
   * @param alLogReasons the alLogReasons to set
   */
  public void setAlLogReasons(ArrayList alLogReasons) {
    this.alLogReasons = alLogReasons;
  }

  /**
   * @return the hInputString
   */
  public String gethInputString() {
    return hInputString;
  }

  /**
   * @param hInputString the hInputString to set
   */
  public void sethInputString(String hInputString) {
    this.hInputString = hInputString;
  }

  /**
   * @return the hctlNumInvString
   */
  public String getHctlNumInvString() {
    return hctlNumInvString;
  }

  /**
   * @param hctlNumInvString the hctlNumInvString to set
   */
  public void setHctlNumInvString(String hctlNumInvString) {
    this.hctlNumInvString = hctlNumInvString;
  }

  /**
   * @return the hfinalComments
   */
  public String getHfinalComments() {
    return hfinalComments;
  }

  /**
   * @param hfinalComments the hfinalComments to set
   */
  public void setHfinalComments(String hfinalComments) {
    this.hfinalComments = hfinalComments;
  }

  /**
   * @return the strTxnIds
   */
  public String getStrTxnIds() {
    return strTxnIds;
  }

  /**
   * @param strTxnIds the strTxnIds to set
   */
  public void setStrTxnIds(String strTxnIds) {
    this.strTxnIds = strTxnIds;
  }

  /**
   * @return the donorId
   */
  public String getDonorId() {
    return donorId;
  }

  /**
   * @param donorId the donorId to set
   */
  public void setDonorId(String donorId) {
    this.donorId = donorId;
  }

  /**
   * @return the controlId
   */
  public String getControlId() {
    return controlId;
  }

  /**
   * @param controlId the controlId to set
   */
  public void setControlId(String controlId) {
    this.controlId = controlId;
  }

  /**
   * @return the hScannedLotStr
   */
  public String gethScannedLotStr() {
    return hScannedLotStr;
  }

  /**
   * @param hScannedLotStr the hScannedLotStr to set
   */
  public void sethScannedLotStr(String hScannedLotStr) {
    this.hScannedLotStr = hScannedLotStr;
  }

  /**
   * @return the hScannedPartStr
   */
  public String gethScannedPartStr() {
    return hScannedPartStr;
  }

  /**
   * @param hScannedPartStr the hScannedPartStr to set
   */
  public void sethScannedPartStr(String hScannedPartStr) {
    this.hScannedPartStr = hScannedPartStr;
  }

  /**
   * @return the hScannedDonorStr
   */
  public String gethScannedDonorStr() {
    return hScannedDonorStr;
  }

  /**
   * @param hScannedDonorStr the hScannedDonorStr to set
   */
  public void sethScannedDonorStr(String hScannedDonorStr) {
    this.hScannedDonorStr = hScannedDonorStr;
  }

}
