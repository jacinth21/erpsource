package com.globus.operations.forms;

import com.globus.common.forms.GmCommonForm;

public class GmChangeLotForm extends GmCommonForm {

	private String transactionId = "";
	private String partNumber = "";
	private String quantity = "";
	private String oldCntrlNumber = "";
	private String newCntrlNumber = "";
	
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}
	/**
	 * @param partNumber the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the oldCntrlNumber
	 */
	public String getOldCntrlNumber() {
		return oldCntrlNumber;
	}
	/**
	 * @param oldCntrlNumber the oldCntrlNumber to set
	 */
	public void setOldCntrlNumber(String oldCntrlNumber) {
		this.oldCntrlNumber = oldCntrlNumber;
	}
	/**
	 * @return the newCntrlNumber
	 */
	public String getNewCntrlNumber() {
		return newCntrlNumber;
	}
	/**
	 * @param newCntrlNumber the newCntrlNumber to set
	 */
	public void setNewCntrlNumber(String newCntrlNumber) {
		this.newCntrlNumber = newCntrlNumber;
	}
}
