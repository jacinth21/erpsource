package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmControlValidationForm extends GmCommonForm {

	private String partcnum = " ";
	private ArrayList alResult=new ArrayList(); 
	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}
	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	public String getPartcnum() {
		return partcnum;
	}
	public void setPartcnum(String partcnum) {
		this. partcnum = partcnum;
	}

}
