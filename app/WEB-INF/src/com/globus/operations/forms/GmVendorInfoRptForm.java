package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmVendorInfoRptForm extends GmCommonForm {
	
	private String vendorId = "";
	private String location ="";	
	private String month ="50720";
	private String projectId = "";	
	private String poType = "3100";
	
	private String curImplantOpr = "";
	private String curImplant = "";
	private String curInsOpr = "";
	private String curIns = "";
	private String coreImplantOpr = "";
	private String coreImplant = "";
	private String coreInsOpr = "";
	private String coreIns = "";
	private String ncoreImplantOpr = "";
	private String ncoreImplant = "";
	private String ncoreInsOpr = "";
	private String ncoreIns = "";
	private String strXmlGridData = "";
	
	private ArrayList alResult = new ArrayList();
	private ArrayList alVendorList = new ArrayList(); 
	private ArrayList alLocationList = new ArrayList();
	private ArrayList alMonthList = new ArrayList();
	private ArrayList alProjectList = new ArrayList();
	private ArrayList alPOTypeList = new ArrayList();
	private ArrayList alOperatorsList = new ArrayList();
	
	public String getStrXmlGridData() {
        return strXmlGridData;
	}

	public void setStrXmlGridData(String strXmlGridData) {
 	   this.strXmlGridData = strXmlGridData;
	}
	
	public ArrayList getAlOperatorsList() {
		return alOperatorsList;
	}
	public void setAlOperatorsList(ArrayList alOperatorsList) {
		this.alOperatorsList = alOperatorsList;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}	
	public String getPoType() {
		return poType;
	}
	public void setPoType(String poType) {
		this.poType = poType;
	}
	public ArrayList getAlResult() {
		return alResult;
	}
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	public ArrayList getAlVendorList() {
		return alVendorList;
	}
	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}
	public ArrayList getAlLocationList() {
		return alLocationList;
	}
	public void setAlLocationList(ArrayList alLocationList) {
		this.alLocationList = alLocationList;
	}
	public ArrayList getAlMonthList() {
		return alMonthList;
	}
	public void setAlMonthList(ArrayList alMonthList) {
		this.alMonthList = alMonthList;
	}
	public ArrayList getAlProjectList() {
		return alProjectList;
	}
	public void setAlProjectList(ArrayList alProjectList) {
		this.alProjectList = alProjectList;
	}
	public ArrayList getAlPOTypeList() {
		return alPOTypeList;
	}
	public void setAlPOTypeList(ArrayList alPOTypeList) {
		this.alPOTypeList = alPOTypeList;
	}
	public String getCurImplantOpr() {
		return curImplantOpr;
	}
	public void setCurImplantOpr(String curImplantOpr) {
		this.curImplantOpr = curImplantOpr;
	}
	public String getCurImplant() {
		return curImplant;
	}
	public void setCurImplant(String curImplant) {
		this.curImplant = curImplant;
	}
	public String getCurInsOpr() {
		return curInsOpr;
	}
	public void setCurInsOpr(String curInsOpr) {
		this.curInsOpr = curInsOpr;
	}
	public String getCurIns() {
		return curIns;
	}
	public void setCurIns(String curIns) {
		this.curIns = curIns;
	}
	public String getCoreImplantOpr() {
		return coreImplantOpr;
	}
	public void setCoreImplantOpr(String coreImplantOpr) {
		this.coreImplantOpr = coreImplantOpr;
	}
	public String getCoreImplant() {
		return coreImplant;
	}
	public void setCoreImplant(String coreImplant) {
		this.coreImplant = coreImplant;
	}
	public String getCoreInsOpr() {
		return coreInsOpr;
	}
	public void setCoreInsOpr(String coreInsOpr) {
		this.coreInsOpr = coreInsOpr;
	}
	public String getCoreIns() {
		return coreIns;
	}
	public void setCoreIns(String coreIns) {
		this.coreIns = coreIns;
	}
	public String getNcoreImplantOpr() {
		return ncoreImplantOpr;
	}
	public void setNcoreImplantOpr(String ncoreImplantOpr) {
		this.ncoreImplantOpr = ncoreImplantOpr;
	}
	public String getNcoreImplant() {
		return ncoreImplant;
	}
	public void setNcoreImplant(String ncoreImplant) {
		this.ncoreImplant = ncoreImplant;
	}
	public String getNcoreInsOpr() {
		return ncoreInsOpr;
	}
	public void setNcoreInsOpr(String ncoreInsOpr) {
		this.ncoreInsOpr = ncoreInsOpr;
	}
	public String getNcoreIns() {
		return ncoreIns;
	}
	public void setNcoreIns(String ncoreIns) {
		this.ncoreIns = ncoreIns;
	}
}
