package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;
/**
* GmLotExpiryReportForm : Contains the methods used for the Lot Expiry report
* author : Agilan Singaravel
*/
public class GmLotExpiryReportForm extends GmCommonForm{
	
	private static final long serialVersionUID = 1L;
	
	private String partInfo;
	private String lotExpDays;
	private String partDetails;
	private String chk_ActiveFl = "";
	private int expDays;
	private String partNum;
	private int expiryFlag;
	private String strType = "";
	private String projectId;	
	private String loadFlg;	
	private String gridXmlData = "";
	private String strPartLiteral = "";
	private ArrayList alPartSearch = new ArrayList();
	/**
	 * @return the partInfo
	 */
	public String getPartInfo() {
		return partInfo;
	}
	/**
	 * @param partInfo the partInfo to set
	 */
	public void setPartInfo(String partInfo) {
		this.partInfo = partInfo;
	}
	/**
	 * @return the lotExpDays
	 */
	public String getLotExpDays() {
		return lotExpDays;
	}
	/**
	 * @param lotExpDays the lotExpDays to set
	 */
	public void setLotExpDays(String lotExpDays) {
		this.lotExpDays = lotExpDays;
	}
	/**
	 * @return the partDetails
	 */
	public String getPartDetails() {
		return partDetails;
	}
	/**
	 * @param partDetails the partDetails to set
	 */
	public void setPartDetails(String partDetails) {
		this.partDetails = partDetails;
	}
	/**
	 * @return the chk_ActiveFl
	 */
	public String getChk_ActiveFl() {
		return chk_ActiveFl;
	}
	/**
	 * @param chk_ActiveFl the chk_ActiveFl to set
	 */
	public void setChk_ActiveFl(String chk_ActiveFl) {
		this.chk_ActiveFl = chk_ActiveFl;
	}
	/**
	 * @return the expDays
	 */
	public int getExpDays() {
		return expDays;
	}
	/**
	 * @param expDays the expDays to set
	 */
	public void setExpDays(int expDays) {
		this.expDays = expDays;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the expiryFlag
	 */
	public int getExpiryFlag() {
		return expiryFlag;
	}
	/**
	 * @param expiryFlag the expiryFlag to set
	 */
	public void setExpiryFlag(int expiryFlag) {
		this.expiryFlag = expiryFlag;
	}
	/**
	 * @return the strType
	 */
	public String getStrType() {
		return strType;
	}
	/**
	 * @param strType the strType to set
	 */
	public void setStrType(String strType) {
		this.strType = strType;
	}
	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the loadFlg
	 */
	public String getLoadFlg() {
		return loadFlg;
	}
	/**
	 * @param loadFlg the loadFlg to set
	 */
	public void setLoadFlg(String loadFlg) {
		this.loadFlg = loadFlg;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the strPartLiteral
	 */
	public String getStrPartLiteral() {
		return strPartLiteral;
	}
	/**
	 * @param strPartLiteral the strPartLiteral to set
	 */
	public void setStrPartLiteral(String strPartLiteral) {
		this.strPartLiteral = strPartLiteral;
	}
	/**
	 * @return the alPartSearch
	 */
	public ArrayList getAlPartSearch() {
		return alPartSearch;
	}
	/**
	 * @param alPartSearch the alPartSearch to set
	 */
	public void setAlPartSearch(ArrayList alPartSearch) {
		this.alPartSearch = alPartSearch;
	}

}
