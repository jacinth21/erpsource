package com.globus.operations.forms;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonForm;


public class GmLoanerTransferForm extends GmCommonForm
{
    private static final long serialVersionUID = 1L;
    
    private ArrayList alFromDistributor = new ArrayList();
    private ArrayList alToDistributor = new ArrayList();
    ArrayList alSetId = new ArrayList();
    ArrayList alStatus = new ArrayList();
    private ArrayList alReps = new ArrayList();
    private ArrayList alAssocReps = new ArrayList();
    private ArrayList alAccounts = new ArrayList();
	private String strStatusFl="";
    private String fromDistributor = "";
    private String toDistributor = "";
    private String gridXmlData = "";
    private String strInputString = "";
    private String setName = "";
    private String transferDtFrom = "";
    private String transferDtTo = "";
    private String setId = "";
    private String fromDistName ="";
    private String toDistName ="";
    private String inputString="";
    private String accessFl ="";
    /**
	 * @return the accessFl
	 */
	public String getAccessFl() {
		return accessFl;
	}
	/**
	 * @param accessFl the accessFl to set
	 */
	public void setAccessFl(String accessFl) {
		this.accessFl = accessFl;
	}
	public ArrayList getAlStatus() {
		return alStatus;
	}
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	public String getStrStatusFl() {
		return strStatusFl;
	}
	public void setStrStatusFl(String strStatusFl) {
		this.strStatusFl = strStatusFl;
	}
    public String getInputString() {
		return inputString;
	}
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	public String getFromDistName() {
		return fromDistName;
	}
	public void setFromDistName(String fromDistName) {
		this.fromDistName = fromDistName;
	}
	public String getToDistName() {
		return toDistName;
	}
	public void setToDistName(String toDistName) {
		this.toDistName = toDistName;
	}
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public String getSetName() {
		return setName;
	}
	public void setSetName(String setName) {
		this.setName = setName;
	}
	
	public String getTransferDtFrom() {
		return transferDtFrom;
	}
	public void setTransferDtFrom(String transferDtFrom) {
		this.transferDtFrom = transferDtFrom;
	}
	public String getTransferDtTo() {
		return transferDtTo;
	}
	public void setTransferDtTo(String transferDtTo) {
		this.transferDtTo = transferDtTo;
	}
	
	public ArrayList getAlSetId() {
		return alSetId;
	}
	public void setAlSetId(ArrayList alSetId) {
		this.alSetId = alSetId;
	}
	/**
	 * @return the strInputString
	 */
	public String getStrInputString() {
		return strInputString;
	}
	/**
	 * @param strInputString the strInputString to set
	 */
	public void setStrInputString(String strInputString) {
		this.strInputString = strInputString;
	}
	
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the toDistributor
	 */
	public String getToDistributor() {
		return toDistributor;
	}
	/**
	 * @param toDistributor the toDistributor to set
	 */
	public void setToDistributor(String toDistributor) {
		this.toDistributor = toDistributor;
	}
	/**
	 * @return the alFromDistributor
	 */
	public ArrayList getAlFromDistributor() {
		return alFromDistributor;
	}
	/**
	 * @param alFromDistributor the alFromDistributor to set
	 */
	public void setAlFromDistributor(ArrayList alFromDistributor) {
		this.alFromDistributor = alFromDistributor;
	}
	/**
	 * @return the alToDistributor
	 */
	public ArrayList getAlToDistributor() {
		return alToDistributor;
	}
	/**
	 * @param alToDistributor the alToDistributor to set
	 */
	public void setAlToDistributor(ArrayList alToDistributor) {
		this.alToDistributor = alToDistributor;
	}
	/**
	 * @return the fromDistributor
	 */
	public String getFromDistributor() {
		return fromDistributor;
	}
	/**
	 * @param fromDistributor the fromDistributor to set
	 */
	public void setFromDistributor(String fromDistributor) {
		this.fromDistributor = fromDistributor;
	}
	/**
	 * @param alReps the alReps to set
	 */
	public void setAlReps(ArrayList alReps) {
		this.alReps = alReps;
	}
	/**
	 * @return the alReps
	 */
	public ArrayList getAlReps() {
		return alReps;
	}
	/**
	 * @param alAccounts the alAccounts to set
	 */
	public void setAlAccounts(ArrayList alAccounts) {
		this.alAccounts = alAccounts;
	}
	/**
	 * @return the alAccounts
	 */
	public ArrayList getAlAccounts() {
		return alAccounts;
	}
	public ArrayList getAlAssocReps() {
		return alAssocReps;
	}
	public void setAlAssocReps(ArrayList alAssocReps) {
		this.alAssocReps = alAssocReps;
	}
	
}
