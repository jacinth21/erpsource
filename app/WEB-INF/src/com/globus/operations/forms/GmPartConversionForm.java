package com.globus.operations.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmPartConversionForm extends GmCommonForm{
	
	private String partNumber = "";
	private String partDescription = "";
	private String invQOH = "";
	private String cogsCost = "";
	private String toPartQty = "";
	private String toCogsCost = "";
	private String toPartNum ="";
	private String partConversionComments ="";
	private String totalConversionQty ="";
	private String totalPrice ="";
	private String fromInvType ="";
	private String toInvType ="";
	private String hinputStr = "";
	private String pcID = "";
	private String createdDate = "";
	private String createdBy = "";
	private String counterValue = "";
	
	private ArrayList alPartDetails = new ArrayList();
	private ArrayList alInvType = new ArrayList();
	private List ldtResult = new ArrayList(); //Stores report output table.
	
	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}
	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}
	/**
	 * @return the alPartDetails
	 */
	public ArrayList getAlPartDetails() {
		return alPartDetails;
	}
	/**
	 * @param alPartDetails the alPartDetails to set
	 */
	public void setAlPartDetails(ArrayList alPartDetails) {
		this.alPartDetails = alPartDetails;
	}
	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}
	/**
	 * @param partNumber the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	/**
	 * @return the partDescription
	 */
	public String getPartDescription() {
		return partDescription;
	}
	/**
	 * @param partDescription the partDescription to set
	 */
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	/**
	 * @return the invQOH
	 */
	public String getInvQOH() {
		return invQOH;
	}
	/**
	 * @param invQOH the invQOH to set
	 */
	public void setInvQOH(String invQOH) {
		this.invQOH = invQOH;
	}
	/**
	 * @return the cogsCost
	 */
	public String getCogsCost() {
		return cogsCost;
	}
	/**
	 * @param cogsCost the cogsCost to set
	 */
	public void setCogsCost(String cogsCost) {
		this.cogsCost = cogsCost;
	}
	/**
	 * @return the toPartQty
	 */
	public String getToPartQty() {
		return toPartQty;
	}
	/**
	 * @param toPartQty the toPartQty to set
	 */
	public void setToPartQty(String toPartQty) {
		this.toPartQty = toPartQty;
	}
	/**
	 * @return the toCogsCost
	 */
	public String getToCogsCost() {
		return toCogsCost;
	}
	/**
	 * @param toCogsCost the toCogsCost to set
	 */
	public void setToCogsCost(String toCogsCost) {
		this.toCogsCost = toCogsCost;
	}
	/**
	 * @return the toPartNum
	 */
	public String getToPartNum() {
		return toPartNum;
	}
	/**
	 * @param toPartNum the toPartNum to set
	 */
	public void setToPartNum(String toPartNum) {
		this.toPartNum = toPartNum;
	}
	/**
	 * @return the partConversionComments
	 */
	public String getPartConversionComments() {
		return partConversionComments;
	}
	/**
	 * @param partConversionComments the partConversionComments to set
	 */
	public void setPartConversionComments(String partConversionComments) {
		this.partConversionComments = partConversionComments;
	}
	/**
	 * @return the totalConversionQty
	 */
	public String getTotalConversionQty() {
		return totalConversionQty;
	}
	/**
	 * @param totalConversionQty the totalConversionQty to set
	 */
	public void setTotalConversionQty(String totalConversionQty) {
		this.totalConversionQty = totalConversionQty;
	}
	/**
	 * @return the totalPrice
	 */
	public String getTotalPrice() {
		return totalPrice;
	}
	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	/**
	 * @return the fromInvType
	 */
	public String getFromInvType() {
		return fromInvType;
	}
	/**
	 * @param fromInvType the fromInvType to set
	 */
	public void setFromInvType(String fromInvType) {
		this.fromInvType = fromInvType;
	}
	/**
	 * @return the toInvType
	 */
	public String getToInvType() {
		return toInvType;
	}
	/**
	 * @param toInvType the toInvType to set
	 */
	public void setToInvType(String toInvType) {
		this.toInvType = toInvType;
	}
	/**
	 * @return the hinputStr
	 */
	public String getHinputStr() {
		return hinputStr;
	}
	/**
	 * @param hinputStr the hinputStr to set
	 */
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}
	/**
	 * @return the pcID
	 */
	public String getPcID() {
		return pcID;
	}
	/**
	 * @param pcID the pcID to set
	 */
	public void setPcID(String pcID) {
		this.pcID = pcID;
	}
	/**
	 * @return the createdDate
	 */
	public String getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the alInvType
	 */
	public ArrayList getAlInvType() {
		return alInvType;
	}
	/**
	 * @param alInvType the alInvType to set
	 */
	public void setAlInvType(ArrayList alInvType) {
		this.alInvType = alInvType;
	}
	/**
	 * @return the counterValue
	 */
	public String getCounterValue() {
		return counterValue;
	}
	/**
	 * @param counterValue the counterValue to set
	 */
	public void setCounterValue(String counterValue) {
		this.counterValue = counterValue;
	}

}
