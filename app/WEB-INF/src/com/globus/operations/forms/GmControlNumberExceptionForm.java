package com.globus.operations.forms;


import com.globus.common.forms.GmLogForm;

public class GmControlNumberExceptionForm extends GmLogForm {
	private String strXmlData="";
	private String partNumber="";
	private String strOpt="";
	private String ctrlAsd_ID = "";
	private String partNM = "";
	private String controlNM = "";
	private String partDesc="";
	private String strMsg="";
	private String leftLink="";
	private String fromDt ="";
	private String toDt = "";
	private String prodType = "";
	private String donorNM = "";
	private String vendorId = "";	
	private String txnId = "";

	public String getLeftLink() {
		return leftLink;
	}


	public void setLeftLink(String leftLink) {
		this.leftLink = leftLink;
	}


	/**
	 * @return the strMsg
	 */
	public String getStrMsg() {
		return strMsg;
	}


	/**
	 * @param strMsg the strMsg to set
	 */
	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}


	/**
	 * @return the partDesc
	 */
	public String getPartDesc() {
		return partDesc;
	}


	/**
	 * @param partDesc the partDesc to set
	 */
	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}


	/**
	 * @return the partNM
	 */
	public String getPartNM() {
		return partNM;
	}

	
	/**
	 * @param partNM the partNM to set
	 */
	public void setPartNM(String partNM) {
		this.partNM = partNM;
	} 

	/**
	 * @return the controlNM
	 */
	public String getControlNM() {
		return controlNM;
	}

	/**
	 * @param controlNM the controlNM to set
	 */
	public void setControlNM(String controlNM) {
		this.controlNM = controlNM;
	}

	/**
	 * @return the ctrlAsd_ID
	 */
	public String getCtrlAsd_ID() {
		return ctrlAsd_ID;
	}

	/**
	 * @param ctrlAsdID the ctrlAsd_ID to set
	 */
	public void setCtrlAsd_ID(String ctrlAsdID) {
		ctrlAsd_ID = ctrlAsdID;
	}
	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	/**
	 * @return the strOpt
	 */
	public String getStrOpt() {
		return strOpt;
	}

	/**
	 * @param strOpt the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}
	
	/**
	 * @return the strXmlData
	 */
	public String getStrXmlData() {
		return strXmlData;
	}

	/**
	 * @param strXmlData the strXmlData to set
	 */
	public void setStrXmlData(String strXmlData) {
		this.strXmlData = strXmlData;
	}


	/**
	 * @return the fromDt
	 */
	public String getFromDt() {
		return fromDt;
	}


	/**
	 * @param fromDt the fromDt to set
	 */
	public void setFromDt(String fromDt) {
		this.fromDt = fromDt;
	}


	/**
	 * @return the toDt
	 */
	public String getToDt() {
		return toDt;
	}


	/**
	 * @param toDt the toDt to set
	 */
	public void setToDt(String toDt) {
		this.toDt = toDt;
	}


	/**
	 * @return the prodType
	 */
	public String getProdType() {
		return prodType;
	}


	/**
	 * @param prodType the prodType to set
	 */
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}


	/**
	 * @return the donorNM
	 */
	public String getDonorNM() {
		return donorNM;
	}


	/**
	 * @param donorNM the donorNM to set
	 */
	public void setDonorNM(String donorNM) {
		this.donorNM = donorNM;
	}


	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}


	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}


	public String getTxnId() {
		return txnId;
	}


	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}


	

	
}
