/**
 * 
 */
package com.globus.operations.forms;

import com.globus.common.forms.GmCommonForm;

/**
 * @author arajan
 *
 */
public class GmLotCodeRptForm extends GmCommonForm {
	
	private String lotNum = "";
	private String partNum = "";
	private String partDesc = "";
	private String partSize = "";
	private String cpcClient = "";
	private String donorNum = "";
	private String donorAge = "";
	private String donorSex = "";
	private String intUse = "";
	private String forResearch = "";
	private String expDate = "";
	private String currStatus = "";
	private String currPlant = "";
	private String exceptionStatus = "";


	
	public String getExceptionStatus() {
		return exceptionStatus;
	}
	public void setExceptionStatus(String exceptionStatus) {
		this.exceptionStatus = exceptionStatus;
	}
	/**
	 * @return the lotNum
	 */
	public String getLotNum() {
		return lotNum;
	}
	/**
	 * @param lotNum the lotNum to set
	 */
	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the partDesc
	 */
	public String getPartDesc() {
		return partDesc;
	}
	/**
	 * @param partDesc the partDesc to set
	 */
	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}
	/**
	 * @return the partSize
	 */
	public String getPartSize() {
		return partSize;
	}
	/**
	 * @param partSize the partSize to set
	 */
	public void setPartSize(String partSize) {
		this.partSize = partSize;
	}
	/**
	 * @return the cpcClient
	 */
	public String getCpcClient() {
		return cpcClient;
	}
	/**
	 * @param cpcClient the cpcClient to set
	 */
	public void setCpcClient(String cpcClient) {
		this.cpcClient = cpcClient;
	}
	/**
	 * @return the donorNum
	 */
	public String getDonorNum() {
		return donorNum;
	}
	/**
	 * @param donorNum the donorNum to set
	 */
	public void setDonorNum(String donorNum) {
		this.donorNum = donorNum;
	}
	/**
	 * @return the donorAge
	 */
	public String getDonorAge() {
		return donorAge;
	}
	/**
	 * @param donorAge the donorAge to set
	 */
	public void setDonorAge(String donorAge) {
		this.donorAge = donorAge;
	}
	/**
	 * @return the donorSex
	 */
	public String getDonorSex() {
		return donorSex;
	}
	/**
	 * @param donorSex the donorSex to set
	 */
	public void setDonorSex(String donorSex) {
		this.donorSex = donorSex;
	}
	/**
	 * @return the intUse
	 */
	public String getIntUse() {
		return intUse;
	}
	/**
	 * @param intUse the intUse to set
	 */
	public void setIntUse(String intUse) {
		this.intUse = intUse;
	}
	/**
	 * @return the forResearch
	 */
	public String getForResearch() {
		return forResearch;
	}
	/**
	 * @param forResearch the forResearch to set
	 */
	public void setForResearch(String forResearch) {
		this.forResearch = forResearch;
	}
	/**
	 * @return the expDate
	 */
	public String getExpDate() {
		return expDate;
	}
	/**
	 * @param expDate the expDate to set
	 */
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	/**
	 * @return the currStatus
	 */
	public String getCurrStatus() {
		return currStatus;
	}
	/**
	 * @param currStatus the currStatus to set
	 */
	public void setCurrStatus(String currStatus) {
		this.currStatus = currStatus;
	}
	
	
	/**
	 * @return the currPlant
	 */
	public String getCurrPlant() {
		return currPlant;
	}
	/**
	 * @param currStatus the currStatus to set
	 */
	public void setCurrPlant(String currPlant) {
		this.currPlant = currPlant;
	}
	
	
	
	
}
