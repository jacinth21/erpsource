package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmVendorPricingSetupForm extends GmCommonForm {

  private String projectId = "";
  private String vendorId = "";
  private Boolean showInactiveVendor = null;
  private String partNumber = "";
  private String search = "";
  private String showSubComponentFl = "";
  private String locked = "";
  private String active = "";
  private String submitAccFl = "";
  private String xmlString = "";
  private String inputString = "";
  private String msgFl = "";
  private int maxCount;
  private String companyId;
  private ArrayList alProjectList = new ArrayList();
  private ArrayList alAllVendorList = new ArrayList();
  private ArrayList alUOMList = new ArrayList();
  private ArrayList alPartsList = new ArrayList();

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public String getVendorId() {
    return vendorId;
  }

  public void setVendorId(String vendorId) {
    this.vendorId = vendorId;
  }

  public Boolean getShowInactiveVendor() {
    return showInactiveVendor;
  }

  public void setShowInactiveVendor(Boolean showInactiveVendor) {
    this.showInactiveVendor = showInactiveVendor;
  }

  public String getPartNumber() {
    return partNumber;
  }

  public void setPartNumber(String partNumber) {
    this.partNumber = partNumber;
  }

  public String getSearch() {
    return search;
  }

  public void setSearch(String search) {
    this.search = search;
  }

  public String getShowSubComponentFl() {
    return showSubComponentFl;
  }

  public void setShowSubComponentFl(String showSubComponentFl) {
    this.showSubComponentFl = showSubComponentFl;
  }

  public String getLocked() {
    return locked;
  }

  public void setLocked(String locked) {
    this.locked = locked;
  }

  public String getActive() {
    return active;
  }

  public void setActive(String active) {
    this.active = active;
  }

  public String getSubmitAccFl() {
    return submitAccFl;
  }

  public void setSubmitAccFl(String submitAccFl) {
    this.submitAccFl = submitAccFl;
  }

  public String getXmlString() {
    return xmlString;
  }

  public void setXmlString(String xmlString) {
    this.xmlString = xmlString;
  }

  public String getInputString() {
    return inputString;
  }

  public void setInputString(String inputString) {
    this.inputString = inputString;
  }
  
  public String getCompanyId() {
		return companyId;
  }

  public void setCompanyId(String companyId) {
		this.companyId = companyId;
  }
  
  public ArrayList getAlProjectList() {
    return alProjectList;
  }

  public void setAlProjectList(ArrayList alProjectList) {
    this.alProjectList = alProjectList;
  }

  public ArrayList getAlAllVendorList() {
    return alAllVendorList;
  }

  public void setAlAllVendorList(ArrayList alAllVendorList) {
    this.alAllVendorList = alAllVendorList;
  }

  public ArrayList getAlUOMList() {
    return alUOMList;
  }

  public void setAlUOMList(ArrayList alUOMList) {
    this.alUOMList = alUOMList;
  }

  public ArrayList getAlPartsList() {
    return alPartsList;
  }

  public void setAlPartsList(ArrayList alPartsList) {
    this.alPartsList = alPartsList;
  }

  public String getMsgFl() {
    return msgFl;
  }

  public void setMsgFl(String msgFl) {
    this.msgFl = msgFl;
  }

	
  public void setMaxCount(int maxCount) { 
	this.maxCount = maxCount; 
  }
	 
  public int getMaxCount() {
	return maxCount; 
  }
	
}
