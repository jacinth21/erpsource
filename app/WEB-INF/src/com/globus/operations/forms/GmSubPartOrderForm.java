

package com.globus.operations.forms;

import com.globus.common.forms.GmCommonForm;
import java.util.ArrayList;
import java.util.List;

public class GmSubPartOrderForm extends GmCommonForm 
{
   private String pnum = "";
   private ArrayList projectList = new ArrayList();
   private List returnList = new ArrayList();  
   
   private String projectListID = "";
   private String hinputStr = "";
   private String displayOrderParts = "";
   private String displaySubParts = "on";
   private String displayParentParts = "on";
   
   private String hsubpartNotcheck = "";
   private String hparentpartNotcheck = "";
 
public String getHinputStr() {
	return hinputStr;
}

public void setHinputStr(String hinputStr) {
	this.hinputStr = hinputStr;
}

public String getPnum() {
	return pnum;
}

public void setPnum(String pnum) {
	this.pnum = pnum;
}

public ArrayList getProjectList() {
	return projectList;
}

public void setProjectList(ArrayList projectList) {
	this.projectList = projectList;
}
 

public String getProjectListID() {
	return projectListID;
}

public void setProjectListID(String projectListID) {
	this.projectListID = projectListID;
}

public List getReturnList() {
	return returnList;
}

public void setReturnList(List returnList) {
	this.returnList = returnList;
}

public String getDisplayOrderParts() {
	return displayOrderParts;
}

public void setDisplayOrderParts(String displayOrderParts) {
	this.displayOrderParts = displayOrderParts;
}

public String getDisplaySubParts() {
	return displaySubParts;
}

public void setDisplaySubParts(String displaySubParts) {
	this.displaySubParts = displaySubParts;
}

public String getDisplayParentParts() {
	return displayParentParts;
}

public void setDisplayParentParts(String displayParentParts) {
	this.displayParentParts = displayParentParts;
}

public String getHsubpartNotcheck() {
	return hsubpartNotcheck;
}

public void setHsubpartNotcheck(String hsubpartNotcheck) {
	this.hsubpartNotcheck = hsubpartNotcheck;
}

public String getHparentpartNotcheck() {
	return hparentpartNotcheck;
}

public void setHparentpartNotcheck(String hparentpartNotcheck) {
	this.hparentpartNotcheck = hparentpartNotcheck;
} 
 
}
