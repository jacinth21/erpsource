package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmVendorPOReportForm extends GmCommonForm {

	private String hpostring = "";
	private String vendid ="";
	private String poid ="";  
	private String frmdt =""; 
	private String todt =""; 
	private String potype ="";
	private String empid =""; 
	private String status =""; 
	private String cbo_Action ="";	 
	private String poamt =""; 
	private String strGridXmlData = "";
	private String strHoldFL = "";
	
	private ArrayList alVendorList=null;
	private ArrayList alUsers = null;
	private ArrayList alTypes = null;
	private ArrayList alPOAction = null;
	private ArrayList alProject = null;
	private ArrayList alPODetails = null;
	private ArrayList alStatus = null;
	/**
	 * @return the hpostring
	 */
	public String getHpostring() {
		return hpostring;
	}
	/**
	 * @param hpostring the hpostring to set
	 */
	public void setHpostring(String hpostring) {
		this.hpostring = hpostring;
	}
	/**
	 * @return the vendid
	 */
	public String getVendid() {
		return vendid;
	}
	/**
	 * @param vendid the vendid to set
	 */
	public void setVendid(String vendid) {
		this.vendid = vendid;
	}
	/**
	 * @return the poid
	 */
	public String getPoid() {
		return poid;
	}
	/**
	 * @param poid the poid to set
	 */
	public void setPoid(String poid) {
		this.poid = poid;
	}
	/**
	 * @return the frmdt
	 */
	public String getFrmdt() {
		return frmdt;
	}
	/**
	 * @param frmdt the frmdt to set
	 */
	public void setFrmdt(String frmdt) {
		this.frmdt = frmdt;
	}
	/**
	 * @return the todt
	 */
	public String getTodt() {
		return todt;
	}
	/**
	 * @param todt the todt to set
	 */
	public void setTodt(String todt) {
		this.todt = todt;
	}
	/**
	 * @return the potype
	 */
	public String getPotype() {
		return potype;
	}
	/**
	 * @param potype the potype to set
	 */
	public void setPotype(String potype) {
		this.potype = potype;
	}
	/**
	 * @return the empid
	 */
	public String getEmpid() {
		return empid;
	}
	/**
	 * @param empid the empid to set
	 */
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the cbo_Action
	 */
	public String getCbo_Action() {
		return cbo_Action;
	}
	/**
	 * @param cbo_Action the cbo_Action to set
	 */
	public void setCbo_Action(String cbo_Action) {
		this.cbo_Action = cbo_Action;
	}
	/**
	 * @return the poamt
	 */
	public String getPoamt() {
		return poamt;
	}
	/**
	 * @param poamt the poamt to set
	 */
	public void setPoamt(String poamt) {
		this.poamt = poamt;
	}
	/**
	 * @return the strGridXmlData
	 */
	public String getStrGridXmlData() {
		return strGridXmlData;
	}
	/**
	 * @param strGridXmlData the strGridXmlData to set
	 */
	public void setStrGridXmlData(String strGridXmlData) {
		this.strGridXmlData = strGridXmlData;
	}
	
	/**
	 * @return the strHoldFL
	 */
	public String getStrHoldFL() {
		return strHoldFL;
	}
	/**
	 * @param strHoldFL the strHoldFL to set
	 */
	public void setStrHoldFL(String strHoldFL) {
		this.strHoldFL = strHoldFL;
	}
	/**
	 * @return the alVendorList
	 */
	public ArrayList getAlVendorList() {
		return alVendorList;
	}
	/**
	 * @param alVendorList the alVendorList to set
	 */
	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}
	/**
	 * @return the alUsers
	 */
	public ArrayList getAlUsers() {
		return alUsers;
	}
	/**
	 * @param alUsers the alUsers to set
	 */
	public void setAlUsers(ArrayList alUsers) {
		this.alUsers = alUsers;
	}
	/**
	 * @return the alTypes
	 */
	public ArrayList getAlTypes() {
		return alTypes;
	}
	/**
	 * @param alTypes the alTypes to set
	 */
	public void setAlTypes(ArrayList alTypes) {
		this.alTypes = alTypes;
	}
	/**
	 * @return the alPOAction
	 */
	public ArrayList getAlPOAction() {
		return alPOAction;
	}
	/**
	 * @param alPOAction the alPOAction to set
	 */
	public void setAlPOAction(ArrayList alPOAction) {
		this.alPOAction = alPOAction;
	}
	/**
	 * @return the alProject
	 */
	public ArrayList getAlProject() {
		return alProject;
	}
	/**
	 * @param alProject the alProject to set
	 */
	public void setAlProject(ArrayList alProject) {
		this.alProject = alProject;
	}
	/**
	 * @return the alPODetails
	 */
	public ArrayList getAlPODetails() {
		return alPODetails;
	}
	/**
	 * @param alPODetails the alPODetails to set
	 */
	public void setAlPODetails(ArrayList alPODetails) {
		this.alPODetails = alPODetails;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	

	
}