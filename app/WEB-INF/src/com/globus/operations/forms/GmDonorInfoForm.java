/**
 * 
 */
package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author arajan
 * 
 */
public class GmDonorInfoForm extends GmCommonForm {

  private String partNum = "";
  private String qtyRec = "";
  private String donorNo = "";
  private String expDate = "";
  private String age = "";
  private String sex = "";
  private String internationalUse = "";
  private String prefix = "";
  private String serialNum = "";
  private String suffix = "";
  private String scan = "";
  private String hInputString = "";
  private String hDhrId = "";
  private String hVendorId = "";
  private String hMode = "";
  private String strGridData = "";
  private String sexCodeName = "";
  private String internationalUsenm = "";
  private String screenType = "";
  private String forResearch = "";
  private String forResearchnm = "";
  private String qtypack = "";
  private String dhrstatus = "";
  private String hDonorNum = "";
  private String strRsBulkIdCnt = "";
  private String ncmrStatus = "";
  private String strBtnDisable = "";
  private String strMsg = "";
  private String rejQty = "";
  private String totalqty = "";
  private String submitAccess = "";
  private String hAccess = "";
  private String matType = "";
  private String serialNumFl = "";

  private ArrayList alGender = new ArrayList();
  private ArrayList alInternationalUse = new ArrayList();
  private ArrayList alCtrlDtls = new ArrayList();
  private ArrayList alTxnType = new ArrayList();
  private ArrayList alDHRSplitdtl = new ArrayList();
  private ArrayList alResearch = new ArrayList();


  public String getStrGridData() {
    return strGridData;
  }

  public void setStrGridData(String strGridData) {
    this.strGridData = strGridData;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the qtyRec
   */
  public String getQtyRec() {
    return qtyRec;
  }

  /**
   * @param qtyRec the qtyRec to set
   */
  public void setQtyRec(String qtyRec) {
    this.qtyRec = qtyRec;
  }

  /**
   * @return the donorNo
   */
  public String getDonorNo() {
    return donorNo;
  }

  /**
   * @param donorNo the donorNo to set
   */
  public void setDonorNo(String donorNo) {
    this.donorNo = donorNo;
  }

  /**
   * @return the expDate
   */
  public String getExpDate() {
    return expDate;
  }

  /**
   * @param expDate the expDate to set
   */
  public void setExpDate(String expDate) {
    this.expDate = expDate;
  }

  /**
   * @return the age
   */
  public String getAge() {
    return age;
  }

  /**
   * @param age the age to set
   */
  public void setAge(String age) {
    this.age = age;
  }

  /**
   * @return the sex
   */
  public String getSex() {
    return sex;
  }

  /**
   * @param sex the sex to set
   */
  public void setSex(String sex) {
    this.sex = sex;
  }

  /**
   * @return the internationalUse
   */
  public String getInternationalUse() {
    return internationalUse;
  }

  /**
   * @param internationalUse the internationalUse to set
   */
  public void setInternationalUse(String internationalUse) {
    this.internationalUse = internationalUse;
  }

  /**
   * @return the alGender
   */
  public ArrayList getAlGender() {
    return alGender;
  }

  /**
   * @param alGender the alGender to set
   */
  public void setAlGender(ArrayList alGender) {
    this.alGender = alGender;
  }

  /**
   * @return the prefix
   */
  public String getPrefix() {
    return prefix;
  }

  /**
   * @param prefix the prefix to set
   */
  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  /**
   * @return the serialNum
   */
  public String getSerialNum() {
    return serialNum;
  }

  /**
   * @param serialNum the serialNum to set
   */
  public void setSerialNum(String serialNum) {
    this.serialNum = serialNum;
  }

  /**
   * @return the suffix
   */
  public String getSuffix() {
    return suffix;
  }

  /**
   * @param suffix the suffix to set
   */
  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  /**
   * @return the scan
   */
  public String getScan() {
    return scan;
  }

  /**
   * @param scan the scan to set
   */
  public void setScan(String scan) {
    this.scan = scan;
  }

  /**
   * @return the alInternationalUse
   */
  public ArrayList getAlInternationalUse() {
    return alInternationalUse;
  }

  /**
   * @param alInternationalUse the alInternationalUse to set
   */
  public void setAlInternationalUse(ArrayList alInternationalUse) {
    this.alInternationalUse = alInternationalUse;
  }

  /**
   * @return the hInputString
   */
  public String gethInputString() {
    return hInputString;
  }

  /**
   * @param hInputString the hInputString to set
   */
  public void sethInputString(String hInputString) {
    this.hInputString = hInputString;
  }

  /**
   * @return the hDhrId
   */
  public String gethDhrId() {
    return hDhrId;
  }

  /**
   * @param hDhrId the hDhrId to set
   */
  public void sethDhrId(String hDhrId) {
    this.hDhrId = hDhrId;
  }

  /**
   * @return the hVendorId
   */
  public String gethVendorId() {
    return hVendorId;
  }

  /**
   * @param hVendorId the hVendorId to set
   */
  public void sethVendorId(String hVendorId) {
    this.hVendorId = hVendorId;
  }

  /**
   * @return the hMode
   */
  public String gethMode() {
    return hMode;
  }

  /**
   * @param hMode the hMode to set
   */
  public void sethMode(String hMode) {
    this.hMode = hMode;
  }

  /**
   * @return the alCtrlDtls
   */
  public ArrayList getAlCtrlDtls() {
    return alCtrlDtls;
  }

  /**
   * @param alCtrlDtls the alCtrlDtls to set
   */
  public void setAlCtrlDtls(ArrayList alCtrlDtls) {
    this.alCtrlDtls = alCtrlDtls;
  }

  /**
   * @return the sexCodeName
   */
  public String getSexCodeName() {
    return sexCodeName;
  }

  /**
   * @param sexCodeName the sexCodeName to set
   */
  public void setSexCodeName(String sexCodeName) {
    this.sexCodeName = sexCodeName;
  }

  /**
   * @return the internationalUsenm
   */
  public String getInternationalUsenm() {
    return internationalUsenm;
  }

  /**
   * @param internationalUsenm the internationalUsenm to set
   */
  public void setInternationalUsenm(String internationalUsenm) {
    this.internationalUsenm = internationalUsenm;
  }

  /**
   * @return the screenType
   */
  public String getScreenType() {
    return screenType;
  }

  /**
   * @param screenType the screenType to set
   */
  public void setScreenType(String screenType) {
    this.screenType = screenType;
  }

  /**
   * @return the alTxnType
   */
  public ArrayList getAlTxnType() {
    return alTxnType;
  }

  /**
   * @param alTxnType the alTxnType to set
   */
  public void setAlTxnType(ArrayList alTxnType) {
    this.alTxnType = alTxnType;
  }

  /**
   * @return the alDHRSplitdtl
   */
  public ArrayList getAlDHRSplitdtl() {
    return alDHRSplitdtl;
  }

  /**
   * @param alDHRSplitdtl the alDHRSplitdtl to set
   */
  public void setAlDHRSplitdtl(ArrayList alDHRSplitdtl) {
    this.alDHRSplitdtl = alDHRSplitdtl;
  }

  /**
   * @return the forResearch
   */
  public String getForResearch() {
    return forResearch;
  }

  /**
   * @param forResearch the forResearch to set
   */
  public void setForResearch(String forResearch) {
    this.forResearch = forResearch;
  }

  /**
   * @return the alResearch
   */
  public ArrayList getAlResearch() {
    return alResearch;
  }

  /**
   * @param alResearch the alResearch to set
   */
  public void setAlResearch(ArrayList alResearch) {
    this.alResearch = alResearch;
  }

  /**
   * @return the forResearchnm
   */
  public String getForResearchnm() {
    return forResearchnm;
  }

  /**
   * @param forResearchnm the forResearchnm to set
   */
  public void setForResearchnm(String forResearchnm) {
    this.forResearchnm = forResearchnm;
  }

  /**
   * @return the qtypack
   */
  public String getQtypack() {
    return qtypack;
  }

  /**
   * @param qtypack the qtypack to set
   */
  public void setQtypack(String qtypack) {
    this.qtypack = qtypack;
  }

  /**
   * @return the dhrstatus
   */
  public String getDhrstatus() {
    return dhrstatus;
  }

  /**
   * @param dhrstatus the dhrstatus to set
   */
  public void setDhrstatus(String dhrstatus) {
    this.dhrstatus = dhrstatus;
  }

  /**
   * @return the hDonorNum
   */
  public String gethDonorNum() {
    return hDonorNum;
  }

  /**
   * @param hDonorNum the hDonorNum to set
   */
  public void sethDonorNum(String hDonorNum) {
    this.hDonorNum = hDonorNum;
  }

  /**
   * @return the strRsBulkIdCnt
   */
  public String getStrRsBulkIdCnt() {
    return strRsBulkIdCnt;
  }

  /**
   * @param strRsBulkIdCnt the strRsBulkIdCnt to set
   */
  public void setStrRsBulkIdCnt(String strRsBulkIdCnt) {
    this.strRsBulkIdCnt = strRsBulkIdCnt;
  }

  /**
   * @return the ncmrStatus
   */
  public String getNcmrStatus() {
    return ncmrStatus;
  }

  /**
   * @param ncmrStatus the ncmrStatus to set
   */
  public void setNcmrStatus(String ncmrStatus) {
    this.ncmrStatus = ncmrStatus;
  }

  /**
   * @return the strBtnDisable
   */
  public String getStrBtnDisable() {
    return strBtnDisable;
  }

  /**
   * @param strBtnDisable the strBtnDisable to set
   */
  public void setStrBtnDisable(String strBtnDisable) {
    this.strBtnDisable = strBtnDisable;
  }

  /**
   * @return the strMsg
   */
  public String getStrMsg() {
    return strMsg;
  }

  /**
   * @param strMsg the strMsg to set
   */
  public void setStrMsg(String strMsg) {
    this.strMsg = strMsg;
  }

  /**
   * @return the rejQty
   */
  public String getRejQty() {
    return rejQty;
  }

  /**
   * @param rejQty the rejQty to set
   */
  public void setRejQty(String rejQty) {
    this.rejQty = rejQty;
  }

  /**
   * @return the totalqty
   */
  public String getTotalqty() {
    return totalqty;
  }

  /**
   * @param totalqty the totalqty to set
   */
  public void setTotalqty(String totalqty) {
    this.totalqty = totalqty;
  }

  /**
   * @return the submitAccess
   */
  public String getSubmitAccess() {
    return submitAccess;
  }

  /**
   * @param submitAccess the submitAccess to set
   */
  public void setSubmitAccess(String submitAccess) {
    this.submitAccess = submitAccess;
  }

  /**
   * @return the hAccess
   */
  public String gethAccess() {
    return hAccess;
  }

  /**
   * @param hAccess the hAccess to set
   */
  public void sethAccess(String hAccess) {
    this.hAccess = hAccess;
  }

  public String getMatType() {
    return matType;
  }

  public void setMatType(String matType) {
    this.matType = matType;
  }

  public String getSerialNumFl() {
    return serialNumFl;
  }

  public void setSerialNumFl(String serialNumFl) {
    this.serialNumFl = serialNumFl;
  }

}
