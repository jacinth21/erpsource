package com.globus.operations.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmPartConversionSummaryForm extends GmCommonForm{
	
	private String pcID = "";
	private String typeId = "";
	private String fromPartNumber = "";
	private String toPartNumber = "";
	private String fromDate = "";
	private String toDate = "";
	
	private List ldtResult = new ArrayList(); //Stores report output table.
	private ArrayList alType = new ArrayList(); //Inventory Type dropdown
	
	/**
	 * @return the pcID
	 */
	public String getPcID() {
		return pcID;
	}

	/**
	 * @param pcID the pcID to set
	 */
	public void setPcID(String pcID) {
		this.pcID = pcID;
	}

	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the fromPartNumber
	 */
	public String getFromPartNumber() {
		return fromPartNumber;
	}

	/**
	 * @param fromPartNumber the fromPartNumber to set
	 */
	public void setFromPartNumber(String fromPartNumber) {
		this.fromPartNumber = fromPartNumber;
	}

	/**
	 * @return the toPartNumber
	 */
	public String getToPartNumber() {
		return toPartNumber;
	}

	/**
	 * @param toPartNumber the toPartNumber to set
	 */
	public void setToPartNumber(String toPartNumber) {
		this.toPartNumber = toPartNumber;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}

	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}

	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}

}
