package com.globus.operations.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmPartAttributeSetupForm extends GmLogForm {

  private String partnumber = "";
  private String attributetype = "101262";
  private String attributevalue = "80130";
  private String mriSafetyInformation = "";
  private String sizefmt = "";
  private String hLabelAttribute = "";
  private String logval = "";
  private String editDisable = "";
  private String cpcID = "";
  private String proSpecType = "";


  ArrayList alAttrType = new ArrayList();
  ArrayList alAttrVal = new ArrayList();
  ArrayList alMriSafetyInfo = new ArrayList();
  ArrayList alLblFormat = new ArrayList();
  ArrayList alCPCList = new ArrayList();
  ArrayList alProSpecType = new ArrayList();
  HashMap hmAttrVal = new HashMap();
  
  /**
 * @return the cpcID
 */
public String getCpcID() {
	return cpcID;
}

/**
 * @param cpcID the cpcID to set
 */
public void setCpcID(String cpcID) {
	this.cpcID = cpcID;
}

/**
 * @return the alCPCList
 */
public ArrayList getAlCPCList() {
	return alCPCList;
}

/**
 * @param alCPCList the alCPCList to set
 */
public void setAlCPCList(ArrayList alCPCList) {
	this.alCPCList = alCPCList;
}

/**
   * @return the partnumber
   */
  public String getPartnumber() {
    return partnumber;
  }

  /**
   * @param partnumber the partnumber to set
   */
  public void setPartnumber(String partnumber) {
    this.partnumber = partnumber;
  }

  /**
   * @return the attributetype
   */
  public String getAttributetype() {
    return attributetype;
  }

  /**
   * @param attributetype the attributetype to set
   */
  public void setAttributetype(String attributetype) {
    this.attributetype = attributetype;
  }

  /**
   * @return the attributevalue
   */
  public String getAttributevalue() {
    return attributevalue;
  }

  /**
   * @param attributevalue the attributevalue to set
   */
  public void setAttributevalue(String attributevalue) {
    this.attributevalue = attributevalue;
  }

  /**
   * @return the mriSafetyInformation
   */
  public String getMriSafetyInformation() {
    return mriSafetyInformation;
  }

  /**
   * @param mriSafetyInformation the mriSafetyInformation to set
   */
  public void setMriSafetyInformation(String mriSafetyInformation) {
    this.mriSafetyInformation = mriSafetyInformation;
  }

  /**
   * @return the alMriSafetyInfo
   */
  public ArrayList getAlMriSafetyInfo() {
    return alMriSafetyInfo;
  }

  /**
   * @param alMriSafetyInfo the alMriSafetyInfo to set
   */
  public void setAlMriSafetyInfo(ArrayList alMriSafetyInfo) {
    this.alMriSafetyInfo = alMriSafetyInfo;
  }

  /**
   * @return the alAttrType
   */
  public ArrayList getAlAttrType() {
    return alAttrType;
  }

  /**
   * @param alAttrType the alAttrType to set
   */
  public void setAlAttrType(ArrayList alAttrType) {
    this.alAttrType = alAttrType;
  }

  /**
   * @return the alAttrVal
   */
  public ArrayList getAlAttrVal() {
    return alAttrVal;
  }

  /**
   * @param alAttrVal the alAttrVal to set
   */
  public void setAlAttrVal(ArrayList alAttrVal) {
    this.alAttrVal = alAttrVal;
  }

  /**
   * @return the hmAttrVal
   */
  public HashMap getHmAttrVal() {
    return hmAttrVal;
  }

  /**
   * @param hmAttrVal the hmAttrVal to set
   */
  public void setHmAttrVal(HashMap hmAttrVal) {
    this.hmAttrVal = hmAttrVal;
  }

  /**
   * @return the sizefmt
   */
  public String getSizefmt() {
    return sizefmt;
  }

  /**
   * @param sizefmt the sizefmt to set
   */
  public void setSizefmt(String sizefmt) {
    this.sizefmt = sizefmt;
  }

  /**
   * @return the alLblFormat
   */
  public ArrayList getAlLblFormat() {
    return alLblFormat;
  }

  /**
   * @param alLblFormat the alLblFormat to set
   */
  public void setAlLblFormat(ArrayList alLblFormat) {
    this.alLblFormat = alLblFormat;
  }

  /**
   * @return the hLabelAttribute
   */
  public String gethLabelAttribute() {
    return hLabelAttribute;
  }

  /**
   * @param hLabelAttribute the hLabelAttribute to set
   */
  public void sethLabelAttribute(String hLabelAttribute) {
    this.hLabelAttribute = hLabelAttribute;
  }

  /**
   * @return the logval
   */
  public String getLogval() {
    return logval;
  }

  /**
   * @param logval the logval to set
   */
  public void setLogval(String logval) {
    this.logval = logval;
  }

  /**
   * @return the editDisable
   */
  public String getEditDisable() {
    return editDisable;
  }

  /**
   * @param editDisable the editDisable to set
   */
  public void setEditDisable(String editDisable) {
    this.editDisable = editDisable;
  }

	/**
	 * @return the proSpecType
	 */
	public String getProSpecType() {
		return proSpecType;
	}
	
	/**
	 * @param proSpecType the proSpecType to set
	 */
	public void setProSpecType(String proSpecType) {
		this.proSpecType = proSpecType;
	}

	/**
	 * @return the alProSpecType
	 */
	public ArrayList getAlProSpecType() {
		return alProSpecType;
	}

	/**
	 * @param alProSpecType the alProSpecType to set
	 */
	public void setAlProSpecType(ArrayList alProSpecType) {
		this.alProSpecType = alProSpecType;
	}
	
	
  
}
