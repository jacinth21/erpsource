package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmVendorPortalMapForm  extends GmCommonForm{
	private String partyId;
	private String partylinkid;
	private String fromId;
	private String toPartyID = "";
	private String gridData="";
	private String fromVendorId = "";

	ArrayList alVendorMapList = new ArrayList();
	/**
	 * @return the partyId
	 */
	public String getPartyId() {
		return partyId;
	}
	/**
	 * @param partyId the partyId to set
	 */
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	
	/**
	 * @return the fromId
	 */
	public String getFromId() {
		return fromId;
	}
	/**
	 * @param fromId the fromId to set
	 */
	public void setFromId(String fromId) {
		this.fromId = fromId;
	}
	/**
	 * @return the partylinkid
	 */
	public String getPartylinkid() {
		return partylinkid;
	}
	/**
	 * @param partylinkid the partylinkid to set
	 */
	public void setPartylinkid(String partylinkid) {
		this.partylinkid = partylinkid;
	}
	/**
	 * @return the alVendorMapList
	 */
	public ArrayList getAlVendorMapList() {
		return alVendorMapList;
	}
	/**
	 * @param alVendorMapList the alVendorMapList to set
	 */
	public void setAlVendorMapList(ArrayList alVendorMapList) {
		this.alVendorMapList = alVendorMapList;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}	


	/**
	 * @return the toPartyID
	 */
	public String getToPartyID() {
		return toPartyID;
	}
	/**
	 * @param toPartyID the toPartyID to set
	 */
	public void setToPartyID(String toPartyID) {
		this.toPartyID = toPartyID;
	}
	/**
	 * @return the fromVendorId
	 */
	public String getFromVendorId() {
		return fromVendorId;
	}
	/**
	 * @param fromVendorId the fromVendorId to set
	 */
	public void setFromVendorId(String fromVendorId) {
		this.fromVendorId = fromVendorId;
	}
	
}
