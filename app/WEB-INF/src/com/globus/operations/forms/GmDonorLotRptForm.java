package com.globus.operations.forms;

import com.globus.common.forms.GmLogForm;

public class GmDonorLotRptForm extends GmLogForm {
	private String donorId = "";
	private String gridXmlData = "";
	/**
	 * @return the donorId
	 */
	public String getDonorId() {
		return donorId;
	}
	/**
	 * @param donorId the donorId to set
	 */
	public void setDonorId(String donorId) {
		this.donorId = donorId;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}	
}
