/**
 * FileName    : GmModifyControlNumberForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Apr 20, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author sthadeshwar
 *
 */
public class GmModifyControlNumberForm extends GmCommonForm {

	private String refId = "";
	private String source = "";
	private String url = "";
	private ArrayList alSource = new ArrayList();
	
	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}
	/**
	 * @param refId the refId to set
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the alSource
	 */
	public ArrayList getAlSource() {
		return alSource;
	}
	/**
	 * @param alSource the alSource to set
	 */
	public void setAlSource(ArrayList alSource) {
		this.alSource = alSource;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
