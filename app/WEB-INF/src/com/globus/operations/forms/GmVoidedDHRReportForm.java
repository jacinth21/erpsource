package com.globus.operations.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmVoidedDHRReportForm extends GmCommonForm {

	


private String dhrID = "";
private String poID = "";
private String invID = "";
private String pnum = "";
private String fromDate = "";
private String toDate = "";
private String vendorID = "";
private String xmlStringData = "";

private ArrayList alVendorName = new ArrayList();

public String getDhrID() {
	return dhrID;
}

public void setDhrID(String dhrID) {
	this.dhrID = dhrID;
}

public String getPoID() {
	return poID;
}

public void setPoID(String poID) {
	this.poID = poID;
}

public String getInvID() {
	return invID;
}

public void setInvID(String invID) {
	this.invID = invID;
}

public String getPnum() {
	return pnum;
}

public void setPnum(String pnum) {
	this.pnum = pnum;
}

public String getFromDate() {
	return fromDate;
}

public void setFromDate(String fromDate) {
	this.fromDate = fromDate;
}

public String getToDate() {
	return toDate;
}

public void setToDate(String toDate) {
	this.toDate = toDate;
}

public String getVendorID() {
	return vendorID;
}

public void setVendorID(String vendorID) {
	this.vendorID = vendorID;
}

public ArrayList getAlVendorName() {
	return alVendorName;
}

public void setAlVendorName(ArrayList alVendorName) {
	this.alVendorName = alVendorName;
}


public String getXmlStringData() {
	return xmlStringData;
}

public void setXmlStringData(String xmlStringData) {
	this.xmlStringData = xmlStringData;
}
	




}