package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.formbeans.GmCommonForm;
import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 


public class GmVendorSetupForm extends GmCommonForm
{
    private static final long serialVersionUID = 1L;
    
    private ArrayList alVendorList = new ArrayList();
    private ArrayList alVendorType = new ArrayList();
    private ArrayList alVendorCategory = new ArrayList();
    
    private String  vendorId         = "";
    private String  vendorType       = "";
    private String  vendorCategory   = "";
    private String  inActiveFlag     = "";
    private String  hvendCatMapId    = "";
    private String strAccessfl="";    /*PMT-26730-->System Manager Changes*/
    /**
     * @return Returns the alVendorCategory.
     */
    
    public GmVendorSetupForm()
    {
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
        log.debug(" vales inside form " + this.toString());
    }
    
    public String toString()
    {
        return " vendor id  " + getVendorId() + " vendortype " + getVendorType() + " vend cat "+ getVendorCategory() + " hvendcatmapid " + getHvendCatMapId();
    }
    
    public ArrayList getAlVendorCategory()
    {
        return alVendorCategory;
    }
    /**
     * @param alVendorCategory The alVendorCategory to set.
     */
    public void setAlVendorCategory(ArrayList alVendorCategory)
    {
        this.alVendorCategory = alVendorCategory;
    }
    /**
     * @return Returns the alVendorList.
     */
    public ArrayList getAlVendorList()
    {
        return alVendorList;
    }
    /**
     * @param alVendorList The alVendorList to set.
     */
    public void setAlVendorList(ArrayList alVendorList)
    {
        this.alVendorList = alVendorList;
    }
    /**
     * @return Returns the alVendorType.
     */
    public ArrayList getAlVendorType()
    {
        return alVendorType;
    }
    /**
     * @param alVendorType The alVendorType to set.
     */
    public void setAlVendorType(ArrayList alVendorType)
    {
        this.alVendorType = alVendorType;
    }
    /**
     * @return Returns the vendorCategory.
     */
    public String getVendorCategory()
    {
        return vendorCategory;
    }
    /**
     * @param vendorCategory The vendorCategory to set.
     */
    public void setVendorCategory(String vendorCategory)
    {
        this.vendorCategory = vendorCategory;
    }
    /**
     * @return Returns the vendorId.
     */
    public String getVendorId()
    {
        return vendorId;
    }
    /**
     * @param vendorId The vendorId to set.
     */
    public void setVendorId(String vendorId)
    {
        this.vendorId = vendorId;
    }
    /**
     * @return Returns the vendorType.
     */
    public String getVendorType()
    {
        return vendorType;
    }
    /**
     * @param vendorType The vendorType to set.
     */
    public void setVendorType(String vendorType)
    {
        this.vendorType = vendorType;
    }

    /**
     * @return Returns the inActiveFlag.
     */
    public String getInActiveFlag()
    {
        return inActiveFlag;
    }

    /**
     * @param inActiveFlag The inActiveFlag to set.
     */
    public void setInActiveFlag(String inActiveFlag)
    {
        this.inActiveFlag = inActiveFlag;
    }

    /**
     * @return Returns the hvendCatMapId.
     */
    public String getHvendCatMapId()
    {
        return hvendCatMapId;
    }

    /**
     * @param hvendCatMapId The hvendCatMapId to set.
     */
    public void setHvendCatMapId(String hvendCatMapId)
    {
        this.hvendCatMapId = hvendCatMapId;
    }

	/**
	 * @return the strAccessfl
	 */
	public String getStrAccessfl() {
		return strAccessfl;
	}

	/**
	 * @param strAccessfl the strAccessfl to set
	 */
	public void setStrAccessfl(String strAccessfl) {
		this.strAccessfl = strAccessfl;
	}
   
}
