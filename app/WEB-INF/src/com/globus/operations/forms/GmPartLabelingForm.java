package com.globus.operations.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmPartLabelingForm extends GmLogForm {
  private String txnid = "";
  private String txntypenm = "";
  private String purpose = "";
  private String refid = "";
  private String setname = "";
  private String cname = "";
  private String comments = "";
  private Date cdate = new Date();
  private String accessFlg = "";
  private String QCAccessFlg = "";
  private String QC = "";
  private String labels = "";

  private String txntype = "";
  private String loctype = "";
  private String statusfl = "";
  private String haction = "";
  private String apprrejdrp = "";
  private String hApprString = "";
  private String hRejString = "";
  private String pnqnTxnId = "";
  private String action = "";
  private String gridXmlData = "";

  private String strProcessAccess = "";
  private String strVerifyAccess = "";
  private String preStagingFlag = "";
  private String stagingFlag = "";
  private String labelprestage = "";
  private String labelstage = "";
  private String lbl_pre_stage = "";
  private String lbl_stage = "";
  /**
 * @return the preStagingFlag
 */
public String getPreStagingFlag() {
	return preStagingFlag;
}

/**
 * @param preStagingFlag the preStagingFlag to set
 */
public void setPreStagingFlag(String preStagingFlag) {
	this.preStagingFlag = preStagingFlag;
}

/**
 * @return the stagingFlag
 */
public String getStagingFlag() {
	return stagingFlag;
}

/**
 * @param stagingFlag the stagingFlag to set
 */
public void setStagingFlag(String stagingFlag) {
	this.stagingFlag = stagingFlag;
}

/**
 * @return the stagingFlg
 */
public String getLabelstage() {
	return labelstage;
}

/**
 * @param stagingFlg the stagingFlg to set
 */
public void setLabelstage(String labelstage) {
	this.labelstage = labelstage;
}

/**
 * @return the stagingFlg
 */
public String getLabelprestage() {
	return labelprestage;
}

/**
 * @param stagingFlg the stagingFlg to set
 */
public void setLabelprestage(String labelprestage) {
	this.labelprestage = labelprestage;
}


ArrayList aldetailinfo = new ArrayList();
  ArrayList alApprRej = new ArrayList();
  HashMap hmTransRules = new HashMap();

  
 

/**
   * @return the accessFlg
   */
  public String getAccessFlg() {
    return accessFlg;
  }

  /**
   * @param accessFlg the accessFlg to set
   */
  public void setAccessFlg(String accessFlg) {
    this.accessFlg = accessFlg;
  }

  /**
   * @return the qCAccessFlg
   */
  public String getQCAccessFlg() {
    return QCAccessFlg;
  }

  /**
   * @param qCAccessFlg the qCAccessFlg to set
   */
  public void setQCAccessFlg(String qCAccessFlg) {
    QCAccessFlg = qCAccessFlg;
  }

  /**
   * @return the qC
   */
  public String getQC() {
    return QC;
  }

  /**
   * @param qC the qC to set
   */
  public void setQC(String qC) {
    QC = qC;
  }

  /**
   * @return the labels
   */
  public String getLabels() {
    return labels;
  }

  /**
   * @param labels the labels to set
   */
  public void setLabels(String labels) {
    this.labels = labels;
  }

  /**
   * @return the txnid
   */
  public String getTxnid() {
    return txnid;
  }

  /**
   * @param txnid the txnid to set
   */
  public void setTxnid(String txnid) {
    this.txnid = txnid;
  }

  /**
   * @return the txntypenm
   */
  public String getTxntypenm() {
    return txntypenm;
  }

  /**
   * @param txntypenm the txntypenm to set
   */
  public void setTxntypenm(String txntypenm) {
    this.txntypenm = txntypenm;
  }

  /**
   * @return the purpose
   */
  public String getPurpose() {
    return purpose;
  }

  /**
   * @param purpose the purpose to set
   */
  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }

  /**
   * @return the refid
   */
  public String getRefid() {
    return refid;
  }

  /**
   * @param refid the refid to set
   */
  public void setRefid(String refid) {
    this.refid = refid;
  }

  /**
   * @return the setname
   */
  public String getSetname() {
    return setname;
  }

  /**
   * @param setname the setname to set
   */
  public void setSetname(String setname) {
    this.setname = setname;
  }

  /**
   * @return the cname
   */
  public String getCname() {
    return cname;
  }

  /**
   * @param cname the cname to set
   */
  public void setCname(String cname) {
    this.cname = cname;
  }

  /**
   * @return the comments
   */
  public String getComments() {
    return comments;
  }

  /**
   * @param comments the comments to set
   */
  public void setComments(String comments) {
    this.comments = comments;
  }

  /**
   * @return the txntype
   */
  public String getTxntype() {
    return txntype;
  }

  /**
   * @param txntype the txntype to set
   */
  public void setTxntype(String txntype) {
    this.txntype = txntype;
  }

  /**
   * @return the loctype
   */
  public String getLoctype() {
    return loctype;
  }

  /**
   * @param loctype the loctype to set
   */
  public void setLoctype(String loctype) {
    this.loctype = loctype;
  }

  /**
   * @return the cdate
   */
  public Date getCdate() {
    return cdate;
  }

  /**
   * @param cdate the cdate to set
   */
  public void setCdate(Date cdate) {
    this.cdate = cdate;
  }

  /**
   * @return the aldetailinfo
   */
  public ArrayList getAldetailinfo() {
    return aldetailinfo;
  }

  /**
   * @param aldetailinfo the aldetailinfo to set
   */
  public void setAldetailinfo(ArrayList aldetailinfo) {
    this.aldetailinfo = aldetailinfo;
  }

  /**
   * @return the statusfl
   */
  public String getStatusfl() {
    return statusfl;
  }

  /**
   * @param statusfl the statusfl to set
   */
  public void setStatusfl(String statusfl) {
    this.statusfl = statusfl;
  }

  /**
   * @return the hmTransRules
   */
  public HashMap getHmTransRules() {
    return hmTransRules;
  }

  /**
   * @param hmTransRules the hmTransRules to set
   */
  public void setHmTransRules(HashMap hmTransRules) {
    this.hmTransRules = hmTransRules;
  }

  /**
   * @return the haction
   */
  public String getHaction() {
    return haction;
  }

  /**
   * @param haction the haction to set
   */
  public void setHaction(String haction) {
    this.haction = haction;
  }

  /**
   * @return the apprrejdrp
   */
  public String getApprrejdrp() {
    return apprrejdrp;
  }

  /**
   * @param apprrejdrp the apprrejdrp to set
   */
  public void setApprrejdrp(String apprrejdrp) {
    this.apprrejdrp = apprrejdrp;
  }

  /**
   * @return the alApprRej
   */
  public ArrayList getAlApprRej() {
    return alApprRej;
  }

  /**
   * @param alApprRej the alApprRej to set
   */
  public void setAlApprRej(ArrayList alApprRej) {
    this.alApprRej = alApprRej;
  }

  /**
   * @return the hApprString
   */
  public String gethApprString() {
    return hApprString;
  }

  /**
   * @param hApprString the hApprString to set
   */
  public void sethApprString(String hApprString) {
    this.hApprString = hApprString;
  }

  /**
   * @return the hRejString
   */
  public String gethRejString() {
    return hRejString;
  }

  /**
   * @param hRejString the hRejString to set
   */
  public void sethRejString(String hRejString) {
    this.hRejString = hRejString;
  }

  /**
   * @return the pnqnTxnId
   */
  public String getPnqnTxnId() {
    return pnqnTxnId;
  }

  /**
   * @param pnqnTxnId the pnqnTxnId to set
   */
  public void setPnqnTxnId(String pnqnTxnId) {
    this.pnqnTxnId = pnqnTxnId;
  }

  /**
   * @return the action
   */
  public String getAction() {
    return action;
  }

  /**
   * @param action the action to set
   */
  public void setAction(String action) {
    this.action = action;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the strProcessAccess
   */
  public String getStrProcessAccess() {
    return strProcessAccess;
  }

  /**
   * @param strProcessAccess the strProcessAccess to set
   */
  public void setStrProcessAccess(String strProcessAccess) {
    this.strProcessAccess = strProcessAccess;
  }

  /**
   * @return the strVerifyAccess
   */
  public String getStrVerifyAccess() {
    return strVerifyAccess;
  }

  /**
   * @param strVerifyAccess the strVerifyAccess to set
   */
  public void setStrVerifyAccess(String strVerifyAccess) {
    this.strVerifyAccess = strVerifyAccess;
  }

/**
 * @return the lbl_pre_stage
 */
public String getLbl_pre_stage() {
	return lbl_pre_stage;
}

/**
 * @param lbl_pre_stage the lbl_pre_stage to set
 */
public void setLbl_pre_stage(String lbl_pre_stage) {
	this.lbl_pre_stage = lbl_pre_stage;
}

/**
 * @return the lbl_stage
 */
public String getLbl_stage() {
	return lbl_stage;
}

/**
 * @param lbl_stage the lbl_stage to set
 */
public void setLbl_stage(String lbl_stage) {
	this.lbl_stage = lbl_stage;
}
  
}
