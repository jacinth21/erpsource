package com.globus.operations.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Karthik
 */
public class GmInsertProcessForm extends GmCommonForm {

	private String txnID = "";
	private String accessFlag ="";
	private String strInputString = "";
	private String strVoidRowIds = "";
	private String gridXmlData = "";
	private String statusFlag ="";
	
	private ArrayList alInsertDetail = new ArrayList();

	
	/**
	 * @return the txnID
	 */
	public String getTxnID() {
		return txnID;
	}
	/**
	 * @param txnID the txnID to set
	 */
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}
	/**
	 * @return the accessFlag
	 */
	public String getAccessFlag() {
		return accessFlag;
	}
	/**
	 * @param accessFlag the accessFlag to set
	 */
	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}
	/**
	 * @return the strInputString
	 */
	public String getStrInputString() {
		return strInputString;
	}
	/**
	 * @param strInputString the strInputString to set
	 */
	public void setStrInputString(String strInputString) {
		this.strInputString = strInputString;
	}
	/**
	 * @return the strVoidRowIds
	 */
	public String getStrVoidRowIds() {
		return strVoidRowIds;
	}
	/**
	 * @param strVoidRowIds the strVoidRowIds to set
	 */
	public void setStrVoidRowIds(String strVoidRowIds) {
		this.strVoidRowIds = strVoidRowIds;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the alInsertDetail
	 */
	public ArrayList getAlInsertDetail() {
		return alInsertDetail;
	}
	/**
	 * @param alInsertDetail the alInsertDetail to set
	 */
	public void setAlInsertDetail(ArrayList alInsertDetail) {
		this.alInsertDetail = alInsertDetail;
	}
	/**
	 * @return the statusFlag
	 */
	public String getStatusFlag() {
		return statusFlag;
	}
	/**
	 * @param statusFlag the statusFlag to set
	 */
	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}
	
}