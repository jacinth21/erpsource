package com.globus.operations.itemcontrol.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.custservice.beans.GmConsignPackSLipInterface;
import com.globus.custservice.beans.GmConsignPackSlipInstance;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.itemcontrol.beans.GmItemControlRptBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.itemcontrol.forms.GmItemControlForm;
import com.globus.common.actions.GmDispatchAction;

public class GmUnAssignItemControlAction extends GmDispatchAction{
    Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward unAssignTransactionDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmItemControlForm gmItemControlForm = (GmItemControlForm) form;
    gmItemControlForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    String strDispatchTo = "GmItemControl";
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    String strTxnid = GmCommonClass.parseNull(gmItemControlForm.getTxnid());
    String strUserId = GmCommonClass.parseNull(gmItemControlForm.getUserId());
	hmParam.put("TXNID", strTxnid);
	hmParam.put("USERID", strUserId);
	log.debug("hmParam,TXNID"+strTxnid);
	log.debug("USERID"+strUserId);
	gmItemControlTxnBean.unAssignTransaction(hmParam);
	log.debug("inside hmParam"+hmParam);
      return mapping.findForward(strDispatchTo);
}
}