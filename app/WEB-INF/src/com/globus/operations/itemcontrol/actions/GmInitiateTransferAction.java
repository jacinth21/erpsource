package com.globus.operations.itemcontrol.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.itemcontrol.beans.GmInitiateTransferBean;
import com.globus.operations.itemcontrol.forms.GmInitiateTransferForm;


public class GmInitiateTransferAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveReplenishment - This method is used to Save Replenishment
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward saveReplenishment(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmInitiateTransferBean gmInitiateTransferBean = new GmInitiateTransferBean(getGmDataStoreVO());
    GmInitiateTransferForm gmInitiateTransferForm = (GmInitiateTransferForm) actionForm;
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    String strInputString = GmCommonClass.parseNull(request.getParameter("v_str"));
    String strWareHouseType = GmCommonClass.parseNull(request.getParameter("hWareHouseType"));
    String strUserID =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
    String strTxnIds = "", strXmlGridData = "";
    HashMap hmParam = new HashMap();
    hmParam.put("VSTRING", strInputString);
    hmParam.put("USERID", strUserID);
    hmParam.put("WHTYPE", strWareHouseType);
    strTxnIds = GmCommonClass.parseNull(gmInitiateTransferBean.saveReplenishment(hmParam));
    strXmlGridData = GmCommonClass.parseNull(gmInitiateTransferBean.getXmlGridData(strSessCompanyLocale));
    gmInitiateTransferForm.setStrXMLGrid(strXmlGridData);
    gmInitiateTransferForm.setStrTxnIds(strTxnIds);
    ArrayList alWareHouse = new ArrayList();
    alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouse(""));
    gmInitiateTransferForm.setAlWareHouse(alWareHouse);
    request.setAttribute("strOpt", "save");
    return actionMapping.findForward("gmInitiateTransfer");
  }

  /**
   * fetchReplenishment - This Method is used to Initiate Replenishment
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchReplenishment(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmInitiateTransferForm gmInitiateTransferForm = (GmInitiateTransferForm) form;
    GmInitiateTransferBean gmInitiateTransferBean = new GmInitiateTransferBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    ArrayList alWareHouse = new ArrayList();
    alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouse(""));
    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
    gmInitiateTransferForm.setAlWareHouse(alWareHouse);

    String xmlGridData = gmInitiateTransferBean.getXmlGridData(strSessCompanyLocale);
    gmInitiateTransferForm.setStrXMLGrid(xmlGridData);
    request.setAttribute("xmlGridData", xmlGridData);
    return actionMapping.findForward("gmInitiateTransfer");
  }

  /**
   * fetchPartDetails -- This method is used to Fetch All Locations from partNum .(Ajax call
   * request)
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchPartDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    String strLocationID = "";
    String strLocQty = "";
    String strReturn = "";
    String strLocCode = "";
    String strWarehouseNm = "";
    instantiate(request, response);
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam.put("PARTNUM", request.getParameter("partNum"));
    hmParam.put("WAREHOUSEID", request.getParameter("warehouseid"));
    hmParam.put("STATUSNUM", "93310");
    hmParam.put("CHKCURRQTYFLAG", "LOCTRANS");
    ArrayList alReturn =
        GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchInvLocationPartMapping(hmParam));
    for (int i = 0; i < alReturn.size(); i++) {
      HashMap locMap = (HashMap) alReturn.get(i);
      strLocationID = GmCommonClass.parseNull((String) locMap.get("LID"));
      strLocQty = (String) locMap.get("CURRQTY");
      strLocCode = GmCommonClass.parseNull((String) locMap.get("LCD"));

      strReturn += strLocCode + "_" + strLocationID + "|";
      // strReturn += strLocationID+"|";
    }
    response.setContentType("text/plain");
    PrintWriter pw = null;
    try {
      pw = response.getWriter();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    if (strReturn == null)
      pw.write("Fail");
    else
      pw.write(strReturn);
    pw.flush();
    return null;
  }

  /**
   * fetchInvLocationByID - This Method is used to Fetch all location from Part Number and Location
   * (Ajax request)
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchInvLocationByID(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    HashMap hmParams = new HashMap();
    HashMap hmTemplateParam = new HashMap();

    ArrayList alInvLocList = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alInvLocTypes = new ArrayList();
    ArrayList alParts = new ArrayList();
    HashMap hmLocationDetails = new HashMap();
    String strLocationID = GmCommonClass.parseNull(request.getParameter("locationId"));
    String strPartNum = GmCommonClass.parseNull(request.getParameter("partNum"));
    String strWareHouseId = GmCommonClass.parseNull(request.getParameter("warehouseid"));
    String strOpt = "";
    String strHAction = "";

    /* Fetch Location Part mapping */
    alInvLocList =
        GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchInvLocationPartMappingByID(
            strLocationID, strPartNum, strWareHouseId));
    HashMap hmReturn = (HashMap) alInvLocList.get(0);
    String strCurrQty = (String) hmReturn.get("CURRQTY");
    String strMaxQty = (String) hmReturn.get("MAXQTY");
    String strMinQty = (String) hmReturn.get("MINQTY");
    String strLocType = (String) hmReturn.get("LOCATIONTYPE");
    String strSugQty = (String) hmReturn.get("SUGGQTY");

    String strInputString =
        strCurrQty + "^" + strMaxQty + "^" + strMinQty + "^" + strLocType + "^" + strSugQty;

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    if (!strInputString.equals(""))
      pw.write(strInputString);
    pw.flush();
    pw.close();
    return null;
  }
}
