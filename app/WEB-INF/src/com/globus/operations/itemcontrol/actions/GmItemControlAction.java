package com.globus.operations.itemcontrol.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.custservice.beans.GmConsignPackSLipInterface;
import com.globus.custservice.beans.GmConsignPackSlipInstance;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.itemcontrol.beans.GmItemControlRptBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.itemcontrol.forms.GmItemControlForm;

public class GmItemControlAction extends GmAction {
    Logger log = GmLogger.getInstance(this.getClass().getName());
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmWhAddResult = new HashMap();
    HashMap hmTxnDetals = new HashMap();
    ArrayList alTemp = new ArrayList();
    ArrayList alLogReasons = new ArrayList();

    String strAccess = "";
    String strUserId = "";
    String strRuleOpt = "";
    String strRulesOpt = "";
    String strIncludeRuleEng = "";
    String strOpt = "";
    String strAction = "";
    String strTxnType = "";
    String strTransType = "";
    String strDispatchTo = "GmItemControl";
    String strRuleTxnStatus = "";
    String strTransTable = "";
    String strPackslipHeader = "";
    String strReForward = "";

    String strFGBinFl = "";
    String strFgBinId = "";
    String strFGBinAvail = "";
    String strFGBinList = "";
    String strTxnId = "";
    String strLocType = "";
    String strControlNo = "";
    String strPartNum = "";
    String strValidationFl = "";
    String strType = "";
    String strTxnStatus = "";
    String strTxnCnt = "";
    String strCheckRule = "";
    String strRuleEntryFl = "N";
    String strValidChk = "";
    String strConId="";
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(getGmDataStoreVO());
    GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
    GmRuleBean gmRuleBean = new GmRuleBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean=new GmInvLocationBean();

    GmItemControlForm gmItemControlForm = (GmItemControlForm) form;

    gmItemControlForm.loadSessionParameters(request);


    HttpSession session = request.getSession(false);

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strPlantId = getGmDataStoreVO().getPlantid();
    // Locale
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

    strAction = GmCommonClass.parseNull(request.getParameter("haction"));// gmItemControlForm.getHaction()
    strAction = strAction.equals("") ? (String) session.getAttribute("haction") : strAction;
    log.debug("  Action from is " + strAction);

    strType = GmCommonClass.parseNull(request.getParameter("hType"));
    strTxnStatus = GmCommonClass.parseNull(request.getParameter("txnStatus"));
    strTxnCnt = GmCommonClass.parseNull(request.getParameter("hCnt"));

    strOpt = GmCommonClass.parseNull(gmItemControlForm.getStrOpt());
    strRuleOpt =
        strOpt.equals("") ? GmCommonClass.parseNull(gmItemControlForm.getHaction()) : strOpt;

    strTxnType = GmCommonClass.parseNull(gmItemControlForm.getTxntype());

    hmParam = GmCommonClass.getHashMapFromForm(gmItemControlForm);
    hmParam.put("RULEID", strTxnType);

    HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
    strRulesOpt = GmCommonClass.parseNull((String) hmTransRules.get("STR_OPT"));
    strLocType = GmCommonClass.parseNull((String) hmParam.get("LOCTYPE"));
    request.setAttribute("TRANS_RULES", hmTransRules);
    session.setAttribute("TRANS_RULES", hmTransRules);
    strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
    strTransTable = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_TABLE"));
    strRuleTxnStatus = GmCommonClass.parseNull((String) hmTransRules.get("ICE_TXN_STATUS"));
    strCheckRule = GmCommonClass.parseNull((String) request.getAttribute("chkRule"));
    strCheckRule =
        strCheckRule.equals("") ? GmCommonClass.parseNull(gmItemControlForm.getChkRule())
            : strCheckRule;

    log.debug("hmTransRules" + hmTransRules);

    strFGBinFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue("REQ_SHIP_REL", strTxnType));// To
                                                                                                 // show
                                                                                                 // or
                                                                                                 // hide
                                                                                                 // FG
                                                                                                 // BIN
                                                                                                 // details
                                                                                                 // section
                                                                                                 // on
                                                                                                 // the
                                                                                                 // screen
    gmItemControlForm.setStrFGBinFl(strFGBinFl);

    strUserId = GmCommonClass.parseNull(gmItemControlForm.getUserId());

    strTransType = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_RLTXN"));
    strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;

    if (!strRuleOpt.equals("") && hmTransRules != null && !strAction.equals("ValidateFGBIN")
        && !strAction.equals("ValidateControlNo")) {
      // //Access Rule Code/////
      String strOperType = GmCommonClass.parseNull(gmItemControlForm.getMode());
      strOperType = strOperType.equals("") ? "CONTROL" : strOperType;
      strOperType = "FN_" + strOperType;
      strOperType = (String) hmTransRules.get(strOperType);

      log.debug("Request URI == " + strOpt + "_" + strOperType);

      if (strOperType != null) {
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                strOperType));
        strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("strAccess==>" + strAccess);
        request.setAttribute("ACCESSFL", strAccess);
        session.setAttribute("ACCESSFL", strAccess);
        if (!strAccess.equals("Y") && !strAction.equals("PicSlip")) {
          throw new AppError("", "20685");
        }
      }
    }
    // ////////
    if (strAction.equals("SaveControl")) {
      hmParam.put("TXNSTATUS", strRuleTxnStatus);
      strValidChk = GmCommonClass.parseNull((String) request.getAttribute("strValidChk"));
      hmParam.put("VALIDCHK", strValidChk);
      /*
       * Save the lot number in the t5055 table with error details and get a flag to check whether
       * any validation is there or not
       */
      strValidationFl = gmItemControlTxnBean.saveItemLotNum(hmParam);
      hmParam.put("VALIDCHK", "Y");
      request.setAttribute("strValidChk", "Y");
      String strEnableNOCfl = GmCommonClass.parseNull(request.getParameter("hEnableNOCfl"));
      /*
       * If validation available then don't call the rule engine to validate the part or do not call
       * the actual save procedure. If all the validations from above method is done, then call the
       * rule engine for rule validaitons Also call the actual save procedure to save the details
       */
      strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
      if (strValidationFl.equalsIgnoreCase("N") || strValidationFl.equals("")) {
        if (!strIncludeRuleEng.equalsIgnoreCase("NO") && strCheckRule.equalsIgnoreCase("Yes") 
        		&& !strEnableNOCfl.equals("Y")) {
          gmItemControlForm.setChkRule("No");
          strRuleEntryFl = "Y";

          request.setAttribute("RE_TXN", strTransType);
          request.setAttribute("txnStatus", strTxnStatus);
          request.setAttribute("txnID", strTxnId);
          request.setAttribute("TxnTypeID", strTxnType);
          request.setAttribute("hType", strType);
          request.setAttribute("hCnt", strTxnCnt);
          request.setAttribute("hAction", "SaveControl");
          request.setAttribute("RE_FORWARD", "gmItemCtrl");
          request.setAttribute("chkRule", "No");
          return mapping.findForward("gmRuleEngine");
        }

        gmItemControlTxnBean.saveItemControlMain(hmParam);
      }
      
      strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
      strTransType = GmCommonClass.parseNull((String) hmParam.get("TXNTYPE"));
      strConId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
     // strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
      strAction = "EditControl";
      // Below code is added to prevent resubmission of form while refreshing
      return actionRedirect("/gmItemControl.do?haction=EditControl&txnid=" + strTxnId
          + "&mode=CONTROL&hConsignId" + strTxnId + "&txntype=" + strTxnType + "&loctype="
          + strLocType, request);
    }
    if (strAction.equals("EditControl")) {

      hmParam.put("RELFLAG", ""); // Code Added to load the pick locations only from warehouse.
      hmReturn = GmCommonClass.parseNullHashMap(gmItemControlRptBean.fetchItenControlMain(hmParam));

      hmTemp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("hmHeaderInfo"));
      String strInsertFlag = GmCommonClass.parseNull((String) hmTemp.get("INSERTFL"));
      String strTransInsertFlag = GmCommonClass.parseNull((String) hmTemp.get("TRANSINSERTFL"));
      gmItemControlForm.setInserFl(strInsertFlag);
      gmItemControlForm.setTransInsertFl(strTransInsertFlag);
      if (strTransTable.equals("CONSIGNMENT")) {// consignment txn
        hmReturn.put("CONDETAILS", hmTemp);
      }
      gmItemControlForm.setErrCnt(GmCommonClass.parseNull((String) hmTemp.get("ERRCNT")));

      gmItemControlForm =
          (GmItemControlForm) GmCommonClass.getFormFromHashMap(gmItemControlForm, hmTemp);
      log.debug("====hmHeaderInfo=====" + hmTemp);
      alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alDetailInfo"));
      gmItemControlForm.setAldetailinfo(alTemp);

      log.debug("strTxnType  " + strTxnType);
      String strSetLoad = strTxnType.equals("50261") ? "CARTDETAILS" : "SETLOAD";// order /inhouse
                                                                                 // txn
      hmReturn.put(strSetLoad, alTemp);

      log.debug("====alDetailInfo====" + alTemp);
      alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alLocDtl"));
      gmItemControlForm.setAllocdtl(alTemp);
      log.debug("====alLocDtl=====" + alTemp);
      String strPums = GmCommonClass.parseNull((String) hmReturn.get("strPnums"));
      gmItemControlForm.setStrpnums(strPums);
      
      alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alInsertDetails"));
      gmItemControlForm.setAlInsertDetails(alTemp);
      
      alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alInsertLocDtl"));
      gmItemControlForm.setAlInsertlocdtl(alTemp);


      String strTransId = GmCommonClass.parseNull((String) hmTemp.get("TXNID"));

      alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strTransId, "1268"));
      gmItemControlForm.setAlLogReasons(alLogReasons);

      // If there is any existing FG Bin id associated eith the transaction, need to fetch it to the
      // screen
      strFGBinList = GmCommonClass.parseNull(gmItemControlRptBean.fetchFGBinId(strTransId));
      gmItemControlForm.setStrFGBinList(strFGBinList);
      String strEnableNOCfl = GmCommonClass.parseNull(request.getParameter("hEnableNOCfl"));

      if (!strTransType.equals("") && !strIncludeRuleEng.equalsIgnoreCase("NO")
          && !strRuleEntryFl.equals("Y") && !strEnableNOCfl.equals("Y")) {
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("RE_FORWARD", "GmItemControl");
        request.setAttribute("RE_TXN", strTransType);

        request.setAttribute("txnStatus", strRuleTxnStatus);
        request.setAttribute("TxnTypeID", strTxnType);
        strDispatchTo = "gmRuleEngine";
      }
    }
    gmItemControlForm.setHaction(strAction);

    if (strAction.equals("PicSlip")) {
      String strSource = GmCommonClass.parseNull(request.getParameter("ruleSource"));
      String strRefId = GmCommonClass.parseNull(request.getParameter("refId"));
      String strAddId = GmCommonClass.parseNull(request.getParameter("addressid"));
      String strConsignId =
          request.getParameter("hConsignId") == null ? "" : request.getParameter("hConsignId");
      String strReTxn = "";
      String strLoanerType = "0";
      String strComments = "";
      String strShowJasperPicSlip = "";
      log.debug("ruleSource .......= " + strSource);
      ArrayList alLoanerExtnLog = new ArrayList();
      ArrayList alDataList = new ArrayList();

      strReForward =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PICK_SLIP.ITEM_REFORWARD"));
      strShowJasperPicSlip =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PICK_SLIP.COMP_CONSIGN"));

      if (strTransTable.equals("IN_HOUSE")) {
        // Code for T412 and T413 table
        hmResult = gmLogisticsBean.viewInHouseTransItemsDetails(strConsignId, "");
        log.debug("Inhouse hmResult 1= " + hmResult);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmLogisticsBean.viewInHouseTransDetails(strConsignId);
        log.debug("hmResult 2= " + hmResult);
        hmReturn.put("CONDETAILS", hmResult);

        // Code for check the pick slip informations based on FGRP and Different warehouse
        // conditions.
        if (strTxnType.equals("93341")) { // FGRP
          hmWhAddResult = gmLogisticsBean.fetchPicSlipHeaderInfo(strConsignId, strTxnType);
          strPackslipHeader = GmCommonClass.parseNull((String) hmWhAddResult.get("PACKSLIPHEAD"));
          if (!strPackslipHeader.equals("")) {
            hmResult.putAll(hmWhAddResult);

            if (strReForward.equals("gmItemPicSlip")) {
              strReForward = "gmItemWhPicSlip";
            } else {
              strReForward = "gmItemWhPicSlipGlobal";
            }

          }
          log.debug("hmResult FINAL = " + hmResult);
        }
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("source", "50183"); // Loaner Extn
        if (strSource.equals("50155") || strSource.equals("50157")) {
          strLoanerType = gmRuleBean.fetchLoanerType(strRefId);
        }
        if (!strSource.equals("")) {
          strReTxn = gmRuleBean.fetchTransId(strSource, strLoanerType);
        }
        alLoanerExtnLog =
            GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strConsignId, "1241"));
        if (alLoanerExtnLog.size() > 0) {
          for (int i = 0; i < alLoanerExtnLog.size(); i++) {
            HashMap hmLoanerExtnLog = (HashMap) alLoanerExtnLog.get(i);
            strComments +=
                GmCommonClass.parseNull((String) hmLoanerExtnLog.get("COMMENTS")) + "<BR>";
          }
        }
      }
      if (strTransTable.equals("CONSIGNMENT")) {
        String strClassName =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CONSIGNPACKSLIP",
                "FETCH_CLASS", strCompanyId));
        log.debug("strShowJasperPicSlip"+strShowJasperPicSlip);
        if (strShowJasperPicSlip.equalsIgnoreCase("Y")
            && (strTxnType.equals("4110") || strTxnType.equals("4112"))) {
          GmConsignPackSLipInterface consignpackSlipClient =
              GmConsignPackSlipInstance.getInstance(strClassName);
          hmResult = consignpackSlipClient.loadSavedSetMaster(strConsignId);
          alDataList = (ArrayList) hmResult.get("SETLOAD");
        } else {
          // code for T504 and T505 table
          hmResult = gmOper.loadSavedSetMaster(strConsignId);
          log.debug("Consignment hmResult 1= " + hmResult);
          hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));

          String strExpiryShipDateFl=
                  GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "EXPIRY_DATE"));
          hmReturn.put("ExpiredShipDateFl",strExpiryShipDateFl);
          
        }
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        log.debug("Consignment hmResult 2= " + hmResult);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("source", "50181"); // consign

        strReTxn = gmRuleBean.fetchTransId(strSource, "0");
        strReTxn = strReTxn.equals("") ? "50911" : strReTxn;


      }
      alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strConsignId, "1268"));
      if (alLogReasons.size() > 0) {
        for (int i = 0; i < alLogReasons.size(); i++) {
          HashMap hmLog = (HashMap) alLogReasons.get(i);
          strComments += GmCommonClass.parseNull((String) hmLog.get("COMMENTS"));
          strComments += "<BR>";
        }
      }
      request.setAttribute("CRCOMMENTS", strComments);
      request.setAttribute("strRuleSource", strSource);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      if (strTxnType.equals("93341")) {
        request.setAttribute("RE_FORWARD", strReForward);
      }
      // For Showing the Pic Slip in Jasper Report for BBA for Consignment
      if (strShowJasperPicSlip.equalsIgnoreCase("Y")
          && (strTxnType.equals("4110") || strTxnType.equals("4112"))) {
        request.setAttribute("HASHMAPDATA", hmResult);
        request.setAttribute("ARRAYLISTDATA", alDataList);
        strReForward = "gmConsignJasperPicSlip";
      }

      if (!strReTxn.equals("") && !strIncludeRuleEng.equalsIgnoreCase("NO")) {
        request.setAttribute("RE_FORWARD", strReForward);
        request.setAttribute("RE_TXN", strReTxn);
        request.setAttribute("txnStatus", "PROCESS");
        strDispatchTo = "gmRuleEngine";
      } else {
        strDispatchTo = strReForward;
      }

    }

    if (strAction.equals("ValidateFGBIN")) {// To check whether the FG Bin is available or not when
                                            // "Add" button is clicked

      strFgBinId = GmCommonClass.parseNull(request.getParameter("Txt_FgBin"));
      strFGBinAvail = GmCommonClass.parseNull(gmOper.validateFGBinId(strFgBinId));

      response.setContentType("text/plain");

      PrintWriter pw = response.getWriter();
      pw.write(strFGBinAvail);
      pw.flush();
      return null;
    }
    if (strAction.equals("ValidateControlNo")) {// To check whether the control number is valid
      strControlNo = GmCommonClass.parseNull(request.getParameter("Txt_ControlNo"));
      strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNo"));
      strTxnType = GmCommonClass.parseNull(request.getParameter("TxnType"));
      String strMsg = GmCommonClass.parseNull(request.getParameter("Msg"));

      strMsg =
          GmCommonClass.parseNull(gmItemControlRptBean.validateControlNoPart(strPartNum,
              strControlNo, strTxnType, strMsg));

      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strMsg);
      pw.flush();
      return null;
    }
    
    if (strAction.equals("saveInsertTrasactionDetails")) {
	    	String strTxnid = GmCommonClass.parseNull(gmItemControlForm.getTxnid());
		    String strLocation = GmCommonClass.parseNull(gmItemControlForm.getLocation());
		    String strcheckflag = GmCommonClass.parseNull(gmItemControlForm.getCheckFl());
		    String strInsertid = GmCommonClass.parseNull(gmItemControlForm.getInsertid());
		    String strRevnum = GmCommonClass.parseNull(gmItemControlForm.getRevnum());
		    String strTxntype = GmCommonClass.parseNull(gmItemControlForm.getTxntype());
		    String strInsertTxnTyp = gmItemControlTxnBean.getInsertTransactionType(strTxntype,strPlantId);
		    hmParam.put("TXNID", strTxnid);
		    hmParam.put("TXNTYPE", strInsertTxnTyp);
		    hmParam.put("INSERTID", strInsertid);
		    hmParam.put("REVNUM", strRevnum);
		    hmParam.put("LOCATION", strLocation);
		    hmParam.put("CHECKFL", strcheckflag);
		    hmParam.put("USERID", strUserId);
		    log.debug("hmParam???"+hmParam);
		    gmItemControlTxnBean.saveInsertTransactionDetails(hmParam);

		      response.setContentType("text/plain");
		      PrintWriter pw = response.getWriter();
		      pw.flush();
		      return null;
		    }
      return mapping.findForward(strDispatchTo);
  }
  
}
