package com.globus.operations.itemcontrol.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.itemcontrol.beans.GmItemControlRptBean;
import com.globus.operations.itemcontrol.beans.GmItemControlTxnBean;
import com.globus.operations.itemcontrol.forms.GmItemControlForm;
import com.globus.operations.logistics.beans.GmIHLoanerItemBean;


public class GmItemVerifyAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmTemp = new HashMap();
    ArrayList alTemp = new ArrayList();


    String strAccess = "";
    String strUserId = "";
    String strRuleOpt = "";
    String strRulesOpt = "";
    String strIncludeRuleEng = "";
    String strOpt = "";
    String strAction = "";
    String strTransType = "";
    String strDispatchTo = "GmItemControl";
    String strRuleTxnStatus = "";
    String strTransTable = "";
    String strTransId = "";
    String strRetMsg = "";
    String strConType = "";

    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    GmIHLoanerItemBean gmIHLoanerItemBean = new GmIHLoanerItemBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));


    GmItemControlForm gmItemControlForm = (GmItemControlForm) form;

    gmItemControlForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmItemControlForm);

    HttpSession session = request.getSession(false);

    strAction = GmCommonClass.parseNull(request.getParameter("haction"));// gmItemControlForm.getHaction()
    log.debug("  Action from request is " + strAction);
    if (strAction.equals("")) {
      strAction = (String) session.getAttribute("haction");
      log.debug(" Action from Session is " + strAction);
    }

    strOpt = GmCommonClass.parseNull(gmItemControlForm.getStrOpt());

    if (strOpt.equals("")) {
      strRuleOpt = GmCommonClass.parseNull(gmItemControlForm.getHaction());
    }
    if (strRuleOpt.equals("")) {
      strRuleOpt = strOpt;
    }
    // The below two lines is added for rules to show for FGRP like transactions.
    String strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
    strTxnStatus =
        strTxnStatus.equals("") ? GmCommonClass.parseNull(request.getParameter("txnStatus"))
            : strTxnStatus;

    String strTxnType = GmCommonClass.parseNull(gmItemControlForm.getTxntype());
    hmParam.put("RULEID", strTxnType);

    HashMap hmTransRules = gmCommonBean.fetchTransactionRules(hmParam);
    strRulesOpt = GmCommonClass.parseNull((String) hmTransRules.get("STR_OPT"));
    request.setAttribute("TRANS_RULES", hmTransRules);
    session.setAttribute("TRANS_RULES", hmTransRules);
    strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
    strTransTable = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_TABLE"));
    log.debug("hmTransRules" + hmTransRules);

    strUserId = GmCommonClass.parseNull(gmItemControlForm.getUserId());

    if (!strRuleOpt.equals("") && hmTransRules != null) {
      // //Access Rule COde/////
      String strOperType = GmCommonClass.parseNull(gmItemControlForm.getMode());
      strOperType = strOperType.equals("") ? "VERIFY" : strOperType;
      strOperType = "FN_" + strOperType;
      log.debug("User ID == " + strUserId);
      strOperType = (String) hmTransRules.get(strOperType);
      log.debug("Request URI == " + strOpt + "_" + strOperType);
      if (strOperType != null) {
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                strOperType));
        strAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("strAccess==>" + strAccess);
        request.setAttribute("ACCESSFL", strAccess);
        session.setAttribute("ACCESSFL", strAccess);
        if (!strAccess.equals("Y")) {
          throw new AppError("", "20685");
        }
      }
    }
    // ////////
    if (strAction.equals("Verify")) {
    	hmParam.put("COMPANYINFO",strCompanyInfo);
      gmItemControlTxnBean.saveItemVerifyMain(hmParam);
      
      if (strTxnType.equals("50155") || strTxnType.equals("50157")) {
    	  strTransId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
          String strCnID = GmCommonClass.parseNull((String)hmParam.get("REFID"));
          GmInvLocationBean gmInvLocationBean = new GmInvLocationBean();
          HashMap hmLnParam = new HashMap ();
          hmLnParam.put("TRANS_ID", strTransId);
          hmLnParam.put("REFID", strCnID);
          hmLnParam.put("TRANS_TYPE", strTxnType);
          hmLnParam.put("USER_ID", strUserId);
          hmLnParam.put("WAREHOUSE_TYPE", "6");
          hmLnParam.put("COMPANY_INFO",strCompanyInfo);
          hmLnParam.put("TRANS_STATUS","4"); //50155 - FGLN(Shelf to Loaner Set), 50157-LNFG(Loaner to Shelf) - When verifying this transaction status will be always 4. So that status flag hard coded here. in PMT-38155  
          gmInvLocationBean.updateLoanerStockSheetInfo(hmLnParam);
      }
     

      if (strTxnType.equals("9104") || strTxnType.equals("9106")) {
        strTransId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
        strRetMsg = GmCommonClass.parseNull(gmIHLoanerItemBean.fetchAssociatedIHLN(strTransId));
        gmItemControlForm.setRetMsg(strRetMsg);
        // log.debug("strRetMsg ::: " + strRetMsg);
      }
      strAction = "EditVerify";
    }
    if (strAction.equals("EditVerify")) {
      hmParam.put("RELFLAG", "ON"); // Code Added to load the pick locations only from warehouse.
      hmReturn = GmCommonClass.parseNullHashMap(gmItemControlRptBean.fetchItenControlMain(hmParam));
      hmTemp = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("hmHeaderInfo"));
      if (strTransTable.equals("CONSIGNMENT")) {// consignment txn
        hmReturn.put("CONDETAILS", hmTemp);
      }
      gmItemControlForm =
          (GmItemControlForm) GmCommonClass.getFormFromHashMap(gmItemControlForm, hmTemp);
      log.debug("====hmHeaderInfo=====" + hmTemp);
      // JMD
        
    
      
      alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alDetailInfo"));
      gmItemControlForm.setAldetailinfo(alTemp);
      hmReturn.put("SETLOAD", alTemp);
      log.debug("====alDetailInfo====" + alTemp);
      alTemp = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("alLocDtl"));
      gmItemControlForm.setAllocdtl(alTemp);
      log.debug("====alLocDtl=====" + alTemp);
      String strPums = GmCommonClass.parseNull((String) hmReturn.get("strPnums"));
      gmItemControlForm.setStrpnums(strPums);
      log.debug("===strPums=====" + strPums);

      strConType = GmCommonClass.parseNull(gmItemControlForm.getContype());
      if (strConType.equals(""))
        hmTemp.put("CONTYPE", GmCommonClass.parseNull((String) hmTransRules.get("CON_TYPE")));

      strTransType = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_RLTXN"));
      strTransType = strTransType.equals("") ? "INHOUSETRANS" : strTransType;
      String strEnableNOCfl = GmCommonClass.parseNull(request.getParameter("hEnableNOCfl"));

      if (!strTransType.equals("") && !strIncludeRuleEng.equalsIgnoreCase("NO") && !strEnableNOCfl.equals("Y") ) {
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("RE_FORWARD", "GmItemControl");
        request.setAttribute("RE_TXN", strTransType);
        request.setAttribute("TxnTypeID", strTransType);
        strRuleTxnStatus =
            strTxnStatus.equals("") ? GmCommonClass.parseNull((String) hmTransRules
                .get("ICE_TXN_STATUS")) : strTxnStatus;
        request.setAttribute("txnStatus", strRuleTxnStatus);
        strDispatchTo = "gmRuleEngine";
      }
    }
    gmItemControlForm.setHaction(strAction);

    return mapping.findForward(strDispatchTo);
  }
}
