package com.globus.operations.itemcontrol.beans;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;

public class GmInitiateTransferBean  extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmInitiateTransferBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());   
	  }

	public GmInitiateTransferBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	/**
	 * saveReplenishment - This method to Save Replenishment
	 * @return String 
	 * @exception Apperror
	 */
	public String saveReplenishment(HashMap hmParams)throws AppError 
	{
		log.debug("hmParams ::: " + hmParams);
		String strInputString = (String) hmParams.get("VSTRING");
		String strUserId = (String) hmParams.get("USERID");
		String strWHType = GmCommonClass.parseNull((String) hmParams.get("WHTYPE"));
		String strGrpPart = GmCommonClass.parseNull(GmCommonClass.getString("GRP_PART_FGLT"));
		String strTxnIds = "";
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    GmTransSplitBean gmTransSplitBean = new GmTransSplitBean(getGmDataStoreVO()); //For PMT-33513
	    HashMap hmStorageBuildInfo = new HashMap(); //for PMT-33513
    
		String strProcedure = "gm_pkg_op_item_control_txn.gm_sav_init_replenishment";
		//strGrpPart is for Grouping the FGLT Txn's.
		if(strGrpPart.equalsIgnoreCase("YES")){
			strProcedure = "gm_pkg_op_item_control_txn.gm_sav_init_grp_replenishment";
		}
		gmDBManager.setPrepareString(strProcedure, 4);
		gmDBManager.setString(1, strInputString);
		gmDBManager.setString(2, strUserId);
		gmDBManager.setString(3, strWHType);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.execute();
		strTxnIds = gmDBManager.getString(4);
		gmDBManager.commit();
		
		//JMS call for Storing Building Info PMT-33513 
		if(strTxnIds != null && !strTxnIds.equals("")){
        log.debug("Storing building information GmInitiateTransferBean saveReplenishment() ");
		if(strTxnIds.contains(",")){ //this code will execute if there are multiple FGRP transactons 
			String[] strTransactionIds = strTxnIds.split(",");
			  for (int i = 0; i < strTransactionIds.length; i++) {
				hmStorageBuildInfo.put("TXNID", strTransactionIds[i]); //consignment id
		        hmStorageBuildInfo.put("TXNTYPE", "INHOUSE412");
		        gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo); 
			}
        }else{//this code will execute if there is only one FGRP txn
        	hmStorageBuildInfo.put("TXNID", strTxnIds);//consignment id
            hmStorageBuildInfo.put("TXNTYPE", "INHOUSE412");
            gmTransSplitBean.updateStorageBuildingInfo(hmStorageBuildInfo); 
        }
	}//JMS call ends
        
		return strTxnIds;
	}
	
	/**
	 * getXmlGridData - This method will get String xmlGrid to load Replenishment (Empty Grid)
	 * @return String AppError
	 * @exception 
	 */
	public String getXmlGridData(String strSessCompanyLocale) throws AppError {
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setDataList("alReturn",new ArrayList());
		templateUtil.setResourceBundle(GmCommonClass
		        .getResourceBundleBean(
		            "properties.labels.operations.itemcontrol.gmInitiateTransfer",
		            strSessCompanyLocale));
		templateUtil.setTemplateName("GmInitiateTransfer.vm");
		templateUtil.setTemplateSubDir("operations/itemcontrol/templates");
		return templateUtil.generateOutput(); 
	}
}



