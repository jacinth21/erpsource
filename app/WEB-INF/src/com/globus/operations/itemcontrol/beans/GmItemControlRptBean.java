package com.globus.operations.itemcontrol.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmItemControlRptBean extends GmBean{

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
	public GmItemControlRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
	}
	
	public GmItemControlRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	/*
	 * function to fetch Transaction Header Info.
	 *@author Gopinathan
	 * 
	 */
	public HashMap fetchItenControlMain(HashMap hmParam) throws AppError {
		GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
		HashMap hmReturn = new HashMap();
		HashMap hmTemp = new HashMap();
		HashMap hmTmp = new HashMap();
		ArrayList alTmp = new ArrayList();
		ArrayList alTemp = new ArrayList();
		
		log.debug("====fetchItenControlMain===hmParam==="+hmParam);
		
		String strPnums = "";
		String strInserts = "";
		String strWhType= "";
		
		String strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
		String strTxnType = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));
		String strLocType = GmCommonClass.parseNull((String)hmParam.get("LOCTYPE"));
		String strRelFlag = GmCommonClass.parseNull((String)hmParam.get("RELFLAG"));
		
		hmTemp = GmCommonClass.parseNullHashMap(fetchTransHeaderInfo(strTxnId,strTxnType));
		hmReturn.put("hmHeaderInfo", hmTemp);
		
		
		
		alTemp = GmCommonClass.parseNullArrayList(fetchTransDetailInfo(strTxnId,strTxnType,strLocType));
		hmReturn.put("alDetailInfo", alTemp);
		
		strPnums = GmCommonClass.parseNull(getItemLocationStr(alTemp));
		hmReturn.put("strPnums", strPnums);
		
		String strPickPutFl = GmCommonClass.parseNull((String)hmTemp.get("PICKPUTFL"));
		
		alTemp = GmCommonClass.parseNullArrayList(fetchPartLocationDtl(strTxnId,strPnums,strTxnType, strPickPutFl,strRelFlag));
		hmReturn.put("alLocDtl", alTemp);
		
		//To fetch the Insert Section Details in Process Transaction screen for the enabled Transaction
		String strCompanyId = getCompId();
		String strPlantId = getGmDataStoreVO().getPlantid();
	    String strInsertTxnTyp = gmItemControlTxnBean.getInsertTransactionType(strTxnType,strPlantId);
	    String strInsertfl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "INSERT_PART"));
	    String strInsertTransfl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strInsertTxnTyp, "INSERT_TRANS"));
	    if(strInsertfl.equalsIgnoreCase("Y") && strInsertTransfl.equalsIgnoreCase("Y") ){
			alTemp = GmCommonClass.parseNullArrayList(fetchInsertDetails(strTxnId,strTxnType));
			hmReturn.put("alInsertDetails", alTemp);
			strInserts = GmCommonClass.parseNull(getItemLocationStr(alTemp));
			alTemp = GmCommonClass.parseNullArrayList(fetchInsertLocationInfo(strInserts,strWhType));
			hmReturn.put("alInsertLocDtl", alTemp);
	    }
		if (strTxnType.equals("103932")) {  // Transaction type is Part Re-designation (103932)
			hmTmp = GmCommonClass.parseNullHashMap(fetchPartLabelParams(strTxnId));
			hmReturn.put("HMLABLEFLAGS", hmTmp);
		};

		
		return hmReturn;
	}
	/*
	 * function to fetch Transaction Header Info.
	 * @author Gopinathan
     * This method/DB File used in shipping Device (erpshipping repository).
     * Please make sure the changes are done in that repository too if param added/removed for the DB call.
	 */
	public HashMap fetchTransHeaderInfo(String strTxnId, String strTxnType) throws AppError {
		
		HashMap hmRuturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		gmDBManager.setPrepareString("gm_pkg_op_item_control_rpt.gm_fch_txn_header", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.setString(2, strTxnType);
		gmDBManager.execute();
		hmRuturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		
		log.debug(" Records is " +hmRuturn);
		return hmRuturn;
	}
	
	/*
	 * function to fetch access flags for part label and QC events.
	 *@author Dsandeep
	 * 
	 */
	public HashMap fetchPartLabelParams(String strTxnId) throws AppError {
		
		HashMap hmResult = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_partlabeling_rpt.gm_fch_partlabel_parameters", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.execute();
		hmResult = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug(" Records is " +hmResult);
		return hmResult;
	}
	
	/*
	 * function to fetch Transaction Header Info.
	 *@author Gopinathan
	 * 
	 */
	public ArrayList fetchTransDetailInfo(String strTxnId, String strTxnType,String strLocType) throws AppError {
		
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString("gm_pkg_op_item_control_rpt.gm_fch_txn_detail", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.setString(2, strTxnType);
		gmDBManager.setString(3, strLocType);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		
		log.debug(" Records is " +alReturn);
		return alReturn;
	}
	
	
	/*
	 * function to fetch item location Details
	 *@author Gopinathan
	 * 
	 */
	public String getItemLocationStr(ArrayList alParam) throws AppError {
		
		ArrayList alPartDtl = new ArrayList();
		String strPart = "";  
		String strTempPart = ""; 
		String strPnums = "";
		alPartDtl = GmCommonClass.parseNullArrayList(alParam);
		//strTxnType = GmCommonClass.parseNull((String)hmParams.get("TXNTYPE")); //It can come as pnumid, pnum
		
		for(int i =0;i < alPartDtl.size();i++)
		{
			HashMap hmPartDtl = (HashMap) alPartDtl.get(i);
			strPart = GmCommonClass.parseNull((String)hmPartDtl.get("ID")); //It can come as pnumid, pnum, id	    			
			if(!strTempPart.equals(strPart)){
				strPnums = strPnums+strPart+","; //prepare string like 124.000,124.465,124.003
				strTempPart = strPart;
			}
		}   
		log.debug("====strPnums===="+strPnums);
		return strPnums;
	} 
	
	/*
	 * function to fetch the Location Part mapping details.
	 *@author Gopinathan
	 * 
	 */
	public ArrayList fetchPartLocationDtl(String strTxnId,String strPnums, String strTxnType, String strPickPutFl, String strRelFlag) throws AppError {
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		log.debug(" strPnums is " +strPnums);
		
		gmDBManager.setPrepareString("gm_pkg_op_item_control_rpt.gm_fch_item_loc_dtl", 6);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.setString(2, strPnums);
		gmDBManager.setString(3, strTxnType);
		gmDBManager.setString(4, strPickPutFl);
		gmDBManager.setString(5, strRelFlag);
		gmDBManager.execute();
		ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		gmDBManager.close();
		log.debug(" Records is " +alReturn);
		return alReturn;
	}
	
	/**
	 * fetchFGBinId - This method is used to fetch the FG Bin List which is already saved
	 * @param String strFgBinId
	 * @return String
	 * @exception AppError
	 */
	public String fetchFGBinId(String strFgBinId) throws AppError {
		
		String strFGBinList;
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_item_control_rpt.gm_fch_fgbin_dtls", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strFgBinId);
		gmDBManager.execute();
		strFGBinList = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strFGBinList;
	}
	
	/**
	     * validateControlNoPart - This method is used to validate control number scanned/entered. Returns message with Part, Lot, txn warehouse, Lot's current warehouse and validation message
	     * @param String strPartNo
	     * @param String strControlNo
	     * @param strTxnType
	     * @return String
	     */
	    public String validateControlNoPart(String strPartNo,String strControlNo,String strTxnType, String strMsg) throws AppError {
	        
	        GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	        gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_chk_control_scan", 4);
	        gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	        gmDBManager.setString(1, strControlNo);
	        gmDBManager.setString(2, strTxnType);
	        gmDBManager.setString(3, strPartNo);
	        gmDBManager.setString(4, strMsg);//Contains 'partnum:' || p_num || ',lotnum:' ||  p_control_num || ',prodmaterial:'  || p_prod_mtrl || ',txnwh:'  || get_code_name(p_txn_wh_type) || ',curwh:' || get_code_name(p_cur_wh_type) || ',msg:' ||  p_out_rtn_msg;
	                    
	        gmDBManager.execute();
	        
	        strMsg = GmCommonClass.parseNull(gmDBManager.getString(4));// Getting the validation message
	        gmDBManager.close();
	        
	        return strMsg;
	    }
	    
	    /**
		   * fetchInsertDetails():function to fetch the Insert id and revision number for the part numbers based on the txn
		   * 
		   * @throws AppError
		   * @Author Karthik
		   */
		public ArrayList fetchInsertDetails(String strTxnId,String strTxnType) throws AppError {
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
			 String strCompanyId = getCompId();
			 String strPlantId = getGmDataStoreVO().getPlantid();
			 String strInsertTxnTyp = gmItemControlTxnBean.getInsertTransactionType(strTxnType,strPlantId);
			 
			gmDBManager.setPrepareString("gm_pkg_op_fch_insert_details.gm_fetch_insert_dtl", 3);
			gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
			gmDBManager.setString(1, strTxnId);
			gmDBManager.setString(2, strInsertTxnTyp);
			gmDBManager.execute();
			ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
			gmDBManager.close();
			log.debug(" alReturn " +alReturn);
			return alReturn;
		}
		  
		  /**
		   * fetchInsertLocationInfo():unction to fetch the active/mapped locations for the Insert id
		   * 
		   * @throws AppError
		   * @Author Karthik
		   */
			public ArrayList fetchInsertLocationInfo(String strInsertId,String strWHType) throws AppError {
				GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
				GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
				
				gmDBManager.setPrepareString("gm_pkg_op_fch_insert_details.gm_fch_insert_Location_details", 3);
				gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
				gmDBManager.setString(1, strInsertId);
				gmDBManager.setString(2, strWHType);
				gmDBManager.execute();
				ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
				gmDBManager.close();
				log.debug(" alReturn " +alReturn);
				return alReturn;
			}
			
			/**
			   * getTransactionID - This method will get the Transaction ID for the passing PSFG ID
			   * 
			   * @param String strRequestId
			   * @exception AppError
			   */
			  public String getTransactionID(String strRequestId) throws AppError {
				    
				    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
				    gmDBManager.setPrepareString("gm_pkg_op_fch_insert_details.gm_fch_PSFG_ref_id", 2);
				    gmDBManager.setString(1, strRequestId);
				    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
				    gmDBManager.execute();
				    String strTxnID = GmCommonClass.parseNull(gmDBManager.getString(2));
				    gmDBManager.commit();
				    return strTxnID;
				  }
}
