package com.globus.operations.itemcontrol.beans;

import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmItemControlTxnBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

  /*
   * function to save the Location Part mapping details.
   * 
   * @author Gopinathan
   */
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmItemControlTxnBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmItemControlTxnBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public void saveItemControlMain(HashMap hmParams) throws AppError {

    log.debug("========hmParams=========" + hmParams);
    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TXNTYPE"));
    String strRelVerFl = GmCommonClass.parseNull((String) hmParams.get("RELVERFL"));
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strLocType = GmCommonClass.parseNull((String) hmParams.get("LOCTYPE"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strComments = GmCommonClass.parseNull((String) hmParams.get("TXT_LOGREASON"));
    String strValidChk = GmCommonClass.parseNull((String) hmParams.get("VALIDCHK"));


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    hmParams.put("CONNECTION", gmDBManager);// Getting the connection to a hashmap
    gmDBManager.setPrepareString("gm_pkg_op_item_control_txn.gm_sav_item_control_main", 6);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strRelVerFl);
    gmDBManager.setString(4, strInputString);
    gmDBManager.setString(5, strLocType);
    gmDBManager.setString(6, strUserID);
    gmDBManager.execute();

    // Save method is used to save the FG Bin details corresponding to the transactions
    saveFGBinDetails(hmParams);
    // If the comment is already save once, no need to save it again for the same submit
    if (!strComments.equals("") && !strValidChk.equals("Y")) {
      gmCommonBean.saveLog(gmDBManager, strTxnId, strComments, strUserID, "1268");
    }
    gmDBManager.commit();

  }

  /*
   * function to save the Location Part mapping details.
   * 
   * @author Gopinathan
   */
  public void saveItemVerifyMain(HashMap hmParams) throws AppError {
	  
    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TXNTYPE"));
    String strRelVerFl = GmCommonClass.parseNull((String) hmParams.get("RELVERFL"));
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strLocType = GmCommonClass.parseNull((String) hmParams.get("LOCTYPE"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    
    log.debug("========strTxnId=========" + strTxnId);
    log.debug("========strTxnType=========" + strTxnType);
    log.debug("========strInputString=========" + strInputString);
    log.debug("========strUserID=========" + strUserID);
    log.debug("========strRelVerFl=========" + strRelVerFl);
    log.debug("========strLocType=========" + strLocType);
    GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    
    gmDBManager.setPrepareString("gm_pkg_op_item_control_txn.gm_sav_item_verify_main", 5);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, strLocType);
    gmDBManager.setString(5, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
   
  }

  /*
   * saveFGBinDetails - To call the save method to save the FG Bin details corresponding to the
   * transaction
   * 
   * @param HashMap
   * 
   * @return void
   * 
   * @exception AppError
   */
  public void saveFGBinDetails(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = null;
    String strFGBinString = GmCommonClass.parseNull((String) hmParams.get("FGBINSTRING"));
    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    gmDBManager = (GmDBManager) hmParams.get("CONNECTION");
    gmDBManager.setPrepareString("gm_pkg_op_item_control_txn.gm_sav_fgbin_dtls", 3);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strFGBinString);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();

  }

  /*
   * saveItemLotNum - Method to call the procedure to validate the lot numbers save the lot numbers
   * in t5055
   * 
   * @param HashMap
   * 
   * @return String
   * 
   * @exception AppError
   */
  public String saveItemLotNum(HashMap hmParams) throws AppError {

    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TXNTYPE"));
    String strRelVerFl = GmCommonClass.parseNull((String) hmParams.get("RELVERFL"));
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
    String strLocType = GmCommonClass.parseNull((String) hmParams.get("LOCTYPE"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strComments = GmCommonClass.parseNull((String) hmParams.get("TXT_LOGREASON"));
    String strValidChk = GmCommonClass.parseNull((String) hmParams.get("VALIDCHK"));
    String strErrFl = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    hmParams.put("CONNECTION", gmDBManager);// Getting the connection to a hashmap
    gmDBManager.setPrepareString("gm_pkg_op_item_control_txn.gm_sav_item_lot_num", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, strLocType);
    gmDBManager.setString(5, strUserID);
    gmDBManager.setString(6, strRelVerFl);
    gmDBManager.execute();
    strErrFl = GmCommonClass.parseNull(gmDBManager.getString(7));
    log.debug("strErrFl>>>>" + strErrFl);
    // Save method is used to save the FG Bin details corresponding to the transactions
    saveFGBinDetails(hmParams);
    // If the comment is already save once, no need to save it again for the same submit
    if (!strComments.equals("") && !strValidChk.equals("Y")) {
      gmCommonBean.saveLog(gmDBManager, strTxnId, strComments, strUserID, "1268");
    }
    gmDBManager.commit();
    return strErrFl;
  }
  
  /*
   * saveInsertTransactionDetails - Method to call the procedure to SAve the Insert Transaction Details
   * in T5056
   * @param HashMap
   * @return String
   * @exception AppError
   */
  public void saveInsertTransactionDetails(HashMap hmParams) throws AppError {

    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TXNTYPE"));
    String strInsertId = GmCommonClass.parseNull((String) hmParams.get("INSERTID"));
    String strRevNum = GmCommonClass.parseNull((String) hmParams.get("REVNUM"));
    String strLocation = GmCommonClass.parseNull((String) hmParams.get("LOCATION"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strvoidfl = GmCommonClass.parseNull((String) hmParams.get("CHECKFL"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_sav_insert_txn.gm_upd_insert_trans_details", 7);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strInsertId);
    gmDBManager.setString(4, strRevNum);
    gmDBManager.setString(5, strLocation);
    gmDBManager.setString(6, strUserID);
    gmDBManager.setString(7, strvoidfl);
    gmDBManager.execute();
    gmDBManager.commit();
  }
  
  /**
   *getInsertTransactionType(): This method will fetch the access code id for the code id
   * 
   * @author karthik
   * @param String
   */
  public String getInsertTransactionType(String strTxnType,String strPlantId) throws AppError {

    String strInsertTxnType = "";
    String strRuleGrp = "INSERT_TXN_MAP"+"_"+strPlantId;
    
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_rule_value", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strRuleGrp);
    gmDBManager.execute();
    strInsertTxnType = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strInsertTxnType;
  }

  /*
   * saveInsertByJMS - Method to call the procedure to Save the Insert Transaction Details
   * by using JMS call
   * @param HashMap
   * @exception AppError
   */
  public void saveInsertByJMS(HashMap hmParams) throws AppError {
	  
	  	GmItemControlTxnBean gmItemControlTxnBean = new GmItemControlTxnBean(getGmDataStoreVO());
	  	String strTxnID = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
	  	String strTxnType =  GmCommonClass.parseNull((String) hmParams.get("TXNTYPE"));
	  	String strStatus =  GmCommonClass.parseNull((String) hmParams.get("STATUS"));
	    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
	    String strCompanyInfo = GmCommonClass.parseNull((String) hmParams.get("COMPANYINFO"));
	    String strConsgnIdStr = GmCommonClass.parseNull((String) hmParams.get("TXNIDSTR"));
	    
	    // To Save the Insert part number details in New order screen Using JMS
	    HashMap hmJMSParam = new HashMap();
	    String strCompanyId = getCompId();
	    String strPlantId = getGmDataStoreVO().getPlantid();
	    String strConsumerClass = GmCommonClass.parseNull(GmCommonClass.getString("INSERT_CONSUMER_CLASS"));
	    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_INSERT_LOC_QUEUE"));
	    String strInsertTxnTyp = gmItemControlTxnBean.getInsertTransactionType(strTxnType,strPlantId);
	    String strInsertfl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlantId, "INSERT_PART"));
	    String strInsertTransfl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strInsertTxnTyp, "INSERT_TRANS"));
	    
	    /* checking whether the Company and Transaction is enabled for the Insert
	     */
	    if (strInsertfl.equalsIgnoreCase("Y") && strInsertTransfl.equalsIgnoreCase("Y"))
	    {
	    	hmJMSParam.put("REFID", strTxnID);
	    	hmJMSParam.put("TRANSTYP", strInsertTxnTyp);
	    	hmJMSParam.put("STATUS", strStatus);
	    	hmJMSParam.put("USERID", strUserID);
	    	hmJMSParam.put("COMPANYINFO", strCompanyInfo);
	    	hmJMSParam.put("TXNIDSTR", strConsgnIdStr);
		
		    // This is JMS Code which will process the notification, jms/gminsertqueue is the JMS
		    // queue using for notification
		    GmQueueProducer qprod = new GmQueueProducer();
		    GmMessageTransferObject tf = new GmMessageTransferObject();
		    tf.setMessageObject(hmJMSParam);
		    tf.setConsumerClass(strConsumerClass);
		    qprod.sendMessage(tf, strQueueName);
	    }
   
  }
  
  /**
   * saveInsertTransaction():save insert details for the transaction in JMS Consumer Job file
   * 
   * @throws AppError
   * @Author Karthik
   */
  public void saveInsertTransaction(String strRefID,String strTransType ,String strStatus,String strUserId,
		  String strScreentyp,String strConsignSTR)
      throws AppError {
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	strConsignSTR = strConsignSTR.equals("") ? strRefID : strConsignSTR;
	
    gmDBManager.setPrepareString("gm_pkg_op_sav_insert_txn.gm_sav_insert_txn_dtl", 5);
    gmDBManager.setString(1, strConsignSTR);
    gmDBManager.setString(2, strTransType);
    gmDBManager.setString(3, strStatus);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strScreentyp);
    gmDBManager.execute();
    gmDBManager.commit();
  }
  
  /*
   * unAssignTransaction - UnAssigns the transaction in the pick table
   * @param HashMap
   * PMT-39215 
   * @exception AppError
   */
  public void unAssignTransaction(HashMap hmParam) throws AppError {
    String strtxnid = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_sav_insert_txn.gm_unassign_transaction", 2);
    gmDBManager.setString(1, strtxnid);
    gmDBManager.setString(2, strUserId); 
    gmDBManager.execute();
    gmDBManager.commit();
  } 
}
