package com.globus.operations.itemcontrol.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmLogForm;

public class GmItemControlForm extends GmLogForm {

  private String mode = "";
  private String txnid = "";
  private String txntype = "";
  private String purpose = "";
  private String refid = "";
  private String setname = "";
  private String cname = "";
  private Date cdate = new Date();
  private Date surgerydate = new Date();
  private String comments = "";
  private String verifyfl = "";
  private String txntypenm = "";
  private String strpnums = "";
  private String inputString = "";
  private String relverfl = "";
  private String loctype = "";
  private String statusfl = "";
  private String pickputfl = "";
  private String contype = "";
  private String isTxnVerified = "";
  private String scanId = "";
  private String AssignedToCombo = "";
  private String retMsg = "";
  private String strFGBinFl = "";
  private String fgBinString = "";
  private String strFGBinList = "";
  private String controlNo = "";
  private String txnStatus = "";
  private String errCnt = "";
  private String chkRule = "No";
  private String distname = "";
  private String inserFl = "";
  private String transInsertFl = "";
  private String location = "";
  private String checkFl = "";
  private String insertid = "";
  private String revnum = "";

  private ArrayList aldetailinfo = new ArrayList();
  private ArrayList allocdtl = new ArrayList();
  private ArrayList alInsertDetails = new ArrayList();
  private ArrayList alInsertlocdtl = new ArrayList();

  public String getControlNo() {
    return controlNo;
  }

  public void setControlNo(String controlNo) {
    this.controlNo = controlNo;
  }

  public String getRetMsg() {
    return retMsg;
  }

  public void setRetMsg(String retMsg) {
    this.retMsg = retMsg;
  }

  public String getMode() {
    return mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }

  public String getPickputfl() {
    return pickputfl;
  }

  public void setPickputfl(String pickputfl) {
    this.pickputfl = pickputfl;
  }

  public Date getCdate() {
    return cdate;
  }

  public void setCdate(Date cdate) {
    this.cdate = cdate;
  }

  public Date getSurgerydate() {
    return surgerydate;
  }

  public void setSurgerydate(Date surgerydate) {
    this.surgerydate = surgerydate;
  }

  public String getStatusfl() {
    return statusfl;
  }

  public void setStatusfl(String statusfl) {
    this.statusfl = statusfl;
  }

  public String getRelverfl() {
    return relverfl;
  }

  public void setRelverfl(String relverfl) {
    this.relverfl = relverfl;
  }

  public String getLoctype() {
    return loctype;
  }

  public void setLoctype(String loctype) {
    this.loctype = loctype;
  }

  public String getInputString() {
    return inputString;
  }

  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  public ArrayList getAllocdtl() {
    return allocdtl;
  }

  public void setAllocdtl(ArrayList allocdtl) {
    this.allocdtl = allocdtl;
  }

  public String getStrpnums() {
    return strpnums;
  }

  public void setStrpnums(String strpnums) {
    this.strpnums = strpnums;
  }

  public ArrayList getAldetailinfo() {
    return aldetailinfo;
  }

  public void setAldetailinfo(ArrayList aldetailinfo) {
    this.aldetailinfo = aldetailinfo;
  }

  public String getTxntypenm() {
    return txntypenm;
  }

  public void setTxntypenm(String txntypenm) {
    this.txntypenm = txntypenm;
  }

  public String getPurpose() {
    return purpose;
  }

  public void setPurpose(String purpose) {
    this.purpose = purpose;
  }

  public String getRefid() {
    return refid;
  }

  public void setRefid(String refid) {
    this.refid = refid;
  }

  public String getSetname() {
    return setname;
  }

  public void setSetname(String setname) {
    this.setname = setname;
  }

  public String getCname() {
    return cname;
  }

  public void setCname(String cname) {
    this.cname = cname;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getVerifyfl() {
    return verifyfl;
  }

  public void setVerifyfl(String verifyfl) {
    this.verifyfl = verifyfl;
  }

  public String getTxnid() {
    return txnid;
  }

  public void setTxnid(String txnid) {
    this.txnid = txnid;
  }

  public String getTxntype() {
    return txntype;
  }

  public void setTxntype(String txntype) {
    this.txntype = txntype;
  }

  public String getContype() {
    return contype;
  }

  public void setContype(String contype) {
    this.contype = contype;
  }

  public String getIsTxnVerified() {
    return isTxnVerified;
  }

  public void setIsTxnVerified(String isTxnVerified) {
    this.isTxnVerified = isTxnVerified;
  }

  /**
   * @return the scanId
   */
  public String getScanId() {
    return scanId;
  }

  /**
   * @param scanId the scanId to set
   */
  public void setScanId(String scanId) {
    this.scanId = scanId;
  }

  /**
   * @return the assignedToCombo
   */
  public String getAssignedToCombo() {
    return AssignedToCombo;
  }

  /**
   * @param assignedToCombo the assignedToCombo to set
   */
  public void setAssignedToCombo(String assignedToCombo) {
    AssignedToCombo = assignedToCombo;
  }

  /**
   * @return the strFGBinFl
   */
  public String getStrFGBinFl() {
    return strFGBinFl;
  }

  /**
   * @param strFGBinFl the strFGBinFl to set
   */
  public void setStrFGBinFl(String strFGBinFl) {
    this.strFGBinFl = strFGBinFl;
  }

  /**
   * @return the fgBinString
   */
  public String getFgBinString() {
    return fgBinString;
  }

  /**
   * @param fgBinString the fgBinString to set
   */
  public void setFgBinString(String fgBinString) {
    this.fgBinString = fgBinString;
  }

  /**
   * @return the strFGBinList
   */
  public String getStrFGBinList() {
    return strFGBinList;
  }

  /**
   * @param strFGBinList the strFGBinList to set
   */
  public void setStrFGBinList(String strFGBinList) {
    this.strFGBinList = strFGBinList;
  }

  /**
   * @return the txnStatus
   */
  public String getTxnStatus() {
    return txnStatus;
  }

  /**
   * @param txnStatus the txnStatus to set
   */
  public void setTxnStatus(String txnStatus) {
    this.txnStatus = txnStatus;
  }

  /**
   * @return the errCnt
   */
  public String getErrCnt() {
    return errCnt;
  }

  /**
   * @param errCnt the errCnt to set
   */
  public void setErrCnt(String errCnt) {
    this.errCnt = errCnt;
  }

  /**
   * @return the chkRule
   */
  public String getChkRule() {
    return chkRule;
  }

  /**
   * @param chkRule the chkRule to set
   */
  public void setChkRule(String chkRule) {
    this.chkRule = chkRule;
  }

  public String getDistname() {
    return distname;
  }

  public void setDistname(String distname) {
    this.distname = distname;
  }

/**
 * @return the alInsertDetails
 */
  public ArrayList getAlInsertDetails() {
	return alInsertDetails;
}

/**
 * @param alInsertDetails the alInsertDetails to set
 */
  public void setAlInsertDetails(ArrayList alInsertDetails) {
	this.alInsertDetails = alInsertDetails;
}

/**
 * @return the inserFl
 */
public String getInserFl() {
	return inserFl;
}

/**
 * @param inserFl the inserFl to set
 */
public void setInserFl(String inserFl) {
	this.inserFl = inserFl;
}

/**
 * @return the transInsertFl
 */
public String getTransInsertFl() {
	return transInsertFl;
}

/**
 * @param transInsertFl the transInsertFl to set
 */
public void setTransInsertFl(String transInsertFl) {
	this.transInsertFl = transInsertFl;
}

/**
 * @return the alInsertlocdtl
 */
public ArrayList getAlInsertlocdtl() {
	return alInsertlocdtl;
}

/**
 * @param alInsertlocdtl the alInsertlocdtl to set
 */
public void setAlInsertlocdtl(ArrayList alInsertlocdtl) {
	this.alInsertlocdtl = alInsertlocdtl;
}

/**
 * @return the location
 */
public String getLocation() {
	return location;
}

/**
 * @param location the location to set
 */
public void setLocation(String location) {
	this.location = location;
}

/**
 * @return the checkFl
 */
public String getCheckFl() {
	return checkFl;
}

/**
 * @param checkFl the checkFl to set
 */
public void setCheckFl(String checkFl) {
	this.checkFl = checkFl;
}

/**
 * @return the insertid
 */
public String getInsertid() {
	return insertid;
}

/**
 * @param insertid the insertid to set
 */
public void setInsertid(String insertid) {
	this.insertid = insertid;
}

/**
 * @return the revnum
 */
public String getRevnum() {
	return revnum;
}

/**
 * @param revnum the revnum to set
 */
public void setRevnum(String revnum) {
	this.revnum = revnum;
}


}
