package com.globus.operations.itemcontrol.forms;
import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmInitiateTransferForm extends GmCommonForm{
	private String  strXMLGrid="";
	private String strTxnIds = "";
	
	private String warehouseid = "";
	
	private ArrayList alWareHouse = new ArrayList();
	
	public ArrayList getAlWareHouse() {
		return alWareHouse;
	}

	public void setAlWareHouse(ArrayList alWareHouse) {
		this.alWareHouse = alWareHouse;
	}

	public String getWarehouseid() {
		return warehouseid;
	}

	public void setWarehouseid(String warehouseid) {
		this.warehouseid = warehouseid;
	}

	public String getStrTxnIds() {
		return strTxnIds;
	}

	public void setStrTxnIds(String strTxnIds) {
		this.strTxnIds = strTxnIds;
	}

	public String getStrXMLGrid() {
		return strXMLGrid;
	}

	public void setStrXMLGrid(String strXMLGrid) {
		this.strXMLGrid = strXMLGrid;
	}

}
