package com.globus.operations.ttpbyvendor.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmTTPbyVendorReportBean - this action used to fetch the TTP By Vendor period summary report Details
 * 
 * @author asingaravel
 *
 */
public class GmTTPbyVendorReportBean extends GmBean {

		public GmTTPbyVendorReportBean() {
			super(GmCommonClass.getDefaultGmDataStoreVO());
		}
	
		public GmTTPbyVendorReportBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
			super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
		}
		
		Logger log = GmLogger.getInstance(this.getClass().getName());
		
		/**
		 * fetchTTPbyVendorSummary - This Method Used to fetch the TTP By vendor summary details based on vendor id or ttp id
		 * 
		 * @param hmParam
		 * @return
		 * @throws AppError
		 */
		public String fetchTTPbyVendorSummary(HashMap hmParam)
				throws AppError {
	
			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
	
			String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
			String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTHID"));
			String strYear = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
			String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
			String strDate = strMonth.concat("/").concat(strYear);
			String strTTPSummaryDtlsJSONString = "";
	
			gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_rpt.gm_fch_ttp_by_vendor_summary_rpt",4);
			gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
			gmDBManager.setString(1, strTTPId);
			gmDBManager.setString(2, strVendorId);
			gmDBManager.setString(3, strDate);
			gmDBManager.execute();
			strTTPSummaryDtlsJSONString = GmCommonClass.parseNull(gmDBManager.getString(4));
			gmDBManager.close();
	
			return strTTPSummaryDtlsJSONString;
	    }
		
		/**fetchTTPVendorByDashboardDtls:This fetch used to get dashboard details
		 * @param hmParam
		 * @return
		 * @throws AppError
		 */
		public String fetchTTPVendorByDashboardDtls(HashMap hmParam) throws AppError {

			GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
			
			//Initialize
			String strTTPId = "";
			String strStatusId = "";
			String strLockPeriod = "";
			String strJsonString = "";
			String strMonth="";
			String strYear="";
			
			strTTPId = GmCommonClass.parseZero((String) hmParam.get("TTPID"));
			strStatusId = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
			strMonth=GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
		    strYear=GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));		
			strLockPeriod = strMonth.concat("/").concat(strYear);
			
			gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_rpt.gm_fch_capacity_dashboard_dtls", 4);
			gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
			gmDBManager.setString(1, strTTPId);
			gmDBManager.setString(2, strStatusId);
			gmDBManager.setString(3, strLockPeriod);
			gmDBManager.execute();
			
			//Get the ttp summary screen vendor approval details
			strJsonString = GmCommonClass.parseNull(gmDBManager.getString(4));
			gmDBManager.close();
			
			return strJsonString;

		}
		
		/**fetchTTPbyVendorPartDetail fetch used to get vendor part details
         * @param hmParam
         * @return
         * @throws AppError
         */
        public String fetchTTPbyVendorPartDetail(HashMap hmParam) throws AppError{
            
            GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
           
            String strTTPId = GmCommonClass.parseNull((String)hmParam.get("TTPID"));
            String strMonth = GmCommonClass.parseNull((String)hmParam.get("MONTHID"));
            String strYear = GmCommonClass.parseNull((String) hmParam.get("YEARID"));
            String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
            String strDate = strMonth.concat("/").concat(strYear);
            String strTTPPartRptDtlsJSONString = "";
           
            gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_rpt.gm_fch_ttp_by_vendor_part_rpt", 4);
            gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
            gmDBManager.setString(1, strTTPId);
            gmDBManager.setString(2, strVendorId);
            gmDBManager.setString(3, strDate);
            gmDBManager.execute();
           
            strTTPPartRptDtlsJSONString = GmCommonClass.parseNull(gmDBManager.getString(4));
            gmDBManager.close();
            return strTTPPartRptDtlsJSONString;   
        }
}


