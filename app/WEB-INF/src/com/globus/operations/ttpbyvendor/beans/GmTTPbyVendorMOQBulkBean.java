package com.globus.operations.ttpbyvendor.beans;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**GmTTPbyVendorMOQBulkBean:This class how a user will upload part minimum order qty for each vendor
 * @author pvigneshwaran
 *
 */
public class GmTTPbyVendorMOQBulkBean extends GmBean {

	/**
	 * 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmTTPbyVendorMOQBulkBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**saveTTPbyVendorMOQBulkUpload:This Method used to save minimum order Quantity upload Details 
	 * in t3010_vendor_moq table
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String saveTTPbyVendorMOQBulkUpload(HashMap hmParam) throws AppError {


		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

		String strJsonString = "";

		String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBERS"));
		String strInputstr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		
		
		gmDBManager.setPrepareString("gm_pkg_op_ttp_by_vendor_moq_bulk.gm_process_ttp_by_vendor_moq_bulk",5);

		gmDBManager.registerOutParameter(5, OracleTypes.CLOB);

		gmDBManager.setString(1, strVendorId);
		gmDBManager.setString(2, strPartNumbers);
		gmDBManager.setString(3, strInputstr);
		gmDBManager.setString(4, strUserId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(5));
		// Code to save the log information (comments) from the screen
		if (!strLogReason.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strVendorId,
					strLogReason, strUserId, "26240845");// 26240845 is the code id for
														// Vendor MOq Type in the
														// log table. 
		}
		
		gmDBManager.commit();

		return strJsonString;
	}

	/**
	 * fetchVendorMOQReportDtls - This method used to fetch the vendor MOQ
	 * Details Based on vendor,division,part number,project name
	 * Screen Location - GOP--Report--TTP By Vendor--vendor MOQ Report
	 * @param hmparam
	 * @return strJsonString
	 * @throws AppError
	 */
	public String fetchTTPbyVendorMOQReport(HashMap hmparam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		log.debug("hmparam=>" + hmparam);
		String strJsonString = "";
		String strVendorId = GmCommonClass.parseNull((String) hmparam.get("VENDORID"));
		String stDivisionId = GmCommonClass.parseZero((String) hmparam.get("DIVISIONID"));
		String strPartnum = GmCommonClass.parseNull((String) hmparam.get("PART"));
		String strProjectId = GmCommonClass.parseNull((String) hmparam.get("PROJECTID"));
		String strPurchaseAgent = GmCommonClass.parseZero((String) hmparam.get("PURCHASEAGENT"));

		gmDBManager.setPrepareString("gm_pkg_op_ttp_by_vendor_moq.gm_fch_ttp_by_vendor_moq_rpt",6);
	
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1, strVendorId);
		gmDBManager.setString(2, strPartnum);
		gmDBManager.setString(3, strProjectId);
		gmDBManager.setString(4, stDivisionId);
		gmDBManager.setString(5, strPurchaseAgent);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();
		return strJsonString;
	}

	/**
	 * fetchTTPbyVendoeMOQLog - this method used to fetch the ttp by vendor moq
	 * log history details
	 * Screen Location - GOP--Report--TTP By Vendor--vendor MOQ Report
	 * @param strVendorMOQId
	 * @return
	 * @throws AppError
	 */
	public String fetchTTPbyVendoeMOQLog(String strVendorMOQId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		String strJsonString = "";
		gmDBManager.setPrepareString("gm_pkg_op_ttp_by_vendor_moq.gm_fch_ttp_by_vendor_moq_log",2);

		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strVendorMOQId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		
		return strJsonString;
	}

}