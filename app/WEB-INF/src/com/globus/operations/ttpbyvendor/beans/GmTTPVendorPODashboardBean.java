package com.globus.operations.ttpbyvendor.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmTTPVendorPODashboardBean - This bean class used to fetch the Vendor PO Details from T4058_TTP_VENDOR_CAPACITY_SUMMARY table.
 * 					
 * 								 
 * 
 * @author rselvaraj
 *
 */
public class GmTTPVendorPODashboardBean extends GmBean {
	
	/**
	 * this used for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/** This  Constructor will validate and populate company info
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmTTPVendorPODashboardBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	
	/**fetchVendorPODashboard method used to fetch PO vendor dashboard details
	 * @param strPriorityId
	 * @return
	 * @throws AppError
	 */
	public String fetchVendorPODashboard(HashMap hmparam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,
				getGmDataStoreVO());
		log.debug("hmparam=>" + hmparam);
		String strJsonString = "";
		String strVendorId = GmCommonClass.parseZero((String) hmparam
				.get("VENDORID"));
		String strLoadDate = GmCommonClass.parseNull((String) hmparam
				.get("STRLOADDATE"));
		String strStatusId = GmCommonClass.parseZero((String) hmparam
				.get("STATUSID"));
		gmDBManager
		.setPrepareString(
				"gm_pkg_oppr_ld_ttp_vendor_po_dashboard_rpt.gm_fch_vendor_po_dashboard_dtls",
				4);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strVendorId);
		gmDBManager.setString(2, strLoadDate);
		gmDBManager.setString(3, strStatusId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(4));
		log.debug("strJsonString=>" + strJsonString);
		gmDBManager.close();
		return strJsonString;
	}
	
	/**fetchVendorPODashboardFailure:Used to fetch  PO Generation Failed status hyperlink report.
	 * @param hmparam
	 * @return
	 * @throws AppError
	 */
	public String fetchVendorPODashboardFailure(HashMap hmparam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,
				getGmDataStoreVO());
		
		String strJsonString = "";
		String strCapaDtlsId = GmCommonClass.parseZero((String) hmparam.get("CAPADTLSID"));
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_vendor_po_dashboard_rpt.gm_fch_vendor_po_dashboard_failure",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strCapaDtlsId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strJsonString;
	}

	/**fetchVendorPODashboardSuccess - Used to fetch PO Generation Success status hyperlink report.
	 * @param strCapaDtlsId
	 * @return strPOSuccessJsonString
	 * @throws AppError
	 */
	public String fetchVendorPODashboardSuccess(String strCapaDtlsId) throws AppError{
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		String strPOSuccessJsonString ="";
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_vendor_po_dashboard_rpt.gm_fch_vendor_po_dashboard_success",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strCapaDtlsId);
		gmDBManager.execute();
		strPOSuccessJsonString = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		
		return strPOSuccessJsonString;
	}

	/**
	 * saveVendorPOTypeDtls - This method used to save the PO type Screen
	 * location - GOP-->Transaction-->TTByVendor-->create PO by Vendor
	 * 
	 * @param strInputStr
	 * @param strUserId
	 * @return
	 * @throws AppError
	 */
	public String saveVendorPOTypeDtls(String strInputStr, String strUserId)
			throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,
				getGmDataStoreVO());
		String strJsonString = "";
		
		gmDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_sav_vendor_po_type_dtls", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(3));
		log.debug("strJsonString=>" + strJsonString);
		gmDBManager.commit();
		
		return strJsonString;
	}

	/**
	 * updateVendorPOStatus - this method used to update the po status as PO in
	 * progress Screen location - GOP-->Transaction-->TTByVendor-->create PO by
	 * Vendor
	 * 
	 * @param hmParam
	 * @return
	 */
	public String updateVendorPOStatus(HashMap hmParam) {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,
				getGmDataStoreVO());
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		String strJsonString = "";
		String strVendorCapaDtlsId = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_upd_bulk_vendor_po_status", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strVendorCapaDtlsId);
		gmDBManager.setInt(2, 26240821); //PO generation in progress
		gmDBManager.setInt(3, Integer.parseInt(strUserId));
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(4));
		log.debug("strJsonString=>" + strJsonString);
		gmDBManager.commit();
		
		// JMS Call for update the PO status as PO Generated or PO Failed

		String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean
						.getJmsConfig("VENDOR_PO_GENERATION_CONSUMER_JOB"));// jmsconsumerconfig
		String strQueueName = GmCommonClass.parseNull(GmCommonClass
				.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));// jms.properties

		hmParam.put("QUEUE_NAME", strQueueName);
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		hmParam.put("STATUS", "26240821");// PO in progress status id

		gmConsumerUtil.sendMessage(hmParam);

		return strJsonString;
	}

}
