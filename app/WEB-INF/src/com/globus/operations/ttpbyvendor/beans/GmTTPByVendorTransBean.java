package com.globus.operations.ttpbyvendor.beans;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.valueobject.common.GmDataStoreVO;
/**
 * @author pvigneshwaran
 *
 */
public class GmTTPByVendorTransBean extends GmBean {

	/**
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmTTPByVendorTransBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**approveTTPbyVendorSummaryDetail:This method used to approve the vendor
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String approveTTPbyVendorSummaryDetail(HashMap hmParam)
			throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());

		//
		String strMonth = GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
		String strYear = GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));
		String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		String strDate = strMonth.concat("/").concat(strYear);
		
		String strApproveString = "";
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		//
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_upd_vendor_capacity_approval",4);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strVendorId);
		gmDBManager.setString(2, strDate);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		strApproveString = GmCommonClass.parseNull(gmDBManager.getString(4));
		
		gmDBManager.commit();

		return strApproveString;
	}
	
	/**
	 * saveTTPbyVendorSummaryDetail:This method used to Update Override
	 * Percentage and RaisePoQty
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String saveTTPbyVendorSummaryDetail(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,
				getGmDataStoreVO());
		GmTTPByVendorTransBean gmTTPByVendorTransBean = new GmTTPByVendorTransBean(
				getGmDataStoreVO());
		String strSaveErrorString = "";
		String strInputstr = GmCommonClass.parseNull((String) hmParam
				.get("INPUTSTRING"));
		String strMonth = GmCommonClass.parseNull((String) hmParam
				.get("TTPMONTH"));
		String strYear = GmCommonClass.parseNull((String) hmParam
				.get("TTPYEAR"));
		String strDate = strMonth.concat("/").concat(strYear);
		String strUserId = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_override_split_qty_dtls",4);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strInputstr);
		gmDBManager.setString(2, strDate);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		strSaveErrorString = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();
		if (strSaveErrorString.equals("")) {
			// This method used to send message for JMS Call
			gmTTPByVendorTransBean.processOverrideQty(hmParam);
		}

		return strSaveErrorString;
	}

	/**
	 * processOverrideQty:This method used to send message for JMS Call
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void processOverrideQty(HashMap hmParam) throws AppError {

		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		String strQueueName = GmCommonClass.parseNull(GmCommonClass
				.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));// jms.properties
		hmParam.put("QUEUE_NAME", strQueueName);
		// TTP_VENDOR_OVERRIDE_CONSUMER_CLASS is mentioned in JMSConsumerConfig.
		// properties file.This key contain the
		// path for :
		// com.globus.jms.consumers.processors.GmTTPApprovalConsumerJob
		String strConsumerClass = GmCommonClass
				.parseNull(GmJMSConsumerConfigurationBean
						.getJmsConfig("TTP_VENDOR_OVERRIDE_CONSUMER_CLASS"));// jmsconsumerconfig
		hmParam.put("CONSUMERCLASS", strConsumerClass);
		log.debug("strQueueName=>" + strQueueName + " strConsumerClass=>"
				+ strConsumerClass + " hmParam=>>>" + hmParam);
		gmConsumerUtil.sendMessage(hmParam);
	}
	/** lockTTPbyVendorDashboard - this method used to update the TTp vendor Staus As Lock in progress 
	 * @param hmParam
	 * @return strJsonString
	 * @throws AppError
	 */
	public String lockTTPbyVendorDashboard(HashMap hmParam) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		HashMap hmJMSParam = new HashMap();
	
		// Initialize
		String strTTPIdString = "";
		String strStatusId = "";
		String strLockPeriod = "";
		String strJsonString = "";
		String strUserId = "";
		String strQueueName = "";
		String strConsumerClass = "";
		
		strTTPIdString = GmCommonClass.parseZero((String) hmParam.get("TTPID"));
		strStatusId = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		strLockPeriod = GmCommonClass.parseNull((String) hmParam.get("LOCKPERIOD"));
		strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strCompanyInfo = GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO"));
		

		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_lock.gm_upd_ttp_by_vendor_lock",4);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strTTPIdString);
		gmDBManager.setString(2, strLockPeriod);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		strJsonString = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();

//		if (strJsonString.equals("")) {
//			// Called JMS For update the status from Lock in progress to Locked or locke failed Status.
//			strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_TTP_SUMMARY_QUEUE"));
//			hmJMSParam.put("QUEUE_NAME", strQueueName);
//			strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("TTP_LOCK_CONSUMER_CLASS"));
//			hmJMSParam.put("CONSUMERCLASS", strConsumerClass);
//			hmJMSParam.put("STATUS", "108863");//Lock In-Progress
//			hmJMSParam.put("HMPARAM", hmParam);
//			gmConsumerUtil.sendMessage(hmJMSParam);
//
//		}
		return strJsonString;
	}

	/** updateTTPbyVendorOrdQty - This methos used to update the PO order Qty
	 * @param hmReturn
	 * @throws AppError
	 */
	public void updateTTPbyVendorOrdQty(HashMap hmReturn) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		String strTTpDtlId = "";
		String strPartNum = "";
		String strLockPeriod = "";
		String strRaisePOQty = "";
		
		strTTpDtlId = GmCommonClass.parseZero((String) hmReturn.get("TTPDTLID"));
		strPartNum = GmCommonClass.parseNull((String) hmReturn.get("PARTNUM"));
		strLockPeriod = GmCommonClass.parseNull((String) hmReturn.get("LOCKPERIOD"));
		strRaisePOQty = GmCommonClass.parseNull((String) hmReturn.get("RAISEPOQTY"));
		String strTTPType = GmCommonClass.parseNull((String) hmReturn.get("TTPTYPE"));
		String strUserId = GmCommonClass.parseNull((String) hmReturn.get("USERID"));

		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_lock.gm_upd_ttp_by_vendor_order_qty",6);

		gmDBManager.setString(1, strTTpDtlId);
		gmDBManager.setString(2, strLockPeriod);
		gmDBManager.setString(3, strPartNum);
		gmDBManager.setString(4, strTTPType);
		gmDBManager.setString(5, strRaisePOQty);
		gmDBManager.setString(6, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}

	/** updateTTPbyVendorStatus - This method used to update the TTP vendor status as locked or lock failed
	 * @param hmDetail
	 */
	public void updateTTPbyVendorStatus(HashMap hmDetail) {
		
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		
		String strTTId = GmCommonClass.parseZero((String) hmDetail.get("TTPID"));
		String strLockPeriod = GmCommonClass.parseNull((String) hmDetail.get("LOCKPERIOD"));
		String strUserId = GmCommonClass.parseNull((String) hmDetail.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_lock.gm_upd_ttp_by_vendor_status",3);
		gmDBManager.setString(1, strTTId);
		gmDBManager.setString(2, strLockPeriod);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}

	/**
	 * updateTTPbyVendorPartDtls - This method used to save the part details to t4053 table
	 * 
	 * @param strTTPId
	 * @param strLockPeriod
	 * @param strStatus
	 * @param strUserId
	 * @throws AppError
	 */
	public void updateTTPbyVendorPartDtls(String strTTPId,String strLockPeriod, String strStatus, String strUserId)throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());

		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_lock.gm_sav_ttp_by_vendor_part_dtls",4);
		gmDBManager.setString(1, strTTPId);
		gmDBManager.setString(2, strLockPeriod);
		gmDBManager.setString(3, strStatus);
		gmDBManager.setString(4, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

	}
	
	
	/**rollBackTTPbyVendorSummaryDetail:This method used to rollback the vendor
	 * details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String rollBackTTPbyVendorSummaryDetail(HashMap hmParam)
			throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());

		//
		String strMonth = GmCommonClass.parseNull((String) hmParam.get("TTPMONTH"));
		String strYear = GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));
		String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		String strDate = strMonth.concat("/").concat(strYear);
		
		String strRollBackString = "";
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		//
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_txn.gm_upd_vendor_capacity_rollback",4);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strVendorId);
		gmDBManager.setString(2, strDate);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		strRollBackString = GmCommonClass.parseNull(gmDBManager.getString(4));
		
		gmDBManager.commit();

		return strRollBackString;
	}
	
}
