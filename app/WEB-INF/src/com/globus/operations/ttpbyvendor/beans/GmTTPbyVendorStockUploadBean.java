/**
 * 
 */
package com.globus.operations.ttpbyvendor.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

/** GmTTPbyVendorStockUploadBean: This class is used to validate part number and fetch ttp nm 
 * 								  and also update or insert safety stock qty
 * @author asingaravel
 *
 */
public class GmTTPbyVendorStockUploadBean extends GmBean{

	public GmTTPbyVendorStockUploadBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
	/**validateandFetchTTPName: This method used to validate part number and fetch primary ttp name
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */

	public String validateandFetchTTPName(HashMap hmParam) throws AppError {

		//GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		GmDBManager gmDMDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO()); 
		//Initialize
		String strJsonString = "";
		String strJsonPartErrString = "";
		String strJsonTTPNMString = "";
		
		gmDMDBManager.setPrepareString("gm_pkg_oppr_ld_safety_stock_txn.gm_fch_ttp_name", 5);
		gmDMDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDMDBManager.registerOutParameter(5, OracleTypes.CLOB);
		gmDMDBManager.setString(1,  GmCommonClass.parseNull((String) hmParam.get("PARTSTR")));
		//In INPUTSTR string contains part num,qty like 124.000,10|171.316,21
		gmDMDBManager.setString(2,  GmCommonClass.parseNull((String) hmParam.get("INPUTSTR")));
		gmDMDBManager.setString(3,  GmCommonClass.parseNull((String) hmParam.get("USERID")));
		gmDMDBManager.execute();
		
		//Json String
		strJsonPartErrString = GmCommonClass.parseNull(gmDMDBManager.getString(4));
		strJsonTTPNMString = GmCommonClass.parseNull(gmDMDBManager.getString(5));
		gmDMDBManager.commit();
		gmDMDBManager.close();
		
		strJsonString = strJsonPartErrString + '^' +strJsonTTPNMString;
	
		return strJsonString;
	}
	
	/**saveStockUpload: This method used to insert or update safety stock qty
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public void saveStockUpload(HashMap hmParam) throws AppError {

		//GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		GmDBManager gmDMDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO()); 
		//Initialize
		String strJsonString = "";
		String strJsonPartErrString = "";
		String strJsonTTPNMString = "";
		
		gmDMDBManager.setPrepareString("gm_pkg_oppr_ld_safety_stock_txn.gm_sav_safety_stock", 3);
		gmDMDBManager.setString(1,  GmCommonClass.parseNull((String) hmParam.get("PARTSTR")));
		//In INPUTSTR string contains part num,qty like 124.000,10|171.316,21
		gmDMDBManager.setString(2,  GmCommonClass.parseNull((String) hmParam.get("INPUTSTR")));
		gmDMDBManager.setString(3,  GmCommonClass.parseNull((String) hmParam.get("USERID")));
		gmDMDBManager.execute();
		
		gmDMDBManager.commit();
		gmDMDBManager.close();
		
	}
}
