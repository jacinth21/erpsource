package com.globus.operations.ttpbyvendor.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

/**
 * PMT-49117 - 5_DB_PO Creation By Vendor
 * 
 * @author tramasamy This class calling from GmVendorPOGenerationConsumerJob
 *         GmTTPVendorPOTransBean - This bean call used to update the PO status
 *         as PO generated or PO Failed
 * 
 */
public class GmTTPVendorPOTransBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	// this will be removed all place changed with Data Store VO constructor

	/**
	 * @param gmDataStoreVO
	 * @throws AppError
	 */

	public GmTTPVendorPOTransBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * createPOInputString - This method used validate the NON WO DCO PO parts. 
	 * if return empty call create PO sting procedure or else update the status as failed
	 * 
	 * To update method return type
	 * PC-376: PO creation only for DCO parts. To exclude the Non DCO parts
	 * 
	 * @param strVendorPOStatus
	 * @param strLoadDate
	 * @param strUserId
	 */
	public ArrayList createPOInputString(String strVendorPOStatus,
			String strLoadDate, String strUserId) throws AppError {
  
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());//GLOBUS_APP DB 
		GmDBManager gmDMDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO()); // GLOBUS_DM_OPERATION DB 
		String strCompId = getCompId();

		ArrayList alVendorDtls = new ArrayList();
		Iterator itrVendorDtls = null;
		HashMap hmTemp = null;
		String strVendorId = "";
		String strCapaSummaryId = "";
		String strPOType = "";
		String strVendorPOPartsStr = "";
		String strNonWODCOParts = "";
		String strErrDtls = "";
		String strPOWOCount ="";
		String strPOGenByTTPFl ="";
		HashMap hmReturn =null ;
		ArrayList alReturn = new ArrayList();
		
       //fetch the vendor details for validate the NON WO DCO parts
		alVendorDtls = fetchVendorDetails(strVendorPOStatus, strLoadDate,gmDMDBManager);
	
		if (alVendorDtls.size() != 0) {
			itrVendorDtls = alVendorDtls.iterator();
			while (itrVendorDtls.hasNext()) {
				hmTemp = (HashMap) itrVendorDtls.next();
				strVendorId = (String) hmTemp.get("VENDORID");
				strCapaSummaryId = (String) hmTemp.get("VENDORCAPASUMMID");
				strPOType = (String) hmTemp.get("POTYPE");
				strPOWOCount = GmCommonClass.parseNull((String) hmTemp.get("PO_WO_CNT"));
				strPOGenByTTPFl = GmCommonClass.parseNull((String) hmTemp.get("PO_GEN_BY_TTP_FL"));
			
                //to fetch the vendor part details
				strVendorPOPartsStr = fetchVendorPartString(strVendorId, strLoadDate,gmDMDBManager);
				
				gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_validate_vendor_WO_DCO", 4);
				gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
				gmDBManager.setString(1, strVendorPOPartsStr);
				gmDBManager.setString(2, strPOType);
				gmDBManager.setString(3, strCompId);
				gmDBManager.execute();
				strNonWODCOParts = GmCommonClass.parseNull(gmDBManager.getString(4));
				gmDBManager.close();
				
				
				gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_upd_wo_dco_flag",5);
				gmDMDBManager.setString(1, strVendorId);
				gmDMDBManager.setString(2, strLoadDate);
				gmDMDBManager.setString(3, "");
				gmDMDBManager.setString(4, strVendorPOPartsStr);
				gmDMDBManager.setInt(5, Integer.parseInt(strUserId));
				gmDMDBManager.execute();
				
				//PC-376: PO creation only for DCO parts. To exclude the Non DCO parts
				
				//When Parts are having WO DCO validation,for those parts WO_DCO Fl should  be set to Y,so for other parts PO can be raised.
				//Set the PO Generation status as "PO Generation Failed",so user can re-try after WO DCO is done.
				if (!strNonWODCOParts.equals("")) {
					
					gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_upd_wo_dco_flag",5);
					gmDMDBManager.setString(1, strVendorId);
					gmDMDBManager.setString(2, strLoadDate);
					gmDMDBManager.setString(3, "Y");
					gmDMDBManager.setString(4, strNonWODCOParts);
					gmDMDBManager.setInt(5, Integer.parseInt(strUserId));
					gmDMDBManager.execute();
					
					
					
					strErrDtls = "PO cannot be raised for the following part number(s) because the Work Order DCO has not been done.:<BR>"
							+ strNonWODCOParts;
					
					hmReturn = new HashMap();
					hmReturn.put("STATUS",  "26240823");// PO generation Failed status id
					hmReturn.put("ERROR_DTLS", strErrDtls);
					hmReturn.put("CAPA_SUMM_ID", strCapaSummaryId);	
					alReturn.add(hmReturn);
				}
				
					
					//Do the PO generation for Partial line items which do not have WO DCO.
              		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_process_vendor_po_str",6);
					gmDMDBManager.setString(1, strVendorId);
					gmDMDBManager.setString(2, strCapaSummaryId);
					gmDMDBManager.setString(3, strPOType);
					gmDMDBManager.setInt(4, Integer.parseInt(strUserId));
					gmDMDBManager.setString(5, strPOWOCount);
					gmDMDBManager.setString(6, strPOGenByTTPFl);
					gmDMDBManager.execute();
				
			}
			
		}
		gmDMDBManager.commit();
		
		
		return alReturn;
	}

	/** fetchVendorDetails - this method used to fetch the vendor details 
	 *  based on vendor status and load date
	 * @param strVendorPOStatus
	 * @param strLoadDate
	 * @param gmDMDBManager 
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchVendorDetails(String strVendorPOStatus,
			String strLoadDate, GmDBManager gmDMDBManager) throws AppError {

		ArrayList alVendorDtlsList = new ArrayList();

		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_fch_vendor_po_dtls", 3);
		gmDMDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDMDBManager.setString(1, strVendorPOStatus);
		gmDMDBManager.setString(2, strLoadDate);
		gmDMDBManager.execute();
		alVendorDtlsList = gmDMDBManager.returnArrayList((ResultSet) gmDMDBManager.getObject(3));
		
		return alVendorDtlsList;
	}

	/**fetchVendorPartString - This method used to fetch the vendor part details
	 *  based on vendor id, PO type, capa summary id , load date to validate the parts
	 * @param strVendorId
	 * @param strPOType
	 * @param strCapaSummaryId
	 * @param strLoadDate
	 * @param gmDMDBManager 
	 * @return
	 * @throws AppError
	 */
	public String fetchVendorPartString(String strVendorId, String strLoadDate, GmDBManager gmDMDBManager) throws AppError {
		
		String strVendorPOParts = "";

		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_fch_vendor_po_parts", 3);
		gmDMDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDMDBManager.setString(1, strVendorId);
		gmDMDBManager.setString(2, strLoadDate);
		gmDMDBManager.execute();
		strVendorPOParts = GmCommonClass.parseNull(gmDMDBManager.getString(3));
		
		return strVendorPOParts;
	}


	/**updateVendorPOStatus - This method used to update the vendor status
	 * @param strCapaSummaryId
	 * @param strPOStatus
	 * @param strErrDtls
	 * @param strUserId
	 * @param gmDMDBManager 
	 * @throws AppError
	 */
	public void updateVendorPOStatus(String strCapaSummaryId,String strPOStatus, 
			String strErrDtls, String strUserId, GmDBManager gmDMDBManager)throws AppError {
		
		String strVendorPOParts = "";
		
		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_upd_vendor_po_status", 4);

		gmDMDBManager.setString(1, strCapaSummaryId);
		gmDMDBManager.setString(2, strPOStatus);
		gmDMDBManager.setString(3, strErrDtls);
		gmDMDBManager.setInt(4, Integer.parseInt(strUserId));
		gmDMDBManager.execute();

	}

	/**
	 * generateVendorPO - this method used to generate the PO and update the
	 * Vendor status as PO Generated/ failed
	 * 
	 * @param strVendorPOStatus
	 * @param strLoadDate
	 * @param strUserId
	 */
	public void generateVendorPO(String strVendorPOStatus, String strLoadDate,
			String strUserId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmDBManager gmDMDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		String strCompId = getCompId();

		ArrayList alVendorDtls = new ArrayList();
		ArrayList alVenPOStrDtls = new ArrayList();
		Iterator itrVendorDtls = null;
		Iterator itrVendPOStrDtls = null;
		HashMap hmTemp = null;
		HashMap hmTempPOStr = null;
		String strVendorId = "";
		String strCapaSummaryId = "";
		String strPOType = "";
		String strPOParts = "";
		String strPOId = "";
		String strPODtlString = "";
		String strErrMsg = "";
		String strPOStatus = "";
		String strErrDtls = "";
		String strPOString="";
		String strPOCmnts ="";
		String strVendPODtlId ="";
		String strPOWOCount ="";
		String strPOGenByTTPFl ="";
		
		//To fetch the vednor details based on status and load date
		alVendorDtls = fetchVendorDetails(strVendorPOStatus, strLoadDate,gmDMDBManager);
		
		if (alVendorDtls.size() != 0) {
			itrVendorDtls = alVendorDtls.iterator();
			while (itrVendorDtls.hasNext()) {
				hmTemp = (HashMap) itrVendorDtls.next();
				strVendorId = GmCommonClass.parseNull((String) hmTemp.get("VENDORID"));
				strCapaSummaryId = GmCommonClass.parseNull((String) hmTemp.get("VENDORCAPASUMMID"));
				strPOType = GmCommonClass.parseNull((String) hmTemp.get("POTYPE"));
				
				strPOStatus ="26240822";  // PO Generated code id
				strErrDtls ="";
				String strPOIds ="";
		
				//fetch the vendor PO details from t4065 table 
				alVenPOStrDtls = fetchVendorPOStr(strCapaSummaryId,gmDMDBManager);
				
				try {
					itrVendPOStrDtls = alVenPOStrDtls.iterator();
					
					while (itrVendPOStrDtls.hasNext()) {
					
						hmTempPOStr = (HashMap) itrVendPOStrDtls.next();
						strPOString = GmCommonClass.parseNull((String) hmTempPOStr.get("POSTRING"));
						strPOCmnts = GmCommonClass.parseNull((String) hmTempPOStr.get("POCOMMENTS"));
					    strVendPODtlId = GmCommonClass.parseNull((String) hmTempPOStr.get("VENPODTLID"));
					   
						gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_sav_ttp_by_vendor_initiate_po",8);
		
						gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
						gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
						gmDBManager.setString(1, strVendorId);
						gmDBManager.setString(2, strPOType);
						gmDBManager.setString(3, strPOString);
						gmDBManager.setString(4, strPOCmnts);
						gmDBManager.setString(5, strCompId);
						gmDBManager.setInt(6, Integer.parseInt(strUserId));
						gmDBManager.execute();
						
						strPOId = GmCommonClass.parseNull(gmDBManager.getString(7));
						strPODtlString = GmCommonClass.parseNull(gmDBManager.getString(8));
						
						gmDBManager.commit();
		
				    //to update the PO details to t4065 table 
					 updateVendorPOdetails(strPOId, strPODtlString,strVendPODtlId, strUserId,gmDMDBManager);
					 
					} //vendor PO details while loop close brace 
				} catch (Exception ex) {
					strPOStatus = "26240823"; // PO Generation Failed code id
					strErrDtls = ex.getMessage();
					
					strErrDtls =strErrDtls.substring(strErrDtls.indexOf(":") + 1);
		
					 strPOIds = GmCommonClass.parseNull((String) fetchVendorPOIds(strCapaSummaryId,gmDMDBManager));
							
					 //to void the PO and WO 
					 voidVendorPODetails(strPOIds, strUserId);
				}//try catch close brace 
				
			    //to update the vendor status (PC-376: PO creation only DCO parts)
				updatePartPOFlag( strVendorId, strLoadDate ,strUserId, gmDMDBManager);
				updateVendorPOStatus(strCapaSummaryId, strPOStatus, strErrDtls,strUserId, gmDMDBManager);
				
			}// vendor Details while loop close brace 	
		
		}//if condition close brace 
		gmDMDBManager.commit();
	}

	/** fetchVendorPOStr - This method used to fetch the PO details from t4065 table based on CAPA summary Id
	 * 
	 * @param strCapaSummaryId
	 * @param gmDMDBManager 
	 * @return alVenPOStrDtls
	 * @throws AppError
	 */
	public ArrayList fetchVendorPOStr(String strCapaSummaryId, GmDBManager gmDMDBManager) throws AppError {
		
		ArrayList alVenPOStrDtls = new ArrayList();

		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_fch_vendor_po_str", 2);
		gmDMDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDMDBManager.setString(1, strCapaSummaryId);
		gmDMDBManager.execute();
		alVenPOStrDtls = gmDMDBManager.returnArrayList((ResultSet) gmDMDBManager.getObject(2));
		
		return alVenPOStrDtls;
	}

	/** updateVendorPOdetails - This method used to update the PO Id, total Qty, po amount to t4065 table
	 * @param strPOId
	 * @param strPODtlString
	 * @param strVendPODtlId
	 * @param strUserId
	 * @param gmDMDBManager 
	 */
	public void updateVendorPOdetails(String strPOId, String strPODtlString,String strVendPODtlId, String strUserId, GmDBManager gmDMDBManager) {
		

		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_upd_vendor_po_dtls", 4);

		gmDMDBManager.setString(1, strPOId);
		gmDMDBManager.setString(2, strPODtlString);
		gmDMDBManager.setString(3, strVendPODtlId);
		gmDMDBManager.setInt(4, Integer.parseInt(strUserId));
		gmDMDBManager.execute();
	}

	/**fetchVendorPOIds - This method used to fetch the vendor PO ids from t4065 table based on capa summery id 
	 * @param strCapaSummaryId
	 * @param gmDMDBManager 
	 * @return strPOIds
	 */
	public String fetchVendorPOIds(String strCapaSummaryId, GmDBManager gmDMDBManager) {

		String strPOIds = "";
			
		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_fch_vendor_po_ids", 2);
		gmDMDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDMDBManager.setString(1, strCapaSummaryId);
		gmDMDBManager.execute();
		strPOIds = GmCommonClass.parseNull(gmDMDBManager.getString(2));
		
		return strPOIds;
	}

	/**voidVendorPODetails - This method used to void the PO and WO  
	 * @param strPOIds
	 * @param strUserId
	 */
	public void voidVendorPODetails(String strPOIds, String strUserId) {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString("gm_pkg_op_purchasing.gm_void_vendor_po_dtls", 2);

		gmDBManager.setString(1, strPOIds);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	/**
	 * updatePartPOFlag - This method used validate the NON WO DCO PO parts. 
	 * if return empty call create PO sting procedure or else update the status as failed
	 * 
	 * @param strVendorPOStatus
	 * @param strLoadDate
	 * @param strUserId
	 */
	public void updatePartPOFlag(String strVendorID,
			String strLoadDate, String strUserId, GmDBManager gmDMDBManager) throws AppError {
		
		gmDMDBManager.setPrepareString("gm_pkg_oppr_vendor_po_txn.gm_upd_part_po_flag", 3);

		gmDMDBManager.setString(1, strVendorID);
		gmDMDBManager.setString(2, strLoadDate);
		gmDMDBManager.setInt(3, Integer.parseInt(strUserId));
		gmDMDBManager.execute();
	}
	
	/**updateVendorPOStatus - This method used to update the vendor status
	 * @param strCapaSummaryId
	 * @param strPOStatus
	 * @param strErrDtls
	 * @param strUserId
	 * @param gmDMDBManager 
	 * @throws AppError
	 */
	public void updateVendorPOStatus(String strCapaSummaryId,String strPOStatus, 
			String strErrDtls, String strUserId)throws AppError {
		
		GmDBManager gmDMDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		updateVendorPOStatus(strCapaSummaryId,strPOStatus,strErrDtls,strUserId,gmDMDBManager);
		gmDMDBManager.commit();

	}
}
