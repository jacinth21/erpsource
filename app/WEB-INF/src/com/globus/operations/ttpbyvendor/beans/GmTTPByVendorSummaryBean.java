package com.globus.operations.ttpbyvendor.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/** PMT-38702 - TTP View By Vendor - view by Part
 * 
 * GmTTPByVendorSummaryBean - this action used to fetch the TTP Summary Details 
 * 
 */
public class GmTTPByVendorSummaryBean extends GmBean {

	public GmTTPByVendorSummaryBean(GmDataStoreVO gmDataStoreVO)
			throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchTTPbyVendorSummaryDetail - This Method Used to fetch the TTP By vendor summary details
	 * 
	 * @param hmParam
	 * @return strTTPSummaryDtlsJSONString
	 * @throws AppError
	 */
	public String fetchTTPbyVendorSummaryDetail(HashMap hmParam)
			throws AppError {

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());

		String strTTPId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
		String strDate = GmCommonClass.parseNull((String) hmParam.get("STRLOADDATE"));
		//String strYear = GmCommonClass.parseNull((String) hmParam.get("TTPYEAR"));
		String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		String strTTPSummaryDtlsJSONString = "";

		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_rpt.gm_fch_capacity_summary_dtls",4);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strTTPId);
		gmDBManager.setString(2, strVendorId);
		gmDBManager.setString(3, strDate);
		gmDBManager.execute();
		strTTPSummaryDtlsJSONString = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();

		return strTTPSummaryDtlsJSONString;
	}
	/**fetchTTPbyVendorPartDetails - This Method used to fetch lock in progress ttp details
	 * @param strTTPId
	 * @param strLockPeriod
	 * @param strStatus
	 * @return alReturn
	 * @throws AppError
	 */
	public ArrayList fetchTTPbyVendorPartDetails(String strTTPId,String strLockPeriod, String strStatus) throws AppError{

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		
		ArrayList alReturn = new ArrayList();
		
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_lock.gm_fetch_ttp_by_vendor_part_dtls", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTTPId);
		gmDBManager.setString(2, strLockPeriod);
		gmDBManager.setString(3, strStatus);
		gmDBManager.execute();
		
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		
		return alReturn;
	}
	/**fetchPOStringDetails:Fetch the PO String which will be used to create the PO
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchPOStringDetails(HashMap hmParam) throws AppError{

		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();
		
		String strTTPDtlIds = GmCommonClass.parseNull((String) hmParam.get("STRTTPDETAILIDS"));
	
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_lock.gm_fch_po_generate_str", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strTTPDtlIds);
		gmDBManager.execute();
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		log.debug("alReturn"+alReturn);
		gmDBManager.commit();
		
		return alReturn;
	}
	
	/**
	 * fetchTTPbyVendorPartChart - This Method Used to fetch the vendor chart details based on part number
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchTTPbyVendorPartChart(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());

		String strTTPCapaId = GmCommonClass.parseNull((String) hmParam.get("TTPCAPAID"));
		String strTTPPartNum = GmCommonClass.parseNull((String) hmParam.get("TTPPARTNUMID"));
		String strChartBy = GmCommonClass.parseNull((String) hmParam.get("STRCHARTBY"));
		String strDate = GmCommonClass.parseNull((String) hmParam.get("STRLOADDATE"));
		String strTTPChartByPartJSONString = "";

		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_rpt.gm_fch_chart_by_part_detail",5);
		gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
		gmDBManager.setString(1, strTTPCapaId);
		gmDBManager.setString(2, strTTPPartNum);
		gmDBManager.setString(3, strChartBy);
		gmDBManager.setString(4, strDate);
		gmDBManager.execute();
		
		strTTPChartByPartJSONString = GmCommonClass.parseNull(gmDBManager.getString(5));
		gmDBManager.close();
		
		return strTTPChartByPartJSONString;
	}
	
	/**
	 * fetchTTPbyVendorChart - This Method Used to fetch the chart details based on vendor id
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public String fetchTTPbyVendorChart(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());

		String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		String strChartBy = GmCommonClass.parseNull((String) hmParam.get("STRCHARTBY"));
		String strDate = GmCommonClass.parseNull((String) hmParam.get("STRLOADDATE"));
		String strTTPChartByVendorJSONString = "";
		
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_rpt.gm_fch_chart_by_vendor_detail",4);
		gmDBManager.registerOutParameter(4, OracleTypes.CLOB);
		gmDBManager.setString(1, strVendorId);
		gmDBManager.setString(2, strDate);
		gmDBManager.setString(3, strChartBy);
		gmDBManager.execute();
		
		strTTPChartByVendorJSONString = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.close();
	
		return strTTPChartByVendorJSONString;
	}

	/**
	 * fetchTTPbyVendorFooterChart - This Method Used to fetch the Footer chart
	 * details based on filters
	 * @param strTTPCapaIdStr
	 * @param strChartBy
	 * @return strTTPFooterChartJSONData
	 * @throws AppError
	 */
	public String fetchTTPbyVendorFooterChart(String strTTPCapaIdStr,String strChartBy)  throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		
		String strTTPFooterChartJSONData = "";
		
		gmDBManager.setPrepareString("gm_pkg_oppr_ld_ttp_by_vendor_capacity_rpt.gm_fch_chart_by_filter_dtls",3);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.setString(1, strTTPCapaIdStr);
		gmDBManager.setString(2, strChartBy);
		gmDBManager.execute();
		strTTPFooterChartJSONData = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		return strTTPFooterChartJSONData;
	}
	
	/**
	 * fetchActiveVendorPriceByParts - This Method Used to fetch the dropdown values for override other vendor
	 * details based part number and vendor id
	 * @param strPartNum
	 * @param strVendorId
	 * @return strVendorPriceResponse
	 * @throws AppError
	 */
	public String fetchActiveVendorPriceByParts(String strTTPCapaId)
			throws AppError {
		GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS,getGmDataStoreVO());
		
		String strVendorPriceResponse = "";
		String strCompanyId = "";
		strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
		
		gmDBManager.setPrepareString("gm_pkg_oppr_ttp_capa_load_override_vendor.gm_fch_vendor_price_by_part",3);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.setString(1, strTTPCapaId);
		gmDBManager.setString(2, strCompanyId);
		gmDBManager.execute();
		
		strVendorPriceResponse = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		
		return strVendorPriceResponse;
	}	
}
