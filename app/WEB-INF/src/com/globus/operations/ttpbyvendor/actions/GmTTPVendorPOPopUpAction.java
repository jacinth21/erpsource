package com.globus.operations.ttpbyvendor.actions;


import java.text.ParseException;
import java.util.HashMap;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.ttpbyvendor.beans.GmTTPVendorPODashboardBean;
import com.globus.operations.ttpbyvendor.forms.GmTTPVendorPODashboardForm;


/**
 * This class used to fetch  PO Generation Failed status hyperlink report and success generation report 
 * From GOP--Transactions--TTP By Vendor--Create PO By Vendor
 * @author pvigneshwaran
 *
 */
public class GmTTPVendorPOPopUpAction extends GmDispatchAction {

	/**
	 * 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**loadVendorPOFailure:This method used to redirect window opener Page from
	 * 					   GOP--Transactions--TTP By Vendor--Create PO By Vendor
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward loadVendorPOFailure(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		return actionMapping.findForward("gmTTPVendorPODashboardFailure");
	}

	/**fetchVendorPOFailure:This Method used to fetch  PO Generation Failed status hyperlink report 
	 *                      and success generation reportFrom GOP--Transactions--TTP By Vendor--Create PO By Vendor
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward fetchVendorPOFailure(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,IOException, ParseException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		gmTTPVendorPODashboardForm.loadSessionParameters(request);
		GmTTPVendorPODashboardBean gmTTPVendorPODashboardBean = new GmTTPVendorPODashboardBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strJSONVendorPOFailureDetails = "";

		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPVendorPODashboardForm);
		strJSONVendorPOFailureDetails = gmTTPVendorPODashboardBean.fetchVendorPODashboardFailure(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONVendorPOFailureDetails.equals("")) {
			pw.write(strJSONVendorPOFailureDetails);
		}
		pw.flush();
		return null;

	}
	
	/**
	 * loadVendorPOSuccess - This method used to redirect window opener Page
	 * PO Generation Success Pop Screen GOP--Transactions--TTP By Vendor--Create PO By Vendor
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward loadVendorPOSuccess(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		return actionMapping.findForward("gmTTPVendorPODashboardSuccess");

	}

	/**
	 * fetchVendorPOSuccess - used to fetch PO Generation Success status
	 * Details  
	 * Screen Location : GOP--Transactions--TTP By Vendor--Create PO By Vendor--PO Generated hyper link
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward fetchVendorPOSuccess(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException {

		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		gmTTPVendorPODashboardForm.loadSessionParameters(request);
		GmTTPVendorPODashboardBean gmTTPVendorPODashboardBean = new GmTTPVendorPODashboardBean(
				getGmDataStoreVO());

		HashMap hmParam = new HashMap();

		String strJSONVendorPOSuccessDetails = "";

		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPVendorPODashboardForm);
		String strCapaDtlsId = GmCommonClass.parseNull((String) hmParam.get("CAPADTLSID"));
		strJSONVendorPOSuccessDetails = gmTTPVendorPODashboardBean.fetchVendorPODashboardSuccess(strCapaDtlsId);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONVendorPOSuccessDetails.equals("")) {
			pw.write(strJSONVendorPOSuccessDetails);
		}
		pw.flush();
		return null;
	}
}
