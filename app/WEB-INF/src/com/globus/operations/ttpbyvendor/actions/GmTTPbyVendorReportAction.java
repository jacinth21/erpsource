package com.globus.operations.ttpbyvendor.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.ttpbyvendor.beans.GmTTPbyVendorReportBean;
import com.globus.operations.ttpbyvendor.forms.GmTTPbyVendorReportForm;

/**
 * GmTTPbyVendorReportAction - this action used to load the TTP By Vendor period summary report Details
 * 
 * @author asingaravel
 *
 */
public class GmTTPbyVendorReportAction extends GmDispatchAction{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /** loadTTPbyVendorSummary - this method used load the Vendor TTP Period Summary Report screen
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws Exception
		 * @throws IOException
		 */

		public ActionForward loadTTPbyVendorSummary(ActionMapping actionMapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) 
				throws AppError, IOException, Exception{
			
			// call the instantiate method
			instantiate(request, response);
			GmTTPbyVendorReportForm gmTTPbyVendorReportForm = (GmTTPbyVendorReportForm) form;
			gmTTPbyVendorReportForm.loadSessionParameters(request);
			
			gmTTPbyVendorReportForm.setAlMonth(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MONTH",getGmDataStoreVO())));
			gmTTPbyVendorReportForm.setAlYear(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YEAR",getGmDataStoreVO())));
			return actionMapping.findForward("gmTTPVendorSummaryRpt");					
		}
		
		/**fetchTTPbyVendorSummary - this method used to fetch the Vendor summary details and return the response
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws ServletException
		 * @throws IOException
		 * @throws ParseException
		 */
		public ActionForward fetchTTPbyVendorSummary(ActionMapping actionMapping, ActionForm form,
					    HttpServletRequest request, HttpServletResponse response) throws AppError,IOException, ParseException,Exception {
 
					  // call the instantiate method
			instantiate(request, response);
			GmTTPbyVendorReportForm gmTTPbyVendorReportForm = (GmTTPbyVendorReportForm) form;
			gmTTPbyVendorReportForm.loadSessionParameters(request);
			GmTTPbyVendorReportBean gmTTPbyVendorReportBean = new GmTTPbyVendorReportBean(getGmDataStoreVO());
			  
			HashMap hmParam =new HashMap();
			String strJSONTTPSummaryDtls="";
			
			//Get HashMap Values
			hmParam = GmCommonClass.getHashMapFromForm(gmTTPbyVendorReportForm);
			
			strJSONTTPSummaryDtls = gmTTPbyVendorReportBean.fetchTTPbyVendorSummary(hmParam);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJSONTTPSummaryDtls.equals("")) {
				pw.write(strJSONTTPSummaryDtls);
			}
				pw.flush();
				return null;
		}
		
		/**loadTTPbyVendor Part Details - This method used to load the TTP By Vendor Report Screen
         * @param actionMapping
         * @param form
         * @param request
         * @param response
         * @return
         * @throws AppError
         * @throws ServletException
         * @throws IOException
         */
        public ActionForward loadTTPbyVendorPartDetails(ActionMapping actionMapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
                            throws AppError, ServletException,IOException{
                // call the instantiate method
                instantiate(request, response);
                GmTTPbyVendorReportForm GmTTPByVendorReportForm = (GmTTPbyVendorReportForm)form;    
                GmTTPByVendorReportForm.setAlMonth(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MONTH",getGmDataStoreVO())));
                GmTTPByVendorReportForm.setAlYear(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YEAR",getGmDataStoreVO())));
                return actionMapping.findForward("gmTTPByVendorPartDet");
            }

        /**fetchTTPbyVendorPartDetail Part Details - This method used to fetch the TTP By Vendor Report Details
         * @param actionMapping
         * @param form
         * @param request
         * @param response
         * @return
         * @throws AppError
         * @throws ServletException
         * @throws IOException
         */
         public  ActionForward  fetchTTPbyVendorPartDetail(ActionMapping actionMapping, ActionForm form,
                HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,IOException, ParseException {
              // call the instantiate method
                instantiate(request, response);
                GmTTPbyVendorReportForm GmTTPByVendorReportForm = (GmTTPbyVendorReportForm) form;
                GmTTPByVendorReportForm.loadSessionParameters(request);
                GmTTPbyVendorReportBean GmTTPByVendorReportBean = new GmTTPbyVendorReportBean(getGmDataStoreVO());
                
                HashMap hmParam = new HashMap();
                String strJSONTTPPartDtls = "";
                
                hmParam = GmCommonClass.getHashMapFromForm(GmTTPByVendorReportForm);
                strJSONTTPPartDtls = GmTTPByVendorReportBean.fetchTTPbyVendorPartDetail(hmParam);
                response.setContentType("text/json");
                PrintWriter pw = response.getWriter();
                if (!strJSONTTPPartDtls.equals("")) {
                    pw.write(strJSONTTPPartDtls);
                }
                pw.flush();
                return null;
        }
}

