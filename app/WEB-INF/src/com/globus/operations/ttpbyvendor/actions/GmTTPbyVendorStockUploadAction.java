/**
 * 
 */
package com.globus.operations.ttpbyvendor.actions;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.io.PrintWriter;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.ttpbyvendor.forms.GmTTPbyVendorStockUploadForm;
import com.globus.operations.ttpbyvendor.beans.GmTTPbyVendorStockUploadBean;

/**GmTTPbyVendorStockUploadAction: This class is used to upload safety stock data
 * @author asingaravel
 *
 */
public class GmTTPbyVendorStockUploadAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    

    /**Description:This Method will Load safety stock bulk upload screen
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
	public ActionForward loadTTPbyVendorStockBulkUpload(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		instantiate(request, response);
		GmTTPbyVendorStockUploadForm gmTTPbyVendorStockUploadForm = (GmTTPbyVendorStockUploadForm) form;
		gmTTPbyVendorStockUploadForm.loadSessionParameters(request);

		return mapping.findForward("GmTTPbyVendorStockUpload");
	}
	
	
	/**saveStockUpload: method used to validate part number and update safety stock qty
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward validateandFetchTTPName(ActionMapping mapping,
					ActionForm form, HttpServletRequest request,
					HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmTTPbyVendorStockUploadForm gmTTPbyVendorStockUploadForm = (GmTTPbyVendorStockUploadForm) form;
		gmTTPbyVendorStockUploadForm.loadSessionParameters(request);
				
		GmTTPbyVendorStockUploadBean gmTTPbyVendorStockUploadBean = new GmTTPbyVendorStockUploadBean(getGmDataStoreVO());
				
		HashMap hmParam = new HashMap();
		String strJsonString = "";
		String strCompanyInfo = "";
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPbyVendorStockUploadForm);
		
		strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		hmParam.put("COMPINFO", strCompanyInfo);
				
		//Get Report details using json string
		strJsonString = gmTTPbyVendorStockUploadBean.validateandFetchTTPName(hmParam);
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
			pw.flush();	
		}	
	  	       	
		return null;	
	}
	 /**saveStockUpload: method used to validate part number and update safety stock qty
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveStockUpload(ActionMapping mapping,
					ActionForm form, HttpServletRequest request,
					HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmTTPbyVendorStockUploadForm gmTTPbyVendorStockUploadForm = (GmTTPbyVendorStockUploadForm) form;
		gmTTPbyVendorStockUploadForm.loadSessionParameters(request);
				
		GmTTPbyVendorStockUploadBean gmTTPbyVendorStockUploadBean = new GmTTPbyVendorStockUploadBean(getGmDataStoreVO());
				
		HashMap hmParam = new HashMap();
		String strJsonString = "";
		String strCompanyInfo = "";
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPbyVendorStockUploadForm);
		
		strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		hmParam.put("COMPINFO", strCompanyInfo);
				
		//Get Report details using json string
		gmTTPbyVendorStockUploadBean.saveStockUpload(hmParam);
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
			pw.flush();	
		}	
	  	       	
		return null;	
	}
}
