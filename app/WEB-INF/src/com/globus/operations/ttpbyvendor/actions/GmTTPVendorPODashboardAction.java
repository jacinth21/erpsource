package com.globus.operations.ttpbyvendor.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.operations.ttpbyvendor.beans.GmTTPVendorPODashboardBean;
import com.globus.operations.ttpbyvendor.forms.GmTTPVendorPODashboardForm;

/**
 * GmTTPVendorPODashboardAction - This action class used to fetch and shows TTP Vendor PO Details on DashBoard screen,
 * 								  and use GmTTPVendorPODashboardBean to fetch PO details.			
 * 
 * @author rselvaraj
 *
 */
public class GmTTPVendorPODashboardAction extends GmDispatchAction {
	
	/**
	 * this used for enabling logging
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /** loadVendorPODashboard - this method used load the Vendor PO DashBoard screen
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 * @throws IOException
	 */

	public ActionForward loadVendorPODashboard(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		gmTTPVendorPODashboardForm.loadSessionParameters(request);
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		HashMap hmParam = new HashMap();
		HashMap hmSaveAccess = new HashMap();
		HashMap hmGenerateAccess = new HashMap();
		HashMap hmLoop = new HashMap();
		ArrayList alPotype = new ArrayList();
		String strPartyId = "";
		String strSaveBtnAcc = "";
		String strGenerateBtnAcc = "";
		String strPOHtml = "";
		int intDiv = 0;

		hmParam = GmCommonClass.getHashMapFromForm(gmTTPVendorPODashboardForm);
		// Button access for PO DashBoard screen
		strPartyId = GmCommonClass.parseNull((String) hmParam
				.get("SESSPARTYID"));
		hmSaveAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean
				.getAccessPermissions(strPartyId, "TTP_VEND_PO_SAVE_ACC")));
		hmGenerateAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean
				.getAccessPermissions(strPartyId, "TTP_VEND_PO_GEN_ACC")));
		strSaveBtnAcc = GmCommonClass.parseNull((String) hmSaveAccess
				.get("UPDFL"));
		strGenerateBtnAcc = GmCommonClass.parseNull((String) hmGenerateAccess
				.get("UPDFL"));
		log.debug(" VendorPO - Permissions Flag = " + hmSaveAccess
				+ "  Save Access = " + strSaveBtnAcc);
		log.debug(" VendorPO - Permissions Flag = " + hmGenerateAccess
				+ "  Save Access = " + strGenerateBtnAcc);
		gmTTPVendorPODashboardForm.setStrSaveBtnAccessFl(strSaveBtnAcc);
		gmTTPVendorPODashboardForm
				.setStrGeneratePOBtnAccessFl(strGenerateBtnAcc);

		gmTTPVendorPODashboardForm.setAlMonth(GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("MONTH",
						getGmDataStoreVO())));
		gmTTPVendorPODashboardForm.setAlYear(GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("YEAR",
						getGmDataStoreVO())));
		gmTTPVendorPODashboardForm.setAlPOtype(GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("POTYP",
						getGmDataStoreVO())));
		gmTTPVendorPODashboardForm.setAlStatus(GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("TTPVST",
						getGmDataStoreVO())));
		
		//To form the POType dropdown array formation as string and set that value to form variable
		alPotype = gmTTPVendorPODashboardForm.getAlPOtype();
		intDiv = alPotype.size();
		hmLoop = new HashMap();
		if (intDiv > 0) {
			for (int i = 0; i < intDiv; i++) {
				hmLoop = (HashMap) alPotype.get(i);
				strPOHtml += "POArr[" + i + "] = new Array(\""
						+ hmLoop.get("CODEID") + "\",\""
						+ hmLoop.get("CODENM") + "\");";
			}
		}
		log.debug("strPOHtml=>"+strPOHtml);
		gmTTPVendorPODashboardForm.setStrPOHtml(GmCommonClass.parseNull(strPOHtml));
		return actionMapping.findForward("gmTTPVendorPODashBoard");

	}

	 /** fetchVendorPODashboard - this method used fetch the Vendor PO details
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws Exception
		 * @throws IOException
		 */
	public ActionForward fetchVendorPODashboard(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		gmTTPVendorPODashboardForm.loadSessionParameters(request);
		GmTTPVendorPODashboardBean gmTTPVendorPODashboardBean = new GmTTPVendorPODashboardBean(
				getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strJSONVendorPODetails = "";

		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPVendorPODashboardForm);
		strJSONVendorPODetails = gmTTPVendorPODashboardBean
				.fetchVendorPODashboard(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONVendorPODetails.equals("")) {
			pw.write(strJSONVendorPODetails);
		}
		pw.flush();
		return null;

	}

	/**saveVendorPODashboard - This method used to save the vendor PO type to t4058 table
	 * Screen location --> GOP-->Transaction-->TTPBYVENDOR-->Create PO By Vendor
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward saveVendorPODashboard(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		gmTTPVendorPODashboardForm.loadSessionParameters(request);
		GmTTPVendorPODashboardBean gmTTPVendorPODashboardBean = new GmTTPVendorPODashboardBean(
				getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strJSONPOTypeResponse = "";
		String strInputStr = "";
		String strUserId = "";
		
		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPVendorPODashboardForm);
		strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		strJSONPOTypeResponse = gmTTPVendorPODashboardBean.saveVendorPOTypeDtls(strInputStr, strUserId);
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONPOTypeResponse.equals("")) {
			pw.write(strJSONPOTypeResponse);
		}
		pw.flush();
		return null;

	}

	/** generateVendorPO - This function used to generate the PO 
	 * Screen location --> GOP-->Transaction-->TTPBYVENDOR-->Create PO By Vendor
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward generateVendorPO(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPVendorPODashboardForm gmTTPVendorPODashboardForm = (GmTTPVendorPODashboardForm) form;
		gmTTPVendorPODashboardForm.loadSessionParameters(request);
		GmTTPVendorPODashboardBean gmTTPVendorPODashboardBean = new GmTTPVendorPODashboardBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strJSONPOTypeResponse = "";
		String strInputStr = "";
		String strUserId = "";
		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPVendorPODashboardForm);
		hmParam.put("COMPANYINFO",GmCommonClass.parseNull(request.getParameter("companyInfo")));
		
		strJSONPOTypeResponse = gmTTPVendorPODashboardBean.updateVendorPOStatus(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONPOTypeResponse.equals("")) {
			pw.write(strJSONPOTypeResponse);
		}
		pw.flush();
		return null;

	}
}
