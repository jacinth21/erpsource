package com.globus.operations.ttpbyvendor.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.ttpbyvendor.beans.GmTTPByVendorTransBean;
import com.globus.operations.ttpbyvendor.beans.GmTTPByVendorSummaryBean;
import com.globus.operations.ttpbyvendor.forms.GmTTPByVendorSummaryForm;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmTTPbyVendorSummaryAction - this axction used to load ,save and approve the TTP By Vendor Summary Details
 * 
 * @author tramasamy
 *
 */
public class GmTTPByVendorSummaryAction extends GmDispatchAction{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

/**loadTTPbyVendorSummary - This method used to load the TTP By Vendor Report Screen
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 */
public ActionForward loadTTPbyVendorSummary(ActionMapping actionMapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		            throws AppError, ServletException,IOException{
		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm)form;
		gmTTPByVendorSummaryForm.loadSessionParameters(request);
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
log.debug("loadTTPbyVendorSummary====");
		HashMap hmParam = new HashMap();
		HashMap hmSaveAccess = new HashMap();
		HashMap hmApproveAccess = new HashMap();
		String strPartyId = "";
		String strSaveBtnAcc = "";
		String strApproveBtnAcc = "";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);
		// Button access for set Priority Details screen
		strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
		hmSaveAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "TTP_BY_VEN_SAV_ACC")));
		hmApproveAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "TTP_BY_VEN_APP_ACC")));
		strSaveBtnAcc = GmCommonClass.parseNull((String) hmSaveAccess.get("UPDFL"));
		strApproveBtnAcc = GmCommonClass.parseNull((String) hmApproveAccess.get("UPDFL"));
		log.debug(" TTP ACCESS - Permissions Flag = " + hmSaveAccess  +  "  APPROVE ACESS = "+strApproveBtnAcc );
		gmTTPByVendorSummaryForm.setSaveBtnAccessFl(strSaveBtnAcc);
		gmTTPByVendorSummaryForm.setApproveBtnAccessFl(strApproveBtnAcc);
		gmTTPByVendorSummaryForm.setAlMonth(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MONTH",getGmDataStoreVO())));
		gmTTPByVendorSummaryForm.setAlYear(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YEAR",getGmDataStoreVO())));
		return actionMapping.findForward("gmTTPByVendorSummary");
	}


 /**fetchTTPbyVendorSummary - this method used to fetch the Vendor summary details and return the response
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 * @throws ParseException
 */
public  ActionForward  fetchTTPbyVendorSummary(ActionMapping actionMapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,IOException {
	 
	  // call the instantiate method
	  instantiate(request, response);
	  GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm)form;
	  gmTTPByVendorSummaryForm.loadSessionParameters(request);
	  GmTTPByVendorSummaryBean gmTTPByVendorSummaryBean = new GmTTPByVendorSummaryBean(getGmDataStoreVO());
	  
	  HashMap hmParam =new HashMap();
	
	  String strJSONTTPSummaryDtls="";
	 
		//Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);
	   
	  strJSONTTPSummaryDtls = gmTTPByVendorSummaryBean.fetchTTPbyVendorSummaryDetail(hmParam);
	  response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONTTPSummaryDtls.equals("")) {
			pw.write(strJSONTTPSummaryDtls);
		}
		pw.flush();
		return null;
}
 


/**approveTTPbyVendorSummary:This method used to approve the vendor when click approve button
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 * @throws ParseException
 */
public  ActionForward  approveTTPbyVendorSummary(ActionMapping actionMapping, ActionForm form,
 HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException{

		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm) form;
		gmTTPByVendorSummaryForm.loadSessionParameters(request);
		GmTTPByVendorTransBean gmTTPByVendorTransBean = new GmTTPByVendorTransBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();

		String strJSONTTPSummaryDtls = "";

		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);

		strJSONTTPSummaryDtls = gmTTPByVendorTransBean.approveTTPbyVendorSummaryDetail(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONTTPSummaryDtls.equals("")) {
			pw.write(strJSONTTPSummaryDtls);
		}
		pw.flush();
		return null;
	}

/**saveTTPbyVendorSummary method used to save the Override Percentage and Raise PO QTY when click save button
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 * @throws ParseException
 */
public  ActionForward  saveTTPbyVendorSummary(ActionMapping actionMapping, ActionForm form,
 HttpServletRequest request,
			HttpServletResponse response) throws AppError, ServletException,
			IOException, ParseException {

		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm) form;
		gmTTPByVendorSummaryForm.loadSessionParameters(request);
		GmTTPByVendorTransBean gmTTPByVendorTransBean = new GmTTPByVendorTransBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();

		String strJSONTTPSummaryDtls = "";

		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);
		String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		hmParam.put("COMPANYINFO",strCompanyInfo);
		strJSONTTPSummaryDtls = gmTTPByVendorTransBean.saveTTPbyVendorSummaryDetail(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJSONTTPSummaryDtls.equals("")) {
			pw.write(strJSONTTPSummaryDtls);
		}
		pw.flush();
		return null;
	}

/**fetchTTPbyVendorChartByPart method used to fetch chart details data based on part number
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 * @throws ParseException
 */
public  ActionForward  fetchTTPbyVendorChartByPart(ActionMapping actionMapping, ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws AppError, ServletException,
			IOException, ParseException {
	
	
		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm) form;
		gmTTPByVendorSummaryForm.loadSessionParameters(request);
		GmTTPByVendorSummaryBean gmTTPByVendorSummaryBean = new GmTTPByVendorSummaryBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strVendorChartByPart = "";

		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);		
		strVendorChartByPart = gmTTPByVendorSummaryBean.fetchTTPbyVendorPartChart(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		
		if (!strVendorChartByPart.equals("")) {
			pw.write(strVendorChartByPart);
		}
		pw.flush();
		return null;
	} 
/**fetchTTPbyVendorChart method used to fetch chart details data based on vendor
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 * @throws ParseException
 */
public  ActionForward  fetchTTPbyVendorChart(ActionMapping actionMapping, ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws AppError, ServletException,
			IOException, ParseException {

		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm) form;
		gmTTPByVendorSummaryForm.loadSessionParameters(request);
		GmTTPByVendorSummaryBean gmTTPByVendorSummaryBean = new GmTTPByVendorSummaryBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strVendorChartDtls = "";

		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);
		strVendorChartDtls = gmTTPByVendorSummaryBean.fetchTTPbyVendorChart(hmParam);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		
		if (!strVendorChartDtls.equals("")) {
			pw.write(strVendorChartDtls);
		}
		pw.flush();
		return null;
	}

	/** Screen Location - GOP-->Transactions -->TTPByVendor -->Summary Details Report
	 * fetchTTPbyVendorFooterChart - method used to fetch the footer chart
	 * details data based on filter
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public ActionForward fetchTTPbyVendorFooterChart(ActionMapping actionMapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws AppError, ServletException, IOException, ParseException {

		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm) form;
		gmTTPByVendorSummaryForm.loadSessionParameters(request);
		GmTTPByVendorSummaryBean gmTTPByVendorSummaryBean = new GmTTPByVendorSummaryBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strFooterChartJSONData = "";
		String strTTPCapaIdStr="";
		String strChartBy="";
		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);
		strTTPCapaIdStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		strChartBy = GmCommonClass.parseNull((String) hmParam.get("STRCHARTBY"));
		
		strFooterChartJSONData = gmTTPByVendorSummaryBean.fetchTTPbyVendorFooterChart(strTTPCapaIdStr,strChartBy);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();

		if (!strFooterChartJSONData.equals("")) {
			pw.write(strFooterChartJSONData);
		}
		pw.flush();
		return null;
	}
	
	
	/**rollbackTTPbyVendorSummary:This method used to rollback the vendor when click rollback button
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward  rollbackTTPbyVendorSummary(ActionMapping actionMapping, ActionForm form,
	 HttpServletRequest request,
				HttpServletResponse response) throws AppError, ServletException,
				IOException{

			// call the instantiate method
			instantiate(request, response);
			GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm) form;
			gmTTPByVendorSummaryForm.loadSessionParameters(request);
			GmTTPByVendorTransBean gmTTPByVendorTransBean = new GmTTPByVendorTransBean(getGmDataStoreVO());

			HashMap hmParam = new HashMap();

			String strJSONTTPRollbackDtls = "";

			// Get HashMap Values
			hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);

			strJSONTTPRollbackDtls = gmTTPByVendorTransBean.rollBackTTPbyVendorSummaryDetail(hmParam);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJSONTTPRollbackDtls.equals("")) {
				pw.write(strJSONTTPRollbackDtls);
			}
			pw.flush();
			return null;
		}
	/** Screen Location - GOP-->Transactions -->TTPByVendor -->Summary Details Report
	 * fetchActiveVendorPriceByParts - method used to fetch the dropdown values for override other vendor
	 * details data based part number and vendor id
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public ActionForward fetchActiveVendorPriceByParts(ActionMapping actionMapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws AppError, ServletException, IOException, ParseException {

		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorSummaryForm gmTTPByVendorSummaryForm = (GmTTPByVendorSummaryForm) form;
		gmTTPByVendorSummaryForm.loadSessionParameters(request);
		GmTTPByVendorSummaryBean gmTTPByVendorSummaryBean = new GmTTPByVendorSummaryBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strVendorPriceResponse = "";
		String strTTPCapaId = "";
		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorSummaryForm);
		strTTPCapaId = GmCommonClass.parseNull((String) hmParam.get("TTPID"));
		
		strVendorPriceResponse = gmTTPByVendorSummaryBean.fetchActiveVendorPriceByParts(strTTPCapaId);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();

		if (!strVendorPriceResponse.equals("")) {
			pw.write(strVendorPriceResponse);
		}
		pw.flush();
		return null;
	}
}