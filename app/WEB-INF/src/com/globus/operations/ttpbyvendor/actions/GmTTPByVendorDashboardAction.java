package com.globus.operations.ttpbyvendor.actions;
/*Collections Imports*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
/**Struts Imports**/
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;



/**GlobusImports**/
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.actions.GmAction;
//Screen Forms and beans
import com.globus.operations.purchasing.orderplanning.beans.GmOPTTPSetupBean;
import com.globus.operations.ttpbyvendor.beans.GmTTPbyVendorReportBean;
import com.globus.operations.ttpbyvendor.beans.GmTTPByVendorTransBean;
import com.globus.operations.ttpbyvendor.forms.GmTTPByVendorForm;

/**
 * @author pvigneshwaran
 *This class used to view the ttp vendor dashboard details
 */
public class GmTTPByVendorDashboardAction extends GmDispatchAction {
	
	/**
	 * 
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**loadTTPbyVendorDashboard:This method used to get header details when click left link
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadTTPbyVendorDashboard(ActionMapping actionMapping,ActionForm form,
			HttpServletRequest request,HttpServletResponse response) throws AppError,Exception{
			
		instantiate(request,response);
		GmTTPByVendorForm gmTTPByVendorForm = (GmTTPByVendorForm) form;
		gmTTPByVendorForm.loadSessionParameters(request);
		GmOPTTPSetupBean gmOPTTPSetupBean = new GmOPTTPSetupBean(getGmDataStoreVO());
		GmTTPbyVendorReportBean gmTTPbyVendorReportBean =  new GmTTPbyVendorReportBean(getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		
		HashMap hmParam = new HashMap();
		HashMap hmTemp = new HashMap();
		HashMap hmAccess = new HashMap();
		
		ArrayList alLockPeriod = new ArrayList();
		
		String strPartyId = "";
	    String strAccesFl = "";
	    
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorForm);
		//
		strPartyId = GmCommonClass.parseNull((String) gmTTPByVendorForm.getSessPartyId());
    	hmAccess = GmCommonClass.parseNullHashMap((gmAccessControlBean.getAccessPermissions(strPartyId, "TTP_BY_VENDOR_LCK_AC")));
    	strAccesFl = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
    	log.debug(" TTP View By Vendor Dashboard - Permissions Flag = " + hmAccess);
    	gmTTPByVendorForm.setStrAccessFl(strAccesFl);
		// get Code Group Value
		//gmTTPByVendorForm.setAlTTPList(gmOPTTPSetupBean.loadTTPList(hmTemp));
		gmTTPByVendorForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TTPCST",getGmDataStoreVO())));
		gmTTPByVendorForm.setAlMonth(GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList()));
		gmTTPByVendorForm.setAlYear(GmCommonClass.parseNullArrayList(gmCommonBean.getYearList()));
		return actionMapping.findForward("GmTTPbyVendorDashBoard");
	}

	/**fetchTTPVendorByDashboard;This fetch used to get dashboard details when click load button
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 * @throws ParseException
	 */
	public  ActionForward  fetchTTPVendorByDashboard(ActionMapping actionMapping, ActionForm form,
		    HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,IOException, ParseException {
				
		   instantiate(request,response);
		   GmTTPByVendorForm gmTTPByVendorForm = (GmTTPByVendorForm) form;
		   gmTTPByVendorForm.loadSessionParameters(request);
		   GmTTPbyVendorReportBean gmTTPbyVendorReportBean =  new GmTTPbyVendorReportBean(getGmDataStoreVO());
	       //Initialize
			HashMap hmParam = new HashMap();   
			hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorForm);
			//fetch ttp vendor approval details
	    	String strJsonString = GmCommonClass.parseNull(gmTTPbyVendorReportBean.fetchTTPVendorByDashboardDtls(hmParam));
	    	response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJsonString.equals("")) {
				pw.write(strJsonString);
			}
			pw.flush();
			return null;
		}
	
	
	/** lockTTPbyVendor - This Method used to Lock the TTP vendor Status
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward lockTTPbyVendor(ActionMapping actionMapping,ActionForm form, 
			 HttpServletRequest request,HttpServletResponse response) throws AppError, ServletException,IOException {
		// call the instantiate method
		instantiate(request, response);
		GmTTPByVendorForm gmTTPByVendorForm = (GmTTPByVendorForm) form;
		gmTTPByVendorForm.loadSessionParameters(request);
		GmTTPByVendorTransBean gmTTPbyVendorTransBean = new GmTTPByVendorTransBean(getGmDataStoreVO());
		
		HashMap hmParam = new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPByVendorForm);
		hmParam.put("COMPANYINFO",GmCommonClass.parseNull(request.getParameter("companyInfo")));

		String strJsonString = GmCommonClass.parseNull(gmTTPbyVendorTransBean.lockTTPbyVendorDashboard(hmParam));
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;

	}
}
