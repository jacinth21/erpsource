package com.globus.operations.ttpbyvendor.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.ttpbyvendor.forms.GmTTPbyVendorMoqForm;
import com.globus.operations.ttpbyvendor.beans.GmTTPbyVendorMOQBulkBean;

/**
 * @author pvigneshwaran
 *GmTTPbyVendorMoqAction:This class how a user will upload part minimum order qty for each vendor
 */
public class GmTTPbyVendorMoqAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**loadTTPbyVendorMOQBulk:This method used to load MOq Bulk Upload screen
	 * when click leflink
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadTTPbyVendorMOQBulk(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmTTPbyVendorMoqForm gmTTPbyVendorMoqForm = (GmTTPbyVendorMoqForm) form;
		gmTTPbyVendorMoqForm.loadSessionParameters(request);

		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		 String strAccessFlag = "";
		 String strPartyId ="";
		//
		HashMap hmAccess = new HashMap();

		// Get 1000 value from MAX DATA for Validation if Excel copy to grid
		// data is 1000 above
		String strMaxUploadData = GmCommonClass.parseNull(GmCommonClass.getRuleValue("MAX_DATA", "MOQ_DATA_SETUP"));
		gmTTPbyVendorMoqForm.setStrMaxUploadData(strMaxUploadData);

		// Security event to Submit button
		 strPartyId = GmCommonClass.parseNull((String)gmTTPbyVendorMoqForm.getSessPartyId());
		 hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"VEN_MOQ_SAV_BTN_ACC")); 
		 strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		 log.debug("Vendor MOQ Bulk  Upload - Permissions Flag = " +hmAccess);
		 gmTTPbyVendorMoqForm.setStrEditAccess(strAccessFlag);

		return mapping.findForward("GmTTPbyVendorMOQBulk");
	}

	/**saveTTPbyVendorMOQBulk:This method used to save vendor minimum order quantity details
	 * for each partnumber.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveTTPbyVendorMOQBulk(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		/* Initiate class to load details */
		instantiate(request, response);
		GmTTPbyVendorMoqForm gmTTPbyVendorMoqForm = (GmTTPbyVendorMoqForm) form;
		gmTTPbyVendorMoqForm.loadSessionParameters(request);
		GmTTPbyVendorMOQBulkBean gmTTPbyVendorMOQBulkBean = new GmTTPbyVendorMOQBulkBean(getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPbyVendorMoqForm);
		String strJsonString = GmCommonClass.parseNull(gmTTPbyVendorMOQBulkBean.saveTTPbyVendorMOQBulkUpload(hmParam));

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
	}
	/**loadVendorMOQReport - this method used to redirect to GmVendorMOQReport.jsp
	 * Screen Location :GOP--Report--TTP By Vendor--vendor MOQ Report
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward loadTTPByVendorMOQReport(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmTTPbyVendorMoqForm gmTTPbyVendorMOQForm = (GmTTPbyVendorMoqForm) form;
		gmTTPbyVendorMOQForm.loadSessionParameters(request);
		GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
		 GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
		
		HashMap hmParam = new HashMap();
		ArrayList alPurchasingAgent = new ArrayList();
		alPurchasingAgent= GmCommonClass.parseNullArrayList(gmLogonBean.getEmployeeList("300", "W"));
		gmTTPbyVendorMOQForm.setAlPurchaseAgentList(alPurchasingAgent);
		gmTTPbyVendorMOQForm.setAlDivisionList(GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam)));
		return actionMapping.findForward("gmTTPbyVendorMOQRpt");
	}

	/**fetchVendorMOQRptDtls - This method used to fetch the vendor MOQ Details 
	 * Based on vendor,division,part number,project name
	 * Screen Location :GOP--Report--TTP By Vendor--vendor MOQ Report
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward fetchTTPbyVendorMOQReport(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmTTPbyVendorMoqForm gmTTPbyVendorMOQForm = (GmTTPbyVendorMoqForm) form;
		gmTTPbyVendorMOQForm.loadSessionParameters(request);
		GmTTPbyVendorMOQBulkBean gmTTPbyVendorMOQBulkBean = new GmTTPbyVendorMOQBulkBean(getGmDataStoreVO());
		
		HashMap hmParam = new HashMap();
		String strvendorMOQJSONString = "";
		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPbyVendorMOQForm);
		// PartNumber Search Using lietral,Like-Prefix,Like_suffix
		HashMap hmRegStr = new HashMap();
		hmRegStr.put("PARTNUM", gmTTPbyVendorMOQForm.getPartNum());
		hmRegStr.put("SEARCH", gmTTPbyVendorMOQForm.getPnumSuffix());
		String strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
		hmParam.put("PART", strPartNumFormat);
		
		strvendorMOQJSONString = gmTTPbyVendorMOQBulkBean.fetchTTPbyVendorMOQReport(hmParam);
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strvendorMOQJSONString.equals("")) {
			pw.write(strvendorMOQJSONString);
		}
		pw.flush();
		return null;

	}
	
	/**loadTTPbyVendorMOQLog - This method used to redirect window opener Page from
	 * 					   GOP--Report--TTP By Vendor--vendor MOQ Report --> history icon
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws ServletException
	 * @throws IOException
	 */
	public ActionForward loadTTPbyVendorMOQLog(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmTTPbyVendorMoqForm gmTTPbyVendorMOQForm = (GmTTPbyVendorMoqForm) form;
		gmTTPbyVendorMOQForm.loadSessionParameters(request);
		return actionMapping.findForward("gmTTPbyVendorMOQLogPopUp");
	}
	
	/**fetchTTPbyVendoeMOQLog -This method used to fetch the ttp by vendor moq log details
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward fetchTTPbyVendorMOQLog(ActionMapping actionMapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmTTPbyVendorMoqForm gmTTPbyVendorMOQForm = (GmTTPbyVendorMoqForm) form;
		gmTTPbyVendorMOQForm.loadSessionParameters(request);
        GmTTPbyVendorMOQBulkBean gmTTPbyVendorMOQBulkBean = new GmTTPbyVendorMOQBulkBean(getGmDataStoreVO());
		
		HashMap hmParam = new HashMap();
		String strvendorMOQLogJSONString = "";
		String strVendorMOQId ="";
		// Get HashMap Values
		hmParam = GmCommonClass.getHashMapFromForm(gmTTPbyVendorMOQForm);
		strVendorMOQId =GmCommonClass.parseNull((String) hmParam.get("VENDORMOQID"));
		
		strvendorMOQLogJSONString = gmTTPbyVendorMOQBulkBean.fetchTTPbyVendoeMOQLog(strVendorMOQId);
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strvendorMOQLogJSONString.equals("")) {
			pw.write(strvendorMOQLogJSONString);
		}
		pw.flush();
		return null;
	}
}


