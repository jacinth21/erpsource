package com.globus.operations.ttpbyvendor.forms;

import java.util.ArrayList;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

/**
 * GmTTPbyVendorReportForm - this action used to get and set the values for TTP By Vendor period summary report Details
 * 
 * @author asingaravel
 *
 */
public class GmTTPbyVendorReportForm extends GmCommonForm{
	
	private String ttpId = "";
	private String ttpName = "";
	private String vendorId = ""; 
	private String vendorName = "";
	private String poQty = "";
	private String yearId = String.valueOf(GmCalenderOperations.getCurrentYear());
	private String monthId = GmCalenderOperations.getCurrentMM();
	
	private ArrayList alVendorList  = new ArrayList();
    private ArrayList alTTPList  = new ArrayList();
    private ArrayList alMonth = new ArrayList();
    private ArrayList alYear  = new ArrayList();
	/**
	 * @return the ttpId
	 */
	public String getTtpId() {
		return ttpId;
	}
	/**
	 * @param ttpId the ttpId to set
	 */
	public void setTtpId(String ttpId) {
		this.ttpId = ttpId;
	}
	/**
	 * @return the ttpName
	 */
	public String getTtpName() {
		return ttpName;
	}
	/**
	 * @param ttpName the ttpName to set
	 */
	public void setTtpName(String ttpName) {
		this.ttpName = ttpName;
	}
	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}
	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @return the yearId
	 */
	public String getYearId() {
		return yearId;
	}
	/**
	 * @param yearId the yearId to set
	 */
	public void setYearId(String yearId) {
		this.yearId = yearId;
	}
	/**
	 * @return the monthId
	 */
	public String getMonthId() {
		return monthId;
	}
	/**
	 * @param monthId the monthId to set
	 */
	public void setMonthId(String monthId) {
		this.monthId = monthId;
	}
	/**
	 * @return the alTTPList
	 */
	public ArrayList getAlTTPList() {
		return alTTPList;
	}
	/**
	 * @param alTTPList the alTTPList to set
	 */
	public void setAlTTPList(ArrayList alTTPList) {
		this.alTTPList = alTTPList;
	}
	/**
	 * @return the alMonth
	 */
	public ArrayList getAlMonth() {
		return alMonth;
	}
	/**
	 * @param alMonth the alMonth to set
	 */
	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}
	/**
	 * @return the alYear
	 */
	public ArrayList getAlYear() {
		return alYear;
	}
	/**
	 * @param alYear the alYear to set
	 */
	public void setAlYear(ArrayList alYear) {
		this.alYear = alYear;
	}
	/**
	 * @return the poQty
	 */
	public String getPoQty() {
		return poQty;
	}
	/**
	 * @param poQty the poQty to set
	 */
	public void setPoQty(String poQty) {
		this.poQty = poQty;
	}
	/**
	 * @return the alVendorList
	 */
	public ArrayList getAlVendorList() {
		return alVendorList;
	}
	/**
	 * @param alVendorList the alVendorList to set
	 */
	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}
    
    
}
