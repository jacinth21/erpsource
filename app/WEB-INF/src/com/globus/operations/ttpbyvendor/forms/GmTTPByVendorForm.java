package com.globus.operations.ttpbyvendor.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

/**
 * @author pvigneshwaran
 *
 */
public class GmTTPByVendorForm extends GmCommonForm{
	private static final long serialVersionUID = 1L;
	private String ttpId="";
	private String ttpName="";
	private String lockPeriod="";
	private String ttpMonth=GmCalenderOperations.getCurrentMM();
	private String ttpYear=String.valueOf(GmCalenderOperations.getCurrentYear());
	private String category="";
	private String inputString="";
	private String status = "";
    private String searchttpId="";
	private String hCompanyID = ""; 
	private String strInvalidName="";
	private String gridData="";
	private String ttpDetailId="";
	private String strXmlGridData="";
	private String strAccessFl="";
	private String strTTPDetailIds=""; //TTP Detail Id String PMT-43160
	 
	 private ArrayList alStatus  = new ArrayList();
	 private ArrayList alLockPeriod  = new ArrayList();
	 private ArrayList alCategory  = new ArrayList();
	 private ArrayList alMonth  = new ArrayList();
	 private ArrayList alYear  = new ArrayList();
	 private ArrayList alTTPList  = new ArrayList();
	/**
	 * @return the ttpId
	 */
	public String getTtpId() {
		return ttpId;
	}
	/**
	 * @param ttpId the ttpId to set
	 */
	public void setTtpId(String ttpId) {
		this.ttpId = ttpId;
	}
	/**
	 * @return the ttpName
	 */
	public String getTtpName() {
		return ttpName;
	}
	/**
	 * @param ttpName the ttpName to set
	 */
	public void setTtpName(String ttpName) {
		this.ttpName = ttpName;
	}
	/**
	 * @return the lockPeriod
	 */
	public String getLockPeriod() {
		return lockPeriod;
	}
	/**
	 * @param lockPeriod the lockPeriod to set
	 */
	public void setLockPeriod(String lockPeriod) {
		this.lockPeriod = lockPeriod;
	}
	/**
	 * @return the ttpMonth
	 */
	public String getTtpMonth() {
		return ttpMonth;
	}
	/**
	 * @param ttpMonth the ttpMonth to set
	 */
	public void setTtpMonth(String ttpMonth) {
		this.ttpMonth = ttpMonth;
	}

	/**
	 * @return the alLockPeriod
	 */
	public ArrayList getAlLockPeriod() {
		return alLockPeriod;
	}
	/**
	 * @param alLockPeriod the alLockPeriod to set
	 */
	public void setAlLockPeriod(ArrayList alLockPeriod) {
		this.alLockPeriod = alLockPeriod;
	}
	/**
	 * @return the alMonth
	 */
	public ArrayList getAlMonth() {
		return alMonth;
	}
	/**
	 * @param alMonth the alMonth to set
	 */
	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}
	/**
	 * @return the ttpYear
	 */
	public String getTtpYear() {
		return ttpYear;
	}
	/**
	 * @param ttpYear the ttpYear to set
	 */
	public void setTtpYear(String ttpYear) {
		this.ttpYear = ttpYear;
	}
	/**
	 * @return the alYear
	 */
	public ArrayList getAlYear() {
		return alYear;
	}
	/**
	 * @param alYear the alYear to set
	 */
	public void setAlYear(ArrayList alYear) {
		this.alYear = alYear;
	}
	/**
	 * @return the alTTPList
	 */
	public ArrayList getAlTTPList() {
		return alTTPList;
	}
	/**
	 * @param alTTPList the alTTPList to set
	 */
	public void setAlTTPList(ArrayList alTTPList) {
		this.alTTPList = alTTPList;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the searchttpId
	 */
	public String getSearchttpId() {
		return searchttpId;
	}
	/**
	 * @param searchttpId the searchttpId to set
	 */
	public void setSearchttpId(String searchttpId) {
		this.searchttpId = searchttpId;
	}
	/**
	 * @return the hCompanyID
	 */
	public String gethCompanyID() {
		return hCompanyID;
	}
	/**
	 * @param hCompanyID the hCompanyID to set
	 */
	public void sethCompanyID(String hCompanyID) {
		this.hCompanyID = hCompanyID;
	}
	/**
	 * @return the strInvalidName
	 */
	public String getStrInvalidName() {
		return strInvalidName;
	}
	/**
	 * @param strInvalidName the strInvalidName to set
	 */
	public void setStrInvalidName(String strInvalidName) {
		this.strInvalidName = strInvalidName;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the ttpDetailId
	 */
	public String getTtpDetailId() {
		return ttpDetailId;
	}
	/**
	 * @param ttpDetailId the ttpDetailId to set
	 */
	public void setTtpDetailId(String ttpDetailId) {
		this.ttpDetailId = ttpDetailId;
	}
	/**
	 * @return the strXmlGridData
	 */
	public String getStrXmlGridData() {
		return strXmlGridData;
	}
	/**
	 * @param strXmlGridData the strXmlGridData to set
	 */
	public void setStrXmlGridData(String strXmlGridData) {
		this.strXmlGridData = strXmlGridData;
	}
	
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alCategory
	 */
	public ArrayList getAlCategory() {
		return alCategory;
	}
	/**
	 * @param alCategory the alCategory to set
	 */
	public void setAlCategory(ArrayList alCategory) {
		this.alCategory = alCategory;
	}
	/**
	 * @return the strAccessFl
	 */
	public String getStrAccessFl() {
		return strAccessFl;
	}
	/**
	 * @param strAccessFl the strAccessFl to set
	 */
	public void setStrAccessFl(String strAccessFl) {
		this.strAccessFl = strAccessFl;
	}
	/**
	 * @return the strTTPDetailIds
	 */
	public String getStrTTPDetailIds() {
		return strTTPDetailIds;
	}
	/**
	 * @param strTTPDetailIds the strTTPDetailIds to set
	 */
	public void setStrTTPDetailIds(String strTTPDetailIds) {
		this.strTTPDetailIds = strTTPDetailIds;
	} 
}
