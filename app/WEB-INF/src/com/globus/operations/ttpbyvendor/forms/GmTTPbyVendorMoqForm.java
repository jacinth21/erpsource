
package com.globus.operations.ttpbyvendor.forms;


import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

/**
 * @author pvigneshwaran
 * 
 */
public class GmTTPbyVendorMoqForm   extends GmLogForm  {

	private String strEditAccess = "";
	private String vendorId = "";
	private String vendorName="";	
	private String inputString="";
	private String strMaxUploadData = "";
    private String strPartNumbers = "";
    
    //PMT-5000- vendor MOQ Report
    private String projectId = "";
	private String projectName = "";
	private String partNum = "";
	private String pnumSuffix = "";
	private String divisionId = "";
	private String vendorMOQId="";
	private String purchaseAgent ="";
	private ArrayList alDivisionList = new ArrayList();
	private ArrayList alPurchaseAgentList = new ArrayList();
	

	/**
	 * @return the strEditAccess
	 */
	public String getStrEditAccess() {
		return strEditAccess;
	}
	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @return the strMaxUploadData
	 */
	public String getStrMaxUploadData() {
		return strMaxUploadData;
	}
	/**
	 * @return the strPartNumbers
	 */
	public String getStrPartNumbers() {
		return strPartNumbers;
	}
	/**
	 * @param strEditAccess the strEditAccess to set
	 */
	public void setStrEditAccess(String strEditAccess) {
		this.strEditAccess = strEditAccess;
	}
	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @param strMaxUploadData the strMaxUploadData to set
	 */
	public void setStrMaxUploadData(String strMaxUploadData) {
		this.strMaxUploadData = strMaxUploadData;
	}
	/**
	 * @param strPartNumbers the strPartNumbers to set
	 */
	public void setStrPartNumbers(String strPartNumbers) {
		this.strPartNumbers = strPartNumbers;
	}
	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the pnumSuffix
	 */
	public String getPnumSuffix() {
		return pnumSuffix;
	}
	/**
	 * @param pnumSuffix the pnumSuffix to set
	 */
	public void setPnumSuffix(String pnumSuffix) {
		this.pnumSuffix = pnumSuffix;
	}
	/**
	 * @return the divisionId
	 */
	public String getDivisionId() {
		return divisionId;
	}
	/**
	 * @param divisionId the divisionId to set
	 */
	public void setDivisionId(String divisionId) {
		this.divisionId = divisionId;
	}
	/**
	 * @return the vendorMOQId
	 */
	public String getVendorMOQId() {
		return vendorMOQId;
	}
	/**
	 * @param vendorMOQId the vendorMOQId to set
	 */
	public void setVendorMOQId(String vendorMOQId) {
		this.vendorMOQId = vendorMOQId;
	}
	/**
	 * @return the purchaseAgent
	 */
	public String getPurchaseAgent() {
		return purchaseAgent;
	}
	/**
	 * @param purchaseAgent the purchaseAgent to set
	 */
	public void setPurchaseAgent(String purchaseAgent) {
		this.purchaseAgent = purchaseAgent;
	}
	/**
	 * @return the alDivisionList
	 */
	public ArrayList getAlDivisionList() {
		return alDivisionList;
	}
	/**
	 * @param alDivisionList the alDivisionList to set
	 */
	public void setAlDivisionList(ArrayList alDivisionList) {
		this.alDivisionList = alDivisionList;
	}
	/**
	 * @return the alPurchaseAgentList
	 */
	public ArrayList getAlPurchaseAgentList() {
		return alPurchaseAgentList;
	}
	/**
	 * @param alPurchaseAgentList the alPurchaseAgentList to set
	 */
	public void setAlPurchaseAgentList(ArrayList alPurchaseAgentList) {
		this.alPurchaseAgentList = alPurchaseAgentList;
	}
	
	
}
