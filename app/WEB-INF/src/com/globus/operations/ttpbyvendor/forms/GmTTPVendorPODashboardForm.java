package com.globus.operations.ttpbyvendor.forms;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

import java.util.ArrayList;
import java.util.HashMap;

public class GmTTPVendorPODashboardForm extends GmCommonForm{
	
	private static final long serialVersionUID = 1L;
	 private String strLoadDate = "";
	 private String vendorId = "";
	 private String vendorName = "";
	 private String statusId = "";
	 private String poId = "";
	 private String strSaveBtnAccessFl ="";
	 private String strGeneratePOBtnAccessFl ="";
	 private String strPOHtml ="";
	 private String yearId = String.valueOf(GmCalenderOperations.getCurrentYear());
	 private String monthId = GmCalenderOperations.getCurrentMM();
	 private String capaDtlsId=""; //PMT-49962
	 private String inputString="";
	 
	 private ArrayList alMonth  = new ArrayList();
	 private ArrayList alYear  = new ArrayList();
	 private ArrayList alPOtype  = new ArrayList();
	 private ArrayList alStatus  = new ArrayList();
	 
	
	/**
	 * @return the strPOHtml
	 */
	public String getStrPOHtml() {
		return strPOHtml;
	}
	/**
	 * @param strPOHtml the strPOHtml to set
	 */
	public void setStrPOHtml(String strPOHtml) {
		this.strPOHtml = strPOHtml;
	}

	/**
	  * @return the yearId
	  */
	public String getYearId() {
		return yearId;
	}
	
	/**
	 * @param yearId the yearId to set
	 */
	public void setYearId(String yearId) {
			this.yearId = yearId;
	}
	 /**
	  * @return the monthId
	  */
	public String getMonthId() {
			return monthId;
	}
	/**
	 * @param monthId the monthId to set
	 */
	public void setMonthId(String monthId) {
		this.monthId = monthId;
	}
	/**
	  * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	  * @return the alPOtype
	 */
	 public ArrayList getAlPOtype() {
		return alPOtype;
	}
	 /**
	   * @param alPOtype the alPOtype to set
	  */
	public void setAlPOtype(ArrayList alPOtype) {
		this.alPOtype = alPOtype;
	}
	/**
	  * @return the strLoadDate
	 */
	public String getStrLoadDate() {
		return strLoadDate;
	}
	/**
	  * @param strLoadDate the strLoadDate to set
	  */
	public void setStrLoadDate(String strLoadDate) {
		this.strLoadDate = strLoadDate;
	}
	/**
	  * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}
	/**
	  * @param vendorId the vendorId to set
	  */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	/**
	  * @return the statusId
	 */
	public String getStatusId() {
		return statusId;
	}
	/**
	  * @param statusId the statusId to set
	  */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	/**
	  * @return the alMonth
	 */
	public ArrayList getAlMonth() {
		return alMonth;
	}
	/**
	  * @param alMonth the alMonth to set
	  */
	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}
	/**
	  * @return the alYear
	 */
	public ArrayList getAlYear() {
		return alYear;
	}
	/**
	  * @param alYear the alYear to set
	  */
	public void setAlYear(ArrayList alYear) {
		this.alYear = alYear;
	}
	/**
	  * @return the strSaveBtnAccessFl
	 */
	 public String getStrSaveBtnAccessFl() {
		return strSaveBtnAccessFl;
	}
	 /**
	  * @param strSaveBtnAccessFl the strSaveBtnAccessFl to set
	  */
	public void setStrSaveBtnAccessFl(String strSaveBtnAccessFl) {
		this.strSaveBtnAccessFl = strSaveBtnAccessFl;
	}
	/**
	  * @return the strGeneratePOBtnAccessFl
	 */
	public String getStrGeneratePOBtnAccessFl() {
		return strGeneratePOBtnAccessFl;
	}
	 /**
	  * @param strGeneratePOBtnAccessFl the strGeneratePOBtnAccessFl to set
	  */
	public void setStrGeneratePOBtnAccessFl(String strGeneratePOBtnAccessFl) {
		this.strGeneratePOBtnAccessFl = strGeneratePOBtnAccessFl;
	}
	/**
	  * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	 /**
	  * @param vendorName the vendorName to set
	  */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	/**
	  * @return the getPoId
	 */	
	 public String getPoId() {
		return poId;
	}
	 /**
	  * @param poId the poId to set
	  */
	public void setPoId(String poId) {
		this.poId = poId;
	}
	/**
	 * @return the capaDtlsId
	 */
	public String getCapaDtlsId() {
		return capaDtlsId;
	}
	/**
	 * @param capaDtlsId the capaDtlsId to set
	 */
	public void setCapaDtlsId(String capaDtlsId) {
		this.capaDtlsId = capaDtlsId;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	
}
