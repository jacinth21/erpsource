/**
 * 
 */
package com.globus.operations.ttpbyvendor.forms;

import com.globus.common.forms.GmCommonForm;

/** Form values is used to store values
 * @author asingaravel
 *
 */
public class GmTTPbyVendorStockUploadForm extends GmCommonForm{
	private String inputStr = "";
	private String partStr= "";

	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}

	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}

	/**
	 * @return the partStr
	 */
	public String getPartStr() {
		return partStr;
	}

	/**
	 * @param partStr the partStr to set
	 */
	public void setPartStr(String partStr) {
		this.partStr = partStr;
	}
}
