package com.globus.operations.ttpbyvendor.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.forms.GmCommonForm;

public class GmTTPByVendorSummaryForm extends GmCommonForm{
	
	 private String ttpId = ""; 
	 private String ttpName = ""; 
	 private String vendorId = ""; 
	 private String vendorName = ""; 
	 private String historicalData = ""; 
	 private String ttpMonth = GmCalenderOperations.getCurrentMM();
	 private String ttpYear = String.valueOf(GmCalenderOperations.getCurrentYear());
	 private String saveBtnAccessFl = ""; 
	 private String approveBtnAccessFl = ""; 
	 private ArrayList alMonth = new ArrayList();
	 private ArrayList alYear  = new ArrayList();
	 private String searchttpId = ""; 
	 private String searchvendorId = ""; 
	 private String strLoadDate = ""; 
	 private String inputString = "";
	 private String ttpCapaId = "";
	 private String ttpPartNumId = "";
	 private String strChartby = "";
	 
	/**
	 * @return the ttpId
	 */
	public String getTtpId() {
		return ttpId;
	}
	/**
	 * @param ttpId the ttpId to set
	 */
	public void setTtpId(String ttpId) {
		this.ttpId = ttpId;
	}
	/**
	 * @return the ttpName
	 */
	public String getTtpName() {
		return ttpName;
	}
	/**
	 * @param ttpName the ttpName to set
	 */
	public void setTtpName(String ttpName) {
		this.ttpName = ttpName;
	}
	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}
	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	/**
	 * @return the historicalData
	 */
	public String getHistoricalData() {
		return historicalData;
	}
	/**
	 * @param historicalData the historicalData to set
	 */
	public void setHistoricalData(String historicalData) {
		this.historicalData = historicalData;
	}
	/**
	 * @return the alMonth
	 */
	public ArrayList getAlMonth() {
		return alMonth;
	}
	/**
	 * @param alMonth the alMonth to set
	 */
	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}
	/**
	 * @return the alYear
	 */
	public ArrayList getAlYear() {
		return alYear;
	}
	/**
	 * @param alYear the alYear to set
	 */
	public void setAlYear(ArrayList alYear) {
		this.alYear = alYear;
	}
	/**
	 * @return the ttpMonth
	 */
	public String getTtpMonth() {
		return ttpMonth;
	}
	/**
	 * @param ttpMonth the ttpMonth to set
	 */
	public void setTtpMonth(String ttpMonth) {
		this.ttpMonth = ttpMonth;
	}
	/**
	 * @return the ttpYear
	 */
	public String getTtpYear() {
		return ttpYear;
	}
	/**
	 * @param ttpYear the ttpYear to set
	 */
	public void setTtpYear(String ttpYear) {
		this.ttpYear = ttpYear;
	}
	/**
	 * @return the saveBtnAccessFl
	 */
	public String getSaveBtnAccessFl() {
		return saveBtnAccessFl;
	}
	/**
	 * @param saveBtnAccessFl the saveBtnAccessFl to set
	 */
	public void setSaveBtnAccessFl(String saveBtnAccessFl) {
		this.saveBtnAccessFl = saveBtnAccessFl;
	}
	/**
	 * @return the approveBtnAccessFl
	 */
	public String getApproveBtnAccessFl() {
		return approveBtnAccessFl;
	}
	/**
	 * @param approveBtnAccessFl the approveBtnAccessFl to set
	 */
	public void setApproveBtnAccessFl(String approveBtnAccessFl) {
		this.approveBtnAccessFl = approveBtnAccessFl;
	}
	/**
	 * @return the searchttpId
	 */
	public String getSearchttpId() {
		return searchttpId;
	}
	/**
	 * @param searchttpId the searchttpId to set
	 */
	public void setSearchttpId(String searchttpId) {
		this.searchttpId = searchttpId;
	}
	/**
	 * @return the searchvendorId
	 */
	public String getSearchvendorId() {
		return searchvendorId;
	}
	/**
	 * @param searchvendorId the searchvendorId to set
	 */
	public void setSearchvendorId(String searchvendorId) {
		this.searchvendorId = searchvendorId;
	}
	/**
	 * @return the strLoadDate
	 */
	public String getStrLoadDate() {
		return strLoadDate;
	}
	/**
	 * @param strLoadDate the strLoadDate to set
	 */
	public void setStrLoadDate(String strLoadDate) {
		this.strLoadDate = strLoadDate;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @return the ttpCapaId
	 */
	public String getTtpCapaId() {
		return ttpCapaId;
	}
	/**
	 * @param ttpCapaId the ttpCapaId to set
	 */
	public void setTtpCapaId(String ttpCapaId) {
		this.ttpCapaId = ttpCapaId;
	}
	/**
	 * @return the ttpPartNumId
	 */
	public String getTtpPartNumId() {
		return ttpPartNumId;
	}
	/**
	 * @param ttpPartNumId the ttpPartNumId to set
	 */
	public void setTtpPartNumId(String ttpPartNumId) {
		this.ttpPartNumId = ttpPartNumId;
	}
	/**
	 * @return the strChartby
	 */
	public String getStrChartby() {
		return strChartby;
	}
	/**
	 * @param strChartby the strChartby to set
	 */
	public void setStrChartby(String strChartby) {
		this.strChartby = strChartby;
	}

	 

}