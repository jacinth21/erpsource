package com.globus.operations.shipping.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCancelForm;

public class GmShipDetailsForm extends GmCancelForm {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  private String shipId = "";
  private String shipTo = "";
  private String names = "";
  private String addressid = "";
  private String shipCarrier = "";
  private String shipMode = "";
  private String dist = "";
  private String accounts = "";
  private String emp = "";
  private String refId = "";
  private String hrefId = "";
  private String shipDate = "";
  private String trackingNo = "";
  private String freightAmt = "";
  private String status = "";
  private String source = "50180";
  private String screenType = "";
  private String tracknos = ""; // to display distinct tracking# for the combination of
                                // shipto,shiptoid,addressid
  private String updAction = "";// Action selected when modifying a Master Record
  private String isMaster = "";
  private String custPO = "";
  private String interPartyId = "";
  private String pkSlipNm = "";
  private String shipType = "";
  private String repID = "";
  private String shipFlag = "";
  private String dtype = "";
  private String hreqid = "";
  private String msgFlag = "";
  private String distID = "";
  private String accID = "";
  private String accCurrSymb = "";
  private String holdFlag = "";
  private String ctype = "";
  private String rmaID = "";
  private String returnID = "";
  private String backScreen = "";
  private String loanerReqFlag = "";
  private String chk_loaner = "";
  private String shipCharge = "";
  private ArrayList alShipTo = new ArrayList();
  private ArrayList alShipToId = new ArrayList();
  private HashMap hmNames = new HashMap();
  private ArrayList alAddress = new ArrayList();
  private ArrayList alShipCarr = new ArrayList();
  private ArrayList alShipMode = new ArrayList();
  private ArrayList alDist = new ArrayList();
  private ArrayList alAccounts = new ArrayList();
  private ArrayList alEmp = new ArrayList();
  private ArrayList alSource = new ArrayList();
  private ArrayList alTracknos = new ArrayList();
  private ArrayList alUpdAction = new ArrayList();
  private String hAddrId = "";
  private String strPurpose = "";
  private String overrideAttnTo = "";
  private String shipInstruction = "";
  private String pickStationId = "";
  private String toteId = "";
  private String asscTxnID = "";
  private String thirdParty = "";
  private String editShipChrgFl = "";
  private String strRBTxn = "";
  private String strOrderType = "";
  private String strSubmitButtonAccess  = "";
  private String strDisableButtonAccess = "";
  private String tissuefl = "";

  //Tissue Parts Shipping Details
  private String tissueShipCarrier ="";
  private String tissueShipMode ="";
  private String tissueAddressId ="";
  private String tissueShipId ="";
  private String tissueOverridenAttnTo ="";
  private String tissueShipInstruction ="";
  private String tissueShipTo = "";
  private String tissueNames = "";
  private String hTissueAddrId = "";
  
  /**
   * loanerReqId is String type
   */
  private String loanerReqId = "";
 // PMT#40555-tissue-part-shipping-validation
  /**
   * @return the tissuefl
   */
  public String getTissuefl() {
	return tissuefl;
}
  /**
   * @param tissuefl the tissuefl to set
   */

public void setTissuefl(String tissuefl) {
	this.tissuefl = tissuefl;
}
  public String getAccCurrSymb() {
    return accCurrSymb;
  }

  public void setAccCurrSymb(String accCurrSymb) {
    this.accCurrSymb = accCurrSymb;
  }

  /**
   * @return the asscTxnID
   */
  public String getAsscTxnID() {
    return asscTxnID;
  }

  /**
   * @param asscTxnID the asscTxnID to set
   */
  public void setAsscTxnID(String asscTxnID) {
    this.asscTxnID = asscTxnID;
  }

  /**
   * @return the strPurpose
   */
  public String getStrPurpose() {
    return strPurpose;
  }

  /**
   * @param strPurpose the strPurpose to set
   */
  public void setStrPurpose(String strPurpose) {
    this.strPurpose = strPurpose;
  }

  public String getLoanerReqFlag() {
    return loanerReqFlag;
  }

  public void setLoanerReqFlag(String loanerReqFlag) {
    this.loanerReqFlag = loanerReqFlag;
  }

  public String getChk_loaner() {
    return chk_loaner;
  }

  public void setChk_loaner(String chk_loaner) {
    this.chk_loaner = chk_loaner;
  }


  /**
   * @return the returnID
   */
  public String getReturnID() {
    return returnID;
  }

  /**
   * @param returnID the returnID to set
   */
  public void setReturnID(String returnID) {
    this.returnID = returnID;
  }

  /**
   * @return the rmaID
   */
  public String getRmaID() {
    return rmaID;
  }

  /**
   * @param rmaID the rmaID to set
   */
  public void setRmaID(String rmaID) {
    this.rmaID = rmaID;
  }

  /**
   * @return the hAddrId
   */
  public String gethAddrId() {
    return hAddrId;
  }

  /**
   * @param hAddrId the hAddrId to set
   */
  public void sethAddrId(String hAddrId) {
    this.hAddrId = hAddrId;
  }

  public String getDistID() {
    return distID;
  }

  public void setDistID(String distID) {
    this.distID = distID;
  }

  public String getAccID() {
    return accID;
  }

  public void setAccID(String accID) {
    this.accID = accID;
  }

  /**
   * @return the msgFlag
   */
  public String getMsgFlag() {
    return msgFlag;
  }

  /**
   * @param msgFlag the msgFlag to set
   */
  public void setMsgFlag(String msgFlag) {
    this.msgFlag = msgFlag;
  }

  public String getHreqid() {
    return hreqid;
  }

  public void setHreqid(String hreqid) {
    this.hreqid = hreqid;
  }

  public String getDtype() {
    return dtype;
  }

  public void setDtype(String dtype) {
    this.dtype = dtype;
  }

  public String getShipType() {
    return shipType;
  }

  public void setShipType(String shipType) {
    this.shipType = shipType;
  }

  public String getRepID() {
    return repID;
  }

  public void setRepID(String repID) {
    this.repID = repID;
  }

  /**
   * @return the holdFlag
   */
  public String getHoldFlag() {
    return holdFlag;
  }

  /**
   * @param holdFlag the holdFlag to set
   */
  public void setHoldFlag(String holdFlag) {
    this.holdFlag = holdFlag;
  }

  /**
   * @return the shipTo
   */
  public String getShipTo() {
    return shipTo;
  }

  /**
   * @param shipTo the shipTo to set
   */
  public void setShipTo(String shipTo) {
    this.shipTo = shipTo;
  }

  /**
   * @return the names
   */
  public String getNames() {
    return names;
  }

  /**
   * @param names the names to set
   */
  public void setNames(String names) {
    this.names = names;
  }

  /**
   * @return the shipCarrier
   */
  public String getShipCarrier() {
    return shipCarrier;
  }

  /**
   * @param shipCarrier the shipCarrier to set
   */
  public void setShipCarrier(String shipCarrier) {
    this.shipCarrier = shipCarrier;
  }

  /**
   * @return the shipMode
   */
  public String getShipMode() {
    return shipMode;
  }

  /**
   * @param shipMode the shipMode to set
   */
  public void setShipMode(String shipMode) {
    this.shipMode = shipMode;
  }

  /**
   * @return the dist
   */
  public String getDist() {
    return dist;
  }

  /**
   * @param dist the dist to set
   */
  public void setDist(String dist) {
    this.dist = dist;
  }

  /**
   * @return the shipFlag
   */
  public String getShipFlag() {
    return shipFlag;
  }

  /**
   * @param shipFlag the shipFlag to set
   */
  public void setShipFlag(String shipFlag) {
    this.shipFlag = shipFlag;
  }

  /**
   * @return the accounts
   */
  public String getAccounts() {
    return accounts;
  }

  /**
   * @param accounts the accounts to set
   */
  public void setAccounts(String accounts) {
    this.accounts = accounts;
  }

  /**
   * @return the emp
   */
  public String getEmp() {
    return emp;
  }

  /**
   * @param emp the emp to set
   */
  public void setEmp(String emp) {
    this.emp = emp;
  }

  /**
   * @return the alShipTo
   */
  public ArrayList getAlShipTo() {
    return alShipTo;
  }

  /**
   * @param alShipTo the alShipTo to set
   */
  public void setAlShipTo(ArrayList alShipTo) {
    this.alShipTo = alShipTo;
  }

  /**
   * @return the alAddress
   */
  public ArrayList getAlAddress() {
    return alAddress;
  }

  /**
   * @param alAddress the alAddress to set
   */
  public void setAlAddress(ArrayList alAddress) {
    this.alAddress = alAddress;
  }

  /**
   * @return the alShipCarr
   */
  public ArrayList getAlShipCarr() {
    return alShipCarr;
  }

  /**
   * @param alShipCarr the alShipCarr to set
   */
  public void setAlShipCarr(ArrayList alShipCarr) {
    this.alShipCarr = alShipCarr;
  }

  /**
   * @return the alShipMode
   */
  public ArrayList getAlShipMode() {
    return alShipMode;
  }

  /**
   * @param alShipMode the alShipMode to set
   */
  public void setAlShipMode(ArrayList alShipMode) {
    this.alShipMode = alShipMode;
  }

  /**
   * @return the alDist
   */
  public ArrayList getAlDist() {
    return alDist;
  }

  /**
   * @param alDist the alDist to set
   */
  public void setAlDist(ArrayList alDist) {
    this.alDist = alDist;
  }

  /**
   * @return the alAccounts
   */
  public ArrayList getAlAccounts() {
    return alAccounts;
  }

  /**
   * @param alAccounts the alAccounts to set
   */
  public void setAlAccounts(ArrayList alAccounts) {
    this.alAccounts = alAccounts;
  }

  /**
   * @return the alEmp
   */
  public ArrayList getAlEmp() {
    return alEmp;
  }

  /**
   * @param alEmp the alEmp to set
   */
  public void setAlEmp(ArrayList alEmp) {
    this.alEmp = alEmp;
  }

  /**
   * @return the serialVersionUID
   */
  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  /**
   * @return the hmNames
   */
  public HashMap getHmNames() {
    return hmNames;
  }

  /**
   * @param hmNames the hmNames to set
   */
  public void setHmNames(HashMap hmNames) {
    this.hmNames = hmNames;
  }

  /**
   * @return the refId
   */
  public String getRefId() {
    return refId;
  }

  /**
   * @param refId the refId to set
   */
  public void setRefId(String refId) {
    this.refId = refId;
  }

  /**
   * @return the shipDate
   */
  public String getShipDate() {
    return shipDate;
  }

  /**
   * @param shipDate the shipDate to set
   */
  public void setShipDate(String shipDate) {
    this.shipDate = shipDate;
  }

  /**
   * @return the trackingNo
   */
  public String getTrackingNo() {
    return trackingNo;
  }

  /**
   * @param trackingNo the trackingNo to set
   */
  public void setTrackingNo(String trackingNo) {
    this.trackingNo = trackingNo;
  }

  /**
   * @return the freightAmt
   */
  public String getFreightAmt() {
    return freightAmt;
  }

  /**
   * @param freightAmt the freightAmt to set
   */
  public void setFreightAmt(String freightAmt) {
    this.freightAmt = freightAmt;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the hsource
   */
  public String getSource() {
    return source;
  }

  /**
   * @param hsource the hsource to set
   */
  public void setSource(String source) {
    this.source = source;
  }

  /**
   * @return the alSource
   */
  public ArrayList getAlSource() {
    return alSource;
  }

  /**
   * @param alSource the alSource to set
   */
  public void setAlSource(ArrayList alSource) {
    this.alSource = alSource;
  }

  /**
   * @return the alShipToId
   */
  public ArrayList getAlShipToId() {
    return alShipToId;
  }

  /**
   * @param alShipToId the alShipToId to set
   */
  public void setAlShipToId(ArrayList alShipToId) {
    this.alShipToId = alShipToId;
  }

  /**
   * @return the addressid
   */
  public String getAddressid() {
    return addressid;
  }

  /**
   * @param addressid the addressid to set
   */
  public void setAddressid(String addressid) {
    this.addressid = addressid;
  }

  /**
   * @return the shipId
   */
  public String getShipId() {
    return shipId;
  }

  /**
   * @param shipId the shipId to set
   */
  public void setShipId(String shipId) {
    this.shipId = shipId;
  }

  /**
   * @return the hrefId
   */
  public String getHrefId() {
    return hrefId;
  }

  /**
   * @param hrefId the hrefId to set
   */
  public void setHrefId(String hrefId) {
    this.hrefId = hrefId;
  }

  /**
   * @return the screenType
   */
  public String getScreenType() {
    return screenType;
  }

  /**
   * @param screenType the screenType to set
   */
  public void setScreenType(String screenType) {
    this.screenType = screenType;
  }

  /**
   * @return the tracknos
   */
  public String getTracknos() {
    return tracknos;
  }

  /**
   * @param tracknos the tracknos to set
   */
  public void setTracknos(String tracknos) {
    this.tracknos = tracknos;
  }

  /**
   * @return the updAction
   */
  public String getUpdAction() {
    return updAction;
  }

  /**
   * @param updAction the updAction to set
   */
  public void setUpdAction(String updAction) {
    this.updAction = updAction;
  }

  /**
   * @return the alTracknos
   */
  public ArrayList getAlTracknos() {
    return alTracknos;
  }

  /**
   * @param alTracknos the alTracknos to set
   */
  public void setAlTracknos(ArrayList alTracknos) {
    this.alTracknos = alTracknos;
  }

  /**
   * @return the alAction
   */
  public ArrayList getAlUpdAction() {
    return alUpdAction;
  }

  /**
   * @param alAction the alAction to set
   */
  public void setAlUpdAction(ArrayList alAction) {
    this.alUpdAction = alAction;
  }

  /**
   * @return the isMaster
   */
  public String getIsMaster() {
    return isMaster;
  }

  /**
   * @param isMaster the isMaster to set
   */
  public void setIsMaster(String isMaster) {
    this.isMaster = isMaster;
  }

  /**
   * @return the custPO
   */
  public String getCustPO() {
    return custPO;
  }

  /**
   * @param custPO the custPO to set
   */
  public void setCustPO(String custPO) {
    this.custPO = custPO;
  }

  /**
   * @return the interPartyId
   */
  public String getInterPartyId() {
    return interPartyId;
  }

  /**
   * @param interPartyId the interPartyId to set
   */
  public void setInterPartyId(String interPartyId) {
    this.interPartyId = interPartyId;
  }

  /**
   * @return the pkSlipNm
   */
  public String getPkSlipNm() {
    return pkSlipNm;
  }

  /**
   * @param pkSlipNm the pkSlipNm to set
   */
  public void setPkSlipNm(String pkSlipNm) {
    this.pkSlipNm = pkSlipNm;
  }

  /**
   * @return the ctype
   */
  public String getCtype() {
    return ctype;
  }

  /**
   * @param ctype the ctype to set
   */
  public void setCtype(String ctype) {
    this.ctype = ctype;
  }

  /**
   * @return the backScreen
   */
  public String getBackScreen() {
    return backScreen;
  }

  /**
   * @param backScreen the backScreen to set
   */
  public void setBackScreen(String backScreen) {
    this.backScreen = backScreen;
  }

  public String getOverrideAttnTo() {
    return overrideAttnTo;
  }

  public void setOverrideAttnTo(String overrideAttnTo) {
    this.overrideAttnTo = overrideAttnTo;
  }

  public String getShipInstruction() {
    return shipInstruction;
  }

  public void setShipInstruction(String shipInstruction) {
    this.shipInstruction = shipInstruction;
  }

  public String getPickStationId() {
    return pickStationId;
  }

  public void setPickStationId(String pickStationId) {
    this.pickStationId = pickStationId;
  }

  public String getToteId() {
    return toteId;
  }

  public void setToteId(String toteId) {
    this.toteId = toteId;
  }

  /**
   * @return the thirdParty
   */
  public String getThirdParty() {
    return thirdParty;
  }

  /**
   * @param thirdParty the thirdParty to set
   */
  public void setThirdParty(String thirdParty) {
    this.thirdParty = thirdParty;
  }

  /**
   * @return the shipCharge
   */
  public String getShipCharge() {
    return shipCharge;
  }

  /**
   * @param shipCharge the shipCharge to set
   */
  public void setShipCharge(String shipCharge) {
    this.shipCharge = shipCharge;
  }

  /**
   * @return the editShipChrgFl
   */
  public String getEditShipChrgFl() {
    return editShipChrgFl;
  }

  /**
   * @param editShipChrgFl the editShipChrgFl to set
   */
  public void setEditShipChrgFl(String editShipChrgFl) {
    this.editShipChrgFl = editShipChrgFl;
  }

  /**
   * @return the strRBTxn
   */
  public String getStrRBTxn() {
    return strRBTxn;
  }

  /**
   * @param strRBTxn the strRBTxn to set
   */
  public void setStrRBTxn(String strRBTxn) {
    this.strRBTxn = strRBTxn;
  }

  public String getStrOrderType() {
		return strOrderType;
	}
	
  public void setStrOrderType(String strOrderType) {
		this.strOrderType = strOrderType;
	}

	public String getStrSubmitButtonAccess() {
		return strSubmitButtonAccess;
	}
	
	public void setStrSubmitButtonAccess(String strSubmitButtonAccess) {
		this.strSubmitButtonAccess = strSubmitButtonAccess;
	}
	
	public String getStrDisableButtonAccess() {
		return strDisableButtonAccess;
	}
	
	public void setStrDisableButtonAccess(String strDisableButtonAccess) {
		this.strDisableButtonAccess = strDisableButtonAccess;
	}

	/**
	 * @return the tissueShipCarrier
	 */
	public String getTissueShipCarrier() {
		return tissueShipCarrier;
	}

	/**
	 * @param tissueShipCarrier the tissueShipCarrier to set
	 */
	public void setTissueShipCarrier(String tissueShipCarrier) {
		this.tissueShipCarrier = tissueShipCarrier;
	}

	/**
	 * @return the tissueShipMode
	 */
	public String getTissueShipMode() {
		return tissueShipMode;
	}

	/**
	 * @param tissueShipMode the tissueShipMode to set
	 */
	public void setTissueShipMode(String tissueShipMode) {
		this.tissueShipMode = tissueShipMode;
	}

	/**
	 * @return the tissueAddressId
	 */
	public String getTissueAddressId() {
		return tissueAddressId;
	}

	/**
	 * @param tissueAddressId the tissueAddressId to set
	 */
	public void setTissueAddressId(String tissueAddressId) {
		this.tissueAddressId = tissueAddressId;
	}

	/**
	 * @return the tissueShipId
	 */
	public String getTissueShipId() {
		return tissueShipId;
	}

	/**
	 * @param tissueShipId the tissueShipId to set
	 */
	public void setTissueShipId(String tissueShipId) {
		this.tissueShipId = tissueShipId;
	}

	/**
	 * @return the tissueOverridenAttnTo
	 */
	public String getTissueOverridenAttnTo() {
		return tissueOverridenAttnTo;
	}

	/**
	 * @param tissueOverridenAttnTo the tissueOverridenAttnTo to set
	 */
	public void setTissueOverridenAttnTo(String tissueOverridenAttnTo) {
		this.tissueOverridenAttnTo = tissueOverridenAttnTo;
	}

	/**
	 * @return the tissueShipInstruction
	 */
	public String getTissueShipInstruction() {
		return tissueShipInstruction;
	}

	/**
	 * @param tissueShipInstruction the tissueShipInstruction to set
	 */
	public void setTissueShipInstruction(String tissueShipInstruction) {
		this.tissueShipInstruction = tissueShipInstruction;
	}

	/**
	 * @return the tissueShipTo
	 */
	public String getTissueShipTo() {
		return tissueShipTo;
	}

	/**
	 * @param tissueShipTo the tissueShipTo to set
	 */
	public void setTissueShipTo(String tissueShipTo) {
		this.tissueShipTo = tissueShipTo;
	}

	/**
	 * @return the tissueNames
	 */
	public String getTissueNames() {
		return tissueNames;
	}

	/**
	 * @param tissueNames the tissueNames to set
	 */
	public void setTissueNames(String tissueNames) {
		this.tissueNames = tissueNames;
	}

	/**
	 * @return the hTissueAddrId
	 */
	public String gethTissueAddrId() {
		return hTissueAddrId;
	}

	/**
	 * @param hTissueAddrId the hTissueAddrId to set
	 */
	public void sethTissueAddrId(String hTissueAddrId) {
		this.hTissueAddrId = hTissueAddrId;
	}
	/**
	 * @return the loanerReqId
	 */
	public String getLoanerReqId() {
		return loanerReqId;
	}
	/**
	 * @param loanerReqId the loanerReqId to set
	 */
	public void setLoanerReqId(String loanerReqId) {
		this.loanerReqId = loanerReqId;
	}
	
}
