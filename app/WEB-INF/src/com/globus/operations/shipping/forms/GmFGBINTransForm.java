/**
 * Description : This form is used to fetch the details of FG BIN Transactions
 * @author arajan
 */
package com.globus.operations.shipping.forms;

import com.globus.common.forms.GmCommonForm;

public class GmFGBINTransForm extends GmCommonForm {
	
	private String scanId = "";
	private String gridXmlData = "";
	
	/**
	 * @param scanId the scanId to set
	 */
	public void setScanId(String scanId) {
		this.scanId = scanId;
	}

	/**
	 * @return the scanId
	 */
	public String getScanId() {
		return scanId;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

}
