/**
 * 
 */
package com.globus.operations.shipping.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.returns.beans.GmReturnsBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

/**
 * @author rshah
 * 
 */
public class GmShipScanReqTransBean extends GmBean {

  /**
   * Constructor will populate default company info.
   * 
   * @throws AppError
   */
  public GmShipScanReqTransBean() throws AppError {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   * @throws AppError
   */
  public GmShipScanReqTransBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /*
   * @author Ritesh Following method is used to save the tote id and station id and status.
   */
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  public void saveShipStation(GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    String strShipToteId = GmCommonClass.parseNull(gmTxnShipInfoVO.getShiptoteid());
    String strShipStationId = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipstationid());
    String strUserId = GmCommonClass.parseNull(gmTxnShipInfoVO.getUserid());
    String strTxnId = GmCommonClass.parseNull(gmTxnShipInfoVO.getTxnid());
    String strSource = GmCommonClass.parseNull(gmTxnShipInfoVO.getSource());

    gmDBManager.setPrepareString("gm_pkg_ship_scan_trans.gm_sav_ship_station", 5);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strSource);
    gmDBManager.setString(3, strShipToteId);
    gmDBManager.setString(4, strShipStationId);
    gmDBManager.setString(5, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /*
   * @author Ritesh Following method is used to complete the tote process.
   */
  public void saveCompleteTote(GmShipScanReqVO gmShipScanReqVO) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    String strUserId = GmCommonClass.parseNull(gmShipScanReqVO.getUserid());
    String strScanId = GmCommonClass.parseNull(gmShipScanReqVO.getScanid());
    String strScanType = GmCommonClass.parseNull(gmShipScanReqVO.getScantype());
    String strTxnId = GmCommonClass.parseNull(gmShipScanReqVO.getTxnid());
    String strSource = GmCommonClass.parseNull(gmShipScanReqVO.getSource());
    String strTrackNo = GmCommonClass.parseNull(gmShipScanReqVO.getTrackingno());
    String strReturnTrackNo = GmCommonClass.parseNull(gmShipScanReqVO.getReturntrackingno());

    gmDBManager.setPrepareString("gm_pkg_ship_scan_trans.gm_sav_complete_tote", 7);
    gmDBManager.setString(1, strScanId);
    gmDBManager.setString(2, strScanType);
    gmDBManager.setString(3, strTxnId);
    gmDBManager.setString(4, strSource);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strTrackNo);
    gmDBManager.setString(7, strReturnTrackNo);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /*
   * @author Ritesh Following method is used for rollback the ship trans status.
   */
  // Changing method parameter from GmShipScanReqVO to GmTxnShipInfoVO for BUG-3223
  public void saveShipRollback(GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    String strUserId = GmCommonClass.parseNull(gmTxnShipInfoVO.getUserid());
    String strScanId = GmCommonClass.parseNull(gmTxnShipInfoVO.getShiptoteid());
    String strShipStationId = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipstationid());// While
                                                                                          // roll
                                                                                          // backing
                                                                                          // from
                                                                                          // RFP to
                                                                                          // PIP,
                                                                                          // need to
                                                                                          // scan
                                                                                          // new
                                                                                          // station
                                                                                          // id
    String strTxnId = GmCommonClass.parseNull(gmTxnShipInfoVO.getTxnid());
    String strSource = GmCommonClass.parseNull(gmTxnShipInfoVO.getSource());
    if (!strTxnId.equals("")) {
      saveShipRollbackTrans(strTxnId, strSource, strShipStationId, strUserId);
    } else { // Passing strScanType as null, because previously this value was getting from
             // GmShipScanReqVO, now we are not using this object
      saveShipRollbackTote(strScanId, "", strShipStationId, strUserId);
    }

  }


  /*
   * @author Ritesh Following method is used for rollback the ship trans status by Using Totes.
   */
  public void saveShipRollbackTrans(String strTxnId, String strSource, String strShipStationId,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_ship_rollback.gm_sav_ship_rollback", 4);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strSource);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strShipStationId);
    gmDBManager.execute();
    gmDBManager.commit();
  }


  /*
   * @author Ritesh Following method is used for rollback the ship trans status by Using Totes.
   */
  public void saveShipRollbackTote(String strScanId, String strScanType, String strShipStationId,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_ship_rollback.gm_sav_ship_rollback_tote", 4);
    gmDBManager.setString(1, strScanId);
    gmDBManager.setString(2, strScanType);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strShipStationId);
    gmDBManager.execute();
    gmDBManager.commit();
  }


  /*
   * @author Ritesh Following method is used to ship the transaction out.
   */
  public void saveShipoutTrans(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strShippingID = GmCommonClass.parseNull((String) hmParams.get("SHIPPINGID"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strStatusFl = GmCommonClass.parseNull((String) hmParams.get("STATUSFL"));
    String strSource = GmCommonClass.parseNull((String) hmParams.get("SOURCE"));
    String strRefID = GmCommonClass.parseNull((String) hmParams.get("TRANSID"));
    String strAutoShipFl = GmCommonClass.parseNull((String) hmParams.get("AUTOSHIPFL"));
    gmDBManager.setPrepareString("gm_pkg_ship_scan_trans.gm_sav_scan_shipout_trans", 4);
    gmDBManager.setString(1, strShippingID);
    gmDBManager.setString(2, strUserID);
    gmDBManager.setString(3, strStatusFl);
    gmDBManager.setString(4, strAutoShipFl);
    gmDBManager.execute();
    gmDBManager.commit();

    // --added below code for PMT-14689, to send RA initiate email notification
    GmReturnsBean gmReturnBean = new GmReturnsBean(getGmDataStoreVO());
    gmReturnBean.sendRAEmail(strRefID, strSource, strUserID);
  }

  /**
   * @Description: Method used to Generate Pending Return Summary Transaction for the corrsponding
   *               IHLN-XXXX transaction
   * @Param : String strRefId, String strSource, String strUserId
   * @Return : String
   * @Author : HReddi
   **/
  public String saveReturnSummary(String strRefId, String strSource, String strUserId)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    String strRMAID = "";
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_op_ihloaner_item_txn.gm_sav_ihloaner_item_return", 4);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.setString(1, strRefId);
    gmDBManager.setString(2, strSource);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    strRMAID = gmDBManager.getString(4);
    gmDBManager.commit();
    log.debug("The generated Pending Return Summary Transaction from saveReturnSummary() is ***** "
        + strRMAID);
    hmReturn.put("RETURNID", strRMAID);
    return strRMAID;
  }
}
