/**
 * 
 */
package com.globus.operations.shipping.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmAccountingBean;
import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.logistics.beans.GmIHLoanerItemBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

/**
 * @author rshah
 * 
 */
public class GmPrintTransactionsBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing


  String strCompanyName = "";
  String strCompanyAddress = "";
  String strCompanyPhone = "";
  String strCompanyId = "";
  String strCompanyFax = "";
  String strCompanyShortName = "";
  String strCustServicePhNumber = "";
  String strApplnDateFmt = "";

  StringBuffer strHeadAddress = new StringBuffer();
  StringBuffer strCustmisedACKHeader = new StringBuffer();

  /**
   * @author kramasamy
   * @param String company Id
   * @return Hashmap
   * @exception Exception
   * @Description Set the all company details taking from constant properties to PDF Jasper.
   * */

  public HashMap setCompanyParameters(String strDivisionID,String strComapnyCD) throws Exception {
    HashMap hmReturn = new HashMap();
		String strCompanyLocale = "";
		strCompanyLocale = GmCommonClass.getCompanyLocale(strComapnyCD);
		GmResourceBundleBean rbCompany = GmCommonClass.getResourceBundleBean(
				"properties.Company", strCompanyLocale);
		strDivisionID = strDivisionID.equals("") ? "2000" : strDivisionID;
		strCompanyName = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID + ".COMPNAME"));
		strCompanyAddress = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID + ".COMPADDRESS"));
		strCompanyPhone = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID + ".PH"));
		strCompanyFax = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID +".FAX"));
		strCompanyShortName = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID + ".COMP_SHORT_NAME"));
		strCustServicePhNumber = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID + ".CUST_SERVICE"));

		String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
		String strCompanyLogo = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID + ".LOGO"));
		strImagePath = strImagePath + strCompanyLogo + ".gif";

		strCompanyAddress = "&nbsp;"
				+ strCompanyAddress.replaceAll("/", "\n<br>&nbsp;");

		strHeadAddress.append("<b>&nbsp;&nbsp;");
		strHeadAddress.append(strCompanyName);
		strHeadAddress.append("</b><BR>&nbsp;");
		strHeadAddress.append(strCompanyAddress);
		strHeadAddress.append("<BR>&nbsp;&nbsp;");
		strHeadAddress.append(strCompanyPhone);

		strCustmisedACKHeader.append("&nbsp;&nbsp;");
		strCustmisedACKHeader.append(strCompanyName);
		strCustmisedACKHeader.append("<BR>&nbsp;");
		strCustmisedACKHeader.append(strCompanyAddress);

		hmReturn.put("COMPANYNAME", strCompanyName);
		hmReturn.put("COMPANYLOGO", strCompanyLogo);
		hmReturn.put("IMAGEPATH", strImagePath);
		hmReturn.put("COMPANYADDRESS", strCompanyAddress);
		hmReturn.put("COMPANYPHONE", strCompanyPhone);
		hmReturn.put("COMPANYFAX", strCompanyFax);
		hmReturn.put("COMPSHNM", strCompanyShortName);
		hmReturn.put("CUSTSRVPH", strCustServicePhNumber);
		hmReturn.put("GLOBUSADDRESS", strHeadAddress.toString());
		hmReturn.put("GLOBUSCUSTMISEDACKADDRESS",
				strCustmisedACKHeader.toString());
		log.debug("hmCompany == after division added" + hmReturn);
		return hmReturn;
  }

  /**
   * @param hmParam
   * @return Hashmap
   * @exception Exception
   * @Description Following method is used to print Consignment paper work US and OUS based on
   *              Consign ID.
   * */

  public HashMap printCNPaperwork(HashMap hmParams) throws Exception {
	HashMap hmRefDetails = new HashMap();
	HashMap hmConsignDetail = new HashMap();
	HashMap hmReturn = new HashMap();
	HashMap hmResult = new HashMap();
	HashMap hmParam = new HashMap();
	HashMap hmPDFDetails = new HashMap();
	HashMap hmConsignPDFDetails = new HashMap();
	HashMap hmPackslipPDFDetails = new HashMap();
	GmDataStoreVO gmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
	GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
	gmTxnShipInfoVO = (GmTxnShipInfoVO) hmParams.get("SHIPINFO");
	gmTxnShipInfoVO = (gmTxnShipInfoVO == null) ? new GmTxnShipInfoVO()	: gmTxnShipInfoVO;
	List ltChildRequest = new ArrayList();
	ArrayList alResult = new ArrayList();
	GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean(gmDataStoreVO);
	GmCustomerBean gmCustomerBean = new GmCustomerBean();
	GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(gmDataStoreVO);
	GmAccountingBean gmAccountingBean = new GmAccountingBean();
	GmOperationsBean gmOperationsBean = new GmOperationsBean(gmDataStoreVO);

		String strPrintType = "PROFORMA";
		String strInvoiceId = "";
		String strInvoiceType = "";
		String strCountryId = "";
		String strNativeCurrency = "";
		String partyId = "";
		String strConsignRptName = "/GmConsignPrint.jasper";
		String strDistId = "";
		String strTransType = "";
		String strPkSlpHdr = "";
		String strDistType = "";
		String strCompanyID = "";
		String strImagePath = "";
		String strCompanyName = "";
		String strCompanyAddress = "";
		String strCompanyPhone = "";
		String strAddress = "";
		String strPDFFileName = "";
		String strCompanyId = "";
		String strShipDt = "";
		String strruleEnforceDate = "";
		String strCustPONum = "";
		String strICTAddress = "";
		String strShipTo = "";
		String strAckDt = "";
		String strConsignDt = "";
		String strPackSlipDt = "";
		String strSessCurrSymbol = "$";

		String strConsignId = GmCommonClass.parseNull((String) hmParams
				.get("TXNID"));
		String strUserId = GmCommonClass.parseNull((String) hmParams
				.get("USERID"));
		String strUserNm = GmCommonClass.parseNull((String) hmParams
				.get("USERNM"));
		if (!strInvoiceId.equals("")) {
			strConsignId = gmAccountingBean
					.fetchConsignmentIdFromInvoice(strInvoiceId);
			strPrintType = "COMMERCIAL";
		}

		String strDivisionID = GmCommonClass.parseNull((String) hmParams
				.get("DIVISION_ID"));
		strDivisionID = strDivisionID.equals("") ? "2000" : strDivisionID;
		String strCompanyCD = GmCommonClass.parseNull((String) hmParams
				.get("COMPANYCD"));
		strCompanyCD = strCompanyCD.equals("") ? GmCommonClass
				.parseNull(GmCommonClass.getRuleValue("DEFAULT_COMPANY",
						"ONEPORTAL")) : strCompanyCD;
		String strCompDTFmt = GmCommonClass.parseNull((String) hmParams
				.get("CMPDFMT"));
		String strCompanyLocale = "";

		strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyCD);
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass
				.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

		GmResourceBundleBean rbCompanyBundleBean = GmCommonClass
				.getResourceBundleBean("properties.Company", strCompanyLocale);
		String strCountryCode = GmCommonClass.parseNull(rbCompanyBundleBean
				.getProperty("COUNTRYCODE"));
		GmResourceBundleBean rbCompany = GmCommonClass.getResourceBundleBean(
				"properties.Company", strCompanyLocale);

		strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
		HashMap hmValues = GmCommonClass
				.parseNullHashMap(setCompanyParameters(strDivisionID,strCompanyCD));
		strCompanyName = GmCommonClass.parseNull((String) hmValues
				.get("COMPANYNAME"));
		strCompanyAddress = GmCommonClass.parseNull(rbCompany
				.getProperty(strDivisionID + ".COMPADDRESS"));
		strCompanyPhone = GmCommonClass.parseNull((String) hmValues
				.get("COMPANYPHONE"));
		// Regular Invoice Type[50200]
		if (strInvoiceType.equals("50200") || strInvoiceId.equals("")) {
			if (!strCountryId.equals("") && !strCountryId.equals("1101")) { // When
																			// Country
																			// Code
																			// is
																			// not
																			// US,
																			// loading
																			// the
																			// OUS
																			// Transaction
																			// from
																			// GOP
																			// DB.
				hmParam.put("TXNID", strConsignId);
				hmParam.put("USERID", strUserId);
				hmParam.put("COUNTRYID", strCountryId);
				hmRefDetails = GmCommonClass.parseNullHashMap(gmICTSummaryBean
						.fetchOUSConsignmentDetails(hmParam));
			} else {
				hmRefDetails = GmCommonClass.parseNullHashMap(gmICTSummaryBean
						.fetchConsignmentDetails(strConsignId, strUserId));
			}
			String strRequestId = GmCommonClass.parseNull((String) hmRefDetails
					.get("REQUESTID"));
			ltChildRequest = gmLogisticsBean.fetchChildRequest(strRequestId);
			hmResult = GmCommonClass.parseNullHashMap((HashMap) hmRefDetails
					.get("HMCONSIGNDETAILS"));
			strShipTo = GmCommonClass
					.parseNull((String) hmResult.get("SHIPTO"));
			strDistId = GmCommonClass
					.parseNull((String) hmResult.get("DISTID"));
			strTransType = GmCommonClass.parseNull((String) hmResult
					.get("TYPE"));
			String strCompDateFmt = GmCommonClass.parseNull((String) hmResult
					.get("CMPDFMT"));
			strConsignDt = GmCommonClass.getStringFromDate(
					(java.util.Date) hmResult.get("UDATE"), strCompDateFmt);
			hmReturn = GmCommonClass.parseNullHashMap((HashMap) hmRefDetails
					.get("HMCONSIGNCHILDDETAILS"));
			hmConsignDetail = gmCustomerBean.loadConsignDetails(strConsignId,
					strUserId);
			hmReturn.put("SETDETAILS", hmConsignDetail);
			strPkSlpHdr = GmCommonClass.parseNull(GmCommonClass.getRuleValue(
					strDistId, "PACKSLIP/SHIPLIST"));
			// IH Loaner Item Type [9100]
			if (strTransType.equals("9110")) {
				strPkSlpHdr = strTransType;
			}
			String strPath = GmCommonClass.parseNull(GmCommonClass
					.getString("GMSUBREPORTLOCATION"));
			hmResult.put("SUBCHILDREQUEST", ltChildRequest);
			hmResult.put("SUBREPORT_DIR", strPath);
			hmResult.put("SETLOAD", hmReturn.get("SETLOAD"));
			partyId = GmCommonClass.parseNull((String) hmResult.get("PARTYID"));
			strDistType = GmCommonClass.parseNull((String) hmResult
					.get("DISTTYPE"));
			strCompanyID = GmCommonClass.parseNull((String) hmResult
					.get("COMPANYID"));
			rbCompany = GmCommonClass.getResourceBundleBean(
					"properties.Company", strCompanyLocale);
			String strCompanyLogo = GmCommonClass.parseNull(rbCompany
					.getProperty(strDivisionID + ".LOGO"));
			String strLogoImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
			hmResult.put("LOGOIMAGE", strLogoImagePath + strCompanyLogo
					+ ".gif");
			hmResult.put("COMPNAME", strCompanyName);
			hmResult.put("COMPADDRESS", strCompanyAddress + "\n"
					+ strCompanyPhone);
			if (strCountryCode.equals("en")) {
			    hmResult.put("SIGNINLBL",
						GmCommonClass.parseNull((String) hmResult.get("ICTNOTES")));
			}
		}
		if (partyId.equals("") && !strInvoiceType.equals("50202")) {
			if (!strCountryId.equals("") && !strCountryId.equals("1101")) { // When
																			// Country
																			// Code
																			// is
																			// not
																			// US,
																			// loading
																			// the
																			// OUS
																			// Print
																			// version.
				strNativeCurrency = GmCommonClass.parseNull(GmCommonClass
						.getRuleValue(strCountryId, "CNTYCURR"));
				strConsignRptName = "/GmOUSConsignPrint.jasper";
				strPDFFileName = strConsignId + "_OUS.pdf";
			} else {
				strNativeCurrency = "USD";
			}
			hmResult.put("VARIABLECURRENCY", strNativeCurrency);
			hmResult.put("ADDWATERMARK", "");
			strPDFFileName = strConsignId + "_US.pdf";
		} else {// to be write for Euro jasper
			HashMap hmEuSectonData = new HashMap();
			GmPartyBean gmPartyBean = new GmPartyBean();
			hmResult.put("USERID", strUserId);
			hmEuSectonData = (HashMap) gmPartyBean.fetchPartyRuleParam(partyId,
					"ICPTY", "PRINT");
			// Add For ICT change
			strShipDt = GmCommonClass.parseNull(gmICTSummaryBean
					.fetchConsignShipDt(strConsignId));
			strruleEnforceDate = GmCommonClass.parseNull(GmCommonClass
					.getRuleValue("50001", "ICTCHANGE"));
			String strAdd = GmCommonClass.parseNull(GmCommonClass
					.getString("GMADDICT"));
			if (strShipDt.equals("")) {
				strShipDt = strruleEnforceDate;
			}
			if (!strDistType.equals("70105")) { // ICS
				if (GmCommonClass.getStringToDate(strShipDt, "MM/dd/yyyy")
						.after(GmCommonClass.getStringToDate(
								strruleEnforceDate, "MM/dd/yyyy"))) {
					if (hmEuSectonData.containsKey("HEADERADDRESS")
							|| hmEuSectonData.containsKey("CURRENCY")) {
						hmEuSectonData.remove("HEADERADDRESS");
						// hmEuSectonData.remove("CURRENCY");
						hmEuSectonData.put("HEADERADDRESS", strAdd);
						// hmEuSectonData.put("CURRENCY", "USD");
					}
				}
				log.debug("ICT CHANGE ENFORCED DATE:" + strruleEnforceDate);
			}// End ICT change
			log.debug(" hmEuSectonData " + hmEuSectonData);
			double dbConsignStatus = Double.parseDouble((String) hmResult
					.get("CONSIGNSTATUS"));
			if (dbConsignStatus < 4 && !strInvoiceType.equals("50202")) {
				hmResult.put("ADDWATERMARK", "Draft Proforma");
				if (strCountryCode.equals("en")) {
					hmResult.put("VARIABLECURRENCY", "USD");
				} else {
					hmResult.put("VARIABLECURRENCY", strSessCurrSymbol);
				}
			} else if (!strInvoiceType.equals("50202")) {
				hmResult.put("VARIABLECURRENCY", hmEuSectonData.get("CURRENCY"));
				log.debug(" strConsignId " + strConsignId);
				strInvoiceId = GmCommonClass.parseNull(gmAccountingBean
						.fetchInvoiceIdFromConsignmentId(strConsignId));
				log.debug(" strInvoiceId " + strInvoiceId);
				strCustPONum = GmCommonClass.parseNull((gmAccountingBean
						.fetchCustomerPoNum(strInvoiceId)));
				log.debug(" strCustPONum " + strCustPONum);

				if (strPrintType.equals("COMMERCIAL") || partyId.equals("203")) {
					hmResult.put("ADDWATERMARK", "Commercial Invoice");
					hmResult.put("USERID", strUserId);
				} else {
					hmResult.put("ADDWATERMARK", "Proforma Invoice");
					hmResult.put("SHIPCOST", "0");
					hmResult.put("USERID", strUserId);
				}
				if (strDistType.equals("70105")) { // ICS
					if (hmResult.containsKey("ADDWATERMARK")) {
						hmResult.remove("ADDWATERMARK");
						// hmEuSectonData.remove("CURRENCY");
						hmResult.put("ADDWATERMARK", "Invoice");
						// hmEuSectonData.put("CURRENCY", "USD");
					}

					String strIBAN = GmCommonClass.parseNull(GmCommonClass
							.getRuleValue(strDistId, "IBAN"));
					String strSwiftCode = GmCommonClass.parseNull(GmCommonClass
							.getRuleValue(strDistId, "SWIFTCODE"));
					String strBank = GmCommonClass.parseNull(GmCommonClass
							.getRuleValue(strDistId, "BANK"));
					log.debug("strIBAN is " + strIBAN);
					hmEuSectonData.put("IBAN", strIBAN);
					hmEuSectonData.put("SWIFTCODE", strSwiftCode);
					hmEuSectonData.put("BANKADD", strBank);
				}
				strICTAddress = GmCommonClass.parseNull(GmCommonClass
						.getString("GMADDICT"));
				strInvoiceId = GmCommonClass.parseNull(gmAccountingBean
						.fetchInvoiceIdFromConsignmentId(strConsignId));

			}// End Else Invoice Type 50202
			strAddress = strCompanyName + "\n" + strCompanyAddress + "\n"
					+ strCompanyPhone;
			hmResult.put("GMADDRESS", strAddress);
			hmResult.put("HEADERADDRESS", strICTAddress);
			hmResult.put("TRANSACTIONUM", strInvoiceId);
			hmResult.put("CUSTPO", strCustPONum);
			hmResult.putAll(hmEuSectonData);
			String strRptName = GmCommonClass.parseNull(GmCommonClass
					.getRuleValue(strDistId, "JASPERRPT"));
			if (strRptName.equals("")) {
				strConsignRptName = "/GmEuropeConsignPrint.jasper";
				strPDFFileName = strConsignId + "_EURO.pdf";
			} else {
				strPDFFileName = strConsignId + "_US.pdf";
		}
	}
	hmResult.put("USERID", strUserId);
	// exportJasperToPdf(strConsignRptName,strPDFFileName,hmResult,null);
	hmPDFDetails = new HashMap();
	hmPDFDetails.put("JASPERFILENAME", strConsignRptName);
	hmPDFDetails.put("PDFFILENAME", strPDFFileName);
	alResult.add(hmPDFDetails);

	if (!strShipTo.equals("4121")) {
		// exportJasperToPdf(strConsignRptName,strPDFFileName,hmResult,null);
		hmConsignPDFDetails = new HashMap();
		hmConsignPDFDetails.put("JASPERFILENAME", strConsignRptName);
		hmConsignPDFDetails.put("PDFFILENAME", strPDFFileName);
		alResult.add(hmConsignPDFDetails);
	}
	// For GmConsignAckPrint.jsp
	HashMap hmJasperValues = new HashMap();
	hmJasperValues.putAll(hmResult);
	hmResult = gmCustomerBean.loadConsignAckDetails(strConsignId);
	String strDistName = GmCommonClass.parseNull((String) hmResult
			.get("NAME"));
	strCompDTFmt = GmCommonClass
			.parseNull((String) hmResult.get("CMPDFMT"));
	strAckDt = GmCommonClass.parseNull((String) hmResult.get("UDATE"));
	String strNextDate = GmCommonClass.parseNull((String) hmResult
			.get("NDATE"));
	strCompanyId = GmCommonClass.parseNull((String) hmResult
			.get("COMPANYID"));
	strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
	strCompanyAddress = GmCommonClass.parseNull(rbCompany
			.getProperty(strDivisionID + ".COMPADDRESS"));
	hmJasperValues.putAll(hmValues);
	hmJasperValues.put("DISTNAME", strDistName);
	hmJasperValues.put("UDATE", strConsignDt);
	hmJasperValues.put("ACKDATE", strAckDt);
	hmJasperValues.put("CONSIGNID", strConsignId);
	hmJasperValues.put("COMPID", strCompanyId);
	hmJasperValues.put("COMPANYNAME", strCompanyName);
	hmJasperValues.put("NEXTDATE", strNextDate);
	hmJasperValues.put("COMPANYADDRESS", strCompanyAddress);
	hmJasperValues.put("USERID", strUserId);
	strPDFFileName = strConsignId + "_ACKPRINT.pdf";
	// exportJasperToPdf("/GmConsignAckPDFPrint.jasper",strPDFFileName,hmJasperValues,null);
	hmPDFDetails = new HashMap();
	hmPDFDetails.put("JASPERFILENAME", "/GmConsignAckPDFPrint.jasper");
	hmPDFDetails.put("PDFFILENAME", strPDFFileName);
	alResult.add(hmPDFDetails);
	// For GmConsignPackslipPrint.jsp
	HashMap hmSetDetails = new HashMap();
	ArrayList alDataList = new ArrayList();
	hmSetDetails = gmOperationsBean.loadSavedSetMaster(strConsignId);
	alDataList = (ArrayList) hmSetDetails.get("SETLOAD");
	HashMap hmConDetails = new HashMap();
	hmConDetails = gmCustomerBean.loadConsignDetails(strConsignId,
			strUserId);
	strConsignId = GmCommonClass
			.parseNull((String) hmConDetails.get("CID"));
	String strConsignAdd = GmCommonClass.parseNull((String) hmConDetails
			.get("BILLADD"));
	String strShipAdd = GmCommonClass.parseNull((String) hmConDetails
			.get("SHIPADD"));
	String strUserName = GmCommonClass.parseNull((String) hmConDetails
			.get("NAME"));
	String strCompDateFmt = GmCommonClass.parseNull((String) hmConDetails
			.get("CMPDFMT"));
	String strHeader = "Packing Slip";
	String strPackSlipHeader = GmCommonClass.parseNull(GmCommonClass
			.getRuleValue(strDistId, "PACKSLIP/SHIPLIST"));
	if (strTransType.equals("9110")) {
		strPkSlpHdr = strTransType;
	}
	strPackSlipDt = GmCommonClass.getStringFromDate(
			(java.util.Date) hmConDetails.get("UDATE"), strCompDateFmt);
	if (strPackSlipHeader.equals("SHIPPINGLIST")) {
		strCompanyName = "Globus Medical Gmbh";
		strCompanyAddress = "Gotthardstrasse 3<br>6304 Zug<br>Switzerland <br>";
		strCompanyAddress = strCompanyAddress.replaceAll("<br>", "/");
		strHeader = "Shipping List";
	}
	strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyCD);
	hmJasperValues.put("COMPID", strCompanyId);
	hmJasperValues.put("COMPANYNAME", strCompanyName);
	hmJasperValues.put("CONSIGNID", strConsignId);
	hmJasperValues.put("COMPANYADDRESS", strCompanyAddress);
	hmJasperValues.put("PAGEHEADER", strHeader);
	hmJasperValues.put("IDTYPE", strPackSlipHeader);
	hmJasperValues.put("ADDRESSONE", strConsignAdd);
	hmJasperValues.put("ADDRESSTWO", strShipAdd);
	hmJasperValues.put("DATE", strConsignDt);
	hmJasperValues.put("BYVALUE", strUserName);
	hmJasperValues.put("USERID", strUserId);
	hmJasperValues.put("SCARR", gmTxnShipInfoVO.getShipcarriernm());
	hmJasperValues.put("SMODE", gmTxnShipInfoVO.getShipmodesh());
	hmJasperValues.put("CONSIGNMENTLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("CONSIGNMENT")));
	hmJasperValues.put("CONSIGNEDTOLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("CONSIGNED_TO")));
	hmJasperValues.put("SHIPTOLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("SHIP_TO")));
	hmJasperValues.put("DATELBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("DATE")));
	hmJasperValues.put("CONSIGNNOLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("CONSIGN_NO")));
	hmJasperValues.put("CONSIGNEDBYLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("CONSIGNED_BY")));
	hmJasperValues.put("SHIPMODELBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("SHIP_MODE")));
	hmJasperValues.put("SHIPVIALBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("SHIP_VIA")));
	hmJasperValues.put("TRACKINGNOLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("TRACKING")));
	hmJasperValues.put("NOTESLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("NOTES")));
	hmJasperValues.put("PARTNMLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("PART")));
	hmJasperValues.put("DESCLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("DESCRIPTION")));
	hmJasperValues.put("QTYLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("QTY")));
	hmJasperValues.put("CONTROLLBL", "Lot #");
	hmJasperValues.put("PRICELBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("PRICE")));
	hmJasperValues.put("AMOUNTLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("AMOUNT")));
	hmJasperValues.put("SIGNINLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("SIGN_IN_LBL")));
	hmJasperValues.put("BACKODRLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("BACK_ODR_LBL")));
	hmJasperValues.put("STATUSLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("STATUS_LBL")));
	hmJasperValues.put("TOTALLBL", GmCommonClass
			.parseNull(gmResourceBundleBean.getProperty("TOTAL_LBL")));
	strPDFFileName = strConsignId + "_PACKSLIP.pdf";
	if (strShipTo.equals("4122")) {
		hmPackslipPDFDetails = new HashMap();
		hmPackslipPDFDetails.put("JASPERFILENAME",
				"/GmConsignPackslipPrint.jasper");
		hmPackslipPDFDetails.put("PDFFILENAME", strPDFFileName);
		hmPackslipPDFDetails.put("ALRESULT", alDataList);
		alResult.add(hmPackslipPDFDetails);
	}
	hmJasperValues.put("HMPDFDETAILS", alResult);
	return hmJasperValues;
	}

  /**
   * @author kramasamy
   * @param hmParam
   * @return Hashmap
   * @exception Exception
   * @Description Following method is used to print Loaner acknowledgment paper work based on
   *              Consign ID.
   * */

  public HashMap printLoanerPaperwork(HashMap hmParam) throws Exception {
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean();
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();

    HashMap hmCONSIGNMENT = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmResult = new HashMap();
    String strConsignRptName = "";
    String strPDFFileName = "";
    HashMap hmItemPaperValues = new HashMap();
    ArrayList alResult = new ArrayList();
    GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
    gmTxnShipInfoVO = (GmTxnShipInfoVO) hmParam.get("SHIPINFO");
    gmTxnShipInfoVO = (gmTxnShipInfoVO == null) ? new GmTxnShipInfoVO() : gmTxnShipInfoVO;
    java.sql.Date dtLDate = null;
    java.sql.Date dtEDate = null;
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUserNm = GmCommonClass.parseNull((String) hmParam.get("USERNM"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("STRACTION"));
    int intCount = Integer.parseInt(GmCommonClass.parseZero((String) hmParam.get("COUNT")));
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("SCANID"));
    String strSource = GmCommonClass.parseNull((String) hmParam.get("SOURCE"));
    String strRMAID = GmCommonClass.parseNull((String) hmParam.get("RMAID"));
    String strShipCarrier = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipcarriernm());
    String strShipMode = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipmodesh());

    hmItemPaperValues.put("CONSIGNID", strConsignId);
    hmItemPaperValues.put("TYPEID", strType);
    hmItemPaperValues.put("USERID", strUserId);
    hmItemPaperValues.put("RMAID", strRMAID);
    hmItemPaperValues.put("SHIPCARRIER", strShipCarrier);
    hmItemPaperValues.put("SHIPMODE", strShipMode);

    // When we have one or more CN id based on Scan ID, it should display one ACK print form. So we
    // passed scan id for PDF file name.
    if (strRefId.equals(""))
      strRefId = strConsignId;

    // IH Loaner Item type
    if (strType.equals("9110")) {
      // The method parameters changed from stings to HashMap object for usage of more parameters.
      hmDetails = printItemPaperwork(hmItemPaperValues);
    } else {
      hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);

      dtLDate = (java.sql.Date) hmCONSIGNMENT.get("LDATE");
      dtEDate = (java.sql.Date) hmCONSIGNMENT.get("EDATE");
      String strDateFmt = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("CMPDFMT"));
      String strLnrDate = GmCommonClass.getStringFromDate(dtLDate,strDateFmt);
      String strEndDate = GmCommonClass.getStringFromDate(dtEDate,strDateFmt);
      ArrayList alSet = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");

      String strPrintedDate = gmCalenderOperations.getCurrentDate(strApplnDateFmt);
      String strBillNm = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("BILLNM"));
      String strAckHeader =
          strType.equals("40050") ? "HOSPITAL CONSIGNMENT AGREEMENT"
              : "LOANER ACKNOWLEDGEMENT LETTER";

      strCompanyId = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYID"));
      String strDivisionID = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("DIVISION_ID"));
      strDivisionID = strDivisionID.equals("") ? "2000" : strDivisionID;
      String strCompanyCd = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYCD"));
      strCompanyCd = strCompanyCd.equals("") ? GmCommonClass.parseNull(GmCommonClass.getRuleValue("DEFAULT_COMPANY","ONEPORTAL")) : strCompanyCd;
      HashMap hmValues = GmCommonClass.parseNullHashMap(setCompanyParameters(strDivisionID,strCompanyCd));

      // Need to change this code do not use request object
      hmDetails.put("PRINTBY", strUserNm);
      hmDetails.put("PRINTDTTIME", strPrintedDate);
      hmDetails.put("COMPID", strCompanyId);
      hmDetails.put("TYPE", strType);
      hmDetails.put("BILLNUM", strBillNm);
      hmDetails.put("HEADER", strAckHeader);
      hmDetails.put("USERID", strUserId);
      hmDetails.put("LDATE", strLnrDate);
      hmDetails.put("EDATE", strEndDate);

      // code for Loaner set ACK and set detail
      hmDetails.putAll(hmValues);
      if (intCount == 1) {
        if (strType.equals("4119")) {
          // exportJasperToPdf("/GmInhouseLoanerAckPrint.jasper",strPDFFileName,hmDetails,null);
          strPDFFileName = "InhouseLoanerACK_" + strRefId + ".pdf";
          hmResult.put("JASPERFILENAME", "/GmInhouseLoanerAckPrint.jasper");
          hmResult.put("PDFFILENAME", strPDFFileName);
        } else {
          // exportJasperToPdf(strConsignRptName,strPDFFileName,hmDetails,null);
          strPDFFileName = "GmLoanerACKPrint_" + strRefId + ".pdf";
          hmResult.put("JASPERFILENAME", "/GmLoanerACKPrint.jasper");
          hmResult.put("PDFFILENAME", strPDFFileName);
        }// else condition
        alResult.add(hmResult);
      }// If condition

      String strShipAdd =
          GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmCONSIGNMENT
              .get("SETNAME")));
      // String strUserName = GmCommonClass.parseNull((String)hmCONSIGNMENT.get("ETCHID"));
      strPDFFileName = "LoanerSetPrint_" + strConsignId + ".pdf";
      String strTxnHeader =
          strType.equals("40050") ? "Hospital Consignment"
              : (strType.equals("4119") ? "In-House Loaner" : "Loaner");
      
      String strCompanyLocale = "";
      strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyCd);
      GmResourceBundleBean rbCompany = GmCommonClass.getResourceBundleBean("properties.Company",strCompanyLocale);
      String strCompanyLogo = GmCommonClass.parseNull(rbCompany.getProperty(strDivisionID + ".LOGO"));
      String strLogoImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
      hmDetails.put("LOGOIMAGE", strLogoImagePath + strCompanyLogo+ ".gif");      
      hmDetails.putAll(hmCONSIGNMENT);
      hmDetails.put("LDATE", strLnrDate);
      hmDetails.put("EDATE", strEndDate);
      hmDetails.put("CONSID", strConsignId);
      strConsignId = strConsignId + "$50182";
      hmDetails.put("BARCODECONSID", strConsignId);
      hmDetails.put("STRACTION", strAction);
      hmDetails.put("TXNHEADER", strTxnHeader);
      // exportJasperToPdf("/GmLoanerSetPrint.jasper",strPDFFileName,hmDetails,alSet);
      hmResult = new HashMap();
      hmResult.put("JASPERFILENAME", "/GmLoanerSetPrint.jasper");
      hmResult.put("PDFFILENAME", strPDFFileName);
      hmResult.put("ALRESULT", alSet);
      alResult.add(hmResult);

      hmDetails.put("HMPDFDETAILS", alResult);
    }
    return hmDetails;
  }

  /**
   * @author kramasamy
   * @param hmParam
   * @return Hashmap
   * @exception Exception
   * @Description Following method is used to print In House loaner Item print Paper work for
   *              acknowledgment based on Consign ID and type.
   * */

  public HashMap printItemPaperwork(HashMap hmItemPaperValues) throws Exception {
    HashMap hmRefDetails = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmCONSIGNMENT = new HashMap();
    HashMap hmPDFDetails = new HashMap();
    HashMap hmRADetails = new HashMap();
    ArrayList alChildReq = new ArrayList();
    ArrayList alResult = new ArrayList();
    GmDataStoreVO gmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
    GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean(gmDataStoreVO);
    GmIHLoanerItemBean gmIHLoanerItemBean = new GmIHLoanerItemBean();
    GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();
    GmShipScanReqTransBean gmShipScanReqTransBean = new GmShipScanReqTransBean();
    java.sql.Date dtLDate = null;
    java.sql.Date dtEDate = null;

    String strConsignId = GmCommonClass.parseNull((String) hmItemPaperValues.get("CONSIGNID"));
    String strType = GmCommonClass.parseNull((String) hmItemPaperValues.get("TYPEID"));
    String strUserId = GmCommonClass.parseNull((String) hmItemPaperValues.get("USERID"));
    String strRMAID = GmCommonClass.parseNull((String) hmItemPaperValues.get("RMAID"));
    String strShipCarrier = GmCommonClass.parseNull((String) hmItemPaperValues.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseNull((String) hmItemPaperValues.get("SHIPMODE"));

    // IHLN
    hmRefDetails = gmICTSummaryBean.fetchConsignmentDetails(strConsignId, "null");
    hmCONSIGNMENT = GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNDETAILS"));
    alChildReq =
        GmCommonClass.parseNullArrayList(gmIHLoanerItemBean.fetchChildRequestDetail("",
            strConsignId, strType));


    // Need to move below code to common code
    dtLDate = (java.sql.Date) hmCONSIGNMENT.get("LDATE");
    dtEDate = (java.sql.Date) hmCONSIGNMENT.get("EDATE");

    String strDivisionID = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("DIVISION_ID"));
    strDivisionID = strDivisionID.equals("") ? "2000" : strDivisionID;
    String strCompanyCd = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYCD"));
    strCompanyCd = strCompanyCd.equals("") ? GmCommonClass.parseNull(GmCommonClass.getRuleValue("DEFAULT_COMPANY","ONEPORTAL")) : strCompanyCd;
    String strDateFmt = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("CMPDFMT"));
    
    String strLnrDate = GmCommonClass.getStringFromDate(dtLDate, strDateFmt);
    String strEndDate = GmCommonClass.getStringFromDate(dtEDate, strDateFmt);
    
    String strCompanyLocale = "";
    strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyCd);
    GmResourceBundleBean rbCompany = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    
    GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    hmDetails.put("CONTROLLBL",
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONTROL_NUMBER")));
    
    strCompanyId = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYID"));
    strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
    strCompanyName = GmCommonClass.parseNull(rbCompany.getProperty(strDivisionID + ".COMPNAME"));
    strCompanyAddress = GmCommonClass.parseNull(rbCompany.getProperty(strDivisionID + ".COMPADDRESS"));
    strCompanyPhone = GmCommonClass.parseNull(rbCompany.getProperty(strDivisionID + ".PH"));
    strCompanyFax = GmCommonClass.parseNull(rbCompany.getProperty(strDivisionID + ".FAX"));
    
    HashMap hmValues = GmCommonClass.parseNullHashMap(setCompanyParameters(strDivisionID,strCompanyCd));

    hmDetails.put("LDATE", strLnrDate);
    hmDetails.put("EDATE", strEndDate);
    hmDetails.put("COMPID", strCompanyId);
    hmDetails.put("USERID", strUserId);
    strCompanyAddress.replaceAll("/", "\n");
    hmDetails.put("COMPADDRESS", strCompanyAddress);
    hmDetails.put("COMPPHONE", strCompanyPhone);
    hmDetails.put("COMPFAX", strCompanyFax);
    hmDetails.put("COMPNAME", strCompanyName);
    hmDetails.putAll(hmValues);
    // code for GmInhouseLoaner ACK and item detail
    String strPDFFileName = "InhouseLoanerACK_" + strConsignId + ".pdf";
    String strCompanyLogo = GmCommonClass.parseNull(rbCompany.getProperty(strDivisionID + ".LOGO"));
    // exportJasperToPdf("/GmInhouseLoanerAckPrint.jasper",strPDFFileName,hmDetails,null);
    hmPDFDetails.put("JASPERFILENAME", "/GmInhouseLoanerAckPrint.jasper");
    hmPDFDetails.put("PDFFILENAME", strPDFFileName);
    alResult.add(hmPDFDetails);

    String strLogoImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
    hmDetails.put("LOGOIMAGE", strLogoImagePath + strCompanyLogo + ".gif");
    hmResult = GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNCHILDDETAILS"));
    hmResult.put("SUBCHILDREQUEST", alChildReq);
    hmResult.put("VARIABLECURRENCY", "USD");
    hmResult.put("SHIPCOST", "0");
    hmResult.put("ADDWATERMARK", "");
    hmResult.put("USERID", strUserId);
    hmResult.putAll(hmCONSIGNMENT);
    hmResult.putAll(hmDetails);
    hmResult.putAll(hmValues);
    hmResult.put("SCARR", strShipCarrier);
    hmResult.put("SMODE", strShipMode);
    strPDFFileName = "InhouseItemDtl_" + strConsignId + ".pdf";

    if (!strType.equals("4119")) {
      hmPDFDetails = new HashMap();
      // exportJasperToPdf("/GmInhouseLoanerItemPrint.jasper",strPDFFileName,hmResult,null);
      hmPDFDetails.put("JASPERFILENAME", "/GmInhouseLoanerItemPrint.jasper");
      hmPDFDetails.put("PDFFILENAME", strPDFFileName);
      alResult.add(hmPDFDetails);
    }

    // The following Code is written for printing the Return Summary Transaction paperwork along
    // with the other IHLN-XXXXX paperworks.
    if (!strRMAID.equals("")) {
      hmRADetails = printIHLNPaperworkPrint(strRMAID,strDateFmt);
      hmResult.putAll(hmRADetails);
      hmPDFDetails = new HashMap();// get the jasper filename, pdf name and arraylist of RA
                                   // detailsto hashmap
      hmPDFDetails.put("JASPERFILENAME", hmRADetails.get("JASPERFILENAME"));
      hmPDFDetails.put("PDFFILENAME", hmRADetails.get("PDFFILENAME"));
      hmPDFDetails.put("ALRESULT", hmRADetails.get("ALRESULT"));
      alResult.add(hmPDFDetails);
    }
    hmResult.put("HMPDFDETAILS", alResult);
    return hmResult;
  }


  /**
   * @author kramasamy
   * @param hmParam
   * @return Hashmap
   * @exception Exception
   * @Description Following method is used to print Orders Pack Slip based on Order ID.
   * */

  public HashMap printOrderPaperwork(HashMap hmParam) throws Exception {

	String strHeadAddress = "";
	String strOrderId = GmCommonClass.parseNull((String) hmParam
			.get("TXNID"));
	String strUserId = GmCommonClass.parseNull((String) hmParam
			.get("USERID"));
	String strUserNm = GmCommonClass.parseNull((String) hmParam
			.get("USERNM"));
	String strProjectType = "";
	String blBackFl = "false";

	HashMap hmReturn = new HashMap();
	HashMap hmOrderDetails = new HashMap();
	HashMap hmPDFDetails = new HashMap();

	ArrayList alCartDetails = new ArrayList();
	ArrayList alBackOrderDetails = new ArrayList();
	ArrayList alResult = new ArrayList();

	GmCustomerBean gmCustomerBean = new GmCustomerBean();
	hmReturn = gmCustomerBean.loadOrderDetails(strOrderId, strProjectType);
	alCartDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn
			.get("CARTDETAILS"));
	alBackOrderDetails = GmCommonClass
			.parseNullArrayList((ArrayList) hmReturn.get("BACKORDER"));
	hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn
			.get("ORDERDETAILS"));

	strCompanyId = GmCommonClass.parseNull((String) hmOrderDetails
			.get("COMPID"));
	String strParentId = GmCommonClass.parseNull((String) hmOrderDetails
			.get("PARENTORDID"));
	String strTrack = GmCommonClass.parseNull((String) hmOrderDetails
			.get("TRACKNO"));
	String strOrdId = (String) hmOrderDetails.get("ID");

	String strDivisionID = GmCommonClass.parseNull((String) hmOrderDetails
			.get("DIVISION_ID"));
	strDivisionID = strDivisionID.equals("") ? "2000" : strDivisionID;
	String strCompanyCd = GmCommonClass.parseNull((String) hmOrderDetails
			.get("COMPANYCD"));
	strCompanyCd = strCompanyCd.equals("") ? GmCommonClass
			.parseNull(GmCommonClass.getRuleValue("DEFAULT_COMPANY",
					"ONEPORTAL")) : strCompanyCd;
	String strDateFmt = GmCommonClass.parseNull((String) hmOrderDetails
			.get("CMPDFMT"));
	HashMap hmValues = GmCommonClass
			.parseNullHashMap(setCompanyParameters(strDivisionID,
					strCompanyCd));
	strOrderId = GmCommonClass.parseNull((String) hmOrderDetails.get("ID"));
	String strOrderDt = GmCommonClass.getStringFromDate(
			(java.util.Date) hmOrderDetails.get("ODT"), strDateFmt);
	hmOrderDetails.put("ODT", strOrderDt);

	/*
	 * In Modify Order Screen, if we clicked on the Order ID before shipping
	 * out, PACKING Slip is comming with back order details. After Ship Out,
	 * if we need Back Order details, need to remove the bellow IF condtion
	 * alone.
	 */
	if (strTrack.equals("")) {
		hmOrderDetails.put("ALBACKORDER", alBackOrderDetails);
		hmOrderDetails.put("ALBACKORDERSIZE", alBackOrderDetails.size());
	}
	hmOrderDetails.put("USERID", strUserId);
	hmOrderDetails.put("COMPID", strCompanyId);
	hmOrderDetails.putAll(hmValues);
	if (!strParentId.equals(strOrdId) || !strTrack.equals("")) {
		blBackFl = "true";
	}
	String strPDFFileName = strOrderId + ".pdf";
	// If back order flag is true then need to display notes comments in
	// PDF.
	hmOrderDetails.put("BLBACKFL", blBackFl);
	// exportJasperToPdf("/GmOrderPackSlipPrint.jasper",strPDFFileName,hmOrderDetails,alCartDetails);

	hmPDFDetails.put("JASPERFILENAME", "/GmOrderPackSlipPrint.jasper");
	hmPDFDetails.put("PDFFILENAME", strPDFFileName);
	hmPDFDetails.put("ALRESULT", alCartDetails);
	alResult.add(hmPDFDetails);
	hmOrderDetails.put("HMPDFDETAILS", alResult);
	return hmOrderDetails;
}

  /**
   * @author kramasamy
   * @param hmParam
   * @return Hashmap
   * @exception Exception
   * @Description Following method is used to print Loaner Extension Pick Slip based on Consign ID.
   * */
	public HashMap printLoanerExtensionPaperwork(HashMap hmParams)
		throws Exception {
	HashMap hmValues = new HashMap();
	HashMap hmResult = new HashMap();
	HashMap hmJasperDetails = new HashMap();
	HashMap hmPDFDetails = new HashMap();
	GmDataStoreVO gmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
	GmLogisticsBean gmLogisticsBean = new GmLogisticsBean(gmDataStoreVO);
	HashMap hmConsignDetails = new HashMap();
	GmCommonBean gmCommonBean = new GmCommonBean();

	String strCompanyLocale = "";

	ArrayList alResult = new ArrayList();

	String strComments = "";
	String strRemVoidFl = GmCommonClass.parseNull((String) hmParams
			.get("REMOVFL"));
	// hRemVoidFl parameter is coming from status log report.This report
	// should open PicSlip for voided txns also.
	String strConsignId = GmCommonClass.parseNull((String) hmParams
			.get("TXNID"));
	String strSource = GmCommonClass.parseNull((String) hmParams
			.get("SOURCE"));
	String strPrintedBy = "";
	String strUserId = GmCommonClass.parseNull((String) hmParams
			.get("USERID"));
	String strReportName = "/GmInHouseItemPicSlipPrint.jasper";
	String strType = "";
	String strSlipHeader = "Pick Slip";
	String strdetails = "Transaction Details";
	String strToHeader = "";
	String ruleCall = "";
	boolean flg = false;

	hmValues.put("CONSIGNID", strConsignId);
	hmValues.put("TYPE", strType);
	hmValues.put("REMVOIDFL", strRemVoidFl);
	hmValues.put("USERID", strUserId);
	hmResult = gmLogisticsBean.viewInHouseTransItemsDetails(hmValues);

	ArrayList alSetdetails = GmCommonClass
			.parseNullArrayList((ArrayList) hmResult.get("SETLOAD"));
	/*
	 * HashMap hmUserNameVal = new HashMap(); hmUserNameVal =
	 * (HashMap)hmResult.get("USERNAME"); strPrintedBy =
	 * GmCommonClass.parseNull((String)hmUserNameVal.get("PRINTUSER"));
	 */
	strPrintedBy = GmCommonClass.parseNull((String) hmParams
			.get("USERSHNM")); // The short name can be taken from GmUserVO,
								// so above code are commented
	hmConsignDetails = gmLogisticsBean
			.viewInHouseTransDetails(strConsignId);

	GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
	strConsignId = GmCommonClass.parseNull((String) hmConsignDetails
			.get("CID"));
	String strSetId = GmCommonClass.parseNull((String) hmConsignDetails
			.get("SETID"));
	String strSetName = GmCommonClass.parseNull((String) hmConsignDetails
			.get("SETNM"));
	String strAccName = GmCommonClass.parseNull((String) hmConsignDetails
			.get("ANAME"));
	String strDistName = GmCommonClass.parseNull((String) hmConsignDetails
			.get("DNAME"));
	strAccName = strAccName.equals("") ? strDistName : strAccName;
	String strUserName = GmCommonClass.parseNull((String) hmConsignDetails
			.get("UNAME"));
	String strIniDate = GmCommonClass.parseNull((String) hmConsignDetails
			.get("CDATE"));
	String strDesc = GmCommonClass.parseNull((String) hmConsignDetails
			.get("COMMENTS"));
	strType = GmCommonClass
			.parseNull((String) hmConsignDetails.get("TYPE"));
	String strCtype = GmCommonClass.parseNull((String) hmConsignDetails
			.get("CTYPE"));
	String strInHousePurpose = GmCommonClass
			.parseNull((String) hmConsignDetails.get("PURP"));
	String strBillNm = GmCommonClass.parseNull((String) hmConsignDetails
			.get("HISTBILLNM"));
	String strRequestorName = GmCommonClass
			.parseNull((String) hmConsignDetails.get("SHIPADD"));
	String strConsignStatusFlag = GmCommonClass
			.parseNull((String) hmConsignDetails.get("SFL"));
	String strCarrier = GmCommonClass.parseNull((String) hmConsignDetails
			.get("CARRIER"));
	String strMode = GmCommonClass.parseNull((String) hmConsignDetails
			.get("SHIPMODE"));
	String strTrackno = GmCommonClass.parseNull((String) hmConsignDetails
			.get("TRACKNO"));
	String strEtchId = GmCommonClass.parseNull((String) hmConsignDetails
			.get("ETCHID"));
	strEtchId = strEtchId.equals("") ? "-" : strEtchId;
	String strRefId = GmCommonClass.parseNull((String) hmConsignDetails
			.get("REFID"));
	String strLog_Comments = GmCommonClass
			.parseNull((String) hmConsignDetails.get("LOG_COMMENTS"));
	String strCompanyId = GmCommonClass.parseNull((String) hmConsignDetails
			.get("COMPANYID"));

	String strDivisionID = "2000";
	String strCompanyCd = GmCommonClass.parseNull((String) hmConsignDetails
			.get("COMPANYCD"));
	strCompanyCd = strCompanyCd.equals("") ? GmCommonClass
			.parseNull(GmCommonClass.getRuleValue("DEFAULT_COMPANY",
					"ONEPORTAL")) : strCompanyCd;
	String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");

	strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyCd);
	GmResourceBundleBean rbCompany = GmCommonClass.getResourceBundleBean(
			"properties.Company", strCompanyLocale);
	String strCompanyLogo = GmCommonClass.parseNull(rbCompany
			.getProperty(strDivisionID + ".LOGO"));
	strImagePath = strImagePath + strCompanyLogo + ".gif";

	if (strCtype.equals("50154")) {// Move from Shelf to Loaner Extension
									// w/Replenish
		strToHeader = "Replenished To";
	} else if (strCtype.equals("9110") || strCtype.equals("1006572")
			|| strCtype.equals("1006571") || strCtype.equals("1006570")
			|| strCtype.equals("1006573") || strCtype.equals("50150")
			|| strCtype.equals("1006575")) {// ISMP,ISBO,BLIS,IHIS,FGIS,IHLE
		strToHeader = "Loaned To";
	} else {
		strToHeader = "Consigned To";
	}
	if (strConsignStatusFlag.equals("4") && strCtype.equals("50154")) {// Packslip
		strSlipHeader = "Packing Slip";
		strdetails = "Transaction Details";
		flg = true;
	}
	if (strCtype.equals("100062")) {
		strRequestorName = "<b><font color=\"red\">Back Order</font></b>";
	}
	String strRequestCommnets = "";
	ArrayList alLogList = new ArrayList();
	alLogList = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(
			strConsignId, "1268"));
	if (alLogList.size() > 0) {
		for (int i = 0; i < alLogList.size(); i++) {
			HashMap hmLog = (HashMap) alLogList.get(i);
			strComments += GmCommonClass.parseNull((String) hmLog
					.get("COMMENTS"));
			strComments += "<BR>";
		}
		strRequestCommnets = strComments;
	}

	if (strConsignStatusFlag.equals("2")
			|| strConsignStatusFlag.equals("3") || strCtype.equals("50154")) {
		strDesc += "<BR>" + strRequestCommnets + "<BR>" + strLog_Comments;
	}

	String strPDFFileName = strConsignId + ".pdf";

	String strId = strConsignId + "$" + strSource;
	String strRuleId = strConsignId + "$" + strCtype;

	hmJasperDetails.put("COMMENTS", strDesc);
	hmJasperDetails.put("COMPID", strCompanyId);
	hmJasperDetails.put("HEADERNAME", strSlipHeader);
	hmJasperDetails.put("CONSIGNID", strConsignId);
	hmJasperDetails.put("SOURCE", strSource);
	hmJasperDetails.put("CTYPE", strCtype);
	hmJasperDetails.put("TYPE", strType);
	hmJasperDetails.put("INHOUSEPURPOSE", strInHousePurpose);
	hmJasperDetails.put("CONSIGNSTATUSFLAG", strConsignStatusFlag);
	hmJasperDetails.put("CONSIGNEDTOHDR", strToHeader);
	hmJasperDetails.put("BILLTONAME", strBillNm);
	hmJasperDetails.put("ACCNAME", strAccName);
	hmJasperDetails.put("SHIPTONAME", strRequestorName);
	hmJasperDetails.put("REQDATE", strIniDate);
	hmJasperDetails.put("REFID", strRefId);
	hmJasperDetails.put("INITIATEDBY", strUserName);
	hmJasperDetails.put("SHIPCARRIER", strCarrier);
	hmJasperDetails.put("SHIPMODE", strMode);
	hmJasperDetails.put("TRACKNO", strTrackno);
	hmJasperDetails.put("TRANSDETAILSHDR", strdetails);
	hmJasperDetails.put("SETNAME", strSetName);
	hmJasperDetails.put("SETID", strSetId);
	hmJasperDetails.put("ETCHID", strEtchId);
	hmJasperDetails.put("SHIPPINGFLAG", flg);
	hmJasperDetails.put("PRINTUSER", strPrintedBy);
	hmJasperDetails.put("USERID", strUserId);
	hmJasperDetails.put("IMAGEWITHPATH", strImagePath);

	int intSize = alSetdetails.size();
	if (intSize > 0) {
		for (int i = 0; i < intSize; i++) {
			HashMap hcboVal = new HashMap();
			hcboVal = GmCommonClass.parseNullHashMap((HashMap) alSetdetails
					.get(i));
			// Get the part desc with TM symbol.
			hcboVal.put("PDESC", GmCommonClass
					.getStringWithTM((String) hcboVal.get("PDESC")));
			alSetdetails.set(i, hcboVal);
		}
	}
	if (intSize == 0) {
		HashMap hmTemp = new HashMap();
		hmTemp.put("PNUM", "");
		hmTemp.put("PDESC",
				"<font color='#FF0000'>Transaction does not contain any part numbers !</font>");
		hmTemp.put("IQTY", "");
		hmTemp.put("CNUM", "");
		alSetdetails.add(hmTemp);
	}
	ArrayList alRuleInfo = new ArrayList();
	int alRuleInfoSize = 0;
	int alPartyListSize = 0;
	if (ruleCall.equals("Y")) {
		alRuleInfoSize = alRuleInfo.size();
		String strPartyId = "";
		ArrayList alPartyList = gmAccessControlBean.getPartyList(
				"CNOVERRIDE", strPartyId);
		alPartyListSize = alPartyList.size();
	}

	hmJasperDetails.put("ALRULEINFO", alRuleInfo);
	hmJasperDetails.put("ALRULEINFOSIZE", alRuleInfoSize);
	hmJasperDetails.put("ALPARTYLISTSIZE", alPartyListSize);
	String strPath = GmCommonClass.parseNull(GmCommonClass
			.getString("GMSUBREPORTLOCATION"));
	hmJasperDetails.put("SUBREPORT_DIR", strPath);
	// exportJasperToPdf(strReportName,strPDFFileName,hmJasperDetails,alSetdetails);

	hmPDFDetails.put("JASPERFILENAME", strReportName);
	hmPDFDetails.put("PDFFILENAME", strPDFFileName);
	hmPDFDetails.put("ALRESULT", alSetdetails);
	alResult.add(hmPDFDetails);
	hmJasperDetails.put("HMPDFDETAILS", alResult);

	return hmJasperDetails;
}

/**
 * @Description: Method which is returning the Pending Return Summary
 *               Transaction details for the corrsponding IHLN-XXXX paper
 *               work
 * @Param : String strRMAID
 * @Return : HashMap
 * @Author : HReddi
 **/
public HashMap printIHLNPaperworkPrint(String strRMAID, String strDateFmt)
		throws Exception {
	GmIHLoanerItemBean gmIHLoanerItemBean = new GmIHLoanerItemBean();
	GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean();

	HashMap hmReturn = new HashMap();
	HashMap hmParam = new HashMap();
	HashMap hmResult = new HashMap();
	HashMap hmReturnDetails = new HashMap();
	ArrayList alItemList = new ArrayList();
	ArrayList alRADetails = new ArrayList();
	java.sql.Date dtEDate = null;
	java.sql.Date dtRetDate = null;
	String strEDate = "";

	// String strApplnDateFmt = "MM/dd/yyyy";
	String strCreditDate = "";
	String strStatus = "";
	String strTitle = "";
	String strRADate = "";
	String strCountryId = "";
	String strNativeCurrency = "";
	String strPDFFileName = "";

	if (!strCountryId.equals("") && !strCountryId.equals("1101")) { // For
																	// OUS
		hmParam.put("TXNID", strRMAID);
		hmReturn = gmProcessReturnsBean.loadOUSCreditReport(hmParam);
		strNativeCurrency = GmCommonClass.parseNull(GmCommonClass
				.getRuleValue(strCountryId, "CNTYCURR"));
		strPDFFileName = "OUSCreditMemo_" + strRMAID + ".pdf";
		hmResult.put("JASPERFILENAME", "/GmOUSCreditMemoPrint.jasper");
	} else {// For US
		hmReturn = gmProcessReturnsBean.loadCreditReport(strRMAID, "PRINT");
		strNativeCurrency = GmCommonClass.parseNull(GmCommonClass
				.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));
		strPDFFileName = "CreditMemo_" + strRMAID + ".pdf";
		hmResult.put("JASPERFILENAME", "/GmCreditMemoPrint.jasper");
	}
	hmReturnDetails = (HashMap) hmReturn.get("RADETAILS");
	alItemList = (ArrayList) hmReturn.get("RAITEMDETAILS");
	// dtEDate = (java.sql.Date)hmReturnDetails.get("EDATE");
	// dtRetDate = (java.sql.Date)hmReturnDetails.get("RETDATE");
	strEDate = GmCommonClass.getStringFromDate(
			(java.util.Date) hmReturnDetails.get("EDATE"), strDateFmt);
	strStatus = GmCommonClass.parseNull(((String) hmReturnDetails
			.get("STATUS")));
	strRADate = GmCommonClass.getStringFromDate(
			(java.util.Date) hmReturnDetails.get("RETDATE"), strDateFmt);
	// strRADate =
	// GmCommonClass.getStringFromDate(dtRetDate,);
	strRADate = strRADate.equals("") ? "NA" : strRADate;
	strCreditDate = GmCommonClass.getStringFromDate(
			(java.util.Date) hmReturnDetails.get("CREDITDATE"), strDateFmt);

	if (strStatus.equals("2")) {
		strTitle = "Credit Summary List";
		strCreditDate = "NA";
	} else {
		strTitle = "Pending Returns Summary List";
		strCreditDate = "NA";
	}
	hmReturnDetails.put("EDATE", strEDate);
	hmReturnDetails.put("TITLE", strTitle);
	hmReturnDetails.put("PO", "NA");
	hmReturnDetails.put("INVID", "NA");
	hmReturnDetails.put("CREDITINVID", "NA");
	hmReturnDetails.put("CREDITDATE", strCreditDate);
	hmReturnDetails.put("RADATE", strRADate);
	hmReturnDetails.put("ITEMLISTSIZE", alItemList.size());
	hmReturnDetails.put("NATIVECURRENCY", strNativeCurrency);
	hmReturnDetails.put("SCOST",
			GmCommonClass.parseZero((String) hmReturnDetails.get("SCOST")));
	hmResult.putAll(hmReturnDetails);
	hmResult.put("PDFFILENAME", strPDFFileName);
	alRADetails.addAll(alItemList);
	hmResult.put("ALRESULT", alRADetails);
	return hmResult;
}

  /**
   * @param Hashmap
   * @return Hashmap
   * @exception Exception
   * @Description Export jasper to PDF print report.
   * */
  public synchronized void exportJasperToPdf(HashMap hmParam) throws Exception {
    long lStart = System.currentTimeMillis();
    String strConsignRptName = GmCommonClass.parseNull((String) hmParam.get("JASPERFILENAME"));
    String strPrintPaperwork = GmCommonClass.parseNull(GmCommonClass.getString("PRINT_PAPERWORK"));
    String strPdfName = GmCommonClass.parseNull((String) hmParam.get("PDFFILENAME"));
    HashMap hmResult = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMDETAILS"));
    ArrayList alList = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT"));

    log.debug("HmParam =====>" + hmParam);

    GmJasperReport gmJasperReport = new GmJasperReport();
    GmBatchBean gmBatchBean = new GmBatchBean();
    HashMap hmPrintParam = new HashMap();

    String strPath = GmCommonClass.parseNull(GmCommonClass.getString("GMSUBREPORTLOCATION"));
    String strUserId = GmCommonClass.parseNull((String) hmResult.get("USERID"));
    String strUserNm = GmCommonClass.parseNull((String) hmResult.get("USERNM"));

    hmResult.put("SUBREPORT_DIR", strPath);
    gmJasperReport.setPageHeight(900);
    gmJasperReport.setJasperReportName(strConsignRptName);
    gmJasperReport.setHmReportParameters(hmResult);
    if (alList.size() > 0) {
      gmJasperReport.setReportDataList(alList);
    } else {
      gmJasperReport.setReportDataList(null);
    }
    strPdfName = strPrintPaperwork + "\\" + strUserId + lStart + "_" + strPdfName;
    log.debug("strPdfName = " + strPdfName);
    gmJasperReport.exportJasperToPdf(strPdfName);
    hmPrintParam.put("PDFFILENAME", strPdfName);
    hmPrintParam.put("RULEID", strUserId);
    
    if(GmCommonClass.shipPrintThruJMS.equalsIgnoreCase("YES"))
    {
    	log.debug("strPrintThruJMS exportJasperToPdf()= " + GmCommonClass.shipPrintThruJMS);
	    String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("SHIPPING_REG_PRINTER_JOB"));
	    log.debug("strConsumerClass = " + strConsumerClass);
	    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_SHIPPING_REG_PRINTER_QUEUE"));
	    log.debug("strQueueName = " + strQueueName);
	    GmQueueProducer qprod = new GmQueueProducer();
	    GmMessageTransferObject tf = new GmMessageTransferObject();
	    log.debug("Regular Printer JMS Set message = " + hmPrintParam);
	    tf.setMessageObject(hmPrintParam);
	    log.debug("Regular Printer JMS consumer class = " + strConsumerClass);
	    tf.setConsumerClass(strConsumerClass);
	    log.debug("Regular Printer JMS send message = " + strQueueName);
	    qprod.sendMessage(tf, strQueueName);
	    log.debug("End of Regular Printer..");
    }
    else{
    	gmBatchBean.batchPrint(hmPrintParam);	
    }
    // printPDFFileDeleted(strPdfName);

  }

  /**
   * This method will delete current PDF file when restart the server
   * 
   * @author Velu
   * @param String
   */

  public void printPDFFileDeleted(String strPdfName) throws Exception {
    File newPDFFile = new File(strPdfName);
    if (newPDFFile.exists()) {
      newPDFFile.deleteOnExit();
      log.debug("File Deleted == " + newPDFFile);
    }
  }
}
