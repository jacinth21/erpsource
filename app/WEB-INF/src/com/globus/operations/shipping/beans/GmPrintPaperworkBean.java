/**
 * 
 */
package com.globus.operations.shipping.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import oracle.jdbc.driver.OracleTypes;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;


/**
 * @author rshah
 *
 */
public class GmPrintPaperworkBean {
	
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	
	/*
	 * @author Ritesh
	 * Following method is used to fetch the transaction information based on scanid and type.
	 */			
	public HashMap fetchShippingTxnDetails(HashMap hmParam)
	{
		HashMap hmTxnShipInfo  = new HashMap();
  		HashMap hmTxnShipAddr  = new HashMap();
  		
  		GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();
		
  		String strAction = "";
		String strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
  		String strSource = GmCommonClass.parseNull((String)hmParam.get("SOURCE"));
  		
	    hmTxnShipInfo = gmShippingInfoBean.fetchShippingDetails(strTxnId,strSource,strAction);
	    hmTxnShipAddr = gmShippingInfoBean.fetchShipAddress(strTxnId,strSource);
		hmTxnShipInfo.putAll(hmTxnShipAddr);
		
		return hmTxnShipInfo;
	}
	
	/**
	 * This method will fetch the weight for CN/LN
	 * 
	 * @author  rshah
	 * @param String
	 */
	public String getToteBinWeight(String strScanId, String strScanType) throws AppError{
		
		String strToteWeight = "";
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setFunctionString("gm_pkg_ship_scan_rpt.get_tote_bin_weight", 2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strScanId); 
		gmDBManager.setString(3, strScanType);
		gmDBManager.execute();
		strToteWeight = GmCommonClass.parseNull(gmDBManager.getString(1));
		gmDBManager.close();
		
		return strToteWeight;
	}
	
	/**
	 * This method will fetch the weight respective transaction
	 * 
	 * @author  rshah
	 * @param String
	 */
	public String getTxnWeight(String strTxnId, String strSource) throws AppError{
		
		String strToteWeight = "";
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setFunctionString("gm_pkg_ship_scan_rpt.get_txn_set_weight", 2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strTxnId); 
		gmDBManager.setString(3, strSource);
		gmDBManager.execute();
		strToteWeight = gmDBManager.getString(1);
		gmDBManager.close();
		
		return strToteWeight;
	}
	
	/**@param Hashmap
	 * @return Hashmap
	 * @exception Exception 
	 * @Description Following method used to Print Transaction Paper work. 
	 * */
	public String printPaperwork(HashMap hmParam)  
	{
		GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();
		
		String strScanId = GmCommonClass.parseNull((String)hmParam.get("SCANID"));
		String strScanType = GmCommonClass.parseNull((String)hmParam.get("SCANTYPE"));
		String strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
  		String strSource = GmCommonClass.parseNull((String)hmParam.get("SOURCE"));
  		String strPaperwork = "Y";
  		int intIncr = 1;
  		String strValue = "";
		try {
	  		if (!strScanId.equals(""))
	  		{
				ArrayList alReturn = GmCommonClass.parseNullArrayList(gmShippingInfoBean.fetchShippingReport(hmParam));
				Iterator itrHashMap = alReturn.iterator();
				//Below while loop is added to iterate through the all transaction fetched from the tote/station.
				while(itrHashMap.hasNext())
				{
					HashMap hmMap =  (HashMap)itrHashMap.next();
					Iterator itrKeys = hmMap.keySet().iterator();
					log.debug("hmMap =="+hmMap);
					hmParam.put("COUNT", intIncr+"");
					//below while loop is added to loop through hashmap to get proper source and REFID.
					//We do not required below loop. we can get all from first hashmap using key. To be changed.
					while(itrKeys.hasNext())
					{
						String strKey = (String)itrKeys.next();
						strValue = String.valueOf(hmMap.get(strKey));
						log.debug("strValue=="+strValue);
						strTxnId = String.valueOf(hmMap.get("REFID"));
						hmParam.put("TXNID", strTxnId);
						hmParam.put("SOURCE", strValue);
						printTxnPaperwork(hmParam);	
						// check inhouse loaner and loaner which need only one acknowledge paperwork. 
						if (strValue.equals("50182")|| strValue.equals("50186")) 
						{
							intIncr++;							
						}
					}
					
				}
	  		}
	  		else if (!strTxnId.equals(""))
	  		{
	  			hmParam.put("COUNT", intIncr+"");
	  			printTxnPaperwork(hmParam);
	  		}
		}
		catch(Exception ex)
		{
			strPaperwork = "N";
			ex.printStackTrace();
		}
		return strPaperwork;
  			
	}
	
	/**@param Hashmap
	 * @return Hashmap
	 * @exception Exception 
	 * @Description Following method used to export JASPER to PDF for the Transactions. 
	 * */
	public void printTxnPaperwork(HashMap hmParam) throws Exception
	{
		String strType = "";
		HashMap hmTxnHeader = new HashMap();
		GmInHouseSetsReportBean gmInHouseSetsReportBean = new GmInHouseSetsReportBean();
		GmShipScanReqTransBean gmShipScanReqTransBean = new GmShipScanReqTransBean();
		GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();
		
		HashMap hmResult = new HashMap();
		HashMap hmValues = new HashMap();
		HashMap hmReturn = new HashMap();
		
		String strSource = GmCommonClass.parseNull((String)hmParam.get("SOURCE"));
		String strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
		String strUserID = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		GmPrintTransactionsBean gmPrintTransactionsBean = new GmPrintTransactionsBean();
		String strRMAID = "";

		// Take Loaner and InHouse Loaner Items to print paper work transaction
  		if (strSource.equals("50182") || strSource.equals("50186"))
		{
  			hmTxnHeader = gmInHouseSetsReportBean.getConsignDetails(strTxnId);
  			String strTxnType = GmCommonClass.parseNull((String)hmTxnHeader.get("CTYPE"));
  			strType = strSource.equals("50186")?"9110":"4127";
  			// FOr IHLN-XXXX we need to Generate the Return Summary Transaction while doing from Pending Shipping to Packing In Progress
  			if(strSource.equals("50186")){
  				// Calling this method is for verifying weather the IHLN is having RA or not.
  				// If we have RA, then we are not generatng Pending Return Summary Transaction. we are using the first created RS only.
  				strRMAID = GmCommonClass.parseNull((String)gmShipScanReqRptBean.fetchAssociateRA(strTxnId));
  				if(strRMAID.equals("")){
  					// Calling this method for Generating Pending Return Summary Transaction for IHLN-XXXXX
  					strRMAID = gmShipScanReqTransBean.saveReturnSummary(strTxnId,strSource,strUserID);
  				}
  			}
  			strType = strTxnType.equals("4119")? strTxnType : strType;
  			hmParam.put("TYPE", strType);
  			hmParam.put("RMAID", strRMAID);
  			hmResult = gmPrintTransactionsBean.printLoanerPaperwork(hmParam);
		}
		// Take Consignments Items to print paper work transaction
		if (strSource.equals("50181"))  
		{
			hmResult = gmPrintTransactionsBean.printCNPaperwork(hmParam);
		}
		// Take Order Items to print paper work transaction
		if (strSource.equals("50180"))
		{
			hmResult = gmPrintTransactionsBean.printOrderPaperwork(hmParam);
		}
		// Take Loaner Extension Items to print paper work transaction
		if (strSource.equals("50183")){	 
			hmParam.put("SOURCE", strSource);
			hmResult = gmPrintTransactionsBean.printLoanerExtensionPaperwork(hmParam);
		}
		
		ArrayList alResult = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("HMPDFDETAILS"));
		
		
		// Take Jasper Files for the current transaction and will call exportJaspertoPdf to print.
		log.debug("The alResult Size ==>"+alResult.size());
		if(alResult.size() > 0){
			for(int i = 0; i < alResult.size(); i++){
				hmValues = (HashMap)alResult.get(i);
				ArrayList alDataList = GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("ALRESULT"));
				hmReturn.put("JASPERFILENAME",GmCommonClass.parseNull((String)hmValues.get("JASPERFILENAME")));
				hmReturn.put("PDFFILENAME",GmCommonClass.parseNull((String)hmValues.get("PDFFILENAME")));
				hmReturn.put("HMDETAILS",GmCommonClass.parseNullHashMap(hmResult));
				hmReturn.put("ALRESULT",alDataList);
				gmPrintTransactionsBean.exportJasperToPdf(hmReturn);
			}// End For loop
		}// If condition closed
	}//End Method 
	
    /** getDivisionID - This method used to get Shipping division Id 
	* @param strTxnId
	* @param strSource
	* @return strDivisionID
	*/
   public String getDivisionID(String strTxnId, String strSource) throws AppError{
			String strDivisionID="";
			
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setFunctionString("get_shipping_division_id", 2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strTxnId);
		gmDBManager.setString(3, strSource);
		gmDBManager.execute();
		strDivisionID =  GmCommonClass.parseNull(gmDBManager.getString(1));
		gmDBManager.close();
	    log.debug("strDivisionID =="+strDivisionID);
		return strDivisionID;
	}	
	
}// End Class
