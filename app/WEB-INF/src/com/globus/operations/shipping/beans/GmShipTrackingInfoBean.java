/**
 * 
 */
package com.globus.operations.shipping.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.valueobject.operations.shipping.GmShipScanReqVO;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.fedex.client.GmFedexShipLabelClient;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.common.fedex.client.GmShipTrackingClient;

/**
 * @author rshah
 *
 */
public class GmShipTrackingInfoBean {	
	/*
	 * @author Ritesh
	 * Following method is used to get the shipped status from the Respective shipmode services
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	public boolean getTrackStatus(HashMap hmparams) throws AppError
	{
		boolean btrackFl = true;
		GmShipTrackingClient gmShipTrackingClient = new GmShipTrackingClient();
		String strShipMode = GmCommonClass.parseNull((String)hmparams.get("SHIPMODE"));
		String strTrackNum = GmCommonClass.parseNull((String)hmparams.get("TRACKNUMBER"));
		if(strShipMode.equals("5001")){
			 btrackFl = gmShipTrackingClient.trackFedExTrackingNumber(strTrackNum); //FedEx Track Status Client Call
		}else if(strShipMode.equals("5000")){
			 // UPS Track Status Client Call
		}
		return btrackFl;
	}	
}
