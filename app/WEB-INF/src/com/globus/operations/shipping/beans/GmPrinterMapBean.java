package com.globus.operations.shipping.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.operations.shipping.GmPrinterMapVO;

public class GmPrinterMapBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing logger class
	
	/**
	 * fetchPrinterMapping - This method is used to fetch the printer mapping to respective user
	 * @param gmPrinterMapVO
	 * @author arajan
	 */
	public void fetchPrinterMapping(GmPrinterMapVO gmPrinterMapVO) throws AppError{
		
		GmCommonClass gmCommonClass = new GmCommonClass();
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmCodeLookupVO gmZPLPrinterVO = new GmCodeLookupVO();
		GmCodeLookupVO gmRegPrinterVO = new GmCodeLookupVO();

		ArrayList alZplPrinter = new ArrayList();
		ArrayList alRegPrinter = new ArrayList();
		List<GmCodeLookupVO> alRegPrinterList = new ArrayList<GmCodeLookupVO>();
		List<GmCodeLookupVO> alZPLPrinterList = new ArrayList<GmCodeLookupVO>();
		String strRegPrinter = "";
		String strZplPrinter = "";
		String strUserId = gmPrinterMapVO.getUserid();
		
		strRegPrinter = getPrinterMappingForUser(strUserId, "PRINTER");
		strZplPrinter = getPrinterMappingForUser(strUserId, "ZEBRA_PRINTER");
		
		alZplPrinter = GmCommonClass.parseNullArrayList(gmCommonClass.getCodeList("ZEBPRN")); // to get drop down values of ZPL printers
		alRegPrinter = GmCommonClass.parseNullArrayList(gmCommonClass.getCodeList("REGPRN")); // to get drop down values of regular printers
		
		alRegPrinterList = gmWSUtil.getVOListFromHashMapList(alRegPrinter, gmRegPrinterVO, gmRegPrinterVO.getCodeListMappingProps());
		alZPLPrinterList = gmWSUtil.getVOListFromHashMapList(alZplPrinter, gmZPLPrinterVO, gmZPLPrinterVO.getCodeListMappingProps());

		// set all the values to the VO file
		gmPrinterMapVO.setAlRegPrinters(alRegPrinterList);
		gmPrinterMapVO.setAlZPLPrinters(alZPLPrinterList);
		gmPrinterMapVO.setRegprinternm(strRegPrinter);
		gmPrinterMapVO.setZplprinternm(strZplPrinter);
	}
	
	/**
	 * fetchPrinterMapping - This method is used to save the printer mapping to respective user
	 * @param gmPrinterMapVO
	 * @author arajan
	 */
	public void savePrinterMapping (GmPrinterMapVO gmPrinterMapVO) throws AppError{
		
		// getting all the values from VO file
		String strRegPrinterNm = gmPrinterMapVO.getRegprinternm();
		String strZplPrinterNm = gmPrinterMapVO.getZplprinternm();
		String strUserId = gmPrinterMapVO.getUserid();

		GmDBManager gmDBManager = new GmDBManager();                
		gmDBManager.setPrepareString("gm_pkg_ship_scan_trans.gm_sav_print_params",3);
		gmDBManager.setString(1,strRegPrinterNm);
		gmDBManager.setString(2,strZplPrinterNm);
		gmDBManager.setString(3,strUserId);

		gmDBManager.execute();
		gmDBManager.commit();

	}
	
	/**
	 * getPrinterMappingForUser - This method is used to get printer name
	 * @param strUserId
	 * @param strType
	 * @return
	 * @throws AppError
	 */
	public String getPrinterMappingForUser(String strUserId, String strType) throws AppError{
		
		GmDBManager gmDBManager = new GmDBManager();      
		String strPrintName = "";
		gmDBManager.setFunctionString("gm_pkg_ship_scan_trans.get_mapped_printer_name",2);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2,strUserId);
		gmDBManager.setString(3,strType);
	    gmDBManager.execute();
		strPrintName = GmCommonClass.parseNull(gmDBManager.getString(1));
	    log.debug("strPrintName to display is   " + strPrintName);
		gmDBManager.close();
		return strPrintName;
	
	}

}
