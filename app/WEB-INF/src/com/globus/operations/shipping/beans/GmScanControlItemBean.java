package com.globus.operations.shipping.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author rshah
 *
 */
public class GmScanControlItemBean {
	
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	/*
	 * @author Ritesh
	 * Following method is used to fetch the item consignment information 
	 * Also this method is only used for device change. Cannot be used anywhere else.
	 */
	public ArrayList fetchConsignedItems(String strTxnId) throws AppError
	{
		
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		//String strScanId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScanid());
		//String strScanType = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScantype());
		gmDBManager.setPrepareString("gm_pkg_scan_cntrl_item_rpt.gm_fch_consign_item", 2);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("alReturn======================"+alReturn);
		return alReturn;
	}
	
	/*
	 * @author Ritesh
	 * Following method is used to fetch the Order item information 
	 * Also this method is only used for device change. Cannot be used anywhere else.
	 */
	public ArrayList fetchOrderItems(String strTxnId) throws AppError
	{
		
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		//String strScanId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScanid());
		//String strScanType = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScantype());
		gmDBManager.setPrepareString("gm_pkg_scan_cntrl_item_rpt.gm_fch_order_items", 2);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("alReturn======================"+alReturn);
		return alReturn;
	}
	
	/*
	 * @author Ritesh
	 * Following method is used to fetch the Loaner item information 
	 * Also this method is only used for device change. Cannot be used anywhere else.
	 */
	public ArrayList fetchLoanedItems(String strTxnId) throws AppError
	{
		
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		//String strScanId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScanid());
		//String strScanType = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScantype());
		gmDBManager.setPrepareString("gm_pkg_scan_cntrl_item_rpt.gm_fch_loaned_item", 2);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("alReturn======================"+alReturn);
		return alReturn;
	}
}