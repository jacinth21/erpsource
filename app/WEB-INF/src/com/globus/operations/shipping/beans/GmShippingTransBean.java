package com.globus.operations.shipping.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisReportBean;
import com.globus.webservice.common.bean.GmMasterRedisTransBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingReportBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingWrapperBean;

public class GmShippingTransBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  GmCommonClass gmCommon = new GmCommonClass();

  public GmShippingTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmShippingTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // this method is written to replace
  // custservice.GmShippingTransBean.saveLoanerShipInfo method
  public void saveShipDetails(GmDBManager gmDBManager, HashMap hmParas) throws AppError {


    String strShipId = GmCommonClass.parseNull((String) hmParas.get("SHIPID"));
    String strRefId = GmCommonClass.parseNull((String) hmParas.get("REFID"));
    String strSource = GmCommonClass.parseZero((String) hmParas.get("SOURCE"));
    String strShipTo = GmCommonClass.parseZero((String) hmParas.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmParas.get("SHIPTOID"));
    String strShipCarr = GmCommonClass.parseZero((String) hmParas.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseZero((String) hmParas.get("SHIPMODE"));
    String strTrackNo = GmCommonClass.parseNull((String) hmParas.get("TRACKNO"));
    String strAddressId = GmCommonClass.parseZero((String) hmParas.get("ADDRESSID"));
    String strFreight = GmCommonClass.parseNull((String) hmParas.get("FREIGHT"));
    String strOverrideAttnTo = GmCommonClass.parseNull((String) hmParas.get("OVERRIDEATTNTO"));
    String strShipInstruction = GmCommonClass.parseNull((String) hmParas.get("SHIPINSTRUCTION"));

    String strUserId = GmCommonClass.parseNull((String) hmParas.get("USERID"));
    // String strShipId = GmCommonClass.parseNull((String) hmParas.get("SHIPID"));

    gmDBManager.setPrepareString("gm_pkg_cm_shipping_trans.gm_sav_shipping", 13);

    log.debug("strShipId " + strShipId);
 

    gmDBManager.registerOutParameter(11, java.sql.Types.CHAR);

    gmDBManager.setString(1, strRefId);
    gmDBManager.setInt(2, Integer.parseInt(strSource));
    gmDBManager.setInt(3, Integer.parseInt(strShipTo));
    gmDBManager.setString(4, strShipToId);
    gmDBManager.setInt(5, Integer.parseInt(strShipCarr));
    gmDBManager.setInt(6, Integer.parseInt(strShipMode));
    gmDBManager.setString(7, strTrackNo);
    gmDBManager.setInt(8, Integer.parseInt(strAddressId));
    gmDBManager.setString(9, strFreight);
    gmDBManager.setString(10, strUserId);
    gmDBManager.setString(11, strShipId);
    gmDBManager.setString(12, strOverrideAttnTo);
    gmDBManager.setString(13, strShipInstruction);

    log.debug("10");

    gmDBManager.execute();
    log.debug("11");

    strShipId = gmDBManager.getString(11);
    log.debug("Shipping id " + strShipId);
    gmDBManager.commit();
  }


  public void releaseShipping(GmDBManager gmDBManager, HashMap hmParas) throws AppError {
    String strRefId = GmCommonClass.parseNull((String) hmParas.get("TXNID"));
    String strSource = GmCommonClass.parseZero((String) hmParas.get("SOURCE"));
    String strUserId = GmCommonClass.parseNull((String) hmParas.get("USERID"));
    String strLogSrc = GmCommonClass.parseNull((String) hmParas.get("LOGSRC"));
    log.debug("release shipping: strRefId:" + strRefId + " strSource:" + strSource + " strUserId:"
        + strUserId);
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_trans.gm_sav_release_shipping", 4);
    gmDBManager.setString(1, strRefId);
    gmDBManager.setInt(2, Integer.parseInt(strSource));
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strLogSrc);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public HashMap saveShipOut(HashMap hmParas) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean();
    GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
    String strShipId = GmCommonClass.parseNull((String) hmParas.get("SHIPID"));
    String strShipFlag = "";
    String strRefId = GmCommonClass.parseNull((String) hmParas.get("REFID"));
    String strSource = GmCommonClass.parseZero((String) hmParas.get("SOURCE"));
    String strShipTo = GmCommonClass.parseZero((String) hmParas.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmParas.get("NAMES"));
    String strShipCarr = GmCommonClass.parseZero((String) hmParas.get("SHIPCARRIER"));
    String strShipMode = GmCommonClass.parseZero((String) hmParas.get("SHIPMODE"));
    String strTrackNo = GmCommonClass.parseNull((String) hmParas.get("TRACKINGNO"));
    String strAddressId = GmCommonClass.parseZero((String) hmParas.get("ADDRESSID"));
    String strFreight = GmCommonClass.parseNull((String) hmParas.get("FREIGHTAMT"));
    String strUserId = GmCommonClass.parseNull((String) hmParas.get("USERID"));
    String strLog = GmCommonClass.parseNull((String) hmParas.get("TXT_LOGREASON"));
    String strUpdAction = GmCommonClass.parseZero((String) hmParas.get("UPDACTION"));
    String strCustPo = GmCommonClass.parseNull((String) hmParas.get("CUSTPO"));
    String strloaneChkFl = GmCommonClass.parseNull((String) hmParas.get("CHK_LOANER"));
    String strOverrideAttnTo = GmCommonClass.parseNull((String) hmParas.get("OVERRIDEATTNTO"));
    String strShipInstruction = GmCommonClass.parseNull((String) hmParas.get("SHIPINSTRUCTION"));
    String strPickStationId = GmCommonClass.parseNull((String) hmParas.get("PICKSTATIONID"));
    String strToteId = GmCommonClass.parseNull((String) hmParas.get("TOTEID"));
    String strAcctId = GmCommonClass.parseNull((String) hmParas.get("strAcctID"));
    String strCompanyInfo= GmCommonClass.parseNull((String) hmParas.get("COMPANY_INFO"));
 
    String strRmaID = "";
    log.debug("strShipId " + strShipId);
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_trans.gm_sav_shipout", 18);

    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(14, java.sql.Types.CHAR);

    gmDBManager.setString(1, strRefId);
    gmDBManager.setInt(2, Integer.parseInt(strSource));
    gmDBManager.setInt(3, Integer.parseInt(strShipTo));
    gmDBManager.setString(4, strShipToId);
    gmDBManager.setInt(5, Integer.parseInt(strShipCarr));
    gmDBManager.setInt(6, Integer.parseInt(strShipMode));
    gmDBManager.setString(7, strTrackNo);
    gmDBManager.setInt(8, Integer.parseInt(strAddressId));
    gmDBManager.setString(9, strFreight);
    gmDBManager.setString(10, strUserId);
    gmDBManager.setInt(11, Integer.parseInt(strUpdAction));
    gmDBManager.setString(12, strShipId);
    gmDBManager.setString(13, strCustPo);
    gmDBManager.setString(15, strOverrideAttnTo);
    gmDBManager.setString(16, strShipInstruction);
    gmDBManager.setString(17, strPickStationId);
    gmDBManager.setString(18, strToteId);

    log.debug("10 " + strLog);
    log.debug("strShipToId"+strShipToId);

    log.debug("strShipTo"+strShipTo);

    gmDBManager.execute();

    strShipId = gmDBManager.getString(12);
    strShipFlag = gmDBManager.getString(14);

    hmReturn.put("SHIPID", strShipId);
    hmReturn.put("SHIPFLAG", strShipFlag);

    if (strloaneChkFl.equals("on") && strSource.equals("50182")) {
      copyLoanerReqShipData(strShipId, gmDBManager);
    }

    if (!strLog.equals("")) {
      if (strSource.equals("50183")) { // loaner extn
        log.debug("entering log details");
        gmCommonBean.saveLog(gmDBManager, strRefId, strLog, strUserId, "1241");
      } else if (strSource.equals("50182")) {// loaners
        gmCommonBean.saveLog(gmDBManager, strRefId, strLog, strUserId, "1222");
      } else if (strSource.equals("50180")) {// orders
        gmCommonBean.saveLog(gmDBManager, strRefId, strLog, strUserId, "1200");
      } else if (strSource.equals("50181")) {// consign
    	 
        gmCommonBean.saveLog(gmDBManager, strRefId, strLog, strUserId, "1221");
      } else if (strSource.equals("50186")) {// Ih-Loaner Items
        gmCommonBean.saveLog(gmDBManager, strRefId, strLog, strUserId, "1243"); /*
                                                                                 * 1243 = IH Loaner
                                                                                 * Item
                                                                                 */
      }
    }
    if (strSource.equals("50186") && !strTrackNo.equals("")) {/*
                                                               * 50186- Inhouse Loaner, create RA#
                                                               * for while shipout IHLN
                                                               */
      gmDBManager.setPrepareString("gm_pkg_op_ihloaner_item_txn.gm_sav_ihloaner_item_return", 4);
      gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
      gmDBManager.setString(1, strRefId);
      gmDBManager.setString(2, strSource);
      gmDBManager.setString(3, strUserId);
      gmDBManager.execute();
      strRmaID = gmDBManager.getString(4);
      log.debug("strRmaID   " + strRmaID);
      hmReturn.put("RETURNID", strRmaID);
    }


    String strReqId = GmCommonClass.parseNull((String) hmParas.get("HREQID"));
    String strInpStr = "";
    HashMap hmAttrib = new HashMap();
    if (!strCustPo.trim().equals("") && strSource.equals("50181")) {
      strInpStr += "1006420^" + strCustPo + "|";
      hmAttrib.put("REQID", strReqId);
      hmAttrib.put("HINPUTSTR", strInpStr);
      hmAttrib.put("USERID", strUserId);
      gmOperationsBean.saveRequestAttribute(gmDBManager, hmAttrib);
    }
    gmDBManager.commit();
    
    
    
    // JMS call for the Net Number change: ATEC 301
    if (strSource.equals("50183")) {

      HashMap hmLnParam = new HashMap();
      hmLnParam.put("TRANS_ID", strRefId);
      hmLnParam.put("USER_ID", strUserId);
      hmLnParam.put("WAREHOUSE_TYPE", "6");
      hmLnParam.put("TRANS_TYPE", strSource); // Pass the Source type
      hmLnParam.put("COMPANY_INFO",strCompanyInfo);
      gmInvLocationBean.updateLoanerStockSheetInfo(hmLnParam);
    }

    // JMS Call to update the Acct net by Lot PMT:14376
    // 50180 - Order
    // 50181 - Consignment
    
    if (strSource.equals("50181")) {

        HashMap hmAcctParam = new HashMap();

        //Ref ID -> Pass the Account ID
        //Trans ID -> Pass the Transaction ID
       // hmAcctParam.put("REFID", strShipToId);
        hmAcctParam.put("TRANS_ID", strRefId);
        hmAcctParam.put("TRANS_TYPE", "50181");
        hmAcctParam.put("USER_ID", strUserId);
        hmAcctParam.put("WAREHOUSE_TYPE", "5");
        hmAcctParam.put("COMPANY_INFO",strCompanyInfo);
        gmInvLocationBean.updateLoanerStockSheetInfo(hmAcctParam);
      }
    
    
    if (strSource.equals("50180")) {
        HashMap hmAcctParam = new HashMap();
    //    hmAcctParam.put("REFID", strShipToId);        
        hmAcctParam.put("TRANS_ID", strRefId);
        hmAcctParam.put("TRANS_TYPE", "50180");
        hmAcctParam.put("USER_ID", strUserId);
        hmAcctParam.put("WAREHOUSE_TYPE", "5");
        hmAcctParam.put("COMPANY_INFO",strCompanyInfo);
        gmInvLocationBean.updateLoanerStockSheetInfo(hmAcctParam);
      }
  
    //Update the set and tag details to the cache server when ship out the CN (PMT-24650)
    if((!strShipId.equals("")) && (strSource.equals("50181"))){    //Consignments (SHIPS)
    	HashMap hmParam = new HashMap();
    	hmParam.put("TRANS_ID", strRefId);
    	hmParam.put("USER_ID", strUserId);
    	hmParam.put("COMPANY_INFO",strCompanyInfo);
    	log.debug("JMS CALL-------------"); 
    	GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
    	gmKitMappingBean.UpdateDatatoRedisKeyJMS(hmParam);
    }
    
    return hmReturn;
  }

  /**
   * saveShippedData - This method will be used to save shipping data for respective rep
   * 
   * @param HashMap hmParam
   * @exception AppError
   */
  public void saveShippedData(HashMap hmParam) throws AppError {
    String strDbRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));

    log.debug("strDbRefID...." + strDbRefID);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_trans.gm_sav_shipped_today", 1);
    gmDBManager.setString(1, strDbRefID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * copyLoanerReqShipData - to copy the planned shipped date to all the requested sets.
   * 
   * @param String
   * @exception AppError
   */
  public void copyLoanerReqShipData(String strShipId, GmDBManager gmDBManager) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_trans.gm_copy_loanerreq_ship_info ", 1);
    gmDBManager.setInt(1, Integer.parseInt(strShipId));
    gmDBManager.execute();
    // gmDBManager.commit();
  }

  /**
   * getTxnCompanyDtls - to get the company id of the transaction.
   * 
   * @param String strTxnId, String strSource, String strShipId
   * @exception AppError
   */
  public String getTxnCompanyDtls(String strTxnId, String strSource, String strShipId)
      throws AppError {
    String strCompId = "";

    if (strTxnId.equals("") && strShipId.equals("")) {
      return getGmDataStoreVO().getCmpid();
    }

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_transaction_compId", 3);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strTxnId);
    gmDBManager.setString(3, strSource);
    gmDBManager.setInt(4, Integer.parseInt(GmCommonClass.parseZero(strShipId)));
    gmDBManager.execute();
    strCompId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strCompId;
  }

  /**
   * getTransactionCompanyId - To get the company id of the transaction.
   * 
   * @author ksomanathan
   * @param String strContextCompID, String strRefID, String strFilter ,String strTranstype
   * @exception AppError
   */
  public String getTransactionCompanyId(String strContextCompID, String strRefID, String strFilter,
      String strTranstype) throws AppError {
    String strTransCompId = "";

    if (strRefID.equals("")) {
      return getGmDataStoreVO().getCmpid();
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_transaction_company_id", 4);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strContextCompID);
    gmDBManager.setString(3, strRefID);
    gmDBManager.setString(4, strFilter);
    gmDBManager.setString(5, strTranstype);
    gmDBManager.execute();
    strTransCompId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strTransCompId;
  }
  
  /**
   * updateOrderShipcost - To update the ship cost for Orders.
   * 
   * @author mselvamani
   * @param HashMap hmParam
   * @exception AppError
   */
  public void updateOrderShipcost(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		String strOrdId = (String) hmParam.get("ORDERID");
		String strShipCost= (String) hmParam.get("SHIPCOST");
		String strUserId = (String) hmParam.get("USERID");
		gmDBManager.setPrepareString("gm_pkg_op_do_process_trans.gm_update_ship_cost",3);
		gmDBManager.setString(1, strOrdId);
		gmDBManager.setString(2, strShipCost);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();	
		gmDBManager.commit();
	}
}
