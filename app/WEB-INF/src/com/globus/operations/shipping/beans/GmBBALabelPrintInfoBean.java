package com.globus.operations.shipping.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;

import oracle.jdbc.driver.OracleTypes;

import java.util.Iterator;
import java.io.*;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.shipping.forms.GmShipDetailsForm;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.sales.beans.GmSalesMappingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import com.globus.webservice.operations.labelprint.resource.GmLabelPrintInterface;


public class GmBBALabelPrintInfoBean extends GmSalesFilterConditionBean implements GmLabelPrintInterface{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

	
	 public static Object getLabelPrintClass(String strCompanyId) throws AppError
        {  
		ApplicationContext context = new ClassPathXmlApplicationContext("xml/GmLabelPrintBean.xml");
		return context.getBean("COM" + strCompanyId);
		}
			
			
	/**
     * ShippedOrderData  - This method will be used to retrieve shipping data for respective rep 
     * @param 
     * @return HashMap 
     * @exception AppError
     */
    public ArrayList fetchLabelDataByTxn(HashMap hmParam) throws AppError{
    	HashMap hmReturn = new HashMap();  
        ArrayList alReturn = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager();
		String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
		log.debug("strTxnId is:"+strTxnId);
		gmDBManager.setPrepareString("gm_pkg_op_bba_label_print.gm_fetch_txn_details", 2);
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
		gmDBManager.setString(1, strTxnId);
		gmDBManager.execute();
		log.debug("After execute in bean is:");

		alReturn = GmCommonClass.parseNullArrayList((ArrayList)gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2)));
		gmDBManager.close();
		
        return alReturn;
    }
    public ArrayList fetchLabelDataByPart(HashMap hmParam) throws AppError{
    	HashMap hmReturn = new HashMap();  
        ArrayList alReturn = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager();
		String strPartId = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
		String strControlNum = GmCommonClass.parseNull((String) hmParam.get("CONTROLNUM"));
		
		gmDBManager.setPrepareString("gm_pkg_op_bba_label_print.gm_fetch_part_details", 3);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
		gmDBManager.setString(1, strPartId);
		gmDBManager.setString(2, strControlNum);
		gmDBManager.execute();

		alReturn = GmCommonClass.parseNullArrayList((ArrayList)gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3)));
		gmDBManager.close();
        return alReturn;
    }
    public ArrayList fetchPartLabelPath(HashMap hmParam) throws AppError{
    	HashMap hmReturn = new HashMap();  
        ArrayList alReturn = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager();
		String strPartId = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
		
		gmDBManager.setPrepareString("gm_pkg_op_bba_label_print.gm_fetch_part_label_path", 2);
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
		gmDBManager.setString(1, strPartId);
		gmDBManager.execute();

		alReturn = GmCommonClass.parseNullArrayList((ArrayList)gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2)));
		gmDBManager.close();
		
        return alReturn;
    }
    public ArrayList fetchPrinterStock() throws AppError{
    	HashMap hmReturn = new HashMap();  
        ArrayList alReturn = new ArrayList();

        GmDBManager gmDBManager = new GmDBManager();

		
		gmDBManager.setPrepareString("gm_pkg_op_bba_label_print.gm_fetch_printer_stock", 1);
        gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
		gmDBManager.execute();

		alReturn = GmCommonClass.parseNullArrayList((ArrayList)gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1)));
		gmDBManager.close();
		
        return alReturn;
    }
    public void saveLabelPath(String strInputString, String strUserId)throws AppError{
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_LabelPrint.gm_sav_part_label_path",2);
		gmDBManager.setString(1, strInputString);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
		
	}
    
    public void voidLabelPath(String strInputString, String strUserId)throws AppError{
		
  		GmDBManager gmDBManager = new GmDBManager();
  		gmDBManager.setPrepareString("gm_pkg_op_LabelPrint.gm_void_part_label_path",2);
  		gmDBManager.setString(1, strInputString);
  		gmDBManager.setString(2, strUserId);
  		gmDBManager.execute();
  		gmDBManager.commit();
  		
  	}
    
	
	

}
