/**
 * 
 */
package com.globus.operations.shipping.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.itemcontrol.beans.GmItemControlRptBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.quality.beans.GmPartAttributeBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.GmTxnHeaderVO;
import com.globus.valueobject.operations.GmTxnPartDtlVO;
import com.globus.valueobject.operations.GmTxnRulesVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqDtlVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

/**
 * @author rshah
 * 
 */
public class GmShipScanReqRptBean extends GmBean {

  /**
   * Constructor will populate default company info.
   * 
   * @throws AppError
   */
  public GmShipScanReqRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   * @throws AppError
   */
  public GmShipScanReqRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   *  Initializing logger
   */
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  /*
   * @author Ritesh Following method is used to fetch the transaction information based on scanid
   * and type.
   * This method/DB File used in shipping Device (erpshipping repository).
   * Please make sure the changes are done in that repository too if param added/removed for the DB call.	
   */
  public HashMap fetchShipScanTrans(String strScanId, String strScanType) throws AppError {

    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    log.debug("fetchShipScanTrans strScanId=="+strScanId);
    log.debug("fetchShipScanTrans strScanType=="+strScanType);
    // String strScanId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScanid());
    // String strScanType = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScantype());
    gmDBManager.setPrepareString("gm_pkg_ship_scan_rpt.gm_fch_ship_scan_trans", 3);
    gmDBManager.setString(1, strScanId);
    gmDBManager.setString(2, strScanType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug("gm_fch_ship_scan_trans hmReturn=="+hmReturn);
    return hmReturn;
  }

  /*
   * @author Ritesh Following method is used to fetch the transaction header and shipping details.
   */
  public HashMap fetchShipScanTransDetails(String strTxnId, String strSource, String strTxnType)
      throws AppError {
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();
    GmItemControlRptBean gmItemControlRptBean = new GmItemControlRptBean();
    GmInHouseSetsReportBean gmInHouseSetsReportBean = new GmInHouseSetsReportBean();

    HashMap hmTxnHeader = new HashMap();
    HashMap hmTxnShipInfo = new HashMap();
    HashMap hmTxnShipAddr = new HashMap();
    HashMap hmShipStation = new HashMap();
    HashMap hmReturn = new HashMap();

    String strAction = "";

    if (strTxnType.equals("")) {
      strTxnType = GmCommonClass.parseNull(getTxnTypeFromSource(strSource));
    }

    if (strSource.equals("50182")) {
      hmTxnHeader = gmInHouseSetsReportBean.getConsignDetails(strTxnId);
    } else {
      hmTxnHeader = gmItemControlRptBean.fetchTransHeaderInfo(strTxnId, strTxnType);
    }
    hmTxnShipInfo = gmShippingInfoBean.fetchShippingDetails(strTxnId, strSource, strAction);
    hmTxnShipAddr = gmShippingInfoBean.fetchShipAddress(strTxnId, strSource);
    hmShipStation = fetchShipStationDtl(strTxnId, strSource, strTxnType);
    gmShippingInfoBean.fetchTissueFlag(strTxnId, strSource); //method added for tissue part validation in shipping device-PMT-45274

    hmTxnShipInfo.putAll(hmTxnShipAddr);
    hmTxnShipInfo.putAll(hmShipStation);

    hmReturn.put("TXNHEADER", hmTxnHeader);
    hmReturn.put("SHIPINFO", hmTxnShipInfo);

    log.debug("hmReturn" + hmReturn);

    return hmReturn;
  }

  /*
   * @author Ritesh Following method is used to fetch current and total txn count.
   */
  public HashMap fetchShipStationDtl(String strTxnId, String strSource, String strTxnType)
      throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ship_scan_rpt.gm_fch_ship_station_dtl", 6);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strSource);
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);

    gmDBManager.execute();
    hmReturn.put("CURRTXNCNT", gmDBManager.getInt(3));
    hmReturn.put("TOTTXNCNT", gmDBManager.getInt(4));
    hmReturn.put("SUGGTOTEID", GmCommonClass.parseNull(gmDBManager.getString(5)));
    hmReturn.put("SUGSTATIONID", GmCommonClass.parseNull(gmDBManager.getString(6)));
    gmDBManager.close();
    return hmReturn;
  }

  /*
   * @author VPrasath Following method is used to fetch Associated Transaction count
   */
  public ArrayList fetchAssociatedToteStation(String strTxnId, String strSource) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ship_scan_rpt.gm_fch_assoc_txn_dtl", 3);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strSource);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alReturn;
  }

  /*
   * @author VPrasath Following method is used to fetch station details
   */
  public ArrayList fetchStationDetails(String strStationId) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ship_scan_rpt.gm_fch_station_dtl", 2);
    gmDBManager.setString(1, strStationId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alReturn;
  }

  /*
   * @author VPrasath Following method is used to fetch station details
   */
  public ArrayList fetchSearchTxnList(String strScanId, String strScanType) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ship_scan_rpt.gm_fch_search_txn_list", 3);
    gmDBManager.setString(1, strScanId);
    gmDBManager.setString(2, strScanType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alReturn;
  }

  /*
   * @author rshah Following method is used to fetch station details
   */
  public ArrayList fetchToteTxnList(String strTxnId, String strSource) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ship_scan_rpt.gm_fch_tote_txn_list", 3);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strSource);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug("alReturn===" + alReturn);
    return alReturn;
  }

  /*
   * @author Ritesh Following method is used to fetch the transaction header and shipping details.
   */
  public String getTxnTypeFromSource(String strSource) {
    String strTxnType = "";
    int iSource = Integer.parseInt(GmCommonClass.parseZero(strSource));
    switch (iSource) {
      case 50183: {
        // FGLE
        strTxnType = "50154";
        break;
      }
      case 50181: {
        // GM-CN
        strTxnType = "4110";
        break;
      }
      case 50180: {
        // GM-CN
        strTxnType = "50261";
        break;
      }
      case 50186: {
        // GM-CN
        strTxnType = "9110";
        break;
      }

    }
    return strTxnType;
  }

  public void populateInputVO(GmShipScanReqDtlVO gmShipScanReqDtlVO) {
    GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();

    String strScanId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScanid());
    String strScanType = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScantype());
    String strTxnId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getTxnid());
    String strSource = GmCommonClass.parseNull(gmShipScanReqDtlVO.getSource());
    String strTxnType = "";

    HashMap hmTxnInfo = new HashMap();

    if (strTxnId.equals("")) {

      hmTxnInfo = gmShipScanReqRptBean.fetchShipScanTrans(strScanId, strScanType);
      if (hmTxnInfo.size() > 0) {
        strTxnId = GmCommonClass.parseNull((String) hmTxnInfo.get("TXNID"));
        strSource = GmCommonClass.parseNull((String) hmTxnInfo.get("SOURCE"));
        strTxnType = GmCommonClass.parseNull((String) hmTxnInfo.get("TXNTYPE"));
        log.debug("hmTxnInfo" + hmTxnInfo);
      } else {
        throw new AppError("", "20708");
      }
    }
    gmShipScanReqDtlVO.setTxnid(strTxnId);
    gmShipScanReqDtlVO.setSource(strSource);
    gmShipScanReqDtlVO.setScantype(strTxnType);
  }

  public void populateShippingDetails(GmShipScanReqDtlVO gmShipScanReqDtlVO) {
    GmTxnHeaderVO gmTxnHeaderVO = new GmTxnHeaderVO();
    GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();
    GmPrintPaperworkBean gmPrintPaperworkBean = new GmPrintPaperworkBean();

    HashMap hmTxnHeader = new HashMap();
    HashMap hmTxnShipInfo = new HashMap();
    HashMap hmTxnDtl = new HashMap();

    String strScanType = GmCommonClass.parseNull(gmShipScanReqDtlVO.getScantype());
    String strTxnId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getTxnid());
    String strSource = GmCommonClass.parseNull(gmShipScanReqDtlVO.getSource());

    hmTxnDtl = gmShipScanReqRptBean.fetchShipScanTransDetails(strTxnId, strSource, strScanType);
    hmTxnHeader = GmCommonClass.parseNullHashMap((HashMap) hmTxnDtl.get("TXNHEADER"));
    hmTxnShipInfo = GmCommonClass.parseNullHashMap((HashMap) hmTxnDtl.get("SHIPINFO"));

    log.debug("hmTxnHeader ==" + hmTxnHeader);

    String strHoldFl = GmCommonClass.parseNull((String) hmTxnShipInfo.get("HOLDFLAG"));
    String strCountryCode = GmCommonClass.parseNull((String) hmTxnShipInfo.get("COUNTRYCODE"));
    String strShipCountry = GmCommonClass.parseNull((String) hmTxnShipInfo.get("SHIPCOUNTRY"));
    String strType = GmCommonClass.parseNull((String) hmTxnHeader.get("TXNTYPE"));
    // When transaction is HOLD, it should not have process. - BUG-3110.
    if (strHoldFl.equals("Y")) {
      throw new AppError("", "20709");
    }
    // OUS shipment validation on complete for NSP device only. 4110 stands for Consignment.
    // Same code is also added while doing complete tote.
    if ((!strCountryCode.equals("US") || !strShipCountry.equals("US")) && strType.equals("4110")) {
      throw new AppError("", "20710");
    }
    if (strSource.equals("50182")) {
      gmTxnHeaderVO =
          (GmTxnHeaderVO) gmWSUtil.getVOFromHashMap(hmTxnHeader, gmTxnHeaderVO,
              gmTxnHeaderVO.getTxnHeaderLoanerMappProps());
    } else {
      gmTxnHeaderVO =
          (GmTxnHeaderVO) gmWSUtil.getVOFromHashMap(hmTxnHeader, gmTxnHeaderVO,
              gmTxnHeaderVO.getTxnHeaderMappProps());
    }
    gmTxnShipInfoVO =
        (GmTxnShipInfoVO) gmWSUtil.getVOFromHashMap(hmTxnShipInfo, gmTxnShipInfoVO,
            gmTxnShipInfoVO.getPendingShippingProperties());

    String strToteWeight = gmPrintPaperworkBean.getTxnWeight(strTxnId, strSource);
    gmTxnHeaderVO.setTxnwt(strToteWeight);

    gmShipScanReqDtlVO.setGmTxnHeaderVO(gmTxnHeaderVO);
    gmShipScanReqDtlVO.setGmTxnShipInfoVO(gmTxnShipInfoVO);
  }

  public void populatePartDetails(GmShipScanReqDtlVO gmShipScanReqDtlVO) {
    List<GmTxnPartDtlVO> gmPartDetailsVOList = new ArrayList<GmTxnPartDtlVO>();
    GmPartAttributeBean gmPartAttributeBean = new GmPartAttributeBean();

    String strTxnId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getTxnid());
    String strSource = GmCommonClass.parseNull(gmShipScanReqDtlVO.getSource());

    gmPartDetailsVOList = gmPartAttributeBean.fetchPartDetails(strTxnId, strSource);
    gmShipScanReqDtlVO.setGmTxnPartListVO(gmPartDetailsVOList);
  }

  public void populateRuleDetails(GmShipScanReqDtlVO gmShipScanReqDtlVO) {
    List<GmTxnRulesVO> gmTxnRulesVOList = new ArrayList<GmTxnRulesVO>();
    GmRuleBean gmRuleBean = new GmRuleBean();

    GmWSUtil gmWSUtil = new GmWSUtil();
    GmTxnRulesVO gmTxnRulesVO = new GmTxnRulesVO();

    HashMap hmRuleParam = new HashMap();

    String strTxnId = GmCommonClass.parseNull(gmShipScanReqDtlVO.getTxnid());
    String strSource = GmCommonClass.parseNull(gmShipScanReqDtlVO.getSource());

    hmRuleParam = fetchShippingRuleParams(strTxnId, strSource, "VERIFY");
    ArrayList alList =
        GmCommonClass.parseNullArrayList(gmRuleBean.fetchRulesForTransaction(hmRuleParam));

    gmTxnRulesVOList =
        gmWSUtil
            .getVOListFromHashMapList(alList, gmTxnRulesVO, gmTxnRulesVO.getMappingProperties());
    if(gmTxnRulesVOList.size()>0){
    	gmShipScanReqDtlVO.setGmTxnRulesListVO(gmTxnRulesVOList);
    }
  }

  public HashMap fetchShippingRuleParams(String strTxnId, String strSource, String strTxnStatus)
      throws AppError {
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();

    HashMap hmRuleParam = new HashMap();

    HashMap hmMap = gmShippingInfoBean.fetchShippingDetails(strTxnId, strSource, "fetch");

    String strAddressId = GmCommonClass.parseNull((String) hmMap.get("ADDRESSID"));
    String strShipTo = GmCommonClass.parseNull((String) hmMap.get("SHIPTO"));
    String strShipToId = GmCommonClass.parseNull((String) hmMap.get("NAMES"));

    log.debug("strAddressId == :: " + strAddressId);

    hmRuleParam.put("RE_TXN", "SHIPOUTWRAPPER");
    hmRuleParam.put("txnStatus", strTxnStatus);
    hmRuleParam.put("refId", strTxnId);
    hmRuleParam.put("source", strSource);
    hmRuleParam.put("addressid", strAddressId);
    hmRuleParam.put("shipTo", strShipTo);
    hmRuleParam.put("names", strShipToId);

    return hmRuleParam;
  }

  /*
   * @author hreddi Following method is used to fetch Ready to pick transaction details
   */
  public ArrayList fetchReadyToPickTrans(String strShipCarrier) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ship_scan_rpt.gm_fch_readytopick_trans", 2);
    gmDBManager.setString(1, strShipCarrier);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchAssociateRA - This method will return associated Pending Return Summary Transaction id for
   * IHLN-XXXX
   * 
   * @param String strTransId
   * @return String
   * @exception AppError
   */
  public String fetchAssociateRA(String strTransId) throws AppError {
    String strRAId = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("gm_pkg_op_ihloaner_item_rpt.get_associated_ra", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strTransId);
    gmDBManager.execute();
    strRAId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strRAId;
  }
  
  /**
   * @Description : getInsertException() - get the INSERT Exception list
   * @author   N Raja  
   * @return   alReturn
   * @throws   AppError
   */
  public ArrayList getInsertException() throws AppError {
	    GmDBManager gmDBManager = new GmDBManager();
	    ArrayList alReturn = new ArrayList();
	    gmDBManager.setPrepareString("gm_pkg_get_inserts_exception.gm_get_insert_exception",1);
	    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
	    gmDBManager.close();
	    return alReturn;
  }
}
