package com.globus.operations.shipping.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.sales.beans.GmSalesMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author bgupta
 * 
 */
public class GmShippingInfoBean extends GmSalesFilterConditionBean {

  GmCommonClass gmCommon = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  public GmShippingInfoBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmShippingInfoBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public HashMap loadShipping() throws Exception {
    HashMap hmReturn = new HashMap();
    HashMap hmNames = new HashMap();
    String strAccountID = "";
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmLogonBean gmlogon = new GmLogonBean(getGmDataStoreVO());
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    ArrayList alShipTo = new ArrayList();
    ArrayList alCarrier = new ArrayList();
    ArrayList alMode = new ArrayList();
    ArrayList alAccList = new ArrayList();
    ArrayList alDistList = new ArrayList();
    ArrayList alEmployeeList = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alAssociateSalesRepList = new ArrayList();
    ArrayList alShipacctList = new ArrayList();
    ArrayList alTemplist = new ArrayList();
    ArrayList alPlantList = new ArrayList();
    ArrayList alDealerList = new ArrayList();
    alShipTo = GmCommonClass.getCodeList("CONSP", getGmDataStoreVO());
    alCarrier = GmCommonClass.getCodeList("DCAR", getGmDataStoreVO());
    alMode = GmCommonClass.getCodeList("DMODE", getGmDataStoreVO());

   //fetching rule group value to fetch dealer list
    String strDealerFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getGmDataStoreVO().getCmpid(),
            "SHOW_DEALER_LIST"));

    /*
     * Following code is added for loading the available Ship Mode values and Default Ship Mode
     * value for the Corrsponding Ship Carriers
     */
    HashMap hmValues = new HashMap();
    HashMap hmShipModes = new HashMap();
    ArrayList alModeValues = new ArrayList();
    String strCodeID = "";
    String strCodeNm = "";
    String strKey = "";
    String strDefaultMode = "";
    String strDefualtKey = "";
    for (Iterator iter = alCarrier.iterator(); iter.hasNext();) {
      hmValues = (HashMap) iter.next();
      strCodeID = GmCommonClass.parseNull((String) hmValues.get("CODEID"));
      strCodeNm = GmCommonClass.parseNull((String) hmValues.get("CODENM"));
      hmShipModes = GmCommonClass.parseNullHashMap(fetchCarrierBasedModes(strCodeID, ""));
      alModeValues = (ArrayList) hmShipModes.get("SHIMODELIST");
      strDefaultMode = GmCommonClass.parseNull((String) hmShipModes.get("DEFAULTMODE"));
      strKey = strCodeNm.toUpperCase() + "MODELIST";
      strDefualtKey = strCodeNm.toUpperCase() + "DEFMODE";
      hmNames.put(strKey, alModeValues);
      hmNames.put(strDefualtKey, strDefaultMode);
    }
    
    String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
    String strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "LOANERINITFILTER")) ;
    HashMap hmFilters = new HashMap();
    hmFilters.put("FILTER", "Active");
    hmFilters.put("USERCODE", "300");
    hmFilters.put("DEPT", "");
    hmFilters.put("STATUS", "311");    
    if(strCompanyPlant.equals("Plant")){
	    hmFilters.put("DISTFILTER", "COMPANY-PLANT");
	    hmFilters.put("REPFILTER", "COMPANY-PLANT");
	    hmFilters.put("EMPFILTER", "COMPANY-PLANT");
    }		

    HashMap hmParam = new HashMap();
    alRepList = gmCustomerBean.getRepList(hmFilters);
    alAccList = gmSales.reportAccount();
    alDistList = gmCustomerBean.getDistributorList(hmFilters);
    alEmployeeList = gmlogon.getEmployeeList(hmFilters);
    alAssociateSalesRepList = gmSalesMappingBean.getAssocRepList(strAccountID);
    alPlantList = gmCommonBean.getPlantList();
    // 26230725, party type- dealer
    if (strDealerFl.equals("Y")) {
      alDealerList = gmPartyInfoBean.fetchPartyNameList("26230725", "Y");
    }
    // 4000641, party type - shipping account
    alShipacctList = gmPartyBean.reportPartyNameList("4000641");
    hmNames.put("REPLIST", alRepList);
    hmNames.put("ACCLIST", alAccList);
    hmNames.put("DISTLIST", alDistList);
    hmNames.put("EMPLIST", alEmployeeList);
    hmNames.put("DEALERLIST", alDealerList);
    hmNames.put("PLANTLIST", alPlantList);
    hmNames.put("ASSOCIATEREPLIST", alAssociateSalesRepList);
    hmNames.put("SHIPACCTLIST", alShipacctList);
    hmReturn.put("SHIPTO", alShipTo);
    hmReturn.put("CARRIER", alCarrier);
    hmReturn.put("MODE", alMode);
    hmReturn.put("NAMES", hmNames);
    return hmReturn;
  }

  public HashMap loadAreaSetShipping(HashMap hmParam) throws Exception {
    HashMap hmReturn = new HashMap();
    HashMap hmNames = new HashMap();
    HashMap hmDist = new HashMap();
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmLogonBean gmlogon = new GmLogonBean(getGmDataStoreVO());

    ArrayList alShipTo = new ArrayList();
    ArrayList alCarrier = new ArrayList();
    ArrayList alMode = new ArrayList();
    // ArrayList alAccList = new ArrayList();
    ArrayList alDistList = new ArrayList();
    // ArrayList alEmployeeList = new ArrayList();
    ArrayList alRepList = new ArrayList();

    alShipTo = GmCommonClass.getCodeList("CONSP");
    alCarrier = GmCommonClass.getCodeList("DCAR", getGmDataStoreVO());
    alMode = GmCommonClass.getCodeList("DMODE", getGmDataStoreVO());


    // alAccList = gmSales.reportAccount();

    hmParam.put("FILTER", "Active");
    alRepList = gmCustomerBean.getRepList(hmParam);
    alDistList = gmCustomerBean.getDistributorList(hmParam);
    // alEmployeeList = gmlogon.getEmployeeList("300","");

    hmNames.put("REPLIST", alRepList);
    hmNames.put("ACCLIST", new ArrayList());
    hmNames.put("DISTLIST", alDistList);
    hmNames.put("EMPLIST", new ArrayList());

    hmReturn.put("SHIPTO", alShipTo);
    hmReturn.put("CARRIER", alCarrier);
    hmReturn.put("MODE", alMode);
    hmReturn.put("NAMES", hmNames);
    return hmReturn;
  }
  
	/**
	 * fetchShippingDetails - This method used do fetch shipping details based on transid and source.
     * This method/DB File used in spineIT ERP (erpsource repository).
     * Please make sure the changes are done in that repository too if param added/removed for the DB call.	 
	 * @param strRefId,strSource,strAction
	 * @return
	 * @throws AppError
	 */  
  public HashMap fetchShippingDetails(String strRefId, String strSource, String strAction)
      throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_ship_details", 4);

    // log.debug("strSource " + Integer.parseInt(GmCommonClass.parseZero(strSource)));
    // log.debug("strAction " + strAction);
    gmDBManager.setString(1, strRefId);
    gmDBManager.setInt(2, Integer.parseInt(GmCommonClass.parseZero(strSource)));
    gmDBManager.setString(3, GmCommonClass.parseNull(strAction));
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();

    return hmReturn;
  }

  public HashMap fetchShippingDetailsByShipId(String strShipId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_ship_details_byid", 2);

    gmDBManager.setString(1, strShipId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }
  
  // PMT#40555-tissue part shipping validation
  public String fetchTissueFlag(String strShipId) throws AppError {
	  	String strTissueFlag = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_shipping_validate.gm_fch_tissue_flag_by_id", 2);
		gmDBManager.setString(1, strShipId);
		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.execute();
		strTissueFlag = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strTissueFlag;
  }
  
	/**
	 * fetchShippingReport - This method used to fetch the shipping details for the ship report.
     * This method/DB File used in shipping Device (erpshipping repository).
     * Please make sure the changes are done in that repository too if param added/removed for the DB call.	 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
  public ArrayList fetchShippingReport(HashMap hmParam) throws AppError {
    ArrayList alReturn = null;
    initializeParameters(hmParam);
    String strSource = GmCommonClass.parseZero((String) hmParam.get("SOURCE"));
    String strScanType = GmCommonClass.parseZero((String) hmParam.get("SCANTYPE"));
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strPendingFor = GmCommonClass.parseNull((String) hmParam.get("PENDINGDAYS"));
    String strCarrier = GmCommonClass.parseZero((String) hmParam.get("CARRIER"));
    String strShipTo = GmCommonClass.parseZero((String) hmParam.get("SHIPTO"));
    String strNames = GmCommonClass.parseNull((String) hmParam.get("NAMES"));

    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
    strDateFmt = strDateFmt.equals("") ? getCompDateFmt() : strDateFmt;

    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));

    String strDtTyp = GmCommonClass.parseNull((String) hmParam.get("DTTYP"));
    String strReqID = GmCommonClass.parseNull((String) hmParam.get("REQID"));
    String strHoldSts = GmCommonClass.parseNull((String) hmParam.get("HOLDSTS"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACESSLID"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("STRCONDITION"));
    String strShipToteId = GmCommonClass.parseNull((String) hmParam.get("SCANID"));
    String strPlantId = getCompPlantId();
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));  // added for PMT-34028 - Shipping Device - Dashboard Filter
    String strGroupId = "100025"; // added for PMT-34028 - Shipping Device - Dashboard Filter
    String strCompPlantFilter = "";
    strCompPlantFilter =
        strPlantId.equals("") ? GmCommonClass.getRuleValue("DEFAULT_PLANT", "ONEPORTAL")
            : strPlantId; // The record will fetch based on the plant,if the Source Dropdown is [Choose One]

    if(strSource.equals("50182")){  //50182 := Loaners
    strCompPlantFilter = GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANERSHIPDASHFILTER"));/*LOANERSHIPDASHFILTER- To fetch the records for EDC entity countries based on plant*/
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : strPlantId;
    } 
    if(strSource.equals("50181")){  //50181 := Consignments
    strCompPlantFilter = GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "CONSIGN_SHIP_DASH")); //CONSIGN_SHIP_DASH-Rule Value:= plant
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : strPlantId;
    }
    log.debug("strAccessLevel" + strAccessLevel);
    log.debug("1@...GmShippingInfoBean...strStatus..." + strStatus);
    log.debug("fetchShippingReport bean strUserId"+strUserId);

    String strFilterString = " WHERE ";
    
    String strPartyId = "";
    
    if(!strUserId.equals("")){ 
    strPartyId = fetchPartyId(strUserId);    // added for PMT-34028 - Shipping Device - Dashboard Filter
    log.debug("fetchShippingReport if strPartyId"+strPartyId);
    }
    
    String strBuildingId = fetchBuildingId(strPartyId, strGroupId);   // added for PMT-34028 - Shipping Device - Dashboard Filter
    log.debug("fetchShippingReport strBuildingId"+strBuildingId);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT   t907.holdfl,t907.asscid, t907.SID, t907.refid,  t907.orderholdflag, t907.reqid, ");
    sbQuery.append(" t907.SOURCE, t907.sourceid, t907.shiptonm,");
    sbQuery.append(" t907.sdate, t907.smode, t907.scarr, t907.track, t907.returntrack, t907.cdate,");
    sbQuery.append(" t907.cuser, t907.lname, t907.luser, t907.smodeid, t907.status,");
    sbQuery.append(" t907.carrierid");
    sbQuery.append(" ,t907.compby");
    sbQuery.append(" ,t907.etchid,t907.isMaster ");
    sbQuery.append(" ,t907.CURRENCY ,t907.Pl_ship_dt,t907.COUNTRYID ");
    sbQuery.append(" ,t907.status_fl, t907.shiptoteid, t907.pickstationid ");
    sbQuery.append(" ,t907.setid, t907.setname ");
    sbQuery.append(" ,DECODE (req_id,");      // get LOANERREQID for PMT-42950 
    sbQuery.append(" NULL, '',");
    sbQuery.append(" req_id");
    sbQuery.append(") LOANERREQID");
    sbQuery
        .append(" FROM (SELECT gm_pkg_cm_shipping_info.get_hold_flg(c907_ref_id,c901_source) holdfl, get_associate_txn_id(c907_ref_id) asscid, c907_shipping_id SID, c907_ref_id refid,");
    if (!strReqID.equals("")) {
      sbQuery.append(" C525_product_request_id reqid,");
    } else {

      sbQuery.append(" null reqid,");
    }
    sbQuery.append("get_order_hold_fl(c907_ref_id,c901_source) orderholdflag, ");
    sbQuery.append(" gm_pkg_op_loaner.get_loaner_etch_id(c907_ref_id) etchid, ");
    sbQuery.append(" get_code_name (c901_source) SOURCE, c901_source sourceid,");
    sbQuery.append(" get_cs_ship_name (c901_ship_to, c907_ship_to_id) shiptonm,");
    sbQuery.append(" c907_ship_to_dt  cdate,");
    sbQuery.append(" get_code_name (c901_delivery_mode) smode,");
    sbQuery.append(" get_code_name (c901_delivery_carrier) scarr,");
    sbQuery.append(" DECODE (c907_tracking_number,");
    sbQuery.append("         NULL, 'Y',");
    sbQuery.append("         c907_tracking_number");
    sbQuery.append("        ) track,");
    sbQuery.append(" c907_return_tracking_no returntrack,");
    sbQuery.append(" c907_shipped_dt  sdate,");
    sbQuery.append(" GET_USER_SH_NAME (c907_created_by) cuser,");
    sbQuery.append(" get_user_name (c907_last_updated_by) lname,");
    sbQuery.append(" TO_CHAR (c907_last_updated_date, '" + strDateFmt + "') luser,");
    sbQuery.append(" c901_delivery_mode smodeid, ");
    sbQuery
        .append(" DECODE (c907_status_fl, 15,gm_pkg_cm_shipping_info.get_pendcontrol_status(c907_ref_id,c901_source), c907_status_fl) status,");
    sbQuery.append(" c901_delivery_carrier carrierid");
    sbQuery
        .append(" ,decode(c907_status_fl, 40, gm_pkg_cm_shipping_info.get_completed_by(c907_ref_id,c901_source,c907_status_fl),'') compby");
    sbQuery.append(" ,GET_CODE_NAME_ALT(C901_RECORD_TYPE)	isMaster ");
    sbQuery
        .append(" ,DECODE(GET_DISTRIBUTOR_INTER_PARTY_ID(t907.C701_DISTRIBUTOR_ID),NULL, get_comp_curr_symbol(t907.c1900_company_id), GET_RULE_VALUE (50218, GET_DISTRIBUTOR_INTER_PARTY_ID (t907.C701_DISTRIBUTOR_ID))) CURRENCY ");
    sbQuery
        .append(" ,C907_PLANNED_SHIP_DATE Pl_ship_dt ,GET_COUNTRY_ID_FROM_DISTID(t907.C701_DISTRIBUTOR_ID) COUNTRYID ");
    sbQuery
        .append(" ,decode(c907_status_fl ,15,'PCN',30,'PS',33,'PIP',36,'RFP',40,'CMP','All') status_fl ");
    sbQuery.append(" ,c907_ship_tote_id shiptoteid, c907_pack_station_id pickstationid");
    sbQuery
        .append(" ,get_set_id_from_cn(c907_ref_id) setid, get_set_name_from_cn(c907_ref_id) setname,c525_product_request_id req_id");  //added req_id for PMT-42590 
    sbQuery.append(" FROM t907_shipping_info t907");

    if (strSource.equals("50181") && strScanType.equalsIgnoreCase("item")) {
      sbQuery.append(" , t504_consignment t504 ");
      sbQuery.append(strFilterString + "  c907_ref_id = t504.c504_consignment_id ");
      sbQuery.append(" AND t504.c207_set_id IS NULL ");
      strFilterString = " AND ";
    } else if (strSource.equals("50181") && strScanType.equalsIgnoreCase("set")) {
      sbQuery.append(" , t504_consignment t504 ");
      sbQuery.append(strFilterString + "  c907_ref_id = t504.c504_consignment_id ");
      sbQuery.append(" AND t504.c207_set_id IS NOT NULL ");
      strFilterString = " AND ";
    }

    if (strDeptId.equals("S")) {
      sbQuery.append(getAccessFilterClauseWithRepID());
    }
    sbQuery.append(strFilterString + " c907_void_fl IS NULL");
    sbQuery.append(" and C907_ACTIVE_FL IS NULL");
    sbQuery.append(" AND (t907.C5040_PLANT_ID =" + strCompPlantFilter );
    sbQuery.append(" OR t907.c1900_company_id =" + strCompPlantFilter );
    sbQuery.append(" )" );
    if (strDeptId.equals("S") && strAccessLevel.equals("1")) {
      sbQuery.append(" AND c907_ship_to_id = V700.REP_ID ");
    } else if (strDeptId.equals("S")
        && (strAccessLevel.equals("2") || strAccessLevel.equals("3") || strAccessLevel.equals("4"))) {
      sbQuery.append(" AND (c907_ship_to_id = V700.REP_ID OR c907_ship_to_id = V700.D_ID)");
    }
    // if scan ID is not empty , no need to check source
    if (!strSource.equals("") && !strSource.equals("0") && strShipToteId.equals("")) {
      sbQuery.append(" AND c901_source = ");
      sbQuery.append(strSource);
    }
    
    if(!strBuildingId.equals("")) {
        sbQuery.append(" AND (t907.c5057_building_id =" + strBuildingId );  //added for PMT-34028 - Shipping Device - Dashboard Filter
		sbQuery.append(" OR t907.c5057_building_id IS NULL) ");
      }
    
    if (!strRefID.equals("")) {
      sbQuery.append(" AND c907_ref_id = '");
      sbQuery.append(strRefID);
      sbQuery.append("'");
    }
    if (!strStatus.equals("")) {
      sbQuery.append(" AND c907_status_fl IN (");
      sbQuery.append(strStatus);
      sbQuery.append(") ");
    }
    sbQuery
        .append(" AND c901_source not in (select  c906_rule_value from t906_rules where c906_rule_grp_id='EXC_SOURCE')");
    // comment this for GOP-379
    /*
     * if (!strCondition.equals("")){ //sbQuery.append(" AND "); sbQuery.append(strCondition); }
     */
    if (!strPendingFor.equals("")) {
      sbQuery.append(" AND TO_CHAR(c907_ship_to_dt,'" + strDateFmt + "') = TO_CHAR((SYSDATE - ");
      sbQuery.append(strPendingFor);
      sbQuery.append("),'" + strDateFmt + "')");
    }

    if (!strCarrier.equals("") && !strCarrier.equals("0")) {
      sbQuery.append(" AND c901_delivery_carrier =  ");
      sbQuery.append(strCarrier);
    }

    if (!strShipTo.equals("") && !strShipTo.equals("0")) {
      sbQuery.append(" AND c901_ship_to = ");
      sbQuery.append(strShipTo);
    }

    if (!strDtTyp.equals("0") && !strFromDate.equals("") && !strToDate.equals("")) {
      String strDt = "";
      if (strDtTyp.equals("51056")) { // Shipped Date
        strDt = " TRUNC(C907_SHIPPED_DT) ";
      } else if (strDtTyp.equals("51057")) {// Released Date
        strDt = " TRUNC(C907_RELEASE_DT) ";
      } else if (strDtTyp.equals("4000096")) {// Planned Ship Date
        strDt = " TRUNC(C907_PLANNED_SHIP_DATE) ";
      }
      sbQuery.append(" and ");
      sbQuery.append(strDt);
      sbQuery.append(" >= TO_DATE('");
      sbQuery.append(strFromDate);
      sbQuery.append("' , '");
      sbQuery.append(strDateFmt);
      sbQuery.append("') ");
      sbQuery.append(" and ");
      sbQuery.append(strDt);
      sbQuery.append("  <= TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("' , '");
      sbQuery.append(strDateFmt);
      sbQuery.append("') ");
    }

    sbQuery.append(" )t907 where 1=1 ");

    if (strHoldSts.equals("on")) {
      sbQuery.append(" and t907.holdfl='Y' ");
    }

    if (!strNames.equals("")) {
      sbQuery.append(" and UPPER (t907.shiptonm) LIKE UPPER ('%");
      sbQuery.append(strNames);
      sbQuery.append("%')");
    }

    if (strStatus.equals("15")) {
      sbQuery.append(" AND status = ");
      sbQuery.append(strStatus);
    }

    if (!strReqID.equals("")) {
      sbQuery.append(" AND t907.reqid = '");
      sbQuery.append(strReqID);
      sbQuery.append("' ");
    }

    if (!strShipToteId.equals("")) { // The scan id may be either station id or tote id. The status
                                     // condition is added to avoid to pull old records and labels
      sbQuery.append(" AND (t907.shiptoteid = '");
      sbQuery.append(strShipToteId);
      sbQuery.append("' ");
      sbQuery.append(" OR t907.pickstationid = '");
      sbQuery.append(strShipToteId);
      sbQuery.append("') AND t907.status < 36 ");
    }

    sbQuery.append("  ORDER BY t907.SID desc");
    log.debug("GmShippingReport...Query..." + sbQuery);
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of fetchShippingReport

  /**
   * fetchShippedData - This method will be used to retrieve shipping data for respective rep
   * 
   * @param
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchShippedData() throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alShippingList = new ArrayList();
    ArrayList alRuleList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_shipped_today", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alShippingList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    alRuleList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    hmReturn.put("alShippingList", alShippingList);
    hmReturn.put("alRuleList", alRuleList);
    gmDBManager.close();
    log.debug("Shipping data list........" + alShippingList + "......rulelist......" + alRuleList);
    return hmReturn;
  }

  /**
   * This method returns the values for the tracking nos dropdown on the modify shipping screen
   * 
   * @param strShipId
   * @return ArrayList
   * @throws AppError
   */

  public ArrayList fetchTrackingNumbers(String strShipId) throws AppError {
    ArrayList aLReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_unique_tracknos", 2);

    gmDBManager.setString(1, strShipId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    aLReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return aLReturn;
  }

  public String fetchApplyAllStatus(String strShipID) throws AppError {
    String strStatus = "N";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_cm_shipping_info.gm_fch_ship_applyall_sts", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strShipID);
    gmDBManager.execute();
    strStatus = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strStatus;
  }// End of fetchInvQtyPartMappingByID

  /**
   * Description : Fetch total shipment values for selected consiment ids
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public ArrayList fetchConsignShipmentVal(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strConsignIds = GmCommonClass.parseZero((String) hmParam.get("STRCONSIGNIDS"));
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_consin_shipment_val", 2);

    gmDBManager.setString(1, strConsignIds);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alResult;
  }

  /**
   * Description : Update planned ship date selected in consignment ids return null
   * 
   * @param hmParam
   * @throws AppError
   */
  public void updatePlannedShipDate(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPlannedShipDt = GmCommonClass.parseNull((String) hmParam.get("STRPLANNEDDATE"));
    String strConsignIds = GmCommonClass.parseNull((String) hmParam.get("STRCONSIGNIDS"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("STRUSERID"));

    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_update_planned_shipdate", 3);

    gmDBManager.setString(1, strConsignIds);
    gmDBManager.setString(2, strPlannedShipDt);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * Description : Fetch all the txn types that are in pending shipping and their count return null
   * 
   * @param hmParam
   * @throws AppError
   */
  public ArrayList fetchPendingShippingCount(String strPartyId) throws AppError {

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_pend_ship_cnt", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartyId);  //added for PMT-34028 - Shipping Device - Dashboard Filter
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alReturn;
  }

  /**
   * Description : fetch address details like city, state, zip, etc..
   * This method/DB File used in shipping Device (erpshipping repository).
   * Please make sure the changes are done in that repository too if param added/removed for the DB call.
   * @param hmParam
   * @return
   * @throws AppError
   */
  public HashMap fetchShipAddress(String strTxnId, String strSource) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmAddr = new HashMap();

    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_ship_address", 3);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strSource);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.execute();
    hmAddr = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return hmAddr;
  }

  /**
   * @Description Fetch Available Ship Mode values and Default Ship Mode Value for the Selected Ship
   *              Carrier
   * @return ArrayList
   * @param String
   * @throws AppError
   * @author hreddy
   */
  public HashMap fetchCarrierBasedModes(String strShipCarrier, String strAcctID) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strDefaultMode = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_ship_car_modes", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strShipCarrier);
    gmDBManager.setString(2, strAcctID);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    strDefaultMode = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.close();
    hmReturn.put("SHIMODELIST", alReturn);
    hmReturn.put("DEFAULTMODE", strDefaultMode);
    return hmReturn;
  }

  /**
   * ShippedOrderData - This method will be used to retrieve shipping data for respective rep
   * 
   * @param
   * @return HashMap
   * @exception AppError
   */
  public HashMap ShippedOrderData() throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alShippingList = new ArrayList();
    ArrayList alRuleList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_shipping_info.gm_fch_shipped_order_today", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alShippingList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    alRuleList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    hmReturn.put("alShippingList", alShippingList);
    hmReturn.put("alRuleList", alRuleList);
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchShipCharge - This method is used to calculate the shipping charge
   * 
   * @param HashMap
   * @return String
   * @exception AppError
   */
  public String fetchShipCharge(HashMap hmParam) throws AppError {
    log.debug("hmParam>>>>>>>>>>>>>>>>" + hmParam);
    String strShipCharge = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_ship_charge.gm_fch_ship_cost_for_order", 8);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("ACCID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("SHIPTO")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("NAMES")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("ADDRESSID")));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("SHIPCARRIER")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("SHIPMODE")));
    gmDBManager.setString(7, "");
    gmDBManager.execute();
    strShipCharge = GmCommonClass.parseNull(gmDBManager.getString(8));
    log.debug("strShipCharge>>>>>>>>>>>>>>>>" + strShipCharge);
    gmDBManager.close();
    strShipCharge = strShipCharge.equals("0") ? "N/A" : strShipCharge;
    return strShipCharge;
  }
  
  /**
   * Description : fetch Building ID
   * PMT-34028 Shipping Device - Dashboard Filter
   * @param String
   * @return String
   * @throws AppError
   */
  public String fetchBuildingId(String strPartyId, String strGroupId) throws AppError {
    
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_inv_location_module.gm_fch_user_building_id", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setInt(1, Integer.parseInt(GmCommonClass.parseZero(strPartyId)));
    gmDBManager.setString(2, strGroupId);
    
    gmDBManager.execute();
    String strBuildingId = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();

    return strBuildingId;
  }
  
  /**
   * Description : fetch Party Id using User Id
   * PMT-34028 Shipping Device - Dashboard Filter
   * @param String
   * @return String
   * @throws AppError
   */
  public String fetchPartyId(String strUserId) throws AppError {
	  HashMap hmReturn = new HashMap();
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String sbQuery = " SELECT c101_party_id PARTYID FROM t101_user where c901_user_status = 311 AND c101_user_id ="+strUserId;	  
	  log.debug("fetchPartyId Query"+sbQuery);
	  hmReturn = GmCommonClass.parseNullHashMap(gmDBManager.querySingleRecord(sbQuery));
	  String strPartyID = GmCommonClass.parseNull((String) hmReturn.get("PARTYID"));;
	  log.debug("fetchPartyId strPartyID"+strPartyID);
      return strPartyID;
	  
  }
  
  /**
   * Description : fetch Tissue flag
   * PMT-45274 Tissue part validation in Shipping Device
   * @param String
   * @throws AppError
   */
  public void fetchTissueFlag(String strTxnId, String strSource) throws AppError {
    
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    HashMap hmAddr = new HashMap();

	    gmDBManager.setPrepareString("gm_pkg_op_shipping_validate.gm_fch_tissue_fl_frm_device", 2);
	    gmDBManager.setString(1, strTxnId);
	    gmDBManager.setString(2, strSource);

	    gmDBManager.execute();
	    gmDBManager.close();

  }
  
}
