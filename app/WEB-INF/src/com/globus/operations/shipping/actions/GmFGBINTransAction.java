/**
 * Description : This action is used to fetch the details of FG BIN Transactions
 * 
 * @author arajan
 */
package com.globus.operations.shipping.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.shipping.beans.GmShipScanReqRptBean;
import com.globus.operations.shipping.forms.GmFGBINTransForm;


public class GmFGBINTransAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward fetchFGBINTransactions(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    GmFGBINTransForm gmFGBINTransForm = (GmFGBINTransForm) actionForm;
    gmFGBINTransForm.loadSessionParameters(request);
    GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean();

    HashMap hmResult = new HashMap();
    String strScanId = "";
    String strXmlGridData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    strScanId = GmCommonClass.parseNull(gmFGBINTransForm.getScanId());
    hmResult =
        GmCommonClass
            .parseNullHashMap(gmShipScanReqRptBean.fetchShipScanTrans(strScanId, "102988")); // 102988:
                                                                                             // FG
    hmResult.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale); // BIN
    strXmlGridData = generateOutPut(hmResult);
    gmFGBINTransForm.setGridXmlData(strXmlGridData);
    return actionMapping.findForward("GmFGBINTransList");
  }

  public String generateOutPut(HashMap hmResult) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmResult.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmResult", hmResult);
    templateUtil.setTemplateSubDir("operations/shipping/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.shipping.GmFGBINTransList", strSessCompanyLocale));
    templateUtil.setTemplateName("GmFGBINTransList.vm");
    return templateUtil.generateOutput();
  }
}
