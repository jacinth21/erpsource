package com.globus.operations.shipping.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmShippingAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.operations.shipping.forms.GmShipDetailsForm;

public class GmShipDetailsAction extends GmShippingAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,

  HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmShipDetailsForm gmShipDetailsForm = (GmShipDetailsForm) form;
    gmShipDetailsForm = loadDrpDwnValues(gmShipDetailsForm);

    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());
    HashMap hmNames = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmParam = new HashMap();

    String strActForward = "";
    String strShipCharge = "";
    Logger log = GmLogger.getInstance(this.getClass().getName());
    log.debug("inside GmShipDetailsAction class");

    hmNames = gmShipDetailsForm.getHmNames();
    request.setAttribute("NAMES", hmNames);

    String strRefId = gmShipDetailsForm.getRefId();
    String strSource = gmShipDetailsForm.getSource();
    String strOpt = gmShipDetailsForm.getStrOpt();
    // To know whether to show text box or label for shipping charge
    String strEditChargeFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("EDTCHRG", "SHIPCHARGE",
            getGmDataStoreVO().getCmpid()));
    gmShipDetailsForm.setEditShipChrgFl(strEditChargeFl);
    log.debug("strRefId " + strRefId + " strOpt " + strOpt + " strSource " + strSource);

    if (strOpt.equals("calcShpChrg")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmShipDetailsForm);
      strShipCharge = gmShippingInfoBean.fetchShipCharge(hmParam);

      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strShipCharge);
      pw.flush();
      return null;
    } else {
      strShipCharge = GmCommonClass.parseNull(gmShipDetailsForm.getShipCharge());
      strShipCharge = strShipCharge.equals("") ? "0.00" : strShipCharge;
      gmShipDetailsForm.setShipCharge(strShipCharge);
      if (!strOpt.equals("")) {
        fetchShippingDetails(gmShipDetailsForm);
        log.debug("address id is " + gmShipDetailsForm.getAddressid());
      }
      strActForward = GmCommonClass.parseNull(request.getParameter("RE_FORWARD"));
      strActForward = strActForward.equals("") ? "success" : strActForward;
      log.debug("strActForward===========" + strActForward);
    }
    return mapping.findForward(strActForward);

  }

}
