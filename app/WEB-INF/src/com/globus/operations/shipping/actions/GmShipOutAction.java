package com.globus.operations.shipping.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmShippingAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.returns.beans.GmReturnsBean;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.operations.shipping.forms.GmShipDetailsForm;
import com.globus.operations.inventory.beans.GmInvLocationBean;

public class GmShipOutAction extends GmShippingAction {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);

    GmShipDetailsForm gmShipDetailsForm = (GmShipDetailsForm) form;

    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());

    String strCompId =
        gmShippingTransBean.getTxnCompanyDtls(gmShipDetailsForm.getRefId(),
            gmShipDetailsForm.getSource(), gmShipDetailsForm.getShipId());

    getGmDataStoreVO().setCmpid(strCompId);
    gmShippingTransBean = new GmShippingTransBean(getGmDataStoreVO());
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean();
    HashMap hmParam = new HashMap();
    HashMap hmNames = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmModAccess = new HashMap();
    ArrayList alSource = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alList = new ArrayList();

    String strOpt = gmShipDetailsForm.getStrOpt();
    String strRefId = "";
    String strAccID ="";
    String strSource = "";
    String strShipId = "";
    String strForward = "success";
    String strAddressId = "";

    String strReqId = "";
    String strInpStr = "";

    String strRedirect = "";
    String strCustPo = "";

    String strNameId = "";
    String strShipToId = "";
    String strLoanerReqFlag = "N";
    String strAccessFl = "";
    String strCompanyInfo=GmCommonClass.parseNull(request.getParameter("companyInfo"));

    // Get the transactions from DB based on the company for which we need to disable/enable the
    // rollback button in modify shipping details screen
    String strRBTxn =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("BTNDISABLE", "RBTXN",
            strCompId));
    gmShipDetailsForm.setStrRBTxn(strRBTxn);

    HttpSession session = request.getSession(false);
    // String strstatus = gmShipDetailsForm.getStatus();
    log.debug("strOpt " + strOpt);
    // log.debug("strRefId " + strRefId + " strSource " + strSource);
    String strUserId = (String) session.getAttribute("strSessUserId");
    alSource = GmCommonClass.getCodeList("SHIPS");
    gmShipDetailsForm.setAlSource(alSource);
    alList = GmCommonClass.getCodeList("SHIPAC");
    gmShipDetailsForm.setAlUpdAction(alList);
    // log.debug("shipping date " + gmShipDetailsForm.getShipDate());
    gmShipDetailsForm = loadDrpDwnValues(gmShipDetailsForm);
    hmNames = gmShipDetailsForm.getHmNames();
    request.setAttribute("NAMES", hmNames);

    gmShipDetailsForm.loadSessionParameters(request);
    String strProjectType = "";


    if (strOpt.equalsIgnoreCase("ShipOut")) {
      strRefId = gmShipDetailsForm.getRefId();
      strSource = gmShipDetailsForm.getSource();
      strAccID=gmShipDetailsForm.getAccID();
      // strstatus = (strstatus.equals("30")) ? "40" : strstatus;
      // gmShipDetailsForm.setStatus(strstatus);

      hmParam = GmCommonClass.getHashMapFromForm(gmShipDetailsForm);
      log.debug("shipout strstatus is " + gmShipDetailsForm.getStatus());
      log.debug("strRefId " + strRefId);
      log.debug("SOURCE " + strSource);
      hmParam.put("ADDRESSID", request.getParameter("addressid"));
      log.debug("ShipId is " + hmParam.get("SHIPID"));
      log.debug("test..." + request.getParameter("addressid"));
      System.out.println("StrAcct id is"+strAccID);
      // HashMap hmReturn = new HashMap();
      // Adding the Changed Cust PO for Request in Request Attribute
      strRedirect =
          "/gmOPEditShipDetails.do?shipId=" + (String) hmParam.get("SHIPID") + "&strOpt=report";
      request.setAttribute("hRedirectURL", strRedirect);
      hmParam.put("strAccID",strAccID);
      hmParam.put("COMPANY_INFO", strCompanyInfo);
      hmReturn = gmShippingTransBean.saveShipOut(hmParam);

      // --added below code for PMT-14689, to send RA initiate email notification
      GmReturnsBean gmReturnBean = new GmReturnsBean(getGmDataStoreVO());
      gmReturnBean.sendRAEmail(strRefId, strSource, strUserId);
           
      strShipId = GmCommonClass.parseNull((String) hmReturn.get("SHIPID"));
      String strShipFlag = GmCommonClass.parseNull((String) hmReturn.get("SHIPFLAG"));
      String strReturnId = GmCommonClass.parseNull((String) hmReturn.get("RETURNID"));
      String strChk_Loaner = GmCommonClass.parseNull((String) hmParam.get("CHK_LOANER"));
      gmShipDetailsForm.setShipId(strShipId);
      gmShipDetailsForm.setShipFlag(strShipFlag);
      gmShipDetailsForm.setRmaID(strReturnId);
      gmShipDetailsForm.setChk_loaner(strChk_Loaner);
      log.debug("strReturnId==>" + strReturnId);
      log.debug("strShipFlag==>" + strShipFlag);
      
      

      // JMS Call to update the Acct net by Lot PMT:14376
      // 50180 - Order
      // 50181 - Consignment
      
     
      // log.debug("strShipId && strShipFlag "+strShipId +"
      // "+strShipFlag);

      /*
       * below code is commented as james said after shipping out no message should be shown but the
       * same page should be displayed with a packslip button
       * 
       * if (strShipFlag.equals("true") && strSource.equals("50183")) {// loaner // extn throw new
       * AppError("Loaner Extension " + strRefId + " Shipped
       * Successfully.", "", 'S'); } if (strShipFlag.equals("true") && strSource.equals("50182"))
       * {// loaner throw new AppError("Loaner " + strRefId + " Shipped Successfully.", "", 'S'); }
       * if (strShipFlag.equals("true") && strSource.equals("50180")) {// orders // check if we can
       * hardcode this value as 1500 here. else we // might have to define Cbo_ProjectType as a
       * parameter. strProjectType = request.getParameter("Cbo_ProjectType") == null ? "1500" :
       * request.getParameter("Cbo_ProjectType"); alList = gmCommon.getCodeList("PKPRN"); hmReturn =
       * gmCust.loadOrderDetails(strRefId, strProjectType); request.setAttribute("hmReturn",
       * hmReturn); request.setAttribute("alcomboList", alList); request.setAttribute("ProjectType",
       * strProjectType); return mapping.findForward("success"); }
       */
      gmShipDetailsForm.setMsgFlag("Y");
    }
    // if (!strRefId.equals("")) {
    if (!strRefId.equals("") || strOpt.equalsIgnoreCase("report")
        || strOpt.equalsIgnoreCase("modify")) {
      HashMap hmAccess = new HashMap();
      HashMap hmAccessFl = new HashMap();
      fetchShippingDetails(gmShipDetailsForm);
      strShipId = gmShipDetailsForm.getShipId();
      strRefId = gmShipDetailsForm.getRefId();
      strSource = gmShipDetailsForm.getSource();
      strAddressId = gmShipDetailsForm.getAddressid();
      /*
       * Following code added for checking the Security Group Event for CS team to access the Loaner
       * Request Papaer Work under PMT-254 Task
       */
      hmAccessFl =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "LR_SHIP_ADDR_ACCESS"));
      String strAccessFlag = GmCommonClass.parseNull((String) hmAccessFl.get("UPDFL"));
      if (strAccessFlag.equals("Y") && strSource.equals("50182")) {
        strLoanerReqFlag = gmShippingInfoBean.fetchApplyAllStatus(strShipId);
      }
      
      /* PMT-40240 - Added below condition to fetch the Tissue flag 
       * Screen: Edit Ship details from Loaner Process request
       * Author: gpalani
       */
      String strTissueFlag=GmCommonClass.parseNull(gmShippingInfoBean.fetchTissueFlag(strShipId));
      gmShipDetailsForm.setTissuefl(strTissueFlag);

      gmShipDetailsForm.setLoanerReqFlag(strLoanerReqFlag);
      log.debug("tissueflag in Java"+strTissueFlag);

      strRedirect = "/gmOPEditShipDetails.do?shipId=" + strShipId + "&strOpt=report";

      strNameId = gmShipDetailsForm.getNames();
      request.setAttribute("NAMEID", strNameId);
      strShipToId = gmShipDetailsForm.getShipTo();
      request.setAttribute("SHIPTOID", strShipToId);
      if (strOpt.equalsIgnoreCase("modify") && strSource.equals("50180")) {
        String strFunctionId = "ORSHIP_ACEESS";
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                strFunctionId));
        String strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("strUpdateAccess" + strUpdateAccess + "strUserId" + strUserId);
        if (!strUpdateAccess.equals("Y")) {
          request.setAttribute("hRedirectURL", strRedirect);
          throw new AppError("", "20685");
        }
      }
      if (!strRefId.equals("")) {
        if (strSource.equals("50183")) { // loaner extn
          alLogReasons = gmCommonBean.getLog(strRefId, "1241");
        } else if (strSource.equals("50182")) {// loaners
          alLogReasons = gmCommonBean.getLog(strRefId, "1222");
        } else if (strSource.equals("50180")) {// orders
          alLogReasons = gmCommonBean.getLog(strRefId, "1200");
        } else if (strSource.equals("50181")) {// consign
          alLogReasons = gmCommonBean.getLog(strRefId, "1221");
        }
        gmShipDetailsForm.setAlLogReasons(alLogReasons);
      }
      request.setAttribute("REFID", strRefId);
      request.setAttribute("ADDRESSID", strAddressId);
      request.setAttribute("SOURCE", strSource);
      request.setAttribute("RE_FORWARD", "gmEditShipDetails");
      request.setAttribute("RE_TXN", "SHIPOUTWRAPPER");
      request.setAttribute("txnStatus", "PROCESS");
      strForward = "gmRuleEngine";
    }
    if (!strOpt.equalsIgnoreCase("ShipOut")) {
      gmShipDetailsForm.setChk_loaner("");
    }
    String strPkSlpNm =
        GmCommonClass.getRuleValue(gmShipDetailsForm.getNames(), "PACKSLIP/SHIPLIST");
    log.debug("strPkSlpNm===>" + strPkSlpNm);
    gmShipDetailsForm.setPkSlipNm(strPkSlpNm);
    
    //PMT-33781 : Security event for Modify Shipping Details 
    String strPartyId = GmCommonClass.parseNull(gmShipDetailsForm.getSessPartyId());
    hmModAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "MODIFY_SHIP_EDIT_ACC"));  	// Allow security event to Submit button in PMT-33781 
    strAccessFl = GmCommonClass.parseNull((String)hmModAccess.get("UPDFL"));
    if(!strAccessFl.equals("Y")){
	  gmShipDetailsForm.setStrSubmitButtonAccess("true");
    }
	
    hmModAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "MODIFY_SHIP_DIS_ACC"));  	// Allow security event to Disable button in PMT-33781
    strAccessFl = GmCommonClass.parseNull((String)hmModAccess.get("UPDFL"));
    if(!strAccessFl.equals("Y")){
  	  gmShipDetailsForm.setStrDisableButtonAccess("true");
      }
    
	return mapping.findForward(strForward);
  }
}
