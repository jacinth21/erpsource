/*****************************************************************************
 * File : GmShippingReportServlet Desc :
 * 
 * Version : 1.0 ---------------------------------------- version : 1.1 12/10/08 bvidyasankar
 * service method altered to accomodate filters in report screen
 * 
 ******************************************************************************/
package com.globus.operations.shipping.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmShippingReportServlet extends GmServlet {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strShipPath = GmCommonClass.getString("GMSHIP");
    String strDispatchTo = "";

    try {
      instantiate(request, response);
      strDispatchTo = (strShipPath + "/GmShippingReport.jsp");
      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
      String intAccLevel = GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl"));
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      HashMap hmApplnParam = new HashMap();
      hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
      hmApplnParam.put("DEPTID", strDeptId);
      hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
      String strAction = request.getParameter("hAction");
      GmCookieManager gmCookieManager = new GmCookieManager();
      GmAccessFilter gmAccessFilter = new GmAccessFilter();
      String strConsignIds = "";
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;
      String strOpt = request.getParameter("hOpt");

      String strCondition = "";
      String strFilterCondition = "";
      String strShipmentType = "";
      String strPlannedDate = "";
      String strPlShipDtSubmitFl = "";
      String strOptCol = "";

      log.debug(" strAction  " + strAction);

      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      // String strTodaysDate = (String) session.getAttribute("strSessTodaysDate");
      GmShippingInfoBean gmReport = new GmShippingInfoBean(getGmDataStoreVO());
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmSalesFilters = new HashMap();
      // To get the Access Level information
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      // strCondition = gmSalesDispatchAction.getAccessCondition(request, response, true);
      StringBuffer strConditionBuffer =
          gmAccessFilter.getHierarchyWhereConditionT907(request, response, "");
      strCondition = strConditionBuffer.toString();
      log.debug("strConditionBuffer.toString():::" + strConditionBuffer.toString());
      request.setAttribute("hmSalesFilters", hmSalesFilters);

      /** ***Added as part of Shipping Dashboard modifications***Start* */

      ArrayList alShippingSource = new ArrayList();
      ArrayList alShippingCarrier = new ArrayList();
      ArrayList alShipTo = new ArrayList();
      ArrayList alStatus = new ArrayList();
      ArrayList alDtTyp = new ArrayList();
      ArrayList alReturn = new ArrayList();
      ArrayList alShipType = new ArrayList();
      ArrayList alResult = new ArrayList();


      // RowSetDynaClass rdReturn = null;
      Date dtFromDate = null;
      Date dtToDate = null;

      String strSource = GmCommonClass.parseNull(request.getParameter("Cbo_ShippingSource"));
      String strRefID = GmCommonClass.parseNull(request.getParameter("Txt_RefId"));
      String strStatus = GmCommonClass.parseNull(request.getParameter("hStatus"));
      strStatus = (strAction.equals("Load")) ? (strStatus.equals("") ? "30" : "") : strStatus;
      log.debug("strStatus ==== " + strStatus);
      String strPendingFor = GmCommonClass.parseNull(request.getParameter("Txt_PendingDays"));
      String strCarrier = GmCommonClass.parseNull(request.getParameter("Cbo_ShippingCarrier"));
      String strShipTo = GmCommonClass.parseNull(request.getParameter("Cbo_ShipTo"));
      String strNames = GmCommonClass.parseNull(request.getParameter("Txt_Names"));

      dtFromDate = getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_FromDate")));
      dtToDate = getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_ToDate")));

      String strFromDate = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
      String strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));

      String strDtTyp = GmCommonClass.parseNull(request.getParameter("Cbo_Date"));
      String strReqID = GmCommonClass.parseNull(request.getParameter("Txt_ReqId"));
      String strHoldSts = GmCommonClass.parseNull(request.getParameter("Cbo_HoldSts"));
      strConsignIds = GmCommonClass.parseNull(request.getParameter("strConsignIds"));
      strShipmentType = GmCommonClass.parseNull(request.getParameter("Cbo_Shippment_Type"));
      strPlannedDate = GmCommonClass.parseNull(request.getParameter("Txt_PlannedDate"));
      strPlShipDtSubmitFl = GmCommonClass.parseNull(request.getParameter("hPlShipDtSubmitFl"));
      strOptCol = GmCommonClass.parseNull(request.getParameter("hchkOptCol"));


      String strSalesFilter = getAccessFilter(request, response);
      hmParam.put("AccessFilter", strSalesFilter);
      hmParam.put("ACESSLID", intAccLevel);
      hmParam.put("DEPTID", strDeptId);
      hmParam.put("SOURCE", strSource);
      hmParam.put("REFID", strRefID);
      hmParam.put("STATUS", strStatus);
      hmParam.put("PENDINGDAYS", strPendingFor);
      hmParam.put("CARRIER", strCarrier);
      hmParam.put("SHIPTO", strShipTo);
      hmParam.put("NAMES", strNames);
      hmParam.put("FROMDATE", strFromDate);
      hmParam.put("TODATE", strToDate);
      hmParam.put("DTTYP", strDtTyp);
      hmParam.put("REQID", strReqID);
      hmParam.put("HOLDSTS", strHoldSts);
      hmParam.put("DATEFORMAT", strApplnDateFmt);
      hmParam.put("STRCONDITION", strCondition);
      hmParam.put("STRCONSIGNIDS", strConsignIds);
      hmParam.put("STRSHIPMENTTYPE", strShipmentType);
      hmParam.put("STRPLANNEDDATE", strPlannedDate);
      hmParam.put("STRUSERID", strUserId);

      log.debug("hmParam..." + hmParam);
      String strCookieSource =
          GmCommonClass.parseNull(gmCookieManager.getCookieValue(request, strUserId + "SHIP"));
      if (!strCookieSource.equals(strSource) && !strSource.equals("")) {
        gmCookieManager.setCookieValue(response, strUserId + "SHIP", strSource);
        request.setAttribute("SOURCE_COOKIE", strSource);
      } else if (!strCookieSource.equals("")) {
        request.setAttribute("SOURCE_COOKIE", strCookieSource);
      }
      if (strAction.equals("UpdShipDate")
          || (strShipmentType.equals("102541") && !strAction.equals("Reload"))) {
        if (strAction.equals("UpdShipDate")) {
          gmReport.updatePlannedShipDate(hmParam);
          strAction = "Reload";
        }
        strDispatchTo = (strShipPath + "/GmShippmentDetDisplay.jsp");
      }
      /** ***Added as part of Shipping Dashboard modifications***End* */
      if (strAction.equals("Reload")) {
        hmReturn.put("hSource", strSource);
        hmReturn.put("hRefID", strRefID);
        hmReturn.put("hReqID", strReqID);
        hmReturn.put("hStatus", strStatus);
        hmReturn.put("hPendingFor", strPendingFor);
        hmReturn.put("hCarrier", strCarrier);
        hmReturn.put("hShipTo", strShipTo);
        hmReturn.put("hNames", strNames);
        hmReturn.put("hFromDate", dtFromDate);
        hmReturn.put("hToDate", dtToDate);
        hmReturn.put("hDtTyp", strDtTyp);
        hmReturn.put("hHoldSts", strHoldSts);
        hmReturn.put("hShipmentType", strShipmentType);
        hmReturn.put("STRCONSIGNIDS", strConsignIds);
        if (strShipmentType.equals("102540")) {
          alResult = gmReport.fetchConsignShipmentVal(hmParam);
        }
        log.debug("strConsignIds..." + strConsignIds);
        log.debug("alResult::" + alResult);
        alReturn = gmReport.fetchShippingReport(hmParam);
        request.setAttribute("REPORT", generateOutPut(alReturn, hmApplnParam));
        hmReturn.put("SHIPMENTVAL", alResult);
        if (strShipmentType.equals("102540") || strShipmentType.equals("102541")) {
          strDispatchTo = (strShipPath + "/GmShippmentDetDisplay.jsp");
        }
      }
      log.debug("alReturn ==>" + alReturn.size());
      if (alReturn != null) {
        request.setAttribute("hCntRows", "" + alReturn.size());
      }

      alShippingSource = GmCommonClass.getCodeList("SHIPS");
      alShippingCarrier = GmCommonClass.getCodeList("DCAR", getGmDataStoreVO());
      alShipTo = GmCommonClass.getCodeList("CONSP");
      alStatus = GmCommonClass.getCodeList("SHPST");
      alDtTyp = GmCommonClass.getCodeList("SHPDT");
      alShipType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SHPTYP"));

      hmReturn.put("SOURCELIST", alShippingSource);
      hmReturn.put("CARRIERLIST", alShippingCarrier);
      hmReturn.put("SHIPTOLIST", alShipTo);
      hmReturn.put("STATUSLIST", alStatus);
      hmReturn.put("DTTYPE", alDtTyp);
      hmReturn.put("SHIPTYPE", alShipType);
      hmReturn.put("STRPLANNEDDATE", strPlannedDate);
      hmReturn.put("HPLSHIPDTSUBMITFL", strPlShipDtSubmitFl);
      // hmReturn.put("hStatus", strStatus);

      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hOpt", strOpt);
      request.setAttribute("status", strStatus);
      request.setAttribute("OPTCOL", strOptCol);
      request.setAttribute("DEPTID", strDeptId);
      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method


  private String generateOutPut(ArrayList alValues, HashMap hmParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessLocal = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setDataList("alResult", alValues);
    templateUtil.setTemplateSubDir("operations/shipping/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.custservice.Shipping.GmShippingReport", strSessLocal));
    templateUtil.setTemplateName("GmShippingReport.vm");
    return templateUtil.generateOutput();
  }
}// End of GmShippingReportServlet
