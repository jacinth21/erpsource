package com.globus.operations.lot.beans;

import java.util.HashMap;   

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.operations.GmLotOnHandVO;


/**
 * @author aprasath
 * @version 1.0
 * @created 08-Apr-2019 9:50:43 PM
 */
public class GmLotHandReportBean extends GmSalesFilterConditionBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	public GmLotHandReportBean(){

	}

	
	/**
	 * 
	 * This method used to show the lots on hand with part number an count
	 * 
	 * Join v205_parts_with_ext_vendor,t2550_part_control_number,
	 * v700_territory_mapping_detail v700
	 * 
	 * C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339
	 * C2550_REF_ID = v700.dist_id
	 * Add strAccessFilter
	 * 
	 * Use below code to execute query and return JSON String
	 * <b>gmDBManager.</b><b>queryJSONRecord</b><b>(sbQuery.toString())</b>
	 * Refer GmCRMPreceptorshipRptBean <b>loadHostCaseJSON</b>
	 * <b>
	 * </b><b>Set into GmLotOnHandVO.partInfo</b>
	 * 
	 * @param hmParam
	 */
	public GmLotOnHandVO fetchLotsOnHand(HashMap hmParam){
		
		StringBuffer sbQuery = new StringBuffer();
		GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmAccessFilter = (HashMap) hmParam.get("ACCESSFILTER");
	    String strAccessFilter = GmCommonClass.parseNull((String) hmAccessFilter.get("AccessFilter"));
		sbQuery
			.append(" select JSON_ARRAYAGG (JSON_OBJECT('PARTNUM' VALUE t2550.c205_part_number_id, 'PARTDESC' VALUE REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', ''), 'COUNT' VALUE count(t2550.c205_part_number_id)) RETURNING CLOB) ");
		sbQuery.append(" FROM v205_parts_with_ext_vendor v205, t2550_part_control_number t2550, t205_part_number t205,");
		sbQuery
        	.append(" (select v700.d_id from v700_territory_mapping_detail v700 where v700.d_id is not null ");
		if (!strAccessFilter.equals("")){
	    	 sbQuery
		        .append(" AND " + strAccessFilter ); 
	     }
		sbQuery.append(" group by v700.d_id ) v700 " );
		sbQuery
        	.append(" WHERE v205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  AND t2550.C2550_REF_ID = v700.D_ID ");
		sbQuery
        	.append(" AND t205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  AND t2550.C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339 ");
		sbQuery.append(" group by t2550.c205_part_number_id, t205.c205_part_num_desc ");
		log.debug("sbQuery lot"+sbQuery);
		String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
		gmLotOnHandVO.setPartInfo(strReturn);
		return gmLotOnHandVO;
		
	}

	/**
	 * 
	 * This method used to show the expiring lots on hand with part number and count based on expiry date
	 *
	 * 
	 * Join v205_parts_with_ext_vendor,t2550_part_control_number,
	 * v700_territory_mapping_detail v700
	 * 
	 * C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339
	 * C2550_REF_ID = v700.dist_id
	 * Add strAccessFilter
	 * 
	 * if days = 30
	 * C2550_EXPIRY_DATE   between sysdate+0 and sysdate+30
	 * 
	 * if days = 60
	 * C2550_EXPIRY_DATE   between sysdate+31 and sysdate+60
	 * 
	 * if days = 90
	 * C2550_EXPIRY_DATE   between sysdate+61 and sysdate+90
	 * 
	 * Use below code to execute query and return JSON String
	 * <b>gmDBManager.</b><b>queryJSONRecord</b><b>(sbQuery.toString())</b>
	 * Refer GmCRMPreceptorshipRptBean <b>loadHostCaseJSON</b>
	 * <b>
	 * </b><b>Set into GmLotOnHandVO.partInfo</b>
	 * 
	 * @param hmParam
	 * @param days
	 */
	public GmLotOnHandVO fetchLotExpByDays(HashMap hmParam, int days){
		StringBuffer sbQuery = new StringBuffer();
		GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmAccessFilter = (HashMap) hmParam.get("ACCESSFILTER");
	    String strAccessFilter = GmCommonClass.parseNull((String) hmAccessFilter.get("AccessFilter"));
		
		sbQuery
			.append(" select JSON_ARRAYAGG (JSON_OBJECT('PARTNUM' VALUE t2550.c205_part_number_id, 'PARTDESC' VALUE REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', ''), 'COUNT' VALUE count(t2550.c205_part_number_id)) RETURNING CLOB) ");
		sbQuery
			.append(" FROM v205_parts_with_ext_vendor v205, t2550_part_control_number t2550, t205_part_number t205, ");
		sbQuery
        	.append(" (select v700.d_id from v700_territory_mapping_detail v700 where v700.d_id is not null ");
		if (!strAccessFilter.equals("")){
	    	 sbQuery
		        .append(" AND " + strAccessFilter ); 
	     }
		sbQuery
			.append(" group by v700.d_id ) v700 " );
		sbQuery
        	.append(" WHERE   v205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  AND t2550.C2550_REF_ID = v700.D_ID ");
		sbQuery
        	.append(" AND  t205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  AND t2550.C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339 ");
		if(days == 30){
   	      sbQuery
   	        .append( " AND t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30 ");
   	     }
        
         if(days == 60){
   	      sbQuery
   	        .append( " AND t2550.C2550_EXPIRY_DATE between sysdate+31 and sysdate+60 ");
   	     }
         if(days == 90){
   	      sbQuery
   	        .append( " AND t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90 ");
   	     }
         
		sbQuery.append(" group by t2550.c205_part_number_id, t205.c205_part_num_desc");
		log.debug("sbQuery"+sbQuery);
		String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
		gmLotOnHandVO.setPartInfo(strReturn);
		return gmLotOnHandVO;
	}

	/**
	 * 
	 * This method used to show the expired lots on hand with part number and count based on expiry date
	 * 
	 * Join v205_parts_with_ext_vendor,t2550_part_control_number,
	 * v700_territory_mapping_detail v700
	 * 
	 * 
	 * C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339
	 * C2550_REF_ID = v700.dist_id
	 * Add strAccessFilter
	 * C2550_EXPIRY_DATE   < sysdate
	 * 
	 * Use below code to execute query and return JSON String
	 * <b>gmDBManager.</b><b>queryJSONRecord</b><b>(sbQuery.toString())</b>
	 * Refer GmCRMPreceptorshipRptBean <b>loadHostCaseJSON</b>
	 * <b>
	 * </b><b>Set into GmLotOnHandVO.partInfo</b>
	 * 
	 * @param hmParam
	 */
	public GmLotOnHandVO fetchExpiredLots(HashMap hmParam){

		StringBuffer sbQuery = new StringBuffer();
		GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmAccessFilter = (HashMap) hmParam.get("ACCESSFILTER");
	    String strAccessFilter = GmCommonClass.parseNull((String) hmAccessFilter.get("AccessFilter"));
		sbQuery
			.append(" select JSON_ARRAYAGG (JSON_OBJECT('PARTNUM' VALUE t2550.c205_part_number_id, 'PARTDESC' VALUE REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', ''), 'COUNT' VALUE count(t2550.c205_part_number_id)) RETURNING CLOB) ");
		sbQuery.append(" FROM v205_parts_with_ext_vendor v205, t2550_part_control_number t2550, t205_part_number t205, ");
		sbQuery
        	.append(" (select v700.d_id from v700_territory_mapping_detail v700 where v700.d_id is not null ");
		if (!strAccessFilter.equals("")){
	    	 sbQuery
		        .append(" AND " + strAccessFilter ); 
	     }
		sbQuery.append(" group by v700.d_id ) v700 " );
		sbQuery
        	.append(" WHERE   v205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  AND t2550.C2550_REF_ID = v700.D_ID ");
		sbQuery
        	.append(" AND  t205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  AND t2550.C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339 ");
		sbQuery
        	.append(" AND t2550.c2550_expiry_date < sysdate ");
		sbQuery.append(" group by t2550.c205_part_number_id, t205.c205_part_num_desc ");
		log.debug("sbQuery expired"+sbQuery);
		String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
		gmLotOnHandVO.setPartInfo(strReturn);
		return gmLotOnHandVO;
	}

	/**
	 * 
	 * This method used to show the expiry graph based on count
	 * 
	 * Join v205_parts_with_ext_vendor,t2550_part_control_number,
	 * v700_territory_mapping_detail v700
	 * 
	 * C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339
	 * C2550_REF_ID = v700.dist_id
	 * Add strAccessFilter
	 * 
	 * case when
	 * C2550_EXPIRY_DATE   between sysdate+0 and sysdate+30 then
	 * count (1) 30days,
	 * case when
	 * C2550_EXPIRY_DATE   between sysdate+31 and sysdate+60 then count (1) 60days
	 * case when
	 * C2550_EXPIRY_DATE   between sysdate+61 and sysdate+90 then count (1) 90days
	 * case when
	 * C2550_EXPIRY_DATE   > sysdate+91 then count (1) 90plusdays
	 * case when
	 * C2550_EXPIRY_DATE  < sysdate then count (1) 30days expired
	 * 
	 * Use below code to execute query and return JSON String
	 * <b>gmDBManager.</b><b>queryJSONRecord</b><b>(sbQuery.toString())</b>
	 * Refer GmCRMPreceptorshipRptBean <b>loadHostCaseJSON</b>
	 * <b>
	 * </b><b>Set into GmLotOnHandVO.lotExpDays</b>
	 * 
	 * @param hmParam
	 */
	public GmLotOnHandVO fetchLotsExpGraph(HashMap hmParam){
		StringBuffer sbQuery = new StringBuffer();
		GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		HashMap hmAccessFilter = (HashMap) hmParam.get("ACCESSFILTER");
	    String strAccessFilter = GmCommonClass.parseNull((String) hmAccessFilter.get("AccessFilter"));
	
		
		sbQuery
			.append(" Select Json_Arrayagg (Json_Object('THIRTY' Value thirtydays, 'SIXTY' Value sixtydays, 'NINTY' Value nintydays, ");
		sbQuery.append("  'NINTYPLUS' Value nintyplus, 'EXPIRED' Value expired ) RETURNING CLOB) ");
		sbQuery
        	.append(" From( Select Sum(thirtydays) thirtydays, sum(sixtydays) sixtydays, sum(nintydays) nintydays, sum(nintyplusdays) nintyplus, ");
		sbQuery.append("  sum(expireddays) expired From (SELECT (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 0 AND sysdate + 30 THEN COUNT(1) END)thirtydays, ");
		sbQuery
        	.append(" (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 31 AND sysdate + 60 THEN COUNT(1) END)sixtydays, ");
		sbQuery
        	.append(" (CASE WHEN t2550.C2550_EXPIRY_DATE BETWEEN sysdate + 61 AND sysdate + 90 THEN COUNT(1) END)nintydays, ");
		sbQuery
    		.append(" (CASE WHEN t2550.C2550_EXPIRY_DATE > sysdate + 91 THEN COUNT(1) END)nintyplusdays, ");
		sbQuery
    		.append(" (CASE WHEN t2550.C2550_EXPIRY_DATE < sysdate THEN COUNT(1) END)expireddays FROM v205_parts_with_ext_vendor v205, t2550_part_control_number t2550, ");
		sbQuery
    		.append(" t205_part_number t205, ( select v700.d_id from v700_territory_mapping_detail v700 where v700.d_id is not null ");
		if (!strAccessFilter.equals("")){
	    	 sbQuery
		        .append(" AND " + strAccessFilter ); 
	     }
		sbQuery.append(" group by v700.d_id ) v700 " );
		sbQuery
			.append(" WHERE v205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  AND t2550.C2550_REF_ID = v700.D_ID ");
		sbQuery
    	.append(" AND T205.C205_Part_Number_Id = T2550.C205_Part_Number_Id  And T2550.C901_Last_Updated_Warehouse_Type = 4000339 ");
		sbQuery.append(" group By T2550.C2550_Expiry_Date)) ");
		log.debug("sbQueryexpgraph"+sbQuery);
		String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
		gmLotOnHandVO.setLotExpDays(strReturn);
		return gmLotOnHandVO;
	}

	/**
	 * 
	 *  This method used to show the details of part number in popup for On hand qty and expired lots
	 *  
	 * Join v205_parts_with_ext_vendor,t2550_part_control_number,t205_part_number,
	 * v700_territory_mapping_detail v700
	 * fetch part details and transaction details
	 * 
	 * C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339
	 * C2550_REF_ID = v700.dist_id
	 * Add strAccessFilter
	 * 
	 * if expflag is Y then add c205_expiry_date < sysdate
	 * 
	 * Use below code to execute query and return JSON String
	 * <b>gmDBManager.</b><b>queryJSONRecord</b><b>(sbQuery.toString())</b>
	 * Refer GmCRMPreceptorshipRptBean <b>loadHostCaseJSON</b>
	 * <b>
	 * </b><b>Set into GmLotOnHandVO.partDetails</b>
	 * 
	 * @param hmParam
	 */
	public GmLotOnHandVO fetchExpiryLotDetails(HashMap hmParam){
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO();
		String strPartNos = GmCommonClass.parseNull((String) hmParam.get("PARTNOS"));
	    String strExpiryFlag = GmCommonClass.parseNull((String) hmParam.get("EXPIRYFLAG"));
	   	HashMap hmAccessFilter = (HashMap) hmParam.get("ACCESSFILTER");
	    String strAccessFilter = GmCommonClass.parseNull((String) hmAccessFilter.get("AccessFilter"));
	    	   
       StringBuffer sbQuery = new StringBuffer();
	   
       	  sbQuery
       	  		.append("  SELECT JSON_ARRAYAGG (JSON_OBJECT( 'LotNum' VALUE lotnum, 'RepName' VALUE dname, 'PartDesc' VALUE partdesc, 'PartNum' VALUE partnum, ");
       	 sbQuery.append(" 'Expdate' VALUE expdate, 'Location' VALUE Location, 'TransID' VALUE transid, 'TransDate' VALUE updtransdate, 'guconsid' VALUE guid )returning CLOB)");
       	 sbQuery
       	 		.append(" FROM (SELECT t2550.C2550_CONTROL_NUMBER lotnum, v700.D_NAME dname, REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') partdesc, t2550.C205_PART_NUMBER_ID partnum, ");
       	 sbQuery
       	 		.append(" TO_CHAR(t2550.C2550_EXPIRY_DATE,'MM/DD/YYYY') expdate, GET_CS_SHIP_NAME(t2550.c901_ship_to_type,t2550.c5060_ship_to_id) Location, t2550.c2550_last_updated_trans_id transid,  ");
       	 sbQuery
       	 		.append(" TO_CHAR(t2550.C2550_LAST_UPDATED_TRANS_DATE,'MM/DD/YYYY') updtransdate, t907.c907_guid guid FROM v205_parts_with_ext_vendor v205, ");
       	 sbQuery
       	 		.append( " t2550_part_control_number t2550, t205_part_number t205, T907_SHIPPING_INFO t907, (SELECT v700.d_id, v700.D_NAME FROM v700_territory_mapping_detail v700 WHERE v700.d_id IS NOT NULL ");
       	 
       	if (!strAccessFilter.equals("")){
	    	 sbQuery
		        .append(" AND " + strAccessFilter ); 
	     }
       	sbQuery.append(" GROUP BY v700.d_id, v700.D_NAME ) v700 " );
       	 sbQuery
       	 		.append(" where v205.C205_Part_Number_Id = T2550.C205_Part_Number_Id AND t205.C205_Part_Number_Id = T2550.C205_Part_Number_Id AND t907.c907_REF_ID = t2550.c2550_last_updated_trans_id ");
         sbQuery
     			.append(" AND t2550.C2550_REF_ID = v700.D_ID AND t2550.C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339 ");
         if(strExpiryFlag.equals("N")){
   	      sbQuery
   		        .append(" AND T2550.C205_Part_Number_Id IN ('" + strPartNos + "'))" ); 
   	     }
	     if(strExpiryFlag.equals("Y")){
	      sbQuery
		        .append(" AND T2550.C205_Part_Number_Id IN ('" + strPartNos + "') AND t2550.C2550_EXPIRY_DATE  < sysdate)" ); 
	     }
	     log.debug("sbQuery<<<<<<<<<<<<<<<<<<" + sbQuery);
	     String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
	   	 gmLotOnHandVO.setPartDetails(strReturn);
	   	    return gmLotOnHandVO;
		
	}

	/**
	 * 
	 * This method used to show the details of part number in popup for 0-30 days, 31-60 days and 61-90 days
	 * 
	 * Join v205_parts_with_ext_vendor,t2550_part_control_number,t205_part_number,
	 * v700_territory_mapping_detail v700
	 * fetch part details and transaction details
	 * 
	 * 
	 * C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339
	 * C2550_REF_ID = v700.dist_id
	 * Add strAccessFilter
	 * 
	 * if days = 30
	 * C2550_EXPIRY_DATE   between sysdate+0 and sysdate+30
	 * 
	 * if days = 60
	 * C2550_EXPIRY_DATE   between sysdate+31 and sysdate+60
	 * 
	 * if days = 90
	 * C2550_EXPIRY_DATE   between sysdate+61 and sysdate+90
	 * 
	 * Use below code to execute query and return JSON String
	 * <b>gmDBManager.</b><b>queryJSONRecord</b><b>(sbQuery.toString())</b>
	 * Refer GmCRMPreceptorshipRptBean <b>loadHostCaseJSON</b>
	 * <b>
	 * </b><b>Set into GmLotOnHandVO.partDetails</b>
	 * 
	 * @param hmParam
	 * @param days
	 */
	public GmLotOnHandVO fetchExpLotDetailsByDays(HashMap hmParam, int days){
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmLotOnHandVO gmLotOnHandVO = new GmLotOnHandVO();
		String strPartNos = GmCommonClass.parseNull((String) hmParam.get("PARTNOS"));
		HashMap hmAccessFilter = (HashMap) hmParam.get("ACCESSFILTER");
	    String strAccessFilter = GmCommonClass.parseNull((String) hmAccessFilter.get("AccessFilter"));	   
        StringBuffer sbQuery = new StringBuffer();
	   
        sbQuery
        	.append(" SELECT JSON_ARRAYAGG (JSON_OBJECT( 'LotNum' VALUE lotnum, 'RepName' VALUE dname, 'PartDesc' VALUE partdesc, 'PartNum' VALUE partnum, ");
        sbQuery
        	.append(" 'Expdate' VALUE expdate, 'Location' VALUE Location, 'TransID' VALUE transid, 'TransDate' VALUE updtransdate, 'guconsid' VALUE guid )returning CLOB) ");
        sbQuery
        	.append("  FROM (SELECT t2550.C2550_CONTROL_NUMBER lotnum, v700.D_NAME dname, REGEXP_REPLACE(t205.c205_part_num_desc,'[^ ,0-9A-Za-z]', '') partdesc, t2550.C205_PART_NUMBER_ID partnum, ");
        sbQuery
        	.append(" TO_CHAR(t2550.C2550_EXPIRY_DATE,'MM/DD/YYYY') expdate, GET_CS_SHIP_NAME(t2550.c901_ship_to_type,t2550.c5060_ship_to_id) Location, t2550.c2550_last_updated_trans_id transid, ");
        sbQuery
        	.append(" TO_CHAR(t2550.C2550_LAST_UPDATED_TRANS_DATE,'MM/DD/YYYY') updtransdate, t907.c907_guid guid FROM v205_parts_with_ext_vendor v205, ");
        sbQuery
        	.append(" t2550_part_control_number t2550, t205_part_number t205, T907_SHIPPING_INFO t907, (SELECT v700.d_id, v700.D_NAME FROM v700_territory_mapping_detail v700 WHERE v700.d_id IS NOT NULL ");
        if (!strAccessFilter.equals("")){
   	 			sbQuery
	        		.append(" AND " + strAccessFilter ); 
    			}
        sbQuery.append(" GROUP BY v700.d_id, v700.D_NAME ) v700  " );
        sbQuery
        	.append( " where v205.C205_Part_Number_Id = T2550.C205_Part_Number_Id AND t205.C205_Part_Number_Id = T2550.C205_Part_Number_Id AND t907.c907_REF_ID = t2550.c2550_last_updated_trans_id ");
        sbQuery
        	.append(" AND t2550.C2550_REF_ID = v700.D_ID AND t2550.C901_LAST_UPDATED_WAREHOUSE_TYPE = 4000339 AND T2550.C205_Part_Number_Id IN ('" + strPartNos + "')  ");
    
        if(days == 30){
        	sbQuery
	        	.append( " AND t2550.C2550_EXPIRY_DATE  between sysdate+0 and sysdate+30) ");
        	}
    
        if(days == 60){
        	sbQuery
	        	.append( " AND t2550.C2550_EXPIRY_DATE between sysdate+31 and sysdate+60) ");
	     	}
        if(days == 90){
        	sbQuery
	        	.append( " AND t2550.C2550_EXPIRY_DATE  between sysdate+61 and sysdate+90) ");
	     	}
          log.debug("sbQuery<<<<<<<<<<<<<<<<<<" + sbQuery);
          String strReturn = gmDBManager.queryJSONRecord(sbQuery.toString());
	   		gmLotOnHandVO.setPartDetails(strReturn);
	   	    return gmLotOnHandVO;
		
	}
}//end GmLotHandReportBean