package com.globus.operations.dashboard.beans;

import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmFGDashBoardBean extends GmBean{
	public GmFGDashBoardBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());

	 /**
	   * loadIndividualDash - To fetch the count of  today request of loaners and to show on loaner dashboard screen
	   * 
	   * @param HashMap
	   * @return HashMap
	   * @throws AppError
	*/
public HashMap loadFGDashBoard() throws AppError {
		
		HashMap hmResult = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_FG_dashboard.gm_fch_fg_overall_dash", 8);
		gmDBManager.registerOutParameter(1, OracleTypes.NUMBER);
		gmDBManager.registerOutParameter(2, OracleTypes.NUMBER);
		gmDBManager.registerOutParameter(3, OracleTypes.NUMBER);
		gmDBManager.registerOutParameter(4, OracleTypes.NUMBER);
		gmDBManager.registerOutParameter(5, OracleTypes.NUMBER);
		gmDBManager.registerOutParameter(6, OracleTypes.NUMBER);
		gmDBManager.registerOutParameter(7, OracleTypes.NUMBER);
		gmDBManager.registerOutParameter(8, OracleTypes.NUMBER);
		gmDBManager.execute();
	    String item_cnt = Integer.toString(gmDBManager.getInt(1)) ;
	    String wip_ord_cnt =Integer.toString( gmDBManager.getInt(2));
	    String set_cnt = Integer.toString(gmDBManager.getInt(3));
	    String lnr_ext_cnt = Integer.toString( gmDBManager.getInt(4));
	    String inht_cnt = Integer.toString(gmDBManager.getInt(5));
	    String repl_cnt = Integer.toString(gmDBManager.getInt(6));
	    String lnr_cnt = Integer.toString(gmDBManager.getInt(7));
	    String ord_cnt = Integer.toString(gmDBManager.getInt(8));
		hmResult.put("CONSIGNEDITEMCONTROL", item_cnt);
		hmResult.put("ORDERCONTROL", wip_ord_cnt);
		hmResult.put("CONSIGNEDSETCONTROL", set_cnt);
		hmResult.put("LOANEREXTENSIONCONTROL", lnr_ext_cnt);
		hmResult.put("INHOUSECONTROL", inht_cnt);
		hmResult.put("REPLCONTROL", repl_cnt);
		hmResult.put("LOANERCONTROL", lnr_cnt);
		hmResult.put("SAMEDAYORDERCONTROL", ord_cnt);
		log.debug("hmResult bean"+hmResult);
		gmDBManager.close();
		return hmResult;
		}

}
