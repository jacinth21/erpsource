package com.globus.operations.dashboard.actions;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.beans.GmCommonClass;


import com.globus.common.actions.GmDispatchAction;
import javax.servlet.http.HttpServletRequest;     
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import com.globus.operations.dashboard.beans.GmFGDashBoardBean;
import com.globus.operations.dashboard.forms.GmFGDashBoardForm;



public class GmFGDashBoardAction extends GmDispatchAction{
	/**
	 * @description Load Loaner Dashboard
	 * @param actionMapping, form, request, response
	 * @return
	 * @throws Exception
	 */
	  public ActionForward loadFGDashBoard(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
	
		instantiate(request, response);
		
		ArrayList alReturn = new ArrayList();
		HashMap hmParam = new HashMap();
		HashMap hmResult = new HashMap();
		
		GmFGDashBoardForm gmFGDashBoardForm = (GmFGDashBoardForm) form;
		gmFGDashBoardForm.loadSessionParameters(request);
		GmFGDashBoardBean gmFGDashBoardBean = new GmFGDashBoardBean(getGmDataStoreVO());
		
		hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmFGDashBoardForm));
		
		hmResult = GmCommonClass.parseNullHashMap(gmFGDashBoardBean.loadFGDashBoard());
		log.debug("hmResult"+hmResult);
		request.setAttribute("HMRESULT", hmResult);
		
		return mapping.findForward("gmFGDashBoardSuccess");
	}

}
