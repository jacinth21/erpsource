package com.globus.operations.loaners.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmFieldSalesLoanersForm extends GmLogForm {
	private String strXmlData="";
	private String setId="";
	private String setName = "";
	private String reqId = "";
	private String etchId="";
	private String cnId="";
	private String returnIn="";
	private String daysOverDue;
	private String[] selectedSets = new String[15];
	private String[] selectedStatus=new String[3];	
	private String[] selectedDists=new String[15];
	
	private ArrayList alFieldSales = new  ArrayList();
	private ArrayList alSets = new  ArrayList();
	private ArrayList alStatus = new  ArrayList();
	
	public String[] getSelectedSets() {
		return selectedSets;
	}
	public void setSelectedSets(String[] selectedSets) {
		this.selectedSets = selectedSets;
	}
	public String[] getSelectedStatus() {
		return selectedStatus;
	}
	public void setSelectedStatus(String[] selectedStatus) {
		this.selectedStatus = selectedStatus;
	}
	public String[] getSelectedDists() {
		return selectedDists;
	}
	public void setSelectedDists(String[] selectedDists) {
		this.selectedDists = selectedDists;
	}
	public String getStrXmlData() {
		return strXmlData;
	}
	public void setStrXmlData(String strXmlData) {
		this.strXmlData = strXmlData;
	}
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public String getSetName() {
		return setName;
	}
	public void setSetName(String setName) {
		this.setName = setName;
	}

	public String getEtchId() {
		return etchId;
	}
	public void setEtchId(String etchId) {
		this.etchId = etchId;
	}
	public String getCnId() {
		return cnId;
	}
	public void setCnId(String cnId) {
		this.cnId = cnId;
	}
	public String getReturnIn() {
		return returnIn;
	}
	public void setReturnIn(String returnIn) {
		this.returnIn = returnIn;
	}

	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getDaysOverDue() {
		return daysOverDue;
	}
	public void setDaysOverDue(String daysOverDue) {
		this.daysOverDue = daysOverDue;
	}
	public ArrayList getAlFieldSales() {
		return alFieldSales;
	}
	public void setAlFieldSales(ArrayList alFieldSales) {
		this.alFieldSales = alFieldSales;
	}
	public ArrayList getAlSets() {
		return alSets;
	}
	public void setAlSets(ArrayList alSets) {
		this.alSets = alSets;
	}
	public ArrayList getAlStatus() {
		return alStatus;
	}
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	
}
