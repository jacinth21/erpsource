package com.globus.operations.loaners.forms;

import com.globus.common.forms.GmCommonForm;

public class GmSetLotMasterForm extends GmCommonForm {

  private String txnId = "";
  private String partNum = "";
  private String ctrlNum = "";



  /**
   * @return the txnId
   */
  public String getTxnId() {
    return txnId;
  }

  /**
   * @param txnId the txnId to set
   */
  public void setTxnId(String txnId) {
    this.txnId = txnId;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  public String getCtrlNum() {
    return ctrlNum;
  }

  public void setCtrlNum(String ctrlNum) {
    this.ctrlNum = ctrlNum;
  }


}
