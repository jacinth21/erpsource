package com.globus.operations.loaners.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.forms.GmLogForm;

public class GmLoanerLotReprocessForm extends GmLogForm {
	private String strXmlData="";
	private String strSetname="";
	private String strSetType="";
	private String strEtchId="";
	private String strLoanStatus="";
	private String strBillNm="";
	private Date dtLDate=null;
	private Date dtEDate=null; 
	private String strType = "";
    private String strLoanTransId=""; 
    private String strHoldFl=""; 
    private String strDistId  = ""; 
    private String strRepId  =""; 
    private String strAssocRepId = ""; 
    private String strRepNm  = ""; 
    private String strAccId  = ""; 
    private String strAccNm  = "";
	private String consignmentId = "";
    private String partNum = "";
    private String ctrlNum = "";
    private String qty = "";
    private String transType = "";
    private String inputString = "";
    private String log = "";
    private String transName = "";
    private String MISSSTR = "";
    private String REPFROM100040 = "";
    private String REPFROM100041 = "";
    private String REPFROM100061 = "";
    private String MOVETO100045 = "";
    private String MOVETO100046 = "";
    private String MOVETO100047 = "";
	private String strStatusFl = "";
    
    ArrayList alReturn = new  ArrayList();
    
	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}
	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}
	/**
	 * @return the strXmlData
	 */
	public String getStrXmlData() {
		return strXmlData;
	}
	/**
	 * @param strXmlData the strXmlData to set
	 */
	public void setStrXmlData(String strXmlData) {
		this.strXmlData = strXmlData;
	}
	/**
	 * @return the strSetname
	 */
	public String getStrSetname() {
		return strSetname;
	}
	/**
	 * @param strSetname the strSetname to set
	 */
	public void setStrSetname(String strSetname) {
		this.strSetname = strSetname;
	}
	/**
	 * @return the strSetType
	 */
	public String getStrSetType() {
		return strSetType;
	}
	/**
	 * @param strSetType the strSetType to set
	 */
	public void setStrSetType(String strSetType) {
		this.strSetType = strSetType;
	}
	/**
	 * @return the strEtchId
	 */
	public String getStrEtchId() {
		return strEtchId;
	}
	/**
	 * @param strEtchId the strEtchId to set
	 */
	public void setStrEtchId(String strEtchId) {
		this.strEtchId = strEtchId;
	}
	/**
	 * @return the strLoanStatus
	 */
	public String getStrLoanStatus() {
		return strLoanStatus;
	}
	/**
	 * @param strLoanStatus the strLoanStatus to set
	 */
	public void setStrLoanStatus(String strLoanStatus) {
		this.strLoanStatus = strLoanStatus;
	}
	/**
	 * @return the strBillNm
	 */
	public String getStrBillNm() {
		return strBillNm;
	}
	/**
	 * @param strBillNm the strBillNm to set
	 */
	public void setStrBillNm(String strBillNm) {
		this.strBillNm = strBillNm;
	}
	
	/**
	 * @return the dtLDate
	 */
	public Date getDtLDate() {
		return dtLDate;
	}
	/**
	 * @param dtLDate the dtLDate to set
	 */
	public void setDtLDate(Date dtLDate) {
		this.dtLDate = dtLDate;
	}
	/**
	 * @return the dtEDate
	 */
	public Date getDtEDate() {
		return dtEDate;
	}
	/**
	 * @param dtEDate the dtEDate to set
	 */
	public void setDtEDate(Date dtEDate) {
		this.dtEDate = dtEDate;
	}
	/**
	 * @return the strType
	 */
	public String getStrType() {
		return strType;
	}
	/**
	 * @param strType the strType to set
	 */
	public void setStrType(String strType) {
		this.strType = strType;
	}
	/**
	 * @return the strLoanTransId
	 */
	public String getStrLoanTransId() {
		return strLoanTransId;
	}
	/**
	 * @param strLoanTransId the strLoanTransId to set
	 */
	public void setStrLoanTransId(String strLoanTransId) {
		this.strLoanTransId = strLoanTransId;
	}
	/**
	 * @return the strHoldFl
	 */
	public String getStrHoldFl() {
		return strHoldFl;
	}
	/**
	 * @param strHoldFl the strHoldFl to set
	 */
	public void setStrHoldFl(String strHoldFl) {
		this.strHoldFl = strHoldFl;
	}
	/**
	 * @return the strDistId
	 */
	public String getStrDistId() {
		return strDistId;
	}
	/**
	 * @param strDistId the strDistId to set
	 */
	public void setStrDistId(String strDistId) {
		this.strDistId = strDistId;
	}
	/**
	 * @return the strRepId
	 */
	public String getStrRepId() {
		return strRepId;
	}
	/**
	 * @param strRepId the strRepId to set
	 */
	public void setStrRepId(String strRepId) {
		this.strRepId = strRepId;
	}
	/**
	 * @return the strAssocRepId
	 */
	public String getStrAssocRepId() {
		return strAssocRepId;
	}
	/**
	 * @param strAssocRepId the strAssocRepId to set
	 */
	public void setStrAssocRepId(String strAssocRepId) {
		this.strAssocRepId = strAssocRepId;
	}
	/**
	 * @return the strRepNm
	 */
	public String getStrRepNm() {
		return strRepNm;
	}
	/**
	 * @param strRepNm the strRepNm to set
	 */
	public void setStrRepNm(String strRepNm) {
		this.strRepNm = strRepNm;
	}
	/**
	 * @return the strAccId
	 */
	public String getStrAccId() {
		return strAccId;
	}
	/**
	 * @param strAccId the strAccId to set
	 */
	public void setStrAccId(String strAccId) {
		this.strAccId = strAccId;
	}
	/**
	 * @return the strAccNm
	 */
	public String getStrAccNm() {
		return strAccNm;
	}
	/**
	 * @param strAccNm the strAccNm to set
	 */
	public void setStrAccNm(String strAccNm) {
		this.strAccNm = strAccNm;
	}

		
	public String getConsignmentId() {
		return consignmentId;
	}

	public void setConsignmentId(String consignmentId) {
		this.consignmentId = consignmentId;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getInputString() {
		return inputString;
	}

	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getPartNum() {
		return partNum;
	}
	 
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	public String getCtrlNum() {
		return ctrlNum;
	}

	public void setCtrlNum(String ctrlNum) {
		this.ctrlNum = ctrlNum;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getTransName() {
		return transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}
	public String getMISSSTR() {
		return MISSSTR;
	}
	public void setMISSSTR(String mISSSTR) {
		MISSSTR = mISSSTR;
	}
	public String getREPFROM100040() {
		return REPFROM100040;
	}
	public void setREPFROM100040(String rEPFROM100040) {
		REPFROM100040 = rEPFROM100040;
	}
	public String getREPFROM100041() {
		return REPFROM100041;
	}
	public void setREPFROM100041(String rEPFROM100041) {
		REPFROM100041 = rEPFROM100041;
	}
	public String getREPFROM100061() {
		return REPFROM100061;
	}
	public void setREPFROM100061(String rEPFROM100061) {
		REPFROM100061 = rEPFROM100061;
	}
	public String getMOVETO100045() {
		return MOVETO100045;
	}
	public void setMOVETO100045(String mOVETO100045) {
		MOVETO100045 = mOVETO100045;
	}
	public String getMOVETO100046() {
		return MOVETO100046;
	}
	public void setMOVETO100046(String mOVETO100046) {
		MOVETO100046 = mOVETO100046;
	}
	public String getMOVETO100047() {
		return MOVETO100047;
	}
	public void setMOVETO100047(String mOVETO100047) {
		MOVETO100047 = mOVETO100047;
	}
	public String getStrStatusFl() {
		return strStatusFl;
	}
	public void setStrStatusFl(String strStatusFl) {
		this.strStatusFl = strStatusFl;
	}
    
    
}
