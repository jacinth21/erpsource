package com.globus.operations.loaners.forms;

import com.globus.common.forms.GmCommonForm;

public class GmReconfigLoanerForm extends GmCommonForm{
	
	private String strConsignId = "";
	private String strSetId = "";
	private String strPrintOpt = "";
	/**
	 * @return the strConsignId
	 */
	public String getStrConsignId() {
		return strConsignId;
	}
	/**
	 * @param strConsignId the strConsignId to set
	 */
	public void setStrConsignId(String strConsignId) {
		this.strConsignId = strConsignId;
	}
	/**
	 * @return the strSetId
	 */
	public String getStrSetId() {
		return strSetId;
	}
	/**
	 * @param strSetId the strSetId to set
	 */
	public void setStrSetId(String strSetId) {
		this.strSetId = strSetId;
	}
	/**
	 * @return the strPrintOpt
	 */
	public String getStrPrintOpt() {
		return strPrintOpt;
	}
	/**
	 * @param strPrintOpt the strPrintOpt to set
	 */
	public void setStrPrintOpt(String strPrintOpt) {
		this.strPrintOpt = strPrintOpt;
	}
	
	

}
