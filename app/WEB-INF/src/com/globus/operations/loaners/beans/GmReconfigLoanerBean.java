package com.globus.operations.loaners.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.standard.MediaName;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmFileReader;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.printwork.inhouseSet.beans.GmInhouseSetPrintInterface;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmWSUtil;

import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmReconfigLoanerBean extends GmBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	public GmReconfigLoanerBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * generatePDFPrintLoanerPPW - This method used to Generate print PPW paper work
	 * for loaner
	 * 
	 * @param strConsignId
	 * @param strSubRptDir
	 * @param strUserName
	 * @throws Exception
	 */
	public void generatePDFPrintLoanerPPW(String strConsignId, String strSubRptDir, String strUserName)
			throws Exception {
		HashMap hmCONSIGNMENT = new HashMap();
		GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean();
		String strPDFFileName = "";

		String strPortalCompanyId = getGmDataStoreVO().getCmpid();
		String strCompanyLocale = "";
		strCompanyLocale = GmCommonClass.getCompanyLocale(strPortalCompanyId);
		// Locale
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork",
				strCompanyLocale);
		GmResourceBundleBean gmResourceBunBeanVal = GmCommonClass.getResourceBundleBean("properties.Company",
				strCompanyLocale);
		HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strPortalCompanyId, "");
		String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
		String strCompLogo = GmCommonClass
				.parseNull(GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));

		hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
		// request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);
		ArrayList alResult = GmCommonClass.parseNullArrayList(gmInHouseRptBean.fetchLoanerBOParts(strConsignId));
		String strCheckBoxImg = strImagePath + "N.gif";
		hmCONSIGNMENT.put("COMPLOGO", strImagePath + strCompLogo + ".gif");
		hmCONSIGNMENT.put("CHECKBOXIMG", strCheckBoxImg);
		hmCONSIGNMENT.put("SUBREPORT_DIR", strSubRptDir);
		log.debug("hmCONSIGNMENT==" + hmCONSIGNMENT);
		log.debug("alResult==" + alResult);
		GmJasperReport gmJasperReport = new GmJasperReport();
		HashMap hmRefDetails = new HashMap();
		ArrayList alSetLoad = new ArrayList();
		String strLoanerSetPDFLocation = GmCommonClass.getString("RECONFIG_LOANER");
		log.debug("strLoanerSetPDFLocation==" + strLoanerSetPDFLocation);
		strPDFFileName = strLoanerSetPDFLocation + "\\PPW" + strConsignId + ".pdf";
		log.debug("strPDFFileName==" + strPDFFileName);
		File newPDFFile = new File(strPDFFileName);
		log.debug("File newPDFFile.exists()  == " + newPDFFile.exists());
		if (newPDFFile.exists()) {
			removeFile(strPDFFileName);
			log.debug("File Deleted == ");
		}

		log.debug("File Deleted == ");
		log.debug(" strPDFFileName == " + strPDFFileName);
		// File newFile = new File(strLoanerSetPDFLocation);
		hmCONSIGNMENT.put("CONSIGNID", strConsignId);
		hmCONSIGNMENT.put("BARCONSIGNID", strConsignId + "$50182");
		hmCONSIGNMENT.put("ALBACKORDER", alResult);
		hmCONSIGNMENT.put("PRINTBY", strUserName);
		gmJasperReport.setJasperReportName("/GmPrintLoanerPPW.jasper");
		gmJasperReport.setHmReportParameters(hmCONSIGNMENT);
		// gmJasperReport.setReportDataList(alResult);
		gmJasperReport.exportJasperToPdf(strPDFFileName);
	}

	/**
	 * removeFile - This method used to remove the file if that PDF file already
	 * exist
	 * 
	 * @param strFileName
	 */
	public void removeFile(String strFileName) {
		log.debug("removeFile strFileName==" + strFileName);
		File newPDFFile = new File(strFileName);
		newPDFFile.delete();
	}

	/**
	 * generatePDFViewPrint - This method used to generate PDF file Loaner Paper
	 * work
	 * 
	 * @param strConsignId
	 * @param strUserName
	 * @throws Exception
	 */
	public void generatePDFViewPrint(HashMap hmParam, String strUserName) throws Exception {
		HashMap hmCONSIGNMENT = new HashMap();
		GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(getGmDataStoreVO());
		 GmLoanerLotReprocessBean gmLoanerLotReprocessBean = new GmLoanerLotReprocessBean(getGmDataStoreVO());
		String strPDFFileName = "";
		java.sql.Date dtLDate = null;
		java.sql.Date dtEDate = null;
		ArrayList alReturn = new ArrayList();

		String strPortalCompanyId = getGmDataStoreVO().getCmpid();
		String strCompanyLocale = "";
		strCompanyLocale = GmCommonClass.getCompanyLocale(strPortalCompanyId);
		// Locale
		GmResourceBundleBean gmPaperworkResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork",
				strCompanyLocale);
		GmResourceBundleBean gmResourceBunBeanVal = GmCommonClass.getResourceBundleBean("properties.Company",
				strCompanyLocale);
		HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), "");
		
		 String strSubRptDir = GmCommonClass.parseNull((String) hmParam.get("SUBREPORT_DIR"));
		   String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSID"));
		    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		   //String strUserName = gmCommonBean.getUserName(strUserId);
		   String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		   String strLoanerReqId = GmCommonClass.parseNull((String) hmParam.get("LOANERREQID"));
		String strITCountryCode = GmCommonClass.parseNull((String) hmCompanyAddress.get("COUNTRYCODE"));  
		
		if( strITCountryCode.equals("it")){                 //   if the company is ITALY  - PMT-42950
  		  hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId, strLoanerReqId);
  	    }else {
  		  hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);
  	  }	  
		      		  
		 strSetID= GmCommonClass.parseNull((String) hmCONSIGNMENT.get("SETID"));
        String strLotTrkfl = gmLoanerLotReprocessBean.validateSetLotTrack(strSetID);
		
        String strLoanerStatusFlag =
                GmCommonClass.parseNull(GmCommonClass.getRuleValue("25", "LOANER_STATUS"));
        String strLoanerPaperworkFlag =
                GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("LOANER_PAPERWORK_FLAG"));
        HashMap hmIn = new HashMap();
        hmIn.put("CONSIGNID", strConsignId);
        hmIn.put("gmResourceBundleBean", gmPaperworkResourceBundleBean);
        hmIn.put("ACTION", "ViewPrint");
        hmIn.put("gmDataStoreVO", getGmDataStoreVO());
        hmIn.put("LOANERSTATUSFLAG", strLoanerStatusFlag);
        hmIn.put("LOANERPAPERWORKFLAG", strLoanerPaperworkFlag);
        hmIn.put("LOTTRKFL", strLotTrkfl);
       // request.setAttribute("LOTTRKFL", strLotTrkfl);

        GmInhouseSetPrintInterface gmInhouseSetPrintInterface =
            (GmInhouseSetPrintInterface) GmCommonClass.getSpringBeanClass(
                "xml/Inhouse_Print_Beans.xml", getGmDataStoreVO().getCmpid());
        alReturn = gmInhouseSetPrintInterface.fetchConsignSetDtls(hmIn);
        
        String strCompAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYADDRESS"));
        String strCompPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYPHONE"));
        String strCompFax = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYFAX"));
        String strCountryCode = GmCommonClass.parseNull((String) hmCompanyAddress.get("COUNTRYCODE"));
        String strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYNAME"));
        String strJasperName =
            GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("LOANER.JAS_LNR_ACK"));
        String strCompanyId = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYID"));
        String strCustServPhone = "";
        String strCompanyPhone = "";
        String strCompanyShortNm = "Globus";
        String  strCompNm = "Globus Medical";
        strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;
        String strDivisionId = strCompanyId.equals("100801") ? "2001" : "2000";
        String strCompleteAddress ="";
       String strCompanyIdLogoFlag =
                GmCommonClass.parseNull(gmResourceBunBeanVal.getProperty("COMPANY_ID_LOGO_FL"));
        if (strCompanyIdLogoFlag.equals("YES")) {
          strCompanyId = GmCommonClass.parseNull(gmResourceBunBeanVal.getProperty("COMP_LOGO"));
        }
        
        if (!strCountryCode.equals("en")) {
            strCompleteAddress =
                strCompanyName + "/" + strCompAddress + "/Phone no:" + strCompPhone + "/FAX:"
                    + strCompFax;
          } else if (strCountryCode.equals("en")) {
            hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strPortalCompanyId, strDivisionId);
            strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
            strCompAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
            strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
            strCompFax = GmCommonClass.parseNull((String) hmCompanyAddress.get("FAX"));
            strCompPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("CUST_SERVICE"));
            strCompanyShortNm =
                GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_SHORT_NAME"));
            strCompAddress = strCompAddress + "/" + strCompanyPhone;
            strCompleteAddress = strCompanyName + "/" + strCompAddress + "/" + strCompFax;
            strCompNm = strCompanyName;
          }

        
		//String strCompanyId = GmCommonClass.parseNull(GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYID")));
		//String strDivisionID = strCompanyId.equals("100801") ? "2001" : "2000";
		// hmCompanyAddress = GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), strDivisionID);
		//String strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
		String strCompanyAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
		String strCompanyLogo = GmCommonClass.parseNull((String) hmCompanyAddress.get("LOGO"));
		strCompanyLogo = "/" + strCompanyLogo + ".gif";
		// strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");
		dtLDate = (java.sql.Date) hmCONSIGNMENT.get("LDATE");
		dtEDate = (java.sql.Date) hmCONSIGNMENT.get("EDATE");
		String strDateFmt = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("CMPDFMT"));
		String strType = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("CTYPE"));
		String strHeader  =	GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("LOANER."+strType));
	    strHeader = strHeader.equals("")? "Loaner": strHeader;
		String strLnrDate = GmCommonClass.getStringFromDate(dtLDate, strDateFmt);
		String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
	
		//alReturn = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
		log.debug("hmCONSIGNMENT==" + hmCONSIGNMENT);
		log.debug("alReturn==" + alReturn);
		hmCONSIGNMENT.put("CONSID", strConsignId);
		// strConsignId = strConsignId + "$50182";
		hmCONSIGNMENT.put("BARCODECONSID", strConsignId + "$50182");
		hmCONSIGNMENT.put("COMPLOGO", strImagePath + strCompanyLogo);
		hmCONSIGNMENT.put("COMPADDRESS", strCompanyAddress);
		hmCONSIGNMENT.put("COMPNAME", strCompanyName);
		hmCONSIGNMENT.put("PRINTBY", strUserName);
		hmCONSIGNMENT.put("COMPPHONE", strCompPhone);
		hmCONSIGNMENT.put("COMPFAX", strCompFax);
		hmCONSIGNMENT.put("COMPDETAILS", strCompleteAddress);
		hmCONSIGNMENT.put("JASNAME", strJasperName);
		hmCONSIGNMENT.put("COMPSHNM", strCompanyShortNm);
		hmCONSIGNMENT.put("COMPANYNAME", strCompNm);
		hmCONSIGNMENT.put("COMPID", strCompanyId);
		
		
		 
	       
	        
	
		GmJasperReport gmJasperReport = new GmJasperReport();
		HashMap hmRefDetails = new HashMap();
		ArrayList alSetLoad = new ArrayList();
		String strLoanerSetPDFLocation = GmCommonClass.getString("RECONFIG_LOANER");
		log.debug("strLoanerSetPDFLocation==" + strLoanerSetPDFLocation);
		strPDFFileName = strLoanerSetPDFLocation + "\\Print" + strConsignId + ".pdf";
		log.debug("strPDFFileName==" + strPDFFileName);
		File newPDFFile = new File(strPDFFileName);
		log.debug("File newPDFFile.exists()  == " + newPDFFile.exists());
		if (newPDFFile.exists()) {
			removeFile(strPDFFileName);
			log.debug("File Deleted == ");
		}
 strJasperName = "/GmLoanerSetReConfigPrint.jasper";
		
		  String strShowCnumLSS=""; 
		  //String strLoanerPaperworkFlag =GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("LOANER_PAPERWORK_FLAG")); 
		  strJasperName = "/GmLoanerSetReConfigPrint.jasper";
		  
		  if (strType.equals("9110")) { 
			  strHeader= "In-House Loaner";
			  strJasperName="/GmLoanerSetReConfigPrint.jasper"; 
		  } else {
		  if (strShowCnumLSS.equalsIgnoreCase("YES")) { 
			  strJasperName="/GmLoanerSetReConfigPrint.jasper"; 
		  
		  } else if ((strLoanerPaperworkFlag.equals("YES"))) { 
			  hmCONSIGNMENT.put("GMCOMPANYNAME",GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME")));
			//FLR_ADDRESS,3FLR_PHONE,3FLR_FAX only for loaner and consignment(loan detail sheet) 
			  hmCONSIGNMENT.put("3FLRADDRESS",GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("3FLR_ADDRESS")));
			hmCONSIGNMENT.put("3FLRPHONE",GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("3FLR_PHONE")));
			hmCONSIGNMENT.put("3FLRFAX",GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("3FLR_FAX")));
			if(alSetLoad.size()!=0){
				Iterator itrMap = alReturn.iterator();
				while (itrMap.hasNext()) {
				  HashMap hmResult = (HashMap) itrMap.next();
				  int strQty = Integer.parseInt(GmCommonClass.parseNull((String) hmResult.get("QTY")));
				for(int i=1;i<=strQty;i++){
					alReturn.add(hmResult);
				  }
				}
				}
			  strJasperName="/GmLoanerDetailList.jasper"; 
		  } else {
		      log.debug("strCompanyId-ITALY==> "+strCompanyId);
			  if(strCountryCode.equals("it")){ 
				  strJasperName="/GmLoanerSetReConfigPrint.jasper"; 
			  }else{ 
				  strJasperName="/GmLoanerSetReConfigPrint.jasper"; 
			  }
			}
		}
		  String strTransCompId = GmCommonClass.parseNull((String)hmCONSIGNMENT.get("TRANS_COMPANY_ID"));
			strTransCompId = strTransCompId.equals("")? getGmDataStoreVO().getCmpid() : strTransCompId; 
			String strTransCompanyLocale = GmCommonClass.getCompanyLocale(strTransCompId);
		// Locale
	    GmResourceBundleBean gmResourceBundleBean =
	        GmCommonClass.getResourceBundleBean("properties.Paperwork", strTransCompanyLocale);
	     strHeader  =	GmCommonClass.parseNull((String)gmPaperworkResourceBundleBean.getProperty("LOANER."+strType));
	    strHeader = strHeader.equals("")? "Loaner": strHeader;
		  
		hmCONSIGNMENT.put("HEADER", strHeader);
		log.debug("hmCONSIGNMENT==" + hmCONSIGNMENT);
		gmJasperReport.setJasperReportName(strJasperName);
		gmJasperReport.setHmReportParameters(hmCONSIGNMENT);
		gmJasperReport.setReportDataList(alReturn);
		gmJasperReport.exportJasperToPdf(strPDFFileName);

	}

	/**
	 * printPaperWorkJMS - to generate PDF file
	 * 
	 * @param hmParam
	 */
	public void printPaperWorkJMS(HashMap hmParam) {
		GmWSUtil gmWSUtil = new GmWSUtil();
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmReconfigLoanerBean gmReconfigLoanerBean = new GmReconfigLoanerBean(getGmDataStoreVO());
		// HashMap hmJMSData = new HashMap();
		// to get the queue name
		String strLoanerQueue = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOANER_QUEUE"));
		String strLoanerMDBClass = GmCommonClass
				.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("RECONFIGURE_LOANER_CONSUMER_CLASS"));
		hmParam.put("SETID", GmCommonClass.parseNull((String) hmParam.get("SETID")));
		hmParam.put("CONSUMERCLASS", strLoanerMDBClass);
		hmParam.put("QUEUE_NAME", strLoanerQueue);
		log.debug("Message Sent Batch hmParam printPaperWorkJMS ---------------> " + hmParam);
		gmConsumerUtil.sendMessage(hmParam);			
}

	/**
	 * getPrinterName - This method used to get printer name
	 * 
	 * @param strUserId
	 * @return
	 */
	public String getPrinterName(String strUserId) {

		String strPrinterName = "";
		log.debug("strUserId" + strUserId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setFunctionString("get_printer_name", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		strPrinterName = gmDBManager.getString(1);
		log.debug("strPrinterName=====>>" + strPrinterName);
		gmDBManager.close();

		return strPrinterName;

	}
}
