package com.globus.operations.loaners.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author mahendrand
 *
 */
public class GmLoanerLotReprocessBean extends GmBean {

	/**
	 * Initializing log variable for debug
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName()); 
	
	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmLoanerLotReprocessBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
	}

	/**
	 * This Method is to validate the set Lot Track
	 * @param strSetId
	 * @return
	 * @throws AppError
	 */
	public String validateSetLotTrack(String strSetId) throws AppError {
		String lottrkfl = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setFunctionString("gm_pkg_op_loaner_lot_process.validate_set_lot_trk", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strSetId);
		gmDBManager.execute();
		lottrkfl = GmCommonClass.parseNull(gmDBManager.getString(1));
		log.debug("lottrkfl>validateSetLotTrack>>>"+lottrkfl);
		gmDBManager.close();
		return lottrkfl;
	}

	public String getConsignDetails(String strConsignId) throws AppError {
		HashMap hmResult = new HashMap();
		String strSetRptJSONString="";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_loaner_lot_process.gm_is_fch__lnset_detail", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strConsignId);
		gmDBManager.execute();
		
		strSetRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(2));
	    log.debug("strSetRptJSONString========= "+strSetRptJSONString);
	    gmDBManager.close();
	    
	    return strSetRptJSONString;
	  }
	
	public String getSetConsignDetails(String strConsignId, String strType) throws AppError {
		HashMap hmResult = new HashMap();
		String strSetRptJSONString="";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_loaner_lot_process.gm_ls_fch__setcsgdetails", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.setString(1, strConsignId);
		gmDBManager.setString(2, strType);
		gmDBManager.execute();
		
		strSetRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(3));
	    log.debug("strSetRptJSONString========= "+strSetRptJSONString);
	    gmDBManager.close();
	    
	    return strSetRptJSONString;
	  }
	
	/**
	   * function that calls a procedure to fetch all the open inhouse trans
	   * 
	   * @param strConsignId
	   * @return
	   * @throws AppError
	   */
	  public String loadLoanerOpenTrans(String strConsignId, String strhScrnFrom) throws AppError {
	    String strSetRptJSONString="";
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_loaner_lot_process.gm_fch_open_loaner_inhouse_trans", 3);
	    gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
	    gmDBManager.setString(1, strConsignId);
	    gmDBManager.setString(2, strhScrnFrom);
	    gmDBManager.execute();
	    strSetRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(3));
	    log.debug("strSetRptJSONString========= "+strSetRptJSONString);
	    gmDBManager.close();

	    return strSetRptJSONString;
	  }
	  
	  /**
	     * validateControlNoPart - This method is used to validate control number scanned/entered. Returns message with Part, Lot, txn warehouse, Lot's current warehouse and validation message
	     * @param String strPartNo
	     * @param String strControlNo
	     * @param strTxnType
	     * @return String
	     */
	    public HashMap validateControlNoPart(String strPartNo,String strControlNo, String strMsg) throws AppError {
	        
	        String strControlNum = "";
	        String strExpiryDt = "";
	        String strPartDesc = "";
	        HashMap hmResult = new HashMap();
	        GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	        gmDBManager.setPrepareString("gm_pkg_op_lot_validation.gm_validate_lot", 7);
	        gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
	        gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	        gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
	        gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
	        gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
	        gmDBManager.setString(1, strControlNo);
	        gmDBManager.setString(2, strPartNo);
	                    
	        gmDBManager.execute();
	        
	        strMsg = GmCommonClass.parseNull(gmDBManager.getString(3));// Getting the validation message
	        strPartNo = GmCommonClass.parseNull(gmDBManager.getString(4));// Getting the part number
	        strControlNum = GmCommonClass.parseNull(gmDBManager.getString(5));// Getting the control number
	        strExpiryDt = GmCommonClass.parseNull(gmDBManager.getString(6));// Getting the expiry date
	        strPartDesc = GmCommonClass.parseNull(gmDBManager.getString(7));// Getting the part description
	        gmDBManager.close();
	        
	        hmResult.put("ERRORMSG", strMsg);
	        hmResult.put("PARTNUM", strPartNo);
	        hmResult.put("CONTROLNUM", strControlNum);
	        hmResult.put("EXPIRYDT", strExpiryDt);
	        hmResult.put("PARTDESC", strPartDesc);
	        log.debug("hmResult=============== "+hmResult);
	        return hmResult;
	    }
	  
	   /**
	   * saveLoanerReturnTrans - This method will
	   * 
	   * @exception AppError
	   */
	  public String saveLoanerReturnTrans(String strConsignId,
		  HashMap hmParam, String strUserID) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
		GmReconfigLoanerBean gmReconfigLoanerBean = new GmReconfigLoanerBean(getGmDataStoreVO());
	    HashMap hmJMSParam = new HashMap();

		String strId = "";
		String strReplenishId = "";
		String strReturnTranIds = "";
		log.debug("hmParam: " + hmParam);
		String strInputStr = GmCommonClass.parseNull((String) hmParam.get("MISSSTR"));
		String strRetStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100045"));// move to
																						 // shelf
		String strQuarStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100047"));// move to
																						  // Quarantine
		String strRepStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100040"));// repln from
																						  // Shelf
		String strScrapStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100046"));// move to
																						   // Scrap
		String strBckStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100061"));// move to
																						  // Back Order
		String strBulkStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100041"));// repln from
																						   // Bulk
		String strIHRepStr = GmCommonClass.parseNull((String) hmParam.get("REPFROM100039"));// repln
																							// from In
																							// House
																							// Shelf
		String strIHMoveStr = GmCommonClass.parseNull((String) hmParam.get("MOVETO100048"));// Move to
																							// In House
																							// shelf 
		String strType = GmCommonClass.parseNull((String) hmParam.get("CTYPE"));
		String strLog = GmCommonClass.parseNull((String) hmParam.get("LOG"));
		gmDBManager.setPrepareString("GM_CS_SAV_LOANER_RETURN", 15);
		/*
		 * register out parameter and set input parameters
		 */

		log.debug("strType: " + strType);
		gmDBManager.registerOutParameter(14, java.sql.Types.CHAR);
		gmDBManager.registerOutParameter(15, java.sql.Types.CHAR);

		gmDBManager.setString(1, strConsignId);
		gmDBManager.setString(2, "");
		gmDBManager.setString(3, strInputStr);
		gmDBManager.setString(4, strRetStr);
		gmDBManager.setString(5, strQuarStr);
		gmDBManager.setString(6, strRepStr);
		gmDBManager.setString(7, strScrapStr);
		gmDBManager.setString(8, strBckStr);
		gmDBManager.setString(9, strBulkStr);
		gmDBManager.setString(10, strIHRepStr);
		gmDBManager.setString(11, strIHMoveStr);
		gmDBManager.setString(12, strType);
		gmDBManager.setString(13, strUserID);
		gmDBManager.execute();

		strId = GmCommonClass.parseNull(gmDBManager.getString(14));
		strReplenishId = GmCommonClass.parseNull(gmDBManager.getString(15));   
		strReturnTranIds = strId +" "+strReplenishId;
		log.debug("after execute " + strId);   
		log.debug("strLog " + strLog);
		if (!strLog.equals("") && !strId.equals("")) {
		  if (!strInputStr.equals("")) { // if missing
			log.debug("save process return comments " + strId);
			gmCom.saveLog(gmDBManager, strId, strLog, strUserID, "1246");
		  } else {
			log.debug("save accept return comments " + strId);
			gmCom.saveLog(gmDBManager, strId, strLog, strUserID, "1245");
		  }
		}
		gmDBManager.commit();
		
		hmJMSParam.put("USERID", strUserID);
		hmJMSParam.put("CONSID", strConsignId);
		hmJMSParam.put("COMPANYINFO", GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO")));
		//hmJMSParam.put("LOANTRANSID",strLoanTransId);
		hmJMSParam.put("STRLOG", strLog);
		hmJMSParam.put("SUBREPORT_DIR", GmCommonClass.parseNull((String) hmParam.get("SUBREPORT_DIR")));
		hmJMSParam.put("SETID", GmCommonClass.parseNull((String) hmParam.get("SETID")));
		hmJMSParam.put("LOANERREQID", GmCommonClass.parseNull((String) hmParam.get("LOANERREQID")));
		
		String strReconfigFl = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("RECONFIG_LOANER_FL", "RECONFIG_LOANER",getCompId()));
		 
		if(strReconfigFl.equals("Y")) {
			//Call JMS for to create PDF
		    gmReconfigLoanerBean.printPaperWorkJMS(hmJMSParam);
		}  
		
		return strId;
	  } // End of saveLoanerReturnTrans
	  
	  public void saveLoanerCharges(String strTransactionId, String strTransType,
			  String strInputString, String strUserId) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		log.debug("strTransactionId = " + strTransactionId);
		log.debug("strTransType = " + strTransType);
		log.debug("strInputString = " + strInputString);
		gmDBManager.setPrepareString("gm_pkg_op_loaner.gm_sav_additional_charges", 4);
		gmDBManager.setString(1, strTransactionId);
		gmDBManager.setString(2, strTransType);
		gmDBManager.setString(3, strInputString);
		gmDBManager.setString(4, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	  }
	  
	  public void saveReceivedLots(String strTransactionId, String strPartNo,String strControlNo,
			  String strQty, String strUserId, String strStatusFl) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
	  log.debug("strTransactionId = " + strTransactionId);
	  log.debug("strPartNo = " + strPartNo);
	  log.debug("strControlNo = " + strControlNo);
	  log.debug("strQty = " + strQty);
		gmDBManager.setPrepareString("gm_pkg_op_loaner_lot_process.gm_sav_received_lots", 6);
		gmDBManager.setString(1, strTransactionId);
		gmDBManager.setString(2, strPartNo);
		gmDBManager.setString(3, strControlNo);
		gmDBManager.setString(4, strQty);
		gmDBManager.setString(5, strUserId);
		gmDBManager.setString(6, strStatusFl);
		gmDBManager.execute();
		gmDBManager.commit();
	  }

	  
	  public void updateLoanerLotDetails(String strTransactionId, String strUserId) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
	  log.debug("strTransactionId = " + strTransactionId);  
		gmDBManager.setPrepareString("gm_pkg_op_loaner_lot_process.gm_sav_add_or_missing_lots", 2);
		gmDBManager.setString(1, strTransactionId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	  }
	  
	  
	  public String getPartDescriptionDetails(String strPartNum) throws AppError {
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  String strPartDesc = "";
		  log.debug(" strPartNum=========== --> " + strPartNum);
		    gmDBManager.setFunctionString("get_partdesc_by_company", 1);
		    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		    gmDBManager.setString(2, strPartNum);
		    gmDBManager.execute();
		    strPartDesc = GmCommonClass.parseNull(gmDBManager.getString(1));
		    gmDBManager.close();
		    log.debug(" strPartDesc --> " + strPartDesc);
		    return strPartDesc;
		  }
	  
	  /**
	     * getExistingReceivedDetails - This method is used to received lot details
	     * @param String strControlNum
	     * @return String
	     */
	  public String getExistingReceivedDetails(String strConsignId) throws AppError {
		  	String strJSONString="";
		  	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
		  
		  	gmDBManager.setPrepareString("gm_pkg_op_loaner_lot_process.gm_fch_received_cn_lots", 2);
		  	gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
			gmDBManager.setString(1, strConsignId);
			gmDBManager.execute();
			strJSONString = GmCommonClass.parseNull(gmDBManager.getString(2));// Getting the validation message
			log.debug("getExistingReceivedDetails=strJSONString====== " + strJSONString);  
			return strJSONString;
	  }

	/** PMT-52000 - Loaner process - additional changes(Priority,Void Y icon)
	 * updateRollbackFlag - this method used to update the roll back flag when save the scanned lot number
	 * @param strConsignmentId
	 * @param strUserId
	 * @throws AppError
	 */
	public void updateRollbackFlag(String strConsignmentId, String strUserId)throws AppError{
		// TODO Auto-generated method stub
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO()); 
		 
	  	gmDBManager.setPrepareString("gm_pkg_op_loaner_lot_process.gm_update_rollback_flag", 2);
	  	
		gmDBManager.setString(1, strConsignmentId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

	}
}