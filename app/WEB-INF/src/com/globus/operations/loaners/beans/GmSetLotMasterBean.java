package com.globus.operations.loaners.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSetLotMasterBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSetLotMasterBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }


  /**
   * This method is used to fetch control number for loaner parts to Japan country
   * 
   * @param hmParam
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList fetchSetPartLotQtyDetail(HashMap hmParam) throws AppError {

    ArrayList alReturn = new ArrayList();
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ln_set_lot_master_rpt.gm_fch_set_part_lot_qty", 3);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();


    return alReturn;
  }

  /**
   * This method is used to fetch the values for loaner set part details
   * 
   * @param hmParam
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList getSetConsignLotDetails(String strConsignID) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ln_set_lot_master_rpt.Gm_fch_set_part_cnum_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignID);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();


    return alReturn;
  }

  /**
   * This method is validate the lot number for the part
   * 
   * @param String
   * @return String
   * @throws com.globus.common.beans.AppError
   */
  public String validateControlNumber(String strPartNum, String strCntrlNum) throws AppError {

    String strMsg = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_cntrl_num_exists", 3);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strCntrlNum);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();

    strMsg = GmCommonClass.parseNull(gmDBManager.getString(3));

    gmDBManager.close();

    return strMsg;
  }
}
