package com.globus.operations.loaners.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.loaners.forms.GmFieldSalesLoanersForm;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmFieldSalesLoanersAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    // TODO Auto-generated method stub
    instantiate(request, response);
    ArrayList alResult = new ArrayList();
    ArrayList alReturn = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmApplnParam = new HashMap();

    GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmLoanerBean gmLoanerBean = new GmLoanerBean();
    HashMap hmSalesFilters = new HashMap();
    String strOpt = "";
    String hinputString = "";
    String strCondition = "";

    GmFieldSalesLoanersForm gmFieldSalesLoanersForm = (GmFieldSalesLoanersForm) form;
    gmFieldSalesLoanersForm.loadSessionParameters(request);
    strOpt = GmCommonClass.parseNull(gmFieldSalesLoanersForm.getStrOpt());
    String strApplnDateFmt = gmFieldSalesLoanersForm.getApplnDateFmt();
    strApplnDateFmt = strApplnDateFmt.equals("") ? "MM/DD/YYYY" : strApplnDateFmt;
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);

    gmCommonBean.loadActiveLoanerSets();// -- to load the set list
    gmFieldSalesLoanersForm.setAlSets(gmCommonBean.getActiveLoanerSets());

    alReturn = GmCommonClass.getCodeList("LOANS", "2005"); // to load the status
    gmFieldSalesLoanersForm.setAlStatus(alReturn);

    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmFieldSalesLoanersForm));

    strCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);

    hmSalesFilters = gmCommonBean.getSalesFilterLists(strCondition);
    strCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
    hmParam.put("AccessFilter", strCondition);
    alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmSalesFilters.get("DIST"));
 
    gmFieldSalesLoanersForm.setAlFieldSales(alReturn);


    if (strOpt.equals("")) {
      String[] strArr = {"20"};
      gmFieldSalesLoanersForm.setSelectedStatus(strArr);
      strOpt = "load";
    }

    if (strOpt.equals("load") || strOpt.equals("reload")) {
      hinputString = GmCommonClass.createInputString(gmFieldSalesLoanersForm.getSelectedDists());
      hmParam.put("DISTIDS", hinputString);
      hinputString = GmCommonClass.createInputString(gmFieldSalesLoanersForm.getSelectedSets());
      hmParam.put("SETID", hinputString);
      hinputString = GmCommonClass.createInputString(gmFieldSalesLoanersForm.getSelectedStatus());
      hmParam.put("STATUS", hinputString);

    
      alReturn = gmLoanerBean.fetchFieldSalesLoanerDetail(hmParam);
    
      hmApplnParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmApplnParam.put("VMFILEPATH", "properties.labels.operations.loaners.GmFieldSalesLoaners");
      hmApplnParam.put("PATH", "operations/loaners/templates");
      hmApplnParam.put("FILENAME", "GmFieldSalesLoaners.vm");
      String strXmlData = generateOutPut(alReturn, hmApplnParam);
      gmFieldSalesLoanersForm.setStrXmlData(strXmlData);
    }

    request.setAttribute("hmSalesFilters", hmSalesFilters);
    return mapping.findForward("success");
  }

  private String generateOutPut(ArrayList alGridData, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));

    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir((String) hmParam.get("PATH"));
    templateUtil.setTemplateName((String) hmParam.get("FILENAME"));
    return templateUtil.generateOutput();
  }
}
