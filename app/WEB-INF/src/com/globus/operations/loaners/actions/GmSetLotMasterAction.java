package com.globus.operations.loaners.actions;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.loaners.beans.GmSetLotMasterBean;
import com.globus.operations.loaners.forms.GmSetLotMasterForm;


public class GmSetLotMasterAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * 
   * This method is used to fetch control number for loaner parts to Japan country
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadSetPartLotDtl(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmSetLotMasterForm gmSetLotMasterForm = (GmSetLotMasterForm) form;
    GmSetLotMasterBean gmSetLotMasterBean = new GmSetLotMasterBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();

    HashMap hmParam = new HashMap();
    String strJSON = "";
    String strTxnId = GmCommonClass.parseNull(gmSetLotMasterForm.getTxnId());
    String strPartNum = GmCommonClass.parseNull(gmSetLotMasterForm.getPartNum());
    hmParam.put("TXNID", strTxnId);
    hmParam.put("PARTNUM", strPartNum);

    strJSON = gmWSUtil.parseArrayListToJson(gmSetLotMasterBean.fetchSetPartLotQtyDetail(hmParam));
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }

    return actionMapping.findForward("success");
  }

  /**
   * 
   * This method is used to validate the control number for parts
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward validateLotNumber(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmSetLotMasterForm gmSetLotMasterForm = (GmSetLotMasterForm) form;
    GmSetLotMasterBean gmSetLotMasterBean = new GmSetLotMasterBean(getGmDataStoreVO());

    String strJSON = "";
    String strCtrlNum = GmCommonClass.parseNull(gmSetLotMasterForm.getCtrlNum());
    String strPartNum = GmCommonClass.parseNull(gmSetLotMasterForm.getPartNum());

    strJSON = gmSetLotMasterBean.validateControlNumber(strPartNum, strCtrlNum);
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }

    return actionMapping.findForward("success");
  }

}
