package com.globus.operations.loaners.actions;

import java.io.File;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmFileReader;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.loaners.beans.GmLoanerLotReprocessBean;
import com.globus.operations.loaners.forms.GmFieldSalesLoanersForm;
import com.globus.operations.loaners.forms.GmLoanerLotReprocessForm;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmLoanerLotReprocessAction extends GmDispatchAction {
	/**
	 * Initializing log variable for debug
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName()); 

	
	public ActionForward loadPartDescription(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		GmLoanerLotReprocessBean gmLoanerLotReproBean = new GmLoanerLotReprocessBean(
				getGmDataStoreVO());

		String strPartDesc = "";
		 String strPartNum = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getPartNum());
		log.debug("strPartNum================ " + strPartNum);
		if (!strPartNum.equals("")) {
			strPartDesc = gmLoanerLotReproBean
					.getPartDescriptionDetails(strPartNum);
			log.debug("strPartDesc================ " + strPartDesc);
		}
		if (!strPartDesc.equals("")) {
			response.setContentType("text/plain");
			PrintWriter pw = response.getWriter();
			pw.write(strPartDesc);
			pw.flush();
		}
		return null;
		
	}
	
  public ActionForward loadLoanerHeader(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError,Exception {
	  
    // TODO Auto-generated method stub
    instantiate(request, response);
    GmLoanerLotReprocessBean gmLoanerLotReproBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
    
    HashMap hmCONSIGNMENT = new HashMap();
    
    String strSetName="";
    String strEtchId="";
    String strSetType="";
    Date dtLDate = null; 
	Date dtEDate = null; 
    
    log.debug("entry in loadLoanerHeader");
    String strConsignId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
    log.debug("strConsignId========== "+strConsignId);
   
    String strSetJsonString =
	          GmCommonClass.parseNull(gmLoanerLotReproBean.getConsignDetails(strConsignId));
	   
	        response.setContentType("text/json");
	        PrintWriter pw = response.getWriter();
	        if (!strSetJsonString.equals("")) {
	        pw.write(strSetJsonString);
	        }
	        pw.flush();
	return null;  
  }   
  
  public ActionForward getSetConsignDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws AppError,Exception {
		  
	    // TODO Auto-generated method stub
	    instantiate(request, response);
	    GmLoanerLotReprocessBean gmLoanerLotReproBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
	    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
	    
	    HashMap hmCONSIGNMENT = new HashMap();
	    
	    String strSetName="";
	    String strEtchId="";
	    String strSetType="";
	    Date dtLDate = null; 
		Date dtEDate = null; 
	    
	    log.debug("entry in getSetConsignDetails");
	    String strConsignId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
	    log.debug("strConsignId========== "+strConsignId);
	   
	    String strSetJsonString =
		          GmCommonClass.parseNull(gmLoanerLotReproBean.getSetConsignDetails(strConsignId,"STOCK"));
		        response.setContentType("text/json");
		        PrintWriter pw = response.getWriter();
		        if (!strSetJsonString.equals("")) {
		        pw.write(strSetJsonString);
		        }
		        pw.flush();
		return null;  
	  }    
	  
	  public ActionForward loadLoanerOpenTransactions(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws AppError,Exception {
			  
		    // TODO Auto-generated method stub
		    instantiate(request, response);
		    GmLoanerLotReprocessBean gmLoanerLotReproBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
		    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		    
		    HashMap hmCONSIGNMENT = new HashMap();
		    
		    String strSetName="";
		    String strEtchId="";
		    String strSetType="";
		    Date dtLDate = null; 
			Date dtEDate = null; 
		    
		    log.debug("entry in loadLoanerOpenTransactions");
		    String strConsignId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
		    log.debug("strConsignId========== "+strConsignId);
		   
		    String strSetJsonString =
			          GmCommonClass.parseNull(gmLoanerLotReproBean.loadLoanerOpenTrans(strConsignId,"STOCK"));
			   
			        response.setContentType("text/json");
			        PrintWriter pw = response.getWriter();
			        if (!strSetJsonString.equals("")) {
			        pw.write(strSetJsonString);
			        }
			        pw.flush();
			return null;  
		  } 
	      
	  public ActionForward controlNoValidation(ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws AppError,Exception {
			  
		    // TODO Auto-generated method stub
		    instantiate(request, response);
		    GmLoanerLotReprocessBean gmLoanerLotReproBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
		    String strJSON = "";
		    GmWSUtil gmWSUtil = new GmWSUtil();
		    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;// To check whether the control number is valid
		    String strPartNum = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getPartNum());
			String strControlNo = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getCtrlNum());
			String strMsg = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getLog());
			log.debug("strMsg========== "+strMsg);
			strJSON =
					gmWSUtil.parseHashMapToJson(gmLoanerLotReproBean.validateControlNoPart(strPartNum,
	              strControlNo, strMsg));

	      response.setContentType("text/plain");
	      PrintWriter pw = response.getWriter();
	      pw.write(strJSON);
	      pw.flush();
	      return null;
	    }
		  
		  public ActionForward loadBioHazardDropdown(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			      HttpServletResponse response) throws AppError,Exception {
				  
			    // TODO Auto-generated method stub
			    instantiate(request, response);
			    GmLoanerLotReprocessBean gmLoanerLotReproBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
			    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
			    ArrayList alReturn = new ArrayList();
			    
			    log.debug("entry in loadBioHazardDropdown");
			    alReturn = GmCommonClass.getCodeList("BHGRP");
			    gmLoanerLotReprocessForm.setAlReturn(alReturn);
			    GmWSUtil gmWSUtil = new GmWSUtil();
		        String strJSON = gmWSUtil.parseArrayListToJson(alReturn);
		        
			    response.setContentType("text/plain");
			      PrintWriter pw = response.getWriter();
			      pw.write(strJSON);
			      pw.flush();
			      return null;
			      
			  }
		  
		  public ActionForward loadDamageDropdown(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			      HttpServletResponse response) throws AppError,Exception {
				  
			    // TODO Auto-generated method stub
			    instantiate(request, response);
			    GmLoanerLotReprocessBean gmLoanerLotReproBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
			    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
			    ArrayList alReturn = new ArrayList();
			    
			    log.debug("entry in loadDamageDropdown");
			    alReturn = GmCommonClass.getCodeList("DMGRP");
			    gmLoanerLotReprocessForm.setAlReturn(alReturn);
			    GmWSUtil gmWSUtil = new GmWSUtil();
		        String strJSON = gmWSUtil.parseArrayListToJson(alReturn);
		        
			    response.setContentType("text/plain");
			      PrintWriter pw = response.getWriter();
			      pw.write(strJSON);
			      pw.flush();
			      return null;

			  }
		  
	      
	public ActionForward saveReceivedLots(ActionMapping actionMapping, ActionForm form,
		  HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		GmLoanerLotReprocessBean gmLoanerLotReprocessBean = new GmLoanerLotReprocessBean(getGmDataStoreVO());

		String strConsignmentId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
		String strPartNum = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getPartNum());
		String strControlNo = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getCtrlNum());
		String strQty = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getQty());
		String strStatusFl = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getStrStatusFl());
		log.debug("strStatusFl===================== "+strStatusFl);
		String strUserId = (String)request.getSession().getAttribute("strSessUserId");  
		
		gmLoanerLotReprocessBean.saveReceivedLots(strConsignmentId, strPartNum, strControlNo, strQty, strUserId, strStatusFl);
		  
		response.setContentType("text/plain");
		PrintWriter pw = response.getWriter();
		pw.write("Y");
		pw.flush();
		return null;
		}

	  /**
	   * 
	   * This method is used to create in-house,missing and replenishment transactions  from lot loaner process check
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   */
	  public ActionForward saveLoanerReturnTrans(ActionMapping actionMapping, ActionForm form,
		  HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		HashMap hmParam = new HashMap();
		GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		GmLoanerLotReprocessBean gmLoanerLotReprocessBean = new GmLoanerLotReprocessBean(getGmDataStoreVO());

		String strConsignmentId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
		String strTransType = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getTransType());
		String strTransName = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getTransName());
		String strInputStr = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getInputString());
		String strLog = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getLog());
		String strUserId = (String)request.getSession().getAttribute("strSessUserId");  
		String strSetID = (String)request.getSession().getAttribute("hSetId");
		String strCompInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		String strMissStr = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getMISSSTR());
		String strRepFrom10040 = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getREPFROM100040());
		String strRepFrom10041 = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getREPFROM100041());
		String strRepFrom10061 = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getREPFROM100061());
		String strRepFrom10045 = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getMOVETO100045());
		String strRepFrom10046 = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getMOVETO100046());
		String strRepFrom10047 = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getMOVETO100047());
		
		log.debug("strMissStr"+strMissStr);
		log.debug("strRepFrom10040"+strRepFrom10040);
		log.debug("strRepFrom10041"+strRepFrom10041);
		log.debug("strRepFrom10061"+strRepFrom10061);
		log.debug("strRepFrom10045"+strRepFrom10045);
		log.debug("strRepFrom10046"+strRepFrom10046);
		log.debug("strRepFrom10047"+strRepFrom10047);
		hmParam.put("CTYPE", strTransType); //4127 - PRODUCT LOANER
		hmParam.put("LOG", strLog);
		//hmParam.put(strTransName, strInputStr); //
		hmParam.put("MISSSTR", strMissStr);
		hmParam.put("REPFROM100040", strRepFrom10040);
		hmParam.put("REPFROM100041", strRepFrom10041);
		hmParam.put("REPFROM100061", strRepFrom10061);
		hmParam.put("MOVETO100045", strRepFrom10045);
		hmParam.put("MOVETO100046", strRepFrom10046);
		hmParam.put("MOVETO100047", strRepFrom10047);
		hmParam.put("SETID", strSetID);
		hmParam.put("COMPANYINFO", strCompInfo);
		
		String strTransIds = GmCommonClass.parseNull(gmLoanerLotReprocessBean.saveLoanerReturnTrans(strConsignmentId, hmParam, strUserId));
		//String strTransIds = "Test";
		log.debug("strTransIds"+strTransIds);
		  
		 // if (!strTransIds.equals("")){
	        	response.setContentType("text/plain");
	        	PrintWriter pw = response.getWriter();
			    pw.write(strTransIds);
			    pw.flush();
			    //return null;
	        //}
	
		return null;
	  }
	  /**
	   * 
	   * This method is used to save charges from lot loaner process check
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   */
	  public ActionForward saveLoanerCharges(ActionMapping actionMapping, ActionForm form,
		  HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		GmLoanerLotReprocessBean gmLoanerLotReprocessBean = new GmLoanerLotReprocessBean(getGmDataStoreVO());

		String strConsignmentId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
		String strTransType = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getTransType());
		String strInputString = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getInputString());
		String strUserId = (String)request.getSession().getAttribute("strSessUserId");  
	    
		gmLoanerLotReprocessBean.saveLoanerCharges(strConsignmentId, strTransType, strInputString, strUserId);
		  
		response.setContentType("text/plain");
		PrintWriter pw = response.getWriter();
		pw.write("Y");
		pw.flush();
		return null;
	  }
	  
	  /**
	   * 
	   * This method is used to update missing lots from lot loaner process check
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   */
	  public ActionForward initiateLoanerLotJMS(ActionMapping actionMapping, ActionForm form,
		  HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		HashMap hmParam = new HashMap();
		GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		GmLoanerLotReprocessBean gmLoanerLotReprocessBean = new GmLoanerLotReprocessBean(getGmDataStoreVO());
		GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
		
		String strConsignmentId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());  
		String strUserId = (String)request.getSession().getAttribute("strSessUserId");  
		//To call from web service to update missing lots Using JMS

	  String strLoanerQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOANER_LOT_PROCESS_QUEUE"));
	  String strLoanerConsumerClass =   GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("LOANER_LOT_PROCESS_CONSUMER_CLASS"));
	  String strCompInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
	  hmParam.put("QUEUE_NAME", strLoanerQueueName);
	  hmParam.put("CONSIGNMENTID", strConsignmentId);
	  hmParam.put("COMPANYINFO", strCompInfo);
	  hmParam.put("USERID", strUserId);  
	  hmParam.put("CONSUMERCLASS", strLoanerConsumerClass);

	  log.debug("hmParam============== "+hmParam);

	  gmConsumerUtil.sendMessage(hmParam);
	  
	  response.setContentType("text/plain");
	  PrintWriter pw = response.getWriter();
	  pw.write("Y");
	  pw.flush();
	  return null;
	}	      

	  public ActionForward getExistingReceivedDetails (ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws AppError,Exception {
			  
		    // TODO Auto-generated method stub
		    instantiate(request, response);
		    GmLoanerLotReprocessBean gmLoanerLotReproBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
		    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		    
		    HashMap hmCONSIGNMENT = new HashMap();
		    
		    log.debug("entry in getExistingReceivedDetails");
		    String strConsignId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
		   
		    String strJsonString =
			          GmCommonClass.parseNull(gmLoanerLotReproBean.getExistingReceivedDetails(strConsignId));
			        response.setContentType("text/json");
			        PrintWriter pw = response.getWriter();
			        if (!strJsonString.equals("")) {
			        pw.write(strJsonString);
			        }
			        pw.flush();
			return null;  
	}

	  /** PMT-52000 - Loaner process - additional changes(Priority,Void Y icon)
	   * updateRollbackFlag - This method used to update the roll back flag for consignment id
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward updateRollbackFlag (ActionMapping mapping, ActionForm form, HttpServletRequest request,
		      HttpServletResponse response) throws AppError,Exception {
		  // TODO Auto-generated method stub
		    instantiate(request, response);
		    GmLoanerLotReprocessBean gmLoanerLotReprocessBean= new GmLoanerLotReprocessBean(getGmDataStoreVO());
		    GmLoanerLotReprocessForm gmLoanerLotReprocessForm = (GmLoanerLotReprocessForm) form;
		    
		    String strConsignmentId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getConsignmentId());
		    String strUserId = GmCommonClass.parseNull(gmLoanerLotReprocessForm.getUserId());
		    
		    gmLoanerLotReprocessBean.updateRollbackFlag (strConsignmentId, strUserId);
		  
		    response.setContentType("text/plain");
			PrintWriter pw = response.getWriter();
			pw.write("Y");
			pw.flush();
			return null;

	  }
	  
}
