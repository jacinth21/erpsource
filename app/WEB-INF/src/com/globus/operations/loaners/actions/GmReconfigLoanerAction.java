package com.globus.operations.loaners.actions;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.standard.MediaName;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmFileReader;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.loaners.beans.GmReconfigLoanerBean;
import com.globus.operations.loaners.beans.GmSetLotMasterBean;
import com.globus.operations.loaners.forms.GmReconfigLoanerForm;
import com.globus.operations.loaners.forms.GmSetLotMasterForm;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmReconfigLoanerAction extends GmDispatchAction{
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	 
		
		  public ActionForward loadReconfgPrintDtl(ActionMapping actionMapping,
		  ActionForm form, HttpServletRequest request, HttpServletResponse response)
		  throws AppError, Exception { 
			  
			  instantiate(request, response);
			  
		  GmReconfigLoanerForm gmReconfigLoanerForm = (GmReconfigLoanerForm) form;
		  GmReconfigLoanerBean gmReconfigLoanerBean = new GmReconfigLoanerBean(getGmDataStoreVO());
		  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		  GmConsumerUtil gmConsumerUtil = new GmConsumerUtil(); 
		  gmReconfigLoanerForm.loadSessionParameters(request);
		  //String strUserId = GmCommonClass.parseNull(gmReconfigLoanerForm.getUserId()); 
           GmWSUtil gmWSUtil = new GmWSUtil();
		  
		  HashMap hmParam = new HashMap(); 
			hmParam = GmCommonClass.getHashMapFromForm(gmReconfigLoanerForm);
		  log.debug("hmParam============ "+hmParam);
		  String strUserId = GmCommonClass.parseNull(gmReconfigLoanerForm.getUserId()); 
		  String strJSON = ""; 
		  String strConsignId = GmCommonClass.parseNull(gmReconfigLoanerForm.getStrConsignId()); 
		  String strCompanyId = getGmDataStoreVO().getCmpid();
		  String strSetID = GmCommonClass.parseNull(gmReconfigLoanerForm.getStrSetId());
		  // to get the queue name 
		  String strLoanerQueue = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOANER_QUEUE")); 
		  String strLoanerMDBClass =GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("RECONFIGURE_LOANER_CONSUMER_CLASS")); 
		  String strLog =GmCommonClass.parseNull(request.getParameter("Txt_LogReason")); 
		  String strCompInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		  String strReconfigFl = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("RECONFIG_LOANER_FL", "RECONFIG_LOANER",strCompanyId));
		  
		
        log.debug("strReconfigFl============ "+strReconfigFl);
		  hmParam.put("CONSID", strConsignId);
		  hmParam.put("COMPANYINFO",strCompInfo); // to set the JMS class name and queue name
		  hmParam.put("CONSUMERCLASS", strLoanerMDBClass); 
		  hmParam.put("QUEUE_NAME",strLoanerQueue); 
		  hmParam.put("USERID",strUserId);
		  hmParam.put("SETID",strSetID); 
		  hmParam.put("SUBREPORT_DIR" ,request.getSession().getServletContext()
                  .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
		  
		  String strSubRptDir = GmCommonClass.parseNull((String) hmParam.get("SUBREPORT_DIR"));
		   String strUserName = gmCommonBean.getUserName(strUserId);
		  log.debug("Message Sent Batch hmParam---------------> " + hmParam); 
		  gmConsumerUtil.sendMessage(hmParam);
		 
		  response.setContentType("text/plain");
		  PrintWriter pw = response.getWriter();
		  pw.write("Y"); pw.flush(); 
		  return null; 
		 }
		 
	 public ActionForward printReconfgPrintDtl(ActionMapping actionMapping, ActionForm form,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		    instantiate(request, response);
		    GmReconfigLoanerForm gmReconfigLoanerForm = (GmReconfigLoanerForm) form;
		    GmWSUtil gmWSUtil = new GmWSUtil();
			  GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
			  GmReconfigLoanerBean gmReconfigLoanerBean = new GmReconfigLoanerBean(getGmDataStoreVO());
		    HashMap hmParam = new HashMap();
		    String strPDFFileName = "";
		    gmReconfigLoanerForm.loadSessionParameters(request);
			hmParam = GmCommonClass.getHashMapFromForm(gmReconfigLoanerForm);
		    String strUserId = GmCommonClass.parseNull(gmReconfigLoanerForm.getUserId()); 
		    String strConsignId = GmCommonClass.parseNull(gmReconfigLoanerForm.getStrConsignId());
		    String strSetId = GmCommonClass.parseNull(gmReconfigLoanerForm.getStrSetId());
		    String strPrintOpt = GmCommonClass.parseNull(gmReconfigLoanerForm.getStrPrintOpt());
		    String strCompInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
		    String strLoanerQueue = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOANER_QUEUE"));
	        String strLoanerMDBClass =
	      		  GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("LOANER_PRINT_CONSUMER_CLASS")); 
	        String strCompanyId = getGmDataStoreVO().getCmpid();
	        hmParam.put("PRINTOPT",strPrintOpt);
	        hmParam.put("CONSID",strConsignId);
	        hmParam.put("SETID",strSetId);
	        hmParam.put("COMPANYINFO",strCompInfo);
	        hmParam.put("CONSUMERCLASS", strLoanerMDBClass);
	        hmParam.put("QUEUE_NAME", strLoanerQueue);
	        hmParam.put("COMPANYID", strCompanyId); 
	        hmParam.put("USERID", strUserId); 
	        hmParam.put("SUBREPORT_DIR" ,request.getSession().getServletContext()
                    .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
	        log.debug(" hmParam---------------> " + hmParam);
	        log.debug("strUserId---------------> " + strUserId);
	        gmConsumerUtil.sendMessage(hmParam);
	       
	          response.setContentType("text/plain");
	    	  PrintWriter pw = response.getWriter();
	    	  pw.write("Y");
	    	  pw.flush();
	    	  return null;
		  }
	 
	
	 
}
