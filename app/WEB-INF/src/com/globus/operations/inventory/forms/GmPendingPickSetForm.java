package com.globus.operations.inventory.forms;

import com.globus.common.forms.GmCommonForm;

public class GmPendingPickSetForm extends GmCommonForm{
 
	private String accessFlag ="";
	private String gridXmlData = "";
	private String strInputString = "";
	private String locationType = "";
	private String pickLocationName = "";
	private String pickLocationID = "";
	private String pType = "";
	private String txnType = "";
	private String operationVal = "";
	private String buttonName = "Close";
	private String wareHouseID = "";
	private String tagId = "";
	
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	/**
	 * @return the wareHouseID
	 */
	public String getWareHouseID() {
		return wareHouseID;
	}
	/**
	 * @param wareHouseID the wareHouseID to set
	 */
	public void setWareHouseID(String wareHouseID) {
		this.wareHouseID = wareHouseID;
	}
	/**
	 * @return the accessFlag
	 */
	public String getAccessFlag() {
		return accessFlag;
	}
	/**
	 * @param accessFlag the accessFlag to set
	 */
	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the strInputString
	 */
	public String getStrInputString() {
		return strInputString;
	}
	/**
	 * @param strInputString the strInputString to set
	 */
	public void setStrInputString(String strInputString) {
		this.strInputString = strInputString;
	}
	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return locationType;
	}
	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	/**
	 * @return the pickLocationName
	 */
	public String getPickLocationName() {
		return pickLocationName;
	}
	/**
	 * @param pickLocationName the pickLocationName to set
	 */
	public void setPickLocationName(String pickLocationName) {
		this.pickLocationName = pickLocationName;
	}
	/**
	 * @return the pickLocationID
	 */
	public String getPickLocationID() {
		return pickLocationID;
	}
	/**
	 * @param pickLocationID the pickLocationID to set
	 */
	public void setPickLocationID(String pickLocationID) {
		this.pickLocationID = pickLocationID;
	}
	/**
	 * @return the pType
	 */
	public String getpType() {
		return pType;
	}
	/**
	 * @param pType the pType to set
	 */
	public void setpType(String pType) {
		this.pType = pType;
	}
	/**
	 * @return the txnType
	 */
	public String getTxnType() {
		return txnType;
	}
	/**
	 * @param txnType the txnType to set
	 */
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	/**
	 * @return the operationVal
	 */
	public String getOperationVal() {
		return operationVal;
	}
	/**
	 * @param operationVal the operationVal to set
	 */
	public void setOperationVal(String operationVal) {
		this.operationVal = operationVal;
	}
	/**
	 * @return the buttonName
	 */
	public String getButtonName() {
		return buttonName;
	}
	/**
	 * @param buttonName the buttonName to set
	 */
	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}
	
	
}