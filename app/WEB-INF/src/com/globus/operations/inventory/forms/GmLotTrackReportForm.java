/**
 * @author arajan
 */

package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmLotTrackReportForm extends GmCommonForm { 

  private String partNum = "";
  private String ctrlNum = "";
  private String donorNum = "";
  private String warehouse = "";
  private String expDate = "";
  private String expDateRange = "";
  private String gridXmlData = "";
  private String internationalUse = "";
  private String transId = "";
  private String inputString = "";
  private String releaseQty = "";
  private String shelfQty = "";
  private String refId = "";
  private String lotType = "";
  private String consigneeNm = "";
  private String fieldSalesNm = "";
  private String accountNm = "";

  private ArrayList alExpDateRange = new ArrayList();
  ArrayList alWareHouse = new ArrayList();
  ArrayList alStatus = new ArrayList();
  ArrayList alFieldSales = new ArrayList();
  ArrayList alAccounts = new ArrayList();

  private String shippedFromDate = "";
  private String shippedToDate = "";
  
  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the ctrlNum
   */
  public String getCtrlNum() {
    return ctrlNum;
  }

  /**
   * @param ctrlNum the ctrlNum to set
   */
  public void setCtrlNum(String ctrlNum) {
    this.ctrlNum = ctrlNum;
  }

  /**
   * @return the donorNum
   */
  public String getDonorNum() {
    return donorNum;
  }

  /**
   * @param donorNum the donorNum to set
   */
  public void setDonorNum(String donorNum) {
    this.donorNum = donorNum;
  }

  /**
   * @return the warehouse
   */
  public String getWarehouse() {
    return warehouse;
  }

  /**
   * @param warehouse the warehouse to set
   */
  public void setWarehouse(String warehouse) {
    this.warehouse = warehouse;
  }

  /**
   * @return the expDate
   */
  public String getExpDate() {
    return expDate;
  }

  /**
   * @param expDate the expDate to set
   */
  public void setExpDate(String expDate) {
    this.expDate = expDate;
  }

  /**
   * @return the expDateRange
   */
  public String getExpDateRange() {
    return expDateRange;
  }

  /**
   * @param expDateRange the expDateRange to set
   */
  public void setExpDateRange(String expDateRange) {
    this.expDateRange = expDateRange;
  }

  /**
   * @return the alExpDateRange
   */
  public ArrayList getAlExpDateRange() {
    return alExpDateRange;
  }

  /**
   * @param alExpDateRange the alExpDateRange to set
   */
  public void setAlExpDateRange(ArrayList alExpDateRange) {
    this.alExpDateRange = alExpDateRange;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the alWareHouse
   */
  public ArrayList getAlWareHouse() {
    return alWareHouse;
  }

  /**
   * @param alWareHouse the alWareHouse to set
   */
  public void setAlWareHouse(ArrayList alWareHouse) {
    this.alWareHouse = alWareHouse;
  }

  /**
   * @return the internationalUse
   */
  public String getInternationalUse() {
    return internationalUse;
  }

  /**
   * @param internationalUse the internationalUse to set
   */
  public void setInternationalUse(String internationalUse) {
    this.internationalUse = internationalUse;
  }

  /**
   * @return the transId
   */
  public String getTransId() {
    return transId;
  }

  /**
   * @param transId the transId to set
   */
  public void setTransId(String transId) {
    this.transId = transId;
  }

  /**
   * @return the inputString
   */
  public String getInputString() {
    return inputString;
  }

  /**
   * @param inputString the inputString to set
   */
  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  /**
   * @return the releaseQty
   */
  public String getReleaseQty() {
    return releaseQty;
  }

  /**
   * @param releaseQty the releaseQty to set
   */
  public void setReleaseQty(String releaseQty) {
    this.releaseQty = releaseQty;
  }

  /**
   * @return the shelfQty
   */
  public String getShelfQty() {
    return shelfQty;
  }

  /**
   * @param shelfQty the shelfQty to set
   */
  public void setShelfQty(String shelfQty) {
    this.shelfQty = shelfQty;
  }

  /**
   * @return the refId
   */
  public String getRefId() {
    return refId;
  }

  /**
   * @param refId the refId to set
   */
  public void setRefId(String refId) {
    this.refId = refId;
  }

  /**
   * @return the lotType
   */
  public String getLotType() {
    return lotType;
  }

  /**
   * @param lotType the lotType to set
   */
  public void setLotType(String lotType) {
    this.lotType = lotType;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the consigneeNm
   */
  public String getConsigneeNm() {
    return consigneeNm;
  }

  /**
   * @param consigneeNm the consigneeNm to set
   */
  public void setConsigneeNm(String consigneeNm) {
    this.consigneeNm = consigneeNm;
  }

  /**
   * @return the alFieldSales
   */
  public ArrayList getAlFieldSales() {
    return alFieldSales;
  }

  /**
   * @param alFieldSales the alFieldSales to set
   */
  public void setAlFieldSales(ArrayList alFieldSales) {
    this.alFieldSales = alFieldSales;
  }

  /**
   * @return the alAccounts
   */
  public ArrayList getAlAccounts() {
    return alAccounts;
  }

  /**
   * @param alAccounts the alAccounts to set
   */
  public void setAlAccounts(ArrayList alAccounts) {
    this.alAccounts = alAccounts;
  }

  /**
   * @return the fieldSalesNm
   */
  public String getFieldSalesNm() {
    return fieldSalesNm;
  }

  /**
   * @param fieldSalesNm the fieldSalesNm to set
   */
  public void setFieldSalesNm(String fieldSalesNm) {
    this.fieldSalesNm = fieldSalesNm;
  }

  /**
   * @return the accountNm
   */
  public String getAccountNm() {
    return accountNm;
  }

  /**
   * @param accountNm the accountNm to set
   */
  public void setAccountNm(String accountNm) {
    this.accountNm = accountNm;
  }

/**
 * @return the shippedFromDate
 */
public String getShippedFromDate() {
	return shippedFromDate;
}

/**
 * @param shippedFromDate the shippedFromDate to set
 */
public void setShippedFromDate(String shippedFromDate) {
	this.shippedFromDate = shippedFromDate;
}

/**
 * @return the shippedToDate
 */
public String getShippedToDate() {
	return shippedToDate;
}

/**
 * @param shippedToDate the shippedToDate to set
 */
public void setShippedToDate(String shippedToDate) {
	this.shippedToDate = shippedToDate;
}

}
