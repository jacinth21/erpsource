

package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Vamsi Kiran Nagavarapu
 */
public class GmInvLocationForm extends GmCommonForm {
	private ArrayList alZones = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alInvLocTypes = new ArrayList();
	private ArrayList alParts = new ArrayList();
	private ArrayList alInvLocList = new ArrayList();
	private ArrayList alWareHouse = new ArrayList();
	private ArrayList alBuildingID = new ArrayList();
	private String hshowFilters= "";
	private String zoneNum = "";
	private String aisleNum = "";
	private String rowNum = "";
	private String columnNum = "";
	private String locationID = "";
	private String typeNum = "";
	private String userNum = "";
	private String statusNum = "";
	private String activeFlag ="";
	private String partNum = "";
	private String userFilter = "";
	private String shelf = "";
	private String accessFlag ="";
	
	private String strInputString = "";
	private String strVoidRowIds = "";
	private String gridXmlData = "";
	private String chkCurrQtyFlag = "";
	private String tagID = "";
	private String setID = "";
	private String ruleType ="";
	private String locMap = "";
	private String warehouseid = "";
	private String buildingID = "";
	private String buildid = "";
	private String bldFlag = "";
	
	private String hlocationId = "";
	private String newZone = "";
	private String activeFl = "";
	
	
	public String getHlocationId() {
		return hlocationId;
	}

	public void setHlocationId(String hlocationId) {
		this.hlocationId = hlocationId;
	}

	public String getChkCurrQtyFlag() {
		return chkCurrQtyFlag;
	}

	public void setChkCurrQtyFlag(String chkCurrQtyFlag) {
		this.chkCurrQtyFlag = chkCurrQtyFlag;
	}
	
	public String getAccessFlag() {
		return accessFlag;
	}

	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}

	/**
	 * @return the zoneNum
	 */
	public String getZoneNum() {
		return zoneNum;
	}

	/**
	 * @param zoneNum the zoneNum to set
	 */
	public void setZoneNum(String zoneNum) {
		this.zoneNum = zoneNum;
	}
	
	/**
	 * @return the aisleNum
	 */
	public String getAisleNum() {
		return aisleNum;
	}

	/**
	 * @param aisleNum the aisleNum to set
	 */
	public void setAisleNum(String aisleNum) {
		this.aisleNum = aisleNum;
	}

	/**
	 * @return the rowNum
	 */
	public String getRowNum() {
		return rowNum;
	}

	/**
	 * @param rowNum the rowNum to set
	 */
	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	/**
	 * @return the columnNum
	 */
	public String getColumnNum() {
		return columnNum;
	}

	/**
	 * @param columnNum the columnNum to set
	 */
	public void setColumnNum(String columnNum) {
		this.columnNum = columnNum;
	}

	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}

	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	/**
	 * @return the alInvLocTypes
	 */
	public ArrayList getAlInvLocTypes() {
		return alInvLocTypes;
	}

	/**
	 * @param alInvLocTypes the alInvLocTypes to set
	 */
	public void setAlInvLocTypes(ArrayList alInvLocTypes) {
		this.alInvLocTypes = alInvLocTypes;
	}

	/**
	 * @return the alParts
	 */
	public ArrayList getAlParts() {
		return alParts;
	}

	/**
	 * @param alParts the alParts to set
	 */
	public void setAlParts(ArrayList alParts) {
		this.alParts = alParts;
	}

	/**
	 * @return the typeNum
	 */
	public String getTypeNum() {
		return typeNum;
	}

	/**
	 * @param typeNum the typeNum to set
	 */
	public void setTypeNum(String typeNum) {
		this.typeNum = typeNum;
	}

	/**
	 * @return the userNum
	 */
	public String getUserNum() {
		return userNum;
	}

	/**
	 * @param userNum the userNum to set
	 */
	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}

	/**
	 * @return the statusNum
	 */
	public String getStatusNum() {
		return statusNum;
	}

	/**
	 * @param statusNum the statusNum to set
	 */
	public void setStatusNum(String statusNum) {
		this.statusNum = statusNum;
	}

	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}

	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	/**
	 * @return the strInputString
	 */
	public String getStrInputString() {
		return strInputString;
	}

	/**
	 * @param strInputString the strInputString to set
	 */
	public void setStrInputString(String strInputString) {
		this.strInputString = strInputString;
	}

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @return the locationID
	 */
	public String getLocationID() {
		return locationID;
	}

	/**
	 * @param locationID the locationID to set
	 */
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	/**
	 * @return the activeFlag
	 */
	public String getActiveFlag() {
		return activeFlag;
	}

	/**
	 * @param activeFlag the activeFlag to set
	 */
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	/**
	 * @return the alInvLocList
	 */
	public ArrayList getAlInvLocList() {
		return alInvLocList;
	}

	/**
	 * @param alInvLocList the alInvLocList to set
	 */
	public void setAlInvLocList(ArrayList alInvLocList) {
		this.alInvLocList = alInvLocList;
	}

	/**
	 * @return the alZones
	 */
	public ArrayList getAlZones() {
		return alZones;
	}

	/**
	 * @param alZones the alZones to set
	 */
	public void setAlZones(ArrayList alZones) {
		this.alZones = alZones;
	}

	/**
	 * @return the userFilter
	 */
	public String getUserFilter() {
		return userFilter;
	}

	/**
	 * @param userFilter the userFilter to set
	 */
	public void setUserFilter(String userFilter) {
		this.userFilter = userFilter;
	}

	/**
	 * @return the shelf
	 */
	public String getShelf() {
		return shelf;
	}

	/**
	 * @param shelf the shelf to set
	 */
	public void setShelf(String shelf) {
		this.shelf = shelf;
	}

	public String getStrVoidRowIds() {
		return strVoidRowIds;
	}

	public void setStrVoidRowIds(String strVoidRowIds) {
		this.strVoidRowIds = strVoidRowIds;
	}

	public String getTagID() {
		return tagID;
	}

	public void setTagID(String tagID) {
		this.tagID = tagID;
	}

	public String getSetID() {
		return setID;
	}

	public void setSetID(String setID) {
		this.setID = setID;
	}

	public String getRuleType() {
		return ruleType;
	}

	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}

	public String getLocMap() {
		return locMap;
	}

	public void setLocMap(String locMap) {
		this.locMap = locMap;
	}

	/**
	 * @param alWareHouse the alWareHouse to set
	 */
	public void setAlWareHouse(ArrayList alWareHouse) {
		this.alWareHouse = alWareHouse;
	}

	/**
	 * @return the alWareHouse
	 */
	public ArrayList getAlWareHouse() {
		return alWareHouse;
	}

	/**
	 * @param warehouseid the warehouseid to set
	 */
	public void setWarehouseid(String warehouseid) {
		this.warehouseid = warehouseid;
	}

	/**
	 * @return the warehouseid
	 */
	public String getWarehouseid() {
		return warehouseid;
	}

	/**
	 * @return the hshowFilters
	 */
	public String getHshowFilters() {
		return hshowFilters;
	}

	/**
	 * @param hshowFilters the hshowFilters to set
	 */
	public void setHshowFilters(String hshowFilters) {
		this.hshowFilters = hshowFilters;
	}

	public ArrayList getAlBuildingID() {
		return alBuildingID;
	}

	public void setAlBuildingID(ArrayList alBuildingID) {
		this.alBuildingID = alBuildingID;
	}

	public String getBuildingID() {
		return buildingID;
	}

	public void setBuildingID(String buildingID) {
		this.buildingID = buildingID;
	}

	public String getBuildid() {
		return buildid;
	}

	public void setBuildid(String buildid) {
		this.buildid = buildid;
	}

	public String getBldFlag() {
		return bldFlag;
	}

	public void setBldFlag(String bldFlag) {
		this.bldFlag = bldFlag;
	}

	public String getNewZone() {
		return newZone;
	}

	public void setNewZone(String newZone) {
		this.newZone = newZone;
	}

	public String getActiveFl() {
		return activeFl;
	}

	public void setActiveFl(String activeFl) {
		this.activeFl = activeFl;
	}
	
	

}