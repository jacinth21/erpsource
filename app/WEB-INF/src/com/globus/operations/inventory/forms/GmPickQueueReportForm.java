

package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Vamsi Kiran Nagavarapu
 */
public class GmPickQueueReportForm extends GmCommonForm {
	private ArrayList alStatus = new ArrayList();
	private ArrayList alTypes = new ArrayList();
	private ArrayList alUsers = new ArrayList();
	private ArrayList alReportList = new ArrayList();
	private ArrayList aldetailList = new ArrayList();
	private ArrayList alLocations = new ArrayList();
	private ArrayList alStatus1 = new ArrayList();
	private ArrayList alStatusOther = new ArrayList();
	
	private String typeNum = "";
	private String userNum = "";
	private String statusNum = "";
	private String statusOper = "";
	private String statusOther = "";
	private String dateType = "";
	private String startDate = "";
	private String endDate = "";
	
	private String strFinalData = "";
	
	private String gridXmlData = "";
	
	private String refID = "";
	private String fromLocationID = "";
	private String toLocationID = "";
	private String source = "";

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public ArrayList getAlLocations() {
		return alLocations;
	}

	public void setAlLocations(ArrayList alLocations) {
		this.alLocations = alLocations;
	}

	public String getFromLocationID() {
		return fromLocationID;
	}

	public void setFromLocationID(String fromLocationID) {
		this.fromLocationID = fromLocationID;
	}

	public String getToLocationID() {
		return toLocationID;
	}

	public void setToLocationID(String toLocationID) {
		this.toLocationID = toLocationID;
	}

	public ArrayList getAlStatus() {
		return alStatus;
	}

	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	public ArrayList getAlTypes() {
		return alTypes;
	}

	public void setAlTypes(ArrayList alTypes) {
		this.alTypes = alTypes;
	}

	public ArrayList getAlUsers() {
		return alUsers;
	}

	public void setAlUsers(ArrayList alUsers) {
		this.alUsers = alUsers;
	}

	public ArrayList getAlReportList() {
		return alReportList;
	}

	public void setAlReportList(ArrayList alReportList) {
		this.alReportList = alReportList;
	}

	public ArrayList getAldetailList() {
		return aldetailList;
	}

	public void setAldetailList(ArrayList aldetailList) {
		this.aldetailList = aldetailList;
	}

	/**
	 * @return the alStatus1
	 */
	public ArrayList getAlStatus1() {
		return alStatus1;
	}

	/**
	 * @param alStatus1 the alStatus1 to set
	 */
	public void setAlStatus1(ArrayList alStatus1) {
		this.alStatus1 = alStatus1;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRefID() {
		return refID;
	}

	public void setRefID(String refID) {
		this.refID = refID;
	}

	/**
	 * @return the statusOper
	 */
	public String getStatusOper() {
		return statusOper;
	}

	/**
	 * @param statusOper the statusOper to set
	 */
	public void setStatusOper(String statusOper) {
		this.statusOper = statusOper;
	}

	public String getStatusNum() {
		return statusNum;
	}

	public void setStatusNum(String statusNum) {
		this.statusNum = statusNum;
	}

	public String getTypeNum() {
		return typeNum;
	}

	public void setTypeNum(String typeNum) {
		this.typeNum = typeNum;
	}

	public String getUserNum() {
		return userNum;
	}

	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}

	/**
	 * @return the dateType
	 */
	public String getDateType() {
		return dateType;
	}

	/**
	 * @param dateType the dateType to set
	 */
	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @return the strFinalData
	 */
	public String getStrFinalData() {
		return strFinalData;
	}

	/**
	 * @param strFinalData the strFinalData to set
	 */
	public void setStrFinalData(String strFinalData) {
		this.strFinalData = strFinalData;
	}

	/**
	 * @return the alStatusOther
	 */
	public ArrayList getAlStatusOther() {
		return alStatusOther;
	}

	/**
	 * @param alStatusOther the alStatusOther to set
	 */
	public void setAlStatusOther(ArrayList alStatusOther) {
		this.alStatusOther = alStatusOther;
	}

	/**
	 * @return the statusOther
	 */
	public String getStatusOther() {
		return statusOther;
	}

	/**
	 * @param statusOther the statusOther to set
	 */
	public void setStatusOther(String statusOther) {
		this.statusOther = statusOther;
	}

	

}