package com.globus.operations.inventory.forms;

import java.util.ArrayList;
import com.globus.common.forms.GmCommonForm;

public class GmActivateLocationForm extends GmCommonForm{
	private ArrayList alWareHouse = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private String strSuccess = "";
	private String strError = "";
	private String strStatusType = "";
	private String strLocationId = "";
	private String strWareHouseId = "";
	/**
	 * @return the alWareHouse
	 */
	public ArrayList getAlWareHouse() {
		return alWareHouse;
	}
	/**
	 * @param alWareHouse the alWareHouse to set
	 */
	public void setAlWareHouse(ArrayList alWareHouse) {
		this.alWareHouse = alWareHouse;
	}
	
	
	/**
	 * @return the strStatusType
	 */
	public String getStrStatusType() {
		return strStatusType;
	}
	/**
	 * @param strStatusType the strStatusType to set
	 */
	public void setStrStatusType(String strStatusType) {
		this.strStatusType = strStatusType;
	}
	/**
	 * @return the strLocationId
	 */
	public String getStrLocationId() {
		return strLocationId;
	}
	/**
	 * @param strLocationId the strLocationId to set
	 */
	public void setStrLocationId(String strLocationId) {
		this.strLocationId = strLocationId;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	
	/**
	 * @return the strWareHouseId
	 */
	public String getStrWareHouseId() {
		return strWareHouseId;
	}
	/**
	 * @param strWareHouseId the strWareHouseId to set
	 */
	public void setStrWareHouseId(String strWareHouseId) {
		this.strWareHouseId = strWareHouseId;
	}
	/**
	 * @return the strSuccess
	 */
	public String getStrSuccess() {
		return strSuccess;
	}
	/**
	 * @param strSuccess the strSuccess to set
	 */
	public void setStrSuccess(String strSuccess) {
		this.strSuccess = strSuccess;
	}
	/**
	 * @return the strError
	 */
	public String getStrError() {
		return strError;
	}
	/**
	 * @param strError the strError to set
	 */
	public void setStrError(String strError) {
		this.strError = strError;
	}
	
	
	

}
