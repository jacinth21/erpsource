package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmRerunTxnSplitForm extends GmCommonForm {
	
	private String strTxnId = "";
	private String strTxnType = "";
	
	private ArrayList alTxnTypeList = new ArrayList();

	public String getStrTxnId() {
		return strTxnId;
	}

	public void setStrTxnId(String strTxnId) {
		this.strTxnId = strTxnId;
	}

	public String getStrTxnType() {
		return strTxnType;
	}

	public void setStrTxnType(String strTxnType) {
		this.strTxnType = strTxnType;
	}

	public ArrayList getAlTxnTypeList() {
		return alTxnTypeList;
	}

	public void setAlTxnTypeList(ArrayList alTxnTypeList) {
		this.alTxnTypeList = alTxnTypeList;
	}


}
