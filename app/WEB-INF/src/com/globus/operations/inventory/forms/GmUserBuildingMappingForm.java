package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmUserBuildingMappingForm extends GmCommonForm{
	
	private String buildingId ="";
	private String secGrp ="";
	private String companyId = "";
	private String userList = "";
	private String userDept = "";
	private String userDeptId = "";
	private String hinputstring ="";
	private String strAction = "";
	private String optVal = "";
	private String showUser= "";
	 
	private ArrayList alBuildingList = new ArrayList();
	private ArrayList alUserList = new ArrayList();
	private ArrayList alSecGrp = new ArrayList();
	private ArrayList alCompany = new ArrayList();
	private ArrayList alSelectedUserList = new ArrayList();
	
	public String getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	public String getSecGrp() {
		return secGrp;
	}
	public void setSecGrp(String secGrp) {
		this.secGrp = secGrp;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getUserList() {
		return userList;
	}
	public void setUserList(String userList) {
		this.userList = userList;
	}
	
	public String getUserDept() {
		return userDept;
	}
	public void setUserDept(String userDept) {
		this.userDept = userDept;
	}
	
	public String getUserDeptId() {
		return userDeptId;
	}
	public void setUserDeptId(String userDeptId) {
		this.userDeptId = userDeptId;
	}
    	
	public String getHinputstring() {
		return hinputstring;
	}
	public void setHinputstring(String hinputstring) {
		this.hinputstring = hinputstring;
	}
	
	public String getOptVal() {
		return optVal;
	}
	public void setOptVal(String optVal) {
		this.optVal = optVal;
	}
	public ArrayList getAlBuildingList() {
		return alBuildingList;
	}
	public void setAlBuildingList(ArrayList alBuildingList) {
		this.alBuildingList = alBuildingList;
	}
	public ArrayList getAlUserList() {
		return alUserList;
	}
	public void setAlUserList(ArrayList alUserList) {
		this.alUserList = alUserList;
	}
	public ArrayList getAlSecGrp() {
		return alSecGrp;
	}
	public void setAlSecGrp(ArrayList alSecGrp) {
		this.alSecGrp = alSecGrp;
	}
	public ArrayList getAlCompany() {
		return alCompany;
	}
	public void setAlCompany(ArrayList alCompany) {
		this.alCompany = alCompany;
	}
	public String getStrAction() {
		return strAction;
	}
	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}
	public String getShowUser() {
		return showUser;
	}
	public void setShowUser(String showUser) {
		this.showUser = showUser;
	}
	public ArrayList getAlSelectedUserList() {
		return alSelectedUserList;
	}
	public void setAlSelectedUserList(ArrayList alSelectedUserList) {
		this.alSelectedUserList = alSelectedUserList;
	}
	
	
}
