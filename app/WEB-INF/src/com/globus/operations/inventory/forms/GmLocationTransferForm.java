package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmLocationTransferForm extends GmCommonForm{
	private String  strXMLGrid="";
	
	private String warehouseid = "";
	
	private ArrayList alWareHouse = new ArrayList();

	/**
	 * @return the strXMLGrid
	 */
	public String getStrXMLGrid() {
		return strXMLGrid;
	}

	/**
	 * @param strXMLGrid the strXMLGrid to set
	 */
	public void setStrXMLGrid(String strXMLGrid) {
		this.strXMLGrid = strXMLGrid;
	}

	/**
	 * @return the warehouseid
	 */
	public String getWarehouseid() {
		return warehouseid;
	}

	/**
	 * @param warehouseid the warehouseid to set
	 */
	public void setWarehouseid(String warehouseid) {
		this.warehouseid = warehouseid;
	}

	/**
	 * @return the alWareHouse
	 */
	public ArrayList getAlWareHouse() {
		return alWareHouse;
	}

	/**
	 * @param alWareHouse the alWareHouse to set
	 */
	public void setAlWareHouse(ArrayList alWareHouse) {
		this.alWareHouse = alWareHouse;
	}
	
	
	
}
