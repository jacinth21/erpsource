

package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Vamsi Kiran Nagavarapu
 */
public class GmAllocatedForm extends GmCommonForm {
	private ArrayList alStatus = new ArrayList();
	private ArrayList alReportList = new ArrayList();
	
	private ArrayList alUsers = new ArrayList();
	

	private String strFinalData = "";
	
	private String gridXmlData = "";

	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}

	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	/**
	 * @return the alUsers
	 */
	public ArrayList getAlUsers() {
		return alUsers;
	}

	/**
	 * @param alUsers the alUsers to set
	 */
	public void setAlUsers(ArrayList alUsers) {
		this.alUsers = alUsers;
	}

	/**
	 * @return the strFinalData
	 */
	public String getStrFinalData() {
		return strFinalData;
	}

	/**
	 * @param strFinalData the strFinalData to set
	 */
	public void setStrFinalData(String strFinalData) {
		this.strFinalData = strFinalData;
	}

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @return the alReportList
	 */
	public ArrayList getAlReportList() {
		return alReportList;
	}

	/**
	 * @param alReportList the alReportList to set
	 */
	public void setAlReportList(ArrayList alReportList) {
		this.alReportList = alReportList;
	}

	
	
	
}