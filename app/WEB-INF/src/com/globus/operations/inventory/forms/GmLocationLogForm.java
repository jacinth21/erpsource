package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
*  GmLocationLogForm.java
*  @author Rick Schmid
*/
public class GmLocationLogForm  extends GmCommonForm {
	
	private String locationID = "";
	private String partNum = "";
	private String txnID = "";
	private String gridXmlData = "";
	private String fromDate = "";
	private String toDate = "";
	private String strWarehouseID = "";	

	private ArrayList alWarehouseList = new ArrayList();
	
	/**
	 * @return the AlWarehouseList
	 */
	public ArrayList getAlWarehouseList() {
		return alWarehouseList;
	}
	/**
	 * @return the AlWarehouseList
	 */
	public void setAlWarehouseList(ArrayList alWarehouseList) {
		this.alWarehouseList = alWarehouseList;
	}
	/**
	 * @return the WarehouseID
	 */
	public String getStrWarehouseID() {
		return strWarehouseID;
	}
	/**
	 * @param warehouseID the warehouseID to set
	 */
	public void setStrWarehouseID(String strWarehouseID) {
		this.strWarehouseID = strWarehouseID;
	}
	/**
	 * @return the locationID
	 */
	public String getLocationID() {
		return locationID;
	}
	/**
	 * @param locationID the locationID to set
	 */
	public void setLocationID(String strLocationID) {
		this.locationID = strLocationID;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the Part Number to set
	 */
	public void setPartNum(String strPartNum) {
		this.partNum = strPartNum;
	}
	/**
	 * @return the txnID
	 */
	public String getTxnID() {
		return txnID;
	}
	/**
	 * @param txnID the Transaction Number to set
	 */
	public void setTxnID(String strTxnID) {
		this.txnID = strTxnID;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the GridXmlData to set
	 */
	public void setGridXmlData(String strGridXmlData) {
		this.gridXmlData = strGridXmlData;
	}
	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String strFromDate) {
		this.fromDate = strFromDate;
	}
	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String strToDate) {
		this.toDate = strToDate;
	}

}
