package com.globus.operations.inventory.forms;

import com.globus.common.forms.GmCommonForm;

public class GmPendingPutSetForm extends GmCommonForm{
 
	private String accessFlag ="";
	private String gridXmlData = "";
	private String strInputString = "";
	private String locationType = "";
	private String pType = "";
	private String wareHouseID = "";
	private String locationID = "";
	private String tagID = "";
	private String hTagID = "";
	private String hGridRowId = "";
	private String hScanLocID = "";
	

	public String gethScanLocID() {
		return hScanLocID;
	}
	public void sethScanLocID(String hScanLocID) {
		this.hScanLocID = hScanLocID;
	}
	public String gethGridRowId() {
		return hGridRowId;
	}
	public void sethGridRowId(String hGridRowId) {
		this.hGridRowId = hGridRowId;
	}
	public String gethTagID() {
		return hTagID;
	}
	public void sethTagID(String hTagID) {
		this.hTagID = hTagID;
	}
	public String getTagID() {
		return tagID;
	}
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}
	/**
	 * @return the locationID
	 */
	public String getLocationID() {
		return locationID;
	}
	/**
	 * @param locationID the locationID to set
	 */
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}
	/**
	 * @return the wareHouseID
	 */
	public String getWareHouseID() {
		return wareHouseID;
	}
	/**
	 * @param wareHouseID the wareHouseID to set
	 */
	public void setWareHouseID(String wareHouseID) {
		this.wareHouseID = wareHouseID;
	}
	/**
	 * @return the accessFlag
	 */
	public String getAccessFlag() {
		return accessFlag;
	}
	/**
	 * @param accessFlag the accessFlag to set
	 */
	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the strInputString
	 */
	public String getStrInputString() {
		return strInputString;
	}
	/**
	 * @param strInputString the strInputString to set
	 */
	public void setStrInputString(String strInputString) {
		this.strInputString = strInputString;
	}
	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return locationType;
	}
	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	/**
	 * @return the pType
	 */
	public String getpType() {
		return pType;
	}
	/**
	 * @param pType the pType to set
	 */
	public void setpType(String pType) {
		this.pType = pType;
	}
	
	
}
