package com.globus.operations.inventory.forms;

import com.globus.common.forms.GmCommonForm;

public class GmInhouseAgingForm extends GmCommonForm{

	private String gridXmlData;

	public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	
}
