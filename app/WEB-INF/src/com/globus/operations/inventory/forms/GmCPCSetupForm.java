package com.globus.operations.inventory.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCancelForm;


public class GmCPCSetupForm extends GmLogForm{
 
	
	private String cpcId = "";
	private String cpcName = "";
	private String cpcAddr = "";
	private String cpcBarcode = "";
	private String cpcFinal = "";
	private String strMsg = "";
	
	public String getStrMsg() {
		return strMsg;
	}
	public void setStrMsg(String strMsg) {
		this.strMsg = strMsg;
	}
	private ArrayList alLogReasons = new ArrayList();
	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

	public ArrayList getAlLogReasons() {
		return alLogReasons;
	}
	public void setAlLogReasons(ArrayList alLogReasons) {
		this.alLogReasons = alLogReasons;
	}
	private ArrayList alCpcList = new ArrayList();
	
	
	public ArrayList getAlCpcList() {
		return alCpcList;
	}
	public void setAlCpcList(ArrayList alCpcList) {
		this.alCpcList = alCpcList;
	}
	private ArrayList alActiveFl = new ArrayList();
	
	public ArrayList getAlActiveFl() {
		return alActiveFl;
	}
	public void setAlActiveFl(ArrayList alActiveFl) {
		this.alActiveFl = alActiveFl;
	}
	/**
	 * @return the CpcId
	 */
	public String getCpcId() {
		return cpcId;
	}
	/**
	 * @param CpcId the CpcId to set
	 */
	public void setCpcId(String cpcId) {
		this.cpcId = cpcId;
	}
	/**
	 * @return the CpcName
	 */
	public String getCpcName() {
		return cpcName;
	}
	/**
	 * @param CpcName the CpcName to set
	 */
	public void setCpcName(String cpcName) {
		this.cpcName = cpcName;
	}
	/**
	 * @return the CpcAddr
	 */
	public String getCpcAddr() {
		return cpcAddr;
	}
	/**
	 * @param CpcAddr the CpcAddr to set
	 */
	public void setCpcAddr(String cpcAddr) {
		this.cpcAddr = cpcAddr;
	}
	/**
	 * @return the cpcBarcode
	 */
	public String getCpcBarcode() {
		return cpcBarcode;
	}
	/**
	 * @param cpcBarcode the cpcBarcode to set
	 */
	public void setCpcBarcode(String cpcBarcode) {
		this.cpcBarcode = cpcBarcode;
	}
	/**
	 * @return the cpcFinal
	 */
	public String getCpcFinal() {
		return cpcFinal;
	}
	/**
	 * @param cpcFinal the cpcFinal to set
	 */
	public void setCpcFinal(String cpcFinal) {
		this.cpcFinal = cpcFinal;
	}
	public String getActiveFl() {
		return activeFl;
	}
	public void setActiveFl(String activeFl) {
		this.activeFl = activeFl;
	}
	private String activeFl ="";

}