package com.globus.operations.inventory.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

public class GmFieldSalesWarehouseForm extends GmCommonForm {

  // Ware house

  private String pNum = "";
  private String warehouseId = "";
  private String fieldSales = "";
  private String locationType = "";
  private String fromDt = "";
  private String toDt = "";
  private String historicData = "";
  private String gridData = "";
  private String locationId = "";
  private String partNumber = "";
  private String refid = "";
  private String account = "";
  private String lotNum = "";
  private String searchaccount = "";
  private String accOutputType = "";
  private String sysIds = "";
  private String downloadType = "";
  private String strInput = "";

  private String surgeryDate = "";
  private String shipDate = "";

  private boolean fsQty;

  private ArrayList alLocationType = new ArrayList();
  private ArrayList alFieldSales = new ArrayList();
  private ArrayList alAccounts = new ArrayList();
  private ArrayList alAccOutType = new ArrayList();
  private ArrayList alDownloadType = new ArrayList();

  /**
   * @return the downloadType
   */
  public String getDownloadType() {
    return downloadType;
  }

  /**
   * @param downloadType the downloadType to set
   */
  public void setDownloadType(String downloadType) {
    this.downloadType = downloadType;
  }

  /**
   * @return the sysId
   */
  public String getSysIds() {
    return sysIds;
  }

  /**
   * @param sysId the sysId to set
   */
  public void setSysIds(String sysIds) {
    this.sysIds = sysIds;
  }



  /**
   * @return the alDownloadType
   */
  public ArrayList getAlDownloadType() {
    return alDownloadType;
  }

  /**
   * @param alDownloadType the alDownloadType to set
   */
  public void setAlDownloadType(ArrayList alDownloadType) {
    this.alDownloadType = alDownloadType;
  }

  private String fsQtyVal = "";
  private String fsQuantity = "";
  private ArrayList alQtySearchList = new ArrayList();

  /**
   * @return the refid
   */
  public String getRefid() {
    return refid;
  }

  /**
   * @param refid the refid to set
   */
  public void setRefid(String refid) {
    this.refid = refid;
  }

  /**
   * @return the pNum
   */
  public String getpNum() {
    return pNum;
  }

  /**
   * @param pNum the pNum to set
   */
  public void setpNum(String pNum) {
    this.pNum = pNum;
  }

  /**
   * @return the warehouseId
   */
  public String getWarehouseId() {
    return warehouseId;
  }

  /**
   * @param warehouseId the warehouseId to set
   */
  public void setWarehouseId(String warehouseId) {
    this.warehouseId = warehouseId;
  }

  /**
   * @return the fieldSales
   */
  public String getFieldSales() {
    return fieldSales;
  }

  /**
   * @param fieldSales the fieldSales to set
   */
  public void setFieldSales(String fieldSales) {
    this.fieldSales = fieldSales;
  }

  /**
   * @return the locationType
   */
  public String getLocationType() {
    return locationType;
  }

  /**
   * @param locationType the locationType to set
   */
  public void setLocationType(String locationType) {
    this.locationType = locationType;
  }

  /**
   * @return the fromDt
   */
  public String getFromDt() {
    return fromDt;
  }

  /**
   * @param fromDt the fromDt to set
   */
  public void setFromDt(String fromDt) {
    this.fromDt = fromDt;
  }

  /**
   * @return the toDt
   */
  public String getToDt() {
    return toDt;
  }

  /**
   * @param toDt the toDt to set
   */
  public void setToDt(String toDt) {
    this.toDt = toDt;
  }

  /**
   * @return the historicData
   */
  public String getHistoricData() {
    return historicData;
  }

  /**
   * @param historicData the historicData to set
   */
  public void setHistoricData(String historicData) {
    this.historicData = historicData;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the locationId
   */
  public String getLocationId() {
    return locationId;
  }

  /**
   * @param locationId the locationId to set
   */
  public void setLocationId(String locationId) {
    this.locationId = locationId;
  }

  /**
   * @return the partNumber
   */
  public String getPartNumber() {
    return partNumber;
  }

  /**
   * @param partNumber the partNumber to set
   */
  public void setPartNumber(String partNumber) {
    this.partNumber = partNumber;
  }

  /**
   * @return the alLocationType
   */
  public ArrayList getAlLocationType() {
    return alLocationType;
  }

  /**
   * @param alLocationType the alLocationType to set
   */
  public void setAlLocationType(ArrayList alLocationType) {
    this.alLocationType = alLocationType;
  }

  /**
   * @return the alFieldSales
   */
  public ArrayList getAlFieldSales() {
    return alFieldSales;
  }

  /**
   * @param alFieldSales the alFieldSales to set
   */
  public void setAlFieldSales(ArrayList alFieldSales) {
    this.alFieldSales = alFieldSales;
  }

  /**
   * @return the fsQty
   */
  public boolean isFsQty() {
    return fsQty;
  }

  /**
   * @param fsQty the fsQty to set
   */
  public void setFsQty(boolean fsQty) {
    this.fsQty = fsQty;
  }

  /**
   * @return the fsQtyVal
   */
  public String getFsQtyVal() {
    return fsQtyVal;
  }

  /**
   * @param fsQtyVal the fsQtyVal to set
   */
  public void setFsQtyVal(String fsQtyVal) {
    this.fsQtyVal = fsQtyVal;
  }

  /**
   * @return the fsQuantity
   */
  public String getFsQuantity() {
    return fsQuantity;
  }

  /**
   * @param fsQuantity the fsQuantity to set
   */
  public void setFsQuantity(String fsQuantity) {
    this.fsQuantity = fsQuantity;
  }


  /**
   * @return the alQtySearchList
   */
  public ArrayList getAlQtySearchList() {
    return alQtySearchList;
  }

  /**
   * @param alQtySearchList the alQtySearchList to set
   */
  public void setAlQtySearchList(ArrayList alQtySearchList) {
    System.out.println("alQtySearchList is" + alQtySearchList);
    this.alQtySearchList = alQtySearchList;
  }

  /**
   * @return the alAccounts
   */
  public ArrayList getAlAccounts() {
    return alAccounts;
  }

  /**
   * @param alAccounts the alAccounts to set
   */
  public void setAlAccounts(ArrayList alAccounts) {
    this.alAccounts = alAccounts;
  }

  public String getAccount() {
    return account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  // Getting Lot Number from the form

  public String getLotNum() {
    return lotNum;
  }

  public void setLotNum(String lotNum) {
    this.lotNum = lotNum;
  }

  public String getSearchaccount() {
    return searchaccount;
  }

  public void setSearchaccount(String searchaccount) {
    this.searchaccount = searchaccount;
  }

  public ArrayList getAlAccOutType() {
    return alAccOutType;
  }

  public void setAlAccOutType(ArrayList alAccOutType) {
    this.alAccOutType = alAccOutType;
  }

  public String getAccOutputType() {
    return accOutputType;
  }

  public void setAccOutputType(String accOutputType) {
    this.accOutputType = accOutputType;
  }
/**
   * @return the surgeryDate
   */
  public String getSurgeryDate() {
	return surgeryDate;
}
   /**
   * @param surgeryDate the surgeryDate to set
   */
public void setSurgeryDate(String surgeryDate) {
	this.surgeryDate = surgeryDate;
}
   /**
   * @return the shipDate
   */
public String getShipDate() {
	return shipDate;
}
  /**
   * @param shipDate the shipDate to set
   */
public void setShipDate(String shipDate) {
	this.shipDate = shipDate;
}

   /**
   * @return the strInput
   */
  public String getStrInput() {
    return strInput;
  }

  /**
   * @param strInput the strInput to set
   */
  public void setStrInput(String strInput) {
    this.strInput = strInput;
  }

}
