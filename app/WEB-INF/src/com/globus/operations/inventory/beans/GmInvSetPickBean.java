package com.globus.operations.inventory.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInvSetPickBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmInvSetPickBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	/**
	 * fetchPendingPickSets - This method will fetch location details for
	 * particular Parameter type
	 * 
	 * @param String - HashMap
	 * @exception AppError
	 */
	public ArrayList fetchPendingPickSets(HashMap hmParam) throws AppError {

		log.debug("Enter fetchPendingPickSets method.");
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alResult = new ArrayList();
		String strParamType = GmCommonClass.parseNull((String) hmParam.get("PTYPE"));

		gmDBManager.setPrepareString("gm_pkg_op_set_pick_rpt.gm_fch_pending_pick_requests", 2);
		gmDBManager.setString(1, strParamType);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("Exit fetchPendingPickSets - Array Size ===>" + alResult.size());
		return alResult;
	}

	/**
	 * fetchLoanerSetsDtls - This method will fetch loaner set details
	 * 
	 * @param  hmParam
	 * @exception AppError
	 */
	public ArrayList fetchLoanerSetsDtls(HashMap hmParam) throws AppError {

		log.debug("Enter fetchLoanerSetsDtls.");
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alResult = new ArrayList();
		String strReqId = GmCommonClass.parseNull((String) hmParam.get("PICKLOCATIONID"));
		
		gmDBManager.setPrepareString("gm_pkg_op_set_pick_rpt.gm_fch_loaner_set_dtls", 2);
		gmDBManager.setString(1, strReqId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("Exit fetchLoanerSetsDtls. Array Size ==> " + alResult.size());
		return alResult;
	}

	/**
	 * fetchInvLocationDetails - This method will fetch location details for
	 * particular Location ID
	 * 
	 * @param String
	 *            - strLocationID
	 * @exception AppError
	 */
	public ArrayList fetchPickRequestDtls(HashMap hmParam) throws AppError {

		log.debug("Enter fetchPickRequestDtls.");
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alResult = new ArrayList();
		String strLocationType = GmCommonClass.parseNull((String) hmParam.get("LOCATIONTYPE"));
		String strRefId = GmCommonClass.parseNull((String) hmParam.get("PICKLOCATIONID"));
		
		gmDBManager.setPrepareString("gm_pkg_op_set_pick_rpt.gm_fch_pick_request_dtls", 3);
		gmDBManager.setString(1, strRefId);
		gmDBManager.setString(2, strLocationType);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		// log.debug(" Records is " + hmResult.size());
		log.debug("Exit fetchPickRequestDtls. Array size ==>" + alResult.size());
		return alResult;
	}

	/**
	 * fetchConsignedSetsDtls - This method will fetch Consigned set details.
	 * 
	 * @param HashMap
	 * @exception AppError
	 */
	public ArrayList fetchConsignedSetsDtls(HashMap hmParam) throws AppError {

		log.debug("Enter fetchConsignedSetsDtls.");
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		ArrayList alResult = new ArrayList();
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("PICKLOCATIONID"));
		
		gmDBManager.setPrepareString("gm_pkg_op_set_pick_rpt.gm_fch_consigned_set_dtls", 2);
		gmDBManager.setString(1, strDistId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		log.debug("Exit fetchConsignedSetsDtls. Array Size ==>" + alResult.size());
		return alResult;
	}
	
	/**
	 * savePendingPickSets - This method will create new locations
	 * 
	 * @param hmParams
	 *            - contains the InputString - which contains the Delminted
	 *            string with all changed records
	 * @exception AppError
	 */
	public void savePendingPickSets(HashMap hmParams) throws AppError {
		// Input String - Deliminted String with all changed records
		String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRINPUTSTRING"));
		String strLocationType = GmCommonClass.parseNull((String) hmParams.get("OPERATIONVAL"));
		String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
		// Database Connection
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		gmDBManager.setPrepareString("gm_pkg_op_set_pick_txn.gm_sav_set_pick_batch", 2);
		gmDBManager.setString(1, strInputString);
		gmDBManager.setString(2, strUserID);

		gmDBManager.execute();
		gmDBManager.commit();
		}
}
