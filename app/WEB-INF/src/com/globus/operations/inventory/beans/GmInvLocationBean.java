// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\\beans\\GmStatusLogBean.java

package com.globus.operations.inventory.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmInvLocationBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmInvLocationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmInvLocationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * fetchInvLocationPartMapping - This method will fetch location part mapping details
   * 
   * @param HashMap - Form values
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchInvLocationPartMapping(HashMap hmParams) throws AppError {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    log.debug("hmParams ::: " + hmParams);
    String strLocationID = GmCommonClass.parseNull((String) hmParams.get("LOCATIONID"));
    String strPart = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
    String strZone = GmCommonClass.parseNull((String) hmParams.get("ZONENUM"));
    String strAisle = GmCommonClass.parseNull((String) hmParams.get("AISLENUM"));
    String strShelf = GmCommonClass.parseNull((String) hmParams.get("SHELF"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STATUSNUM"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strUserFilter = GmCommonClass.parseNull((String) hmParams.get("USERFILTER"));
    String strSetId = GmCommonClass.parseNull((String) hmParams.get("SETID"));
    String strTagId = GmCommonClass.parseNull((String) hmParams.get("TAGID"));
    String strLocationMap = GmCommonClass.parseNull((String) hmParams.get("LOCMAP"));
    String strWareHouse = GmCommonClass.parseNull((String) hmParams.get("WAREHOUSEID"));
    String strCurrQty = GmCommonClass.parseNull((String) hmParams.get("CHKCURRQTYFLAG"));
    String strPrintLoc = GmCommonClass.parseNull((String) hmParams.get("PRINTLOC"));
    String strBuildingId = GmCommonClass.parseNull((String) hmParams.get("BUILDINGID"));   // Added for PMT-33507 Add or Edit location
    String strPlantid = getCompPlantId();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    if (strPrintLoc.equals("PRINTLOC")) {
      sbQuery.append(" SELECT DISTINCT t5052.C5052_LOCATION_ID LOCATION_ID ");
      sbQuery.append(" , NVL (C5052_LOCATION_CD, t5052.C5052_LOCATION_ID) LCD");
    } else {
      sbQuery
          .append("   SELECT GM_PKG_OP_INV_WAREHOUSE.get_warehouse_name('',t5052.C5051_INV_WAREHOUSE_ID) WAREHOUSEID, T5052.C5051_INV_WAREHOUSE_ID WID, C5053_LOCATION_PART_MAP_ID ID, t5052.C5052_LOCATION_ID LID, t5052.C5052_LOCATION_ID LOCATION_ID, get_code_name(t5052.C901_LOCATION_TYPE) TYPE, ");
      sbQuery
          .append("   get_code_name(t5052.C901_STATUS) STATUS,t5052.C901_STATUS STATUSNUM,t5052.C901_LOCATION_TYPE LOCTYPENUM, C5010_TAG_ID  TAGID,NVL(C5052_LOCATION_CD,t5052.C5052_LOCATION_ID) LCD,");
      if (strLocationMap.equals("SetMap")) { // Sets - Consignment, Loaner and In house Loaner.
        sbQuery
            .append(" NVL(GET_SET_ID_FROM_TAG(C5010_TAG_ID), GET_SET_ID_FROM_CN(T5053.C5053_LAST_UPDATE_TRANS_ID)) PART_SETID, NVL(GET_SET_NAME_FROM_TAG(C5010_TAG_ID),GET_SET_NAME_FROM_CN(T5053.C5053_LAST_UPDATE_TRANS_ID))  PART_SETNAME , ");
      } else { // Parts - Pick or Bulk
        sbQuery
            .append(" t5053.C205_PART_NUMBER_ID PART_SETID,GET_PARTNUM_DESC(t5053.C205_PART_NUMBER_ID) PART_SETNAME , ");
      }
      sbQuery
          .append(" 	GET_LOC_PART_REP_PERCENT(t5053.C5053_CURR_QTY,t5053.C5053_MIN_QTY,t5053.C5053_MAX_QTY) RQTYPER,  ");
      sbQuery
          .append(" 	(t5053.C5053_CURR_QTY - gm_pkg_op_item_control_rpt.get_part_location_cur_qty(t5053.c205_part_number_id,t5052.C5052_LOCATION_ID,null,"
              + strPlantid + ")) ALLOCQTY, ");
      sbQuery
          .append(" 	gm_pkg_op_item_control_rpt.get_part_location_cur_qty(t5053.c205_part_number_id,t5052.C5052_LOCATION_ID,null,"
              + strPlantid + ")  CURRQTY,");
      sbQuery.append("    t5053.C5053_MIN_QTY MINQTY, t5053.C5053_MAX_QTY MAXQTY,");
      sbQuery.append(" t5057.C5057_BUILDING_NAME BNM, t5057.C5057_BUILDING_ID BID");   // Added for PMT-33507 Add or Edit location
    }
    sbQuery.append("   FROM T5052_LOCATION_MASTER t5052, T5053_LOCATION_PART_MAPPING t5053, t5057_building t5057");   // t5057_building Added for PMT-33507 Add or Edit location
    sbQuery.append("   WHERE t5052.C5052_LOCATION_ID =  t5053.C5052_LOCATION_ID (+)  ");
    sbQuery
        .append("   AND t5052.c5051_inv_warehouse_id NOT IN (SELECT c906_rule_value  FROM t906_rules  WHERE c906_rule_id = 'WHID' AND c906_rule_grp_id = 'RULEWH' AND c906_void_fl IS NULL) "); // FS
                                                                                                                                                                                                // and
                                                                                                                                                                                                // Account
                                                                                                                                                                                                // warehouse
    sbQuery.append("   AND t5052.C5052_VOID_FL IS NULL  ");
    sbQuery.append("   AND t5052.C5057_BUILDING_ID = t5057.C5057_BUILDING_ID(+) ");
    sbQuery.append("   AND t5057.C5057_VOID_FL IS NULL  ");
    if (!strWareHouse.equals("0") && !strWareHouse.equals("")) {
      sbQuery.append("   AND t5052.c5051_inv_warehouse_id =" + strWareHouse);
    }
    if (!strLocationID.equals("")) {
      sbQuery
          .append("   AND REGEXP_LIKE(NVL(t5052.C5052_LOCATION_CD,t5052.C5052_LOCATION_ID) ,  CASE WHEN UPPER('"
              + strLocationID
              + "') IS NOT NULL THEN UPPER('"
              + strLocationID
              + "') ELSE NVL(t5052.C5052_LOCATION_CD,t5052.C5052_LOCATION_ID) END)");
    }
    if (!strPart.trim().equals("")) {
   	 String[] strArrayId ={};
       if (!strPart.equals("")) {
         strArrayId = strPart.trim().split(",");
       }
       sbQuery.append(" AND (");
       int length = strArrayId.length;
       for (int i = 0; i < length; i++) {
         if (i == (length - 1)) {
           sbQuery.append(" t5053.C205_PART_NUMBER_ID LIKE ");
           sbQuery.append("'%" + strArrayId[i].trim() + "%'");
         } else {
           sbQuery.append(" t5053.C205_PART_NUMBER_ID LIKE ");
           sbQuery.append("'%" + strArrayId[i].trim() + "%' OR");
         }
       }
       sbQuery.append(" ) ");
}

    if (!strZone.equals("0") && !strZone.equals("")) {
      sbQuery.append("   AND t5052.C901_ZONE =" + strZone);
    }
    if (!strAisle.equals("")) {
      sbQuery.append("   AND t5052.C5052_AISLE ='" + strAisle + "'");
    }
    if (!strShelf.equals("")) {
      sbQuery.append("   AND t5052.C5052_SHELF ='" + strShelf + "'");
    }
    if (!strType.equals("0") && !strType.equals("")) {
      sbQuery.append("   AND t5052.C901_LOCATION_TYPE =" + strType);
    }
    if (!strStatus.equals("0") && !strStatus.equals("")) {
      sbQuery.append("   AND t5052.C901_STATUS =" + strStatus);
    }
    if (!strBuildingId.equals("0") && !strBuildingId.equals("")) {
        sbQuery.append(" AND t5052.C5057_BUILDING_ID =" + strBuildingId);  //  Added for PMT-33507 Add or Edit location
      }
    /*
     * This Function code is commenting since SPP Project is not moving currently. For BUG-2304
     * Resolution under GOP Project
     */
    if (!strSetId.equals("")) {
      sbQuery.append(" AND GET_SET_ID_FROM_TAG(t5053.C5010_TAG_ID) ='" + strSetId + "'");
    }
    if (!strTagId.equals("")) {
      sbQuery.append(" AND t5053.c5010_tag_id ='" + strTagId + "'");
    }
    if (strLocationMap.equals("SetMap")) { // Sets
      sbQuery
          .append(" AND t5052.c901_location_type in (93501,93502,93503) AND (t5053.c5053_curr_qty > 0 or t5053.c5053_curr_qty is null)");// Consignment
                                                                                                                                         // ,
                                                                                                                                         // Product
                                                                                                                                         // Loaner,
                                                                                                                                         // In
                                                                                                                                         // House
                                                                                                                                         // Loaner
    } else { // Parts
      sbQuery.append(" AND t5052.c901_location_type in (93320,93336)");// Pick Face , Bulk
    }
    if (strUserFilter.equals("YES")) {
      sbQuery.append("   AND t5052.C5052_CREATED_BY =" + strUserID);
    }
    // Added PlantId for to load records depends on PlandId
    sbQuery.append(" AND  t5052.C5040_PLANT_ID =" + strPlantid);
    /*
     * else{ alert("No Matches Found For Your Part Number"); }
     */
    sbQuery.append("   ORDER BY t5052.C5052_LOCATION_ID ");
    log.debug(" Query is " + sbQuery.toString());

    gmDBManager.setPrepareString(sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecord();

    return alResult;
  }



  private void alert(String string) {
    // TODO Auto-generated method stub

  }

  /**
   * fetchInvLocationPartMappingByID - This method will fetch location part mapping details for
   * particular location id
   * 
   * @param String strLocationID
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchInvLocationPartMappingByID(String strLocationID, String strPartNum,
      String strWareHouseId, String strBuildingId) throws AppError {

    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_OP_INV_LOCATION_MODULE.GM_FCH_INV_LOC_PART_MAPPING", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLocationID);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setString(3, strWareHouseId);
    gmDBManager.setString(4, strBuildingId);  // t5057_building Added for PMT-33507 Add or Edit location
    gmDBManager.execute(); 
    ArrayList alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();
    log.debug(" Records is " + alResult);
    log.debug("Exit");
    return alResult;
  }
  
  /**
   * fetchInvLocationPartMappingByID - method overloading used since this method is called 2 times PMT# 33507
   * particular location id
   * 
   * @param String strLocationID
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchInvLocationPartMappingByID(String strLocationID, String strPartNum,
      String strWareHouseId) throws AppError {

    log.debug("Enter");
    ArrayList alResult = fetchInvLocationPartMappingByID(strLocationID, strPartNum, strWareHouseId, "");
    log.debug(" Records is " + alResult);
    return alResult;
  }

  /**
   * fetchLocationLog - This method will return the Location Log details for the listed IN Parameter
   * 
   * @param HashMap hmParams
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchLocationLog(HashMap hmParams) throws AppError {
    ArrayList alReturn = new ArrayList();
    StringBuilder sbQuery = new StringBuilder();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPartID = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
    String strFromDt = GmCommonClass.parseNull((String) hmParams.get("FROMDATE"));
    String strToDt = GmCommonClass.parseNull((String) hmParams.get("TODATE"));
    String strTransId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strLocationId = GmCommonClass.parseNull((String) hmParams.get("LOCATIONID"));
    String strCompDateFormat = getCompDateFmt();
    String strWarehouseID = GmCommonClass.parseZero((String) hmParams.get("STRWAREHOUSEID"));
    String[] strArrayId = new String[10];

    sbQuery.append("SELECT t5054.c5054_txn_id TXNID,");
    sbQuery.append("t5054.c205_part_number_id partnum,");
    sbQuery.append("get_partnum_desc(t5054.c205_part_number_id) partdesc,");
    sbQuery.append("NVL(t5054.c5054_orig_qty,0) orgqty,");
    sbQuery.append(" NVL(t5054.c5054_txn_qty,0) txnqty,");
    sbQuery.append("NVL(t5054.c5054_new_qty,0) newqty,");
    sbQuery.append("t5054.c5052_location_id locationid,");
    sbQuery.append("t5052.c5052_location_cd LOCATIONCD,");
    sbQuery
        .append("GM_PKG_OP_INV_WAREHOUSE.GET_INV_WAREHOUSE_NM('',t5052.c5051_inv_warehouse_id) WAREHOUSENM,");
    sbQuery.append("to_char(t5054.c5054_created_date,'");
    sbQuery.append(strCompDateFormat);
    sbQuery.append("') TXNDATE,");
    sbQuery.append("get_user_name(t5054.c5054_created_by) Createdby ");
    sbQuery
        .append("From T5054_Inv_Location_Log T5054,T5052_LOCATION_MASTER T5052 Where T5054.C5054_TXN_ID is not null ");
    sbQuery.append("AND t5052.C5052_LOCATION_ID = t5054.C5052_LOCATION_ID  ");
    sbQuery.append("AND t5052.C5052_VOID_FL IS NULL  ");
    sbQuery
        .append("AND t5052.c5051_inv_warehouse_id NOT IN (SELECT c906_rule_value  FROM t906_rules  WHERE c906_rule_id = 'WHID' AND c906_rule_grp_id = 'RULEWH' AND c906_void_fl IS NULL) "); // FS
                                                                                                                                                                                             // and
                                                                                                                                                                                             // Account
                                                                                                                                                                                             // Warehouse
    if (!strPartID.trim().equals("")) {
      if (!strPartID.equals("")) {
        strArrayId = strPartID.trim().split(",");
      }
      sbQuery.append(" AND (");
      int length = strArrayId.length;
      for (int i = 0; i < length; i++) {
        if (i == (length - 1)) {
          sbQuery.append(" t5054.C205_PART_NUMBER_ID LIKE ");
          sbQuery.append("'%" + strArrayId[i].trim() + "%'");
        } else {
          sbQuery.append(" t5054.C205_PART_NUMBER_ID LIKE ");
          sbQuery.append("'%" + strArrayId[i].trim() + "%' OR");
        }
      }
      sbQuery.append(" ) ");
    }
    // From/To Dates
    if (!strFromDt.equals("")) {
      sbQuery.append(" AND t5054.c5054_created_date >= TO_DATE ('" + strFromDt + "', '"
          + strCompDateFormat + "')   ");
    }

    if (!strToDt.equals("")) {
      sbQuery.append("  AND trunc(t5054.c5054_created_date) <= TO_DATE ('" + strToDt + "', '"
          + strCompDateFormat + "')   ");
    }

    if (!strLocationId.trim().equals("")) {
      if (!strLocationId.equals("")) {
        strArrayId = strLocationId.trim().split(",");
      }
      sbQuery.append(" AND (");
      int length = strArrayId.length;
      for (int i = 0; i < length; i++) {
        if (i == (length - 1)) {
          sbQuery.append(" t5052.C5052_LOCATION_CD LIKE ");
          sbQuery.append("'%" + strArrayId[i].trim() + "%'");
        } else {
          sbQuery.append(" t5052.C5052_LOCATION_CD LIKE ");
          sbQuery.append("'%" + strArrayId[i].trim() + "%' OR");
        }
      }
      sbQuery.append(" ) ");
    }

    if (!strTransId.trim().equals("")) {
      if (!strTransId.equals("")) {
        strArrayId = strTransId.trim().split(",");
      }
      sbQuery.append(" AND (");
      int length = strArrayId.length;
      for (int i = 0; i < length; i++) {
        if (i == (length - 1)) {
          sbQuery.append(" T5054.C5054_TXN_ID LIKE ");
          sbQuery.append("'%" + strArrayId[i].trim() + "%'");
        } else {
          sbQuery.append(" T5054.C5054_TXN_ID LIKE ");
          sbQuery.append("'%" + strArrayId[i].trim() + "%' OR");
        }
      }
      sbQuery.append(" ) ");
    }
    if (!strWarehouseID.trim().equals("0")) {
      sbQuery.append(" AND T5052.c5051_inv_warehouse_id = '" + strWarehouseID + "'");
    }
    // Added PlantId for to load records depends on PlandId
    sbQuery.append(" AND  t5052.C5040_PLANT_ID =" + getCompPlantId());
    sbQuery.append(" AND  t5052.c5040_plant_id = T5054.c5040_plant_id ");
    sbQuery.append(" ORDER BY t5054.c5054_created_date desc ");
    gmDBManager.setPrepareString(sbQuery.toString());
    log.debug(" Query for reporting Inv Location Log details " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecord();

    return alReturn;
  } // End of fetchLocationLog

  /**
   * saveInvLocation - This method will create new locations
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void saveInvLocation(HashMap hmParams) throws AppError {
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRINPUTSTRING"));
    String strZone = "";
    String strAisle = GmCommonClass.parseNull((String) hmParams.get("AISLENUM"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strShelf = GmCommonClass.parseNull((String) hmParams.get("SHELF"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STRSTATUS"));
    String strWareHouse = GmCommonClass.parseNull((String) hmParams.get("WAREHOUSEID"));
    String strBuildingId = GmCommonClass.parseNull((String) hmParams.get("BUILDINGID"));  //  Added for PMT-33507 Add or Edit location
    String strNewZone = GmCommonClass.parseNull((String) hmParams.get("NEWZONE"));
    ArrayList outList = null;
    String strTemp = "";

    // Database Connection
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    if(!strNewZone.equals("0")&&!strNewZone.equals("")){
    	log.debug("strNewZone"+strNewZone);
    	gmDBManager.setPrepareString("GM_PKG_OP_INV_LOCATION_MODULE.GM_ADD_ZONE", 3);
    	gmDBManager.setString(1, strNewZone);
    	gmDBManager.setString(2, strUserID);
    	 gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    	gmDBManager.execute();
    	strZone = GmCommonClass.parseNull(gmDBManager.getString(3));
    	gmDBManager.commit();
    	gmCacheManager.removePatternKeys("LOCZT");   
    	log.debug("strZone"+strZone);
    }
    else{
    	 strZone = GmCommonClass.parseNull((String) hmParams.get("ZONENUM"));
    }

    if (strInputString != null && strInputString.length() > 4000) {

      outList = GmCommonClass.convertStringToList(strInputString, 4000, "^");
    } else {
      outList = new ArrayList();
      outList.add(strInputString);
    }
   if (outList != null) {
      for (Iterator itr = outList.iterator(); itr.hasNext();) {
        strTemp = (String) itr.next();

        if (strInputString != null) {
          gmDBManager.setPrepareString("GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOCATION", 9);
          gmDBManager.setString(1, strTemp);
          gmDBManager.setString(2, strZone);
          gmDBManager.setString(3, strAisle);
          gmDBManager.setString(4, strUserID);
          gmDBManager.setString(5, strShelf);
          gmDBManager.setString(6, strStatus);
          gmDBManager.setString(7, strType);
          gmDBManager.setString(8, strWareHouse);
          gmDBManager.setString(9, strBuildingId);  //  Added for PMT-33507 Add or Edit location
          gmDBManager.execute();

        }
      }
    }
   
    gmDBManager.commit();

  }


  /**
   * saveInvLocationPartDetails - This method will update or save the change of Parts for various
   * locations
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void saveInvLocationPartDetails(HashMap hmParams) throws AppError {
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRINPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    ArrayList outList = null;
    String strTemp = "";
    // Database Connection
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    if (strInputString != null && strInputString.length() > 4000) {

      outList = GmCommonClass.convertStringToList(strInputString, 4000, "|");
    } else {
      outList = new ArrayList();
      outList.add(strInputString);
    }
    if (outList != null) {
      for (Iterator itr = outList.iterator(); itr.hasNext();) {
        strTemp = (String) itr.next();

        if (strInputString != null) {
          gmDBManager.setPrepareString("GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOC_PART_DETAILS",
              2);
          gmDBManager.setString(1, strTemp);
          gmDBManager.setString(2, strUserID);

          gmDBManager.execute();
          gmDBManager.commit();
        }
      }
    }
  }

  /**
   * saveLocationPartDetails - This method will update or save the change of Parts for the specific
   * Location
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void saveInvLocationPartDetailById(HashMap hmParams) throws AppError {
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRINPUTSTRING"));
    String strLocationID = GmCommonClass.parseNull((String) hmParams.get("HLOCATIONID"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STATUSNUM"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strVoidRowID = GmCommonClass.parseNull((String) hmParams.get("STRVOIDROWIDS"));
    String strWareHouse = GmCommonClass.parseNull((String) hmParams.get("WAREHOUSEID"));
    String strBuildingId = GmCommonClass.parseNull((String) hmParams.get("BUILDINGID"));  //  Added for PMT-33507 Add or Edit location
    // Database Connection
    log.debug("in saveInvLocationPartDetailById :");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    log.debug("strVoidRowID :" + strVoidRowID);
    gmDBManager.setPrepareString("GM_PKG_OP_INV_LOCATION_MODULE.GM_SAV_INV_LOC_PART_DETAIL", 8);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strLocationID);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strStatus);
    gmDBManager.setString(5, strUserID);
    gmDBManager.setString(6, strVoidRowID);
    gmDBManager.setString(7, strWareHouse);
    gmDBManager.setString(8, strBuildingId);  //  Added for PMT-33507 Add or Edit location
    gmDBManager.execute();
    gmDBManager.commit();


  }

  /**
   * fetchInvLocationDetails - This method will fetch location details for particular Location ID
   * 
   * @param String - strLocationID
   * @exception AppError
   */
  public HashMap fetchInvLocationDetailsById(String strLocationID, String strWareHouseId, String strBuildingId)
      throws AppError {

    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_OP_INV_LOCATION_MODULE.GM_FCH_INV_LOC_DETAILS", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLocationID);
    gmDBManager.setString(2, strWareHouseId);
    gmDBManager.setString(3, strBuildingId);  //  Added for PMT-33507 Add or Edit location
    gmDBManager.execute();
    HashMap hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    // log.debug(" Records is " + hmResult.size());
    log.debug("Exit");
    return hmResult;
  }



  /**
   * getPartNumDesc - This method will get the Part Number Description for given part num
   * 
   * @param String strPartNum
   * @exception AppError
   */
  public String getPartNumDesc(String strPartNum) throws AppError {

    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_OP_INV_LOCATION_MODULE.GM_GET_PART_NUM_DESC", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.execute();
    HashMap hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    // log.debug(" Records is " + hmResult.size());
    log.debug("Exit");
    return ((String) hmResult.get("PART_DESC"));
  }

  /**
   * getXmlGridData - This method will return the Xml String
   * 
   * @param hmInvLocParams - contains the Template and Template path
   * @return String XML String
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmInvLocParams) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();

    ArrayList alParam =
        GmCommonClass.parseNullArrayList((ArrayList) hmInvLocParams.get("ALINVLOCLIST"));
    ArrayList alParts = GmCommonClass.parseNullArrayList((ArrayList) hmInvLocParams.get("PARTS"));
    ArrayList alInvLocTypes =
        GmCommonClass.parseNullArrayList((ArrayList) hmInvLocParams.get("INVLOCTYPES"));
    ArrayList alStatus = GmCommonClass.parseNullArrayList((ArrayList) hmInvLocParams.get("STATUS"));
    String strOpt = GmCommonClass.parseNull((String) hmInvLocParams.get("STROPT"));
    String strTemplate = GmCommonClass.parseNull((String) hmInvLocParams.get("TEMPLATE"));
    String strTemplatePath = GmCommonClass.parseNull((String) hmInvLocParams.get("TEMPLATEPATH"));
    String StrCurrentQtyFlag =
        GmCommonClass.parseNull((String) hmInvLocParams.get("STRCURRENTQTYFLAG"));
    String StrLocationType =
        GmCommonClass.parseNull((String) hmInvLocParams.get("STRLOCATIONTYPE"));
    String StrStatus = GmCommonClass.parseNull((String) hmInvLocParams.get("STRSTATUS"));
    String strAccessfl = GmCommonClass.parseNull((String) hmInvLocParams.get("ACCESSFLAG"));
    String strLocationMap = GmCommonClass.parseNull((String) hmInvLocParams.get("SETMAP"));
    String strBldFlag = GmCommonClass.parseNull((String) hmInvLocParams.get("BLDFLAG"));  // Added for PMT-33507 Add or Edit Building locations
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmInvLocParams.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmInvLocParams.get("VMFILEPATH"));
    HashMap hmLcnParam = new HashMap();
    hmLcnParam.put("STROPT", strOpt);
    hmLcnParam.put("SETMAP", strLocationMap);
    hmLcnParam.put("ACCESSFLAG", strAccessfl);
    hmLcnParam.put("STRCURRENTQTYFLAG", StrCurrentQtyFlag);
    hmLcnParam.put("STRLOCATIONTYPE", StrLocationType);
    hmLcnParam.put("STRSTATUS", StrStatus);
    hmLcnParam.put("BLDFLAG", strBldFlag);  // Added for PMT-33507 Add or Edit Building locations
    // log.debug("alParam==="+alParam);
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmLcnParam", hmLcnParam);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setDropDownMaster("alParts", alParts);
    templateUtil.setDropDownMaster("alInvLocTypes", alInvLocTypes);
    templateUtil.setDropDownMaster("alStatus", alStatus);

    return templateUtil.generateOutput();
  }

  public ArrayList fetchWareHouse(String strTxnType) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_OP_INV_WAREHOUSE.GM_FCH_WAREHOUSE_DTL", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTxnType);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }


  /**
   * fetchWareHouseDtl - This method will fetch details for warehouse author Jignesh Shah
   * 
   * @param hmParams
   * @exception AppError
   */
  public ArrayList fetchWareHouseDtl(HashMap hmParams) throws AppError {
    ArrayList alResult = new ArrayList();

    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TXNTYPE"));
    String strCodeGrp = GmCommonClass.parseNull((String) hmParams.get("CODEGRP"));
    String strDeptId = GmCommonClass.parseNull((String) hmParams.get("DEPTID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse.gm_fetch_inv_warehouse_dtls", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTxnType);
    gmDBManager.setString(2, strCodeGrp);
    gmDBManager.setString(3, strDeptId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchLotTrackingDetails - This method will fetch the lot tracking details
   * 
   * @param HashMap hmParams
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchLotTrackingDetails(HashMap hmParams) throws AppError {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strPart = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
    String strctrlNum = GmCommonClass.parseNull((String) hmParams.get("CTRLNUM"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParams.get("DONORNUM"));
    String strWareHouse = GmCommonClass.parseZero((String) hmParams.get("WAREHOUSE"));
    String strExpDateRange = GmCommonClass.parseZero((String) hmParams.get("EXPDATERANGE"));
    String strExpDate = GmCommonClass.parseNull((String) hmParams.get("EXPDATE"));
    String strInternationalUse = GmCommonClass.parseNull((String) hmParams.get("INTERNATIONALUSE"));
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    strInternationalUse = strInternationalUse.equalsIgnoreCase("on") ? "YES" : "NO";
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("LOTTYPE"));
    String strFieldSalesNm = GmCommonClass.parseZero((String) hmParams.get("FIELDSALESNM"));
    String strAccountNm = GmCommonClass.parseZero((String) hmParams.get("ACCOUNTNM"));
    String strConsigneeNm =
        !strFieldSalesNm.equals("0") ? strFieldSalesNm : !strAccountNm.equals("0") ? strAccountNm
            : "0";

    String shippedFromDate = GmCommonClass.parseNull((String) hmParams.get("SHIPPEDFROMDATE"));
    String shippedToDate = GmCommonClass.parseNull((String) hmParams.get("SHIPPEDTODATE"));
    
    HashMap hmTempComp = new HashMap(); // To set the comparison signs to corresponding code id
    hmTempComp.put("90191", "<");
    hmTempComp.put("90192", "=");
    hmTempComp.put("90194", "<=");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery
        .append(" SELECT T2550.C205_PART_NUMBER_ID partnum, GET_PARTNUM_DESC(T2550.C205_PART_NUMBER_ID) partdesc, T2550.C2550_CONTROL_NUMBER ctrlnum ");
    sbQuery.append(" , to_char(T2550.C2550_EXPIRY_DATE, '" + strDateFmt + "') expdate");
    sbQuery
        .append(" , T2540.C2540_DONOR_NUMBER donornum, GET_CODE_NAME(T2540.C901_SEX) sex, T2540.C2540_AGE age ");
    // 103923: Account Id; 103922: Field Sales Id
    sbQuery
        .append(" , T5060.C5060_QTY qty, DECODE(T5060.C901_REF_TYPE,103923, t704.c704_account_nm,103922,  t701.c701_distributor_name , T5052.C5052_LOCATION_CD) locationcode");
    sbQuery
        .append(" , get_code_name(t5060.C901_WAREHOUSE_TYPE) warehouse, get_code_name(T5060.C901_REF_TYPE) type");
    sbQuery
        .append(" , DECODE(T2540.C901_INTERNATIONAL_USE,'1960','Y','1961','N',null) internationaluse"); // 1960:
                                                                                                        // Yes,
                                                                                                        // 1961:
                                                                                                        // No
                                                                                                        // [code
                                                                                                        // lookup
                                                                                                        // values]
    sbQuery
        .append(" , NVL(T2550.C2550_CUSTOM_SIZE, V205G.C205D_LABEL_SIZE) customsize, get_code_name(t2540.c901_research) research");
    sbQuery.append(" , T5062.C5062_RESERVED_QTY reservQty, T5062.C5062_TRANSACTION_ID refid, get_code_name(T5060.c901_ship_to_type) shiptotype, GET_CS_SHIP_NAME(T5060.c901_ship_to_type,T5060.C5060_SHIP_TO_ID) shippingname, to_char(T5060.C5060_SHIPPED_DT, '" + strDateFmt + "') shipdate, T5060.C5060_CUSTOMER_PO customerpo");
    sbQuery.append(" , T2550.c2550_last_updated_trans_id transactionid ");
    sbQuery
        .append(" FROM T2550_PART_CONTROL_NUMBER T2550, T2540_DONOR_MASTER T2540, T5060_CONTROL_NUMBER_INV T5060, T5052_LOCATION_MASTER T5052, V205G_PART_LABEL_PARAMETER V205G ");
    sbQuery
        .append(" , T5062_CONTROL_NUMBER_RESERVE T5062, T704_ACCOUNT T704, T701_DISTRIBUTOR T701, T205_PART_NUMBER T205, T901_CODE_LOOKUP T901 ");
    sbQuery.append(" WHERE T2550.C205_PART_NUMBER_ID = T5060.C205_PART_NUMBER_ID ");
    sbQuery.append(" and t2550.c205_part_number_id = t205.c205_part_number_id ");
    sbQuery.append(" AND t5060.C901_WAREHOUSE_TYPE = t901.c901_code_id (+) ");
    sbQuery.append(" AND T5060.C205_PART_NUMBER_ID = V205G.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T2550.C2540_DONOR_ID = T2540.C2540_DONOR_ID(+) ");
    sbQuery
        .append(" AND T5060.C5060_CONTROL_NUMBER_INV_ID = T5062.C5060_CONTROL_NUMBER_INV_ID(+) ");
    // sbQuery.append(" AND T2550.C301_VENDOR_ID = T2540.C301_VENDOR_ID(+) ");
    sbQuery.append(" AND T5060.C5060_CONTROL_NUMBER = T2550.C2550_CONTROL_NUMBER ");
    sbQuery.append(" AND T5060.C5060_REF_ID = T5052.C5052_LOCATION_CD(+) ");
    sbQuery.append(" AND T2540.C2540_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND T5052.C5052_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND T5062.C5062_VOID_FL(+) IS NULL ");
    if (strStatus.equals("104006")) {// 104006: UnReserved
      sbQuery.append(" AND t5062.C5062_TRANSACTION_ID IS NULL ");
    } else if (strStatus.equals("104005")) {// 104005: Reserved
      sbQuery.append(" AND t5062.C5062_TRANSACTION_ID IS NOT NULL ");
    }
    sbQuery.append(" AND T5060.C5060_REF_ID = T704.C704_ACCOUNT_ID (+)");
    sbQuery.append(" AND T5060.C5060_REF_ID = T701.C701_DISTRIBUTOR_ID (+)");
    sbQuery.append(" AND NVL(T5060.C5060_QTY,'0') > 0");
    sbQuery.append(" AND T5062.C901_STATUS(+) <> ('103962') "); // 103962: closed; no need to fetch
                                                                // the details of closed
                                                                // transactions

    if (!strPart.equals("")) { // If part number is given in filter
    
    	sbQuery.append(" AND REGEXP_LIKE(T2550.c205_part_number_id, CASE WHEN TO_CHAR(REGEXP_REPLACE('"+strPart
	    		  +"','[+]','\\+')) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE('"+strPart
	    		  +"','[+]','\\+')) ELSE T2550.c205_part_number_id END) ");
    }
    if (!strctrlNum.equals("")) {// If control number is given in filter
      sbQuery.append(" AND UPPER(T2550.C2550_CONTROL_NUMBER) = UPPER('" + strctrlNum + "') ");
    }
    if (!strDonorNum.equals("")) {// If donor number is given
      sbQuery.append(" AND UPPER(T2540.C2540_DONOR_NUMBER) = UPPER('" + strDonorNum + "') ");
    }

    if (!strWareHouse.equals("0")) { // If Warehouse value is selected in dropdown
      sbQuery.append(" AND t5060.C901_WAREHOUSE_TYPE = " + strWareHouse);
    }

    if (!strExpDate.equals("")) { // Filter based on Expiry date range is selected
      sbQuery.append(" AND TRUNC(T2550.C2550_EXPIRY_DATE) ");
      sbQuery.append(hmTempComp.get(strExpDateRange));
      sbQuery.append(" TRUNC(TO_DATE('" + strExpDate + "','" + strDateFmt + "'))");
    }

    if (strInternationalUse.equals("YES")) { // Should fetch only the records having international
                                             // use value as yes
      sbQuery.append(" AND T2540.C901_INTERNATIONAL_USE = '1960'");// 1960: Yes [code lookup value]
    }
    sbQuery.append(" AND T2550.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(" AND T2550.C5040_PLANT_ID = " + getCompPlantId());
    if (!strConsigneeNm.equals("0")) {
      sbQuery
          .append(" AND DECODE(T5060.C901_REF_TYPE,103923, T704.C704_ACCOUNT_ID,103922,  T701.C701_DISTRIBUTOR_ID) = '");
      sbQuery.append(strConsigneeNm);
      sbQuery.append("'");
    }

    //If warehouse type is Account Qty or Field Sales Qty
    if(strWareHouse.equals("4000339") || strWareHouse.equals("56002")){
	    if (!shippedFromDate.equals("")){
	    	sbQuery.append(" AND TRUNC(T5060.c5060_shipped_dt)  >= TRUNC(TO_DATE('" + shippedFromDate + "','" + strDateFmt + "'))");
	    } 
	    
	    if (!shippedToDate.equals("")){
	    	sbQuery.append(" AND TRUNC(T5060.c5060_shipped_dt)  <= TRUNC(TO_DATE('" + shippedToDate + "','" + strDateFmt + "'))");
	    }
    }
    log.debug(" Query For fetchLotTrackingDetails() : " + sbQuery.toString());

    gmDBManager.setPrepareString(sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecord();
    return alResult;
  }

  /**
   * fetchPartReserveDetails - This method will fetch the Part Reserve Details for ACK Order
   * 
   * @param HashMap hmParams
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchPartReserveDetails(HashMap hmParams) throws AppError {
    ArrayList alResult = new ArrayList();
    String strPart = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
    String strOrderID = GmCommonClass.parseNull((String) hmParams.get("TRANSID"));;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse.gm_fetch_part_resrve_dtls", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPart);
    gmDBManager.setString(2, strOrderID);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alResult;
  }

  /**
   * savePartReserve - This method will save the Part Reserve Details for ACK Order
   * 
   * @param HashMap hmParams
   * @return void
   * @exception
   */
  public void savePartReserve(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse.gm_save_part_resrve", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchReservedLotsDetails - This method will fetch the Reserved Lots details
   * 
   * @param HashMap hmParams
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchReservedLotsDetails(HashMap hmParams) throws AppError {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strPart = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
    String strctrlNum = GmCommonClass.parseNull((String) hmParams.get("CTRLNUM"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParams.get("DONORNUM"));
    String strWareHouse = GmCommonClass.parseZero((String) hmParams.get("WAREHOUSE"));
    String strExpDateRange = GmCommonClass.parseZero((String) hmParams.get("EXPDATERANGE"));
    String strExpDate = GmCommonClass.parseNull((String) hmParams.get("EXPDATE"));
    String strInternationalUse = GmCommonClass.parseNull((String) hmParams.get("INTERNATIONALUSE"));
    String strDateFmt =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    strInternationalUse = strInternationalUse.equalsIgnoreCase("on") ? "YES" : "NO";
    String strRefId = GmCommonClass.parseNull((String) hmParams.get("REFID"));
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    String strPlantId = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());

    HashMap hmTempComp = new HashMap(); // To set the comparison signs to corresponding code id
    hmTempComp.put("90191", "<");
    hmTempComp.put("90192", "=");
    hmTempComp.put("90194", "<=");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery
        .append(" SELECT T2550.C205_PART_NUMBER_ID partnum, GET_PARTNUM_DESC(T2550.C205_PART_NUMBER_ID) partdesc, T2550.C2550_CONTROL_NUMBER ctrlnum ");
    sbQuery.append(" , to_char(T2550.C2550_EXPIRY_DATE, '" + strDateFmt + "') expdate");
    sbQuery
        .append(" , T2540.C2540_DONOR_NUMBER donornum, GET_CODE_NAME(T2540.C901_SEX) sex, T2540.C2540_AGE age ");
    sbQuery
        .append(" , T5060.C5060_QTY qty, T5052.C5052_LOCATION_CD locationcode, get_code_name(t5060.C901_WAREHOUSE_TYPE) warehouse, get_code_name(T5060.C901_REF_TYPE) type");
    sbQuery
        .append(" , DECODE(T2540.C901_INTERNATIONAL_USE,'1960','Y','1961','N',null) internationaluse"); // 1960:
                                                                                                        // Yes,
                                                                                                        // 1961:
                                                                                                        // No
                                                                                                        // [code
                                                                                                        // lookup
                                                                                                        // values]
    sbQuery.append(" , NVL(T2550.C2550_CUSTOM_SIZE, V205G.C205D_LABEL_SIZE) customsize");
    sbQuery
        .append(" , T5062.C5062_RESERVED_QTY reservedqty,T5062.C5062_TRANSACTION_ID refid , get_code_name(t2540.c901_research) research");
    sbQuery
        .append(" FROM T2550_PART_CONTROL_NUMBER T2550, T2540_DONOR_MASTER T2540,T5062_CONTROL_NUMBER_RESERVE T5062, T5060_CONTROL_NUMBER_INV T5060, T5052_LOCATION_MASTER T5052, V205G_PART_LABEL_PARAMETER V205G ");
    sbQuery.append(" WHERE T2550.C205_PART_NUMBER_ID = T5060.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T5060.C205_PART_NUMBER_ID = V205G.C205_PART_NUMBER_ID ");
    sbQuery.append(" AND T2550.C2540_DONOR_ID = T2540.C2540_DONOR_ID(+) ");
    sbQuery.append(" AND T5060.C5060_CONTROL_NUMBER = T2550.C2550_CONTROL_NUMBER ");
    sbQuery.append(" AND T5060.C5060_CONTROL_NUMBER_INV_ID = T5062.C5060_CONTROL_NUMBER_INV_ID ");
    sbQuery.append(" AND T5060.C5060_REF_ID = T5052.C5052_LOCATION_CD(+) ");
    sbQuery.append(" AND T2550.C1900_COMPANY_ID = T5060.C1900_COMPANY_ID ");
    sbQuery.append(" AND T2550.C5040_PLANT_ID = T5060.C5040_PLANT_ID ");
    sbQuery.append(" AND T5060.C1900_COMPANY_ID= '" + strCompanyId + "'");
    sbQuery.append(" AND T5060.C5040_PLANT_ID= '" + strPlantId + "'");
    sbQuery.append(" AND T2550.C1900_COMPANY_ID = T2540.C1900_COMPANY_ID(+) ");
    sbQuery.append(" AND T5060.C5040_PLANT_ID = T5052.C5040_PLANT_ID(+) ");
    sbQuery.append(" AND T5060.C1900_COMPANY_ID = T5062.C1900_COMPANY_ID ");
    sbQuery.append(" AND T5060.C5040_PLANT_ID = T5062.C5040_PLANT_ID");
    sbQuery.append(" AND T2540.C2540_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND T5052.C5052_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND T5062.C5062_VOID_FL (+) IS NULL ");
    sbQuery.append(" AND NVL(T5060.C5060_QTY,'0') > 0");
    sbQuery.append(" AND T5062.C901_STATUS(+) <> ('103962') "); // 103962: closed; ; no need to
                                                                // fetch the details of closed
                                                                // transactions

    if (!strPart.equals("")) { // If part number is given in filter
     
    	sbQuery.append(" AND REGEXP_LIKE(T2550.c205_part_number_id, CASE WHEN TO_CHAR(REGEXP_REPLACE('"+strPart
	    		  +"','[+]','\\+')) IS NOT NULL THEN TO_CHAR(REGEXP_REPLACE('"+strPart
	    		  +"','[+]','\\+')) ELSE T2550.c205_part_number_id END) ");
    }
    if (!strctrlNum.equals("")) {// If control number is given in filter
      sbQuery.append(" AND UPPER(T2550.C2550_CONTROL_NUMBER)  = UPPER('" + strctrlNum + "') ");
    }
    if (!strDonorNum.equals("")) {// If donor number is given
      sbQuery.append(" AND UPPER(T2540.C2540_DONOR_NUMBER) = UPPER('" + strDonorNum + "') ");
    }

    if (!strWareHouse.equals("0")) { // If Warehouse value is selected in dropdown
      sbQuery.append(" AND t5060.C901_WAREHOUSE_TYPE = " + strWareHouse);
    }

    if (!strExpDate.equals("")) { // Filter based on Expiry date range is selected
      sbQuery.append(" AND TRUNC(T2550.C2550_EXPIRY_DATE) ");
      sbQuery.append(hmTempComp.get(strExpDateRange));
      sbQuery.append(" TRUNC(TO_DATE('" + strExpDate + "','" + strDateFmt + "'))");
    }

    if (strInternationalUse.equals("YES")) { // Should fetch only the records having international
                                             // use value as yes
      sbQuery.append(" AND T2540.C901_INTERNATIONAL_USE = '1960'");// 1960: Yes [code lookup value]
    }

    if (!strRefId.equals("")) {// If RefId is given in filter
      sbQuery.append(" AND UPPER(T5062.C5062_TRANSACTION_ID) = UPPER('" + strRefId + "') ");
    }

    log.debug(" Query is " + sbQuery.toString());

    gmDBManager.setPrepareString(sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecord();

    return alResult;
  }

  /**
   * updateLoanerStockSheetInfo - This method will form the message and send through JMS for Loaner
   * Stock sheet to sync. Loaner warehouse information.
   * 
   * @param - strTransactionID:Transaction ID
   * @param - strUserId:user Login Id.
   * @return - Void
   * @exception - AppError
   */
  public void updateLoanerStockSheetInfo(HashMap hmLnParam) throws AppError {

    HashMap hmParam = new HashMap();
    log.debug( " HM LN values :: "+hmLnParam);
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();

  
    // LOANER_LOT_CONSUMER_CLASS is mentioned in constant properties file.This key contain the
    // path for : com.globus.jms.consumers.processors.GmLoanerWarehouseConsumerJob
    String strConsumerClass =
        GmCommonClass.parseNull(GmCommonClass.getString("LOANER_LOT_CONSUMER_CLASS"));

    hmParam.put("HMLNPARAM", hmLnParam);
   // hmParam.put("STRUSERID", strUserId);
    hmParam.put("CONSUMERCLASS", strConsumerClass);

    gmConsumerUtil.sendMessage(hmParam);

  }


  public void syncLoanerWarehouse(HashMap hmParam) throws AppError {
	  
	    ArrayList alResult = new ArrayList();
	   
	    log.debug( " HM LN hmParam :: "+hmParam);
	    String strTransId = GmCommonClass.parseNull((String)  hmParam.get("TRANS_ID"));
	    String strRefID= GmCommonClass.parseNull((String)  hmParam.get("REFID"));
	    String strWarehouseType = GmCommonClass.parseNull((String)  hmParam.get("WAREHOUSE_TYPE"));
	    String strTransType= GmCommonClass.parseNull((String)  hmParam.get("TRANS_TYPE"));
	    String strUserId= GmCommonClass.parseNull((String)  hmParam.get("USER_ID"));
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("GM_PKG_SET_LOT_TRACK.GM_PKG_SET_LOT_UPDATE", 7);
	    //gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	    gmDBManager.setString(1, strTransId);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.setString(3, strRefID);
	    gmDBManager.setString(4, strTransType);
	    gmDBManager.setString(5, strWarehouseType);
	    gmDBManager.setString(6, "");
	    gmDBManager.setString(7, "");
	    gmDBManager.execute();
	    // 50183  Loaner Extension
        //  50155 Shelf to Loaner Set
	    // 100063 Bulk to Loaner
	    
//	    if (strTransType.equals("50183") || strTransType.equals("50155") || strTransType.equals("100063"))
//	    {
//	    	syncLoanerMissingPart(hmParam,gmDBManager);	
//	    }
	    gmDBManager.commit();
	  }

  public void syncLoanerMissingPart (HashMap hmParam, GmDBManager gmDBManager) throws AppError {
	  
	
	    String strTransId = GmCommonClass.parseNull((String) hmParam.get("TRANS_ID"));
	    String strRefID= GmCommonClass.parseNull((String) hmParam.get("REFID"));
        String strLoanerSalesTxnId="";
	    String strLoanerSalesTxnType="";

  	    gmDBManager.setPrepareString("GM_PKG_SET_LOT_TRACK.GM_PKG_FETCH_LOANER_SALES", 3);
	    gmDBManager.registerOutParameter(2, java.sql.Types.VARCHAR);
	    gmDBManager.registerOutParameter(3, java.sql.Types.INTEGER);

	    gmDBManager.setString(1, strTransId); // FG-LN or FG-LE
	    gmDBManager.execute();
	    
	// Adding the parsenull check
	    strLoanerSalesTxnId   = GmCommonClass.parseNull(gmDBManager.getString(2)); // GM-LN
	    strLoanerSalesTxnType = GmCommonClass.parseNull(gmDBManager.getString(3));
	    
	    // Call the Loaner Net Number Procedure

        String strWarehouseType = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSE_TYPE"));
	    String strUserId= GmCommonClass.parseNull((String) hmParam.get("USER_ID"));


      if (!strLoanerSalesTxnId.equals(""))
     {
    	  	  
	  	gmDBManager.setPrepareString("GM_PKG_SET_LOT_TRACK.GM_PKG_SET_LOT_UPDATE", 5);
	  	gmDBManager.setString(1, strLoanerSalesTxnId);
	  	gmDBManager.setString(2, strUserId);
	  	gmDBManager.setString(3, strRefID);
	  	gmDBManager.setString(4, strLoanerSalesTxnType);
	  	gmDBManager.setString(5, strWarehouseType);
	  	gmDBManager.execute();
		   
     }
  }
  
  /**
   * fetchBuildingInfo - This method will fetch the Building info ( Building id and building short name)
   * 
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchBuildingInfo() throws AppError {
	    ArrayList alResult = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_inv_location_module.gm_fch_building_info", 1);
	    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
	    log.debug("alResult"+alResult);
	    gmDBManager.close();
	    return alResult;
	  }
  
  /**
   * syncLotTrackChanges - This method will fetch the loaner lot track info
   * @param hmParam
   * @throws AppError
   */
  public void syncLotTrackChanges(HashMap hmParam) throws AppError{
		log.debug("syncLotTrackChanges hmParam--->>" + hmParam);
	    String strTransId =  GmCommonClass.parseNull((String) hmParam.get("TRANS_ID"));
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_lot_track_data.gm_fch_loaner_lot_track", 1);
	    gmDBManager.setString(1, strTransId);
	    gmDBManager.execute();
	    gmDBManager.commit();
  }
}
