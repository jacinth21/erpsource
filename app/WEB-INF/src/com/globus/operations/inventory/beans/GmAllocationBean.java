// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\\beans\\GmStatusLogBean.java

package com.globus.operations.inventory.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmAllocationBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAllocationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchInventoryUsers - This method will fetch all Inventory Active and Suspended Users
   * 
   * @param No Parameters
   * @exception AppError
   */
  public ArrayList fetchInventoryUsers() throws AppError {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    try {
      // Database Connection

      sbQuery
          .append("   SELECT t5051.C101_USER_ID ID, get_user_name(t5051.C101_USER_ID) NAME,t5051.C901_STATUS STATUS ");
      sbQuery.append("   FROM T5051_ALLOCATE_USER t5051 ");
      sbQuery.append("   WHERE t5051.C901_STATUS IN(93025, 93027)  ");

      log.debug(" Query is " + sbQuery.toString());

      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmAllocationBean:fetchInventoryUsers", "Exception is:" + e);
    }
    return alResult;
  }


  /**
   * reportPickQueueDetails - This method will return an ArrayList - Contains Data from
   * t5050_invpick_assign_detail
   * 
   * @param hmParams
   * @return ArrayList alResult
   * @exception AppError
   */
  public ArrayList reportPickQueueDetails(HashMap hmParams) throws AppError {

    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    int iConditionCount = 0;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPickType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strUser = GmCommonClass.parseNull((String) hmParams.get("USERNUM"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STATUSNUM"));
    String strStatusOper = GmCommonClass.parseNull((String) hmParams.get("STATUSOPER"));
    String strStatusOther = GmCommonClass.parseNull((String) hmParams.get("STATUSOTHER"));
    String strDateType = GmCommonClass.parseNull((String) hmParams.get("DATETYPE"));
    String strStartDate = GmCommonClass.parseNull((String) hmParams.get("STARTDATE"));
    String strEndDate = GmCommonClass.parseNull((String) hmParams.get("ENDDATE"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParams.get("APPLDATEFMT"));
    // For EDC it should filter based on plant id
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "PICK_QUEUE_RPT")); // Rule
    // PICK_QUEUE_RPT:=
    // plant
    String strCompPlantFilter =
        strFilter.equals("") ? strCompanyId : getGmDataStoreVO().getPlantid();

    strPickType = (!strPickType.equals("0")) ? strPickType : "";
    strStatus = (!strStatus.equals("0")) ? strStatus : "";
    strStatusOper = (!strStatusOper.equals("0")) ? strStatusOper : "";
    strStatusOther = (!strStatusOther.equals("0")) ? strStatusOther : "";
    strUser = (!strUser.equals("0")) ? strUser : "";
    strDateType = (!strDateType.equals("0")) ? strDateType : "";
    // Database Connection

    sbQuery
        .append("   SELECT t5050.C5050_INVPICK_ASSIGN_ID ID, t5050.c901_ref_type reftypeid, get_code_name (t5050.c901_ref_type) reftype, ");
    sbQuery.append("        get_user_name (t5050.c101_allocated_user_id) userid, ");
    sbQuery
        .append("        t5050.c5050_ref_id refid, t5050.c901_status statusid, get_code_name (t5050.c901_status) status, ");
    sbQuery.append("        TO_CHAR (t5050.c5050_allocated_dt, '" + strApplDateFmt
        + "' ||' HH:MI:SS') adate, TO_CHAR (t5050.c5050_processed_dt, '" + strApplDateFmt
        + "' || ' HH:MI:SS') pdate");
    // sbQuery.append("        t5050.c5050_allocated_dt adate, t5050.c5050_processed_dt pdate");
    sbQuery.append("   FROM t5050_invpick_assign_detail t5050 ");

    iConditionCount = 0;

    if (!strPickType.equals("") || !strUser.equals("") || !strStatus.equals("")
        || !strDateType.equals("0") || !strDateType.equals("0")) {
      sbQuery.append(" WHERE ");
    }
    if (!strPickType.equals("")) {
      sbQuery.append("  t5050.c901_ref_type = " + strPickType);
      iConditionCount++;
    }
    if (!strUser.equals("")) {
      if (iConditionCount > 0)
        sbQuery.append("  AND ");
      sbQuery.append("  t5050.c101_allocated_user_id = " + strUser);
      iConditionCount++;

    }
    if (!strStatusOther.equals("")) {
      if (iConditionCount > 0)
        sbQuery.append("  AND ");
      if (strStatusOther.equals("93080"))
        sbQuery.append("    t5050.c901_status IS NULL ");
      else if (strStatusOther.equals("93081"))
        sbQuery.append("    (t5050.c901_status < 93006 OR t5050.c901_status = 93007)  ");
      iConditionCount++;
    }

    else if (!strStatus.equals("")) {
      if (iConditionCount > 0)
        sbQuery.append("  AND ");
      if (strStatusOper.equals("90190"))
        sbQuery.append("    t5050.c901_status > " + strStatus);
      else if (strStatusOper.equals("90191"))
        sbQuery.append("    t5050.c901_status < " + strStatus);
      else if (strStatusOper.equals("90192"))
        sbQuery.append("    t5050.c901_status = " + strStatus);
      else if (strStatusOper.equals("90193"))
        sbQuery.append("    t5050.c901_status >= " + strStatus);
      else if (strStatusOper.equals("90194"))
        sbQuery.append("    t5050.c901_status <= " + strStatus);
      else
        sbQuery.append("    t5050.c901_status = " + strStatus);
      iConditionCount++;
    }

    if (strDateType.equals("ALLOCATED")) {
      if (!strStartDate.equals("") && !strEndDate.equals("")) {
        if (iConditionCount > 0)
          sbQuery.append("  AND ");
        sbQuery.append("    trunc(t5050.c5050_allocated_dt) >= TO_DATE ('" + strStartDate + "', '"
            + strApplDateFmt + "') AND ");
        sbQuery.append("    trunc(t5050.c5050_allocated_dt) <= TO_DATE ('" + strEndDate + "', '"
            + strApplDateFmt + "') ");
        iConditionCount++;
      }
    } else if (strDateType.equals("PROCESSED")) {
      if (!strStartDate.equals("") && !strEndDate.equals("")) {
        if (iConditionCount > 0)
          sbQuery.append("  AND ");
        sbQuery.append(" trunc(t5050.c5050_processed_dt) >= TO_DATE ('" + strStartDate + "', '"
            + strApplDateFmt + "') AND ");
        sbQuery.append("  trunc(t5050.c5050_processed_dt) <= TO_DATE ('" + strEndDate + "', '"
            + strApplDateFmt + "') ");
        iConditionCount++;
      }
    } else if (strDateType.equals("CREATEDDATE")) {
      if (!strStartDate.equals("") && !strEndDate.equals("")) {
        if (iConditionCount > 0)
          sbQuery.append("  AND ");
        sbQuery.append("  trunc(t5050.c5050_created_date) >= TO_DATE ('" + strStartDate + "', '"
            + strApplDateFmt + "') AND ");
        sbQuery.append("  trunc(t5050.c5050_created_date) <= TO_DATE ('" + strEndDate + "', '"
            + strApplDateFmt + "') ");
      }
    }
    // Added CompanyId and PlantId Filter
    sbQuery.append(" AND (t5050.C1900_COMPANY_ID =" + strCompPlantFilter);
    sbQuery.append(" OR  t5050.C5040_PLANT_ID =" + strCompPlantFilter);
    sbQuery.append(") AND t5050.c5050_void_fl IS NULL");

    log.debug(" Query is " + sbQuery.toString());


    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alResult;
  }

  /**
   * reportUnAssignedPickQueueDetails - This method will return an ArrayList - Contains all
   * UnAssigned Data (No user assigned to Requests) from t5050_invpick_assign_detail
   * 
   * @return ArrayList alResult
   * @exception AppError
   */
  public ArrayList reportUnAssignedPickQueueDetails() throws AppError {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    int iConditionCount = 0;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    try {
      // Database Connection

      sbQuery
          .append("   SELECT t5050.C5050_INVPICK_ASSIGN_ID ID, t5050.c901_ref_type reftypeid, get_code_name (t5050.c901_ref_type) reftype, get_user_name (t5050.c101_allocated_user_id) userid, t5050.c5050_ref_id refid, t5050.c901_status statusid, get_code_name (t5050.c901_status) status,         TO_CHAR (t5050.c5050_allocated_dt, get_rule_value('DATEFMT','DATEFORMAT') ||' HH:MI:SS') adate, TO_CHAR (t5050.c5050_processed_dt, get_rule_value('DATEFMT','DATEFORMAT')||' HH:MI:SS') pdate ");
      sbQuery.append("   FROM t5050_invpick_assign_detail t5050   ");
      sbQuery.append("   WHERE  t5050.C101_ALLOCATED_USER_ID IS NULL ");

      log.debug(" Query is " + sbQuery.toString());


      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmAllocationBean:reportUnAssignedPickQueueDetails",
          "Exception is:" + e);
    }
    return alResult;
  }

  /**
   * fetchAllocateUsers - This method will return an ArrayList - Contains Data from
   * T5051_ALLOCATE_USER
   * 
   * @param No parameter's
   * @return ArrayList alResult
   * @exception AppError
   */
  public ArrayList fetchAllocateUsers() {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    int iConditionCount = 0;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    try {
      // Database Connection

      sbQuery
          .append("    SELECT C5051_ALLOCATE_USER_ID ID, get_user_name (t5051.c101_user_id) USER_NAME, get_code_name (t5051.c901_status) STATUS, ");
      sbQuery
          .append("          NVL(assigned,0) assigned, NVL(initiated,0) initiated, NVL(wip,0) wip, NVL(suspended,0) suspended, NVL(closedtoday,0) closedtoday  ");
      sbQuery.append("     FROM t5051_allocate_user t5051, t101_user t101,t101_party t101p, ");
      sbQuery.append("          (SELECT   t5050.c101_allocated_user_id, ");
      sbQuery
          .append("                    SUM (DECODE (t5050.c901_status, 93003, 1, 0)) assigned, ");
      sbQuery
          .append("                    SUM (DECODE (t5050.c901_status, 93004, 1, 0)) initiated, ");
      sbQuery.append("                    SUM (DECODE (t5050.c901_status, 93005, 1, 0)) wip, ");
      sbQuery
          .append("                    SUM (DECODE (t5050.c901_status, 93007, 1, 0)) suspended ");
      sbQuery.append("               FROM t5050_invpick_assign_detail t5050 ");
      sbQuery.append("              WHERE t5050.c901_status IN (93003, 93004, 93005, 93007) ");
      sbQuery.append("           GROUP BY t5050.c101_allocated_user_id) t5050_open, ");
      sbQuery.append("          (SELECT   t5050.c101_allocated_user_id, COUNT (1) closedtoday ");
      sbQuery.append("               FROM t5050_invpick_assign_detail t5050 ");
      sbQuery
          .append("              WHERE TRUNC (t5050.c5050_processed_dt) = TRUNC (CURRENT_DATE) ");
      sbQuery.append("           GROUP BY t5050.c101_allocated_user_id) t5050_closedtoday ");
      sbQuery.append("    WHERE t5051.c5051_void_fl IS NULL ");
      sbQuery.append("      AND t5051.c101_user_id = t5050_open.c101_allocated_user_id(+) ");
      sbQuery.append("      AND t101.c101_user_id     = t101p.C101_PARTY_ID ");
      sbQuery.append("      AND t101p.c1900_company_id = '" + getCompId() + "'");
      sbQuery.append("      AND t5051.c101_user_id = t5050_closedtoday.c101_allocated_user_id(+) ");
      sbQuery.append("      AND t101.C101_user_id = t5051.C101_user_id ");
      sbQuery.append("      AND t101.c901_user_status=311");
      sbQuery.append("    ORDER BY t5051.c901_status(+) ");

      log.debug(" Query is " + sbQuery.toString());


      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmAllocationBean:fetchAllocateUsers", "Exception is:" + e);
    }
    return alResult;
  }


  /**
   * savePickQueueDetails - This method will update or save the change of user for the specific
   * order
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void savePickQueueDetails(HashMap hmParams) throws AppError {

    HashMap hmReturn = new HashMap();
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRFINALDATA"));
    log.debug("InputString ==== " + strInputString);
    // Database Connection
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    if (strInputString != null) {
      gmDBManager.setPrepareString("GM_PKG_ALLOCATION.GM_INVPICK_ASSIGN_DETAIL", 1);
      gmDBManager.setString(1, strInputString);
      gmDBManager.execute();
      gmDBManager.commit();
    }
  }

  /**
   * saveAllocateuser - This method will update or save the change of status for specific user or
   * add new user
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void saveAllocateuser(HashMap hmParams) throws AppError {
    HashMap hmReturn = new HashMap();
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRFINALDATA"));
    log.debug("InputString ==== " + strInputString);
    // Database Connection
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    if (strInputString != null) {
      gmDBManager.setPrepareString("GM_PKG_ALLOCATION.GM_ALLOCATE_USER", 1);
      gmDBManager.setString(1, strInputString);
      gmDBManager.execute();
      gmDBManager.commit();
    }
  }


  /**
   * getXmlGridData - This method will return the Xml String
   * 
   * @param hmParamV - contains the Template and Template path
   * @return String XML String
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParamV) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();

    ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
    ArrayList alUsers = (ArrayList) hmParamV.get("RUSERS");
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParamV.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParamV.get("VMFILEPATH"));

    templateUtil.setDataList("alResult", alParam);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setDropDownMaster("alUsers", alUsers);

    return templateUtil.generateOutput();
  }

  /**
   * getXmlGridData_AllocateUser - This method will return the Xml String
   * 
   * @param hmParamV - contains the Template and Template path
   * @return String XML String
   * @exception AppError
   */
  public String getXmlGridData_AllocateUser(HashMap hmParamV) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();

    ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
    ArrayList alStatus = (ArrayList) hmParamV.get("RSTATUS");
    ArrayList alUsers = (ArrayList) hmParamV.get("RUSERS");
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParamV.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParamV.get("VMFILEPATH"));

    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setDropDownMaster("alUsers", alUsers);
    templateUtil.setDropDownMaster("alStatus", alStatus);

    return templateUtil.generateOutput();
  }
}
