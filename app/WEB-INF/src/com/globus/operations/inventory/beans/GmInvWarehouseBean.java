//Source file: C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\\beans\\GmStatusLogBean.java

package com.globus.operations.inventory.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;
import com.globus.common.util.GmTemplateUtil;

public class GmInvWarehouseBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public GmInvWarehouseBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO()); 
	}
	public GmInvWarehouseBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	/* Method : fetchInvWarehouse
       In Parameter : HashMap
       ReturnType : ArrayList

	 **/
	public ArrayList fetchInvWarehouse(HashMap hmParams) throws AppError{
		ArrayList alResult = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		
		String strWarehouseID = GmCommonClass.parseZero((String) hmParams.get("STRWAREHOUSEID"));
		String strLocationID  = GmCommonClass.parseNull((String) hmParams.get("STRLOCATIONID"));
		String strWarehouseType = GmCommonClass.parseNull((String) hmParams.get("STRWAREHOUSETYPE"));
		String strStatusID  = GmCommonClass.parseNull((String) hmParams.get("STRSTATUSID"));
		String WarehouseListValues  = GmCommonClass.parseZero((String) hmParams.get("WarehouseListAll"));
		strWarehouseID = WarehouseListValues.equals("0")?"0":strWarehouseID;
		
		GmDBManager gmDBManager = new GmDBManager();
		
			sbQuery.append("SELECT C5051_INV_WAREHOUSE_ID WAREHOUSEID, C5051_INV_WAREHOUSE_NM WAREHOUSENM,GET_CODE_NAME_ALT(C901_WH_CNTY_ID) WAREHOUSELOCNM,");
			sbQuery.append("C901_WH_CNTY_ID WAREHOUSELOCID, GET_CODE_NAME(C901_WAREHOUSE_TYPE) WAREHOUSETYPENM,C901_WAREHOUSE_TYPE WAREHOUSETYPEID,");
			sbQuery.append("GET_CODE_NAME(C901_STATUS_ID) WAREHOUSESTATUSNM,C901_STATUS_ID WAREHOUSESTATUSID ");
			sbQuery.append(" FROM T5051_INV_WAREHOUSE WHERE C5051_INV_WAREHOUSE_ID IS NOT NULL");  //93310 is for active status.
			sbQuery.append(" AND c5051_inv_warehouse_id NOT IN (SELECT c906_rule_value  FROM t906_rules  WHERE c906_rule_id = 'WHID' AND c906_rule_grp_id = 'RULEWH' AND c906_void_fl IS NULL) "); // FS and Account Warehouse
			if(!strWarehouseID.equals("0"))
			{
			sbQuery.append(" AND C5051_INV_WAREHOUSE_ID = '"+ strWarehouseID +"'");
			}
			if(!strLocationID.equals(""))
			{
				sbQuery.append(" AND C901_WH_CNTY_ID = '"+ strLocationID +"'");
			}
			if(!strWarehouseType.equals(""))
			{  
					sbQuery.append(" AND C901_WAREHOUSE_TYPE = '"+ strWarehouseType +"'");
			}
			if(!strStatusID.equals(""))
			{
					sbQuery.append(" AND C901_STATUS_ID = '"+ strStatusID +"'");
			}		
			log.debug(" Query is "+sbQuery.toString());
			
			gmDBManager.setPrepareString(sbQuery.toString());
			alResult = GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecord());
				
		return alResult;
	}
}