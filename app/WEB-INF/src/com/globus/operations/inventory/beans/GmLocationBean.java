// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\\beans\\GmStatusLogBean.java

package com.globus.operations.inventory.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLocationBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmLocationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmLocationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public ArrayList fetchLocationPartMapping(HashMap hmParams) {
    ArrayList alResult = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    String strLocationID = GmCommonClass.parseNull((String) hmParams.get("LOCATIONID"));
    String strPart = GmCommonClass.parseNull((String) hmParams.get("PARTNUM"));
    String strZone = GmCommonClass.parseNull((String) hmParams.get("ZONENUM"));
    String strAisle = GmCommonClass.parseNull((String) hmParams.get("AISLENUM"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STATUSNUM"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strUserFilter = GmCommonClass.parseNull((String) hmParams.get("USERFILTER"));

    log.debug("Zone --- " + strZone);
    log.debug("Aisle --- " + strAisle);

    DBConnectionWrapper dbCon = null;

    if (strLocationID.equals("") && strPart.equals("") && strZone.equals("0")
        && (strType.equals("0") || strType.equals("")) && strAisle.equals("")
        && (strStatus.equals("0") || strStatus.equals(""))) {
      return null;
    }


    try {
      // Database Connection
      dbCon = new DBConnectionWrapper();

      sbQuery
          .append("   SELECT t5053.C5053_LOCATION_PART_MAP_ID ID, t5052.C5052_LOCATION_ID LOCATION_ID, get_code_name(t5052.C901_LOCATION_TYPE) TYPE, get_code_name(t5052.C901_STATUS) STATUS,  ");
      sbQuery
          .append("   t5053.C205_PART_NUMBER_ID PART,GET_PARTNUM_DESC(t5053.C205_PART_NUMBER_ID) PART_DESC, t5053.C5053_CURR_QTY CURRQTY, t5053.C5053_MIN_QTY MINQTY, t5053.C5053_MAX_QTY MAXQTY  ");
      sbQuery.append("   FROM T5052_LOCATION_MASTER t5052, T5053_LOCATION_PART_MAPPING t5053  ");
      sbQuery.append("   WHERE t5052.C5052_LOCATION_ID =  t5053.C5052_LOCATION_ID (+)  ");
      sbQuery
          .append("   AND t5052.c5051_inv_warehouse_id NOT IN (SELECT c906_rule_value  FROM t906_rules  WHERE c906_rule_id = 'WHID' AND c906_rule_grp_id = 'RULEWH' AND c906_void_fl IS NULL) "); // FS
                                                                                                                                                                                                  // and
                                                                                                                                                                                                  // Account
                                                                                                                                                                                                  // Warehouse
      if (!strLocationID.equals("")) {
        sbQuery.append("   AND  t5052.C5052_LOCATION_ID = '" + strLocationID + "'");
      }
      if (!strPart.equals("")) {
        sbQuery.append("   AND t5053.C205_PART_NUMBER_ID = '" + strPart + "'");
      }
      if (!strZone.equals("0")) {
        sbQuery.append("   AND t5052.C901_ZONE =" + strZone);
      }
      if (!strAisle.equals("")) {
        sbQuery.append("   AND t5052.C5052_AISLE =" + strAisle);
      }
      if (!strType.equals("0")) {
        sbQuery.append("   AND t5052.C901_LOCATION_TYPE =" + strType);
      }
      if (!strStatus.equals("0")) {
        sbQuery.append("   AND t5052.C901_STATUS =" + strStatus);
      }
      if (strUserFilter.equals("yes")) {
        sbQuery.append("   AND t5052.C5052_CREATED_BY =" + strUserID);
      }
      sbQuery.append("   ORDER BY t5052.C5052_LOCATION_ID ");


      log.debug(" Query is " + sbQuery.toString());


      alResult = dbCon.queryMultipleRecords(sbQuery.toString());
      log.debug(" Records is " + alResult.size());

    } catch (Exception e) {
      GmLogError.log("Exception in GmAllocationBean:fetchLocationPartMapping", "Exception is:" + e);
    }
    return alResult;
  }


  public ArrayList reportLocationPartMapping(String strLocationID) throws AppError {
    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_OP_LOCATION_MODULE.GM_FCH_LOCATION_PART_MAPPING", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLocationID);
    gmDBManager.execute();
    ArrayList alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" Records is " + alResult.size());
    log.debug("Exit");
    return alResult;
  }


  /**
   * saveLocationPartDetails - This method will update or save the change of Parts for the specific
   * Location
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void saveNewLocation(HashMap hmParams) throws AppError {
    HashMap hmReturn = new HashMap();
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRFINALDATA"));
    String strZone = GmCommonClass.parseNull((String) hmParams.get("ZONENUM"));
    String strAisle = GmCommonClass.parseNull((String) hmParams.get("AISLENUM"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    log.debug("InputString ==== " + strInputString);
    log.debug(" Type ==== " + strZone);
    log.debug(" Status ==== " + strAisle);
    log.debug(" UserID ==== " + strUserID);

    // Database Connection
    GmDBManager gmDBManager = new GmDBManager();
    GmCommonBean gmCommonBean = new GmCommonBean();

    if (strInputString != null) {
      gmDBManager.setPrepareString("GM_PKG_OP_LOCATION_MODULE.GM_SAV_NEW_LOCATION", 4);
      gmDBManager.setString(1, strInputString);
      gmDBManager.setString(2, strZone);
      gmDBManager.setString(3, strAisle);
      gmDBManager.setString(4, strUserID);

      gmDBManager.execute();
      gmDBManager.commit();
    }

  }

  /**
   * saveLocationPartDetails - This method will update or save the change of Parts for the specific
   * Location
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void saveLocationPartDetails(HashMap hmParams) throws AppError {
    HashMap hmReturn = new HashMap();
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRFINALDATA"));
    String strLocationID = GmCommonClass.parseNull((String) hmParams.get("LOCATIONID"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STATUSNUM"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));

    log.debug("InputString ==== " + strInputString);
    log.debug(" Location ID ==== " + strLocationID);
    log.debug(" Type ==== " + strType);
    log.debug(" Status ==== " + strStatus);

    // Database Connection
    GmDBManager gmDBManager = new GmDBManager();
    GmCommonBean gmCommonBean = new GmCommonBean();

    if (strInputString != null) {
      gmDBManager.setPrepareString("GM_PKG_OP_LOCATION_MODULE.GM_SAV_LOCATION_PART_DETAIL", 5);
      gmDBManager.setString(1, strInputString);
      gmDBManager.setString(2, strLocationID);
      gmDBManager.setString(3, strType);
      gmDBManager.setString(4, strStatus);
      gmDBManager.setString(5, strUserID);

      gmDBManager.execute();
      gmDBManager.commit();
    }

  }

  /**
   * getXmlGridData - This method will return the Xml String
   * 
   * @param hmParamV - contains the Template and Template path
   * @return String XML String
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParamV) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();

    ArrayList alParam = GmCommonClass.parseNullArrayList((ArrayList) hmParamV.get("RLIST"));
    ArrayList alParts = GmCommonClass.parseNullArrayList((ArrayList) hmParamV.get("RPARTS"));
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParamV.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParamV.get("VMFILEPATH"));

    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setDropDownMaster("alParts", alParts);

    return templateUtil.generateOutput();
  }


  public HashMap fetchLocationDetails(String strLocationID) throws AppError {

    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_OP_LOCATION_MODULE.GM_FCH_LOCATION_DETAILS", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLocationID);
    gmDBManager.execute();
    HashMap hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" Records is " + hmResult.size());
    log.debug("Exit");
    return hmResult;
  }

  public String getPartNumDesc(String strPartNum) throws AppError {

    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("GM_PKG_OP_LOCATION_MODULE.GM_GET_PART_NUM_DESC", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.execute();
    HashMap hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" Records is " + hmResult.size());
    log.debug("Exit");
    return ((String) hmResult.get("PART_DESC"));
  }

  /*
   * function to fetch the Location Part mapping details.
   * 
   * @author Ritesh
   */
  public ArrayList fetchPartLocationDetails(HashMap hmValues) throws AppError {

    log.debug("hmValues===" + hmValues);
    GmDBManager gmDBManager = new GmDBManager();
    String strTxnId = GmCommonClass.parseNull((String) hmValues.get("TXNID"));
    String strTxnType = GmCommonClass.parseNull((String) hmValues.get("TXNTYPE"));
    String strPnum = GmCommonClass.parseNull((String) hmValues.get("PNUMSTR"));
    gmDBManager.setPrepareString("gm_pkg_op_inv_scan.gm_fch_part_location_dtl", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.setString(3, strPnum);
    gmDBManager.setString(4, "PORTAL");
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();
    log.debug(" Records is " + alReturn);
    return alReturn;
  }

  /*
   * function to save the Location Part mapping details.
   * 
   * @author Ritesh
   */
  public void savePartLocationDetails(GmDBManager gmDBManager, String strTxnId, String strPnumLcn,
      String strOperation, String strUserID) throws AppError {

    gmDBManager.setPrepareString("gm_pkg_op_inv_scan.gm_upd_inventory_loc_qty", 4);
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strPnumLcn);
    gmDBManager.setString(3, strOperation);
    gmDBManager.setString(4, strUserID);
    gmDBManager.execute();

  }

  /**
   * fetchInvLocationPartMappingByID - This method will fetch location part mapping details for
   * particular location id
   * 
   * @param String strLocationID
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchFieldSalesPartMappingDtls(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strLocID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strWarehouse = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSEID"));
    String strFieldSales = GmCommonClass.parseZero((String) hmParam.get("FIELDSALES"));
    String strAccount = GmCommonClass.parseZero((String) hmParam.get("ACCOUNT"));
    String strLocationType = GmCommonClass.parseZero((String) hmParam.get("LOCATIONTYPE"));
 
    log.debug("strLocationType========= "+strLocationType);
    String strPartNumFormat = "";
    // Regular Expression
    if (!strPartNum.equals("")) {
      strPartNumFormat = strPartNum.replaceAll(",", "|");
    }
    strFieldSales = strWarehouse.equals("5") ? strAccount : strFieldSales;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_OP_INV_FIELD_SALES_RPT.GM_FCH_FS_PART_MAPPING", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.setString(1, strWarehouse);
    gmDBManager.setString(2, strPartNumFormat);
    gmDBManager.setString(3, strLocID);
    gmDBManager.setString(4, strFieldSales);
    gmDBManager.setString(5, strLocationType);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
  //  log.debug("alReturn========= "+alReturn);
    gmDBManager.close();
    return alReturn;
  }
  
  /**
   * fetchInvLocationPartMappingByID - This method will fetch location part mapping details for
   * particular location id
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchDealerPartMappingDtls(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
  //  String strLocID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strWarehouse = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSEID"));
    String strFieldSales = GmCommonClass.parseZero((String) hmParam.get("FIELDSALES"));
    String strAccount = GmCommonClass.parseZero((String) hmParam.get("ACCOUNT"));
    String strSysId = GmCommonClass.parseNull((String) hmParam.get("SYSIDS"));
    String strOutType = GmCommonClass.parseNull((String) hmParam.get("ACCOUTPUTTYPE"));


    log.debug("strWarehouse========= " + strWarehouse);
    String strLocationType = GmCommonClass.parseZero((String) hmParam.get("LOCATIONTYPE"));
    log.debug("strLocationType========= " + strLocationType);
    String strPartNumFormat = "";
    // Regular Expression
    if (!strPartNum.equals("")) {
      strPartNumFormat = strPartNum.replaceAll(",", "|");
    }
    strFieldSales = strWarehouse.equals("5") ? strAccount : strFieldSales;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_OP_INV_FIELD_SALES_RPT.gm_fch_acct_part_mapping", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.setString(1, strWarehouse);
    gmDBManager.setString(2, strPartNumFormat);
    gmDBManager.setString(3, strFieldSales);
    gmDBManager.setString(4, strLocationType);
    gmDBManager.setString(5, strSysId);
    gmDBManager.setString(6, strOutType);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    log.debug("alReturn========= " + alReturn);
    gmDBManager.close();
    return alReturn;
  }  
  
  
  

  /**
   * @throws ParseException fetchInvLocationPartMappingByID - This method will fetch location part
   *         mapping details for particular location id
   * @param String strLocationID
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchInvLocationLog(HashMap hmParam) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    String strLocId = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strHistoricDT = GmCommonClass.parseNull((String) hmParam.get("HISTORICDATA"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    Date dtFromDt = GmCommonClass.getStringToDate(strFromDt, strDateFmt);
    Date dtToDt = GmCommonClass.getStringToDate(strToDt, strDateFmt);
    log.debug("dtFromDt"+dtFromDt);
    log.debug("dtToDt"+dtToDt);

    if (dtFromDt != null) {
      dtFromDt = new java.sql.Date(dtFromDt.getTime());
    }

    if (dtToDt != null) {
      dtToDt = new java.sql.Date(dtToDt.getTime());
    }
    strHistoricDT = (strHistoricDT.equals("on")) ? "Y" : "N";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    if (strHistoricDT.equals("Y")) {
      gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    }
    gmDBManager.setPrepareString("GM_PKG_OP_INV_FIELD_SALES_RPT.GM_FCH_INV_LOC_LOG", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strLocId);
    gmDBManager.setDate(3, dtFromDt);
    gmDBManager.setDate(4, dtToDt);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();
    return alResult;
  }
  
  
  /**
   * @throws ParseException fetchInvLocationPartMappingByID - This method will fetch location part
   *         mapping details for particular location id
   * @param String strLocationID
   * @return ArrayList
   * @exception
   */
  public ArrayList fetchSetLocationLog(HashMap hmParam) throws AppError, Exception {
    ArrayList alResult = new ArrayList();
    String strSetLotMasterId = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    String strHistoricDT = GmCommonClass.parseNull((String) hmParam.get("HISTORICDATA"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strLot = GmCommonClass.parseNull((String) hmParam.get("lot"));
    log.debug("strLot"+strLot);
    Date dtFromDt = GmCommonClass.getStringToDate(strFromDt, strDateFmt);
    Date dtToDt = GmCommonClass.getStringToDate(strToDt, strDateFmt);
    log.debug("dtFromDt"+dtFromDt);
    log.debug("dtToDt"+dtToDt);

    if (dtFromDt != null) {
      dtFromDt = new java.sql.Date(dtFromDt.getTime());
    }

    if (dtToDt != null) {
      dtToDt = new java.sql.Date(dtToDt.getTime());
    }
    strHistoricDT = (strHistoricDT.equals("on")) ? "Y" : "N";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    if (strHistoricDT.equals("Y")) {
      gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    }
    log.debug("strSetLotMasterId"+strSetLotMasterId);
    gmDBManager.setPrepareString("GM_PKG_OP_INV_FIELD_SALES_RPT.GM_FCH_ACCT_LOT_LOG", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strSetLotMasterId);
    gmDBManager.setDate(3, dtFromDt);
    gmDBManager.setDate(4, dtToDt);   
    gmDBManager.setString(5, strLot);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
    gmDBManager.close();
    return alResult;
  }


}
