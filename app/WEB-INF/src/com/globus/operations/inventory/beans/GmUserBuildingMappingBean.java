package com.globus.operations.inventory.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmUserBuildingMappingBean extends GmBean  {
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	 
	 public GmUserBuildingMappingBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }
  /**
   * Constructor will populate company info.
   * @param gmDataStore
   */
	  public GmUserBuildingMappingBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	
	 
	  /**
	   * This method used to fetch building list
	   * @param strCompanyId
	   * @return
	   * @throws AppError
	   */
	  public ArrayList fetchBuildingDetails() throws AppError {
		    ArrayList alReturn = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_op_user_bldg_mapping.gm_fch_building_list", 1);
		    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		    gmDBManager.execute();
		    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		    gmDBManager.close();
		    return alReturn;
		  }
	  
	  /**
	   * This method used to save users to building
	   * @param hmParam
	   * @return
	   * @throws AppError
	   */
	  public String saveMultiUserGroupBuilding(HashMap hmParam) throws AppError {
		    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPSTR"));
		    String strGID = GmCommonClass.parseNull((String) hmParam.get("GID"));
		    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		    String strBuildingId = GmCommonClass.parseNull((String) hmParam.get("BID"));
            String resultBuildId = "";
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_op_user_bldg_mapping.gm_upd_user_group_map", 4);  
		    gmDBManager.setString(1, strInputString);
		    gmDBManager.setString(2, strGID);
		    gmDBManager.setString(3, strCompanyId);
		    gmDBManager.setString(4, strBuildingId);
		    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		    gmDBManager.execute();
		    resultBuildId = gmDBManager.getString(4);
		    gmDBManager.commit();
		    return resultBuildId;
       }
	  
	 
	  /**
	   * This method used to fetch assgined user list to groups ,company then building
	   * @param hmParam
	   * @return
	   * @throws AppError
	   */
	  public ArrayList fetchUserGroupMappingDetails(HashMap hmParam) throws AppError {
		    ArrayList alReturn = new ArrayList();
		    String strGrpMappingID = GmCommonClass.parseNull((String) hmParam.get("GRPMAPPINGID"));
		    String strUser = GmCommonClass.parseNull((String) hmParam.get("USERNM"));
		    String strGroupName = GmCommonClass.parseNull((String) hmParam.get("GROUPNAME"));
		    String strDefaultGrp = GmCommonClass.parseNull((String) hmParam.get("DEFAULTGRP"));
		    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		    String strDept = GmCommonClass.parseNull((String) hmParam.get("USERDEPT"));
		    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		    String strBuildId = GmCommonClass.parseZero((String)hmParam.get("BUILDID"));
		    
		    strDefaultGrp = strDefaultGrp.equals("on") ? "Y" : "";
		    
		    if (strGrpMappingID.equalsIgnoreCase("")) {
		      strGrpMappingID = null;
		    }
		    if (strUser.equalsIgnoreCase("0")) {
		      strUser = null;
		    }
		    if (strGroupName.equalsIgnoreCase("0")) {
		      strGroupName = null;
		    }
		    if (strUserId.equalsIgnoreCase("")) {
		      strUserId = null;
		    }
		    if (strDefaultGrp.equalsIgnoreCase("")) {
		      strDefaultGrp = null;
		    }
		    if (strDept.equalsIgnoreCase("0")) {
		      strDept = null;
		    }
		    if (strCompanyId.equalsIgnoreCase("0")) {
		      strCompanyId = null;
		    }
		    if (strBuildId.equalsIgnoreCase("0")) {
		    	strBuildId = null;
			    }
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_op_user_bldg_mapping.gm_fch_selected_user_group", 8);
		    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
		    gmDBManager.setString(1, strGroupName);
		    gmDBManager.setString(2, strUser);
		    gmDBManager.setString(3, strUserId);
		    gmDBManager.setString(4, strGrpMappingID);
		    gmDBManager.setString(5, strDept);
		    gmDBManager.setString(6, strCompanyId);
		    gmDBManager.setString(7, strBuildId);
		    gmDBManager.execute();
		    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
		    gmDBManager.close();
		    return alReturn;
		  }
}