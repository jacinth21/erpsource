package com.globus.operations.inventory.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import oracle.jdbc.driver.OracleTypes;


public class GmActivateLocationBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	  GmCommonClass gmCommon = new GmCommonClass();
	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmActivateLocationBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	
	  /**
	   * saveLocationStatus():function to fetch the Insert id and revision number for the part numbers based on the txn
	   * @throws AppError
	   * @Author MSelvamani
	   */
	  public HashMap saveLocationStatus(HashMap hmParam) throws AppError {
//		  String strSuccess="";
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
			log.debug("hmReturn in saveLocationStatus() " +hmParam);
			String statusId = GmCommonClass.parseNull((String) hmParam.get("STRSTATUSTYPE"));
			String locId = GmCommonClass.parseNull((String) hmParam.get("STRLOCATIONID"));
			String userId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
			String warehouseId = GmCommonClass.parseNull((String) hmParam.get("STRWAREHOUSEID"));
			gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse_loc_txn.gm_upd_warhouse_location", 6);
			gmDBManager.setString(1, locId);
			gmDBManager.setString(2, statusId);
			gmDBManager.setString(3, userId);
			gmDBManager.setString(4, warehouseId);
			gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
			gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
			gmDBManager.execute();
			String strSuccess =  GmCommonClass.parseNull(gmDBManager.getString(5));
			String strError =  GmCommonClass.parseNull(gmDBManager.getString(6));
			gmDBManager.commit();
			hmParam.put("SUCCESS", strSuccess);
			hmParam.put("ERROR", strError);
			return hmParam;
		}

}
