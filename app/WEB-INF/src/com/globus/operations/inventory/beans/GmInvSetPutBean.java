package com.globus.operations.inventory.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInvSetPutBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  GmCommonClass gmCommon = new GmCommonClass();

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmInvSetPutBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * 
   * fetchPendingPutRequests - This method will fetch Pending Fg
   * 
   * @param HashMap - hmParam
   * @exception AppError
   */
  public ArrayList fetchPendingPutRequests(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strScreenType = GmCommonClass.parseNull((String) hmParam.get("PTYPE"));
    gmDBManager.setPrepareString("gm_pkg_op_set_put_rpt.gm_fch_pending_put_requests", 2);
    gmDBManager.setString(1, strScreenType);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }

  /**
   * validateLocationId - This method will validate the Location ID
   * 
   * @param String strLocationID
   * @exception AppError
   */
  public String validateLocationId(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmResult = new HashMap();
    String strlocID = GmCommonClass.parseNull((String) hmParams.get("LOCATIONID"));
    String strWareHouseID = GmCommonClass.parseNull((String) hmParams.get("WAREHOUSEID"));
    gmDBManager.setPrepareString("gm_pkg_op_set_validate.gm_validate_location_id", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strlocID);
    gmDBManager.setString(2, strWareHouseID);
    gmDBManager.execute();
    hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    String strLocationType = "";
    String strLocationID = "";
    strLocationID = GmCommonClass.parseNull((String) hmResult.get("LOCID"));
    strLocationType = GmCommonClass.parseNull((String) hmResult.get("TYPEID"));
    return strLocationType + '^' + strLocationID;
  }

  /**
   * savePendingPutSets - This method will update the location
   * 
   * @param hmParams - contains the InputString - which contains the Delminted string with all
   *        changed records
   * @exception AppError
   */
  public void savePendingPutSets(HashMap hmParams) throws AppError {
    // Input String - Deliminted String with all changed records
    String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRINPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    // Database Connection
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_op_set_put_txn.gm_sav_set_put_batch", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }
}
