package com.globus.operations.inventory.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import oracle.jdbc.driver.OracleTypes;
import java.util.HashMap;

/**
* 
*
* @author Swetha
*/
public class GmRerunTxnSplitBean extends GmBean{

	/**
	* 
	*
	* Code to Initialize the Logger Class.
	*/ 
	Logger log = GmLogger.getInstance(this.getClass().getName());
	 
     /**
	   * Constructor 
	   * 
	   * 
	   */
	 public GmRerunTxnSplitBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }
  /**
   * Constructor will populate company info.
   * @param gmDataStore
   */
	
	public GmRerunTxnSplitBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		
	}
	
	
	
	/**
	 * validateRerunTxn - This method is used to process to validate the transaction ID  and transaction type  For PMT-36676
	 * 
	 * @param strInhouseId
	 *  * @throws AppError
	 */
	public HashMap processSplitTransactionByLoc(HashMap hmParam)throws AppError{
		
		String strTransactionId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
	    String strTransactionType = GmCommonClass.parseNull((String)hmParam.get("TXNTYPE"));
	    String strRerunValidation = "";
	    String strRerunTxnId = "";
	    HashMap hmReturn = new HashMap();
		
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_storage_building.gm_process_split_txn_ByLoc", 4);
	    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
	    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
	    gmDBManager.setString(1, strTransactionId);
	    gmDBManager.setString(2, strTransactionType);
	  
	    gmDBManager.execute();
	    strRerunValidation = GmCommonClass.parseNull(gmDBManager.getString(3));
	    strRerunTxnId = GmCommonClass.parseNull(gmDBManager.getString(4));
		gmDBManager.commit();
	    
	    hmReturn.put("VALIDATIONMSG", strRerunValidation);
	    hmReturn.put("TRANSID", strRerunTxnId);
		
	    return hmReturn;
		
	}

}
