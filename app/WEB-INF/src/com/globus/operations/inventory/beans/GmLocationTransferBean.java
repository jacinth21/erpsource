package com.globus.operations.inventory.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;


import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;
import oracle.jdbc.driver.OracleTypes;



public class GmLocationTransferBean extends GmBean{
Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public GmLocationTransferBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());   
	  }

	public GmLocationTransferBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }

	/**
	 * saveBulkLocation - This method to Bulk Qty to Location
	 * @return String 
	 * @exception Apperror
	 */
	public String saveBulkLocation(HashMap hmParams)throws AppError 
	{
		log.debug("hmParams ::: " + hmParams);
		String strInputString = (String) hmParams.get("VSTRING");
		String strUserId = (String) hmParams.get("USERID");
		String strWHType = GmCommonClass.parseNull((String) hmParams.get("WHTYPE"));
		String strResult = "";
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	   
		gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse_loc_txn.gm_sav_bulk_location", 4);
		gmDBManager.setString(1, strInputString);
		gmDBManager.setString(2, strWHType);
		gmDBManager.setString(3, strUserId);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.execute();
		strResult = gmDBManager.getString(4);
		gmDBManager.commit();
		return strResult;
	}
	
}
