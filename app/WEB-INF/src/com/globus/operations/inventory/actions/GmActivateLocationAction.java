package com.globus.operations.inventory.actions;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.operations.inventory.beans.GmActivateLocationBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;

import com.globus.operations.inventory.forms.GmActivateLocationForm;

public class GmActivateLocationAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchWareHouseDtl - to Load the Warehouse Details for Activate/Inactivate
	 * Location
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return to JSP
	 * @throws Exception
	 */
	public ActionForward fetchWareHouseDtl(ActionMapping actionMapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ArrayList alWareHouse = new ArrayList();
		ArrayList alStatus = new ArrayList();
		instantiate(request, response);
		String strDeptId = "";
	    String strWHRType = "";
	    String strCodeGrp = "LOCWT";
		GmActivateLocationForm gmActiveLocationForm = (GmActivateLocationForm) form;
		gmActiveLocationForm.loadSessionParameters(request);
		  strDeptId = GmCommonClass.parseNull(gmActiveLocationForm.getDeptId());
		GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
		HashMap hmParams = new HashMap();
		 hmParams.put("TXNTYPE", strWHRType);
		    hmParams.put("CODEGRP", strCodeGrp);
		    hmParams.put("DEPTID", strDeptId);
		GmActivateLocationBean gmActiveLocationBean = new GmActivateLocationBean(getGmDataStoreVO());
		alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouseDtl(hmParams));// Used to fetch warehouse types
		alStatus = GmCommonClass.getCodeListNotIn("LOCSS", "93202");// Used to fetch status types
		gmActiveLocationForm.setAlWareHouse(alWareHouse);
		gmActiveLocationForm.setAlStatus(alStatus);
		return actionMapping.findForward("gmActivateLocation");
	}

	/**
	 * saveLocationStatus - to Load the Change Lot# screen
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return to JSP
	 * @throws Exception
	 */
	public ActionForward saveLocationStatus(ActionMapping actionMapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HashMap hmParam = new HashMap();
		ArrayList alWareHouse = new ArrayList();
		ArrayList alStatus = new ArrayList();
		String strDeptId = "";
		String strWHRType = "";
		String strCodeGrp = "LOCWT";
		// call the instantiate method
		instantiate(request, response);
		GmActivateLocationForm gmActiveLocationForm = (GmActivateLocationForm) form;
		gmActiveLocationForm.loadSessionParameters(request);
		GmActivateLocationBean gmActiveLocationBean = new GmActivateLocationBean(getGmDataStoreVO());
		GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
		hmParam = GmCommonClass.getHashMapFromForm(gmActiveLocationForm);
		hmParam = gmActiveLocationBean.saveLocationStatus(hmParam);
		strDeptId = GmCommonClass.parseNull(gmActiveLocationForm.getDeptId());
		gmActiveLocationForm.setStrSuccess(GmCommonClass.parseNull((String) hmParam.get("SUCCESS")));
		gmActiveLocationForm.setStrError(GmCommonClass.parseNull((String) hmParam.get("ERROR")));
		HashMap hmParams = new HashMap();
		hmParams.put("TXNTYPE", strWHRType);
		hmParams.put("CODEGRP", strCodeGrp);
		hmParams.put("DEPTID", strDeptId);
		alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouseDtl(hmParams));// Used to fetch warehouse types
		alStatus = GmCommonClass.getCodeListNotIn("LOCSS", "93202");
		gmActiveLocationForm.setAlWareHouse(alWareHouse);
		gmActiveLocationForm.setAlStatus(alStatus);
		hmParam = GmCommonClass.getHashMapFromForm(gmActiveLocationForm);
		log.debug("saveLocationStatus  --- > " + hmParam);
		return actionMapping.findForward("gmActivateLocation");
	}

}
