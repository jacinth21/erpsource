package com.globus.operations.inventory.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.inventory.beans.GmInvSetPutBean;
import com.globus.operations.inventory.forms.GmPendingPutSetForm;

public class GmPendingPutSetAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  HashMap hmParam = new HashMap();
  HashMap hmAccess = new HashMap();
  HashMap hmTemplateParam = new HashMap();

  /**
   * loadPendingPutSets
   * 
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadPendingPutSets(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPutSetForm gmPendingSetForm = (GmPendingPutSetForm) actionForm;
    // Bean Initialization
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvSetPutBean gmInvSetPutBean = new GmInvSetPutBean(getGmDataStoreVO());
    gmPendingSetForm.loadSessionParameters(request);
    ArrayList alPendingPutSet = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPendingSetForm);
    String strParamType = GmCommonClass.parseNull((String) hmParam.get("PTYPE"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // to get the access permission
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007")); // Location Part Mapping Supervisor
    String strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    gmPendingSetForm.setAccessFlag(strAccessFl);

    alPendingPutSet =
        GmCommonClass.parseNullArrayList(gmInvSetPutBean.fetchPendingPutRequests(hmParam));

    // Get XML Grid Data
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", "strSessCompanyLocale");
    hmTemplateParam.put("TEMPLATE_NAME", "GmPendingPutSets.vm");
    hmTemplateParam.put("TEMPLATE_PATH", "operations/inventory/templates");
    hmTemplateParam.put("PARAMTYPE", strParamType);
    // XML String
    String strXmlString = getXmlGridData(alPendingPutSet, hmTemplateParam);
    gmPendingSetForm.setGridXmlData(strXmlString);

    return actionMapping.findForward("GmPendingPutSetList");
  }

  /**
   * savePendingPutSets
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward savePendingPutSets(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPutSetForm gmPendingSetForm = (GmPendingPutSetForm) actionForm;
    // Bean Initialization
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvSetPutBean gmInvSetPutBean = new GmInvSetPutBean(getGmDataStoreVO());

    gmPendingSetForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmPendingSetForm);

    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strWarehouseID = GmCommonClass.parseNull((String) hmParam.get("WAREHOUSEID")); // 4
                                                                                          // inhouse
                                                                                          // warehouse
    String strType = strWarehouseID.equals("4") ? "IN" : "CNLN";
    HashMap hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007"));// Location Part Mapping Supervisor
    String strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    gmPendingSetForm.setAccessFlag(strAccessFl);

    if (strOpt.equals("Save")) {
      gmInvSetPutBean.savePendingPutSets(hmParam);
    }

    String strRedirectURL =
        "/gmPendingPutSet.do?method=loadPendingPutSets&pType=" + strType + "&wareHouseID="
            + strWarehouseID;
    return actionRedirect(strRedirectURL, request);
  }

  /**
   * @getXmlGridData xml content generation for Field Sales lock grid
   * @author mmuthusamy
   * @param array list and hash map
   * @return String
   */

  private String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alDropDownDistName = new ArrayList();
    ArrayList alDropDownAuditor = new ArrayList();
    ArrayList alDropDownDistStatus = new ArrayList();
    String strTemplateName = "";
    String strTemplatePath = "";

    strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE_NAME"));
    strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE_PATH"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("strSessCompanyLocale"));

    templateUtil.setDataList("alResult", alResult);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.inventory.GmPendingPutSetList", strSessCompanyLocale));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setTemplateName(strTemplateName);

    return templateUtil.generateOutput();
  }

  /**
   * validateLocation
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward validateLocation(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPutSetForm gmPendingSetForm = (GmPendingPutSetForm) form;
    // Bean Initialization
    GmInvSetPutBean gmInvSetPutBean = new GmInvSetPutBean(getGmDataStoreVO());
    hmParam = GmCommonClass.getHashMapFromForm(gmPendingSetForm);

    gmPendingSetForm.loadSessionParameters(request);

    String strLocationId = GmCommonClass.parseNull(request.getParameter("locationId"));
    String strOutMsg = gmInvSetPutBean.validateLocationId((hmParam));
    log.debug(" strOutMsg ==> " + strOutMsg);
    response.setContentType("text/plain");
    PrintWriter pw = response.getWriter();

    pw.write(strOutMsg);
    pw.flush();
    return null;
  }
}
