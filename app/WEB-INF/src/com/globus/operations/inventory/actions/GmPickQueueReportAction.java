package com.globus.operations.inventory.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.inventory.beans.GmAllocationBean;
import com.globus.operations.inventory.forms.GmPickQueueReportForm;

public class GmPickQueueReportAction extends GmAction {

  /**
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      // Form Intitalization
      GmPickQueueReportForm gmPickQueueForm = (GmPickQueueReportForm) actionForm;
      // Bean Initalization
      GmAllocationBean gmAllocationListBean = new GmAllocationBean(getGmDataStoreVO());
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code
      HttpSession session = request.getSession(false);
      String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
      HashMap hmParams = new HashMap();
      ArrayList alReportList = new ArrayList();
      ArrayList alStatus = new ArrayList();
      ArrayList alStatus1 = new ArrayList();
      ArrayList alStatusOther = new ArrayList();
      ArrayList alTypes = new ArrayList();
      ArrayList alUsers = new ArrayList();
      String strOpt = "";
      String strHAction = "";
      GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
      GmCommonClass gmCommonClass = new GmCommonClass();
      GmCalenderOperations gmCal = new GmCalenderOperations();
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      // Get Hidden Parameter's
      strOpt = gmCommonClass.parseNull(gmPickQueueForm.getStrOpt());
      strHAction = gmCommonClass.parseNull(gmPickQueueForm.getHaction());
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      log.debug("strOpt:" + strOpt + " --strHAction:" + strHAction);

      alTypes = gmCommonClass.getCodeList("INVPT");
      // alUsers = gmLogonBean.getEmployeeList("300", "V"); // have to be changed

      alUsers = gmAllocationListBean.fetchInventoryUsers();

      alStatus = gmCommonClass.getCodeList("INVSN");
      alStatus1 = gmCommonClass.getCodeList("CPRSN");
      alStatusOther = gmCommonClass.getCodeList("INVOS");

      gmPickQueueForm.setAlTypes(alTypes);
      gmPickQueueForm.setAlUsers(alUsers);
      gmPickQueueForm.setAlStatus(alStatus);
      gmPickQueueForm.setAlStatus1(alStatus1);
      gmPickQueueForm.setAlStatusOther(alStatusOther);

      // String strTodayDate = (String) session.getAttribute("strSessTodaysDate");
      String strTodayDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      if (!strHAction.equals("rptReload") && !strOpt.equals("save")) {
        gmPickQueueForm.setHaction("initiating");
      }
      hmParams = GmCommonClass.getHashMapFromForm(gmPickQueueForm);

      if (strOpt.equals("save")) {
        gmAllocationListBean.savePickQueueDetails(hmParams);
      }
      hmParams.put("APPLDATEFMT", strApplDateFmt);
      if (strHAction.equals("rptReload") || strOpt.equals("save")) {
        log.debug("Inside the Save ----- : " + hmParams);
        alReportList =
            GmCommonClass.parseNullArrayList(gmAllocationListBean.reportPickQueueDetails(hmParams));
        log.debug("alReportList:" + alReportList);
        gmPickQueueForm.setAlReportList(alReportList);
        // gmPickQueueForm.setHaction("initiating");


      } else // be default show all unassigned list
      {
        hmParams.put("STARTDATE", strTodayDate);
        hmParams.put("ENDDATE", strTodayDate);
        hmParams.put("DATETYPE", "CREATEDDATE");
        alReportList =
            GmCommonClass.parseNullArrayList(gmAllocationListBean.reportPickQueueDetails(hmParams));
        log.debug("alReportList:" + alReportList);
        gmPickQueueForm.setStatusOther("93080");
        gmPickQueueForm.setAlReportList(alReportList);
        gmPickQueueForm.setHaction("Load");
        gmPickQueueForm.setDateType(gmCommonClass.parseNull((String) hmParams.get("DATETYPE")));
        gmPickQueueForm.setStartDate(gmCommonClass.parseNull((String) hmParams.get("STARTDATE")));
        gmPickQueueForm.setEndDate(gmCommonClass.parseNull((String) hmParams.get("ENDDATE")));
      }
      gmPickQueueForm.setStrOpt("REPORT");
      hmParams = GmCommonClass.getHashMapFromForm(gmPickQueueForm);
      gmPickQueueForm =
          (GmPickQueueReportForm) GmCommonClass.getFormFromHashMap(gmPickQueueForm, hmParams);

      // Get XML Grid Data
      HashMap hmParamV = new HashMap();
      hmParamV.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmParamV.put("VMFILEPATH", "properties.labels.operations.inventory.gmInventoryPickQueue");
      hmParamV.put("TEMPLATE", "GmInventoryPickQueue.vm");
      hmParamV.put("TEMPLATEPATH", "operations/inventory/templates");
      hmParamV.put("RLIST", alReportList);
      hmParamV.put("RUSERS", alUsers);

      // XML String
      String strXmlString = gmAllocationListBean.getXmlGridData(hmParamV);

      log.debug(" XML STRING :   " + strXmlString);

      gmPickQueueForm.setGridXmlData(strXmlString);


    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return actionMapping.findForward("report");

  }

}
