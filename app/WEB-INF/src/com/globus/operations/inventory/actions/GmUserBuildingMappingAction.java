package com.globus.operations.inventory.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.actions.GmAction;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmUserMgmtBean;
import com.globus.operations.inventory.beans.GmUserBuildingMappingBean;
import com.globus.operations.inventory.forms.GmUserBuildingMappingForm;

public class GmUserBuildingMappingAction extends GmAction{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
/**
 * This method used to show the Building to User Mapping page 
 * @param mapping,form,request,response
 * @return
 * @throws AppError,Exception
 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	          HttpServletResponse response) throws AppError, Exception {
			    instantiate(request, response);
			    String strDispatchTo = "gmUserBuildingMapping";
			    String strDepID = "";
			    String strCompanyId = "";
			    String strGroupId = "";
			    String strOpt = "";
			    String strGrpId = "";
			    String strBuildingId ="";
			    String strAction = "";
			    String strInputString = "";
			    String strOptVal = "";
			    String strResult ="";
			    String strSaveBuildId = "";
			    String strSelectBuildId = "";
			    
			    ArrayList alUser = new ArrayList();
			    ArrayList alSecGrp = new ArrayList();
			    ArrayList alBuildingList = new ArrayList();
			    ArrayList alResult = new ArrayList();
			    ArrayList alSelectedUser = new ArrayList(); 
			    
			    HashMap hmParam = new HashMap();
			    HashMap hmVal = new HashMap();
		    
			    GmUserBuildingMappingForm gmUserBuildingMappingForm = (GmUserBuildingMappingForm) form;
			    gmUserBuildingMappingForm.loadSessionParameters(request);
			    GmUserBuildingMappingBean gmUserBuildingMappingBean = new GmUserBuildingMappingBean(getGmDataStoreVO());
			   
			    GmUserMgmtBean gmUserMgmtBean = new GmUserMgmtBean(getGmDataStoreVO()); 
			    alSecGrp=GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("BLDGRP"));  // fetch the Groups 
			    gmUserBuildingMappingForm.setAlSecGrp(alSecGrp);
			    HashMap hmUserlist = gmUserMgmtBean.loadUserMgmtLists();
			    gmUserBuildingMappingForm.setAlCompany((ArrayList) hmUserlist.get("COMPANY"));   // fetch Company List and set it
			    
			    alBuildingList = GmCommonClass.parseNullArrayList(gmUserBuildingMappingBean.fetchBuildingDetails());  // fetch the Building list
			    gmUserBuildingMappingForm.setAlBuildingList(alBuildingList);
			   
	 		    strOptVal = GmCommonClass.parseNull(gmUserBuildingMappingForm.getOptVal());
			    strOpt = gmUserBuildingMappingForm.getStrOpt();
			    strAction = gmUserBuildingMappingForm.getHaction(); 
			    
			    strGrpId = gmUserBuildingMappingForm.getSecGrp();
			    strBuildingId = gmUserBuildingMappingForm.getBuildingId(); 
		         
		        strCompanyId = gmUserBuildingMappingForm.getCompanyId();
		        if (strOpt.equals("SECUSR") ){
			    	  hmParam.put("GROUPNAME",strGrpId);
				      hmParam.put("USERNM", "0");
				      hmParam.put("GRPMAPPINGID", "");
				      hmParam.put("USERDEPT", strDepID);
				      hmParam.put("COMPANYID", strCompanyId);
				      if(!strCompanyId.equals("")==true && !strGrpId.equals("")==true ){
				      alUser = GmCommonClass.parseNullArrayList(gmUserBuildingMappingBean.fetchUserGroupMappingDetails(hmParam)); // Assigned users list to group and company
				      
				      if(alUser.isEmpty()==false){
				    	  gmUserBuildingMappingForm.setShowUser("USER");
				    	  gmUserBuildingMappingForm.setAlUserList(alUser);
				        }
				      }
			     }
			     if(strAction.equals("save")){
			    	 strInputString = GmCommonClass.parseNull(gmUserBuildingMappingForm.getHinputstring());
			         hmParam.put("HINPSTR", strInputString);
			         hmParam.put("GID", strGrpId);
			         hmParam.put("BID", strBuildingId);
			         hmParam.put("COMPANYID", strCompanyId);
			         strSaveBuildId=gmUserBuildingMappingBean.saveMultiUserGroupBuilding(hmParam);
			         gmUserBuildingMappingForm.setBuildingId(strSaveBuildId);
			     }
			     else if (strOpt.equals("SECUSR") && !strOptVal.equals("") ) {   // Ajax call show only assigned users(checked in check box) to building from group and company users list 
			    	  strGrpId = strOptVal;
	                  hmParam=new HashMap();
	                  hmParam.put("GROUPNAME",strOptVal);
				      hmParam.put("USERNM", "0");
				      hmParam.put("GRPMAPPINGID", "");
				      hmParam.put("USERDEPT", strDepID);
				      hmParam.put("COMPANYID", strCompanyId);
				      hmParam.put("BUILDID", strBuildingId);
				      
				      if(!strCompanyId.equals("")==true && !strGrpId.equals("")==true ){
				      alSelectedUser = GmCommonClass.parseNullArrayList(gmUserBuildingMappingBean.fetchUserGroupMappingDetails(hmParam));  
				      }
				      if(alSelectedUser.isEmpty()==false)
				      gmUserBuildingMappingForm.setShowUser("USER");
				      gmUserBuildingMappingForm.setAlUserList(alSelectedUser); 
				      
				      if(!strBuildingId.equalsIgnoreCase("0")){
			          strResult = "";
			          for (int i = 0; i < alSelectedUser.size(); i++) {
				           hmVal = GmCommonClass.parseNullHashMap((HashMap) alSelectedUser.get(i));
				           strResult += (String) hmVal.get("PARTYID") + ",";
				         }
			         PrintWriter writer = response.getWriter();
			         writer.println(strResult);
			         writer.close();
			         return null;
				     }
			     }
			    return mapping.findForward(strDispatchTo);
   }
}