package com.globus.operations.inventory.actions;


import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.AppError;
import com.globus.common.actions.GmAction;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.jms.consumers.beans.GmTransSplitBean;
import com.globus.operations.inventory.beans.GmRerunTxnSplitBean;
import com.globus.operations.inventory.forms.GmRerunTxnSplitForm;



public class GmRerunTxnSplitAction extends GmDispatchAction{

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**
	   * fetchRerunTxnDetails  This method is used to fetch the transaction  types from code lookup table for PMT-36676
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward  
	   */
	  public ActionForward fetchRerunTxnDetails(ActionMapping actionMapping, ActionForm actionForm,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	    instantiate(request, response);
	    
       // Form Intitalization
	    GmRerunTxnSplitForm gmRerunTxnSplitForm = (GmRerunTxnSplitForm) actionForm;
	    gmRerunTxnSplitForm.loadSessionParameters(request);
      // Bean Initalization
	   GmRerunTxnSplitBean gmRerunTxnSplitBean = new GmRerunTxnSplitBean(getGmDataStoreVO());
	   gmRerunTxnSplitForm.setAlTxnTypeList(GmCommonClass.parseNullArrayList(GmCommonClass
	           .getCodeList("SPLIT", getGmDataStoreVO()))); 
	    
	    return actionMapping.findForward("gmRerunTxnSplit");
	  }
	  
	  
	/**
	 * processSplitTransactionByLocation
	 * @description Used to validate and Split the Transaction based on the transaction type for PMT-36676
	 * @param mapping
	 *            ,form,request,response
	 * @return
	 * @throws Exception
	 */
	public ActionForward processSplitTransactionByLocation(ActionMapping actionMapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
 
		instantiate(request, response);
		
		GmRerunTxnSplitForm gmRerunTxnSplitForm = (GmRerunTxnSplitForm) form;
		gmRerunTxnSplitForm.loadSessionParameters(request);
        GmRerunTxnSplitBean gmRerunTxnSplitBean = new GmRerunTxnSplitBean(getGmDataStoreVO());
		
		
		String strSessCompanyLocale =
		        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
		
		GmResourceBundleBean gmResourceBundleBeanlbl =
		        GmCommonClass.getResourceBundleBean("properties.labels.operations.inventory.GmRerunTxnSplit",
		            strSessCompanyLocale);
		
	    HashMap hmParam = new HashMap();
	    HashMap hmResult = new HashMap();
		
		String strRerunValidationMsg = "";
		String strOutTxnId = "";
		
	    String strTransactionId = GmCommonClass.parseNull(gmRerunTxnSplitForm
				.getStrTxnId());
		String strTransactionType = GmCommonClass.parseNull(gmRerunTxnSplitForm
				.getStrTxnType());
		
        log.debug(" saveRerunTxn strTransactionId is "+strTransactionId);
        log.debug(" saveRerunTxn strTransactionType is "+strTransactionType);      
		
		hmParam.put("TXNID", strTransactionId);
		hmParam.put("TXNTYPE", strTransactionType);
		String strMsg = "";

		//Method call to validate the transaction from rerun split screen
		hmResult = gmRerunTxnSplitBean.processSplitTransactionByLoc(hmParam);
		
		strRerunValidationMsg = GmCommonClass.parseNull((String)hmResult.get("VALIDATIONMSG"));
	    strOutTxnId = GmCommonClass.parseNull((String)hmResult.get("TRANSID"));
	    if(strRerunValidationMsg.equalsIgnoreCase("yes")){
			strMsg = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_SUCCESS_MSG1")) + "\t" + strOutTxnId + "\t" + GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_SUCCESS_MSG2")) ;

	    }else{
	    	strMsg = strRerunValidationMsg;
	    }
		    
		response.setContentType("text/plain");
		PrintWriter pw = response.getWriter();
		if (!strMsg.equals("")) {
			pw.write(strMsg);// return error message if validations throws while splitting the transaction else response will be empty
		}
		pw.flush();
		return null;
	}
}
