package com.globus.operations.inventory.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmCPCSetupBean;
import com.globus.operations.inventory.forms.GmCPCSetupForm;

public class GmCPCSetupAction extends GmAction
{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        try {
            	instantiate(request,response);
        	GmCPCSetupForm gmCPCSetupForm = (GmCPCSetupForm)form;
        	gmCPCSetupForm.loadSessionParameters(request);
        	GmCPCSetupBean gmCPCSetupBean = new GmCPCSetupBean(getGmDataStoreVO());
        	GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
        	GmLogonBean gmLogonBean	= new GmLogonBean(getGmDataStoreVO());
        
            Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
               

        String strOpt = gmCPCSetupForm.getStrOpt();
        String strCpcId = gmCPCSetupForm.getCpcId();
        String strCpcAddr = gmCPCSetupForm.getCpcAddr();
        String strMsg = "";

       
        HashMap hmTemp = new HashMap();
        HashMap hmParam = new HashMap ();
        HashMap hmValues = new HashMap();
        ArrayList alCpcList = new ArrayList();
        ArrayList alLogReasons = new ArrayList();
        ArrayList alActiveFl = new ArrayList();
        
       // log.debug(GmCommonClass.getHashMapFromForm(gmCPCSetupForm));

        if (strOpt.equals("save") || strOpt.equals("add"))
        {
			// to save the address line by line entered by using ENTER keyword
        	// gmCPCSetupForm.setCpcAddr(strCpcAddr.replaceAll("\n", "||CHR(13)||"));
        	

        	
            hmParam = GmCommonClass.getHashMapFromForm(gmCPCSetupForm);

/*            if(strOpt.equals("add") && gmCPCSetupBean.getCpcCount(gmCPCSetupForm.getCpcName())>0)
			{
				 throw new AppError("CPC Name already available, Please Choose dropdown and update for further", "", 'E');
			}*/
            strCpcId = gmCPCSetupBean.saveCPCDtls(hmParam);
            if(!strCpcId.equals("")){
            	strMsg = "Record Saved Successfully";
            }
            
            gmCPCSetupForm.setCpcId(strCpcId);
            
            gmCPCSetupForm.setStrMsg(strMsg);
            
            hmValues = gmCPCSetupBean.loadEditCpcInfo(strCpcId);
            gmCPCSetupForm = (GmCPCSetupForm)GmCommonClass.getFormFromHashMap(gmCPCSetupForm,hmValues);
        }else if (strOpt.equals("edit")){
            hmValues = gmCPCSetupBean.loadEditCpcInfo(strCpcId);
            gmCPCSetupForm = (GmCPCSetupForm)GmCommonClass.getFormFromHashMap(gmCPCSetupForm,hmValues);
        }
        
       // hmTemp.put("FILTER_INACTIVE", "NO");
        alCpcList = gmCPCSetupBean.loadCpcList();        
        gmCPCSetupForm.setAlCpcList(alCpcList);
        
        gmCPCSetupForm.setAlActiveFl(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YES/NO")));
        //      displaying comments
        if(!strCpcId.equals("0") || !strCpcId.equals("")){
           alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strCpcId,"1295"));
           gmCPCSetupForm.setAlLogReasons(alLogReasons);
       }
        
        

        }
        catch(Exception exp){
            exp.printStackTrace();
            throw new AppError(exp);
        }
        
        return mapping.findForward("GmCPCSetup");
    }

}
