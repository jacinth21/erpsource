package com.globus.operations.inventory.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.inventory.beans.GmLocationTransferBean;
import com.globus.operations.inventory.forms.GmLocationTransferForm;
import com.globus.operations.itemcontrol.beans.GmInitiateTransferBean;
import com.globus.operations.itemcontrol.forms.GmInitiateTransferForm;





public class GmLocationTransferAction extends GmDispatchAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());

	  /**
	   * fetchReplenishment - This Method is used to Initiate Replenishment
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   */
	  public ActionForward fetchBuildingInfo(ActionMapping actionMapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	    instantiate(request, response);
	    GmLocationTransferForm gmLocationTransferForm =  (GmLocationTransferForm) form;
	    gmLocationTransferForm.loadSessionParameters(request);
	    GmLocationTransferBean gmLocationTransferBean = new GmLocationTransferBean(getGmDataStoreVO());
	    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
	    ArrayList alWareHouse = new ArrayList();
	    alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouse(""));
	    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
	    gmLocationTransferForm.setAlWareHouse(alWareHouse);
	    return actionMapping.findForward("gmLocationTransfer");
	  }
	
	  /**
	   * saveReplenishment - This method is used to Save Bulk Location Transfer
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   */
	  public ActionForward saveReplenishment(ActionMapping actionMapping, ActionForm actionForm,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	    instantiate(request, response);
	    GmLocationTransferBean gmLocationTransferBean = new GmLocationTransferBean(getGmDataStoreVO());
	    GmLocationTransferForm gmLocationTransferForm = (GmLocationTransferForm) actionForm;
	    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
	    String strInputString = GmCommonClass.parseNull(request.getParameter("v_str"));
	    String strWareHouseType = GmCommonClass.parseNull(request.getParameter("warehouseid"));
	    log.debug("strWareHouseType--"+strWareHouseType);
	    String strUserID =
	        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
	    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
	    String strResult = "";
	    HashMap hmParam = new HashMap();
	    hmParam.put("VSTRING", strInputString);
	    hmParam.put("USERID", strUserID);
	    hmParam.put("WHTYPE", strWareHouseType);
	    ArrayList alList = new ArrayList();
	    strResult = GmCommonClass.parseNull(gmLocationTransferBean.saveBulkLocation(hmParam));
	    ArrayList alWareHouse = new ArrayList();
	    alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouse(""));
	    gmLocationTransferForm.setAlWareHouse(alWareHouse);
	    response.setContentType("text/plain");
	    PrintWriter pw = null;
	    try {
	      pw = response.getWriter();
	    } catch (IOException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    }
	    if (strResult == null)
	      pw.write("Fail");
	    else
	      pw.write(strResult);
	    pw.flush();
	    return null;
	  }
	  /**
	   * fetchPartDetails -- This method is used to Fetch All Locations from partNum .(Ajax call
	   * request)
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return org.apache.struts.action.ActionForward
	   */
	  public ActionForward fetchPartDetails(ActionMapping actionMapping, ActionForm actionForm,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	    String strLocationID = "";
	    String strLocQty = "";
	    String strReturn = "";
	    String strLocCode = "";
	    String strWarehouseNm = "";
	    String strValid ="";
	    instantiate(request, response);
	    // Bean Initalization
	    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    hmParam.put("PARTNUM", request.getParameter("partNum"));
	    hmParam.put("WAREHOUSEID", request.getParameter("warehouseid"));
	    hmParam.put("STATUSNUM", "93310");
	    hmParam.put("CHKCURRQTYFLAG", "LOCTRANS");
	    ArrayList alReturn =
	        GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchInvLocationPartMapping(hmParam));
	    for (int i = 0; i < alReturn.size(); i++) {
	      HashMap locMap = (HashMap) alReturn.get(i);
	      strLocationID = GmCommonClass.parseNull((String) locMap.get("LID"));
	      strLocCode = GmCommonClass.parseNull((String) locMap.get("LCD"));
	      	      strReturn += strLocCode + "_" + strLocationID + "|";
	    }
	    response.setContentType("text/plain");
	    PrintWriter pw = null;
	    try {
	      pw = response.getWriter();
	    } catch (IOException e) {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	    }
	    if (strReturn == null)
	      pw.write("Fail");
	    else
	      pw.write(strReturn);
	    pw.flush();
	    return null;
	  }

}
