package com.globus.operations.inventory.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.inventory.beans.GmInvSetPickBean;
import com.globus.operations.inventory.forms.GmPendingPickSetForm;

public class GmPendingPickSetAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  HashMap hmParam = new HashMap();
  HashMap hmTemplateParam = new HashMap();
  HashMap hmAccess = new HashMap();
  String strUserID = "";
  String strAccessFl = "";

  /**
   * loadPendingPickSets
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadPendingPickSets(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPickSetForm gmPendingPickSetForm = (GmPendingPickSetForm) actionForm;
    // Bean Initialization
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvSetPickBean gmInvSetPickBean = new GmInvSetPickBean(getGmDataStoreVO());

    gmPendingPickSetForm.loadSessionParameters(request);

    ArrayList alSetPendingPick = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmPendingPickSetForm);

    String strParamType = GmCommonClass.parseNull((String) hmParam.get("PTYPE"));
    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // get the access permission
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007")); // Location Part Mapping Supervisor
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    hmTemplateParam.put("SCREEN_TYPE", strParamType);
    gmPendingPickSetForm.setAccessFlag(strAccessFl);

    alSetPendingPick =
        GmCommonClass.parseNullArrayList(gmInvSetPickBean.fetchPendingPickSets(hmParam));

    // Get XML Grid Data
    hmTemplateParam.put("TEMPLATE_NAME", "GmPendingPickSets.vm");
    hmTemplateParam.put("TEMPLATE_PATH", "operations/inventory/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    // XML String
    String strXmlString = getXmlGridData(alSetPendingPick, hmTemplateParam);
    gmPendingPickSetForm.setGridXmlData(strXmlString);

    return actionMapping.findForward("GmPendingPickSetList");
  }

  /**
   * loadLoanerSetsDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadLoanerSetsDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPickSetForm gmPendingPickSetForm = (GmPendingPickSetForm) actionForm;
    // Bean Initialization
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvSetPickBean gmInvSetPickBean = new GmInvSetPickBean(getGmDataStoreVO());

    gmPendingPickSetForm.loadSessionParameters(request);

    ArrayList alSetPendingPickLoaner = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmPendingPickSetForm);

    String strLocationType = GmCommonClass.parseNull((String) hmParam.get("LOCATIONTYPE"));
    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // get the access permission.
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007")); // Location Part Mapping Supervisor
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    hmTemplateParam.put("LOCATIONTYPE", strLocationType);
    gmPendingPickSetForm.setAccessFlag(strAccessFl);

    alSetPendingPickLoaner =
        GmCommonClass.parseNullArrayList(gmInvSetPickBean.fetchPickRequestDtls(hmParam));

    // Get XML Grid Data
    hmTemplateParam.put("TEMPLATE_NAME", "GmPendingPickSetDtls.vm");
    hmTemplateParam.put("TEMPLATE_PATH", "operations/inventory/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    // XML String
    String strXmlString = getXmlGridData(alSetPendingPickLoaner, hmTemplateParam);
    gmPendingPickSetForm.setGridXmlData(strXmlString);

    return actionMapping.findForward("GmPendingPickSetDtls");
  }

  /**
   * loadConsignedSetsDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadConsignedSetsDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPickSetForm gmPendingPickSetForm = (GmPendingPickSetForm) actionForm;
    // Bean Initialization
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvSetPickBean gmInvSetPickBean = new GmInvSetPickBean(getGmDataStoreVO());

    gmPendingPickSetForm.loadSessionParameters(request);

    ArrayList alSetPendingPickConsigned = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmPendingPickSetForm);

    String strLocationType = GmCommonClass.parseNull((String) hmParam.get("LOCATIONTYPE"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007")); // Location Part Mapping Supervisor
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    hmTemplateParam.put("LOCATIONTYPE", strLocationType);
    gmPendingPickSetForm.setAccessFlag(strAccessFl);

    alSetPendingPickConsigned =
        GmCommonClass.parseNullArrayList(gmInvSetPickBean.fetchPickRequestDtls(hmParam));

    // Get XML Grid Data

    hmTemplateParam.put("TEMPLATE_NAME", "GmPendingPickSetDtls.vm");
    hmTemplateParam.put("TEMPLATE_PATH", "operations/inventory/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    // XML String
    String strXmlString = getXmlGridData(alSetPendingPickConsigned, hmTemplateParam);
    gmPendingPickSetForm.setGridXmlData(strXmlString);

    return actionMapping.findForward("GmPendingPickSetDtls");
  }

  /**
   * loadInhouseLoanerSetsDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadInhouseLoanerSetsDtls(ActionMapping actionMapping,
      ActionForm actionForm, HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPickSetForm gmPendingPickSetForm = (GmPendingPickSetForm) actionForm;
    // Bean Initialization
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvSetPickBean gmInvSetPickBean = new GmInvSetPickBean(getGmDataStoreVO());

    gmPendingPickSetForm.loadSessionParameters(request);

    ArrayList alSetPendingPickInhouse = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPendingPickSetForm);

    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLocationType = GmCommonClass.parseNull((String) hmParam.get("LOCATIONTYPE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    HashMap hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007")); // Location Part Mapping Supervisor
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    hmTemplateParam.put("LOCATIONTYPE", strLocationType);
    gmPendingPickSetForm.setAccessFlag(strAccessFl);

    alSetPendingPickInhouse =
        GmCommonClass.parseNullArrayList(gmInvSetPickBean.fetchPickRequestDtls(hmParam));

    // Get XML Grid Data

    hmTemplateParam.put("TEMPLATE_NAME", "GmPendingPickSetDtls.vm");
    hmTemplateParam.put("TEMPLATE_PATH", "operations/inventory/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    // XML String
    String strXmlString = getXmlGridData(alSetPendingPickInhouse, hmTemplateParam);
    gmPendingPickSetForm.setGridXmlData(strXmlString);

    return actionMapping.findForward("GmPendingPickSetDtls");
  }

  /**
   * savePendingPickSetStatus
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward savePendingPickSetStatus(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    // Form Initialization
    GmPendingPickSetForm gmPendingPickSetForm = (GmPendingPickSetForm) actionForm;
    // Bean Initialization
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmInvSetPickBean gmInvSetPickBean = new GmInvSetPickBean(getGmDataStoreVO());

    gmPendingPickSetForm.loadSessionParameters(request);

    ArrayList alSetPendingPickDtls = new ArrayList();
    String strOpt = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmPendingPickSetForm);

    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserID,
            "3250007")); // Location Part Mapping Supervisor
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    hmTemplateParam.put("ACCESSFLAG", strAccessFl);
    gmPendingPickSetForm.setAccessFlag(strAccessFl);

    if (strOpt.equals("Save")) {
      gmInvSetPickBean.savePendingPickSets(hmParam);
      gmPendingPickSetForm.setButtonName("Cancel"); // To show the button name to Cancel
    }

    alSetPendingPickDtls =
        GmCommonClass.parseNullArrayList(gmInvSetPickBean.fetchPickRequestDtls(hmParam));

    // Get XML Grid Data

    hmTemplateParam.put("TEMPLATE_NAME", "GmPendingPickSetDtls.vm");
    hmTemplateParam.put("TEMPLATE_PATH", "operations/inventory/templates");
    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    // XML String
    String strXmlString = getXmlGridData(alSetPendingPickDtls, hmTemplateParam);
    gmPendingPickSetForm.setGridXmlData(strXmlString);

    return actionMapping.findForward("GmPendingPickSetDtls");
  }

  /**
   * @getXmlGridData xml content generation for Field Sales lock grid
   * @author
   * @param array list and hash map
   * @return String
   */

  private String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alDropDownDistName = new ArrayList();
    ArrayList alDropDownAuditor = new ArrayList();
    ArrayList alDropDownDistStatus = new ArrayList();
    String strTemplateName = "";
    String strTemplatePath = "";

    strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE_NAME"));
    strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE_PATH"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));

    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.inventory.GmPendingPickSetDtls", strSessCompanyLocale));
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setTemplateName(strTemplateName);

    return templateUtil.generateOutput();
  }
}
