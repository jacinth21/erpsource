package com.globus.operations.inventory.actions;


import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.ObjectOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.inventory.beans.GmLocationBean;
import com.globus.operations.inventory.forms.GmFieldSalesWarehouseForm;
import com.globus.operations.loaners.beans.GmSetLotMasterBean;
import com.globus.operations.loaners.forms.GmSetLotMasterForm;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmJasperReport;

public class GmFieldSalesWarehouseRptAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /*
   * The below method is used to load - Field Sales Warehouse reports
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmFieldSalesWarehouseForm gmFieldSalesWarehouseForm = (GmFieldSalesWarehouseForm) form;
    gmFieldSalesWarehouseForm.loadSessionParameters(request);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmLocationBean gmLocationBean = new GmLocationBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    HashMap hmLocationType = new HashMap();
    HashMap hmFilter = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmDataVal = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alLocationType = new ArrayList();
    ArrayList alFieldSales = new ArrayList();
    ArrayList alQtySearchList = new ArrayList();
    ArrayList alAccOutType = new ArrayList();
    ArrayList alAccounts = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alSystemList = new ArrayList();
    ArrayList alDownloadType = new ArrayList();
    ArrayList alAccRpt = new ArrayList();
    String strOpt = "";
    String strApplDateFmt = "";
    String strTodayDt = "";
    int currMonth = 0;
    String strUserId = "";
    String strxml = "";
    String strForward = "";
    String strLocationType = "";
    strForward = "";
   
	Calendar cal = Calendar.getInstance();
    String strApplnDateFormat = getGmDataStoreVO().getCmpdfmt();
    SimpleDateFormat dateFormat = new SimpleDateFormat(strApplnDateFormat);
    
    alQtySearchList = GmCommonClass.getCodeList("OPERS");
    alAccOutType = GmCommonClass.getCodeList("ACCOUT");
    gmFieldSalesWarehouseForm.setAlQtySearchList(alQtySearchList);
    gmFieldSalesWarehouseForm.setAlAccOutType(alAccOutType);
    alDownloadType = GmCommonClass.getCodeList("ACNTRT");
    gmFieldSalesWarehouseForm.setAlDownloadType(alDownloadType);
    alSystemList = gmPricingRequestBean.loadSystemsList();
    hmReturn.put("SYSLIST", alSystemList);
    request.setAttribute("hmReturn", hmReturn);
    
    String strAcctPath = GmCommonClass.parseNull(GmCommonClass.getString("GMACCOUNTS"));

    // alLocationType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LOCTP"));

    // The following code added for Getting the Distributor, Sales Rep and Employee List based on
    // the PLANT too if the Selected Company is EDC
    String strCompanyId = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    String strCompanyPlant =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "FIELD_SALES_RPT"));

    if (strCompanyPlant.equals("Plant")) {
      hmFilter.put("DISTFILTER", "COMPANY-PLANT");
    }
    // PMT-16379 - Adding ICS Dist type in the below Exclusion Dist Type Parameter. 
    hmFilter.put("EXCL_DISTTYP", "26240143,70105");
    
    hmParam = GmCommonClass.getHashMapFromForm(gmFieldSalesWarehouseForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    log.debug("strOpt1111111111111============= " + strOpt);
    hmFilter.put("SCREEN", strOpt);
    alFieldSales = GmCommonClass.parseNullArrayList(gmCustomerBean.getDistributorList(hmFilter));
    
     String strExportType = GmCommonClass.parseNull((String) hmParam.get("DOWNLOADTYPE"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strVmFile = "GmFieldSalesWarehouseRpt.vm";
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    hmParam.put("SESSDATEFMT", strApplnDateFmt);
    strTodayDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    currMonth = gmCal.getCurrentMonth() - 1;
    String strFromDT = "";
    strFromDT = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    strLocationType = GmCommonClass.parseNull((String) hmParam.get("LOCATIONTYPE"));
    gmFieldSalesWarehouseForm.setAlAccounts(alAccounts);
    log.debug("strLocationType============ "+strLocationType);
    /*
     * if(strLocationType.equals("")){ hmParam.put("LOCATIONTYPE","4000338");// Field sales
     * gmFieldSalesWarehouseForm.setLocationType("4000338"); }
     */
    if (strFromDT.equals("")) {
      strFromDT = gmCal.getAnyDateOfMonth(currMonth, 1, strApplnDateFmt);
    }
    if (strTodayDt.equals("")) {
      strTodayDt = GmCommonClass.parseNull(gmCal.getCurrentDate(strApplnDateFmt));;
    }
    // Showing Filed Sales and Account Location Types only
    gmFieldSalesWarehouseForm.setAlFieldSales(alFieldSales);
    gmFieldSalesWarehouseForm.setAlAccounts(alAccounts);
    if (strOpt.equals("4000338") || strOpt.equals("56005")) {
    	 strForward ="GmFieldSalesWarehouseRpt";
    	log.debug("strOpt============= "+strOpt);
      gmFieldSalesWarehouseForm.setFsQty(true);
      // to set the default Qty. values greater then 0.
      gmFieldSalesWarehouseForm.setFsQtyVal("1"); // > - symbol (code id - 50601)
      gmFieldSalesWarehouseForm.setFsQuantity("0");
      strLocationType = strOpt;
      gmFieldSalesWarehouseForm.setLocationType(strLocationType);
      if (!strLocationType.equals("")) {
          hmLocationType.put("CODEID", strLocationType);
          hmLocationType.put("CODENM",
              GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strLocationType)));
          alLocationType.add(hmLocationType);
          gmFieldSalesWarehouseForm.setAlLocationType(alLocationType);
        }
      strLocationType =
              GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
                  getGmDataStoreVO().getCmpid());
    alAccounts = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
  	log.debug("alAccounts11============= "+alAccounts);
    gmFieldSalesWarehouseForm.setAlAccounts(alAccounts);
    }
    
    if(strOpt.equals("26230710") || strOpt.equals("70110")){
    	strForward ="GmAccountNetRptByLot";
    	alLocationType = gmSales.reportAccountCodeName("ACTYP");
    	gmFieldSalesWarehouseForm.setFsQty(true);
        // to set the default Qty. values greater then 0.
        gmFieldSalesWarehouseForm.setFsQtyVal("1"); // > - symbol (code id - 50601)
        gmFieldSalesWarehouseForm.setFsQuantity("0");
        String strCompanyLocale =
                GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
        GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale); 
        String strARReportByDealerFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));
        log.debug("strARReportByDealerFlag============= "+strARReportByDealerFlag);
        if(strARReportByDealerFlag.equals("YES")){
           strLocationType = strOpt;
        }else{
        	 strLocationType ="70110";
        }
        strOpt = strLocationType;
        gmFieldSalesWarehouseForm.setLocationType(strLocationType);
        	strLocationType =
                GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
                    getGmDataStoreVO().getCmpid());
      
        gmFieldSalesWarehouseForm.setAlLocationType(alLocationType);
        alAccounts = GmCommonClass.parseNullArrayList(gmSales.reportAccount(strOpt));
        System.out.println("1#"+alAccounts);
        gmFieldSalesWarehouseForm.setAlAccounts(alAccounts);
      	log.debug("alAccounts11============= "+alAccounts);

        strLocationType="";
    } 
    if(strOpt.equals("ReloadRptByLot")){
    	 strLocationType=(String)request.getParameter("locationType");
     alReturn =
  	  GmCommonClass.parseNullArrayList(gmLocationBean.fetchDealerPartMappingDtls(hmParam));
    	      log.debug("alReturn============= "+alReturn);
    	      alLocationType = gmSales.reportAccountCodeName("ACTYP");
    	      gmFieldSalesWarehouseForm.setAlLocationType(alLocationType);
    	       gmFieldSalesWarehouseForm.setLocationType(strLocationType);
    	       alAccounts = GmCommonClass.parseNullArrayList(gmSales.reportAccount(strLocationType));
    	       gmFieldSalesWarehouseForm.setAlAccounts(alAccounts);
    	      strForward ="GmAccountNetRptByLot";
    }
    if (strOpt.equals("exportReport")) {
      String strSurgDate =GmCommonClass.parseNull((String)hmParam.get("SURGERYDATE"));
      String strShipDate =GmCommonClass.parseNull((String)hmParam.get("SHIPDATE"));  	    
      String strAccOutType = GmCommonClass.parseNull((String) hmParam.get("ACCOUTPUTTYPE"));
      String strUserName = gmCommonBean.getUserName(strUserId);
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      String strString = GmCommonClass.parseNull((String) hmParam.get("STRINPUT"));
      String strAccountName = GmCommonClass.parseNull((String) hmParam.get("SEARCHACCOUNT"));
      String currentDate = dateFormat.format(cal.getTime());
      // Download report file name
      String strFileName = "acc_net_qty_by_lot_" + currentDate;
      String strFileType = "";
      String[] strInputStrSubArray;
      String[] strFinalTemp;
      String[] strInputStrArray = strString.split("\\|");
     
     // String format setname,,part,,partdesc,,lot,,qty\\|setname,,part,,partdesc,,lot,,qty
     // Split string by '\\|' symbol
    
      for (int i = 0; i < strInputStrArray.length; i++) {
      
        if (!strInputStrArray[i].equals("")) {
        // Split string by ',,' symbol and put each value into hashmap
          strFinalTemp = strInputStrArray[i].split("#@#");
          HashMap hmTemp = new HashMap();
          hmTemp.put("SET_NAME", strFinalTemp[0]);
          hmTemp.put("PART", strFinalTemp[1]);
          hmTemp.put("PART_DESC", strFinalTemp[2]);
          hmTemp.put("LOT", strFinalTemp[3]);
          hmTemp.put("QTY", strFinalTemp[4]);
          hmTemp.put("DEALERID", strFinalTemp[5]);
          hmTemp.put("DEALERNAME", strFinalTemp[6]);
          hmTemp.put("ACCOUNTID", strFinalTemp[7]);
          hmTemp.put("ACCOUNTNAME", strFinalTemp[8].substring(0, strFinalTemp[8].length() - 1));
          
        // Adding hashmap values to arraylist
          alAccRpt.add(hmTemp);
       
        }
      }

// Fetching company details
      HashMap hmCompanyAddress =
          GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), "");

      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

      GmResourceBundleBean gmPaperworkResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      hmDataVal.put("GMCOMPANYNAME",
          GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYNAME")));


      hmDataVal.put("3FLRADDRESS",
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("3FLR_ADDRESS")));

      hmDataVal.put("3FLRPHONE",
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("3FLR_PHONE")));

      hmDataVal.put("3FLRFAX",
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("3FLR_FAX")));
               
      hmDataVal.put("REQUESTEDBY", strUserName);
      hmDataVal.put("SURGDATE", strSurgDate);
      hmDataVal.put("SHIPDATE", strShipDate);
      hmDataVal.put("ACCT_NAME", strAccountName);

       GmJasperReport gmJasperReport = new GmJasperReport();
       gmJasperReport.setRequest(request);
       gmJasperReport.setResponse(response);


      if (strExportType.equals("106932")) { // 106932 -Excel download
        strFileType = "EXCEL";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename="+strFileName+".xls");
        strForward = "GmAccountRptExcel";
        
        request.setAttribute("HMDATAVAL", hmDataVal);
        request.setAttribute("ALACCRPT", alAccRpt);
        request.setAttribute("SURGDATE", strSurgDate);
        request.setAttribute("SHIPDATE", strShipDate);
        request.setAttribute("ACCOUTTYPE", strAccOutType);
      }else{
    	gmJasperReport.setJasperReportName("/GmJPAccNetPrint.jasper");
    	
    	if (strExportType.equals("106933")) {//106933 -PDF download
        strFileType = "PDF";
      } else if (strExportType.equals("106934")) {//106934 -RTF download
        strFileType = "RTF";
      }
      
       gmJasperReport.setHmReportParameters(hmDataVal);
       gmJasperReport.setReportDataList(alAccRpt);
       gmJasperReport.exportJasperReportByType(strFileName, strFileType);
       ObjectOutputStream sendStream = null;
       sendStream = new ObjectOutputStream(response.getOutputStream());
       sendStream.close();
       strForward = "GmAccountNetRptByLot";
      }
    }
    if(strOpt.equals("ReloadRpt")) { 
    	 strLocationType=(String)request.getParameter("locationType");
      alReturn =
          GmCommonClass.parseNullArrayList(gmLocationBean.fetchFieldSalesPartMappingDtls(hmParam));
      log.debug("alReturn============= "+alReturn);
      hmLocationType.put("CODEID", strLocationType);
      hmLocationType.put("CODENM",
          GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strLocationType)));
      alLocationType.add(hmLocationType);
      gmFieldSalesWarehouseForm.setAlLocationType(alLocationType);
      gmFieldSalesWarehouseForm.setLocationType(strLocationType);
      alAccounts = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
      gmFieldSalesWarehouseForm.setAlAccounts(alAccounts);
      strForward ="GmFieldSalesWarehouseRpt";
    } else if (strOpt.equals("QtyLog")) {
    	
    	String locationType=GmCommonClass.parseNull(request.getParameter("locationType"));
    	String contrl_num=GmCommonClass.parseNull(request.getParameter("lotNum"));
    	if (contrl_num.equals("NOLOT")) {
            contrl_num = "NOC#";
        }
        hmParam.put("lot", contrl_num);

        // default to load all the data. so, commented the date filter
      /*
       * hmParam.put("FROMDT", strFromDT); hmParam.put("TODT", strTodayDt);
       */
        
    	if(locationType.equals("26230710") || locationType.equals("70110"))
    	{
    		alReturn = GmCommonClass.parseNullArrayList(gmLocationBean.fetchSetLocationLog(hmParam));    	    
    	}
    	
    	else
    	{
    		alReturn = GmCommonClass.parseNullArrayList(gmLocationBean.fetchInvLocationLog(hmParam));
      
    	}
      strForward = "GmFieldSalesWarehouseLogRpt";
      strVmFile = "GmFieldSalesWarehouseLogRpt.vm";
    } else if(strOpt.equals("locType")){
  	  String strJSON = "";
      String locationId=(String)request.getParameter("locationId");
	    strJSON = gmWSUtil.parseArrayListToJson(gmSales.reportAccount(locationId));
//      gmFieldSalesWarehouseForm.setAlAccounts(alAccounts);
	    if (!strJSON.equals("")) {
	      response.setContentType("text/plain");
	      PrintWriter pw = response.getWriter();
	      pw.write(strJSON);
	      pw.flush();
	      return null;
	    }	
  }
    hmParam.put("VMFILE", strVmFile);
    if (alReturn.size() != 0) {
      alReturn = applySearchCriteria(alReturn, gmFieldSalesWarehouseForm);
      log.debug("alReturn in applySearchCriteria :" + alReturn);
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));
      log.debug("strSessCompanyLocale==" + strSessCompanyLocale);

      hmParam.put("COMPANYLOCALE", strSessCompanyLocale);
      
      log.debug("hmParam="+hmParam);
      strxml = generateOutPut(hmParam, alReturn);
      log.debug("strxml="+strxml);
      
      // remove the special character
      // PMT-34783 : Part Setup With Special Char (BUG-11275)
      // Replace XML not required - commented the line
      //strxml = GmCommonClass.replaceForXML(strxml);
      
    }
    
    gmFieldSalesWarehouseForm.setGridData(strxml);
    /*
     * gmFieldSalesWarehouseForm.setToDt(strTodayDt);
     * gmFieldSalesWarehouseForm.setFromDt(strFromDT);
     */
    System.out.println("2 before foraward"+strForward);

    return mapping.findForward(strForward);
  }
  

  private String generateOutPut(HashMap hmParam, ArrayList alReturn) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strVmFile = GmCommonClass.parseNull((String) hmParam.get("VMFILE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alReturn", alReturn);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmFieldSalesWarehouseRpt",
        GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"))));
    templateUtil.setTemplateSubDir("operations/inventory/templates");
    templateUtil.setTemplateName(strVmFile);
    return templateUtil.generateOutput();
  }

  // This Method used for Appyling the Search Criteria.
  public ArrayList applySearchCriteria(ArrayList alResult,
      GmFieldSalesWarehouseForm gmFieldSalesWarehouseForm) throws AppError {
    GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
    String strOpt = GmCommonClass.parseNull(gmFieldSalesWarehouseForm.getStrOpt());
    String strFsQtyVal = GmCommonClass.parseNull(gmFieldSalesWarehouseForm.getFsQtyVal());
    String strFsQuantity = GmCommonClass.parseNull(gmFieldSalesWarehouseForm.getFsQuantity());
    if (!strOpt.equals("QtyLog")) {
      if (!strFsQtyVal.equals("0") && !strFsQuantity.equals("")) {
        gmSearchCriteria.addSearchCriteria("QTY", strFsQuantity, strFsQtyVal);
      }
    }
    gmSearchCriteria.query(alResult);
    return alResult;
  }
}
