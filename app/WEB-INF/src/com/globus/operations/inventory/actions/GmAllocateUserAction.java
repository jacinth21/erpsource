package com.globus.operations.inventory.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.inventory.beans.GmAllocationBean;
import com.globus.operations.inventory.forms.GmAllocatedForm;



public class GmAllocateUserAction extends GmAction {
  /**
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      // Form Intitalization
      GmAllocatedForm gmAllocatedForm = (GmAllocatedForm) actionForm;
      // Bean Initalization
      GmAllocationBean gmAllocationListBean = new GmAllocationBean(getGmDataStoreVO());
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code
      HashMap hmParams = new HashMap();
      ArrayList alReportList = new ArrayList();
      ArrayList alStatus = new ArrayList();
      ArrayList alUsers = new ArrayList();
      String strOpt = "";
      String strHAction = "";
      GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
      GmCommonClass gmCommonClass = new GmCommonClass();
      // Get Hidden Parameter's
      strOpt = gmCommonClass.parseNull(gmAllocatedForm.getStrOpt());
      strHAction = gmCommonClass.parseNull(gmAllocatedForm.getHaction());
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));
      // Get Inventory User's
      alUsers = gmLogonBean.getEmployeeList("300", "V"); // have to be changed
      alStatus = gmCommonClass.getCodeList("INVUS");
      gmAllocatedForm.setAlUsers(alUsers);
      gmAllocatedForm.setAlStatus(alStatus);
      hmParams = GmCommonClass.getHashMapFromForm(gmAllocatedForm);
      if (strOpt.equals("save")) {
        /* Save Record */
        gmAllocationListBean.saveAllocateuser(hmParams);
      }
      /* Fetch all records */
      alReportList = GmCommonClass.parseNullArrayList(gmAllocationListBean.fetchAllocateUsers());
      gmAllocatedForm.setAlReportList(alReportList);
      gmAllocatedForm.setStrOpt("REPORT");
      hmParams = GmCommonClass.getHashMapFromForm(gmAllocatedForm);
      gmAllocatedForm =
          (GmAllocatedForm) GmCommonClass.getFormFromHashMap(gmAllocatedForm, hmParams);
      // Get XML Grid Data
      HashMap hmParamV = new HashMap();
      hmParamV.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      hmParamV.put("VMFILEPATH", "properties.labels.operations.inventory.gmInventoryAllocateUser");
      hmParamV.put("TEMPLATE", "GmInventoryAllocateUser.vm");
      hmParamV.put("TEMPLATEPATH", "operations/inventory/templates");
      hmParamV.put("RLIST", alReportList);
      hmParamV.put("RUSERS", alUsers);
      hmParamV.put("RSTATUS", alStatus);
      // XML String
      String strXmlString = gmAllocationListBean.getXmlGridData_AllocateUser(hmParamV);
      gmAllocatedForm.setGridXmlData(strXmlString);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return actionMapping.findForward("report");

  }

}
