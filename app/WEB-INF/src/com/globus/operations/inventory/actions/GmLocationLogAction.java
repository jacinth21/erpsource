package com.globus.operations.inventory.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.inventory.beans.GmInvWarehouseBean;
import com.globus.operations.inventory.forms.GmLocationLogForm;

/**
 * GmLocationLogAction.java
 * 
 * @author Rick Schmid
 */

public class GmLocationLogAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward fetchLocationLog(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();

    String strAction = "";
    String strOpt = "";
    // Form Intitalization
    GmLocationLogForm gmLocationLogForm = (GmLocationLogForm) actionForm;
    // Bean Initalization
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    GmInvWarehouseBean gmInvWarehouseBean = new GmInvWarehouseBean(getGmDataStoreVO());
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
    ArrayList alLocLogList = new ArrayList();
    ArrayList alWarehouse = new ArrayList();
    HashMap hmParams = new HashMap();
    hmParams = GmCommonClass.getHashMapFromForm(gmLocationLogForm);
    hmParams.put("APPLDATEFMT", strApplDateFmt);
    hmParams.put("WarehouseListAll", "0");
    alWarehouse = gmInvWarehouseBean.fetchInvWarehouse(hmParams);
    gmLocationLogForm.setAlWarehouseList(alWarehouse);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // Get Hidden Parameter's
    strOpt = GmCommonClass.parseNull(gmLocationLogForm.getStrOpt());
    // Code added for showing Todays date in TO date field and 14 days before from today's date in
    // FROM date field.
    String strFromDate = GmCommonClass.parseNull(gmLocationLogForm.getFromDate());
    String strToDate = GmCommonClass.parseNull(gmLocationLogForm.getToDate());
    String strSessTodaysDate = gmCalenderOperations.getCurrentDate(strApplDateFmt);

    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.getString("COUNTRYCODE"));
    if (strCountryCode.equals("en")) {
      if (strFromDate.equals("") || strToDate.equals("")) {
        strFromDate =
            strFromDate.equals("") ? gmCalenderOperations.addDays(-14, strApplDateFmt)
                : strFromDate;
        strToDate = strToDate.equals("") ? strSessTodaysDate : strToDate;
        gmLocationLogForm.setFromDate(strFromDate);
        gmLocationLogForm.setToDate(strToDate);
      }
    }

    if (strOpt.equals("load")) {
      alLocLogList = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchLocationLog(hmParams));
    }

    gmLocationLogForm.setGridXmlData(generateOutPut(alLocLogList, strSessCompanyLocale));

    return actionMapping.findForward("success");
  }

  private String generateOutPut(ArrayList alParam, String strSessCompanyLocale) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setTemplateName("GmLocationLog.vm");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmLocationLog", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("operations/inventory/templates");

    return templateUtil.generateOutput();
  }

}
