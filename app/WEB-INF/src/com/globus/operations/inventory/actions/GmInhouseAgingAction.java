package com.globus.operations.inventory.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.dashboard.beans.GmOperDashBoardBean;
import com.globus.operations.inventory.forms.GmInhouseAgingForm;

public class GmInhouseAgingAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                  // Class.
    GmInhouseAgingForm gmInhouseAgingForm = (GmInhouseAgingForm) form;
    gmInhouseAgingForm.loadSessionParameters(request);
    ArrayList alResult = new ArrayList();
    HashMap hmParamV = new HashMap();
    GmOperDashBoardBean gmOperDashBoardBean = new GmOperDashBoardBean(getGmDataStoreVO());
    // Getting Template column names from properties
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    String strRBPath = "properties.labels.common.GmInhouseAging";
    hmParamV.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParamV.put("STRVMPATH", strRBPath);

    alResult = gmOperDashBoardBean.fetchInhouseAgingSummary();
    hmParamV.put("RLIST", alResult);
    hmParamV.put("TEMPLATE", "GmInhouseAging.vm");
    hmParamV.put("TEMPLATEPATH", "/common/templates");
    String strGridXmlData = gmOperDashBoardBean.getXmlGridData(hmParamV);
    gmInhouseAgingForm.setGridXmlData(strGridXmlData);
    return mapping.findForward("gmInhouseAging");
  }

}
