// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\\beans\\GmStatusLogBean.java

package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmInsertEditBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  GmCommonClass gmCommon = new GmCommonClass();

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmInsertEditBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	  public GmInsertEditBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * fetchInsertDetail():function to fetch the Insert id and revision number for the part numbers based on the txn
	   * @throws AppError
	   * @Author Karthik
	   */
	  public HashMap fetchInsertDetail(String strTxnId) throws AppError {
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
			HashMap hmReturn = new HashMap();
			gmDBManager.setPrepareString("gm_pkg_op_fch_insert_details.gm_fetch_txn_insert_info", 3);
			gmDBManager.setString(1, strTxnId);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
			gmDBManager.execute();
			ArrayList alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
			String strStatusFl = GmCommonClass.parseNull(gmDBManager.getString(3));
			gmDBManager.close();
			hmReturn.put("ALINSERTDETAILS", alReturn);
			hmReturn.put("STATUSFL", strStatusFl);
			
			log.debug("hmReturn in fetchInsertDetail() " +hmReturn);
		    return hmReturn;
		}
	
	/**
	 * saveInsertDetail - This method will update or save the Insert Details for the specific
	 * Transaction in Insert Master Setup Screen
	 * @param hmParams - contains the InputString - which contains the Delminted string with all changed records
	 * @exception AppError
	 */
	public void saveInsertDetail(HashMap hmParams) throws AppError {
	  // Input String - Deliminted String with all changed records
	  String strInputString = GmCommonClass.parseNull((String) hmParams.get("STRINPUTSTRING"));
	  String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
	  String strVoidRowID = GmCommonClass.parseNull((String) hmParams.get("STRVOIDROWIDS"));
	  String strTxnID = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
	  log.debug("in saveInvLocationPartDetailById :");
	  
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  log.debug("strVoidRowID :" + strVoidRowID);
	  gmDBManager.setPrepareString("gm_pkg_op_sav_insert_txn.gm_sav_edit_insert_details", 4);
	  gmDBManager.setString(1, strTxnID);
	  gmDBManager.setString(2, strInputString);
	  gmDBManager.setString(3, strVoidRowID);
	  gmDBManager.setString(4, strUserID);
	  gmDBManager.execute();
	  gmDBManager.commit();
	}

}
