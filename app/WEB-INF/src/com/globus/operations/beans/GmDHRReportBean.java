package com.globus.operations.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDHRReportBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
	
	public GmDHRReportBean(GmDataStoreVO gmDataStoreVO) {
		// TODO Auto-generated constructor stub
	}
	
 /**
   * getXmlGridData- This method will display the DHR report using VM file
   * 
   * @param HashMap
   * @return String
   * @exception AppError
 */
	public String getXmlGridData(HashMap hmParam) throws AppError {

		    GmTemplateUtil templateUtil = new GmTemplateUtil();
		    String strSessCompanyLocale =
		        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
		    templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("DHRLIST"));
		    templateUtil.setDataMap("hmParam", hmParam);
		    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
		        "properties.labels.operations.GmDHRReport", strSessCompanyLocale));
		    templateUtil.setTemplateName("GmDHRReportVendor.vm");
		    templateUtil.setTemplateSubDir("operations/receiving/templates");
		    return templateUtil.generateOutput();
		  }

}
