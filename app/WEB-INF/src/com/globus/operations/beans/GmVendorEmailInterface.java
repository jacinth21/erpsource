package com.globus.operations.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;

/**
 * GmVendorEmailInterface used to send notification mail for PO/Wo Transactions
 * 
 * @author Parthiban
 *
 */
public interface GmVendorEmailInterface {

	 /**
		 * saveVendorEmail - This Methods will save the Purchase order email details
		 * @param hmParam
		 * @throws AppError
		 */

  public void saveVendorEmail(HashMap hmParam) throws AppError;


  
  /**
	 * fetchEmailDetail - This Methods will load the details for email
	 * @param hmParam
	 * @throws AppError
	 */
  
  public ArrayList fetchEmailDetail(HashMap hmParam) throws AppError;

  
}
