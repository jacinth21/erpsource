package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author jkumar This bean is for converting a part from one to another
 */
public class GmPartConversionBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger

  /**
   * Constructor will be called default company info.
   */
  public GmPartConversionBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStoreVO
   */
  public GmPartConversionBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  } // Class.

  /**
   * This method is to load details of the part which will be converted to a different part
   * 
   * @author jkumar
   * @date Feb 18, 2008
   * @param partNum
   * @param Inventory Type
   */
  public HashMap loadFromPartDetails(String strPartNum, String strInvType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmValues = new HashMap();

    gmDBManager.setPrepareString("gm_pkg_op_partconversion.gm_fch_FromPartDetails", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strInvType);
    gmDBManager.execute();
    hmValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return hmValues;
  }

  /**
   * This method is to load details of the mapped part to which conversion can happen
   * 
   * @author jkumar
   * @date Feb 18, 2008
   * @param partNum
   * @param Inventory Type
   */
  public ArrayList loadToPartDetails(String strPartNum, String strInvType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();

    gmDBManager.setPrepareString("gm_pkg_op_partconversion.gm_fch_ToPartDetails", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strInvType);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alResult;
  }

  /**
   * This method is to save details of the part conversion screen
   * 
   * @author jkumar
   * @date Feb 18, 2008
   * @param partNum
   */
  public String savePartConversionDetails(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPartConversionId = "";
    log.debug(" Values to save - " + hmParam);
    String strPartNum = (String) hmParam.get("PARTNUMBER");
    String strPartConvDesc = (String) hmParam.get("PARTCONVERSIONCOMMENTS");
    String strInputString = (String) hmParam.get("HINPUTSTR");
    String strConversionQuantity = (String) hmParam.get("TOTALCONVERSIONQTY");
    String strTotalPrice = (String) hmParam.get("TOTALPRICE");
    String strInventoryType = (String) hmParam.get("FROMINVTYPE");
    String strUserId = (String) hmParam.get("USERID");

    gmDBManager.setPrepareString("gm_pkg_op_partconversion.gm_sav_topartdetails", 8);
    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strPartConvDesc);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setInt(4, Integer.parseInt(strConversionQuantity));
    gmDBManager.setDouble(5, Double.parseDouble(strTotalPrice));
    gmDBManager.setInt(6, Integer.parseInt(strInventoryType));
    gmDBManager.setString(7, strUserId);
    gmDBManager.execute();
    strPartConversionId = gmDBManager.getString(8);
    gmDBManager.commit();

    return strPartConversionId;
  }

  /**
   * This method is to fwetch details of the part conversion screen
   * 
   * @author jkumar
   * @date Feb 18, 2008
   * @param partNum
   */
  public HashMap fetchPCDetail(final String strMAId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmPCHeader;
    RowSetDynaClass rdPCDetail = null; // hashmap for header and rowsetdyna detail.

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_partconversion.gm_ac_fch_pcdetails", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // header query 1
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // MA Detail query 2
    gmDBManager.setString(1, strMAId);
    gmDBManager.execute();

    hmPCHeader = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)); // hashmap.
    rdPCDetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    hmReturn.put("RDPCDETAIL", rdPCDetail);
    hmReturn.put("HMPCHEADER", hmPCHeader); // key HMMAHEADER and alUnselected to new hm variable.
    return hmReturn;
  }

  /**
   * This method is to fetch Inventory type list by removing PO type in it
   * 
   * @author jkumar
   * @date Feb 28, 2008
   */
  public ArrayList fetchInventoryTypeList() throws AppError {
    ArrayList alInvType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTTXY"));
    alInvType.remove(1); // removing "PO Qty" from the drop down since it is not needed for Part
                         // conversion
    return alInvType;
  }

  /**
   * This method is to generate the Part Conversion summary report
   * 
   * @author jkumar
   * @date Feb 28, 2008
   */
  public RowSetDynaClass loadPCSummary(HashMap hmConditions) throws AppError {
    String strPCId = (String) hmConditions.get("PCID");
    String strInvType = (String) hmConditions.get("TYPEID");
    String strFromPartNumber = (String) hmConditions.get("FROMPARTNUMBER");
    String strToPartNumber = (String) hmConditions.get("TOPARTNUMBER");
    String strFromDate = (String) hmConditions.get("FROMDATE");
    String strToDate = (String) hmConditions.get("TODATE");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdResult = null;

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T217.C217_PART_CONVERSION_ID PCID, T217.C205_FROM_PART_NUMBER_ID FROMPNUM, GET_PARTNUM_DESC(T217.C205_FROM_PART_NUMBER_ID) FROMPNUMDESC ");
    sbQuery
        .append(" , T218.C205_TO_PART_NUMBER_ID TOPNUM, GET_PARTNUM_DESC(T218.C205_TO_PART_NUMBER_ID) TOPNUMDESC ");
    sbQuery
        .append(" , get_code_name(T217.C901_TYPE) PCTYPE, T218.C218_CONVERSION_QTY PCQTY, get_user_name(T217.C217_CREATED_BY) CREATEDBY ");
    sbQuery.append(" FROM T217_PART_CONVERSION T217, T218_PART_CONVERSION_DETAIL T218 ");
    sbQuery.append(" WHERE T217.C217_PART_CONVERSION_ID = T218.C217_PART_CONVERSION_ID ");
    sbQuery.append(" AND T217.C217_VOID_FL IS NULL ");

    if (!strPCId.equals("")) {
      sbQuery.append(" AND T217.C217_PART_CONVERSION_ID = '");
      sbQuery.append(strPCId);
      sbQuery.append("'");
    }

    if (!strInvType.equals("") && !strInvType.equals("0")) {
      sbQuery.append(" AND T217.C901_TYPE =  ");
      sbQuery.append(Integer.parseInt(strInvType));
    }

    if (!strFromPartNumber.equals("")) {
      sbQuery.append(" AND T217.C205_FROM_PART_NUMBER_ID = '");
      sbQuery.append(strFromPartNumber);
      sbQuery.append("'");
    }

    if (!strToPartNumber.equals("")) {
      sbQuery.append(" AND T218.C205_TO_PART_NUMBER_ID  =  '");
      sbQuery.append(strToPartNumber);
      sbQuery.append("'");
    }

    if (!strFromDate.equals("")) {
      sbQuery.append(" AND TRUNC(T217.C217_CREATED_DATE) >= TO_DATE('");
      sbQuery.append(strFromDate);
      sbQuery.append("','" + getCompDateFmt() + "') ");
    }

    if (!strToDate.equals("")) {
      sbQuery.append(" AND TRUNC(T217.C217_CREATED_DATE) <= TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("', '" + getCompDateFmt() + "')  ");
    }

    log.debug("Query for part Conversion SummaryReport : " + sbQuery.toString());
    rdResult = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    gmDBManager.close();

    return rdResult;
  }
}
