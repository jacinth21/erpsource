package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.oracore.OracleType;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmVendorPortalMapBean extends GmBean {
	GmCommonClass gmCommon = new GmCommonClass();

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j
	  public GmVendorPortalMapBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmVendorPortalMapBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	/**
	 * fetchVendorPortalMap - this is used fech the vendor portal map
	 * @param partyid
	 * @return
	 * @throws AppError
	 */
	 public ArrayList fetchVendorPortalMap(String strPartyId) throws AppError {
		 ArrayList alReturn = new ArrayList();
		 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 gmDBManager.setPrepareString("gm_pkg_vendor_portal_map.gm_pkg_fch_vendor_users", 2);
		 gmDBManager.setString(1, strPartyId);
		 gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		 gmDBManager.execute();
		 alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		 gmDBManager.close();
         return alReturn;
	 }
	 
	 /**
	  * voidVendorPortalMap - This is used remove the Vendor portal map details
	  * @param hmParam
	  * @return
	  */
	 public void voidVendorPortalMap(HashMap hmParam){
		 String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		 String strPartyLinkId = GmCommonClass.parseNull((String)hmParam.get("PARTYLINKID"));
		 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		 gmDBManager.setPrepareString("gm_pkg_vendor_portal_map.gm_pkg_void_vendor_user", 2);
		 gmDBManager.setString(1, strUserId);
		 gmDBManager.setString(2, strPartyLinkId);
		 gmDBManager.execute();
		 gmDBManager.commit();
		
	 }
	  
	 /**
	  * strNameList - used to fetchVendorUserNm JSON String
	  * @param hmParam
	  * @return
	  * @throws AppError
	  */
	  public String fchNameList (HashMap hmParam) throws AppError {
		  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		  String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		  int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		  String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		  gmCacheManager.setKey(strKey);
		  gmCacheManager.setSearchPrefix(searchPrefix);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  HashMap hmdata = new HashMap();
		  hmdata.put("TYPE", strType);
		  
		  String strReturn = gmCacheManager.returnJsonString(fetchVendorUserNm(hmParam));
		
		  return strReturn; 
	  }
	  
	  /**
	   * fetchVendorUserNm  - used to fetch Vendor User Name
	   * @param hmParam
	   * @return
	   * @throws AppError
	   */
	  public String fetchVendorUserNm(HashMap hmParam) throws AppError {
		  String strPartytype="26240605";
		  StringBuffer sbQuery = new StringBuffer();
		  String strCompId = getGmDataStoreVO().getCmpid();
		  if(strCompId.equals("")){
			  strCompId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		  }
		  String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFL")); 
		  sbQuery.append("SELECT C101_PARTY_ID ID, C101_FIRST_NM ||' '|| C101_LAST_NM NAME FROM T101_PARTY WHERE C101_VOID_FL IS NULL AND C901_PARTY_TYPE = '"+strPartytype+"' ");
		  log.debug("sbQuery"+sbQuery.toString());
		  return sbQuery.toString();	  
	} 
	  
	  /**
	   * saveVendorPortalMap  - This is used Save Vendor Portal Mapping 
	   * @param hmParam
	   * @return
	   * @throws AppError
	   */
	  public void saveVendorPortalMap(HashMap hmParam) throws AppError {
		     String strFromPartyId = GmCommonClass.parseNull((String) hmParam.get("FROMVENDORID"));
		     String strToPartyId = GmCommonClass.parseNull((String) hmParam.get("TOPARTYID"));
		     String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		     GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			 gmDBManager.setPrepareString("gm_pkg_vendor_portal_map.gm_pkg_sav_vendor_user", 3);
			 gmDBManager.setString(1, strFromPartyId);
			 gmDBManager.setString(2, strToPartyId);
			 gmDBManager.setString(3, strUserId);
			 gmDBManager.execute();
			 gmDBManager.commit();
		  
	  }

}
