package com.globus.operations.beans;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerReconBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

  public GmLoanerReconBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmLoanerReconBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchDoItems - This method will load the orders in reconciliation
   * 
   * @param Hashmap hmParam
   * @return ArrayList
   * @exception AppError, ParseException
   */
  public ArrayList fetchDoItems(HashMap hmParam) throws AppError, ParseException {

    ArrayList alResult = new ArrayList();
    String strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strAccId = (strAccId.equals("0")) ? "" : strAccId;
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    strDistId = (strDistId.equals("0")) ? "" : strDistId;
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    strRepId = (strRepId.equals("0")) ? "" : strRepId;
    String strOrdId = GmCommonClass.parseNull((String) hmParam.get("ORDID"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("STARTDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("ENDDATE"));
    String strDateFormat = GmCommonClass.parseNull((String) hmParam.get("DTFMT"));

    Date dtFrom = GmCommonClass.getStringToDate(strFromDate, strDateFormat);
    Date dtTo = GmCommonClass.getStringToDate(strToDate, strDateFormat);
    // GmCommonClass.parseNull((String)hmParam.get("ENDDATE"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("strOpt"));
    StringBuffer sbQuery = new StringBuffer();
    String strCompanyId = getGmDataStoreVO().getCmpid();
    // To fetch the records for EDC entity countries based on plant
    String strFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "ORD_LOANER_FILTER"));
    String strCompPlantFilter =
        strFilter.equals("") ? strCompanyId : getGmDataStoreVO().getPlantid();

    sbQuery.append(" SELECT ORDID, PNUM, ORDER_QTY ");
    sbQuery
        .append(", DECODE(REC_FL,'N','',ETCH_ID) ETCH_ID, DECODE(REC_FL,'N','',MISSING_TXN) MISSING_TXN, REPNAME");
    sbQuery.append(" ,DISTNM, ACCNM, REC_FL, QTY FROM (");
    sbQuery.append(" SELECT ORDID, PNUM, ORDER_QTY");
    sbQuery.append(", ETCH_ID, MISSING_TXN, GET_REP_NAME(REPID) REPNAME");
    sbQuery.append(",GET_ACCOUNT_DIST_NAME(ACCID) DISTNM, GET_ACCOUNT_NAME(ACCID) ACCNM");
    // IF ORDER STATUS DROPDOWN IS SELECTED AS UNRECONCILED
    if (strOpt.equals("UNRECON")) {
      sbQuery.append(", UNRECONCILED_QTY QTY, 'N' REC_FL ");
    } else if (strOpt.equals("RECON")) {// ELSE IF ORDER STATUS DROPDOWN IS SELECTED AS RECONCILED
      sbQuery.append(", RECONCILED_QTY QTY, 'Y' REC_FL ");
    } else {// ELSE
      sbQuery.append(", ( ");
      sbQuery.append(" CASE");
      sbQuery.append(" WHEN RECONCILED_QTY = ORDER_QTY");
      sbQuery.append(" THEN RECONCILED_QTY");
      sbQuery.append(" ELSE UNRECONCILED_QTY");
      sbQuery.append(" END) QTY, (");
      sbQuery.append(" CASE");
      sbQuery.append(" WHEN RECONCILED_QTY = ORDER_QTY");
      sbQuery.append(" THEN 'Y'");
      sbQuery.append(" ELSE 'N'");
      sbQuery.append(" END) REC_FL");
    } // END IF
    sbQuery.append("       FROM");
    sbQuery.append(" (");
    sbQuery.append(" SELECT ORDDET.ORDID, ORDDET.PNUM, ORDDET.QTY ORDER_QTY");
    sbQuery
        .append(", NVL (RECONCILED.QTY, 0) RECONCILED_QTY, (ORDDET.QTY - NVL(GM_PKG_OP_FETCH_RECON.GET_ORDER_RECONCILED_QTY(ORDDET.ORDID, ORDDET.PNUM),0)) UNRECONCILED_QTY, NVL (");
    sbQuery
        .append(" RECONCILED.ETCH_ID, '') ETCH_ID, NVL (RECONCILED.MISSING_TXN, '') MISSING_TXN, ORDDET.REPID");
    sbQuery.append(" , ORDDET.ACCID");
    sbQuery.append(" FROM");
    sbQuery.append(" (");
    sbQuery.append(" SELECT ORDID, PNUM, ETCH_ID");
    sbQuery.append(" , MISSING_TXN, SUM (QTY) QTY");
    sbQuery.append(" FROM");
    sbQuery.append(" (");
    sbQuery
        .append(" SELECT t413.c413_ref_id ORDID, NVL (t413.c205_rec_part_number_id, t413.c205_part_number_id)");
    sbQuery.append(" PNUM, t413.c413_item_qty QTY, t504.c504a_etch_id ETCH_ID");
    sbQuery.append(" , t412.c412_inhouse_trans_id MISSING_TXN");
    sbQuery
        .append(" FROM t412_inhouse_transactions t412, t413_inhouse_trans_items t413, t504a_loaner_transaction");
    sbQuery.append(" t504a, t504a_consignment_loaner t504");
    sbQuery.append(" WHERE t412.c412_inhouse_trans_id        = t413.c412_inhouse_trans_id");
    sbQuery.append(" AND t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id");
    sbQuery.append(" AND t504a.c504a_void_fl              IS NULL");
    sbQuery.append(" AND t504.c504_consignment_id          = t504a.c504_consignment_id");
    sbQuery.append(" AND t504.c504a_void_fl               IS NULL");
    sbQuery.append(" AND t413.c413_ref_id                 IS NOT NULL");
    sbQuery.append(" AND t413.c413_status_fl               = 'S' "); // SOLD
    sbQuery.append(" AND t412.c412_void_fl                IS NULL");
    sbQuery.append(" AND t413.c413_void_fl                IS NULL");
    /*
     * if(!strDistId.equals("")){// IF strDistID IS NOT NULL THEN
     * sbQuery.append(" AND T504a.C703_SALES_REP_ID IN"); sbQuery.append(" (");
     * sbQuery.append(" SELECT c703_sales_rep_id"); sbQuery.append(" FROM t703_sales_rep t703");
     * sbQuery.append(" WHERE t703.c701_distributor_id = " + strDistId); sbQuery.append(" )"); }//
     * END strDistID IF if(!strRepId.equals("")){//IF strRepId IS NOT NULL THEN
     * sbQuery.append(" AND T504a.C703_SALES_REP_ID = " + strRepId); } // END strRepId IF
     * if(!strOrdId.equals("")){//IF strOrderID IS NOT NULL THEN
     * sbQuery.append(" AND t413.c413_ref_id = '" + strOrdId + "' "); }// END strOrderID IF
     * if(!strAccId.equals("")){// IF strAccId IS NOT NULL THEN
     * sbQuery.append(" AND T504a.C704_ACCOUNT_ID = " + strAccId); }// END IF;
     * if(!strPnum.equals("")){// IF strPnum IS NOT NULL THEN
     * sbQuery.append(" AND T413.C205_PART_NUMBER_ID = '" + strPnum + "' "); }// END IF;
     */

    sbQuery.append(" )");
    sbQuery.append(" GROUP BY ORDID, PNUM, ETCH_ID");
    sbQuery.append(" , MISSING_TXN");
    sbQuery.append(" )");
    sbQuery.append(" RECONCILED, (");
    sbQuery
        .append(" SELECT T501.C703_SALES_REP_ID REPID, T501.C704_ACCOUNT_ID ACCID, T501.C501_ORDER_ID ORDID");
    sbQuery
        .append(" , T502.C205_PART_NUMBER_ID PNUM, "
            + "(SUM (T502.C502_ITEM_QTY)-GM_PKG_OP_FETCH_RECON.GET_SWAPPED_QTY(T501.C501_ORDER_ID,T502.C205_PART_NUMBER_ID) - GM_PKG_OP_FETCH_RECON.GM_CHK_RETURNS(T501.C501_ORDER_ID,T502.C205_PART_NUMBER_ID)) QTY");
    sbQuery.append(" FROM T501_ORDER T501, T502_ITEM_ORDER T502");
    sbQuery.append(" WHERE T501.C501_ORDER_ID     = T502.C501_ORDER_ID");
   // sbQuery.append(" AND T501.C501_STATUS_FL   >= 2"); // ORDERS THAT ARE CONTROLLED & SHIPPED   commented for PMT-43181
    sbQuery.append(" AND (t501.c901_order_type IS NULL"); // BILL&SHIP
    sbQuery.append(" OR t501.c901_order_type   IN (2525, 2526, 2530, 2531, 400185))"); // BACK
                                                                                       // ORDER,
                                                                                       // BILL ONLY,
                                                                                       // BILL ONLY
    // LOANER, LOAN-CONSIGN SWAP, MERGED WITH ANOTHER DO
    sbQuery.append(" AND T502.C901_TYPE            = '50301'"); // LOANER PARTS
    sbQuery.append(" AND T501.C501_VOID_FL        IS NULL");
    sbQuery.append(" AND T502.C502_VOID_FL        IS NULL");
    sbQuery.append(" AND t501.C501_DELETE_FL      IS NULL");
    sbQuery.append(" AND (T501.C901_EXT_COUNTRY_ID IS NULL");
    sbQuery.append(" OR T501.C901_EXT_COUNTRY_ID  in (select country_id from v901_country_codes))");
   // sbQuery.append(" AND t501.c501_shipping_date  IS NOT NULL");
    if (!strDistId.equals("")) { // IF strDistID IS NOT NULL THEN
      sbQuery.append(" AND T501.C703_SALES_REP_ID IN");
      sbQuery.append(" (");
      sbQuery.append(" SELECT c703_sales_rep_id");
      sbQuery.append(" FROM t703_sales_rep t703");
      sbQuery.append(" WHERE t703.c701_distributor_id =" + strDistId);
      sbQuery.append("            )");
    }// END strDistID IF
    if (!strRepId.equals("")) {// IF strRepId IS NOT NULL THEN
      sbQuery.append("    AND T501.C703_SALES_REP_ID = " + strRepId);
    }// END strRepId IF
    if (!strOrdId.equals("")) {// IF strOrderID IS NOT NULL THEN
      sbQuery.append("    AND T501.C501_ORDER_ID = '" + strOrdId + "'");
    }// END strOrderID IF
    if (!strFromDate.equals("") && !strToDate.equals("")) {// IF strFromDate IS NOT NULL AND
                                                           // strTODate IS NOT NULL THEN
      sbQuery.append(" AND T501.C501_ORDER_DATE >= to_date ('" + strFromDate + "', '"
          + strDateFormat + "')");
      sbQuery.append(" AND T501.C501_ORDER_DATE <= to_date ('" + strToDate + "', '" + strDateFormat
          + "')");
    }// END DATE IF
    if (!strAccId.equals("")) {// IF strAccId IS NOT NULL THEN
      sbQuery.append("      AND T501.C704_ACCOUNT_ID     = " + strAccId);
    }// END IF;
    if (!strPnum.equals("")) {// IF strPnum IS NOT NULL THEN
      sbQuery.append("     AND T502.C205_PART_NUMBER_ID = '" + strPnum + "'");
    }// END IF;
     // Added CompanyId and PlantId Filter
    sbQuery.append(" AND  (T501.C1900_COMPANY_ID =" + strCompPlantFilter);
    sbQuery.append(" OR  T501.C5040_PLANT_ID =" + strCompPlantFilter);
    sbQuery
        .append(" ) GROUP BY T501.C501_ORDER_ID, T502.C205_PART_NUMBER_ID, T501.C703_SALES_REP_ID");
    sbQuery.append(" , T501.C704_ACCOUNT_ID");
    sbQuery.append(" )");
    sbQuery.append(" ORDDET");
    sbQuery.append(" WHERE ORDDET.ORDID = RECONCILED.ORDID (+)");
    sbQuery.append(" AND ORDDET.PNUM  = RECONCILED.PNUM (+)");
    sbQuery.append(" )");
    if (strOpt.equals("UNRECON")) {// IF ORDER STATUS DROPDOWN IS SELECTED AS UNRECONCILED
      sbQuery.append("    WHERE UNRECONCILED_QTY > 0");
    } else if (strOpt.equals("RECON")) {// ELSE IF ORDER STATUS DROPDOWN IS SELECTED AS RECONCILED
      sbQuery.append(" WHERE RECONCILED_QTY > 0");
    }// END IF;
    sbQuery.append(")");
    log.debug("Order Fetch Query " + sbQuery.toString()); 

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alResult;
  }

  public void reconcileLoanerTrans(GmDBManager gmDBManager, String strTransId, String strInputStr,
      String strUserId) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_op_process_recon.gm_save_rec_do", 3);
    gmDBManager.setString(1, strTransId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
  }

  /**
   * loadReconReport - This method will load the Loaner Reconciliation Report
   * 
   * @param Hashmap hmParam
   * @return ArrayList
   * @exception AppError
   */

  public ArrayList loadReconReport(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    String strRep = GmCommonClass.parseNull((String) hmParam.get("REP"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("LOANERTYPE"));
    String strSetName = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQID"));
    String strstatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strDist = GmCommonClass.parseNull((String) hmParam.get("DIST"));
    String strLoanTo = GmCommonClass.parseNull((String) hmParam.get("LOANTO"));
    String strLoanToName = GmCommonClass.parseNull((String) hmParam.get("LOANTONAME"));
    String strCompDFmt = getCompDateFmt();
    String strQtType = GmCommonClass.parseNull((String) hmParam.get("TRANSTYPE"));

    String strEmId = GmCommonClass.parseNull((String) hmParam.get("TRANSTYPE"));

    String strFilter = 
    		GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "LOANER_RECON")); /*LOANER_RECON- To fetch the records for EDC entity countries based on plant*/
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    if (strLoanTo.equals("19518") && strDist.equals("0")) {
      strDist = strLoanToName;
    }
    if (strstatus.equals("1006441")) {
      strstatus = "50822";
    } else if (strstatus.equals("1006442")) {
      strstatus = "50823";
    }

    sbQuery.append(" SELECT * FROM ");
    sbQuery.append("  (SELECT t504a.c504a_consigned_to_id did,");
    sbQuery.append(" TO_CHAR(NVL(t526.C526_PRODUCT_REQUEST_DETAIL_ID,0)) reqdetid,");
    sbQuery.append(" TO_CHAR(NVL(t526.C525_PRODUCT_REQUEST_ID,0)) reqid,");
    sbQuery.append(" NVL (t525.c525_email_count, 0) emailcnt,");
    sbQuery
        .append(" DECODE(t504.C504_TYPE, '4127',gm_pkg_op_loaner.get_reconciled_flg (NVL (t526.c525_product_request_id, 0)),'N') pendingrecon, ");
    sbQuery
        .append(" DECODE(t504.C504_TYPE,'4127',get_distributor_name (t504a.c504a_consigned_to_id),DECODE (NVL(t412.C412_RELEASE_TO_ID,'0'),'0',GET_ACCOUNT_NAME ('01'),GET_USER_NAME (t412.C412_RELEASE_TO_ID))) dname,");
    sbQuery.append(" GET_REP_NAME(t504a.C703_SALES_REP_ID) repname,");
    sbQuery.append(" GET_REP_NAME(t504a.c703_ass_rep_id) assocrepname,");
    sbQuery.append(" t504b.c504a_etch_id etchid,");
    sbQuery.append(" get_set_name (t504.c207_set_id) sname,");
    sbQuery.append(" t504a.c504a_loaner_dt ldate,");
    sbQuery.append(" t504a.C504A_EXPECTED_RETURN_DT ExpRetdate,");
    sbQuery.append(" t504a.c504a_return_dt rdate,");
    sbQuery
        .append(" get_work_days_count (TO_CHAR(t504a.C504A_EXPECTED_RETURN_DT,'MM/dd/yyyy') , TO_CHAR(NVL(t504a.c504a_return_dt,t504a.C504A_EXPECTED_RETURN_DT),'MM/dd/yyyy')) DaysLate,");
    sbQuery.append(" NVL(t412.c412_inhouse_trans_id,' ') inhid,");
    sbQuery.append(" get_user_name (t412.c412_created_by) cuser,");
    sbQuery.append(" t412.c412_created_date cdate,");
    sbQuery.append(" t412.c412_status_fl sfl,");
    sbQuery.append(" DECODE(t412.c412_status_fl,'4','R','Y') consfl,");
    sbQuery.append(" gm_pkg_cm_loaner_jobs.get_jira_ticket_id(t526.c526_product_request_detail_id, t526.c525_product_request_id)  ticketid,");
    sbQuery.append(" t5010.c5010_tag_id tagid");
    sbQuery.append(" FROM t412_inhouse_transactions t412 ,");
    sbQuery.append(" t504a_loaner_transaction t504a ,");
    sbQuery.append(" t504a_consignment_loaner t504b , ");
    sbQuery.append(" t526_product_request_detail t526,");
    sbQuery.append(" t525_product_request t525,");
    sbQuery.append(" t504_consignment t504,");
    sbQuery.append(" t5010_tag t5010");
    sbQuery.append(" WHERE t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id");
    sbQuery.append(" AND t504b.c504_consignment_id          = t504a.c504_consignment_id");
    sbQuery.append(" AND t504a.c504_consignment_id          = t504.c504_consignment_id ");
    sbQuery
        .append(" AND t504a.c526_product_request_detail_id = t526.c526_product_request_detail_id(+) ");
    sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id(+) ");
    sbQuery
        .append(" AND t504.c504_CONSIGNMENT_ID            =  t5010.c5010_last_updated_trans_id(+) ");
    if (strType.equals("0")) {
      sbQuery.append(" AND t412.c412_type IN (50159,1006571)  "); // MISSING TXN
      sbQuery.append(" AND t504.C504_TYPE IN (4127,4119) "); // PRODUCT LOANER, INHOUSE LOANER
    } else if (strType.equals("4127")) {
      sbQuery.append(" AND t412.c412_type = 50159 "); // MISSING TXN
      sbQuery.append(" AND t504.C504_TYPE = 4127 "); // PRODUCT LOANER
    } else {
      sbQuery.append(" AND t412.c412_type = 1006571 "); // MISSING TXN
      sbQuery.append(" AND t504.C504_TYPE = 4119 "); // INHOUSE LOANER
    }
    // commented for PMT-862 issue
    // sbQuery.append(" AND t504b.c504a_status_fl               !=60 "); // INACTIVE LOANER
    sbQuery.append(" AND t504b.c504a_void_fl              IS NULL ");
    sbQuery.append(" AND t412.c412_void_fl                IS NULL ");
    sbQuery.append(" AND t504a.c504a_void_fl              IS NULL ");
    sbQuery.append(" AND t526.c526_void_fl                IS NULL ");
    sbQuery.append(" AND t525.c525_void_fl                IS NULL ");
    sbQuery.append(" AND t504.c504_void_fl                IS NULL ");
    sbQuery.append(" AND t5010.c5010_void_fl(+)           IS NULL ");
    sbQuery.append(" AND ( t525.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t525.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    sbQuery.append(" AND ( t504a.C1900_company_id = t525.C1900_company_id");
    sbQuery.append(" OR t525.C5040_plant_id  = t504a.C5040_plant_id)");
    sbQuery.append(" AND t525.C5040_plant_id = t504a.C5040_plant_id");

    if (strstatus.equals("50822")) { // IF strStatus is SELECTED as RECONCILED
      sbQuery.append(" AND c412_status_fl = 4 ");
    } // END IF
    if (strstatus.equals("50823")) {// IF strSTATUS is Selected as UnRECONCILED
      sbQuery.append(" AND c412_status_fl < 4 ");
    } // END IF

    if (!strDist.equals("0") && !strDist.equals("") && strType.equals("4127")) { // IF strDistID IS
                                                                                 // NOT NULL THEN
      sbQuery.append(" AND T504a.C703_SALES_REP_ID IN ");
      sbQuery.append(" (SELECT c703_sales_rep_id ");
      sbQuery.append(" FROM t703_sales_rep t703 ");
      sbQuery.append(" WHERE t703.c701_distributor_id = " + strDist);
      sbQuery.append(" ) ");
    }// END strDistID IF

    if (strType.equals("4119")) {
      if (!strLoanTo.equals("0") && !strLoanTo.equals("")) {
        sbQuery
            .append(" AND  NVL(t412.c412_release_to,T504A.C901_CONSIGNED_TO ) = DECODE(t412.c412_release_to,NULL,DECODE('"
                + strLoanTo + "' ,'19518','50170','50172'),'" + strLoanTo + "' )");
      }
      if (!strLoanToName.equals("0") && !strLoanToName.equals("")) {
        sbQuery.append(" AND NVL(t412.C412_RELEASE_TO_ID,T504A.C504A_CONSIGNED_TO_ID) = '"
            + strLoanToName + "' ");
      }
    }
    if (!strRep.equals("0") && !strRep.equals("") && strType.equals("4127")) {// IF strRepId IS NOT
                                                                              // NULL THEN
      sbQuery.append(" AND T504a.C703_SALES_REP_ID = " + strRep);
    }// END strRepId ID
    if (!strFromDate.equals("") && !strToDate.equals("")) {// IF strFromDate IS NOT NULL AND
                                                           // strTODate IS NOT NULL THEN
      sbQuery.append(" AND TRUNC(T412.C412_created_date) >= to_date('" + strFromDate + "','"
          + strCompDFmt + "') ");
      sbQuery.append(" AND TRUNC(T412.C412_created_date) <= to_date('" + strToDate + "','"
          + strCompDFmt + "') ");
    }// END DATE IF
    sbQuery.append(" ) ");
    if (!strSetName.equals("")) { // IF strSetName is not null
      sbQuery.append(" WHERE UPPER(sname) LIKE UPPER('%" + strSetName + "%') ");
    } // END IF

    sbQuery.append(" UNION ALL ");
    sbQuery.append(" SELECT * FROM ( ");
    sbQuery
        .append("SELECT t504.c504_ship_to_id did, ' ' reqdetid,  t504.c504_ref_id reqid, 0 emailcnt, ");
    sbQuery.append("'' pendingrecon, ");
    sbQuery
        .append(" DECODE(t412.c412_type,'9112',GET_USER_NAME(t412.C412_RELEASE_TO_ID),get_distributor_name (t506.c701_distributor_id)) dname, GET_REP_NAME (t506.C703_SALES_REP_ID) repname, ");
    sbQuery
        .append(" '' assocrepname,'' etchid, get_set_name (t506.c207_set_id) sname, t506.c506_created_date  ldate, t506.c506_expected_date ExpRetdate, ");
    sbQuery
        .append("t506.c506_return_date rdate, get_work_days_count (TO_CHAR (t506.c506_expected_date, ");
    sbQuery
        .append("get_rule_value('DATEFMT','DATEFORMAT')), TO_CHAR (NVL (t506.c506_return_date, t506.c506_expected_date),get_rule_value('DATEFMT','DATEFORMAT'))) ");
    sbQuery
        .append("DaysLate, NVL (t412.c412_inhouse_trans_id, ' ') inhid, get_user_name (t412.c412_created_by) cuser ");
    sbQuery
        .append(", t412.c412_created_date cdate, t412.c412_status_fl sfl, DECODE (t412.c412_status_fl, ");
    sbQuery.append("'4', 'R', 'Y') consfl, '' ticketid, '' tagid ");
    sbQuery
        .append("FROM t412_inhouse_transactions t412, t506_returns t506, t504_consignment t504 ");
    sbQuery.append("WHERE  ");
    sbQuery.append(" t504.c504_consignment_id = t506.c506_ref_id ");
    if (!strDist.equals("0") && !strDist.equals("")) {
      sbQuery.append(" AND t504.c504_ship_to_id =" + strDist);
    }
    sbQuery.append(" AND t506.c506_rma_id = t412.c412_ref_id ");
    if (strType.equals("0")) {
      sbQuery.append("AND  t412.c412_type IN ('9112','9115') "); // In-House Item and Product Loaner
                                                                 // Item missing
    } else if (strType.equals("4127")) {
      sbQuery.append("AND  t412.c412_type IN ('9115') "); // Product Loaner Item missing
    } else {
      sbQuery.append("AND  t412.c412_type IN ('9112') "); // In-House Loaner Item missing
    }
    if (strType.equals("4119")) {
      if (!strLoanTo.equals("0") && !strLoanTo.equals("")) {
        sbQuery.append(" AND NVL(t412.c412_release_to,'-9999') = '" + strLoanTo + "' ");
      }
      if (!strLoanToName.equals("0") && !strLoanToName.equals("")) {
        sbQuery.append(" AND NVL(t412.C412_RELEASE_TO_ID,'-9999') = '" + strLoanToName + "' ");
      }
    }
    if (!strFromDate.equals("") && !strToDate.equals("")) {// IF strFromDate IS NOT NULL AND
                                                           // strTODate IS NOT NULL THEN
      sbQuery.append(" AND TRUNC(T412.C412_created_date) >= to_date('" + strFromDate + "','"
          + strCompDFmt + "') ");
      sbQuery.append(" AND TRUNC(T412.C412_created_date) <= to_date('" + strToDate + "','"
          + strCompDFmt + "') ");
    }// END DATE IF
    sbQuery.append("AND t412.c412_void_fl IS NULL ");
    sbQuery.append("AND t506.c506_void_fl IS NULL ");
    sbQuery.append(" AND ( t412.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t412.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    if (strstatus.equals("50822")) { // IF strStatus is SELECTED as RECONCILED
      sbQuery.append(" AND c412_status_fl = 4 ");
    } // END IF
    if (strstatus.equals("50823")) {// IF strSTATUS is Selected as UnRECONCILED
      sbQuery.append(" AND c412_status_fl < 4 ");
    } // END IF
    sbQuery.append(" ) ");
    if (!strSetName.equals("")) {
      sbQuery.append(" WHERE upper(sname) like '%" + strSetName + "%'");
    }
    log.debug("sbQuery ss::: " + sbQuery);
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }

  public ArrayList fetchReconciledItems(String strTransId) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_fetch_recon.gm_fch_rec_lnitems", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // header
    gmDBManager.setString(1, strTransId);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }
}
