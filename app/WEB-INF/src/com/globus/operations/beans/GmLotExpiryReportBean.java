package com.globus.operations.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
/**
* GmLotExpiryReportBean : Contains the methods used for the Lot Expiry Report
* author : Agilan Singaravel
*/
public class GmLotExpiryReportBean extends GmBean {

	/**This used to get the instance of this class for enabling logging purpose*/
	Logger log = GmLogger.getInstance(this.getClass().getName());
	 
	/**
	 *  Default Constructor
	 */
	public GmLotExpiryReportBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmLotExpiryReportBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}
	
	/**
	 * fetchLotExpGraph - This method used to show the details of sterile part number in expiry report graph for 0-30
	 * days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchLotExpGraph(HashMap hmParam) throws AppError{
		
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_lot_exp_graph", 6);	
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strProjectId);
		gmDBManager.setString(3, strEluQuar);
		gmDBManager.setString(4, strFlag);
		gmDBManager.setString(5, strSearch);
		
		gmDBManager.execute();
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();
		
		return strReturn;
	}
	/**
	 * fetchTissueLotExpGraph - This method used to show the details of tissue part number in expiry report graph for 0-30
	 * days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchTissueLotExpGraph(HashMap hmParam) throws AppError{
		
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_tissue_lot_exp_graph", 6);	
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strProjectId);
		gmDBManager.setString(3, strEluQuar);
		gmDBManager.setString(4, strFlag);
		gmDBManager.setString(5, strSearch);
		
		gmDBManager.execute();
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();
		return strReturn;
	}
	
	/**
	 * fetchLotExpByRange - This method used to show the details of sterile part number in Lot expiry report for 0-30
	 * days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchLotExpByRange(HashMap hmParam) throws AppError{
		
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strdays = GmCommonClass.parseNull((String) hmParam.get("EXPDAYS"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_lot_exp_by_range", 7);	
		gmDBManager.registerOutParameter(7, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strProjectId);
		gmDBManager.setString(3, strEluQuar);
		gmDBManager.setString(4, strdays);
		gmDBManager.setString(5, strFlag);
		gmDBManager.setString(6, strSearch);
		
		gmDBManager.execute();
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(7));
		gmDBManager.close();
			
		return strReturn;
	}
	/**
	 * fetchLotExpByRange - This method used to show the details of Tissue part number in Lot expiry report for 0-30
	 * days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchTissueLotExpByRange(HashMap hmParam) throws AppError{
		
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strdays = GmCommonClass.parseNull((String) hmParam.get("EXPDAYS"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_tissue_lot_exp_by_range", 6);	
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strProjectId);
		gmDBManager.setString(3, strEluQuar);
		gmDBManager.setString(4, strFlag);
		gmDBManager.setString(5, strSearch);
		
		gmDBManager.execute();
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();
			
		return strReturn;
	}
	
	/**
	 * fetchInventoryLotDetails - This method used to show the details of part number in popup inventory details for 
	 * 0-30 days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchInventoryLotDetails(HashMap hmParam){
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_inventory_lot_details", 7);	
		gmDBManager.registerOutParameter(7, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strFlag);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strProjectId);
		gmDBManager.setString(5, strEluQuar);
		gmDBManager.setString(6, strSearch);
		gmDBManager.execute();
		
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(7));
		gmDBManager.close();
		return 	strReturn;
	}
	/**
	 * fetchFieldSalesLotDetails - This method used to show the details of part number in popup Field Sales details for 
	 * 0-30 days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchFieldSalesLotDetails(HashMap hmParam){
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_fieldsales_lot_details", 7);	
		gmDBManager.registerOutParameter(7, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strFlag);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strProjectId);
		gmDBManager.setString(5, strEluQuar);
		gmDBManager.setString(6, strSearch);
		gmDBManager.execute();
		
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(7));
		gmDBManager.close();
		return 	strReturn;
	}
	/**
	 * fetchConsignmentSetLotDetails - This method used to show the details of part number in popup Consignment set details for 
	 * 0-30 days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchConsignmentSetLotDetails(HashMap hmParam){
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_consignment_set_lot_details", 7);	
		gmDBManager.registerOutParameter(7, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strFlag);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strProjectId);
		gmDBManager.setString(5, strEluQuar);
		gmDBManager.setString(6, strSearch);
		gmDBManager.execute();
		
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(7));
		gmDBManager.close();
		return 	strReturn;
	}
	/**
	 * fetchLoanerSetLotDetails - This method used to show the details of part number in popup Loaner set details for 
	 * 0-30 days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchLoanerSetLotDetails(HashMap hmParam){
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_loaner_set_lot_details", 6);	
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strFlag);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strProjectId);
		gmDBManager.setString(5, strSearch);
		gmDBManager.execute();
		
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();
		return 	strReturn;
	}	
	/**
	 * fetchTissueLoanerSetLotDetails - This method used to show the details of part number in popup Loaner set details for 
	 * 0-30 days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchTissueLoanerSetLotDetails(HashMap hmParam){
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_tissue_loaner_set_lot_details", 6);	
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strFlag);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strProjectId);
//		gmDBManager.setString(5, strEluQuar);
		gmDBManager.setString(5, strSearch);
		gmDBManager.execute();
		
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(6));
		gmDBManager.close();
		return 	strReturn;
	}
	
	/**
	 * This method is used to format the part number based on likes
	 * 
	 * @param hmParam
	 */	
	public String getPartNumFromLikes(HashMap hmParam) throws AppError {
		
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strPartNumFormat = strPartNum;
		if (strSearch.equals("LIT")) {
			strPartNumFormat = ("^").concat(strPartNumFormat);
			strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
			strPartNumFormat = strPartNumFormat.concat("$");
		} else if (strSearch.equals("LIKEPRE")) {
			strPartNumFormat = ("^").concat(strPartNumFormat);
			strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
		} else if (strSearch.equals("LIKESUF")) {
			strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
			strPartNumFormat = strPartNumFormat.concat("$");
		} else if (strSearch.equals("")) {
			strPartNumFormat = strPartNumFormat;
		} else {
			strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
		}

		return strPartNumFormat;
	}
	/**
	 * fetchTissueLotPopupDetails - This method used to show the details of tissue part number in popup inventory
	 * ,Consignment set, Field Sales/Account, Loaner Set details for 0-30 days, 31-60 days, 61-90 days, Expired and Others
	 * 
	 * @param hmParam
	 */
	public String fetchTissueLotPopupDetails(HashMap hmParam){
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
		String strFlag = GmCommonClass.parseNull((String) hmParam.get("LOADFLG"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("STRTYPE"));
		String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strEluQuar = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
		String strSearch = GmCommonClass.parseNull((String) hmParam.get("STRPARTLITERAL"));
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_tissue_lot_popup_details", 7);	
		gmDBManager.registerOutParameter(7, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setString(2, strFlag);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strProjectId);
		gmDBManager.setString(5, strEluQuar);
		gmDBManager.setString(6, strSearch);
		gmDBManager.execute();
		
		String strReturn = GmCommonClass.parseNull(gmDBManager.getString(7));
		gmDBManager.close();
		return 	strReturn;
	}
	/**
	   * loadProjectNameList - This method to fetch the project list information based on user search
	   * 
	   * @return String
	   * @throws AppError
	   */
	  public String loadProjectNameList(HashMap hmParam, String searchPrefix) throws AppError {

	    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
	    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	    String striMaxCount = GmCommonClass.parseNull((String) hmParam.get("STRMAXCOUNT"));
	    int iMaxCount = Integer.parseInt(striMaxCount);
	    gmCacheManager.setKey(strKey);
	    gmCacheManager.setSearchPrefix(searchPrefix);
	    gmCacheManager.setMaxFetchCount(iMaxCount);
	    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
	    String strReturn = gmCacheManager.returnJsonString(loadProjectNameQueryStr(hmParam));
	    return strReturn;
	  }

	  /**
	   * loadProjectNameQueryStr - This method will fetch all the project list
	   * 
	   * @param HashMap hmParam
	   * @return ArrayList
	   * @exception AppError
	   **/
	  public String loadProjectNameQueryStr(HashMap hmParam) throws AppError {

	    StringBuffer sbQuery = new StringBuffer();

		sbQuery.append(" SELECT T202.C202_PROJECT_ID ID, T202.C202_PROJECT_ID ||' / ' ||C202_PROJECT_NM NAME ");
		sbQuery.append(" FROM T202_PROJECT T202 ");
		sbQuery.append(" WHERE c901_status_id >= '20301' ");
		sbQuery.append(" AND C202_VOID_FL IS NULL ");
	    sbQuery.append(" ORDER BY UPPER(C202_PROJECT_ID) ");
	    log.debug(" Query inside reportProject is >>> " + sbQuery.toString());

	    return sbQuery.toString();
	  }
}