/**
 * @author arajan
 */
package com.globus.operations.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmDonorInfoBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing log4j

  public GmDonorInfoBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDonorInfoBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchDonorInfo - This method is used to fetch the donor details to the screen
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchDonorInfo(String strDHRId) throws AppError {
    ArrayList alCtrlDtls = new ArrayList();
    ArrayList alDonorDtls = new ArrayList();
    HashMap hmReturn = new HashMap();


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_fch_donor_info", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDHRId);

    gmDBManager.execute();
    alDonorDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    alCtrlDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    hmReturn.put("DONORDTLS", alDonorDtls);
    hmReturn.put("CTRLDTLS", alCtrlDtls);
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * saveDonorInfo - This method is used to save the donor details to the screen
   * 
   * @param HashMap hmParam
   * @return void
   * @exception AppError
   **/
  public void saveDonorInfo(HashMap hmParam) throws AppError {

    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("DONORNO"));
    String strVendor = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));
    String strAge = GmCommonClass.parseNull((String) hmParam.get("AGE"));
    String strSex = GmCommonClass.parseZero((String) hmParam.get("SEX"));
    String strInternationalUse = GmCommonClass.parseZero((String) hmParam.get("INTERNATIONALUSE"));
    String strResearch = GmCommonClass.parseZero((String) hmParam.get("FORRESEARCH"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    strSex = (!strSex.equals("0")) ? strSex : "";
    strInternationalUse = (!strInternationalUse.equals("0")) ? strInternationalUse : "";
    strResearch = (!strResearch.equals("0")) ? strResearch : "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_op_sav_donor_info", 10);
    gmDBManager.setString(1, strDHRId);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.setString(3, strDonorNum);
    gmDBManager.setString(4, strVendor);
    gmDBManager.setString(5, strAge);
    gmDBManager.setString(6, strSex);
    gmDBManager.setString(7, strInternationalUse);
    gmDBManager.setString(8, strResearch);
    gmDBManager.setString(9, strInputString);
    gmDBManager.setString(10, strUserId);

    gmDBManager.execute();

    gmDBManager.commit();

  }

  /**
   * getXmlGridData - This method will return the Xml String
   * 
   * @param hmParamV - contains the Template and Template path
   * @return String XML String
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParamV) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParamV.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParamV.get("VMFILEPATH"));
    hmParamV.put("SCREEN", GmCommonClass.parseNull((String) hmParamV.get("SCREEN")));
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setDataMap("hmParam", hmParamV);
    templateUtil.setTemplateName(strTemplate);

    templateUtil.setTemplateSubDir(strTemplatePath);

    return templateUtil.generateOutput();
  }

  /**
   * fetchDHRTxnTypeList - Fetch the values of transaction types to the screen
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchDHRTxnTypeList(HashMap hmParam) throws AppError {
    ArrayList alDHRSplitType = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("DHRID"));
    String strLocGroup = GmCommonClass.parseNull((String) hmParam.get("LOCGRP"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_fch_dhr_split_types", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(2, strLocGroup);
    gmDBManager.setString(1, strDHRId);

    gmDBManager.execute();
    alDHRSplitType = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    log.debug("fetchDHRTxnTypeList>>>>>" + alDHRSplitType);
    gmDBManager.close();

    return alDHRSplitType;
  }

  /**
   * saveDHRSplit - Save the transaction details corresponding to the control numbers
   * 
   * @param HashMap hmParam
   * @return void
   * @exception AppError
   **/
  public void saveDHRSplit(HashMap hmParam) throws AppError {

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_sav_dhr_split", 3);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strDHRId);

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * fetchDHRSplitDtls - To fetch DHR Split control number details
   * 
   * @param HashMap hmParam
   * @return fetchDHRSplitDtls
   * @exception AppError
   **/
  public ArrayList fetchDHRSplitDtls(HashMap hmParam) throws AppError {
    ArrayList alDHRSplitDtls = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("DHRID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_fch_dhr_split_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDHRId);

    gmDBManager.execute();
    alDHRSplitDtls = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alDHRSplitDtls;
  }

  /**
   * fetchDonorInfo -
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchLotListing(String strDHRId) throws AppError {
    ArrayList alDHRLotListing = new ArrayList();
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_fch_combined_lotnums", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDHRId);

    gmDBManager.execute();
    alDHRLotListing = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    log.debug("fetchLotListing>>>>>" + alDHRLotListing);
    gmDBManager.close();

    return alDHRLotListing;
  }

  /**
   * saveDonorInfo - This method is used to save the donor master info on the screen
   * 
   * @param HashMap hmParam
   * @return void
   * @exception AppError
   **/
  public void saveDonorMaster(HashMap hmParam) throws AppError {

    String strDonorNum = GmCommonClass.parseNull((String) hmParam.get("DONORNO"));
    String strVendor = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));
    String strAge = GmCommonClass.parseNull((String) hmParam.get("AGE"));
    String strSex = GmCommonClass.parseZero((String) hmParam.get("SEX"));
    String strInternationalUse = GmCommonClass.parseZero((String) hmParam.get("INTERNATIONALUSE"));
    String strResearch = GmCommonClass.parseZero((String) hmParam.get("FORRESEARCH"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    strSex = (!strSex.equals("0")) ? strSex : "";
    strInternationalUse = (!strInternationalUse.equals("0")) ? strInternationalUse : "";
    strResearch = (!strResearch.equals("0")) ? strResearch : "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_op_sav_donor_master", 7);
    gmDBManager.setString(1, strDonorNum);
    gmDBManager.setString(2, strVendor);
    gmDBManager.setString(3, strAge);
    gmDBManager.setString(4, strSex);
    gmDBManager.setString(5, strInternationalUse);
    gmDBManager.setString(6, strResearch);
    gmDBManager.setString(7, strUserId);

    gmDBManager.execute();

    gmDBManager.commit();

  }

  /**
   * fetchDonorParamDtls - To fetch the donor parameter details
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public HashMap fetchDonorParamDtls(String strDonorNum, String strVendorId) throws AppError {

    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr.gm_fch_donor_master_info", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDonorNum);
    gmDBManager.setString(2, strVendorId);

    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));

    gmDBManager.close();

    return hmReturn;
  }

  /**
   * getRSBulkIdCnt - This method is used to fetch the bulk id count
   * 
   * @param String strDHRId
   * @return String
   * @exception AppError
   **/
  public String getRSBulkIdCnt(String strDHRId) throws AppError {
    String strRsBulkIdCnt = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_appr.get_bulk_shipment_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strDHRId);

    gmDBManager.execute();
    strRsBulkIdCnt = GmCommonClass.parseNull((String) gmDBManager.getObject(1));
    gmDBManager.close();

    return strRsBulkIdCnt;
  }

  /**
   * fetchLotNumStatus - To fetch the control number and status corresponding to the dhr id
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList fetchLotNumStatus(String strDhrId) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_appr.gm_fch_lotcode_status", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDhrId);

    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    gmDBManager.close();

    return alReturn;
  }

  /**
   * saveLotCodeStatus - Save the lot code and corresponding status
   * 
   * @param HashMap hmParam
   * @return void
   * @exception AppError
   **/
  public void saveLotCodeStatus(HashMap hmParam) throws AppError {

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strDHrId = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_bulk_dhr_appr.gm_sav_lotcode_status", 3);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strDHrId);
    gmDBManager.setString(3, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * validateCtrlNum - To validate whether the control number is already used for any other DHRs
   * 
   * @param String
   * @return HashMap
   * @exception AppError
   **/
  public String validateCtrlNum(String strScanCode, String strDhrId) throws AppError {

    String strCtrlNums = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_op_bulk_dhr_appr.get_duplicate_control_num", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.CLOB);
    gmDBManager.setString(2, strScanCode);
    gmDBManager.setString(3, strDhrId);

    gmDBManager.execute();
    strCtrlNums = GmCommonClass.parseNull(gmDBManager.getString(1));
    log.debug("strCtrlNums>>>>>>>>" + strCtrlNums);
    gmDBManager.close();

    return strCtrlNums;
  }
}
